<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * 
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}
if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/analytics.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';
require_once JPATH_LIBRARIES . '/axslibs/payment.php';
/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class Clidemo extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute() {

        $options = array();
        $options['driver']   = 'mysqli';           // Database driver name
        $options['host']     = 'localhost';        // Database host name
        /*$options['user']     = 'sandbox_dbuser1';
        $options['password'] = 'DawDnQBhMus29Hja';
        $options['database'] = 'sandbox';*/

        $options['user']     = 'xbwpms';
        $options['password'] = 'PDAnwG7ljQ4Kh7NI';
        $options['database'] = 'main_gin';
        

        $options['prefix']   = 'joom_';

        $db = JDatabaseDriver::getInstance($options);

        //$query = "SELECT * FROM sandbox.axs_daily_stats";
        $query = "SELECT * FROM axs_daily_stats";

        $db->setQuery($query);
        $results = $db->loadObjectList();

        //$query = "TRUNCATE sandbox.axs_daily_stats_min";
        $query = "TRUNCATE axs_daily_stats_min";
        $db->setQuery($query);
        $db->execute();

        foreach ($results as $result) {

            $site_visits = json_decode($result->site_visits);
            $members_new = json_decode($result->members_new);
            $membership_current = json_decode($result->membership_current);
            $membership_current = json_decode($result->membership_current);
            $membership_current = json_decode($result->membership_current);
            $revenue = json_decode($result->revenue);
            $refunds = json_decode($result->refunds);
            $reengages = json_decode($result->reengages);
            $downgrades = json_decode($result->downgrades);
            $cancels = json_decode($result->cancels);
            $cc_declines = json_decode($result->cc_declines);
            $cc_fails = json_decode($result->cc_fails);
            $cc_recoveries = json_decode($result->cc_recoveries);

            unset($site_visits->data);
            unset($members_new->data);
            unset($membership_current->current->data);
            unset($membership_current->grace_period->data);
            unset($membership_current->affiliates->data);
            unset($revenue->data);
            unset($refunds->data);
            unset($reengages->data);
            unset($downgrades->data);
            unset($cancels->data);
            unset($cc_declines->data);
            unset($cc_fails->data);
            unset($cc_recoveries->data);

            $data = new stdClass();

            $data->id = $result->id;
            $data->date = $result->date;
            $data->site_visits = json_encode($site_visits);
            $data->members_new = json_encode($members_new);
            $data->membership_current = json_encode($membership_current);
            $data->membership_current = json_encode($membership_current);
            $data->membership_current = json_encode($membership_current);
            $data->revenue = json_encode($revenue);
            $data->refunds = json_encode($refunds);
            $data->reengages = json_encode($reengages);
            $data->downgrades = json_encode($downgrades);
            $data->cancels = json_encode($cancels);
            $data->cc_declines = json_encode($cc_declines);
            $data->cc_fails = json_encode($cc_fails);
            $data->cc_recoveries = json_encode($cc_recoveries);

            $db->insertObject('axs_daily_stats_min', $data);
        }
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('Clidemo')->execute();