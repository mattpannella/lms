<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 *
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once("/var/www/files/creds.php");


/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class DatabaseBackup extends JApplicationCli {

    public $ip;

    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute() {

        if (!class_exists("AxsClients")) {
            require_once("/var/www/html/libraries/axslibs/clients.php");
        }

        if (!class_exists("AxsEncryption")) {
            require_once("/var/www/html/libraries/axslibs/encryption.php");
        }

        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_dbmanager WHERE enabled = 1 AND status LIKE 'active%'";
        $db->setQuery($query);
        $sites = $db->loadObjectList();


        $credentials = dbCreds::getCreds();
        $creds = "-h $credentials->dbhost -u $credentials->dbuser -p" . $credentials->dbpass;

        $maindir = "/mnt/axs/backup/";

        $date = date("Y_m_d_H_i_s");
        $today = date("Y_m_d");

        $logfile = fopen($maindir . "backup.log", "a");
        fwrite($logfile, "Starting backup at " . date("Y-m-d H:i:s") . "\n");
        $starttime = microtime(true);

        $filesize = 0;

        //Back up the entire database once a day.

        $db_dir = $maindir . "database/";

        $files = scandir($db_dir);
        foreach ($files as $file) {

            $ran_today = false;

            if ($file != "." && $file != "..") {
                $parts = explode("_", $file);
                $numparts = count($parts);
                $file_date = $parts[$numparts - 6] . "_" . $parts[$numparts - 5] . "_" . $parts[$numparts - 4];
                if ($file_date == $today) {
                    $ran_today = true;
                }

                if (self::too_old($file)) {
                    unlink ($db_dir . $file);
                }
            }
        }

//        if (!$ran_today) {
//
//            //Don't run until after 4am
//
//            $hour = date("H");
//            if ($hour >= 4) {
//                $dest = $db_dir . "database_$date";
//
//                //Dump the full database
//                shell_exec("mysqldump --single-transaction $creds --all-databases --set-gtid-purged=OFF > $dest");
//
//                //Compress the file
//                shell_exec("gzip -9 $dest");
//
//                //Change ownership to apache
//                shell_exec("sudo chown ubuntu:www-data $dest" . ".gz");
//
//                $filesize += filesize($dest . ".gz");
//            }
//        }

        //Setup the params
        foreach ($sites as $site) {
            //Skip disabled sites or sites with no info
            if (!$site->dbparams || !$site->enabled) {
                $site = null;
                continue;
            }

            $site->dbparams = AxsClients::decryptClientParams($site->dbparams);

        }

        foreach ($sites as $site) {
            sleep(0.5);
            //Skip disabled sites or sites with no info
            if (!$site->dbparams || !$site->enabled) {
                continue;
            }

            $dbname = $site->dbparams->dbname;

            $dir = $maindir . $dbname . "/";
            //Create a directory for the db if it doesn't exist.
            if (!file_exists("$dir")) {
                mkdir("$dir");
                shell_exec("sudo chown ubuntu:www-data $dir");
                shell_exec("sudo chmod 775 $dir");
            }

            $dest = $dir . $dbname . "_" . $date;

            //Dump it locally
            shell_exec("mysqldump -h $credentials->dbhost --single-transaction --set-gtid-purged=OFF $creds $dbname > $dest");

            //Compress the file
            shell_exec("gzip -9 $dest");

            //Change ownership
            shell_exec("sudo chown ubuntu:www-data $dest" . ".gz");

            $filesize += filesize($dest . ".gz");

            //Check for old backups

            $files = scandir($dir);

            foreach ($files as $file) {

                //ignore the directory aliases
                if ($file != "." && $file != "..") {
                    //Remove anything older than 1 month
                    if (self::too_old($file)) {
                        unlink($dir . $file);
                    }
                }
            }
        }

        $runtime = (float)(((int)((microtime(true) - $starttime) * 10)) / 10);

        fwrite($logfile, "Finished backup at " . date("Y-m-d H:i:s") . "\nExecution time: $runtime seconds\nFilesize: $filesize\n\n");
        fclose($logfile);
    }

    private function too_old($file) {
        $monthago = strtotime("-1 month");

        if (substr($file, -3) == ".gz") {
            $parts = explode("_", substr($file, 0, -3));
        } else {
            $parts = explode("_", $file);
        }

        $len = count($parts);

        //The date is always the last 6 elements.

        //site_temp_2018_04_22_07_33_45
        //vs
        //my_new_site_test_2018_04_22_07_33_45

        $year = $parts[$len-6];
        $month = $parts[$len-5];
        $day = $parts[$len-4];
        $hour = $parts[$len-3];
        $min = $parts[$len-2];
        $sec = $parts[$len-1];

        $timestamp = strtotime("$year-$month-$day $hour:$min:$sec");

        if ($timestamp < $monthago) {
            return true;
        } else {
            return false;
        }
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('DatabaseBackup')->execute();
