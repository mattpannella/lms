<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

error_reporting(1);
ini_set('display_errors', 1);
class CliICArchiveMigration extends JApplicationCli {

    public function doExecute() {
        $creds = dbCreds::getCreds();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);

        echo "Executing IC archive migration...\n";

        while ($dbs = $db_result->fetch_object()) {

            $params = AxsClients::decryptClientParams($dbs->dbparams);
            echo "Processing database $params->dbname...\n";

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            // Create the ic_h5p_contents_archive table
            $query = <<<SQL
            CREATE TABLE ic_h5p_contents_archive (
                id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                archived_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                content_id int(10) NOT NULL DEFAULT 0,
                version_number int(10) NOT NULL DEFAULT 0,
                user_id int(10) UNSIGNED NOT NULL,
                title varchar(255) NOT NULL,
                library_id int(10) UNSIGNED NOT NULL,
                parameters longtext NOT NULL,
                filtered longtext NOT NULL,
                slug varchar(127) NOT NULL,
                embed_type varchar(127) NOT NULL,
                disable int(10) UNSIGNED NOT NULL DEFAULT 0,
                content_type varchar(127) DEFAULT NULL,
                license varchar(32) DEFAULT NULL,
                authors longtext DEFAULT NULL,
                source varchar(2083) DEFAULT NULL,
                year_from int(10) UNSIGNED DEFAULT NULL,
                year_to int(10) UNSIGNED DEFAULT NULL,
                license_version varchar(10) DEFAULT NULL,
                license_extras longtext DEFAULT NULL,
                author_comments longtext DEFAULT NULL,
                changes longtext DEFAULT NULL,
                default_language varchar(32) DEFAULT NULL,
                settings text DEFAULT NULL,
                PRIMARY KEY (id)
            )
            ENGINE = INNODB,
            CHARACTER SET utf8mb4,
            COLLATE utf8mb4_unicode_520_ci;
SQL;

            $db->setQuery($query);
            $success = $db->execute();

            echo ($success ? "Successfully created the ic_h5p_contents_archive table.\n" : "Could not create the ic_h5p_contents_archive table.\n");

            // Create the ic_h5p_contents_user_data_archive table
            $query = <<<SQL
                CREATE TABLE ic_h5p_contents_user_data_archive (
                    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                    content_id int(10) UNSIGNED NOT NULL,
                    user_id int(10) UNSIGNED NOT NULL,
                    sub_content_id int(10) UNSIGNED NOT NULL,
                    data_id varchar(127) NOT NULL,
                    version_number int(10) NOT NULL DEFAULT 0,
                    data longtext NOT NULL,
                    preload tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
                    invalidate tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
                    archived_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                    started_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                    PRIMARY KEY (id)
                )
                ENGINE = INNODB,
                CHARACTER SET utf8mb4,
                COLLATE utf8mb4_unicode_520_ci;
SQL;

            $db->setQuery($query);
            $success = $db->execute();

            echo ($success ? "Successfully created the ic_h5p_contents_user_data_archive table.\n" : "Could not create the ic_h5p_contents_user_data_archive table.\n");

            // Create the ic_h5p_results_archive table
            $query = <<<SQL
            CREATE TABLE ic_h5p_results_archive (
                id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
                content_id int(10) UNSIGNED NOT NULL,
                user_id int(10) UNSIGNED NOT NULL,
                archived_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
                version_number int(10) NOT NULL DEFAULT 0,
                score int(10) UNSIGNED NOT NULL,
                max_score int(10) UNSIGNED NOT NULL,
                opened int(10) UNSIGNED NOT NULL,
                finished int(10) UNSIGNED NOT NULL,
                time int(10) UNSIGNED NOT NULL,
                PRIMARY KEY (id),
                INDEX content_user (content_id, user_id)
            )
            ENGINE = INNODB,
            CHARACTER SET utf8mb4,
            COLLATE utf8mb4_unicode_520_ci;
SQL;

            $db->setQuery($query);
            $success = $db->execute();

            echo ($success ? "Successfully created the ic_h5p_results_archive table.\n" : "Could not create the ic_h5p_results_archive table.\n");

            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliICArchiveMigration')->execute();