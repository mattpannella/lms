<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/interactiveContent.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class InteractiveSyncUsers extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        $ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        $query = "SELECT * FROM `axs_dbmanager` WHERE server_ip = '$ip' AND enabled=1 AND id = 9";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = 'localhost';         // Database host name
            $options['user']     = $params->dbuser;     // User for database authentication
            $options['password'] = $params->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $params->dbuser);
            $config->set('password', $params->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;
                        
            $this->db = $db;

            $query = $db->getQuery(true);
            $query->select('*')
                  ->from('#__users');
            $db->setQuery($query);
            
            $allUsers = $db->loadObjectList();

            foreach($allUsers as $user) {
                $userObj = JFactory::getUser($user->id);
                echo $userObj->name ."\n";
                AxsInteractiveContent::createICUser($userObj);
            }            
        }
    }
}

JApplicationCli::getInstance('InteractiveSyncUsers')->execute();