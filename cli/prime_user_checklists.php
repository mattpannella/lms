<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/checklist.php';
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
error_reporting(0);
ini_set('display_errors', 0);

class CliPrimeUserChecklists extends JApplicationCli {

    public function doExecute() {
        $creds = dbCreds::getCreds();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        // Run this for all clients
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled = 1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            /* What we need to do is grab all of the users who have incomplete checklists and make sure that all of
             * their checklist items have been auto-checked so that the new code can start with a solid foundation.
             * This will remove the performance sink from the Learner Dashboard.
             */
            $query = $db->getQuery(true);
            $query->select('id')
                  ->from('joom_users');

            $db->setQuery($query);

            try {

                echo "Updating all checklist activity items for existing checklists and associated users... This may take awhile.\n";

                $start = microtime(true);

                $users = $db->loadObjectList();

                if(!is_null($users) && count($users) > 0) {

                    foreach($users as $user) {

                        AxsChecklist::updateAllChecklistsForUser($user->id);
                    }
                } else {

                    echo "Could not find any users to update checklists for.\n\n";
                }

                $elapsed = microtime(true) - $start;
                echo "Updated all user checklists in $elapsed s.\n\n";
            } catch(Exception $ex) {

                echo $ex->getMessage();
            }
        }
    }
}

JApplicationCli::getInstance('CliPrimeUserChecklists')->execute();
