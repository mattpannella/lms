<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';


//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class TrimTitles extends JApplicationCli {

    public function doExecute() {

        $creds = dbCreds::getCreds();

        $db = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager`";
        $db_result = $db->query($query);

        
        $tables = [
            [
                'name' => 'axs_teams',
                'primary' => 'id'
            ],
            [
                'name' => 'joom_eb_events',
                'primary' => 'id'
            ],
            [
                'name' => 'joom_splms_courses',
                'primary' => 'splms_course_id'
            ],
            [
                'name' => 'joom_splms_lessons',
                'primary' => 'splms_lesson_id'
            ],
            [
                'name' => 'joom_usergroups',
                'primary' => 'id'
            ],
        ];
        $time = time();
        echo "Start Title Trimming Process {$time}\n";
        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;
            
            echo "Starting database: {$params->dbname}\n";
            //iterate through each table that potentially needs trimmed titles
            foreach($tables as $table) {
                $name = $table['name'];
                $primary_id = $table['primary'];
            
                //check for records with bad titles
                $query = $db->getQuery(true);
                $query->select('*')
                        ->from($db->quoteName($name))
                        ->where('CHAR_LENGTH(title) != CHAR_LENGTH(TRIM(title))');
            
                $db->setQuery($query);
                $results = $db->loadObjectList();
            
                if($results) {
                    foreach($results as $row) {
                        $id = $row->$primary_id;
            
                        $query = $db->getQuery(true);
            
                        $fields = [
                            $db->quoteName('title') . ' = TRIM(' . $db->quoteName('title') . ')'
                        ];
            
                        $where = [
                            $db->quoteName($primary_id) . ' = ' . $id
                        ];
                        //trim the title column
                        $query->update($db->quoteName($name))->set($fields)->where($where);
                        $db->setQuery($query);
                        $result = $db->execute();
                        if (!empty($db->getErrorMsg())) {
                            echo "Error: " . $db->getErrorMsg() . "\n";
                        } else {
                            echo "Trimmed title in {$name} record {$id}\n";
                        }
                    }
                }
            }
            $db->disconnect();
            echo "Ending database: {$params->dbname}\n";
            echo "-----------------------------------\n";
        }
        $time = time();
        echo "Finish Title Trimming Process {$time}\n";
    }
}

JApplicationCli::getInstance('TrimTitles')->execute();