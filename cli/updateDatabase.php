<?php
/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class Clidemo extends JApplicationCli {

    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute() {

        require_once('/var/www/html/libraries/axslibs/clients.php');
        require_once('/var/www/html/libraries/axslibs/encryption.php');

        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_dbmanager WHERE enabled = 1";
        $db->setQuery($query);
        $manager = $db->loadObjectList();

        foreach ($manager as $client) {
            $params = AxsClients::decryptClientParams($client->dbparams);

            echo "\n\nConnecting to " . $params->dbname . "\n\n";

            $options['driver']   = 'mysqli';
            $options['host']     = "localhost";

            $options['user']     = $params->dbuser;
            $options['password'] = $params->dbpass;
            //$options['user'] = "xbwpms";
            //$options['password'] = "3XBYhNnGbnbfbcnc";
            $options['database'] = $params->dbname;

            try {
                $db = JDatabaseDriver::getInstance($options);
                $query = "SHOW TABLE STATUS";
                $db->setQuery($query);
                $results = $db->loadObjectList();
            } catch (Exception $e) {
                echo "\nCan't connect to " . strtoupper($params->dbname) . "\n";
                continue;
            }

            $innoCount = 0;
            $samCount = 0;
            $total = 0;
            $other = 0;

            $tables = "";
            $first = true;

            foreach ($results as $result) {

                if (!$first) {
                    $tables .= ", ";
                }

                $tables .= $result->Name;

                $first = false;

                $total++;
                switch ($result->Engine) {
                    case "InnoDB":
                        $innoCount++;
                        break;
                    case "MyISAM":

                        $samCount++;
                        echo "Altering " . $result->Name . "  ";
                        try {
                            $query = "ALTER TABLE " . $result->Name . " ENGINE=InnoDB";
                            $db->setQuery($query);
                            $db->execute();

                            echo $result->Name . " altered\n";
                        } catch (Exception $e) {
                            echo strtoupper($result->Name) . " NOT ALTERED\n";
                        }
                        break;

                    default:
                        $other++;
                        //echo $result->Name . "\n";

                }
                //echo $result->Name . " " . $result->Engine . "\n";
            }
            echo "Optimizing...\n";
            $dbname = $params->dbname;
            $query = "OPTIMIZE NO_WRITE_TO_BINLOG TABLE $tables;";
            $db->setQuery($query);
            $db->execute();

            //echo "Total: $total\nMyISAM: $samCount\nInnoDB: $innoCount\nOther: $other\n";
        }
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('Clidemo')->execute();