<?php

date_default_timezone_set('America/Boise');

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
    die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

class CleanBoards extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute()
    {
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('language') .'=' . $db->quote('french'),
            $db->quoteName('splms_course_id') . '=' . $db->quote(6),
            $db->quoteName('section') . '=' . $db->quote('training')
        );

        $query
            ->select('*')
            ->from($db->quoteName('joom_splms_lessons'))
            ->where($conditions);

        $db->setQuery($query);

        $results = $db->loadObjectList();

        $count = 0;
        foreach($results as $result) {
            
            $materials = $result->teacher_materials;            
            $lesson_id = $result->splms_lesson_id;
            
            $materials_json = json_decode($materials);

            $video_file = $materials_json->video_file[0];            
            $audio_file = str_replace("MP4", "MP3", $video_file);
            //echo ($video_file) . "\n";

            $materials_json->video_file = array();
            $materials_json->audio_file = array();

            array_push($materials_json->video_file, "");
            array_push($materials_json->audio_file, $audio_file);

            $materials = json_encode($materials_json);            

            //echo ($materials) . "\n";
            
            $query = $db->getQuery(true);

            $fields = array(
                $db->quoteName('teacher_materials') . '=' . $db->quote($materials)
            );

            $conditions = array(
                $db->quoteName('splms_lesson_id') . '=' . $db->quote($lesson_id)
            );

            $query
                ->update($db->quoteName('joom_splms_lessons'))
                ->set($fields)
                ->where($conditions);

            $db->setQuery($query);
            //$db->execute();
            //echo $query . "\n";
        }
    }
}




// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('CleanBoards')->execute();

