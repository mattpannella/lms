<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/sso.php';
require_once JPATH_LIBRARIES . '/axslibs/sso/metadata_reader.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateMetadata extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND status LIKE 'active%'";
        $db_result = $conn->query($query);
        $i = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('#__sso_saml_config');
            $query->where("type = 'SAML' AND metadata_endpoint != '' AND metadata_auto_update = 1");
            $db->setQuery($query);
            $results = $db->loadObjectList();

            if($results) {
              foreach($results as $row) {
                $document = new DOMDocument();
                $document->load($row->metadata_endpoint);
                restore_error_handler();
                $first_child = $document->firstChild;
                if(!empty($first_child)) {
                  $metadata = new IDPMetadataReader($document);
                  $identity_providers = $metadata->getIdentityProviders();

                  if(empty($identity_providers)) {
                    echo 'error1';
                  return;
                  }
                  foreach($identity_providers as $key => $idp){
                    $saml_login_url = $idp->getLoginURL('HTTP-Redirect');
                    $saml_issuer = $idp->getEntityID();
                    $saml_x509_certificate = $idp->getSigningCertificate();
                    // Fields to update.
                    $data = new stdClass();
                    $data->id = $row->id;
                    $data->idp_entity_id = $saml_issuer;
                    $data->single_signon_service_url =  $saml_login_url;
                    /* if($saml_x509_certificate[1]) {
                      $data->certificate = $saml_x509_certificate[1];
                    } else {
                      $data->certificate = $saml_x509_certificate[0];
                    } */
                    $data->certificate = implode("\n/////////Certificate/////////\n",$saml_x509_certificate);
                    break;
                  }
                  if($data) {
                    $db->updateObject('joom_sso_saml_config',$data,'id');
                    echo $i++.'.'.$name.": updated\n";
                  }
                }
              }
            }
        }
    }
}

JApplicationCli::getInstance('CliUpdateMetadata')->execute();