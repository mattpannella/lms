<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliAddArchiveDateColumn extends JApplicationCli {

    private $db = null;


    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND (status LIKE 'active%' OR status = 'test' OR status = 'trial')";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            echo "Ran: ".$params->dbname."\n";

            // Add the archive_date column to the following tables
            $db->setQuery("ALTER TABLE joom_splms_course_progress ADD COLUMN archive_date DATE;");
            echo "Altered joom_splms_course_progress, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE joom_splms_lesson_status ADD COLUMN archive_date DATE;");
            echo "Altered joom_splms_lesson_status, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE joom_splms_student_activities ADD COLUMN archive_date DATE;");
            echo "Altered joom_splms_student_activities, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE joom_splms_quizresults ADD COLUMN archive_date DATE;");
            echo "Altered joom_splms_quizresults, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE axs_survey_responses ADD COLUMN archive_date DATE;");
            echo "Altered axs_survey_responses, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE axs_survey_progress ADD COLUMN archive_date DATE;");
            echo "Altered axs_survey_progress, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE ic_h5p_contents_user_data ADD COLUMN archive_date DATE;");
            echo "Altered ic_h5p_contents_user_data, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE ic_h5p_results ADD COLUMN archive_date DATE;");
            echo "Altered ic_h5p_results, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE axs_checklist_activity ADD COLUMN archive_date DATE;");
            echo "Altered axs_checklist_activity, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE axs_video_tracking ADD COLUMN archive_date DATE;");
            echo "Altered axs_video_tracking, successful? : ".$db->execute()."\n";

            // We need to update these db indexes with the archive_date column so new inserts to these
            // databases aren't rejected after some rows have been archived
            $db->setQuery("ALTER TABLE joom_splms_lesson_status DROP INDEX course_id");
            echo "Removed joom_splms_lesson_status course_id db index, successful? : ".$db->execute()."\n";

            $db->setQuery("CREATE INDEX course_id ON joom_splms_lesson_status (course_id, user_id, lesson_id, status, language, archive_date);");
            echo "Created new joom_splms_lesson_status cours_id db index, successful? : ".$db->execute()."\n";

            $db->setQuery("ALTER TABLE joom_splms_course_progress DROP INDEX UK_joom_splms_course_progress;  ");
            echo "Removed joom_splms_course_progress UK_joom_splms_course_progress db index, successful? : ".$db->execute()."\n";

            $db->setQuery("CREATE INDEX UK_joom_splms_course_progress ON joom_splms_course_progress (course_id, user_id, language, archive_date);");
            echo "Created joom_splms_course_progress UK_joom_splms_course_progress db index, successful? : ".$db->execute()."\n";


            echo "DONE\n";
        }
    }
}

JApplicationCli::getInstance('CliAddArchiveDateColumn')->execute();