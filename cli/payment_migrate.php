<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 5/20/16
 * Time: 12:53 PM
 */

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
	die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', dirname(__DIR__));
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_CONFIGURATION . '/configuration.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_LIBRARIES . '/axslibs/payment.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';

class PaymentMigrate extends JApplicationCli {
	/**
	 * Entry point for CLI script
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function doExecute() {
		//#################################### CLEAN DB FOR NEW TABLES ####################################
		$db = JFactory::getDbo();
		$db->setQuery("DROP TABLE IF EXISTS 
										axs_pay_subscription_field_values,
										axs_course_purchases, 
										axs_pay_failed_subscriptions, 
										axs_pay_pplans, 
										axs_pay_subscriptions, 
										axs_pay_subscription_categories, 
										axs_pay_subscription_periods, 
										axs_pay_subscription_plans, 
										axs_pay_subscription_reg_fields, 
										axs_pay_transactions,
										axs_pay_transactions_errors,
										axs_pay_user_pplans, 
										axs_pay_user_pplan_periods, 
										axs_rewards_data,
										axs_rewards_transactions;");
		$db->execute();

		//#################################### SET UP NEW TABLES ##########################################
		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscription_field_values` (
  `field_id` int(11) NOT NULL,
  `sub_id` int(11) NOT NULL,
  `value` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("ALTER TABLE `axs_pay_subscription_field_values` ADD PRIMARY KEY( `field_id`, `sub_id`);");
		$db->execute();

		$db->setQuery("CREATE TABLE `axs_course_purchases` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("ALTER TABLE `axs_course_purchases` ADD UNIQUE( `user_id`, `course_id`);");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_failed_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `first` varchar(64) NOT NULL,
  `last` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `phone` varchar(16) NOT NULL,
  `address` varchar(64) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `country` varchar(32) NOT NULL,
  `zip` varchar(16) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_pplans` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `term_length` int(11) NOT NULL,
  `term_unit` char(1) NOT NULL,
  `total` tinyint(4) NOT NULL,
  `interest` decimal(10,2) NOT NULL,
  `late_fee` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `cancel` datetime DEFAULT NULL,
  `plan_id` int(11) NOT NULL,
  `original_amount` decimal(10,2) NOT NULL,
  `status` varchar(4) NOT NULL,
  `period_length` varchar(4) NOT NULL,
  `card_id` int(11) NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("ALTER TABLE `axs_pay_subscriptions` ADD UNIQUE( `user_id`, `plan_id`);");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscription_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `brand_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscription_periods` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `sub_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscription_plans` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text NOT NULL,
  `default_initial_amount` decimal(10,2) NOT NULL,
  `default_amount` decimal(10,2) NOT NULL,
  `default_sub_length` varchar(4) NOT NULL,
  `community_groups` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `usergroups` text NOT NULL,
  `courses` text NOT NULL,
  `default_trial_amount` decimal(10,2) NOT NULL,
  `default_trial_length` varchar(4) NOT NULL,
  `reg_fields` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_subscription_reg_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(64) NOT NULL,
  `label` varchar(255) NOT NULL,
  `tooltip` text NOT NULL,
  `required` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `class` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` int(11) NOT NULL,
  `placeholder` varchar(255) NOT NULL,
  `type` varchar(32) NOT NULL,
  `field_mapping` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("INSERT INTO `axs_pay_subscription_reg_fields` (`id`, `name`, `label`, `tooltip`, `required`, `size`, `class`, `ordering`, `published`, `placeholder`, `type`, `field_mapping`) VALUES
(3, 'organization', 'Business Name', '', 0, 0, '', 4, 1, '', 'Text', 'FIELD_BUSINESS_NAME'),
(11, 'fax', 'Fax', '', 0, 0, '', 12, 0, '', 'Text', ''),
(13, 'comment', 'Comment', '', 0, 0, '', 14, 0, '', 'Textarea', '');");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_id` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` datetime NOT NULL,
  `status` varchar(4) NOT NULL,
  `card` smallint(6) NOT NULL,
  `trxn_id` varchar(64) DEFAULT NULL,
  `refund_id` varchar(64) DEFAULT NULL,
  `initiator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE axs_pay_transactions_errors LIKE cctransactions_errors;");
		$db->execute();
		$db->setQuery("INSERT axs_pay_transactions_errors SELECT * FROM cctransactions_errors;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_user_pplans` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pplan_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `count` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `type_id` int(11) NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_pay_user_pplan_periods` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `upp_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `status` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE IF NOT EXISTS `axs_rewards_data` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `level` int(11) NOT NULL,
  `referrer_id` int(11) NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
		$db->execute();

		$db->setQuery("CREATE TABLE axs_rewards_transactions LIKE axs_gin_rewards_transactions;");
		$db->execute();
		$db->setQuery("INSERT axs_rewards_transactions SELECT * FROM axs_gin_rewards_transactions;");
		$db->execute();

		//#################################### COURSE PURCHASES ###########################################
		//move all single trxns into new transactions table
		$db->setQuery("SET @i:=0;");
		$db->execute();

		//move all single trxns into new course table
		$db->setQuery("INSERT IGNORE INTO axs_course_purchases 
											SELECT
													@i:=@i+1,
													user_id,
													item_id,
													date,
													CASE WHEN status='STL' OR status='SUC' THEN 'PAID' ELSE 'NONE' END
											FROM cctransactions WHERE type=0 AND (status='SUC' OR status='STL');");
		$db->execute();

		$db->setQuery("INSERT IGNORE INTO axs_course_purchases 
											SELECT
													@i:=@i+1,
													user_id,
													item_id,
													date,
													CASE WHEN status='STL' OR status='SUC' THEN 'PAID' ELSE 'NONE' END
											FROM cctransactions WHERE type=0 AND status!='SUC' AND status!='STL';");
		$db->execute();

		$db->setQuery("INSERT INTO axs_pay_transactions 
									    SELECT 
									        a.id, 
									        a.user_id,
									        1,
									        b.id,
									        a.amount, 
									        a.`date`, 
									        CASE WHEN a.`status`='STL' THEN 'PLHO' ELSE a.`status` END, 
									        CASE WHEN a.`status`='STL' THEN 0 ELSE a.`card` END, 
									        CASE WHEN a.trxn_id LIKE '%Placeholder%' THEN NULL ELSE a.trxn_id END, 
									        a.refund_trxn_id,
									        3
									    FROM cctransactions a LEFT JOIN axs_course_purchases b ON (a.user_id=b.user_id AND a.item_id=b.course_id) WHERE a.type=0;");
		$db->execute();



		//#################################### SUBSCRIPTIONS ##############################################
		$db->setQuery("SET @i:=0;");
		$db->execute();

		$db->setQuery("INSERT INTO axs_pay_subscriptions
											SELECT 
													@i:=@i+1,
													a.user_id,
													a.canceldate,
													a.item_id,
													a.original_amount,
													CASE WHEN (`status`='STL' OR `status`='SUC' OR `status`='REF') AND next_payment_date>NOW() THEN 'ACT' WHEN (`status`='STL' OR `status`='SUC' OR `status`='REF') AND next_payment_date>(NOW() - INTERVAL 30 DAY) THEN 'GRC' ELSE 'INAC' END,
													'1M',
													0,
													0
											FROM (SELECT * FROM `cctransactions` WHERE type=1 ORDER BY `date` DESC) a GROUP BY a.user_id");
		$db->execute();

		//#################################### FIX SUB SETTINGS ###############################################
		$db->setQuery("SELECT a.*, b.gin_payment_settings FROM axs_pay_subscriptions a LEFT JOIN axs_gin_extras b ON a.user_id=b.user_id;");
		$subs = $db->loadObjectList();
		foreach($subs as $s) {
			$settings = unserialize($s->gin_payment_settings);
			if (is_array($settings)) {
				foreach ($settings as $plan_id => $setts) {
					if ($s->plan_id == $plan_id) {
						$card = $setts['card'];
						$points = $setts['points'];
						$famids = AxsExtra::getFamilyMembers($s->user_id);
						$famids = implode(", ", $famids);
						$db->setQuery("SELECT id FROM ccdata WHERE card=$card AND user_id IN ($famids);");
						$c = $db->loadObject();
						$card_id = is_object($c) ? $c->id : 0;
						$db->setQuery("UPDATE axs_pay_subscriptions SET card_id=$card_id, points=$points WHERE id=$s->id;");
						$db->execute();
					}
				}
			}
		}

		//#################################### SUBSCRIPTION PERIODS #######################################
		$db->setQuery("SET @i:=0;");
		$db->execute();

		$db->setQuery("INSERT INTO axs_pay_subscription_periods
											SELECT 
													@i:=@i+1,
													b.id,
													SUBMONTH(a.next_payment_date),
													a.next_payment_date,
													CASE WHEN a.status='STL' OR a.status='SUC' OR a.status='REF' THEN 'PAID' ELSE 'NONE' END
											FROM (SELECT * FROM (SELECT * FROM `cctransactions` WHERE type=1 AND next_payment_date IS NOT NULL ORDER BY date DESC) t GROUP BY t.user_id, t.next_payment_date) a INNER JOIN axs_pay_subscriptions b ON a.user_id=b.user_id;");
		$db->execute();

		//#################################### TRANSACTIONS ###############################################
		$db->setQuery("INSERT INTO axs_pay_transactions
											SELECT 
													a.id,
													a.user_id,
													2,
													b.id,
													a.amount,
													a.`date`,
													CASE WHEN a.status='STL' THEN 'PLHO' ELSE a.status END,
													CASE WHEN a.status='STL' THEN 0 ELSE a.card END,
													CASE WHEN a.trxn_id LIKE '%Placeholder%' THEN NULL ELSE a.trxn_id END,
													a.refund_trxn_id,
													1
											FROM (SELECT * FROM cctransactions WHERE type=1) a INNER JOIN (SELECT c.*, d.user_id FROM axs_pay_subscription_periods c JOIN axs_pay_subscriptions d ON c.sub_id=d.id) b ON a.user_id=b.user_id AND a.next_payment_date=b.end;");
		$db->execute();

		//#################################### FIX INITIATORS ###############################################
		$db->setQuery("UPDATE `axs_gin_admin_actions` a join axs_pay_transactions b ON a.data_row=b.id SET b.initiator=2 WHERE a.type_id IN (8, 10) AND a.date >= b.date - INTERVAL 3 SECOND AND a.date <= b.date + INTERVAL 3 SECOND;");
		$db->execute();

		$db->setQuery("UPDATE axs_pay_transactions a left join (SELECT MIN(id) as id FROM axs_pay_transactions WHERE type=2 GROUP BY type_id) b ON a.id=b.id SET a.initiator=2 WHERE a.type=2 AND b.id IS NULL;");
		$db->execute();

		//#################################### MIGRATE SUB PLANS && CATEGORIES ###############################################

		$db->setQuery("INSERT IGNORE INTO axs_pay_subscription_categories
											SELECT 
													id,
													1,
													title,
													description,
													ordering,
													published
											FROM #__osmembership_categories;");
		$db->execute();

		$db->setQuery("INSERT IGNORE INTO axs_rewards_data
											SELECT 
													user_id,
                          affiliate_level,
                          referrer_id,
                          gin_rewards_points
											FROM axs_gin_extras;");
		$db->execute();
	}
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('PaymentMigrate')->execute();