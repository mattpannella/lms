<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

class updateCommentsTable extends JApplicationCli {

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled = 1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $package_id = 0;
            $name = "German (deutsch)";
            $type = "language";
            $element = "de-DE";
            $client_id = 1;
            $enabled = 1;
            $access = 1;
            $protected = 0;
            $manifest_cache = '{"name":"German (deutsch)","type":"language","creationDate":"2022-06-23","author":"Google Translate","description":"German language pack for Tovuti"}';


            print "\nUpdating DB. dbname  Table = " . $params->dbname . "\n";

            $db->setQuery("
            INSERT INTO joom_extensions (package_id,name,type,element,client_id,enabled,access,protected,manifest_cache) VALUES ('".$package_id."','".$name."','".$type."','".$element."','".$client_id."','".$enabled."','".$access."','".$protected."','".$manifest_cache."');
            ");

            $db->execute();

            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $client_id = 0;

            $db->setQuery("
            INSERT INTO joom_extensions (package_id,name,type,element,client_id,enabled,access,protected,manifest_cache) VALUES ('".$package_id."','".$name."','".$type."','".$element."','".$client_id."','".$enabled."','".$access."','".$protected."','".$manifest_cache."');
            ");

            $db->execute();

            $name = "German";
            $title_native = "Deutsch";
            $sef = "de";

            $image = "de_de";

            $db->setQuery("
            INSERT INTO joom_languages (lang_code,title,title_native,sef,image,published,access) VALUES ('".$element."','".$name."','".$title_native."','".$sef."','".$image."','".$enabled."','".$access."');
            ");

            $db->execute();

            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $db->setQuery("UPDATE joom_languages SET published = 1");

            $db->execute();
            
            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $db->setQuery("UPDATE joom_languages SET published = 1");

            $db->execute();
            
            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('updateCommentsTable')->execute();