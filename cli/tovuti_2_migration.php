<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/sso.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/tracking.php';



require_once JPATH_LIBRARIES . '/axslibs/virtualClassroom.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualMeetingServers.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';
require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
require_once JPATH_LIBRARIES . '/axslibs/okta.php';
require_once '/var/www/html/cli/migration.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateEditor extends JApplicationCli {

    private $db = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";  AND id != 9

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);
        $i = 0;
        $total = 0;

        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();

            // Migrate Admin Permissions
            $query = "DROP TABLE IF EXISTS axs_permissions_old";
            $db->setQuery($query);
            $db->execute();

            $query = "CREATE TABLE IF NOT EXISTS axs_permissions_old (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `enabled` tinyint(4) DEFAULT NULL,
                `name` text,
                `type` varchar(255) DEFAULT NULL,
                `access_level` text,
                `user_group` text,
                `user_list` text,
                `permissions` text,
                `updated` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              )";
            $db->setQuery($query);
            $db->execute();

            $query = "SELECT * FROM axs_permissions";
            $db->setQuery($query);
            $result = $db->loadObjectList();

            foreach($result as $row) {
                $db->insertObject('axs_permissions_old',$row);
                $rules = array();
                $permissions = json_decode($row->permissions);
                foreach($permissions as $rule) {
                    if($rule->system) {
                        $rules[$rule->system] = $rule->allowed;
                    } else {
                        $rules['access'] = $rule->access;
                    }
                }
                $newPermissionRules = Migration::processRules($rules);
                $row->permissions = json_encode($newPermissionRules);
                $db->updateObject('axs_permissions',$row,'id');
            }
            // End Migrate Admin Permissions

            // Migrate the welcome email to a notification
            $userSettings = null;
            $query = $db->getQuery(true);
            $query->select('*')
                  ->from('axs_user_settings')
                  ->setLimit(1);
            $db->setQuery($query);
            $result = $db->loadObject();

            if(!is_null($result)) {

                $userSettings = json_decode($result->params);

                $emailSubject = $userSettings->subject;
                $emailBody = $userSettings->body;

                $newWelcomeEmailParams = new stdClass();

                $newWelcomeEmailParams->welcome_email_subject = $emailSubject;
                $newWelcomeEmailParams->welcome_email_body = $emailBody;

                $params = json_encode($newWelcomeEmailParams);

                $newWelcomeEmailAlert = new stdClass();

                $newWelcomeEmailAlert->params = $params;
                $newWelcomeEmailAlert->alert_type = 'new user welcome email';
                $newWelcomeEmailAlert->name = 'Global New User Welcome Email';
                $newWelcomeEmailAlert->description = 'New User Welcome Email Notification';
                $newWelcomeEmailAlert->brand_id = 0;
                $newWelcomeEmailAlert->enabled = 1;

                // We're going to assume that no notifications of this type exist and just insert it
                $success = $db->insertObject('axs_auto_alerts', $newWelcomeEmailAlert);

            }
            // End Migrate welcome email

            // Migrate Brands

            $db->setQuery(
                "CREATE TABLE axs_brand_backups (
                    id INT NOT NULL AUTO_INCREMENT,
                    brand_id INT NOT NULL,
                    date DATETIME NOT NULL,
                    name VARCHAR(100) NOT NULL,
                    params TEXT,
                    PRIMARY KEY ( id )
                );"
            );
            $db->execute();

            $db->setQuery(
                "CREATE TABLE axs_email_settings (
                    id INT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(100) NOT NULL,
                    params TEXT,
                    is_default INT NOT NULL DEFAULT 0,
                    PRIMARY KEY ( id )
                );"
            );
            $db->execute();

            $db->setQuery(
                "CREATE TABLE axs_integrations (
                    id INT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(100) NOT NULL,
                    params TEXT,
                    is_default INT NOT NULL DEFAULT 0,
                    PRIMARY KEY ( id )
                );"
            );
            $db->execute();

            $db->setQuery(
                "CREATE TABLE axs_payment_gateways (
                    id INT NOT NULL AUTO_INCREMENT,
                    name VARCHAR(100) NOT NULL,
                    params TEXT,
                    is_default INT NOT NULL DEFAULT 0,
                    PRIMARY KEY ( id )
                );"
            );
            $db->execute();

            $db->setQuery("
                SELECT *
                FROM axs_brands;
            ");
            $emailSettingsCounter = 1;
            $paymentGatewaysCounter = 1;
            $integrationsCounter = 1;
            $brands = $db->loadObjectList();
            foreach ($brands as $brand) {
                $brandBackup = new stdClass();
                $brandBackup->name =  $brand->site_title . ' (backup)';
                $brandBackup->brand_id = $brand->id;
                $brandBackup->date = date('Y-m-d H:i:s');
                $brandBackup->params = $brand->data;
                $db->insertObject('axs_brand_backups', $brandBackup);

                $brandData = json_decode($brand->data);

                $emailSettings = new stdClass();
                $emailSettings->name = 'Email Settings ' . $emailSettingsCounter;
                $emailSettings->params = json_encode($brandData->email);
                $db->setQuery("
                    SELECT id
                    FROM axs_email_settings
                    WHERE md5(params) = " . $db->quote(md5($emailSettings->params))
                );
                $existingEmailSettings = $db->loadObject();
                if (!empty($existingEmailSettings)) {
                    $brandData->email_settings_id = strval($existingEmailSettings->id);
                } else {
                    $emailSettingsCounter++;
                    $db->insertObject('axs_email_settings', $emailSettings);
                    $brandData->email_settings_id = strval($db->insertid());
                }

                $paymentGateway = new stdClass();
                $paymentGateway->name = 'Payment Gateway ' . $paymentGatewaysCounter;
                if (!empty($brandData->billing->gateway_type)) {
                    $paymentGateway->name .= ' (' .$brandData->billing->gateway_type . ')';
                }
                $paymentGateway->params = json_encode($brandData->billing);
                $db->setQuery("
                    SELECT id
                    FROM axs_payment_gateways
                    WHERE md5(params) = " . $db->quote(md5($paymentGateway->params))
                );
                $existingPaymentGateway = $db->loadObject();
                if (!empty($existingPaymentGateway)) {
                    $brandData->payment_gateway_id = strval($existingPaymentGateway->id);
                } else {
                    $paymentGatewaysCounter++;
                    $db->insertObject('axs_payment_gateways', $paymentGateway);
                    $brandData->payment_gateway_id = strval($db->insertid());
                }

                $integration = new stdClass();
                $integration->name = 'Integration ' . $integrationsCounter;
                $integration->params = json_encode($brandData->api_settings);
                $db->setQuery("
                    SELECT id
                    FROM axs_integrations
                    WHERE md5(params) = " . $db->quote(md5($integration->params))
                );
                $existingIntegration = $db->loadObject();
                if (!empty($existingIntegration)) {
                    $brandData->integration_id = strval($existingIntegration->id);
                } else {
                    $integrationsCounter++;
                    $db->insertObject('axs_integrations', $integration);
                    $brandData->integration_id = strval($db->insertid());
                }

                $brand->data = json_encode($brandData);
                $db->updateObject('axs_brands', $brand, 'id');
            }

            // End Migrate Brands

            // Create Start Table

            $db->setQuery(
                "CREATE TABLE axs_starred (
                    id INTEGER NOT NULL AUTO_INCREMENT,
                    user_id INTEGER NOT NULL,
                    text VARCHAR(500) NOT NULL,
                    category VARCHAR(255) NOT NULL,
                    uri VARCHAR(1000) NOT NULL,
                    PRIMARY KEY (id)
                )"
            );
            $db->execute();

            $db->setQuery("CREATE INDEX user_id_uri ON axs_starred (user_id, uri)");
            $db->execute();

            $db->setQuery("CREATE INDEX user_id ON axs_starred (user_id)");
            $db->execute();

            // End Create Star Table

            echo "\n";
            echo 'client id: '.$dbs->id.' named: '.$name;
            echo "\n";
            $db->disconnect();
      }

    }
}

JApplicationCli::getInstance('CliUpdateEditor')->execute();