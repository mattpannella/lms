<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualClassroom.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualMeetingServers.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';
require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateMetadata extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        $ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for

        $query = "SELECT * FROM `axs_dbmanager` WHERE  server_ip='$ip' AND enabled=1";
        $db_result = $conn->query($query);
        $i = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('#__jbusinessdirectory_companies');
            $db->setQuery($query);
            $businessDirectoryListings = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('axs_contracts');
            $db->setQuery($query);
            $contracts = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('axs_affiliate_dashboards');
            $db->setQuery($query);
            $affiliateDashboards = $db->loadObjectList();

            //must be more than one
            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('axs_rewards_data');
            $db->setQuery($query);
            $rewardsData = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('#__eshop_products');
            $db->setQuery($query);
            $eshopProducts = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('axs_gantt_charts');
            $db->setQuery($query);
            $ganttCharts = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('axs_reports_custom');
            $db->setQuery($query);
            $campaigns = $db->loadObjectList();

            //must be more than one
            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('#__community_activities');
            $db->setQuery($query);
            $communityActivity = $db->loadObjectList();

            //must be more than one
            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('#__easyblog_post');
            $db->setQuery($query);
            $blogPosts = $db->loadObjectList();

            $query = $db->getQuery(true);
            $query->select('COUNT(*) as total');
            $query->from('#__kunena_topics');
            $db->setQuery($query);
            $forumTopics = $db->loadObjectList();
            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliUpdateMetadata')->execute();