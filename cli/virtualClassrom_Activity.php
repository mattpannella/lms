<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualClassroom.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualMeetingServers.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';
require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateMetadata extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        $ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        $recordingsArray = [
            '071e05e7063a934c5cf1efc1a3e0b3d6c4c8c5af-1589553930279',
            '104a5ab414b712c2518859d7628e415d75bfa09d-1591214115570',
            '121db1a44fccfeefe27907fdc0288b7a63a9a7d2-1592829760484',
            '133fcb8aa16a3f5f99a3b73e2fe4853480537e7b-1591126165184',
            '133fcb8aa16a3f5f99a3b73e2fe4853480537e7b-1591194590241',
            '133fcb8aa16a3f5f99a3b73e2fe4853480537e7b-1591216793868',
            '133fcb8aa16a3f5f99a3b73e2fe4853480537e7b-1591280963096',
            '133fcb8aa16a3f5f99a3b73e2fe4853480537e7b-1591367321207',
            '13f19caedc42c64cfe6f8577d74efbc5bd3c04b9-1592856069508',
            '1e3d71ee27a436642577bc49000bde14da153451-1591981346029',
            '1e3d71ee27a436642577bc49000bde14da153451-1592250479612',
            '1e3d71ee27a436642577bc49000bde14da153451-1592333440345',
            '1e3d71ee27a436642577bc49000bde14da153451-1592423617028',
            '1e3d71ee27a436642577bc49000bde14da153451-1592506424531',
            '1e3d71ee27a436642577bc49000bde14da153451-1592510055337',
            '1e3d71ee27a436642577bc49000bde14da153451-1592592922472',
            '1e3d71ee27a436642577bc49000bde14da153451-1592596068232',
            '1e53f575436123f900836ca238f072b471366e54-1592857512414',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589899955773',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589900858078',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589900979711',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589901161426',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589901880030',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589917309847',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589923944391',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589924120474',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589924199823',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589925326105',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589925440221',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589925817577',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589926366738',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589982412231',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1589997510617',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590083901821',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590170378310',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590505083037',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590505394372',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590505485138',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590505642209',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590505701065',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590506040136',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590506096329',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590507203067',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590508256164',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590508420935',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590508508851',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590508573225',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590515714810',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590528681119',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590528908278',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590529105693',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590529154585',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590529658399',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590605868932',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1590685151924',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1591037920528',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1591208231397',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1591295325899',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1591381609559',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1591387068743',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592419697206',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592420334111',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592427453319',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592427687380',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592428149943',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592428772339',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592428867257',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592431028179',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592431619028',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592487318041',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592487397882',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592488289591',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592572445020',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592573302802',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592573759002',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592574068681',
            '21ca710cecbb64257ba53b5a15f9565a8613559b-1592574391086',
            '21d8fb437fd585635695165732623478d8ba4802-1592833795096',
            '222dbd17f4cd8e799f55bac78e6b476ca08f6308-1590265693997',
            '2542ae6e03dffbc28a71cf1e312f0f760d6e552e-1589987973660',
            '2daf1aab8293ba8ad3952854ef341acb2e10e1e7-1589460177827',
            '2eed05e4c5e1e838d70a9406b6ba5881e2ea4042-1592829191558',
            '2eed05e4c5e1e838d70a9406b6ba5881e2ea4042-1592856915684',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592237525698',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592324591192',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592410246284',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592419331663',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592496373510',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592504745410',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592583187819',
            '309fd6ede17fbeed6bf2f1bff5527c6868a2cf31-1592590414899',
            '3271b5b51b047f563a5f031f3a4aa4eecfda262a-1589471582749',
            '39616c63ebf2755834f85ad785546de4aff7fe16-1592416000495',
            '3a483b274d6fca206acd83e5224e0b801bfa25bf-1591898358321',
            '3d8fa9d0ca3c9a29a5ad2e430d585952df1ad4b2-1589565394364',
            '3d8fa9d0ca3c9a29a5ad2e430d585952df1ad4b2-1590685089190',
            '3d8fa9d0ca3c9a29a5ad2e430d585952df1ad4b2-1591289355934',
            '3d8fa9d0ca3c9a29a5ad2e430d585952df1ad4b2-1592413855327',
            '43ce6e496fcc8241d29a7226ae43a7115d95a8fe-1592417458916',
            '43ce6e496fcc8241d29a7226ae43a7115d95a8fe-1592583646165',
            '43ce6e496fcc8241d29a7226ae43a7115d95a8fe-1592590179337',
            '4be364d17d11b816433d6e4d076b54dc717791a3-1591802560755',
            '4f76fbf456f5e8bc6da834e2b32e0cfa8e9feba0-1593005811032',
            '506e43e2abb8146213b144a4697b545c02480ab3-1590089922854',
            '506e43e2abb8146213b144a4697b545c02480ab3-1590529486592',
            '51ec559b4982d55b7e7020389e45bb62f7d2898e-1591128774115',
            '5320f1d56b1c1f01e10b807abd465089446bfb2b-1590006425514',
            '5585c73abcc48ab50318ecadb2706ae5c4b7a9b1-1592487897179',
            '5585c73abcc48ab50318ecadb2706ae5c4b7a9b1-1592495764415',
            '561239efe8b4d3efc5206722be3e93c410073b8a-1593093336420',
            '57d98728b60565e7a6009745fe205c6602099182-1592250780686',
            '58b5b5dc43d38a7b73f143e05513ff0f6484167e-1591517741476',
            '5b6ffccb59018614e2e23bb283ddad170fba1cb2-1592829232616',
            '5b6ffccb59018614e2e23bb283ddad170fba1cb2-1592857356656',
            '5caaa3777dc47445d68003783d38b05bfd5f2b5f-1592410146266',
            '5caaa3777dc47445d68003783d38b05bfd5f2b5f-1592496571826',
            '5caaa3777dc47445d68003783d38b05bfd5f2b5f-1592583488619',
            '5caaa3777dc47445d68003783d38b05bfd5f2b5f-1592589801315',
            '62a21b30337f564ca2a856c7481a2835876b56e3-1592413293213',
            '62a21b30337f564ca2a856c7481a2835876b56e3-1592586255092',
            '62a21b30337f564ca2a856c7481a2835876b56e3-1592595988342',
            '664f0fc85ecbd27c297cd1f12040dc5b5a06a67b-1592857507194',
            '67ab5c6a094ed09ee71a52c97a6768fb6bf45f2a-1592855502653',
            '6817f0e5e10bf471f6272c6b5c4157d2ddaebd4b-1593092881803',
            '6817f0e5e10bf471f6272c6b5c4157d2ddaebd4b-1593102124737',
            '7bdc2119dfa103108addd36bb9c130e2c3d56fb2-1592401846590',
            '7f5b539c4bb4b18129a48e10b1861ebf0c4496f7-1592488180147',
            '805dbeaf7e13894336215acdc8484a222545ab51-1592228715135',
            '82ccbb0f1ccbf802f6fe12ca17364baa06795513-1591136651346',
            '84028e1c10c6a1ac7d9dc99154b0e55e75d07ad9-1592829011638',
            '865befd45e20b586797dcb40556aea893010ad08-1593179572414',
            '8826e06dffc0c6118919a1427cb103344cf9fceb-1589458841078',
            '8826e06dffc0c6118919a1427cb103344cf9fceb-1589461348296',
            '8826e06dffc0c6118919a1427cb103344cf9fceb-1589472233957',
            '882e24b10f6d43150bae951bc8b695ff8d95c704-1589471008375',
            '8ba904934c3e6c4180e38a1febc9af107327374f-1591968881124',
            '8dc3a0e88880bbed937a0c86164de632a1db38af-1591232806552',
            '8dc3a0e88880bbed937a0c86164de632a1db38af-1591233137209',
            '8ed6756733f30a455d7eacf71754219a1c479b33-1592318036078',
            '8f6554b19728104b263c830ae3e9bc66cc7aa8c7-1592592704800',
            '8f6554b19728104b263c830ae3e9bc66cc7aa8c7-1593111244547',
            '93707530a8bfffaa6454257c9ce203f383e3e822-1591211833123',
            '93707530a8bfffaa6454257c9ce203f383e3e822-1591295888982',
            '93707530a8bfffaa6454257c9ce203f383e3e822-1592841518221',
            '966615c4bc874aaaa21a9a82b2c40f7c0d3ff06a-1593178477389',
            '966615c4bc874aaaa21a9a82b2c40f7c0d3ff06a-1593187057191',
            '990f99684bf353adeb74f9d62e3fd8cbe3552e56-1591624226274',
            '990f99684bf353adeb74f9d62e3fd8cbe3552e56-1591630855903',
            '990f99684bf353adeb74f9d62e3fd8cbe3552e56-1591710153163',
            '990f99684bf353adeb74f9d62e3fd8cbe3552e56-1591797422665',
            '990f99684bf353adeb74f9d62e3fd8cbe3552e56-1591882140439',
            '9e2c5c306e31e0ee12ead3a444955c9f3878753f-1592829335196',
            'a29f8aa6289ebb1b860aa2e07c0cb613ac4f13f0-1590753035256',
            'a29f8aa6289ebb1b860aa2e07c0cb613ac4f13f0-1591706749197',
            'a29f8aa6289ebb1b860aa2e07c0cb613ac4f13f0-1592307896591',
            'a29f8aa6289ebb1b860aa2e07c0cb613ac4f13f0-1592567992293',
            'a4655e892e479dbd024e28eb525cb9a235f4ba8c-1592395352781',
            'a4655e892e479dbd024e28eb525cb9a235f4ba8c-1592489925419',
            'a4655e892e479dbd024e28eb525cb9a235f4ba8c-1592570125918',
            'a75287dcab88c690ef9115e3431d7e6f8a19d92e-1591719262685',
            'a91afc69a3b3d6b8eb0098c0c387fb083e272daf-1590780912158',
            'ab6da2fb9ac1ebbcdc30da494d24d506b061c2b0-1592343567826',
            'abba6b6185b49c9a4ccdd995cbd770072c28445d-1592828994487',
            'abba6b6185b49c9a4ccdd995cbd770072c28445d-1592856206269',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592226616875',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592313892822',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592403901440',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592486680817',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592490280548',
            'aeb255cb667ec1fcc3147d2ceee9e3a300e298a0-1592573107863',
            'b002c6a9415dda58b41a1c26c5b3bf9f7b1022c6-1592493518128',
            'b13addaa4ca51d048e7440677708806d17b865a5-1590118387506',
            'b13addaa4ca51d048e7440677708806d17b865a5-1590132271695',
            'b1d788b38726a94fffd50c62a96145252426a741-1591624326502',
            'b1d788b38726a94fffd50c62a96145252426a741-1591710828659',
            'b1d788b38726a94fffd50c62a96145252426a741-1591797250076',
            'b1d788b38726a94fffd50c62a96145252426a741-1591883330205',
            'b1f7c40a809494abaf6ebaf67aa25a87c7b22baf-1589471879555',
            'b1f7c40a809494abaf6ebaf67aa25a87c7b22baf-1589558298273',
            'c050f6416f5859f2860bf9155790eefea01f6c23-1592315544889',
            'c2cbcf12ffba50ccc096c0bc167153a1bfe1837b-1592417066810',
            'c2cbcf12ffba50ccc096c0bc167153a1bfe1837b-1592576386604',
            'c2cbcf12ffba50ccc096c0bc167153a1bfe1837b-1592589785385',
            'c4fe93a9dd8785a9be16c6e66268fac19d9a9e3f-1589489543691',
            'c4fe93a9dd8785a9be16c6e66268fac19d9a9e3f-1590181013420',
            'c811d658eb0167924281ce2fa032ef542cd625af-1592574884073',
            'cadb4d3dce9b2d3a929eb08ae48c9957b4aab225-1590502992317',
            'cc1bb95056e84165ecf8b48b5b2f5d2db7283e75-1593805627599',
            'cd729a75f6a25e4627db3fe0a73f5138e7d24f7f-1591118449203',
            'd7dd5a1f21e78c483aa792b397c47706ad03ac87-1592857408026',
            'd9be2acc9b5e5599db7f1a28692940833a065f7b-1590481738384',
            'dc83ff7308927de9bb06f2d75a4c85d97a38a63d-1592574959195',
            'dc83ff7308927de9bb06f2d75a4c85d97a38a63d-1592582219076',
            'dd2d53ea5b74aeeb540554be3fd5be32e0ebd3ed-1590499708404',
            'df1b939869ef559a2c988daf9992f214b38e9489-1592833386236',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589808679955',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589824259951',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589829281020',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589910260563',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589923928408',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1589981182228',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1590007116734',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1590097216573',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592226199636',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592242146292',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592314666751',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592330387577',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592420108797',
            'e0c99b2094fd55fea6e2e5efe89cf6f99205e1e4-1592506600917',
            'e99ebabdd6b53aef0a19d677bb7a6ef5b29a95c0-1592829490319',
            'e99ebabdd6b53aef0a19d677bb7a6ef5b29a95c0-1592856663294',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592243019003',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592329962502',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592417104119',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592423371382',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592503475011',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592510146122',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592589663549',
            'ea4176f6c8c27921f06e1421807846b1c2874c3f-1592596337760',
            'ee49ee93ae0b123fcf79073ee9df32445f291ae0-1592315126019',
            'ee5a4c18cbea5f8fffd6000b052b24d778ab222c-1592228569453',
            'ee5a4c18cbea5f8fffd6000b052b24d778ab222c-1592833448522',
            'ee5a4c18cbea5f8fffd6000b052b24d778ab222c-1592841282910',
            'f7002984d94bbd61ab8177160940ea0fdfba5695-1591371805131',
            'f7002984d94bbd61ab8177160940ea0fdfba5695-1591375805589',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592311091240',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592396944964',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592406641694',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592483997263',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592493085564',
            'fcb206f24adeeb3c1a8800930910cc3ef34f30d9-1592579356569',
            'fde56ec7f38fa992da70dd8f53f5771243fd0d6e-1593006063056',
            '02ed2c14a8f4e4e6b1245a4feaecda1a5d723c2c-1592915905653',
            '1c398b5b2ac2f7e2aedca63195ddde3da17a8a74-1592939615513',
            '1c398b5b2ac2f7e2aedca63195ddde3da17a8a74-1593116549014',
            '4f29a88c5cce3e6c047768bcb3a6425b8db8c5bd-1589390898944',
            '4f29a88c5cce3e6c047768bcb3a6425b8db8c5bd-1589402541774',
            '6f94eaac1a3cf3aaa58177e5eca60ae06a776d6b-1593529119255',
            '6f94eaac1a3cf3aaa58177e5eca60ae06a776d6b-1593723066473',
            '7595a54be454ec81a7b6b94c79b9c7c6890a12cd-1592935500719',
            '8b8ceca1cbc6ecc4fb8d678f2daa4263ea0f6edf-1592868008148',
            '935e6d78682884219c150db538075f6c246d074e-1593453068285',
            '9d11150dff0bfa4fcbcedae11dd7bc20a4ab14cf-1593115340980',
            '9f1b46570ca9846ea47ddc1c313dfe6591c15419-1592915391451',
            '9f1b46570ca9846ea47ddc1c313dfe6591c15419-1592943377094',
            'b18eaf5a056c1d600e1d1a22a8f00031b37df503-1593024960517',
            'b9096ce2ee871de7ee244bade1e7e20d43ae2a1b-1594767630617',
            'c06e99b8704d3fbdf3a95819648cd80f8580e867-1593095150985',
            'c571f1de79f693c4922fa38a14293f8250ec8d33-1594155118904',
            'd61ae7a9a3a0a1233b98921b26af87ab176eea77-1593116367913',
            'e88297353655a49b9dc84e3ce05df42e6b0bfcc4-1593088469056',
            'f7f17b961a899b5ce2fa224b12e002d13ef5cafd-1593534373006',
            'fb67cc41f8aa8fc1d8b5bcb6e9e01f5037338c3a-1593088622332',
            'fcf0da7faf7df1a733f38ba7df8b21ef9c7b5e99-1593121104771',
        ];

        $query = "SELECT * FROM `axs_dbmanager` WHERE  server_ip='$ip' AND enabled=1";
        $db_result = $conn->query($query);
        $i = 0;
        $number = 0;
        $numberFound = 0;
        $recordingList = array();
        $meetingList = array();
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $params->dbuser;     // User for database authentication
            $options['password'] = $params->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $params->dbuser);
            $config->set('password', $params->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('#__bbb_meetings');
            $db->setQuery($query);
            $results = $db->loadObjectList();
			$now = date('Y-m-d H:i:s');
            $attendanceTable = 'axs_virtual_classroom_attendees';


            $clientParams = json_decode($dbs->params);
            $VC_Server = $clientParams->virtual_meeting_server;
            $setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
            $virtualMeeting = new AxsVirtualClassroom($setupParams);
            if($results) {

				foreach($results as $row) {
					$params = json_decode($row->settings);
					if($params->meetingType == 'tovuti') {
/*
                        $VC_Server = $clientParams->virtual_meeting_server;
                        $setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
                        $virtualMeeting = new AxsVirtualClassroom($setupParams);
                        $recordings = $virtualMeeting->getRecordings($row->meetingId);
                        $countNew = count($recordings); */

                        $VC_Server_Old = 'server1';
                        $setupParams_Old = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server_Old);
                        $virtualMeeting_Old = new AxsVirtualClassroom($setupParams_Old);

                        $recordings_Old = $virtualMeeting_Old->getRecordings($row->meetingId);
                        $count = count($recordings_Old);

                        $missing = '';
                        $found = '';
                        for($i=0;$i<$count;$i++) {


                            if(in_array($recordings_Old[$i]['recordId'],$recordingsArray)) {
                                $found .=  'recordId: '.$recordings_Old[$i]['recordId']."\n";
                                $numberFound++;
                            } else {
                                if(!in_array($row->meetingId,$meetingList)) {
                                    $meetingList[] = $row->meetingId;
                                }

                                if(!in_array($recordings_Old[$i]['recordId'],$recordingList)) {
                                    $recordingList[] = $recordings_Old[$i]['recordId'];
                                }
                                $number++;
                            }
                        }

                        if($missing) {
                            /* echo "\nClient: ".$name."\n";
                            echo "\nMeeting: ".$row->meetingName."\n";
                            echo "\n---MISSING RECORDINGS---\n"; */
                            //echo $missing;
                        }

                        /* if($found) {
                            echo "\nClient: ".$name."\n";
                            echo "\nMeeting: ".$row->meetingName."\n";
                            echo "\n---FOUND RECORDINGS---\n";
                            echo $found;
                        } */

                        /* if($recordings_Old && !$recordings) {
                            echo "\n".$i++.'.'.$name."\n";
                            echo "Meeting: ".$row->meetingName."\n";
                            foreach($recordings_Old as $rec) {
                                echo 'recordId: '.$r['recordId']."\n";
                            }
                        } */
						/* $virtualClassIsRunning = $virtualMeeting->isMeetingRunning($row->meetingId);
						if($virtualClassIsRunning == 'true') {
                            echo $i++.'.'.$name.": running\n";
                            echo "Meeting: ".$row->meetingName."\n";
                            $meetingParams = new stdClass();
							$meetingParams->meetingId = $row->meetingId;
							$meetingParams->moderatorPW = $row->moderatorPW;
                            $virtualClass = $virtualMeeting->getMeetingInfo($meetingParams);
                            $attendeeArray = $virtualClass->attendees->attendee;
                            echo "Attendees: ".count($attendeeArray)."\n";
							/* $meetingParams = new stdClass();
							$meetingParams->meetingId = $row->meetingId;
							$meetingParams->moderatorPW = $row->moderatorPW;
							$virtualClass = AxsVirtualClassroom::getMeetingInfo($meetingParams);
							if($virtualClass) {
								$attendeeArray = $virtualClass->attendees->attendee;
								foreach($attendeeArray as $attendee) {
									$conditions = array();
									$conditions[] = $db->quoteName('meeting_id').'='.$db->quote($row->meetingId);
									$conditions[] = $db->quoteName('session').'='.$db->quote($virtualClass->createTime);
									if(strpos($attendee->userID,'-')) {
										$conditions[] = $db->quoteName('guest_id').'='.$db->quote($attendee->userID);
									} else {
										$conditions[] = $db->quoteName('user_id').'='.$db->quote($attendee->userID);
									}
									$query = $db->getQuery(true);
									$query->select('*');
									$query->from($db->quoteName($attendanceTable));
									$query->where($conditions);
									$db->setQuery($query);
									$attendanceResults = $db->loadObjectList();
									if($attendanceResults) {
										foreach($attendanceResults as $attendanceRow) {
											$attendanceRow->check_out = $now;
											$db->updateObject($attendanceTable,$attendanceRow,'id');
										}
									}
								}
							}
						} */
					}
				}
              //echo $i++.'.'.$name.": updated\n";
            }
        }
        $meetingText = '$meetingList = [';
        foreach($meetingList as $m) {
            $meetingText .= "'$m',\n";
        }
        $meetingText .= '];';
        echo "\n\n$meetingText\n\n";
        $recordingText = '$recordingsList = [';
        foreach($recordingList as $r) {
            $recordingText .= "'$r',\n";
        }
        $recordingText .= '];';
        echo "\n\n$recordingText\n";
        echo "\n\nTotal Recordings Missing: $number\n";
        echo "\n\nTotal Recordings Found: $numberFound\n";
    }

}

JApplicationCli::getInstance('CliUpdateMetadata')->execute();