<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 3/28/16
 * Time: 4:51 PM
 */

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
    die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_LIBRARIES . '/axslibs/payment.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';

class Config{
    private static $dbServer = "localhost";
    private static $dbUsername = "xbwpms";
    private static $dbPassword = "JEQKm25Kz3_";

    public static function getDBConnection($dbName){
        $dbConnection = new mysqli(
            self::$dbServer,
            self::$dbUsername,
            self::$dbPassword,
            $dbName
        );
        return $dbConnection;
    }
}

class FamilyDuplicate extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */
    public function doExecute()
    {
        //grab the dsipatcher and load axs plugins
        JPluginHelper::importPlugin('axs');
        $dispatcher = JEventDispatcher::getInstance();
        //load the libraries
        AxsPayment::init($dispatcher); //use tmp as the default converge account for now
        AxsExtra::init($dispatcher);

        $total = 0;
        $family = 0;
        $success = 0;
        $error = array();

        $conn = Config::getDBConnection('old_gin');

        $sql = "SELECT * FROM FamilyMemberInfo_temp;";
        $response = $conn->query($sql);
        While($row = $response->fetch_object()) {
            $total++;

            $db = JFactory::getDbo();
            $query = "SELECT * FROM joom_users a INNER JOIN cctransactions b ON a.id=b.user_id WHERE a.email='{$row->Email}' AND b.item_id != 6;";
            $db->setQuery($query);
            $old = $db->loadObject();

            if(is_object($old)) {
                $success++;
            }

            /*
            if(is_object($old)) {
                $family++;

                //grab the new user_id
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->select('user_id')
                    ->from('axs_gin_extras')
                    ->where($db->quoteName('old_gin_user_id').'='.$db->q($row->FamilyMemberID));
                $db->setQuery($query);
                $new = $db->loadObject();
                if(!is_object($new)) {
                    $error[] = $old;
                    continue; //skip rest of loop.
                }

                //fix axs_gin_extras
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->delete('axs_gin_extras')
                    ->where($db->quoteName('user_id').'='.(int)$old->id);
                $db->setQuery($query);
                $db->execute();

                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('axs_gin_extras')
                    ->set($db->qn('user_id').'='.(int)$old->id)
                    ->where($db->quoteName('user_id').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                //fix joom_users
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->delete('#__users')
                    ->where($db->quoteName('id').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                //fix joom_user_usergroup_map
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->delete('joom_user_usergroup_map')
                    ->where($db->quoteName('user_id').'='.(int)$old->id);
                $db->setQuery($query);
                $db->execute();

                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('joom_user_usergroup_map')
                    ->set($db->qn('user_id').'='.(int)$old->id)
                    ->where($db->quoteName('user_id').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                //cctransactions
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->delete('cctransactions')
                    ->where($db->quoteName('user_id').'='.(int)$old->id);
                $db->setQuery($query);
                $db->execute();

                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('cctransactions')
                    ->set($db->qn('user_id').'='.(int)$old->id)
                    ->where($db->quoteName('user_id').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                //update ccdata
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('ccdata')
                    ->set($db->qn('user_id').'='.(int)$old->id)
                    ->where($db->quoteName('user_id').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                //update settings for ccdata
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->update('ccdata')
                    ->set($db->qn('family_member_access')."=REPLACE(family_member_access, '{$new->user_id}', '{$old->id}')")
                    ->where($db->quoteName('user_id').'='.(int)$old->id);
                $db->setQuery($query);
                $db->execute();

                //delete community
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->delete('#__community_users')
                    ->where($db->quoteName('userid').'='.(int)$new->user_id);
                $db->setQuery($query);
                $db->execute();

                $success++;
            }
            */
        }
        $response->close();

        echo "Attempted to migrate: $total total Family Accounts.\nSuccessfully migrated: $success Family Accounts.\nError with the following Accounts:\n";
        print_r($error);
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('FamilyDuplicate')->execute();