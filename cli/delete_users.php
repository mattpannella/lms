<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * 
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}
if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';


/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class DeleteUser extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute()
    {
        $excluded_users = 
        "1620032,
        1620066,
        1617476,
        1620194,
        1619983,
        1619968,
        1620150,
        1620021,
        1620206,
        1620163,
        1532813,
        1620123,
        1619988,
        1620112,
        1620197,
        1620061,
        1619835,
        1620228,
        1620057,
        1619961,
        1620001,
        1615730,
        1620214,
        1619969,
        601,
        1619964,
        1619949,
        1620028,
        1619984,
        1620051,
        1686,
        1617006,
        1617464,
        1620211,
        1657,
        1620176,
        1620134,
        1620029,
        1689,
        1619989,
        1620146,
        1620049,
        1619992,
        1620017,
        1620052,
        1620004,
        1620122,
        1614850,
        1619956,
        1619966,
        1620181,
        1620023,
        1620223,
        1620050,
        1620067,
        1620208,
        1620159,
        1620033,
        1620035,
        1546933";
        $delete_user = '1562289';
        echo "Users\n\n\n";
        $db = JFactory::getDBO();
        //$query = "SELECT * FROM joom_users WHERE id NOT IN (".$excluded_users.")";
        $query = "SELECT * FROM joom_users WHERE id = ".$delete_user."";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        //jimport( 'joomla.database.table.user' );
        foreach ($result as $row) {
           //echo $row->name."\n\n";
            $user = JFactory::getUser($row->id);
            echo $user->name."\n\n";
            $user->delete(true);
            //$user->delete();
        }


       
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.

JApplicationCli::getInstance('DeleteUser')->execute();