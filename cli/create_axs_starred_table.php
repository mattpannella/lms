<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 *
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}
if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class CreateAxsStarredTable extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute()
    {
        $creds = dbCreds::getCreds();
    	//$ip = AxsClients::getPublicIp();

    	$conn = new mysqli(
    		$creds->dbhost,
    		$creds->dbuser,
    		$creds->dbpass,
    		$creds->dbname
    	);
        $query = "SELECT * FROM `axs_dbmanager` WHERE run_crons = 1 AND enabled=1 AND status LIKE 'active%';";
		$db_result = $conn->query($query);
		if(!$db_result) {
			exit;
		}
		while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);
			$config = &JFactory::getConfig();
			$config->set('db', $params->dbname);
			$config->set('user', $creds->dbuser);
			$config->set('password', $creds->dbpass);
			$config->set('dbprefix', $dbs->dbprefix);

			$options = array();
			$options['driver']   = 'mysqli';           // Database driver name
			$options['host']     = $creds->dbhost;        // Database host name
			$options['user']     = $creds->dbuser;       // User for database authentication
			$options['password'] = $creds->dbpass;       // Password for database authentication
			$options['database'] = $params->dbname;       // Database name
			$options['prefix']   = $dbs->dbprefix;     // Database prefix (may be empty)
			$db = JDatabaseDriver::getInstance($options);

            $db->setQuery(
                "CREATE TABLE axs_starred (
                    id INTEGER NOT NULL AUTO_INCREMENT,
                    user_id INTEGER NOT NULL,
                    text VARCHAR(500) NOT NULL,
                    category VARCHAR(255) NOT NULL,
                    uri VARCHAR(1000) NOT NULL,
                    PRIMARY KEY (id)
                )"
            );
            $db->execute();

            $db->setQuery("CREATE INDEX user_id_uri ON axs_starred (user_id, uri)");
            $db->execute();

            $db->setQuery("CREATE INDEX user_id ON axs_starred (user_id)");
            $db->execute();

        }
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('CreateAxsStarredTable')->execute();