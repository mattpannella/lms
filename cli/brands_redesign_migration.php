<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * 
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}
if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class BrandsRedesignMigration extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute()
    {
        $creds = dbCreds::getCreds();
    	//$ip = AxsClients::getPublicIp();

    	$conn = new mysqli(
    		$creds->dbhost,
    		$creds->dbuser,
    		$creds->dbpass,
    		$creds->dbname
    	);
        $query = "SELECT * FROM `axs_dbmanager` WHERE run_crons = 1 AND enabled=1 AND status LIKE 'test%';";
		$db_result = $conn->query($query);
		if(!$db_result) {
			exit;
		}
		while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);
			$config = &JFactory::getConfig();
			$config->set('db', $params->dbname);
			$config->set('user', $creds->dbuser);
			$config->set('password', $creds->dbpass);
			$config->set('dbprefix', $dbs->dbprefix);

			$options = array();
			$options['driver']   = 'mysqli';           // Database driver name
			$options['host']     = $creds->dbhost;        // Database host name
			$options['user']     = $creds->dbuser;       // User for database authentication
			$options['password'] = $creds->dbpass;       // Password for database authentication
			$options['database'] = $params->dbname;       // Database name
			$options['prefix']   = $dbs->dbprefix;     // Database prefix (may be empty)

			$db = JDatabaseDriver::getInstance($options);

            $db->transactionStart();
            try {
                $db->setQuery(
                    "CREATE TABLE axs_brand_backups (
                        id INT NOT NULL AUTO_INCREMENT,
                        brand_id INT NOT NULL,
                        date DATETIME NOT NULL,
                        name VARCHAR(100) NOT NULL,
                        params TEXT,
                        PRIMARY KEY ( id )
                    );"
                );
                $db->execute();
                
                $db->setQuery(
                    "CREATE TABLE axs_email_settings (
                        id INT NOT NULL AUTO_INCREMENT,
                        name VARCHAR(100) NOT NULL,
                        params TEXT,
                        is_default INT NOT NULL DEFAULT 0,
                        PRIMARY KEY ( id )
                    );"
                );
                $db->execute();

                $db->setQuery(
                    "CREATE TABLE axs_integrations (
                        id INT NOT NULL AUTO_INCREMENT,
                        name VARCHAR(100) NOT NULL,
                        params TEXT,
                        is_default INT NOT NULL DEFAULT 0,
                        PRIMARY KEY ( id )
                    );"
                );
                $db->execute();

                $db->setQuery(
                    "CREATE TABLE axs_payment_gateways (
                        id INT NOT NULL AUTO_INCREMENT,
                        name VARCHAR(100) NOT NULL,
                        params TEXT,
                        is_default INT NOT NULL DEFAULT 0,
                        PRIMARY KEY ( id )
                    );"
                );
                $db->execute();

                $db->setQuery("
                    SELECT *
                    FROM axs_brands;
                ");
                $emailSettingsCounter = 1;
                $paymentGatewaysCounter = 1;
                $integrationsCounter = 1;
                $brands = $db->loadObjectList();
                foreach ($brands as $brand) {
                    $brandBackup = new stdClass();
                    $brandBackup->name =  $brand->site_title . ' (backup)';
                    $brandBackup->brand_id = $brand->id;
                    $brandBackup->date = date('Y-m-d H:i:s');
                    $brandBackup->params = $brand->data;
                    $db->insertObject('axs_brand_backups', $brandBackup);

                    $brandData = json_decode($brand->data);

                    $emailSettings = new stdClass();
                    $emailSettings->name = 'Email Settings ' . $emailSettingsCounter;
                    $emailSettings->params = json_encode($brandData->email);
                    $db->setQuery("
                        SELECT id
                        FROM axs_email_settings
                        WHERE md5(params) = " . $db->quote(md5($emailSettings->params))
                    );
                    $existingEmailSettings = $db->loadObject();
                    if (!empty($existingEmailSettings)) {
                        $brandData->email_settings_id = strval($existingEmailSettings->id);
                    } else {
                        $emailSettingsCounter++;
                        $db->insertObject('axs_email_settings', $emailSettings);
                        $brandData->email_settings_id = strval($db->insertid());
                    }

                    $paymentGateway = new stdClass();
                    $paymentGateway->name = 'Payment Gateway ' . $paymentGatewaysCounter;
                    if (!empty($brandData->billing->gateway_type)) {
                        $paymentGateway->name .= ' (' .$brandData->billing->gateway_type . ')';
                    }
                    $paymentGateway->params = json_encode($brandData->billing);
                    $db->setQuery("
                        SELECT id
                        FROM axs_payment_gateways
                        WHERE md5(params) = " . $db->quote(md5($paymentGateway->params))
                    );
                    $existingPaymentGateway = $db->loadObject();
                    if (!empty($existingPaymentGateway)) {
                        $brandData->payment_gateway_id = strval($existingPaymentGateway->id);
                    } else {
                        $paymentGatewaysCounter++;
                        $db->insertObject('axs_payment_gateways', $paymentGateway);
                        $brandData->payment_gateway_id = strval($db->insertid());
                    }

                    $integration = new stdClass();
                    $integration->name = 'Integration ' . $integrationsCounter;
                    $integration->params = json_encode($brandData->api_settings);
                    $db->setQuery("
                        SELECT id
                        FROM axs_integrations
                        WHERE md5(params) = " . $db->quote(md5($integration->params))
                    );
                    $existingIntegration = $db->loadObject();
                    if (!empty($existingIntegration)) {
                        $brandData->integration_id = strval($existingIntegration->id);
                    } else {
                        $integrationsCounter++;
                        $db->insertObject('axs_integrations', $integration);
                        $brandData->integration_id = strval($db->insertid());
                    }

                    $brand->data = json_encode($brandData);
                    $db->updateObject('axs_brands', $brand, 'id');

                    $db->transactionCommit();
                }
            } catch (Exception $e) {
                $db->transactionRollback();

                // throw $e;
            }
            
        }
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('BrandsRedesignMigration')->execute();