<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * 
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
*/
// Set flag that this is a parent file.
const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}
if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/analytics.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';
require_once JPATH_LIBRARIES . '/axslibs/payment.php';
/**
 * @package  Joomla.CLI
 * @since    3.0
 */
class Clidemo extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute() {
        $options = array();
        $options['driver']   = 'mysqli';           // Database driver name
        $options['host']     = 'localhost';        // Database host name
        /*$options['user']     = 'sandbox_dbuser1';
        $options['password'] = 'DawDnQBhMus29Hja';
        $options['database'] = 'sandbox';*/
        $options['user']     = 'demo';
        $options['password'] = 'uL2yzM9V2H9fJuDu';
        $options['database'] = 'site_demo';
        $options['prefix']   = 'joom_';

        $db = JDatabaseDriver::getInstance($options);
        //$query = "SELECT * FROM sandbox.axs_daily_stats";
        //$db->setQuery($query);
        //$result = $db->loadObject();
        //print_r($result);

        $start_date = new DateTime("2015-01-01");
        $end_date = new DateTime("2017-03-08");

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($start_date, $interval, $end_date);

        $membership = 2000;

        foreach ($period as $day) {
            $start = $start_date->format('Y-m-d');
            $date = $day->format('Y-m-d');
            

            $data = new stdClass();
            
            
            $site_visits = new stdClass();
            $site_visits->count = rand(0, 100);
            $site_visits->data = [];

            for ($i = 0; $i < $site_visits->count; $i++) {
                array_push($site_visits->data, rand(100000, 200000));
            }

            $members_new = new stdClass();
            
            $members_new->count = rand(0, 20);
            $members_new->data = [];
            for ($i = 0; $i < $site_visits->count; $i++) {
                array_push($members_new->data, rand(100000, 200000));
            }

            $membership_current = new stdClass();
            $membership_current->current = new stdClass();
            $membership_current->grace_period = new stdClass();
            $membership_current->affiliates = new stdClass();

            $membership_current->current->count = $membership;
            $membership_current->grace_period->count = rand(280, 320);
            $membership_current->affiliates->count = rand(800, 850);

            $membership_current->current->data = [];
            $membership_current->grace_period->data = [];
            $membership_current->affiliates->data = [];

            for ($i = 0; $i < $membership_current->current->count; $i++) {
                array_push($membership_current->current->data, rand(100000, 200000));
            }

            for ($i = 0; $i < $membership_current->grace_period->count; $i++) {
                array_push($membership_current->grace_period->data, rand(100000, 200000));
            }

            for ($i = 0; $i < $membership_current->affiliates->count; $i++) {
                array_push($membership_current->affiliates->data, rand(100000, 200000));
            }

            $revenue = new stdClass();
            $revenue->upgrades = rand(-5000, 10000);
            $revenue->dues = rand(-5000, 8000);
            $revenue->events = rand(-5000, 5000);
            if ($revenue->upgrades < 0) {
                $revenue->upgrades = 0;
            }

            if ($revenue->dues < 0) {
                $revenue->dues = 0;
            }

            if ($revenue->events < 0) {
                $revenue->events = 0;
            }

            $revenue->total = $revenue->upgrades + $revenue->dues + $revenue->events;

            $refunds = new stdClass();            
            $refunds->total = 0;
            $refunds->data = [];
            $count = rand(-20, 10);
            if ($count < 0) {
                $count = 0;
            }
            for ($i = 0; $i < $count; $i++) {
                $ref = new stdClass();
                $ref->transaction_id = rand(10000, 99999);
                $ref->user_id = rand(100000, 200000);
                $ref->amount = rand(0, 5000);
                $ref->date = $date;

                $refunds->total += $ref->amount;
                
                array_push($refunds->data, $ref);
            }

            $reengages = new stdClass();
            $reengages->count = rand(-20, 30);
            if ($reengages->count < 0) {
                $reengages->count = 0;
            }
            $reengages->data = [];
            for ($i = 0; $i < $reengages->count; $i++) {
                array_push($reengages->data, rand(100000, 200000));
            }

            $downgrades = new stdClass();
            $downgrades->count = rand(-20, 10);
            if ($downgrades->count < 0) {
                $downgrades->count = 0;
            }
            $downgrades->data = [];
            for ($i = 0; $i < $downgrades->count; $i++) {
                array_push($downgrades->data, rand(100000, 200000));
            }

            $cancels = new stdClass();
            $cancels->total = rand(-10, 10);
            if ($cancels->total < 0) {
                $cancels->total = 0;
            }
            $cancels->data = [];
            for ($i = 0; $i < $cancels->total; $i++) {
                $can = new stdClass();
                $can->id = rand(10000, 99999);
                $can->user_id = rand(100000, 200000);
                array_push($cancels->data, $can);
            }

            /*
            {"total":19.95,"data":[{"transaction_id":88247,"user_id":1620283,"date":"2017-01-03 02:02:02","amount":"19.95"}]}
            */

            $cc_declines = new stdClass();
            $cc_declines->total = 0;
            $cc_declines->data = [];
            $count = rand(-20, 10);
            if ($count < 0) {
                $count = 0;
            }
            for ($i = 0; $i < $count; $i++) {
                $dec = new stdClass();
                $dec->transaction_id = rand(10000,100000);
                $dec->user_id = rand(100000, 200000);
                $dec->date = $date;
                $dec->amount = rand(10, 10000);
                $cc_declines->total += $dec->amount;

                array_push($cc_declines->data, $dec);
            }

            $cc_fails = new stdClass();
            $count = rand(-20, 10);
            if ($count < 0) {
                $count = 0;
            }
            
            $cc_fails->data = [];
            $cc_fails->total = 0;
            $count = 10;
            for ($i = 0; $i < $count; $i++) {
                $dec = new stdClass();
                $dec->id = rand(10000,100000);
                $dec->user_id = rand(100000, 200000);
                $dec->date = $date;
                $dec->amount = rand(10, 10000);
                $cc_fails->total += $dec->amount;

                array_push($cc_fails->data, $dec);
            }

            /*
            {"total":39.9,"data":[{"id":"88244","user_id":"1620123","date":"2016-12-29 02:02:02","amount":"19.95","type":"2","fails":[{"id":"88117","date":"2016-12-02 02:02:01"},{"id":"88126","date":"2016-12-05 02:02:02"},{"id":"88133","date":"2016-12-06 02:02:02"},{"id":"88140","date":"2016-12-07 02:02:02"},{"id":"88147","date":"2016-12-08 02:02:02"},{"id":"88154","date":"2016-12-09 02:02:03"},{"id":"88161","date":"2016-12-12 02:02:02"},{"id":"88168","date":"2016-12-13 02:02:01"},{"id":"88176","date":"2016-12-14 02:02:02"},{"id":"88182","date":"2016-12-15 02:02:01"},{"id":"88188","date":"2016-12-16 02:02:02"},{"id":"88194","date":"2016-12-19 02:02:02"},{"id":"88200","date":"2016-12-20 02:02:01"},{"id":"88207","date":"2016-12-21 02:02:02"},{"id":"88213","date":"2016-12-22 02:02:02"},{"id":"88218","date":"2016-12-23 02:02:01"},{"id":"88230","date":"2016-12-26 02:02:01"},{"id":"88234","date":"2016-12-27 02:02:02"},{"id":"88237","date":"2016-12-28 02:02:01"}]},{"id":"88245","user_id":"1620283","date":"2016-12-29 02:02:07","amount":"19.95","type":"2","fails":[{"id":"88128","date":"2016-12-05 02:02:02"},{"id":"88137","date":"2016-12-06 02:02:02"},{"id":"88144","date":"2016-12-07 02:02:03"},{"id":"88151","date":"2016-12-08 02:02:02"},{"id":"88158","date":"2016-12-09 02:02:03"},{"id":"88165","date":"2016-12-12 02:02:02"},{"id":"88172","date":"2016-12-13 02:02:02"},{"id":"88180","date":"2016-12-14 02:02:02"},{"id":"88186","date":"2016-12-15 02:02:02"},{"id":"88192","date":"2016-12-16 02:02:02"},{"id":"88198","date":"2016-12-19 02:02:02"},{"id":"88205","date":"2016-12-20 02:02:01"},{"id":"88210","date":"2016-12-21 02:02:02"},{"id":"88216","date":"2016-12-22 02:02:02"},{"id":"88222","date":"2016-12-23 02:02:02"},{"id":"88231","date":"2016-12-26 02:02:01"},{"id":"88235","date":"2016-12-27 02:02:02"},{"id":"88238","date":"2016-12-28 02:02:02"}]}]}
            */

            $cc_recoveries = new stdClass();
            $count = rand(-20, 20);
            if ($count < 0) {
                $count = 0;
            }
            $cc_recoveries->data = [];
            $cc_recoveries->total = 0;
            $count = 10;
            for ($i = 0; $i < $count; $i++) {
                $rec = new stdClass();
                $rec->id = rand(10000, 99999);
                $rec->user_id = rand(100000, 200000);
                $rec->date = $date;
                $rec->amount = rand(5, 2000);
                $cc_recoveries->total += $rec->amount;
                $rec->type = rand(1, 3);
                $rec->fails = [];

                $rec_count = rand(1, 20);
                for ($j = 0; $j < $rec_count; $j++) {
                    $fail = new stdClass();
                    $fail->id = rand(10000, 99999);
                    $fail->date = self::randomDate($start, $date);
                }
            }

            $membership += $members_new->count;
            $membership -= $cancels->total;
            
            $data->date = $date;
            $data->site_visits = json_encode($site_visits);
            $data->members_new = json_encode($members_new);
            $data->membership_current = json_encode($membership_current);
            $data->revenue = json_encode($revenue);
            $data->refunds = json_encode($refunds);
            $data->reengages = json_encode($reengages);
            $data->cancels = json_encode($cancels);
            $data->downgrades = json_encode($downgrades);
            $data->cc_declines = json_encode($cc_declines);
            $data->cc_fails = json_encode($cc_fails);
            $data->cc_recoveries = json_encode($cc_recoveries);
            $db->insertObject('axs_daily_stats', $data);

            unset($site_visits->data);
            unset($members_new->data);
            unset($membership_current->current->data);
            unset($membership_current->grace_period->data);
            unset($membership_current->affiliates->data);
            unset($revenue->data);
            unset($refunds->data);
            unset($reengages->data);
            unset($downgrades->data);
            unset($cancels->data);
            unset($cc_declines->data);
            unset($cc_fails->data);
            unset($cc_recoveries->data);

            $data->site_visits = json_encode($site_visits);
            $data->members_new = json_encode($members_new);
            $data->membership_current = json_encode($membership_current);
            $data->revenue = json_encode($revenue);
            $data->refunds = json_encode($refunds);
            $data->reengages = json_encode($reengages);
            $data->cancels = json_encode($cancels);
            $data->downgrades = json_encode($downgrades);
            $data->cc_declines = json_encode($cc_declines);
            $data->cc_fails = json_encode($cc_fails);
            $data->cc_recoveries = json_encode($cc_recoveries);

            $db->insertObject('axs_daily_stats_min', $data);
        }
    }

    function randomDate($start, $end) {
        //var_dump($start);
        $start = strtotime($start);
        $end = strtotime($end);
        $day = rand($start, $end);

        return date('Y-m-d h:m:s', $day);
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('Clidemo')->execute();