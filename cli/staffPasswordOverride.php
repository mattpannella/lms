<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/sso.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/tracking.php';



require_once JPATH_LIBRARIES . '/axslibs/virtualClassroom.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualMeetingServers.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';
require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
require_once JPATH_LIBRARIES . '/axslibs/okta.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateEditor extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";  AND id != 9

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND AND (status LIKE 'active%' OR status = 'trial')";
        $db_result = $conn->query($query);
        $i = 0;
        $total = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();


            $emailArray = [
                'cameron@cswatson.com',
                'mr.anthonyparker52@gmail.com',
                'ryanwillrankin@gmail.com',
                'mcavoybn@gmail.com',
                'info@designboise.com',
                'camdynnfunk@gmail.com',
                'bryanphactfunk@yahoo.com',
                'haydendanielson@gmail.com',
                'davidjacksondickman@gmail.com',
                'info@boisemobiledj.com',
                'dshewmaker@gmail.com',
                'greygerber@gmail.com',
                'troy@troymcclain.com',
                'Schual04@gmail.com ',
                'bowser@noreply.com',
                'bowserjr@noreply.com',
                'diddykong@noreply.com',
                'heather.miller+2@tovutiteam.com',
                'heatherlynnmiller33@gmail.com',
                'KingKoopa@noreply.com',
                'luigi@noreply.com',
                'mario@noreply.com',
                'princessdaisy@noreply.com',
                'princesspeach@noreply.com',
                'spiny@noreply.com',
                'heatherdurband@gmail.com',
                'heatherdurband+1@gmail.com',
                'heatherlynnmiller33+2@gmail.com',
                'yoshi@noreply.com',
                'misterdybarra@gmail.com',
                'neenermom@gmail.com',
                'callie.mulvihill@gmail.com',
                'callie.mulvhill@tovutiteam.com',
                'abby@castingextras.com',
                'ben@friends.com',
                'bryce@castingextras.com',
                'chandler@friends.com',
                'david@friends.com',
                'gary@friends.com',
                'gunther@friends.com',
                'janice@friends.com',
                'joey@friends.com',
                'kevin@friends.com',
                'marcel@friends.com',
                'marina@castingextras.com',
                'marta@friends.com',
                'mike@friends.com',
                'monica@friends.com',
                'phoebe@friends.com',
                'rachel@friends.com',
                'richard@friends.com',
                'riley@castingextras.com',
                'ross@friends.com',
                'will@friends.com',
                'rawhited20@gmail.com',
                'raustinwhiteid@gmail.com',
                'austinwhite@nnu.edu',
                'misterdybarra@gmail.com',
                'dennis@funnel33.com'
                ];
            $emailList = implode(',',$emailArray);

            $conditions[] = "FIND_IN_SET(email,'$emailList')";
            $conditions[] = "email LIKE '%tovuti%'";
            $conditions[] = "email LIKE '%axsteam%'";
            $conditions[] = "email LIKE '%ikrause%'";
            $conditions[] = "email LIKE '%nioak%'";
            $conditionSet = implode(' OR ',$conditions);

            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('joom_users');
            $query->where($conditionSet);
            $db->setQuery($query);
            $result = $db->loadObjectList();

            jimport('joomla.user.helper');
            echo "\n";
            echo 'id: '. $dbs->id.' named: '. $name;
            echo "\n";
            foreach($result as $user) {
                $password = AxsLMS::randomString(25);
                $user->password = JUserHelper::hashPassword($password);
                $db->updateObject('joom_users',$user,'id');
                echo "\n";
                echo 'id: '. $user->id.' name: '. $user->name.' email: '. $user->email;
                echo "\n";
            }
            $db->disconnect();
      }

    }
}

JApplicationCli::getInstance('CliUpdateEditor')->execute();