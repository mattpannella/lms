<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/sso.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/tracking.php';
require_once JPATH_LIBRARIES . '/axslibs/okta.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateEditor extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";  AND id != 9

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);
        $i = 0;
        $total = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();

            $updates = array();
            $video = new stdClass();
            $video->id = 133;
            $video->new_id = 173;
            $video->minor_version = 5;
            array_push($updates, $video);
            $ICvideo = new stdClass();
            $ICvideo->id = 127;
            $ICvideo->new_id = 174;
            $ICvideo->minor_version = 20;
            array_push($updates, $ICvideo);
            $image = new stdClass();
            $image->id = 158;
            $image->new_id = 175;
            $image->minor_version = 1;
            array_push($updates, $image);

            foreach($updates as $update) {
                $query = $db->getQuery(true);
                $query->select('*');
                $query->from('ic_h5p_libraries');
                $query->where("id=".$update->id);
                $db->setQuery($query);
                $ICItem = $db->loadObject();

                if($ICItem) {
                    $ICItem->id = '';
                    $ICItem->minor_version = $update->minor_version;
                    $db->insertObject('ic_h5p_libraries',$ICItem);

                    $query = $db->getQuery(true);
                    $query->select('*');
                    $query->from('ic_h5p_libraries_libraries');
                    $query->where("library_id=".$update->id);
                    $db->setQuery($query);
                    $libraries = $db->loadObjectList();
                    foreach($libraries as $row) {
                        $row->library_id = $update->new_id;
                        $db->insertObject('ic_h5p_libraries_libraries',$row);
                    }
                }
            }
            echo "\n";
            echo 'id: '. $dbs->id.' named: '. $name;
            echo "\n";
            $db->disconnect();
      }

    }
}

JApplicationCli::getInstance('CliUpdateEditor')->execute();