<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_WARNING);
// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
  die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
  require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
  define('JPATH_BASE', dirname(__DIR__));
  require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_CONFIGURATION . '/configuration.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_LIBRARIES . '/axslibs/payment.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';
require_once JPATH_LIBRARIES . '/axslibs/analytics.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once(JPATH_LIBRARIES . "/axslibs/clients.php");
require_once(JPATH_LIBRARIES . "/axslibs/language.php");
require_once(JPATH_LIBRARIES . "/axslibs/encryption.php");
require_once(JPATH_LIBRARIES . "/axslibs/lms.php");
require_once("/var/www/files/creds.php");

class updateDailyData extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    /*public $db = null;
    public $dispatcher = null;
    public $grace_period = null;*/
    public $grace_period = null;
    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND run_crons=1 AND (status LIKE 'active%' OR status = 'test')";
        $db_result = $conn->query($query);

        while($dbs = $db_result->fetch_object()) {

            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $config = &JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $options = array();
            $options['driver']   = 'mysqli';           // Database driver name
            $options['host']     = $creds->dbhost;        // Database host name
            $options['user']     = $creds->dbuser;       // User for database authentication
            $options['password'] = $creds->dbpass;       // Password for database authentication
            $options['database'] = $params->dbname;       // Database name
            $options['prefix']   = $dbs->dbprefix;     // Database prefix (may be empty)
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;
            echo $name.": checking\n";
            if (self::checkExists() != true) {

                //grab the dispatcher and load plugins
                $dispatcher = JEventDispatcher::getInstance();
                JPluginHelper::importPlugin('axs');
                $query = "SELECT plans.id AS id, cat.brand_id AS brand
                        FROM axs_pay_subscription_plans AS plans
                        INNER JOIN axs_pay_subscription_categories AS cat
                        WHERE plans.cat_id = cat.id";
                $db->setQuery($query);
                $result = $db->loadObjectList();

                $this->grace_period = [];
                $result_count = count($result);
                for ($i = 0; $i < $result_count; $i++) {

                    $brand_id = $result[$i]->brand;

                    $brand = AxsBrands::getBrandById($brand_id);

                    $plan = $result[$i]->id;
                    $grace_period = $brand->billing->grace_period;

                    $this->grace_period[$plan] = $grace_period;
                }

                $info = new stdClass();
                //echo "Checkpoint 1\n";
                self::calculateCurrent();
                //echo "Checkpoint 2\n";


                /*

                    date                                            Date of record.  Should be only one per day.
                    site_visits                                     Amount of users that visited the site.
                    members_new                                     New members added on this date as well as sign up amount.
                    membership_current                              Data on all members, such as if they're current, in a grace period, affiliates, etc.
                    revenue                                         Revenue of the day from upgrades, events, membership dues, etc.
                    refunds                                         Amount of money refunded
                    reengages                                       Members that previously downgraded and have since started paying again.
                    downgrades                                      Members that are now no longer paying.
                    cancels                                         Members that actively cancelled.
                    cc_declines                                     Credit card declines
                    cc_recoveries                                   Credit card declines or fails that have been recovered.
                    cc_fails                                        Credit card failures.

                */
                 echo $name.": updated\n";
            }
            //self::recalculatePrev($name);
            //echo "Checkpoint 3\n";
            sleep(0.5);
            $db->disconnect();
        }
    }

    private function checkExists() {

        /*
            In case the CLI does not run, it can be checked multiple times per day.
            If it has not run, there will be no data.
            If it has run, the query will return results, which means it has already been run for the day
            and all is well.
        */

        $query = "SELECT * FROM axs_daily_stats WHERE DATE(date) = DATE_SUB(CURDATE(), INTERVAL 1 DAY)";

        $db = JFactory::getDbo();

        $db->setQuery($query);
        $result = $db->loadObjectList();
        if (count($result) == 0) {
            return false;
        } else {
            return true;
        }
    }

    private function calculateCurrent() {

        /*
            This function calculates all data for the day.  Since it is run at midnight, or 00:00:00, it has to be run for the previous day.
            Some data can only be calculated on the exact date (such as the current membership) since it is not time stamped.
        */

        $format = "Y-m-d h:i:s";
        $date = date($format, strtotime('yesterday'));

        $info = new stdClass();
        //echo "  Sub_Checkpoint 1\n";
        $info->site_visits = self::getSiteVisits($date);
        //echo "  Sub_Checkpoint 2\n";
        $info->membership_current = self::getMembershipData($date);
        //echo "  Sub_Checkpoint 3\n";
        $info->members_new = self::getNewUsers($date);
        //echo "  Sub_Checkpoint 4\n";
        $info->revenue = self::getRevenue($date);
        //echo "  Sub_Checkpoint 5\n";
        $info->refunds = self::getRefunds($date);
        //echo "  Sub_Checkpoint 6\n";
        //$info->reengages = self::getReengages($date);
        //echo "  Sub_Checkpoint 7\n";
        //$info->downgrades = self::getDowngrades($date);
        //echo "  Sub_Checkpoint 8\n";
        $info->cancels = self::getCancels($date);
        //echo "  Sub_Checkpoint 9\n";
        $info->cc_declines = self::getCCDeclines($date);
        //echo "  Sub_Checkpoint 10\n";
        $info->cc_recoveries = self::getCCRecoveries($date);
        //echo "  Sub_Checkpoint 11\n";
        $info->cc_fails = self::getCCFails($date);
        //echo "  Sub_Checkpoint 12\n";
        /*$info->courses = self::getCoursesStats($date);
        echo "  Sub_Checkpoint 13\n";
        $info->certificates = self::getCertificatesStats($date);
        echo "  Sub_Checkpoint 14\n";
        $info->users = self::getAllUsers($date);
        echo "  Sub_Checkpoint 15\n";*/


        $info_min = unserialize(serialize($info));


        unset($info_min->site_visits->data);
        unset($info_min->members_new->data);
        unset($info_min->membership_current->current->data);
        unset($info_min->membership_current->grace_period->data);
        unset($info_min->membership_current->affiliates->data);
        unset($info_min->revenue->data);
        unset($info_min->refunds->data);
        //unset($info_min->reengages->data);
        //unset($info_min->downgrades->data);
        unset($info_min->cancels->data);
        unset($info_min->cc_declines->data);
        unset($info_min->cc_fails->data);
        unset($info_min->cc_recoveries->data);

        foreach ($info as $key => $value) {
            $info->$key = json_encode($info->$key);
            $info_min->$key = json_encode($info_min->$key);
        }

        $info->date = $date;
        $info_min->date = $date;

        $info->reengages = '';
        $info->downgrades = '';
        $info_min->reengages = '';
        $info_min->downgrades = '';

        $db = JFactory::getDbo();

        // Duplicate Entries are not allowed.  Will crash if reran on same day.
        $db->insertObject('axs_daily_stats', $info);
        $db->insertObject('axs_daily_stats_min', $info_min);


    }

    private function recalculatePrev($name) {
        /*
            This function goes through all of the days in the database and recalculates date specific data.  This allows it to
            recorrect any data that was changed at a later date than the original 'snapshot'.
        */
        echo "Recalculating...\n";
        $range = AxsAnalytics::getDateRange();
        $date = $range->first->string;

        $previousMembership = null;

        while (strtotime($date) <= strtotime('yesterday')) {
            $db = JFactory::getDbo();

            $query = "SELECT * FROM axs_daily_stats WHERE DATE('" . $date . "') = DATE(date)";

            $db->setQuery($query);
            $info = $db->loadObject();

            $newEntry = false;

            if (count($info) == 0) {
                $newEntry = true;
                $info = new stdClass();
            } else {
                $info->site_visits = json_decode($info->site_visits);
                $info->membership_current = json_decode($info->membership_current);
            }


            //echo "  Sub_Checkpoint 1\n";
            $info->site_visits = self::getSiteVisits($date);
            //echo "  Sub_Checkpoint 2\n";
            $info->membership_current = self::getMembershipData($date);
            //echo "  Sub_Checkpoint 3\n";
            $info->members_new = self::getNewUsers($date);
            //echo "  Sub_Checkpoint 4\n";
            $info->revenue = self::getRevenue($date);
            //echo "  Sub_Checkpoint 5\n";
            $info->refunds = self::getRefunds($date);
            //echo "  Sub_Checkpoint 6\n";
            //$info->reengages = self::getReengages($date);
            //echo "  Sub_Checkpoint 7\n";
            //$info->downgrades = self::getDowngrades($date);
            //echo "  Sub_Checkpoint 8\n";
            $info->cancels = self::getCancels($date);
            //echo "  Sub_Checkpoint 9\n";
            $info->cc_declines = self::getCCDeclines($date);
            //echo "  Sub_Checkpoint 10\n";
            $info->cc_recoveries = self::getCCRecoveries($date);
            //echo "  Sub_Checkpoint 11\n";
            $info->cc_fails = self::getCCFails($date);
            //echo "  Sub_Checkpoint 12\n";
            /*$info->courses = self::getCoursesStats($date);
            echo "  Sub_Checkpoint 13\n";
            $info->certificates = self::getCertificatesStats($date);
            echo "  Sub_Checkpoint 14\n";
            $info->users = self::getAllUsers($date);
            echo "  Sub_Checkpoint 15\n";*/


            $info_min = unserialize(serialize($info));


            unset($info_min->site_visits->data);
            unset($info_min->members_new->data);
            unset($info_min->membership_current->current->data);
            unset($info_min->membership_current->grace_period->data);
            unset($info_min->membership_current->affiliates->data);
            unset($info_min->revenue->data);
            unset($info_min->refunds->data);
            //unset($info_min->reengages->data);
            //unset($info_min->downgrades->data);
            unset($info_min->cancels->data);
            unset($info_min->cc_declines->data);
            unset($info_min->cc_fails->data);
            unset($info_min->cc_recoveries->data);

            //echo "  Sub_Checkpoint 13\n";
            $skip_list = array(
                "id",
                "date"
            );
            //echo "  Sub_Checkpoint 14\n";
            foreach ($info as $key => $value) {

                if (in_array($key, $skip_list)) {
                    continue;
                }

                $info->$key = json_encode($info->$key);
                $info_min->$key = json_encode($info_min->$key);
            }
            $info->reengages = '';
            $info->downgrades = '';
            $info_min->reengages = '';
            $info_min->downgrades = '';
            $db = JFactory::getDbo();
            //echo "  Sub_Checkpoint 15\n";
            if ($newEntry) {
                //Day is missing from database
                //echo "  Sub_Checkpoint 16\n";
                $info->date = $date;
                $info_min->date = $date;

                $info->site_visits = json_encode(
                    (object) array(
                        'count' => 0,
                        'data' => array()
                    )
                );

                $info_min->site_visits = json_encode(
                    (object) array(
                        'count' => 0
                    )
                );
                //echo "  Sub_Checkpoint 17\n";
                $membership_current = json_decode($previousMembership);
                $membership_current->current->data = null;
                $membership_current->affiliates->data = null;
                $membership_current->grace_period->data = null;

                $info->membership_current = json_encode($membership_current);
                //echo "  Sub_Checkpoint 18\n";
                unset($membership_current->current->data);
                unset($membership_current->affiliates->data);
                unset($membership_current->grace_period->data);

                $info_min->membership_current = json_encode($membership_current);

                $db->insertObject('axs_daily_stats', $info);
                $db->insertObject('axs_daily_stats_min', $info_min);
            } else {
                echo "  Sub_Checkpoint 19\n";
                echo "\n".var_dump($info)."\n";
                $db->updateObject('axs_daily_stats', $info, 'id');
                echo "  Sub_Checkpoint 20\n";
                $db->updateObject('axs_daily_stats_min', $info_min, 'id');
                $previousMembership = $info->membership_current;
                echo "  Sub_Checkpoint 21\n";
            }

            $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
        }
        echo $name.": recalculated\n";

    }

    private function getSiteVisits($date) {
        $db = JFactory::getDbo();

        $query =
            "SELECT id FROM joom_users
            WHERE DATE(lastvisitDate) = DATE('" . $date . "')";

        $db->setQuery($query);

        $visits = new stdClass();

        $visit_today = $db->loadObjectList();

        $visitors = array();

        foreach($visit_today as $visitor) {
            $id = (int)$visitor->id;
            array_push($visitors, $id);
        }

        $visits->count = count($visitors);
        $visits->data = $visitors;

        return $visits;
    }

    private function getAllUsers($date) {
        $db = JFactory::getDbo();
        $query = "SELECT id FROM joom_users WHERE block=0";
        $db->setQuery($query);
        $users = $db->loadObjectList();

        $userData = new stdClass();
        $userData->count = count($users);
        return $userData;
    }

    private function getMembershipData($date) {

        $db = JFactory::getDbo();

        $query = "SELECT * FROM axs_pay_subscriptions WHERE status = 'ACT'";
        $db->setQuery($query);
        $userListCurrent = $db->loadObjectList();

        $query = "SELECT * FROM axs_pay_subscriptions WHERE status = 'GRC'";
        $db->setQuery($query);
        $userListGrace = $db->loadObjectList();

        $query = "SELECT * FROM axs_pay_subscriptions WHERE status = 'INAC'";
        $db->setQuery($query);
        $userListInactive = $db->loadObjectList();

        $current_list = [];
        $grace_list = [];
        $innactive_list = [];

        $countCurrent = count($userListCurrent);
        $countGrace = count($userListGrace);
        $countInnactive = count($userListInactive);

        for ($i = 0; $i < $countCurrent; $i++) {
            $sub = $userListCurrent[$i];
            $plan = $sub->plan_id;

            if (!isset($current_list[$plan])) {
                $current_list[$plan] = [];
            }

            array_push($current_list[$plan], $sub->user_id);
        }

        for ($i = 0; $i < $countGrace; $i++) {
            $sub = $userListGrace[$i];
            $plan = $sub->plan_id;

            if (!isset($grace_list[$plan])) {
                $grace_list[$plan] = [];
            }

            array_push($grace_list[$plan], $sub->user_id);
        }

        for ($i = 0; $i < $countInnactive; $i++) {
            $sub = $userListInactive[$i];
            $plan = $sub->plan_id;

            if (!isset($innactive_list[$plan])) {
                $innactive_list[$plan] = [];
            }

            array_push($innactive_list[$plan], $sub->user_id);
        }

        $current_info = new stdClass();
        $grace_info = new stdClass();
        $innactive_info = new stdClass();

        $current_info->count = $countCurrent;
        $current_info->data = $current_list;

        $grace_info->count = $countGrace;
        $grace_info->data = $grace_list;

        $innactive_info->count = $countInnactive;
        $innactive_info->data = $innactive_list;

        $membership_data = new stdClass();
        $membership_data->current = $current_info;
        $membership_data->affiliates = $innactive_info;
        $membership_data->grace_period = $grace_info;

        return $membership_data;
    }

    private function getNewUsers($date) {
        $db = JFactory::getDbo();

        $query =
            "SELECT DISTINCT id FROM joom_users
            WHERE DATE(registerDate) = DATE('" . $date . "')";

        $db->setQuery($query);

        $newUsers = $db->loadObjectList();
        $newUserList = array();
        foreach($newUsers as $user) {
            $id = (int)$user->id;
            array_push($newUserList, $id);
        }
        $newUsers = new stdClass();
        $newUsers->count = count($newUserList);
        $newUsers->data = $newUserList;

        return $newUsers;
    }


    private function getCoursesStats($date) {
        $db = JFactory::getDbo();
        $query = "SELECT splms_course_id AS id, title FROM joom_splms_courses WHERE enabled = 1";
        $db->setQuery($query);
        $courses = $db->loadObjectList();
        $coursesStatsArray = array();
        foreach($courses as $course) {
            $coursesStats = new stdClass();
            $coursesStats->start_count = AxsLMS::getCourseDateStartedCount($course->id,$date);
            $coursesStats->last_activity_count = AxsLMS::getCourseLastActivityCount($course->id,$date);
            $coursesStats->completed_count = AxsLMS::getCourseDateCompletedCount($course->id,$date);
            $coursesStats->title = $course->title;
            $coursesStatsArray[$course->id] = $coursesStats;
        }
        $courseStats = new stdClass();
        $courseStats->data = $coursesStatsArray;
        return $courseStats;
    }

    private function getCertificatesStats($date) {
        $db = JFactory::getDbo();

        $query =
            "SELECT DISTINCT id FROM joom_users
            WHERE DATE(registerDate) = DATE('" . $date . "')";

        $db->setQuery($query);

        $newUsers = $db->loadObjectList();
        $newUserList = array();
        foreach($newUsers as $user) {
            $id = (int)$user->id;
            array_push($newUserList, $id);
        }
        $newUsers = new stdClass();
        $newUsers->count = count($newUserList);
        $newUsers->data = $newUserList;

        return $newUsers;
    }

    private function getRevenue($date) {
        $db = JFactory::getDbo();

        $revenue_types = AxsPayment::$trxn_types;

        $query = "SELECT SUM(amount) FROM axs_pay_transactions WHERE status='SUC' AND DATE(date) = DATE('$date') AND type=" . AxsPayment::$trxn_types['course'];
        $db->setQuery($query);
        $upgrades = $db->loadResult();

        $query = "SELECT SUM(amount) FROM axs_pay_transactions WHERE status='SUC' AND DATE(date) = DATE('$date') AND type=" . AxsPayment::$trxn_types['subscription'];
        $db->setQuery($query);
        $dues = $db->loadResult();

        $query = "SELECT SUM(amount) FROM axs_pay_transactions WHERE status='SUC' AND DATE(date) = DATE('$date') AND type=" . AxsPayment::$trxn_types['event'];
        $db->setQuery($query);
        $events = $db->loadResult();

        $query = "SELECT SUM(amount) FROM axs_pay_transactions WHERE status='SUC' AND DATE(date) = DATE('$date') AND type=" . AxsPayment::$trxn_types['e-commerce'];
        $db->setQuery($query);
        $ecommerce = $db->loadResult();

        /*
            Payment Plans for Courses
        */
        $query = "SELECT SUM(amount) FROM axs_pay_transactions AS a JOIN axs_pay_user_pplan_periods AS b
                    ON a.type_id = b.id
                    JOIN axs_pay_user_pplans AS c
                    ON b.upp_id = c.id
                    WHERE a.type = '" . AxsPayment::$trxn_types['payment_plan'] . "' AND
                    c.type='" . AxsPayment::$trxn_types['course'] . "' AND
                    a.status='SUC'";

        $db->setQuery($query);
        $pay_course = $db->loadResult();

        /*
            Payment Plans for Events
        */
        $query = "SELECT SUM(amount) FROM axs_pay_transactions AS a JOIN axs_pay_user_pplan_periods AS b
                    ON a.type_id = b.id
                    JOIN axs_pay_user_pplans AS c
                    ON b.upp_id = c.id
                    WHERE a.type = '" . AxsPayment::$trxn_types['payment_plan'] . "' AND
                    c.type='" . AxsPayment::$trxn_types['event'] . "' AND
                    a.status='SUC'";

        $db->setQuery($query);
        $pay_event = $db->loadResult();

        $total = $dues + $upgrades + $events + $ecommerce + $pay_course + $pay_event;

        /*
            Adding 0 removes any NULLs in case there are no values returned

            $result = NULL;
            $total = $result;

            Now $total is NULL

            $total = $result + 0;

            Now $total is 0
        */

        $revenue = new stdClass();
        $revenue->upgrades = $upgrades + $pay_course + 0;
        $revenue->dues = $dues + 0;
        $revenue->events = $events + $pay_event + 0;
        $revenue->total = $total + 0;
        $revenue->ecommerce = $ecommerce + 0;

        return $revenue;
    }

    private function getRefunds($date) {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM axs_pay_transactions WHERE DATE(date) = DATE('" . $date . "') AND status='REF'";

        $db->setQuery($query);
        $refunds = $db->loadObjectList();

        $refund_list = array();

        $total = 0;

        foreach ($refunds as $refund) {
            $total += $refund->amount;

            $data = new stdClass();
            $data->transaction_id = (int)$refund->id;
            $data->user_id = (int)$refund->user_id;
            $data->amount = $refund->amount;
            $data->date = $refund->date;

            array_push($refund_list, $data);
        }

        $refunds = new stdClass();
        $refunds->total = $total;
        $refunds->data = $refund_list;

        return $refunds;
    }

    private function getReengages($date) {
        /*
            The first query checks to see if there was a lapse in payment.
            Check to see if they
                *have a payment on this date that is a membership plan
                *doesn't have a next_payment_date in the previous month

            The second query checks to see if they were an old member before
            moving to the new system and have recently signed back up.
        */

        $db = JFactory::getDbo();

        $query = "SELECT a.* from axs_pay_subscriptions AS a
                INNER JOIN axs_pay_subscription_periods AS b ON a.id = b.sub_id
                LEFT JOIN axs_pay_subscription_periods AS c ON a.id = c.sub_id
                AND DATE(b.start) = DATE(c.end)
                INNER JOIN axs_pay_subscription_periods AS d ON a.id = d.sub_id
                AND DATE(b.start) > DATE(d.end)
                WHERE a.status = 'ACT'
                AND DATE(b.start) = DATE('" . $date . "')
                AND b.status='PAID'
                AND c.id IS NULL
                AND d.id IS NOT NULL
                GROUP BY a.id";

        $db->setQuery($query);
        $reengages = $db->loadObjectList();

        $reengage_list = array();
        foreach($reengages as $reengage) {
            $user_id = (int)$reengage->user_id;
            if (self::checkIgnore($user_id)) {
                continue;
            }

            array_push($reengage_list, $user_id);
        }

        $data = new stdClass();
        $data->count = count($reengage_list);
        $data->data = $reengage_list;

        return $data;
    }

    private function getDowngrades($date) {

        $db = JFactory::getDbo();

        $downgrade_list = [];

        foreach($this->grace_period as $plan => $grace_period) {

            $days = new DateInterval("P" . $grace_period . "D");        //Create an interval of time with the grace period.

            $grace_date = date_create($date);                           //Create a DateInterval object from the date string
            date_sub($grace_date, $days);                               //Subtract the grace period
            $grace_date = $grace_date->format('Y-m-d');                 //Get the string of the date the grace period starts.

            $query = "SELECT a.* FROM axs_pay_subscriptions AS a
                INNER JOIN axs_pay_subscription_periods AS b ON a.id = b.sub_id
                INNER JOIN axs_pay_subscription_periods AS c ON a.id = c.sub_id
                WHERE a.status = 'INAC'
                AND a.plan_id = " . $plan . "
                AND DATE(b.end) = DATE('" . $grace_date . "')
                AND b.status = 'PAID'
                AND DATE(c.start) = DATE('" . $grace_date . "')
                AND c.status = 'NONE'";

            $db->setQuery($query);
            $result = $db->loadObjectList();

            $count = count($result);

            for ($i = 0; $i < $count; $i++) {
                array_push($downgrade_list, (int)$result[$i]->user_id);
            }
        }

        $downgrades = new stdClass();
        $downgrades->count = count($downgrade_list);
        $downgrades->data = $downgrade_list;

        return $downgrades;
    }

    private function getCancels($date) {
        $db = JFactory::getDbo();

        $query = "SELECT * FROM axs_pay_subscriptions WHERE DATE(cancel) = DATE('" . $date . "')";

        $db->setQuery($query);

        $cancels = $db->loadObjectList();

        $cancel_list = array();

        foreach($cancels as $cancel) {
            $data = new stdClass();
            $data->id = (int)$cancel->id;
            $data->user_id = (int)$cancel->user_id;

            array_push($cancel_list, $data);
        }

        $cancels = new stdClass();
        $cancels->total = count($cancel_list);
        $cancels->data = $cancel_list;

        return $cancels;
    }

    private function getCCDeclines($date) {
        $db = JFactory::getDbo();

        $query =
            "SELECT * FROM axs_pay_transactions
            WHERE DATE(date) = DATE('" . $date . "')
            AND status='DEC'";

        $db->setQuery($query);

        $declines = $db->loadObjectList();
        $total = 0;

        $decline_list = array();

        foreach($declines as $decline) {
            $total += $decline->amount;

            $data = new stdClass();
            $data->transaction_id = (int)$decline->id;
            $data->user_id = (int)$decline->user_id;
            $data->date = $decline->date;
            $data->amount = $decline->amount;

            array_push($decline_list, $data);
        }

        $declines = new stdClass();
        $declines->total = $total;
        $declines->data = $decline_list;

        return $declines;
    }

    private function getCCRecoveries($date) {
        $db = JFactory::getDbo();

        $query = "SELECT
            a.id AS recover_id,
            a.date AS recover_date,
            a.amount AS recover_amount,
            a.user_id AS user_id,

            b.id AS fail_id,
            b.date AS fail_date,
            b.type AS trans_type

            FROM axs_pay_transactions AS a
            INNER JOIN axs_pay_transactions AS b
            ON (a.type_id = b.type_id AND a.type = b.type)

            WHERE a.status = 'SUC'
            AND (b.status = 'DEC' OR b.status = 'FAIL')

            AND DATE(a.date) = DATE('" . $date . "')
            ";

        $db->setQuery($query);
        $recoveries = $db->loadObjectList();

        $recovery_list = [];

        foreach($recoveries as $recovery) {
            $recover_id = $recovery->recover_id;
            if (!isset($recovery_list[$recover_id])) {
                $recovery_list[$recover_id] = array();
            }

            array_push($recovery_list[$recover_id], $recovery);
        }

        $recoveries_list = array();

        $total = 0;

        foreach($recovery_list as $list) {

            $fail_list = array();
            foreach($list as $fail) {
                $info = new stdClass();
                $info->id = $fail->fail_id;
                $info->date = $fail->fail_date;

                array_push($fail_list, $info);
            }

            $recovery = new stdClass();

            $recovery->id = $list[0]->recover_id;
            $recovery->user_id = $list[0]->user_id;
            $recovery->date = $list[0]->recover_date;
            $recovery->amount = $list[0]->recover_amount;
            $recovery->type = $list[0]->trans_type;
            $recovery->fails = $fail_list;

            $total += $recovery->amount;

            array_push($recoveries_list, $recovery);
        }

        $recoveries = new stdClass();

        $recoveries->total = $total;
        $recoveries->data = $recoveries_list;

        return $recoveries;
    }

    private function getCCFails($date) {
        $db = JFactory::getDbo();

        $query =
            "SELECT * FROM axs_pay_transactions
            WHERE DATE(date) = DATE('" . $date . "')
            AND status='FAIL'";

        $db->setQuery($query);

        $fails = $db->loadObjectList();

        $fail_list = array();

        $total = 0;
        foreach($fails as $fail) {
            $id = (int)$fail->id;
            $user_id = (int)$fail->user_id;

            $data = new stdClass();
            $data->id = $id;
            $data->user_id = $user_id;
            $data->amount = $fail->amount;
            $data->date = $fail->date;

            $total += $fail->amount;

            array_push($fail_list, $data);
        }

        $fails = new stdClass();
        $fails->total = $total;
        $fails->data = $fail_list;

        return $fails;
    }

    /* ignore users that are FREE or SUPER USERS */
    private function checkIgnore($id) {
        $userGroups = JUserHelper::getUserGroups($id);
        $ignoreList = [8, 31];
        for ($i = 0; $i < count($ignoreList); $i++) {
            if (in_array($ignoreList[$i], $userGroups)) {
                return true;
            }
        }
        return false;
    }
}

// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('updateDailyData')->execute();
