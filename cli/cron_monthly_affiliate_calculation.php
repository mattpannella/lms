<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 3/28/16
 * Time: 4:51 PM
 */

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
    die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_CONFIGURATION . '/configuration.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_LIBRARIES . '/axslibs/payment.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';

class MonthlyAffiliateCalculation extends JApplicationCli {
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */
    public function doExecute() {
      //grab the dispatcher and load axs plugins
      JPluginHelper::importPlugin('axs');
      $dispatcher = JEventDispatcher::getInstance();

	    $total = 0;
	    $aborted = 0;
	    $success = 0;
	    $error = 0;

	    //first grab which plans belong to which brand.
	    $brandPlans = array();
	    $planDescs = array();
	    $brandPlanEarns = array();
	    $brandPlanRefers = array();

	    $db = JFactory::getDbo();
	    $query = "SELECT a.id, a.title, a.earn_points, a.refer_points, b.brand_id FROM axs_pay_subscription_plans a JOIN axs_pay_subscription_categories b ON a.cat_id=b.id;";
	    $db->setQuery($query);
	    $plans = $db->loadObjectList();
	    foreach($plans as $p) {
		    if(array_key_exists($p->brand_id, $brandPlans)) {
			    $brandPlans[$p->brand_id][] = $p->id; //if existing brand, append to plan_id array.
		    } else {
			    $brandPlans[$p->brand_id] = array($p->id); //if new brand, create an array of plan_id belonging to it.
		    }

		    $brandPlanEarns[$p->brand_id][$p->id] = $p->earn_points;
		    $brandPlanRefers[$p->brand_id][$p->id] = $p->refer_points;
		    $planDescs[$p->id] = $p->title;
	    }

	    $start = date('Y-m-01 00:00:00', strtotime("first day of previous month")); //first of last month
	    $end = date('Y-m-01 00:00:00'); //first of current year and month

	    //now loop through the brands and do the payments stuff.
	    foreach($brandPlans as $brand_id => $planArray) {
		    //first grab the brand.
		    $brand = AxsBrands::getBrandById($brand_id);

		    //init the libs with the correct brand's configs
		    AxsPayment::init($dispatcher, $brand);
		    AxsExtra::init($dispatcher, $brand);

		    //grab the affilate levels for this brand.
		    $query = "SELECT * FROM axs_gin_affiliate_levels WHERE brand_id=$brand_id;";
		    $db->setQuery($query);
		    $affiliateLevels = $db->loadObjectList();
		    $affiliateLevels = array_reverse($affiliateLevels);

		    //make sure there are affiliate levels for this brand and that rewards system is turned on.
		    if(count($affiliateLevels) && $brand->billing->rewards_active) {
		    	$canEarn = array();
			    foreach($brandPlanEarns[$brand_id] as $plan => $earn) {
			    	if($earn) {
			    		$canEarn[] = $plan;
				    }
			    }
			    $canRefer = array();
			    foreach($brandPlanRefers[$brand_id] as $plan => $refer) {
				    if($refer) {
					    $canRefer[] = $plan;
				    }
			    }
			    $canEarn = implode(', ', $canEarn);
			    $canRefer = implode(', ', $canRefer);

          //foreach user that's referred someone within this brand to a plan.
			    $query = "SELECT c.*, COUNT(*) as active_referees
										FROM axs_pay_subscriptions a 
										JOIN axs_rewards_data b ON a.user_id=b.user_id AND (a.status='GRC' OR a.status='ACT')
										JOIN axs_pay_subscriptions c ON b.referrer_id=c.user_id AND (c.status='GRC' OR c.status='ACT')
										WHERE a.plan_id IN ($canRefer) AND c.plan_id IN ($canEarn)
										GROUP BY b.referrer_id;";
			    $db->setQuery($query);
			    $referrers = $db->loadObjectList();
			    foreach($referrers as $user) {
			      $total++;

			      //calc the user's affiliate level
				    $aflevel = 0;
				    foreach($affiliateLevels as $afl) {
					    if($user->active_referees >= $afl->members_needed) {
						    //they have enough active referees to be this affiliate level
						    $aflevel = $afl;
						    break;
					    }
				    }

				    //remove all rewards transactions for the last month where status is Pending for this user.
				    $db->setQuery("DELETE FROM axs_rewards_transactions WHERE user_id=".(int)$user->user_id." AND status='Pending';");
				    $db->execute();

				    //now actually grab all the referees trxns for this user
				    $pointsToAdd = 0;
				    $query = "SELECT a.referrer_id, b.* 
											FROM axs_rewards_data a
											JOIN axs_pay_transactions b ON a.user_id=b.user_id  AND b.status='SUC'
											WHERE a.referrer_id=$user->user_id AND b.date>'$start' AND b.date<'$end';";
				    $db->setQuery($query);
				    $referees_trxns = $db->loadObjectList();
				    foreach($referees_trxns as $trxn) {
					    $trxnPoints = 0;
				      if($trxn->type == AxsPayment::$trxn_types['subscription']) {
				        $trxnPoints = $trxn->amount * $brand->billing->rewards_quotient * ($aflevel->on_monthly_percentage / 100);
						    $desc = 'Subscription Payment';
					    } else {
					      //all other trxn types get the upgrade percentage.
						    $trxnPoints = $trxn->amount * $brand->billing->rewards_quotient * ($aflevel->on_upgrade_percentage / 100);
						    $desc = 'Miscellaneous Purchase';
					    }

					    $pointsToAdd += $trxnPoints;

					    if($trxnPoints) {
						    //make a rewards trxn for them.
						    $query = "INSERT INTO axs_rewards_transactions 
												(user_id, date, referee_id, amount, reason, status)
												VALUES ($user->user_id, '$end', $trxn->user_id, $trxnPoints, '$desc', 'Awarded');";
						    $db->setQuery($query);
						    $db->execute();
					    }
				    }

				    //now that we know how many points to add and what their level is, assign this new info to their account.
				    $query = "UPDATE axs_rewards_data SET level=$aflevel->level_id, points=points+$pointsToAdd WHERE user_id=$user->user_id;";
				    $db->setQuery($query);
				    $db->execute();

				    //now that we know all the info, lets store a snapshot of this user in the aff history table.
				    $query = "INSERT INTO axs_rewards_history (user_id, date, affiliate_level, points, active_referred)
												SELECT
										      user_id,
										      '$end',
										      $aflevel->level_id,
										      points,
										      $user->active_referees
											  FROM axs_rewards_data WHERE user_id=$user->user_id;";
				    $db->setQuery($query);
				    $db->execute();
			    }
		    }
	    }

	    $date = date('Y-m-d');
	    echo "\nDate: $date\n"
		    ."Attempted to make: $total\n";
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('MonthlyAffiliateCalculation')->execute();