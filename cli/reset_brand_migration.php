<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/sendgrid.php';
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
error_reporting(0);
ini_set('display_errors', 0);

class CliResetBrandMigration extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE id=325 AND enabled=1 AND (status LIKE 'active%' OR status = 'test' OR status = 'trial')";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            
            // Brands 
            $db->setQuery('
            DELETE FROM axs_brands WHERE id > 1;
            ');
            $db->execute();

            // // Get the default brand
            $db->setQuery('
            SELECT * FROM axs_brands WHERE id = 1;
            ');
            $brand = $db->loadObject();
            $brand->default_brand = "0";
            $brand->data = '{
                "site_title": "Default Brand",
                "description": "",
                "site_details": {
                    "plans_for_active": null,
                    "default_font": "Arial",
                    "language_selector": "1",
                    "language_selector_options": [
                        "en-GB",
                        "fr-FR",
                        "es-ES"
                    ],
                    "tovuti_root_styles": "0",
                    "root_primary_color": "#030303",
                    "root_accent_color": "#7D7D7D",
                    "root_base_color": "#FFFFFF",
                    "homepage_type": "landing_page",
                    "login_page": "7"
                },
                "billing": {
                    "admin_emails": [],
                    "currency_id": "124",
                    "currency_code": "USD",
                    "currency_country": "United States",
                    "currency_name": "Dollars",
                    "currency_symbol": "$",
                    "auto_reprocess_declines": false,
                    "auto_reprocess_days": "1",
                    "weekend_billing": true,
                    "weekend_fallback": false,
                    "holiday_billing": false,
                    "grace_period": 30,
                    "rewards_active": false,
                    "rewards_quotient": 0,
                    "show_unpaid_alert": false,
                    "gateway_type": "heartland",
                    "converge_production_mode": "0",
                    "converge_dev_merchant_id": "000314",
                    "converge_dev_user_id": "000314",
                    "converge_dev_account_pin": "L5GHNVQD4GA7IVO0WTSXOBJ37PF3ZM77WU81EBRIIMK7BD2LCN1VOY9SVWLNIYK0",
                    "converge_prod_merchant_id": "",
                    "converge_prod_user_id": "",
                    "converge_prod_account_pin": "",
                    "authnet_production_mode": "0",
                    "authnet_dev_api_login_id": "5FkL3bw3Y",
                    "authnet_dev_transaction_key": "985Rjd87NYxc6j4M",
                    "authnet_prod_api_login_id": "",
                    "authnet_prod_transaction_key": "",
                    "stripe_production_mode": "0",
                    "stripe_dev_api_key": "sk_test_yPOQVPrUNr18xNw54vyv64750068RUBgJv",
                    "stripe_prod_api_key": "",
                    "stripe_dev_public_api_key": "pk_test_maXjYdMQKoSB9r39pQTdgRig00eov2Tvvl",
                    "stripe_prod_public_api_key": "",
                    "stripe_hide_zip": "0",
                    "heartland_production_mode": "0",
                    "heartland_prod_secret_api_key": "",
                    "heartland_prod_public_api_key": "",
                    "heartland_dev_secret_api_key": "skapi_cert_MSXeAQAF2F4ApPbDWSq7u9",
                    "heartland_dev_public_api_key": "pkapi_cert_XS5kb6dCSDquvmWRxc"
                },
                "logos": {
                    "favicon": "https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg",
                    "video_watermark": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/50-Awards-and-Badges_48.png"
                },
                "legal": {
                    "privacy_policy": "asdf",
                    "terms_of_use": "asdf",
                    "member_agreement": "asdf",
                    "require_privacy_policy": "1",
                    "require_terms_of_use": "1",
                    "require_member_agreement": "1"
                },
                "homepage": {
                    "template": "1",
                    "multilingual": "yes",
                    "language_templates": "{\"en-GB\":\"1\",\"fr-FR\":\"1\",\"es-ES\":\"1\"}"
                },
                "dashboard": {
                    "template": "2",
                    "multilingual": "no",
                    "language_templates": "{\"en-GB\":\"2\",\"fr-FR\":\"2\",\"es-ES\":\"2\"}"
                },
                "social": {
                    "pinterest_link": "D",
                    "instagram_link": "C",
                    "facebook_link": "A",
                    "twitter_link": "B",
                    "google_plus_link": "E",
                    "youtube_link": "F",
                    "linkedin_link": "G",
                    "socialmedia_text": ""
                },
                "board": {
                    "template": null,
                    "multilingual": null,
                    "language_templates": null
                },
                "site_faq": {
                    "faq_heading": "",
                    "type": "1",
                    "faq": null,
                    "site_faq_free": ""
                },
                "features": {
                    "lms_active": null,
                    "ecommerce_active": null,
                    "media_active": null,
                    "community_active": null,
                    "mentor_active": null,
                    "goals_active": null,
                    "affirmations_active": null,
                    "image_sharing_active": null
                },
                "email": {
                    "email_custom_smtp": "1",
                    "sitename": "Ben",
                    "fromname": "TOVUTI QA",
                    "replytoname": "TOVUTI QA",
                    "mailfrom": "tovutiqa@gmail.com",
                    "smtpauth": "1",
                    "smtpuser": "tovutiqa@gmail.com",
                    "smtphost": "tovutiqa@gmail.com",
                    "replyto": "tovutiqa@gmail.com",
                    "smtpsecure": "none",
                    "smtpport": "587",
                    "smtppass": "T0vuti!@AdmIn"
                },
                "default_settings": {
                    "lms_cover_image": "",
                    "event_cover_image": "",
                    "lms_category_cover_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/courses-header-1.jpg",
                    "event_category_cover_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/events-cover.jpg",
                    "default_avatar_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/profile-user.png"
                },
                "api_settings": {
                    "zoom_integration": "yes",
                    "zoom_api_public_key": "UIuvGO2jQsy2ymeOhOU2jw",
                    "zoom_api_secret_key": "UIuvGO2jQsy2ymeOhOU2jw"
                }
            }';
            $db->updateObject('axs_brands', $brand, 'id');
            unset($brand->id);
            $db->insertObject('axs_brands', $brand);

            // // This version of brand data has all the email, payment gateway
            // // and integration settings cleared.
            $brand->data = '{
                "site_title": "Default Brand",
                "description": "",
                "site_details": {
                    "plans_for_active": null,
                    "default_font": "Arial",
                    "language_selector": "1",
                    "language_selector_options": [
                        "en-GB",
                        "fr-FR",
                        "es-ES"
                    ],
                    "tovuti_root_styles": "0",
                    "root_primary_color": "#030303",
                    "root_accent_color": "#7D7D7D",
                    "root_base_color": "#FFFFFF",
                    "homepage_type": "landing_page",
                    "login_page": "7"
                },
                "billing": {
                    "admin_emails": [],
                    "currency_id": "124",
                    "currency_code": "USD",
                    "currency_country": "United States",
                    "currency_name": "Dollars",
                    "currency_symbol": "$",
                    "auto_reprocess_declines": false,
                    "auto_reprocess_days": "1",
                    "weekend_billing": true,
                    "weekend_fallback": false,
                    "holiday_billing": false,
                    "grace_period": 30,
                    "rewards_active": false,
                    "rewards_quotient": 0,
                    "show_unpaid_alert": false,
                    "gateway_type": "heartland",
                    "converge_production_mode": "0",
                    "converge_dev_merchant_id": "",
                    "converge_dev_user_id": "",
                    "converge_dev_account_pin": "",
                    "converge_prod_merchant_id": "",
                    "converge_prod_user_id": "",
                    "converge_prod_account_pin": "",
                    "authnet_production_mode": "0",
                    "authnet_dev_api_login_id": "",
                    "authnet_dev_transaction_key": "",
                    "authnet_prod_api_login_id": "",
                    "authnet_prod_transaction_key": "",
                    "stripe_production_mode": "0",
                    "stripe_dev_api_key": "",
                    "stripe_prod_api_key": "",
                    "stripe_dev_public_api_key": "",
                    "stripe_prod_public_api_key": "",
                    "stripe_hide_zip": "0",
                    "heartland_production_mode": "0",
                    "heartland_prod_secret_api_key": "",
                    "heartland_prod_public_api_key": "",
                    "heartland_dev_secret_api_key": "",
                    "heartland_dev_public_api_key": ""
                },
                "logos": {
                    "favicon": "https://hatrabbits.com/wp-content/uploads/2017/01/random.jpg",
                    "video_watermark": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/50-Awards-and-Badges_48.png"
                },
                "legal": {
                    "privacy_policy": "asdf",
                    "terms_of_use": "asdf",
                    "member_agreement": "asdf",
                    "require_privacy_policy": "1",
                    "require_terms_of_use": "1",
                    "require_member_agreement": "1"
                },
                "homepage": {
                    "template": "1",
                    "multilingual": "yes",
                    "language_templates": "{\"en-GB\":\"1\",\"fr-FR\":\"1\",\"es-ES\":\"1\"}"
                },
                "dashboard": {
                    "template": "2",
                    "multilingual": "no",
                    "language_templates": "{\"en-GB\":\"2\",\"fr-FR\":\"2\",\"es-ES\":\"2\"}"
                },
                "social": {
                    "pinterest_link": "D",
                    "instagram_link": "C",
                    "facebook_link": "A",
                    "twitter_link": "B",
                    "google_plus_link": "E",
                    "youtube_link": "F",
                    "linkedin_link": "G",
                    "socialmedia_text": ""
                },
                "board": {
                    "template": null,
                    "multilingual": null,
                    "language_templates": null
                },
                "site_faq": {
                    "faq_heading": "",
                    "type": "1",
                    "faq": null,
                    "site_faq_free": ""
                },
                "features": {
                    "lms_active": null,
                    "ecommerce_active": null,
                    "media_active": null,
                    "community_active": null,
                    "mentor_active": null,
                    "goals_active": null,
                    "affirmations_active": null,
                    "image_sharing_active": null
                },
                "email": {
                    "email_custom_smtp": "1",
                    "sitename": "",
                    "fromname": "",
                    "replytoname": "",
                    "mailfrom": "",
                    "smtpauth": "1",
                    "smtpuser": "",
                    "smtphost": "",
                    "replyto": "",
                    "smtpsecure": "none",
                    "smtpport": "587",
                    "smtppass": ""
                },
                "default_settings": {
                    "lms_cover_image": "",
                    "event_cover_image": "",
                    "lms_category_cover_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/courses-header-1.jpg",
                    "event_category_cover_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/events-cover.jpg",
                    "default_avatar_image": "images/QrNmneWVCFMNL3Ke9PEeIdVGNTI1Nzk2MTk2NTc1/graphics/profile-user.png"
                },
                "api_settings": {
                    "zoom_integration": "yes",
                    "zoom_api_public_key": "",
                    "zoom_api_secret_key": ""
                }
            }';
            $db->insertObject('axs_brands', $brand);

            $db->setQuery('
                DROP TABLE axs_integrations;
            ');
            $db->execute();

            $db->setQuery('
                DROP TABLE axs_email_settings;
            ');
            $db->execute();

            $db->setQuery('
                DROP TABLE axs_payment_gateways;
            ');
            $db->execute();

            $db->setQuery('
                DROP TABLE axs_brand_backups;
            ');
            $db->execute();

            // Remove any new user welcome emails
            $db->setQuery("
                DELETE FROM axs_auto_alerts WHERE alert_type = 'new user welcome email';
            ");
            $db->execute();
            
        }
    }
}

JApplicationCli::getInstance('CliResetBrandMigration')->execute();