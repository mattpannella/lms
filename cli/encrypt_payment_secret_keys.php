<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';

// error_reporting(E_ALL | E_NOTICE);
// ini_set('display_errors', 1);

class CliPaymentKey extends JApplicationCli {

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled = 1 AND id >= 1279";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            print "Updating DB. dbname = " . $params->dbname . "\n";

            $db->setQuery("
                SELECT * FROM axs_payment_gateways; 
            ");
            $payment_gateways = $db->loadObjectList();

            echo json_encode($payment_gateways) . '\n';

            foreach($payment_gateways as $payment_gateways_row) {
                $updated = false;
                $params = json_decode($payment_gateways_row->params);
                if (isset($params->converge_dev_account_pin) && empty($params->converge_dev_account_pin_encrypted)) {
                    $params->converge_dev_account_pin = AxsEncryption::encrypt($params->converge_dev_account_pin, AxsKeys::getKey('lms'));
                    $params->converge_dev_account_pin_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->converge_prod_account_pin) && empty($params->converge_prod_account_pin_encrypted)) {
                    $params->converge_prod_account_pin = AxsEncryption::encrypt($params->converge_prod_account_pin, AxsKeys::getKey('lms'));
                    $params->converge_prod_account_pin_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->authnet_dev_transaction_key) && empty($params->authnet_dev_transaction_encrypted)) {
                    $params->authnet_dev_transaction_key = AxsEncryption::encrypt($params->authnet_dev_transaction_key, AxsKeys::getKey('lms'));
                    $params->authnet_dev_transaction_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->authnet_prod_transaction_key) && empty($params->authnet_prod_transaction_key_encrypted)) {
                    $params->authnet_prod_transaction_key = AxsEncryption::encrypt($params->authnet_prod_transaction_key, AxsKeys::getKey('lms'));
                    $params->authnet_prod_transaction_key_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->stripe_dev_api_key) && empty($params->stripe_dev_api_key_encrypted)) {
                    $params->stripe_dev_api_key = AxsEncryption::encrypt($params->stripe_dev_api_key, AxsKeys::getKey('lms'));
                    $params->stripe_dev_api_key_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->stripe_prod_api_key) && empty($params->stripe_prod_api_key_encrypted)) {
                    $params->stripe_prod_api_key = AxsEncryption::encrypt($params->stripe_prod_api_key, AxsKeys::getKey('lms'));
                    $params->stripe_prod_api_key_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->heartland_prod_secret_api_key) && empty($params->heartland_prod_secret_api_key_encrypted)) {
                    $params->heartland_prod_secret_api_key = AxsEncryption::encrypt($params->heartland_prod_secret_api_key, AxsKeys::getKey('lms'));
                    $params->heartland_prod_secret_api_key_encrypted = 1;
                    $updated = true;
                }
                if (isset($params->heartland_dev_secret_api_key) && empty($params->heartland_dev_secret_api_key_encrypted)) {
                    $params->heartland_dev_secret_api_key = AxsEncryption::encrypt($params->heartland_dev_secret_api_key, AxsKeys::getKey('lms'));
                    $params->heartland_dev_secret_api_key_encrypted = 1;
                    $updated = true;
                }
                if ($updated) {
                    $payment_gateways_row->params = json_encode($params);
                    $db->updateObject('axs_payment_gateways', $payment_gateways_row, 'id');
                    echo "payment gateways row id = " . $payment_gateways_row->id . "  updated\n";
                }
            }
            $db->disconnect();
        }
        
    }
}

JApplicationCli::getInstance('CliPaymentKey')->execute();
