<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

class updateCommentsTable extends JApplicationCli {

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled = 1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            print "\nUpdating DB. dbname  Table = " . $params->dbname . "\n";

            $db->setQuery("
            ALTER TABLE comments ADD COLUMN enabled TINYINT(1);
            ");

            $db->execute();

            $errormsg = $db->getErrorMsg();

            if (empty($errormsg) == false) {
                echo "your error is " . $errormsg . "\n";
            }

            $db->setQuery("
                SELECT * FROM comments;
            ");
            $user_comments = $db->loadObjectList();

            foreach($user_comments as $user_comments_row) {
                if ($user_comments_row->status == "published") {
                    $user_comments_row->enabled = 1;
                } else if ($user_comments_row->status == "unpublished") {
                    $user_comments_row->enabled = 0;
                }
                $db->updateObject('comments', $user_comments_row, 'id');
                $errormsg = $db->getErrorMsg();

                echo "\nUpdated " . $user_comments_row->id . "\n";

                if (empty($errormsg) == false) {
                    echo "your error is " . $errormsg . "\n";
                }
               
            }

            $db->execute();

            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('updateCommentsTable')->execute();