<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

class CliNotifications extends JApplicationCli {

    private $db = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND (status LIKE 'active%' OR status = 'test')";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $db = JDatabaseDriver::getInstance($options);
            $this->db = $db;

            /*
                INTERVAL
            */
            $query = $db->getQuery(true);

            $schedule_type = $db->quoteName('schedule_type');
            $type_interval = $db->quote('interval');
            $type_date = $db->quote('date');
            $type_schedule = $db->quote('schedule');
            $enabled = $db->quoteName('enabled');
            $start_date = $db->quoteName('start_date');
            $end_date = $db->quoteName('end_date');
            $send_date = $db->quoteName('send_date');

            $today = 'DATE(NOW())';

            $conditions = "
                $enabled = 1 AND
                (
                    ($schedule_type = $type_schedule OR $schedule_type = $type_interval) AND
                    (DATE($start_date) <= $today AND DATE($end_date) >= $today)
                    OR ($schedule_type = $type_date AND DATE($send_date) = $today)
                )
            ";

            $query->select('*')
                  ->from($db->quoteName('axs_notifications_emails'))
                  ->where($conditions);

            $db->setQuery($query);
            $send_list = $db->loadObjectList();

            self::sendEmails($send_list);
            $db->disconnect();
        }
    }

    private function sendEmails($list) {

        $list = self::checkIntervalDates($list);

        $db = $this->db;
        $query = $db->getQuery(true);

        foreach ($list as $result) {

            $recipients = array();

            self::getParams($result);

            //Start with creating a list from the additionally supplied emails.
            $additional_emails = $result->params->additional_emails;
            if ($additional_emails) {
                $names = $additional_emails->name;
                $emails = $additional_emails->email;

                $count = count($names);

                for ($i = 0; $i < $count; $i++) {
                    $new = new stdClass;
                    $new->name = $names[$i];
                    $new->email = $emails[$i];
                    $recipients[]= $new;
                }
            }

            $recipient_list = self::getUserList($result, "recipient");
            $report_list = self::getUserList($result, "report");

            /*

            //TODO Report binding on a value.

            $report_binding = $result->params->report_binding;

            $report_binding = 23;

            if (count($report_binding) > 0) {

                $recipient_ids = array();
                $user_ids = array();

                foreach ($recipient_list as $recip) {
                    $recipient_ids[]= $recip->user_id;
                }

                $recipient_ids = implode(",", array_unique($recipient_ids));

                foreach ($report_list as $rep) {
                    $user_ids[]= $rep->user_id;
                }

                $user_ids = implode(",", array_unique($user_ids));

                $ids = array();


                $query = "SELECT
                    fields1.user_id AS user_id,
                    fields2.user_id AS recip_id,
                    fields1.field_id AS field,
                    fields1.value AS value
                    FROM joom_community_fields_values fields1
                    JOIN joom_community_fields_values fields2
                    ON fields1.field_id = fields2.field_id
                    WHERE   FIND_IN_SET(fields1.user_id, '$user_ids')
                        AND FIND_IN_SET(fields2.user_id, '$recipient_ids')
                        AND fields1.field_id = $report_binding
                        AND fields1.value = fields2.value
                    ";

                echo $query . "\n";

                $db->setQuery($query);
                $bindings = $db->loadObjectList();

                var_dump($bindings);

                $list = array();
                foreach ($bindings as $binding) {
                    $value = strtolower($binding->value);
                    if (!isset($list[$value])) {
                        $list[$value] = new stdClass();
                        $list[$value]->users = array();
                        $list[$value]->recipients = array();
                    }

                    if (!in_array($binding->user_id, $list[$value]->users)) {
                        $list[$value]->users[]= $binding->user_id;
                    }

                    if (!in_array($binding->recip_id, $list[$value]->recipients)) {
                        $list[$value]->recipients[]= $binding->recip_id;
                    }
                }

                var_dump($list);

            } else {*/

                foreach ($recipient_list as $recipient) {

                    $new = new stdClass;
                    $new->name = $recipient->name;
                    $new->email = $recipient->email;
                    $recipients[]= $new;
                }

            //}

            $message = self::makeNotificationMessage($result, $report_list);

            $today = date("Y-m-d H:i:s");

            if (!$result->code) {
                //This is the first time this has been sent out, it does not have a matched email queue code
                $result->code = AxsEmail::getCode();
            }

            $result->last_send_date = $today;

            $db->updateObject('axs_notifications_emails', $result, 'id');

            $axs_mailer = new AxsEmail();

            $axs_mailer->content = $message->content;
            $axs_mailer->subject = $message->subject;
            $axs_mailer->attachment = json_encode($message->attachment);
            $axs_mailer->code = $result->code;
            $axs_mailer->recipients = $recipients;
            $axs_mailer->date = $today;
            $axs_mailer->brand_id = $result->brand_id;
            $axs_mailer->db = $db;

            $success = $axs_mailer->send();

            if($success) {

                echo "Successfully queued the email notification \"$result->name\"!\n";
            }
        }
    }

    private function getUserList($data, $type) {

        /*
            Build the query to pull the user lists.
            The first user list is who will be receiving the report and
            the second user list is the students who are being reported on.
        */

        $params = $data->params;

        $report_join = "";
        $report_where = "";


        $db = $this->db;

        $list = "user.id AS user_id, user.name AS name, user.email AS email";

        switch ($type) {
            case "recipient":

                $access = $data->access_type;
                $userlist = $data->userlist;
                $usergroup = $data->usergroup;

                $binding_conditions = $params->recipients_bind_conditions;
                $user_conditions = $params->recipients_conditions;

                break;
            case "report":

                $access = $params->member_access_type;
                $userlist = $params->member_userlist;
                $usergroup = $params->member_usergroup;

                $binding_conditions = $params->report_bind_conditions;
                $user_conditions = $params->report_conditions;

                switch ($params->report_type) {
                    case "student_progress":

                        $report_join = "
                            JOIN joom_splms_course_progress AS progress ON user.id = progress.user_id
                            JOIN joom_splms_courses AS course ON course.splms_course_id = progress.course_id ";

                        if ($params->report_courses_select == "selected") {
                            $report_where = " progress.archive_date IS NULL AND FIND_IN_SET(progress.course_id, {$db->quote($params->report_courses)}) ";
                        }

                        $list .= ", progress.course_id AS course_id, progress.language AS language, progress.lessons_completed_list AS lessons_completed_list, progress.lessons_total_list AS lessons_total_list, progress.progress AS progress, progress.date_started AS date_started, progress.date_last_activity AS date_last_activity, progress.date_completed AS date_completed, progress.date_due AS date_due";

                        break;
                }

                break;
        }

        $query = "SELECT $list FROM joom_users AS user $report_join";

        switch ($access) {
            case "none":

                //$connector = "WHERE";
                if ($report_where) {
                    $query .= " WHERE $report_where ";
                    $connector = "AND";
                } else {
                    $connector = "WHERE";
                }

                break;
            case "userlist":

                $query .= " WHERE FIND_IN_SET(user.id, {$db->quote($userlist)})";
                if ($report_where) {
                    $query .= " AND $report_where ";
                }

                $connector = "AND";

                break;
            case "usergroup":
                $query .= " LEFT JOIN joom_user_usergroup_map AS map
                    ON user.id = map.user_id
                    WHERE FIND_IN_SET(map.group_id, {$db->quote($usergroup)})";

                if ($report_where) {
                    $query .= " AND $report_where ";
                }

                $connector = "AND";

                break;

            default:
                return null;
        }

        if ($user_conditions) {

            $query .= " $connector ";

            if ($binding_conditions == "any") {
                $query .= "(";
            }

            switch ($binding_conditions) {
                case "any":
                    $operator = "OR";
                    break;
                case "all":
                    $operator = "AND";
                    break;
            }

            $total = count(get_object_vars($user_conditions));
            $count = 0;

            foreach ($user_conditions as $condition) {

                $query .= "
                    EXISTS (
                    SELECT 1 FROM joom_community_fields_values AS val
                    WHERE {$db->quoteName('user.id')} = {$db->quoteName('val.user_id')} AND
                    {$db->quoteName('val.field_id')} = {$db->quote($condition->field)} AND ";

                switch ($condition->operator) {
                    case "equal_to":
                        $query .= $db->quoteName('val.value') . '=' . $db->quote($condition->value);
                        break;
                    case "not_equal_to":
                        $query .= $db->quoteName('val.value') . '!=' . $db->quote($condition->value);
                        break;
                    case "contains":
                        $query .= "(LOCATE(" . $db->quote($condition->value) . ", " . $db->quoteName('val.value') . ") > 0)";
                        break;
                    case "does_not_contain":
                        $query .= "(LOCATE(" . $db->quote($condition->value) . ", " . $db->quoteName('val.value') . ") = 0)";
                        break;
                }

                if (++$count < $total) {
                    $query .= ") $operator ";
                } else {
                    $query .= ")";
                }
            }

            if ($binding_conditions == "any") {
                $query .= ")";
            }
        }

        $db->setQuery($query);
        $users = $db->loadObjectList();

        return $users;
    }

    private function getParams(&$result) {

        //Check if the params have already been converted so that they're not converted twice
        $type = gettype($result->params);

        if ($type == "string") {
            $params = json_decode($result->params);

            $params->recipients_conditions = json_decode($params->recipients_conditions);
            $params->report_conditions = json_decode($params->report_conditions);
            $params->additional_emails = json_decode($params->additional_emails);
            $params->report_binding = json_decode($params->report_binding);

            $result->params = $params;
        }
    }

    private function checkIntervalDates($list) {

        $sendList = array();

        foreach ($list as $entry) {

            self::getParams($entry);
            $params = $entry->params;
            $lastSentToday = false;

            $now = new DateTime(date("Y-m-d H:i:s"));

            $dayLastSent = null;

            if(!is_null($entry->last_send_date)) {

                try {

                    $dayLastSent = new DateTime(date("Y-m-d H:i:s", strtotime($entry->last_send_date)));

                    // Does the date of the last time this notification was sent match today's day of the month?
                    $lastSentToday = ($dayLastSent->format('d') == $now->format('d'));
                } catch(Exception $ex) {

                    die('Last notification send date is invalid - Exception Thrown: ' . $ex->getMessage());
                }
            }

            $send_today = false;

            if ($entry->schedule_type != 'schedule') {

                switch ($entry->schedule_type) {
                    case 'interval':

                        // Don't need to do this check if there is no previous send date
                        if(!is_null($dayLastSent)) {

                            $number = $params->interval_number;
                            $unit = $params->interval_type;

                            //Get the difference between now and the last send date.
                            $diff = date_diff($dayLastSent, $now);

                            switch ($unit) {
                                case "days":

                                    if ($diff->days >= $number) {
                                        $send_today = true;
                                    }
                                break;
                                case "weeks":

                                    $weeks = $diff->days / 7;

                                    if ($weeks >= $number) {
                                        $send_today = true;
                                    }
                                break;
                                case "months":

                                    $months = $diff->y * 12 + $diff->m;

                                    if ($months >= $number) {
                                        $send_today = true;
                                    }
                                break;
                                case "years":

                                    if ($diff->y >= $number) {
                                        $send_today = true;
                                    }
                                break;
                            }
                        } else {

                            $send_today = true;
                        }
                    break;

                    // This case has already been covered by the SQL query, and the time check is done later.
                    // The notification already matches the send date, so we just need to check the time of day.
                    case 'date':

                        $send_today = true;
                    break;
                }
            } else {

                // If there is a previous send date and the schedule matches the current day or month, send the
                // notification
                $dotw = $dotm = $moty = true;

                if ($params->schedule_use_day_of_the_week) {

                    $current_day_of_week = $now->format('w');
                    $dotw = in_array($current_day_of_week, $params->schedule_day_of_the_week);
                }

                if ($params->schedule_use_day_of_the_month) {
                    $current_day_of_month = $now->format('d');
                    $dotm = in_array($current_day_of_month, $params->schedule_day_of_the_month);
                }

                if ($params->schedule_use_month_of_the_year) {
                    $current_month_of_the_year = $now->format('n');
                    $moty = in_array($current_month_of_the_year, $params->schedule_month_of_the_year);
                }

                $send_today = $dotw && $dotm && $moty;
            }

            // If we're going to send the notification today, make sure that the send_time entry is respected.

            /* Logic:
             * If the send time is before the current time, and the notification hasn't already been sent today, send.
             * If the send time is after the current time, or the notification has been sent today, do not send.
             */
            if ($send_today) {

                // Get the send time for today
                $send_time = new DateTime($now->format('Y-m-d') . " " . $entry->send_time);

                $after = $send_time->getTimestamp() <= $now->getTimestamp();

                if ($after && !$lastSentToday) {

                    $sendList[] = $entry;
                }
            }
        }

        return $sendList;
    }

    private function makeNotificationMessage($data, $report_list) {

        $params = $data->params;

        $email_message = $data->message;
        $email_subject = $data->subject;

        if ($params->include_date) {
            $email_subject .= " " . date("Y-m-d");
        }

        $course_data = self::getCourseData($params, $report_list);

        $course_titles = array();
        $db = $this->db;
        $query = $db->getQuery(true);

        $attachments = array();

        /*$filename = "notification_email" . strtotime('now') . ".csv";
        $file = fopen($filename, "w");*/

        $headers = array(
            "Course",
            "ID",
            "Name",
            "Email",
            "Progress",
            "Date of Last Activity",
            "Date Started",
            "Date Completed",
            "Date Due"
        );

        foreach ($course_data as $key => $data) {

            $newfile = new stdClass();
            $filename = "notification_email_" . $key . "_" . strtotime('now') . ".csv";
            $newfile->name = $key . "_" . date("Y_m_d") . ".csv";
            $file = fopen($filename, "w");

            fputcsv($file, $headers);

            foreach ($data as $course_id => $datum) {

                if (!isset($course_titles[$course_id])) {
                    $query->clear();
                    $query
                        ->select('title')
                        ->from($db->quoteName('#__splms_courses'))
                        ->where($db->quoteName('splms_course_id') . '=' . $db->quote($course_id));

                    $db->setQuery($query);
                    $course_titles[$course_id] = $db->loadResult();
                }

                foreach ($datum as $student_id => $student) {
                    if (
                        gettype($student) == "string" ||    //The data is not an object (either NULL or a "lock" string)
                        empty((array)$student)  //The student data was never set
                    ) {
                        continue;
                    }

                    if ($student->last_activity) {
                        $last_activity = date("Y-m-d", strtotime($student->last_activity));
                    } else {
                        $last_activity = null;
                    }

                    if ($student->started) {
                        $started = date("Y-m-d", strtotime($student->started));
                    } else {
                        $started = null;
                    }

                    if ($student->completed) {
                        $completed = date("Y-m-d", strtotime($student->completed));
                    } else {
                        $completed = null;
                    }

                    if ($student->due) {
                        $due = date("Y-m-d", strtotime($student->due));
                    } else {
                        $due = null;
                    }

                    $row = array(
                        $course_titles[$course_id],
                        $student_id,
                        $student->name,
                        $student->email,
                        (int)$student->progress . "%",
                        $last_activity,
                        $started,
                        $completed,
                        $due
                    );

                    fputcsv($file, $row);
                }
            }

            fclose($file);

            $newfile->contents = file_get_contents($filename);

            unlink($filename);

            $attachments[]= $newfile;
        }

        $return = new stdClass;
        //$return->content = "Report " . date("Y-m-d H:i:s");
        $return->content = $email_message;
        $return->attachment = $attachments;
        //$return->subject = "Course Notification " . date("Y-m-d H:i:s");
        $return->subject = $email_subject;

        return $return;
    }

    private function getCourseData($params, $report_list) {

        $status_types = explode(",", $params->report_courses_status_types);

        //progress          The percentage of completion of the student.
        //past_due          Students who past the due date of the course
        //completed         Students who have completed the course
        //incomplete        Students who have not completed the course
        //no_start          Students who have not even started the course.

        $report = new stdClass();

        $progress = in_array("progress", $status_types);
        $past_due = in_array("past_due", $status_types);
        $completed = in_array("completed", $status_types);
        $incomplete = in_array("incomplete", $status_types);
        $no_start = in_array("no_start", $status_types);

        $current_date = date("Y-m-d H:i:s");

        foreach ($report_list as $student) {

            if ($progress) {
                if (!isset($report->progress)) {
                    $report->progress = array();
                }

                if (!isset($report->progress[$student->course_id])) {
                    $report->progress[$student->course_id] = array();
                }

                if (!isset($report->progress[$student->course_id][$student->user_id])) {
                    $report->progress[$student->course_id][$student->user_id] = new stdClass();
                }

                $temp = $report->progress[$student->course_id][$student->user_id];

                //If progress is already set, and it's greater than the current progress, than don't set it.
                //ie. They may have 0 progress in French, but 75% progress in English.

                $update = false;
                $clear = false;

                if (!isset($temp->progress)) {
                    //No progress has been set. This is the first occurence (as there may be multiple for different languages)
                    $update = true;
                } else {
                    //date started is set, but it's null.
                    if (isset($temp->date_started) && !$temp->date_started) {
                        $update = true;
                    } else {
                        //The current progress is greater than the one recorded.
                        if ($student->progress > $temp->progress) {
                            $update = true;
                        }
                    }
                }

                if ($update) {
                    $temp->name = $student->name;
                    $temp->email = $student->email;
                    $temp->language = $student->language;
                    $temp->progress = $student->progress;
                    $temp->started = $student->date_started;
                    $temp->completed = $student->date_completed;
                    $temp->last_activity = $student->date_last_activity;
                    $temp->due = $student->date_due;
                }

                if ($clear) {
                    //This has been completed, don't let it get remarked as incomplete.
                    $report->progress[$student->course_id][$student->user_id] = "lock";
                }
            }

            if ($past_due) {
                if (!isset($report->past_due)) {
                    $report->past_due = array();
                }

                if (!isset($report->past_due[$student->course_id])) {
                    $report->past_due[$student->course_id] = array();
                }

                if (!isset($report->past_due[$student->course_id][$student->user_id])) {
                    $report->past_due[$student->course_id][$student->user_id] = new stdClass();
                }

                $temp = $report->past_due[$student->course_id][$student->user_id];

                $update = false;
                $clear = false;

                if ($student->date_completed) {
                    //The student has completed this course.  Remove info that says they haven't completed it, if it exists.
                    $clear = true;
                } else if ($temp != "lock") {
                    //There is a due date, and it's prior to the current date.
                    if ($student->date_due && $student->date_due < $current_date) {

                        if (isset($temp->progress) && $temp->progress) {
                            if ($student->progress > $temp->progress) {
                                $update = true;
                            }
                        } else {
                            $update = true;
                        }

                    }
                }

                if ($update) {
                    $temp->id = $student->user_id;
                    $temp->name = $student->name;
                    $temp->email = $student->email;
                    $temp->language = $student->language;
                    $temp->progress = $student->progress;
                    $temp->started = $student->date_started;
                    $temp->completed = $student->date_completed;
                    $temp->last_activity = $student->date_last_activity;
                    $temp->due = $student->date_due;
                }

                if ($clear) {
                    //This has been completed, don't let it get remarked as incomplete.
                    $report->past_due[$student->course_id][$student->user_id] = "lock";
                }
            }

            if ($completed) {
                if (!isset($report->completed)) {
                    $report->completed = array();
                }

                if (!isset($report->completed[$student->course_id])) {
                    $report->completed[$student->course_id] = array();
                }

                if (!isset($report->completed[$student->course_id][$student->user_id])) {
                    $report->completed[$student->course_id][$student->user_id] = new stdClass();
                }

                $temp = $report->completed[$student->course_id][$student->user_id];

                $update = false;
                $clear = false;

                if ($student->date_completed) {
                    //The student has completed this course.
                    $update = true;
                }

                if ($update) {
                    $temp->id = $student->user_id;
                    $temp->name = $student->name;
                    $temp->email = $student->email;
                    $temp->language = $student->language;
                    $temp->progress = $student->progress;
                    $temp->started = $student->date_started;
                    $temp->completed = $student->date_completed;
                    $temp->last_activity = $student->date_last_activity;
                    $temp->due = $student->date_due;
                }

                if ($clear) {
                    //This has been completed, don't let it get remarked as incomplete.
                    $report->completed[$student->course_id][$student->user_id] = "lock";
                }
            }

            if ($incomplete) {
                if (!isset($report->incomplete)) {
                    $report->incomplete = array();
                }

                if (!isset($report->incomplete[$student->course_id])) {
                    $report->incomplete[$student->course_id] = array();
                }

                if (!isset($report->incomplete[$student->course_id][$student->user_id])) {
                    $report->incomplete[$student->course_id][$student->user_id] = new stdClass();
                }

                $temp = $report->incomplete[$student->course_id][$student->user_id];

                $update = false;
                $clear = false;

                if ($student->date_completed) {
                    //The student has completed this course.  It's not incomplete
                    $clear = true;
                } else if ($temp != "lock") {
                    if (isset($temp->progress)) {
                        if ($student->progress > $temp->progress) {
                            $update = true;
                        }
                    } else {
                        $update = true;
                    }
                }

                if ($update) {
                    $temp->id = $student->user_id;
                    $temp->name = $student->name;
                    $temp->email = $student->email;
                    $temp->language = $student->language;
                    $temp->progress = $student->progress;
                    $temp->started = $student->date_started;
                    $temp->completed = $student->date_completed;
                    $temp->last_activity = $student->date_last_activity;
                    $temp->due = $student->date_due;
                }

                if ($clear) {
                    //This has been completed, don't let it get remarked as incomplete.
                    $report->incomplete[$student->course_id][$student->user_id] = "lock";
                }
            }

            if ($no_start) {
                if (!isset($report->no_start)) {
                    $report->no_start = array();
                }

                if (!isset($report->no_start[$student->course_id])) {
                    $report->no_start[$student->course_id] = array();
                }

                if (!isset($report->no_start[$student->course_id][$student->user_id])) {
                    $report->no_start[$student->course_id][$student->user_id] = new stdClass();
                }

                $temp = $report->no_start[$student->course_id][$student->user_id];

                $update = false;
                $clear = false;

                if ($student->date_started) {
                    //The student has started this course.
                    $clear = true;
                } else if ($temp != "lock") {
                    $update = true;
                }

                if ($update) {
                    $temp->id = $student->user_id;
                    $temp->name = $student->name;
                    $temp->email = $student->email;
                    $temp->language = $student->language;
                    $temp->progress = $student->progress;
                    $temp->started = $student->date_started;
                    $temp->completed = $student->date_completed;
                    $temp->last_activity = $student->date_last_activity;
                    $temp->due = $student->date_due;
                }

                if ($clear) {
                    //This has been completed, don't let it get remarked as incomplete.
                    $report->no_start[$student->course_id][$student->user_id] = "lock";
                }
            }
        }

        return $report;

    }
}

JApplicationCli::getInstance('CliNotifications')->execute();