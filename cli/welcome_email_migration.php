<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/alerts.php';
require_once JPATH_LIBRARIES . '/axslibs/sendgrid.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliWelcomeEmailMigration extends JApplicationCli {

    public function doExecute() {

        $creds = dbCreds::getCreds();

        $db = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND (status LIKE 'active%' OR status = 'test')";
        $db_result = $db->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            // Migrate the welcome email to a notification
            $userSettings = null;
            $query = $db->getQuery(true);

            $query->select('*')
                  ->from('axs_user_settings')
                  ->limit(1);

            $db->setQuery($query);

            $result = $db->loadObject();

            if(!is_null($result)) {

                $userSettings = json_decode($result->params);

                $emailSubject = $userSettings->subject;
                $emailBody = $userSettings->body;

                $newWelcomeEmailParams = new stdClass();

                $newWelcomeEmailParams->welcome_email_subject = $emailSubject;
                $newWelcomeEmailParams->welcome_email_body = $emailBody;

                $params = json_encode($newWelcomeEmailParams);

                $newWelcomeEmailAlert = new stdClass();

                $newWelcomeEmailAlert->params = $params;
                $newWelcomeEmailAlert->alert_type = 'new user welcome email';
                $newWelcomeEmailAlert->name = 'Global New User Welcome Email';
                $newWelcomeEmailAlert->description = 'New User Welcome Email Notification';
                $newWelcomeEmailAlert->brand_id = 0;
                $newWelcomeEmailAlert->enabled = 1;

                // We're going to assume that no notifications of this type exist and just insert it
                $success = $db->insertObject('axs_auto_alerts', $newWelcomeEmailAlert);

                print('\n' . ($success ? 'true' : 'false') . '\n');
            }

            $db->disconnect();
            sleep(0.5);
        }
    }
}

JApplicationCli::getInstance('CliWelcomeEmailMigration')->execute();