#!/usr/bin/php
<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

/**
 * This migration script was built for any fields which have been changed from
 * repeatable (modal) fields, to repeatable-subform fields. This change
 * is in response to bootstrap 5 disallowing modals opening up within modals.
 * All repeatable fields with media fields or module select fields in them were converted.
 * 
 * List of the fields to be translated:
 * - axs_homepage_templates (brand_homepages): data->slides, data->testimonials->list, zones->zone_[]->zone_modules, zones->zone_[]->zone_columns
 * - axs_dashboard_templates (brand_dashboard): data->modules
 * - joom_splms_lessons (lessons): files/params->files, teacher_materials, required_quizzes
 * - axs_contracts (contracts): files
 * 
 * All fields are translated using the same pattern. See the description for
 * the translate() function for more details.
 * 
 * This script has 1 argument: 'execute'
 * If 'execute' isn't passed to the script, nothing in the database will be updated
 * 
 */

$execute = false;
if (count($argv) > 1 && $argv[1] == 'execute') {
    $execute = true;
}

do_migration($execute);

function do_migration($execute) {
    
    $tables_columns = [
        'axs_homepage_templates' => [
            'data'
        ],
        'joom_splms_lessons' => [
            'params',
            'teacher_materials',
            'required_quizzes'
        ],
        'axs_dashboard_templates' => [
            'data'
        ],
        'axs_contracts' => [
            'files'
        ]
    ];
    
    foreach ($tables_columns as $table_name => &$columns) {
        foreach ($columns as $column_name) {
            // because just naming the pkey column 'id' would be too simple
            $id_column_name = 'id';
            if ($table_name == 'joom_splms_lessons') {
                $id_column_name = 'splms_lesson_id';
            }
            $rows = get_table_column($table_name, $column_name, $id_column_name);

            if (count($rows) > 0) {
                foreach($rows as &$row) {
                    // each column value is a json object, which depending
                    // on the table/column pair we are modifying, may have a unique
                    // set of fields we need to translate.
                    $column_value = json_decode($row->{$column_name});
                    $column_value_original = json_encode($column_value);
                    if ($table_name == 'axs_homepage_templates' && $column_name == 'data') 
                    {
                        // data->slides
                        $column_value->header->slides = translate($column_value->header->slides, "slides");

                        // data->testimonials
                        $column_value->testimonials->list = translate($column_value->testimonials->list, "testimonials");

                        foreach($column_value->zones as $zone_key => &$zone_obj) {
                            $zone_obj->modules = translate($zone_obj->modules, "zone_modules");
                            $zone_obj->columns = translate($zone_obj->columns, "zone_columns");
                        }
                    } 

                    else if ($table_name == 'joom_splms_lessons' && $column_name == 'params') {
                        $column_value->files = json_encode(translate($column_value->files, "files"));
                    }

                    else if ($table_name == 'axs_dashboard_templates' && $column_name == 'data') {
                        // there are some config options that apply to all modules which
                        // are included under the "module" object in the legacy version of the form data.
                        // we need to move these config options to a new object assign the modules to 
                        // their own field called dashboard_modules in order for the view model to work properly
                        $modules = new stdClass();
                        foreach ($column_value->modules as $module_param_key => $module_param_value) {
                            // list is the key where the legacy modules data is locateed
                            if ($module_param_key != "list") {
                                $modules->{$module_param_key} = $module_param_value;
                            }
                        }
                        $column_value->modules = $modules;

                        if (property_exists($column_value->modules, 'list')) {
                            $column_value->modules = translate($column_value->modules->list, "modules");
                        }
                    }
        
                    else {
                        // the legacy data is the column value itself, not a field of the 
                        // json data in the column
                        $column_value = translate($column_value, $column_name, $column_name);
                    }
                    
                    $row->{$column_name} = json_encode($column_value);
                    
                    // determine if the column value was updated by comparing the md5
                    // hashes of the two column values
                    $value_updated = md5($column_value_original) != md5(json_encode($column_value));
                    var_dump("md5 hashes: ");
                    var_dump(md5($column_value_original));
                    var_dump($column_value_original);
                    var_dump(md5(json_encode($column_value)));
                    var_dump($column_value);

                    // if we are executing the migration, and the column_value has been changed
                    if ($execute && $value_updated) {
                        // send the modified data to the database
                        update_table_column(
                            $table_name, 
                            $column_name, 
                            $id_column_name, 
                            $row->{$id_column_name}, 
                            json_encode($column_value)
                        );
                    }
                }

                $columns[$column_name] = $rows;
            }
        }
    }

    echo "Final values for all columns : \n";
    var_dump($tables_columns);
}


function get_table_column($table_name, $column_name, $id_column_name) {
    
    var_dump('select');
    var_dump($column_name, $id_column_name);
    var_dump('from');
    var_dump($table_name);
    
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query
        ->select(array($column_name, $id_column_name))
        ->from($db->quoteName($table_name));
    $db->setQuery($query);

    $results = $db->loadObjectList();
    var_dump('results : ');
    var_dump($results);

    return $results;
}

function update_table_column($table_name, $column_name, $id_column_name, $row_id, $value) {
    
    var_dump("update");
    var_dump($table_name);
    var_dump("set");
    var_dump($column_name);
    var_dump("=");
    var_dump($value);
    var_dump("where");
    var_dump($id_column_name);
    var_dump("=");
    var_dump($row_id);
    
    $db = JFactory::getDbo();

    $query = $db->getQuery(true);
    
    // Fields to update.
    $fields = array(
        $db->quoteName($column_name) . ' = ' . $db->quote($value),
    );

    $conditions = array(
        $db->quoteName($id_column_name) . ' = ' . $row_id, 
    );
    
    $query->update($db->quoteName($table_name))->set($fields)->where($conditions);;
    
    $db->setQuery($query);
    
    $db->execute();
}

function get_table_column_id($table_name, $column_name, $id_column_name, $id_value) {
    
    var_dump('select');
    var_dump($column_name, $id_column_name);
    var_dump('from');
    var_dump($table_name);
    var_dump('where');
    var_dump($id_column_name . ' = ' . $id_value);
    
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query
        ->select(array($column_name, $id_column_name))
        ->from($db->quoteName($table_name))
        ->where($id_column_name . ' = ' . $id_value);
    $db->setQuery($query);

    $results = $db->loadObjectList();

    var_dump('results : ');
    var_dump($results);

    return $results[0];
}

/**
 *  * 
 * in a repeatable field data is arranged like so:
 * { "parent-fieldname": 
 *     { 
 *         "field1": ['value1', 'value2'], 
 *         "field2": ['value1', 'value2'] 
 *     } 
 * }
 * 
 * this data shape is translated to:
 * { 
 *    "parent-fieldname0": { 
 *        "field1": "value1", 
 *        "field2": "value2" 
 *    }, 
 *    "parent-fieldname1": { 
 *        "field1": "value1", 
 *        "field2": "value2" 
 *    }, 
 * }
 *
 * @param   string  $parent_fieldname  The fieldname of the field we are translating data for. This name needs to be used 
 *                                   to encode the data we are translating. It doesn't neccisarily need to be the same as the
 *                                   name of the column or field we are getting the data from.
 * 
 * @param   string  $to_fieldname    The fieldname that we will use in the translated data.
 *
 * @param   object|string  $data     Field data we are checking and possibly translating
 *
 * @return  array                    Translated data. If data doesn't need to be translated we will simply return the value of the $data param
 *
 * 
 */
function translate($data, $parent_fieldname) {

    var_dump('attempting translation...');
    var_dump('$parent_fieldname : ');
    var_dump($parent_fieldname);
    
    if (gettype($data) == "string") {
        $data = json_decode($data);
    }
    
    var_dump('$data: ');
    var_dump($data);

    var_dump('checking for legacy data type/shape:');
    var_dump('gettype($data) == object');
    var_dump(gettype($data) == 'object');

    var_dump('!property_exists($data, $parent_fieldname . 0)');
    var_dump(!property_exists((object)$data, $parent_fieldname . '0'));

    // if there are rows in the data, and they have legacy shape
    if (gettype($data) == 'object' && !property_exists($data, $parent_fieldname . '0')) {
        // translate the data to a shape that will work with the subform
        $new = new stdClass();
        foreach ($data as $fieldname => $values) {
            if (gettype($values) != "array") {
                // this is a secondary check that we are indeed translating
                // legacy data. if $values isn't an array, this whole party will
                // come crashing down (fatal php error) and that's no good.
                // this is typically a result of running the migration on a 
                // column which has already been migrated at a previous point.
                return $data;
            }
            var_dump('executing translation...');
            for($i = 0; $i < count($values); $i++) {
                if (!property_exists($new, $parent_fieldname . $i)) {
                    $new->{$parent_fieldname . $i} = array();
                }
                $new->{$parent_fieldname . $i}[$fieldname] = $values[$i];
            }
        }
        return $new;
    } else {
        return $data;
    }
}

?>
