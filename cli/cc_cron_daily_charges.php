<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/16/15
 * Time: 4:28 PM
 */

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
    die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_CONFIGURATION . '/configuration.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_LIBRARIES . '/axslibs/payment.php';
require_once JPATH_LIBRARIES . '/axslibs/extra.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';

require_once(JPATH_ADMINISTRATOR . "/components/com_award/helpers/helper.php");

class DailyCronCharges extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */
    public function doExecute()
    {
    	$timer_start = microtime(true);

        //grab the dispatcher and load plugins
        $dispatcher = JEventDispatcher::getInstance();
        JPluginHelper::importPlugin('axs');

        $total = 0;
        $aborted = 0;
        $success = 0;
        $error = 0;

        //first grab which plans belong to which brand.
        $brandPlans = array();
        $planDescs = array();
	    $planActiveUsergroups = array();

        $db = JFactory::getDbo();
        $query = "SELECT a.id, a.title, a.active_usergroup, b.brand_id FROM axs_pay_subscription_plans a LEFT JOIN axs_pay_subscription_categories b ON a.cat_id=b.id;";
        $db->setQuery($query);
        $plans = $db->loadObjectList();
        foreach($plans as $p) {
            if(array_key_exists($p->brand_id, $brandPlans)) {
                $brandPlans[$p->brand_id][] = $p->id; //if existing brand, append to plan_id array.
            } else {
                $brandPlans[$p->brand_id] = array($p->id); //if new brand, create an array of plan_id belonging to it.
            }
            
            $planDescs[$p->id] = $p->title;
	        $planActiveUsergroups[$p->id] = $p->active_usergroup;
        }

        //now loop through the brands and do the payments stuff.
        foreach($brandPlans as $brand_id => $planArray) {
            //first grab the brand
            $brand = AxsBrands::getBrandById($brand_id);

            //init the libs with the correct brand's configs
            AxsPayment::init($dispatcher, $brand);
            AxsExtra::init($dispatcher, $brand);

            //calculate which days to process for
	        $runCharges = true;
            $runDays = 'DATE(a.end)=CURDATE()'; // all of today
            //first check the day of the week, days 1, 5, 6, 7 are the ones we are concerned with..
            $day = date('N');
            if(($day < 2 || $day > 4) && !$brand->billing->weekend_billing) { //if it's a critical day and no weekend billing...
                if($day == 5 && $brand->billing->weekend_fallback) { // today is friday and fallback is friday
                    //run today and the next two days...
                    $runDays = 'DATE(a.end)>=CURDATE() AND DATE(a.end)<(CURDATE()+INTERVAL 3 DAY)';
                } elseif($day == 1 && !$brand->billing->weekend_fallback) { //today is monday and fallback is monday
                    //run today and the last two days...
                    $runDays = 'DATE(a.end)>(CURDATE()-INTERVAL 3 DAY) AND DATE(a.end)<=CURDATE()';
                } elseif($day == 1 || $day == 5) { //today is monday or friday
                    //run today
                } else {
                    //today is saturday or sunday.. skip doing anything.
                    echo "Today is saturday or sunday, do nothing.\n";
	                $runCharges = false;
                }
            }

            //calculate which plans to run for
            $plans = implode(', ', $planArray);
            $plans = "plan_id IN ($plans)";

		        if($runCharges) {
		        	//######################### THIS WILL GO AND CHARGE EVERYONE WHO HAS A MEMBERSHIP ################################
			        $query = "SELECT a.*, b.status as sub_status, b.user_id, b.cancel, b.plan_id, b.original_amount, b.period_length, b.card_id, b.points 
											FROM axs_pay_subscription_periods a 
											LEFT JOIN axs_pay_subscriptions b ON a.sub_id=b.id 
											WHERE $runDays AND b.$plans AND b.status!='INAC';";
			        $db->setQuery($query);
			        $subs = $db->loadObjectList();
			        foreach($subs as $sp) {
				        $total++;

				        //make sure they don't have a sub period already for this time.
				        $query = "SELECT * FROM axs_pay_subscription_periods WHERE sub_id=$sp->sub_id AND id!=$sp->id AND `end`>'$sp->end';";
				        $db->setQuery($query);
				        $period = $db->loadObject();
				        if(is_object($period)) {
					        $aborted++;
					        continue;
				        }

				        //lets see when the new sub period needs to end.
				        $end = null;
				        $length = substr($sp->period_length, 0, -1);
				        $term = substr($sp->period_length, -1);
				        switch($term) {
					        case 'M':
						        $end = new DateTime($sp->end);
						        for($i=0; $i<$length; $i++) {
							        $end = AxsExtra::getDayNextMonth($end);
						        }
						        $end = $end->format("Y-m-d H:i:s");
						        break;
					        case 'D':
						        $end = (new DateTime($sp->end))->modify("+$length day")->format("Y-m-d H:i:s");
						        break;
					        case 'Y':
						        $end = (new DateTime($sp->end))->modify("+$length year")->format("Y-m-d H:i:s");
				        }

				        //now deal with the original sub-period.  If it's prorated unpaid then we need to calc how much to tack on to next period's charge
				        $amount = 0;
				        if($sp->status == 'PRO1') {
					        $per_day_amount = 0;
					        switch($term) {
						        case 'M':
							        $days_per_month = 30.44;  //average based on 365.2425 days per year.
							        $per_day_amount = $sp->original_amount/($days_per_month*$length);
							        break;
						        case 'D':
							        $per_day_amount = $sp->original_amount/$length;
							        break;
						        case 'Y':
							        $days_per_year = 365.2425;
							        $per_day_amount = $sp->original_amount/($days_per_year*$length);
					        }

					        $days_to_charge = (new DateTime($sp->start))->diff((new DateTime($sp->end)))->format("%a");
					        $amount = $days_to_charge*$per_day_amount;
				        }

				        //add the amount for next period's charge
				        $amount += $sp->original_amount;

				        //this is the rewards adjustments.
				        $rewards = $dispatcher->trigger("onCronCalcPoints", array(&$brand, &$sp->user_id, &$amount, &$sp->points));
				        $rewards = $rewards[0];
				        if($rewards) {
					        $amount = $rewards->total;
				        }

				        //now that we have the actual total, if it's greater than 0, make the charge.
				        if($amount > 0) {
					        $card = 0;
					        if ($sp->card_id) {

					        	$cardParams = AxsPayment::getCardParams();
						        $cardParams->id = $sp->card_id;
						        $card = AxsPayment::newCard($cardParams);
						        $card->getCardWithId();
						        //if they don't own the card and they aren't part of the family, try to just grab the first card available to them.
						        if($card->user_id != $sp->user_id && !in_array($sp->user_id, $card->family_access)) {
							        $cards = AxsPayment::getUserAllCards($sp->user_id);
							        $card = $cards[0];
						        }
					        } else {
						        //if they have no card set to use for this sub, try to just grab the first card available to them.
						        $cards = AxsPayment::getUserAllCards($sp->user_id);
						        $card = $cards[0];
					        }

					        $trxnParams = AxsPayment::getTransactionParams();

					        $trxnParams->user_id = $sp->user_id;
							$trxnParams->type = AxsPayment::$trxn_types['subscription'];
							$trxnParams->amount = $amount;
							$trxnParams->date = date("Y-m-d H:i:s");
							$trxnParams->initiator = AxsPayment::$initiators['system'];
							$trxnParams->description = $planDescs[$sp->plan_id];

					        //if there is still no charge-able card, then we need to make a failed trxn and sub period and continue.
					        if (!$card) {
					        	$subParams = AxsPayment::getSubscriptionParams();
					        	$subParams->id = $sp->sub_id;
					        	$subParams->status = "GRC";
						        $sub = AxsPayment::newSubscription($subParams);
						        $sub->save();

						        $subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

						        $subPeriodParams->sub_id = $sp->sub_id;
						        $subPeriodParams->start = $sp->end;
						        $subPeriodParams->end = $end;
						        $subPeriodParams->status = "NONE";
						        $subPeriodParams->user_id = $sp->user_id;

						        $subPeriod = AxsPayment::newSubscriptionPeriod($subPeriodParams);
						        $subPeriod->save();

						        $trxnParams->type_id = $subPeriod->id;
						        $trxnParams->status = "FAIL";						        
						        $trxnParams->code = -1;
						        $trxnParams->message = 'No cards on file that can be used for this transaction, so never sent to converge.';

						        $trxn = AxsPayment::newTransaction($trxnParams);
						        $trxn->save();
						        $error++;
						        continue;
					        }

					        $subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

					        $subPeriodParams->sub_id = $sp->sub_id;
					        $subPeriodParams->start = $sp->end;
					        $subPeriodParams->end = $end;
					        $subPeriodParams->user_id = $sp->user_id;

					        //lets make the charge if we got to here
					        $subPeriod = AxsPayment::newSubscriptionPeriod($subPeriodParams);
					        $subParams = AxsPayment::getSubscriptionParams();

					        $subParams->id = $sp->sub_id;
					        
					        $trxnParams->card = $card;

					        $trxn = AxsPayment::newTransaction($trxnParams);
					        $trxn->charge(false);
					        if($trxn->status == "SUC") {
						        $subPeriod->status = "PAID";
						        if($sp->status == 'PRO1') {

						        	$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

							        $subPeriodParams->id = $sp->id;
							        $subPeriodParams->status = "PRO2";

							        $old_sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);
							        $old_sp->save();
						        }
						        $success++;
						        $subParams->status = "ACT";
						        $sub = AxsPayment::newSubscription($subParams);
						        $sub->save();
						        $dispatcher->trigger("onCronSuccess", array(&$brand, &$sp->user_id, &$amount, &$rewards->pointsToUse, &$planDescs[$sp->plan_id]));
					        } else {
						        $subPeriod->status = "NONE";
						        $error++;
						        $subParams->status = "GRC";
						        $sub = AxsPayment::newSubscription($subParams);
						        $sub->save();
					        }
					        $subPeriod->save();
					        $trxn->type_id = $subPeriod->id;
					        $trxn->save();
				        } else {
					        //the total is 0, just make a sub-period with status PAID bc they paid with points or something
					        $subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

					        $subPeriodParams->sub_id = $sp->sub_id;
					        $subPeriodParams->start = $sp->end;
					        $subPeriodParams->end = $end;
					        $subPeriodParams->status = "PAID";
					        $subPeriodParams->user_id = $sp->user_id;

					        $subPeriod = AxsPayment::newSubscriptionPeriod($subPeriodParams);
					        $subPeriod->save();
					        //update the old sub period status
					        if($sp->status == 'PRO1') {
					        	$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

						        $subPeriodParams->id = $sp->id;
						        $subPeriodParams->status = "PRO2";

						        $old_sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);
						        $old_sp->save();
					        }
					        $success++;
					        $subParams = AxsPayment::getSubscriptionParams();
					        $subParams->id = $sp->sub_id;
					        $subParams->status = "ACT";
					        $sub = AxsPayment::newSubscription($subParams);
					        $sub->save();
					        $dispatcher->trigger("onCronSuccess", array(&$brand, &$sp->user_id, &$amount, &$rewards->pointsToUse, &$planDescs[$sp->plan_id]));
				        }
			        }

			        //######################### THIS WILL GO AND AUTOMATICALLY REPROCESS #####################################
			        if($brand->billing->auto_reprocess_declines) {
			        	$days = $brand->billing->auto_reprocess_days;
				        $grace = $brand->billing->grace_period;

				        $query = "SELECT b.*, a.id as sub_id, a.status as sub_status, a.user_id, a.cancel, a.plan_id, a.original_amount, a.period_length, a.card_id, a.points 
													FROM `axs_pay_subscriptions` a
													JOIN axs_pay_subscription_periods b ON a.id=b.sub_id 
													WHERE a.$plans AND a.status='GRC' AND b.status='NONE'
													AND DATE(b.start)>=(CURDATE()-INTERVAL $grace DAY) AND DATEDIFF(CURDATE(), b.start) !=0 AND DATEDIFF(CURDATE(), b.start)%$days=0;";
				        $db->setQuery($query);
				        $subs = $db->loadObjectList();
				        foreach($subs as $s) {
					        $total++;

					        //make sure that a charge hasn't already occurred today
					        $type = AxsPayment::$trxn_types['subscription'];
					        $query = "SELECT * FROM axs_pay_transactions WHERE type=$type AND type_id=$s->id AND DATE(`date`)=CURDATE();";
					        $db->setQuery($query);
					        $period = $db->loadObject();
					        if(is_object($period)) {
						        $aborted++;
						        continue;
					        }

					        $subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

					        $subPeriodParams->id = $s->id;
					        $subPeriodParams->sub_id = $s->sub_id;
					        $subPeriodParams->start = $s->start;
					        $subPeriodParams->end = $s->end;
					        $subPeriodParams->status = $s->status;
					        $subPeriodParams->user_id = $s->user_id;

					        $sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);

					        $subParams = AxsPayment::getSubscriptionParams();
					        
					        $subParams->id = $s->sub_id;
					        $subParams->user_id = $s->user_id;
					        
					        $subParams->cancel = $s->cancel;
					        $subParams->plan_id = $s->plan_id;
					        $subParams->original_amount = $s->original_amount;
					        $subParams->status = $s->sub_status;
					        $subParams->period_length = $s->period_length;
					        $subParams->card_id = $s->card_id;
					        $subParams->points = $s->points;

					        $sub = AxsPayment::newSubscription($subParams);
					        $plan = $sub->getPlan();

					        $amount = $sub->original_amount;

					        //lets see if we can subtract from this with the rewards points
					        $rewards = $dispatcher->trigger("onCronCalcPoints", array(&$brand, &$sub->user_id, &$amount, &$sub->points));
					        $rewards = $rewards[0];
					        if ($rewards) {
						        $amount = $rewards->total;
					        }

					        if ($amount > 0) {
						        $card = 0;
						        if ($sub->card_id) {
						        	$cardParams = AxsPayment::getCardParams();
							        $cardParams->id = $sub->card_id;
							        $card = AxsPayment::newCard($cardParams);
							        $card->getCardWithId();
							        //if they don't own the card and they aren't part of the family, try to just grab the first card available to them.
							        if($card->user_id != $sub->user_id && !in_array($sub->user_id, $card->family_access)) {
								        $cards = AxsPayment::getUserAllCards($sub->user_id);
								        $card = $cards[0];
							        }
						        } else {
							        //if they have no card set to use for this sub, try to just grab the first card available to them.
							        $cards = AxsPayment::getUserAllCards($sub->user_id);
							        $card = $cards[0];
						        }

						        $trxnParams = AxsPayment::getTransactionParams();
						        
						        $trxnParams->user_id = $sub->user_id;
						        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
						        $trxnParams->type_id = $sp->id;
						        $trxnParams->amount = $amount;
						        $trxnParams->date = date("Y-m-d H:i:s");
						        $trxnParams->initiator = AxsPayment::$initiators['system (auto recover)'];
						        $trxnParams->description = $plan->title;

						        //if there is still no charge-able card, then we need to make a failed trxn and continue.
						        if (!$card) {

						        	$trxnParams->status = "FAIL";
						        	$trxnParams->code = -1;
						        	$trxnParams->message = 'No cards on file that can be used for this transaction, so never sent to converge.';

							        $trxn = AxsPayment::newTransaction($trxnParams);
							        $trxn->save();
							        continue;
						        }

						        $trxnParams->card = $card;

						        //make charge and get status
						        $trxn = AxsPayment::newTransaction($trxnParams);
						        $trxn->charge();
						        if($trxn->status == "SUC") {
							        $sp->status = "PAID";
							        $sp->save();
							        //if the sub period is for the current time, set the status to active for the sub.
							        $now = new DateTime();
							        if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
								        $sub->status = "ACT";
								        $sub->save();
							        }
							        $dispatcher->trigger("onCronSuccess", array(&$brand, &$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
						        }
					        } else {
						        $sp->status = "PAID";
						        $sp->save();
						        //if the sub period is for the current time, set the status to active for the sub.
						        $now = new DateTime();
						        if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
							        $sub->status = "ACT";
							        $sub->save();
						        }
						        $dispatcher->trigger("onCronSuccess", array(&$brand, &$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
					        }
				        }
			        }
		        }

	          //######################### THIS WILL GO AND DOWNGRADE ANYONE WHO RUNS OUT OF GRACE ################################
	          $grace = $brand->billing->grace_period;
	          $query = "SELECT a.*
											FROM axs_pay_subscriptions a
											JOIN axs_pay_subscription_periods b ON a.id=b.sub_id
											WHERE a.$plans AND DATE(b.start)=(CURDATE()-INTERVAL $grace DAY)
											AND b.status='NONE' AND a.status='GRC';";
	          $db->setQuery($query);
	          $subs = $db->loadObjectList();
	          foreach($subs as $s) {
	          	  //update their status to INAC and remove the usergroup for that subscription.
		            $query = "UPDATE axs_pay_subscriptions SET status='INAC' WHERE id=$s->id;";
			        $db->setQuery($query);
			        $db->execute();

		            $active_usergroup = $planActiveUsergroups[$s->plan_id];
		            $query = "DELETE FROM #__user_usergroup_map WHERE user_id=$s->user_id AND group_id=$active_usergroup;";
			        $db->setQuery($query);
			        $db->execute();

			        BadgeHelper::updateBadges(null, $s->user_id, $db);
	          }

	          //######################### THIS WILL GO AND REMOVE THE ACTIVE USERGROUP FROM EVERYONE WHO CANCELLED WHEN THEY WHERE ACTIVE ###############
	          $query = "SELECT * FROM (
												SELECT a.*, b.end 
												FROM axs_pay_subscriptions a 
												JOIN axs_pay_subscription_periods b ON a.id=b.sub_id 
												WHERE a.status='INAC' AND a.cancel IS NOT NULL ORDER BY b.end DESC
											) c WHERE DATE(c.end)=CURDATE() GROUP BY c.id;";
		        $db->setQuery($query);
		        $subs = $db->loadObjectList();
		        foreach($subs as $s) {
			        //remove the usergroup for that subscription.
			        $active_usergroup = $planActiveUsergroups[$s->plan_id];
			        $query = "DELETE FROM #__user_usergroup_map WHERE user_id=$s->user_id AND group_id=$active_usergroup;";
			        $db->setQuery($query);
			        $db->execute();

			        BadgeHelper::updateBadges(null, $s->user_id, $db);
		        }

	        //######################### THIS WILL GO AND REMOVE THE ACTIVE USERGROUP FROM EVERYONE WHO PAUSED WHEN THEY WHERE ACTIVE ###############
	        $query = "";
        }

        $time = microtime(true) - $timer_start;

        $date = date('Y-m-d');
        echo "\nDate: $date\n"
        ."Attempted to make: $total charges\n"
        ."Successfully made: $success\n"
        ."Aborted: $aborted\n"
        ."Failed: $error\n"
        ."Execution Time: $time\n";
    }
}
// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('DailyCronCharges')->execute();