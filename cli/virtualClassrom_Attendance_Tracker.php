<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/lms.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualClassroom.php';
require_once JPATH_LIBRARIES . '/axslibs/virtualMeetingServers.php';
require_once JPATH_LIBRARIES . '/axslibs/keys.php';
require_once JPATH_LIBRARIES . '/axslibs/encryption.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateMetadata extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND (status LIKE 'active%' OR status = 'test')";
        $db_result = $conn->query($query);
        $i = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('#__bbb_meetings');
            $db->setQuery($query);
            $results = $db->loadObjectList();
			$now = date('Y-m-d H:i:s');
            $attendanceTable = 'axs_virtual_classroom_attendees';

            $clientParams = json_decode($dbs->params);
            $VC_Server = $clientParams->virtual_meeting_server;
            $setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
            $virtualMeeting = new AxsVirtualClassroom($setupParams);
            if($results) {
				foreach($results as $row) {
					$params = json_decode($row->settings);
					if($params->meetingType == 'tovuti') {
						$virtualClassIsRunning = $virtualMeeting->isMeetingRunning($row->meetingId);
						if($virtualClassIsRunning == 'true') {
							$meetingParams = new stdClass();
							$meetingParams->meetingId = $row->meetingId;
							$meetingParams->moderatorPW = $row->moderatorPW;
							$virtualClass = $virtualMeeting->getMeetingInfo($meetingParams);
							if($virtualClass) {
                                $attendeeArray = $virtualClass->attendees->attendee;
								foreach($attendeeArray as $attendee) {
									$conditions = array();
									$conditions[] = $db->quoteName('meeting_id').'='.$db->quote($row->meetingId);
									$conditions[] = $db->quoteName('session').'='.$db->quote($virtualClass->createTime);
									if(strpos($attendee->userID,'-')) {
										$conditions[] = $db->quoteName('guest_id').'='.$db->quote($attendee->userID);
									} else {
										$conditions[] = $db->quoteName('user_id').'='.$db->quote($attendee->userID);
									}
									$query = $db->getQuery(true);
									$query->select('*');
									$query->from($db->quoteName($attendanceTable));
									$query->where($conditions);
                                    $db->setQuery($query);
									$attendanceResults = $db->loadObjectList();
									if($attendanceResults) {
										foreach($attendanceResults as $attendanceRow) {
											$attendanceRow->check_out = $now;
                                            $db->updateObject($attendanceTable,$attendanceRow,'id');
										}
									}
								}
							}
						}
					}
				}
              echo $i++.'.'.$name.": updated\n";
            }
            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliUpdateMetadata')->execute();