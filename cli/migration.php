<?php

class Migration {

    public static function processRules($rules) {
        $permissions = array();
        $newPermissions = json_decode(file_get_contents('/var/www/html/cli/admin_permissions_new.json'));
        foreach($newPermissions as $newRule) {
            $adminRule = new stdClass();
            switch($newRule->system) {
                case "tovuti_nav_people":
                    $adminRule->system = "tovuti_nav_people";
                    $adminRule->allowed = $rules['axsusers'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_manger":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_manger";
                    $adminRule->allowed = $rules['axsusers-user_manager'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_groups":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_groups";
                    $adminRule->allowed = $rules['axsusers-groups'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_teams":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_teams";
                    $adminRule->allowed = $rules['axsusers-user_teams'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_importer":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_importer";
                    $adminRule->allowed = $rules['axsusers-user_importer'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_notes":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_notes";
                    $adminRule->allowed = $rules['axsusers-user_notes'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_note_categories":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_note_categories";
                    $adminRule->allowed = $rules['axsusers-user_notes_categories'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_access_levels":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_access_levels";
                    $adminRule->allowed = $rules['axsusers-access_levels'];
                    break;
                case "tovuti_nav_people-tovuti_nav_user_admin_permissioning":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_user_admin_permissioning";
                    $adminRule->allowed = $rules['axsusers-admin_permissioning'];
                    break;
                case "tovuti_nav_people-tovuti_nav_teachers":
                    $adminRule->system = "tovuti_nav_people-tovuti_nav_teachers";
                    $adminRule->allowed = $rules['learning_management-teachers'];
                    break;
                case "tovuti_nav_content_creation":
                    $adminRule->system = "tovuti_nav_content_creation";
                    $adminRule->allowed =
                    $rules['learning_management-interactive']
                    ||
                    $rules['content-media_manager']
                    ||
                    $rules['content-media_categories']
                    ||
                    $rules['content-files']
                    ||
                    $rules['content-lockedfiles']
                    ||
                    $rules['learning_management-quiz_questions']
                    ||
                    $rules['learning_management-surveys']
                    ||
                    $rules['learning_management-content_library']
                    ||
                    $rules['learning_management-scorm_library'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_interactive_content":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_interactive_content";
                    $adminRule->allowed = $rules['learning_management-interactive'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_content_library":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_content_library";
                    $adminRule->allowed = $rules['learning_management-content_library'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_scorm_library":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_scorm_library";
                    $adminRule->allowed = $rules['learning_management-scorm_library'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_content_assessments_header":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_content_assessments_header";
                    $adminRule->allowed =
                    $rules['learning_management-quiz_questions']
                    ||
                    $rules['learning_management-surveys'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_quizzes":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_quizzes";
                    $adminRule->allowed = $rules['learning_management-quiz_questions'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_surveys":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_surveys";
                    $adminRule->allowed = $rules['learning_management-surveys'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_content_media_header":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_content_media_header";
                    $adminRule->allowed =
                    $rules['content-media_manager']
                    ||
                    $rules['content-media_categories']
                    ||
                    $rules['content-files']
                    ||
                    $rules['content-lockedfiles'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_media_items":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_media_items";
                    $adminRule->allowed = $rules['content-media_manager'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_media_categories":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_media_categories";
                    $adminRule->allowed = $rules['content-media_categories'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_file_manager":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_file_manager";
                    $adminRule->allowed = $rules['content-files'];
                    break;
                case "tovuti_nav_content_creation-tovuti_nav_secure_file_locker":
                    $adminRule->system = "tovuti_nav_content_creation-tovuti_nav_secure_file_locker";
                    $adminRule->allowed = $rules['content-lockedfiles'];
                    break;
                case "tovuti_nav_learning":
                    $adminRule->system = "tovuti_nav_learning";
                    $adminRule->allowed = $rules['learning_management'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_settings":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_settings";
                    $adminRule->allowed = $rules['learning_management-lms_settings'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_course_categories":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_course_categories";
                    $adminRule->allowed = $rules['learning_management-course_categories'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_courses":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_courses";
                    $adminRule->allowed = $rules['learning_management-courses'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_lessons":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_lessons";
                    $adminRule->allowed = $rules['learning_management-lessons'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_checklists":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_checklists";
                    $adminRule->allowed = $rules['learning_management-checklist'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_courseassignments":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_courseassignments";
                    $adminRule->allowed = $rules['learning_management-assign'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_virtual_classrooms":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_virtual_classrooms";
                    $adminRule->allowed = $rules['learning_management-virtual_classroom'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_comments":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_comments";
                    $adminRule->allowed = $rules['axsusers-comments'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_gamification_header":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_gamification_header";
                    $adminRule->allowed = $rules['gamification'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_badges":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_badges";
                    $adminRule->allowed = $rules['gamification-gamification_badges'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_certificates":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_certificates";
                    $adminRule->allowed = $rules['gamification-certificates'];
                    break;
                case "tovuti_nav_learning-tovuti_nav_learning_points_categories":
                    $adminRule->system = "tovuti_nav_learning-tovuti_nav_learning_points_categories";
                    $adminRule->allowed = $rules['gamification-points_categories'];
                    break;
                case "tovuti_nav_communications":
                    $adminRule->system = "tovuti_nav_communications";
                    $adminRule->allowed = $rules['communications'];
                    break;
                case "tovuti_nav_communications-tovuti_nav_notifications":
                    $adminRule->system = "tovuti_nav_communications-tovuti_nav_notifications";
                    $adminRule->allowed = $rules['communications-notifications-auto_notifications'];
                    break;
                case "tovuti_nav_communications-tovuti_nav_email_notifications":
                    $adminRule->system = "tovuti_nav_communications-tovuti_nav_email_notifications";
                    $adminRule->allowed = $rules['communications-notifications-email_notifications'];
                    break;
                case "tovuti_nav_communications-tovuti_nav_popup_notifications":
                    $adminRule->system = "tovuti_nav_communications-tovuti_nav_popup_notifications";
                    $adminRule->allowed = $rules['communications-notifications-popup_notifications'];
                    break;
                case "tovuti_nav_ecommerce":
                    $adminRule->system = "tovuti_nav_ecommerce";
                    $adminRule->allowed =
                    $rules['transaction_manager']
                    ||
                    $rules['subscriptions-sub_plans']
                    ||
                    $rules['subscriptions-sub_categories']
                    ||
                    $rules['subscriptions-sub_cancels']
                    ||
                    $rules['subscriptions-subscription_lms_promos2'];
                    break;
                case "tovuti_nav_ecommerce-tovuti_nav_sales":
                    $adminRule->system = "tovuti_nav_ecommerce-tovuti_nav_sales";
                    $adminRule->allowed = $rules['transaction_manager'];
                    break;
                case "tovuti_nav_ecommerce-tovuti_nav_subscriptions_categories":
                    $adminRule->system = "tovuti_nav_ecommerce-tovuti_nav_subscriptions_categories";
                    $adminRule->allowed = $rules['subscriptions-sub_categories'];
                    break;
                case "tovuti_nav_ecommerce-tovuti_nav_subscriptions_plans":
                    $adminRule->system = "tovuti_nav_ecommerce-tovuti_nav_subscriptions_plans";
                    $adminRule->allowed = $rules['subscriptions-sub_plans'];
                    break;
                case "tovuti_nav_ecommerce-tovuti_nav_subscriptions_requests":
                    $adminRule->system = "tovuti_nav_ecommerce-tovuti_nav_subscriptions_requests";
                    $adminRule->allowed = $rules['subscriptions-sub_cancels'];
                    break;
                case "tovuti_nav_ecommerce-tovuti_nav_subscriptions_promos":
                    $adminRule->system = "tovuti_nav_ecommerce-tovuti_nav_subscriptions_promos";
                    $adminRule->allowed = $rules['subscriptions-subscription_lms_promos2'];
                    break;
                case "tovuti_nav_analytics":
                    $adminRule->system = "tovuti_nav_analytics";
                    $adminRule->allowed =
                    $rules['reports']
                    ||
                    $rules['learning_management-quiz_results'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_report_builder":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_report_builder";
                    $adminRule->allowed = $rules['reports-report_manager'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_activity_reports":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_activity_reports";
                    $adminRule->allowed = $rules['reports-activity_reports'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_expiration_reports":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_expiration_reports";
                    $adminRule->allowed = $rules['reports-expiration_reports'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_activity_dashboards":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_activity_dashboards";
                    $adminRule->allowed = $rules['reports-activity_dashboard'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_financial_reports":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_financial_reports";
                    $adminRule->allowed = $rules['reports-dashboard'];
                    break;
                case "tovuti_nav_analytics-tovuti_nav_analytics_quiz_results":
                    $adminRule->system = "tovuti_nav_analytics-tovuti_nav_analytics_quiz_results";
                    $adminRule->allowed = $rules['learning_management-quiz_results'];
                    break;
                case "tovuti_nav_social":
                    $adminRule->system = "tovuti_nav_social";
                    $adminRule->allowed =
                    $rules['events']
                    ||
                    $rules['community']
                    ||
                    $rules['forum'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_events_header":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_events_header";
                    $adminRule->allowed = $rules['events'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_event_dashboard":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_event_dashboard";
                    $adminRule->allowed = $rules['events-events_dash'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_event_registrants":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_event_registrants";
                    $adminRule->allowed = $rules['events-events_registrants'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_event_categories":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_event_categories";
                    $adminRule->allowed = $rules['events-events_categories'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_events":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_events";
                    $adminRule->allowed = $rules['events-events_events'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_event_configuration":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_event_configuration";
                    $adminRule->allowed = $rules['events-events_config'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_community_header":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_community_header";
                    $adminRule->allowed = $rules['community'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_community_dashboard":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_community_dashboard";
                    $adminRule->allowed = $rules['community-community_dash'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_community_settings":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_community_settings";
                    $adminRule->allowed = $rules['community-community_settings'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_community_groups":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_community_groups";
                    $adminRule->allowed = $rules['community-community_groups'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_community_events":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_community_events";
                    $adminRule->allowed = $rules['community-community_events'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_forum_header":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_forum_header";
                    $adminRule->allowed = $rules['forum'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_forum_dashboard":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_forum_dashboard";
                    $adminRule->allowed = $rules['forum'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_forum_settings":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_forum_settings";
                    $adminRule->allowed = $rules['forum-forum_config'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_forum_categories":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_forum_categories";
                    $adminRule->allowed = $rules['forum-forum_categories'];
                    break;
                case "tovuti_nav_social-tovuti_nav_social_forum_attachments":
                    $adminRule->system = "tovuti_nav_social-tovuti_nav_social_forum_attachments";
                    $adminRule->allowed = $rules['forum-forum_attachments'];
                    break;
                case "tovuti_nav_design":
                    $adminRule->system = "tovuti_nav_design";
                    $adminRule->allowed = $rules['design'];
                    break;
                case "tovuti_nav_design-tovuti_nav_design_quick_start":
                    $adminRule->system = "tovuti_nav_design-tovuti_nav_design_quick_start";
                    $adminRule->allowed = $rules['design-quick_start'];
                    break;
                case "tovuti_nav_design-tovuti_nav_design_brands":
                    $adminRule->system = "tovuti_nav_design-tovuti_nav_design_brands";
                    $adminRule->allowed = $rules['design-brand_manager'];
                    break;
                case "tovuti_nav_design-tovuti_nav_design_user_portals":
                    $adminRule->system = "tovuti_nav_design-tovuti_nav_design_user_portals";
                    $adminRule->allowed = $rules['design-user_dashboard_themes'];
                    break;
                case "tovuti_nav_design-tovuti_nav_design_user_dashboard":
                    $adminRule->system = "tovuti_nav_design-tovuti_nav_design_user_dashboard";
                    $adminRule->allowed = $rules['design-learner_dashboards'];
                    break;
                case "tovuti_nav_design-tovuti_nav_design_login_pages":
                    $adminRule->system = "tovuti_nav_design-tovuti_nav_design_login_pages";
                    $adminRule->allowed = $rules['design-login_pages'];
                    break;
                case "tovuti_nav_design_website":
                    $adminRule->system = "tovuti_nav_design_website";
                    $adminRule->allowed =
                    $rules['design-home_page_themes']
                    ||
                    $rules['content-articles']
                    ||
                    $rules['content-blog']
                    ||
                    $rules['communications-pages'];
                    break;
                case "tovuti_nav_design_website-tovuti_nav_design_landing_pages":
                    $adminRule->system = "tovuti_nav_design_website-tovuti_nav_design_landing_pages";
                    $adminRule->allowed = $rules['design-home_page_themes'];
                    break;
                case "tovuti_nav_design_website-tovuti_nav_design_pages":
                    $adminRule->system = "tovuti_nav_design_website-tovuti_nav_design_pages";
                    $adminRule->allowed = $rules['content-articles'];
                    break;
                case "tovuti_nav_design_website-tovuti_nav_design_blogs":
                    $adminRule->system = "tovuti_nav_design_website-tovuti_nav_design_blogs";
                    $adminRule->allowed = $rules['content-blog'];
                    break;
                case "tovuti_nav_design_website-tovuti_nav_design_forms":
                    $adminRule->system = "tovuti_nav_design_website-tovuti_nav_design_forms";
                    $adminRule->allowed = $rules['communications-pages'];
                    break;
                case "tovuti_nav_configuration":
                    $adminRule->system = "tovuti_nav_configuration";
                    $adminRule->allowed =
                    $rules['axsusers']
                    ||
                    $rules['design']
                    ||
                    $rules['accessibility'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_profile_fields":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_profile_fields";
                    $adminRule->allowed = $rules['community-community_user_fields'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_domains":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_domains";
                    $adminRule->allowed = $rules['design-domain_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_email":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_email";
                    $adminRule->allowed = $rules['design-brand_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_payment_gateways":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_payment_gateways";
                    $adminRule->allowed = $rules['design-brand_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_navigation":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_navigation";
                    $adminRule->allowed = $rules['design-menu_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_fonts":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_fonts";
                    $adminRule->allowed = $rules['design-font_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_integrations":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_integrations";
                    $adminRule->allowed = $rules['design-brand_manager'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_sso":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_sso";
                    $adminRule->allowed = $rules['axsusers-sso'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_api_keys":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_api_keys";
                    $adminRule->allowed = $rules['design-api_keys'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_security":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_security";
                    $adminRule->allowed = $rules['axsusers-user_settings'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_configuration_user_avatars":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_configuration_user_avatars";
                    $adminRule->allowed = $rules['axsusers-user_avatars'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_accessibility":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_accessibility";
                    $adminRule->allowed = $rules['accessibility'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_accessibility_images":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_accessibility_images";
                    $adminRule->allowed = $rules['accessibility-accessibility_images'];
                    break;
                case "tovuti_nav_configuration-tovuti_nav_accessibility_videos":
                    $adminRule->system = "tovuti_nav_configuration-tovuti_nav_accessibility_videos";
                    $adminRule->allowed = $rules['accessibility-accessibility_videos'];
                    break;
            }


            if($adminRule->system) {
                array_push($permissions,$adminRule);
            }
        }
        $adminAccess = new stdClass();
        $adminAccess->access = $rules['access'];
        array_push($permissions,$adminAccess);
        return $permissions;
    }

}