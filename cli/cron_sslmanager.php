<?php
/**
 * @package     Joomla.Cli
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 *
 * Joomla 3.2 example CLI script
 * Written by: Rene Kreijveld, email [at] renekreijveld.nl
 * 05-feb-2014
 * Put this script in the /cli folder in the root of your Joomla 3.2 website
 * Execute by php <path_to_your_joomla_root>/cli/clidemo_3.2.php
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

// Set flag that this is a parent file.
const _JEXEC = 1;


// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once("/var/www/files/creds.php");


/**
 * @package  Joomla.CLI
 * @since    3.0
 */

require JPATH_BASE . '/plugins/aws/aws-autoloader.php';

use Aws\Acm\AcmClient;
use Aws\ElasticLoadBalancingV2\ElasticLoadBalancingV2Client;
use Aws\Acm\Exception as acmException;
use Aws\ElasticLoadBalancingV2\Exception as elbException;

use Aws\Exception as awsException;


class RetryableException extends Exception {}


class LetsEncrypt extends JApplicationCli
{

    public $ip;


    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */


    public function doExecute()
    {
        if (!class_exists("AxsClients")) {
            require_once("/var/www/html/libraries/axslibs/clients.php");
        }

        if (!class_exists("AxsEncryption")) {
            require_once("/var/www/html/libraries/axslibs/encryption.php");
        }
        $date = date("Y_m_d_H_i_s");
        $today = date("Y_m_d");
        $config = dbCreds::getAwsConfig();
        $verification_email = $config->email;
        $log_dir = $config->log_dir;

        $logfile = fopen($log_dir . "sslmanager/letsencrypt.log", "a");
        fwrite($logfile, "Starting lets encrypt at " . date("Y-m-d H:i:s") . "\n");
        $starttime = microtime(true);


        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_sslmanager WHERE enabled = 1";
        $db->setQuery($query);
        $domains = $db->loadObjectList();


        $credentials = dbCreds::getCreds();
        $creds = "-h $credentials->dbhost -u $credentials->dbuser -p" . $credentials->dbpass;

        // active == 1
        $current_active = [];
        // active == 0
        $current_nonactive = [];
        //all domains
        $all_domains = [];
        // successful dry run
        $validated = [];

        foreach ($domains as $domain) {
            if (!$domain->active) {
                array_push($current_nonactive, $domain->domain);
            } else {
                array_push($current_active, $domain->domain);
            }
            array_push($all_domains, $domain->domain);

        }

        fwrite($logfile, "Found " . count($current_active) . " Active Sites and " . count($current_nonactive) . " Non Active Sites \n");

        // validate current domains
        foreach ($all_domains as $domain) {
            $output = null;
            $retval = null;
            exec("certbot certonly --apache -q -d " . $domain . " --email " . $verification_email . " --agree-tos --dry-run 2>&1", $output, $retval);
            fwrite($logfile, "$domain validation returned with status $retval and output: " . json_encode($output) . "\n");
            if ($retval != 0) {
                $db = JFactory::getDBO();
                $query = "UPDATE axs_sslmanager SET status = '" . json_encode($output) . "', active = 0 WHERE domain = '" . $domain . "' ";
                $db->setQuery($query);
                $db->execute();
            } else {
                array_push($validated, $domain);
            }
        }

        //confirm whether any domains are valid
        if (empty($validated)) {
            fwrite($logfile, "None of the existing domains were validated exiting \n");
            exit("None of the existing domains were validated");
        } else {
            // need to add logic to chunk by 100
            $chunked = array_chunk($validated, 100);
            foreach ($chunked as $i => $c) {
                $domains = $c;
                $name = "tovuti-bundle-" . $i;
                $final = implode(",", $domains);
                $output = null;
                $retval = null;

                $retry = 5;

                again:
                try {
                    exec("certbot certonly --apache -d " . $final . " --email " . $verification_email . " --agree-tos --cert-name " . $name . " --keep-until-expiring --non-interactive 2>&1", $output, $retval);
                    fwrite($logfile, "certificate creation returned with status $retval and output: " . json_encode($output) . "\n");
                    // create/extend certificate
                    fwrite($logfile, "certbot certonly --apache -d " . $final . " --email " . $verification_email . " --agree-tos --cert-name " . $name . " --keep-until-expiring --non-interactive \n");
                    if($retval != 0){
                        throw new RetryableException();
                    }
                } catch (RetryableException $e) {
                    if ($retry-- > 0) {
                        fwrite($logfile, "Retrying certificate creation" . $retry . " retries left");
                        goto again;
                    }
                    # if retry does not work then allow the successful domains through
                    exec("certbot certonly --apache -d " . $final . " --email " . $verification_email . " --agree-tos --cert-name " . $name . " --keep-until-expiring --non-interactive --allow-subset-of-names 2>&1", $output, $retval);
                    fwrite($logfile, "certificate creation returned with status $retval and output: " . json_encode($output) . "\n");
                    // create/extend certificate
                    fwrite($logfile, "certbot certonly --apache -d " . $final . " --email " . $verification_email . " --agree-tos --cert-name " . $name . " --keep-until-expiring --non-interactive --allow-subset-of-names \n");
                }


                $path = "/etc/letsencrypt/live/" . $name;

                $new_cert_hash = md5_file($path . "/cert.pem");
                if(self::isCertificateUpdated($name, $new_cert_hash, $domains)){
                    fwrite($logfile, "New certificate bundle, or existing certificate updated, uploading to AWS ");
                    $arn = self::acmCertificateUpload($path, $name, $logfile);
                    self::updateListenerCertificate($arn, $config->external_listener_arn, 'add', $logfile);
                    if ($config->internal_listener_arn) {
                        self::updateListenerCertificate($arn, $config->internal_listener_arn, 'add', $logfile);
                    }
                    $db = JFactory::getDBO();
                    $query = "UPDATE axs_sslmanager SET certificate_arn = '" . $arn . "', active = 1 WHERE domain IN ('" . implode("','", $domains) . "') ";
                    fwrite($logfile, $query);
                    $db->setQuery($query);
                    $db->execute();

                    $db = JFactory::getDBO();
                    $query = "UPDATE axs_ssl_status SET certificate_arn = '" . $arn . "', certificate_hash = '" . $new_cert_hash . "', updated = CURRENT_TIMESTAMP(), domains = '" . $final . "', domain_count = '" . count($domains) . "'  WHERE name = '" . $name . "' ";
                    fwrite($logfile, $query);
                    $db->setQuery($query);
                    $db->execute();

                } else {
                    fwrite($logfile, "No Changes to certificate\n");
                }
            }
        }
        fwrite($logfile, "Checking for unassociated certificates to cleanup\n");
        $this->removeUnassociatedCertificates($config, $logfile);

        $runtime = (float)(((int)((microtime(true) - $starttime) * 10)) / 10);
        fwrite($logfile, "Finished lets encrypt at " . date("Y-m-d H:i:s") . "\nExecution time: $runtime seconds\n\n");
        fclose($logfile);

    }

    /**
     * Uploads certificate to ACM and returns an ARN
     * @param string path path of certificate bundle
     * @param string name name of certificate bundle
     * @return  string acm certificate arn
     */
    private static function acmCertificateUpload($path, $name, $logfile)
    {
        $config = dbCreds::getAwsConfig();
        $sharedConfig = [
            'region' => $config->region,
            'version' => 'latest'
        ];
        $sdk = new Aws\Sdk($sharedConfig);
        $client = $sdk->createAcm();
        $cert = file_get_contents($path . "/cert.pem");
        $chain = file_get_contents($path . "/chain.pem");
        $privkey = file_get_contents($path . "/privkey.pem");
        $acmRequestData = [
            'Certificate' => $cert,
            'CertificateChain' => $chain,
            'PrivateKey' => $privkey,
            'Tags' => [
                [
                    'Key' => 'Name',
                    'Value' => $name,
                ],
            ],
        ];

        try {
            $result = $client->importCertificate($acmRequestData);
            $result = $result['CertificateArn'];
        } catch (Exception $e) {
            fwrite($logfile, $e->getMessage());
            exit("Unable to upload certificate: \n" . $e->getMessage());
        }
        fwrite($logfile, $result);

        return $result;
    }

    /**
     * removes certificate from ACM
     * @param  string arn acm certificate arn
     */
    private static function acmCertificateRemove($arn, $logfile)
    {
        $config = dbCreds::getAwsConfig();
        $sharedConfig = [
            'region' => $config->region,
            'version' => 'latest'
        ];
        $sdk = new Aws\Sdk($sharedConfig);
        $client = $sdk->createAcm();
        $acmRequestData = [
            'CertificateArn' => $arn,
        ];

        // very ugly attempt at exponential backoff... this should be converted to use a retry function

        $retry = 5;
        $sleep = 10;
        retry:
        try {
            sleep($sleep);
            $client->deleteCertificate($acmRequestData);
        } catch (Exception $e) {
            fwrite($logfile, $e->getMessage());
            if ($retry-- > 0) {
                fwrite($logfile, "Retrying certificate deletion " . $retry . " retries left");
                $sleep = $sleep + 10;
                goto retry;
            }
        }
    }

    /**
     * Associates certificate with loadbalancer
     * @param string $certArn ARN of certificate to attach
     * @param string $elbListenerArn ARN of elb listener to attach certificate to
     * @param string $type add | remove  flag to switch update type
     */
    private static function updateListenerCertificate(string $certArn, string $elbListenerArn, string $type, $logfile)
    {
        $config = dbCreds::getAwsConfig();
        $sharedConfig = [
            'region' => $config->region,
            'version' => 'latest'
        ];
        $sdk = new Aws\Sdk($sharedConfig);
        $client = $sdk->createElasticLoadBalancingV2();
        $requestData = [
            'Certificates' => [
                [
                    'CertificateArn' => $certArn,
                ],
            ],
            'ListenerArn' => $elbListenerArn,
        ];

        try {
            if ($type == 'add') {
                $result = $client->addListenerCertificates($requestData);
            } else {
                $result = $client->removeListenerCertificates($requestData);
            }

        } catch (Exception $e) {
            fwrite($logfile, $e->getMessage());
            exit("Unable to add certificate: \n" . $e->getMessage());
        }
        fwrite($logfile, $result);
    }




    /**
     * return a list of certificates attached to a listener
     * @param string $elbListenerArn ARN of elb listener to attach certificate to
     */
    private static function getListenerCertificates(string $elbListenerArn, $logfile)
    {
        $config = dbCreds::getAwsConfig();
        $sharedConfig = [
            'region' => $config->region,
            'version' => 'latest'
        ];
        $sdk = new Aws\Sdk($sharedConfig);
        $client = $sdk->createElasticLoadBalancingV2();
        $requestData = [
            'ListenerArn' => $elbListenerArn,
            'PageSize' => 50,
        ];

        $response = [];

        try {
            $result = $client->describeListenerCertificates($requestData);
            foreach ($result['Certificates'] as $cert) {
                if ($cert['IsDefault'] == false) {
                    array_push($response, $cert['CertificateArn']);
                }
            }

        } catch (elbException\ElasticLoadBalancingV2Exception $e) {
            fwrite($logfile, $e->getMessage());
        }
        fwrite($logfile, $result);
        return $response;
    }


    /**
     * @param string $name
     * @param string $new_cert_hash
     * @param array $domains
     * @return bool
     */
    private static function isCertificateUpdated(string $name, string $new_cert_hash, array $domains): bool
    {
        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_ssl_status WHERE name = '" . $name . "'";
        $db->setQuery($query);
        $ssl_status = $db->loadObjectList();
        if ($ssl_status) {
            $response = false;
            if ($new_cert_hash != $ssl_status[0]->certificate_hash) {
                $response = true;
            }
        } else {
            //create initial entry
            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $columnsArray = array(
                $db->quoteName('name'), $db->quoteName('domain_count'), $db->quoteName('domains'), $db->quoteName('created'), $db->quoteName('updated')
            );

            $final = implode(',', $domains);

            $query->clear()
                ->insert($db->quoteName('axs_ssl_status'))
                ->columns($columnsArray)
                ->values(
                    $db->quote($name) . ', '
                    . $db->quote(count($domains)) . ', '
                    . $db->quote($final) . ', '
                    . $query->currentTimestamp() . ', '
                    . $query->currentTimestamp()
                );
            echo $query;
            $db->setQuery($query);
            $db->execute();

            $response = true;
        }
        return $response;
    }


    /**
     * looks for unassociated certificates and removes them from the listener
     * @param $config
     */
    private function removeUnassociatedCertificates($config, $logfile)
    {
        $unassociatedCerts = [];
        $listeners = array($config->external_listener_arn);
        if ($config->internal_listener_arn) {
            array_push($listeners, $config->internal_listener_arn);
        }
        foreach ($listeners as $l) {
            if ($l) {
                $listenerCerts = self::getListenerCertificates($l, $logfile);
                $db = JFactory::getDBO();
                $query = "SELECT DISTINCT certificate_arn from axs_sslmanager WHERE certificate_arn IS NOT NULL";
                $db->setQuery($query);
                $distinctCerts = $db->loadObjectList();
                $associatedCerts = [];
                foreach ($distinctCerts as $d) {
                    array_push($associatedCerts, $d->certificate_arn);
                }
                $associatedCerts = array_merge($associatedCerts, $config->managed_certificates);
                foreach ($listenerCerts as $cert) {
                    if (in_array($cert, $associatedCerts) == false) {
                        array_push($unassociatedCerts, $cert);
                        fwrite($logfile, "Removing the following certificate because it has no associated domains: \n " . $cert . "\n");
                        self::updateListenerCertificate($cert, $l, 'remove', $logfile);
                    }
                }
            }
        }
        // after certificates are removed from listeners we can delete them
        foreach ($unassociatedCerts as $cert){
            self::acmCertificateRemove($cert, $logfile);
        }
    }
}

// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('LetsEncrypt')->execute();
