<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliUpdateDateFormat extends JApplicationCli {

    private $db = null;


    public function doExecute() {


        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND status LIKE 'active%'";
        $db_result = $conn->query($query);
        $i = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;
            $db = JFactory::getDbo();

            //get birthdate field id
            $query = $db->getQuery(true);
            $query->select('id');
            $query->from('#__community_fields');
            $query->where("type = 'birthdate'");
            $db->setQuery($query);
            $results = $db->loadObjectList();
            $fieldId = $results[0]->id;

            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('#__community_fields_values');
            $query->where("field_id = $fieldId");
            $db->setQuery($query);
            $values = $db->loadObjectList();
            
            if($values) {
                foreach($values as $value) {                    
                    $date = date("Y-m-d", strtotime($value->value));
                    $query = "UPDATE joom_community_fields_values SET value=$date WHERE id=$value->id;";
			        $db->setQuery($query);
			        $db->execute();

                    echo $value->id . ": updated\n";
                }
            }           
            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliUpdateDateFormat')->execute();