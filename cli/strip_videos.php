<?php

date_default_timezone_set('America/Boise');

// Make sure we're being called from the command line, not a web interface
if (PHP_SAPI !== 'cli') {
    die('This is a command line only application.');
}

// Initialize Joomla framework
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}
require_once JPATH_CONFIGURATION . '/configuration.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';

class StripVideos extends JApplicationCli
{
    /**
     * Entry point for CLI script
     *
     * @return  void
     *
     * @since   3.0
     */

    public function doExecute()
    {
        

        //error_reporting(E_ALL);
        //ini_set('display_errors', 1);






$htmlinput = <<<EOT
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300">
                    <img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">What 97% of Women Do With Their Own Self Worth
                        <br> featuring Cinthia Moura</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    What 97% of Women Do With Their Own Self Worth2
                    <br>
                    <br> Businesswoman and International Fashion Model Cinthia Moura presents this compelling Webinar for women today.
                    <p><a href="/members/webinars/?webinarid=147" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Social Media Basics: Twitter Part 2
                        <br> featuring George Seybold</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Social Media Basics: Twitter Part 2
                    <br>
                    <br> Social Media expert George Seybold continues his lesson on the basics of Twitter. Learn how to promote your business and find other like-minded people and businesses. .
                    <p><a href="/members/webinars/?webinarid=144" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Social Media Basics: Twitter Part 1
                        <br> featuring George Seybold</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Social Media Basics: Twitter Part 1
                    <br>
                    <br> Social Media Guru George Seybold teaches the basics of social media within Twitter. Learning the how-to for this powerful social networking tool can help you connect with friends, family and consumers. Find out why and how Twitter can work with you.
                    <p><a href="/members/webinars/?webinarid=143" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">FILO 2: Power Through Perseverance
                        <br> featuring Brian Tooley</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    FILO 2: Power Through Perseverance
                    <br>
                    <br> Brian Tooley continues his FILO (First In, Last Out) series of webinars with this one, entitled Power Through Perseverance. Brian shares his thoughts on preparation and persistence. A positive mind is a positive life and there is success through preparation - and knowing what to look for among groups and individuals can be a key component of success. Brian discusses negative forces and how they can disrupt the path to your goals, yet knowing and recognizing the negative can help push through to positivity. If we recognize manipulation, we can avoid it.
                    <p><a href="/members/webinars/?webinarid=141" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Introduction to the FILO Webinar Series
                        <br> featuring Brian Tooley</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Introduction to the FILO Webinar Series.
                    <br>
                    <br> Former Counter-Intelligence Special Agent Brian Tooley presents the F.I.L.O Webinar Series. Having served on a Tactical Human Team, which helps build relationships of trust, Brian learned what it means to be intentional about a positive result. In the first of the FILO Series of Webinars, Brian demonstrates that building success through positive thoughts and preparation can be the key to accomplishment. Download the FILO 5 Affirmations in the 'resources' section of this page.
                    <p><a href="/members/webinars/?webinarid=140" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">How To Connect To Increase Your Network One Link At A Time - Part 2
                        <br> featuring George Seybold</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In Part 2 of this informative GIN Webinar, Social Media Guru George Seybold shows you how to use social media to drive your business, create awareness and drive opportunity. Using the ultimate combination of business and social networking through LinkedIn and other social networking sites. Learn more about how to sell through your connections, rather than trying to sell directly to them. George teaches invaluable tips and tricks on how to draw attention and drive your business forward.
                    <p><a href="/members/webinars/?webinarid=136" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">How To Connect To Increase Your Network One Link At A Time - Part 1
                        <br> featuring George Seybold</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Social Media Guru George Seybold shows you how to use social media to drive your business, create awareness and drive opportunity. George focuses on a particular social media outet called LinkedIn.
                    <p><a href="/members/webinars/?webinarid=135" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/TroyMcClain_Webinar-5.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Action Plan For Success: Debt Management - Part 2
                        <br> featuring Troy McClain</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In Part 2 of this informative webinar, Troy McClain continues with how your credit score can directly effect debt management. Find your debt to income ratio and why it can be important. Learn the effective system for proper debt management and get on the path to financial success.
                    <p><a href="/members/webinars/?webinarid=133" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/TroyMcClain_Webinar-5.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Action Plan For Success: Debt Management - Part 1
                        <br> featuring Troy McClain</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Successful Businessman and Entrepreneur, Troy McClain shares some of the 'how', regarding debt management, in building your goals and dreams. Many of us are looking for a path to success, which should include proper debt management. In part 1 of this educational webinar, Troy teaches about the "3 Cs of Business: Credit, Capacity to repay and Collateral." Watch this webinar to reveal the importance of credit score and how to build yours to its ultimate peak.
                    <p><a href="/members/webinars/?webinarid=132" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/ChrisMcGarahan_FEAR_Webinar.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Fear And The Law Of Detachment - Part 2
                        <br> featuring Chris McGarahan</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    FEAR: False Evidence Appearing Real.
                    <br>
                    <br> Chris McGarahan continues this Webinar on Fear and the Law of Detachment. We need to accept that we can not do anything about our past. Most people look at their past with regret, resentment and fear. Learn the series of steps to move forward and detach yourself from the negativity of your past. The law of detachment from fear begins with asking, believing and receiving. Learn to address your fears and start to feel good about who you are. Remember that all of the things in your past added up to who you are and the opportunities that are presented to you today. After watching the Webinar Fear and the Law of Detachment Part 1, continue with part 2 of this informative and inspirational GIN Webinar.
                    <p><a href="/members/webinars/?webinarid=131" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/ChrisMcGarahan_FEAR_Webinar.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Fear And The Law Of Detachment - Part 1
                        <br> featuring Chris McGarahan</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    FEAR: False Evidence Appearing Real.
                    <br>
                    <br> We always want the Law of Attraction working in a positive manner. Often, the thing that stops you from getting what you want in life is summed up in one word: fear. Fear can stop us from getting what we want. The fastest way to get over fear is to address it head-on. Turn your fears into positives in your life. Have pen and paper handy for the lessons in overcoming fear in this powerful Webinar by Chris McGarahan.
                    <p><a href="/members/webinars/?webinarid=130" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule" src="/uploadedImages/Content/Members/Training/Webinars/SteveBeck_Webinar_2.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">The Importance of Goal Setting
                        <br> featuring Steve Beck</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Successful Author, Dynamic Speaker and Life Coach Steve Beck presents the GIN Webinar, The Importance of Goal Setting. Only 5% of people have goals in life, which means that 95% of you don't have goals. Setting goals gives you a map of where you are going to be in a month, or a year, or 5 years or more. Steve talks about why people don't have goals, what's holding you back, how to develop and achieve your goals. Download the Participants Guide and begin with Steve Beck, The Importance of Goal Setting.
                    <p><a href="/members/webinars/?webinarid=119" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule" alt="Upcoming Schedule - Morter" src="/uploadedImages/Content/Members/Training/Webinars/CoralGrant_Webinar.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">The Secret to Manifesting Money
                        <br> featuring Coral Grant</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Coral Grant, CEO and Founder of The Secret to Life Coaching delivers her secrets to manifesting money and dreams. Coral is a true example of using the GIN training to achieve what she wants in life. Coral shares the goals she has accomplished since becoming a member of GIN, walking us through her beautiful Colorado home and sharing the tools to her amazing success. Coral also introduces her close circle of friends - GIN Members, Nancy Ashley, Michele Brown, Brenda Maetzold and Terha Tullius and demonstrates how they are a positive support system for each other in life.
                    <p><a href="/members/webinars/?webinarid=118" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Michelle Arbeau" alt="Upcoming Schedule Michelle Arbeau" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMichelleArbeau.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">The Energy of Words
                        <br> featuring Michelle Arbeau</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Numerologist, author and inspirational speaker, Michelle Arbeau presents the GIN Webinar "Energy of Words". We think, speak and write in words and language. These words play a huge role in the life we create for ourselves. Words have started and ended wars, built and destroyed nations, created peace, saved lives, launched life-changing movements, etc.
                    <p><a href="/members/webinars/?webinarid=113" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Roger Seip" alt="Upcoming Schedule Roger Seip" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRogerSeip.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Train Your Brain for Success
                        <br> featuring Roger Seip</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In this session, you will learn to harness the incredible creative capacity on your mind and achieve your personal best-professionally, financially and personally. You will learn:
                    <ul>
                        <li>The ways in which your brain literally creates your experience- and your results</li>
                        <li>Your brains extremely Unhelpful default settings</li>
                        <li>How to overcome these default settings, and put you in charge of brain and your results</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=112" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Steve Beck" alt="Upcoming Schedule Steve Beck" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleSteveBeck.jpg" style="border-width: 0px; border-style: solid; width: 269px; height: 204px;" border="0" height="204" width="269"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Leave Your Funk at the Door
                        <br> featuring Steve Beck</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Steve will provide you the knowledge that helps you set the tone for every interaction. It will assist you in dealing with the everyday stress in the workplace and home life so that it becomes a positive experience. You will learn:
                    <ul>
                        <li>How your mood affects others in a positive or negative way</li>
                        <li>Seven Ways to 'Leave Your FUNK at the Door!'</li>
                        <li>The importance of tone and body language while communicating</li>
                        <li>An Army Ranger-Navy Seal technique to eliminate FUNK</li>
                        <li>How to 'Fire Up' every day regardless of the day of the week</li>
                        <li>To realize the importance of "Leaving Your FUNK at the Door"</li>
                        <li>The art of dream building</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=111" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 8 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 8, Part 2:
                    <ul>
                        <li>Picks up where he left off with the art of dream building
                        </li>
                        <li>Dream destroyers</li>
                        <li>Success illusions to avoid</li>
                        <li>Shares the formula to dream building</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=110" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 8 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 8, Part 1:
                    <ul>
                        <li>The art of dream building
                        </li>
                        <li>What material goals can do for you</li>
                        <li>There is a formula to dream building</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=109" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 7 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 7, Part 2:
                    <ul>
                        <li>More personality characteristics of successful people
                        </li>
                        <li>Nine interpersonal traits of success</li>
                        <li>Creating new neuropath ways</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=108" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 7 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 7, Part 1:
                    <ul>
                        <li>The science of success</li>
                        <li>Personality characteristics of successful people</li>
                        <li>Turning aptitude into skill</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=107" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 6 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 6, Part 2:
                    <ul>
                        <li>Picks up where he left off and continues to discuss traits of successful people</li>
                        <li>The importance of taking risks</li>
                        <li>Discover the power of self-control</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=106" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 6 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    The following topics are covered in Volume 6, Part 1:
                    <ul>
                        <li>Habits of success</li>
                        <li>How to manage your money</li>
                        <li>Why loyalty is so vital</li>
                        <li>Teaming with the right people</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=105" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 5 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In part 2 of Volume 5, Ron continues with the eight reasons for failure. He explains:
                    <ul>
                        <li>
                            Types of positive attitudes</li>
                        <li>
                            Attitudes that hold you back from success</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=104" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 5 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Volume 5 of this series recaps the previous Webinars and starts you on a new path of learning. In this volume, you will learn:
                    <ul>
                        <li>
                            Four simple things to focus on to achieve success</li>
                        <li>
                            Eight reasons for failure</li>
                        <li>
                            The importance of mental breaks</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=103" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 4 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Volume 4 of this series closes the mental toughness section of this teaching. You will learn:
                    <ul>
                        <li>
                            Why mental toughness is essential to success</li>
                        <li>
                            How to keep your perception positive</li>
                        <li>
                            What is meant by "Foundational Development" and how you can get it</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=102" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 4 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    As we begin Volume 4 of this incredible series, you'll be delighted as we move into the following:
                    <ul>
                        <li>
                            Why having mental toughness is so vital</li>
                        <li>
                            How to replace worry with the power of expectation</li>
                        <li>
                            Recognizing that you are a product of your choices</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=10" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 3 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Part 2 of this exciting webinar continues with Ron Ball's explanation of the important Success Mastery Course teachings. He discusses:
                    <ul>
                        <li>
                            The importance of treating others how you want to be treated</li>
                        <li>
                            The critical traits and thinking patterns you can use to increase your teachability index</li>
                        <li>
                            Why you should be a giver</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=39" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball"> &nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Explore the Essentials of the Mastery Course - Volume 3 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Ron Ball has been digging deeply into the fundamentals of GIN's training. In this session, he talk about the following:
                    <ul>
                        <li>The importance of humility</li>
                        <li>What is means to have a teachable attitude</li>
                        <li>Essential behaviors and thinking patterns to avoid</li>
                        <li>Why the way you treat others directly effects your teachability index</li>
                    </ul>
                    <p><a href="/members/webinars/?webinarid=38" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Ron Ball" alt="Upcoming Schedule Ron Ball" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Explore the Essentials of the Mastery Course - Volume 2 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Part 2 of this volume delves in the characteristics of what it takes to BE success. Ron Ball continues with:
                    <ul>
                        <li>The traits of a great mentor</li>
                        <li>Why having humility is a good thing</li>
                    </ul>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=59">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Ron Ball" alt="Upcoming Schedule Ron Ball" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Explore the Essentials of the Mastery Course - Volume 2 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Ron Ball recaps the classics of success mentioned in Volume 1. In this volume, he continues building and going in depth as he provides:</p>
                    <ul>
                        <li>More insider information about the Mastery Course</li>
                        <li>What "maneuverability" means in this context, and why it's important</li>
                        <li>And more!</li>
                    </ul>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=88">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Ron Ball" alt="Upcoming Schedule Ron Ball" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Explore the Essentials of the Mastery Course - Volume 1 - Part 2
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In Part 2 of this webinar, Ron Ball continues to build on the essentials of the GIN Mastery Course. He will share the following:</p>
                    <ul>
                        <li>Why enthusiasm makes a difference</li>
                        <li>Why its important to have passion for your benefits.</li>
                        <li>And more!</li>
                    </ul>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=61">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Ron Ball" alt="Upcoming Schedule Ron Ball" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Explore the Essentials of the Mastery Course - Volume 1 - Part 1
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Join Ron Ball in this incredible new series as he delves deep into the GIN Mastery Course.
                    <br>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=89">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/CinthiaMoura_webinar.jpg" alt="Cinthia Moura" title="Cinthia Moura"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Body Language
                        <br> featuring Cinthia Moura</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Join GIN's own Cinthia Moura as she tells her humble transformation story from being a shy girl living in Brazil to hitting the runways of Milan! Cinthia's dream was to become a fashion model and her journey for a better life will inspire you and warm your heart.
                    <br>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=87">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Generic.jpg" alt="Upcoming Schedule Generic" title="Upcoming Schedule Generic"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Explore Social Networking
                        <br> featuring Jim Spellos</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Building your relationships in person is the best way to network but in today's fast paced technological world, why not expand your social horizon? And technology expert Jim Spellos can navigate you on how to use those really cool new phones and social networks!
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=41">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Food and Wine" alt="Upcoming Schedule Food and Wine" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-FoodWine.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Sommelier Secrets to Pairing Wine &amp; Food
                        <br> featuring Jay Youmans </div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In this webinar, Advanced Level Sommelier and Certified Wine Educator, Jay Youmans (Master of Wine), reveals the secrets to selecting the best wines with food. Jay's number one rule in matching food and wine – "the match only works if the person likes the wine and the food."
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=37">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Eileen Fitzpatrick" alt="Upcoming Schedule Eileen Fitzpatrick" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-EileenFitzpatrick.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Feng Shui Essentials
                        <br> featuring Eileen M. Fitzpatrick</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Multi-disciplined Holistic Healing Arts Practitioner and Certified Feng Shui Consultant and teacher, Eileen M. Fitzpatrick, will take you through the ancient art of Feng Shui. She will show you how it can work wonders in your life.
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=57">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Al Carter" alt="upcoming Schedule - Al Carter" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-AlCarter.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Ultimate Exercise
                        <br> featuring Al Carter</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In this informative Webinar, Al Carter Share the amazing benefits of "Rebounding." If you haven't already, you'll want to get in on this fun way to work out and stay healthy!
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=97">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Troy McLain" alt="upcoming Schedule - Troy McLain" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-TroyMcLain.jpg"> &nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Looking Beyond the Obvious
                        <br> featuring Troy McClain</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Join Troy McClain, from the first season of The Apprentice, demonstrates how you can achieve greatness, no matter what the circumstances are in your life!
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=96">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Joanne Callahan" alt="Upcoming Schedule Joanne Callahan" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleJoanneCallahan.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Eliminating Self Sabotage
                        <br> featuring Joanne Callahan</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In this webinar, Joanne Callahan is going to teach you all about how to use Thought Field Therapy® (TFT), also known as "tapping." Joanne Callahan is the co-developer of this incredible technique and she is going to teach you how to use it to help you balance your body's energy system within minutes.
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=95">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Beymer" alt="Upcoming Schedule - Beymer" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Being Happy With You
                        <br> featuring Lee Beymer </div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Lee Beymer gives an introduction to Quantum Emotional Clearing (QEC). He is going to teach you how to live a happy, fulfilling life by enjoying your life with your closest companion --- YOURSELF!
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=94">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Vining" alt="Upcoming Schedule - Vining" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEVining.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Twelve Characteristics of Winners
                        <br> featuring Earlene Vining</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    Earlene Vining shares inspiring insights on how you can create a well rounded and balanced life. You'll find yourself applying these tips in every facet of your life.
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=93">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Why You Do What You Do
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    In this Webinar, Ron Ball reveals the science behind our personal characteristics. If you've ever wondered why you have certain personality traits or do things that you can't even explain, this Webinar will give some insights.
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=92">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Palkhivala" alt="Upcoming Schedule - Palkhivala" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Palkhivala.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Secret Power of Love of Manifestation
                        <br> featuring Aadil Palkhivala</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        Yoga Master Aadil Palkhivala shares incredible insights about the amazing power of love, and how it relates to manifestation. And who wouldn't want to be able to manifest the right things; especially when they come from a place of love!</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=91">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller(1).jpg" alt="Upcoming Schedule Dr Theresa Dale" title="Upcoming Schedule Dr Theresa Dale">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Reducing Your Biological Age
                        <br> featuring Dr. Theresa Dale</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        Dr. Theresa Dale shares how to use various tips and tricks to stay happy and healthy every day. The results can have a lasting effect that reverse the aging process.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=90">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller.jpg" alt="Upcoming Schedule Mary Miller" title="Upcoming Schedule Mary Miller">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Changing Your Patterns, Changing Your Life
                        <br> featuring Mary Miller</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        It's incredible how Mary Miller uses ancient knowledge and technology in the form of I Ching Systems to help people change the quality of their lives today! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=86">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-EdForeman.jpg" alt="upcoming Schedule - Ed Foreman" title="upcoming Schedule - Ed Foreman">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Turn Worry and Stress Into Happiness and Success
                        <br> featuring Ed Foreman</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        Ed Foreman's contagious energy comes through loud and clear in this pleasant Webinar presentation. You'll see how you can take all your worries and stresses and turn them around for good.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=85">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Robert Allen" alt="upcoming Schedule - Robert Allen" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-RAllen.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Multiple Streams of Income - How to Generate a Lifetime of Unlimited, Enlightened Wealth - Part 3
                        <br> featuring Robert Allen</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>
                        In this Webinar, Robert Allen continues his seminar on gaining financial success. Robert Allen has written ten books on financial success and has helped thousands of individual investors become millionaires in his 30 year career. He is a popular television and radio guest, appearing on hundreds of radio and television programs including Good Morning America, Regis Philbin, and Larry King. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=84">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg" alt="Upcoming Schedule - Beymer" title="Upcoming Schedule - Beymer">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Transform Your Life! Part 2
                        <br> featuring Lee Beymer</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>
                        In Part 2 of this Webinar, Lee Beymer, founder of Quantum Emotional Clearing, continues teaching the steps needed to live a healthy, clear physical and emotional life.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=83">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Al Carter" alt="upcoming Schedule - Al Carter" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-AlCarter.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Miracles of Exercise: Rebound Your Way to Health and Fitness - Part 2
                        <br> featuring Al Carter</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        In this informative Webinar, Al Carter continues to share the amazing benefits of "Rebounding." If you haven't already, you'll want to get in on this fun way to work out and stay healthy!</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=82">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Robert Allen" alt="upcoming Schedule - Robert Allen" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-RAllen.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Multiple Streams of Income, Part 2 - Section 1
                        <br> featuring Robert Allen</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>
                        In this Webinar, Robert Allen continues his seminar on gaining financial success. Robert Allen has written ten books on financial success and has helped thousands of individual investors become millionaires in his 30 year career. He is a popular television and radio guest, appearing on hundreds of radio and television programs including Good Morning America, Regis Philbin, and Larry King.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=80">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleDanielPostSenning.jpg" alt="Upcoming Schedule Daniel Post Senning" title="Upcoming Schedule Daniel Post Senning">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Personal Skills for Professional Success, 1
                        <br> featuring Daniel Post Senning</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>
                        Daniel Post Senning is the great-great-grandson of Emily Post; the original etiquette expert! In this Webinar, Daniel provides etiquette details on using the strengths of your own personal skills in order to create professional success.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=79">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-GlynisMcCants.jpg" alt="upcoming Schedule - Glynis McCants" title="upcoming Schedule - Glynis McCants">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Numerology! Part 2
                        <br> featuring Glynis McCants</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>
                        In this Webinar, Glynis goes deeper into the meaning of numbers and the amazing impact they have on your life. She builds on how they influence your personality traits, what financial implications they possess, how your love life is affected and much more.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=78">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller(1).jpg" alt="Upcoming Schedule Dr Theresa Dale" title="Upcoming Schedule Dr Theresa Dale">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Healing the Thyroid
                        <br> featuring Dr. Theresa Dale</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>
                        In this groundbreaking Webinar, Dr. Theresa Dale shares explosive medical news about healing the thyroid gland naturally. She fully covers the thyroid's function as well as the risks and major illnesses that are directly related to it.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=77">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Beymer" alt="Upcoming Schedule - Beymer" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Transform Your Life! Part 1
                        <br> featuring Lee Beymer</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>If you've ever wanted to take full control of your life and guide the direction of your future, this is must see Webinar. Join Lee Beymer, founder of Quantum Emotional Clearing, as he teaches you the steps you need to take to live a healthy and clear, physical and emotional life. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=76">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Palkhivala.jpg" alt="Upcoming Schedule - Palkhivala" title="Upcoming Schedule - Palkhivala">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Releasing Stress and Building Energy Through Purna Yoga
                        <br> featuring Aadil Palkhivala</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Yoga Master Aadil Palkhivala's live classes are tough to get into. But when you watch this Webinar, it will be as though you're right in the class! You will study with the world's most famous Purna Yoga teacher as he teaches the basics of alignment, posture, stress-releasing breathings, upper-body stress release (neck and shoulders), and Purna Yoga's Morning Series. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=75">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEVining.jpg" alt="Upcoming Schedule - Vining" title="Upcoming Schedule - Vining">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Daily Techniques to Assure a Successful Life of Health, Wealth and Happiness
                        <br> featuring Earlene Vining</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Earlene Vining will delight you in this Member Webinar. Join her as she guides you on a journey to help you fulfill your dreams. Earlene will inspire you as well as give you real tools that you can use to launch you from where you are, to where you want to be!</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=74">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Troy McLain" alt="upcoming Schedule - Troy McLain" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-TroyMcLain.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Beyond the Obvious, Part 2
                        <br> featuring Troy McClain</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In Part 2 of this developmental Webinar, Troy McClain will continue to inspire you as you as he demonstrates how you can achieve greatness, no matter what the circumstances are in your life!</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=73">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Robert Allen" alt="upcoming Schedule - Robert Allen" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-RAllen.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Multiple Streams of Income - How to Generate a Lifetime of Unlimited, Enlightened Wealth - Part 1
                        <br> featuring Robert Allen</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>If you've ever wanted to have more money coming into your bank account, then this is a must see Webinar! Our host, Robert Allen, has written ten books on financial success and has helped thousands of individual investors become millionaires in his 30 year career. He is a popular television and radio guest, appearing on hundreds of radio and television programs including Good Morning America, Regis Philbin, and Larry King. Now Robert will share his cutting edge secrets with you!</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=72">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMiltonMorter.jpg" alt="Upcoming Schedule Dr Milton Morter" title="Upcoming Schedule Dr Milton Morter">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Law of Attraction/Distraction
                        <br> featuring Dr. Milton Morter</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>You've heard of the "Law of Attraction" but did you know there is also a "Law of Distraction?" And you may be using it to keep what you want from coming into your life. In this Webinar, Dr. Milton Morter is going to cover both of these laws and you will be amazed at what you learn. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=71">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleTedMorter.jpg" alt="Upcoming Schedule Dr Ted Morter" title="Upcoming Schedule Dr Ted Morter">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Six Essentials for Life
                        <br> featuring Dr. Ted Morter III</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In this captivating Webinar, Dr. Ted discloses every day techniques that you can use to help improve your life. He'll not only introduce you to them, but he'll explain, in detail, how you can apply them. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=70">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Palkhivala.jpg" alt="Upcoming Schedule - Palkhivala" title="Upcoming Schedule - Palkhivala">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Harnessing Solar Energy to Release Blockages in Purna Yoga
                        <br> featuring Aadil Palkivala</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Yoga Master Aadil Palkhivala leads an exclusive, live class of GIN members in the famous Surya Namaskar (Sun Salutation), as taught only in Purna Yoga, and customized for GIN. The traditional version of this famous practice is over 2,500 years old. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=69">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleJoanneCallahan.jpg" alt="Upcoming Schedule Joanne Callahan" title="Upcoming Schedule Joanne Callahan">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Advanced Tapping
                        <br> featuring Joanne Callahan</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>In this Webinar, Joanne Callahan, the co-developer of Thought Field Therapy® (TFT), also known as "tapping" goes deeper into the benefits of this incredible technique. She will teach you how to use tapping to help you balance your body's energy system within minutes. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=68">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Vining" alt="Upcoming Schedule - Vining" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEVining.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Twelve Characteristics for Improving Personal, Family and Business Growth
                        <br> featuring Earlene Vining</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Using her delightful quiet Southern confidence, Earlene Vining shares inspiring insights about how to create a well rounded and balanced life. You'll find yourself applying these tips in every facet of your life. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=67">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Troy McLain" alt="upcoming Schedule - Troy McLain" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-TroyMcLain.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Beyond the Obvious
                        <br> featuring Troy McClain</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Join Troy McClain, from the first season of The Apprentice, as he demonstrates how you can achieve greatness, no matter what the circumstances are in your life! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=66">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Palkhivala.jpg" alt="Upcoming Schedule - Palkhivala" title="Upcoming Schedule - Palkhivala">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Power of Love and How it Makes Your Dreams Come True
                        <br> featuring Aadil Palkhivala</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>In this must-see Webinar, Yoga Master Aadil Palkhivala shares with you the great Purna Yoga Meditation secrets of mastering the mind and focusing it. This technique is especially critical for those who believe they cannot still their minds. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=65">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg" alt="Upcoming Schedule - Beymer" title="Upcoming Schedule - Beymer">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Five Steps to Success
                        <br> featuring Lee Beymer</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Lee Beymer has developed a method where you can use these five steps to create success in your life. No matter what you define as success, now you can use these principles and design your future. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=64">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleValBiktashev.jpg" alt="Upcoming Schedule Val Biktashev" title="Upcoming Schedule Val Biktashev">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Feng Shui Principals for Your House
                        <br> featuring Feng Shui Master Val Biktashev</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Master Val Biktashev and his student, Terha Gottal, will show you how to create the ultimate living space through Feng Shui. If you don't know anything about Feng Shui, you'll be pleasantly surprised at how this ancient system, based upon the observation of heavenly time and earthly space, influences your home and your life. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=63">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="upcoming Schedule - Glynis McCants" alt="upcoming Schedule - Glynis McCants" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-GlynisMcCants.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Numerology! Part 1
                        <br> featuring Glynis McCants</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Did you know that your name and birth date hold the blueprint of who you really are?
                        <br>
                        <br>They reveal if you are independent with a strong desire to be #1 or if you seek fun, adventure, and freedom! The blueprint shows if you're the type of person who prefers to be alone and have a love of nature, or if you're a natural born leader.
                        <br>
                        <br>These and other characteristics are ingrained in the 2,500 year old science of numbers. And, it can actually help you have a more fulfilling life. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=62">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-AlCarter.jpg" alt="upcoming Schedule - Al Carter" title="upcoming Schedule - Al Carter">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Miracles of Exercise: Rebound Your Way to Health and Fitness - Part 1
                        <br> featuring Al Carter</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In this informative Webinar, get ready to learn all about an effective form of health and fitness called "Rebounding." It's also fun, easy, convenient and safe. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=60">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Beymer" alt="Upcoming Schedule - Beymer" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Seven Levels of Consiousness
                        <br> featuring Lee Beymer</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Where do the thoughts you think each moment come from? Why do the same thoughts circulate, over and over? How can these thoughts be directed? Can the mind be trained into thinking a certain way? </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=58">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Daniel Post Senning" alt="Upcoming Schedule Daniel Post Senning" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleDanielPostSenning.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Personal Skills for Professional Success, II
                        <br> featuring Daniel Post Senning</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Daniel Post Senning, great-great-grandson of Emily Post and etiquette expert, continues to show us how our personal skills can create professional success.</p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=56">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller.jpg" alt="Upcoming Schedule Mary Miller" title="Upcoming Schedule Mary Miller">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Are Your Beliefs Holding You Back?
                        <br> featuring Mary Miller</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Did you know that the things you believe are tied to your own sense of self-worth? It's true! In fact, the things you were taught by the age of 10 are a function of how you view your relationships, your job potentials, your success capacity, and pretty much everything you do. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=55">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Palkhivala" alt="Upcoming Schedule - Palkhivala" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedule-Palkhivala.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Whose Dreams Are You Dreaming? An Introduction to Manifestation
                        <br> featuring Aadil Palkhivala</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In this provocative Webinar, Yoga Master Aadil Palkhivala explains, through the ancient and powerful Purna Yoga Philosophy, why our dreams often do not manifest even when we visualize, dream, work, and try to do all the right things. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=54">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Dr Theresa Dale" alt="Upcoming Schedule Dr Theresa Dale" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller(1).jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Everything You Have heard About Aging is Wrong!
                        <br> featuring Dr. Theresa Dale</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>If you think you have a pretty good idea about how to handle your health as you age, you may want to re-think what you've been taught. If you are ready to take control of the aging process …
                        <br>
                        <br>… you are going to love the information Dr. Dale presents in this webinar! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=53">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleTedMorter.jpg" alt="Upcoming Schedule Dr Ted Morter" title="Upcoming Schedule Dr Ted Morter">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">It's All About Energy
                        <br> featuring Dr. Ted Morter, III</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>The more you know about energy, and how you can use it, the more likely it is that you'll begin to see improvements in your life. Dr. Ted Morter is going to help you navigate your way through these vibrations so you can get a better understanding of how energy works in your life. </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=52">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Ron Ball" alt="Upcoming Schedule Ron Ball" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Linguistic Psychology
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>The spoken word is the most powerful force we use to communicate and what we say, may not always be what we intend. Join us as Ron Ball explores the magical connection between your mind and the way you communicate. You will be surprised at what you'll learn! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=51">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Jaunita Ecker" alt="Upcoming Schedule Jaunita Ecker" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleJuanitaEcker.jpg">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Art of Business Dining
                        <br> featuring Juanita Ecker</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>In this delightful Webinar, image expert Juanita Ecker shows us the dos and don'ts of dining in business. Simple s it may seem, you'll be surprised by some of these "need to know" tips! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=49">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMiltonMorter.jpg" alt="Upcoming Schedule Dr Milton Morter" title="Upcoming Schedule Dr Milton Morter">&nbsp;</td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">Advanced Brain Concepts
                        <br> featuring Dr. Milton Morter</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Join Dr. Milton Morter as he explores the mysteries of the brain, in this eye-opening presentation. Not only will you learn secrets of the brain, but you'll also gain a brighter understanding of what makes you tick.
                        <br>
                        <br>And if understanding isn't enough, you'll also learn how to take this information and actually apply it to change your future! </p>
                    <p><a class="btn btn-mini" href="/members/webinars/?webinarid=48">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Vining" alt="Upcoming Schedule - Vining" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEVining.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Pebbles, Rocks and Boulders
                        <br> featuring Earlene Vining</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Earlene Vining has been inspiring audiences across the country for years. Using her hard-hitting strategies and delightful stories, she demonstrates how life is bountiful, and whatever your dreams may be, they are within your own reach!
                        <br>
                        <br>She reminds us that sometimes we don't allow ourselves to attain our dreams because of past events or circumstances that may hold us back. Earlene calls this baggage. And, she is going to teach you how to get rid of it, once and for all! </p>
                    <p><a href="/members/webinars/?webinarid=47" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleLeeBeymer.jpg" alt="Upcoming Schedule - Beymer" title="Upcoming Schedule - Beymer">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Quantum Awakening
                        <br> featuring Lee Beymer</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Live the life you are fully capable of living! In this webinar, you will learn how to clear out any negative energy that may be holding you back. Through a technology called Quantum Emotional Clearing, or QEC, Lee Beymer will show you how to overcome lifetime fears in order to move forward and experience a clearer and calmer way of living.
                        <br>
                        <br>Since you are responsible for your own energy, you have it within yourself to make any of the changes you desire. When you make QEC a regular practice, you'll begin to see those changes. </p>
                    <p><a href="/members/webinars/?webinarid=46" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule - Patenaude" alt="Upcoming Schedule - Patenaude" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingSchedulePatenaude.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Ignite the Force
                        <br> featuring Jacques Patenaude</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Join Jacques Patenaude as he shares his incredible fighting art philosophy and shows you how it can be applied to every area of your life. Jacques isn't just going to teach you how to defend yourself; he is going to show you how to use your mind's power to gain control and direction of your life. </p>
                    <p><a href="/members/webinars/?webinarid=45" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleJoanneCallahan.jpg" alt="Upcoming Schedule Joanne Callahan" title="Upcoming Schedule Joanne Callahan">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Tapping Away Fear and Negativity
                        <br> featuring Joanne Callahan</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>What if you could instantly conquer any anxiety, fear, and emotional distress in a quick and painless way? Now you can, by learning to use Thought Field Therapy® (TFT), also known as "tapping."
                        <br>
                        <br>Joanne Callahan is the co-developer of this incredible technique and she is going to teach you how to use it to help you balance your body's energy system within minutes. </p>
                    <p><a href="/members/webinars/?webinarid=44" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller.jpg" alt="Upcoming Schedule Mary Miller" title="Upcoming Schedule Mary Miller">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">The Electromagnetics of 2012
                        <br> featuring Mary Miller</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Have you ever heard of "Full Moon Madness?" In case you haven't, it's an expression used to describe how a full moon effects and influences careless or reckless behavior on people.
                        <br>
                        <br>It's a well documented reality that the movement and phases of the moon, and other planets, have "electromagnetic" effects on earth. During the period of a full moon, there are increased incidences in violent behavior, higher arrests rates, more traffic accidents as well as other intense, unexplainable occurrences. </p>
                    <p><a href="/members/webinars/?webinarid=43" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Leadership Laboratory
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Ron Ball has worked with some of the most incredible leaders in this country! Now, he is going to share with you what you can do to BE a leader.
                        <br>
                        <br>At some point in your life, you'll probably be placed into a situation where you're going to require leadership skills. Wouldn't it be great to know exactly what to do to succeed? </p>
                    <p><a href="/members/webinars/?webinarid=35" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMiltonMorter.jpg" alt="Upcoming Schedule Dr Milton Morter" title="Upcoming Schedule Dr Milton Morter">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Nutrition and Your Health
                        <br> featuring Dr. Milton Morter</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>For years, Dr. Milton Morter has been empowering individuals with his amazing insight on how the body functions. Now, you have the incredible opportunity to learn about why proper nutrition is the foundation to good health from this leading edge health care expert! </p>
                    <p><a href="/members/webinars/?webinarid=29" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Val Biktashev" alt="Upcoming Schedule Val Biktashev" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleValBiktashev.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Feng Shui and You
                        <br> featuring Master Val Biktashev</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Join Master Val Biktashev as he and his student, Terha Gottal, introduce you to the astonishing world of Feng Shui. If you don't know anything about Feng Shui, you'll be pleasantly surprised at how this ancient system, based upon the observation of heavenly time and earthly space, influences your life. </p>
                    <p><a href="/members/webinars/?webinarid=23" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Eric P" alt="Upcoming Schedule Eric P" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEricP.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">The Anatomy of Results
                        <br> featuring Eric Plantenberg</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Eric Plantenberg's infectious positive attitude comes through loud and clear in this incredible webinar. He'll inspire you to live your life with positive results being the goal. </p>
                    <p><a href="/members/webinars/?webinarid=22" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Dr Ted Morter" alt="Upcoming Schedule Dr Ted Morter" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleTedMorter.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Live Your Dreams - Get Your Goals
                        <br> featuring Dr. Ted Morter</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Using delightful personal stories, Dr. Ted shares that you too can manifest everything you want in life. Creating abundance is a matter of dreaming it and getting it! He reaffirms that you don't have to worry about the how. </p>
                    <p><a href="/members/webinars/?webinarid=18" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Dr Theresa Dale" alt="Upcoming Schedule Dr Theresa Dale" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller(1).jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">What You Need To Know About Hormone Testing
                        <br> featuring Dr. Theresa Dale</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Based on her 30+ years of expertise, Dr. Theresa Dale shares this "must know" important information about hormone testing and how it can help you. </p>
                    <p><a href="/members/webinars/?webinarid=17" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleRonBall.jpg" alt="Upcoming Schedule Ron Ball" title="Upcoming Schedule Ron Ball">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Fearless Living
                        <br> featuring Ron Ball</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>The Global Information Network is pleased to invite you to an incredible webinar that will help you conquer any fears or phobias! Ron Ball has a method that is tried and true - successfully tested on thousands of people all over the world. </p>
                    <p><a href="/members/webinars/?webinarid=158" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleTedMorter.jpg" alt="Upcoming Schedule Dr Ted Morter" title="Upcoming Schedule Dr Ted Morter">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Thoughts Are Things
                        <br> featuring Dr. Ted Morter</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>Join Dr. Ted as he explores and explains how the thoughts circulating around in your brain have an effect on yourphysical body. He'll guide you through the area of the brain where thoughts live and how negative ones bring youinto a "danger zone." He also shows you how to steer clear of the type of thinking that gets you there. </p>
                    <p><a href="/members/webinars/?webinarid=14" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Mary Miller" alt="Upcoming Schedule Mary Miller" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">It's All About Relationships
                        <br> featuring Mary Miller</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Every time you interact with another person, verbal or non-verbal information is being exchanged. Some information is obvious and direct. Some is subtle but often more powerful than the words being spoken. Success in life, in business and in your personal relationships is a function of what and how you communicate. Now you can know the secrets to successful, balanced relationships. </p>
                    <p><a href="/members/webinars/?webinarid=13" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Daniel Post Senning" alt="Upcoming Schedule Daniel Post Senning" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleDanielPostSenning.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Dining Etiquette
                        <br> featuring Daniel Post Senning</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>In this delightful webinar, Daniel Post Senning, the great-great-grandson of Emily Post, shares dining etiquette standards that have stood the test of time as well as new etiquette practices. </p>
                    <p><a href="/members/webinars/?webinarid=12" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Eric P" alt="Upcoming Schedule Eric P" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleEricP.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Feeling On Top Of The World
                        <br> featuring Eric Plantenberg</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Join us for this inspiring webinar as Eric Plantenberg shares his personal quest to climb Mount Everest. Eric's infectious positive attitude will motivate and energize you. If you have any doubts about achieving your dreams and goals, they will be cast aside after this presentation! </p>
                    <p><a href="/members/webinars/?webinarid=8" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller(1).jpg" alt="Upcoming Schedule Dr Theresa Dale" title="Upcoming Schedule Dr Theresa Dale">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Natural Medical Secrets You've Always Wanted To Know
                        <br> featuring Dr. Theresa Dale</div>
                    <p><strong>Access Level:</strong> Level IV Members &amp; Above</p>
                    <p>This information packed webinar provides you with everything you need to know about homeopathy by natural health expert, Dr. Theresa Dale! </p>
                    <p><a href="/members/webinars/?webinarid=19" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Mary Miller" alt="Upcoming Schedule Mary Miller" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleMaryMiller.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">You and Your Destiny: Have You Found Your Path?
                        <br> featuring Mary Miller</div>
                    <p><strong>Access Level:</strong> Level III Members &amp; Above</p>
                    <p>Mary Miller will delight you in this presentation that shows you how to bring yourself back to your natural state of happiness. You'll learn how to uncover, and use, your inherent gifts and resources to set yourself on the path you're destined to live! </p>
                    <p><a href="/members/webinars/?webinarid=42" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Jaunita Ecker" alt="Upcoming Schedule Jaunita Ecker" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleJuanitaEcker.jpg">&nbsp;</td>
                <td>
                    <div class="title5" style="padding-bottom: 7px;">Dress For Success
                        <br> featuring Jaunita Ecker</div>
                    <p><strong>Access Level:</strong> Level II Members &amp; Above</p>
                    <p>Join image expert, Juanita Ecker, as she explores the world of business etiquette and how the way you dress impacts your relationships. You'll learn what's appropriate in today's professional world as well as how you have total control over how you are perceived. This is a MUST SEE webinar for anyone who wants to improve upon their professional and personal image. You will be amazed and inspired by the impact it will make on your life! </p>
                    <p><a href="/members/webinars/?webinarid=40" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div class="well">
    <table border="0" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td valign="top" width="300"><img title="Upcoming Schedule Dr Ted Morter" alt="Upcoming Schedule Dr Ted Morter" src="/uploadedImages/Content/Members/Training/Webinars/UpcomingScheduleTedMorter.jpg"> &nbsp;
                </td>
                <td>
                    <div style="padding-bottom: 7px;" class="title5">The Body Heals Itself
                        <br> featuring Dr. Ted Morter, III</div>
                    <p><strong>Access Level:</strong> Level I Members &amp; Above</p>
                    <p>Dr. Ted introduces incredible healing concepts that will amaze you in this brand new Global Information Network webinar. He will show you a technique that can change your physiology (body) and how to apply it to your life. </p>
                    <p><a href="/members/webinars/?webinarid=36" class="btn btn-mini">View Webinar</a>&nbsp;</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>

EOT;


?>

<?php

        /*$htmlinput = str_replace("<br />", " || ", $htmlinput);
        $htmlinput = str_replace("<br>", " || ", $htmlinput);*/

        include_once '../components/shared/controllers/common.php';

        $date =  strtotime('now');

        $doc = new DOMDocument();
        $doc->loadHTML($htmlinput);
        $classname="well";
        $finder = new DomXPath($doc);
        $content = $finder->query("//*[contains(@class, '$classname')]");
              

        //$all = array();

        $i = 0;
        $j = 0;
        var_dump($content);
        $links = array();
        foreach($content as $item) {
          $j++;
          // $i++;
         
              
            $wbid = $item->getElementsByTagName('a')[0]->getAttribute('href');
            $img = $item->getElementsByTagName('img')[0]->getAttribute('src');
            $id = explode('=',$wbid);
            $alllinks = array_push($links,$id[1]); 

            $db = JFactory::getDBO();

            $query = $db->getQuery(true);
    
            $conditions = array(
                $db->quoteName('WebinarID') . '=' . (int)$id[1]
                
            );
            
            $query
                ->select('*')
                ->from($db->quoteName('GINWebinars_temp'))
                ->where($conditions)
                ->setLimit(1);
            
            $db->setQuery($query);

            $data = new stdClass();

            $rows = $db->loadObjectList();
            foreach($rows as $row) {
              $brand = $row->brand;
              $data->source = 'images/videos/webinar/'.$row->VideoSource;
              $data->title = $row->WebinarTitle;
              $data->speaker  = $row->Presenter;
              $data->created_on  = $row->CreatedOn;
              $data->description  = $row->Description;
              $data->resources  = $row->Resources;
            }

           
           
            $data->id = "";
            $data->ordering = (0-$j); 
            $data->media_type  = "video";
            $data->category = 7;
            $data->subcategory  = 0;
            $data->ad_source = "";
            $data->ad_link  = "";
            $data->brand  = "gin";
            $data->featured  = "";
            $data->featured_home  = "";
            $data->playlist  = "";
            $data->interactive_source = ""; 
            $data->thumbnail_image  = "images".$img;
            $data->cover_image  = "";
            $data->access = 10;
            $data->go_live_date = '2014-11-08 00:00:00';
            $data->archive_date = '0000-00-00 00:00:00';
            
           
            $data->columns = "";
            $data->tags  = "";
            $data->language = "english";


            $db->insertObject("axs_gin_media", $data); 

            //var_dump($alllinks);
          }
          //var_dump($link);
        /*foreach($content as $item) {
            $i++;   
            if ($i == 1) {      
                $all[$j] = new stdClass;

                $numDigits = strlen((string)($j + 1));
                $text = $item->textContent;

                $divider = " || ";
                $dividerLength = strlen($divider);

                $middle = strpos($text, $divider);

               // $lesson = substr($text, $numDigits + 2, $middle - ($numDigits + 2));
                $lessonArray = explode(" || ",$text); $lesson = $lessonArray[0];
                $teacher = substr($text, $middle + $dividerLength);

                
                $all[$j]->lesson = $lesson;
                $all[$j]->teacher = trim($teacher);

                $j++;

            } else {
                if ($i == 3) {
                    $i = 0;
                }
            }   
        }

        $content = $doc->getElementsByTagName('a');

        $i = 0;
        foreach ($content as $item) {

            $folder = $item->getAttribute('alias');

            if ($folder == '') {
                $folder = '/' . $item->getAttribute('href');
                $type = 1;
            } else {
                $type = 0;
            }



            $all[$i]->folder = $folder;
            $all[$i]->fileType = $type;

            $file = strrchr($folder, "/");
            $file = substr($file, 1);

            $all[$i]->file = $file;

            $i++;
        }

        echo var_dump($all);
        //return;

        $db  = JFactory::getDBO();

        $columns = array(
            'title',
            'slug',
            'description',
            'short_description',
            'access_level',
            'teacher_materials',
            'section',
            'ordering',
            'splms_course_id',
            'lesson_type',
            'enabled',
            'created_by',
            'created_on',
            'modified_by',
            'modified_on',
            'locked_by',
            'locked_on',
            'required_declarations',
            'required_inspirations',
            'required_gratitudes',
            'language'
        );

        for($i = 0; $i < count($all); $i++) {

            $query = $db->getQuery(true);                                    

            $title = $all[$i]->lesson;

            $slug = str_replace(" ", "-", strtolower($all[$i]->lesson));
            $slug = str_replace("---", "-", $slug);
            $slug = str_replace("--", "-", $slug);
            $slug = preg_replace("/[^a-zA-Z0-9\-]/", "", $slug);

            $long_description = $title;
            $short_description = $title;
            $access_level = 6;

            $teacher_num = -1;

            $teacher_extra = '';
            $setTeacher = strtolower($all[$i]->teacher);
            $teacher = str_replace("presented by ", "", $setTeacher);
            echo $teacher;
            echo ("\n");
            switch ($teacher) {
                case "kevin trudeau": $teacher_num = 4; break;
                case "chris mcgarahan": $teacher_num = 23; break;
                case "ron ball": $teacher_num = 5; break;
                case "ed foreman": $teacher_num = 6; break;
                case "les brown": $teacher_num = 7; break;
                case "blaine athorn":$teacher_num = 8; break;
                case "dr. john gray":
                case "john gray": $teacher_num = 9; break;
                case "dr. ted morter": $teacher_num = 10; break;
                case "mary miller": $teacher_num = 11; break;
                case "daniel post senning": $teacher_num = 12; break;
                case "brett deimler": $teacher_num = 13; break;
                case "jaunita ecker": $teacher_num = 14; break;
                case "peter ragnar": $teacher_num = 15; break;
                case "dr. theresa dale": $teacher_num = 16; break;
                case "dave pitcock": $teacher_num = 17; break;
                case "barb pitcock": $teacher_num = 18; break;
                case "earl nightingale": $teacher_num = 19; break;
                case "dr. callahan": $teacher_num = 21; break;

                default: $teacher_num = 20; $teacher_extra = $all[$i]->teacher; break;
            }
            //tabs: training  instructions   tests additional webinars resources


              $course_id = 9;
            $section = "training";
            $language = "english";

             if (preg_match('/(\.MP3|\.mp3)$/i', $file)) {
                //MP3
                if($language == 'french') {
                $file = html_entity_decode(utf8_decode($all[$i]->file));
                } else { $file = $all[$i]->file; }

                $audio_file = 'images/media/MP3/SanctionedSpeakers/' . $file;                
                $video_file = '';
                echo ($audio_file);
            } else {
                
                 if($language == 'french') {
                $file = html_entity_decode(utf8_decode($all[$i]->file));
                } else { $file = $all[$i]->file; }

                $audio_file = '';                
                $video_file = 'images/videos/sanctionedspeakers/' . $file;
                echo ($video_file);
            }
            echo ("\n");

          

            if($language == 'french') {
            $title = html_entity_decode(utf8_decode($title));
            $slug = html_entity_decode(utf8_decode($slug));
            $long_description = html_entity_decode(utf8_decode($long_description));
            $short_description = html_entity_decode(utf8_decode($short_description));
            

        }

            $materials = new stdClass;
        
            $materials->splms_teacher_id = array();
            $materials->teacher_multiple = array();
            $materials->audio_file = array();
            $materials->audio_duration = array();
            $materials->video_file = array();
            $materials->video_duration = array();
            $materials->pdf_file = array();
            $materials->video_thumb = array();

            array_push($materials->splms_teacher_id, (string)$teacher_num);
            array_push($materials->teacher_multiple, (string)$teacher_extra);            
            array_push($materials->audio_file, $audio_file);
            array_push($materials->audio_duration, "");
            array_push($materials->video_file, $video_file);
            array_push($materials->video_duration, "");
            array_push($materials->pdf_file, "");
            array_push($materials->video_thumb, "");

            if($language == 'french') {
            $teacher_materials = json_encode($materials, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        } else {
            $teacher_materials = json_encode($materials);
        }
            //echo ($teacher_materials . "\n\n");
            
           
            $values = array(
                $db->quote($title),
                $db->quote($slug),
                $db->quote($long_description),
                $db->quote($short_description),
                $db->quote($access_level),
                $db->quote($teacher_materials),
                $db->quote($section),
                $db->quote(0),
                $db->quote($course_id),
                $db->quote(1),
                $db->quote(1),
                $db->quote(1686),
                $db->quote('2016-03-10 23:00:00'),
                $db->quote(0),
                $db->quote('0000-00-00 00:00:00'),
                $db->quote(1686),
                $db->quote('2016-03-10 23:00:00'),
                $db->quote(0),
                $db->quote(0),
                $db->quote(0),
                $db->quote($language)
            );

            $query
                ->insert($db->quoteName('#__splms_lessons'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            //echo $query . "\n\n\n";
            //continue;

            $db->setQuery($query);
      $result = $db->execute();           

            

            if ($teacher_num != -1) {
             // echo $teacher_num . " " . $teacher_extra . "\n";
            } else {
              // echo $all[$i]->teacher . "\n";
            }
                        
        }*/
    }
}




// Instantiate the application object, passing the class name to JCli::getInstance
// and use chaining to execute the application.
JApplicationCli::getInstance('StripVideos')->execute();

