<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliUpdateEditor extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        $ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select("*");
            $query->from("#__wf_profiles");
            $query->where('id=1');
            $db->setQuery($query);

            $result = $db->loadObject();
            $params = json_decode($result->params);
            //$params->editor->relative_urls = 0;
           /*  $directoryFilter = [
                "h5p",
                "certificates",
                "blog",
                "business_directory",
                "community",
                "events",
                "inspiration",
                "lmsfiles",
                "store"
            ];
            $params->editor->dir_filter = $directoryFilter; */
           /*  $params->editor->allow_css = '1';
            $params->editor->allow_javascript = '1'; */
           /* $params->source->theme = 'monokai';
            */
            $directoryFilter = [
                "h5p",
                "certificates",
                "blog",
                "business_directory",
                "community",
                "events",
                "inspiration",
                "lmsfiles",
                "store"
            ];
            $params->editor->dir_filter = $directoryFilter;
            $params->browser->extensions = "office=doc,docx,ppt,xls,xlsx,pptx,vtt,srt,csv,txt;image=gif,jpeg,jpg,png;acrobat=pdf;archive=zip,tar,gz,rar;flash=swf;quicktime=mov,mp4,qt;windowsmedia=wmv,asx,asf,avi;audio=wav,mp3,aiff;openoffice=odt,odg,odp,ods,odf";
            $params = json_encode($params);
            $result->params = $params;
            $update = $db->updateObject("#__wf_profiles",$result,'id');
            echo $name.": $update\n";
            $db->disconnect();


        }
    }
}

JApplicationCli::getInstance('CliUpdateEditor')->execute();