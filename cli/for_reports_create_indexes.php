<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliAddArchiveDateColumn extends JApplicationCli {

    private $db = null;


    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND (status LIKE 'active%' OR status = 'test')";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            echo "Ran: ".$params->dbname."\n";

            $db->setQuery("CREATE INDEX activity_completion_check_index ON joom_splms_student_activities (user_id, course_id, lesson_id, activity_type, archive_date);");
            echo "Created new index activity_completion_check_index, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            $db->setQuery("CREATE FULLTEXT INDEX usergroups_fulltext ON joom_splms_courses_groups (usergroups);");
            echo "Created new index activity_completion_check_index, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            $db->setQuery("CREATE FULLTEXT INDEX user_ids_fulltext ON joom_splms_courses_groups (user_ids);");
            echo "Created new index user_ids_fulltext, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            $db->setQuery("CREATE INDEX archive_date_splms_quizquestion_id_user_id ON joom_splms_quizresults (archive_date,splms_quizquestion_id,user_id);");
            echo "Created new index archive_date_splms_quizquestion_id_user_id, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            $db->setQuery("CREATE INDEX action_user_id_date ON axs_actions (action, user_id, date);");
            echo "Created new index action_user_id_date, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            $db->setQuery("CREATE INDEX user_id ON joom_splms_quizresults (user_id);");
            echo "Created new index joom_splms_quizresults, successful? : ".$db->execute()."\n";
            if (!empty($db->getErrorMsg())) {
                echo "Error: " . $db->getErrorMsg() . "\n";
            }

            echo "DONE\n";
        }
    }
}

JApplicationCli::getInstance('CliAddArchiveDateColumn')->execute();