<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliInteractiveContentArchiveIndex extends JApplicationCli {

    private $db = null;


    public function doExecute() {
        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);

            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            echo "Ran: ".$params->dbname."\n";

            // SELECT *
            // FROM ic_h5p_contents_user_data_archive
            // WHERE 
            // (content_id = '421') AND 
            // (user_id = 1637690)
            // ORDER BY version_number DESC

            $db->setQuery("CREATE INDEX content_id_user_id ON ic_h5p_contents_user_data_archive (content_id, user_id);");
            echo "Created new ic_h5p_contents_user_data_archive content_id_user_id db index, successful? : ".$db->execute()."\n";

            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliInteractiveContentArchiveIndex')->execute();