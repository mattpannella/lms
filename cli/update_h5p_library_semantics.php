<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class CliUpdateH5PLibrarySemantics extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {
        $creds = dbCreds::getCreds();
        $ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for
        //$query = "SELECT * FROM `axs_dbmanager` WHERE run_crons=1 AND server_ip='$ip' AND enabled=1;";
        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1";
        $db_result = $conn->query($query);

        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select("*");
            $query->from("ic_h5p_libraries");
            $query->where('id = 157');

            $db->setQuery($query);
            $result = $db->loadObject();

            $semantics = json_decode($result->semantics);
            $filesObject = $semantics[0];

            $filesObject->description = 'Only the first file is playable. Any other files will only be playable if there is an error loading the first one.';

            // Repack the JSON array with the updated files object
            $semantics[0] = $filesObject;
            $updatedSemantics = json_encode($semantics);

            $updateQuery = "UPDATE ic_h5p_libraries
            SET semantics = '$updatedSemantics' WHERE id = 157";

            $db->setQuery($updateQuery);

            $result = $db->execute();

            echo "\n";
            echo 'id: '. $dbs->id.' named: '. $name;
            echo "\n";
            print($result ? "Success" : "Failure");
            print("\n");
            $db->disconnect();
        }
    }
}

JApplicationCli::getInstance('CliUpdateH5PLibrarySemantics')->execute();