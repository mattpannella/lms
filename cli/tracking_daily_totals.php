<?php

// Set flag that this is a parent file.
const _JEXEC = 1;

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php')) {
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES')) {
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once JPATH_LIBRARIES . '/axslibs/email.php';
require_once JPATH_LIBRARIES . '/axslibs/brands.php';
require_once JPATH_LIBRARIES . '/axslibs/language.php';
require_once JPATH_LIBRARIES . '/axslibs/actions.php';
require_once JPATH_LIBRARIES . '/axslibs/tracking.php';
/*error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);*/
error_reporting(1);
ini_set('display_errors', 1);
class CliTrackDailyTotals extends JApplicationCli {

    private $db = null;

    //private static $mailer_list = null;

    public function doExecute() {

        $creds = dbCreds::getCreds();
        //$ip = AxsClients::getPublicIp();

        $conn = new mysqli(
            $creds->dbhost,
            $creds->dbuser,
            $creds->dbpass,
            $creds->dbname
        );

        //Pull which to run crons for id != 9 AND id != 114

        $query = "SELECT * FROM `axs_dbmanager` WHERE enabled=1 AND id != 9 AND id != 114 AND status LIKE 'active%'";
        $db_result = $conn->query($query);
        $i = 0;
        while ($dbs = $db_result->fetch_object()) {
            $update = null;
            $params = AxsClients::decryptClientParams($dbs->dbparams);

            $options = array();
            $options['driver']   = 'mysqli';            // Database driver name
            $options['host']     = $creds->dbhost;         // Database host name
            $options['user']     = $creds->dbuser;     // User for database authentication
            $options['password'] = $creds->dbpass;     // Password for database authentication
            $options['database'] = $params->dbname;     // Database name
            $options['prefix']   = $dbs->dbprefix;      // Database prefix (may be empty)

            $config = JFactory::getConfig();
            $config->set('db', $params->dbname);
            $config->set('user', $creds->dbuser);
            $config->set('password', $creds->dbpass);
            $config->set('dbprefix', $dbs->dbprefix);
            $name = $params->dbname;
            $db = JDatabaseDriver::getInstance($options);

            //reset the db and config.
            JFactory::$config = $config;
            JFactory::$database = $db;

            $this->db = $db;

            $totalLogins = null;
            $totalRegisteredUsers = null;
            $totalActiveUsersMonthToDate = null;
            $bizLibraryAllTimeCourseRegistration = null;
            $bizLibraryRegisteredLearners = null;
            $bizLibraryAllTimeLearners = null;
            $totalActiveUsers30 = null;
            $totalActiveUsers60 = null;
            $totalActiveUsers90 = null;

            $query = $db->getQuery(true);
            $query->select('COUNT(DISTINCT user_id) as total');
            $query->from('axs_actions');
            $query->where($db->quoteName('action').'='.$db->quote('login'));
            $query->where('DATE(date) = CURDATE()');
            $db->setQuery($query);
            $totalLogins = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('#__users');
            $query->where($db->quoteName('block').'= 0');
            $db->setQuery($query);
            $totalRegisteredUsers = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('#__users');
            $query->where('MONTH(lastvisitDate) = MONTH(CURDATE()) AND YEAR(lastvisitDate) = YEAR(CURDATE())');
            $query->where($db->quoteName('block').'= 0');
            $db->setQuery($query);
            $totalActiveUsersMonthToDate = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('#__users');
            $query->where('DATE(lastvisitDate) BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE()');
            $query->where($db->quoteName('block').'= 0');
            $db->setQuery($query);
            $totalActiveUsers30 = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('#__users');
            $query->where('DATE(lastvisitDate) BETWEEN (CURDATE() - INTERVAL 60 DAY) AND CURDATE()');
            $query->where($db->quoteName('block').'= 0');
            $db->setQuery($query);
            $totalActiveUsers60 = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('#__users');
            $query->where('DATE(lastvisitDate) BETWEEN (CURDATE() - INTERVAL 90 DAY) AND CURDATE()');
            $query->where($db->quoteName('block').'= 0');
            $db->setQuery($query);
            $totalActiveUsers90 = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(DISTINCT user_id) as total');
            $query->from('axs_actions');
            $query->where('DATE(date) BETWEEN (CURDATE() - INTERVAL 365 DAY) AND CURDATE()');
            $query->where($db->quoteName('action')." = 'login'");
            $db->setQuery($query);
            $totalUserLogins12Months = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(DISTINCT user_id) as total');
            $query->from('axs_actions');
            $query->where('DATE(date) BETWEEN (CURDATE() - INTERVAL 30 DAY) AND CURDATE()');
            $query->where($db->quoteName('action')." = 'login'");
            $db->setQuery($query);
            $totalUserLogins1Month = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(id) as total');
            $query->from('axs_scorm_learner_data');
            $query->where($db->quoteName('scorm_content_id').' LIKE '.$db->quote('%SVL%'));
            $db->setQuery($query);
            $bizLibraryAllTimeCourseRegistration = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(DISTINCT user_id) as total');
            $query->from('axs_scorm_learner_data');
            $query->where($db->quoteName('scorm_content_id').' LIKE '.$db->quote('%SVL%'));
            $db->setQuery($query);
            $bizLibraryAllTimeLearners = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('COUNT(DISTINCT data.user_id) as total');
            $query->from('axs_scorm_learner_data as data');
            $query->join('INNER','#__users AS users ON users.id = data.user_id');
            $query->where($db->quoteName('data.scorm_content_id').' LIKE '.$db->quote('%SVL%'));
            $query->where($db->quoteName('users.block').' = '.$db->quote('0'));
            $db->setQuery($query);
            $bizLibraryRegisteredLearners = $db->loadObject();


            //echo $query."\n"; die();
            echo $i++.'.'.$name.":\n";
           /*  echo "totalLogins: ".$totalLogins->total."\n";
            echo "totalRegisteredUsers: ".$totalRegisteredUsers->total."\n";
            echo "totalActiveUsers: ".$totalActiveUsers->total."\n";
            echo "totalBizlibraryRegistrations: ".$totalBizlibraryRegistrations->total."\n"; */

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Daily Total Logins';
            $trackingParams->quantity = (int)$totalLogins->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalLogins updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Registered Users';
            $trackingParams->quantity = (int)$totalRegisteredUsers->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalRegisteredUsers updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Active Users [Month to Date]';
            $trackingParams->quantity = (int)$totalActiveUsersMonthToDate->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalActiveUsers updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Active Users [Past 30 Days]';
            $trackingParams->quantity = (int)$totalActiveUsers30->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalActiveUsers updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Active Users [Past 60 Days]';
            $trackingParams->quantity = (int)$totalActiveUsers60->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalActiveUsers updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Active Users [Past 90 Days]';
            $trackingParams->quantity = (int)$totalActiveUsers90->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalActiveUsers updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Total Unique User Logins [Past 12 Months]';
            $trackingParams->quantity = (int)$totalUserLogins12Months->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->setAttribute = 'cf_TotalAnnualUsers';
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            $trackingParams->activeUsers = $trackingParams->quantity;
            AxsTracking::setPendoAccountInfo($trackingParams);
            echo "totalUserLogins12Months updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'Total Unique User Logins [Past 30 Days]';
            $trackingParams->quantity = (int)$totalUserLogins1Month->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "totalUserLogins1Month updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'BizLibrary All Time Course Registrations';
            $trackingParams->quantity = (int)$bizLibraryAllTimeCourseRegistration->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "bizLibraryAllTimeCourseRegistration updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'BizLibrary Registered Learners';
            $trackingParams->quantity = (int)$bizLibraryRegisteredLearners->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "bizLibraryRegisteredLearners updated:\n";

            $trackingParams = new stdClass();
            $trackingParams->initiator = 'Tovuti System';
            $trackingParams->clientId = $dbs->id;
            $trackingParams->eventName = 'BizLibrary All Time Learners';
            $trackingParams->quantity = (int)$bizLibraryAllTimeLearners->total;
            $trackingParams->status = $dbs->status;
            $trackingParams->clientName = $dbs->client_name;
            $trackingParams->subscription_type = $dbs->subscription_type;
            AxsTracking::sendToPendo($trackingParams);
            $trackingParams->action_type ="totals";
            AxsActions::storeAdminAction($trackingParams);
            echo "bizLibraryAllTimeLearners updated:\n";
            $db->disconnect();
            sleep(0.5);
        }
    }
}

JApplicationCli::getInstance('CliTrackDailyTotals')->execute();