<?php


const _JEXEC = 1;
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__));
    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';

require_once("/var/www/sitebuilder/AwsBase.php");

class Clidemo extends JApplicationCli {

    public static $increase_gb = 50;
    public static $buffer_gb = 10;

    public function doExecute() {

        //Get all of the volumes
        $db = AwsDatabase::getDBO();
        $query = "SELECT * FROM root_db.axs_aws_volumes";
        $db->setQuery($query);
        $volumes = $db->loadObjectList();

        foreach ($volumes as $volume) {

            //If it's already full, don't mess with it.
            if ($volume->size < $volume->maximum_size) {

                //Create a volume object out of the ID                
                $aws_volume = new AwsVolume($volume->id);
                if (!$aws_volume) {
                    continue;
                }

                //Create an instance object from the volume's owner
                $start = microtime(true);
                $aws_instance = new AwsInstance($aws_volume->getAttachedInstanceId());
                if (!$aws_instance) {
                    continue;
                }
                /* if($aws_volume->getVolumeId() == 'vol-0f1c376ef06abfeb9') {
                    echo $aws_volume->getVolumeId()."\n";
                    $usage = $aws_instance->getVolumeUsage($aws_volume);                
                    $gb = 1024 * 1024 * 1024;
                    
                    //Get how much space is remaining
                    $remaining = $usage->remaining / $gb;
                    echo "increase_gb: ".self::$increase_gb."\n";
                    $new_size = 500;
                    echo "new size: ". $new_size."\n";
                    $aws_instance->increaseVolumeSize($aws_volume, $new_size);
                }  */        
                //Get the volume's usage.
                $usage = $aws_instance->getVolumeUsage($aws_volume);                
                $gb = 1024 * 1024 * 1024;
                //Get how much space is remaining
                $remaining = $usage->remaining / $gb;

                //If the amount remaining is smaller than the buffer, increase its size.
                if ($remaining < self::$buffer_gb) {
                    
                    $new_size = $volume->size + self::$increase_gb;
                
                    if ($new_size > $volume->maximum_size) {
                        $new_size = $volume->maximum_size;
                    }
                    
                    $aws_instance->increaseVolumeSize($aws_volume, $new_size);

                    //Update the DB with the new size
                    $query = "UPDATE root_db.axs_aws_volumes SET size = " . $new_size . " WHERE id = '" . $volume->id . "'";
                    $db->setQuery($query);
                    $db->execute();
                }
            }
        }
    }
}

JApplicationCli::getInstance('Clidemo')->execute();