<?php
 /**
  * @version		
  * @copyright	Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
  * @license	GNU General Public License version 2 or later; see LICENSE.txt
  */
 
 defined('JPATH_BASE') or die;

  class plgUserProfilelms extends JPlugin{

	protected static $avatar = array();

	protected static $last_avatar = '';

	function onContentPrepareData($context, $data){
		// Check we are manipulating a valid form.
		if (!in_array($context, array('com_users.profile','com_users.registration','com_users.user','com_admin.profile'))){
			return true;
		}
 
		$userId = isset($data->id) ? $data->id : 0;
 
		// Load the profile data from the database.
		$db = JFactory::getDbo();
		$db->setQuery(
			'SELECT profile_key, profile_value FROM #__user_profiles' .
			' WHERE user_id = '.(int) $userId .
			' AND profile_key LIKE \'profilelms.%\'' .
			' ORDER BY ordering'
		);
		$results = $db->loadRowList();
 
		// Check for a database error.
		if ($db->getErrorNum()) {
			$this->_subject->setError($db->getErrorMsg());
			return false;
		}
 	
 		// echo "<pre>";
		// print_r($data->profilelms);
		// echo "</pre>";
		// die();

		// Merge the profile data.

		if (isset($results) && !empty($results)) {
			$data->profilelms = array();
			foreach ($results as $v) {
				$k = str_replace('profilelms.', '', $v[0]);
				$data->profilelms[$k] = json_decode($v[1], true);
			}
		}

		return true;
	}
 
	/**
	 * @param	JForm	The form to be altered.
	 * @param	array	The associated data for the form.
	 * @return	boolean
	 * @since	1.6
	 */
	function onContentPrepareForm($form, $data){

		// Load user_profile plugin language
		$lang = JFactory::getLanguage();
		$lang->load('plg_user_profilelms', JPATH_ADMINISTRATOR);
 
		if (!($form instanceof JForm)) {
			$this->_subject->setError('JERROR_NOT_A_FORM');
			return false;
		}
		// Check we are manipulating a valid form.
		if (!in_array($form->getName(), array('com_users.profile', 'com_users.registration','com_users.user','com_admin.profile'))) {
			return true;
		}

		//unset($data->profilelms['avatar']);
		
		//self::$last_avatar = $data->profilelms['avatar']['avatar'];
		// echo "<pre>";
		// print_r(self::$last_avatar);
		// echo "</pre>";
		// die();

		if ($form->getName()=='com_users.profile')
		{
			// Add the profile fields to the form.
			JForm::addFormPath(dirname(__FILE__).'/profiles');
			$form->loadFile('profile', false);
 
			// Toggle whether the something field is required.
			if ($this->params->get('profile-require_something', 1) > 0) {
				$form->setFieldAttribute('something', 'required', $this->params->get('profile-require_something') == 2, 'profilelms');
			} else {
				$form->removeField('something', 'profilelms');
			}
		}
 
		//In this example, we treat the frontend registration and the back end user create or edit as the same. 
		elseif ($form->getName()=='com_users.registration' || $form->getName()=='com_users.user' ){		
			// Add the registration fields to the form.
			JForm::addFormPath(dirname(__FILE__).'/profiles');
			$form->loadFile('profile', false);

			// Toggle whether the something field is required.
			if ($this->params->get('register-require_something', 1) > 0) {
				$form->setFieldAttribute('something', 'required', $this->params->get('register-require_something') == 2, 'profilelms');
			} else {
				$form->removeField('something', 'profilelms');
			}
		}			
	}

	// check file type
	protected static function fileExtensionCheck($file, $allowed){
		$ext = pathinfo($file['name']['profilelms']['lms_avatar'], PATHINFO_EXTENSION);
		if(in_array( strtolower($ext), $allowed) ) {
			return true;
		}
		return false;
	}

	function onUserBeforeSave($user, $isnew, $new){


		//Import filesystem libraries. Perhaps not necessary, but does not hurt
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');
		self::$avatar = JRequest::getVar( 'jform', null, 'files');

		//get last uploaded avatar
		self::$last_avatar = json_decode(self::existAvatar($new['id'])->profile_value)->avatar;
		
		// New avatar
		$avatar_name = self::$avatar['name']['profilelms']['lms_avatar'];
		if (isset($avatar_name) && $avatar_name !='') {
			if (!self::fileExtensionCheck(self::$avatar, array('png', 'jpg'))) {
				throw new RuntimeException(JText::_('INVALID_FILE_TYPE'), 1);
			}elseif (self::$avatar['size']['profilelms']['lms_avatar'] > 800000) {
				throw new RuntimeException(JText::_('INVALID_FILE_SIZE'), 1);
			}
		}

		// unset profile pic
		//unset(self::$avatar);
	
	}

 
	function onUserAfterSave($data, $isNew, $result, $error){

		$userId	= JArrayHelper::getValue($data, 'id', 0, 'int');

		$avatar_name = self::$avatar['name']['profilelms']['lms_avatar'];

		// has avatar
		if (isset($avatar_name) && $avatar_name != '') {
			$folder_path = JPATH_ROOT . '/media/com_splms/users/';
			if (!JFolder::exists( $folder_path )) {
				JFolder::create($folder_path);
			}

			// Cleans the name of teh file by removing weird characters
			$filename = JFile::makeSafe($avatar_name); 
			$src = self::$avatar['tmp_name']['profilelms']['lms_avatar'];
		}
 
		if (($userId && $result && isset($data['profilelms']) && (count($data['profilelms']))) || $src){
			try{

				if (isset($avatar_name) && $avatar_name != '') {
					$exist_avatar = json_decode(self::existAvatar($userId)->profile_value)->avatar;

					//print_r($exist_avatar);
					if (isset($exist_avatar) && $exist_avatar != '') {
						JFile::delete(JPATH_ROOT.$exist_avatar);
					}
					//echo JPATH_ROOT.$exist_avatar;
					//die();	
				}

				$db = JFactory::getDbo();
				// Create a new query object.
				$query = $db->getQuery(true);

				$result = $db->setQuery('SELECT profile_value FROM #__user_profiles WHERE profile_key = profilelms.avatar ');

				//if (isset($avatar_name) && $avatar_name != '') {
					$db->setQuery('DELETE FROM #__user_profiles WHERE user_id = '.$userId.' AND profile_key LIKE \'profilelms.%\'');
				// }else{
				// 	$db->setQuery('DELETE FROM #__user_profiles WHERE profile_key NOT IN profilelms.avatar AND user_id = '.$userId.' AND profile_key LIKE \'profilelms.%\'');
				// }
				

				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
 				
 				if ((isset($avatar_name) && $avatar_name != '')) {
	 				$file_path = '/media/com_splms/users/'. $userId.'-'.$filename;

					// if file upload then insert into DB
					if (JFile::upload($src, $folder_path. '/' .$userId.'-'.$filename)) {
						$data['profilelms']['avatar'] = array('avatar'=> $file_path);
					}
				}

				// already has avatar and not uploaded
				if (self::$last_avatar != '' && $avatar_name =='') {
					$data['profilelms']['avatar'] = array('avatar'=> self::$last_avatar);
				}elseif (self::$last_avatar == '' && $avatar_name =='') {
					$data['profilelms']['avatar'] = array('avatar'=> '');
				}

				$tuples = array();
				$order	= 1;
				foreach ($data['profilelms'] as $k => $v) {
					$tuples[] = '('.$userId.', '.$db->quote('profilelms.'.$k).', '.$db->quote(json_encode($v)).', '.$order++.')';
				}
 
				$db->setQuery('INSERT INTO #__user_profiles VALUES '.implode(', ', $tuples));

				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
			}
			catch (JException $e) {
				$this->_subject->setError($e->getMessage());
				return false;
			}
		}
 
		return true;
	}
 
	/**
	 * Remove all user profile information for the given user ID
	 *
	 * Method is called after user data is deleted from the database
	 *
	 * @param	array		$user		Holds the user data
	 * @param	boolean		$success	True if user was succesfully stored in the database
	 * @param	string		$msg		Message
	 */

	protected static function existAvatar($user_id){

		$lmsdb = JFactory::getDbo();
		$lmsquery = $lmsdb->getQuery(true);
		$lmsquery->select('profile_value');
		$lmsquery->from($lmsdb->quoteName('#__user_profiles'));
		$lmsquery->where($lmsdb->quoteName('user_id')." = ".$user_id);
		$lmsquery->where($lmsdb->quoteName('profile_key') . ' = '. $lmsdb->quote('profilelms.avatar'));
		$lmsdb->setQuery($lmsquery);
		$result = $lmsdb->loadObject();

		return $result;
	}

	function onUserAfterDelete($user, $success, $msg){
		if (!$success) {
			return false;
		}
 
		$userId	= JArrayHelper::getValue($user, 'id', 0, 'int');
 
		if ($userId)
		{
			try
			{
				$db = JFactory::getDbo();
				$db->setQuery(
					'DELETE FROM #__user_profiles WHERE user_id = '.$userId .
					" AND profile_key LIKE 'profilelms.%'"
				);
 
				if (!$db->query()) {
					throw new Exception($db->getErrorMsg());
				}
			}
			catch (JException $e)
			{
				$this->_subject->setError($e->getMessage());
				return false;
			}
		}
 
		return true;
	}
 
 
 }