<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/20/16
 * Time: 4:52 PM
 */
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

/**
 * @package     Joomla.plugin
 * @subpackage  System.axslibs
 */
class plgAxsLms extends JPlugin {

	public function userInteractiveSync($user) {

	}

	public function onBeforeRender() {
		//require_once JPATH_ROOT.'/components/com_interactivecontent/autoloader.php';

		$input = JFactory::getApplication()->input;
		$encryptedReturnParams = $input->get('return_params',0,'STRING');
		if($encryptedReturnParams) {
			$key = AxsKeys::getKey('lms');
			$virtualClassParams = AxsEncryption::decrypt(base64_decode($encryptedReturnParams), $key);
			$meetingCookieIdentifier = base64_encode($virtualClassParams->meetingId);

			if($_COOKIE[$meetingCookieIdentifier.'_u']) {
				$virtualClassParams->userId = $_COOKIE[$meetingCookieIdentifier.'_u'];
				setcookie($meetingCookieIdentifier.'_u','',time() -3600);
			}

			if($_COOKIE[$meetingCookieIdentifier.'_g']) {
				$virtualClassParams->guestId = $_COOKIE[$meetingCookieIdentifier.'_g'];
				setcookie($meetingCookieIdentifier.'_g','',time() -3600);
			}

			if($_COOKIE[$meetingCookieIdentifier.'_u'] || $_COOKIE[$meetingCookieIdentifier.'_g']) {
				$result = AxsVirtualClassroom::trackAttendance($virtualClassParams,'checkout');
			}

			if($virtualClassParams->returnUrl) {
				header("Location: $virtualClassParams->returnUrl");
			}
		}
	}

	public function onContentAfterSave($context, $item, $isNew) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$task = $app->input->get('task');
		$view = $app->input->get('view');
		$layout = $app->input->get('layout');
		$itemParams = json_decode($item->params);

		$params = new stdClass();
		if ( $option == 'com_splms' && $view == 'courseassignment') {
			if($itemParams->send_notifications) {
	        	$params->item = $item;
	        	$alerts = new AxsAlerts($params);
	        	if($item->type == 1) {
	        		$alertType = 'course recommended';
	        	} else {
	        		$alertType = 'course assigned';
	        	}
	        	$alerts->runAutoAlert($alertType,$params);
	        }
		}

		if ($option == 'com_splms' && $view == 'course') {
			if($isNew) {
				$params->eventName = 'Course Creation';
			} else {
				$params->eventName = 'Course Edit';
			}

			$params->description = $item->title.' ['.$item->splms_course_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brand') {
			if($isNew) {
				$params->eventName = 'Brand Creation';
			} else {
				$params->eventName = 'Brand Edit';
			}
			$params->description = $item->site_title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'lesson') {
			if($isNew) {
				$params->eventName = 'Lesson Creation';
			} else {
				$params->eventName = 'Lesson Edit';
			}
			$params->description = $item->title.' ['.$item->splms_lesson_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}


		if ($option == 'com_axs' && $view == 'importer') {
			if($isNew) {
				$params->eventName = 'User Importer Creation';
			} else {
				$params->eventName = 'User Importer Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_reports' && $view == 'report') {
			if($isNew) {
				$params->eventName = 'Report Creation';
			} else {
				$params->eventName = 'Report Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_reports' && $view == 'activity_dashboard') {
			if($isNew) {
				$params->eventName = 'Activity Dashboard Creation';
			} else {
				$params->eventName = 'Activity Dashboard Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'virtual_classroom') {
			if($isNew) {
				$params->eventName = 'Virtual Classroom Creation';
			} else {
				$params->eventName = 'Virtual Classroom Edit';
			}
			$params->description = $item->meetingName.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_permissioning' && $view == 'group') {
			if($isNew) {
				$params->eventName = 'Admin Permissions Creation';
			} else {
				$params->eventName = 'Admin Permissions Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'apikey') {
			if($isNew) {
				$params->eventName = 'API Key Creation';
			} else {
				$params->eventName = 'API Key Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'sso_config') {
			if($isNew) {
				$params->eventName = 'SSO Creation';
			} else {
				$params->eventName = 'SSO Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_domains' && $view == 'domain') {
			if($isNew) {
				$params->eventName = 'Domain Creation';
			} else {
				$params->eventName = 'Domain Edit';
			}
			$params->description = $item->domain.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_converge' && $view == 'plan') {
			if($isNew) {
				$params->eventName = 'Subscription Plan Creation';
			} else {
				$params->eventName = 'Subscription Plan Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brand_homepage') {
			if($isNew) {
				$params->eventName = 'Landing Page Creation';
			} else {
				$params->eventName = 'Landing Page Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'learner_dashboard') {
			if($isNew) {
				$params->eventName = 'Learner Dashboard Creation';
			} else {
				$params->eventName = 'Learner Dashboard Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brand_dashboard') {
			if($isNew) {
				$params->eventName = 'Learner Portal Creation';
			} else {
				$params->eventName = 'Learner Portal Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_menus' && $view == 'item' && $layout == 'edit') {
			if($isNew) {
				$params->eventName = 'Navigation Item Creation';
			} else {
				$params->eventName = 'Navigation Item Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'font') {
			if($isNew) {
				$params->eventName = 'Font Creation';
			} else {
				$params->eventName = 'Font Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'courseassignment') {
			if($isNew) {
				$params->eventName = 'Assign & Recommend Creation';
			} else {
				$params->eventName = 'Assign & Recommend Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'checklist') {
			if($isNew) {
				$params->eventName = 'Checklist Creation';
			} else {
				$params->eventName = 'Checklist Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'quizquestion') {
			if($isNew) {
				$params->eventName = 'Quizzes & Survey Creation';
			} else {
				$params->eventName = 'Quizzes & Survey Edit';
			}
			$params->description = $item->title.' ['.$item->splms_quizquestion_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'quizresult') {
			if($isNew) {
				$params->eventName = 'Quiz Result Creation';
			} else {
				$params->eventName = 'Quiz Result Edit';
			}
			$params->description = 'Quiz ID: ['.$item->splms_quizquestion_id.'] User ID: ['.$item->user_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_converge' && $view == 'promo') {
			if($isNew) {
				$params->eventName = 'Course & Subscription Promo Code Creation';
			} else {
				$params->eventName = 'Course & Subscription Promo Code Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'scormitem') {
			if($isNew) {
				$params->eventName = 'SCORM Item Creation';
			} else {
				$params->eventName = 'SCORM Item Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);

			$expirationData = new stdClass();
			$info 			= new stdClass();
			$expParams 	    = json_decode($item->params);

			$expirationData->content_type    = 'SCORM item';
			$expirationData->content_id      = $item->id;
			$expirationData->custom_id       = $expParams->custom_id;
			$expirationData->title           = $item->title;
			$expirationData->expiration_date = $expParams->expiration_date;
			$expirationData->data            = "";
			$expirationData->params          = "";
			$expirationObject = new AxsContentExpiration($expirationData);
			if($expParams->use_expiration && ($expParams->custom_id || $expParams->expiration_date)) {
				$expirationObject->store();
			} else {
				if($expirationObject->getRow()) {
					$expirationObject->delete();
				}
			}
		}

		if ($option == 'com_award' && $view == 'badge') {
			require_once(JPATH_ROOT."/administrator/components/com_award/helpers/helper.php");
			if($isNew) {
				$params->eventName = ucfirst($item->type).' Creation';
			} else {
				$params->eventName = ucfirst($item->type).' Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
			BadgeHelper::updateBadges($item->id);
		}

		if ($option == 'com_splms' && $view == 'certificate') {
			if($isNew) {
				$params->eventName = 'Certificate Design Creation';
			} else {
				$params->eventName = 'Certificate Design Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'points_category') {
			if($isNew) {
				$params->eventName = 'Points Category Creation';
			} else {
				$params->eventName = 'Points Category Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'coursescategory') {
			if($isNew) {
				$params->eventName = 'Course Category Creation';
			} else {
				$params->eventName = 'Course Category Edit';
			}
			$params->description = $item->title.' ['.$item->splms_coursescategory_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'category') {
			if($isNew) {
				$params->eventName = 'Media Category Creation';
			} else {
				$params->eventName = 'Media Category Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'video') {
			if($isNew) {
				$params->eventName = 'Media Item Creation';
			} else {
				$params->eventName = 'Media Item Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);

			$expirationData = new stdClass();
			$info 			= new stdClass();
			$expParams 		= json_decode($item->params);

			$expirationData->content_type    = 'media item';
			$expirationData->content_id      = $item->id;
			$expirationData->custom_id       = $expParams->custom_id;
			$expirationData->title           = $item->title;
			$expirationData->expiration_date = $expParams->expiration_date;
			$expirationData->data            = "";
			$expirationData->params          = "";
			$expirationObject = new AxsContentExpiration($expirationData);
			if($expParams->use_expiration && ($expParams->custom_id || $expParams->expiration_date)) {
				$expirationObject->store();
			} else {
				if($expirationObject->getRow()) {
					$expirationObject->delete();
				}
			}

		}

		if ($option == 'com_axs' && $view == 'team') {
			if($isNew) {
				$params->eventName = 'Team Creation';
			} else {
				$params->eventName = 'Team Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}



		if ($option == 'com_splms' && $view == 'teacher') {
			if($isNew) {
				$params->eventName = 'Teacher Creation';
			} else {
				$params->eventName = 'Teacher Edit';
			}
			$params->description = $item->title.' ['.$item->splms_teacher_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_contactpage' && $view == 'contactpage') {
			if($isNew) {
				$params->eventName = 'Contact Form Creation';
			} else {
				$params->eventName = 'Contact Form Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'popup_notification') {
			if($isNew) {
				$params->eventName = 'Popup Notification Creation';
			} else {
				$params->eventName = 'Popup Notification Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'email_notification') {
			if($isNew) {
				$params->eventName = 'Email Notification Creation';
			} else {
				$params->eventName = 'Email Notification Edit';
			}
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'auto_alert') {
			if($isNew) {
				$params->eventName = 'Auto Notification Creation';
			} else {
				$params->eventName = 'Auto Notification Edit';
			}
			$params->description = $item->name.' - '.ucfirst($item->alert_type).' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_content' && $layout == 'edit') {
			if($isNew) {
				$params->eventName = 'Site Page Creation';
			} else {
				$params->eventName = 'Site Page Edit';
			}
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

	}

	public function onContentAfterDelete($context, $item) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$task = $app->input->get('task');
		$view = $app->input->get('view');
		$layout = $app->input->get('layout');
		$itemParams = json_decode($item->params);
		$params = new stdClass();

		if ($option == 'com_splms' && $view == 'courses') {
			$params->eventName = 'Course Delete';
			$params->description = $item->title.' ['.$item->splms_course_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brands') {
			$params->eventName = 'Brand Delete';
			$params->description = $item->site_title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'lessons') {
			$params->eventName = 'Lesson Delete';
			$params->description = $item->title.' ['.$item->splms_lesson_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}


		if ($option == 'com_axs' && $view == 'importers') {
			$params->eventName = 'User Importer Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_reports' && $view == 'reports') {
			$params->eventName = 'Report Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_reports' && $view == 'activity_dashboards') {
			$params->eventName = 'Activity Dashboard Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'virtual_classrooms') {
			$params->eventName = 'Virtual Classroom Delete';
			$params->description = $item->meetingName.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_permissioning' && $view == 'groups') {
			$params->eventName = 'Admin Permissions Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'apikeys') {
			$params->eventName = 'API Keys Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'sso_configs') {
			$params->eventName = 'SSO Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brand_homepages') {
			$params->eventName = 'Landing Page Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'brand_dashboards') {
			$params->eventName = 'Learner Portal Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'learner_dashboards') {
			$params->eventName = 'Learner Dashboard Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_menus' && $view == 'items') {
			$params->eventName = 'Navigation Item Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'fonts') {
			$params->eventName = 'Font Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'courseassignments') {
			$params->eventName = 'Assign & Recommend Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'checklists') {
			$params->eventName = 'Checklist Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'quizquestions') {
			$params->eventName = 'Quizzes & Survey Delete';
			$params->description = $item->title.' ['.$item->splms_quizquestion_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'quizresults') {
			$params->eventName = 'Quiz Result Delete';
			$params->description = 'Quiz ID: ['.$item->splms_quizquestion_id.'] User ID: ['.$item->user_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_converge' && $view == 'promos') {
			$params->eventName = 'Course & Subscription Promo Code Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'scormitems') {
			$params->eventName = 'SCORM Item Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);

			$expirationData = new stdClass();
			$expirationData->content_type    = 'SCORM item';
			$expirationData->content_id      = $item->id;
			$expirationObject = new AxsContentExpiration($expirationData);
			if($expirationObject->getRow()) {
				$expirationObject->delete();
			}
		}

		if ($option == 'com_award' && $view == 'badges') {
			$params->eventName = ucfirst($item->type).' Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'certificates') {
			$params->eventName = 'Certificate Design Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'points_categories') {
			$params->eventName = 'Points Category Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_splms' && $view == 'coursescategories') {
			$params->eventName = 'Course Category Delete';
			$params->description = $item->title.' ['.$item->splms_coursescategory_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'categories') {
			$params->eventName = 'Media Category Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_axs' && $view == 'videos') {
			$params->eventName = 'Media Item Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);

			$expirationData = new stdClass();
			$expirationData->content_type    = 'media item';
			$expirationData->content_id      = $item->id;
			$expirationObject = new AxsContentExpiration($expirationData);
			if($expirationObject->getRow()) {
				$expirationObject->delete();
			}
		}

		if ($option == 'com_splms' && $view == 'teachers') {
			$params->eventName = 'Teacher Delete';
			$params->description = $item->title.' ['.$item->splms_teacher_id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_contactpage' && $view == 'contactpages') {
			$params->eventName = 'Contact Form Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'popup_notifications') {
			$params->eventName = 'Popup Notification Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'email_notifications') {
			$params->eventName = 'Email Notification Delete';
			$params->description = $item->name.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_notifications' && $view == 'auto_alerts') {
			$params->eventName = 'Auto Notification Delete';
			$params->description = $item->name.' - '.ucfirst($item->alert_type).' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if ($option == 'com_content' && $view == 'articles') {
			$params->eventName = 'Site Page Delete';
			$params->description = $item->title.' ['.$item->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
	}

	public function onAfterSaveEvent($row, $data, $isNew) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		$params = new stdClass();

		if ($option == 'com_eventbooking' && $view == 'event' ) {
			if($isNew) {
				$params->eventName = 'Event Creation';
			} else {
				$params->eventName = 'Event Edit';
			}
			$params->description = $row->title.' ['.$row->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

	}

	public function onKunenaAfterSave($context, $table, $isNew) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		$params = new stdClass();
		if ($option == 'com_kunena' && $view == 'categories' && $app->isAdmin()) {
			if($isNew) {
				$params->eventName = 'Forum Category Creation';
			} else {
				$params->eventName = 'Forum Category Edit';
			}
			$params->description = $table->name.' ['.$table->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
	}

	public function onKunenaAfterDelete($context, $table) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		$params = new stdClass();
		if ($option == 'com_kunena' && $view == 'categories' && $app->isAdmin()) {
			$params->eventName = 'Forum Category Delete';
			$params->description = $table->name.' ['.$table->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
	}

	public function onUserAfterSaveGroup($data,$group,$isNew) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$layout = $app->input->get('layout');
		$params = new stdClass();
		if ($option == 'com_users' && $layout == 'edit' && $app->isAdmin()) {
			if($isNew) {
				$params->eventName = 'User Group Creation';
			} else {
				$params->eventName = 'User Group Edit';
			}
			$params->description = $group->title.' ['.$group->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

	}

	public function onUserAfterDeleteGroup($data) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		$params = new stdClass();
		if ($option == 'com_users' && $view == 'groups' && $app->isAdmin()) {
			$params->eventName = 'User Group Delete';
			$params->description = $data['title'].' ['.$data['id'].']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

	}

	public function onUserAfterSave($user,$isNew) {
        if(is_object($user)) {
			$id = $user->id;
		}

		if(is_array($user)) {
			$id = $user['id'];
		}

		$userObj = JFactory::getUser($id);
		//AxsInteractiveContent::createICUser($userObj);
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$layout = $app->input->get('layout');
		$params = new stdClass();
		if ($option == 'com_users' && $layout == 'edit' && $app->isAdmin()) {
			if($isNew) {
				$CUser = new CUser($id);
				$initCUser = $CUser->init();
				AxsUser::setCommunityFullName($id);
				$params->eventName = 'User Creation';
			} else {
				$params->eventName = 'User Edit';
			}
			$params->description = $userObj->name.' ['.$userObj->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
	}

	public function onUserLogin($user, $options) {
		$_SESSION['oldSessionId'] = JFactory::getSession()->getId();
	}

	public function onUserAfterLogin($user) {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$params = new stdClass();
		if ($app->isAdmin()) {
			$params->eventName = 'Admin Login';
			$params->description = $user->name.' ['.$user->id.']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
		AxsActions::storeAction($user->id,'login');
	}

	public function onUserLogout($user, $options) {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$params = new stdClass();
		if ($app->isAdmin()) {
			$params->eventName = 'Admin Logout';
			$params->description = $user->name.' ['.$user->id.']';
			if ($_POST['task'] != 'axsusers.block') {
				AxsTracking::sendToPendo($params);
			}
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
		AxsActions::storeAction($user->id,'logout');
	}

	/*public function onAfterRender() {
		$newSessionId = JFactory::getSession()->getId();
		$oldSessionId = $_SESSION['oldSessionId'];
		if($oldSessionId) {
			//run update in analytics table
			$user = JFactory::getUser();
			$db = JFactory::getDbo();
			$db->setQuery("UPDATE #__realtimeanalytics_serverstats SET session_id_person='$newSessionId', user_id_person=$user->id, customer_name='$user->name' WHERE session_id_person='$oldSessionId';");
			$db->execute();
			//clear session var
			unset($_SESSION['oldSessionId']);
		}
	}*/

	public function onCoursePurchase($cp) {
        $course = $cp->getCourse();
        if ($cp->status == 'PAID' && $course->upgrade && $course->upgrade != 8 && $cp->user_id) {
         	$user_groups = JUserHelper::getUserGroups($cp->user_id);
            if(!in_array($course->upgrade,$user_groups)) {
                JUserHelper::addUserToGroup($cp->user_id,$course->upgrade);
                AxsActions::trackUserGroup($cp->user_id,$course->upgrade,'add');
            }
		}
    }

	public function onSubscriptionPlay($sub) {
		$brand = AxsBrands::getBrand();
		$plan = $sub->getPlan();

		$courses = json_decode($plan->courses);
		foreach($courses as $course) {
			if ($course) {

				$courseParams = AxsPayment::getCoursePurchaseParams();
				$courseParams->user_id = $sub->user_id;
				$courseParams->course_id = $course;
				$courseParams->date = date("Y-m-d H:i:s");
				$courseParams->status = "PAID";

				$purchase = AxsPayment::newCoursePurchase($courseParams);
				$purchase->save();
			}
		}


	}

	public function onContentPrepareForm($form, $data) {


		$brand = AxsBrands::getBrand();
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$task = $app->input->get('task');
		$view = $app->input->get('view');
		if ( $option == 'com_splms' && ($view == 'course' || $view == 'lesson') ) {
			$scormAccess = AxsDbAccess::getAccessLevels('scorm');
			$contentLibraryAccess = AxsDbAccess::getAccessLevels('bizlibrary');

			if($scormAccess || $contentLibraryAccess) {
				ob_start();
		?>
				<fieldset
					name="content_library_tab"
					label="Content Library"
					class='tab-pane'
				>

						<field
				            name="use_library_content"
				            type="radio"
				            default="0"
				            label="Use Library Content"
				            
						 >        
							<option value="1">Yes</option>      
				            <option value="0">No</option>
				                
				        </field>

				        <?php if ( $option == 'com_splms' && $view == 'lesson' ) { ?>

					    	<?php if ($scormAccess || $contentLibraryAccess) { ?>

						    	<field 
						            name="scorm_required" 
						            type="radio" 
						            default="0" 
						            label="Completion Required"
						            
						            showon="use_library_content:1"
								 >           
								 	<option value="1">Yes</option>   
						            <option value="0">No</option>
						                
						        </field>

					        <?php } ?>

				    	<?php } ?>

				        <field
				            name="library_content_type"
				            type="list"
				            label="Choose Library"
				            showon="use_library_content:1"
				         >
				         		<?php if ($contentLibraryAccess) { ?>
				                	<option value="bizlibrary">BizLibrary</option>
				                <?php } ?>

				         		<?php if ($scormAccess) { ?>
				                	<option value="scorm">SCORM Library</option>
				                <?php } ?>

				        </field>

				    	<?php if ($scormAccess) { ?>
					        <field
					            name="scorm_id"
					            label="SCORM Package"
					            type="scormselector"
					            showon="use_library_content:1[AND]library_content_type:scorm"
					            header="--Select SCORM Content--"
					        />
				        <?php } ?>

				        <?php if ($contentLibraryAccess) { ?>
					        <field
					            name="bizlibrary_content_id"
					            label="BizLibrary ID"
					            type="hidden"
					            showon="use_library_content:1[AND]library_content_type:bizlibrary"
					        />

					        <field
					            name="bizlibrary_content_title"
					            label="BizLibrary Content"
					            type="hidden"
					            showon="use_library_content:1[AND]library_content_type:bizlibrary"
					        />
					        <field
						            name="bizlibrary_catalog"
						            type="bizlibrary"
									label="BizLibrary Catalog"  
									showlabel="false"         
						            class="bizlibrary_catalog"
						            showon="use_library_content:1[AND]library_content_type:bizlibrary"
						         >
						    </field>
				        <?php } ?>
				</fieldset>
		<?php
				$newField = ob_get_clean();
				$form->setField(new SimpleXMLElement($newField));
			}
		}

		if ($option == 'com_converge' && $view == 'plan') {
			ob_start();

			?>

				<fieldset
					name="lms"
					label="LMS Options"
					class="tab-pane"
				>
					<field
						name="courses"
		               	type="sql"
		               	query="SELECT * FROM joom_splms_courses"
		               	key_field="splms_course_id"
		               	value_field="title"
		               	label="Initial signup also purchases these Course(s)"
		               	class="big-multi"
		               	multiple="true"
					>
			        	<option value="">--None--</option>
			        </field>
				</fieldset>

			<?php

			$newField = ob_get_clean();
			$xmlElement = new SimpleXMLElement($newField);
			$form->setField($xmlElement);
		}

		if ($option == "com_splms") {

			if ($view == "course") {

				ob_start();
				?>

					<fieldset
						name='multilanguage'
						label='Multilingual Overrides'
						class='tab-pane'
					>
						<!--  This is not stored -->
						<field
							name='lang_select'
							label='Language'
							type='sql'
							query='SELECT * FROM joom_languages WHERE published = 1'
							key_field='lang_code'
							value_field='title'
							description='Only one override is shown at a time to reduce clutter.&lt;br&gt;Overrides can be set for every language.'
						/>

						<?php

							$db = JFactory::getDBO();
							$query = "SELECT * FROM joom_languages WHERE published = 1";
							$db->setQuery($query);
							$languages = $db->loadObjectList();
							foreach ($languages as $language) {
								?>
									<field
										name="lang_use_override_<?php echo $language->sef;?>"
										label='Enable <?php echo $language->title;?> Override'
										type='radio'
										class="radio btn-group"
										default="0"
										showon='lang_select:<?php echo $language->lang_code;?>'
									>
										<option value="0">No</option>
										<option value="1">Yes</option>
									</field>

									<field
										name='lang_title_<?php echo $language->sef;?>'
										label='Title for <?php echo $language->title;?>'
										type='text'
										showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									/>

									<field
									 	name="lang_video_type_<?php echo $language->sef;?>"
									 	type="list"
									 	label="<?php echo $language->title;?> Video Type"
									 	showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									 	default="mp4"
									 >
									 	<option value="none">Use Default</option>
										<option value="mp4">MP4</option>
										<option value="youtube">YouTube</option>
										<option value="screencast">Screencast</option>
										<option value="facebook">Facebook</option>
										<option value="vimeo">Vimeo</option>
									</field>

									<field
										name="lang_video_url_<?php echo $language->sef;?>"
									    type="media"
            							mediatype="files"
										label="<?php echo $language->title;?> Video File"
										showon="lang_video_type_<?php echo $language->sef;?>:mp4[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
										filter="none"
									/>

									<field
									 	name="lang_youtube_id_<?php echo $language->sef;?>"
									 	type="text"
										class="inputbox"
										label="<?php echo $language->title;?> YouTube Video URL or ID"
										labelclass="splms-label splms-label-main"
										showon="lang_video_type_<?php echo $language->sef;?>:youtube[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"

									/>

									<field
									 	name="lang_screencast_id_<?php echo $language->sef;?>"
									 	type="textarea"
									 	filter="raw"
										label="<?php echo $language->title;?> Screencast Embed Code"
										showon="lang_video_type_<?php echo $language->sef;?>:screencast[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"

									/>

									<field
									 	name="lang_facebook_url_<?php echo $language->sef;?>"
									 	type="text"
										class="inputbox"
										label="<?php echo $language->title;?> Facebook Video URL"
										labelclass="splms-label splms-label-main"
										showon="lang_video_type_<?php echo $language->sef;?>:facebook[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"

									/>

									<field
									 	name="lang_vimeo_id_<?php echo $language->sef;?>"
									 	type="text"
										class="inputbox"
										label="<?php echo $language->title;?> Vimeo URL or ID"
										labelclass="splms-label splms-label-main"
										showon="lang_video_type_<?php echo $language->sef;?>:vimeo[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"

									/>

									<field
										name='lang_description_<?php echo $language->sef;?>'
										label='Description for <?php echo $language->title;?>'
										type='editor'
										showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									/>

									<field
										name="lang_minimum_students_not_met_text_<?php echo $language->sef;?>"
										type="text"
										label="Minimum Students Not Met Text"
										showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									/>

									<field
										name="lang_lesson_button_text_<?php echo $language->sef;?>"
										type="text"
										label="Lesson Button Text for <?php echo $language->title;?>"
										showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
									/>

									<field
										name="lang_students_button_text_<?php echo $language->sef;?>"
										type="text"
										label="Students Button Text for <?php echo $language->title;?>"
										showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
									/>

									<field
										name="lang_progress_area_text_<?php echo $language->sef;?>"
										type="text"
										label="Progress Area Text for <?php echo $language->title;?>"
										showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
									/>

									<field
										name="lang_student_list_text_<?php echo $language->sef;?>"
										type="text"
										label="Student List Text for <?php echo $language->title;?>"
										showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
									/>

									<field
										name="lang_teacher_list_text_<?php echo $language->sef;?>"
										type="text"
										label="Teacher List Text for <?php echo $language->title;?>"
										showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
									/>

									<?php if($scormAccess) { ?>
										<field
								            name="lang_scorm_content_<?php echo $language->sef;?>"
								            type="radio"
								            default="0"
								            label="<?php echo $language->title;?> Use SCORM Library"
								            
								            showon="lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
								         >
								                <option value="0">No</option>
								                <option value="1">Yes</option>
								        </field>

								        <field
								            name="lang_scorm_id_<?php echo $language->sef;?>"
								            label="<?php echo $language->title;?> SCORM Package"
								            type="scormselector"
								            showon="lang_scorm_content_<?php echo $language->sef;?>:1[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
								            header="--Select SCORM Content--"
								        />

								        <field
								            name="lang_scorm_start_button_<?php echo $language->sef;?>"
								            type="radio"
								            default="1"
								            label="<?php echo $language->title;?> Use Start Button"
								            
								            showon="lang_scorm_content_<?php echo $language->sef;?>:1[AND]lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1"
								         >
								                <option value="0">No</option>
								                <option value="1">Yes</option>
								        </field>
								    <?php } ?>

								<?php
							}
						?>
					</fieldset>

				<?php


				$newField = ob_get_clean();
				$form->setField(new SimpleXMLElement($newField));
			} else if ($view == "coursescategory") {
				ob_start();

				?>

					<fieldset
						name='multilanguage'
						label='Multilingual Overrides'
						class='tab-pane'
					>
						<!--  This is not stored -->
						<field
							name='lang_select'
							label='Language'
							type='sql'
							query='SELECT * FROM joom_languages WHERE published = 1'
							key_field='lang_code'
							value_field='title'
							description='Only one override is shown at a time to reduce clutter.&lt;br&gt;Overrides can be set for every language.'
						/>

						<?php

							$db = JFactory::getDBO();
							$query = "SELECT * FROM joom_languages WHERE published = 1";
							$db->setQuery($query);
							$languages = $db->loadObjectList();
							foreach ($languages as $language) {
								?>
									<field
										name="lang_use_override_<?php echo $language->sef;?>"
										label='Enable <?php echo $language->title;?> Override'
										type='radio'
										class="radio btn-group"
										default="0"
										showon='lang_select:<?php echo $language->lang_code;?>'
									>
										<option value="0">No</option>
										<option value="1">Yes</option>
									</field>

									<field
										name='lang_title_<?php echo $language->sef;?>'
										label='Title for <?php echo $language->title;?>'
										type='text'
										showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									/>

									<field
										name='lang_description_<?php echo $language->sef;?>'
										label='Description for <?php echo $language->title;?>'
										type='editor'
										showon='lang_select:<?php echo $language->lang_code;?>[AND]lang_use_override_<?php echo $language->sef;?>:1'
									/>
								<?php
							}
						?>
					</fieldset>

				<?php

				$newField = ob_get_clean();
				$form->setField(new SimpleXMLElement($newField));

			}
		}
	}

	public function onUserAfterDelete($user, $success, $msg) {

		if($success) {
			$id = $user['id'];
			if($id) {
				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_course_purchases WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();
			}
		}
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		$params = new stdClass();
		if ($option == 'com_users' && $view == 'axsusers' && $app->isAdmin()) {
			$params->eventName = 'User Delete';
			$params->description = $user['name'].' ['.$user['id'].']';
			AxsTracking::sendToPendo($params);
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}
	}
}
