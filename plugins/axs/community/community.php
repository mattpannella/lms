<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/20/16
 * Time: 4:51 PM
 */

defined('_JEXEC') or die;

/**
 * @package     Joomla.plugin
 * @subpackage  System.axslibs
 */
class plgAxsCommunity extends JPlugin {


	private static $default_fields = array (
		'FIELD_GIVENNAME' => 'first',
		'FIELD_FAMILYNAME' => 'last',
		'FIELD_ADDRESS' => 'address',
		'FIELD_CITY' => 'city',
		'FIELD_STATE' => 'state',
		'FIELD_COUNTRY' => 'country',
		'FIELD_ZIP' => 'zip',
		'FIELD_MOBILE' => 'phone'
	);

	public function onSubscriptionPlay($sub) {
			
		$brand = AxsBrands::getBrand();
		
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();

		//give the user all community groups for plan in one query.
		$plan = $sub->getPlan();
		if($plan->community_groups) {

			$community_groups = json_decode($plan->community_groups);
			$query = "REPLACE INTO `#__community_groups_members` (`groupid`,`memberid`,`approved`,`permissions`) VALUES ";
			$values = array();
			foreach ($community_groups as $cg) {
				//make sure it's not id 0.
				
				if($cg) {
					$values[] = '(' . $db->q($cg) . ',' . $db->q($sub->user_id) . ', 1, 0)';
				}
			}

			if(count($values)) {
				$query .= implode(', ', $values);
				$db->setQuery($query);
				$db->execute();
			}

		}
		

		//now check if this user already exists in db.
		$sql = 'SELECT COUNT(*) FROM #__community_users WHERE userid=' . $sub->user_id;
		$db->setQuery($sql);
		if(!$db->loadResult()) {		

			//append the default fields with any custom fields added to the form
			//var_dump($plan);
			$fieldIds = json_decode($plan->reg_fields);
			//$ids = implode(',', $fieldIds);
			/*$query = $db->getQuery(true);
			$query
	            ->select(array(
	                $db->quoteName('name'),
	                $db->quoteName('field_mapping')
	            ))
	            ->from($db->quoteName('axs_pay_subscription_reg_fields'))
	            ->where(array(
	                $db->quoteName('published') . '=1',
	                "(FIND_IN_SET(" . $db->quoteName('id') . ", " . $db->quote($ids) . ") > 0)",
	                $db->quoteName('field_mapping') . '!=' . $db->quote(""),
	                $db->quoteName('field_mapping') . ' IS NOT NULL'
	            ));
			$db->setQuery($query);
			$fields = $db->loadObjectList();*/

			/*$formFields = array();
			foreach($fields as $f) {
				$formFields[$f->field_mapping] = $f->name;
			}
			$formFields += self::$default_fields;*/

			//grab the community fields
			/*$sql = "SELECT id, fieldcode FROM #__community_fields WHERE fieldcode !='';";
			$db->setQuery($sql);
			$fields = $db->loadObjectList();
			$communityFields = array();
			foreach ($fields as $f) {
				$communityFields[$f->fieldcode] = $f->id;
			}*/

			//grab all the field values from the form.

			
			//create a user row for them
			$sql = "INSERT IGNORE INTO #__community_users (userid) VALUES (".(int)$sub->user_id.");";
			$db->setQuery($sql);
			$db->execute();

			//actually give the user all the community values we can.
			$query = "INSERT IGNORE INTO #__community_fields_values(user_id, field_id, `value`, `access`) VALUES ";
			$values = array();
			foreach($fieldIds as $id) {
				$field_id = (int)$id;
				$fieldParams = AxsHTML::getFieldById($field_id);		
				$field_value = null;

				$is_checkbox_or_list_fieldtype = $fieldParams->type == 'list' || $fieldParams->type == 'checkbox';
				if($is_checkbox_or_list_fieldtype) {
					// Use the $_REQUEST field instead of JInput for
					// multiselect fields because more than one parameter
					// may be defined for a given field. If more than one parameter
					// is defined, it needs to have '[]' appended to it to signify
					// it should be parsed as an array
					// https://www.php.net/manual/en/faq.html.php#faq.html.arrays
					// https://stackoverflow.com/questions/3980228/multiple-http-get-parameters-with-the-same-identifier
					$field_value = $_REQUEST['field'.$field_id];
					$field_value = implode(',', $field_value);
				} else {
					$field_value = $app->input->get('field'.$field_id, 0, 'STRING');
				}

				if($fieldParams->type == 'url') {
					if($field_value[1]) {
						$field_value = $field_value[0].$field_value[1];
					} else {
						$field_value = '';
					}						
				}

				if($field_value) {
					$values[] = "($sub->user_id, $field_id, ".$db->q($field_value).", 40)";
				}				
			}

			$defaultFormFields = self::$default_fields;
			foreach($defaultFormFields as $key => $value) {
				$id = AxsHTML::getFieldByFieldCode($key)->id;
				$field_value = $app->input->get($value, 0, 'STRING');
				if($field_value) {
					$values[] = "($sub->user_id, $id, ".$db->q($field_value).", 40)";
				}
			}

			if(count($values)) {
				$query .= implode(', ', $values);
				$db->setQuery($query);
				$db->execute();
			}
		}

		if($plan->community_groups) {
			$query = $db->getQuery(true);
			$query->select($db->quoteName('groups'))
				->from($db->quoteName('#__community_users'))
				->where($db->quoteName('userid') .'='. $db->quote($sub->user_id))
				->limit(1);
			$db->setQuery($query);
			$result = $db->loadObject();

			if($result->groups) {
				$groupsArray   = explode(',',$result->groups);
				$groupsMerged  = array_unique(array_merge($groupsArray, $community_groups));
			} else {
				$groupsMerged  = array_unique($community_groups);
			}
			
			$communityUser = new stdClass();
			$communityUser->userid = $sub->user_id;
			$communityUser->groups = $newGroupsList;
			$db->updateObject('#__community_users',$communityUser,'userid');
		}
	}

	public function onSubscriptionPause($sub) {
		$brand = AxsBrands::getBrand();
		
		$plan = $sub->getPlan();
		$community_groups = json_decode($plan->community_groups);
		foreach($community_groups as $cg) {
			if($cg) {
				$db = JFactory::getDBO();
				$query = "DELETE FROM #__community_groups_members WHERE groupid=".(int)$cg." AND memberid=".(int)$sub->user_id.";";
				$db->setQuery($query);
			}
		}
	}

	public function onSubscriptionStop($sub) {
		$brand = AxsBrands::getBrand();
		
		$plan = $sub->getPlan();
		$community_groups = json_decode($plan->community_groups);
		foreach($community_groups as $cg) {
			if($cg) {
				$db = JFactory::getDBO();
				$query = "DELETE FROM #__community_groups_members WHERE groupid=".(int)$cg." AND memberid=".(int)$sub->user_id.";";
				$db->setQuery($query);
			}
		}
	}

	public function onContentPrepareForm($form, $data) {
		$brand = AxsBrands::getBrand();
		
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$view = $app->input->get('view');
		if ($option == 'com_converge') {
			$newField = null;
			if ($view == 'plan') {

				ob_start();
				?>
					<fieldset 
						name="community" 
						label="Community Options"
						class="tab-pane"
					>
						<field 
							name="community_groups"
							type="sql"
							query="SELECT * FROM #__community_groups WHERE published = 1 ORDER BY name"
							key_field="id"
							value_field="name"
							label="Add user to these Community Group(s)"
							class="big-multi"
							multiple="true"
						>
							<option value="">--None--</option>
						</field>
					</fieldset>

				<?php
				$newField = ob_get_clean();
				
			} elseif ($view == 'registration_field') {
				ob_start();
				?>
					<fieldset
						name="community" 
						label="Community Options"
					>
						<field 
							name="field_mapping"
							type="sql"
							query="SELECT * FROM #__community_fields WHERE fieldcode != ''"
							key_field="fieldcode"
							value_field="name"
							label="Corresponding community field"
						>
							<option value="">--None--</option>
						</field>
					</fieldset>
				<?php
				$newField = ob_get_clean();				
			}

			if ($newField) {
				$xmlElement = new SimpleXMLElement($newField);
				$form->setField($xmlElement);
			}
		}
	}
}