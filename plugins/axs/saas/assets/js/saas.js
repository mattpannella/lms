var server_options = {
		'eshop': false,
		'lms': false,
		'community': false,
		'forum': false,
		'events': false,
		'business_directory': false,
		'boards': false,
		'blog': false,
		'media': false,
		'reporting': false,
		'polls': false,
		'contract_signing': false,
		'subscriptions': false,
		'virtual_classroom': false
	};

var current_step = 1;
var max_steps = 3;

var server_settings = null;
var subscription_id = null;
var total_steps = null;

var progress_error = false;


sendAjax(
	"getRootDomainName",
	{},
	function(response) {
		if (response) {
			try {
				window.root_domain = JSON.parse(response)["domain"];

			} catch (e) {
				console.log(response);
			}
		}
	}
);


//Used to stop the status box from displaying fun messages if there is an error.

$('document').ready( function() {

	var header_bar = document.getElementById("header_bar");
	if (header_bar) {
		header_bar.className += " hide-on-build";
	}



	courseProg(course_prog);

	//Move the new builder forms onto the original page.
	document.getElementById("appForm").append(document.getElementById("app-builder"));
	document.getElementById("serverForm").append(document.getElementById("server-builder"));
	document.getElementById("saasSteps").append(document.getElementById("saas-steps"));

	//Let the necessary elements now be visibile.

	$("#app-builder").show();
	$("#appForm").show();
	$("#server-builder").show();
	$("#saas-builder").show();
	$("#saas-steps").show();

	$(".app-out").draggable({
		revert: "invalid",
		containment: "document",
		helper: "clone",
		cursor: "move",
		start: function() {
			$(this).addClass("app-selected");
		},
		stop: function() {
			if (!$(this).hasClass("ui-draggable-disabled")) {
				$(this).removeClass("app-selected");
			}
		}
	});

	$(document).on('revert', '.app-out', function() {
		console.log("stop");
		$(this).removeClass("app-selected");
	});

	function addCommas($number) {
		return $number.toLocaleString('en-US');
	}

	$(".cloud").droppable({
		accept: ".app-out",
		classes: {
	        "ui-droppable-active": "ui-state-active",
	        "ui-droppable-hover": "ui-state-hover"
      	},
		drop: function(event, ui) {
			var item = ui.draggable;
			var setting = $(item).data('setting');
			var name = $(item).data('name');

			var newApp = item.clone();

			changeOptions("add", setting);

			$(newApp).removeClass("app-out");
			$(newApp).addClass("app-in");
			$(newApp).removeClass("app");
			$(newApp).addClass("app-small");
			$(newApp).removeClass("app-selected");
			$(newApp).draggable({
				revert: "invalid",
				containment: "document",
				cursor: "move"
			});
			if($('.landing-zone1').find('div').length < 5) {
				$('.landing-zone1').append(newApp);
			} else if($('.landing-zone2').find('div').length < 11) {
				$('.landing-zone2').append(newApp);
			} else if($('.landing-zone3').find('div').length < 14) {
				$('.landing-zone3').append(newApp);
			} else {
				$('.landing-zone3').append(newApp);
			}
			$(item).addClass("app-selected");
			$(item).draggable("disable");

			$(this).css('background-image', 'url("https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/tovuti_cloud_dark.png")');
			updateTotal();
		},
		activate: function(event, ui) {
			$(this).css('background-image', 'url("https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/tovuti_cloud_blue.png")');
		},
		deactivate: function(event, ui) {
			$(this).css('background-image', 'url("https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/tovuti_cloud_dark.png")');
		}
	});

	$(".app-container").droppable({
		accept: ".app-in",
		classes: {
        "ui-droppable-active": "ui-state-active",
        "ui-droppable-hover": "ui-state-hover"
      	},
		drop: function(event, ui) {
			var item = ui.draggable;
			var setting = $(item).data('setting');
			var name = $(item).data('name');

			changeOptions("remove", setting);

			$(item).remove();
			$('.app-out').each( function(){
				if($(this).data('name') == name) {
					$(this).removeClass('app-selected');
					$(this).draggable('enable');
				}
			});

			updateTotal();
		},
		activate: function(event, ui) {
			$(this).css('border', 'dashed 2px #2caae2');
		},
		deactivate: function(event, ui) {
			$(this).css('border', 'dashed 2px transparent');
		}
	});

	function getStepText(step) {
		var text = [];
		switch(step) {
			case 1:
				text['title'] = 'apps';
				text['description'] = 'Drag Apps Into Your TOVUTI Cloud';
				break;

			case 2:
				text['title'] = 'server';
				text['description'] = 'Configure Your TOVUTI Server & Domain Name';
				break;

			case 3:
				text['title'] = 'checkout';
				text['description'] = 'Register & Checkout. Then Launch Your TOVUTI!';
				break;
		}

		return text;
	}

	function changeOptions(action, setting) {
		console.log(action + " " + setting);

		if (action === "add") {
			server_options[setting] = true;
		} else if (action === "remove") {
			server_options[setting] = false;
		}

		console.log(server_options);
	}

	$('.saas-nav').click( function() {
		var data_step = $(this).data('step');

		if ($.isNumeric(data_step)) {
			current_step  = parseInt(data_step);
		} else {
			switch (data_step) {
				case "next":
					current_step += 1;
					if (current_step > max_steps) {
						current_step = max_steps;
					}
					break;
				case "back":
					current_step -= 1;
					if (current_step < 1) {
						current_step = 1;
					}
			}
		}

		var stepText = getStepText(current_step);

		$('.step-title-text, .saas-title-description').fadeOut(300);
		$('.step-title-text').hide().html(stepText['title']);
		$('.saas-title-description').hide().html(stepText['description']);
		$('.saas-step' + current_step + ', .step-title-text, .saas-title-description').delay(400).fadeIn(300);

		$("#saas-nav-next, #saas-nav-back").fadeOut(300);

		$("#serverForm, #appForm, #registrationForm").fadeOut(300);

		switch (current_step) {
			case 1:
				$("#appForm, #saas-nav-next").delay(400).fadeIn(300);
				break;
			case 2:
				$("#saas-nav-back, #saas-nav-next, #serverForm").delay(400).fadeIn(300);
				break;
			case 3:
				$("#registrationForm, #saas-nav-back").delay(400).fadeIn(300);
				break;
		}
	});

	$(document).on('click', '.totals-stats', function() {
        var lower_area = $(this).parents('.stats_bar').find('.totals-lower');
        var icon = $(this).find('.icon');
        console.log(icon);
        lower_area.slideToggle();
        if (icon.hasClass('lizicon-circle-down')) {
            icon.removeClass('lizicon-circle-down');
            icon.addClass('lizicon-circle-up');
        } else {
            icon.addClass('lizicon-circle-down');
            icon.removeClass('lizicon-circle-up');
        }
    });

	var type_pause = null;
	function startTypeTimer(domain) {
		if (type_pause) {
			clearTimeout(type_pause);
		}

		type_pause = setTimeout(
			function() {

				document.getElementById("subdomain_name").value = domain;
				document.getElementById("preview_domain").innerHTML = domain + '.' + root_domain;



				var data = {
					'domain_name': domain
				}

				$.ajax({
			        type: 'POST',
			        url: '/index.php?option=com_axs&task=server.validateDomainName&format=raw',
			        data: data,
			        success: function(response) {

			        	console.log(response);


						var success = JSON.parse(response);
			        	if (!success.success) {
			        		$("#in_use_error").show(250);
			        	} else {
			        		$("#in_use_error").hide(250);
			        	}
			        }
			    });
			}, 1000
		);
	}
    $("#subdomain_name").on("change keyup paste",
    	function() {

    		var current = this.value;
    		var current = current.toLowerCase();

    		var valid = "";

    		//remove any invalid characters.
    		for (var i = 0; i < current.length; i++) {
    			//Only allow lowercase a - z, 0 - 9, and hyphens.
	    		if (current[i].match(/^[0-9a-z\-]/)) {

	    			valid += current[i];
	    		}
    		}

    		var good = false;
    		//Do not allow hyphens at the beginning or the end.

    		while (!good) {

    			if (valid[0] === "-") {
    				good = false;
    				valid = valid.substring(1, valid.length);
    			} else {
    				good = true;
    			}

    			if (valid[valid.length - 1] === "-") {
    				good = false;
    				valid = valid.substring(0, valid.length - 1);
    			} else {
    				good = true;
    			}
    		}


    		//Max length of 32 characters.
    		valid = valid.substring(0, 32);

    		//console.log(valid);

    		startTypeTimer(valid);
    	}
    );

    var gb_slider = document.getElementById("storageSizeSlider");
    var iops_slider = document.getElementById("iopsSlider");

    function updateTotal() {

    	//Update the global variable server_settings
    	createServerSettings();

    	sendAjax(
			"validateAndPrice",
			{
				'settings': server_settings,
			},
			function(response) {
				if (response) {
					try {
						var data = JSON.parse(response);
						var app_price = data.price.app;
						var server_price = data.price.server;
						var volume_price = data.price.volume;
						var total_price = data.price.total;

						$('#your-apps').find('.amount').text(addCommas(parseInt(app_price)));
				    	$('#your-server').find('.amount').text(addCommas(parseInt(server_price + volume_price)));
				    	$('#your-total').find('.amount').text(addCommas(parseInt(total_price)));

						$("#default_amount").val(parseInt(total_price));
						$("#initial_amount").val(parseInt(total_price));

						//console.log(data);
					} catch (e) {
						console.log(response);
					}
				}
			}
		);
    }

    gb_slider.oninput = function() {
		var size = this.value;
		document.getElementById("storageSizeVis").innerHTML = size + " GB";

		var min_iops = size * 2;
		var max_iops = size * 50;

		if (max_iops > 32000) {
			max_iops = 32000;
		}

		if (min_iops > max_iops) {
			min_iops = max_iops;
		}

		iops_slider.setAttribute("min", min_iops);
		iops_slider.setAttribute("max", max_iops);

		document.getElementById("iops-val").innerHTML = iops_slider.value + " MiB/s";
	}

	gb_slider.onchange = function() {
		updateTotal();
	}

	iops_slider.oninput = function() {
		document.getElementById("iops-val").innerHTML = this.value + " MiB/s";
	}

	iops_slider.onchange = function() {
		updateTotal();
	}

    $("#storageSpecs").change(
    	function() {
    		var index = this.options[this.selectedIndex];
    		var min = parseInt(index.getAttribute("data-min"));
    		var max = parseInt(index.getAttribute("data-max"));

    		//Change the GB slider to represent the new min/max values.

    		gb_slider.setAttribute("min", min);
    		gb_slider.setAttribute("max", max);

    		document.getElementById("storageSizeVis").innerHTML = gb_slider.value + " GB";

			//If the volume type is io1, and IOPS needs to be specified.

			var value = this.value;
			if (value === "io1") {
				$("#iopsSliderArea").show(250);
			} else {
				$("#iopsSliderArea").hide(250);
			}

			updateTotal();
    	}
    );

    $("#storageType").change(
    	function() {
    		switch (this.value) {
    			case "dedicated":
    				$("#storageView").show(500);
    				break;
    			case "shared":
    				$("#storageView").hide(500);
    				break;
    		}

    		updateTotal();
    	}
    );

    $("#serverType").change(
    	function() {
    		switch (this.value) {
    			case "dedicated":
    				$("#serverView").show(500);
    				$("#storageType").val("dedicated").prop("selected", true);
    				$("#storageType").parent().parent().hide(500);
    				$("#storageView").show(500);
    				break;
    			case "shared":
    				$("#serverView").hide(500);
    				$("#storageType").parent().parent().show(500);
    				break;
    		}

    		updateTotal();
    	}
    );

	$("#serverLocation").change(
		function() {
			updateTotal();
		}
	);

	$("#serverSpecsDedicated").change(
		function() {
			updateTotal();
		}
	);



    /*$("#buildServerButton").click(
    	function() {

    		$(".hide-on-build").hide(250);
			$(".show-on-build").show(250);

			statusUpdate();

			setInterval(
				function() {
					statusUpdate();
				}, 1000
			);

			runServerProcess();

    	}
    );*/

    updateTotal();
});

var log = null;
var plog = null;
var code = null;

function createServerSettings() {
	var client_name = document.getElementById("client_name").value;
	var template = document.getElementById("template").value;
	var subscription_type = document.getElementById("subscription_type").value;
	var status = document.getElementById("status").value;
	var site_domain = document.getElementById("subdomain_name").value;
	var account_region = document.getElementById("serverLocation").value;
	//var language = document.getElementById("siteLanguage").value;
	var language = "en-GB";
	var site_name = document.getElementById("website_name").value;

	total_steps = 4;


	server_settings = {
		"client_name": client_name,
		"template": template,
		"subscription_type": subscription_type,
		"status": status,
		"account_region": account_region,
		"site_name": site_name,
		"site_domain": site_domain,
		"language": language,
		"server_options": server_options,
		"plan_id": plan_id,
	};

	//console.log(server_settings);

}

function runServerProcess() {

	var runNextStage = function(stage) {

		console.log("Stage: " + stage);

		if (log == null || code == null) {
			sendAjax(
				"getCodeLog",
				null,
				function(response) {
					if (response) {
						var data = JSON.parse(response);
						console.log(data);
						log = data.log;
						plog = data.plog;
						code = data.code;
					}
				}
			);
		}

		switch (parseInt(stage)) {
			case 0:
			case -1:
				sendAjax(
					"createServer",
					{
						'init':'init',
						'subscription': subscription_id
					},
					function(response) {

						var data = null;
						try {
							data = JSON.parse(response);
							console.log(data);
						} catch (e) {
							console.log(response);
							console.log(e.message);
						}

						if (data) {
							if (data.success !== false) {
								runServerProcess();
							} else {
								alert(data.message);
							}
						}
					}
				);

				break;

			case 1:

				sendAjax(
					"createServer",
					{
						'settings': server_settings
					},
					function(response) {

						if (response == "success") {
							runServerProcess();
						} else {
							showError(response);
						}
					}
				);

				break;

			case 2:
				sendAjax(
					"createServer",
					null,
					function(response) {
						if (response == "success") {
							runServerProcess();
						} else {
							showError(response);
						}
					}
				);

				break;

			case 3:
				sendAjax(
					"createServer",
					null,
					function(response) {
						runServerProcess();
					}

				);

				break;
			case 4:
				sendAjax(
					"createServer",
					null,
					function(response) {

					}
				);
				console.log("Done");
				break;
		}
	}

	sendAjax(
		"getStage",
		null,
		function(response) {
        	runNextStage(response);
        }
    );
}

function sendAjax(task, data, success) {
	//console.log(task);
	$.ajax({
        type: 'POST',
        data: data,
        url: "index.php?option=com_axs&task=server." + task + "&format=raw",
        success: success
    });
}

function showError(response) {

	var message = null;
	try {
		var data = JSON.parse(response);
		if (data.success == false) {
			//alert(data.message);
			message = data.message;
		}
	} catch (e) {
		console.log(e);
		message = "There was an unknown error. Please contact an Tovuti tech support.";
	}

	$(".course-progress-bar").hide(500);
	$("#launch_button").remove();
	progress_error = true;

	var status_list = document.getElementById("status-update-list");
	$(status_list).empty();
	message = "Error: " + message;

	var list_element = document.createElement("li");
	list_element.appendChild(document.createTextNode(message));
	status_list.appendChild(list_element);
}

/*
var updateMessages = [
	"Making the world a better place one TOVUTI at a time!",
	"Creating your database...",
	"Filling your database with all your hopes, dreams, and sample data.",
	"Answer a phone call from my mom, be right back.",
	"Ok, I'm back now.",
	"Getting a drink of water...building awesome websites makes me thirsty!",
	"Building your new website...",
	"Fighting radioactive spotted ninjas that are trying to hack your new website!",
	"Radioactive spotted ninjas defeated! Your website remains secure!",
	"That's no moon... it's your TOVUTI.",
	"It's alive! Your TOVUTI is ready! How neat is that?"
];
*/

var updateMessages = [
	"Making the world a better place one TOVUTI at a time!",
	"Paving digital highway to success.",
	"Getting a cup of coffee.",
	"Placing hamster in the wheel.",
	"Cranking it to 11.",
	"Inflating warehouse.",
	"Staying on target.",
	"Filling database with your hopes, dreams, and sample data.",
	"Fighting off radioactive server ninjas.",
	"Turning on the lights.",
	"It's alive! Your TOVUTI is ready! How neat is that?"
];

function buildMessage(index) {

	if (progress_error == true) {
		return;
	}

	if (index < 0 || index >= updateMessages.length) {
		return;
	}

	console.log("Index: " + index);

	return "<li>" + updateMessages[index] + "</li>";
}

var previous_prog = -1;
var previous_status = -1;
//var previous_prog = 0;
var progress = 0;

function statusUpdate() {
	//console.log(log);
	//console.log('status update');
	/*if (log) {
    	$("#status-update").load(log);
    }*/


    //plog = true;
    if (plog) {
    	var progress_update = document.getElementById("progress-update");

    	$(progress_update).load(plog);
    	var progress = parseFloat(progress_update.innerHTML);

    	if (isNaN(progress)) {
    		return;
    	}

    	//console.log(progress);


    	//progress++;

    	var percent = parseInt(100 * (progress / total_steps));

    	if (previous_prog != progress) {
    		//console.log(progress);
    		courseProg(percent);
    		previous_prog = progress;
    		var index = parseInt(percent / 10);

    		if (index != previous_status) {
    			$("#status-update-list").append(buildMessage(index));
    			previous_status = index;
    		}

    		//$("#status-update").load(log);
    	}

    	if (percent >= 100) {
    		game.blockrain('pause');

    		var vis = $("#launch_button").is(":visible");

    		if (!vis) {
	    		var website_name = document.getElementById("website_name").value;
	    		var domain = document.getElementById("subdomain_name").value;

	    		var launch_button = document.getElementById("launch_button");
	    		var launch_text = document.getElementById("launch_button_text");

	    		launch_button.setAttribute("href", "https://" + domain + "." + root_domain);
	    		var btnText = " Launch " + website_name;
	    		launch_text.innerHTML = btnText;

	    		$(launch_button).show(250);

	    		var readyMessage = `
					<div class="blockrain-start">
						<div class="blockrain-start-msg">
							Your TOVUTI is now ready! How neat is that?
						</div>
						<a class="blockrain-btn" href="https://` + domain + `.` + root_domain + `">
							<i class="fa fa-paper-plane"></i> Launch Your Site
						</a>
						<a class="blockrain-btn blockrain-resume-btn">
							<i class="fa fa-refresh"></i> Continue Playing
						</a>
					</div>
				`;
				$('.blockrain-start-holder').html(readyMessage);
				$('.blockrain-start-holder').show();
	    		var progress_icon = document.getElementById("progress_bar_icon");
	    		var progress_text = document.getElementById("progress_bar_text");

	    		progress_icon.className = "fa fa-trophy";
	    		progress_text.innerHTML = " Complete";
	    	}
    	}
    }
}

function buildServerTest() {

	statusUpdate();

	setInterval(
		function() {
			if (previous_prog != 100) {
				statusUpdate();
			}
		}, 3000
	);
}

function buildServer(sub_id) {

	if (sub_id) {
		subscription_id = sub_id;
	}

	$(".hide-on-build").hide(250);
	$(".show-on-build").show(250);
	$("body").addClass('saas-builder-backgound');

	statusUpdate();

	setInterval(
		function() {
			statusUpdate();
		}, 1000
	);

	runServerProcess();
}

var course_prog_start = 0;
var course_prog = 0;

function courseProg (course_prog) {

	if (isNaN(course_prog) || course_prog < 0) {
		course_prog = 0;
	}

	if (course_prog > 100) {
		course_prog = 100;
	}

    $(".progress-bar").animate(
    	{
			width: course_prog +"%"
		},
		{
      		duration: 2000,
      		complete: function () {
        		$(this).removeClass( 'progress-bar-striped' );
      		}
    	}
    );

    $('.course-bar-percentage').each(
    	function () {
      		var $this = $(this);
      		$({ Counter: course_prog_start })
      		.animate({ Counter: course_prog },
      			{
        			duration: 2100,
        			step: function () {
          				$this.text(Math.ceil(this.Counter)+'%');
        			}
      			}
      		);
    	}
    );
    course_prog_start = course_prog;
}


