<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("/var/www/files/creds.php");



/**
 * @package     Joomla.plugin
 */
class plgAxsSaas extends JPlugin {

    public function setupSaas() {
        $option = JFactory::getApplication()->input->get('option');
        $view = JFactory::getApplication()->input->get('view');

        if ($option == 'com_converge' && $view == 'subscription') {

            echo self::getSaasTitle();
            echo self::getAppBuilder();
            echo self::getServerBuilder();
            echo self::getStepNav();
            echo self::getBuildView();

            echo '<link rel="stylesheet" href="/plugins/axs/saas/assets/css/saas.css">
                  <script src="/plugins/axs/saas/assets/js/saas.js?v=600"></script>';

            return true;

        }
    }

    public function getSaasTitle() {
        ob_start();

        ?>

            <?php /*  ?>
                <div id='testbuttonhide' class='btn btn-success'>Hide</div>
                <div id='testbuttonbuild' class='btn btn-success'>Build</div>

                <script>
                    var hidden = false;
                    $("#testbuttonhide").click(function() {

                        if (!hidden) {
                            $(".hide-on-build").hide(250);
                            $(".show-on-build").show(250);
                            $("#testbuttonhide").html("Show");
                            hidden = true;
                        } else {
                            $(".hide-on-build").show(250);
                            $(".show-on-build").hide(250);
                            $("#testbuttonhide").html("Hide");
                            hidden = false;
                        }
                    });

                    $("#testbuttonbuild").click(
                        function() {
                            //buildServerTest();
                            runServerProcess();
                        }
                    );
                </script>
            <?php  */ ?>

            <div id="saas-builder" class="hide-on-build saas-builder" style="display:none;">
                <?php echo self::buildStatBars(); ?>

                <div class="saas-title">Build Your TOVUTI<sup>&trade;</sup>
                    <span class="step-title-text">Apps</span>
                    <span class="saas-title-description">Drag Apps Into Your TOVUTI Cloud<span>
                </div>

                <div class="row saas-top-nav-container hide-on-build">
                    <div class="col-md-4">
                        <div class="saas-nav saas-nav-top" data-step="1">
                            <span class="step-small-text">step</span>
                            <span class="step-big-text">1</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="saas-nav saas-nav-top" data-step="2">
                            <span class="step-small-text">step</span>
                            <span class="step-big-text">2</span>oo
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="saas-nav saas-nav-top" data-step="3">
                            <span class="step-small-text">step</span>
                            <span class="step-big-text">3</span>
                        </div>
                    </div>
                </div>
            </div>



        <?php
        return ob_get_clean();
    }

    public function getServerOptions() {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM axs_saas_server_options WHERE published = 1 ORDER BY ordering ASC LIMIT 1";
        $db->setQuery($query);
        $server = $db->loadObject();
        $options = new stdClass();

        //get storage options
        $storageOptions = '';
        $storageData = json_decode($server->storage);
        $storageCount = count($storageData->type);

        $min = (int)$storageData->min[0];
        $max = (int)$storageData->max[0];

        ob_start();
        ?>

            <div>
                <input id="storageSizeSlider" type="range" value="<?php echo $min;?>" min="<?php echo $min;?>" max="<?php echo $max;?>" class="slider" />
                <div class="clearfix"></div>
                <div id="storageSizeVis" class=""><?php echo $min;?> GB</div>
            </div>

        <?php
        $storageSize = ob_get_clean();

        ob_start();
        for ($i = 0; $i < $storageCount; $i++) {
            ?>
                <option
                    value="<?php echo $storageData->volume_alias[$i]; ?>"
                    data-price="<?php echo $storageData->price[$i]; ?>"
                    data-price_iops="<?php echo $storageData->price_iops[$i];?>"
                    data-min="<?php echo $storageData->min[$i]?>"
                    data-max="<?php echo $storageData->max[$i]?>"
                >
                    <?php echo $storageData->type[$i]; ?>
                </option>
            <?php
        }

        $storageOptions = ob_get_clean();

        $serverData = json_decode($server->server);
        $serverCount = count($serverData->type);

        switch ($serverData->type[$i]) {
            case 'shared':
                ob_start();
                for ($i = 0; $i < $serverCount; $i++) {
                    if ($serverData->type[$i] == "shared") {
                        ?>

                            <option
                                value="<?php echo $serverData->server_alias[$i]; ?>"
                                data-price="<?php echo $serverData->price[$i]; ?>">
                                    <?php echo $serverData->specs[$i]; ?>
                            </option>

                        <?php
                    }
                }

                $serverSpecsShared = ob_get_clean();

                break;
            case 'dedicated':
                ob_start();
                for ($i = 0; $i < $serverCount; $i++) {
                    if ($serverData->type[$i] == "dedicated") {
                        ?>
                            <option
                                value="<?php echo $serverData->server_alias[$i]; ?>"
                                data-price="<?php echo $serverData->price[$i]; ?>"
                            >
                                    <?php echo $serverData->specs[$i]; ?>
                            </option>
                        <?php
                    }
                }
                $serverSpecsDedicated = ob_get_clean();

                break;
        }

        //get location options
        $locationOptions = '';
        $locationData = json_decode($server->location);
        $locationCount = count($locationData->region_name);

        ob_start();
            for ($i = 0; $i < $locationCount; $i++) {
                ?>
                    <option
                        value="<?php echo $locationData->region_aws[$i];?>"
                        data-price="<?php echo $locationData->price_increase[$i]; ?>">
                            <?php echo $locationData->region_name[$i]; ?>
                    </option>
                <?php
            }
        $locationOptions = ob_get_clean();

        $options->title = $server->title;
        $options->storage = $storageOptions;
        $options->location = $locationOptions;
        $options->server_shared = $serverSpecsShared;
        $options->server_dedicated = $serverSpecsDedicated;
        $options->storage_size = $storageSize;

        return $options;
    }

    public function getApps() {

        $db = JFactory::getDbo();
        $query = "SELECT * FROM axs_saas_apps WHERE published = 1 ORDER BY ordering ASC";
        $db->setQuery($query);
        $apps = $db->loadObjectList();

        ob_start();

        foreach ($apps as $app) {
            $params = json_decode($app->params);
            ?>
                <div class="app-item">
                    <div
                        class="app app-out"
                        style="background: <?php echo $params->icon_background_color;?>"
                        data-price="<?php echo $app->price; ?>"
                        data-name="<?php echo $app->title; ?>"
                        data-setting="<?php echo $app->setting; ?>"
                    >

                        <?php
                            if ($params->icon_type == 'icon') {
                        ?>
                                <span class="app-icon <?php echo $params->app_icon; ?>" style="color: <?php echo $params->icon_color; ?>"></span>
                        <?php
                            } else {
                        ?>
                                <img class="icon-image" src="<?php echo $params->app_image; ?>"/>
                        <?php
                            }
                        ?>
                    </div>

                    <div class="app-price">$<?php echo $app->price; ?></div>
                    <div class="app-title"><?php echo $app->title; ?></div>
                </div>
            <?php

        }

        return ob_get_clean();
    }

    public function getServerBuilder() {
        $config = dbCreds::getAwsConfig();

        $root_domain = $config->root_domain;

//        echo '<pre>';
//        var_export( get_defined_vars() );
//        echo '</pre>';
//        die();
        $showServerOptions = false;
        $server = self::getServerOptions();
        $admin = false;
        $user = JFactory::getUser();
        $superadmin = $user->authorise('core.admin');
        $usergroups = JAccess::getGroupsByUser($user->id);
        if(in_array(32, $usergroups) || in_array(50, $usergroups)) {
            $admin = true;
        }
        if (!$superadmin && !$admin) {
            return "You do not have access.";
        }
        ob_start();
        ?>

        <div id="server-builder" class="form form-horizontal saas-view hide-on-build" style="display:none;">
            <div class="sub-heading"><?php echo $server->title; ?></div>
            <?php if ($superadmin || $admin) { ?>
            <div class="form-group">
                <label class="col-md-4 control-label">Client Name <span class="required">*</span></label>
                <div class="col-md-4">
                    <input id="client_name" type="text" class="form-control" name="client_name" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Template <span class="required">*</span></label>
                <div class="col-md-4">
                    <select id="template" class="form-control" name="template">
                        <?php
                            $templates = AxsDbAccess::getTovutiTempates();
                            foreach($templates as $template) {
                                $templateParams = AxsClients::decryptClientParams($template->dbparams);
                                $templateData = new stdClass();
                                $templateData->id = $template->id;
                                $templateData->dbname = $templateParams->dbname;
                                $templateData->title  = $template->client_name;
                                $templateData->symlink_path  = $template->encode;
                                $key = AxsKeys::getKey('clients');
                                $encryptedParams = base64_encode(AxsEncryption::encrypt($templateData, $key));
                                echo '<option value="'.$encryptedParams.'">'.$template->client_name.'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Subscription Type <span class="required">*</span></label>
                <div class="col-md-4">
                    <select id="subscription_type" class="form-control" name="subscription_type">
                        <option value="None">None</option>
                        <option value="Tovuti Core">Tovuti Core</option>
                        <option value="Tovuti Workspace">Tovuti Workspace</option>
                        <option value="Tovuti Pro">Tovuti Pro</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Status <span class="required">*</span></label>
                <div class="col-md-4">
                    <select id="status" class="form-control" name="status">
                        <option value="active">Active</option>
                        <option value="active (onboarding)">Active (Onboarding)</option>
                        <option value="inactive">Inactive</option>
                        <option value="trial">Trial</option>
                        <option value="test">Test</option>
                    </select>
                </div>
            </div>
            <?php } ?>
            <div class="form-group">
                <label class="col-md-4 control-label">Website Name <span class="required">*</span></label>
                <div class="col-md-4">
                    <input id="website_name" type="text" placeholder="My Website" class="form-control" name="websiteName" />
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Sub Domain Name <span class="required">*</span></label>
                <div class="col-md-4">
                    <input id="subdomain_name" type="text" placeholder="my" class="form-control" name="subdomainName" />
                </div>
                <div class="col-md-4" id="preview_domain">my.<?php echo $root_domain; ?></div>

            </div>
            <div id="in_use_error" class="form-group" style="display:none;">
                <div class="col-md-4"></div>
                <div class="col-md-4 error-message">
                    This domain name is already in use.<br>Please select another.
                </div>
            </div>

            <?php if($showServerOptions) { //This will remove the server setting options to be used in a futer build ?>
            <div class="form-group">
                <label class="col-md-4 control-label">Server Type</label>
                <div class="col-md-4">
                    <select id="serverType" class="form-control" name="serverType">
                        <option value="shared">Shared</option>
                        <option value="dedicated">Dedicated</option>
                    </select>
                </div>
            </div>

            <div id="serverView" style="display:none;">
                <?php /*
                <div class="form-group" style="display:none;">
                    <label class="col-md-4 control-label">Server Specs</label>
                    <div class="col-md-4">
                        <select class="form-control" name="serverSpecsShared">
                            <?php echo $server->server_shared; ?>
                        </select>
                    </div>
                </div>
                */?>

                <div class="form-group">
                    <label class="col-md-4 control-label">Server Specs</label>
                    <div class="col-md-4">
                        <select id="serverSpecsDedicated" class="form-control" name="serverSpecsDedicated">
                            <?php echo $server->server_dedicated; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Server Location</label>
                    <div class="col-md-4">
                        <select id="serverLocation" class="form-control" name="serverLocation">
                            <?php echo $server->location; ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Storage Type</label>
                <div class="col-md-4">
                    <select id="storageType" class="form-control" name="serverType">
                        <option value="shared">Shared</option>
                        <option value="dedicated">Dedicated</option>
                    </select>
                </div>
            </div>

            <div id="storageView" style="display:none;">
                <div class="form-group">
                    <label class="col-md-4 control-label">Storage Specs</label>
                    <div class="col-md-4">
                        <select id="storageSpecs" class="form-control" name="storageSpecs">
                            <?php echo $server->storage; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4 control-label">Storage Size (GB)</label>
                    <div class="col-md-4">
                        <?php echo $server->storage_size; ?>
                    </div>
                </div>

                <div id="iopsSliderArea" class="form-group" style="display:none;">
                    <label class="col-md-4 control-label">IOPS</label>
                    <div class="col-md-4">
                        <input id="iopsSlider" min="60" max="3000" value="60" type="range" class="slider">
                        <div class="clearfix"></div>
                        <div id="iops-val">60 MiB/s</div>
                        <div class="clearfix"></div>
                        <div style="font-size: 12px; color: gray">In/Out reads Per Second</div>
                    </div>
                </div>
            </div>


            <div id="wrong_size_error" class="form-group" style="display:none;">
                <div class="col-md-4"></div>
                <div class="col-md-4 error-message">
                    The volume size must be between 500 and 16384
                </div>
            </div>
            <?php } else { ?>
                <input id="serverType"  class="form-control" name="serverType" value="shared" type="hidden" />
                <input id="storageType" class="form-control" name="serverType" value="shared" type="hidden" />
                <input id="serverSpecsDedicated" class="form-control" name="serverSpecsDedicated" value="m4.large" type="hidden" />
                <input id="serverLocation" class="form-control" name="serverLocation" value="us-east-1" type="hidden" />
                <input id="storageSpecs" class="form-control" name="storageSpecs" value="gp2" type="hidden" />
                <input id="iopsSlider" class="form-control" value="60" type="hidden" />
                <input id="storageSizeSlider" class="form-control" value="30" type="hidden" />
            <?php } ?>


            <?php /*
            <div class="col-md-6"></div>
            <div id="buildServerButton" class="btn btn-primary">
                Build
            </div>
            */ ?>

            <?php
                $admin = false;
                $user = JFactory::getUser();
                $superadmin = $user->authorise('core.admin');
                $usergroups = JAccess::getGroupsByUser($user->id);
                if(in_array(32, $usergroups)) {
                    $admin = true;
                }
                if ($superadmin || $admin) {
            ?>
                    <br><br>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Admin Launch Site</label>
                        <div id="superadmin_build_button" class="col-md-4 btn btn-primary" style="max-width: 120px">Build</div>
                    </div>
                    <script>
                        $("#superadmin_build_button").click(
                            function() {
                                createServerSettings();
                                buildServer();
                            }
                        );
                    </script>

            <?php
                }
            ?>
        </div>
        <?php

        return ob_get_clean();
    }

    public function getAppBuilder() {
        ob_start();
        ?>


        <!-- <div id="app-builder" class="<?php /* saas-step1 */?> saas-view hide-on-build" style="display:none;">
            <div class="container-fluid">
                <div class="row">
                    <?php //echo self::getApps(); ?>
                </div> -->
        <div id="app-builder" class="<?php /* saas-step1 */?> saas-view hide-on-build" style="display:none;">
            <div class="cloud">
                <div class="landing-zone1"></div>
                <div class="landing-zone2"></div>
                <div class="landing-zone3"></div>
            </div>
            <div class="app-container">
                <?php echo self::getApps();  ?>
            </div>
        </div>
        <?php

        return ob_get_clean();
    }

    public function getStepNav() {
        ob_start();
        ?>

            <div id="saas-steps" style="display:none;">
                <div class="clearfix"></div>
                <span id="saas-nav-back"class="pull-left saas-nav btn btn-default" data-step="back" style="display:none;"><span class="lizicon-arrow-left"></span> Back</span>
                <span id="saas-nav-next"class="pull-right saas-nav btn btn-default" data-step="next">Next <span class="lizicon-arrow-right"></span></span>
            </div>

        <?php
        return ob_get_clean();
    }

    public function getBuildView() {

        //unset($_SESSION['aws_creation']);

        ob_start();
        ?>

            <div class="row show-on-build" style="display:none; width: 100%">
                <script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
                <div class="container-fluid">
                    <div class="col-md-8 server-builder-title">We're building your TOVUTI!</div>
                    <div class="clearfix"></div>
                    <div class="row course-progress-bar" style="margin-bottom: 15px;margin-top: 10px;">
                        <div class="col-md-12 no-padding">
                            <span class="pull-left btn btn-primary"> <span id="progress_bar_icon" class="fa fa-cog fa-spin"></span><span id="progress_bar_text"> Building</span></span>
                            <div class="course-bar-percentage pull-left"></div>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" style="transition: all ease;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div id="blocks-game" class="server-builder"></div>
                        </div>
                        <div class="col-md-5">
                            <div id="status-update" class="server-builder">
                                <b>STATUS REPORT:</b>
                                <ul id="status-update-list" style="margin-top: 10px;"></ul>
                                <center><a id="launch_button" href="https://www.tovuti.io" style="display:none;"><div style="margin-top: 15px;" class="btn btn-success btn-lg"><span class="fa fa-paper-plane"></span><span id="launch_button_text"> Launch Your Tovuti</span></div></a></center>
                            </div>
                            <div id="progress-update" class="server-builder" style="display:none;"></div>
                        </div>
                    </div>
                </div>


                <script src="/media/games/blocks/blocks.js"></script>
                <link rel="stylesheet" href="/media/games/blocks/blocks.css">

                <script>

                    var checkVisible = setInterval(
                        function() {
                            var visible = $("#blocks-game").is(":visible");
                            if (visible) {
                                //console.log("visible");
                                clearInterval(checkVisible);

                                setTimeout(
                                    startGame,
                                    250
                                );
                            }
                        }, 100
                    );
                    var gamePlaying = false;
                    var game =  $("#blocks-game");
                    function startGame() {

                        game.blockrain({
                            autoplay: false, // Let a bot play the game
                            autoplayRestart: true, // Restart the game automatically once a bot loses
                            showFieldOnStart: true, // Show a bunch of random blocks on the start screen (it looks nice)
                            theme: 'candy', // The theme name or a theme object
                            blockWidth: 28, // How many blocks wide the field is (The standard is 10 blocks)
                            autoBlockWidth: false, // The blockWidth is dinamically calculated based on the autoBlockSize. Disabled blockWidth. Useful for responsive backgrounds
                            autoBlockSize: 24, // The max size of a block for autowidth mode
                            difficulty: 'normal', // Difficulty (normal|nice|evil).
                            speed: 9, // The speed of the game. The higher, the faster the pieces go.

                            // Copy
                            playText: 'Enjoy a game (on us) while we build your Tovuti!',
                            playButtonText: 'Play',
                            gameOverText: 'Game Over',
                            restartButtonText: 'Play Again',
                            scoreText: 'Score',

                            // Basic Callbacks
                            onStart: function(){
                                gamePlaying = true;
                            },
                            onRestart: function(){
                                gamePlaying = true;
                            },

                            onGameOver: function(score){
                                gamePlaying = false;
                            },

                            // When a line is made. Returns the number of lines, score assigned and total score
                            onLine: function(lines, scoreIncrement, score){}
                        });
                    }

                    $(document).on('click','.blockrain-resume-btn', function() {
                        $('.blockrain-start-holder').hide();
                        game.blockrain('resume');
                    });

                </script>

                <div class="clearfix"></div>


            </div>




        <?php
        return ob_get_clean();
    }

    public function buildStatBars() {
        ob_start();

        ?>
            <div class="row totals">
                <div class="col-md-4 saas-apps">
                    <div class="stats_bar" style="background-color: #2caae2;">
                        <div class="totals-inner">
                            <div class="totals-icon">
                                <span class="lizicon-database" style="font-size: 35px;"></span>
                            </div>
                            <div class="totals-data" id="your-apps">
                                $<span class="amount">0</span>
                            </div>
                            <div class="totals-footer">
                                <span class="expand_button pull-left totals-stats" title="Expand for More Info">
                                <span class="lizicon-circle-down icon"></span>
                                </span>
                                Your Apps
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 saas-apps">
                    <div class="stats_bar" style="background-color: #f8974b;">
                        <div class="totals-inner">
                            <div class="totals-icon"><span class="lizicon-drive"></span></div>
                            <div class="totals-data" id="your-server">$<span class="amount">500</span></div>
                            <div class="totals-footer">
                                <span class="expand_button pull-left totals-stats" title="Expand for More Info">
                                <span class="lizicon-circle-down icon"></span>
                                </span>
                                Your Server
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 saas-apps">
                    <div class="stats_bar" style="background-color: #57b947;">
                        <div class="totals-inner">
                            <div class="totals-icon"><span class="lizicon-cloud-check"></span></div>
                            <div class="totals-data" id="your-total">$<span class="amount">500</span></div>
                            <div class="totals-footer">
                                <span class="expand_button pull-left totals-stats" title="Expand for More Info">
                                <span class="lizicon-circle-down icon"></span>
                                </span>
                                Your Total Monthly Cost
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <br/>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        <?php


        return ob_get_clean();
    }

    public function onContentPrepareForm ($form, $data) {
        $app = JFactory::getApplication();
        $option = $app->input->get('option');
        $view = $app->input->get('view');

        if ($option == 'com_converge' && $view == 'plan') {
            ob_start();
            ?>
                <fieldset name="tovuti_builder" label="Tovuti Builder" class="tab-pane">
                    <field name="tovuti_builder"
                           type="radio"
                           label="Show Builder"

                           default="0">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </field>
                    <field
                        name="tovuti_builder_price_text"
                        label="Pricing Text"
                        type="text"
                        description="Text to be used for pricing in plans list view"
                        showon="tovuti_builder:1"
                    />
                </fieldset>
            <?php

                $newField = ob_get_clean();
                $xmlElement = new SimpleXMLElement($newField);
                $form->setField($xmlElement);
            }
    }

    public function onSubscriptionPlay($sub) {

    }
}