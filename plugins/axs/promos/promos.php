<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/5/16
 * Time: 9:13 AM
 */

defined('_JEXEC') or die;

/**
 * @package     Joomla.plugin
 */
class plgAxsPromos extends JPlugin {
    private $promo;

    //code, plan, course, event
    public function verifyPromo($promo_code, $plan, $course, $event, $initial_amount = null, $recurring_amount = null) {
        //look up the promo code
        $promo = $this->getPromoCodeByCode($promo_code);

        $res = new stdClass();
        if (is_object($promo)) {
           
            $this->promo = $promo;

            $now = new DateTime();
            if (($promo->active == '0000-00-00 00:00:00' || new DateTime($promo->active) <= $now) && ($promo->expired == '0000-00-00 00:00:00' || $now <= new DateTime($promo->expired))) {
                    //if promo is 0 (unlimited) or, if available is not 0 and there are still some available to use
                    if (!$promo->available || ($promo->available && $promo->available > $promo->used)) {
                    	//Get an object wiht valid course and plan IDs
                    	$valid_products = json_decode($promo->valid_products);
                        //subscription purchases
                        if ($plan) {

                            //Use the supplied dollar amount for the initial amount if it exists.
                            if ($initial_amount) {
                                $plan->default_initial_amount = $initial_amount;
                            }

                            //Use the supplied dollar amount for the initial amount if it exists.
                            if ($recurring_amount) {
                                $plan->default_amount = $recurring_amount;
                            }

                    		//Get array of plans
                    		$product_list = array(json_decode($valid_products->plans, true));
                    		$itemId = $plan->id;
                            if ($promo->initial_discount_unit == 'prct') {                                
                                $res->initial_amount = (100 - $promo->initial_discount_amount) / 100 * $plan->default_initial_amount;
                            } else {
                                $res->initial_amount = $plan->default_initial_amount - $promo->initial_discount_amount;
                            }

                            if ($res->initial_amount < 0) {
                                $res->initial_amount = '0.00';
                            }

                            if ($promo->recurring_discount_unit == 'prct') {
                                $res->recurring_amount = (100 - $promo->recurring_discount_amount) / 100 * $plan->default_amount;
                            } else {
                                $res->recurring_amount = $plan->default_amount - $promo->recurring_discount_amount;
                            }

                            if ($res->recurring_amount < 0) {
                                $res->recurring_amount = '0.00';
                            }
                        } else if ($course) {
                    		//Get an array of courses
                    		$product_list = array(json_decode($valid_products->courses, true));
                    		$itemId = $course->splms_course_id;
                            if($promo->item_discount_unit == 'prct') {
                                $res->item_amount = (100 - $promo->item_discount_amount) / 100 * $course->price;
                            } else {
                                $res->item_amount = $course->price - $promo->item_discount_amount;
                            }
                            if ($res->item_amount < 0) {
                                $res->item_amount = '0.00';
                            }
                        }
                        //TODO: add for event and other purchases
                        //check is course or plan is NOT in array of valid courses/plans
                        if (!in_array($itemId, $product_list[0])){
                         	$res->initial_amount = $plan->default_initial_amount;
                        	$res->recurring_amount = $plan->default_amount;
                        	$res->item_amount = $course->price;
                            $res->result = 'error';
							$res->message = 'The Promo Code is not valid for this item.';
						} else {
                            $res->result = 'success';
                            $res->message = 'Promo Code was applied to purchase.';
                    	}

                        	
                    } else {
                        $res->initial_amount = $plan->default_initial_amount;
                        $res->recurring_amount = $plan->default_amount;
                        $res->item_amount = $course->price;
                        $res->result = 'error';
                        $res->message = 'A limited amount of these Promo Codes have all been used.';
                    }
            } else {
                $res->initial_amount = $plan->default_initial_amount;
                $res->recurring_amount = $plan->default_amount;
                $res->item_amount = $course->price;
                $res->result = 'error';
                $res->message = 'Promo Code is no longer active.';
            }
        } else {
            $res->initial_amount = $plan->default_initial_amount;
            $res->recurring_amount = $plan->default_amount;
            $res->item_amount = $course->price;
            $res->result = 'error';
            $res->message = 'Promo Code is not valid.';
        }

        return $res;
    }

    public function getPromoCodeByCode($code) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from($db->qn('axs_pay_promo_codes'))
                ->where($db->qn('code').'='.$db->q($code));
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function onSubscriptionPlay($sub) {
        $app = JFactory::getApplication();
        $promo_code = $app->input->get('promo', '');
        if($promo_code) {
            $promo = $this->getPromoCodeByCode($promo_code);
            if(is_object($promo)) {
                $this->promo = $promo;

                //store that the promo code was used in axs_pay_promo_codes
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
                $query->update($db->qn('axs_pay_promo_codes'))
                        ->set($db->qn('used').'='.$db->qn('used').'+1')
                        ->where($db->qn('id').'='.$promo->id);
                $db->setQuery($query);
                $db->execute();

                //store promo code purchase activity
                $data = new stdClass();
                $data->user_id = $sub->user_id;
                $data->trxn_id = count($sub->periods[0]->trxns) ? $sub->periods[0]->trxns[0]->id : 0;
                $data->type = AxsPayment::$trxn_types['subscription'];
                $this->storeActivity($data);
            }
        }
    }

    public function onCoursePromo($trxn) {
        $app = JFactory::getApplication();
        $promo_code = $app->input->get('promo', '');
        if ($promo_code) {
            $promo = $this->getPromoCodeByCode($promo_code);
            if (is_object($promo)) {
                $this->promo = $promo;

                //store that the promo code was used in axs_pay_promo_codes
                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
                $query
                    ->update($db->qn('axs_pay_promo_codes'))
                    ->set($db->qn('used').'='.$db->qn('used').'+1')
                    ->where($db->qn('id').'='.$promo->id);
                $db->setQuery($query);
                $db->execute();

                //store promo code purchase activity
                $data = new stdClass();
                $data->user_id = JFactory::getUser()->id;
                $data->trxn_id = $trxn->id;
                $data->type = AxsPayment::$trxn_types['course'];
                $this->storeActivity($data);
            }
        }
    }

    public function storeActivity($data) {
        if($this->promo) {
            $data->session_id = JFactory::getSession()->getId();
            $data->code_id = $this->promo->id;
            $data->date = date("Y-m-d H:i:s");
            $db = JFactory::getDbo();
            $db->insertObject('axs_pay_promo_codes_activity', $data);
        }
    }
}