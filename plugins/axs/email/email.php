<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 4/25/16
 * Time: 12:16 PM
 */

defined('_JEXEC') or die;

/**
 * Mylib plugin class.
 *
 * @package     Joomla.plugin
 * @subpackage  System.axslibs
 */
class plgAxsEmail extends JPlugin {
	/*
	public function __construct($subject, array $config) {
		parent::__construct($subject, $config);

		if(JFactory::getUser()->id == 1689) {
			$subs = AxsPayment::getUserSubscriptions();
			foreach($subs as $s) {
				$this->onSubscriptionStopRequest($s);
			}
		}
	}
	*/

	private function getPluginParamsWithPrefix($prefix) {
		$keyArray = array (
			'send_email',
			'subject_line',
			'sender_name',
			'sender_email',
			'use_html',
			'body_text'
		);
		$configs = new stdClass();
		foreach($keyArray as $key) {
			$configs->$key = $this->params->get($prefix.$key, '');
		}
		return $configs;
	}

  /* public function onSuccessfulPayment($user_id, $amount, $desc, $trxn_id) {
	
	$brand = AxsBrands::getBrand();
	$currency_code = 'USD';
	$currency_symbol = '$';
	if($brand->billing->currency_code) {
		$currency_code = $brand->billing->currency_code;
	}
	if($brand->billing->currency_symbol) {
		$currency_symbol = $brand->billing->currency_symbol;
	}

    //grab the configs from the plugin config
    $configs = $this->getPluginParamsWithPrefix('1');

    if($configs->send_email == '1') {
      //check if sender name / email are blank, if so use system default
      $system_config = JFactory::getConfig();
      $sender = array(
        $configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
        $configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
      );

      //grab the user data
      $user = JFactory::getUser($user_id);
      //set the mailto
      $reciever = $user->email;

	    $replacements = array (
		    "USERNAME" => $user->name,
		    "ITEMNAME" => $desc,
		    "DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
		    "AMOUNT" => $currency_symbol.$amount,
		    "TRXNID" => $trxn_id,
		    "SENDERNAME" => $sender[1],
		    "SENDEREMAIL" => $sender[0]
	    );

	    //send mail
	    $this->sendMail($sender, $reciever, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
    }
  } */

  /* public function onDeclinedFailedAutomatedPayment($user_id, $amount, $desc, $trxn_id) {
	  //grab the configs from the plugin config
	  $configs = $this->getPluginParamsWithPrefix('2');

    if($configs->send_email == '1') {
      //check if sender name / email are blank, if so use system default
      $system_config = JFactory::getConfig();
      $sender = array(
        $configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
        $configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
      );

      //grab the user data
      $user = JFactory::getUser($user_id);

      //set the mailto
      $reciever = $user->email;

	    $replacements = array (
	    	"USERNAME" => $user->name,
		    "ITEMNAME" => $desc,
		    "DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
		    "AMOUNT" => $amount,
		    "TRXNID" => $trxn_id,
		    "SENDERNAME" => $sender[1],
		    "SENDEREMAIL" => $sender[0]
	    );

      //send mail
      $this->sendMail($sender, $reciever, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
    }
  } */

  public function onSubscriptionStopRequest($sub) {
	  //grab the configs from the plugin config
	  $configs = $this->getPluginParamsWithPrefix('3');

	  if($configs->send_email == '1') {
		  //check if sender name / email are blank, if so use system default
		  $system_config = JFactory::getConfig();
		  $sender = array(
			  $configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
			  $configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
		  );

		  $brand = AxsBrands::getBrand();
		  $admin_emails = $brand->billing->admin_emails;
		  foreach($admin_emails as $email) {

			  $replacements = array (
				  "ADMINNAME" => $email->name,
				  "USERNAME" => JFactory::getUser($sub->user_id)->name,
				  "USERID" => $sub->user_id,
				  "PLANNAME" => $sub->getPlan()->title,
				  "DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
				  "SENDERNAME" => $sender[1],
				  "SENDEREMAIL" => $sender[0]
			  );

			  //send mail
			  $this->sendMail($sender, $email->email, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
		  }
	  }
  }

  public function onSubscriptionChangeRequest($old_sub, $new_plan) {
		//grab the configs from the plugin config
	  $configs = $this->getPluginParamsWithPrefix('6');

	  if($configs->send_email == '1') {
		  //check if sender name / email are blank, if so use system default
		  $system_config = JFactory::getConfig();
		  $sender = array(
			  $configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
			  $configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
		  );

		  $brand = AxsBrands::getBrand();
		  $admin_emails = $brand->billing->admin_emails;
		  foreach($admin_emails as $email) {

			  $replacements = array (
				  "ADMINNAME" => $email->name,
				  "USERNAME" => JFactory::getUser($old_sub->user_id)->name,
				  "USERID" => $old_sub->user_id,
				  "OLDPLANNAME" => $old_sub->getPlan()->title,
				  "NEWPLANNAME" => $new_plan->title,
				  "DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
				  "SENDERNAME" => $sender[1],
				  "SENDEREMAIL" => $sender[0]
			  );

			  //send mail
			  $this->sendMail($sender, $email->email, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
		  }
	  }
  }

	public function onFailedSignup($user_id, $first, $last, $plan_id) {
		//grab the configs from the plugin config
		$configs = $this->getPluginParamsWithPrefix('4');

		if($configs->send_email == '1') {
			//check if sender name / email are blank, if so use system default
			$system_config = JFactory::getConfig();
			$sender = array(
				$configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
				$configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
			);

			$brand = AxsBrands::getBrand();
			$admin_emails = $brand->billing->admin_emails;
			foreach($admin_emails as $email) {

				$replacements = array (
					"ADMINNAME" => $email->name,
					"USERNAME" => $user_id ? JFactory::getUser($user_id)->name : $first." ".$last,
					"USERID" => $user_id ? $user_id : 'no id available',
					"PLANNAME" => AxsPayment::getPlanById($plan_id)->title,
					"DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
					"SENDERNAME" => $sender[1],
					"SENDEREMAIL" => $sender[0]
				);

				//send mail
				//$this->sendMail($sender, $email->email, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
			}
		}
	}

	public function onSubscriptionPlay($sub) {
		//grab the configs from the plugin config
		$configs = $this->getPluginParamsWithPrefix('5');

		if($configs->send_email == '1') {
			//check if sender name / email are blank, if so use system default
			$system_config = JFactory::getConfig();
			$sender = array(
				$configs->sender_email ? $configs->sender_email : $system_config->get('mailfrom'),
				$configs->sender_name ? $configs->sender_name : $system_config->get('fromname')
			);

			$brand = AxsBrands::getBrand();
			$admin_emails = $brand->billing->admin_emails;
			foreach($admin_emails as $email) {

				$replacements = array (
					"ADMINNAME" => $email->name,
					"USERNAME" => JFactory::getUser($sub->user_id)->name,
					"USERID" => $sub->user_id,
					"PLANNAME" => $sub->getPlan()->title,
					"DATE" => AxsExtra::format_date(date("Y-m-d H:i:s"), true),
					"SENDERNAME" => $sender[1],
					"SENDEREMAIL" => $sender[0]
				);

				//send mail
				$this->sendMail($sender, $email->email, $configs->subject_line, $configs->body_text, $replacements, $configs->use_html);
			}
		}

		$params = new stdClass();
		$params->subscription_id = $sub->plan_id;
		$params->user_id = $sub->user_id;
		$alerts = new AxsAlerts($params);
    	$alertType = 'subscription registration';
	    $alerts->runAutoAlert($alertType,$params);
	}

  public function sendMail($sender, $reciever, $subject, $tmpl, $replacements, $use_html) {
    //validate email first.
    if (!filter_var($reciever, FILTER_VALIDATE_EMAIL)) {
      return false;
    }

    //replace the const with the value in the email tmpl
    foreach($replacements as $const => $value) {
    	$tmpl = str_replace("{{".$const."}}", $value, $tmpl);
    }

    $mailer = JFactory::getMailer();
    $mailer->setSender($sender);
    $mailer->addRecipient($reciever);
    $mailer->setSubject($subject);
    if($use_html) {
      $mailer->isHtml(true);
      $mailer->Encoding = 'base64';
    }
    $mailer->setBody($tmpl);
    $mailer->Send();
  }
}