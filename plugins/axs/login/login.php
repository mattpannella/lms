<?php

defined('_JEXEC') or die;

class plgAxsLogin extends JPlugin {

	public function onUserLoginFailure($data) {
		AxsSecurity::storeFailedAttempt($data);
		$app = JFactory::getApplication();
		$task = $app->input->get('task');
		if($task == 'user.login') {
			$app->enqueueMessage('Username and password do not match or you do not have an account yet.', 'warning');
			$app->redirect(JRoute::_('index.php?option=com_users&view=login', false));
		}		
	}

	public function onUserLogin($user, $options) {
		AxsSecurity::deleteFailedAttempt();
		AxsSecurity::purgeFailedAttempts();
	}

}