<?php

defined('_JEXEC') or die;

class plgAXSPartners extends JPlugin {
	private $partner;

	public function __construct($subject, array $config) {
		parent::__construct($subject, $config);
		//only run if not cli
		if (PHP_SAPI !== 'cli') {
			$input = JFactory::getApplication()->input;
			$this->partner = $input->get('partner', 0);
			if($this->partner) {
				$input->cookie->set("partner", $this->partner, time()+360000);
				$data = new stdClass();
				$data->partner_id = $this->partner;
				$data->user_id = JFactory::getUser()->id;
				$data->date = date('Y-m-d H:i:s');
				$this->saveActivityData($data);
			} else {
				//otherwise also check cookie to see if it's set.
				$this->partner = $input->cookie->get("partner", 0);
			}
		}
	}

	public function getActivityData($id) {
    	$db = JFactory::getDbo();
	    $query = $db->getQuery(true);
	    $query->select('*')
	      ->from($db->qn('axs_affiliate_partners_activity'))
	      ->where($db->qn('id').'='.(int)$id);
	    $db->setQuery($query);
	    return $db->loadObject();
	}

	public function saveActivityData($data) {
		$session = JFactory::getSession();
		$sessionID = $session->getId();
		$data->session_id = $sessionID;
		$db = JFactory::getDbo();
		$db->insertObject('axs_affiliate_partners_activity', $data);
	}
}