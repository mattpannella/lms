<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/12/16
 * Time: 10:55 AM
 */
defined('_JEXEC') or die;

class plgAxsRewards extends JPlugin {
	private $refid;

	public function __construct($subject, array $config) {
		parent::__construct($subject, $config);
		//only run if not cli
		if (PHP_SAPI !== 'cli') {
			$brand = AxsBrands::getBrand();
			if($brand->billing->rewards_active) {
				$input = JFactory::getApplication()->input;
				$this->refid = $input->get('refid', 0);
				if($this->refid) {
					$input->cookie->set("refid", $this->refid, time()+360000);
				} else {
					//otherwise also check cookie to see if it's set.
					$this->refid = $input->cookie->get("refid", 0);
				}
			}
		}
	}

	public function onGetRefid() {
		return true;
		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {
			return $this->refid;
		} else {
			//return true if the rewards program is turned off so the field isn't show in the login.
			return true;
		}
	}

	public function onValidateRefid() {
		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {
			//if refid is set go ahead and validate it.
			if($this->refid) {
				$db  = JFactory::getDBO();

				if(is_numeric($this->refid)) {
					//look for it in joom_users
					$query = $db->getQuery(true);
					$query->select('id')
						->from($db->qn('#__users'))
						->where($db->qn('id').'='.(int)$this->refid);
					$db->setQuery($query);
					if(is_object($db->loadObject())) {
						return true;
					} else {
						JFactory::getApplication()->input->cookie->set("refid", null, time() - 1); //clear cookie
						return false;
					}
				} else {
					//look for it in axs_gin_affiliate_codes
					$query = $db->getQuery(true);
					$query->select('user_id')
						->from($db->qn('axs_gin_affiliate_codes'))
						->where($db->qn('code').'='.$db->q($this->refid));
					$db->setQuery($query);
					$ret = $db->loadObject();
					if(is_object($ret)) {
						$this->refid = $ret->user_id;
						return true;
					} else {
						JFactory::getApplication()->input->cookie->set("refid", null, time() - 1); //clear cookies
						return false;
					}
				}
			}
		}
		return true;
	}

	public function getReferrer($user_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('axs_rewards_data'))
			->where($db->qn('user_id').'='.$db->q($user_id))
			->limit(1);
		$db->setQuery($query);
		$row = $db->loadObject();
		if(is_object($row)) {
			$this->refid = $row->referrer_id;
		}

		return $this->refid;
	}

	public function onAfterCompleteOrder($data) {
		self::onStorePurchase($data);
	}

	public function onAfterCompleteEventPurchase($data) {
		self::onEventPurchase($data);
	}

	public function getOrderData($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__eshop_orderproducts'))
			->where($db->qn('order_id').'='.$db->q($id));
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$orderList = '';

		foreach($rows as $row) {
			$orderList .= '<div><b>Product:</b> '.$row->product_name.' <br/><b>Quantity:</b> '.$row->quantity.'</div>
						   <div class="clearfix"></div>';
		}

		return $orderList;
	}

	public function getEventData($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__eb_events'))
			->where($db->qn('id').'='.$db->q($id))
			->limit(1);
		$db->setQuery($query);
		$row = $db->loadObject();

		$title = '<b>Event:</b> '.$row->title.' <br/><b>Date: </b>'.date("m/d/Y", strtotime($row->event_date));

		return $title;
	}

	public function onStorePurchase($data) {
		//this is where we'll set the refid if there IS one, as well as save some other stuff relative to the rewards system
		if($data->customer_id) {
			$user_id = $data->customer_id;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {

			self::getReferrer($user_id);

			//if they have a referrer, go ahead and save commission
			if($this->refid) {
				//look up referrer's affiliate level
				$level = AxsExtra::getUserAffiliateLevel($this->refid);
				$affiliateParams = json_decode($level->params);
				$commisionAmount = 0;

				if($affiliateParams->store_commision_type == 'percentage' && $affiliateParams->store_commision_percentage) {
					$commisionAmount = ($affiliateParams->store_commision_percentage / 100) * $data->total;
				}

				if($affiliateParams->store_commision_type == 'flat' && $affiliateParams->store_commision_flat) {
					$commisionAmount = $affiliateParams->store_commision_flat;
				}

				$orderList = self::getOrderData($data->id);

				if(!$orderList) {
					$orderList = 'Product Order';
				}

				$params = new stdClass();
				$params->amount = $commisionAmount;
				$params->referrer_id = $this->refid;
				$params->referee_id = $user_id;
				$params->reason = $orderList;

				AxsExtra::setUserCommission($params);

			}
		}
	}


	public function onEventPurchase($data) {
		//this is where we'll set the refid if there IS one, as well as save some other stuff relative to the rewards system
		if($data->user_id) {
			$user_id = $data->user_id;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {

			self::getReferrer($user_id);

			//if they have a referrer, go ahead and save commission
			if($this->refid) {
				//look up referrer's affiliate level
				$level = AxsExtra::getUserAffiliateLevel($this->refid);
				$affiliateParams = json_decode($level->params);
				$commisionAmount = 0;

				if($affiliateParams->event_commision_type == 'percentage' && $affiliateParams->event_commision_percentage) {
					$commisionAmount = ($affiliateParams->event_commision_percentage / 100) * $data->amount;
				}

				if($affiliateParams->event_commision_type == 'flat' && $affiliateParams->event_commision_flat) {
					$commisionAmount = $affiliateParams->event_commision_flat;
				}

				$orderList = self::getEventData($data->event_id);

				if(!$orderList) {
					$orderList = 'Event Purchase';
				}

				$params = new stdClass();
				$params->amount = $commisionAmount;
				$params->referrer_id = $this->refid;
				$params->referee_id = $user_id;
				$params->reason = $orderList;

				AxsExtra::setUserCommission($params);

			}
		}
	}

	public function onCoursePurchaseCommision($data) {
		//this is where we'll set the refid if there IS one, as well as save some other stuff relative to the rewards system

		if($data->user_id) {
			$user_id = $data->user_id;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {

			self::getReferrer($user_id);

			//if they have a referrer, go ahead and save commission
			if($this->refid) {
				//look up referrer's affiliate level
				$level = AxsExtra::getUserAffiliateLevel($this->refid);
				$affiliateParams = json_decode($level->params);
				$commisionAmount = 0;

				if($affiliateParams->course_commision_type == 'percentage' && $affiliateParams->course_commision_percentage) {
					$commisionAmount = ($affiliateParams->course_commision_percentage / 100) * $data->amount;
				}

				if($affiliateParams->course_commision_type == 'flat' && $affiliateParams->course_commision_flat) {
					$commisionAmount = $affiliateParams->course_commision_flat;
				}


				if(!$data->description) {
					$title = 'Course Purchase';
				} else {
					$title = '<b>Courses:</b> '.$data->description;
				}

				$params = new stdClass();
				$params->amount = $commisionAmount;
				$params->referrer_id = $this->refid;
				$params->referee_id = $user_id;
				$params->reason = $title;

				AxsExtra::setUserCommission($params);

			}
		}
	}

	public function onUserAfterSave($data) {
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('axs_rewards_data'))
			->where($db->qn('user_id').'='.$db->q($data['id']))
			->limit(1);
		$db->setQuery($query);
		$row = $db->loadObject();
		$refid = 0;

		if( (!$user->id && $data['id']) || ($user->id == $data['id']) ) {
			if($this->refid) {
				$refid = $this->refid;
			}
		}

		if(!$row) {

			$col = array (
				'user_id',
				'level',
				'referrer_id',
				'points'
			);
			$val = array (
				(int)$data['id'],
				(int)1,
				(int)$refid,
				(int)0
			);
			$query = $db->getQuery(true);
			$query->insert($db->qn('axs_rewards_data'))
				->columns($col)
				->values(implode(',', $val));
			$db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
			$db->execute();
		}
	}

	public function onSubscriptionPlay($sub) {
		//this is where we'll set the refid if there IS one, as well as save some other stuff relative to the rewards system

		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->qn('axs_rewards_data'))
				->where($db->qn('user_id').'='.$db->q($sub->user_id))
				->limit(1);
			$db->setQuery($query);
			$row = $db->loadObject();

			if(is_object($row)) {
				$this->refid = $row->referrer_id;
			} else {
				$col = array (
					'user_id',
					'level',
					'referrer_id',
					'points'
				);
				$val = array (
					(int)$sub->user_id,
					(int)1,
					(int)$this->refid,
					(int)0
				);
				$query = $db->getQuery(true);
				$query->insert($db->qn('axs_rewards_data'))
					->columns($col)
					->values(implode(',', $val));
				$db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
				$db->execute();
			}

			//if they have a referrer, go ahead and save commission
			if($this->refid) {
				$plan = $sub->getPlan();
				if($plan->refer_commission) {
					//look up referrer's affiliate level
					$level = AxsExtra::getUserAffiliateLevel($this->refid);
					$affiliateParams = json_decode($level->params);
					$commisionAmount = 0;

					if($affiliateParams->subscription_commision_type == 'percentage' && $affiliateParams->subscription_commision_percentage) {
						$commisionAmount = ($affiliateParams->subscription_commision_percentage / 100) * $sub->periods[0]->trxns[0]->amount;
					}

					if($affiliateParams->subscription_commision_type == 'flat' && $level->sales_commision) {
						$commisionAmount = $level->sales_commision;
					}

					$params = new stdClass();
					$params->amount = $commisionAmount;
					$params->referrer_id = $this->refid;
					$params->referee_id = $sub->user_id;
					$params->reason = '<b>Subscription: </b>'.$plan->title;

					AxsExtra::setUserCommission($params);
				}
			}
		}
	}

	public function onCronCalcPoints($brand, $user_id, $total, $pointsToUse) {
		if($brand->billing->rewards_active) {
			//we need to look up how many points they actually have.
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->qn('axs_rewards_data'))
				->where($db->qn('user_id') . '=' . $user_id);
			$db->setQuery($query);
			$data = $db->loadObject();

			$pointsToCoverInFull = $total * $brand->billing->rewards_quotient;
			$pointsAvailable = $data->points;
			if($pointsAvailable > 0) {
				//if they have it set to use more points than what is actually needed to pay for their bill... just adjust their points to use
				if($pointsToUse > $pointsToCoverInFull) {
					$pointsToUse = $pointsToCoverInFull;
				}
				//if they have it set to use more points than they actually have... just use what they have
				if($pointsAvailable < $pointsToUse) {
					$pointsToUse = $pointsAvailable;
				}
			} else {
				$pointsToUse = 0;
			}

			$rewards = new stdClass();
			$rewards->total =  $total - ($pointsToUse / $brand->billing->rewards_quotient);
			$rewards->pointsToUse = $pointsToUse;

			return $rewards;
		}
		return 0;
	}

	public function onCronSuccess($brand, $user_id, $dollarAmount, $pointsUsed, $desc) {
		if($brand->billing->rewards_active && $pointsUsed) {
			//remove this amount of point from their points
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->update($db->qn('axs_rewards_data'))
				->set($db->qn('points').'='.$db->qn('points').'-'.(int)$pointsUsed)
				->where($db->qn('user_id') . '=' . $user_id);
			$db->setQuery($query);
			$db->execute();

			//also make a rewards trxn showing system removed points
			$col = array (
				'user_id',
				'date',
				'referee_id',
				'amount',
				'reason',
				'status'
			);
			$val = array (
				(int)$user_id,
				$db->q(date("Y-m-d H:i:s")),
				0,
				-1 * (int)$pointsUsed,
				$db->q($desc),
				$db->q('Awarded')
			);
			$query = $db->getQuery(true);
			$query->insert($db->qn('axs_rewards_transactions'))
				->columns($col)
				->values(implode(',', $val));
			$db->setQuery($query);
			$db->execute();

			//finally, make a rewards trxn showing their referrer gets some points.
			if($dollarAmount) {
				$date = date("Y-m-d H:i:s");
				$amount = $dollarAmount * $brand->billing->rewards_quotient / 100;
				$query = "INSERT IGNORE INTO axs_rewards_transactions (user_id, `date`, referee_id, amount, reason, `status`)
									SELECT
								      referrer_id,
								      '$date',
								      user_id,
								      $amount * b.on_upgrade_percentage,
								      '$desc',
								      'Pending'
								  FROM axs_rewards_data a
								  JOIN axs_gin_affiliate_levels b ON a.level=b.level_id
                  WHERE a.user_id=$user_id AND a.referrer_id !=0;";
				$db->setQuery($query);
				$db->execute();
			}
		}
	}

	public function onReprocessCalcPoints($user_id, $total, $pointsToUse) {
		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active) {
			//we need to look up how many points they actually have.
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('*')
				->from($db->qn('axs_rewards_data'))
				->where($db->qn('user_id') . '=' . $user_id);
			$db->setQuery($query);
			$data = $db->loadObject();

			$pointsToCoverInFull = $total * $brand->billing->rewards_quotient;
			$pointsAvailable = $data->points;
			//if they have it set to use more points than what is actually needed to pay for their bill... just adjust their points to use
			if($pointsToUse > $pointsToCoverInFull) {
				$pointsToUse = $pointsToCoverInFull;
			}
			//if they have it set to use more points than they actually have... just use what they have
			if($pointsAvailable < $pointsToUse) {
				$pointsToUse = $pointsAvailable;
			}

			$rewards = new stdClass();
			$rewards->total =  $total - ($pointsToUse / $brand->billing->rewards_quotient);
			$rewards->pointsToUse = $pointsToUse;

			return $rewards;
		}
		return 0;
	}

	public function onReprocessSuccess($user_id, $dollarAmount, $pointsUsed, $desc) {
		$brand = AxsBrands::getBrand();
		if($brand->billing->rewards_active && $pointsUsed) {
			//remove this amount of point from their points
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->update($db->qn('axs_rewards_data'))
				->set($db->qn('points').'='.$db->qn('points').'-'.(int)$pointsUsed)
				->where($db->qn('user_id') . '=' . $user_id);
			$db->setQuery($query);
			$db->execute();

			//also make a rewards trxn showing system removed points
			$col = array (
				'user_id',
				'date',
				'referee_id',
				'amount',
				'reason',
				'status'
			);
			$val = array (
				(int)$user_id,
				$db->q(date("Y-m-d H:i:s")),
				0, //for system
				-1 * (int)$pointsUsed,
				$db->q($desc),
				$db->q('Awarded')
			);
			$query = $db->getQuery(true);
			$query->insert($db->qn('axs_rewards_transactions'))
				->columns($col)
				->values(implode(',', $val));
			$db->setQuery($query);
			$db->execute();

			//finally, make a rewards trxn showing their referrer gets some points.
			if($dollarAmount) {
				$date = date("Y-m-d H:i:s");
				$amount = $dollarAmount * $brand->billing->rewards_quotient / 100;
				$query = "INSERT IGNORE INTO axs_rewards_transactions (user_id, `date`, referee_id, amount, reason, `status`)
									SELECT
								      referrer_id,
								      '$date',
								      user_id,
								      $amount * b.on_upgrade_percentage,
								      '$desc',
								      'Pending'
								  FROM axs_rewards_data a
								  JOIN axs_gin_affiliate_levels b ON a.level=b.level_id
                  WHERE a.user_id=$user_id AND a.referrer_id !=0;";
				$db->setQuery($query);
				$db->execute();
			}
		}
	}

	/* public function onContentPrepareForm($form, $data) {
		$brand = AxsBrands::getBrand();
		if ($brand->billing->rewards_active) {

			$app = JFactory::getApplication();
			$option = $app->input->get('option');
			$view = $app->input->get('view');

			if ($option == 'com_converge' && $view == 'plan') {
				ob_start();
				?>
					<fieldset
						name="rewards"
						label="Rewards Options"
						class="tab-pane"
					>
						<field
							name="earn_points"
							type="radio"
							label="Active subscribers can earn rewards points"
							
							default="1"
						>
				            <option value="0">No</option>
				            <option value="1">Yes</option>
						</field>
				        <field
				        	name="refer_points"
				            type="radio"
				            label="Active subscribers can refer rewards points"
				            
				            default="1"
				        >
				            <option value="0">No</option>
				            <option value="1">Yes</option>
				        </field>
				        <field
				        	name="refer_commission"
				            type="radio"
				            label="Active subscribers can refer commission"
				            
				            default="1"
				        >
				            <option value="0">No</option>
				            <option value="1">Yes</option>
				        </field>
					</fieldset>
				<?php

				$newField = ob_get_clean();
				$xmlElement = new SimpleXMLElement($newField);
				$form->setField($xmlElement);
			}
		}
	} */

	public function onUserAfterDelete($user, $success, $msg) {
		if($success) {
			$id = $user['id'];
			if($id) {
				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_gin_commissions WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_gin_affiliate_codes WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_rewards_data WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_rewards_transactions WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$db = JFactory::getDbo();
				$query = "DELETE FROM axs_rewards_history WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();
			}
		}
	}
}