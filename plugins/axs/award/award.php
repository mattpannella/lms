<?php

defined('_JEXEC') or die;

require_once(JPATH_ADMINISTRATOR . "/components/com_award/helpers/helper.php");

class plgAxsAward extends JPlugin {
	public function onUserAfterSave($user) {
		$id = $user['id'];
		BadgeHelper::updateBadges(null, $id, null, null);
	}

	public function onUserAfterDelete($user) {
		$id = $user['id'];
		BadgeHelper::removeAllBadgesForUser($id);
	}

	public function onAfterRender() {
		//This is used in case an action requires that the badges be recalculated.
		//Especially if that action is done in a core file.  Otherwise, the BadgeHelper::updateBadges() function could just be put there.
		if (!empty($_SESSION['badge_refresh_on_reload']) && $_SESSION['badge_refresh_on_reload'] == true) {
			unset($_SESSION['badge_refresh_on_reload']);
			BadgeHelper::updateBadges();
		}
	}
}