<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 8/25/16
 * Time: 2:46 PM
 */

defined('_JEXEC') or die;

/**
 * @package     Joomla.plugin
 * @subpackage  System.axslibs
 */
class plgAxsMms extends JPlugin {
	public function onUserAfterDelete($user, $success, $msg) {
		if($success) {
				//delete junk from sub table, sub periods, trxns, others?
			$id = $user['id'];
			if($id) {
				$db = JFactory::getDbo();
				$query = "DELETE a, b, c
									FROM axs_pay_subscriptions a 
									LEFT JOIN axs_pay_subscription_periods b ON a.id=b.sub_id 
									LEFT JOIN axs_pay_subscription_field_values c ON a.id=c.sub_id
									WHERE a.user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$query = "DELETE a, b
									FROM axs_pay_transactions a  
									LEFT JOIN axs_pay_transactions_errors b ON a.id=b.trxn_row_id
									WHERE a.user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$query = "DELETE a, b 
									FROM axs_pay_user_pplans a 
									LEFT JOIN axs_pay_user_pplan_periods b ON a.id=b.upp_id
									WHERE a.user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$query = "DELETE FROM axs_gin_admin_actions WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$query = "DELETE FROM axs_cancellation_requests WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();

				$query = "DELETE FROM axs_events_registration WHERE user_id=$id;";
				$db->setQuery($query);
				$db->execute();
			}
		}
	}
}