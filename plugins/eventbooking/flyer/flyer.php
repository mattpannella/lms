<?php
defined('_JEXEC') or die;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/



class plgEventbookingFlyer extends JPlugin {

	public function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');		
	}

	/**
	 * Render settings form
	 *
	 * @param $row
	 *
	 * @return array
	 */
	
	public function onEditEvent($row) {

		
		$form = $this->drawSettingForm($row);

		$data = array (
					'title' => 'Flyer',
					'form'  => $form,
				);

		return $data;
	}

	public function onAfterSaveEvent($row, $data, $isNew) {

		/*
			The template data is decoded since it is saved in base64 in the div, to avoid issues with special characters.
		*/			

		$params = new JRegistry($row->params);
		$params->set('flyer_html', json_encode($data['flyer_html_editor']));
		$params->set('flyer_template', base64_decode($data['flyer_template_data']));
		$row->params = $params->toString();
		$row->store();
	}

	public function drawSettingForm($row) {

		$params = new JRegistry($row->params);
		$flyer_html = json_decode($params->get('flyer_html', ''));
		$flyer_template = json_decode($params->get('flyer_template', ''));

		$db = JFactory::getDBO();

		//jDump($flyer_template);

		ob_start();

		?>			

			<script src="/plugins/eventbooking/flyer/assets/js/flyer.js"></script>
			<link rel="stylesheet" type="text/css" href="/plugins/eventbooking/flyer/assets/css/flyer.css">

			<span id="flyer_buttons">
				<button which="usage" class="flyer_show btn btn-primary">Usage</button>
				<button which="html" class="flyer_show btn btn-primary">HTML</button>
				<button which="template" class="flyer_show btn btn-primary">Template</button>
			</span>
			<span id="load_flyer" style="float:right;">
				<span>
					<input id="new_flyer_name" type="text" maxlength="64">
					<button id="flyer_save" class="btn btn-primary" disabled>Save</button>
					<button id="flyer_save_new" class="btn btn-primary" disabled>Save New</button>
				</span>
				<span>
					<select id="flyer_list">
						<option value="-1">--SELECT TEMPLATE--</option>
						<?php
							$query = "SELECT id, name FROM joom_eb_flyers";
							$db->setQuery($query);
							$results = $db->loadObjectList();
							foreach ($results as $result) {
								?>
									<option value="<?php echo $result->id;?>"><?php echo $result->name; ?></option>
								<?php
							}
						?>
					</select>
					<button id="flyer_load" class="btn btn-primary" disabled >Load</button>
					<button id="flyer_delete" class="btn btn-danger" disabled >Delete</button>
				</span>
			</span>

			<div id="flyer_usage_area" class="flyer_display" style="display:none;">
				The flyer is made of two parts.  
				<ul>
					<li>HTML Editor</li>
					<li>Repeatable Template</li>
				</ul>

				<br><b>HTML Editor</b><br>
				The editor is used to design the look and feel of the flyer in standard HTML.
				<br><b>Repeatable Template</b><br>
				The repeatable text/replace template is used to swap out text in the HTML for data that is entered by each individual affiliate.
				The repeatable consists of two parts: 
				<br>
				The first is the text that the affiliate will be presented with (such as <b>Name</b> and <b>Email</b>)
				<br>
				The second is a text string that will be replaced in the flyer's HTML with the affiliate's input.
				A search/replace will be done on the entire HTML text and all instances of a given template will be replaced with what the affiliate enters.
				<br>
				When selecting a style for your template text, it is recommended to choose something that does not normally occur in standard writing. 
				A suggested template text string is enclosing in double brackets, such as <b>{{NAME}}</b>. However, any template will work. 
				*Note that templates are case sensitive.
				<br><br>
				Templates can also be saved and loaded for creating flyer drafts and sharing work between events.
				<br><br>
				<b>Example:</b>
				<hr>
				<br>HTML:<br>
				<textarea readonly style="min-width: 25%; width: 500px; min-height: 10%; height: 250px;">&lt;div&gt;My name is {{NAME}}.&lt;/div&gt;</textarea>
				<hr>
				<br>Template:<br>
				<table>
					<tr>
						<td>Text</td><td><input readonly value="Your Name"/></td>
					</tr>
					<tr>
						<td>Template</td><td><input readonly value="{{Name}}"/></td>
					</tr>
				</table>
				<hr>
				<br>Affiliate's Prompt:</br>
				Your Name: <input readonly value="John Doe"/>
				<br>
				<hr>
				<br>Output:</br>
				My name is John Doe.
			</div>			

			<div id="flyer_html_area" class="flyer_display">
				<?php

				echo "Flyer HTML<br>";
				$default_value = JFactory::getApplication()->input->get('flyer_html_editor', $flyer_html);
				$editor = JFactory::getConfig()->get('editor');
				$editor = JEditor::getInstance($editor);
				echo $editor->display( 'flyer_html_editor', $default_value, 250, 500, 20, 20);

				?>
			</div>

			<?php 
				function makeTemplate($text = "", $temp = "") {
					
					if ($text) {
						$text = htmlspecialchars($text);
						$textVal = ' value="' . $text . '"';
					} else {
						$textVal = '';
					}

					if ($temp) {
						$temp = htmlspecialchars($temp);
						$tempVal = ' value="' . $temp . '"';
					} else {
						$tempVal = '';
					}

					$template = '
						<span>
							Text: <input class="template_text" placeholder="Your Text" onkeyup="save_templates()"' . $textVal . '>
						</span>
						<span style="margin-left: 15px;">
							Template: <input class="template_template" placeholder="{{TEXT}}" onkeyup="save_templates()"' . $tempVal . '>
						</span>
						<span class="remove_template btn btn-danger" onclick="remove_template(this)">Remove</span>';

					return $template;
				}
			?>
			
			<div id="flyer_template_area" class="flyer_display" style="display:none;">

				<button class="add_template btn btn-success" style="margin-bottom: 10px;">Add</button>
				
				<div id="template" style="display:none;">
					<?php echo makeTemplate(); ?>
				</div>
				<div id="flyer_template" name="flyer_template">
					<?php
						if (count($flyer_template)) {

							foreach ($flyer_template as $template) {
								echo "<div class='text_template'>";
								echo makeTemplate($template->text, $template->template);
								echo "</div>";
							}
						}
					?>
				</div>
			</div>

			<!--  The div where the information is stored for FOF saving -->
			<div class="control-group" style="display:none;">
				<div class="controls">
					<input id="flyer_template_data" type="text" name="flyer_template_data" value="<?php echo base64_encode(json_encode($flyer_template));?>">
				</div>
			</div>			

		<?php

		return ob_get_clean();

	}
}
