var which_save = null;
var current_usage = "html";
var save_template_timeout = null;
var new_flyer_name = null;
var flyer_save = null;
var flyer_delete = null;
var flyer_load = null;
var dropdown = null;

jQuery(document).ready(
	function() {

		new_flyer_name = document.getElementById("new_flyer_name");
		flyer_save = document.getElementById("flyer_save");
		flyer_delete = document.getElementById("flyer_delete");
		flyer_load = document.getElementById("flyer_load");
		dropdown = document.getElementById("flyer_list");

		jQuery(dropdown).change(check_dropdown);

		jQuery(".flyer_show").click(
			function(e) {
				e.preventDefault();
				var which = this.getAttribute("which");
				if (which == current_usage) {
					return;
				}

				current_usage = which;

				jQuery(".flyer_display").hide(500);
				switch (which) {
					case "usage":
						jQuery("#flyer_usage_area").show(500);
						break;
					case "html":
						jQuery("#flyer_html_area").show(500);
						break;
					case "template":
						jQuery("#flyer_template_area").show(500);
						break;
				}
			}
		);		

		jQuery(new_flyer_name).keyup(
			function(){
				if (new_flyer_name.value.length > 0) {
					flyer_save_new.disabled = false;
					if (which_save) {
						flyer_save.disabled = false;
					}
				} else {
					flyer_save_new.disabled = true;
					flyer_save.disabled = true;
				}
			}
		);

		jQuery("#flyer_save").click(
			function(e) {
				e.preventDefault();

				var name = new_flyer_name.value;
				var templatelist = get_templates();
				var html = get_html();

				var old_name = document.getElementById("flyer_list").options[dropdown.selectedIndex].innerHTML;

				if (confirm("Are you sure you want to save over \"" + old_name + "\"?")) {
					data = {
						name: btoa(name),
						template: btoa(JSON.stringify(templatelist)),
						html: btoa(html),
						type: 'save',
						which: which_save
					};

					sendAjax(data);
				}
			}
		);

		jQuery("#flyer_save_new").click(
			function(e) {
				e.preventDefault();
				var name = new_flyer_name.value;
				var templatelist = get_templates();
				var html = get_html();

				data = {
					name: btoa(name),
					template: btoa(JSON.stringify(templatelist)),
					html: btoa(html),
					type: 'savenew'
				};

				sendAjax(data);
			}
		);

		jQuery("#flyer_delete").click(
			function(e) {
				e.preventDefault();

				var old_name = document.getElementById("flyer_list").options[dropdown.selectedIndex].innerHTML;
				if (confirm("Are you sure you want to delete \"" + old_name + "\"?\nThis operation is irreversable.")) {

					data = {
						type: 'delete',
						which: which_save
					};
					sendAjax(data);
				}
			}
		);

		jQuery("#flyer_load").click(
			function(e) {
				e.preventDefault();
				if (confirm("Are you sure you want to load?\nYou will lose any unsaved progress.")) {
					data = {					
						type: 'load',
						which: which_save
					};
					sendAjax(data);
				}
			}
		);

		jQuery(".add_template").click(
			function(e) {
				e.preventDefault();
				var container = document.getElementById("flyer_template");
				var template = document.getElementById("template");
				var newTemplate = document.createElement("div");
				newTemplate.className = "text_template";
				newTemplate.innerHTML = template.innerHTML;
				container.appendChild(newTemplate);
			}
		);
	}
);

function check_dropdown() {
	if (dropdown.value > 0) {
		which_save = dropdown.value;
		new_flyer_name.value = dropdown.options[dropdown.selectedIndex].innerHTML;
		flyer_save.disabled = false;
		flyer_save_new.disabled = false;
		flyer_load.disabled = false;
		flyer_delete.disabled = false;
	} else {
		flyer_save.disabled = true;
		flyer_load.disabled = true;
		flyer_delete.disabled = true;
		if (!new_flyer_name.value) {
			flyer_save_new.disabled = true;
		}
	}
}

function sendAjax(data) {

	if (data === undefined) {
		return;
	}

	data.option 	= 'com_eventbooking';
	data.task 		= 'flyer.' + data.type;
	data.format		= 'raw';

	jQuery.ajax({
        type:       'POST',
        url:        'index.php',
        data:       data,
        success: function(response) {
            update_flyer_dropdown(response, data.type);
        }
    });
}

function update_flyer_dropdown(data, type) {
	
	var dropdown = document.getElementById("flyer_list");

	function populate_dropdown() {

		try {
			var list = JSON.parse(data);
		} catch (err) {
			alert("There was an error with " + type);
			return;
		}

		var highest = -1;
		for (var i = 0; i < list.length; i++) {
			var option = document.createElement("OPTION");
			option.value = list[i].id;
			option.innerHTML = list[i].name;
			dropdown.appendChild(option);
			if (parseInt(list[i].id) > highest) {
				highest = parseInt(list[i].id);
			}
		}

		return highest;
	}
	
	switch (type) {
		case "savenew":
			dropdown.length = 1;
			var highest = populate_dropdown();
			dropdown.value = highest;
			break;
		case "save":
			dropdown.length = 1;
			populate_dropdown();
			dropdown.value = which_save;
			break;
		case "delete":
			dropdown.value = -1;
			break;
		case "load":
			var loaded = JSON.parse(data);
			loaded.template = JSON.parse(loaded.template);

			
			var html_editor = document.getElementById("flyer_html_editor");
			var template_editor = document.getElementById("flyer_template");
			var template = document.getElementById("template");
			
			html_editor.value = loaded.html;
			template_editor.innerHTML = "";

			for (var i = 0; i < loaded.template.length; i++) {
				var text = loaded.template[i].text;
				var temp = loaded.template[i].template;

				var newTemplate = document.createElement("DIV");
				newTemplate.className = "text_template";
				newTemplate.innerHTML = template.innerHTML;

				newTemplate.getElementsByClassName("template_text")[0].value = text;
				newTemplate.getElementsByClassName("template_template")[0].value = temp;

				template_editor.appendChild(newTemplate);
			}
			
			break;
	}

	check_dropdown();
}

function remove_template (element) {
	var template = element.parentNode;
	jQuery(template).remove();
	save_templates();
}



function get_html() {
	var editor = document.getElementById("flyer_html_editor");
	return editor.value;
}

function get_templates() {
	var templates = document.getElementsByClassName('text_template');
	var list = [];
	for (var i = 0; i < templates.length; i++) {
		var template = templates[i];
		var text = template.getElementsByClassName("template_text")[0].value;
		var template = template.getElementsByClassName("template_template")[0].value;
		var data = {
			text: text,
			template: template
		};
		list.push(data);
	}
	return list;
}

function save_templates() {
	if (save_template_timeout != null) {
		clearTimeout(save_template_timeout);
	}

	save_template_timeout = setTimeout(
		function() {
			var list = get_templates();
			var save_div = document.getElementById("flyer_template_data");
			save_div.value = btoa(JSON.stringify(list));
		}, 250
	);
}