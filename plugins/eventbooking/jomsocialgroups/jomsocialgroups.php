<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
// no direct access
defined('_JEXEC') or die;

class plgEventbookingJomsocialgroups extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		JFactory::getLanguage()->load('plg_eventbooking_jomsocialgroups', JPATH_ADMINISTRATOR);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');
		
	}

	/**
	 * Render settings form
	 *
	 * @param $row
	 *
	 * @return array
	 */
	public function onEditEvent($row)
	{
		ob_start();
		$this->drawSettingForm($row);
		$form = ob_get_contents();
		ob_end_clean();

		return array('title' => 'Community Social Groups',
		             'form'  => $form,
		);
	}

	/**
	 * Store setting into database
	 *
	 * @param Event   $row
	 * @param Boolean $isNew true if create new plan, false if edit
	 */
	public function onAfterSaveEvent($row, $data, $isNew)
	{
		$params = new JRegistry($row->params);
		$params->set('community_group_ids', implode(',', $data['community_group_ids']));
		$row->params = $params->toString();

		$row->store();
	}

	/**
	 * Add registrants to selected Joomla groups when payment for registration completed
	 *
	 * @param PlanOsMembership $row
	 */
	public function onAfterPaymentSuccess($row)
	{  
		$db = JFactory::getDBO();
		if ($row->user_id) {
			
			$event = JTable::getInstance('EventBooking', 'Event');
			$config = EventbookingHelper::getConfig();
			$event->load($row->event_id);
			$params = json_decode($event->params);
			if($params->community_group_ids) {
				$community_groups = explode(',',$params->community_group_ids);
				$query = "REPLACE INTO `#__community_groups_members` (`groupid`,`memberid`,`approved`,`permissions`) VALUES ";
				
				foreach ($community_groups as $cg) {
					if($cg) {
						$values[] = '(' . $db->q($cg) . ',' . $db->q($row->user_id) . ',  1, 0)';
					}
				}
				if(count($values)) {
					$query .= implode(', ', $values);
					$db->setQuery($query);
					$db->execute();
				}

				$query = $db->getQuery(true);
				$query->select($db->quoteName('groups'))
					->from($db->quoteName('#__community_users'))
					->where($db->quoteName('userid') .'='. $db->quote($row->user_id))
					->limit(1);
				$db->setQuery($query);
				$result = $db->loadObject();

				if($result->groups) {
					$groupsArray   = explode(',',$result->groups);
					$groupsMerged  = array_unique(array_merge($groupsArray, $community_groups));
				} else {
					$groupsMerged  = array_unique($community_groups);
				}
				
				$newGroupsList = implode(',',$groupsMerged);

				$communityUser = new stdClass();
				$communityUser->userid = $row->user_id;
				$communityUser->groups = $newGroupsList;
				$db->updateObject('#__community_users',$communityUser,'userid');
			}
			
		}
	}

	public function drawSettingForm($row) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = "SELECT id,name FROM `#__community_groups` WHERE published = 1 ORDER BY name";
		$db->setQuery((string) $query);
		$groups = $db->loadObjectList();
		$params = new JRegistry($row->params);
		$community_group_ids = explode(',', $params->get('community_group_ids', ''));
		$count = count($groups);
		$form = '
		<table>
			<tr>
				<td width="220" class="key">
					Assign Community Groups on Event Registration
				</td>
				<td>';
		foreach($groups as $group) {		
					//echo JHtml::_('select.option', 'community_group_ids[]', $community_group_ids, ' multiple="multiple" size="6" ', false);
			$options[] = JHtml::_('select.option', $group->id, $group->name);
		}
		$form .= JHTML::_('select.genericlist', $options,'community_group_ids[]',  'multiple="multiple" size="'.$count.'"','value','text', $community_group_ids);		
		$form .= '</td>
				
			</tr>
		</table>';
		echo $form;
	}
}
