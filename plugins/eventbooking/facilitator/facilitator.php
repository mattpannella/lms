<?php
defined('_JEXEC') or die;

/*
The com_splms field doesn't exist on the site level and will crash when called.  
Also, it isn't needed.  
So, don't run any of this code on site side.
*/

$app = JFactory::getApplication();
if (!$app->isAdmin()) {
	return;
}



require_once("components/com_splms/fields/users.php");

class plgEventbookingFacilitator extends JPlugin {
	public function __construct(& $subject, $config) {

		parent::__construct($subject, $config);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');		
	}

	/**
	 * Render settings form
	 *
	 * @param $row
	 *
	 * @return array
	 */
	
	public function onEditEvent($row) {

		
		$form = $this->drawSettingForm($row);

		$data = array (
					'title' => 'Facilitators',
					'form'  => $form,
				);

		return $data;
	}

	public function onAfterSaveEvent($row, $data, $isNew) {

		$row->facilitator_ids = $data['user_ids'];
		$row->store();
	}

	public function drawSettingForm($row) {
		ob_start();

		$user_ids = $row->facilitator_ids;
		$user_view = new FOFFormFieldUsers();
		
		echo "Facilitators / Affiliates that will be at this event.";
		echo "<br><br><br>";
		echo $user_view->getInput('user_ids', $user_ids);

		return ob_get_clean();

	}
}
