<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

class plgEventbookingLms extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');

	}

	/**
	 * Render settings form
	 *
	 * @param $row
	 *
	 * @return array
	 */
	public function onEditEvent($row)
	{
		ob_start();
		$this->drawSettingForm($row);
		$form = ob_get_contents();
		ob_end_clean();

		return array('title' => 'LMS Courses',
		             'form'  => $form,
		);
	}

	/**
	 * Store setting into database
	 *
	 * @param Event   $row
	 * @param Boolean $isNew true if create new plan, false if edit
	 */
	public function onAfterSaveEvent($row, $data, $isNew)
	{
		$params = new JRegistry($row->params);
		$params->set('course_ids', implode(',', $data['course_ids']));
		$params->set('time_zone', $data['time_zone']);
		$params->set('virtual_meeting', $data['virtual_meeting']);
		$params->set('auto_checkin', $data['auto_checkin']);
		$params->set('calendar_location', $data['calendar_location']);
		$row->params = $params->toString();
		$row->store();
	}

	/**
	 * Add registrants to selected Joomla groups when payment for registration completed
	 *
	 * @param PlanOsMembership $row
	 */
	public function onAfterPaymentSuccess($row)
	{
		$db = JFactory::getDBO();
		if ($row->user_id)
		{
			$event = JTable::getInstance('EventBooking', 'Event');
			$config = EventbookingHelper::getConfig();
			$event->load($row->event_id);
			$params = json_decode($event->params);
			$courses = explode(',',$params->course_ids);

			foreach ($courses as $course) {
				if ($course) {

					$courseParams = AxsPayment::getCoursePurchaseParams();
					$courseParams->user_id = $row->user_id;
					$courseParams->course_id = $course;
					$courseParams->date = date("Y-m-d H:i:s");
					$courseParams->status = "PAID";

					$purchase = AxsPayment::newCoursePurchase($courseParams);
					$purchase->save();
				}
			}
		}
	}

	public function drawSettingForm($row) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = "SELECT splms_course_id, title FROM `#__splms_courses` ORDER BY title";
		$db->setQuery((string) $query);
		$courses = $db->loadObjectList();
		$params = new JRegistry($row->params);
		$course_ids = explode(',', $params->get('course_ids', ''));
		$form = '
		<table class="admintable adminform eventlist" style="width: 90%;">
			<tr>
				<td width="220" class="key">
					Allow access to Courses on Event Registration
				</td>
				<td>';
					foreach($courses as $course) {
						$options[] = JHtml::_('select.option', $course->splms_course_id, $course->title);
					}
					$form .= JHTML::_('select.genericlist', $options,'course_ids[]','multiple="multiple"','value','text', $course_ids);
					$form .= '
				</td>
			</tr>
		</table>';
		echo $form;
	}

	public function onAfterStoreRegistrant($row) {
		if (!$this->canRun) {
			return;
		}

		if ($row->user_id) {
			$awardCheckParams = new stdClass();
			$awardCheckParams->requirement_type = 'event';
			$awardCheckParams->event_id = $row->event_id;
			$award_ids = AxsLMS::getAvailableBadges($awardCheckParams);
			if($award_ids) {
				AxsLMS::updateBadges($award_ids, $row->user_id, null, null);
			}
		}
	}
}
