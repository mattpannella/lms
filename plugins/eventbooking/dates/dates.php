<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

class plgEventBookingDates extends JPlugin
{
	public function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);

		JTable::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_eventbooking/table');
	}

	/**
	 * Render setting form
	 *
	 * @param JTable $row
	 *
	 * @return array
	 */
	public function onEditEvent($row)
	{
		if ($row->parent_id > 0)
		{
			return;
		}

		ob_start();
		$this->drawSettingForm($row);
		$form = ob_get_clean();

		return array('title' => JText::_('EB_ADDITIONAL_DATES'),
		             'form'  => $form,
		);
	}

	/**
	 * Store setting into database
	 *
	 * @param event   $row
	 * @param Boolean $isNew true if create new plan, false if edit
	 */
	public function onAfterSaveEvent($row, $data, $isNew)
	{
		// The plugin will only be available in the backend
		$app = JFactory::getApplication();
		if ($app->isSite() || $row->parent_id > 0)
		{
			return;
		}
		$config = EventbookingHelper::getConfig();
		$maxNumberDates = (int) $data['count_event_dates'];
		$additionalEventIds = array();
		$numberChildrenEvents = 0;
		/*
		* This for loop allows for the processing of each 
		* additional date regardless of the event id.
		* The event ID is held as a var with underscore and
		* number (_1, _2, etc) tied to the key for the 
		* "addtional Date" by **count** --NOT-- by ID
		*/

		for ($i = 0; $i < $maxNumberDates; $i++)
		{
			if (empty($data['event_date_' . $i]))
			{
				continue;// nothing to do, continue to loop without passing go
			}
			//clone allows us to loop without concern of changing the original dataset and keeping all refernces accurate
			$rowEvent     = clone $row; 

			//check to see if the event is new (not set) and set the ID			
			$id           = isset($data['event_id_' . $i]) ? $data['event_id_' . $i] : 0;
			$rowEvent->id = $id;

			//create the dates "filtered" by the keyName_$i number (Not ID) i.e. 'event_date_'.$i
			$rowEvent->event_date     = $data['event_date_' . $i] . ' ' . $data['event_date_hour_' . $i] . ':' . $data['event_date_minute_' . $i] . ':00';
			$rowEvent->event_end_date = $data['event_end_date_' . $i] . ' ' . $data['event_end_date_hour_' . $i] . ':' . $data['event_end_date_minute_' . $i] . ':00';
			$rowEvent->registration_start_date = $data['registration_start_date_' . $i] . ' ' . $data['registration_start_date_hour_' . $i] . ':' . $data['registration_start_date_minute_' . $i] . ':00';
			$rowEvent->cut_off_date            = $data['cut_off_date_' . $i] . ' ' . $data['cut_off_date_hour_' . $i] . ':' . $data['cut_off_date_minute_' . $i] . ':00';
			//set additional properties **NOT** including anything in params
			$rowEvent->parent_id          = $row->id;
			$rowEvent->event_type         = 2;
			$rowEvent->is_additional_date = 1;
			$rowEvent->alias = JApplicationHelper::stringURLSafe($rowEvent->title . '-' . JHtml::_('date', $rowEvent->event_date, $config->date_format, null));
			$rowEvent->title = $rowEvent->title . ' (' . JHtml::_('date', $rowEvent->event_date, $config->date_format, null).')';
			//Check on the Alias and create if needed
			if($rowEvent->alias) {
				$aliasExists = AxsEvents::checkAlias($rowEvent->alias,$rowEvent->id);
				if($aliasExists) {
					$random = rand(100,1000);
					$rowEvent->alias = $rowEvent->alias.'-'.$random;
				}
			}
			//Check the locationId and pull the Parent's if needed
			if($data['location_id_' . $i]){
				$rowEvent->location_id = $data['location_id_' . $i];
			}
			else{
				$rowEvent->location_id = $row->location_id;
			}
			if (!$rowEvent->id)
			{				
				$rowEvent->hits = 0;//Unsure what this is used for... if you do, please add it.
			}
			if ($id == 0)
			{
				$isChildEventNew = true;
			}
			else
			{
				$isChildEventNew = false;
			}
			/*
			* Virtual Meetings are stored in the joom_eb_events table
			* BUT it is nested in the params column
			* SO my steps are:
			*  1. select current params from DB
			*  2. json_decode to an $Object
			*  3. update the $Object->virtual_meeting with $virtualMeeting
			*  4. json_encode to $rowEvent->params
			* The end
			*/ 
			$eventID = $data['event_id_' . $i];
			$virtualMeetingKey = "virtual_meeting_$i";
			$timeZoneKey = "time_zone_$i";
			$calendarLocationKey = "calendar_location_$i";
			//IF we get here, we know that there is a child event with a date
			if ($eventID > 0){//if it is not new, we update the record
				$childEventParams 			= $this->getEventDBParams($eventID);
				
				if(!empty($data[$virtualMeetingKey])){
					$submittedVirtualMeeting = $data[$virtualMeetingKey];
				}
				else {
					$submittedVirtualMeeting =  $data['virtual_meeting'];//parent
				}
				if(!empty($data[$timeZoneKey])){
					$submittedTimeZone 	= $data[$timeZoneKey];
				}
				else {
					$submittedTimeZone = $data['time_zone'];
				}
				if(!empty($data[$calendarLocationKey])){
					$submittedCalendarLocation 	= $data[$calendarLocationKey];
				}
				else {
					$submittedCalendarLocation = $data['calendar_location'];
				}
				
				$childEventParams->virtual_meeting = $submittedVirtualMeeting;
				$childEventParams->time_zone = $submittedTimeZone;
				$childEventParams->calendar_location = $submittedCalendarLocation;
				$rowEvent->params = json_encode($childEventParams);
			}
			else{// If it is new, we check to see if params are set from submitted $data
				
				$childPrams = json_decode($row->params);//str to obj
				// virtual_meeting
				if(!empty($data[$virtualMeetingKey])){
					$childPrams->virtual_meeting = $data[$virtualMeetingKey];
				}
				else{
					$virtualMeeting = $data['virtual_meeting'];//parent
					$childPrams->virtual_meeting = $virtualMeeting;//grab what was sent via the parent
				}
				//time_zone
				if(!empty($data[$timeZoneKey])){
					$childPrams->time_zone = $data[$timeZoneKey];
				}
				else{
					$time_zone = $data['time_zone'];
					$childPrams->time_zone = $time_zone;//grab what was sent via the parent
				}
				//$calendarLocationKey
				if(!empty($data[$calendarLocationKey])){
					$childPrams->calendar_location = $data[$calendarLocationKey];
				}
				else{

					$calendar_location = $data['calendar_location'];
					$childPrams->calendar_location = $calendar_location;//grab what was sent via the parent
				}
				$rowEvent->params =json_encode($childPrams);

			}

			$rowEvent->store();

			$numberChildrenEvents++;


			// Store categories
			$this->storeEventCategories($rowEvent->id, $data, $isChildEventNew);

			// Store price
			$this->storeEventGroupRegistrationRates($rowEvent->id, $data, $isChildEventNew);

			$additionalEventIds[] = $rowEvent->id;
		}

		if ($numberChildrenEvents)
		{
			$row->event_type = 1;
		}

		$row->store();

		// Remove the events which are removed by users
		if (!$isNew)
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('id')
					->from('#__eb_events')
					->where('parent_id = ' . $row->id)
					->where('is_additional_date = 1');
			$db->setQuery($query);
			$allChildrenEventIds = $db->loadColumn();

			if (count($allChildrenEventIds))
			{
				$deletedEventIds = array_diff($allChildrenEventIds, $additionalEventIds);

				if (count($deletedEventIds))
				{
					$model = new EventbookingModelEvent();

					$model->delete($deletedEventIds);
				}
			}
		}

		if ($numberChildrenEvents)
		{
			EventbookingHelper::updateParentMaxEventDate($row->id);
		}
	}

	/**
	 * Display form allows users to change settings on events add/edit screen
	 *
	 * @param object $row
	 */
	private function drawSettingForm($row)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$nullDate = $db->getNullDate();
		$format = 'Y-m-d';
		
		$rowEvents = array();
		
		if ($row->id > 0)
		{	//CW: Add params so we can get current virtual meeting uuid	
			$query->select('id, event_date, event_end_date, cut_off_date, registration_start_date, location_id,params')
					->from('#__eb_events')
					->where('parent_id = ' . (int) $row->id)
					->where('is_additional_date = 1')
					->order('id');
			$db->setQuery($query);
			$rowEvents = $db->loadObjectList();
		}
		
		$maxNumberDates = max(10, count($rowEvents));

		$options                    = array();
		$options[]                  = JHtml::_('select.option', 0, JText::_('EB_SELECT_PARENT'), 'id', 'name');
		$options                    = array_merge($options, EventbookingHelperDatabase::getAllLocations());
		?>
		<div id="advance-date_content">
			<?php

			for($i = 0; $i < $maxNumberDates ; $i++)
			{
				//clear last loop's vars
				unset($time_zone);
				unset($locationId);
				unset($virtualMeetingUUID);
				unset($calendar_location);

				if (isset($rowEvents[$i]))
				{
						
					$rowEvent     = $rowEvents[$i];
					$eventId      = $rowEvent->id;
					$eventDate    = $rowEvent->event_date;
					$eventEndDate = $rowEvent->event_end_date;
					$cutOffDate   = $rowEvent->cut_off_date;
					$registrationStartDate = $rowEvent->registration_start_date;
					
					/*/
					|--------------------------------------------------------------------------
					| overly verbose - I am trying to figure out how the underlining code works. 
					|--------------------------------------------------------------------------
					*/
					$childEventParamsDBstring 	  = ( $rowEvent->params);
					$childEventParamsDB	= json_decode($childEventParamsDBstring);
					/*
					 * if the params do not have a virtual_meeting, time_zone 
					 *  calendar_location property
					 * In the else clause, set the virtual_meeting, time_zone 
					 *  calendar_location property  with the parent's info...
					 * If you comment out the else clauses, it will show a null 
					 *  and will display "select a..."  in the dropdown lists 
					 */ 
					if (!empty($childEventParamsDB->virtual_meeting)){
						$virtualMeetingUUID = $childEventParamsDB->virtual_meeting;
					}
					// else { //use the parent Virtual meeting UUID
					// 	$virtualMeetingUUID = $row->params->virtual_meeting;
					// }
					if (!empty($childEventParamsDB->time_zone)){
						$time_zone = $childEventParamsDB->time_zone;
					}
					// elseif (!empty($row->params->time_zone)) { //use the parent 
					// 	$time_zone = $row->params->time_zone;
					// }
					//I do not like pre filling out the form. Uncomment to have it default to Boise
					// else {
					// 	$time_zone = 'America/Boise'; //same as 
					// 	//administrator/components/com_eventbooking/view/event/tmpl/default.php
					// 	// line ~32ish
					// }

					if (!empty($childEventParamsDB->calendar_location)){
						$calendar_location = $childEventParamsDB->calendar_location;
					}
					// else { //use the parent
					// 	$calendar_location = $row->params->calendar_location;
					// }

					/*
					 * Do the same thing with $locationId   = $rowEvent->location_id;
					 */
					if(!empty($rowEvent->location_id)){
						$locationId   =  $rowEvent->location_id;
					}
					// else {
					// 	$locationId   = $row->location_id;//PARENT LOCATION
					// }

					// Brake dates into time parts
					$eventDateHour   = JHtml::_('date', $eventDate, 'G', null);
					$eventDateMinute = JHtml::_('date', $eventDate, 'i', null);
					if ($eventEndDate == $nullDate)
					{
						$eventEndDateHour = 0;
						$eventEndDateMinute = 0;
					}
					else
					{
						$eventEndDateHour   = JHtml::_('date', $eventEndDate, 'G', null);
						$eventEndDateMinute = JHtml::_('date', $eventEndDate, 'i', null);
					}

					if ($cutOffDate == $nullDate)
					{
						$cutOffDateHour = 0;
						$cutOffDateMinute = 0;
					}
					else
					{
						$cutOffDateHour   = JHtml::_('date', $cutOffDate, 'G', null);
						$cutOffDateMinute = JHtml::_('date', $cutOffDate, 'i', null);
					}

					if ($registrationStartDate == $nullDate)
					{
						$registrationStartDateHour = 0;
						$registrationStartDateMinute = 0;
					}
					else
					{
						$registrationStartDateHour   = JHtml::_('date', $registrationStartDate, 'G', null);
						$registrationStartDateMinute = JHtml::_('date', $registrationStartDate, 'i', null);
					}
				}
				else // $rowEvents[$i] is not set
				{
					$eventId               = 0;
					$eventDate             = $nullDate;
					$eventEndDate          = $nullDate;
					$registrationStartDate = $nullDate;
					$cutOffDate            = $nullDate;
					
					// I am not a fan of having the form pre filled out. 
					// $locationId            = $row->location_id;
					// $virtualMeetingUUID	   = $row->params->virtual_meeting;//parent event virtual Meeting UUID

					// if($row->params->time_zone) {//parent event Time Zone
					// 	$time_zone = $row->params->time_zone;
					// } 
					
					// else {
					// 	$time_zone = 'America/Boise'; //see line ~32ish of file
					// 	// administrator/components/com_eventbooking/view/event/tmpl/default.php
					// }
					
					// $calendar_location	   = $row->params->calendar_location;//parent Add to Calendar Location
					// Brake date into it's time parts to set the calendar... same as above. NOT DRY @todo: stay dry
					$eventDateHour   = JHtml::_('date', $row->event_date, 'G', null);
					$eventDateMinute = JHtml::_('date', $row->event_date, 'i', null);
					if ($row->event_end_date == $nullDate)
					{
						$eventEndDateHour = 0;
						$eventEndDateMinute = 0;
					}
					else
					{
						$eventEndDateHour   = JHtml::_('date', $row->event_end_date, 'G', null);
						$eventEndDateMinute = JHtml::_('date', $row->event_end_date, 'i', null);
					}

					if ($row->cut_off_date == $nullDate)
					{
						$cutOffDateHour = 0;
						$cutOffDateMinute = 0;
					}
					else
					{
						$cutOffDateHour   = JHtml::_('date', $row->cut_off_date, 'G', null);
						$cutOffDateMinute = JHtml::_('date', $row->cut_off_date, 'i', null);
					}

					if ($row->registration_start_date == $nullDate)
					{
						$registrationStartDateHour = 0;
						$registrationStartDateMinute = 0;
					}
					else
					{
						$registrationStartDateHour   = JHtml::_('date', $row->registration_start_date, 'G', null);
						$registrationStartDateMinute = JHtml::_('date', $row->registration_start_date, 'i', null);
					}
				}
				?>
				<fieldset id="date_<?php echo $i; ?>" class="form-inline form-inline-header">
					<input type="hidden" name="event_id_<?php echo $i; ?>" value="<?php echo $eventId; ?>" />
					<input type="hidden" name="count_additional_date[]" value="" />
					<div class="row">
						<legend><?php echo JText::sprintf('EB_EVENT_DATE_COUNT', ($i + 1)); ?></legend>
					</div>
					<div class="row mb-3">
						<div class="col-sm-3">
							<?php echo JText::_('EB_EVENT_START_DATE'); ?>
						</div>
						<div class="col-sm-6">
							<div class="row mb-3">
								<div class="col-sm-6">
									<?php echo JHtml::_('calendar', ($eventDate == $nullDate) ? '' : JHtml::_('date', $eventDate, $format, null), 'event_date_' . $i, 'event_date_' . $i, '%Y-%m-%d', null) ; ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 23, 1, 'event_date_hour_' . $i, ' class="input-mini" ', $eventDateHour); ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 55, 5, 'event_date_minute_' . $i, ' class="input-mini" ', $eventDateMinute, '%02d'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_EVENT_END_DATE'); ?>
						</label>
						<div class="col-sm-6">
							<div class="row mb-3">
								<div class="col-sm-6">
									<?php echo JHtml::_('calendar', ($eventEndDate == $nullDate) ? '' : JHtml::_('date', $eventEndDate, $format, null), 'event_end_date_' . $i, 'event_end_date_' . $i, '%Y-%m-%d', null) ; ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 23, 1, 'event_end_date_hour_' . $i, ' class="input-mini" ', $eventEndDateHour); ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 55, 5, 'event_end_date_minute_' . $i, ' class="input-mini" ', $eventEndDateMinute, '%02d'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_REGISTRATION_START_DATE'); ?>
						</label>
						<div class="col-sm-6">
							<div class="row mb-3">
								<div class="col-sm-6">
									<?php echo JHtml::_('calendar', ($registrationStartDate == $nullDate) ? '' : JHtml::_('date', $registrationStartDate, $format, null), 'registration_start_date_' . $i, 'registration_start_date_' . $i, '%Y-%m-%d', null) ; ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 23, 1, 'registration_start_date_hour_' . $i, ' class="input-mini" ', $registrationStartDateHour); ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 55, 5, 'registration_start_date_minute_' . $i, ' class="input-mini" ', $registrationStartDateMinute, '%02d'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_CUT_OFF_DATE'); ?>
						</label>
						<div class="col-sm-6">
							<div class="row mb-3">
								<div class="col-sm-6">
									<?php echo JHtml::_('calendar', ($cutOffDate == $nullDate) ? '' : JHtml::_('date', $cutOffDate, $format, null), 'cut_off_date_' . $i, 'cut_off_date_' . $i, '%Y-%m-%d', null) ; ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 23, 1, 'cut_off_date_hour_' . $i, ' class="input-mini" ', $cutOffDateHour); ?>
								</div>
								<div class="col-sm-2">
									<?php echo JHtml::_('select.integerlist', 0, 55, 5, 'cut_off_date_minute_' . $i, ' class="input-mini" ', $cutOffDateMinute, '%02d'); ?>
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_LOCATION'); ?>
						</label>
						<div class="col-sm-9">
							<?php echo JHtml::_('select.genericlist', $options, 'location_id_' . $i, '', 'id', 'name', $locationId); ?>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_VIRTUAL_MEETING'); ?>
						</label>
						<div class="col-sm-9">
							<?php
							$subEvent = $i;//CW: I am verbose on purpose
							$selectorAppendString = "_$subEvent";
							echo AxsVirtualClassroom::getMeetingDropDown($virtualMeetingUUID,$selectorAppendString,'Use Parent Virtual Meeting');
							?>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_CALENDAR_LOCATION'); ?>
						</label>
						<div class="col-sm-9">
						<select name="calendar_location_<?=$i?>">
								<option value="" 
								<?php if($calendar_location !== 'physical_location' && $calendar_location !== 'virtual') { echo 'selected'; } ?>
								>Use Parent Calendar Location Type</option>
								<option value="physical_location" 
								<?php if($calendar_location == 'physical_location') { echo 'selected'; } ?>
								>Use Physical Location</option>
								<option value="virtual" <?php if($calendar_location == 'virtual') { echo 'selected'; } ?>>Use Virtual Meeting</option>
							</select>
						</div>
					</div>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_TIME_ZONE'); ?>
						</label>
						<div class="col-sm-9">
							<?php 
							$subEvent = $i;//CW: I am verbose on purpose
							$selectorAppendString = "_$subEvent";
								echo AxsTimezones::getTimezoneList($time_zone,$selectorAppendString,'Use Parent Time Zone');
							?>
						</div>
					</div>
					<div class="row mb-3">
                        <div class="col-sm-3">
						    <button type="button" class="btn btn-danger" onclick="removeEventContainer(<?php echo $i; ?>)"><i class="icon-remove"></i><?php echo JText::_('EB_REMOVE'); ?></button>
						</div>
					</div>
				</fieldset>
			<?php
			}//end of the for loop
			?>
		</div>
		<div class="row-fluid">
			<!-- <button type="button" class="btn btn-success" onclick="addEventContainer()"><i class="icon-new icon-white"></i><?php echo JText::_('EB_ADD'); ?></button> -->
			<input type="hidden" id="count_event_dates" name="count_event_dates" value="<?php echo $maxNumberDates; ?>" />
		</div>
		<script language="JavaScript">
			function removeEventContainer(id)
			{
				if (confirm('<?php echo JText::_('EB_REMOVE_ITEM_CONFIRM'); ?>'))
				{
					jQuery('#date_' + id).remove();
				}
			}

			(function($){
				var countDate = '<?php echo $maxNumberDates;?>';
				addEventContainer  = (function(){					
					var  html = '<fieldset id="date_' + countDate + '" class="form-inline form-inline-header">'
					html += '<legend class="item_date_' + countDate + '"></legend>';
					html += '<input type="hidden" name="event_id_' + countDate + '" value="0" />';
					//event start date
					html += '<div class="row mb-3">';
					html += '<label class="col-sm-3"><?php echo JText::_('EB_EVENT_START_DATE'); ?></label>';
					html += '<div class="col-sm-9">';
					html += '<div class="input-group">';
					html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="event_date_'+countDate+'" name="event_date_'+countDate+'">';
					html += '<button id="event_date_'+countDate+'_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
					html += '</div>';
					html += '<?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 23, 1, 'event_date_hour_' . $i, ' class="input-mini event_date_hour" ', $eventDateHour)); ?><?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 55, 5, 'event_date_minute_' . $i, ' class="input-mini event_date_minute" ', $eventDateMinute, '%02d')); ?>';
					html += '</div>';
					html += '</div>';
					//event end date
					html += '<div class="row mb-3">';
					html += '<label class="col-sm-3"><?php echo JText::_('EB_EVENT_END_DATE'); ?></label>';
					html += '<div class="col-sm-9">';
					html += '<div class="input-group">';
					html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="event_end_date_'+countDate+'" name="event_end_date_'+countDate+'">';
					html += '<button id="event_end_date_'+countDate+'_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
					html += '</div>';
					html += '<?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 23, 1, 'event_end_date_hour_' . $i, ' class="input-mini event_end_date_hour" ', $eventEndDateHour)); ?> <?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 55, 5, 'event_end_date_minute_' . $i, ' class="input-mini event_end_date_minute" ', $eventEndDateMinute, '%02d')); ?>';
					html += '</div>';
					html += '</div>';
					//registranstart date
					html += '<div class="row mb-3">';
					html += '<label class="col-sm-3"><?php echo JText::_('EB_REGISTRATION_START_DATE'); ?></label>';
					html += '<div class="col-sm-9">';
					html += '<div class="input-group">';
					html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="registration_start_date_'+countDate+'" name="registration_start_date_'+countDate+'">';
					html += '<button id="registration_start_date_'+countDate+'_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
					html += '</div>';
					html += '<?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 23, 1, 'registration_start_date_hour_' . $i, ' class="registration_start_date_hour input-mini" ', $registrationStartDateHour)); ?> <?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 55, 5, 'registration_start_date_minute_' . $i, ' class="registration_start_date_minute input-mini" ', $registrationStartDateMinute, '%02d')); ?>';
					html += '</div>';
					html += '</div>';
					//cut of date
					////////////////////////////////////
					html += '<div class="row mb-3">';
					html += '<label class="col-sm-3"><?php echo JText::_('EB_CUT_OFF_DATE'); ?></label>';
					html += '<div class="col-sm-9">';
					html += '<div class="input-group">';
					html += '<input type="text" style="width: 100px;" class="input-medium hasTooltip" value="" id="cut_off_date_'+countDate+'" name="cut_off_date_'+countDate+'">';
					html += '<button id="cut_off_date_'+countDate+'_img" class="btn" type="button"><i class="icon-calendar"></i></button>';
					html += '</div>';
					html += '<?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 23, 1, 'cut_off_date_hour_' . $i, ' class="cut_off_date_hour input-mini" ', $cutOffDateHour)); ?> <?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.integerlist', 0, 55, 5, 'cut_off_date_minute_' . $i, ' class="cut_off_date_minute input-mini" ', $cutOffDateMinute, '%02d')); ?>';
					html += '</div>';
					html += '</div>';
					//location
					html += '<div class="row mb-3">';
					html += '<label class="col-sm-3"><?php echo JText::_('EB_SELECT_LOCATION'); ?></label>';
					html += '<div class="col-sm-9">';
					html += `<?php echo preg_replace(array('/\r/', '/\n/'), '', JHtml::_('select.genericlist', $options, 'location_id_' . $i, 'class="location_id"', 'id', 'name', $locationId)); ?>`;
					html += '</div>';
					html += '</div>';
					//remove button
					html += '<div class="row mb-3">';
					html += '<button type="button" class="btn btn-danger" onclick="removeEventContainer('+countDate+')"><i class="icon-remove"></i><?php echo JText::_('EB_REMOVE'); ?></button>';
					html += '</div>';
					html += '</fieldset>';
					////////////////////////////////////////////////////////
					$('#advance-date_content').append(html);
					var countNumber = countDate;
					countNumber++;
					$('legend.item_date_'+countDate).text('Extra Event Date '+ countNumber);
					$("#date_"+countDate+" .event_date_hour").attr("name","event_date_hour_"+countDate);
					$("#date_"+countDate+" .event_date_minute").attr("name","event_date_minute_"+countDate);
					$("#date_"+countDate+" .event_end_date_hour").attr("name","event_end_date_hour_"+countDate);
					$("#date_"+countDate+" .event_end_date_minute").attr("name","event_end_date_minute_"+countDate);
					$("#date_"+countDate+" .registration_start_date_hour").attr("name","registration_start_date_hour_"+countDate);
					$("#date_"+countDate+" .registration_start_date_minute").attr("name","registration_start_date_minute_"+countDate);
					$("#date_"+countDate+" .cut_off_date_hour").attr("name","cut_off_date_hour_"+countDate);
					$("#date_"+countDate+" .cut_off_date_minute").attr("name","cut_off_date_minute_"+countDate);
					$("#date_"+countDate+" .location_id").attr("name","location_id_"+countDate);
					Calendar.setup({
						// Id of the input field
						inputField: "event_date_"+countDate,
						// Format of the input field
						ifFormat: "%Y-%m-%d",
						// Trigger for the calendar (button ID)
						button: "event_date_"+countDate+"_img",
						// Alignment (defaults to "Bl")
						align: "Tl",
						singleClick: true,
						firstDay: 0
					});
					Calendar.setup({
						// Id of the input field
						inputField: "registration_start_date_"+countDate,
						// Format of the input field
						ifFormat: "%Y-%m-%d",
						// Trigger for the calendar (button ID)
						button: "registration_start_date_"+countDate+"_img",
						// Alignment (defaults to "Bl")
						align: "Tl",
						singleClick: true,
						firstDay: 0
					});
					Calendar.setup({
						// Id of the input field
						inputField: "event_end_date_"+countDate,
						// Format of the input field
						ifFormat: "%Y-%m-%d",
						// Trigger for the calendar (button ID)
						button: "event_end_date_"+countDate+"_img",
						// Alignment (defaults to "Bl")
						align: "Tl",
						singleClick: true,
						firstDay: 0
					});
					Calendar.setup({
						// Id of the input field
						inputField: "cut_off_date_"+countDate,
						// Format of the input field
						ifFormat: "%Y-%m-%d",
						// Trigger for the calendar (button ID)
						button: "cut_off_date_"+countDate+"_img",
						// Alignment (defaults to "Bl")
						align: "Tl",
						singleClick: true,
						firstDay: 0
					});
					countDate++;
					$('#count_event_dates').val(countDate);
				})
			})(jQuery)
		</script>
		<?php
	}

	/**
	 * Store categories of an event
	 *
	 * @param $eventId
	 * @param $data
	 * @param $isNew
	 */
	protected function storeEventCategories($eventId, $data, $isNew)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$isNew)
		{
			$query->delete('#__eb_event_categories')->where('event_id=' . $eventId);
			$db->setQuery($query);
			$db->execute();
		}
		$mainCategoryId = (int) $data['main_category_id'];

		if ($mainCategoryId)
		{
			$query->clear();
			$query->insert('#__eb_event_categories')
					->columns('event_id, category_id, main_category')
					->values("$eventId, $mainCategoryId, 1");
			$db->setQuery($query);
			$db->execute();
		}

		$categories = isset($data['category_id']) ? $data['category_id'] : array();

		for ($i = 0, $n = count($categories); $i < $n; $i++)
		{
			$categoryId = (int) $categories[$i];
			if ($categoryId && ($categoryId != $mainCategoryId))
			{
				$query->clear();
				$query->insert('#__eb_event_categories')
						->columns('event_id, category_id, main_category')
						->values("$eventId, $categoryId, 0");
				$db->setQuery($query);
				$db->execute();
			}
		}
	}

	/**
	 * Store group registration rates of an event
	 *
	 * @param $eventId
	 * @param $data
	 * @param $isNew
	 */
	protected function storeEventGroupRegistrationRates($eventId, $data, $isNew)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		if (!$isNew)
		{
			$query->delete('#__eb_event_group_prices')->where('event_id=' . $eventId);
			$db->setQuery($query);
			$db->execute();
		}

		$prices            = $data['price'];
		$registrantNumbers = $data['registrant_number'];
		for ($i = 0, $n = count($prices); $i < $n; $i++)
		{
			$price            = $prices[$i];
			$registrantNumber = $registrantNumbers[$i];
			if (($registrantNumber > 0) && ($price > 0))
			{
				$query->clear();
				$query->insert('#__eb_event_group_prices')
						->columns('event_id, registrant_number, price')
						->values("$eventId, $registrantNumber, $price");
				$db->setQuery($query);
				$db->execute();
			}
		}
	}
	
	protected function getEventDBParams($eventID){
		$db = JFactory::getDbo();
        $conditions[] = $db->quoteName('id'). '=' .$db->quote($eventID);
        $query = $db->getQuery(true);
        $query->select('params');
        $query->from('joom_eb_events');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
		$eventParams = json_decode($result->params);
		return $eventParams;
		 
	}
}
