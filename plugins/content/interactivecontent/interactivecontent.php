<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.loadmodule
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Plugin to enable loading modules into content (e.g. articles)
 * This uses the {loadmodule} syntax
 *
 * @since  1.5
 */
class PlgContentInteractivecontent extends JPlugin
{
	protected static $modules = array();

	protected static $mods = array();



	/**
	 * Plugin that loads module positions within content
	 *
	 * @param   string   $context   The context of the content being passed to the plugin.
	 * @param   object   &$article  The article object.  Note $article->text is also available
	 * @param   mixed    &$params   The article params
	 * @param   integer  $page      The 'page' number
	 *
	 * @return  mixed   true if there is an error. Void otherwise.
	 *
	 * @since   1.6
	 */
	
	public function onContentPrepare($context, &$article, &$params, $page = 0){
    
    /************************* INTERACTIVE CONTENT SHORTCODE START ******************************/
	    
	   
    	require_once JPATH_ROOT.'/components/com_interactivecontent/autoloader.php';
		
   
		
		// Don't run this plugin when the content is being indexed
		if ($context === 'com_finder.indexer')
		{
			return true;
		}

		// Simple performance check to determine whether bot should process further
		if (strpos($article->text, 'interactivecontent') === false)
		{
			return true;
		}

		
		if (strpos($article->text, 'id') === false)
		{
			return true;
		}

		// Expression to search for(modules)
		$regexh5p = '/{interactivecontent\s(.*?)}/i';
		$styleh5p = $this->params->def('style', 'none');

		preg_match_all($regexh5p, $article->text, $matchesh5p, PREG_SET_ORDER);


		// If no matches, skip this
		if ($matchesh5p)
		{
			foreach ($matchesh5p as $matchh5p)
			{
				$matchesh5plist = explode(',', $matchh5p[1]);

				// We may not have a specific module so set to null
				if (!array_key_exists(1, $matchesh5plist))
				{
					$matchesh5plist[1] = null;
				}

				// We may not have a module style so fall back to the plugin default.
				
				if (!array_key_exists(2, $matchesh5plist))
				{
					$matchesh5plist[2] = $styleh5p;
				}
				
				
				$h5p = trim($matchesh5plist[0]);
				$h5p_id = explode('=',$h5p)[1];
				

				$name   = htmlspecialchars_decode(trim($matchesh5plist[1]));
				$styleh5p  = trim($matchesh5plist[2]);
				

				// $match[0] is full pattern match, $match[1] is the module,$match[2] is the title
				
				$plugin = H5P_Plugin::get_instance();
				$plugin->enqueue_styles_and_scripts();
				$output = $plugin->shortcode($h5p_id);
				$plugin->add_settings();
				
				 

				// We should replace only first occurrence in order to allow positions with the same name to regenerate their content:
				
				$article->text = preg_replace(addcslashes("|$matchh5p[0]|", '()'), addcslashes($output, '\\$'), $article->text, 1);
				
				$styleh5p = $this->params->def('style', 'none');
				
			}
		}

		 /************************* INTERACTIVE CONTENT SHORTCODE END ******************************/
		
	}


	
}



