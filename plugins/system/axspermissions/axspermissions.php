<?php

defined('_JEXEC') or die;

/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

/**
 * Mylib plugin class.
 *
 * @package     Joomla.plugin
 * @subpackage  System.axspermissions
 */

//edit_admin
//edit_usergroup
//edit_self
//edit_users
//create_subadmin
//create_users

/*

    This plugin intercepts user actions and checks their permissions before allowing them to continue.

*/

class plgSystemAxspermissions extends JPlugin {

    private static $adminRoles;

    private static function getAdminRoles() {

        //Get the admin roles and store them.

        if (!isset(self::$adminRoles)) {
            $roles = new stdClass();
            $user_id = JFactory::getUser()->id;

            $roles->superUser = AxsUser::isSuperUser($user_id);
            $roles->admin = AxsUser::isAdmin($user_id);
            $roles->subadmin = AxsUser::isSubadmin($user_id);
            $roles->developer = AxsUser::isDeveloper($user_id);

            self::$adminRoles = $roles;
        }

        return self::$adminRoles;
    }

    public function onAfterRoute() {

        //For some reason... this won't load with the rest of the axs libraries. So load it manually.
        require_once(JPATH_LIBRARIES . "/axslibs/permissions.php");

        if (self::isAdminSection()) {
            //Only get the access permissions if they're in the admin section.
            AxsAdminPermissions::getPermissions();
        }
        
        AxsAccess::checkSiteAccessLevels();     //Check to see which Tovuti options the client has enabled overall.
        AxsAccess::checkViewingAccess();        //See if it's a restricted Super User Tovuti only section.
        AxsAccess::checkAdminPermissions();     //If they're sub-admin, see if they're allowed to go to the content
        AxsAccess::checkRestrictedContent();    //Content NO ONE (including developers) is allowed to go to.  System sensitive content.
    }    

    public function onContentBeforeSave($action, $tableObject, $isNew) {

        //Called before any content is saved.  Except users and user groups.

        //jDump('onContentBeforeSave');
        //return false;
    }

    public function onContentBeforeDelete($action, $tableObject) {

        //Called before any content is deleted.  Except users and user groups.

        //jDump('onContentBeforeDelete');
        //self::preventDelete();
    }

    public function onUserBeforeSave($previousValues, $isNew, $newValues) {

        if (!self::isAdminSection()) {
            //These checks are only for the admin section.
            return;
        }

        $roles = self::getAdminRoles();
        if ($roles->superUser || $roles->admin) {
            //They have full admin, don't block them from saving users.
            return;
        }

        $rights = self::getRights();

        if (!$rights['edit_users']) {
            //Insufficient priviledges to edit any accounts
            self::createErrorMessage('edit_users');
            return false;
        }

        if ($isNew) {
            //New User
            if (!$rights['create_users']) {
                //Insufficient priviledges to create new accounts
                self::createErrorMessage('create_users');
                return false;
            }
        } else {
            //Existing user
            if (!$rights['edit_self'] && JFactory::getUser()->id == $previousValues['id']) {
                //Insufficient priviledges to edit their own account.
                self::createErrorMessage('edit_self');
                return false;
            }
        }

        if (!$rights['edit_admin'] && AxsUser::isAdmin($previousValues['id'])) {
            //Insufficient priviledges to edit administrators
            self::createErrorMessage('edit_admin');
            return false;            
        }

        if (!$rights['edit_subadmin'] && AxsUser::isSubadmin($previousValues['id'])) {
            //Insufficient priviledges to edit sub-administrators
            self::createErrorMessage('edit_subadmin');
            return false;
        }

        if (!$rights['create_admin']) {

            $admin_user_group = AxsUser::$restricted_user_groups["admin"];

            $startAdmin = in_array($admin_user_group, $previousValues['groups']);
            $endAdmin = in_array($admin_user_group, $newValues['groups']);

            if ($startAdmin != $endAdmin) {
                //Insufficient priviledges to change admin user group
                //They've either added or removed the admin user group
                self::createErrorMessage('create_admin');
                return false;
            }
        }

        if (!$rights['create_subadmin']) {

            $subadmin_user_group = AxsUser::$restricted_user_groups["subadmin"];

            $startSubAdmin = in_array($subadmin_user_group, $previousValues['groups']);
            $endSubAdmin = in_array($subadmin_user_group, $newValues['groups']);

            if ($startSubAdmin != $endSubAdmin) {
                //Insufficient priviledges to change sub-admin user group
                //They've either added or removed the admin user group
                self::createErrorMessage('create_subadmin');
                return false;
            }
        }
    }

    public function onUserBeforeDelete($data) {
        $app = JFactory::getApplication();
        $option = $app->input->get('option');
        $task = $app->input->get('task');
        $view = $app->input->get('view');

        $roles = self::getAdminRoles();
        if ( ($roles->superUser || $roles->admin) || ( $option == 'com_eventbooking' && ( $task == 'process_individual_registration' ||  $task == 'process_group_registration') ) ) {
            return;
        }
        
        $rights = self::getRights();

        if (!$rights['delete_users']) {
            //See if they have the right to delete users.
            self::preventDelete('delete_users');
            return;
        }
        if (AxsUser::isAdmin($data['id']) && !$rights['delete_admin']) {
            //See if they have the right to delete admin
            self::preventDelete('delete_admin');
            return;
        }

        if (AxsUser::isSubadmin($data['id']) && !$rights['delete_subadmin']) {
            //see if they have the right ot delete sub-admin.
            self::preventDelete('delete_subadmin');
            return;
        }
    }

    public function onUserBeforeSaveGroup($action, $tableObject, $isNew, $data) {
        
        if (in_array($data['id'], AxsUser::$restricted_user_groups)) {
            //Don't allow this for anyone.  Changing these is detrimental, even if you're a super user.
            self::createErrorMessage('restricted_user_group');
            return false;
        }

        $roles = self::getAdminRoles();
        if ($roles->superUser || $roles->admin) {
            return;
        }

        $rights = self::getRights();

        if (!$rights['edit_usergroup']) {
            self::createErrorMessage('edit_usergroup');
            return false;
        }
    }

    public function onUserBeforeDeleteGroup($data) {
        
        if (in_array($data['id'], AxsUser::$restricted_user_groups)) {
            //Don't allow this for anyone.  Changing these is detrimental, even if you're a super user.
            self::preventDelete('restricted_user_group');
            return;
        }

        $roles = self::getAdminRoles();
        if ($roles->superUser || $roles->admin) {
            return;
        }

        $rights = self::getRights();

        if (!$rights['edit_usergroup']) {
            self::preventDelete('edit_usergroup');
            return;
        }
    }

    public function onUserBeforeBatchSetGroup($action, $users) {

        $roles = self::getAdminRoles();
        if ($roles->superUser || $roles->admin) {
            return;
        }

        $rights = self::getRights();

        if (!$rights['edit_users']) {
            //Can't edit users
            self::createErrorMessage('edit_users');
            return false;
        }

        if (!$rights['edit_usergroup']) {
            //Can't edit user groups
            self::createErrorMessage('edit_usergroup');
            return false;
        }

        if (!$rights['create_admin'] && AxsUser::$restricted_user_groups["admin"] == $action['group_id']) {
            //Can't assign admin rights
            self::createErrorMessage('create_admin');
            return false;
        }

        if (!$rights['create_subadmin'] && AxsUser::$restricted_user_groups["subadmin"] == $action['group_id']) {
            //Can't assign sub admin rights.
            self::createErrorMessage('create_subadmin');
            return false;
        }

        //If they have rights for both, don't bother looping through all of the users.
        if (!$rights['edit_admin'] || !$rights['edit_subadmin']) {
            foreach ($users as $user) {
                if (!$rights['edit_admin'] && AxsUser::isAdmin($user)) {
                    //Can't edit admin accounts.
                    self::createErrorMessage('edit_admin');
                    return false;
                }

                if (!$rights['edit_subadmin'] && AxsUser::isSubadmin($user)) {
                    //Can't edit subadmin accounts.
                    self::createErrorMessage('edit_subadmin');
                    return false;
                }
            }
        }

        return false;
    }

    private static function isAdminSection() {

        $app = JFactory::getApplication();

        if ($app->isAdmin()) {
            return true;
        } else {
            return false;
        }
    }

    private static function getErrorMessage($action) {
        $lang = JFactory::getLanguage();
        $lang->load('plg_system_axspermissions', JPATH_ADMINISTRATOR);
        switch ($action) {
            case "edit_users":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_USERS');                
            case "edit_users":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_USERS');                
            case "create_users":
                return JText::_('PLG_AXSPERMISSIONS_CREATE_USERS');                
            case "edit_self":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_SELF');                
            case "edit_usergroup":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_USERGROUP');                
            case "restricted_user_group":
                return JText::_('PLG_AXSPERMISSIONS_RESTRICTED_USER_GROUP');                
            case "edit_admin":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_ADMIN');                
            case "edit_subadmin":
                return JText::_('PLG_AXSPERMISSIONS_EDIT_SUBADMIN');                
            case "create_admin":
                return JText::_('PLG_AXSPERMISSIONS_CREATE_ADMIN');                
            case "create_subadmin":
                return JText::_('PLG_AXSPERMISSIONS_CREATE_SUBADMIN');                
            case "delete_admin":
                return JText::_('PLG_AXSPERMISSIONS_DELETE_ADMIN');                
            case "delete_subadmin":
                return JText::_('PLG_AXSPERMISSIONS_DELETE_SUBADMIN');                
            case "delete":
                return JText::_('PLG_AXSPERMISSIONS_GENERAL_DELETE');                
            default:
                return $action;
        }
    }

    private static function createErrorMessage($action) {       
        //remove error message for now 
        //JFactory::getApplication()->enqueueMessage(self::getErrorMessage($action), 'error');
    }

    private function preventDelete($action = null) {
        $app = JFactory::getApplication();
        if (!$action) {
            $action = "delete";
        }

        $message = self::getErrorMessage($action);

        $app->enqueueMessage($message, 'error');

        JError::raiseWarning(500, $message);

        if (isset($_SERVER['REQUEST_URI'])) {
            $app->redirect($_SERVER['REQUEST_URI']);
        } else {
            $app->redirect('/administrator');
        }
    }

    private function getRights() {
        if (isset($_SESSION['admin_access_levels']) && isset($_SESSION['admin_access_levels']->rights)) {
            return $_SESSION['admin_access_levels']->rights;
        }
    }
}
