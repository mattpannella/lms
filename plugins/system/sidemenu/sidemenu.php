<?php
defined ('_JEXEC') or die;
class plgSystemSidemenu extends JPlugin {
  
    public function onContentPrepareForm($form, $data) {

        $option = JFactory::getApplication()->input->get('option');
        
        if ($option == 'com_menus') {
            ob_start();

            ?>
                <fields name="params">
                    <fieldset 
                        name="basic" 
                        label="Options"
                        addfieldpath="/administrator/components/com_axs/fields"
                    >
                        <field 
                            name="full_width"
                            type="radio"
                            label="Full Width Layout"
                            default="0"
                            class="btn-group"
                        >
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </field>

                        <field 
                            name="sidemenu-icon" 
                            type="icon"
                            class="icon"
                            label="Side Menu Icon"
                            
                        >
                        </field>
                        <field 
                            name="sidemenu-icon-position" 
                            type="list" 
                            label="Icon Position" 
                            description="Set to Top, Left, or Right">
                            <option value="top">Top</option>
                            <option value="left">Left</option>
                            <option value="right">Right</option>
                        </field>

                        <field
                            name="header_background"
                            label="Header Background Image"
                            type="media"
                        />

                        <field
                            name="header_background_text"
                            label="Header Text"
                            type="text"
                        />

                        <field
                            name="header_background_text_size"
                            label="Header Text Size (pixels)"
                            type="number"
                        />

                        <field
                            name="header_background_text_font"
                            label="Header Font"
                            type="font"
                        />
                       <!--  <field 
                            name="tour-order" 
                            type="number" 
                            label="Tour Order">
                        </field>
                        <field 
                            name="tour-text" 
                            type="text" 
                            label="Tour Description">
                        </field>
                        <field 
                            name="tour-position" 
                            type="list" 
                            label="Tour Position">
                            <option value="right">Right</option>
                            <option value="left">Left</option>
                            <option value="top">Top</option>
                            <option value="bottom">Bottom</option>
                        </field> -->
                    </fieldset>
                </fields>
            <?php

            $newField = ob_get_clean();
            $xmlElement = new SimpleXMLElement($newField);
            $form->setField($xmlElement);
        }
    } 
}

?>