var pathname = window.location.pathname;

if (pathname.indexOf('administrator') !== -1) {
	var changelinks = function() {

		var dropDownUser = document.getElementsByClassName('menu-user');
		
		if (dropDownUser.length > 0) {
			/*
				Change drop down menu
			*/
			for (i = 0; i < dropDownUser.length; i++) {
				var userMenu = dropDownUser[i];
				userMenu.href = 'index.php?option=com_users&view=axsusers';
			}

			var addUserMenu = document.getElementById('menu-com-users-users');
			if (addUserMenu) {
				var items = addUserMenu.getElementsByTagName('li');

				/*
					Change user links to go to individual user
				*/

				for (i = 0; i < items.length; i++) {
					var link = items[i].getElementsByTagName('a');
					for (j = 0; j < link.length; j++) {
						var menu_href = link[j].href.replace('task=user', 'task=axsuser');
						link[j].href = menu_href;
					}
				}
			}
		}

		/*
			Change the menus on the side bar in the users menu
		*/

		var submenu = document.getElementById('submenu');
		if (submenu) {
			var list = submenu.getElementsByTagName('li');
			for (i = 0; i < list.length; i++) {
				var link = list[i].getElementsByTagName('a');
				for (j = 0; j < link.length; j++) {
					var menu_href = link[j].href.replace('view=user', 'view=axsuser');
					link[j].href = menu_href;
				}
			}
		}

		/*
			Change the menus on the side bar in the administrator menu.
		*/

		var sideMenu = document.getElementsByClassName('nav-list');
		if (sideMenu) {
			for (i = 0; i < sideMenu.length; i++) {
				var link = sideMenu[i].getElementsByTagName('li');
				for (j = 0; j < link.length; j++) {
					var menu = link[j].getElementsByTagName('a');
					for (k = 0; k < menu.length; k++) {
						menu_href = menu[k].href;
						if (menu_href.indexOf('option=com_users') > 1) {
							menu[k].href = menu[k].href.replace('option=com_users', 'option=com_users&view=axsusers');
						}
					}
				}
			}
		}

		var controls = document.getElementsByClassName('controls');
		if (controls) {
			for (i = 0; i < controls.length; i++) {
				var menu = controls[i].getElementsByTagName('a');
					for (k = 0; k < menu.length; k++) {
						menu_href = menu[k].href;
						if (menu_href.indexOf('view=users') > 1) {
							menu[k].href = menu[k].href.replace('view=users', 'view=axsusers');
						}
					}
				
			}
		}	
	}

	window.addEventListener ?
	window.addEventListener('load', changelinks, false) :
	window.attachEvent && window.attachEvent('onload', changelinks);

	var search = window.location.search;

	if (search.indexOf('?option=com_users') !== -1) {
		var href = window.location.href;

		var newAddress = null;

		if (search == '?option=com_users&view=users' || search == '?option=com_users') {
			newAddress = window.location.origin + window.location.pathname + "?option=com_users&view=axsusers";			
		} else if (href.indexOf('task=user') !== -1) {
			newAddress = href.replace('task=user', 'task=axsuser');
		} else if (href.indexOf('view=user&layout=edit') !== -1) {
			newAddress = href.replace('view=user&layout=edit', 'task=axsuser.edit');
		}

		if (newAddress != null) {				
			window.location = newAddress;
		}
	}
}