$(document).ready(function() {
	//remove two factor authentication fro user profile edit form on the frontend community section
	$('#jform_twofactor_method-lbl').remove();
	$('#com_users_twofactor_forms_container').remove();
	$('#jform_twofactor_method').remove();
});