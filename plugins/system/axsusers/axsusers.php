<?php
defined('_JEXEC') or die;

class plgSystemAxsusers extends JPlugin {

	public function onUserBeforeSave($user) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$layout = $app->input->get('layout');
        if ( $option == 'com_users' && $layout == 'edit') {		
			$_SESSION['user_groups_before_save'] = $user['groups'];
		}
	}

	public function onUserAfterSave($user) {
		$app = JFactory::getApplication();
		$option = $app->input->get('option');
		$layout = $app->input->get('layout');
        if ( $option == 'com_users' && $layout == 'edit') {
			if(isset($_SESSION['user_groups_before_save'])) {
				$oldGroups = $_SESSION['user_groups_before_save'];
				foreach($user['groups'] as $user_group_id) {
					if(!in_array($user_group_id,$oldGroups)) {
						AxsActions::trackUserGroup($user['id'],$user_group_id,'add');
					}					
				}
				foreach($oldGroups as $old_user_group_id) {
					if(!in_array($old_user_group_id,$user['groups'] )) {
						AxsActions::trackUserGroup($user['id'],$old_user_group_id,'remove');
					}					
				}
				unset($_SESSION['user_groups_before_save']);
			}
		}			
	}

	public function onUserBeforeBatchSetGroup($action, $users) {
		/* jdump($action['group_action']);
		jdump($action, 'action');
		jdump($user, 'users'); */
		$group_id = $action['group_id'];
		switch($action['group_action']) {
			case 'add':
				$actionToTrack = 'add';
			break;
			case 'del':
				$actionToTrack = 'remove';
			break;
			case 'set':
				$actionToTrack = 'add';
			break;
		}
		foreach($users as $user_id) {
			$user_groups = JUserHelper::getUserGroups($user_id);
			if($action['group_action'] == 'set') {				
				foreach($user_groups as $user_group_id) {
					if($group_id != $user_group_id) {
						AxsActions::trackUserGroup($user_id,$user_group_id,'remove');
					}					
				}
			}
			if( (in_array($group_id, $user_groups) && $actionToTrack == 'remove') || ($actionToTrack == 'add' && !in_array($group_id, $user_groups)) ) {
				AxsActions::trackUserGroup($user_id,$group_id,$actionToTrack);
			}
		}        
    }
    
    public function onAfterInitialise() {

    	/*
    		This plugin changes all links to the original user view to the new axsuser view.
    	*/

		JFactory::getDocument()->addScript('/plugins/system/axsusers/axsusers.js');
	}
}