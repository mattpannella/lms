<?php
/**
 * J!Dump
 * @version      $Id$
 * @package      jdump
 * @copyright    Copyright (C) 2007 Mathias Verraes. All rights reserved.
 * @license      GNU/GPL
 * @link         https://github.com/mathiasverraes/jdump
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

jDumpMakeCookie();

jimport( 'joomla.event.helper' );

if( file_exists( JPATH_ADMINISTRATOR.'/components/com_dump/helper.php' ) ) {
    require_once( JPATH_ADMINISTRATOR.'/components/com_dump/helper.php' );
    require_once( JPATH_ADMINISTRATOR.'/components/com_dump/defines.php' );
} else {
    JError::raiseNotice( 20, 'The J!Dump Plugin needs the J!Dump Component to function.' );
}

$version = new JVersion();
if(!$version->isCompatible('2.5.5')) {
    JError::raiseNotice( 20, 'J!Dump requires Joomla 2.5.5 or later. For older Joomla versions, please use https://github.com/downloads/mathiasverraes/jdump/unzip_first_jdump_v2012-10-08.zip');
} 

class plgSystemDump extends JPlugin {

    public static $string = "jDump Admin";

    function __construct(& $subject, $params) {
        parent::__construct($subject, $params);
    }

    function onAfterRender() {
        $mainframe = JFactory::getApplication(); 
        $option = JRequest::getCmd('option');
        
        if($option == 'com_dump'){
            return;
        }

        // settings from config.xml
        $dumpConfig = JComponentHelper::getParams( 'com_dump' );
        $autopopup  = $dumpConfig->get( 'autopopup', 1 );

        $userstate = $mainframe->getUserState( 'dump.nodes' );
        $cnt_dumps  = is_countable($userstate) ? count( $userstate ) : 0;

        if( $autopopup && $cnt_dumps) {
            DumpHelper::showPopup();
        }
    }
}


/**
 * Add a variable to the list of variables that will be shown in the debug window
 * @param mixed $var The variable you want to dump
 * @param mixed $name The name of the variable you want to dump
 */
function jDump( $var = null, $name = '(unknown name)', $type = null, $level = 0 )
{

    if (!jDumpCheckAccess()) {
        return;
    }

    $mainframe = JFactory::getApplication(); 
    $option = JRequest::getCmd('option');
    
    require_once JPATH_ADMINISTRATOR.'/components/com_dump/node.php';

	$source = '';
	if (function_exists('debug_backtrace'))
	{
			$trace = debug_backtrace();

			$source = DumpHelper::getSourceFunction($trace);
			//$source .= '@';
			$source .= DumpHelper::getSourcePath($trace);
	}

    // create a new node array
    $node           = DumpNode::getNode( $var, $name, $type, $level, $source );    
    //get the current userstate
    $userstate      = $mainframe->getUserState( 'dump.nodes' );
    // append the node to the array
    $userstate[]    = $node;    
    // set the userstate to the new array
    $mainframe->setUserState( 'dump.nodes', $userstate );
}

/**
 * Shortcut to dump the parameters of a template
 * @param object $var The "$this" object in the template
 */
function jDumpTemplate( $var, $name = false ) {
    $name = $name ? $name :  $var->template;
    jDump( $var->params->_registry['parameter']['data'], $name );
}

/**
 * Shortcut to dump a message
 * @param string $msg The message
 */
function jDumpMessage( $msg = '(Empty message)' ) {    
    jDump( $msg, null, 'message', 0 );
}

/**
 * Shortcut to dump system information
 */
function jDumpSysinfo() {
    require_once JPATH_ADMINISTRATOR.'/components/com_dump/sysinfo.php';
    $sysinfo = new DumpSysinfo();
    jDump( $sysinfo->data, 'System Information');
}

/**
 * Shortcut to dump the backtrace
 */
function jDumpTrace()
{
	$trace = debug_backtrace();

	$arr = jDumpTraceBuild($trace);

	jDump($arr, 'Backtrace', 'backtrace');
}

function jDumpTraceBuild($trace)
{
	$ret = array();

	$ret['file']     = $trace[0]['file'];
	$ret['line']     = $trace[0]['line'];
	if (isset($trace[0]['class']) && isset($trace[0]['type']))
		$ret['function'] = $trace[0]['class'].$trace[0]['type'].$trace[0]['function'];
	else
		$ret['function'] = $trace[0]['function'];
	$ret['args']     = $trace[0]['args'];

	array_shift($trace);
	if (count($trace)>0)
	{
		$ret['backtrace'] = dumpTraceBuild($trace);
	}

	return $ret;
}

function jDumpCheckAccess() {
    $user = JFactory::getUser();
    $admin = $user->authorise('core.admin');

    if ($admin) {
        return true;
    } else {
        //Only check if they're not logged in.
        //If they're logged in, and not admin, they already don't have access.
        if ($user->id == 0) {
            return jDumpCheckCookie();
        } else {
            //If an super user is logged in as a non-super user for testing, see if they have the showjdump variable set.
            $showJDump = JRequest::getVar('jdump');
            if ($showJDump == 1) {
                return jDumpCheckCookie();
            }
            return false;
        }
    }

    return false;
}

function jDumpCheckCookie() {
    $cookie = $_COOKIE['isAdmin'];
    if (!$cookie) {
        //The cookie doesn't exist.  They were never an admin.
        return false;
    } else {
        //Decrypt the cookie and make sure it has the valid message (which is the key itself).
        $decrypt = (AxsEncryption::decrypt($cookie, AxsKeys::getKey('jDump')));
        if ($decrypt === AxsKeys::getKey('jDump')) {
            return true;
        }
    }
}

function jDumpMakeCookie() {

    $admin = JFactory::getUser()->authorise('core.admin');

    if ($admin) {
        //Since the validation message can be anything, just use the key itself.  No point in coming up with something else.
        $id = AxsEncryption::encrypt(AxsKeys::getKey('jDump'), AxsKeys::getKey('jDump'));
        setcookie('isAdmin', $id);
    }
}