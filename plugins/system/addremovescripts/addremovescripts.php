<?php
defined('_JEXEC') or die;

class plgSystemAddremovescripts extends JPlugin {
    
    public function onBeforeCompileHead() {
    	
	      $addScripts = $this->params->get('add');
	      $removeScripts = $this->params->get('remove');
	      $app = JFactory::getApplication();
	      $doc = JFactory::getDocument();
	      $admin = $app->isAdmin();
	      $front = $app->isSite();
				$add = json_decode($this->params->get('addscripts'));
				$remove = json_decode($this->params->get('removescripts'));

				$addcount = (!empty($add) && is_countable($add->script)) ? count($add->script) : 0;
				$removecount = (!empty($remove) && is_countable($remove->script)) ? count($remove->script) : 0;
				
				for($i = 0; $i<$addcount; $i++)
				{	
						if(strpos($add->script[$i], '.css')) 
						{
							$type = 'addStyleSheet';
						} 
						else 
						{
							$type = 'addScript'; 
						}


					if (($admin) && ($add->side[$i] == 'admin'))
					{	
						$doc->{$type}($add->script[$i]);
					
					}

					elseif (($front) && ($add->side[$i] == 'front'))
					{
						$doc->{$type}($add->script[$i]);
					}

					else
					{
						$doc->{$type}($add->script[$i]);
					}
					
					
				}

				for($i = 0; $i<$removecount; $i++)
				{
					if(strpos($remove->script[$i], '.css')) 
						{
							$type = '_styleSheets';
						} 
						else 
						{
							$type = '_scripts'; 
						}

					if (($admin) && ($remove->side[$i] == 'admin'))
					{
						
						unset($doc->{$type}[$remove->script[$i]]);
					}

					elseif (($front) && ($remove->side[$i] == 'front'))
					{
						
						unset($doc->{$type}[$remove->script[$i]]);
					}

					else
					{
						
						unset($doc->{$type}[$remove->script[$i]]);
					}
					
				}

	}
}