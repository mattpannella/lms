<?php
/**
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later.
 */

defined('_JEXEC') or die;

//load axs libraries
JLoader::discover('Axs', JPATH_LIBRARIES . '/axslibs', false, true);
/**
 * Mylib plugin class.
 *
 * @package     Joomla.plugin
 * @subpackage  System.axslibs
 */
class plgSystemAxslibs extends JPlugin {

    function onBeforeCompileHead() {
        JHtml::_('behavior.keepalive');
        return;
    }

    /**
     * Method to register custom library.
     *
     * return  void
     */
    public function onAfterInitialise() {

        //Load all of the Libraries

        //grab the dispatcher
        $dispatcher = JEventDispatcher::getInstance();
        $brand = AxsBrands::getBrand();

        AxsPayment::init($dispatcher, $brand);
        AxsExtra::init($dispatcher, $brand);
        //AxsAi::init($dispatcher);

        //make sure all axs plugins are loaded and ready to dispatch events to.
        JPluginHelper::importPlugin('axs');
        if (!isset($_COOKIE['int_usr'])) {
            $userId = JFactory::getUser()->id;
            $encrypt = AxsEncryption::encrypt($userId, 'interuser5');
            setcookie('int_usr', $encrypt,0,'/');
        }

        $app = JFactory::getApplication();
        if($app->isAdmin()) {
            if (!isset($_COOKIE['int_ad_usr'])) {
                $userId = JFactory::getUser()->id;
                $encrypt = AxsEncryption::encrypt($userId, 'interuser5');
                setcookie('int_ad_usr', $encrypt,0,'/');
            }
        }   
        
    }

    public function onUserAfterLogin() {
        $userId = JFactory::getUser()->id;
        $encrypt = AxsEncryption::encrypt($userId, 'interuser5');
        setcookie('int_usr', $encrypt,0,'/');
        $app = JFactory::getApplication();
        if($app->isAdmin()) {
            $userId = JFactory::getUser()->id;
            $encrypt = AxsEncryption::encrypt($userId, 'interuser5');
            setcookie('int_ad_usr', $encrypt,0,'/');            
        } 
    }

    public function onUserLogout() {
        if (isset($_COOKIE['int_usr'])) {
            unset($_COOKIE['int_usr']);
            setcookie('int_usr', '', time() - 3600, '/');
        }
        if (isset($_COOKIE['int_ad_usr'])) {
            unset($_COOKIE['int_ad_usr']);
            setcookie('int_ad_usr', '', time() - 3600, '/');
        }
    }

    public function onBeforeRender() {

        $document = JFactory::getDocument();
        //remove the existing favicon.ico
        if (isset($document->_links)) {
            foreach ($document->_links as $k => $array) {
                if (!empty($array['attribs']['type']) && $array['attribs']['type'] == 'image/vnd.microsoft.icon') {
                    unset($document->_links[$k]);
                }
            }
        }

        $app = JFactory::getApplication();
        $option = $app->input->get('option');
        $task = $app->input->get('task');
        $view = $app->input->get('view');
        $isFrontend = $app->isSite();
        if ($option == 'com_community' && $view == 'profile' && $isFrontend) {
            JHtml::_('jquery.framework');
            JFactory::getDocument()->addScriptVersion('/plugins/system/axsusers/useredit.js');
        }
    }
   
}
