<?php
/**
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.plugin.plugin' );

class plgSystemPlug_loginbyemail extends JPlugin
{
	/**
	 */
	function __construct(& $subject, $config) {
		parent::__construct($subject, $config);
	}

	function onAfterRoute(){
		$this->unconvert();
	}

	public function onUserAuthenticate($credentials, $options, &$response){
		$this->unconvert();
	}
	
	function unconvert(){
		$option	= strtolower(JRequest::getVar('option', '', 'request'));
		if ($option != 'com_user' && $option != 'com_users') return;
		$task 	= strtolower(JRequest::getVar('task', '', 'request'));
		if ($task != 'login' && $task != 'user.login') return;

		$ar = array('username');

		foreach ($ar as $item){
			$username = JRequest::getVar($item, '', 'post');
			if ( $username != '' && strpos($username,'@')!==false && strpos($username,'.')!==false ) {
				$aun = $this->getUsername( $username );
				$_POST[$item] = $aun;
                $_REQUEST[$item] = $aun;
			}else{
				if ($this->params->get('restrict_username', 0)==1){
					$_POST[$item] = 'i8432ij8sfdsf8dsjfds8fdsfsdfsdfds';
    	            $_REQUEST[$item] = 'i8432ij8sfdsf8dsjfds8fdsfsdfsdfds';
				}
			}
		}
	}

	function getUsername( $email ){
		$db	=& JFactory::getDBO();
		$query = "select username from #__users where email = '$email' ";

		$db->setQuery( $query );
		return $db->loadResult();
	}

}