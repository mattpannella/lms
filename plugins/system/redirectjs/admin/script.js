window.addEvent("domready",function(){
	// fix for the accordion
	document.getElementById('basic-options').addEvent('click', function(){
		if(document.getElementById('basic-options').hasClass('pane-toggler')) {
			(function(){ $$('.pane-slider').setStyle('height', 'auto'); }).delay(750);
		}
	});
	//

	// switchers
	$$('.onoff_switch').each(function(el){
		el.setStyle('display','none');
		var style = (el.value == 1) ? 'on' : 'off';
		var switcher = new Element('div',{'class' : 'switcher-'+style});
		switcher.inject(el, 'after');
		switcher.addEvent("click", function(){
			if(el.value == 1){
				switcher.setProperty('class','switcher-off');
				el.value = 0;
			}else{
				switcher.setProperty('class','switcher-on');
				el.value = 1;
			}
		});
	});
});