<?php
/**
 * @version	$Id: redirectjs.php 21766 2011-07-08 Adonis Media Ltd $
 * @copyright	Copyright (C) 2012 LBSaud. All rights reserved.
 * @license	GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.plugin.plugin');

/**
 * Plugin class for Redirecting JomSocial after successfully registering.
 *
 * @package	Joomla.Plugin
 * @subpackage	System.redirectjs
 */
class plgSystemRedirectjs extends JPlugin
{
    /**
     * Object Constructor.
     *
     * @access	public
     * @param	object	The object to observe -- event dispatcher.
     * @param	object	The configuration object for the plugin.
     * @return	void
     * @since	1.5
     */
    function __construct(&$subject, $config)
    {
        parent::__construct($subject, $config);
        $this->db = JFactory::getDbo();
        $this->session = JFactory::getSession();
    }

    /**
     * This method handles redirection of JomSocial when the task registerSuccess has been called
     * @param	array	$app		Gets application data.
     * @param	array	$options	Array holding options (client, ...).
     *
     * @return	object	True on success
     * @since	1.5
     */
    function onAfterRoute()
    {
        $option 	= JRequest::getCmd('option');
        $view 		= JRequest::getCmd('view');
        $task 		= JRequest::getCmd('task');
        $itemid 	= JRequest::getInt('Itemid');
        $return		= $this->params->def('return', '');
        $custom 	= $this->params->def('custom', '');
        $message 	= $this->params->get('message', '');
        $msgtype 	= ($this->params->get('message')!='' ? 'message' : '');
        $remember 	= $this->params->get('remember', 0);

        // Get the application object.
        $app = JFactory::getApplication();
        $options = array();
        $options['return'] = $return;
        $options['remember'] = $remember;

        if ($app->isSite()) {

            /*
            if there is no $user variable its probably JomSocial
            so lets get the session tmpUser
            */
            if($this->session->get('tmpUser')){
                $user['username'] = $this->session->get('tmpUser')->username;
                $user['password_clear'] = $this->session->get('tmpUser')->password_clear;
                $login = $this->encryptLogin($user, $app, $options);
            }

            if ($option=='com_community' && $view=='register' && $task=='registerSucess') {
                if(!empty($return) && empty($custom)){
                    $item 		= $app->getMenu()->getItem( $return );
                    $return 	= ($item->type=='url' ? $item->link : $item->link . '&Itemid=' . $itemid);
                }
                $link = ($custom ? $custom : JRoute::_($return, false));
                $app->redirect($link, $message, $msgtype);
            }
        }
    }
    private function encryptLogin($user, $app, $options) {

        $query = 'SELECT COUNT(`id`)'
            . ' FROM `#__users`'
            . ' WHERE username=' . $this->db->Quote( $user['username'] )
            . '   AND password=' . $this->db->Quote( $user['password_clear'] )
        ;
        $this->db->setQuery( $query );
        $result = $this->db->loadObject();

        if($result) {
            JPluginHelper::importPlugin('user');
            $options['action'] = 'core.login.site';
            $result = $app->triggerEvent('onUserLogin', array((array)$user, $options));
            return true;
        }else{
            return false;
        }
    }
}
