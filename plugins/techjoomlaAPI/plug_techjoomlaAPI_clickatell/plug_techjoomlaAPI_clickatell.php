<?php
/*
	* @package LinkedIn plugin for Invitex
	* @copyright Copyright (C)2010-2011 Techjoomla, Tekdi Web Solutions . All rights reserved.
	* @license GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
	* @link http://www.techjoomla.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}
jimport( 'joomla.filesystem.folder' );
jimport('joomla.plugin.plugin');
jimport('techjoomla.jsocial.jsocial');
		jimport('techjoomla.jsocial.joomla');


$lang =  JFactory::getLanguage();
$lang->load('plug_techjoomlaAPI_clickatell', JPATH_ADMINISTRATOR);
class plgTechjoomlaAPIplug_techjoomlaAPI_clickatell extends JPlugin
{

	function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);
		$appUsername	= $this->params->get('appUsername');
		$appPassword	= $this->params->get('appPassword');
		$appkey	= $this->params->get('appkey');
		$this->callbackUrl='';
		$this->errorlogfile='clickatell_error_log.php';
		$this->user = JFactory::getUser();

		$this->db=JFactory::getDBO();
		$this->API_CONFIG=array(
		'appUsername'       => trim($appUsername),
		'appPassword'    => trim($appPassword),
		'appkey'    => trim($appkey),
		'callbackUrl'  => NULL
		);
	}

	function renderPluginHTML($config)
	{
		$plug=array();
		$plug['name']="clickatell";
		//check if keys are set
		if($this->API_CONFIG['appUsername']=='' || $this->API_CONFIG['appPassword']=='' || $this->API_CONFIG['appkey']=='' || !in_array($this->_name,$config)) #TODO add condition to check config
		{
			$plug['error_message']=true;
			return $plug;
		}

		$plug['api_used']=$this->_name;
		$plug['message_type']='sms';
		$plug['img_file_name']="clickatell.png";
		if(isset($config['client']))
		$client=$config['client'];
		else
		$client='';

		return $plug;
	}


	function plug_techjoomlaAPI_clickatellsend_message($mail,$post)
	{
		require(JPATH_SITE.DS.'components'.DS.'com_invitex'.DS.'helper.php');
		$cominvitexHelper = new cominvitexHelper();
		$invitex_settings	= $cominvitexHelper->getconfigData();

		if($post['invite_type'] > 0)
		{
			$types_res=$cominvitexHelper->types_data($post['invite_type']);
			$template	=	stripslashes($types_res->template_clickatell);
		}
		else
		{
			$template	=	stripslashes($invitex_settings->get('sms_message_body'));
		}

		$mail['msg_body']=$template;
		$message	=	$cominvitexHelper->tagreplace($mail);
		//call library for actual sending of sms
		$user= $this->params->get('appUsername');
		$password= $this->params->get('appPassword');
		$api_id= $this->params->get('appkey');
		//get the number in the format 91NUMBER i.e countrycodenumber without space or any separator
		 $to=$post['invitee_email'];
		$to = str_replace("-","",$to);
		$SocialLibraryObject = new JSocialJoomla();
		$actual_Send_message = $SocialLibraryObject->send_SMS($user,$password,$api_id,$message,$to);
		return $actual_Send_message;
	}
}//end class
