<?php
/*
	* @package Hotmail plugin for Invitex
	* @copyright Copyright (C)2010-2011 Techjoomla, Tekdi Web Solutions . All rights reserved.
	* @license GNU GPLv2 <http://www.gnu.org/licenses/old-licenses/gpl-2.0.html>
	* @link http://www.techjoomla.com
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
if(!defined('DS')){
define('DS',DIRECTORY_SEPARATOR);
}
//mhash deprecated in php 5.3 so use hash in lib
if( !function_exists('mhash') AND function_exists('hash') ){function mhash( $hash, $data, $key='' ) {return hash( "sha256", $data, true );   }}



$lang =  JFactory::getLanguage();
$lang->load('plug_techjoomlaAPI_hotmail', JPATH_ADMINISTRATOR);

class plgTechjoomlaAPIplug_techjoomlaAPI_hotmail extends JPlugin
{
	function __construct(& $subject, $config)
	{

		parent::__construct($subject, $config);
		$appKey	= $this->params->get('appKey');
		$appSecret	= $this->params->get('appSecret');
		$this->db=JFactory::getDBO();
		$this->callbackUrl=JURI::root().'techjoomla_hotmail_api.php';
		$this->errorlogfile='hotmail_error_log.php';
		$this->user = JFactory::getUser();

		$this->API_CONFIG=array(
		'appKey'       => trim($appKey),
		'appSecret'    => trim($appSecret),
		'callbackUrl'  => $this->callbackUrl
		);


	}

	/*
		 * Get the plugin output as a separate html form
     *
     * @return  string  The html form for this plugin
     * NOTE: all hidden inputs returned are very important
	*/
  function renderPluginHTML($config)
   {
   	$plug=array();
   	$plug['name']="Hotmail";
   //check if keys are set
		if($this->API_CONFIG['appKey']=='' || $this->API_CONFIG['appSecret']=='' || !in_array($this->_name,$config))
		{
			$plug['error_message']=true;
			return $plug;
		}
   //Check if plugin has been configured.. Then only display the image. Else show message:This plugin is not configured propelry. Please contact the site Administrator .

     $plug['name']="Hotmail";
     $plug['api_used']=$this->_name;
     $plug['message_type']='email';
     $plug['img_file_name']="hotmail.png";
     if(isset($config['client']))
		$client=$config['client'];
		else
		$client='';
		$plug['apistatus'] ='';
     return $plug;
   }

	function get_request_token($callback)
	{

			try{
				$url="https://login.live.com/oauth20_authorize.srf?client_id=".$this->API_CONFIG['appKey']."&scope=wl.basic+wl.contacts_emails&response_type=code&redirect_uri=".urlencode($this->callbackUrl);
				$response=header('Location:'.$url);

			}
			catch(Exception $e)
			{
				$this->raiseException($e->getMessage());
				return false;
			}


			$return=$this->raiseLog($response,JText::_('LOG_GET_REQUEST_TOKEN'),$this->user->id,0);

			return true;

	}

	function get_access_token($get,$client,$callback)
	{
		$header=	"Content-type: application/x-www-form-urlencoded";
		$accessTokenUrl = 'https://login.live.com/oauth20_token.srf';
		$postvals = "client_id=".$this->API_CONFIG['appKey']
			."&client_secret=".$this->API_CONFIG['appSecret']
			."&grant_type=authorization_code"
			."&redirect_uri=".urlencode($this->callbackUrl)
			."&code=".$get['code'];
		$redata	=	json_decode($this->curl_request($accessTokenUrl,'POST',$postvals,$header));

		$hotmail_access_token =	$redata->access_token;
		if(!$hotmail_access_token){

			$this->raiseException("Error in getting access token");
			$return=$this->raiseLog($_REQUEST,JText::_('LOG_GET_ACCESS_TOKEN'),$this->user->id,0);
			return;
		}
		//keep access token in seesion added by ANIKET
		$session = JFactory::getSession();
		$com_invitex_access_token_hotmail=$session->set('com_invitex_access_token_hotmail',$hotmail_access_token);

		//$this->store($client,$response_data);
		return true;

   }

   public function curl_request($url,$method,$postvals,$header){

		$ch = curl_init($url);
		if ($method == "POST"){
		   $options = array(
	                CURLOPT_POST => 1,
	                CURLOPT_POSTFIELDS => $postvals,
	                CURLOPT_RETURNTRANSFER => 1,
			);

		}else{

		   $options = array(
	                CURLOPT_RETURNTRANSFER => 1,
			);

		}
		curl_setopt_array( $ch, $options );
		if($header){

			curl_setopt( $ch, CURLOPT_HTTPHEADER, array( $header . $postvals) );
		}

		$response = curl_exec($ch);
		curl_close($ch);
	#	print_r($response);
		return $response;
	}

	function plug_techjoomlaAPI_hotmailget_contacts()
	{
		$session = JFactory::getSession();
		$hotmail_access_token=$session->get('com_invitex_access_token_hotmail');

		//access token url to get contacts for hotmail
		$userProfileUrl = "https://apis.live.net/v5.0/me/contacts?access_token=".$hotmail_access_token;
		//we get all contacts data in json format
		try{

			$mydata =	$this->curl_request($userProfileUrl,"GET",$hotmail_access_token,'');
		}
		catch(Exception $e)
		{
			$error_msg=$e->getMessage();
			JFactory::getApplication()->enqueueMessage('HOTMAIL-APP-Get Contacts-Error:'.$error_msg, 'warning');

		}
		$mydata=json_decode($mydata);

		if(empty($mydata))
		{
			$no_contact_msg=JText::_('NO_CONTACTS');
			JFactory::getApplication()->enqueueMessage($no_contact_msg, 'warning');
			return array();

		}

		$emails=array();
		$cnt=0;
			foreach ($mydata->data as $contact)
			{
					if ($contact->emails->preferred)
					{
						 $emails[$cnt]['id']= $contact->emails->preferred;
					}
					if ($contact->name)
					{
							$emails[$cnt]['name'] = $contact->name;
					}

				$cnt++;
			}


			$contacts=$this->renderContacts($emails);

		if(count($contacts)==0)
		{
			$no_contact_msg=JText::_('NO_CONTACTS');
			JFactory::getApplication()->enqueueMessage($no_contact_msg, 'warning');
			$this->raiseException(JText::_('NO_CONTACTS'));
		}
		if($contacts)
		return $contacts;
		else
		return array();
	}

	function renderContacts($emails)
	{
			$count=0;
			$r_connections=array();
			foreach($emails as $connection)
			{
				if($connection['id'])
				{
					$r_connections[$count]=new stdClass();
				$r_connections[$count]->id  =$connection['id'];
				$first_name ='';
				$last_name ='';
				if(array_key_exists('first-name',$connection))
					$first_name =$connection['first-name'];
				if(array_key_exists('last-name',$connection))
					$last_name  =$connection['last-name'];
				if(array_key_exists('first-name',$connection) or array_key_exists('last-name',$connection))
				$r_connections[$count]->name=$first_name.' '.$last_name;

				if(trim($first_name)=='' and trim($last_name)=='')
				{
					if(array_key_exists('name',$connection))
					{
						$r_connections[$count]->name=$connection['name'];
					}
				}
				if(array_key_exists('picture-url',$connection))
				{
							$r_connections[$count]->picture_url=$connection['picture-url'];
				}
				else
				{
							$r_connections[$count]->picture_url='';
				}

				$count++;
			}
			}

		return $r_connections;
	}

	function plug_techjoomlaAPI_hotmailget_profile()
	{

  }
	function plug_techjoomlaAPI_hotmailsend_message($post)
	{

	}

	function plug_techjoomlaAPI_hotmailgetstatus()
	{

	}
	function plug_techjoomlaAPI_hotmailsetstatus($userid,$originalContent,$comment,$attachment='')
	{
	}

	function raiseException($exception,$userid='',$display=1,$params=array())
	{
		$path="";
		$params['name']=$this->_name;
		$params['group']=$this->_type;
		$loghelperobj=	new techjoomlaHelperLogs();
		$loghelperobj->simpleLog($exception,$userid,'plugin',$this->errorlogfile,$path,$display,$params);
		return;
	}

	function raiseLog($status_log,$desc="",$userid="",$display="")
	{

		$params=array();
		$params['desc']	=	$desc;
		if(is_object($status))
		$status=JArrayHelper::fromObject($status_log,true);

		if(is_array($status))
		{
			if(isset($status['info']['http_code']))
			{
				$params['http_code']		=	$status['info']['http_code'];
				if(!$status['success'])
				{
							if(isset($status['hotmail']))
							$response_error=techjoomlaHelperLogs::xml2array($status['hotmail']);


					$params['success']			=	false;
					$this->raiseException($response_error['error']['message'],$userid,$display,$params);
					return false;

				}
				else
				{
					$params['success']	=	true;
					$this->raiseException(JText::_('LOG_SUCCESS'),$userid,$display,$params);
					return true;

				}

			}
		}
		$this->raiseException(JText::_('LOG_SUCCESS'),$userid,$display,$params);
		return true;
	}

}//end class
