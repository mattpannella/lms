<?php

/**

 * @version			$Id: appointments.php 179 2014-07-25 05:55:43Z sanawin $

 * @subpackage	JomSocial

 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com

 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

 * 

 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.

 * - The system Can make an appointment within the people group.

 * - The system Can make personal appointments.

 * - The system Calendar Have Monthly View, Year View and Weekly View

 * - Add People to list of appointments none limited.

 * - The appointment can change the status to Accept, Not Accept and Not Sure

 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure

 */

// no direct access

defined('_JEXEC') or die('Restricted access');



if(!defined('DS')){

	define('DS',DIRECTORY_SEPARATOR);

}



JFactory::getLanguage()->load( 'com_community_appointment', JPATH_SITE );



class CommunityAppointmentsController extends CommunityBaseController {



	protected $_disabledMessage = '';



	/**

	 * Constructer

	 */

	public function __construct()

	{

		$this->_disabledMessage = JText::_('JSAPP_EVENTS_DISABLED');

	}





	/**

	 * Displays the default events view

	 **/

	public function month($cacheable=false, $urlparams=false)

	{

		$my 	= CFactory::getUser();



		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$jinput = JFactory::getApplication()->input;

		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('month');



		echo $view->get(__FUNCTION__);

	}





	/**

	 * Method show only current user appointment

	*/

	public function display()

	{

		$jinput = JFactory::getApplication()->input;

		$my 	= CFactory::getUser();



		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('default');



		echo $view->get(__FUNCTION__);

	}



	/**

	 * Method show appointments weeks

	*/

	public function week()

	{

		$jinput = JFactory::getApplication()->input;

		$my 	= CFactory::getUser();



		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('week');



		echo $view->get(__FUNCTION__);

	}



	/**

	 * Method show appointments day

	*/

	public function day()

	{

		$jinput = JFactory::getApplication()->input;

		$my 	= CFactory::getUser();



		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('day');



		echo $view->get(__FUNCTION__);

	}



	/**

	 * Method to create / edit an appointment

	 */

	 public function edit()

	{

		$jinput = JFactory::getApplication()->input;

		$my 	= CFactory::getUser();

		

		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$id 	= $jinput->get('id', 0 , 'Int');



		if($id)

		{

			$row = $this->loadItem($id);

			 if (empty($row->id)) {

            	return JError::raiseWarning(404, JText::_('JSAPP_APPOINTMENT_NOT_FOUND_ERROR'));

            }



            if($my->get('id') != $row->author)

            {

            	return JError::raiseWarning(404, JText::_('JSAPP_APPOINTMENT_NOT_FOUND_ERROR'));

            }

        }



		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('edit');



		echo $view->get(__FUNCTION__);

	 }



	 public function viewappointment()

	 {

	 	$jinput = JFactory::getApplication()->input;

		$my 	= CFactory::getUser();



		if ($my->id == 0) {

			return $this->blockUnregister();

		}



		$document = JFactory::getDocument();

		$viewType = $document->getType();

		$viewName = $jinput->get('view', $this->getName(),'String');

		$view = $this->getView($viewName, '', $viewType);



		$view->setLayout('viewappointment');



		echo $view->get(__FUNCTION__);

	 }



	 /**

	  * Method to save appointments

	  */

	 public function save()

	 {

	 	// Check for request forgeries

		JSession::checkToken('post') or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));



	 	//Initialise variables

	 	$mainframe  = JFactory::getApplication();

		$jinput		= $mainframe->input;

		$document	= JFactory::getDocument();

		$user		= CFactory::getUser();

		$db			= JFactory::getDbo();

		$filter		= JFilterInput::getInstance();

		$viewType	= $document->getType();

		$viewName	= $jinput->get('view', $this->getName(),'String');

		$view		= $this->getView($viewName, '', $viewType);



		if (JString::strtoupper($jinput->getMethod()) != 'POST')

		{

			$view->addWarning(JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING'));

			return false;

		}



		// Get vars

		$title			= $jinput->get('title', '', 'STRING');

		// Despite the bind, we would still need to capture RAW description

		$text 			= JRequest::getVar('text', '', 'post', 'string', JREQUEST_ALLOWRAW);

		$text 			= JComponentHelper::filterText($text);

		$act_status		= $jinput->get('act_status');

		$sdate 			= $jinput->get('sdate');

		$s_hour 		= $jinput->get('s_hour');

		$s_minute 		= $jinput->get('s_minute');

		$edate 			= $jinput->get('edate');

		$e_hour 		= $jinput->get('e_hour');

		$e_minute 		= $jinput->get('e_minute');

		$receivers		= $jinput->get('receivers', array(), 'ARRAY');

		$receivers 		= array_unique($receivers);

		$receivers 		= array_diff($receivers, array('0'));

		$id 			= $jinput->get('id');

		$Itemid 		= $jinput->get('Itemid');

		$wanttosendmail = $jinput->get('wanttosendmail', 0);



		// Set vars

		$sdate		= explode("-", $sdate);

		$sdate		= mktime($s_hour, $s_minute, 0, $sdate[1], $sdate[2], $sdate[0]);

		$edate		= explode("-", $edate);

		$edate		= mktime($e_hour, $e_minute, 0, $edate[1], $edate[2], $edate[0]);

		$appid		= $id;

		$vars		= array();

		$validated	= true;



	 	//If has already, an appointment.

	 	if($appid) 

	 	{

            $rcvs			= $receivers;

            $receivers_     = "'".implode("','", $receivers)."'";



            // appointment before edited

            $app_old	= $this->loadItem($appid);



            // update appointments

            $query = $db->getQuery(true);

            $query = " UPDATE #__community_appointment SET title=".$db->quote($title).", content=".$db->quote($text).", sdate='{$sdate}',edate='{$edate}' WHERE id='{$id}';";

            $db->setQuery($query);

            $db->query();



            // update receivers.

            $query = $db->getQuery(true);

            $query = "DELETE FROM #__community_appointment_users WHERE appid='{$id}' AND userid NOT IN ({$receivers_});";

            $db->setQuery($query);

            $db->query();



            // Select receiver user for send email

            $query = $db->getQuery(true);

            $query = "SELECT userid FROM #__community_appointment_users WHERE appid='{$id}';";

            $db->setQuery($query);

            $userids = $db->loadColumn();



            //Computes the difference of receivers

            $diff_receivers = array_diff($receivers, $userids);



            if($wanttosendmail) {

                $vars['to'] = $receivers;

            }

            else {

                $vars['to'] = $diff_receivers;

            }



            $receivers 	    = $diff_receivers;



            // check for difference

            $isRename	= $this->compareTitle($app_old->title, $title);

            $isEdit		= $this->compareInfo($app_old->content, $text) || $this->compareInfo($app_old->sdate, $sdate) || $this->compareInfo($app_old->edate, $edate);



            //Parameter for notification

            $params = new CParameter('');

            $params->set('actor', $user->getDisplayName());

            $params->set('actor_url', 'index.php?option=com_community&view=profile&userid=' . $user->id); // Link for the {actor} tag

            $params->set('appointment', $title);

            $params->set('appointment_url', 'index.php?option=com_community&view=appointments&task=viewappointment&id='.$appid.'&Itemid='.$Itemid);



            // send notify unless information is same to receivers

            if($isRename || $isEdit)

            {

                foreach($rcvs as $r)

                {

                    // skip if select "None"

                    if((int)$r==0) continue;



                    //Notification for receivers about renamed appointment

                    if($isRename)

                    {

                        CNotificationLibrary::add( 'system_messaging' , $user->id , $r , JText::sprintf('JSAPP_NOTIFY_RENAME',$app_old->title) , JText::sprintf('JSAPP_NOTIFY_EMAIL_RENAME', $app_old->title) , '' , $params );

                    }



                    //Notification for receivers about edited appointment

                    if($isEdit)

                    {

                        CNotificationLibrary::add( 'system_messaging' , $user->id , $r , JText::sprintf('JSAPP_NOTIFY_EDIT') , JText::sprintf('JSAPP_NOTIFY_EMAIL_EDIT') , '' , $params );

                    }

                }

            }

	 	}

	 	else

	 	{

	 		//Set vars

            $vars['to'] = $receivers;

			$time = time();



			$query = $db->getQuery(true);

			$query = "INSERT INTO #__community_appointment VALUES(NULL, ".$db->quote($title).", ".$db->quote($text).",'{$user->id}','{$time}','{$sdate}','{$edate}');";

			$db->setQuery($query);

			$db->query();



			//Get current ID

			$appid   = $db->insertid();



            $content = JText::sprintf('JSAPP_MSG_BODY_MAIL', $text);



            // Content plugins

            $dispatcher	    = JDispatcher::getInstance();

            JPluginHelper::importPlugin('content');

            $row 			= new stdClass();

            $row->text 		= $content;



            $params 		= class_exists('JParameter') ? new JParameter(null) : new JRegistry(null);

            $dispatcher->trigger('onContentPrepare', array ('com_community.appointments', &$row, &$params, 0));



            $content 		= $row->text;



            //Change video to image

            $content 		= $this->video($content);



            preg_match_all("/(src|background)=[\"'](.*)[\"']/Ui", $content, $images);

            if (isset($images[2])) {

                foreach ($images[2] as $i => $url) {

                    // do not change urls for absolute images (thanks to corvuscorax)

                    if (!preg_match('#^[A-z]+://#', $url)) {

                        $filename = basename($url);

                        $directory = dirname($url);

                        if ($directory == '.') {

                            $directory = '';

                        }

                        $cid = md5($url).'@phpmailer.0'; //RFC2392 S 2

                        if (strlen($basedir) > 1 && substr($basedir, -1) != '/') {

                            $basedir .= '/';

                        }

                        if (strlen($directory) > 1 && substr($directory, -1) != '/') {

                            $directory .= '/';

                        }

                        if ($mailer->AddEmbeddedImage($basedir.$directory.$filename, $cid, $filename, 'base64', self::_mime_types(self::mb_pathinfo($filename, PATHINFO_EXTENSION)))) {

                            $content = preg_replace("/".$images[1][$i]."=[\"']".preg_quote($url, '/')."[\"']/Ui", $images[1][$i]."=\"cid:".$cid."\"", $content);

                        }

                    }

                }

            }



            //Add notification

            $params = new CParameter( '' );

            $params->set('url' , 'index.php?option=com_community&view=appointments&task=viewappointment&id='.$appid);

            $params->set('message' , $content );

            $params->set('title'	, $title );



            $params->set('msg', $title);

            $params->set('msg_url', 'index.php?option=com_community&view=appointments&task=viewappointment&id='.$appid);



            $users  = is_array($vars['to'])?$vars['to']:array();



            foreach($users AS $recepientId)

            {

                CNotificationLibrary::add( 'inbox_create_message' , $user->id , $recepientId , JText::sprintf('JSAPP_MSG_SUBJECT') , '' , 'inbox.sent' , $params );

            }

	 	}



	 	// Insert receivers to database

	 	foreach($receivers as $r) 

	 	{

			if((int)$r==0) continue;

			$query = "INSERT INTO #__community_appointment_users VALUES('{$appid}', '{$r}', '0', '');";

			$db->setQuery($query);

			$db->query();

		}



		// Set message success

		$msg = JText::_('JSAPP_ITEM_SAVED');

		$mainframe->enqueueMessage($msg);



		// Set URL vars

		$redirect = CRoute::_("index.php?option=com_community&view=appointments&task=viewappointment&id=".$appid, false);



		$mainframe->redirect($redirect);

	 }



    /**

     * Method to change state of appointment

     */

    public function changeState()

    {

        // Check for request forgeries

        JSession::checkToken('post') or jexit(JText::_('COM_COMMUNITY_INVALID_TOKEN'));



        //Initialise variables

        $mainframe  = JFactory::getApplication();

        $jinput		= $mainframe->input;

        $db			= JFactory::getDbo();



        if (JString::strtoupper($jinput->getMethod()) != 'POST')

        {

            $view->addWarning(JText::_('COM_COMMUNITY_PERMISSION_DENIED_WARNING'));

            return false;

        }



        //Get vars

        $id 			= $jinput->get('id', 0);

        $status			= $jinput->get('status', 0);

        $Itemid 		= $jinput->get('Itemid');

        $reason			= $jinput->get('reason', '', 'STRING');



        //Get data

        $row			= $this->loadItem($id);



        //Get actor and target

        $actor  = CFactory::getUser();

        $target = CFactory::getUser($row->author);



        //Update state of appointment for each user.

        $query 	= $db->getQuery(true);

        $query	= "UPDATE #__community_appointment_users SET accept='$status', reason=".$db->quote($reason)." WHERE appid='$id' AND userid='{$actor->id}';";

        $db->setQuery($query);

        $result = $db->query();



        // Set color status reply notification

        if((int)$status == 1) {

            $rep    = "Accept";

            $color	= "green";

        } elseif((int)$status == 2)	{

            $rep	= "Not Sure";

            $color  = "blue";

        }

        elseif((int)$status == -1)	{

            $rep	= "Not Accept";

            $color  = "red";

        }



        //Send Notification and email

        $params = new CParameter('');

        $params->set('actor', $actor->getDisplayName()); // can be used as {actor} tag

        $params->set('actor_url', 'index.php?option=com_community&view=profile&userid=' . $actor->id); // Link for the {actor} tag

        $params->set('appointment', $row->title);

        $params->set('appointment_url', 'index.php?option=com_community&view=appointments&task=viewappointment&&id='.$id.'&Itemid='.$Itemid);

        $params->set('color', $color);

        $params->set('state', $rep);



        CNotificationLibrary::add( 'system_messaging' , $actor->id , $target->id , JText::sprintf('JSAPP_NOTIFY_REPLY') , JText::sprintf('JSAPP_REPLY_EMAIL_CONTENT') , '' , $params );



        // Set message success

        $msg = JText::_('JSAPP_ITEM_SAVED');

        $mainframe->enqueueMessage($msg);



        // Set URL vars

        $redirect = CRoute::_("index.php?option=com_community&view=appointments&task=viewappointment&id=".$id."&Itemid=".$Itemid, false);



        $mainframe->redirect($redirect);

    }



    /**

	  * Method to get a appointment

	*/

	 private function loadItem($id) 

	 {

		$db		= JFactory::getDbo();

		$query 	= $db->getQuery(true);

		$query	= "SELECT * FROM `#__community_appointment` WHERE id = '$id'";



		$db->setQuery($query);

		$row	= $db->loadObject();

		

		return $row;       

	}



	/**

	 * Function for title comparison between store and input data

	 *

	 * @param $title_old

	 * @param $title_new

	 *

	 * @return boolean

	 */

	private function compareTitle($title_old, $title_new) 

	{

		$title_old	= JString::trim($title_old);

		$title_new	= JString::trim($title_new);



		// compare two title :: if 0 return false (no difference) and else return true (any difference was counted)

		if(JString::strcmp($title_old , $title_new) == 0)

			return false;

		else

			return true;

	}



	/**

	 * Method to compare info

	 */

	private function compareInfo($old, $new) 

	{

		$old	= JString::trim($old);

		$new	= JString::trim($new);



		if(JString::strcmp($old, $new) == 0)

			return false;

		else

			return true;

	}



	/**

	 * Method to get params of component community

	 *

	 * @access private

	 * @return mixed

	*/

	private function getCommunityConfig()

	{

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query = 'SELECT params FROM #__community_config WHERE name="config";';

		$db->setQuery($query);

		$config	= $db->loadObject()->params;

		if($config) {

			$config	= get_object_vars( json_decode($config) );

		}else {

			$config['event_calendar_firstday']	= '';

			$config['eventdateformat']			= '';

			$config['eventshowampm']			= 1;

		}



		return $config;

	}



	/**

	 * Gets the MIME type of the embedded or inline image

	 * @param string $ext File extension

	 * @access public

	 * @return string MIME type of ext

	 * @static

	 */

	public static function _mime_types($ext = '') {

		$mimes = array(

			'xl'    =>  'application/excel',

			'hqx'   =>  'application/mac-binhex40',

			'cpt'   =>  'application/mac-compactpro',

			'bin'   =>  'application/macbinary',

			'doc'   =>  'application/msword',

			'word'  =>  'application/msword',

			'class' =>  'application/octet-stream',

			'dll'   =>  'application/octet-stream',

			'dms'   =>  'application/octet-stream',

			'exe'   =>  'application/octet-stream',

			'lha'   =>  'application/octet-stream',

			'lzh'   =>  'application/octet-stream',

			'psd'   =>  'application/octet-stream',

			'sea'   =>  'application/octet-stream',

			'so'    =>  'application/octet-stream',

			'oda'   =>  'application/oda',

			'pdf'   =>  'application/pdf',

			'ai'    =>  'application/postscript',

			'eps'   =>  'application/postscript',

			'ps'    =>  'application/postscript',

			'smi'   =>  'application/smil',

			'smil'  =>  'application/smil',

			'mif'   =>  'application/vnd.mif',

			'xls'   =>  'application/vnd.ms-excel',

			'ppt'   =>  'application/vnd.ms-powerpoint',

			'wbxml' =>  'application/vnd.wap.wbxml',

			'wmlc'  =>  'application/vnd.wap.wmlc',

			'dcr'   =>  'application/x-director',

			'dir'   =>  'application/x-director',

			'dxr'   =>  'application/x-director',

			'dvi'   =>  'application/x-dvi',

			'gtar'  =>  'application/x-gtar',

			'php3'  =>  'application/x-httpd-php',

			'php4'  =>  'application/x-httpd-php',

			'php'   =>  'application/x-httpd-php',

			'phtml' =>  'application/x-httpd-php',

			'phps'  =>  'application/x-httpd-php-source',

			'js'    =>  'application/x-javascript',

			'swf'   =>  'application/x-shockwave-flash',

			'sit'   =>  'application/x-stuffit',

			'tar'   =>  'application/x-tar',

			'tgz'   =>  'application/x-tar',

			'xht'   =>  'application/xhtml+xml',

			'xhtml' =>  'application/xhtml+xml',

			'zip'   =>  'application/zip',

			'mid'   =>  'audio/midi',

			'midi'  =>  'audio/midi',

			'mp2'   =>  'audio/mpeg',

			'mp3'   =>  'audio/mpeg',

			'mpga'  =>  'audio/mpeg',

			'aif'   =>  'audio/x-aiff',

			'aifc'  =>  'audio/x-aiff',

			'aiff'  =>  'audio/x-aiff',

			'ram'   =>  'audio/x-pn-realaudio',

			'rm'    =>  'audio/x-pn-realaudio',

			'rpm'   =>  'audio/x-pn-realaudio-plugin',

			'ra'    =>  'audio/x-realaudio',

			'wav'   =>  'audio/x-wav',

			'bmp'   =>  'image/bmp',

			'gif'   =>  'image/gif',

			'jpeg'  =>  'image/jpeg',

			'jpe'   =>  'image/jpeg',

			'jpg'   =>  'image/jpeg',

			'png'   =>  'image/png',

			'tiff'  =>  'image/tiff',

			'tif'   =>  'image/tiff',

			'eml'   =>  'message/rfc822',

			'css'   =>  'text/css',

			'html'  =>  'text/html',

			'htm'   =>  'text/html',

			'shtml' =>  'text/html',

			'log'   =>  'text/plain',

			'text'  =>  'text/plain',

			'txt'   =>  'text/plain',

			'rtx'   =>  'text/richtext',

			'rtf'   =>  'text/rtf',

			'xml'   =>  'text/xml',

			'xsl'   =>  'text/xml',

			'mpeg'  =>  'video/mpeg',

			'mpe'   =>  'video/mpeg',

			'mpg'   =>  'video/mpeg',

			'mov'   =>  'video/quicktime',

			'qt'    =>  'video/quicktime',

			'rv'    =>  'video/vnd.rn-realvideo',

			'avi'   =>  'video/x-msvideo',

			'movie' =>  'video/x-sgi-movie'

		);

		return (!isset($mimes[strtolower($ext)])) ? 'application/octet-stream' : $mimes[strtolower($ext)];

	}



	/**

	 * Drop-in replacement for pathinfo(), but multibyte-safe, cross-platform-safe, old-version-safe.

	 * Works similarly to the one in PHP >= 5.2.0

	 * @link http://www.php.net/manual/en/function.pathinfo.php#107461

	 * @param string $path A filename or path, does not need to exist as a file

	 * @param integer|string $options Either a PATHINFO_* constant, or a string name to return only the specified piece, allows 'filename' to work on PHP < 5.2

	 * @return string|array

	 * @static

	 */

	public static function mb_pathinfo($path, $options = null) {

		$ret = array('dirname' => '', 'basename' => '', 'extension' => '', 'filename' => '');

		$m = array();

		preg_match('%^(.*?)[\\\\/]*(([^/\\\\]*?)(\.([^\.\\\\/]+?)|))[\\\\/\.]*$%im', $path, $m);

		if(array_key_exists(1, $m)) {

			$ret['dirname'] = $m[1];

		}

		if(array_key_exists(2, $m)) {

			$ret['basename'] = $m[2];

		}

		if(array_key_exists(5, $m)) {

			$ret['extension'] = $m[5];

		}

		if(array_key_exists(3, $m)) {

			$ret['filename'] = $m[3];

		}

		switch($options) {

			case PATHINFO_DIRNAME:

			case 'dirname':

				return $ret['dirname'];

				break;

			case PATHINFO_BASENAME:

			case 'basename':

				return $ret['basename'];

				break;

			case PATHINFO_EXTENSION:

			case 'extension':

				return $ret['extension'];

				break;

			case PATHINFO_FILENAME:

			case 'filename':

				return $ret['filename'];

				break;

			default:

				return $ret;

		}

	}



	/**

	* Method to get video image

	*/

	public function video($item){

		if(preg_match_all('@<iframe\s[^>]*src=[\"|\']([^\"\'\>]+)[^>].*?</iframe>@ms', $item, $iframesrc) > 0){

			

			if(isset($iframesrc[1])){

				foreach ($iframesrc[1] as $i => $url) {

					if(strpos($url, 'vimeo.com') !== false ) {

						$vid = str_replace(

							array(

								'http:',

								'https:', 

								'//player.vimeo.com/video/'

							), '', $url);

					} else {

						$vid = str_replace(

							array(

								'http:',

								'https:',

								'//youtu.be/',

								'//www.youtube.com/embed/',

								'//youtube.googleapis.com/v/'

							), '', $url);

					}



					//remove any parameter

					$vid = preg_replace('@(\/|\?).*@i', '', $vid);

					

					if(!(empty($vid))){ 

						if(strpos($url, 'vimeo.com') !== false ) {

							require_once (JPATH_ADMINISTRATOR . '/components/com_joomlaupdate/helpers/download.php');

							$filepath = JPATH_SITE . '/cache/vimeo/' . $vid . '.json';



							if(!is_file($filepath)){							

								AdmintoolsHelperDownload::download("http://vimeo.com/api/v2/video/$vid.json", $filepath);

							}



							if(is_file($filepath)){

								$vimeojson 	= json_decode(@file_get_contents($filepath));

								$link 		= 'http://vimeo.com/'.$vid;

								$src 		= $vimeojson[0]->thumbnail_large;

							}

						} else {

							$link = 'http://www.youtube.com/watch?v='.$vid;

							$src  = 'http://img.youtube.com/vi/'.$vid.'/0.jpg';

						}

						

						$item = str_replace($iframesrc['0'][$i], '<a href="'.$link.'" target="_bank"><img src="'.htmlspecialchars($src).'"/></a>', $item);

					}

				} //End foreach

			}

		}



		return $item;

	}

}