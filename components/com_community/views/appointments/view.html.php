<?php
/**
 * @version			$Id: view.html.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

if(!defined('DS')){
	define('DS',DIRECTORY_SEPARATOR);
}

jimport( 'joomla.application.component.view');

class CommunityViewAppointments extends CommunityView {

	/**
	 * Method to display appointments  view month
	 * @access public
	 */
	 public function month($tpl=null)
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

		//Parse version of communuty
		$cmtversion = $this->loadCMTVersion();

		 //Initialise variables
		 $document		= JFactory::getDocument();
		 $jinput			= JFactory::getApplication()->input;

		//Get vars
		$Itemid			= (int) $jinput->get('Itemid');
		$year				= (int) $jinput->get('year', date("Y"));
		$month          = (int) $jinput->get('month', date("n"));
		$today			= (int) $jinput->get('day', date("j"));

		//Add style
		$document->addStyleSheet(JURI::root(true).'/components/com_community/views/appointments/assets/css/appointments.css');

		//Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}

		$document->addScriptDeclaration
			('
				function stopReloadKey(evt) {
					var evt = (evt) ? evt : ((event) ? event : null);
					var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
					if (evt.keyCode == 13)  {
						return false;
					}
				}

				document.onkeypress = stopReloadKey;
			');
		$document->addScript(JURI::base().'components/com_community/views/appointments/assets/js/jquery-1.9.1.min.js');
		
		//Page title
		$this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), CRoute::_('index.php?option=com_community&view=appointments&task=myappointments'));
		$this->addPathway(JText::_('JSAPP_VIEW'), '');

		//Set title
		$title = JText::_('JSAPP_VIEW');
		$document->setTitle($title);
	
		 // Display the submenu
        $this->showSubmenu();

		//Get config of component community
		$config = $this->getCommunityConfig();

		$this->startDay			= (($config['event_calendar_firstday'] == 'Sunday') ? 'sun' : 'mon');
		$this->dateFormat		= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M j' : 'j M');
		$this->evtdateFormat	= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M d, Y' : 'd M Y');
		$this->timeFormat		= (($config['eventshowampm'] == 1) ? 12 : 24);

		$real_today					= date("j");
		$real_year					= date('Y');
		$real_month				= date('n');
		$days_of_month			= date("t", mktime(0,0,0,$month,1,$year));
		$month_start				= date('w',mktime(0, 0, 0, $month, 1, $year));
		$first_day					= date("w",mktime(0,0,0,$month,1,$year)) - 1;
		$days							= date('t', mktime(0, 0, 0, $month, 1, $year));

		$counter						= 0;
		$current_day				= 1;
		$ctoday						= 0;

		//Get data
		$rows = $this->loadMonth($year, $month, $days_of_month);
		
		if($month_start == 0) {
			$month_start = 6;
		} else {
			$month_start--;
		}
		
		//Assign data to layout
		$this->assignRef('rows', $rows);
		$this->assignRef('year', $year);
		$this->assignRef('month', $month);
		$this->assignRef('today', $today);
		$this->assignRef('real_today', $real_today);
		$this->assignRef('real_year', $real_year);
		$this->assignRef('real_month', $real_month);
		$this->assignRef('days_of_month', $days_of_month);
		$this->assignRef('month_start', $month_start);
		$this->assignRef('first_day', $first_day);
		$this->assignRef('days', $days);
		$this->assignRef('counter', $counter);
		$this->assignRef('current_day', $current_day);
		$this->assignRef('ctoday', $ctoday);
		$this->assignRef('config', $config);
		$this->assignRef('cmtversion', $cmtversion);

		parent::display($tpl);
	 }

	 /**
     * Method to display appointments that belongs to a user.
     *
     * @access public
     */
    public function display($tpl = null) 
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

		//Parse version of communuty
		$cmtversion = $this->loadCMTVersion();
		
		 //Initialise variables
		 $mainframe = JFactory::getApplication();
		 $document 	= JFactory::getDocument();

		 $jinput 	= $mainframe->input;

		 //Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}
		 
		 //Page title
		 $this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), '');

		 //Set title
		 $title = JText::_('JSAPP_MYAPPOINTMENTS');
		 $document->setTitle($title);

		  // Display the submenu
        $this->showSubmenu();

        // Get the pagination request variables
		$limit		= $mainframe->getUserStateFromRequest( 'global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int' );
		$limitstart	= $jinput->get('limitstart', 0, 'int' );

		// In case limit has been changed, adjust limitstart accordingly
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

        //Get data
        $rows 	= $this->getMyAppointments($limitstart, $limit);

        // Get pagination
        jimport('joomla.html.pagination');
		$pagination = new JPagination( $this->getTotalMyAppointments(), $limitstart, $limit );

        // Sort Items
        $sortItems =  array(
							'coming' 		=> JText::_('JSAPP_COMING') ,
 							'past'		=> JText::_('JSAPP_PAST')
						);

        $sortingHTML	= CFilterBar::getHTML( CRoute::getURI(), $sortItems, 'coming');

        //Assign data to layout
        $this->assignRef('appointments', $rows);
        $this->assignRef('sortings'	, $sortingHTML);
        $this->assignRef('pagination', $pagination);

		parent::display($tpl);
	}

	public function week($tpl=null)
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

		//Parse version of communuty
		$cmtversion = $this->loadCMTVersion();

		 //Initialise variables
		 $document		= JFactory::getDocument();
		 $jinput		= JFactory::getApplication()->input;

		//Get vars
		$Itemid			= (int) $jinput->get('Itemid');
		$year			= (int) $jinput->get('year', date("Y"));
		$month          = (int) $jinput->get('month', date("n"));
		$today			= (int) $jinput->get('day', date("j"));

		//Add style
		$document->addStyleSheet(JURI::root(true).'/components/com_community/views/appointments/assets/css/appointments.css');

		//Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}

		//Page title
		$this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), CRoute::_('index.php?option=com_community&view=appointments&task=myappointments'));
		$this->addPathway(JText::_('JSAPP_VIEW_WEEK'), '');

		//Set title
		$title = JText::_('JSAPP_VIEW_WEEK');
		$document->setTitle($title);
	
		 // Display the submenu
        $this->showSubmenu();

		//Get config of component community
		$config = $this->getCommunityConfig();

		$this->startDay			= (($config['event_calendar_firstday'] == 'Sunday') ? 'sun' : 'mon');
		$this->dateFormat		= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M j' : 'j M');
		$this->evtdateFormat	= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M d, Y' : 'd M Y');
		$this->timeFormat		= (($config['eventshowampm'] == 1) ? 12 : 24);

		//Set vars
		$week						= date("W", mktime(0,0,0,$month,$today,$year));
		$day_of_week				= strftime( '%w', mktime(0,0,0,$month,$today,$year));
		$day_of_week				= (($this->startDay == 'sun') ? $day_of_week : $day_of_week -1);
		$start_of_week			= date("d", mktime(0,0,0,$month,$today - $day_of_week,$year));
		$start_of_week_y			= date("Y", mktime(0,0,0,$month,$today - $day_of_week,$year));
		$start_of_week_m		= date("m", mktime(0,0,0,$month,$today - $day_of_week,$year));
		$end_of_week				= date("d", mktime(0,0,0,$month,$today + (6 - $day_of_week),$year));
		$end_of_week_y			= date("Y", mktime(0,0,0,$month,$today + (6 - $day_of_week),$year));
		$end_of_week_m			= date("m", mktime(0,0,0,$month,$today + (6 - $day_of_week),$year));

		//Get data
		$rows	 = $this->loadWeek($start_of_week_y, $start_of_week_m, $start_of_week, $end_of_week_y, $end_of_week_m, $end_of_week);

		if($this->startDay == 'mon') 
		{
			$day_names = array('0' => JText::_('Monday'), '1' => JText::_('Tuesday'), '2' => JText::_('Wednesday'),
									 '3' => JText::_('Thursday'), '4' => JText::_('Friday'), '5' => JText::_('Saturday'),
									 '6' => JText::_('Sunday'));
		}
		elseif($this->startDay == 'sun') 
		{
			$day_names = array('0' => JText::_('Sunday'),		'1' => JText::_('Monday'),		'2' => JText::_('Tuesday'),
									 '3' => JText::_('Wednesday'),	'4' => JText::_('Thursday'),	'5' => JText::_('Friday'),
									 '6' => JText::_('Saturday'));
		}

		$hoursList[24] = array(	'00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
		$hoursList[12] = array('12<br/>'.JText::_('JSAPP_AM'),'1<br/>'.JText::_('JSAPP_AM'),'2<br/>'.JText::_('JSAPP_AM'),'3<br/>'.JText::_('JSAPP_AM'),'4<br/>'.JText::_('JSAPP_AM'),'5<br/>'.JText::_('JSAPP_AM'),'6<br/>'.JText::_('JSAPP_AM'),'7<br/>'.JText::_('JSAPP_AM'),'8<br/>'.JText::_('JSAPP_AM'),'9<br/>'.JText::_('JSAPP_AM'),'10<br/>'.JText::_('JSAPP_AM'),'11<br/>'.JText::_('JSAPP_AM'),
			'12<br/>'.JText::_('JSAPP_PM'),'1<br/>'.JText::_('JSAPP_PM'),'2<br/>'.JText::_('JSAPP_PM'),'3<br/>'.JText::_('JSAPP_PM'),'4<br/>'.JText::_('JSAPP_PM'),'5<br/>'.JText::_('JSAPP_PM'),'6<br/>'.JText::_('JSAPP_PM'),'7<br/>'.JText::_('JSAPP_PM'),'8<br/>'.JText::_('JSAPP_PM'),'9<br/>'.JText::_('JSAPP_PM'),'10<br/>'.JText::_('JSAPP_PM'),'11<br/>'.JText::_('JSAPP_PM'));

		$hours	= $hoursList[$this->timeFormat];

		//Assign data to layout
		$this->assignRef('year', $year);
		$this->assignRef('month', $month);
		$this->assignRef('today', $today);
		$this->assignRef('week', $week);
		$this->assignRef('day_of_week', $day_of_week);
		$this->assignRef('start_of_week', $start_of_week);
		$this->assignRef('start_of_week_y', $start_of_week_y);
		$this->assignRef('start_of_week_m', $start_of_week_m);
		$this->assignRef('end_of_week', $end_of_week);
		$this->assignRef('end_of_week_y', $end_of_week_y);
		$this->assignRef('end_of_week_m', $end_of_week_m);
		$this->assignRef('rows', $rows);
		$this->assignRef('day_names', $day_names);
		$this->assignRef('hours', $hours);
		$this->assignRef('cmtversion', $cmtversion);
		
		parent::display($tpl);
	}

	public function day($tpl=null)
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

		$cmtversion = $this->loadCMTVersion();
		
		 //Initialise variables
		 $document		= JFactory::getDocument();
		 $jinput		= JFactory::getApplication()->input;

		 //Get vars
		$Itemid			= (int) $jinput->get('Itemid');
		$year			= (int) $jinput->get('year', date("Y"));
		$month          = (int) $jinput->get('month', date("n"));
		$today			= (int) $jinput->get('day', date("j"));
		$week			= date("W", mktime(0,0,0,$month,$today,$year));

		//Add style
		$document->addStyleSheet(JURI::root(true).'/components/com_community/views/appointments/assets/css/appointments.css');

		 //Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}

		//Page title
		$this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), CRoute::_('index.php?option=com_community&view=appointments&task=myappointments'));
		$this->addPathway(JText::_('JSAPP_VIEW_DAY'), '');

		//Set title
		$title = JText::_('JSAPP_VIEW_DAY');
		$document->setTitle($title);
	
		 // Display the submenu
        $this->showSubmenu();

		//Get config of component community
		$config = $this->getCommunityConfig();

		$this->startDay			= (($config['event_calendar_firstday'] == 'Sunday') ? 'sun' : 'mon');
		$this->dateFormat		= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M j' : 'j M');
		$this->evtdateFormat	= ((substr($config['eventdateformat'], 0, 1) == 'M') ? 'M d, Y' : 'd M Y');
		$this->timeFormat		= (($config['eventshowampm'] == 1) ? 12 : 24);

		//Get data
		$rows	 = $this->loadDay($year, $month, $today);

		$hoursList[24] = array(	'00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
		$hoursList[12] = array('12<br/>'.JText::_('JSAPP_AM'),'1<br/>'.JText::_('JSAPP_AM'),'2<br/>'.JText::_('JSAPP_AM'),'3<br/>'.JText::_('JSAPP_AM'),'4<br/>'.JText::_('JSAPP_AM'),'5<br/>'.JText::_('JSAPP_AM'),'6<br/>'.JText::_('JSAPP_AM'),'7<br/>'.JText::_('JSAPP_AM'),'8<br/>'.JText::_('JSAPP_AM'),'9<br/>'.JText::_('JSAPP_AM'),'10<br/>'.JText::_('JSAPP_AM'),'11<br/>'.JText::_('JSAPP_AM'),
			'12<br/>'.JText::_('JSAPP_PM'),'1<br/>'.JText::_('JSAPP_PM'),'2<br/>'.JText::_('JSAPP_PM'),'3<br/>'.JText::_('JSAPP_PM'),'4<br/>'.JText::_('JSAPP_PM'),'5<br/>'.JText::_('JSAPP_PM'),'6<br/>'.JText::_('JSAPP_PM'),'7<br/>'.JText::_('JSAPP_PM'),'8<br/>'.JText::_('JSAPP_PM'),'9<br/>'.JText::_('JSAPP_PM'),'10<br/>'.JText::_('JSAPP_PM'),'11<br/>'.JText::_('JSAPP_PM'));

		$hours			= $hoursList[$this->timeFormat];

		//Assign data to layout
		$this->assignRef('year', $year);
		$this->assignRef('month', $month);
		$this->assignRef('today', $today);
		$this->assignRef('week', $week);
		$this->assignRef('rows', $rows);
		$this->assignRef('hours', $hours);
		$this->assignRef('cmtversion', $cmtversion);

		parent::display($tpl);
	}

	public function edit($tpl=null)
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

        //Parse version of communuty
		$cmtversion = $this->loadCMTVersion();

		//Initialise variables
		$document		= JFactory::getDocument();
		$config 		= CFactory::getConfig();
		$jinput			= JFactory::getApplication()->input;

		$editorType 	= ($config->get('allowhtml') ) ? $config->get('htmleditor', 'none') : 'none';

        $editor 		= new CEditor($editorType);

		//Add style
		$document->addStyleSheet(JURI::root(true).'/components/com_community/views/appointments/assets/css/appointments.css');

		//Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}

		 $js = 'assets/validate-1.5.min.js';
        CAssets::attach($js, 'js');

		//Get vars
		$year			= $jinput->get('year', date("Y"));
		$month			= str_pad($jinput->get('month', date("m")), 2, "0", STR_PAD_LEFT);
		$today			= str_pad($jinput->get('day', date("d")), 2, "0", STR_PAD_LEFT);
		$hour			= str_pad($jinput->get('hour', date('H')), 2, "0", STR_PAD_LEFT);
		$hasTime		= (($jinput->get('hour') >= 0) ? true : false);
		$id				= $jinput->get('id', 0, 'INT');

		//Get data
		$row	 	= $this->loadItem($id);
		$appusers 	= $this->loadAppUsers($id);
		$users 		= $this->getAllFriends();

		if($row) {
			$s_hour		= @$row->sdate ? date('H', $row->sdate) : $hour;
			$s_min		= @$row->sdate ? date('i', $row->sdate) : '00';
			$e_hour		= @$row->edate ? date('H', $row->edate) : $hour;
			$e_min		= @$row->edate ? date('i', $row->edate) : '00';

			$start_date	= @$row->sdate ? date('Y-m-d', @$row->sdate) : date('Y-m-d', mktime(0, 0, 0, $month, $today, $year));
			$end_date	= @$row->edate ? date('Y-m-d', @$row->edate) : date('Y-m-d', mktime(0, 0, 0, $month, $today, $year));
		}
		else {
			$s_hour		= !$hasTime ? date('H') : $hour;
			$s_min		= '00';
			$e_hour		= !$hasTime ? date('H', strtotime('+1 hour')) : $hour + 1;
			$e_min		= '00';

			$start_date	= date('Y-m-d', mktime(0, 0, 0, $month, $today, $year));
			$end_date	= date('Y-m-d', mktime($s_hour+1, 0, 0, $month, $today, $year));
		}

		$options 	= array();
		$options[] 	= JHTML::_('select.option',  0, JText::_( 'JSAPP_NONE' ) );
		foreach($users as $u) { 
			$options[] = JHTML::_('select.option',  $u->id, $u->name ); 
		}
		$lists['select_user'] = JHTML::_('select.genericlist', $options, 'receivers[]', 'size="1"'.'', 'value', 'text', 0 );
		
		$lists['selected_users'] = array();
		foreach($appusers as $u) {
			$lists['selected_users'][] = JHTML::_('select.genericlist', $options, 'receivers[]', 'size="1"'.'', 'value', 'text', $u->userid );
		}

		$options_h = array();
		for($h=0;$h<24;$h++) {
			$h_str = str_pad($h, 2, '0', STR_PAD_LEFT);
			$options_h[] = JHTML::_('select.option', $h_str, $h_str );
		}
		$lists['s_hour'] = JHTML::_('select.genericlist', $options_h, 's_hour', 'class="input-mini" size="1"'.'', 'value', 'text', $s_hour );
		$lists['e_hour'] = JHTML::_('select.genericlist', $options_h, 'e_hour', 'class="input-mini" size="1"'.'', 'value', 'text', $e_hour );

		$options_m = array();
		for($m=0;$m<60;$m=$m+5) {
			$m_str = str_pad($m, 2, '0', STR_PAD_LEFT);
			$options_m[] = JHTML::_('select.option', $m_str, $m_str );
		}
		$lists['s_minute'] = JHTML::_('select.genericlist', $options_m, 's_minute', 'class="input-mini" size="1"'.'', 'value', 'text', $s_min );
		$lists['e_minute'] = JHTML::_('select.genericlist', $options_m, 'e_minute', 'class="input-mini" size="1"'.'', 'value', 'text', $e_min );

		if($row) {
			$title = JText::_('JSAPP_EDITING_APPOINTMENT');
		} else {
			$title = JText::_('JSAPP_CREATE_NEW_APPOINTMENT');
		}

		//Set path way
		$this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), CRoute::_('index.php?option=com_community&view=appointments&task=myappointments'));
		$this->addPathway(JText::_($title), '');

		//Set title
		$document->setTitle($title);

		// Display the submenu
        $this->showSubmenu();

		//Assign data to layout
		$this->assignRef('editor', $editor);
		$this->assignRef('row', $row);
		$this->assignRef('id', $id);
		$this->assignRef('year', $year);
		$this->assignRef('month', $month);
		$this->assignRef('today', $today);
		$this->assignRef('hour', $hour);
		$this->assignRef('config', $config);
		$this->assignRef('editor', $editor);
		$this->assignRef('row', $row);
		$this->assignRef('users', $users);
		$this->assignRef('start_date', $start_date);
		$this->assignRef('end_date', $end_date);
		$this->assignRef('s_hour', $s_hour);
		$this->assignRef('s_min', $s_min);
		$this->assignRef('e_hour', $e_hour);
		$this->assignRef('e_min', $e_min);
		$this->assignRef('editor', $editor);
		$this->assignRef('lists', $lists);
		$this->assignRef('cmtversion', $cmtversion);

		parent::display($tpl);
	}

	public function viewappointment($tpl=null)
	{
		if (!$this->accessAllowed('registered')) {
            return;
        }

		$cmtversion = $this->loadCMTVersion();

		//Initialise variables
		$dispatcher	= JDispatcher::getInstance();
		$document	= JFactory::getDocument();
		$editor		= JFactory::getEditor();
		$jinput		= JFactory::getApplication()->input;

		//Add style
		$document->addStyleSheet(JURI::root(true).'/components/com_community/views/appointments/assets/css/appointments.css');

		//Add script
		if(version_compare($cmtversion,'2.8.4.2','>')) 
		{
			$document->addScriptDeclaration('
						document.onready = function() {
							var e = document.getElementsByClassName("dropdown");
							var el = e.length;
							e[el-1].className = " dropdown active";

							var actives = document.getElementsByClassName("active");
							if(actives.length > 0 && actives[0].className != " dropdown active") {
								actives[0].className = actives[0].className.replace("active", "");
							}
						}
					');
		} 
		else 
		{
			$document->addScriptDeclaration('
					document.onready = function() {
						var e = document.getElementsByClassName("cMenu-Text cHas-Childs");
						var el = e.length;
						e[el-1].getElementsByTagName("a").item(0).className = "active";
					}
				');
		}

		//Get vars
		$id = $jinput->get('id', 0, 'INT');

		//Get data
		$row 		= $this->loadItem($id);
		$users 		= $this->loadAppUsers($id);

		// Set vars
		$start_date = date('Y-m-d', $row->sdate);
		$end_date   = date('Y-m-d', $row->edate);
		$s_hour     = date('H', $row->sdate);
		$s_min      = date('i', $row->sdate);
		$e_hour     = date('H', $row->edate);
		$e_min      = date('i', $row->edate);

		//Set path way
		$this->addPathway(JText::_('JSAPP_MYAPPOINTMENTS'), CRoute::_('index.php?option=com_community&view=appointments&task=myappointments'));
		$this->addPathway(JText::_($row->title), '');

		//Set title
		$document->setTitle($row->title);

		//Suppport plugin allVideos
		$row->text = $row->content;

		// Content plugins
		JPluginHelper::importPlugin('content');
		$params = class_exists('JParameter') ? new JParameter(null) : new JRegistry(null);
		$dispatcher->trigger('onContentPrepare', array ('com_community.appointments', &$row, &$params, 0));

		$row->content = $row->text;

		//Set data
		$this->row = $row;

		// Display the submenu
        $this->showSubmenu();

		//Assign data to layout
		$this->assignRef('row', $row);
		$this->assignRef('users', $users);
		$this->assignRef('start_date', $start_date);
		$this->assignRef('end_date', $end_date);
		$this->assignRef('s_hour', $s_hour);
		$this->assignRef('s_min', $s_min);
		$this->assignRef('e_hour', $e_hour);
		$this->assignRef('e_min', $e_min);
        
		parent::display($tpl);
	}

	public function showSubmenu() 
	{
        $this->_addSubmenu();
        parent::showSubmenu();
    }

	public function _addSubmenu()
	{
		//Initialise variables
		$my		= CFactory::getUser();
		$db 	= JFactory::getDbo();
		$jinput	= JFactory::getApplication()->input;
		$year	= (int) $jinput->get('year', date("Y"));
		$month  = (int) $jinput->get('month', date("n"));
		$today	= (int) $jinput->get('day', date("j"));
		
		$author	= isset($this->row)?CFactory::getUser($this->row->author)->get('id'):0;
	
		$this->addSubmenuItem('index.php?option=com_community&view=appointments&task=myappointments', JText::_('JSAPP_MYAPPOINTMENTS'));
		$this->addSubmenuItem('index.php?option=com_community&view=appointments&task=month', JText::_('JSAPP_VIEW'));
		$this->addSubmenuItem('index.php?option=com_community&view=appointments&task=week', JText::_('JSAPP_VIEW_WEEK'));
		$this->addSubmenuItem('index.php?option=com_community&view=appointments&task=day', JText::_('JSAPP_VIEW_DAY'));

		//Set add link
		$link_add 	= 'index.php?option=com_community&view=appointments&task=edit&year='.$year.'&month='.$month.'&day='.$today;
		
		if($my->get('id') == $author)
		{
			//Set edit link
			$link_edit	= 'index.php?option=com_community&view=appointments&task=edit&id='.$this->row->id;

			$this->addSubmenuItem('javascript:void(0);' , JText::_('JSAPP_DELETE'), 'javascript:joms.appointments.deleteAppointment('.$db->quote($this->row->id).')', SUBMENU_RIGHT);
			$this->addSubmenuItem($link_edit , JText::_('JSAPP_EDIT'), '', SUBMENU_RIGHT);
		}

		$this->addSubmenuItem($link_add , JText::_('JSAPP_NEW'), '', SUBMENU_RIGHT);
	}

	/**
	 * Method to get params of component community
	 *
	 * @access private
	 * @return mixed
	*/
	private function getCommunityConfig()
	{
		$db = JFactory::getDbo();
		$db->getQuery(true);
		$db->setQuery('SELECT params FROM #__community_config WHERE name="config";');
		$config	= $db->loadObject()->params;
		if($config) {
			$config	= get_object_vars( json_decode($config) );
		}else {
			$config['event_calendar_firstday']	= '';
			$config['eventdateformat']			= '';
			$config['eventshowampm']			= 1;
		}

		return $config;
	}

	/**
	 * Method to get month
	 *
	 *@access private
	 *@return mixed
	 */
	 private function loadMonth($year, $month, $days_of_month)
	{
		//Initialise variables
		$user    	= CFactory::getUser();
		$db 		= JFactory::getDbo();

		//Set vars
		$start_date	= mktime(0,0,0,$month,1,$year);
		$end_date	= mktime(23,59,0,$month,$days_of_month,$year);
		$day			= 1;
		$rows			= array();
		$events		= array();
		$tasks		= array();
		$milestones	= array();
		$projects	= array();

		$query = $db->getQuery(true);
		// load events
		$query = "SELECT app.* FROM #__community_appointment as app "
			." LEFT JOIN #__community_appointment_users as u on u.appid=app.id "
		   //." WHERE app.sdate BETWEEN '$start_date' AND '$end_date'"
		   //. "\n AND app.project IN($workspace)"
		   ." WHERE (app.author=".(int) $user->get('id')
		   ." OR u.userid=".(int) $user->get('id').") "
		   ." GROUP BY app.id ORDER BY app.sdate ";
		$db->setQuery($query);
		$events = $db->loadObjectList();
		
		if(!is_array($events))  $events = array(); 

		while ($day <= $days_of_month) 
		{
			// set date
			$start_date					= mktime(0,0,0,$month,$day,$year);
			$end_date					= mktime(23,59,59,$month,$day,$year);
			$rows[$day]				= array();
			$rows[$day]['events']	= array();

			// add events
			foreach ($events AS $r) {
				if( ($r->sdate >= $start_date && $r->sdate <= $end_date) || 
					($r->edate >= $start_date && $r->edate <= $end_date) || 
					($start_date >= $r->sdate && $end_date <= $r->edate) ) 
				{
					$rows[$day]['events'][] = $r;
				}
			}

			$day++;
		}

		return $rows;
	 }

	/**
	 * Method to get week
	 *
	 *@access private
	 *@return mixed
	 */
	 private function loadWeek($start_of_week_y, $start_of_week_m, $start_of_week, $end_of_week_y, $end_of_week_m, $end_of_week)
	{
		//Initialise variables
		$db			= JFactory::getDbo();
		$user		= CFactory::getUser();

		//Set var
		$start_date = mktime(0,0,0,$start_of_week_m, $start_of_week, $start_of_week_y);
		$end_date  	= mktime(23,59,0,$end_of_week_m, $end_of_week, $end_of_week_y);
		$tmp_date	= $start_date;
		$rows		= array();
		$day		= 1;
		$hour		= 0;
		
		$query		= $db->getQuery(true);
		$query		= "SELECT app.* FROM #__community_appointment as app LEFT JOIN #__community_appointment_users as u on u.appid=app.id"
							." WHERE (app.sdate BETWEEN '$start_date' AND '$end_date' OR app.edate BETWEEN '$start_date' AND '$end_date' OR '$start_date' >= app.sdate AND '$end_date' <= app.edate)"
							."\n AND (app.author='".$user->get('id')."'"
							."\n OR u.userid='".$user->get('id')."') "
							. "\n GROUP BY app.id ORDER BY app.sdate";
		$db->setQuery($query);
		$events = $db->loadObjectList();

		if(!is_array($events)) $events = array();
		
		while($day <= 7)  
		{
			$rows[$day] = array();

			if($day != 1) {
				$tmp_date		= $tmp_date + 86400;
				$start_date		= $tmp_date;
				$end_date		= $start_date;
			}

			while($hour < 24) {

				$rows[$day][$hour]					= array();
				$rows[$day][$hour]['events']     = array();

				if($hour != 0) {
					$start_date = $start_date + 3600;
				}
				$end_date = $start_date + 3599;

				foreach ($events AS $r) {
					if( ($r->sdate >= $start_date && $r->sdate <= $end_date) || ($r->edate >= $start_date && $r->edate <= $end_date) || ($start_date >= $r->sdate & $end_date <= $r->edate) ) {
						if( ($r->sdate >= $start_date) && ($r->sdate <= $end_date) )
							$r->showcheckbox=true;
						else
							$r->showcheckbox=false;
						$rows[$day][$hour]['events'][] = $r;
					}
				}
				$hour++;
			}
		    $day++;
		    $hour = 0;
		}

		return $rows;
	 }

	 /**
	  * Method to day
	  *
	  *@access private
	  *@return mixed
	  */
	 private function loadDay($year, $month, $day) 
	{
		//Initialise variables
		$user	= CFactory::getUser();
		$db		= JFactory::getDBO();

		//Set var
		$start_date = mktime(0,0,0,$month,$day,$year);
		$end_date	= mktime(23,59,59,$month,$day,$year);
		$hour			= 0;
		$rows			= array();

		$query = $db->getQuery(true);
		$query = "SELECT app.* FROM #__community_appointment as app LEFT JOIN #__community_appointment_users as u on u.appid=app.id "
				." WHERE (app.sdate BETWEEN '$start_date' AND '$end_date' OR app.edate BETWEEN '$start_date' AND '$end_date' OR '$start_date' >= app.sdate AND '$end_date' <= app.edate) "
				."\n AND (app.author='".$user->get('id')."'"
				."\n OR u.userid='".$user->get('id')."') "
				. "\n GROUP BY app.id ORDER BY app.sdate";

		$db->setQuery($query);
		$events = $db->loadObjectList();

		if(!is_array($events)) $events = array(); 

		while($hour < 24) 
		{
			$rows[$hour]               = array();
			$rows[$hour]['events']     = array();

			if($hour != 0) {
				$start_date = $start_date + 3600;
			}

		    $end_date = $start_date + 3599;

		    foreach ($events AS $r) {
			if( ($r->sdate >= $start_date && $r->sdate <= $end_date) || ($r->edate >= $start_date && $r->edate <= $end_date) || ($start_date >= $r->sdate && $end_date <= $r->edate) ) {
		    		$rows[$hour]['events'][] = $r;
		    	}
		    }

		    $hour++;
		}

		return $rows;
	}

	/**
	 * Method to translate date
	 *
	 * @access public
	 * @return string
	 */
	 public function transDate($date, $format) 
	{
		$date	= explode(' ', $date);
		if($format == 'j M') {
			$date[1]	= JText::_('JSAPP_'.strtoupper($date[1]));
		}
		elseif($format == 'M j') {
			$date[0]	= JText::_('JSAPP_'.strtoupper($date[0]));
		}

		return implode(' ', $date);
	}

	/**
	 * Method to build month list
	 * 
	 * @access public
	 * @return array
	 */
	public function buildmonthlist()
	{
		$options_months = array();
		$options_months[] = JHTML::_('select.option',  1, JText::_('January') );
		$options_months[] = JHTML::_('select.option',  2, JText::_('February') );
		$options_months[] = JHTML::_('select.option',  3, JText::_('March') );
		$options_months[] = JHTML::_('select.option',  4, JText::_('April') );
		$options_months[] = JHTML::_('select.option',  5, JText::_('May') );
		$options_months[] = JHTML::_('select.option',  6, JText::_('June') );
		$options_months[] = JHTML::_('select.option',  7, JText::_('July') );
		$options_months[] = JHTML::_('select.option',  8, JText::_('August') );
		$options_months[] = JHTML::_('select.option',  9, JText::_('September') );
		$options_months[] = JHTML::_('select.option',  10, JText::_('October') );
		$options_months[] = JHTML::_('select.option',  11, JText::_('November') );
		$options_months[] = JHTML::_('select.option',  12, JText::_('December') );
		
		return $options_months;
	}

	/**
	 * Method to build year list
	 * 
	 * @access public
	 * @return array
	 */
	public function buildyearlist()
	{
		$year_option	= array();
		$year=date('Y', strtotime('-2 years'));
		$year_max=date('Y', strtotime('+2 years'));
		for(; $year<=$year_max; $year++) {
			$year_option[]	= JHtml::_('select.option', $year, $year);
		}

		return $year_option;
	}

	/**
	 * Method to get item of appointment
	 *
	 *@access public
	 *@return object
	 */
	public function loadItem($id)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = "SELECT * FROM #__community_appointment WHERE id = ".(int) $id;
	    $db->setQuery($query);
	    $row = $db->loadObject();

	    if(!is_object($row))
	    {
	    	$row = new stdClass();
	    	$row->id 		= 0;
	    	$row->title 	= null;
	    	$row->content 	= null;
	    	$row->author 	= null;
	    	$row->cdate 	= null;
	    	$row->sdate 	= null;
	    	$row->edate 	= null;
	    }

		return $row;
	}

	/**
	 * Method to get appointment for user
	 *
	 * @access public
	 * @return mixed
	*/
	public function loadAppUsers($appid, $all=false) 
	{
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);

		if($all)
			$query = "SELECT * FROM #__community_appointment_users as au LEFT JOIN #__users as u ON  u.id=au.userid WHERE au.appid='$appid';";
		else
			$query = "SELECT * FROM #__community_appointment_users WHERE appid='$appid';";
		$db->setQuery($query);
		$users = $db->loadObjectList();

		if(!is_array($users)) $users = array();

		return $users;
	}


	/**
	 * Method to get total status of appointment for user
	 *
	 * @access public
	 * @return int
	*/
	public function loadAppStatusUsers($appid, $state) 
	{
		$db 	= JFactory::getDbo();
		$query 	= $db->getQuery(true);

		$query = " SELECT COUNT(*) FROM #__community_appointment_users as au "
				." LEFT JOIN #__users as u ON  u.id=au.userid "
				." WHERE au.appid=".(int) $appid
				." AND accept=".$db->quote($state)
				;
		$db->setQuery($query);
		$users = $db->loadResult();
		//echo $users."<br />";
		return $users;
	}

	/**
	 * Method to get all friends
	 *
	 * @access public
	 * @return mixed
	*/
	public function getAllFriends()
	{
		static $friends;

		if(!$friends) 
		{
			//Initialise variables
			$db 	= JFactory::getDBO();
			$user 	= CFactory::getUser();

			//set var
			$user_ids 	= array();
			$query 		= $db->getDbo();
			$query 		= "SELECT connect_from,connect_to FROM #__community_connection WHERE (connect_from='{$user->id}' OR connect_to='{$user->id}') AND status='1';";
			$db->setQuery($query);
			$users 		= $db->loadObjectList();

			foreach($users as $u) {
				$user_ids[] = $u->connect_from;
				$user_ids[] = $u->connect_to;
			}

			//set user ID
			$user_ids = "'".implode("','", $user_ids)."'";

			$query = $db->getQuery(true);
			$query = "SELECT u.id,u.name FROM #__users as u WHERE u.id IN ({$user_ids}) AND u.block='0' AND u.id!='{$user->id}' ORDER BY u.name ASC;";
			$db->setQuery($query);
			$friends = $db->loadObjectList();
		}


		return $friends;
	}

	/**
	 * Method to load component community version
	 *
	 * @return string
	 */
	private function loadCMTVersion()
	{
		$cmtversion = '';

		 //Parse version of communuty
		$filePath = JPath::clean(JPATH_ROOT.'/administrator/components/com_community/manifest.xml');
		if (is_file($filePath))
		{
			$xml = @JInstaller::parseXMLInstallFile($filePath);
			$cmtversion = $xml['version'];
		}

		return $cmtversion;	
	}

	/**
	 * Method to get my appointments
	 */
	private function getMyAppointments($limitstart, $limit)
	{
		//Initialise variables
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);

		//get query for myappointment
		$query 		= $this->_buildQueryMyAppointment();
		
		$db->setQuery($query, $limitstart, $limit);
		$rows = $db->loadObjectList();

		return $rows;
	}

	private function getTotalMyAppointments()
	{
		//Initialise variables
		$db 		= JFactory::getDbo();
		$query 		= $db->getQuery(true);

		//get query for myappointment
		$query 		= $this->_buildQueryMyAppointment();

		$query = clone $query;
		$query->clear('select')->clear('order')->clear('group')->select('COUNT( DISTINCT(a.id))');

		$db->setQuery($query);
		$total = (int) $db->loadResult();

		return $total;
	}

	/**
	 * Method to build query for my appointments
	 */
	private function _buildQueryMyAppointment()
	{
		//Initialise variables
		$mainframe	= JFactory::getApplication();
		$jinput		= $mainframe->input;
		$db 		= JFactory::getDbo();
		$my 		= CFactory::getUser();
		$date		= JFactory::getDate();

		// get User ID
		$id 		= $my->get('id');

		// get UNIX Current date
		$now 		= $date->toSql();

		$date 		= explode(' ', $now);

        //Get vars
        $sorted		= $jinput->get->get('sort' , 'coming', 'STRING');

		//Query selection
		$query = $db->getQuery(true);
		$query->select('a.*');
		$query->from('#__community_appointment AS a');

		//Join
		$query->join('LEFT', '#__community_appointment_users AS u ON u.appid=a.id');

		//Conditions
		$query->where('( (a.author='.(int) $id.') OR (u.userid='.(int) $id.'))');

		//Check get date for coming appointments
		if($sorted == 'coming') {
			$query->where('(a.sdate >='.$db->quote(strtotime($date[0])) .' OR a.edate>='.$db->quote(strtotime($date[0])).')');
			$query->order('a.sdate ASC');
		} else {
			$query->where('a.edate <'.$db->quote(strtotime($date[0])));
			$query->order('a.sdate DESC');
		}

		$query->group('a.id');
		
		return $query;
	}
}