<?php
/**
 * @version			$Id: month.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
$Itemid = JFactory::getApplication()->input->get('Itemid');
?>
<form action="<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=month');?>" method="post" name="adminForm" id="adminForm">
	<a id="pf_top"></a>
	<div class="pf_container">
		<div class="pf_body">
			<div class="cSubmenu cResetList cFloatedList clearfix">
				<span>
					<label for="day"><strong><?php echo JText::_('JSAPP_GO_TO_DATE'); ?> </strong></label>
					<input type="text" id="day" class="input input-mini" size="3" value="<?php echo $this->today;?>" name="day" />
				</span>
				<span>
					<select name="month" class="input input-medium" id="month">
						<?php echo JHtml::_('select.options', $this->buildmonthlist(), 'value', 'text', $this->month); ?>
					</select>
				</span>
				<span>
					<select name="year" class="input input-small" id="year">
						<?php echo JHtml::_('select.options', $this->buildyearlist(), 'value', 'text', $this->year); ?>
					</select>
				</span>
				<span <?php if(version_compare($this->cmtversion,'2.8.4.2','>')): ?>style="margin-bottom: 10px;"<?php endif; ?> onclick="submitbutton('month');" class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn btn-primary':'cButton cButton-Black'; ?>"><?php echo JText::_('JSAPP_VIEWS');?></span>
				<span style="float: right">
					<?php
						$monthNext = CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=month&day=1&month=%d&year=%d&startDay=%s', date('m', mktime(0,0,0,$this->month+1, $this->today, $this->year)), date('Y', mktime(0,0,0,$this->month+1, $this->today, $this->year)), $this->startDay));
						$monthPrev	= CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=month&day=1&month=%d&year=%d&startDay=%s', date('m', mktime(0,0,0,$this->month-1, $this->today, $this->year)), date('Y', mktime(0,0,0,$this->month-1, $this->today, $this->year)), $this->startDay));
					?>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $monthPrev; ?>', '_parent')">◄</span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=month'); ?>', '_parent')"><?php echo JText::_('JSAPP_TODAY');?></span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $monthNext; ?>', '_parent')">►</span>
				</span>
			</div>
			<!-- end resetlist -->
			<table class="pf_table" width="100%" cellpadding="1" cellspacing="1">
				<thead>
				<tr>
					<?php //TODO First date switcher (Sun/Mon) ?>
					<?php if($this->startDay == 'sun'): ?>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Sunday');?></th>
					<?php endif ?>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Monday');?></th>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Tuesday');?></th>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Wednesday');?></th>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Thursday');?></th>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Friday');?></th>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Saturday');?></th>
					<?php if($this->startDay == 'mon'): ?>
					<th align="left" width="14%" class="sectiontableheader title"><?php echo JText::_('Sunday');?></th>
					<?php endif; ?>
				</tr>
				</thead>
				<tbody>
				<?php
				
				$firstStartDay = (($this->startDay == 'mon') ? $this->month_start : $this->month_start + 1);

				for ($i = 0; $i < $this->days + $firstStartDay; $i++):

					$we         = '';
					$events    = $this->rows[$this->current_day]['events'];

					if ($this->counter == 0) { echo "\n <tr class='row0 sectiontableentry1'>"; }
					if ($this->counter >= 5) { $we = '_we'; }

					if ($i < $firstStartDay) 
					{
						if($firstStartDay == 7) {
							$i += $firstStartDay-1;
							continue;
						}
						echo "\n <td class=\"pf_monthstart\">&nbsp;</td>";
					}
					else 
					{
						//Set URL link
						$link_add			= CRoute::_('index.php?option=com_community&view=appointments&task=edit&year='.$this->year.'&month='.$this->month.'&day='.$this->current_day);
						$event_link_view	= 'index.php?option=com_community&view=appointments&task=viewappointment&id=';
				?>
					<td align='left' valign='top' class="day<?php echo $we;?> <?php if($this->real_today == $this->current_day & $this->real_month == $this->month & $this->real_year == $this->year) { echo "today"; }?>">
						<table id="tb_daywrap" width="100%" ondblclick="window.open('<?php echo $link_add; ?>', '_parent');">
							<tr>
								<td valign="top" width="20">
									<div class="pf_daywrap">
										<span class="pf_daynumber"><?php echo $this->current_day;?></span>
											<div class="pf_addevent" onclick="window.open('<?php echo $link_add ?>', '_parent')"></div>
									</div>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<?php
									$start_date		= mktime(0,0,0,$this->month,$this->current_day,$this->year);
									$end_date		= mktime(23,59,59,$this->month,$this->current_day,$this->year);
									foreach ($events AS $x => $event) 
									{
										$user_from = CFactory::getUser($event->author);	
									?>
										<div class="event">
											<?php $eventTitle = htmlspecialchars($event->title); ?>
											<a href="<?php echo CRoute::_($event_link_view.$event->id);?>"
											   class="jomTips"
											   title="
											   <?php // Information displayed when hover ?>
											   <strong><?php echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle;?></strong>::
											   <?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
											   <?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $event->sdate), $this->dateFormat); ?><br>
											   <?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $event->edate), $this->dateFormat); ?><br>
											   <?php echo JText::_('JSAPP_FROM').': '.htmlspecialchars($user_from->getDisplayName());?><br><br>
											   <?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
											   "
											>
												<?php echo (JString::strlen($eventTitle) > 17) ? (JString::substr($eventTitle , 0 , 16).'..') : $eventTitle; ?></a>
										</div>
									<?php
									} // End foreach
									?>
								</td>
							</tr>
						</table>
					</td>
					<?php
						$this->current_day++;
					} //End Else

					if ($this->counter == 6) 
					{
						$this->counter = -1;
						echo "\n </tr>";
					}
					$this->counter++;
				endfor; //End for
				?>
				</tr>
				</tbody>
			</table>
		</div>
		<!-- end body -->
	</div>
	<!-- end container -->
	<input type="hidden" name="option" value="com_community" />
	<input type="hidden" name="view" value="appointments" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
</form>
