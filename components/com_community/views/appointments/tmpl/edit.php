<?php
/**
 * @version			$Id: edit.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::addIncludePath(JPATH_SITE . '/components/com_jsappointment/helpers/html');
JHtml::_('behavior.keepalive');
?>
<div class="cLayout cAppointments-Create">
	<form action="<?php echo CRoute::getURI();?>" method="post" name="adminForm" class="cForm community-form-validate">

		<ul class="cFormList cFormHorizontal cResetList">
			<li>
				<label for="title" class="form-label">
					<?php echo JText::_('JSAPP_TITLE');?> <?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'<span class="required-sign"> *</span>':'*'; ?>
				</label>

				<div class="form-field">
					<input type="text" maxlength="56" size="40" value="<?php echo @$this->row->title;?>" name="title" id="title" class="required input text title jomNameTips" title="<?php echo JText::_('JSAPP_APPOINTMENT_TITLE_TIPS'); ?>" />
					<?php  if(version_compare($this->cmtversion,'2.8.4.2','>')): ?><span id="errtitlemsg" style="display:none;">&nbsp;</span><?php endif; ?>
				</div>
			</li>

			<li>
				<label for="text" class="form-label">
					<?php echo JText::_('JSAPP_DESCRIPTION');?>
				</label>

				<div class="form-field">
					<?php if( $this->config->get( 'htmleditor' ) == 'none' && $this->config->getBool('allowhtml') ) { ?>
						<div class="htmlTag"><?php echo JText::_('COM_COMMUNITY_HTML_TAGS_ALLOWED');?></div>
					<?php } ?>

					<?php
					if( !CStringHelper::isHTML(@$this->row->content)
						&& $this->config->get('htmleditor') != 'none'
						&& $this->config->getBool('allowhtml') )
					{
						$this->row->content = CStringHelper::nl2br(@$this->row->content);
					}
					?>
					<?php
							/*if($this->use_editor && !defined('PF_DEMO_MODE')) {
								?>
								<span class="jomNameTips" title="<?php echo JText::_('JSAPP_APPOINTMENT_DESC_TIPS');?>">
								<?php
								echo $this->editor->display( 'text', @$this->row->content , '100%', '350', '75', '20' ) ;
								?>
								</span>
								<?php
							} else {
							?>
								<textarea cols="42" rows="7" id="text" style="height:auto;width:auto;" name="text" class="jomNameTips input text" title="<?php echo JText::_('JSAPP_APPOINTMENT_DESC_TIPS');?>"><?php echo @$this->row->content;?></textarea>
							<?php
							}*/


							echo $this->editor->displayEditor( 'text',  @$this->row->content , '95%', '150', '10', '20' , false );
					?>
				</div>
			</li>

			<li>
				<label for="title" class="form-label">
					<?php echo JText::_('JSAPP_START');?>
				</label>

				<div class="form-field">
					<span><?php echo JHtml::_((version_compare(JVERSION, '3.0', 'ge')) ? 'appointment.calendar':'calendar', $this->start_date, 'sdate', 'sdate', '%Y-%m-%d');?></span>
					<span class="jomNameTips" title="<?php echo JText::_('JSAPP_START_TIME_TIPS'); ?>">
					<span><?php echo JText::_('JSAPP_HOUR');?>
						<?php echo $this->lists['s_hour'];?></span>
					<span><?php echo JText::_('JSAPP_MINUTE');?>
						<?php echo $this->lists['s_minute'];?></span>
					</span>
				</div>
			</li>

			<li>
				<label for="title" class="form-label">
					<?php echo JText::_('JSAPP_END');?>
				</label>

				<div class="form-field">
					<span><?php echo JHtml::_((version_compare(JVERSION, '3.0', 'ge')) ? 'appointment.calendar':'calendar', $this->end_date, 'edate', 'edate', '%Y-%m-%d');?></span>
					<span><?php echo JText::_('JSAPP_HOUR');?>
						<?php echo $this->lists['e_hour'];?></span>
					<span><?php echo JText::_('JSAPP_MINUTE');?>
						<?php echo $this->lists['e_minute'];?></span>
				</div>
			</li>

			<li class="has-seperator">
				<label for="title" class="form-label">
					<?php echo JText::_('JSAPP_RECEIVER'); ?>
				</label>

				<div class="form-field">
					<?php
						if(count($this->lists['selected_users'])){
							foreach($this->lists['selected_users'] as $su) {
								echo "<div style=\"padding-top: 7px;\">$su</div>\n";
							}
						} else {
							?>
							<div style="padding-top: 7px;">
								<?php echo $this->lists['select_user']; ?>
							</div>
							<?php
						}
					?>
					<div id="user_container"></div>
					<div>
						<a class="add" href="javascript:void(0)" onclick="javascript:add_user();">
							<i class="com-icon-add"></i><span><?php echo JText::_('JSAPP_ADD_RECEIVER');?></span>
						</a>
					</div>
				</div>
			</li>

			<!-- group buttons -->
			<li class="form-action has-seperator">
				<div class="form-field">
					<input type="button" class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?"btn":"cButton";?>" onclick="history.go(-1);return false;" value="<?php echo JText::_('COM_COMMUNITY_CANCEL_BUTTON');?>" />
					<input type="submit" value="<?php echo (!$this->row) ? JText::_('JSAPP_CREATE_APPOINTMENT') : JText::_('COM_COMMUNITY_SAVE_BUTTON');?>" class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?"btn btn-primary":"cButton cButton-Blue"; ?> validateSubmit" />
					<?php echo JHTML::_( 'form.token' ); ?>
				</div>
			</li>
		</ul>
		<div>
			<input type="hidden" name="option" value="com_community" />
			<input type="hidden" name="task" value="save" />
			<input type="hidden" name="view" value="appointments" />
			<input type="hidden" name="id" value="<?php echo intVal(@$this->row->id);?>" />
			<?php echo JHTML::_( 'form.token' ); ?>
		</div>
	</form>
</div>
<script type="text/javascript">
	cvalidate.init();
	<?php if(version_compare($this->cmtversion,'2.8.4.2','>')): ?>
	cvalidate.setSystemText('REM','<?php echo addslashes(JText::_("COM_COMMUNITY_ENTRY_MISSING")); ?>');
	<?php endif; ?>

	function add_user(){
		joms.jQuery('#user_container').append('<div><?php echo str_replace("\n","",$this->lists['select_user']); ?></div>');
	}
</script>