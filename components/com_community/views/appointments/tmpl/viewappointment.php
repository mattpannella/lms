<?php

/**

 * @version			$Id: viewappointment.php 179 2014-07-25 05:55:43Z sanawin $

 * @subpackage		JomSocial

 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com

 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html

 * 

 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.

 * - The system Can make an appointment within the people group.

 * - The system Can make personal appointments.

 * - The system Calendar Have Monthly View, Year View and Weekly View

 * - Add People to list of appointments none limited.

 * - The appointment can change the status to Accept, Not Accept and Not Sure

 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure

 */

defined( '_JEXEC' ) or die( 'Restricted access' );



JHtml::_('behavior.keepalive');



$author		= CFactory::getUser($this->row->author);

$my			= CFactory::getUser();

$jversion	= new JVersion();

$Itemid 	= JFactory::getApplication()->input->get('Itemid');

?>

<div class="cLayout cAppointment-ViewAppointment">

	<form class="cForm" method="post" action="<?php echo CRoute::getURI();?>" name="changeStatus">

		<ul class="cFormList cFormHorizontal cResetList">



			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_TITLE');?>

				</label>



				<div class="form-field">

					<?php echo $this->row->title;?>

				</div>

			</li>



			<?php if($this->row->sdate == $this->row->edate) : ?>

			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_DATE'); ?>

				</label>

				<div class="form-field">

					<?php echo date("l, M. jS Y - g.i a", $this->row->sdate); ?>

				</div>

			</li>

			<?php else: ?>

			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_START_DATE'); ?>

				</label>

				<div class="form-field">

					<?php echo date("l, M. jS Y - g.i a", $this->row->sdate); ?>

				</div>

			</li>

			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_END_DATE'); ?>

				</label>

				<div class="form-field">

					<?php echo date("l, M. jS Y - g.i a", $this->row->edate); ?>

				</div>

			</li>

			<?php endif; ?>



			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_FROM'); ?>

				</label>

				<div class="form-field">

					<ul class="cIndexList forReceiversList cResetList">

						<li>

							<div class="cIndex-Box clearfix">

								<a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid='.$author->id ); ?>" style="display: inline-block; vertical-align: middle;" class="cIndex-Avatar cFloat-L">

									<img class="avatar jomNameTips" src="<?php echo $author->getThumbAvatar(); ?>" title="<?php echo version_compare($jversion->RELEASE, '2.5', '<') ? cAvatarTooltip($author) : CTooltip::cAvatarTooltip($author); ?>" width="40" height="40" alt="<?php echo $author->getDisplayName();?>" />

								</a>

								<div class="cIndex-Content">

									<?php echo $author->getDisplayName();?>

								</div>

							</div>

						</li>

					</ul>

				</div>

			</li>



			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_DESCRIPTION'); ?>

				</label>

				<div class="form-field">

					<?php echo $this->row->content; ?>

				</div>

			</li>



			<li>

				<label for="title" class="form-label">

					<?php echo JText::_('JSAPP_RECEIVERS'); ?>

				</label>

				<div class="form-field">

					<ul class="cIndexList forReceiversList cResetList">

					<?php

						$status 		= 0;

						$images 		= array();

						$live_site 		= JURI::root(true);

						$images[-1] 	= $live_site.'/components/com_community/views/appointments/assets/images/not_accept.png';

						$images[0] 		= $live_site.'/components/com_community/views/appointments/assets/images/wait.png';

						$images[1] 		= $live_site.'/components/com_community/views/appointments/assets/images/accept.png';

						$images[2] 		= $live_site.'/components/com_community/views/appointments/assets/images/notsure.png';

						$tooltips 		= array();

						$tooltips[-1] 	= JText::_("JSAPP_REPLY_NOT_ACCEPT");

						$tooltips[0] 	= JText::_("JSAPP_REPLY_NOT_RESPONSE");

						$tooltips[1] 	= JText::_("JSAPP_ACCEPT");

						$tooltips[2] 	= JText::_("JSAPP_NOT_SURE");



						$reply_color	= array();

						$reply_color[-1]= "<span style='color: red'>(%s)</span>";

						$reply_color[0]	= "<span>(%s)</span>";

						$reply_color[1]	= "<span style='color: green'>(%s)</span>";

						$reply_color[2]	= "<span style='color: blue'>(%s)</span>";



						foreach($this->users as $u):

							$cuser = CFactory::getUser($u->userid);

							$reply = sprintf($reply_color[$u->accept], $tooltips[$u->accept]);

							if($u->userid == $my->id) 

							{

								?>

								<li>

										<div class="cIndex-Box clearfix">

											<a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid='.$u->userid ); ?>" style="display: inline-block; vertical-align: middle;" class="cIndex-Avatar cFloat-L">

												<img class="avatar jomNameTips" src="<?php echo $cuser->getThumbAvatar(); ?>" title="<?php echo version_compare($jversion->RELEASE, '2.5', '<') ? cAvatarTooltip($author) : CTooltip::cAvatarTooltip($cuser); ?>" width="40" height="40" alt="<?php echo $cuser->getDisplayName();?>" />

											</a>



											<div class="cIndex-Content">

												<?php echo $cuser->getDisplayName(); ?>

												<div>

													<span id='reasontext_<?php echo $u->userid; ?>'><?php echo $u->reason; ?></span>

												</div>

												

												<div class="cIndex-Actions cStatus">

													<div>

														<img class='jomNameTips' title='<?php echo $tooltips[$u->accept]; ?>' src='<?php echo $images[$u->accept]; ?>' />&nbsp;<?php echo $reply; ?>&nbsp;

													</div>

													<div id='showreason_<?php echo $u->userid; ?>'  style="clear: both; display: block;"></div>

												</div>

											</div>

										</div>

								</li>		

								<?php

							}

							else

							{

								?>

									<li>

										<div class="cIndex-Box clearfix">

											<a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid='.$u->userid ); ?>" style="display: inline-block; vertical-align: middle;" class="cIndex-Avatar cFloat-L">

												<img class="avatar jomNameTips" src="<?php echo $cuser->getThumbAvatar(); ?>" title="<?php echo version_compare($jversion->RELEASE, '2.5', '<') ? cAvatarTooltip($author) : CTooltip::cAvatarTooltip($cuser); ?>" width="40" height="40" alt="<?php echo $cuser->getDisplayName();?>" />

											</a>



											<div class="cIndex-Content">

												<?php echo $cuser->getDisplayName(); ?>

												<?php if($u->reason): ?>

													<div>

														<?php echo $u->reason; ?>

													</div>

												<?php endif; ?>

												<div class="cIndex-Actions">

													<img class='jomNameTips' src='<?php echo $images[$u->accept]; ?>' title='<?php echo $tooltips[$u->accept]; ?>'/>&nbsp;<?php echo $reply; ?>

												</div>

											</div>

										</div>

									</li>

								<?php

							}

						endforeach;

					?>

					</ul>

				</div>

			</li>



		</ul>

		<input type="hidden" name="option" value="com_community" />

		<input type="hidden" name="view" value="appointments" />

		<input type="hidden" name="task" value="changestate" />

		<input type="hidden" name="status" value="" />

		<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />

		<input type="hidden" name="id" value="<?php echo $this->row->id;?>" />

		<?php echo JHTML::_( 'form.token' ); ?>

	

</div>

<?php if($u->userid == $my->id) 

							{ ?>
<div id="changestatus_row">

					<div>

						<div><strong><?php echo JText::_("JSAPP_NOTE");?></strong></div>

						<textarea name="reason" class="reason"></textarea><br/>

						<a href="javascript:chageStatus(1);" class="btn btn-small"><img class="jomNameTips" src="<?php echo $images[1];?>" border="0" title="<?php echo $tooltips[1];?>" /><?php echo JText::_("JSAPP_ACCEPT");?></a>

						<a href="javascript:chageStatus(-1);" class="btn btn-small"><img class="jomNameTips" src="<?php echo $images[-1];?>" border="0" title="<?php echo $tooltips[-1];?>" /><?php echo JText::_("JSAPP_REPLY_NOT_ACCEPT");?></a>

						<a href="javascript:chageStatus(2);" class="btn btn-small"><img class="jomNameTips" src="<?php echo $images[2];?>" border="0" title="<?php echo $tooltips[2];?>" /><?php echo JText::_("JSAPP_NOT_SURE");?></a>

					</div>

				</div>
                
                <?php } ?>
                </form>
                <script type="text/javascript">


	function show_changestatus_row(userid) 

	{

		var showreason = joms.jQuery("#showreason_"+userid);

		var reasontext = joms.jQuery("#reasontext_"+userid);

		var cactions   = joms.jQuery(".cStatus");

		if(showreason.html() == '') {

			cactions.css( "position", "relative" );

			showreason.append(jsstring.replace('REASONVALUE', reasontext.html()));

		} else {

			cactions.removeAttr( "style" );

			showreason.html('');

		}

	}



	function chageStatus(status) 

	{

		var form = document.changeStatus;

		form.status.value = status;

		form.submit();

	}





	(function($) {

		joms.extend({

			appointments:{

				deleteAppointment:function(a){

					var b="jax.call('community', 'plugins,appointments,ajaxWarnAppointmentDeletion', '"+a+"');";

					cWindowShow(b,"",450,100,"warning")

				}

			}

		});

	})(joms.jQuery);



</script>