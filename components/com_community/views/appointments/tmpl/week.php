<?php
/**
 * @version			$Id: week.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
$Itemid = JFactory::getApplication()->input->get('Itemid');
$onlineUser = CFactory::getUser();
?>
<form action="<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=week');?>" method="post" name="adminForm" id="adminForm">
	<a id="pf_top"></a>
	<div class="pf_container">
		<div class="pf_body">
			<div class="cSubmenu cResetList cFloatedList clearfix">
				<span class="pf_nav_day">
					<label for="day"><strong><?php echo JText::_('JSAPP_GO_TO_DATE'); ?> </strong></label>
					<input type="text" size="3" value="<?php echo $this->today;?>" class="input input-mini" name="day">
				</span>
				<span class="pf_nav_month">
					<select name="month" class="input input-medium" id="month">
						<?php echo JHtml::_('select.options', $this->buildmonthlist(), 'value', 'text', $this->month); ?>
					</select>
				</span>
				<span class="pf_nav_year">
					<select name="year" class="input input-small" id="year">
						<?php echo JHtml::_('select.options', $this->buildyearlist(), 'value', 'text', $this->year); ?>
					</select>
				</span>
				<span>
					<a href="javascript:submitbutton('week');" class="pf_button">
						<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn btn-primary':'cButton cButton-Black'; ?>" <?php if(version_compare($this->cmtversion,'2.8.4.2','>')): ?>style="margin-bottom: 10px;"<?php endif; ?>>
						<?php echo JText::_('JSAPP_VIEWS');?>
						</span>
					</a>
				</span>
				<span style="float: right">
					<?php
					$weekNext	= CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=week&day=%d&month=%d&year=%d&startDay=%s', date('d', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+7,$this->start_of_week_y)), date('m', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+7,$this->start_of_week_y)), date('Y', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+7,$this->start_of_week_y)), $this->startDay));
					$weekPrev	= CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=week&day=%d&month=%d&year=%d&startDay=%s', date('d', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week-7,$this->start_of_week_y)), date('m', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week-7,$this->start_of_week_y)), date('Y', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week-7,$this->start_of_week_y)), $this->startDay));
					?>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $weekPrev; ?>', '_parent')">◄</span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=week'); ?>', '_parent')"><?php echo JText::_('JSAPP_TODAY');?></span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $weekNext; ?>', '_parent')">►</span>
				</span>
			</div>
			<!-- TABLE START -->
			<table class="pf_table_week" width="100%" cellpadding="1" cellspacing="1">
			<thead>
				<tr>
					<th class="odd" width="1%"><?php echo JText::_('JSAPP_HOUR');?></th>
					<?php
					$c = 0;
					$s_day = $this->start_of_week;
					$s_month = $this->start_of_week_m;
					$s_year = $this->start_of_week_y;
					$date_arr	= array();
					while ($this->start_of_week_y.$this->start_of_week_m.$this->start_of_week <= $this->end_of_week_y.$this->end_of_week_m.$this->end_of_week ) {
						$date_of_week	= date($this->dateFormat, mktime(0,0,0,$this->start_of_week_m,$this->start_of_week,$this->start_of_week_y));
						if($this->dateFormat == 'j M') {
							$day_of_week	= date('j', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week,$this->start_of_week_y));
							$month_of_week	= date('M', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week,$this->start_of_week_y));
							$date_of_week	= "$day_of_week ".JText::_('JSAPP_'.strtoupper($month_of_week));
						}
						elseif($this->dateFormat == 'M j') {
							$day_of_week	= date('j', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week,$this->start_of_week_y));
							$month_of_week	= date('M', mktime(0,0,0,$this->start_of_week_m,$this->start_of_week,$this->start_of_week_y));
							$date_of_week	= JText::_('JSAPP_'.strtoupper($month_of_week))." $day_of_week";
						}
					?>
						<th width="14%"><?php echo $date_arr[$c] = $this->day_names[$c]."<br>".$date_of_week; ?></th>
						<?php
						$this->start_of_week_y = date("Y", mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+1,$this->start_of_week_y));
						if (date("m", mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+1,$this->start_of_week_y)) == $this->start_of_week_m) {
							$this->start_of_week = date("d", mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+1,$this->start_of_week_y));
						}
						else {
							$this->start_of_week_m = date("m", mktime(0,0,0,$this->start_of_week_m,$this->start_of_week+1,$this->start_of_week_y));
							$this->start_of_week   = date("d", mktime(0,0,0,$this->start_of_week_m,1,$this->start_of_week_y));
						}
						$c++;
					}
					?>
			</tr>
		</thead>
		<tbody>
				<?php
				$k = 0;
				$h = 0;
				$e = array(0, 0, 0, 0, 0, 0, 0, 0);
				$start_hour = array();
				$start_event = array();
				$event_width = 10;
				define('event_width', "width: $event_width%; ");
				$date	= $start_date = mktime(0, 0, 0, $s_month, $s_day, $s_year);
				$link_view = 'index.php?option=com_community&view=appointments&task=viewappointment&id=';
				foreach ($this->hours AS $hour=>$hour_str) {
					$start_day	= $s_day;
					$link_add	= CRoute::_('index.php?option=com_community&view=appointments&task=edit&hour='.$hour);
					?>
					<tr class="row<?php echo $k; ?> sectiontableentry<?php echo $k + 1; ?>">
						<td class="hour">
							<a id="<?php echo str_replace('<br/>', '', $hour_str); ?>"></a>
							<div class="hournum"><?php echo $hour_str; ?></div>
						</td>
						<!-- 1st column EVENTS -->
						<?php
							// events
							$start_date	= mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
						?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[0]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
									$events = count($this->rows[1][$h]['events']);
									foreach ($this->rows[1][$h]['events'] AS $x => $row) {
										$user_from = CFactory::getUser($row->author);
										$checkbox  = '';
										$isHead		= false;
										if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
											$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
											$isHead = true;
										}
										if($isHead || $h==0) {
											$start_event[1][$row->id]	= $e[1]++;
											$start_hour[1][$x]	= $hour;
										}
										$height = 0;
										$top	= '';
										// calculate height of <div> event in percentage
										if($row->sdate <= $start_date) {
											// if event end before date
											if($row->edate < $end_date) {
												$minute	= date('i', $row->edate);
												$event_height	= $minute/60*100;
												$height = round( $event_height, 3 );
											}
											// if event end at date or still continue
											elseif($row->edate >= $end_date) $height = 100;
										}
										elseif($row->sdate > $start_date) {
											$minute_start	= date('i', $row->sdate);
											$cal_height = $minute_start/60*100;
											// calculate height for free time above event
											$free_height = round( $cal_height, 3 );
											$top = "top: $free_height%";
											// if event end before date
											if($row->edate < $end_date) {
												$minute_end	= date('i', $row->edate);
												$event_height = ($minute_end-$minute_start)/60*100;
												$height = round( $event_height, 3 );
											}
											// if event end at date or still continue
											elseif($row->edate >= $end_date) $height = 100-$free_height;
										}
										else $height = 0;
										// merge style
										$left = isset($start_event[1][$row->id]) && $start_event[1][$row->id] > 0  ? $start_event[1][$row->id]*$event_width : 0;
										$style = "style='height: $height%; $top; left: $left%; ".event_width;
										$style.= $height > 0 ? "min-height: 50%;'" : "'";
										// check to use head or sub class
										if($isHead) $class = "appointment_event";
										else $class	= "appointment_subevent";
									?>
									<div
										class="<?php echo $class?> jomTips"
										id="event_c1_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
										<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
										>
										<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 2nd column EVENTS -->
							<?php
								// events
								++$start_day;
								$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
								$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
								$day			= '&day='.date('d',$start_date);
								$month		= '&month='.date('m',$start_date);
								$year			= '&year='.date('Y',$start_date);
								$date_get	= $day.$month.$year;
							?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[1]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
								$events = count($this->rows[2][$h]['events']);
								foreach ($this->rows[2][$h]['events'] AS $x => $row) {
									$user_from = CFactory::getUser($row->author);
									$checkbox  = '';
									$isHead		= false;
									if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[2][$row->id]	= $e[2]++;
										$start_hour[2][$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = isset($start_event[2][$row->id]) && $start_event[2][$row->id] > 0  ? $start_event[2][$row->id]*$event_width : 0;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									$style.= $height > 0 ? "min-height: 50%;'" : "'";
									// check to use head or sub class
									if($isHead) $class = "appointment_event";
									else $class	= "appointment_subevent";
								?>
								<div
									class="<?php echo $class?> jomTips"
										id="event_c2_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
									<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
									<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 3rd column EVENTS -->
						<?php
							// events
							++$start_day;
							$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
						?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[2]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
							<?php
							$events	= count($this->rows[3][$h]['events']);
							foreach ($this->rows[3][$h]['events'] AS $x => $row) {
								$user_from = CFactory::getUser($row->author);
								$checkbox  = '';
								$isHead	= false;
								if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
									$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
									$isHead = true;
								}
								if($isHead || $h==0) {
									$start_event[3][$row->id]	= $e[3]++;
									$start_hour[3][$x]	= $hour;
								}
								$height	= 0;
								$top		= '';
								// calculate height of <div> event in percentage
								if($row->sdate <= $start_date) {
									// if event end before date
									if($row->edate < $end_date) {
										$minute	= date('i', $row->edate);
										$event_height	= $minute/60*100;
										$height = round( $event_height, 3 );
									}
									// if event end at date or still continue
									elseif($row->edate >= $end_date) $height = 100;
								}
								elseif($row->sdate > $start_date) {
									$minute_start	= date('i', $row->sdate);
									$cal_height = $minute_start/60*100;
									// calculate height for free time above event
									$free_height = round( $cal_height, 3 );
									$top = "top: $free_height%";
									// if event end before date
									if($row->edate < $end_date) {
										$minute_end	= date('i', $row->edate);
										$event_height = ($minute_end-$minute_start)/60*100;
										$height = round( $event_height, 3 );
									}
									// if event end at date or still continue
									elseif($row->edate >= $end_date) $height = 100-$free_height;
								}
								else $height = 0;
								// merge style
								$left = isset($start_event[3][$row->id]) && $start_event[3][$row->id] > 0  ? $start_event[3][$row->id]*$event_width : 0;
								$style = "style='height: $height%; $top; left: $left%; ".event_width;
								$style.= $height > 0 ? "min-height: 50%;'" : "'";
								// check to use head or sub class
								if($isHead) $class = "appointment_event";
								else $class	= "appointment_subevent";
								?>
								<div
									class="appointment_event jomTips"
									id="event_c3_<?php echo $x?>_<?php echo $hour?>"
									title="
										<?php
									$eventTitle = htmlspecialchars($row->title);
									echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
									<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
									<?php echo $style;?>
									onmouseover="this.style.cursor='help';"
									onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
									<div></div>
								</div>
							<?php
							}
							?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 4th column EVENTS -->
						<?php
							// events
							++$start_day;
							$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
						?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[3]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
								$events = count($this->rows[4][$h]['events']);
								foreach ($this->rows[4][$h]['events'] AS $x => $row) {
									$user_from = CFactory::getUser($row->author);
									$checkbox  = '';
									$isHead		= false;
									if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[4][$row->id]	= $e[4]++;
										$start_hour[4][$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = isset($start_event[4][$row->id]) && $start_event[4][$row->id] > 0  ? $start_event[4][$row->id]*$event_width : 0;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									$style.= $height > 0 ? "min-height: 50%;'" : "'";
									// check to use head or sub class
									if($isHead) $class = "appointment_event";
									else $class	= "appointment_subevent";
							?>
								<div
									class="<?php echo $class?> jomTips"
										id="event_c4_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
									<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
									<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 5th column EVENTS -->
						<?php
							// events
							++$start_day;
							$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
							?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[4]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
								$events = count($this->rows[5][$h]['events']);
								foreach ($this->rows[5][$h]['events'] AS $x => $row) {
									$user_from = CFactory::getUser($row->author);
									$checkbox  = '';
									$isHead	= false;
									if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[5][$row->id]	= $e[5]++;
										$start_hour[5][$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = isset($start_event[5][$row->id]) && $start_event[5][$row->id] > 0 ? $start_event[5][$row->id]*$event_width : 0;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									$style.= $height > 0 ? "min-height: 50%;'" : "'";
									// check to use head or sub class
									if($isHead) $class = "appointment_event";
									else $class	= "appointment_subevent";
								?>
								<div
									class="<?php echo $class?> jomTips"
										id="event_c5_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
									<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
									<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 6th column EVENTS -->
						<?php
							// events
							++$start_day;
							$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
						?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[5]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
								$events = count($this->rows[6][$h]['events']);
								foreach ($this->rows[6][$h]['events'] AS $x => $row) {
									$user_from = CFactory::getUser($row->author);
									$checkbox  = '';
									$isHead	= false;
									if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[6][$row->id]	= $e[6]++;
										$start_hour[6][$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = isset($start_event[6][$row->id]) && $start_event[6][$row->id] > 0  ? $start_event[6][$row->id]*$event_width : 0;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									$style.= $height > 0 ? "min-height: 50%;'" : "'";
									// check to use head or sub class
								if($isHead) $class = "appointment_event";
								else $class	= "appointment_subevent";
							?>
								<div
									class="<?php echo $class?> jomTips"
										id="event_c6_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
									<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
									<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
						<!-- 7th column EVENTS -->
						<?php
							// events
							++$start_day;
							$start_date = mktime($hour, 0, 0, $s_month, $start_day, $s_year);
							$end_date	= mktime($hour, 59, 59, $s_month, $start_day, $s_year);
							$day			= '&day='.date('d',$start_date);
							$month		= '&month='.date('m',$start_date);
							$year			= '&year='.date('Y',$start_date);
							$date_get	= $day.$month.$year;
						?>
						<td>
							<div class="event_wrapper">
								<div
									class="event_add"
									title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br>',' ', $date_arr[6]).' : '.str_replace('<br/>','', $hour_str);?> <<"
									onmouseover="this.style.cursor='pointer';"
									onclick="window.open('<?php echo $link_add.$day.$month.$year; ?>', '_parent');">
								</div>
								<?php
								$events = count($this->rows[7][$h]['events']);
								foreach ($this->rows[7][$h]['events'] AS $x => $row) {
									$user_from = CFactory::getUser($row->author);
									$checkbox  = '';
									$isHead	= false;
									if (($onlineUser->id == $row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$checkbox = '<input id="cb' . $x . '" name="cid[]" value="' . $row->id . '" onclick="Joomla.isChecked(this.checked);" type="checkbox" style="margin: 2px;">';
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[7][$row->id]	= $e[7]++;
										$start_hour[7][$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = isset($start_event[7][$row->id]) && $start_event[7][$row->id] > 0  ? $start_event[7][$row->id]*$event_width : 0;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									$style.= $height > 0 ? "min-height: 50%;'" : "'";
									// check to use head or sub class
								if($isHead) $class = "appointment_event";
								else $class	= "appointment_subevent";
							?>
								<div
									class="<?php echo $class?> jomTips"
										id="event_c7_<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php
										$eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br>".$this->transDate(date($format, $row->sdate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_END').":<br>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
										<?php echo $style;?>
										onmouseover="this.style.cursor='help';"
										onclick="window.open('<?php echo CRoute::_($link_view . $row->id . $date_get); ?>', '_parent');"
									>
										<div></div>
									</div>
								<?php
								}
								?>
								<div class="event_30" style="pointer-events: none;"></div>
							</div>
						</td>
					</tr>
					<?php
					$k = 1 - $k;
					$h++;
				}
				?>
			</tbody>
		</table>
		<!-- end table -->
		</div>
		<!-- end body -->
	</div>
	<!-- end container -->
	<input type="hidden" name="option" value="com_community" />
	<input type="hidden" name="view" value="appointments" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
</form>