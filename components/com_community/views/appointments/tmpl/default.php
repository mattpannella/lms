<?php
/**
 * @version			$Id: default.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
$jversion	= new JVersion();
$config = CFactory::getConfig();
?>

<?php echo $this->sortings; ?>


<div class="cLayout cAppointments-Myappointment">
	<?php 
		if( !empty( $this->appointments ) ) 
		{ 
	?>
	<ul class="cIndexList forMyAppointmentList cResetList">
		<?php
			$event_link_view	= 'index.php?option=com_community&view=appointments&task=viewappointment&id=';
			$live_site 			= JURI::root(true);
			$images 			= array();
			$images[-1] 		= $live_site.'/components/com_community/views/appointments/assets/images/not_accept.png';
			$images[0] 			= $live_site.'/components/com_community/views/appointments/assets/images/wait.png';
			$images[1] 			= $live_site.'/components/com_community/views/appointments/assets/images/accept.png';
			$images[2] 			= $live_site.'/components/com_community/views/appointments/assets/images/notsure.png';

			$tooltips 			= array();
			$tooltips[-1] 		= JText::_("JSAPP_REPLY_NOT_ACCEPT");
			$tooltips[0] 		= JText::_("JSAPP_REPLY_NOT_RESPONSE");
			$tooltips[1] 		= JText::_("JSAPP_ACCEPT");
			$tooltips[2] 		= JText::_("JSAPP_NOT_SURE");

			for( $i = 0; $i < count( $this->appointments ); $i++ )
			{
				$row 			=& $this->appointments[$i];
				$row->startdate = $row->sdate;
				$author			= CFactory::getUser($row->author);
				$not 			= $this->loadAppStatusUsers($row->id, '-1');
				$accept 		= $this->loadAppStatusUsers($row->id, '1');
				$notsure 		= $this->loadAppStatusUsers($row->id, '2');
				$wait 			= $this->loadAppStatusUsers($row->id, '0');
				$receivers      = (int)($not + $accept + $notsure + $wait);


		?>
		<li id="appointment">
			<div class="cIndex-Box clearfix">
				<a href="<?php echo CRoute::_('index.php?option=com_community&view=profile&userid='.$row->author ); ?>" class="cIndex-Avatar cFloat-L">
					<img class="avatar jomNameTips" src="<?php echo $author->getThumbAvatar(); ?>" title="<?php echo version_compare($jversion->RELEASE, '2.5', '<') ? cAvatarTooltip($author) : CTooltip::cAvatarTooltip($author); ?>" width="40" height="40" alt="<?php echo $author->getDisplayName();?>" />
				</a>
				<div class="cIndex-Content">
					<a href="<?php echo CRoute::_($event_link_view.$row->id);?>"><?php echo $row->title; ?></a>
					<div class="cIndex-Status">
						<div class="cIndex-Date"><?php echo CEventHelper::formatStartDate($row, $config->get('eventdateformat') ); ?></div>
					</div>

					<div class="cIndex-Actions">
						<div>
							<i class="com-icon-groups"></i>
							<?php echo $receivers.' '.JText::_('JSAPP_RECEIVERS'); ?>
						</div>

						<!-- accept -->
						<div>
							<img src="<?php echo $images[1]; ?>" title="<?php echo $tooltips[1]; ?>" /> <?php echo $accept.' '.$tooltips[1]; ?>
						</div>

						<!-- not -->
						<div>
							<img src="<?php echo $images[-1]; ?>" title="<?php echo $tooltips[-1]; ?>" /> <?php echo $not.' '.$tooltips[-1]; ?>
						</div>

						<!-- notsure -->
						<div>
							<img src="<?php echo $images[2]; ?>" title="<?php echo $tooltips[2]; ?>" /> <?php echo $notsure.' '.$tooltips[2]; ?>
						</div>

						<!-- wait -->
						<div>
							<img src="<?php echo $images[0]; ?>" title="<?php echo $tooltips[0]; ?>" /> <?php echo $wait.' '.$tooltips[0]; ?>
						</div>

					</div>
				</div>
			</div>
		</li>
		<?php } ?>
	</ul>
		<?php
		if ( !empty($this->pagination))
		{
		?>
		<div class="cPagination">
			<?php echo $this->pagination->getPagesLinks(); ?>
		</div>
		<?php
		}
		?>
	<?php 
		} else {
	?>
	<div class="cEmpty cAlert"><?php echo JText::_('JSAPP_NO_APPOINTMENTS_YET'); ?></div>
	<?php
		}
	?>	
</div>
