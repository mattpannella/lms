<?php
/**
 * @version			$Id: day.php 179 2014-07-25 05:55:43Z sanawin $
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
defined( '_JEXEC' ) or die( 'Restricted access' );

JHtml::_('behavior.keepalive');
$Itemid = JFactory::getApplication()->input->get('Itemid');
$onlineUser = CFactory::getUser();
?>
<form action="<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=day');?>" method="post" name="adminForm">
	<a id="pf_top"></a>
	<div class="pf_container">
		<div class="pf_body">
			<div class="cSubmenu cResetList cFloatedList clearfix">
				<span class="pf_nav_day">
					<label for="day"><strong><?php echo JText::_('JSAPP_GO_TO_DATE'); ?> </strong></label>
					<input type="text" size="3" value="<?php echo $this->today;?>" class="input input-mini" name="day">
				</span>
				<span class="pf_nav_month">
					<select name="month" class="input input-medium" id="month">
						<?php echo JHtml::_('select.options', $this->buildmonthlist(), 'value', 'text', $this->month); ?>
					</select>
				</span>
				<span class="pf_nav_year">
					<select name="year" class="input input-small" id="year">
						<?php echo JHtml::_('select.options', $this->buildyearlist(), 'value', 'text', $this->year); ?>
					</select>
				</span>
				<span>
					<a href="javascript:submitbutton('day');" class="pf_button">
						<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn btn-primary':'cButton cButton-Black'; ?>" <?php if(version_compare($this->cmtversion,'2.8.4.2','>')): ?>style="margin-bottom: 10px;"<?php endif; ?>><?php echo JText::_('JSAPP_VIEWS');?></span>
					</a>
				</span>
				<span style="float: right">
					<?php
						$day_before	= mktime(0,0,0,$this->month,$this->today-1,$this->year);
						$day_after		= mktime(0,0,0,$this->month,$this->today+1,$this->year);
						$dayNext		= CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=day&day=%d&month=%d&year=%d', date('d', $day_after), date('m', $day_after), date('Y', $day_after)));
						$dayPrev		= CRoute::_(sprintf('index.php?option=com_community&view=appointments&task=day&day=%d&month=%d&year=%d', date('d', $day_before), date('m', $day_before), date('Y', $day_before)));

						$date_today	= mktime(0,0,0,$this->month,$this->today,$this->year);
						$day_today		= date('j', $date_today);
						$month_today	= date('F', $date_today);
						if($this->dateFormat == 'j M') $date_today = $day_today.' '.JText::_(strtoupper($month_today));
						elseif($this->dateFormat == 'M j') $date_today = JText::_(strtoupper($month_today)).' '.$day_today;
					?>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $dayPrev; ?>', '_parent')">◄</span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo CRoute::_('index.php?option=com_community&view=appointments&task=day'); ?>', '_parent')"><?php echo JText::_('JSAPP_TODAY');?></span>
					<span class="<?php echo version_compare($this->cmtversion,'2.8.4.2','>')?'btn':'cButton cButton-Black'; ?>" onclick="window.open('<?php echo $dayNext; ?>', '_parent')">►</span>
				</span>
			</div>
			<!-- TABLE START -->
			<table class="pf_table_week" width="100%" cellpadding="1" cellspacing="1">
				<thead>
					<tr>
						<th class="odd" width="1%"><?php echo JText::_('JSAPP_HOUR');?></th>
						<th class="odd"><?php echo JText::_('JSAPP_APPOINTMENT').' ('.$date_today.')';?></th>
						<th class="odd" width="1%"></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$k = 0;
					$h = 0;
					$e = 0;
					$start_hour = array();
					$start_event = array();
					$event_width = 5;
					define('event_width', "width: $event_width%; ");
					foreach ($this->hours AS $hour=>$hour_str) {
						$link_add	= CRoute::_('index.php?option=com_community&view=appointments&task=edit&year='.$this->year.'&month='.$this->month.'&day='.$this->today.'&hour='.intval($hour));
						$link_view	= 'index.php?option=com_community&view=appointments&task=viewappointment&id=';
						$start_date = mktime($hour, 0, 0, $this->month, $this->today, $this->year);
						$end_date   = mktime($hour, 59, 59, $this->month, $this->today, $this->year);
					?>
						<tr class="row<?php echo $k;?> sectiontableentry<?php echo $k + 1;?>">
							<td class="colum1" style="text-align: center; vertical-align: middle">
								<a id="<?php echo str_replace('<br/>', '', $hour_str); ?>"></a>
								<div class="hournum"><?php echo $hour_str;?></div>
							</td>
							<td>
								<div class="event_wrapper">
								<?php
								$events	= count($this->rows[$h]['events']);
								foreach($this->rows[$h]['events'] AS $x => $row) 
								{
									$user_from = CFactory::getUser($row->author);
									$isHead	= false;
									if( ($onlineUser->id==$row->author) && $row->sdate >= $start_date && $row->sdate <= $end_date) {
										$isHead = true;
									}
									if($isHead || $h==0) {
										$start_event[$row->id]	= $e++;
										$start_hour[$x]	= $hour;
									}
									$height = 0;
									$top	= '';
									// calculate height of <div> event in percentage
									if($row->sdate <= $start_date) {
										// if event end before date
										if($row->edate < $end_date) {
											$minute	= date('i', $row->edate);
											$event_height	= $minute/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100;
									}
									elseif($row->sdate > $start_date) {
										$minute_start	= date('i', $row->sdate);
										$cal_height = $minute_start/60*100;
										// calculate height for free time above event
										$free_height = round( $cal_height, 3 );
										$top = "top: $free_height%";
										// if event end before date
										if($row->edate < $end_date) {
											$minute_end	= date('i', $row->edate);
											$event_height = ($minute_end-$minute_start)/60*100;
											$height = round( $event_height, 3 );
										}
										// if event end at date or still continue
										elseif($row->edate >= $end_date) $height = 100-$free_height;
									}
									else $height = 0;
									// merge style
									$left = $start_event[$row->id]*$event_width;
									$style = "style='height: $height%; $top; left: $left%; ".event_width;
									if($isHead) $style.= $height > 0 ? "min-height: 50%;'" : "'";
									else	$style.= "'";
									// check to use head or sub class
									if($isHead) $class = "appointment_event";
									else $class	= "appointment_subevent";
								?>
									<div
										class="<?php echo $class?> jomTips"
										id="event<?php echo $x?>_<?php echo $hour?>"
										title="
										<?php $eventTitle = htmlspecialchars($row->title);
										echo JString::strlen($eventTitle) < 0 ? (JString::substr($eventTitle , 0 , 15).'...') : $eventTitle; ?>::
										<?php if($this->timeFormat == 12) $format = "{$this->evtdateFormat} g:i:s A"; else $format = "{$this->evtdateFormat} G:i:s"; ?>
										<?php echo JText::_('JSAPP_START').":<br/>".$this->transDate(date($format, $row->sdate), $this->dateFormat)."<br/>".JText::_('JSAPP_END').":<br/>".$this->transDate(date($format, $row->edate), $this->dateFormat); ?><br/>
										<?php echo JText::_('JSAPP_FROM') . ': ' . htmlspecialchars($user_from->getDisplayName()); ?><br><br>
										<?php echo JText::_('JSAPP_CLICK_FOR_DETAIL'); ?>
										"
										<?php echo $style;?>
										onmouseover="this.style.cursor = 'help';" onclick="window.open('<?php echo CRoute::_("$link_view{$row->id}&day={$this->today}&month={$this->month}&year={$this->year}"); ?>', '_parent');"
									>
										<div></div>
									</div>
								<?php } ?>
									<div class="event_30" style="pointer-events: none;"></div>
								</div>
							</td>
							<td style="vertical-align: middle"><div class="pf_addevent" title="<?php echo JText::_('JSAPP_CLICK_TO_ADD_APPOINTMENT'); ?>&#013;>> <?php echo str_replace('<br/>','', $hour_str);?> <<" onclick="window.open('<?php echo $link_add;?>', '_parent');"></div></td>
						</tr>
						<?php
						$k = 1-$k;
						$h++;
					}
					?>
					</tbody>
			</table>
		</div>
	</div>
	<input type="hidden" name="option" value="com_community" />
	<input type="hidden" name="view" value="appointments" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
</form>