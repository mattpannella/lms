<?php
/**
 * @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die();
?>

<?php if ($disableUpload) { ?>

    <div style="width:100%;text-align:center;height:2em;"><?php echo $preMessage;?></div>

<?php } else { ?>

    <script>
        joms || (joms = {});
        joms.language || (joms.language = {});
        joms.language.multiupload || (joms.language.multiupload = {});
        joms.language.multiupload.size = '<?php echo JText::_("COM_COMMUNITY_PHOTOS_MULTIUPLOAD_SIZE"); ?>';
        joms.language.multiupload.status = '<?php echo JText::_("COM_COMMUNITY_PHOTOS_MULTIUPLOAD_STATUS"); ?>';
        joms.language.multiupload.drag_files = '<?php echo JText::_("COM_COMMUNITY_PHOTOS_MULTIUPLOAD_DRAG_FILES"); ?>';
        joms.language.multiupload.start_upload = '<?php echo JText::_("COM_COMMUNITY_PHOTOS_MULTIUPLOAD_START_UPLOAD"); ?>';
    </script>    

    <div id="photo-uploader" class="joms-share-popup" style="clearfix">

        <div id="upload-header" class="clearfix">

            <div id="new-album" style="margin-top:10px">
                <input type="text" id="album-name" style="margin-bottom:0px;" placeholder="<?php echo JText::_('COM_COMMUNITY_PHOTOS_NEW'); ?> <?php echo JText::_('COM_COMMUNITY_PHOTOS_ALBUM_NAME'); ?>" />
                <div id="optional-album" style="display:inline">
                    <span style="padding:0px 12px;"><?php echo JText::_('COM_COMMUNITY_OR'); ?></span>
                    <a href="javascript:joms.photos.multiUpload.showExistingAlbum();" class="btn btn-warning"><?php echo JText::_('COM_COMMUNITY_PHOTOS_ADD_TO_EXISTING_ALBUM'); ?></a>
                </div>
            </div>

            <div id="select-album" style="display:none;margin-top:10px;">
                <select id="albumid" name="albumid" style="margin-bottom:0px;">
                    <?php foreach ($allAlbums as $index => $objAlbumProp) { ?>
                        <option value="<?php echo $objAlbumProp->id;?>"><?php echo $objAlbumProp->name;?></option>
                    <?php } ?>
                </select>
                <span style="padding:0px 12px;"><?php echo JText::_('COM_COMMUNITY_OR'); ?></span>
                <a class="btn btn-primary" href="javascript:joms.photos.multiUpload.createNewAlbum();"><?php echo strtolower(JText::_('COM_COMMUNITY_PHOTOS_CREATE_NEW_ALBUM_TITLE')); ?></a>
            </div>

            <div class="joms-share-status-action" style="float:right;">
                <div data-stream-id="2" class="joms-privacy-dropdown joms-album-privacy">
                    <input type="hidden" name="joms-postbox-privacy" value="<?php echo PRIVACY_PUBLIC?>">
                    <button data-toggle="dropdown" class="dropdown-toggle" data-selected-privacy="" type="button"><span class="dropdown-value"><i class="joms-icon-globe"></i></span><span class="dropdown-caret joms-icon-caret-down"></span></button>
                    <ul class="dropdown-menu" style="right:0;left:auto;min-width:135px">
                        <li><a data-option-value="<?php echo PRIVACY_PUBLIC ?>" href="#"><i class="joms-icon-globe"></i><span><?php echo JText::_('COM_COMMUNITY_PRIVACY_PUBLIC');?></span></a></li>
                        <li><a data-option-value="<?php echo PRIVACY_MEMBERS; ?>" href="#"><i class="joms-icon-users"></i><span><?php echo JText::_('COM_COMMUNITY_PRIVACY_SITE_MEMBERS');?></span></a></li>
                        <li><a data-option-value="<?php echo PRIVACY_FRIENDS; ?>" href="#"><i class="joms-icon-user"></i><span><?php echo JText::_('COM_COMMUNITY_PRIVACY_FRIENDS');?></span></a></li>
                        <li><a data-option-value="<?php echo PRIVACY_PRIVATE; ?>" href="#"><i class="joms-icon-lock"></i><span><?php echo JText::_('COM_COMMUNITY_PRIVACY_ME');?></span></a></li>
                    </ul>
                </div>
            </div>


            <?php if (intval($groupId) === 0): ?>
                <div style="margin-top:12px;">
                    <small id="photoUploaderNotice"><?php echo $preMessage;?></small>
                </div>
            <?php endif; ?>


        </div>




        <div id="upload-content" class="clrfix">
            <div id="multi_uploader" style="overflow: hidden;"></div>
        </div><!--#upload-content-->

        <div id="upload-footer" style="display: none; padding: 10px 0">
            <!-- <a class="btn add-more" href="javascript: void(0); "><?php echo JText::_('COM_COMMUNITY_PHOTOS_ADD_MORE_PHOTOS'); ?></a> -->
            <a class="btn btn-primary" href="javascript: void(0);" id="album_link"><?php echo JText::_('COM_COMMUNITY_UPLOAD_VIEW_ALBUM'); ?></a>
            <!-- 		<a href="javascript: void(0);" class="btn" onclick="cWindowHide();" >
			<?php echo JText::_('COM_COMMUNITY_CLOSE_BUTTON'); ?></a> -->
            <!-- <div id="photoUploadedCounter"></div> -->
        </div><!--#upload-footer-->
    </div><!--#photo-uploader-->
<?php } ?>
