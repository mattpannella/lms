<?php

defined('_JEXEC') or die();

$avatars = AxsAvatars::getAvatars();
?>

<style>
    .gallery-container {
        padding: 2px;
        border: 1px solid #333;
        margin: 5px 0;

        border-radius: 10px;
    }

    .gallery {
        overflow-y: auto;
        display: flex;
        flex-flow: row wrap;
        overflow-y: scroll !important;
        max-height: 282px !important;
    }

    .gallery img {
        align-items: flex-start;
        cursor: pointer;
    }

    .info-message {
        color: green;
    }

    .error-message {
        color: red;
    }
</style>

<link rel="stylesheet" href="/administrator/components/com_axs/assets/css/images.css" type="text/css">
<link rel="stylesheet" href="/components/com_axs/assets/css/avatar.css" type="text/css">

<script src='/components/com_axs/assets/js/avatar.js' type="text/javascript"></script>

<div>
    <?php if(!empty($avatars)) : ?>

        <div class="gallery-container">
            <div class="gallery">
                <?php foreach($avatars as $avatar) : ?>

                    <img data-avatar-id="<?php echo $avatar->id; ?>" class="img-thumbnail small-thumbnail avatar" src="<?php echo '/' . $avatar->path; ?>" />
                <?php endforeach; ?>
            </div>

            <div class="input-container">

                <hr class="joms-divider">

                <button id="setAvatar" data-avatar-post-upload-action="<?php echo $successRedirect; ?>" class="btn btn-primary">Select Avatar</button>

                <div id="responseMessage"></div>
            </div>
        </div>
    <?php else: ?>
        <h4><strong>There are no avatars designated for this brand.</strong></h4>
    <?php endif; ?>
</div>