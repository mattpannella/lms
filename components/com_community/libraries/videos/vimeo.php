<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/

defined('_JEXEC') or die('Restricted access');

require_once (COMMUNITY_COM_PATH.'/models/videos.php');

/**
 * Class to manipulate data from vimeo video
 *
 * @access	public
 */
class CTableVideoVimeo extends CVideoProvider
{
	var $xmlContent = null;
	var $url 		= '';
	var $videoId	= '';
	/**
	 * Return feedUrl of the video
	 */
	public function getFeedUrl()
	{	
		$url = 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' .$this->videoId;
		$unlisted_hash = $this->getUnlistedHash();
		if (!empty($unlisted_hash)) {
			$url .= '/' . $unlisted_hash;
		}
		return $url;
	}

	/*
	 * Return true if successfully connect to remote video provider
	 * and the video is valid
	 */
	public function isValid()
	{
		if ( !parent::isValid())
		{
			return false;
		}

		// In this case, xmlContent is actually JSON because the url used in 
		// getFeedUrl returns json
		$data = json_decode($this->xmlContent);

		// If the video wasn't found, short circuit
		if( $data == null )
		{
			// $this->setError( JText::_('COM_COMMUNITY_VIDEOS_FETCHING_VIDEO_ERROR') );
			return false;
		}

		//get Video title
		$this->title = (string)$data->title;

		//Get Video duration
		$this->duration = (int)$data->duration;

		//Get Video thumbnail
		$this->thumbnail = (string)$data->thumbnail_url;

		//Get Video description
		$this->description = (string)$data->description;

		return true;
	}

	/**
	 * Extract Vimeo video id from the video url submitted by the user
	 *
	 * @return string
	 */
	public function getId()
	{	
		$url_parts = explode('/', $this->url);
		return  isset($url_parts[3]) ? $url_parts[3] : '';
	} 

	/**
	 * Extract optional Vimeo unlisted hash which is included with any
	 * unlisted videos
	 *
	 * @return string
	 */
	public function getUnlistedHash()
	{
		$url_parts = explode('/', $this->url);
		return isset($url_parts[4]) ? $url_parts[4] : '';
	}
	/**
	 * Return the video provider's name
	 *
	 */
	public function getType()
	{
		return 'vimeo';
	}

	public function getTitle()
	{
		$title	= '';
		$title	= $this->title;

		return $title;
	}

	/**
	 *
	 * @param $videoId
	 * @return unknown_type
	 */
	public function getDescription()
	{
		$description	= '';
		$description = $this->description;
		return $description;
	}

	public function getDuration()
	{
		$duration	= '';
		$duration	= $this->duration;

		return $duration;
	}

	/**
	 *
	 * @param $videoId
	 * @return unknown_type
	 */
	public function getThumbnail()
	{
		$thumbnail	= '';
		$thumbnail	= $this->thumbnail;

		return CVideosHelper::getIURL($thumbnail);
	}

	/**
	 *
	 *
	 * @return $embedCode specific embeded code to play the video
	 */
	public function getViewHTML($videoId, $videoWidth, $videoHeight)
	{
		if (!$videoId)
		{
			$videoId	= $this->videoId;
		}

		$embedCode = '<iframe src="'.CVideosHelper::getIURL('http://player.vimeo.com/video/' . $videoId ).'" width="' . $videoWidth . '" height="' . $videoHeight . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

        return $embedCode;
	}
}
