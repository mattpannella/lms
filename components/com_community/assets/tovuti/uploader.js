/**
 * Tovupload v1.0
 *
 * Internal Tovuti custom uploader tool
 *
 * (c)2021 Tovuti
 * Author: Justin Howe
 */
var tovupload = {

    Uploader: function(options) {
        var self = this;

        // Bind the options to the tovupload object
        self.settings = options;

        console.log("Options: ", self.settings);

        // bind and init prototypes - needed to make the machinery calling this script work w/o breaking execution
        self.bind = function(funcName, functionDefinition) {

            // Bind the function name with the given function definition to this object
            self[funcName] = functionDefinition;
        };

        self.init = function() {

            // Hide error message if it is shown
            $('.joms-js--avatar-uploader-error').hide();

            // This will obtain the data from the upload field and send it to the backend.
            self.uploadHandler();
        };

        self.start = function() {};
        self.refresh = function() {};

        self.uploadHandler = function() {
            var self = this;
            var fileInputElement = $('#' + self.settings.browse_button).find('input[type=file]');

            if(fileInputElement.length == 0) {
                // Element doesn't exist, so create it
                let multipleProp = self.settings.multiple_files ? " multiple " : "";
                let fileUploader = "<input type='file' name='file_uploader' " + multipleProp + " />";

                $('#' + self.settings.browse_button).append(fileUploader);
            }

            fileInputElement = $('#' + self.settings.browse_button).find('input[type=file]');

            // We want to apply any filetype filters provided by the client code.
            if(self.settings.filters != null) {
                
                // Break out the filters into a comma separated list, prefixed with . (.jpg, .jpeg, etc.)
                let filteredExtensions = (self.settings.filters[0].extensions || "");

                let formattedExtensions = filteredExtensions.split(',').map(ext => {
                    return "." + ext;
                }).join(', ');

                fileInputElement.attr('accept', formattedExtensions);
            }
            
            $(fileInputElement).on('change', function(event) {

                if(self.BeforeUpload != undefined) {
                    self.BeforeUpload(self);
                }

                tovupload.uploadFiles(self, this.files, self.settings.url);
            });
        };
    },

    uploadFiles: function(uploader, files, destination) {

        if(files != undefined && files.length > 0) {

            // Now we want to send this information to the backend
            var fileData = new FormData();

            // Parse the files selected in the uploader
            for(let fileIndex = 0; fileIndex < files.length; fileIndex++) {

                let file = files.item(fileIndex);
                let fileUploaderName = uploader.settings.file_data_name || 'file';

                fileData.append(fileUploaderName, file, file.name);
            }

            // Fix the URL if it has crazy stuff like '&amp;', etc.
            destination = destination.replace(/&amp;/g, '&');

            $.ajax({
                type: 'POST',
                url: destination,
                data: fileData,
                contentType: false,
                processData: false,
                cache: false,

                success: function(data, status, xhr) {

                    let uploadedFiles = JSON.parse(data);
                    var processedFiles;

                    // Hide any error message that may be displayed before uploading new files
                    $('.joms-js--avatar-uploader-error').hide();

                    if(uploadedFiles != undefined) {
                        
                        if(uploadedFiles.error != 'true') {

                            // Client code usually works with arrays, so make sure it gets an array to work with
                            processedFiles = Array.isArray(uploadedFiles) ? uploadedFiles : [uploadedFiles];

                            // Invoke the FilesAdded event handler for the files that have been added to the upload set
                            uploader.FilesAdded(uploader, processedFiles);

                            // Pass off the resulting photo objects to the FileUploaded event handler
                            processedFiles.forEach(function(fileData) {

                                uploader.FileUploaded(uploader, fileData, {

                                    response: JSON.stringify(fileData),
                                    status: xhr.status
                                });
                            });
                        } else {

                            // Display the error message
                            let errorElement = $('.joms-js--avatar-uploader-error');

                            errorElement.html("<span style='color: black;'>" + uploadedFiles.msg + "</span>");
                            errorElement.show();
                        }
                    }
                }
            });
        }
    },

    // This needs to be here to stop errors from breaking JS if there's ever an issue uploading
    stop: function() {},

    // noop - exists to prevent JS from breaking execution
    refresh: function() {},

    // noop - exists to prevent JS from breaking execution
    start: function() {}
}