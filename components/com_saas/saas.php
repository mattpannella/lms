<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

//JError::raiseError ('401', 'No access');
//return;

$view = JFactory::getApplication()->input->get('view');

//These are admin views that need to be blocked from the front end.
switch ($view) {
	case "app":
	case "apps":
	case "server":
	case "servers":
	case "site":
	case "sites":
		JError::raiseError('401', 'No access');
		return;
		break;
}

// Load FOF
include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
    JError::raiseError ('500', 'FOF is not installed');
    return;
}

FOFDispatcher::getTmpInstance('com_saas')->dispatch();