function rotate_pic(img, deg) {

    if (deg == 0 || deg == 360) {
        return;
    }

    var image = $("." + img);

    var rotate = "rotate(" + deg + "deg)";

    //alert(image.is(":visible"));

    var x = function() {
        if (!image.is(":visible")) {            
            setTimeout(x, 50);
        } else {
            var width = image.innerWidth();
            var height = image.innerHeight();
            image.css(
                {
                    '-webkit-transform': rotate,
                    '-moz-transform': rotate,
                    '-o-transform': rotate,
                    '-ms-transform': rotate,
                    'transform': rotate
                }
            );
            image.width(height);
            image.height(width);
            //alert(image.width() + " " + image.height() + "\n" + image.innerWidth() + " " + image.innerHeight() + "\n" + image.outerWidth() + " " + image.outerHeight());
        }
    }

    x();
}