/*

    Issues
        If localStorage is empty, all values come on.
        Changing all/none buttons in title bar to 1 turn them on
        being on the declaration and gratitude board at the same time make both pages continually reload

                Store it in a local variable
*/

var current_board = null;
var base_url = null;

function pulse_init() {
    create_pulse();

    setCurrentUrl("#");

    setInterval(
        function() {

            var curPage = getCurrentUrl();                  //Get the url stored in memory
            var curHash = window.location.hash;             //Get the current hash

            //If the current hash is empty, it is the first time a viewer has been to the page.
            if (curHash == "") {
                var newUrl = makeUrl();             //Make a new URL based on the current settings.
                if (newUrl != "") {                 //If a url was able to be made:
                    setUrl();                       //Put the url in the browser bar
                    setCurrentUrl(newUrl);          //Save it to localStorage
                } else {                            //If the url is empty
                    setCurrentUrl("");              //erase the current url from localStorage
                    return;                         //exit the function.
                }

                set_pagination_dropdown();
                refresh_search(getLimit(), getPage(), getSearchSettings(), getTeam());         //Populate the page since it is their first visit.
                return;
            }

            //Only update the url if it has changed to avoid duplicates
            if (curPage != curHash) {   
                var parseSearch = function(value) {
                    for (i = 0; i < value.length; i++) {
                        var num = parseInt(value[i]);

                        if (num == 1) {
                            selected[values[i]] = true;
                            //localStorage.setItem("search_" + values[i], 'true');
                        } else {
                            selected[values[i]] = false;
                            //localStorage.setItem("search_" + values[i], 'false');
                        }

                        changeButton(values[i]);
                    }
                }

                var info = curHash.split('#');
                //console.log(info);

                for (let i = 0; i < info.length; i++) {
                    setting = info[i].split('=');
                    
                    switch (setting[0]) {
                        case "page":
                            setPage(setting[1]);
                            break;
                        case "limit":
                            setLimit(setting[1]);
                            break;
                        case "search":
                            parseSearch(setting[1]);
                            break;
                        case "team":
                            setTeam(setting[1]);
                            break;
                    }
                }                

                set_pagination_dropdown();
                refresh_search(getLimit(), getPage(), getSearchSettings(), getTeam());
                setCurrentUrl(curHash);
            } 
        }, 250
    );

    var pulseGraph = document.getElementById('pulse_display');
    var pulseHeart = document.getElementById('ginHeart');
    if (pulseHeart && pulseGraph) {
        pulseHeart.appendChild(pulseGraph);
    }
}

function set_pagination_dropdown() {
    var limit = getLimit();

    $("select#pagination_amount option").each(
        function() {
            this.selected = (this.value == limit);
        }
    );
}

function set_current_board(board_id) {
    current_board = "board_ " + board_id;    
}

//Leave all_search and none_search at the end of the list.
//Later checks iterate through all values except the last two

var buttons = [];
var selected = [];
var values = [];
var categories;
var category_keys;

function setCategories(cat) {
    
    categories = JSON.parse(cat);
    category_keys = Object.keys(categories);

    for(i = 0; i < category_keys.length; i++) {
        var num = category_keys[i];
        values.push(categories[num].name);
    }

    values.push("friends");
    values.push("all_search");
    values.push("none_search");
 
}

//Check if they're clicked or not and set the design accordingly
function changeButton(buttonName) {

    /*var border = 20; //The thickness of the border around the button when clicked

    var currentButton = buttons[buttonName];
    var currentButtonBorder = $("#" + buttonName + "_border");

    currentButtonBorder.css("background", "white");*/

    /*if (selected[buttonName]) {
        currentButton.css("width", currentButton.originalWidth - border);
        currentButton.css("height", currentButton.originalHeight - border);
        currentButton.css("padding-top", currentButton.originalPadding - (border / 2));
        currentButtonBorder.css("padding-top", border / 2);
    } else {
        currentButton.css("width", currentButton.originalWidth);
        currentButton.css("height", currentButton.originalHeight);
        currentButton.css("padding-top", currentButton.originalPadding);
        currentButtonBorder.css("padding-top", 0);
    }*/

    if (selected[buttonName]) {
        $("#" + buttonName + "_checkbox").show(500);
        $("#" + buttonName + "_x").hide(500);
    } else {
        $("#" + buttonName + "_checkbox").hide(500);
        $("#" + buttonName + "_x").show(500);
    }
}

function createButtons() {

    var checkMinFont = function(div) {
        var curfont = parseInt($(div).css('font-size'));
        if (curfont < 16) {
            $(div).css({'font-size':"16px"});
        }
    }

    var names = {};
    for (i = 0; i < category_keys.length; i++) {
        var num = category_keys[i];
        names[categories[num].name] = num;
    }

    var searchButtonArea = document.getElementById("postSort");
    for (i = 0; i < values.length; i++) {
        var newButton = document.createElement('div');
        searchButtonArea.appendChild(newButton);
        newButton.id = values[i];
        newButton.className += " search_button";

        var text;
        var col;
        var icon;

        switch (values[i]) {
            case "friends": 
                text = "Friends Only"; 
                col = "rgb(80,  140, 20)";        //#508C14
                icon = "lizicon-users";
                break;
            case "all_search": 
                text = "Show All"; 
                col = "rgb(255, 0,   0)";         //#FF0000
                icon = "lizicon-plus";
                break;
            case "none_search": 
                text = "Show None"; 
                col = "rgb(32,  32,  32)";        //#202020
                icon = "lizicon-minus";
                break;
            default:
                var cat = categories[names[values[i]]];
                text = cat.text;
                col = cat.color.hex;
                icon = cat.icon;
                break;
        }

        //Checkmark and X for each category + extra for Friends
        if (i < category_keys.length + 1) {
            var checkmarkBox = document.createElement('span');
            newButton.appendChild(checkmarkBox);
            checkmarkBox.id = values[i] + "_checkbox";
            checkmarkBox.className = "lizicon-checkmark";

            var xBox = document.createElement('span');
            newButton.appendChild(xBox);
            xBox.id = values[i] + "_x";
            xBox.className = "lizicon-cross";
        }

        var iconBox = document.createElement('span');
        newButton.appendChild(iconBox);
        iconBox.className = icon;

        var textNode = document.createTextNode(text);
        newButton.appendChild(textNode);
        

        $(iconBox).css({
            "margin": "15px"
        });

        $(checkmarkBox).css({
            "color": "rgb(40, 255, 40)",
            "margin-left": "10px"
        });

        $(xBox).css({
            "color": "rgb(255, 40, 40)",
            "margin-left": "10px"
        });

        $(newButton).css({
            "width": "20%",
            "color": "rgb(255, 255, 255)",
            "font-size": "1.2vw",
            "box-shadow": "2px 2px 3px #555",
            "border-radius": "5px",
            "margin": "10px",
            "background-color": col,
            "padding": "5px",
            "cursor": "pointer",
            "min-width": "250px"
        });

        checkMinFont(newButton);
    }

    $(window).resize(
        function() {
            $(".search_button").each(
                function(which, item) {
                    $(item).css({'font-size': "1.2vw"});
                    checkMinFont(item);
                }
            );
        }
    );
}

function create_pulse() {

    createButtons();

    //Make the buttons and their functionality when clicked
    var makeButton = function(name) {
        return function() {
            buttons[name].click(
                function() {
                    setPage(0);
                    if (name == "all_search") {
                        for (i = 0; i < values.length; i++) {
                            var n = values[i];

                            if (checkSkip(n)) {
                                continue;
                            }

                            selected[n] = true;
                            //localStorage.setItem("search_" + n, true);
                            changeButton(n);
                        }
                    } else if (name == "none_search") {
                        for (i = 0; i < values.length; i++) {
                            var n = values[i];

                            if (checkSkip(n)) {
                                continue;
                            }

                            selected[n] = false;
                            //localStorage.setItem("search_" + n, false);
                            changeButton(n);
                        }
                    } else {
                        selected[name] = !selected[name];
                        //localStorage.setItem("search_" + name, selected[name]);
                        changeButton(name);
                    }
                    setUrl();
                }
            );
        }
    }

    var heartbeat_check = $(".heartbeat_check");
    var sort_posts = $(".sort_posts");

    if (heartbeat_check.val() != null) {
        heartbeat_check.click(function() {
            $('#ginHeart').slideToggle();
            window.dispatchEvent(new Event('resize'));
        });
    }

    if (sort_posts.val() != null) {
        sort_posts.click(function() {
            $('#postSort').slideToggle();
        });
    }

    for (i = 0; i < values.length; i++) {

        var name = values[i];
        //var storage_name = "search_" + name;

        /*
        if (localStorage.getItem(storage_name)) {
            var setting = localStorage.getItem(storage_name);
            if (setting == "true") {
                selected[name] = true;
            } else {
                selected[name] = false;
            }
        } else { 
        */
            if (i < (values.length - 2)) {
                if (name == "friends") {
                    //localStorage.setItem(storage_name, "false");
                    selected[name] = false;
                } else {
                    //localStorage.setItem(storage_name, "true");
                    selected[name] = true;
                }                
            } else {
                selected[name] = false;
            }
        /*
        }
        */

        buttons[name] = $("#" + name);
        //buttons[name].originalWidth = parseInt(buttons[name].css("width"));
        //buttons[name].originalHeight = parseInt(buttons[name].css("height"));
        //buttons[name].originalPadding = parseInt(buttons[name].css("padding-top"));

        changeButton(name);
        makeButton(name)();
    }

    //$("#pagination_set").click(
    $("#pagination_amount").change(
        function() {
            setPage(0);
            setLimit($("#pagination_amount").val());
            setUrl();
        }
    );

    $("#team_select").change(
        function() {
            setPage(0);
            setTeam($("#team_select").val());
            setUrl();
        }
    );
}

function getSearchSettings() {

    var string = '';
    for (i = 0; i < values.length; i++) {

        //var setting = localStorage.getItem("search_" + values[i]);
        var setting = selected[values[i]];
        if (setting) {
            //if (setting == 'true') {
            if (setting == true) {
                string += values[i] + ',';
            }
        }
    }
    string = string.substr(0, string.length - 1);

    return string;
}

function checkSkip(name) {
    if (
        name == "friends" ||
        name == "all_search" ||
        name == "none_search"
    ) {
        return true;
    }

    return false;
}

/*

        Creates the pagination buttons and arrows

        section = which section the display is for (ie. declaration)
        post_count = how many posts there are total

*/

function make_pagination(post_count) {

    var current_page = parseInt(getPage());

    var limit = $("#pagination_amount").val();
    var num_pages = Math.ceil(post_count / limit);

    setPageLimit(num_pages);

    for (b = 0; b < 2; b++) {

        var button_area;

        if (b == 0) {
            button_area = document.getElementById("pagination_buttons");
        } else if (b == 1) {
            button_area = document.getElementById("pagination_buttons_bottom");
        }

        if (button_area == null) {
            continue;
        }

        if (button_area != null) {
            button_area.className = "pagination";
        }

        $(button_area).html("");

        button_list = document.createElement("div");
        button_list.className = "pagination-list";
        button_area.appendChild(button_list);

        //Don't show the navigation buttons if there is only one page.
        if (num_pages <= 1) {
            $(button_area).hide();
            continue;
        } else {
            $(button_area).show();
        }

        //$(button_area).css('margin-top', '10px');

        var make_button = function(className) {
            var button = document.createElement("a");
            if (className != '') {
                button.className = className;
            }

            return button;
        }

        //Make the '|<'' button to return to the beginning
        var start_button = make_button("pagenav lizicon-first");
        start_button.title = "First Page";
        $(start_button).click(
            function() {
                changePage(0);
            }
        );

        //Make the '<<' button to back up one page
        var back_button = make_button("pagenav lizicon-backward2");
        back_button.title = "Previous page";
        $(back_button).click(
            function() {
                changePage(current_page - 1);
            }
        );

        //If the current page is the first, gray out the back butons
        if (current_page == 0) {
            $(start_button).css("background-color", "rgb(80,80,80)");
            $(start_button).css("border-left", "1px solid white");
            $(back_button).css("background-color", "rgb(80,80,80)");
        }

        //Attack the two buttons.
        button_list.appendChild(start_button);
        button_list.appendChild(back_button);

        //Create all of the numbered buttons for each page
        for (i = 0; i < num_pages; i++) {
            var button_div = make_button("pagenav");
            button_div.textContent = (i + 1);
            button_div.title = "Page: " + (i + 1);

            //Highlight the currently selected page
            if (i == current_page) {
                $(button_div).css(
                    'background-color', 'rgb(200, 200, 200)'
                );
            }

            //Set the click to change the page
            var setClick = function(id, i) {
                return function() {
                    $(button_div).click(
                        function() {
                            changePage(i);
                        }
                    );
                }
            }

            //Attach the button and activate it
            button_list.appendChild(button_div);
            setClick(button_div.id, i)();
        }

        //Create the '>>' button to move forward one page
        var next_button = make_button("pagenav lizicon-forward3");
        next_button.title = "Next Page";
        $(next_button).click(
            function() {
                changePage(current_page + 1);
            }
        );

        //Create the '>|' button to go to the last page
        var last_button = make_button("pagenav lizicon-last");
        last_button.title = "Last Page";
        $(last_button).click(
            function() {
                changePage(num_pages - 1);
            }
        );

        //If the page is the last, gray out the foward buttons
        if (current_page == num_pages - 1) {
            $(next_button).css("background-color", "rgb(80,80,80)");
            $(last_button).css("background-color", "rgb(80,80,80)");
        }

        //Attach the forward buttons
        button_list.appendChild(next_button);
        button_list.appendChild(last_button);
    }
}

/*
    Change the page to whichPage
*/
function changePage(whichPage) {
    if (!whichPage) {
        whichPage = 0;
    }

    var num_pages = getPageLimit();

    if (whichPage < 0) {
        return;
    }

    if (whichPage > num_pages - 1) {
        return;
    }

    setPage(whichPage);
    setUrl();    
}

//Creates a new URL to display in the task bar
function makeUrl() {
    //var url = current_board;

    

    if (!base_url) {
        base_url = window.location.href + window.location.search;
        var parts = base_url.split("#");    //If the URL already has # in it, get rid of them or they'll stack.
        base_url = parts[0];
    }
    
    url = base_url;

    if (!url) {
        return "";
    }

    url += "#page=" + getPage();
    url += "#limit=" + getLimit();
    url += "#search=" + getSearchString();
    url += "#team=" + getTeam();

    return url;
}

//Makes and updates the url
function setUrl() {
    
    var curPage = getCurrentUrl();
    var url = makeUrl();    

    if (url == "") {
        return false;
    }

    if (curPage != url) {
        ChangeUrl(current_board + " page " + getPage(), url);
        setCurrentUrl(url);
    }
}

/*
    creates a string of 1s and 0s to represent which search settings are turned on/off
*/
function getSearchString() {
    var string = '';
    for (i = 0; i < values.length; i++) {
        /*
        if (localStorage.getItem("search_" + values[i]) == 'true') {
            string += "1";
        } else {
            string += "0";
        }
        */
        if (selected[values[i]] == true) {
            string += "1";
        } else {
            string += "0";
        }
    }
    return string;
}

/*
    Gets / sets the limit to how many posts can be shown at once
    Set by the drop down menu.
*/
function getLimit() {
    var item = current_board + "_limit";
    var limit = localStorage.getItem(item);
    if (limit == null) {
        limit = 50;
        localStorage.setItem(item, limit);
    }

    return limit;
}

function setLimit(newLimit) {    
    var item = current_board + "_limit";
    localStorage.setItem(item, newLimit);
}

/*
    Gets/Sets how many pages there are available.
    This is set by how many posts there are divided by
    the limit of posts per page.

    ie.
    171 posts
    20 limit

    171 / 20 = 9 Page Limit

*/
function getPageLimit() {
    var pageLimit = localStorage.getItem(current_board + "_page_limit");
    if (pageLimit == null) {
        pageLimit = 0;
        localStorage.setItem(current_board + "_page_limit", pageLimit);
    }

    return pageLimit;
}

function setPageLimit(newPageLimit) {
    localStorage.setItem(current_board + "_page_limit", newPageLimit);
}

        /*function getLanguage() {
            var language = localStorage.getItem("language");
            if (!language) {
                language = 'english';
                localStorage.setItem("language", language);
            }

            return language;
        }

        function setLanguage(newLanguage) {
            localStorage.setItem("language", newLanguage);
        }*/

/*
    Gets / Sets the current page
    Set by clicking on the desired page number / arrow buttons.
*/
function getPage() {
    var item = current_board + "_page";
    var page = localStorage.getItem(item);
    if (page == null) {
        page = 0;
        localStorage.setItem(item, page);
    }

    return page;
}

function setPage(newPage) {
    var item = current_board + "_page";
    localStorage.setItem(item, newPage);    
}

function getTeam() {
    var item = current_board + "_team";
    var team = localStorage.getItem(item);
    if (team == null) {
        team = "all";
        localStorage.setItem(item, team);
    }

    return team;
}

function setTeam(newTeam) {
    var item = current_board + "_team";
    localStorage.setItem(item, newTeam);
}

/*
    Gets / Sets what the current url is to storage.
    Used to compare against the url in the task bar to see if it has changed.
*/

function getCurrentUrl() {
    var item = "current_" + current_board + "_url";
    var currentPage = localStorage.getItem(item);

    if (currentPage == null) {
        currentPage = makeUrl();
        localStorage.setItem(item, currentPage);
    }

    return currentPage;
}

function setCurrentUrl(newUrl) {    
    var item = "current_" + current_board + "_url";
    localStorage.setItem(item, newUrl);
}
