function set_rating(data) {

	jQuery.ajax({
        type: 'POST',
        url:    '/index.php?option=com_axs&task=ratings.updateRatings&format=raw',
        data: {            
            data: data
        },
        success: function(response) {
            var data = JSON.parse(response);
            jQuery("#rating_container_" + data.rating_id).html(data.html);
        }
    });
}

function highlight_rating(rating_id, rating) {
	jQuery("#topic_rating_" + rating_id).hide();
	for (i = 1; i <= 5; i++) {
		jQuery("#topic_rating_user_" + rating_id + "_" + i).hide();	
	}
	jQuery("#topic_rating_user_" + rating_id + "_" + rating).show();
}

function clear_rating_hover(rating_id) {

	for (i = 1; i <= 5; i++) {
		jQuery("#topic_rating_user_" + rating_id + "_" + i).hide();
	}

	jQuery("#topic_rating_" + rating_id).show();
}