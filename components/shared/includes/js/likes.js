function like_comment(like_id) {

    $.ajax({
        type: 'POST',
        url: '/index.php?option=com_axs&task=likes.setLike&format=raw',
        data: {            
            lid : like_id
        },
        success: function(response) {            
            $('#like_button_' + like_id).html(response);
        }
    });
}