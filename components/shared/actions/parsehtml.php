<?PHP
defined('_JEXEC') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$comPath = "components/shared/";
include_once $comPath . 'controllers/common.php';

$date =  strtotime('now');
$db = getDatabase();


if($_GET['go'] == 1) {
	$query = $db->getQuery(true);
	 $query
        ->select('*')
        ->from($db->quoteName('axs_gin_dev_series'))
		->order('id ASC');
        
    
    $db->setQuery($query);
    $data = $db->loadObjectList();
	echo count($data);

    foreach ($data as $row) {
        $id = $row->id;
		$title = $row->title;
		$author = $row->author;
		$description = $row->description;
		$source = $row->source;
		$date = $row->date;
		$access = $row->access;
   
	
	
		$query2 = $db->getQuery(true);
		 
		  $columns = array(
            'id',
			'ordering', 
            'title',
            'speaker',
			'description',
			'media_type',
            'category',
			'subcategory',
			'ad_source',
			'ad_link',
			'brand',
			'featured',
			'featured_home',
			'playlist',
			'source',
			'placeholder_image',			
			'access',
			'resources',
			'go_live_date',
			'archive_date',
			'created_on',
			'columns',
			'tags'
        );
        
        $values = array(
            (int)'',
			(int)$id,
            $db->quote($title),
            $db->quote($author),
			$db->quote($description),
			$db->quote('audio'),
			(int)8,
			(int)'',
			$db->quote(''),
			$db->quote(''),
			$db->quote('gin'),
			$db->quote(''),
			$db->quote(''),
			$db->quote(''),
			$db->quote($source),
			$db->quote(''),
            (int)'10',
			$db->quote(''),
			$db->quote(''),
			$db->quote(''),
			$db->quote(''),
			(int)'',
			$db->quote('')
        );

        $query2
            ->insert($db->quoteName('axs_gin_media'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query2);
		 $db->execute();
		
	}
	
	
} else {

include 'simple_html_dom.php';
 $url = "http://www.allaxs.com/devseries.html";
 $html = file_get_html($url);
  
  
    $content = $html->find('tr');
	//$content = $html->find('tr');
	// $content2 = $html->find('tr > tr');
   
  // $content = array_merge($content, $content2);
      //$content->find('a.openevent', 0)->innertext = '';
      //$content->find('h3.lshtitle', 0)->onclick = '';
      //$content->find('h3.lshtitle', 0)->tag = 'div';
     // $content->find('div.lshtitle', 0)->class = 'ttl';
	 $i = 0;
	 $array = array();
	 $array2 = array();
	 foreach ($content as $item) {
		 
		 $heading = $item->find('td.tableheading > strong');
		 $count1 = count($heading);
		 if ($count1 > 0) {
		
		 $headingNew = $heading[0]->innertext;
		 $name = $item->find('td.tableheading');
		 $name = preg_replace('/<strong\b[^>]*>(.*?)<\/strong>/i', '', $name);
		 $name = strip_tags($name[0],'<br/>');
		
		  $query = $db->getQuery(true);
		 
		  $columns = array(
            'id', 
            'title',
            'author',
			'description',
			'file',
            'date',
			'access'
        );
        
        $values = array(
            (int)'',
            $db->quote($headingNew),
            $db->quote($name),
			$db->quote(''),
			$db->quote(''),
            (int)$date,
			(int)'10'
        );

        $query
            ->insert($db->quoteName('axs_gin_dev_series'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
		 $db->execute();
		 }
		 
		 $href = $item->find("a");
		 $count = count($href);
		 if ($count > 0) {
			 
		$description = $item->find('td');
		$description = preg_replace('/<strong\b[^>]*>(.*?)<\/strong>/i', '', $description);	
		$description = preg_replace('/<td colspan="3" valign="middle" align="left" height="40">/i', '', $description);
		$description = preg_replace('/<\/td>/i', '', $description);  
		$description = preg_replace('/<a\b[^>]*>(.*?)<\/a>/i', '', $description);
		$descriptionNEW = $description[0];
		
		 $file = $href[0]->getAttribute("href");
		 $query = $db->getQuery(true);
    
    $query
        ->select('id')
        ->from($db->quoteName('axs_gin_webinars'))
		->order('id DESC')
         ->setLimit(1); 
    
    $db->setQuery($query);
    $data = $db->loadObjectList();
    
    if ($data != null) {
        $id = $data[0]->id;
    }
		$query2 = $db->getQuery(true);
		$conditions = array(
            $db->quoteName('id') . '=' . (int)$id
        );

        $fields = array(
            $db->quoteName('file') . '=' . $db->quote($file),
			$db->quoteName('description') . '=' . $db->quote($descriptionNEW)
        );
        
        $query2
            ->update($db->quoteName('axs_gin_webinars'))
            ->set($fields)
            ->where($conditions);
    
        $db->setQuery($query2);
  		$result = $db->execute();
		 }
		
		 
		
	 }
	
}
	 
?>