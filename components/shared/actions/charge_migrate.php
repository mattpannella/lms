<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 1/13/16
 * Time: 9:28 AM
 */

defined('_JEXEC') or die;

class Config{
    private static $dbServer = "localhost";
    private static $dbUsername = "liz_oldgin";
    private static $dbPassword = "Oregon11";

    public static function getDBConnection($dbName){
        $dbConnection = new mysqli(
            self::$dbServer,
            self::$dbUsername,
            self::$dbPassword,
            $dbName
        );
        return $dbConnection;
    }
}

$total = 0;
$success = 0;
$error = array();
$conn = Config::getDBConnection('liz_oldschoolgin');
$sql = "SELECT * FROM ChargeDate;";
$response = $conn->query($sql);
$conn->close();

While($row = $response->fetch_array(MYSQLI_ASSOC)) {
    $total++;

    $uid = $row['MemberId'];
    $date = $row['Date'];
    $amount = $row['Amount'];
    if($amount == '30') {
        //family member

    } else {
        //primary account
        //now save a first transaction so that the cc_cron job will charge them
        $db = JFactory::getDbo();
        $cols = array(
            'trxn_id',
            'user_id',
            'type',
            'item_id',
            'item',
            'amount',
            'date',
            'card',
            'next_payment_date',
            'status'
        );
        $vals = array(
            $db->q('first-charge-placeholder'),
            $uid,
            1, //recurring
            6, //id of the plan
            $db->q('GIN Member'),
            150,
            $db->q(date("Y-m-d H:i:s")),
            $lastFour,
            $db->q($nextChargeDate),
            $db->q('STL') //set it to settled because this is just a placeholder
        );
        $query = $db->getQuery(true);
        $query->insert($db->qn('cctransactions'))
            ->columns($db->qn($cols))
            ->values(implode(',', $vals));
        $db->setQuery($query);
        $db->execute();

        //they should already have access to the right courses based on their level, but they need a subscription
        $db = JFactory::getDbo();
        $cols = array(
            'plan_id',
            'user_id',
            'first_name',
            'last_name',
            'address',
            'city',
            'state',
            'zip',
            'country',
            'created_date',
            'payment_date',
            'from_date',
            'to_date',
            'published',
            'amount',
            'gross_amount',
            'subscription_code',
            'payment_method',
            'transaction_id',
            'act',
            'params',
            'is_profile',
            'invoice_number',
            'profile_id',
            'language'
        );
        $vals = array(
            6,
            $userid,
            $db->q($fname),
            $db->q($lname),
            $db->q($address),
            $db->q($city),
            $db->q($state),
            $db->q($zip),
            $db->q($country),
            $db->q(date("Y-m-d H:i:s")),
            $db->q(date("Y-m-d H:i:s")),
            $db->q(date("Y-m-d H:i:s")),
            $db->q($nextChargeDate),
            1,
            150,
            150,
            $db->q('placeholder'),
            $db->q('os_converge_axs'),
            $db->q('placeholder'),
            $db->q('subscribe'),
            $db->q('{"regular_amount":150,"regular_discount_amount":0,"regular_tax_amount":0,"regular_payment_processing_fee":0,"regular_gross_amount":150}'),
            1,
            1,
            162,
            $db->q('en-GB')

        );
        $query = $db->getQuery(true);
        $query->insert($db->qn('#__osmembership_subscribers'))
            ->columns($db->qn($cols))
            ->values(implode(',', $vals));
        $db->setQuery($query);
        $db->execute();
    }

    $success++;
}

echo 'Attempted to migrate: '.$total.' total Payments.<br><br>Successfully migrated: '.$success.' Payments.<br><br>Error with the following Payments: ';
print_r($error);