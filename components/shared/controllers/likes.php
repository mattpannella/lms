<?php 
defined( '_JEXEC' ) or die( 'Restricted access' );

?>

<script type="text/javascript" src="/components/shared/includes/js/likes.js"></script>
<link rel="stylesheet" type="text/css" href="components/shared/includes/css/likes.css">

<?php

function like_button($like_id) {  
    ?>
        <div id="like_button_<?php echo($like_id); ?>" class="like_button">
            <?php create_like_button($like_id); ?>
        </div>
    <?php
}

function create_like_button($like_id) {
    $user_like = check_user_like($like_id);
    $num_likes = get_num_likes($like_id);
    
    $class = "words like_button_css lizicon";    
    
    if ($user_like != null) {
        //They've already liked this.  Option = UNLIKE
        $class .= " icon_text_blue lizicon-thumb";
        $title = "Unlike this post";
        $like_text = "Unlike";
    } else {        
        //They haven't liked this yet.  Option = LIKE
        $class .= " icon_text_grey lizicon-thumb";
        $title = "Like this post!";
        $like_text = "Like!";
    }
?>
    <span id="like_button" 
        class="<?php echo $class ?>" 
        title="<?php echo $title ?>" 
        onclick="like_comment(<?php echo "'" . $like_id . "'" ; ?>)"
    >
        <?php echo $like_text ?>
        (<?php 
            echo $num_likes;
            if ($num_likes == 1) {
                echo " like";
            } else {
                echo " likes";
            }
        ?>)
    </span>
<?php
}

function check_user_like($like_id) {
    $userId = JFactory::getUser()->id;
    $db = JFactory::getDBO();

    $query = $db->getQuery(true);
    
    $conditions = array(
        $db->quoteName('like_id') . '=' . $db->quote($like_id),
        $db->quoteName('user_id') . '=' . (int)$userId
    );
    
    $query
        ->select('*')
        ->from($db->quoteName('likes'))
        ->where($conditions);
    
    $db->setQuery($query);
    $data = $db->loadObjectList();
    
    return $data;
}

function get_num_likes($like_id) {
    $db = JFactory::getDBO();
    $query = $db->getQuery(true);
        
    $conditions = array(
        $db->quoteName('like_id') . '=' . $db->quote($like_id)
    );

    $query
        ->select('*')
        ->from($db->quoteName('likes'))
        ->where($conditions);

    $db->setQuery($query);
    $data = $db->loadObjectList();
        
    return count($data);
}
?>