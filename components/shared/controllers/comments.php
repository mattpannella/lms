<?php  
    defined( '_JEXEC' ) or die( 'Restricted access' );
    // error_reporting(E_ALL);
    // ini_set('display_errors', 1);

    $module = JModuleHelper::getModule("comments");
    echo JModuleHelper::renderModule($module);

    //function display_comments($title, $button_title, $placeholder, $post_id, $auto_update = true, $refresh_time = 2000, $limit = 10, $loadmore = 5) {
    function display_comments($params) {

        if (!$params) {
            return;
        }

        if ($params->post_id == null) {
            //No post is set.
            return;   
        }

        //Set the params with the defaults if it is not set.
        $template = get_display_comments_params();
        foreach ($template as $key => $val) {
            if (!isset($params->$key)) {
                $params->$key = $val;
            }
        }

        if ($params->auto_update) {
            $params->auto_update = "true";
        } else {
            $params->auto_update = "false";
        }

        //Some validation

        $params->refresh_time = (int)$params->refresh_time;
        $params->initial_limit = (int)$params->initial_limit;
        $params->load_more = (int)$params->load_more;

        if ($params->refresh_time == 0 && $params->auto_update == "true") {
            //A refresh time of 0 will cause the page to lock up.
            $params->refresh_time = $template->refresh_time;
        }

        if ($params->refresh_time < 1000) {
            $params->refresh_time = 1000;
        }

        if ($params->initial_limit < 0) {
            $params->initial_limit = 0;
        }

        if ($params->load_more < 1) {
            $params->load_more = 1;
        }

        ?>
            <div
                id="comments_<?php echo $params->post_id;?>"
                class="comment_module"
                button-title="<?php echo $params->button_title; ?>"
                placeholder="<?php echo $params->placeholder;?>"
                post-id="<?php echo $params->post_id;?>"
                auto-update="<?php echo $params->auto_update;?>"
                title="<?php echo $params->title;?>"
                load-more-button="<?php echo $params->load_more_button;?>"
                refresh-time="<?php echo $params->refresh_time; ?>"
                init-load="<?php echo $params->initial_limit; ?>"
                load-more="<?php echo $params->load_more;?>"
                current-load="0"
                last-load="<?php echo strtotime('now');?>"

            >        
            </div>
        <?php
    }

    function get_display_comments_params() {
        $params = new stdClass();

        $params->button_title = "Submit";
        $params->placeholder = "Your comment";
        $params->auto_update = true;
        $params->title = "Comments";
        $params->post_id = null;
        $params->refresh_time = 5000;
        $params->initial_limit = 5;
        $params->load_more = 3;

        return $params;        
    }