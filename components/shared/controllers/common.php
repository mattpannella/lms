<?php  defined( '_JEXEC' ) or die( 'Restricted access' ); 
 function checkGroup($group, $user_id, $inherited){
     //example use === checkAccess(17, TRUE);
	
	if($inherited){
	//include inherited groups
	jimport( 'joomla.access.access' );
	$groups = JAccess::getGroupsByUser($user_id);
	}else{
	//exclude inherited groups
	$user =& JFactory::getUser($user_id);
	$groups = isset($user->groups) ? $user->groups : array();
								}
	$return = 0;
	if(in_array($group, $groups)){
	$return = true;
	}
	return $return;
}



function ginProfile($profileID) {

	$profile = new stdClass;	

    $db  = JFactory::getDBO();   
    
    $conditions = array(
        $db->quoteName('userid') . '=' . $db->quote($profileID)
    );
    
    $query = $db->getQuery(true);
    $query
        ->select('*')
        ->from($db->quoteName('joom_community_users'))
        ->where($conditions)
        ->setLimit(1);        
        
    $db->setQuery($query);
    $row = $db->loadObject();
    if (is_object($row)) {
        $profile->photo = $row->avatar;
        $profile->alias = $row->alias;        
    }

    if (!file_exists($profile->photo)) {
    	$profile->photo = '/images/user.png';
    }

    $query = $db->getQuery(true);
     $conditions = array(
        $db->quoteName('id') . '=' . $db->quote($profileID)
    );
    $query
        ->select('*')
        ->from($db->quoteName('joom_users'))
        ->where($conditions)
        ->setLimit(1);
    $db->setQuery($query);
    $user = $db->loadObject();
	$profile->email = $user->email;
    
    $conditions = array(
        $db->quoteName('user_id') . '=' . $db->quote($profileID)
        
    );
    
    $query = $db->getQuery(true);
    $query
        ->select('*')
        ->from($db->quoteName('joom_community_fields_values'))
        ->where($conditions);
        
        
    $db->setQuery($query);
    $rows = $db->loadObjectList();

    foreach($rows as $row) {
    	switch ($row->field_id) {
    		case 19:
    			$profile->firstname = $row->value;
    			break;
    		case 20:
    			$profile->lastname = $row->value;
    			break;
    		case 6:
    			$profile->phone = $row->value;
    			break;
    	}
    }

	if($profile->firstname == '') {
		$nameArray = explode(' ', $user->name);
		$profile->firstname = $nameArray[0];
		$profile->lastname = $nameArray[1];
	}

	return $profile;
}

function getGET($which) {
    if (isset($_GET[$which])) {
        return $_GET[$which];
    }
}

function readMore($text, $maxChar) {
    $unencoded_text = html_entity_decode($text, ENT_QUOTES);
	$textCount = strlen($unencoded_text);
	if ($textCount > $maxChar) {
		$trimText = substr($unencoded_text, 0, $maxChar);
		$moreText = substr($unencoded_text, $maxChar);
        
        $return =   $trimText .
                    '<span class="more_text">' . $moreText . '</span>' .
                    '<a class="read_more">' . '</a>';
        return $return;
        
	} else {
	   return $text;	
	}
}

function getFriends($userId) {
    $db = getDatabase();
    
    $conditions = array(
        $db->quoteName('userid') . '=' . $db->quote($userId)
    );
    
    $query = $db->getQuery(true);
    
    $query
        ->select('*')
        ->from($db->quoteName('joom_community_users'))
        ->where($conditions);
        
    $db->setQuery($query);
    $row = $db->loadObject();
    if (is_object($row)) {
        $allFriends = explode(',', $row->friends);
        return $allFriends;
    } else {
        return null;
    }
}

function getDatabase() {
    //$app = JFactory::getApplication();
    //$dbprefix = $app->getCfg('dbprefix');
    $db  = JFactory::getDBO();
    return $db;
}

function getUserId() {
    $user = JFactory::getUser();
    $userId = $user->id;
    return $userId;
}

function getPostUserId($comment_id, $section) {
    $db = getDatabase();
    
    $query = $db->getQuery(true);
    
    $conditions = array(
        $db->quoteName('id') . '=' . $db->quote($comment_id)
    );
    
    $query
        ->select('*')
        ->from('comments')
        ->where($conditions);
        
    $db->setQuery($query);
    $rows = $db->loadObjectList();
    
    if ($rows != null) {
        $row = $rows[0];
        $post_id = $row->post_id;
    }
    
    $query = $db->getQuery(true);
    
    switch ($section) {
        case 'gratitude': $table = "gratitude_board"; break;
        case 'declaration': $table = "declaration_board"; break;
        case 'inspiration': $table = "inspiration_board"; break;
        default:
            return -1;
    }
    
    $conditions = array(
        $db->quoteName('id') . '=' . $db->quote($post_id)
    );
    
    $query
        ->select('*')
        ->from($db->quoteName($table))
        ->where($conditions);
    
    $db->setQuery($query);     
    $rows = $db->loadObjectList();
    
    if ($rows != null) {
        $row = $rows[0];
        $post_user_id = $row->user_id;
        return $post_user_id;
    }
    
    return $query;
}


/*
function getPercentages($section) {    
    
    $db  = JFactory::getDBO();
    
    $percentages = array();
    
    for ($i = 0; $i < 7; $i++) {
        $query = $db->getQuery(true);
        
        switch ($i) {
            case 1: $category = "financial"; break;
            case 2: $category = "health"; break;
            case 3: $category = "family"; break;
            case 4: $category = "business"; break;
            case 5: $category = "personal"; break;
            case 6: $category = "community"; break;
        }
    
        if ($i == 0) {
            $query
                ->select('id')
                ->from($db->quoteName($section.'_board'));

            $db->setQuery($query);
            $db->execute();
            $all_count = $db->getNumRows();
            
        } else {
        
            $query
                ->select('category')
                ->from($db->quoteName($section.'_board'))
                ->where($db->quoteName('category') . '=' . $db->quote($category));

            $db->setQuery($query);
            $db->execute();
            $count = $db->getNumRows();
            
            if ($all_count != 0) {
                $per = round(($count / $all_count) * 100);  
            } else {
                $per = 0;
            }
            
            $percentages[$category] = $per;
        }     
    }
    
    return $percentages;
}
*/

//////////////// Video Player /////////////////

function setVar($videoParams,$variable,$default){
	
	if (isset($videoParams[$variable])) { $output = $videoParams[$variable]; }
	else 
	{ 
	if ($default == '') { $default = getRand(6); }
	$output = $default;  }
	return $output;
}

function getRand($length) {

        $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ";
        $validCharNumber = strlen($validCharacters);
        $result = "";

        for ($i = 0; $i < $length; $i++) {

            $index = mt_rand(0, $validCharNumber - 1);

            $result .= $validCharacters[$index];
        }

        return $result;
    }

     


function getVideo ($videoParams) {
		
		
	
					$instanceName = setVar($videoParams,'instanceName','');
					$parentId = setVar($videoParams,'mainFolderPath','');
					$playlistsId = setVar($videoParams,'playlistsId','');
					$mainFolderPath = setVar($videoParams,'mainFolderPath','/assets/video-player/player/content');
					$skinPath = setVar($videoParams,'skinPath','minimal_skin_dark');
					$displayType="responsive";
					$facebookAppId="213684265480896";
					$autoScale="yes";
					$useDeepLinking="yes";
					$rightClickContextMenu="developer";
					$addKeyboardSupport="yes";
					$stopVideoWhenPlayComplete="no";
					$autoPlay="no";
					$loop="no";
					$shuffle="no";
					$maxWidth=960;
					$maxHeight=599;
					$volume=.8;
					$backgroundColor="#000000";
					$posterBackgroundColor="#000000";
					//logo settings
					$showLogo="yes";
					$hideLogoWithController="yes";
					$logoPosition="topRight";
					$logoLink="http=//www.webdesign-flash.ro";
					$logoMargins=5;
					//playlists / categories settings
					$showPlaylistsButtonAndPlaylists="yes";
					$showPlaylistsByDefault="no";
					$thumbnailSelectedType="opacity";
					$startAtPlaylist=0;
					$buttonsMargins=0;
					$thumbnailMaxWidth=350; 
					$thumbnailMaxHeight=350;
					$horizontalSpaceBetweenThumbnails=40;
					$verticalSpaceBetweenThumbnails=40;
					//playlist settings
					$showPlaylistButtonAndPlaylist="yes";
					$playlistPosition="bottom";
					$showPlaylistByDefault= setVar($videoParams,'showPlaylistByDefault','no');
					$showPlaylistToolTips="yes";
					$forceDisableDownloadButtonForFolder="yes";
					$addMouseWheelSupport="yes";
					$startAtRandomVideo="no";
					$folderVideoLabel="Video ";
					$startAtVideo=0;
					$maxPlaylistItems=50;
					$playlistToolTipMaxWidth=270;
					$thumbnailWidth=120;
					$thumbnailHeight=90;
					$spaceBetweenControlerAndPlaylist=1;
					$playlistToolTipFontColor="#000000";
					//controller settings
					$showButtonsToolTip="yes";
					$showControllerWhenVideoIsStopped="yes";
					$showVolumeScrubber="yes";
					$showVolumeButton="yes";
					$showTime="yes";
					$showLoopButton="yes";
					$showShuffleButton="yes";
					$showYoutubeQualityButton="yes";
					$showDownloadButton="yes";
					$showFacebookButton="no";
					$showEmbedButton="no";
					$showFullScreenButton="yes";
					$repeatBackground="no";
					$buttonsToolTipHideDelay=1.5;
					$controllerHeight=41;
					$controllerHideDelay=3;
					$startSpaceBetweenButtons=7;
					$spaceBetweenButtons=8;
					$mainScrubberOffestTop=14;
					$scrubbersOffsetWidth=2;
					$timeOffsetLeftWidth=5;
					$timeOffsetRightWidth=3;
					$timeOffsetTop=0;
					$volumeScrubberWidth=80;
					$volumeScrubberOffsetRightWidth=0;
					$timeColor="#888888";
					$youtubeQualityButtonNormalColor="#888888";
					$youtubeQualityButtonSelectedColor="#FFFFFF";
					$buttonsToolTipFontColor="#5a5a5a";
					//embed window
					$embedWindowCloseButtonMargins=0;
					$borderColor="#333333";
					$mainLabelsColor="#FFFFFF";
					$secondaryLabelsColor="#a1a1a1";
					$shareAndEmbedTextColor="#5a5a5a";
					$inputBackgroundColor="#000000";
					$inputColor="#FFFFFF";
	$videoPlayer = '
	<script type="text/javascript" src="/assets/video-player/player/java/FWDRVPlayer.js"></script>
		
		<!-- Setup video player-->
		<script type="text/javascript">
			FWDRVPUtils.onReady(function(){

				FWDRVPlayer.useYoutube = "yes";
				
				new FWDRVPlayer({		
					//main settings
					instanceName:"'.$instanceName.'",
					parentId:"'.$parentId.'",
					playlistsId:"'.$playlistsId.'",
					mainFolderPath:"'.$mainFolderPath.'",
					skinPath:"'.$skinPath.'",
					displayType:"responsive",
					facebookAppId:"213684265480896",
					autoScale:"yes",
					useDeepLinking:"yes",
					rightClickContextMenu:"AXS",
					addKeyboardSupport:"yes",
					stopVideoWhenPlayComplete:"no",
					autoPlay:"no",
					loop:"no",
					shuffle:"no",
					maxWidth:1920,
					maxHeight:1080,
					volume:.8,
					backgroundColor:"#000000",
					posterBackgroundColor:"#000000",
					//logo settings
					showLogo:"no",
					hideLogoWithController:"no",
					logoPosition:"topRight",
					logoLink:"/images/logo-sm-fade.png",
					logoMargins:5,
					//playlists / categories settings
					showPlaylistsButtonAndPlaylists:"yes",
					showPlaylistsByDefault:"no",
					thumbnailSelectedType:"opacity",
					startAtPlaylist:0,
					buttonsMargins:0,
					thumbnailMaxWidth:350, 
					thumbnailMaxHeight:350,
					horizontalSpaceBetweenThumbnails:40,
					verticalSpaceBetweenThumbnails:40,
					//playlist settings
					showPlaylistButtonAndPlaylist:"yes",
					playlistPosition:"bottom",
					showPlaylistByDefault:"'.$showPlaylistByDefault.'",
					showPlaylistToolTips:"no",
					forceDisableDownloadButtonForFolder:"yes",
					addMouseWheelSupport:"yes",
					startAtRandomVideo:"no",
					folderVideoLabel:"Video ",
					startAtVideo:0,
					maxPlaylistItems:50,
					playlistToolTipMaxWidth:270,
					thumbnailWidth:120,
					thumbnailHeight:90,
					spaceBetweenControlerAndPlaylist:1,
					playlistToolTipFontColor:"#000000",
					//controller settings
					showButtonsToolTip:"yes",
					showControllerWhenVideoIsStopped:"yes",
					showVolumeScrubber:"yes",
					showVolumeButton:"yes",
					showTime:"yes",
					showLoopButton:"yes",
					showShuffleButton:"yes",
					showYoutubeQualityButton:"yes",
					showDownloadButton:"no",
					showFacebookButton:"no",
					showEmbedButton:"no",
					showFullScreenButton:"yes",
					repeatBackground:"no",
					buttonsToolTipHideDelay:1.5,
					controllerHeight:41,
					controllerHideDelay:3,
					startSpaceBetweenButtons:7,
					spaceBetweenButtons:8,
					mainScrubberOffestTop:14,
					scrubbersOffsetWidth:2,
					timeOffsetLeftWidth:5,
					timeOffsetRightWidth:3,
					timeOffsetTop:0,
					volumeScrubberWidth:80,
					volumeScrubberOffsetRightWidth:0,
					timeColor:"#888888",
					youtubeQualityButtonNormalColor:"#888888",
					youtubeQualityButtonSelectedColor:"#FFFFFF",
					buttonsToolTipFontColor:"#5a5a5a",
					//embed window
					embedWindowCloseButtonMargins:0,
					borderColor:"#333333",
					mainLabelsColor:"#FFFFFF",
					secondaryLabelsColor:"#a1a1a1",
					shareAndEmbedTextColor:"#5a5a5a",
					inputBackgroundColor:"#000000",
					inputColor:"#FFFFFF",
					//ads
					openNewPageAtTheEndOfTheAds:"no",
					playAdsOnlyOnce:"no",
					adsButtonsPosition:"left",
					skipToVideoText:"You can skip to video in: ",
					skipToVideoButtonText:"Skip Ad",
					adsTextNormalColor:"#888888",
					adsTextSelectedColor:"#FFFFFF",
					adsBorderNormalColor:"#666666",
					adsBorderSelectedColor:"#FFFFFF",
					showContextMenu:"no"
				});
			});
		</script>
		


	
	<div class="embed-responsive embed-responsive-16by9">
		<div id="'.$parentId.'" class="embed-responsive-item dashvideo"></div>
	
		<!--  Playlists -->
		<ul id="'.$playlistsId.'" style="display:none;">';
		$videoList = $videoParams["videoPlaylists"];
	foreach ($videoList as $list){
		$videoPlayer .= '<li data-source="playlist1" data-thumbnail-path="assets/video-player/player/content/thumbnails/large1.jpg">';
		if (isset($list["title"])) {
			$videoPlayer .= '<p class="minimalDarkCategoriesTitle"><span class="bold">Title: </span>'.$list["title"].'</p>';
		}
		if (isset($list["description"])) {
			$videoPlayer .= '<p class="minimalDarkCategoriesDescription"><span class="bold">Description: </span>'.$list["description"].'</p>';
		}
			$videoPlayer .= '</li>';
	
	}
			
		
			
			
		$videoPlayer .= '</ul>';
		
		$playlists = $videoParams["videoPlaylists"];
		$videos = $videoParams["videoPlaylists"];
		$count = count($videos);
	
	foreach ($playlists as $key => $val){
		$videoPlayer .= '
		<ul id="'.$key.'" style="display:none;">';
		foreach ($videos[$key]["videos"] as $video){
		$videoPlayer .= '
		
			<li data-thumb-source="'.$video["image"].'" data-video-source="'.$video["url"].'" data-poster-source="'.$video["image"].'" data-downloadable="yes">
				<div>
					<img class="floatLeft" src="'.$video["image"].'" width="120px" height="90px"></img>';
					if (isset($video["description"])) {
					$videoPlayer .= '<p class="floatP">'.$video["description"].'</p>';
					}
				$videoPlayer .= '</div>
			</li>';
		}
			$videoPlayer .= '</ul>';
			
			$videoPlayer .= '<script> '.$instanceName.'.stop();</script>';
			
		
	}
		
		
	$videoPlayer .= '</div>';
	
		
		return $videoPlayer;
}

?>
