<?php

function getCategories($allowed = null) {
	$db = JFactory::getDBO();
	$query = $db->getQuery(true);

	$query
        ->select('*')
        ->from('axs_pulse_categories');

    $db->setQuery($query);
    $return = $db->loadObjectList();

    $categories = array();

    foreach($return as $cat) {

    	if ($cat->disabled == 1) {
    		continue;
    	}

        if ($allowed) {
            if (!in_array($cat->id, $allowed)) {
                continue;
            }
        }

    	$obj = new stdClass();
        $obj->id = $cat->id;
    	$obj->name = $cat->name;
    	$obj->text = Jtext::_($cat->text);
    	$obj->icon = $cat->icon;

        $colors = $cat->color;

        $red = hexdec(substr($colors, 1, 2));
        $green = hexdec(substr($colors, 3, 2));
        $blue = hexdec(substr($colors, 5, 2));

        /*

        $colors = explode(',', $cat->color);

    	switch (count($colors)) {
    		case 3: 
    			$r = $colors[0];
    			$g = $colors[1];
    			$b = $colors[2];
    			$a = 1;

    			$colString = "rgb(" . $r . "," . $g . "," . $b . ")";

    			break;
    		case 4:
    			$r = $colors[0];
    			$g = $colors[1];
    			$b = $colors[2];
    			$a = $colors[3];

    			$colString = "rgba(" . $r . "," . $g . "," . $b . "," . $a . ")";
    			break;
    		default:
    			$r = 255;
    			$g = 255;
    			$b = 255;
    			$a = 1;
    			$colString = "rgb(255,255,255)";
    			break;
    	}

        $color = new stdClass();
    	$color->rgb = $colString;

    		$hex[0] = dechex($r);
    		$hex[1] = dechex($g);
    		$hex[2] = dechex($b);

    		for ($i = 0; $i < 3; $i++) {
    			if (strlen($hex[$i]) == 1) {
	    			$hex[$i] = "0" . $hex[$i];
	    		}
    		}   		


    	$color->hex = "#" . $hex[0] . $hex[1] . $hex[2];
    	$color->r = $r;
    	$color->g = $g;
    	$color->b = $b;
    	$color->a = $a;
        */

        $color = new stdClass();
        $color->rgb = "(" . $red . "," . $green . "," . $blue . ")";
        $color->r = $red;
        $color->g = $green;
        $color->b = $blue;
        $color->a = 1;

        $color->hex = $colors;

    	$obj->color = $color;

    	$categories[$cat->id] = $obj;
    }

    return $categories;
    
}
