<?php
	defined( '_JEXEC' ) or die( 'Restricted access' );

	$document = JFactory::getDocument();
	$document->addStyleSheet("components/shared/includes/css/ratings.css");
	$document->addScript("components/shared/includes/js/rating.js?v=3");

	function display_ratings($rating_id, $hover_array = null, $display_own = true) {

		/*
			Get the POST's current rating
			$current_rating
		*/

		$db = JFactory::getDBO();
		$user_id = JFactory::getUser()->id;
		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('rating_id') . '=' . $db->quote($rating_id)
		);

		$query
			->select('*')
			->from($db->quoteName('ratings_post'))
			->where($conditions);

		$db->setQuery($query);
		$results = $db->loadObjectList();

		if ($results) {

			$result = $results[0];
			$total_votes = $result->stars1 + $result->stars2 + $result->stars3 + $result->stars4 + $result->stars5;
			$total_score = (1 * $result->stars1) + (2 * $result->stars2) + (3 * $result->stars3) + (4 * $result->stars4) + (5 * $result->stars5);
			$current_rating = $total_score / $total_votes;
		} else {
			$current_rating = 0;
		}

		/*
			Get the USER's rating for the post, if they are logged in.
			$user_rating
		*/

		if ($user_id) {
			$query = $db->getQuery(true);

			$conditions = array(
				$db->quoteName('user_id') . '=' . $db->quote($user_id),
				$db->quoteName('rating_id') . '=' . $db->quote($rating_id)
			);

			$query
				->select('*')
				->from($db->quoteName('ratings_user'))
				->where($conditions);

			$db->setQuery($query);
			$results = $db->loadObjectList();
			if ($results) {
				$result = $results[0];
				if ($result) {
					$user_rating = $result->user_rating;
				}
			} else {
				$user_rating = 0;
			}
		} else {
			$user_rating = 0;
		}

		if ($user_rating > 0 && $display_own) {
			$class = "topic_rating_user";
			$star_rating = $user_rating;
		} else {
			$class = "topic_rating";
			$star_rating = $current_rating;
		}

?>

		<div id="rating_container_<?php echo $rating_id ?>" class="rating_container">

			<div
				id="topic_rating_<?php echo $rating_id ?>"
				class="<?php echo $class; ?>"
			>

				<?php
					$stars = array();

					/*
						Check if the user has already rated this post.
						If so, show their score.
						If not, show the average of other users.

						Or, if $display_own == false, always show the average.
					*/

					//0 = empty
					//1 = half
					//2 = full

					if ($star_rating == 0) {
						$full_stars = 0;
						$fraction = 0;
						$empty_stars = 5;
					} else if ($star_rating == 5) {
						$full_stars = 5;
						$fraction = 0;
						$empty_stars = 0;
					} else {
						$full_stars = floor($star_rating);			//Number of full stars
						$fraction = $star_rating - $full_stars;		//The fraction
						if ($fraction > 0) {
							$empty_stars = 5 - ($full_stars + 1);			//Number of empty stars
						} else {
							$empty_stars = 5 - $full_stars;
						}
					}

					$count = 1;
					for ($i = 0; $i < $full_stars; $i++) {
						ratings_make_span('full', $rating_id, $count);
						$count++;
					}

					if ($fraction  != 0) {

						if ($fraction > 0 && $fraction <= 0.33) {
							$star_type = "empty";
						} else if ($fraction > 0.33 && $fraction <= 0.66) {
							$star_type = "half";
						} else {
							$star_type = "full";
						}

						ratings_make_span($star_type, $rating_id, $count);
						$count++;
					}

					for ($i = 0; $i < $empty_stars; $i++) {
						ratings_make_span('empty', $rating_id, $count);
						$count++;
					}

				?>
			</div>

			<?php

				for ($i = 1; $i <= 5; $i++) {
					$title = "";

					if (isset($hover_array)) {
						$title .= $hover_array[$i - 1];
					} else {
						switch ($i) {
							case 1: $title .= "Okay"; break;
							case 2: $title .= "Good"; break;
							case 3: $title .= "Great"; break;
							case 4: $title .= "Awesome!"; break;
							case 5: $title .= "Loved it!"; break;
						}
					}

					if ($user_rating > 0) {
						$title .= "\nYour current rating: " . $user_rating . " / 5";
					}

					if ($current_rating > 0) {

						if ($current_rating - round($current_rating) != 0) {
							$formatted_rating = number_format($current_rating, 1);
						} else {
							$formatted_rating = $current_rating;
						}

						$title .= "\nAve. user rating: " . $formatted_rating . " / 5";
						$title .= "\n" . $total_votes . " vote";
						if ($total_votes > 1) {
							$title .= "s";
						}

					} else {
						$title .= "\nYou're the first to rate this.";
					}
			?>
				<div
					id="topic_rating_user_<?php echo $rating_id . "_" . $i ?>"
					class="topic_rating_user rating_title"
					style="display:none;"
					title = "<?php echo $title ?>"
					onmouseout="clear_rating_hover(<?php echo "'" . $rating_id . "'" ?>)"
			>
					<span title="">
					<?php
						for ($j = 1; $j <= 5; $j++) {

							if ($j <= $i) {
								$star_type = "full";
							} else {
								$star_type = "empty";
							}

							ratings_make_span($star_type, $rating_id, $j);
						}
					?>
					</span>
				</div>

			<?php
				}
			?>
		</div>
<?php

	}

function ratings_make_span($type, $rating_id, $count) {

	$key = AxsKeys::getKey('ratings');

	$data = array(
		"rating_id" => $rating_id,
		"count" => $count
	);

	$user_id = JFactory::getUser()->id;

	if ($user_id) {
		$function_data = base64_encode(AxsEncryption::encrypt($data, $key));

		?>

			<span
				id="rating_<?php echo $rating_id . "_" . $count ?>"
				class="lizicon-star-<?php echo $type ?>"

				onclick="set_rating('<?php echo $function_data ?>')"
			></span>

		<?php

	} else {
		?>

			<span
				id="rating_<?php echo $rating_id . "_" . $count ?>"
				class="lizicon-star-<?php echo $type ?>"
			></span>

		<?php
	}
}

?>

