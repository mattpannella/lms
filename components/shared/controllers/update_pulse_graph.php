<?php

function update_pulse_graph()
{

    require_once('components/shared/controllers/category.php');
    $allCategories = getCategories();
    
    $data = array();

    /*
        Get all of the user data from each of the boards.
    */

    for ($i = 0; $i < 3; $i++) {
        switch ($i) {
            case 0: $board = 'declaration'; break;
            case 1: $board = 'gratitude'; break;
            case 2: $board = 'inspiration'; break;
        }

        $results = get_results($board);                     //Get the results
        $data = compile_data($board, $results, $data);      //Compile it into usable data
    }

    if (count($data) == 0) {
        //The boards are empty.
        $noData = true;
    } else {
        $noData = false;
    }

    $categoryList = array();
    foreach($allCategories as $cat) {
        $name = $cat->name;
        array_push($categoryList, $name);
        $search[$cat->name] = $cat->id;
    }

    $numCategories = count($categoryList);

    /*
        Set up the columns ahead of time since they will always be the same.
    */
    $columns = array(                        
                    'year',
                    'month',
                    'day',
                    'date'
                );

    for ($i = 0; $i < $numCategories; $i++) {
        array_push($columns, $categoryList[$i]);
    }

    $db = JFactory::getDBO();

    /*
        Collect all the values first so that they can be sent in one query.
    */

    $values = array();

    $years = array_keys($data);
    foreach($years as $year) {
        
        $months = array_keys($data[$year]);        
      
        foreach ($months as $month) {
            
            $days = array_keys($data[$year][$month]);        
            foreach ($days as $day) {

                $queryList = "";

                $info = array();
                $categories = array_keys($data[$year][$month][$day]);
                foreach ($categories as $category) {
                    
                    $categoryInfo = array();
                    $boards = array_keys($data[$year][$month][$day][$category]);
                    foreach ($boards as $board) {
                        array_push($categoryInfo, array(
                            $board => $data[$year][$month][$day][$category][$board]
                        ));                            
                    }

                    $value = json_encode($categoryInfo);                       

                    $info[$allCategories[$category]->name] = $value;                        
                }

                /*
                    z - Day of year
                    Y - Year
                    U - datecode
                */

                $date = DateTime::createFromFormat('z Y', strval($day) . ' ' . strval($year))->format('U');

                $queryList .= $db->quote($year) . ", " . $db->quote($month) . ", " . $db->quote($day) . ", " . $db->quote($date);

                for ($i = 0; $i < $numCategories; $i++) {
                    if (isset($info[$categoryList[$i]])) {                    
                        $queryList .= ", " . $db->quote($info[$categoryList[$i]]);
                    } else {
                        $queryList .= ", ''";
                    }
                }

                $values[] = $queryList;
            }
        }
    }

    /*
        Create a list of columns, starting with year, month, day, date, and then adding a column for each category.
        Create this before dropping the table so minimal work is done while the table is deleted.
    */

    $columnQuery = "CREATE TABLE axs_pulse_daily_data (year INT(11), month INT(11), day INT(11), date BIGINT(20)";
    for ($i = 0; $i < $numCategories; $i++) {
        $columnQuery .= ", " . $categoryList[$i] . " VARCHAR(255)";            
    }
    $columnQuery .= ");";

    /*
        Create the query for dropping the whole table.
    */
    $dropQuery = 'DROP TABLE axs_pulse_daily_data;';

    /*
        Execute all of the queries
    */

    //$tableStart = microtime(true);
    $db->transactionStart();

    /*
        Drop the table.
    */
    $db->setQuery($dropQuery);
    $db->execute();

    /*
        Create the table with the new columns
    */
    $db->setQuery($columnQuery);
    $db->execute();
    //$tableEnd = microtime(true);        

    //$queryStart = microtime(true);


    if ($noData == true) {
        return;
    }
    
    /*
        Insert all of the rows into it.
    */
    $dataQuery = $db->getQuery(true);
    $dataQuery
        ->insert($db->quoteName('axs_pulse_daily_data'))
        ->columns($db->quoteName($columns))
        ->values($values);

    $db->setQuery($dataQuery);
    $db->execute();

    $db->transactionCommit();

    //$queryEnd = microtime(true);

    //$tableTime = $tableEnd - $tableStart;
    //$queryTime = $queryEnd - $queryStart;

    //$txt = "\nTable creation: " . $tableTime . "\nexecution took: " . $queryTime . "\nrun at: " . date("h:i:s M d Y", strtotime('now')):q;
    //$myfile = file_put_contents('/var/www/html/cli/pulse_update.out', $txt.PHP_EOL, FILE_APPEND);

}

function get_results($board_name) {

    $db = JFactory::getDBO();

    $conditions = array(        
        $db->quoteName('deleted') . '=' . $db->quote(0),
        $db->quoteName('public') . '=' . $db->quote('yes')
    );

    $query = $db->getQuery(true);
    $query
        ->select('*')
        ->from($db->quoteName($board_name . "_board"))
        ->where($conditions);

    $db->setQuery($query);

    return $db->loadObjectList();
}

function compile_data($board, $results, $data) {    
    foreach($results as $result) {  
        $day = date('z', $result->date);
        $month = date('m', $result->date);
        $year = date('Y', $result->date);
        $category = $result->category;
        
        if (isset($data[$year][$month][$day][$category][$board])) {        
            $data[$year][$month][$day][$category][$board]++;
        } else {
            $data[$year][$month][$day][$category][$board] = 1;
        }
    }

    return $data;
}