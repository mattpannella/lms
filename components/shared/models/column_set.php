<style>
    .columnChange {
        margin-left:        10px;
        margin-right:       10px;
        cursor:             pointer;
        color:              rgb(128, 128, 128);
        font-size:          24px;
    }    

    .columnChange:hover {
        color:              rgb(92, 92, 255);
    }

    #column_set {
        font-size:          20px; 
        color:              rgb(128,128,128);
        display:            none;
    }
</style>

<script>
    function decrease_rows() {
        num_columns--;        
    }

    function increase_rows() {
        num_columns++;    
    }
</script>
	
<div id="column_set" class="hide_on_reload" onselectstart="return false">
    Columns:
    <span class="lizicon-minus columnChange" title="Decrease number of columns" onclick="decrease_rows()"></span>
    <span class="lizicon-plus columnChange" title="Increase number of columns" onclick="increase_rows()"></span>
</div> 