<?php
	defined( '_JEXEC' ) or die( 'Restricted access' );
?>
    <script type="text/javascript" src="components/shared/includes/js/pulse.js?v=2"></script>
<?php

function showPulse($data, $board_id) {
    require_once('components/shared/controllers/category.php');
    $categories = getCategories($data->categories);
?>    

    <script>
        setCategories(<?php echo "'" . json_encode($categories) . "'"; ?>);
    </script>

    <style>
        #pagination_view {
            font-size:          20px; 
            color:              rgb(100, 100, 100); 
            margin-top:         10px; 
            margin-bottom:      10px;
        }

        #team_select_view {
            font-size:          20px; 
            color:              rgb(100, 100, 100); 
            margin-top:         10px; 
            margin-bottom:      10px;
        }

        .inspiration_info {
            position:           absolute;
            float:              right;
            display:            none;            
            color:              rgb(64,64,64);
            width:              65%;
            right:              0%;
            top:                110px;
            font-size:          1.0rem;
            border:             1px solid gray;
            border-radius:      4px;
            background-color:   rgb(225,225,225);
            opacity:            0.95;
            padding:            15px;
            z-index:            100;
            max-width:          1200px;
        }
    </style>

    <span class="heartbeat-title">
        <?php /*
        <button class="heartbeat heartbeat_check btn btn-primary">
            <span class="lizicon-health"></span>
            Check the Pulse
        </button>
        */ ?>
        <button class="heartbeat sort_posts btn btn-primary">
            <span class="lizicon-search" style="margin-left: 20px;"></span>
            Sort posts
        </button>

        <?php
            $id = JRequest::getVar('id');
            $myRoute = JRoute::_("index.php?option=com_axs&view=board&id=$id&edit=1");
            $reportRoute = JRoute::_("index.php?option=com_axs&view=board&layout=report&id=$id");
        
            $show_my_button = false;
            $show_report_button = false;

            if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {               

                if ($_SESSION['user_board'][$board_id]->board_access) {
                    $show_my_button = true;
                }

                if ($_SESSION['user_board'][$board_id]->report_access) {
                    $show_report_button = true;
                }
            } else {
                $show_my_button = true;
            }
        ?>

        <?php 
            if ($show_my_button) { 
        ?>
                <a href="<?php echo $myRoute;?>">
                    <button class="heartbeat btn btn-primary">
                        <span class="lizicon-blog" style="margin-left: 20px;"></span>
                        <?php echo $data->board->my_button; ?>
                    </button>
                </a>
        <?php 
            } 

            if ($show_report_button) {
        ?>
                <a href="<?php echo $reportRoute;?>">
                    <button class="heartbeat btn btn-primary">
                        <span class="lizicon-user-tie" style="margin-left: 20px;"></span>
                        <?php echo "Reports";?>
                    </button>
                </a>
        <?php
            }
        ?>

        <span id="whats_this_button"class='lizicon-info btn btn-primary' style="color: white; float: right; font-size: 18px;">
            What's this?
        </span>
        <div id="inspiration_info" class='inspiration_info'></div>

        <script>                    
            $("#whats_this_button").click(
                function() {                
                    $("#inspiration_info").slideToggle(750);
                }   
            );
        </script>        
    </span>

    <div id="clr"></div>

    <div id = "ginHeart" class="row" style="padding: 0px 50px; max-width: 1200px; display:none;"></div>
    <div id="clr"></div>
    <div id = "postSort" class="row" style="display:none;"></div>

    <div class="hide_on_reload" style="display:none;">
        <div id="pagination_view">
            Amount per page
            <br>
            <!--<div id="video_count" value="<?php //echo $video_count ?>"></div>-->
            <select id="pagination_amount">
                <option value="10">10</option>
                <option value="25">25</option>
                <option value="50">50</option>
                <option value="75">75</option>
                <option value="100">100</option>
                <option value="150">150</option>
                <option value="200">200</option>
                <option value="-1">All</option>
            </select>
            <?php /* <button id="pagination_set">Set</button> */ ?>

        </div>
        <div id="team_select_view" style="display:none;">
            Select Team to Show
            <br>
            <select id="team_select">
                <option value="all">All</option>
            </select>
        </div>
        
        <br>
        <div id="pagination_buttons"></div>
        <br/>
    </div>
<?php
}
?>