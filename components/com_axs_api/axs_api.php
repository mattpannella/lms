<?php

defined('_JEXEC') or die();

JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addStyleSheet(JURI::root(true) . '/components/com_axs/assets/css/main.css');

// Load FOF
include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
    JError::raiseError ('500', 'FOF is not installed');
    return;
}

FOFDispatcher::getTmpInstance('com_axs_api')->dispatch();