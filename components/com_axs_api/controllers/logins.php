<?php

defined('_JEXEC') or die();

header('Access-Control-Allow-Origin: *');

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class Axs_apiControllerLogins extends FOFController {

	public function login() {
		$data = JFactory::getApplication()->input->get('data');
				
		if (!$data) {
			return;
		}

		$user = json_decode(base64_decode($data));

		$userName = $user->usr;
		$password = $user->psw;

		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_users WHERE username = '" . $userName . "'";
		$db->setQuery($query);
		$dbUser = $db->loadObjectList()[0];

		if (!$dbUser) {
			return;
		}

		$verify = JUserHelper::verifyPassword($password, $dbUser->password, $dbUser->id);

		if (!$verify) {					
			return;
		} 

		$user = AxsApiPortal::getUserInfoFromDBObject($dbUser);		
		$jwt = AxsApiPortal::makeJWT($user);
		
		echo base64_encode($jwt);
	}
}

?>