<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

// No direct access
defined('_JEXEC') or die;


$user = JFactory::getUser();
$contactPageId = 0;
$unique = rand(10, 1000);
$formLayout = 'stacked';
$fieldClass = 'col-sm-12';
$buttonClass = 'btn-primary';
$buttonText = 'Send';

//first grab the brand
$brand = AxsBrands::getBrand();

if($contact_module_settings->contact_form_id) {
	$contactPageId = $contact_module_settings->contact_form_id;
} else {
	$app = JFactory::getApplication();
	$contactPageId = (int)$app->input->get('id');
}
$contactTemplate = AxsContact::getContactTemplate($brand->id,$contactPageId);
$contactParams = json_decode($contactTemplate->params);
if($contactParams->show_title) {
	$showTitle = true;
} else {
	$showTitle = false;
}

$isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
if($isIOS) {
	$contactParams->captcha_field = 'no';
}

if($contact_module_settings->contact_form_id) {
	$showContactUs = false;
	$class = 'modForm';
	$container = 'container-fluid';
	$style = ' margin: 10px;';
	$buttonStyle = '';
	$buttonHoverStyle = '';

	if(!$contact_module_settings->contact_form_show_title) {
		$showTitle = false;
	}

	if($contact_module_settings->contact_form_show_contact_us) {
		$showContactUs = true;
	}
	if($contact_module_settings->contact_form_background_color) {
		$formBackgroundOpacity = 1;
        list($r, $g, $b) = sscanf($contact_module_settings->contact_form_background_color, "#%02x%02x%02x");
        if ($contact_module_settings->contact_form_background_transparency) {
        	$formBackgroundOpacity = $contact_module_settings->contact_form_background_transparency / 100;
    	}
        $formBackgroundColor = "rgba( $r, $g, $b, $formBackgroundOpacity )";
		$style .= ' background-color: '.$formBackgroundColor.';';
	}
	if($contact_module_settings->contact_form_text_color) {
		$style .= ' color: '.$contact_module_settings->contact_form_text_color.';';
	}

	if($contact_module_settings->contact_form_padding) {
		$style .= ' padding: '.$contact_module_settings->contact_form_padding.'px; ';
	}

	if($contact_module_settings->contact_form_button_style_custom) {
		$buttonClass = 'modFormButton';

		if($contact_module_settings->contact_form_submit_button_text) {
			$buttontext = $contact_module_settings->contact_form_submit_button_text;
		}

		if($contact_module_settings->contact_form_button_position) {
			$buttonStyle .= ' text-align: '.$contact_module_settings->contact_form_button_position.';';
		}

		if($contact_module_settings->contact_form_submit_button_text_color) {
			$buttonStyle .= ' color: '.$contact_module_settings->contact_form_submit_button_text_color.';';
		}

		if($contact_module_settings->contact_form_submit_button_background_color) {
			$buttonStyle .= ' background-color: '.$contact_module_settings->contact_form_submit_button_background_color.';';
		}

		if($contact_module_settings->contact_form_submit_button_hover_text_color) {
			$buttonHoverStyle .= ' color: '.$contact_module_settings->contact_form_submit_button_hover_text_color.';';
		}

		if($contact_module_settings->contact_form_submit_button_hover_background_color) {
			$buttonHoverStyle .= ' background-color: '.$contact_module_settings->contact_form_submit_button_hover_background_color.';';
		}
	}

	if($contact_module_settings->contact_form_layout == 'inline') {
		$formLayout = 'inline';
	}


} else {
	$style = 'default';
	$class = 'box';
	$buttonStyle = '';
	$container = 'container';
	$showContactUs = true;
}



if($formLayout == 'inline') {
	$fieldCount = 1;
	if($contactParams->name_field != 'hide') {
		$fieldCount++;
	}
	if($contactParams->email_field != 'hide') {
		$fieldCount++;
	}
	if($contactParams->phone_field != 'hide') {
		$fieldCount++;
	}
	if($contactParams->message_field != 'hide') {
		$fieldCount++;
	}
	if($contactParams->captcha_field != 'no') {
		$fieldCount++;
	}
	switch ($fieldCount) {
		case 1:
			$fieldClass = 'col-sm-12';
		break;

		case 2:
			$fieldClass = 'col-sm-6';
		break;

		case 3:
			$fieldClass = 'col-sm-4';
		break;

		case 4:
			$fieldClass = 'col-sm-3';
		break;

		case 5:
			$fieldClass = 'col-sm-2';
		break;

		case 6:
			$fieldClass = 'col-sm-2';
		break;
	}

}


switch ($contactParams->map_position) {
	case 'right':
		$mapClass = 'col-sm-6';
	break;

	case 'bottom':
		$mapClass = 'col-sm-12';
	break;
}

?>
	<style>
	  .contact {
	  	color: #777;
	  	size: 16px;
	  	font-weight: bold;
	  }
	  .subheader {
	    font-size: 20px;
	    margin-top: 15px;
	  }
	  .subtitle {
	    font-size: 20px;
	    margin-bottom: 15px;
	    font-weight: bold;
	  }
	  .form-control {
	  	margin-bottom: 5px;
	  	margin-top: 5px;

	  }
	</style>
<?php if($style == 'default') { ?>
	<style>
	  .box {
	    background-color: white;
	    color: black;
	    padding: 20px;
	    margin: 15px 0px;
	    box-shadow: 0 1px 3px rgba(34,25,25,0.4);
	  }
	</style>
<?php } else { ?>
	<style>
	.modForm {

	}

	.modFormButton {
		<?php echo $buttonStyle; ?>
	}

	.modFormButton:hover {
		<?php echo $buttonHoverStyle; ?>
	}


	</style>
<?php } ?>
<div class="<?php echo $container; ?>" style="<?php echo $style; ?>">
	<div class="row">
	<?php if($showTitle) { ?>
		<h1><?php if(!empty($contactTemplate->title)) { echo $contactTemplate->title; }; ?></h1>
	<?php } ?>
	<?php if($contactTemplate->image_top) { ?>
		<div class="col-sm-12">
			<img class="img-responsive" src="<?php echo $contactTemplate->image_top; ?>">
		</div>
	<?php } ?>


	<?php if($contactParams->show_form) { ?>
		<?php if($contactParams->show_map || $contactTemplate->image_side) { ?>
			<div class="col-sm-6">
		<?php } else { ?>
			<div class="col-sm-12">
		<?php } ?>
				<div class="<?php echo $class; ?>">
					<?php if($showContactUs) { ?>
		        		<h1 class="subtitle"><?php echo AxsLanguage::text('COM_CONTACT_US', 'Contact Us'); ?></h1>
		        		<hr/>
		        	<?php } ?>
		        	<?php
		        		if ($contactTemplate->contact_person) {
		        			$has_name = true;
		        	?>
		        		<span class="contact pull-left"><span class="lizicon lizicon-user"></span> <?php echo $contactTemplate->contact_person; ?></span>
		        	<?php
		        		}

		        		if ($contactTemplate->phone) {
		        			if ($has_name) {
		        				$pull_class = "pull-right";
		        			} else {
		        				$pull_class = "pull-left";
		        			}
		        	?>
		        			<span class="contact <?php echo $pull_class;?>"><span class="lizicon lizicon-phone"></span> <?php echo $contactTemplate->phone; ?></span>
		        	<?php
		        		}

		        		if ($contactParams->notes) {
		        	?>
			        		<br>
			        		<div class="contact"><span class="lizicon lizicon-file-text"></span> <?php echo $contactParams->notes; ?></div>
		        	<?php
		        		}
		        	?>
		        	<?php if($showContactUs) { ?>
		        	<br/><br/>
		        	<?php } ?>
					<form class="form-horizontal contactForm_<?php echo $unique; ?>" role="form" method="post">
						<div class="form-group">
						<?php if($contactParams->name_field != 'hide') { ?>

							<div class="<?php echo $fieldClass; ?>">
								<input <?php if($contactParams->name_field == 'require') { echo 'required'; } ?> type="text" class="form-control" id="name" name="name" placeholder="<?php echo AxsLanguage::text('COM_CONTACT_FIRST_LAST', 'First & Last Name'); ?>" value="">
							</div>
						<?php } ?>
						<?php if($contactParams->email_field != 'hide') { ?>
							<div class="<?php echo $fieldClass; ?>">
								<input <?php if($contactParams->email_field == 'require') { echo 'required'; } ?> type="email" class="form-control" id="email" name="email" value="" placeholder="<?php echo AxsLanguage::text('COM_CONTACT_EMAIL', 'Email'); ?>">
							</div>
						<?php } ?>
						<?php if($contactParams->phone_field != 'hide') { ?>
							<div class="<?php echo $fieldClass; ?>">
								<input <?php if($contactParams->phone_field == 'require') { echo 'required'; } ?> type="text" class="form-control" id="phone" name="phone"  value="" placeholder="<?php echo AxsLanguage::text('COM_CONTACT_PHONE', 'Phone'); ?>">
							</div>
						<?php } ?>
						<?php if($contactParams->message_field != 'hide') { ?>
							<div class="<?php echo $fieldClass; ?>">
								<textarea <?php if($contactParams->message_field == 'require') { echo 'required'; } ?> class="form-control" rows="4" name="message" placeholder="<?php echo AxsLanguage::text('COM_CONTACT_MESSAGE', 'Message'); ?>"></textarea>
							</div>
						<?php } ?>
						<?php if($contactParams->captcha_field != 'no') { ?>
							<div class="<?php echo $fieldClass; ?>" style="text-align: center; padding: 10px;">
							<?php
								$captcha_plugin = JFactory::getConfig()->get('captcha');
								if ($captcha_plugin != '0') {
								 	$captcha = JCaptcha::getInstance($captcha_plugin);
								 	$field_id = 'recap_1';
								 	echo $captcha->display($field_id, $field_id, 'g-recaptcha');
								}
							?>
							</div>
						<?php } ?>
							<div class="<?php echo $fieldClass; ?>">
								<button  id="send" class="btn <?php echo $buttonClass; ?> send form-control">
									<?php echo AxsLanguage::text('COM_CONTACT_SEND', $buttontext); ?>
								</button>
							</div>
						</div>
						<input type="hidden" value="<?php echo $contactTemplate->id; ?>" name="form_id"/>
						<input type="hidden" value="<?php echo $contactTemplate->title; ?>" name="form_title"/>
					</form>
		        </div>
			</div>
	<?php } ?>



	<?php if( ($contactTemplate->image_side) && (!$contactParams->show_map || ($contactParams->show_map && $contactParams->map_position == 'bottom')) ) { ?>
		<div class="col-sm-6">
			<div class="box">
				<img class="img-responsive" style="width:100%;" src="<?php echo $contactTemplate->image_side; ?>">
			</div>
		</div>
	<?php } ?>

	<?php if($contactParams->show_map) { ?>
		<div class="<?php echo $mapClass; ?>">
			<div class="box">
	        	<h1 class="subtitle"><?php echo AxsLanguage::text('COM_CONTACT_LOCATION', 'Location'); ?></h1>
	        	<hr/>
				<iframe
		          width="100%"
		          height="343"
		          seamless="seamless""
		          frameborder="0" style="border:0"
		          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC_6XKStjrrBJqzidJyrF6fp_g9u1w_iQ0
		            &q=<?php echo urlencode($contactTemplate->address.','.$contactTemplate->city.','.$contactTemplate->state.','.$contactTemplate->country); ?>" allowfullscreen>
	        	</iframe>
	        </div>
		</div>
	<?php } ?>

	<?php if($contactParams->page_type == 'freeform') { ?>
			<div class="col-sm-12">
				<div class="box">
					<?php echo $contactTemplate->custom_text; ?>
				</div>
			</div>
	<?php } ?>
	</div>
</div>

<script>
(function($){
    $(function() {
		$(document).on('submit', '.contactForm_<?php echo $unique; ?>', function(e) {
				e.preventDefault();
	            var data = $('.contactForm_<?php echo $unique; ?>').serialize();
	            if(data) {
		            $.ajax({
		                url: '/index.php?option=com_contactpage&task=contactpages.sendMessage&format=raw',
		                data: data,
		                type: 'post'
			            }).done(function(result) {
			            	var response = JSON.parse(result);
			                if(response.success) {
			                    if(response.message) {
			                    	alert(response.message);
									window.location.reload();
			                    } else {
			                    	alert("<?php echo AxsLanguage::text('AXS_CONTACT_MESSAGE_SENT', 'Your Message Was Sent'); ?>");
									window.location.reload();
			                    }
			                } else {
								var alertMessage = "<?php echo AxsLanguage::text('AXS_CONTACT_MESSAGE_ERROR', 'There was a problem trying to send your message.'); ?>";
								if (response.validationErrors && response.validationErrors.length > 0) {
									alertMessage += ' Reason: ';
									alertMessage += response.validationErrors.join(', ');
								}
								grecaptcha.reset();
			                    alert(alertMessage);
			                }
			            }).fail(function() {
			                alert("<?php echo AxsLanguage::text('AXS_CONTACT_MESSAGE_ERROR', 'There was a problem trying to send your message.'); ?>");
			            });

				}
		});
	});
}(jQuery));
</script>