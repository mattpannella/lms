<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die();


class ContactpageControllerContactpages extends FOFController {
    public $input;
    public $brand;

    /**
     * List of sql injection probes to check for in message
     * 
     * LMS-5227
     */
    private static $evilStrings = [
        "20'||DBMS_PIPE.RECEIVE_MESSAGE(CHR(98)||CHR(98)||CHR(98),15)||'",
        "20*DBMS_PIPE.RECEIVE_MESSAGE(CHR(99)||CHR(99)||CHR(99),15)",
        "FhdiaJmt')) OR 434=(SELECT 434 FROM PG_SLEEP(15))--",
        "I0u54ChM') OR 950=(SELECT 950 FROM PG_SLEEP(15))--",
        "2Kb1y51v' OR 845=(SELECT 845 FROM PG_SLEEP(15))--",
        "-1)) OR 479=(SELECT 479 FROM PG_SLEEP(15))--",
        "-5) OR 523=(SELECT 523 FROM PG_SLEEP(15))--",
        "-5 OR 181=(SELECT 181 FROM PG_SLEEP(15))--",
        "wNqouSMz'; waitfor delay '0:0:15' -- ",
        "1 waitfor delay '0:0:15' -- ",
        "-1); waitfor delay '0:0:15' -- ",
        "-1; waitfor delay '0:0:15' -- ",
        "(select(0)from(select(sleep(15)))v)/*'+(select(0)from(select(sleep(15)))v)+'\"+(select(0)from(select(sleep(15)))v)+\"*/",
        "0\"XOR(if(now()=sysdate(),sleep(15),0))XOR\"Z",
        "0'XOR(if(now()=sysdate(),sleep(15),0))XOR'Z",
        "if(now()=sysdate(),sleep(15),0)",
        "-1\" OR 2+53-53-1=0+0+0+1 -- ",
        "-1' OR 2+728-728-1=0+0+0+1 or 'tTIkJcs5'='",
        "-1' OR 2+94-94-1=0+0+0+1 -- ",
        "-1 OR 2+875-875-1=0+0+0+1",
        "-1 OR 2+879-879-1=0+0+0+1 -- ",
        "B5PO3Zm8"
    ];

    public function __construct(array $config) {
        parent::__construct($config);
        $this->input = JFactory::getApplication()->input;
        $this->brand = AxsBrands::getBrand();
    }


    public function getContactInfo($form_id) {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('axs_contact_page')
            ->where('id ='. (int)$form_id)
            ->limit(1);
        $db->setQuery($query);
        $contactPage = $db->loadObject();
        return $contactPage;
    }

    /**
     * Returns true if a submission for form_id on the ip exists
     * from less than 1 minutes ago
     */
    public function checkLastSubmissionTimestamp($form_id, $ip) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('axs_messages')
            ->where([
                'form_id =' . (int)$form_id,
                'user_ip =' . $db->quote($ip),
                'date_sent >= CURRENT_TIMESTAMP - INTERVAL 1 MINUTE' 
            ])
            ->limit(1);
        $db->setQuery($query);
        $message = $db->loadObject();
        return (bool)$message;
    }


    public function sendMail($params) {

        $mailer = JFactory::getMailer();

        // Make sure we're processing all recipients that are available
        $recipients = explode(',', $params->reciever);

        if(!empty($recipients)) {

            $mailer->setSender($params->sender);
            $mailer->addRecipient($recipients);
            $mailer->addReplyTo($params->reply);
            $mailer->setSubject($params->subject);

            if($params->html) {
                $mailer->isHtml(true);
                $mailer->Encoding = 'base64';
            }

            $mailer->setBody($params->message);
            $mailer->Send();
        }
   }

    public function sendMessage() {

        /*$config = JFactory::getConfig()->get('captcha');
        $captcha = JCaptcha::getInstance($config);
        $completed = $captcha->CheckAnswer();

        if ($completed === false) {
            echo "Invalid Captcha";
            return;
        } */
        $array = JRequest::get();
        $isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
        if(!$isIOS) {
            $captcha = $this->input->get('g-recaptcha-response', '', 'STRING');

            if( ($captcha != 'passed') && (array_key_exists('g-recaptcha-response', $array)) ) {
                $captchaResponse = JRequest::get('g-recaptcha-response');
                JPluginHelper::importPlugin('captcha');
                $dispatcher = JDispatcher::getInstance();
                $res = $dispatcher->trigger('onCheckAnswer',$captchaResponse);

                if(!$res[0]) {
                    echo json_encode([
                        "success" => false,
                        "validationErrors" => [ "Invalid Captcha" ]
                    ]);
                    return;
                }
            }
        }


        $system_config = JFactory::getConfig();

        $data   = new stdClass();
        $params = new stdClass();

        $data->name         = $this->input->get('name', '', 'STRING');
        $data->email        = $this->input->get('email', '', 'STRING');
        $data->phone        = $this->input->get('phone', '', 'STRING');
        $data->message      = $this->input->get('message', '', 'STRING');
        $data->form_id      = $this->input->get('form_id', '', 'INT');
        $data->form_title   = $this->input->get('form_title', '', 'STRING');
        $data->date_sent    = date('Y-m-d H:i:s');
        
		$user_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		$user_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : '';

        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : 'USER AGENT NOT FOUND';

        $dataParams = [
            'user_ip' => $user_ip,
            'user_agent' => $user_agent
        ];
        $data->params = json_encode($dataParams);

        $contactInfo = self::getContactInfo($data->form_id);
        $validationErrors = [];

        if (self::checkLastSubmissionTimestamp($data->form_id, $data->user_ip)) {
            $validationErrors []= AxsLanguage::text("AXS_TOO_MANY_SUBMISSION_TRY_AGAIN_MINUTE", "Too many submissions, wait and try again in a minute");
        }

        if ($contactInfo->title != $data->form_title) {
            $validationErrors []= AxsLanguage::text("AXS_FORM_NAME_DOESNT_MATCH", "Form name doesn't match form name on record");
        }

        $evilStringInMessage = false;
        foreach (self::$evilStrings as $evilString) {
            if (strpos($data->message, $evilString) !== false) {
                $evilStringInMessage = true;
            }
        }
        if ($evilStringInMessage) {
            $validationErrors []= AxsLanguage::text("AXS_SQL_INJECTION_DETECTED", "SQL injection attempt detected");
        }

        if (!empty(preg_replace("/[A-z0-9\s]+/", "", $data->name))) {
            $validationErrors []= AxsLanguage::text("AXS_INVALID_CHARACTERS_IN_NAME", "Invalid characters in name, only alphanumeric and whitespace allowed");
        }

        if (!filter_var($data->email, FILTER_VALIDATE_EMAIL)) {
            $validationErrors []= AxsLanguage::text("AXS_INVALID_EMAIL_FORMAT", "Invalid email format");
        }

        if (!empty(preg_replace("/[0-9-]+/", "", $data->phone))) {
            $validationErrors []= AxsLanguage::text("AXS_INVALID_PHONE_NUMBER_FORMAT", "Invalid phone number format");
        }

        if((!$data->form_id) || ( !$data->name && !$data->email && !$data->phone && !$data->message)) {
            $validationErrors []= AxsLanguage::text("AXS_MISSING_REQUIRED_FIELDS", "Missing required fields");
        }

        if (!empty($validationErrors)) {
            echo json_encode([
                'success' => false,
                'validationErrors' => $validationErrors
            ]);
            return;
        }

        $data->content = $contactInfo->params;

        $db  = JFactory::getDbo();
        $res = $db->insertObject('axs_messages', $data);

        
        $contactParams = json_decode($contactInfo->params);
        if($data->name) {
            $name = $data->name;
        } else {
            $name = $system_config->get('fromname');
        }

        if($data->email) {
            $email = $data->email;
        } else {
            $email = $system_config->get('mailfrom');
        }

        $params->reciever   = $contactInfo->emails;
        $params->subject    = $contactInfo->subject;
        $params->sender     = array($system_config->get('mailfrom'),$name);
        $params->reply      = array($email,$name);

        $message = '';
        if($data->form_title) {
            $message .= "Form: {$data->form_title}\n\n";
        }
        if($data->name) {
            $message .= "Name: {$data->name}\n\n";
        }
        if($data->email) {
            $message .= "Email: {$data->email}\n\n";
        }
        if($data->phone) {
            $message .= "Phone: {$data->phone}\n\n";
        }
        if($data->message) {
            $message .= "Message: {$data->message}\n";
        }


        $params->message    = $message;
        $params->html       = false;

        $thankYouMessage = new stdClass();
        $thankYouMessage->success = false;
        $thankYouMessage->message = '';

        $this->sendMail($params);

        if($contactParams->thank_you_message) {
            $thankYouMessage->message = $contactParams->thank_you_message;
        }

        if($res == 1) {
            $thankYouMessage->success = true;
        }

        echo json_encode($thankYouMessage);
    }

}