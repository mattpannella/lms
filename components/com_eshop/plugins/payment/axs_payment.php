<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/16/16
 * Time: 11:17 AM
 */

defined( '_JEXEC' ) or die;

class axs_payment extends os_payment
{

	/**
	 * Constructor function
	 *
	 * @param object $config
	 */
	function __construct($params)
	{
		$config = array(
			'type' => 1,
			'show_card_type' => false,
			'show_card_holder_name' => false
		);
		parent::__construct($params, $config);
	}

	/**
	 * Process payment with the posted data
	 * @param OrderEshop $row Object
	 * @param array $data array
	 * @return void
	 */
	function processPayment($row, $data) {

		$cardParams = AxsPayment::getCardParams();
		$brand = AxsBrands::getBrand();
		
		if ($data['card_on_file']) {			
			$cardParams->id = $data['card_on_file'];
			$card = AxsPayment::newCard($cardParams);
			$card->getCardWithId();
			$saveCard = false;
		} else {
			if($brand->billing->gateway_type == 'stripe') {
				$cardParams->user_id = $data['customer_id'];
				$cardParams->stripeToken = $data['stripeToken'];
				$cardParams->cardNumber = $data['last4'];
				$user = JFactory::getUser($cardParams->user_id);
				$cardParams->email = $data['payment_email'];
				$cardParams->description = $data['payment_firstname'].' '.$data['payment_lastname'];
			} else { 
				$cardParams->firstName = $data['payment_firstname'];
				$cardParams->lastName = $data['payment_lastname'];
				$cardParams->cardNumber = $data['card_number'];
				$cardParams->expire = str_pad($data['exp_month'], 2, '0', STR_PAD_LEFT) . substr($data['exp_year'], 2, 2);
				$cardParams->cvv = $data['cvv_code'];
				$cardParams->address = $data['payment_address_1'];
				$cardParams->zip = $data['payment_postcode'];
				$cardParams->city = $data['payment_city'];
				$cardParams->state = $data['payment_zone_name'];
				$cardParams->country = $data['payment_country_name'];
				$cardParams->phone = $data['payment_telephone'];
				$cardParams->email = $data['payment_email'];
				$cardParams->user_id = $data['customer_id'];
			}

			$card = AxsPayment::newCard($cardParams);
			$saveCard = true;
		}

		$trxnParams = AxsPayment::getTransactionParams();

		$trxnParams->user_id = $data['customer_id'];
        $trxnParams->type = AxsPayment::$trxn_types['e-commerce'];
        $trxnParams->type_id = $row->id;
        $trxnParams->amount = $data['total'];
        $trxnParams->date = date("Y-m-d H:i:s");
        $trxnParams->card = $card;
        $trxnParams->initiator = AxsPayment::$initiators['user'];
        $trxnParams->description = $_SERVER['SERVER_NAME'] . " " . $row->order_number;        
        $trxnParams->saveCard = $saveCard;

		$trxn = AxsPayment::newTransaction($trxnParams);

		$trxn->charge();

		if($trxn->status == 'SUC') {
			// Payment success
			$row->transaction_id = $trxn->trxn_id;
			$row->axs_trxn_id = $trxn->id;
			$row->order_status_id = EshopHelper::getConfigValue('complete_status_id');
			
			$row->store();

			EshopHelper::completeOrder($row);
			JPluginHelper::importPlugin('eshop');

			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onAfterCompleteOrder', array($row));
			//Send confirmation email here
			
			if (EshopHelper::getConfigValue('order_alert_mail')) {
				EshopHelper::sendEmails($row);
			}
			$redirectUrl = JRoute::_(EshopRoute::getViewRoute('checkout').'&layout=complete');
			header('Location: '.$redirectUrl);
			exit();
		} else {
			//Payment failure, display error message to users
			$session = JFactory::getSession();
			$session->set('omnipay_payment_error_reason', $trxn->message);
			$redirectUrl = JRoute::_(EshopRoute::getViewRoute('checkout').'&layout=failure');
			header('Location: '.$redirectUrl);
			exit();
		}
	}


	public function renderPaymentInformation() { 
		$brand = AxsBrands::getBrand();
	?>
		<script type="text/javascript">
			<?php if (EshopHelper::getConfigValue('enable_checkout_captcha')) {
				$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
				if ($captchaPlugin == 'recaptcha') {
					$recaptchaPlugin = JPluginHelper::getPlugin('captcha', 'recaptcha');
					$params = new JRegistry($recaptchaPlugin->params);
					$version	= $params->get('version', '1.0');
					$pubkey		= $params->get('public_key', '');
					?>
					(function($) {
						$(document).ready(function() {
							<?php if ($version == '1.0') {
								$theme = $params->get('theme', 'clean');
								?>
								Recaptcha.create("<?php echo $pubkey; ?>", "dynamic_recaptcha_1", {theme: "<?php echo $theme; ?>"});
							<?php } else {
								$theme = $params->get('theme2', 'light');
								$langTag = JFactory::getLanguage()->getTag();
								if (JFactory::getApplication()->isSSLConnection()) {
									$file = 'https://www.google.com/recaptcha/api.js?hl=' . $langTag . '&onload=onloadCallback&render=explicit';
								} else {
									$file = 'http://www.google.com/recaptcha/api.js?hl=' . $langTag . '&onload=onloadCallback&render=explicit';
								}
								JHtml::_('script', $file, true, true);
								?>
								grecaptcha.render("dynamic_recaptcha_1", {sitekey: "' . <?php echo $pubkey;?> . '", theme: "' . <?php echo $theme; ?> . '"});
							<?php } ?>
						})
					})(jQuery);
				<?php }
			} ?>

			function checkNumber(input) {
				var num = input.value;
				if(isNaN(num)) {
					alert("<?php echo JText::_('ESHOP_ONLY_NUMBER_IS_ACCEPTED'); ?>");
					input.value = "";
					input.focus();
				}
			}

			function checkPaymentData() {
				var gateway = $('#payment_method_form').data('gtw');
				<?php if ($this->type) { ?>
					form = document.getElementById('payment_method_form');
					if (!form.card_on_file || form.card_on_file.value == 0) {
						if(gateway == 'stripe') {
							if(!form.stripeToken.value) {
								alert("<?php echo  JText::_('ESHOP_ENTER_CARD_NUMBER'); ?>");
								return false;
							}
						} else {
							if (!form.card_number.value) {							
								form.card_number.focus();
								return false;
							}
							if (form.cvv_code.value == "") {
								alert("<?php echo JText::_('ESHOP_ENTER_CARD_CVV_CODE'); ?>");
								form.cvv_code.focus();
								return false;
							}
						}
						
					}
					return true;
				<?php } else { ?>
					return true;
				<?php } ?>
			}

			Eshop.jQuery(document).ready(function($) {
				// New Card button
				$('#use_new_card').click(function() {
					//clear card on file
					$('#card_on_file').val(0);
					//hide card on file section
					$('#card_on_file_group').slideToggle();
					$('#new_card_group').slideToggle();
				});

				// Card on File button
				$('#use_card_on_file').click(function() {
					//clear card data
					$('#card_number').val('');
					$('#exp_month').val('');
					$('#exp_year').val('');
					$('#cvv_code').val('');

					$('#new_card_group').slideToggle();
					$('#card_on_file_group').slideToggle();
				});

				// Confirm button
				$('#button-confirm').click(function() {
					if (checkPaymentData()) {
						<?php 
							if (EshopHelper::getConfigValue('enable_checkout_captcha')) {
								$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
								if ($captchaPlugin == 'recaptcha') { 
						?>
									var siteUrl = '<?php echo EshopHelper::getSiteUrl(); ?>';
									jQuery.ajax({
										url: siteUrl + 'index.php?option=com_eshop&task=checkout.validateCaptcha',
										type: 'post',
										dataType: 'json',
										//data: jQuery('#payment_method_form input[type=\'text\'], #payment_method_form input[type=\'radio\']:checked, #payment_method_form input[type=\'hidden\']'),
										data: jQuery('#payment_method_form').serialize(),
										beforeSend: function() {
											$('#button-confirm').attr('disabled', true);
											$('#button-confirm').after('<span class="wait">&nbsp;<img src="components/com_eshop/assets/images/loading.gif" alt="" /></span>');
										},
										complete: function() {
											$('#button-confirm').attr('disabled', false);
											$('.wait').remove();
										},
										success: function(data) {
											if (data['error']) {
												alert(data['error']);
											}
											if (data['success']) {
												$('#payment_method_form').submit();
											}
										}
									});
							<?php 
								} else { 
							?>
									$('#button-confirm').attr('disabled', true);
									$('#button-confirm').after('<span class="wait">&nbsp;<img src="components/com_eshop/assets/images/loading.gif" alt="" /></span>');
									$('#payment_method_form').submit();
						<?php 
								}
							} else { 
						?>
							$('#button-confirm').attr('disabled', true);
							$('#button-confirm').after('<span class="wait">&nbsp;<img src="components/com_eshop/assets/images/loading.gif" alt="" /></span>');
							$('#payment_method_form').submit();
						<?php 
							} 
						?>
					}
				});
			});
		</script>
		<style>
			.no_margin_left {
			    float: none;
			    margin-top: 15px;
			}
		</style>
		<form action="<?php echo EshopHelper::getSiteUrl(); ?>index.php?option=com_eshop&task=checkout.processOrder" method="post" name="payment_method_form" id="payment_method_form" class="form form-horizontal" data-gtw="<?php echo $brand->billing->gateway_type; ?>">
			<?php if ($this->type) {
				$currentYear = date('Y');

				$cards = AxsPayment::getUserAllCards();
				if (count($cards)) { ?>
					<div id="card_on_file_group">
						<div class="control-group">
							<div class="control-label">
								Card on File<span class="required">*</span>
							</div>
							<div class="controls">
								<select id="card_on_file" name="card_on_file">
									<option value="0">--select card--</option>
									<?php foreach($cards as $card) { ?>
										<option value="<?php echo $card->id; ?>">Ending in <?php echo $card->cardNumber; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="control-group">
							<b>or</b>
						</div>
						<div class="control-group">
							<button type="button" id="use_new_card" class="btn btn-primary">Enter New Card</button>
						</div>
					</div>
				<?php } ?>

				<div id="new_card_group" <?php echo count($cards) ? 'style="display:none;"' : ''; ?>>
					<?php if (count($cards)) { ?>
						<div class="control-group">
							<button type="button" id="use_card_on_file" class="btn btn-primary">Use Card on File</button>
						</div>
					<?php } ?>

					<?php
						if($brand->billing->gateway_type == 'stripe') {
				            $stripeForm = AxsPayment::buildStipePaymentForm();
				    ?>
				    <div class="<?php echo $controlGroupClass;  ?> payment_information">
						<div class="<?php echo $controlsClass; ?>">
							<?php echo $stripeForm; ?>
						</div>
					</div>
				    <?php       
				        } else {
					?>
					<div class="control-group">
						<div class="control-label">
							<?php echo  JText::_('ESHOP_CARD_NUMBER'); ?><span class="required">*</span>
						</div>
						<div class="controls">
							<input type="text" id="card_number" name="card_number" class="inputbox" onkeyup="checkNumber(this)" value="" class="input-large" />
						</div>
					</div>
					<div class="control-group">
						<div class="control-label">
							<?php echo  JText::_('ESHOP_CARD_EXPIRY_DATE'); ?><span class="required">*</span>
						</div>
						<div class="controls">
							<?php echo  JHtml::_('select.integerlist', 1, 12, 1, 'exp_month', ' class="input-small" ', date('m'), '%02d').'  /  '.JHtml::_('select.integerlist', $currentYear, $currentYear + 10, 1, 'exp_year', ' class="input-small"'); ?>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="cvv_code">
							<?php echo JText::_('ESHOP_CVV_CODE'); ?><span class="required">*</span>
						</label>
						<div class="controls">
							<input type="text" id="cvv_code" name="cvv_code" class="input-small" onKeyUp="checkNumber(this)" value="" />
						</div>
					</div>
					<?php } //end of stripe if statement ?>
				</div>
			<?php }

			if (EshopHelper::getConfigValue('enable_checkout_captcha')) {
				$captchaPlugin = JFactory::getApplication()->getParams()->get('captcha', JFactory::getConfig()->get('captcha'));
				if ($captchaPlugin) { ?>
					<div class="control-group">
						<label class="control-label" for="recaptcha_response_field">
							<?php echo JText::_('ESHOP_CAPTCHA'); ?><span class="required">*</span>
						</label>
						<div class="controls docs-input-sizes">
							<?php echo JCaptcha::getInstance($captchaPlugin)->display('dynamic_recaptcha_1', 'dynamic_recaptcha_1', 'required'); ?>
						</div>
					</div>
				<?php }
			} ?>
			<div class="no_margin_left">
				<input id="button-confirm" type="button" class="btn btn-primary pull-right" value="<?php echo JText::_('ESHOP_CONFIRM_ORDER'); ?>" />
			</div>
		</form>
		<script>
			function stripeTokenHandler(token) {
		        // Insert the token ID into the form so it gets submitted to the server
		        var form = document.getElementById('payment_method_form');
		        var hiddenInput = document.createElement('input');
		        hiddenInput.setAttribute('type', 'hidden');
		        hiddenInput.setAttribute('id', 'stripeToken');
		        hiddenInput.setAttribute('name', 'stripeToken');
		        hiddenInput.setAttribute('value', token.id);
		        form.appendChild(hiddenInput);
		        var hiddenInputLastFour = document.createElement('input');
		        hiddenInputLastFour.setAttribute('type', 'hidden');
		         hiddenInputLastFour.setAttribute('id', 'last4');
		        hiddenInputLastFour.setAttribute('name', 'last4');
		        hiddenInputLastFour.setAttribute('value', token.card.last4);
		        form.appendChild(hiddenInputLastFour);
		    }

			
		    var gateway = $('#payment_method_form').data('gtw');
		    if(gateway == 'stripe') {
		    	card.addEventListener('change', function(event) {
		    		if(event.complete == true) {
		    			stripe.createToken(card).then(function(result) {
				            if (result.error) {
				              // Inform the user if there was an error.
				              var errorElement = document.getElementById('card-errors');
				              errorElement.textContent = result.error.message;
				            } else {
				                //console.log(result);
				              // Send the token to your server.
				               stripeTokenHandler(result.token);
				            }
				        });
		    		}	        
			    });
		    }
		</script>
		<?php
	}
}