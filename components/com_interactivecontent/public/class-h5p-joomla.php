<?php

class H5PJoomla implements H5PFrameworkInterface {

  /**
   * Kesps track of messages for the user.
   *
   * @since 1.0.0
   * @var array
   */
  private $messages = array('error' => array(), 'info' => array());

  public $stream_handle;
  public $plugin_slug;
  const CURL_7_10_5 = 0x070A05;
  const CURL_7_16_2 = 0x071002;
  public $version;
  public $done_headers = false;

  /**
   * Implements setErrorMessage
   */
  public function setErrorMessage($message, $code = NULL) {
    //if (current_user_can('edit_h5p_contents')) {
      $this->messages['error'][] = (object)array(
        'code' => $code,
        'message' => $message
      );
    //}
  }

  /**
   * Implements setInfoMessage
   */
  public function setInfoMessage($message) {
    //if (current_user_can('edit_h5p_contents')) {
      $this->messages['info'][] = $message;
    //}
  }

  /**
   * Return the selected messages.
   *
   * @since 1.0.0
   * @param string $type
   * @return array
   */
  public function getMessages($type) {
    if (empty($this->messages[$type])) {
      return NULL;
    }
    $messages = $this->messages[$type];
    $this->messages[$type] = array();
    return $messages;
  }

  /**
   * Implements t
   */
  public function t($message, $replacements = array()) {
    // Insert !var as is, escape @var and emphasis %var.
    foreach ($replacements as $key => $replacement) {
      if ($key[0] === '@') {
        //$replacements[$key] = esc_html($replacement);
        $replacements[$key] = $replacement;
      }
      elseif ($key[0] === '%') {
        //$replacements[$key] = '<em>' . esc_html($replacement) . '</em>';
        $replacements[$key] = '<em>' . $replacement . '</em>';
      }
    }
    $message = preg_replace('/(!|@|%)[a-z0-9-]+/i', '%s', $message);
    
    /*
    $plugin = H5P_Plugin::get_instance();
    $this->plugin_slug = $plugin->get_plugin_slug();
    */

    
    // Assumes that replacement vars are in the correct order.
    /*return vsprintf(__($message, $this->plugin_slug), $replacements);*/
     return vsprintf($message, $replacements);
  }

  /**
   * Helper
   */
  private function getH5pPath() {
    $plugin = H5P_Plugin::get_instance();
    return $plugin->get_h5p_path();
  }

  /**
   * Get the URL to a library file
   */
  public function getLibraryFileUrl($libraryFolderName, $fileName) {
    $h5p_dir = '/'.AxsImages::getImagesPath('h5p').'/libraries';
    return $h5p_dir . '/' . $libraryFolderName . '/' . $fileName;
  }

  /**
   * Implements getUploadedH5PFolderPath
   */
  public function getUploadedH5pFolderPath() {
    static $dir;

    if (is_null($dir)) {
      $plugin = H5P_Plugin::get_instance();
      $core = $plugin->get_h5p_instance('core');
      $dir = $core->fs->getTmpPath();
    }

    return $dir;
  }

  /**
   * Implements getUploadedH5PPath
   */
  public function getUploadedH5pPath() {
    static $path;

    if (is_null($path)) {
      $plugin = H5P_Plugin::get_instance();
      $core = $plugin->get_h5p_instance('core');
      $path = $core->fs->getTmpPath() . '.h5p';
    }

    return $path;
  }

  /**
   * Implements getLibraryId
   */
  
  public function getLibraryId($name, $majorVersion = NULL, $minorVersion = NULL) {
    
    $db = JFactory::getDbo();
    $query = "SELECT id FROM `ic_h5p_libraries` WHERE name = ".$db->quote($name)." AND major_version = ".$db->quote($majorVersion)." AND minor_version = ".$db->quote($minorVersion)." ORDER BY major_version DESC";
    $db->setQuery($query);
    $results = $db->loadResult();
    
    
   $id = $results;
   return $id === NULL ? FALSE : $id;
  }

  /**
   * Implements isPatchedLibrary
   */
  

  public function isPatchedLibrary($library) {
    
    $operator = $this->isInDevMode() ? '<=' : '<';
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select($db->quoteName(array('id')));
    $query->from($db->quoteName('ic_h5p_libraries'));
    $query->where(
                    $db->quoteName('name') . ' = ' . $db->quote($name). 
                    'AND'. $db->quoteName('major_version') . ' = ' . $db->quote($major_version). 
                    'AND'. $db->quoteName('minor_version') . ' = ' . $db->quote($minorVersion).
                    'AND'. $db->quoteName('patch_version') . $operator . $db->quote($patchVersion)
                  );
    $query->order($db->quoteName('major_version') . ' DESC');
    $db->setQuery($query);
    $results = $db->loadResult();
    $id = $results['id'];
    return $id === NULL ? FALSE : $id;
  }

  /**
   * Implements isInDevMode
   */
  public function isInDevMode() {
    return false;
  }

  /**
   * Implements mayUpdateLibraries
   */
  public function mayUpdateLibraries() {
    return TRUE;
  }

  /**
   * Implements getLibraryUsage
   */
  

  public function getLibraryUsage($id, $skipContent = FALSE) {
    
    $db = JFactory::getDbo();
    
    $query = $db->getQuery(true);
    $query->select(COUNT($db->quoteName('DISTINCT c.id')));
    $query->from($db->quoteName('ic_h5p_libraries', 'l'));
    $query->join('INNER', $db->quoteName('ic_h5p_contents_libraries', 'cl') . ' ON ' . $db->quoteName('l.id') . ' = ' . $db->quoteName('cl.library_id'));
    $query->join('INNER', $db->quoteName('ic_h5p_contents', 'c') . ' ON ' . $db->quoteName('cl.content_id') . ' = ' . $db->quoteName('c.id'));
    $query->where($db->quoteName('l.id') . ' = ' . $db->quote($id));
    $db->setQuery($query);
    $count_content = $db->loadResult();

    $query = $db->getQuery(true);
    $query->select('COUNT(*)');
    $query->from($db->quoteName('ic_h5p_libraries_libraries'));
    $query->where($db->quoteName('required_library_id') . ' = ' . $db->quote($id));
    $db->setQuery($query);
    $count_libraries = $db->loadResult();
    



    return array(
      'content' => $skipContent ? -1 : intval($count_content),
      'libraries' => intval($count_libraries)
    );
  }

  /**
   * Implements saveLibraryData
   */


  public function saveLibraryData(&$library, $new = TRUE) {


    $db = JFactory::getDbo();
    
    $preloadedJs = $this->pathsToCsv($library, 'preloadedJs');
    $preloadedCss =  $this->pathsToCsv($library, 'preloadedCss');
    $dropLibraryCss = '';

    if (isset($library['dropLibraryCss'])) {
      $libs = array();
      foreach ($library['dropLibraryCss'] as $lib) {
        $libs[] = $lib['machineName'];
      }
      $dropLibraryCss = implode(', ', $libs);
    }

    $embedTypes = '';
    if (isset($library['embedTypes'])) {
      $embedTypes = implode(', ', $library['embedTypes']);
    }
    if (!isset($library['semantics'])) {
      $library['semantics'] = '';
    }
    if (!isset($library['fullscreen'])) {
      $library['fullscreen'] = 0;
    }
    if (!isset($library['hasIcon'])) {
      $library['hasIcon'] = 0;
    }

    $addTo = isset($library['addTo']) ? json_encode($library['addTo']) : NULL;
    $hasIcon = $library['hasIcon'] ? 1 : 0;

    if ($new) {
     
      $query = "INSERT INTO `ic_h5p_libraries` SET 
                name = ".$db->quote($library['machineName']).",
                title = ".$db->quote($library['title']).",
                major_version = ".$db->quote($library['majorVersion']).",
                minor_version = ".$db->quote($library['minorVersion']).",
                patch_version = ".$db->quote($library['patch_version']).",
                runnable = ".$db->quote($library['runnable']).",
                fullscreen = ".$db->quote($library['fullscreen']).",
                embed_types = ".$db->quote($embedTypes).",
                preloaded_js = ".$db->quote($preloadedJs).",
                preloaded_css = ".$db->quote($preloadedCss).",
                drop_library_css = ".$db->quote($dropLibraryCss).",
                tutorial_url = '',
                semantics = ".$db->quote($library['semantics']).",
                metadata_settings = ".$db->quote($library['metadataSettings']).",
                add_to = ".$db->quote($addTo).",
                has_icon = ".$db->quote($hasIcon);

      $db->setQuery($query);
      $db->execute();       
      //echo $db->insertid();

      $library['libraryId'] = $db->insertid();

    }
    else {
      
      $query = "UPDATE `ic_h5p_libraries` SET 
                title = ".$db->quote($library['title']).", 
                patch_version = ".$db->quote($library['patchVersion']).", 
                runnable = ".$db->quote($library['runnable']).", 
                fullscreen = ".$db->quote($library['fullscreen']).", 
                embed_types = ".$db->quote($embedTypes).", 
                preloaded_js = ".$db->quote($preloadedJs).", 
                preloaded_css = ".$db->quote($preloadedCss).", 
                embed_types = ".$db->quote($embedTypes).", 
                preloaded_js = ".$db->quote($preloadedJs).", 
                preloaded_css = ".$db->quote($preloadedCss).", 
                drop_library_css = ".$db->quote($dropLibraryCss).", 
                semantics = ".$db->quote($library['semantics']).", 
                metadata_settings = ".$db->quote($library['metadataSettings']).", 
                add_to = ".$db->quote($addTo).", 
                has_icon = ".$db->quote($hasIcon)." WHERE id = ".$db->quote($library['libraryId']);


      $db->setQuery($query);
      $db->execute(); 

      $this->deleteLibraryDependencies($library['libraryId']);
      
    }
   
    
    new H5P_Event('library', ($new ? 'create' : 'update'),
        NULL, NULL,
        $library['machineName'], $library['majorVersion'] . '.' . $library['minorVersion']);
    
    

    $query = "DELETE FROM `ic_h5p_libraries_languages` WHERE library_id = ".$db->quote($library['libraryId']);
    $db->setQuery($query);
    $result = $db->execute();  

    if (isset($library['language'])) {
      foreach ($library['language'] as $languageCode => $translation) {
      
     $query = "INSERT INTO `ic_h5p_libraries_languages` SET 
              library_id = ".$db->quote($library['libraryId']).",
              language_code = ".$db->quote($languageCode).",
              translation = ".$db->quote($translation);

      $db->setQuery($query);
      $result = $db->execute();

      }
    }
  }

  /**
   * Convert list of file paths to csv
   *
   * @param array $library
   *  Library data as found in library.json files
   * @param string $key
   *  Key that should be found in $libraryData
   * @return string
   *  file paths separated by ', '
   */
  private function pathsToCsv($library, $key) {
    if (isset($library[$key])) {
      $paths = array();
      foreach ($library[$key] as $file) {
        $paths[] = $file['path'];
      }
      return implode(', ', $paths);
    }
    return '';
  }

  /**
   * Implements deleteLibraryDependencies
   */
  
  public function deleteLibraryDependencies($id) {

    $db = JFactory::getDbo();

    $query = $db->getQuery(true);
    $query = "DELETE FROM `ic_h5p_libraries_libraries` WHERE library_id = '".$id."'";
    $db->setQuery($query);
    $result = $db->execute(); 


  }

  /**
   * Implements deleteLibrary
   */
  

  public function deleteLibrary($library) {
    $db = JFactory::getDbo();
   

    // *****Delete library files
    H5PCore::deleteFileTree($this->getH5pPath() . '/' . $library->name . '-' . $library->major_version . '.' . $library->minor_version);

    // Remove library data from database
    $query = $db->getQuery(true);
    $conditions = array(
      $db->quoteName('library_id') . ' = ' . $db->quote($library->id)
    );
    $query->delete($db->quoteName('ic_h5p_libraries_libraries'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute(); 


    $query = $db->getQuery(true);
    $conditions = array(
      $db->quoteName('library_id') . ' = ' . $db->quote($library->id)
    );
    $query->delete($db->quoteName('ic_h5p_libraries_languages'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute(); 


    $query = $db->getQuery(true);
    $conditions = array(
      $db->quoteName('id') . ' = ' . $db->quote($library->id)
    );
    $query->delete($db->quoteName('ic_h5p_libraries'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute(); 
  }

  /**
   * Implements saveLibraryDependencies
   */
 

  public function saveLibraryDependencies($id, $dependencies, $dependencyType) {
    $db = JFactory::getDbo();
    

    foreach ($dependencies as $dependency) {

      
      $query = "INSERT INTO `ic_h5p_libraries_libraries` (library_id, required_library_id, dependency_type) 
      SELECT '".$id."', hl.id, '".$dependencyType."'   
      FROM `ic_h5p_libraries` hl
      WHERE name = '".$dependency['machineName']."'
      AND major_version = '".$dependency['majorVersion']."'
      AND minor_version = '".$dependency['minorVersion']."'
      ON DUPLICATE KEY UPDATE dependency_type = '".$dependencyType."'"; 

      $db->setQuery($query);
      $result = $db->execute();


    }
  }

  /**
   * Implements updateContent
   */
  

  public function updateContent($content, $contentMainId = NULL) {
    
    $db = JFactory::getDbo();
   
    $metadata = (array)$content['metadata'];
    $current_date_time = date('Y-m-d H:i:s');

    $format = array();
    $data = array_merge(\H5PMetadata::toDBArray($metadata, true, true, $format), array(
      'updated_at' => $current_date_time,
      'parameters' => $content['params'],
      'embed_type' => 'div', // TODO: Determine from library?
      'library_id' => $content['library']['libraryId'],
      'filtered' => '',
      'disable' => $content['disable']
    ));

   
    if (!isset($content['id'])) {
      
      $current_user = JFactory::getUser();
      $current_user_id = $current_user->id;

    
      $query = "INSERT INTO `ic_h5p_contents` SET ";
      $i=0;
      foreach ($data as $key => $value) {
          if($i==0)
          {
            $query.= $key.' = '. $db->quote($value);
          } else {
            $query.= ' , '.$key.' = '. $db->quote($value);
          }
          
          $i++;
      }

      $query.= ' , created_at = '. $db->quote($current_date_time);
      $query.= ' , user_id = '. $db->quote($current_user_id);

      $db->setQuery($query);
      $db->execute();       
      
      $content['id'] = $db->insertid();
      
      $event_type = 'create';
    }
    else {
      
      $query = "UPDATE `ic_h5p_contents` SET ";
      $i=0;
      foreach ($data as $key => $value) {
          if($i==0)
          {
            $query.= $key.' = '. $db->quote($value);
          } else {
            $query.= ' , '.$key.' = '. $db->quote($value);
          }
          
          $i++;
      }

      $query .= " WHERE id = ".$db->quote($content['id']);     
  
      $db->setQuery($query);
      $db->execute();  
      $event_type = 'update';

    }

    // Log content create/update/upload
    if (!empty($content['uploaded'])) {
      $event_type .= ' upload';
    }
    new H5P_Event('content', $event_type,
        $content['id'],
        $metadata['title'],
        $content['library']['machineName'],
        $content['library']['majorVersion'] . '.' . $content['library']['minorVersion']);

    return $content['id'];
  }


  /**
   * Implements insertContent
   */
  public function insertContent($content, $contentMainId = NULL) {
    return $this->updateContent($content);
  }

  /**
   * Implement getWhitelist
   */
  public function getWhitelist($isLibrary, $defaultContentWhitelist, $defaultLibraryWhitelist) {
    // TODO: Get this value from a settings page.
    $whitelist = $defaultContentWhitelist;
    if ($isLibrary) {
      $whitelist .= ' ' . $defaultLibraryWhitelist;
    }
    return $whitelist;
  }

  /**
   * Implements copyLibraryUsage
   */
 

   public function copyLibraryUsage($contentId, $copyFromId, $contentMainId = NULL) {

    $db = Jfactory::getDbo();
    
    $query = $db->getQuery(true);
    $columns = array('updated_at', 'parameters', 'embed_type', 'library_id','filtered','disable');
    $values = array($db->quote($data['updated_at']), $db->quote($data['parameters']), $db->quote($data['embed_type']), $db->quote($data['library_id']), $db->quote($data['filtered']), $db->quote($data['disable']));
    $query->insert($db->quoteName($table))
              ->columns($db->quoteName($columns))
              ->values(implode(',', $values));
    $db->setQuery($query);
    $db->execute();  
  

  }

  /**
   * Implements deleteContentData
   */
 

  public function deleteContentData($id) {
    $db = Jfactory::getDbo();

    // Remove content data and library usage
    $query = $db->getQuery(true);
    $conditions = array(
          $db->quoteName('id') . ' = ' . $db->quote($id)
      );

    $query->delete($db->quoteName('ic_h5p_contents'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute();


    $this->deleteLibraryUsage($id);

    // Remove user scores/results
    $query = $db->getQuery(true);
    $conditions = array(
          $db->quoteName('content_id') . ' = ' . $db->quote($id)
      );

    $query->delete($db->quoteName('ic_h5p_results'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute();

    // Remove contents user/usage data
    $query = $db->getQuery(true);
    $conditions = array(
          $db->quoteName('content_id') . ' = ' . $db->quote($id)
      );

    $query->delete($db->quoteName('ic_h5p_contents_user_data'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute();

  }

  /**
   * Implements deleteLibraryUsage
   */
 

  public function deleteLibraryUsage($contentId) {
    $db = Jfactory::getDbo();

    $query = $db->getQuery(true);
    $conditions = array(
          $db->quoteName('content_id') . ' = ' . $db->quote($contentId)
      );
    $query->delete($db->quoteName('ic_h5p_contents_libraries'));
    $query->where($conditions);
    $db->setQuery($query);
    $result = $db->execute();

  }

  /**
   * Implements saveLibraryUsage
   */
  

  public function saveLibraryUsage($contentId, $librariesInUse) {
    $db = Jfactory::getDbo();

    $dropLibraryCssList = array();
    foreach ($librariesInUse as $dependency) {
      if (!empty($dependency['library']['dropLibraryCss'])) {
        $dropLibraryCssList = array_merge($dropLibraryCssList, explode(', ', $dependency['library']['dropLibraryCss']));
      }
    }

    foreach ($librariesInUse as $dependency) {
      $dropCss = in_array($dependency['library']['machineName'], $dropLibraryCssList) ? 1 : 0;
     $query = "INSERT INTO `ic_h5p_contents_libraries` 
                SET content_id = ".$db->quote($contentId). 
                ", library_id = ".$db->quote($dependency['library']['libraryId']). 
                ", dependency_type = ".$db->quote($dependency['type']). 
                ", drop_css = ".$db->quote($dropCss). 
                ", weight = ".$db->quote($dependency['weight']);             
      $db->setQuery($query);
      $db->execute();    

    }
  }

  /**
   * Implements loadLibrary
   */
  public function loadLibrary($name, $majorVersion, $minorVersion) {

    $db = Jfactory::getDbo();

    $query = "SELECT id as libraryId, name as machineName, title, major_version as majorVersion, minor_version as minorVersion, patch_version as patchVersion,
          embed_types as embedTypes, preloaded_js as preloadedJs, preloaded_css as preloadedCss, drop_library_css as dropLibraryCss, fullscreen, runnable,
          semantics, has_icon as hasIcon
        FROM `ic_h5p_libraries`
        WHERE name = ".$db->quote($name)."
        AND major_version = ".$db->quote($majorVersion)."
        AND minor_version =".$db->quote($minorVersion);

    $db->setQuery($query);
    $library = $db->loadAssoc();

   
     $query = "SELECT hl.name as machineName, hl.major_version as majorVersion, hl.minor_version as minorVersion, hll.dependency_type as dependencyType 
        FROM `ic_h5p_libraries_libraries` hll
        JOIN `ic_h5p_libraries` hl ON hll.required_library_id = hl.id
        WHERE hll.library_id = ".$db->quote($library['libraryId']);

    $db->setQuery($query);
    $dependencies = $db->loadObjectList();

    foreach ($dependencies as $dependency) {
      $library[$dependency->dependencyType . 'Dependencies'][] = array(
        'machineName' => $dependency->machineName,
        'majorVersion' => $dependency->majorVersion,
        'minorVersion' => $dependency->minorVersion,
      );
    }
    
    if ($this->isInDevMode()) {
      $semantics = $this->getSemanticsFromFile($library['machineName'], $library['majorVersion'], $library['minorVersion']);
      if ($semantics) {
        $library['semantics'] = $semantics;
      }
    }
    return $library;
  }

  private function getSemanticsFromFile($name, $majorVersion, $minorVersion) {
    $semanticsPath = $this->getH5pPath() . '/libraries/' . $name . '-' . $majorVersion . '.' . $minorVersion . '/semantics.json';
    if (file_exists($semanticsPath)) {
      $semantics = file_get_contents($semanticsPath);
      if (!json_decode($semantics, TRUE)) {
        $this->setErrorMessage($this->t('Invalid json in semantics for %library', array('%library' => $name)));
      }
      return $semantics;
    }
    return FALSE;
  }

  /**
   * Implements loadLibrarySemantics
   */
  public function loadLibrarySemantics($name, $majorVersion, $minorVersion) {
    
    global $wpdb;

    if ($this->isInDevMode()) {
      $semantics = $this->getSemanticsFromFile($name, $majorVersion, $minorVersion);
    }
    else {

    $db = Jfactory::getDbo();
    $query = $db->getQuery(true);
    $query = "SELECT semantics 
          FROM `ic_h5p_libraries` 
          WHERE name = ".$db->quote($name)."  
          AND major_version = ".$db->quote($majorVersion)." 
          AND minor_version = ".$db->quote($minorVersion);

    $db->setQuery($query);
    $semantics = $db->loadResult();

    }

    return ($semantics === FALSE ? NULL : $semantics);
    
  }

  /**
   * Implements alterLibrarySemantics
   */
  public function alterLibrarySemantics(&$semantics, $name, $majorVersion, $minorVersion) {
    /**
     * Allows you to alter the H5P library semantics, i.e. changing how the
     * editor looks and how content parameters are filtered.
     *
     * @since 1.5.3
     *
     * @param object &$semantics
     * @param string $libraryName
     * @param int $libraryMajorVersion
     * @param int $libraryMinorVersion
     */
   /* do_action_ref_array('h5p_alter_library_semantics', array(&$semantics, $name, $majorVersion, $minorVersion)); */
  }

  /**
   * Implements loadContent
   */
  public function loadContent($id, $archiveVersionNumber = null) {

    $db = Jfactory::getDbo();

    $conditions = [];
    $queryColumns = [
        'hc.id',
        'hc.title',
        'hc.parameters AS params',
        'hc.filtered',
        'hc.slug AS slug', 
        'hc.user_id', 
        'hc.embed_type AS embedType', 
        'hc.disable',
        'hl.id AS libraryId', 
        'hl.name AS libraryName', 
        'hl.major_version AS libraryMajorVersion', 
        'hl.minor_version AS libraryMinorVersion', 
        'hl.embed_types AS libraryEmbedTypes', 
        'hl.fullscreen AS libraryFullscreen', 
        'hc.authors AS authors', 
        'hc.source AS source', 
        'hc.year_from AS yearFrom', 
        'hc.year_to AS yearTo', 
        'hc.license AS license', 
        'hc.license_version AS licenseVersion',
        'hc.license_extras AS licenseExtras', 
        'hc.author_comments AS authorComments', 
        'hc.changes AS changes', 
        'hc.default_language AS defaultLanguage'
    ];

    if (
        (!is_null($archiveVersionNumber)) &&
        $archiveVersionNumber !== -1
    ) {

        $table = '`ic_h5p_contents_archive` hc';
        $conditions[] = "hc.content_id = $db->quote($id)";
        $conditions[] = "hc.version_number = $archiveVersionNumber";

        $queryColumns[] = 'hc.content_id';
    } else {

        $table = '`ic_h5p_contents` hc';
        $conditions[] = "hc.id = $db->quote($id)";
    }

    $whereConditions = implode(' AND ', $conditions);
    $columns = implode(',', $queryColumns);

    $query = "
        SELECT $columns
        FROM $table
        JOIN `ic_h5p_libraries` hl ON hl.id = hc.library_id
        WHERE $whereConditions
    ";

    $db->setQuery($query);
    $content = $db->loadAssoc();

    if ($content !== NULL) {
      $content['metadata'] = array();
      $metadata_structure = array('title', 'authors', 'source', 'yearFrom', 'yearTo', 'license', 'licenseVersion', 'licenseExtras', 'authorComments', 'changes', 'defaultLanguage');
      foreach ($metadata_structure as $property) {
        if (!empty($content[$property])) {
          if ($property === 'authors' || $property === 'changes') {
            $content['metadata'][$property] = json_decode($content[$property]);
          }
          else {
            $content['metadata'][$property] = $content[$property];
          }
          if ($property !== 'title') {
            unset($content[$property]); // Unset all except title
          }
        }
      }
    }

    return $content;
    
  }

  /**
   * Implements loadContentDependencies
   */
  public function loadContentDependencies($id, $type = NULL) {

    $db = Jfactory::getDbo();

    $query =
        "SELECT hl.id
              , hl.name AS machineName
              , hl.major_version AS majorVersion
              , hl.minor_version AS minorVersion
              , hl.patch_version AS patchVersion
              , hl.preloaded_css AS preloadedCss
              , hl.preloaded_js AS preloadedJs
              , hcl.drop_css AS dropCss
              , hcl.dependency_type AS dependencyType
        FROM `ic_h5p_contents_libraries` hcl
        JOIN `ic_h5p_libraries` hl ON hcl.library_id = hl.id
        WHERE hcl.content_id = ".$db->quote($id);

    
    if ($type !== NULL) {
      $query .= " AND hcl.dependency_type = ". $db->quote($type);
    }

    $query .= " ORDER BY hcl.weight";
    $db->setQuery($query);
    $results = $db->loadAssocList();

    return $results; 
  }

  /**
   * Implements getOption().
   */

  /*
  public function getOption($name, $default = FALSE) {
    if ($name === 'site_uuid') {
      $name = 'h5p_site_uuid'; // Make up for old core bug
    }
    return get_option('h5p_' . $name, $default);
  }
  */

  public function getOption($name, $default = FALSE) {
    if ($name === 'site_uuid') {
      $name = 'h5p_site_uuid'; // Make up for old core bug
    }
    //return get_option('h5p_' . $name, $default);
    
  }


  /**
   * Implements setOption().
   */
  public function setOption($name, $value) {
    
    if ($name === 'site_uuid') {
      $name = 'h5p_site_uuid'; // Make up for old core bug
    }
    $var = $this->getOption($name);
    $name = 'h5p_' . $name; // Always prefix to avoid conflicts
    if ($var === FALSE) {
      add_option($name, $value);
    }
    else {
      update_option($name, $value);
    }
  }

  /**
   * Convert variables to fit our DB.
   */
  private static function camelToString($input) {
    $input = preg_replace('/[a-z0-9]([A-Z])[a-z0-9]/', '_$1', $input);
    return strtolower($input);
  }

  /**
   * Implements setFilteredParameters().
   */
  public function updateContentFields($id, $fields) {
    
    $db = Jfactory::getDbo();

    $processedFields = array();
    $format = array();
    foreach ($fields as $name => $value) {

      $query = "UPDATE `ic_h5p_contents` SET ".self::camelToString($name)." = ".$db->quote($value)." WHERE id = ".$db->quote($id);
      $db->setQuery($query);
      $db->execute();

    }

      
  }

  /**
   * Implements clearFilteredParameters().
   */
  public function clearFilteredParameters($library_ids) {
    
    $db = Jfactory::getDbo();
   
    $query = "UPDATE `ic_h5p_contents` SET filtered = NULL 
        WHERE library_id IN (".implode(',', $library_ids).")";
    $db->setQuery($query);
    $db->execute();
    
  }

  /**
   * Implements getNumNotFiltered().
   */
  public function getNumNotFiltered() {
    /*
    global $wpdb;

    return (int) $wpdb->get_var(
      "SELECT COUNT(id)
        FROM {$wpdb->prefix}h5p_contents
        WHERE filtered = ''"
    );
    */
  }

  /**
   * Implements getNumContent().
   */
  public function getNumContent($library_id, $skip = NULL) {
    /*
    global $wpdb;
    $skip_query = empty($skip) ? '' : " AND id NOT IN ($skip)";

    return (int) $wpdb->get_var($wpdb->prepare(
      "SELECT COUNT(id)
         FROM {$wpdb->prefix}h5p_contents
        WHERE library_id = %d
              {$skip_query}",
      $library_id
    ));
    */
  }


  /**
   * Implements loadLibraries.
   */
  

  public function loadLibraries() {
    $db = Jfactory::getDbo();
    
    $query = "SELECT id, name, title, major_version, minor_version, patch_version, runnable, restricted FROM `ic_h5p_libraries` ORDER BY title ASC, major_version ASC, minor_version ASC";
    $db->setQuery($query);
    $results = $db->loadObjectList();


    $libraries = array();
    foreach ($results as $library) {
      $libraries[$library->name][] = $library;
    }

    return $libraries;
  }

  /**
   * Implements getAdminUrl.
   */
  public function getAdminUrl() {

  }

  /**
   * Implements getPlatformInfo
   */
  public function getPlatformInfo() {
    global $wp_version;

    return array(
      'name' => 'Joomla',
      'version' => $wp_version,
      'h5pVersion' => H5P_Plugin::VERSION
    );
  }

  /**
   * Implements fetchExternalData
   */

  public function fetchExternalData($url, $data = NULL, $blocking = TRUE, $stream = NULL) {
    @set_time_limit(0);
    $options = array(
      'timeout' => !empty($blocking) ? 30 : 0.01,
      'stream' => !empty($stream),
      'filename' => !empty($stream) ? $stream : FALSE
    );

    if ($data !== NULL) {

      
      // Post
      $options['body'] = $data;
       /* $response = wp_remote_post($url, $options); */
       /* die('1'); */ 

      $i=0;
      $options_body = '';
      foreach($options['body'] as $key => $value) {
        if($i==0)
        {
          $options_body .= $key.'='.$value;  
        }
        else
        {
          $options_body .= '&'.$key.'='.$value;   
        }
        $i++;
      }
      
      
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,            $url );
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
      curl_setopt($ch, CURLOPT_POST,           1 );
      curl_setopt($ch, CURLOPT_POSTFIELDS,     $options_body); 
      curl_setopt($ch, CURLOPT_HTTPHEADER,     array("Content-Type: application/json")); 
      

      $response=curl_exec ($ch);
      $response_error = curl_error($ch);
      $curl_getinfo_code  = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
      
     
    }
    else {

      
      // Get

      if (empty($options['filename'])) {
        // Support redirects
        
        /* $response = wp_remote_get($url); */

        die('2');
       
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $url );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_POST,           false );
        curl_setopt($ch, CURLOPT_HTTPHEADER,     array("Content-Type: application/json")); 
        $response=curl_exec ($ch);
        $response_error = curl_error($ch);
        $curl_getinfo_code  = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        
         
      }
      else {
       
        // Use safe when downloading files
        /* $response = wp_safe_remote_get($url, $options); */

        /* die('3');*/

        /* MY ADDED START */
        
        $headers = array();

        $curl = curl_version();
        $this->version = $curl['version_number'];
        $this->handle = curl_init();

        curl_setopt($this->handle, CURLOPT_HEADER, false);
        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, 1);
        if ($this->version >= self::CURL_7_10_5) {
          curl_setopt($this->handle, CURLOPT_ENCODING, '');
        }
        /*
        if (defined('CURLOPT_PROTOCOLS')) {
          curl_setopt($this->handle, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        }
        if (defined('CURLOPT_REDIR_PROTOCOLS')) {
          curl_setopt($this->handle, CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
        }
        */

        $this->setup_handle($url, $headers, $data, $options);
        $response = curl_exec($this->handle);

        if ($options['filename'] !== false) {
          $this->stream_handle = fopen($options['filename'], 'wb');
        }
        fwrite($this->stream_handle, $response);
        $response_error = curl_error($this->handle);
        $curl_getinfo_code  = curl_getinfo($this->handle, CURLINFO_HTTP_CODE );
      
       
        /* MY ADDED END */

       
      }
    }

    if (!empty($response_error)) {
      $this->setErrorMessage($response->get_error_message(), 'failed-fetching-external-data');
      return FALSE;
    }
    elseif ($curl_getinfo_code === 200) {
      return empty($response) ? TRUE : $response;
    }

    return NULL;
  }


 function setup_handle($url, $headers, $data, $options) {
    //$options['hooks']->dispatch('curl.before_request', array(&$this->handle));

    // Force closing the connection for old versions of cURL (<7.22).
    if ( ! isset( $headers['Connection'] ) ) {
      $headers['Connection'] = 'close';
    }

    $headers = Requests::flatten($headers);
    /*
    if (!empty($data)) {
      $data_format = $options['data_format'];

      if ($data_format === 'query') {
        $url = self::format_get($url, $data);
        $data = '';
      }
      elseif (!is_string($data)) {
        $data = http_build_query($data, null, '&');
      }
    }
    */
    

    switch ($options['type']) {
      case Requests::POST:
        curl_setopt($this->handle, CURLOPT_POST, true);
        curl_setopt($this->handle, CURLOPT_POSTFIELDS, $data);
        break;
      case Requests::HEAD:
        curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, $options['type']);
        curl_setopt($this->handle, CURLOPT_NOBODY, true);
        break;
      case Requests::TRACE:
        curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, $options['type']);
        break;
      case Requests::PATCH:
      case Requests::PUT:
      case Requests::DELETE:
      case Requests::OPTIONS:
      default:
        curl_setopt($this->handle, CURLOPT_CUSTOMREQUEST, $options['type']);
        if (!empty($data)) {
          curl_setopt($this->handle, CURLOPT_POSTFIELDS, $data);
        }
    }

    // cURL requires a minimum timeout of 1 second when using the system
    // DNS resolver, as it uses `alarm()`, which is second resolution only.
    // There's no way to detect which DNS resolver is being used from our
    // end, so we need to round up regardless of the supplied timeout.
    //
    // https://github.com/curl/curl/blob/4f45240bc84a9aa648c8f7243be7b79e9f9323a5/lib/hostip.c#L606-L609
    $timeout = max($options['timeout'], 1);

    if (is_int($timeout) || $this->version < self::CURL_7_16_2) {
      curl_setopt($this->handle, CURLOPT_TIMEOUT, ceil($timeout));
    }
    else {
      curl_setopt($this->handle, CURLOPT_TIMEOUT_MS, round($timeout * 1000));
    }

    if (is_int($options['connect_timeout']) || $this->version < self::CURL_7_16_2) {
      curl_setopt($this->handle, CURLOPT_CONNECTTIMEOUT, ceil($options['connect_timeout']));
    }
    else {
      curl_setopt($this->handle, CURLOPT_CONNECTTIMEOUT_MS, round($options['connect_timeout'] * 1000));
    }
    curl_setopt($this->handle, CURLOPT_URL, $url);
    curl_setopt($this->handle, CURLOPT_REFERER, $url);
    curl_setopt($this->handle, CURLOPT_USERAGENT, $options['useragent']);
    if (!empty($headers)) {
      curl_setopt($this->handle, CURLOPT_HTTPHEADER, $headers);
    }
    if ($options['protocol_version'] === 1.1) {
      curl_setopt($this->handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
    }
    else {
      curl_setopt($this->handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
    }

    if (true === $options['blocking']) {
      curl_setopt($this->handle, CURLOPT_HEADERFUNCTION, array(&$this, 'stream_headers'));
      curl_setopt($this->handle, CURLOPT_WRITEFUNCTION, array(&$this, 'stream_body'));
      curl_setopt($this->handle, CURLOPT_BUFFERSIZE, Requests::BUFFER_SIZE);
    }
  }

  public function stream_body($handle, $data) {
    /*
    $this->hooks->dispatch('request.progress', array($data, $this->response_bytes, $this->response_byte_limit));
    $data_length = strlen($data);

    // Are we limiting the response size?
    if ($this->response_byte_limit) {
      if ($this->response_bytes === $this->response_byte_limit) {
        // Already at maximum, move on
        return $data_length;
      }

      if (($this->response_bytes + $data_length) > $this->response_byte_limit) {
        // Limit the length
        $limited_length = ($this->response_byte_limit - $this->response_bytes);
        $data = substr($data, 0, $limited_length);
      }
    }
    */

    if ($this->stream_handle) {
      fwrite($this->stream_handle, $data);
    }
    else {
      $this->response_data .= $data;
    }

    $this->response_bytes += strlen($data);
    return $data_length;
  }

  public function stream_headers($handle, $headers) {
    // Why do we do this? cURL will send both the final response and any
    // interim responses, such as a 100 Continue. We don't need that.
    // (We may want to keep this somewhere just in case)
    if ($this->done_headers) {
      $this->headers = '';
      $this->done_headers = false;
    }
    $this->headers .= $headers;

    if ($headers === "\r\n") {
      $this->done_headers = true;
    }
    return strlen($headers);
  }

  /**
   * Implements setLibraryTutorialUrl
   */
  
  public function setLibraryTutorialUrl($library_name, $url) {

    $db = Jfactory::getDbo();

    $query = "UPDATE `ic_h5p_libraries` SET tutorial_url = '".$url."', name = '".$library_name."'";
    $db->setQuery($query);
    $result = $db->execute();
  }

  /**
   * Implements resetContentUserData
   */
  public function resetContentUserData($contentId) {
    
    $db = Jfactory::getDbo();
    $current_date_time = date('Y-m-d H:i:s');
    // Reset user datas for this content
    
    $query = "UPDATE `ic_h5p_contents_user_data` SET updated_at = ".$db->quote($current_date_time).", data = 'RESET' WHERE content_id = ".$db->quote($contentId)." AND invalidate = 1";
    $db->setQuery($query);
    $result = $db->execute();
    
  }

  /**
   * Implements isContentSlugAvailable
   */
  public function isContentSlugAvailable($slug) {
    
    $db = Jfactory::getDbo();
    $query = "SELECT slug FROM `ic_h5p_contents` WHERE slug = ".$db->quote($slug);
    $db->setQuery($query);
    $result = $db->loadResult();
    return !$result;
  }

  /**
   * Implements getLibraryContentCount
   */
  

  public function getLibraryContentCount() {

    $db = Jfactory::getDbo();
    $count = array();

    // Find number of content per library
    
    
    $query = "SELECT l.name, l.major_version, l.minor_version, COUNT(*) AS count FROM `ic_h5p_contents` c, `ic_h5p_libraries` l WHERE c.library_id = l.id GROUP BY l.name, l.major_version, l.minor_version";

    $db->setQuery($query);
    $results = $db->loadObjectList();

    // Extract results
    foreach($results as $library) {
      $count[$library->name . ' ' . $library->major_version . '.' . $library->minor_version] = $library->count;
    }
    return $count;
  }

  /**
   * Implements getLibraryStats
   */
  /*
  public function getLibraryStats($type) {
    global $wpdb;
    $count = array();

    $results = $wpdb->get_results($wpdb->prepare("
        SELECT library_name AS name,
               library_version AS version,
               num
          FROM {$wpdb->prefix}h5p_counters
         WHERE type = %s
        ", $type));

    // Extract results
    foreach($results as $library) {
      $count[$library->name . ' ' . $library->version] = $library->num;
    }

    return $count;
  }
  */

  public function getLibraryStats($type) {
    $db = Jfactory::getDbo();
    $count = array();
   
    $query = "SELECT library_name AS name, library_version AS version, num FROM ic_h5p_counters  WHERE type = '".$type."'";
    $db->setQuery($query);
    $results = $db->loadObjectList();

    // Extract results
    foreach($results as $library) {
      $count[$library->name . ' ' . $library->version] = $library->num;
    }

    return $count;
  }

  /**
   * Implements getNumAuthors
   */
  public function getNumAuthors() {
    /*
    global $wpdb;
    return $wpdb->get_var("
        SELECT COUNT(DISTINCT user_id)
          FROM {$wpdb->prefix}h5p_contents
    ");
    */
  }

  // Magic stuff not used, we do not support library development mode.
  public function lockDependencyStorage() {}
  public function unlockDependencyStorage() {}

  

  /**
   * Implements deleteCachedAssets
   */
  public function deleteCachedAssets($library_id) {
    
    $db = Jfactory::getDbo();

    // Get all the keys so we can remove the files
    $query = "SELECT hash FROM `ic_h5p_libraries_cachedassets` WHERE library_id = '".$library_id."'";
    $db->setQuery($query);
    $results = $db->loadObjectList();

    // Remove all invalid keys
    $hashes = array();
    foreach ($results as $key) {
      $hashes[] = $key->hash;
      $query = "SELECT hash FROM `ic_h5p_libraries_cachedassets` WHERE hash = '".$key->hash."'";
      $db->setQuery($query);
      $db->execute();

    }

    return $hashes;
    
  }

  /**
   * Implements afterExportCreated
   */
  public function afterExportCreated($content, $filename) {
    // Clear cached value for dirsize.
    /* delete_transient('dirsize_cache'); */
  }

  /**
   * Check if current user can edit H5P
   *
   * @method currentUserCanEdit
   * @param  int             $contentUserId
   * @return boolean
   */
  private static function currentUserCanEdit ($contentUserId) {
    /*
    if (current_user_can('edit_others_h5p_contents')) {
      return TRUE;
    }
    return get_current_user_id() == $contentUserId;
    */
    return TRUE;
  }

  /**
   * Implements hasPermission
   *
   * @method hasPermission
   * @param  H5PPermission    $permission
   * @param  int              $contentUserId
   * @return boolean
   */
  public function hasPermission($permission, $contentUserId = NULL) {
    switch ($permission) {
      case H5PPermission::DOWNLOAD_H5P:
      case H5PPermission::EMBED_H5P:
      case H5PPermission::COPY_H5P:
        return self::currentUserCanEdit($contentUserId);

      case H5PPermission::CREATE_RESTRICTED:
      case H5PPermission::UPDATE_LIBRARIES:
        /* return current_user_can('manage_h5p_libraries'); */
        return TRUE;

      case H5PPermission::INSTALL_RECOMMENDED:
        /* current_user_can('install_recommended_h5p_libraries'); */
        return TRUE;
    }
    return FALSE;
  }

  /**
   * Replaces existing content type cache with the one passed in
   *
   * @param object $contentTypeCache Json with an array called 'libraries'
   *  containing the new content type cache that should replace the old one.
   */
  public function replaceContentTypeCache($contentTypeCache) {
    /*
    global $wpdb;

    // Replace existing content type cache
    $wpdb->query("TRUNCATE TABLE {$wpdb->base_prefix}h5p_libraries_hub_cache");
    foreach ($contentTypeCache->contentTypes as $ct) {
      // Insert into db
      $wpdb->insert("{$wpdb->base_prefix}h5p_libraries_hub_cache", array(
        'machine_name'      => $ct->id,
        'major_version'     => $ct->version->major,
        'minor_version'     => $ct->version->minor,
        'patch_version'     => $ct->version->patch,
        'h5p_major_version' => $ct->coreApiVersionNeeded->major,
        'h5p_minor_version' => $ct->coreApiVersionNeeded->minor,
        'title'             => $ct->title,
        'summary'           => $ct->summary,
        'description'       => $ct->description,
        'icon'              => $ct->icon,
        'created_at'        => self::dateTimeToTime($ct->createdAt),
        'updated_at'        => self::dateTimeToTime($ct->updatedAt),
        'is_recommended'    => $ct->isRecommended === TRUE ? 1 : 0,
        'popularity'        => $ct->popularity,
        'screenshots'       => json_encode($ct->screenshots),
        'license'           => json_encode(isset($ct->license) ? $ct->license : array()),
        'example'           => $ct->example,
        'tutorial'          => isset($ct->tutorial) ? $ct->tutorial : '',
        'keywords'          => json_encode(isset($ct->keywords) ? $ct->keywords : array()),
        'categories'        => json_encode(isset($ct->categories) ? $ct->categories : array()),
        'owner'             => $ct->owner
      ), array(
        '%s',
        '%d',
        '%d',
        '%d',
        '%d',
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%d',
        '%d',
        '%d',
        '%d',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s',
        '%s'
      ));

    }
    */
  }

  /**
   * Convert datetime string to unix timestamp
   *
   * @param string $datetime
   * @return int unix timestamp
   */
  public static function dateTimeToTime($datetime) {
    $dt = new DateTime($datetime);
    return $dt->getTimestamp();
  }

  /**
   * Load addon libraries
   *
   * @return array
   */
  public function loadAddons() {
    
    $db = JFactory::getDbo();
    $query = "SELECT l1.id as libraryId, l1.name as machineName,
              l1.major_version as majorVersion, l1.minor_version as minorVersion,
              l1.patch_version as patchVersion, l1.add_to as addTo,
              l1.preloaded_js as preloadedJs, l1.preloaded_css as preloadedCss
        FROM `ic_h5p_libraries` AS l1
        LEFT JOIN `ic_h5p_libraries` AS l2
          ON l1.name = l2.name AND 
            (l1.major_version < l2.major_version OR 
              (l1.major_version = l2.major_version AND 
               l1.minor_version < l2.minor_version)) 
        WHERE l1.add_to IS NOT NULL AND l2.name IS NULL";
    
    $db->setQuery($query);
    $results = $db->loadAssocList();
    
    return $results;

    // NOTE: These are treated as library objects but are missing the following properties:
    // title, embed_types, drop_library_css, fullscreen, runnable, semantics, has_icon
  }

  /**
   * Implements getLibraryConfig
   *
   * @param array $libraries
   * @return array
   */
  public function getLibraryConfig($libraries = NULL) {
     return defined('H5P_LIBRARY_CONFIG') ? H5P_LIBRARY_CONFIG : NULL;
  }

  /**
   * Implements libraryHasUpgrade
   */
  public function libraryHasUpgrade($library) {
  
    $db = JFactory::getDbo();
    $query = "SELECT id 
          FROM `ic_h5p_libraries` 
          WHERE name = ".$db->quote($library['machineName'])." 
          AND (major_version > ".$db->quote($library['majorVersion'])." 
           OR (major_version = ".$db->quote($library['majorVersion'])."  AND minor_version > " .$db->quote($library['minorVersion'])." ))
        LIMIT 1";

    $db->setQuery($query);
    $results = $db->loadResult();

    $id = $results;
    return $id!==NULL;    
   
  }

  public function saveCachedAssets($key, $libraries) {
   
    $db = JFactory::getDbo();
    foreach ($libraries as $library) 
    {
      
      $library_id = isset($library['id']) ? $library['id']:$library['libraryId'];
      $query = $db->getQuery(true);
      $columns = array('library_id', 'hash');
      $values = array($db->quote($library_id), $db->quote($key));
      $query->insert($db->quoteName('ic_h5p_libraries_cachedassets'))
                    ->columns($db->quoteName($columns))
                    ->values(implode(',', $values));
      $db->setQuery($query);
      $db->execute();  
       

    }
    
  }
  

}
