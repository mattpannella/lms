<?php

/**
 * Makes it easy to track events throughout the H5P system.
 *
 * @package    H5P
 * @copyright  2016 Joubel AS
 * @license    MIT
 */
class H5P_Event extends H5PEventBase {
  private $user;

  /**
   * Adds event type, h5p library and timestamp to event before saving it.
   *
   * @param string $type
   *  Name of event to log
   * @param string $library
   *  Name of H5P library affacted
   */
  function __construct($type, $sub_type = NULL, $content_id = NULL, $content_title = NULL, $library_name = NULL, $library_version = NULL) {

    
    // Track the user who initiated the event as well
    
    /* $current_user = wp_get_current_user();*/
    $current_user = JFactory::getUser();
    $this->user = $current_user->id;

    parent::__construct($type, $sub_type, $content_id, $content_title, $library_name, $library_version);
    
    
  }

  /**
   * Store the event.
   */
  protected function save() {
   
    $db = JFactory::getDbo();

    // Get data in array format without NULL values
    $data = $this->getDataArray();
   
    // Add user
    $data['user_id'] = $this->user;
   

    // Insert into DB
    $query = "INSERT INTO `ic_h5p_events` SET ";
    $i=0;
    foreach($data as $key=>$val){
      $i++;
      if($i==1)
      {
        $query.= $key."=".$val;  
      }
      else
      {
        $query.= ",".$key."="."'".$val."'";
      }
      
    }

    $db->setQuery($query);
    $db->execute(); 

    $this->id = $wpdb->insert_id;
    return $this->id;
  }

  /**
   * Count number of events.
   */
  protected function saveStats() {

    $db = JFactory::getDbo();

    $type = $this->type . ' ' . $this->sub_type;
  
    $query = "SELECT num FROM `ic_h5p_counters` WHERE type='".$type."' AND library_name='".$this->library_name."' AND library_version='".$this->library_version."'";
    $db->setQuery($query);
    $current_num = $db->loadResult();
  
    if ($current_num === NULL) {
      
      $query = "INSERT INTO `ic_h5p_counters` SET 
              type = '".$type."' ,
              library_name = '".$this->library_name."',
              library_version = '".$this->library_version."',
              num = 1";
       $db->setQuery($query);
       $db->execute();   

    }
    else {

      // Update num+1
      $query = "UPDATE `ic_h5p_counters` SET num = num + 1
            WHERE type = '".$type."' 
              AND library_name = '".$this->library_name."' 
              AND library_version = '".$this->library_version."'";
       $db->setQuery($query);
       $db->execute();     

    }
  }
}
