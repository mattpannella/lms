<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * com_interactivecontent Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 * @since       0.0.9
 */
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
class InteractiveContentControllerInteractiveContent extends JControllerForm
{

  public function ajax_setFinished() {

    $db = Jfactory::getDbo();

    $content_id = filter_input(INPUT_POST, 'contentId', FILTER_VALIDATE_INT);
    $token = filter_input(INPUT_GET, 'token');

    if (!$content_id) {
      H5PCore::ajaxError('Invalid content');
      exit;
    }

    if ($token!='h5p_result') {
      H5PCore::ajaxError('Invalid security token');
      exit;
    }

    $current_time = date('Y-m-d H:i:s');

    $user = JFactory::getUser();
    $user_id = $user->id;
    
    $query = "SELECT id FROM `ic_h5p_results` WHERE user_id = ".$db->quote($user_id)." AND content_id = ".$db->quote($content_id)." AND archive_date IS NULL";
    $db->setQuery($query);
    $result_id = $db->loadResult();

    //$table = $wpdb->prefix . 'h5p_results';
    $data = array(
      'score' => filter_input(INPUT_POST, 'score', FILTER_VALIDATE_INT),
      'max_score' => filter_input(INPUT_POST, 'maxScore', FILTER_VALIDATE_INT),
      'opened' => filter_input(INPUT_POST, 'opened', FILTER_VALIDATE_INT),
      'finished' => filter_input(INPUT_POST, 'finished', FILTER_VALIDATE_INT),
      'time' => filter_input(INPUT_POST, 'time', FILTER_VALIDATE_INT)
    );

    if ($data['time'] === NULL) {
      $data['time'] = 0;
    }


    if (!$result_id) {

      // Insert new results

        $data['user_id'] = $user_id;
        $data['content_id'] = $content_id;

        $query = "INSERT INTO `ic_h5p_results` SET ";

        $i=0;
        foreach($data as $key=>$val)
        {
            if($i==0)
            {
                $query .= $key."=".$db->quote($val);
            }
            else
            {
                $query .= " , ".$key."=".$db->quote($val);
            }

            $i++;
        }

        $db->setQuery($query);
        $db->execute();

    }
    else {

      // Update existing results

        $query = "UPDATE `ic_h5p_results` SET ";

        $i=0;
        foreach($data as $key=>$val)
        {
            if($i==0)
            {
                $query .= $key."=".$db->quote($val);
            }
            else
            {
                $query .= " , ".$key."=".$db->quote($val);
            }

            $i++;
        }

        $query .= " WHERE id=".$db->quote($result_id);

        $db->setQuery($query);
        $db->execute();

    }

    // Get content info for log
     $query = "SELECT c.title, l.name, l.major_version, l.minor_version
          FROM `ic_h5p_contents` c
          JOIN `ic_h5p_libraries` l ON l.id = c.library_id
         WHERE c.id = ".$db->quote($content_id);

    $db->setQuery($query);
    $content = $db->loadObject();

    // Log view
    new H5P_Event('results', 'set',
        $content_id, $content->title,
        $content->name, $content->major_version . '.' . $content->minor_version);

    // Success

    //Tovuti integration
    $lessonEncoded = filter_input(INPUT_POST, 'lesson');
    $lessonParams = json_decode(base64_decode($lessonEncoded));
    $contentTitle = $content->title;

    if($lessonParams->activity_title) {
      $contentTitle = $lessonParams->activity_title;
    }

    if($lessonParams->activity_id && $user_id && $lessonParams->course_id && $lessonParams->lesson_id && $content_id) {

      $activityQuery = "SELECT *
      FROM joom_splms_student_activities
      WHERE user_id          = $user_id
      AND   course_id        = $lessonParams->course_id
      AND   lesson_id        = $lessonParams->lesson_id
      AND   activity_type    = 'interactive'
      AND   student_response = $content_id
      AND   archive_date IS NULL";

      $db->setQuery($activityQuery);
      $activityRow = $db->loadObject();

      // Insert new Activity data
      if($content->name != "H5P.DocumentationTool") {
        $score = round(($data['score'] / $data['max_score']) * 100);
        $scoreText = $score."%";
        /*echo $lessonParams->activity_required."<br/>";
        echo (int)$score."<br/>";
        echo (int)$lessonParams->activity_required_score."<br/>";
        die();*/
        if($lessonParams->activity_required && ((int)$score >= (int)$lessonParams->activity_required_score || !$lessonParams->activity_required_score) ) {
          $completed = 1;
        } else {
          $completed = 0;
        }

          /* if($content->name == 'H5P.Questionnaire') {
            $completed = 1;
          } */


        $activityData = new stdClass();
        $activityData->course_id =  $lessonParams->course_id;
        $activityData->lesson_id =  $lessonParams->lesson_id;
        $activityData->activity_id =  $lessonParams->activity_id;
        $activityData->user_id =  $user_id;
        $activityData->activity_type =  'interactive';
        $activityData->activity_request =  $contentTitle;
        $activityData->student_response =  $content_id;
        $activityData->grade =  $scoreText;
        $activityData->completed =  $completed;


        if(!$activityRow) {
          $activityData->date_submitted =  $current_time;
          $db->insertObject('joom_splms_student_activities',$activityData);
        } else {
          $activityData->id = $activityRow->id;
          $activityData->date_modified =  $current_time;
          $db->updateObject('joom_splms_student_activities',$activityData,'id');
        }
      }
    }
    // end of tovuti integration
    $params = new stdClass();
    $params->activity_id =  $lessonParams->activity_id;
    $params->grade =  $scoreText;
    $params->completed =  $completed;
    H5PCore::ajaxSuccess(json_encode($params),TRUE);

    exit;


  }


  public function ajax_contents_user_data() {

    $db = Jfactory::getDbo();

    $token = filter_input(INPUT_GET, 'token');
    $content_id = filter_input(INPUT_GET, 'content_id');
    $data_id = filter_input(INPUT_GET, 'data_type');
    $sub_content_id = filter_input(INPUT_GET, 'sub_content_id');
    $current_user = JFactory::getUser();
    $user_id = $current_user->id;

    $data = filter_input(INPUT_POST, 'data');
    // Not logged in, so store responses in session
    /* if(empty($user_id)) {
      $interactiveResponse = [
        'token' => $token,
        'content_id' => $content_id,
        'data_id' => $data_id,
        'sub_content_id' => $sub_content_id,
        'data' => $data
      ];

      $_SESSION['h5p_user_ic_responses'] = base64_encode(json_encode($interactiveResponse));
    } */

    if ($content_id === NULL ||
        $data_id === NULL ||
        $sub_content_id === NULL ||
        !$current_user->id) {
      return; // Missing parameters
    }

    $response = (object) array(
      'success' => TRUE
    );

    $lessonEncoded = filter_input(INPUT_POST, 'lesson');
    $lessonParams = json_decode(base64_decode($lessonEncoded));
    $preload = filter_input(INPUT_POST, 'preload');
    $invalidate = filter_input(INPUT_POST, 'invalidate');

    if (
      $data !== NULL &&
      $preload !== NULL &&
      $invalidate !== NULL
    ) {
      if ($token!='h5p_contentuserdata') {
        H5PCore::ajaxError('Invalid security token');
        exit;
      }

      if ($data === '0') {
        // Remove data

        $query = "DELETE FROM `ic_h5p_contents_user_data` WHERE  content_id =".$db->quote($content_id)." AND data_id =".$db->quote($data_id)." AND user_id =".$db->quote($current_user->id)." AND sub_content_id =".$db->quote($sub_content_id);
        $db->setQuery($query);
        $db->execute();
      }
      else {
        // Wash values to ensure 0 or 1.
        $preload = ($preload === '0' ? 0 : 1);
        $invalidate = ($invalidate === '0' ? 0 : 1);

        // Determine if we should update or insert

        $query = "SELECT content_id FROM `ic_h5p_contents_user_data` WHERE  content_id =".$db->quote($content_id)." AND data_id =".$db->quote($data_id)." AND user_id =".$db->quote($current_user->id)." AND sub_content_id =".$db->quote($sub_content_id);
        $db->setQuery($query);
        $update = $db->loadResult();

        $current_time = date('Y-m-d H:i:s');

        $contentQuery = "SELECT c.title, l.name, l.major_version, l.minor_version
          FROM `ic_h5p_contents` c
          JOIN `ic_h5p_libraries` l ON l.id = c.library_id
          WHERE c.id = ".$db->quote($content_id);

        $db->setQuery($contentQuery);
        $contentRow = $db->loadObject();

        $contentTitle = $contentRow->title;

        if($lessonParams->activity_title) {
          $contentTitle = $lessonParams->activity_title;
        }

        if($lessonParams->activity_id && $current_user->id && $lessonParams->course_id && $lessonParams->lesson_id && $content_id) {
          if($sub_content_id) {
            $subContentQuery = "SELECT title
                            FROM `ic_h5p_contents`
                            WHERE id = ".$db->quote($sub_content_id);

            $db->setQuery($subContentQuery);
            $subContentRow = $db->loadObject();
            $subTitle = $subContentRow->title;
            $activity_content_id = "$content_id".":"."$sub_content_id";
            $contentTitle = $contentTitle." (".$subTitle.")";
          } else {
            $activity_content_id = $content_id;
          }

          $activityQuery = "SELECT *
          FROM joom_splms_student_activities
          WHERE user_id          = $user_id
          AND   course_id        = $lessonParams->course_id
          AND   lesson_id        = $lessonParams->lesson_id
          AND   activity_type    = 'interactive'
          AND   student_response = $content_id
          AND   archive_date IS NULL";
          
          $db->setQuery($activityQuery);
          $activityRow = $db->loadObject();

          $doNotStoreList = ['H5P.Audio','H5P.Dialogcards'];

          $activityData = new stdClass();
          $activityData->course_id =  $lessonParams->course_id;
          $activityData->lesson_id =  $lessonParams->lesson_id;
          $activityData->activity_id =  $lessonParams->activity_id;
          $activityData->user_id =  $user_id;
          $activityData->activity_type =  'interactive';
          $activityData->activity_request =  $contentTitle;
          $activityData->student_response =  $activity_content_id;
          //$activityData->grade =  $scoreText;
          //$activityData->completed =  $completed;

          // Insert new Activity data
          if(!$activityRow && !in_array($contentRow->content_type, $doNotStoreList)) {
            $activityData->date_submitted =  $current_time;
            $db->insertObject('joom_splms_student_activities',$activityData);
          }

          $parsedData = json_decode($data);

          if($contentRow->name == 'H5P.Questionnaire' && $parsedData->finished) {
            $completed = 1;
            $activityData->completed = $completed;
            $activityData->id = $activityRow->id;
            $activityData->date_modified =  $current_time;
            $db->updateObject('joom_splms_student_activities',$activityData,'id');
          }
        }

        if ($update === NULL) {
          // Insert new data

          $query  = "INSERT INTO `ic_h5p_contents_user_data` SET ";
          $query .= "user_id=".$db->quote($current_user->id);
          $query .= ", content_id=".$db->quote($content_id);
          $query .= ", sub_content_id=".$db->quote($sub_content_id);
          $query .= ", data_id=".$db->quote($data_id);
          $query .= ", data=".$db->quote($data);
          $query .= ", preload=".$db->quote($preload);
          $query .= ", invalidate=".$db->quote($invalidate);
          $query .= ", updated_at=".$db->quote($current_time);
          $db->setQuery($query);
          $db->execute();
        }
        else {
          // Update old data

          $query  = "UPDATE `ic_h5p_contents_user_data` SET ";
          $query .= " data=".$db->quote($data);
          $query .= ", preload=".$db->quote($preload);
          $query .= ", invalidate=".$db->quote($invalidate);
          $query .= ", updated_at=".$db->quote($current_time);
          $query .= " WHERE user_id=".$db->quote($current_user->id);
          $query .= " AND content_id=".$db->quote($content_id);
          $query .= " AND data_id=".$db->quote($data_id);
          $query .= " AND sub_content_id=".$db->quote($sub_content_id);

          $db->setQuery($query);
          $db->execute();
        }
      }

      // Inserted, updated or deleted
      $params = new stdClass();
      $params->activity_id =  $lessonParams->activity_id;
      $params->completed =  $completed;

      // Success
      H5PCore::ajaxSuccess(json_encode($params),TRUE);
      exit;
    }
    else {

        $query = $db->getQuery(true);
        $query->select('hcud.data')
              ->from('ic_h5p_contents_user_data hcud')
              ->where('user_id = ' . $db->quote($current_user->id))
              ->andWhere('content_id = ' . $db->quote($content_id))
              ->andWhere('data_id = ' . $db->quote($data_id))
              ->andWhere('sub_content_id = ' . $db->quote($sub_content_id));

        $db->setQuery($query);
        $response->data = $db->loadResult();

        if ($response->data === NULL) {
            unset($response->data);
        }
    }

    header('Cache-Control: no-cache');
    header('Content-type: application/json; charset=utf-8');
    print json_encode($response);
    exit;
  }
}