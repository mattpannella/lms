<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/7/16
 * Time: 10:10 AM
 */

defined('_JEXEC') or die();

class ConvergeModelSubscriptions extends FOFModel {

	private $customFields;

	public function saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $reason) {
		$session = JFactory::getSession();
		$session_id = $session->getId();

		$db = JFactory::getDbo();
		$cols = array (
			'user_id',
			'first',
			'last',
			'email',
			'phone',
			'address',
			'city',
			'state',
			'country',
			'zip',
			'plan_id',
			'date',
			'session_id',
			'reason'
		);
		$vals = array (
			(int)$user_id,
			$db->q($first),
			$db->q($last),
			$db->q($email),
			$db->q($phone),
			$db->q($address),
			$db->q($city),
			$db->q($state),
			$db->q($country),
			$db->q($zip),
			(int)$plan_id,
			$db->q(date("Y-m-d H:i:s")),
			$db->q($session_id),
			$db->q($reason)
		);
		$query = $db->getQuery(true);
		$query->insert('axs_pay_failed_subscriptions')
			->columns($db->qn($cols))
			->values(implode(',', $vals));
		$db->setQuery($query);
		$db->execute();
		return $db->insertid();
	}

	public function getPlan($brand_id, $plan_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('a.*,b.params as cat_params')
			->from('axs_pay_subscription_plans a')
			->leftJoin('axs_pay_subscription_categories b ON a.cat_id=b.id')
			->where('a.published=1')
			->where('b.published=1')
			//->where('b.brand_id='.(int)$brand_id)
			->where('a.id='.(int)$plan_id);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function isJumpingWithinCategory($plan_id, $user_id) {
		$db = JFactory::getDbo();
		$query = "SELECT e.* FROM 
							(SELECT a.id, b.cat_id, c.downgrade_approval, c.upgrade_approval 
							FROM axs_pay_subscriptions a 
							JOIN axs_pay_subscription_plans b ON a.plan_id=b.id 
							JOIN axs_pay_subscription_categories c ON c.id=b.cat_id
							WHERE a.user_id=$user_id AND (a.status='ACT' OR a.status='GRC')) e
							JOIN 
							(SELECT cat_id FROM `axs_pay_subscription_plans` WHERE id=$plan_id) d on e.cat_id=d.cat_id";
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getCustomFields($field_ids) {
		if(count($field_ids)) {
			$db = JFactory::getDbo();
			$ids = implode(', ', $field_ids);
			$query = "SELECT id, `name`, `required` FROM joom_community_fields WHERE published=1 AND id IN ($ids);";
			$db->setQuery($query);
			$this->customFields = $db->loadObjectList();
			return $this->customFields;
		}
		return array();
	}

	public function saveCustomFieldValues($sub_id) {
		
		/*$app = JFactory::getApplication();
		$db = JFactory::getDbo();

		$fieldValues = array();
		foreach($this->customFields as $cf) {
			$fieldValues[$cf->id] = $app->input->get('field'.$cf->id);
		}
		$query = "INSERT INTO axs_pay_subscription_field_values (`field_id`, `sub_id`, `value`) VALUES ";
		$values = array();
		foreach($fieldValues as $field_id => $value) {
			if($value) {
				$values[] = "($field_id, $sub_id,".$db->q($value).")";
			}
		}
		if(count($values)) {
			$query .= implode(', ', $values);
			$db->setQuery($query);
			$db->execute();
		}*/
	}

	/* Old Save Custom Fields Method
		public function saveCustomFieldValues($sub_id) {
		
			$app = JFactory::getApplication();
			$db = JFactory::getDbo();

			$fieldValues = array();
			foreach($this->customFields as $cf) {
				$fieldValues[$cf->id] = $app->input->get($cf->name, 0, 'STRING');
			}
			$query = "INSERT INTO axs_pay_subscription_field_values (`field_id`, `sub_id`, `value`) VALUES ";
			$values = array();
			foreach($fieldValues as $field_id => $value) {
				if($value) {
					$values[] = "($field_id, $sub_id,".$db->q($value).")";
				}
			}
			if(count($values)) {
				$query .= implode(', ', $values);
				$db->setQuery($query);
				$db->execute();
			}
		}
	*/

	public function saveCancellationRequest($sub) {
		$db = JFactory::getDbo();
		$now = date("Y-m-d H:i:s");
		$query = "INSERT IGNORE INTO axs_cancellation_requests (`user_id`, `type`, `sub_id`, `date`) VALUES ($sub->user_id, 0, $sub->id, '$now');";
		$db->setQuery($query);
		$db->execute();
	}

	public function savePlanChangeRequest($old_sub, $new_plan_id) {
		$db = JFactory::getDbo();
		$now = date("Y-m-d H:i:s");
		$query = "INSERT IGNORE INTO axs_cancellation_requests (`user_id`, `type`, `new_plan_id`, `sub_id`, `date`) VALUES ($old_sub->user_id, 1, $new_plan_id, $old_sub->id, '$now');";
		$db->setQuery($query);
		$db->execute();
	}
}
