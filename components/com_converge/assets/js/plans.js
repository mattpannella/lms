(function($) {
	$(document).ready(
	function() {
		//Set a small timeout.  The size changes slightly after the initial display.
		setTimeout(function(){
			//var plans = document.getElementsByClassName('plan');
			var plan_prices = document.getElementsByClassName('plan-price');
			var plan_containers = document.getElementsByClassName('plan-container');

			var tallest = 0;
			for (let i = 0; i < plan_prices.length; i++) {
				
				let div = $(plan_prices[i]);
				let height = div.outerHeight();

				if (height > tallest) {
					tallest = height;
				}
			}

			for (let i = 0; i < plan_prices.length; i++) {

				let div = $(plan_prices[i]);

				div.animate({
					"height": tallest + "px"
				});
			}

			var tallest = 0;
			for (let i = 0; i < plan_containers.length; i++) {

				let div = $(plan_containers[i]);
				let height = div.outerHeight();

				if (height > tallest) {
					tallest = height;
				}
			}

			for (let i = 0; i < plan_containers.length; i++) {

				let div = plan_containers[i];

				let lis = div.getElementsByTagName('li');
				let total = 0;
				for (let j = 0; j < lis.length; j++) {
					total += $(lis).outerHeight();
				}

				if (total < tallest) {
					let diff = tallest - total;
					let last = $(lis[lis.length - 1]);
					let currentHeight = last.outerHeight();
					last.css({
						"height": (currentHeight + diff) + "px"
					});
				}
			}

		}, 200);
	}
)})(jQuery);