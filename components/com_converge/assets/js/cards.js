/**
 * Created by mar on 2/1/16.
 */

(function($){
    $(function() {
        $('body').append($('#myModal')); //needed to fix the bug where modal pops up below background

        //######################################### Toggles plus / minus for accordion ########################################
        $(document).on('click', '#accordion .expand', function() {
            $('#accordion .expand').html('<span class="lizicon-plus"></span>');
            $('#accordion .expand').parent().parent().parent().removeClass('print-area');
            if(!$(this).hasClass('collapsed')) {
                $(this).html('<span class="lizicon-minus"></span>');
                $(this).parent().parent().parent().addClass('print-area');
            }
        });

        //######################################### For updating an existing card ########################################
        $(document).on('click', '.updateCard', function() {
            var card_id = this.dataset.cardid;
            //grab the new settings and send to php with ajax
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.updateCard&format=raw',
                data: {
                    card_id: card_id
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'error') {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_CONVERGE_ERROR_TRYING_TO_GET_CARD);
                } else {
                    result = JSON.parse(result);
                    var exp = result.expire;
                    var month = exp.substring(0, 2);
                    var year = '20'+ exp.substring(2, 4);
                    //console.log('m: '+month+' y: '+year);
                    var form = $('.saveCardUpdateFormClone');
                    form.find('input[name=card]').val(result.cardNumber);
                    form.find('select[name=exp_month]').val(month);
                    form.find('select[name=exp_year]').val(year);
                    form.find('input[name=first]').val(result.first);
                    form.find('input[name=last]').val(result.last);
                    form.find('input[name=addr]').val(result.address);
                    form.find('input[name=zip]').val(result.zip);
                    form.find('input[name=city]').val(result.city);
                    form.find('input[name=state]').val(result.state);
                    form.find('select[name=country]').val(result.country);
                    form.find('input[name=phone]').val(result.phone);
                }
            }).fail(function() {
                alert(AXS_CARDS_LOCALIZATION_VARS.AXS_CONVERGE_ERROR_TRYING_TO_GET_CARD);
            });

            $('#myModalLabel').text(AXS_CARDS_LOCALIZATION_VARS.AXS_EDIT_CARD_PERMISSIONS);
            var clone = $('#saveCardUpdateForm').clone().addClass('saveCardUpdateFormClone');
            $('#myModalBody').html(clone);
            $('.saveCardUpdateFormClone').show();
            $('.saveCardUpdateFormClone').data('cardid', card_id); //set the data-card attribute so we can grab it on form submit

            //reset class so the form submits correctly
            $('.modal-footer .btn-success').removeClass('saveCard').removeClass('editCardSave').addClass('updateCardSave').text(AXS_CARDS_LOCALIZATION_VARS.AXS_UPDATE);
        });

        function cardFormValidation() {
            var message = '';
            var form = $('.saveCardUpdateFormClone');

            var card = form.find('input[name=card]').val();
            if(!card) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_CARD_REQUIRED + "\n"
            }
            var exp_month = form.find('select[name=exp_month]').val();
            if(!card) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_EXPIRATION_DATE_REQUIRED + "\n"
            }
            var exp_year = form.find('select[name=exp_year]').val();
            if(!exp_year) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_EXPIRATION_DATE_REQUIRED + "\n"
            }
            var first = form.find('input[name=first]').val();
            if(!first) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_FIRST_NAME_REQUIRED + "\n"
            }
            var last = form.find('input[name=last]').val();
            if(!last) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_LAST_NAME_REQUIRED+ "\n"
            }
            var addr = form.find('input[name=addr]').val();
            if(!addr) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_ADDRESS_REQUIRED + "\n"
            }
            var zip = form.find('input[name=zip]').val();
            if(!zip) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_ZIP_REQUIRED+ "\n"
            }
            var city = form.find('input[name=city]').val();
            if(!city) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_CITY_REQUIRED + "\n"
            }
            var state = form.find('input[name=state]').val();
            if(!state) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_STATE_REQUIRED + "\n"
            }
            var country = form.find('select[name=country]').val();
            if(!country) {
                message += AXS_CARDS_LOCALIZATION_VARS.AXS_COUNTRY_REQUIRED + "\n"
            }
            return message;
        }
        $(document).on('click', '.updateCardSave', function() {
            //grab the new settings and send to php with ajax
            var card_id = $('.saveCardUpdateFormClone').data('cardid');
            var error = cardFormValidation();
            if(error) {
                alert(error)
            } else {
                $.ajax({
                    url: 'index.php?option=com_converge&task=cards.updateCardSave&format=raw',
                    data: {
                        card_id: card_id,
                        formData: $('.saveCardUpdateFormClone').serialize()
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if(result == 'success') {
                        alert(AXS_CARDS_LOCALIZATION_VARS.AXS_CARD_UPDATE_SUCCESSFUL);
                    } else {
                        alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_SETTING_PERMISSIONG_FOR_CARD);
                    }
                }).fail(function() {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_AN_ERROR_OCCURRED);
                });
            }
        });

        //######################################### For editing the permissions of a card ########################################
        $(document).on('click', '.editCard', function() {
            var card_id = this.dataset.cardid;
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.editCardPermissions&format=raw',
                data: {
                    card_id: card_id
                },
                type: 'post'
            }).done(function(result) {
                //console.log(result);
                result = JSON.parse(result);
                $.each(result, function() {
                    $('.saveCardPermissionsFormClone input[name='+this+']').prop('checked', true);
                });
            }).fail(function() {
                alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_GETTING_CARD_PERMISSIONS);
            });

            $('#myModalLabel').text(AXS_CARDS_LOCALIZATION_VARS.AXS_EDIT_CARD_PERMISSIONS);
            var clone = $('#saveCardPermissionsForm').clone().addClass('saveCardPermissionsFormClone');
            $('#myModalBody').html(clone);
            $('.saveCardPermissionsFormClone').show();
            $('.saveCardPermissionsFormClone').data('cardid', card_id); //set the data-card attribute so we can grab it on form submit

            //reset class so the form submits correctly
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success editCardSave').text(AXS_CARDS_LOCALIZATION_VARS.JSAVE);
        });

        $(document).on('click', '.editCardSave', function() {
            //grab the new settings and send to php with ajax
            var card_id = $('.saveCardPermissionsFormClone').data('cardid');
            $(this).prop('disabled', true);

            $.ajax({
                url: 'index.php?option=com_converge&task=cards.editCardPermissionsSave&format=raw',
                data: {
                    card_id: card_id,
                    formData: $('.saveCardPermissionsFormClone').serialize()
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_CARD_SETTINGS_SAVED);
                    location.reload();
                } else {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_SETTING_PERMISSIONG_FOR_CARD);
                }
            }).fail(function() {
                alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_SETTING_PERMISSIONG_FOR_CARD);
            });
        });

        //######################################### For deleting card ########################################
        $(document).on('click', '.deleteCard', function() {
            var card_id = this.dataset.cardid;
            var row = $(this).parent().parent();

            if (confirm(AXS_CARDS_LOCALIZATION_VARS.AXS_DELETE_CARD_CONFIRMATION_MESSAGE)) {
                $.ajax({
                    url: 'index.php?option=com_converge&task=cards.deleteCard&format=raw&card_id='+card_id,
                    type: 'get'
                }).done(function (result) {
                    if (result == 'success') {
                        alert(AXS_CARDS_LOCALIZATION_VARS.AXS_CARD_DELETED_SUCCESSFULLY);
                        row.remove();
                        $('.useCard').find('option[value='+card_id+']').remove();
                    } else {
                        alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_DELETING_YOUR_CARD);
                    }
                }).fail(function () {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_AN_ERROR_OCCURRED);
                });
            }
        });

        //######################################### For Adding a new card ########################################
        /*$(document).on('click', '.addCard', function() {
            $('#myModalLabel').text('Add New Card');
            $('#saveCardForm').addClass('saveCardFormClone');
            var form = $('#saveCardForm');
            $('#myModalBody').html(form);
            $('.saveCardFormClone').show();

            //reset class so the form submits correctly
            $('.modal-footer .btn-success').removeClass('editCardSave').removeClass('updateCardSave').addClass('saveCard').text('Save');
        });*/

        $(document).on('click', '.saveCard', function() {
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.saveCard&format=raw',
                data: {
                    card: $('#saveCardForm').serialize()
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    //refresh the page so they can see their new card
                    location.reload();
                } else {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_ADDING_CARD_CHECK_INFORMATION);
                }
            }).fail(function() {
                alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_ADDING_CARD_CHECK_INFORMATION);
            });
            
        });

        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('saveCardForm');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
            var hiddenInputLastFour = document.createElement('input');
            hiddenInputLastFour.setAttribute('type', 'hidden');
            hiddenInputLastFour.setAttribute('name', 'last4');
            hiddenInputLastFour.setAttribute('value', token.card.last4);
            form.appendChild(hiddenInputLastFour);
        }

        
        var gateway = $('#saveCardForm').data('gtw');
        if(gateway == 'stripe') {
            console.log('here')
            card.addEventListener('change', function(event) {
                 console.log('change')
                if(event.complete == true) {
                    stripe.createToken(card).then(function(result) {
                        if (result.error) {
                        console.log(result);
                          // Inform the user if there was an error.
                          var errorElement = document.getElementById('card-errors');
                          errorElement.textContent = result.error.message;
                        } else {
                            console.log(result);
                          // Send the token to your server.
                           stripeTokenHandler(result.token);
                        }
                    });
                }           
            });
        }

        //######################################### For Updating plan settings ########################################
        $(document).on('change', '.usePoints, .useCard', function() {
            var sub_id = $(this).data('subid');
            $('.savePlan[data-subid='+sub_id+']').removeClass('btn-primary').addClass('btn-danger');
        });

        $(document).on('click', '.savePlan', function() {
            var sub_id = this.dataset.subid;
            var points = $('.usePoints[data-subid='+sub_id+']').val();
            var card_id = $('.useCard[data-subid='+sub_id+']').val();

            $(this).prop('disabled', true);
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.savePlan&format=raw',
                data: {
                    sub_id: sub_id,
                    card_id: card_id,
                    points: points
                },
                type: 'post'
            }).done(function (result) {
                if (result == 'success') {
                    $('.savePlan[data-subid='+sub_id+']').addClass('btn-primary').removeClass('btn-danger').prop('disabled', false);
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_SETTINGS_SAVED);
                } else {
                    alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_PLEASE_REFRESH_PAGE);
                }
            }).fail(function () {
                alert(AXS_CARDS_LOCALIZATION_VARS.AXS_ERROR_PLEASE_REFRESH_PAGE);
            });
        });

        //######################################### For retrying payments ########################################
        $(document).on('click', '#updatePlanSettings', function() {
            jQuery('#mytabs a[href=#rewards-settings]').tab('show');
        });

        $(document).on('click', '#retryPayment', function() {
            $(this).attr('disabled', true);
            $(this).click(function(ev) {
                ev.preventDefault();
            });
        });
    });
}(jQuery));