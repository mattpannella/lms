<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 2/1/16
 * Time: 2:07 PM
 */
/*error_reporting(E_ALL);
    ini_set('display_errors', 1);*/
defined('_JEXEC') or die();

class ConvergeControllerCards extends FOFController {
    //pull the card data from converge and return it.
    public function updateCard() {

        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $this->input->get('card_id', '0');
        $card = AxsPayment::newCard($cardParams);
        $card->getCardWithId(); //fetch the other card data
        if($card->fetchTokenizedData()) {
            echo json_encode($card);
        } else {
            echo 'error';
        }
    }

    //send the card data back to converge with updated info.
    public function updateCardSave() {
        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $this->input->get('card_id', '0');
        $card = AxsPayment::newCard($cardParams);
        $card->getCardWithId(); //fetch the other card data

        //parse form data
        $cardData = array();
        parse_str($_POST['formData'], $cardData);
        $card->cardNumber = $cardData['card'];
        $card->expire = str_pad($cardData['exp_month'], 2, '0', STR_PAD_LEFT) . substr($cardData['exp_year'], 2, 2);
        $card->cvv = $cardData['cvv'];
        $card->firstName = $cardData['first'];
        $card->lastName = $cardData['last'];
        $card->address = $cardData['addr'];
        $card->zip = $cardData['zip'];
        $card->city = $cardData['city'];
        $card->state = $cardData['state'];
        $card->country = $cardData['country'];
        $card->phone = $cardData['phone'];

        $result = $card->updateTokenizedData();
        if($result['result']) {
            echo 'success';
        } else {
            echo 'error';
        }

    }

    public function editCardPermissions() {
        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $this->input->get('card_id', '0');
        $card = AxsPayment::newCard($cardParams);
        $card->getCardWithId();
        echo json_encode($card->family_access);
    }

    public function editCardPermissionsSave() {
        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $this->input->get('card_id', '0');
        $card = AxsPayment::newCard($cardParams);
        $card->getCardWithId();
        //parse form data
        $permissions = array();
        parse_str($_POST['formData'], $permissions);
        $perms = array();
        foreach($permissions as $key => $val) {
            //key is the family member's id
            $perms[] = $key;
        }
        $card->family_access = $perms;
        $card->save();
        echo 'success';
    }

    public function deleteCard() {
        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $this->input->get('card_id', '0');
        $card = AxsPayment::newCard($cardParams);
        $card->getCardWithId(); //fetch the other card data
        if($card->deTokenize()) {
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function saveCard() {
        //parse form data
        $cardData = array();
        parse_str($_POST['card'], $cardData);
        $user = JFactory::getUser();
        // grab stuff from form

        $cardParams = AxsPayment::getCardParams();
        $brand = AxsBrands::getBrand();

        if($brand->billing->gateway_type == 'stripe') {
            $cardParams->user_id = $user->id;
            $cardParams->stripeToken = $cardData['stripeToken'];
            $cardParams->cardNumber = $cardData['last4'];
            $user = JFactory::getUser($cardParams->user_id);
            $cardParams->email = $user->email;
            $cardParams->description = $user->name;
        } else {
            $cardParams->firstName = $cardData['first'];
            $cardParams->lastName = $cardData['last'];
            $cardParams->cardNumber = $cardData['card'];
            $cardParams->address = $cardData['addr'];
            $cardParams->zip = $cardData['zip'];
            $cardParams->city = $cardData['city'];
            $cardParams->state = $cardData['state'];
            $cardParams->country = $cardData['country'];
            $cardParams->phone = $cardData['phone'];
            $cardParams->email = '';
            $cardParams->expire = str_pad($cardData['exp_month'], 2, '0', STR_PAD_LEFT) . substr($cardData['exp_year'], 2, 2);
            $cardParams->cvv = $cardData['cvv'];
            $cardParams->user_id = $user->id;
        }

        $card = AxsPayment::newCard($cardParams);

        if($card->tokenize()) {
        	$card->save();
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function savePlan() {
        $subParams = AxsPayment::getSubscriptionParams();
        $subParams->id = $this->input->get('sub_id', '0');
        $subParams->card_id = $this->input->get('card_id', '0');
        $subParams->points = $this->input->get('points', '0');
        $sub = AxsPayment::newSubscription($subParams);
        $sub->save();
        echo 'success';
    }
}