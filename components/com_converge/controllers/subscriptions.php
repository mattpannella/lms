<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/5/16
 * Time: 3:28 PM
 */

defined('_JEXEC') or die();



class ConvergeControllerSubscriptions extends FOFController {

	public static $plan_password = "#\#%/IA0aaEcqi(^=<cmS[~=7p,([hJ<8lRI{o.0)0}g>nu!nkc~8OBz8dCBD:r";
	public static $subscription_password = "32:W3x:KjMoy'akYt#6W7`K@WjuYzSI5'/oNR6pI6YP.&jt_{dQv`P'}]CO)2[c";

	private $override_setup_fee = null;
	private $override_monthly_fee = null;


	public function cancelSubscription() {
		$app = JFactory::getApplication();
		$model = $this->getThisModel();
		$dispatcher = JEventDispatcher::getInstance();

		$sub_id = $this->input->get('sub_id', 0);
		$cat = $this->input->get('cat', 0);
		//set this sub to inactive

		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$sub = AxsPayment::newSubscription($subParams);
		$sub->getSubscriptionWithId(false);
		$user = JFactory::getUser();

		if($sub->user_id != $user->id) {
			$app->enqueueMessage(AxsLanguage::text("AXS_THIS_IS_NOT_YOUR_SUBSCRIPTION", "This is not your subsription"), 'error');
			$url = JRoute::_('index.php?option=com_converge&view=plans'.($cat ? '&cat='.$cat : ''), false);
			$app->redirect($url);
		} else {
			//see if plan allows auto cancels or requires a request.
			$plan = $sub->getPlan();

			if ($plan->request_cancel) {
				//save a requested cancel
				$model->saveCancellationRequest($sub);
				//send admins email
				$dispatcher->trigger('onSubscriptionStopRequest', array(&$sub));
				//send them back to the plan page with a message.
				$app->enqueueMessage(AxsLanguage::text("AXS_YOUR_CANCELLATION_REQUEST_SUBMITTED", "Your cancellation request was submitted!"), 'success');
			} else {
				//if they are already in grace period, we need to remove their usergroups right away
				if ($sub->status == "GRC") {

					if(!is_array($plan->active_usergroup)) {
						$activeUserGroup = array($plan->active_usergroup);
					} else {
						$activeUserGroup = $plan->active_usergroup;
					}
					$user->groups = array_diff($user->groups, $activeUserGroup);
					$user->save();
				}

				$sub->cancel();
				$dispatcher->trigger('onSubscriptionStop', array(&$sub));

				//send them back to the plan page with a message.
				$app->enqueueMessage(AxsLanguage::text("AXS_YOUR_SUBSCRIPTION_WAS_CANCELLED", "Your subscription was cancelled!"), 'success');
			}

			$url = JRoute::_('index.php?option=com_converge&view=plans'.($cat ? '&cat='.$cat : ''), false);
			$app->redirect($url);
		}

	}

	public function subscribe() {

		$app = JFactory::getApplication();
		$model = $this->getThisModel();
		$user = JFactory::getUser();
		$dispatcher = JEventDispatcher::getInstance();
		$user_id = $user->id;
		$first = $this->input->get('first', 0, 'STRING');
		$last = $this->input->get('last', 0, 'STRING');
		$email = $this->input->get('email', 0, 'STRING');
		$username = $this->input->get('username', 0, 'USERNAME');
		$password = $this->input->get('password', 0, 'STRING');
		$password2 = $this->input->get('password2', 0, 'STRING');
		$address = $this->input->get('address', 0, 'STRING');
		$city = $this->input->get('city', 0, 'STRING');
		$state = $this->input->get('state', 0, 'STRING');
		$country = $this->input->get('country', 0, 'STRING');
		$zip = $this->input->get('zip', 0, 'STRING');
		$phone = $this->input->get('phone', 0, 'STRING');

		//For the site launcher
		$server_settings = $this->input->get('server_settings');
		$subscriptioncode = $this->input->get('subscriptioncode', 0, 'STRING');


		//validate that there was a plan sent with this request.
		//This is encrypted so that users cannot send in another plan id and circumvent security features.
		//The unencrypted value should be "plan_id:valid"
		$plan_id = AxsEncryption::decrypt($this->input->get('plan_id'), self::$plan_password);
		$plan_id = explode(":", $plan_id);
		if (count($plan_id) != 2) {
			$plan_id = false;
		} else {
			if ($plan_id[1] == "valid") {
				$plan_id = (int)$plan_id[0];
			} else {
				$plan_id = false;
			}
		}

		$return = new stdClass();

		$brand = AxsBrands::getBrand();

		if (!$plan_id) {
			//store failed subscription
			$message = AxsLanguage::text("AXS_NO_SUBSCRIPTION_PLAN_SUPPLIED", "No subscription plan was supplied.");

			$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
			$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));
			//return them to the signup page

			$return->success = false;
			$return->message = $message;
			echo json_encode($return);
			return;
		} else {
			//they made it to here, lookup details about this plan
			$plan = $model->getPlan($brand->id, $plan_id);

			if (!is_object($plan)) {
				//store failed subscription
				$message = AxsLanguage::text("AXS_SUBSCRIPTION_PLAN_IS_INVALID", "This subscription plan is invalid.");
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));
				//return them to the signup page

				$return->success = false;
				$return->message = $message;
				echo json_encode($return);
				return;
			}
			if(!$plan->usergroups || $plan->usergroups == "null") {
				$defaultGroup = ["2"];
				$plan->usergroups = json_encode($defaultGroup);
			}
		}

		$checkCode = self::checkSubscriptionCode($plan, $subscriptioncode);
		if (!$checkCode) {
			//They most likely tried to cheat their way past
			$return->success = false;
			$return->message = AxsLanguage::text("AXS_INVALID_PASSCODE", "Invalid Passcode");
			echo json_encode($return);
			return;
		}

		$tovuti_builder = false;
		$plan_params = null;
		if (isset($plan->params)) {
			$plan_params = json_decode($plan->params);
			if (isset($plan_params->tovuti_builder) && $plan_params->tovuti_builder !== "0") {
				$tovuti_builder = true;
			}
		}


		//Check to see if the plan is the AWS site launcher

		//If this is set, then the subscription is for a new TOVUTI site.
		if ($tovuti_builder && $server_settings) {
			$server_settings = json_decode(base64_decode($server_settings));
			//require_once("/var/www/sitebuilder/aws/AwsControl.php");
			require_once("/var/www/sitebuilder/AwsBase.php");
			$price_validate = AwsControl::validateAndPrice($server_settings);

			//$subscription_id_password = AwsControl::key('subscription');

			if (isset($price_validate->valid) && $price_validate->valid === true) {
				$this->override_monthly_fee = $price_validate->price->total;
				//For now there is no setup fee
				$this->override_setup_fee = $price_validate->price->total;
			}
		}

		//validate that they accepted the terms
		$terms = $this->input->get('terms', 0);
		/*if(!$terms && ($brand->legal->terms_of_use || $brand->legal->member_agreement) ) {
			//store failed subscription
			$message = 'You must agree to the terms and conditions.';
			$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
			$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

			$return->success = false;
			$return->message = $message;
			echo json_encode($return);
			return;
		}*/

		//if it's a new user...
		if (!$user_id) {
			$require_address = false;
			$params = json_decode($plan->params);
			if (($params->include_address && $params->require_address) || ($plan->default_initial_amount > 0 || $plan->default_amount > 0)) {
				if(!($address && $city && $state && $country && $zip)) {
					$require_address = true;
				}
			}

			$mobileDeviceList = ['iPad','iPhone'];
			$isIOS = AxsMobileHelper::isClientUsingMobile($mobileDeviceList);
			if ($params->captcha && !$isIOS) {
				$captchaResponse = JRequest::get('g-recaptcha-response');
		        JPluginHelper::importPlugin('captcha');
		        $dispatcher = JDispatcher::getInstance();
		        $res = $dispatcher->trigger('onCheckAnswer',$captchaResponse);

		        if (!$res[0]) {
		            $message = 'Invalid Captcha';
					$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
					$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

					$return->success = false;
					$return->message = $message;
					echo json_encode($return);
					return;
		        }
	    	}


			if ( (!($first && $last && $email && $username && $password && $password2)) || ($require_address) ) {
				//store failed subscription
				$message = AxsLanguage::text("AXS_ONE_OR_MORE_REQUIRED_FIELDS_LEFT_BLANK", "One or more required fields were left blank.");
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				echo json_encode($return);
				return;
			}

			$validatePassword = AxsSecurity::validatePassword($password);
			if($validatePassword->success === false) {
				$return->success = false;
				$return->message = $validatePassword->error;
				echo json_encode($return);
				return;
			}

			//validate that the user account can be created before attempting to charge, then send them to be processed.
			$groups = json_decode($plan->usergroups);

			$groups[] = $plan->active_usergroup;
			$details = array (
				'username' => $username,
				'email' => $email,
				'name' => $first." ".$last,
				'password' => $password,
				'password2' => $password2,
				'groups' => $groups,
				'setlastvisit' => true
			);

			//if any of the user details are invalid values...
			if (!$user->bind($details)) {
				//store failed subscription
				$message = AxsLanguage::text("AXS_FAILED_TO_BIND_SUBSCRIPTION_DETAILS", "Failed to bind subscription details.");
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				echo json_encode($return);
				return;
			}
			//if the username or email already exist...
			$table = $user->getTable();
			$table->bind($user->getProperties());
			// Check if the user can be stored.
			if (!$table->check()) {
				//store failed subscription
				$message = $table->getError();
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				echo json_encode($return);
				return;
			}

			//validate that they filled in any required field
			$customFields = $model->getCustomFields(json_decode($plan->reg_fields));
			foreach ($customFields as $field) {
				$value = $this->input->get('field'.$field->id, 0, 'STRING');
				if ($field->required && !$value) {
					//store failed subscription
					$message = AxsLanguage::text("AXS_ONE_OR_MORE_REQUIRED_FIELDS_LEFT_BLANK", "One or more required fields were left blank.");
					$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
					$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

					$return->success = false;
					$return->message = $message;
					echo json_encode($return);
					return;
				}
			}

			//validate that the refid belongs to a real user.
			$dispatcher = JEventDispatcher::getInstance();
			$res = $dispatcher->trigger('onValidateRefid');

			if (!$res[0]) {
				//store failed subscription
				$message = AxsLanguage::text("AXS_SPONSORS_AFFILIATE_CODE_NOT_VALID", "The Sponsor's Affiliate Code that was supplied is not valid.");
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				echo json_encode($return);
				return;
			}

			//go ahead and send them to be processed if they make it past the validation.
			$newSub = $this->newSubscription($plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user);
		} else {
			//validate that they filled in any required field
			$customFields = $model->getCustomFields(json_decode($plan->reg_fields));
			foreach($customFields as $field) {
				$value = $this->input->get('field'.$field->id, 0, 'STRING');

				if ($field->required && !$value) {
					//store failed subscription
					$message = AxsLanguage::text("AXS_ONE_OR_MORE_REQUIRED_FIELDS_LEFT_BLANK", "One or more required fields were left blank.");
					$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
					$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

					$return->success = false;
					$return->message = $message;
					echo json_encode($return);
					return;
				}

			}

			//give them the new usergroups that come from this plan.
			//This checks just to make sure active plan is not an array already

			if (!is_array($plan->active_usergroup)) {
				$activeUserGroup = array($plan->active_usergroup);
			} else {
				$activeUserGroup = $plan->active_usergroup;
			}

			$groups = array();
			if (is_array($user->groups)) {
				$groups += $user->groups;
			}

			if (is_array(json_decode($plan->usergroups))) {
				$groups += json_decode($plan->usergroups);
			}

			if (is_array($activeUserGroup)) {
				$groups += $activeUserGroup;
			}

			$user->groups = array_unique($groups);

			//get categories params
			$cat_params = json_decode($plan->cat_params);

			// we need to see if they are changing plans or not and if can not purchase multiple plans
			$changePlans = $model->isJumpingWithinCategory($plan_id, $user_id);


			if(is_object($changePlans) && !$cat_params->multi_purchase) {
				$newSub = $this->changeSubscription($changePlans->id, $changePlans->cat_id, $changePlans->downgrade_approval, $changePlans->upgrade_approval, $plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user);
			} else {
				$newSub = $this->newSubscription($plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user);
			}
		}

		if ($newSub->success == true) {
			if (isset($plan_params) && $plan_params->usepasscodes == "yes") {
				self::usePasscode($plan, $subscriptioncode);
			}
		}

		echo json_encode($newSub);	//The return message and url from the new/change subscription functions
	}

	public function usePasscode($plan, $subscriptioncode) {

		if (!$plan && !$subscriptioncode) {
			return false;
		}

		$db = JFactory::getDBO();

		$conditions = array(
			$db->quoteName('plan_id') . '=' . (int)$plan->id,
			$db->quoteName('code') . '=' . $db->quote($subscriptioncode)
		);

		$query = $db->getQuery(true);
		$query
			->update($db->quoteName('axs_subscription_plan_codes'))
			->set($db->quoteName('inuse') . ' = IFNULL(' . $db->quoteName('inuse') . ', 0) + 1')
			->where($conditions);

		$db->setQuery($query);
		$db->execute();

	}

	public function checkSubscriptionCode($plan, $subscriptioncode) {

		if (!$plan && !$subscriptioncode) {
			return false;
		}

		$params = json_decode($plan->params);
		$plan_id = $plan->id;

		if ($params->usepasscodes == "yes") {
			//$passcodes = json_decode($params->passcodes);

			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			$conditions = array(
				$db->quoteName('plan_id') . '=' . (int)$plan_id,
				$db->quoteName('code') . '=' . $db->quote($subscriptioncode),
				$db->quoteName('active') . '=1'
			);

			$query
				->select('*')
				->from($db->quoteName('axs_subscription_plan_codes'))
				->where($conditions)
				->setLimit(1);

			$db->setQuery($query);
			$passcode = $db->loadObject();

			if (!$passcode) {
				//One is required, but none match.
				return false;
			}

			if ($passcode->code == $subscriptioncode) {

				$expire = strtotime($passcode->expire);
				//Dates with 0000-00-00 00:00 00:00 equate to a large negative number
				if ($expire < 0) {
					$expire = null;
				}

				if ($expire) {
					//It has an expiration date

					if ($expire <= strtotime('now')) {
						//It's expired
						echo "expired";
						return false;
					}
				}

				//It's not expired.

				if ($passcode->userlimit) {
					//It's a good code that has unlimited uses.
					return true;
				} else {
					if (!$passcode->inuse) {
						$passcode->inuse = 0;
					}

					$remaining = $passcode->maximum - $passcode->inuse;
					if ($remaining > 0) {
						//It's a valid code with uses left.
						return true;
					} else {
						//It's all used up
						return false;
					}
				}
			}

		} else {
			//No passcode required.
			return true;
		}
	}

	public function newSubscription($plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user) {

		$return = new stdClass();
		$brand = AxsBrands::getBrand();
		$dispatcher = JEventDispatcher::getInstance();
		//do some calculations to determine the charge amount and regular period amount
		$promo_code = $this->input->get('promo', '');

		$planParams = json_decode($plan->params);
		$dispatcher = JEventDispatcher::getInstance();
		if($planParams->tovuti_builder) {
			$eventRes = $dispatcher->trigger('verifyPromo', array($promo_code, $plan, null, null, $this->override_setup_fee, $this->override_monthly_fee), $this->override_setup_fee, $this->override_monthly_fee);
		} else {
			$eventRes = $dispatcher->trigger('verifyPromo', array($promo_code, $plan, null, null), $this->override_setup_fee, $this->override_monthly_fee);
		}

		if ($eventRes[0]->result == 'success') {
			$recurring_amount = $eventRes[0]->recurring_amount;
			$total = $eventRes[0]->initial_amount;
		} else {

			if ($this->override_monthly_fee) {
				$recurring_amount = $this->override_monthly_fee;
			} else {
				$recurring_amount = $plan->default_amount;
			}

			if ($this->override_setup_fee) {
				$total = $this->override_setup_fee;
			} else {
				$total = $plan->default_initial_amount;
			}
		}

		$card_on_file = $this->input->get('card_on_file', 0, 'INT');
		$cardNumber = $this->input->get('card', 0, 'STRING');

		if($brand->billing->gateway_type == 'stripe' && !$card_on_file) {
			$cardParams = AxsPayment::getCardParams();
			$cardParams->user_id = $user_id;
			$cardParams->stripeToken = $this->input->get('stripeToken', 0, 'STRING');
			$cardParams->cardNumber = $this->input->get('last4', 0);
			$user = JFactory::getUser($cardParams->user_id);
			$cardParams->email = $user->email;
			$cardParams->description = $user->name;
			$card = AxsPayment::newCard($cardParams);
		}

		if($brand->billing->gateway_type == 'stripe' && $card_on_file) {
			$cardParams = AxsPayment::getCardParams();
			$cardParams->id = $card_on_file;
			$card = AxsPayment::newCard($cardParams);
			$card->getCardWithId(); //fetch token and other card info
		}

		if(($card_on_file || $cardNumber) && $brand->billing->gateway_type != 'stripe') {

			if(!$card_on_file) {
				$exp_month = $this->input->get('exp_month', 0, 'INT');
				$exp_year = $this->input->get('exp_year', 0, 'INT');
				$cvv = $this->input->get('cvv', 0, 'STRING');
				$bill_first = $this->input->get('billing_first', 0, 'STRING');
				$bill_last = $this->input->get('billing_last', 0, 'STRING');
				$same = $this->input->get('same_billing', 'no', 'STRING');

				if ($same == 'on') {

					//if they're logged in fetch this stuff from their account
					if($user_id) {
						$u = AxsExtra::getUserProfileData($user_id);
						$bill_address = $u->address;
						$bill_city = $u->city;
						$bill_state = $u->state;
						$bill_country = $u->country;
						$bill_zip = $u->zip;
					} else {
						//else fetch it from above
						$bill_address = $address;
						$bill_city = $city;
						$bill_state = $state;
						$bill_country = $country;
						$bill_zip = $zip;
					}
				} else {

					$bill_address = $this->input->get('billing_address', 0, 'STRING');
					$bill_city = $this->input->get('billing_city', 0, 'STRING');
					$bill_state = $this->input->get('billing_state', 0, 'STRING');
					$bill_country = $this->input->get('billing_country', 0, 'STRING');
					$bill_zip = $this->input->get('billing_zip', 0, 'STRING');
				}
			}

			if ($user_id && $card_on_file) {
				$cardParams = AxsPayment::getCardParams();
				$cardParams->id = $card_on_file;
				$card = AxsPayment::newCard($cardParams);
				$card->getCardWithId(); //fetch token and other card info
			} elseif(
				$cardNumber &&
				$exp_month &&
				$exp_year &&
				$cvv &&
				$bill_first &&
				$bill_last &&
				$bill_city &&
				$bill_address &&
				$bill_state &&
				$bill_country &&
				$bill_zip
			) {

				$cardParams = AxsPayment::getCardParams();

				$cardParams->firstName = $bill_first;
				$cardParams->lastName = $bill_last;
				$cardParams->cardNumber = $cardNumber;
				$cardParams->expire = str_pad($exp_month, 2, '0', STR_PAD_LEFT) . substr($exp_year, 2, 2);
				$cardParams->cvv = $cvv;
				$cardParams->address = $bill_address;
				$cardParams->zip = $bill_zip;
				$cardParams->city = $bill_city;
				$cardParams->state = $bill_state;
				$cardParams->country = $bill_country;

				$card = AxsPayment::newCard($cardParams);
			} else {
				//store failed subscription
				$message = AxsLanguage::text("AXS_ONE_OR_MORE_REQUIRED_FIELDS_LEFT_BLANK", "One or more required fields were left blank.");
				$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				//echo json_encode($return);
				return $return;
			}
		}

		if ($total > 0) {

			$trxnParams = AxsPayment::getTransactionParams();

			$trxnParams->id = 0;
		    $trxnParams->user_id = $user_id;
		    $trxnParams->type = AxsPayment::$trxn_types['subscription'];
		    $trxnParams->amount = $total;
		    $trxnParams->date = date("Y-m-d H:i:s");
		    $trxnParams->card = $card;
		    $trxnParams->initiator = AxsPayment::$initiators['user'];
		    $trxnParams->description = $plan->title;

			//now make the charge
			$trxn = AxsPayment::newTransaction($trxnParams);
			$trxn->charge(false); //false so don't save trxn to db immediately.

			if ($trxn->status == 'SUC') {
				//if it's a new user, go ahead and save them
				if (!$user_id) {
					if ($user->save()) {
						$user_id = $user->id;
						$trxn->user_id = $user_id;
					} else {
						//just in case the user account can't be created, this will void the trxn,
						// but it should never get here bc we validate that the user account can be saved before we make the charge.
						$trxn->refund();

						//store failed subscription
						$message = AxsLanguage::text("AXS_ACCOUNT_CREATE_ERROR_EMAIL_EXISTS_ALREADY", "There was a problem creating your new account. An account with that username or email might already exist in our system.");
						$trxn->type_id = $model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
						$trxn->save();
						$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

						$return->success = false;
						$return->message = $message;
						//echo json_encode($return);
						return $return;
					}
				}

				//if it's a new card, go ahead and save it
				if (!$card->id) {
					$card->user_id = $user_id;
					$card->save();
				}

				//create subscription regardless of if it's a new user or not.
				$subParams = AxsPayment::getSubscriptionParams();

				$subParams->user_id = $user_id;
		        $subParams->plan_id = $plan->id;
		        $subParams->original_amount = $recurring_amount;
		        $subParams->status = 'ACT';
		        $subParams->period_length = $plan->default_sub_length;
		        $subParams->card_id = $trxn->card->id;

				$sub = AxsPayment::newSubscription($subParams);
				$sub->save();

				if ($plan->default_sub_length) {
					$end    = null;
					$length = substr($plan->default_sub_length, 0, -1);
					$term   = substr($plan->default_sub_length, -1);
					switch ($term) {
						case 'M':
							$end = new DateTime();
							for ($i = 0; $i < $length; $i++) {
								$end = AxsExtra::getDayNextMonth($end);
							}
							$end = $end->format("Y-m-d H:i:s");
							break;
						case 'D':
							$end = (new DateTime())->modify("+$length day")->format("Y-m-d H:i:s");
							break;
						case 'Y':
							$end = (new DateTime())->modify("+$length year")->format("Y-m-d H:i:s");
					}
				} else {
					$end = date("Y-m-d H:i:s"); //lifetime membership
				}

				$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
				$subPeriodParams->sub_id = $sub->id;
				$subPeriodParams->start = date("Y-m-d H:i:s");
				$subPeriodParams->end = $end;
				$subPeriodParams->status = "PAID";
				$subPeriodParams->user_id = $user_id;

				$subPeriod = AxsPayment::newSubscriptionPeriod($subPeriodParams);
				$subPeriod->save();
				$sub->periods[] = $subPeriod;

				$trxn->type_id = $subPeriod->id;
				$trxn->save();
				$subPeriod->trxns[] = $trxn;

				$sub->plan = $plan;

				//save the new usergroups that they get with this plan.
				if (is_array(json_decode($plan->usergroups))) {
					$usergroups = json_decode($plan->usergroups);
				}

				$current_user_groups = JUserHelper::getUserGroups($user->id);
				if($usergroups) {
					foreach($usergroups as $group) {
						if(!in_array($group, $current_user_groups)) {
							AxsActions::trackUserGroup($user->id,$group,'add');
						}
					}
				}
				$usergroups = array_merge($usergroups,$current_user_groups);
				array_unique($usergroups);
				JUserHelper::setUserGroups($user->id,$usergroups);
				$user->save();
				//$model->saveCustomFieldValues($sub->id);
				$dispatcher->trigger('onSubscriptionPlay', array(&$sub));
			} else {
				//the trxn was not successful, lets store them in the fails so we can look them up and reach out to them later.
				$message = AxsLanguage::text("AXS_FAILED_TO_CHARGE_CARD_ERROR", "Failed to charge your card. Error: ").$trxn->message;
				$trxn->type = AxsPayment::$trxn_types['failed_signup'];
				$trxn->type_id = $model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
				$trxn->save();
				$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

				$return->success = false;
				$return->message = $message;
				//echo json_encode($return);
				return $return;
			}
		} else {
			//if we got to here the subscription is free, just make a subscription for them with a subscription period that is free.
			if (!$user_id) {
				if ($user->save()) {
					if (is_array(json_decode($plan->usergroups))) {
						$usergroups = json_decode($plan->usergroups);
					}
					if($usergroups) {
						foreach($usergroups as $group) {
							AxsActions::trackUserGroup($user->id,$group,'add');
						}
					}
					$user_id = $user->id;
				} else {
					//just in case the user account can't be created, even though we validate it above already.
					$message = AxsLanguage::text("AXS_ACCOUNT_CREATE_ERROR_EMAIL_EXISTS_ALREADY", "There was a problem creating your new account. An account with that username or email might already exist in our system.");
					$model->saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $message);
					$dispatcher->trigger("onFailedSignup", array(&$user_id, &$first, &$last, &$plan_id));

					$return->success = false;
					$return->message = $message;
					//echo json_encode($return);
					return $return;
				}
			} else {
				//by saving this gives them the usergroups for this plan.
				if (is_array(json_decode($plan->usergroups))) {
					$usergroups = json_decode($plan->usergroups);
				}

				$current_user_groups = JUserHelper::getUserGroups($user->id);
				if($usergroups) {
					foreach($usergroups as $group) {
						if(!in_array($group, $current_user_groups)) {
							AxsActions::trackUserGroup($user->id,$group,'add');
						}
					}
				}

				$usergroups = array_merge($usergroups,$current_user_groups);
				array_unique($usergroups);

				JUserHelper::setUserGroups($user->id,$usergroups);
				$user->save();
			}

			//if it's a new card, go ahead and save it
			if ($card && !$card->id && !$card->token && $user_id) {
				$card->user_id = $user_id;
				$isTokenized = $card->tokenize();
				if($isTokenized) {
					$card->save();
				}
			}

			$subParams = AxsPayment::getSubscriptionParams();

			$subParams->user_id = $user_id;
			$subParams->plan_id = $plan->id;
			$subParams->original_amount = $recurring_amount;
			$subParams->status = "ACT";
			$subParams->period_length = $plan->default_sub_length;

			$sub = AxsPayment::newSubscription($subParams);
			$sub->save();

			$end = null;
			$length = substr($plan->default_sub_length, 0, -1);
			$term = substr($plan->default_sub_length, -1);

			switch($term) {
				case 'M':
					$end = new DateTime();
					for($i=0; $i<$length; $i++) {
						$end = AxsExtra::getDayNextMonth($end);
					}
					$end = $end->format("Y-m-d H:i:s");
					break;
				case 'D':
					$end = (new DateTime())->modify("+$length day")->format("Y-m-d H:i:s");
					break;
				case 'Y':
					$end = (new DateTime())->modify("+$length year")->format("Y-m-d H:i:s");
			}

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub->id;
			$subPeriodParams->start = date("Y-m-d H:i:s");
			$subPeriodParams->end = $end;
			$subPeriodParams->status = "FREE";
			$subPeriodParams->user_id = $user_id;

			$subPeriod = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$subPeriod->save();
			$sub->periods[] = $subPeriod;

			$sub->plan = $plan;

			$model->saveCustomFieldValues($sub->id);
			$dispatcher->trigger('onSubscriptionPlay', array(&$sub));


		}

		//send them to the receipt

		$url = JRoute::_('index.php?option=com_converge&view=receipt&sub_id='.$sub->id, false);

		$return->success = true;
		$return->url = $url;
		$return->sub_id = AxsEncryption::encrypt($sub->id, self::$subscription_password);
		//echo json_encode($return);
		return $return;
	}

	public function changeSubscription($old_sub_id, $cat, $downgrade_approval, $upgrade_approval, $new_plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user) {
		$dispatcher = JEventDispatcher::getInstance();

		//grab old subscription plan
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $old_sub_id;
		$sub = AxsPayment::newSubscription($subParams);
		$sub->getSubscriptionWithId(false);
		$old_plan = $sub->getPlan();

		$return = new stdClass();

		//see if they are downgrading or upgrading and if it needs approval or not.
		if (($new_plan->default_initial_amount + $new_plan->default_amount) > ($old_plan->default_initial_amount + $old_plan->default_amount)) {

			if (!$upgrade_approval) {
				//doesn't need upgrade approval, go and do it.
				//if they are already in grace period, we need to remove their usergroups right away
				if ($sub->status == "GRC") {
					$user = JFactory::getUser($sub->user_id);
					if (!is_array($old_plan->active_usergroup)) {
						$activeUserGroup = array($old_plan->active_usergroup);
					} else {
						$activeUserGroup = $old_plan->active_usergroup;
					}
					$user->groups = array_diff($user->groups, $activeUserGroup);
					$user->save();
				}
				$sub->cancel();
				$dispatcher->trigger('onSubscriptionStop', array(&$sub));

				//now go and create the new subscription
				$return = $this->newSubscription($new_plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user);
			} else {
				//save a requested plan change
				$model->savePlanChangeRequest($sub, $plan_id);
				//send admins email
				$dispatcher->trigger('onSubscriptionChangeRequest', array(&$sub, $new_plan));

				$app->enqueueMessage(AxsLanguage::text("AXS_CHANGE_PLAN_REQUEST_SUBMITTED", "Your change plan request was submitted!"), 'success');
				$url = JRoute::_('index.php?option=com_converge&view=plans'.($cat ? '&cat='.$cat : ''), false);
				//$app->redirect($url);

				$return->success = true;
				$return->url = $url;
			}
		} else {
			if (!$downgrade_approval) {
				//doesn't need downgrade approval, go and do it.
				//if they are already in grace period, we need to remove their usergroups right away
				if ($sub->status == "GRC") {
					$user = JFactory::getUser($sub->user_id);
					if(!is_array($old_plan->active_usergroup)) {
						$activeUserGroup = array($old_plan->active_usergroup);
					} else {
						$activeUserGroup = $old_plan->active_usergroup;
					}
					$user->groups = array_diff($user->groups, $activeUserGroup);
					$user->save();
				}
				$sub->cancel();
				$dispatcher->trigger('onSubscriptionStop', array(&$sub));

				//now go and create the new subscription
				$return = $this->newSubscription($new_plan, $user_id, $address, $city, $state, $country, $zip, $first, $last, $email, $phone, $plan_id, $model, $app, $user);
			} else {
				//save a requested plan change
				$model->savePlanChangeRequest($sub, $plan_id);
				//send admins email
				$dispatcher->trigger('onSubscriptionChangeRequest', array(&$sub, $new_plan));

				$app->enqueueMessage(AxsLanguage::text("AXS_CHANGE_PLAN_REQUEST_SUBMITTED", "Your change plan request was submitted!"), 'success');
				$url = JRoute::_('index.php?option=com_converge&view=plans'.($cat ? '&cat='.$cat : ''), false);
				//$app->redirect($url);

				$return->success = true;
				$return->url = $url;
			}
		}

		return $return;
	}

	public function retryPayment() {
		$sub_id = $this->input->get('sub_id', '0');
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$sub = AxsPayment::newSubscription($subParams);
		$sub->getSubscriptionWithId(false);
		$plan = $sub->getPlan();

		$dispatcher = JEventDispatcher::getInstance();

		//get latest sub period, this is the one we're retrying.
		$sp = $sub->periods[count($sub->periods)-1];
		if($sp->status == "NONE") {
			$amount = $sub->original_amount;

			//lets see if we can subtract from this with the rewards points
			$rewards = $dispatcher->trigger("onReprocessCalcPoints", array(&$sub->user_id, &$amount, &$sub->points));
			$rewards = $rewards[0];
			if($rewards) {
				$amount = $rewards->total;
			}

			if($amount > 0) {
				//grab a card to use
				$card = null;
				if ($sub->card_id) {
					$cardParams = AxsPayment::getCardParams();
					$cardParams->id = $sub->card_id;
					$card = AxsPayment::newCard($cardParams);
					$card->getCardWithId();
					//if they don't own the card and they aren't part of the family, try to just grab the first card available to them.
					if($card->user_id != $sub->user_id && !in_array($sub->user_id, $card->family_access)) {
						$cards = AxsPayment::getUserAllCards($sub->user_id);
						$card = $cards[0];
					}
				} else {
					//if they have no card set to use for this sub, try to just grab the first card available to them.
					$cards = AxsPayment::getUserAllCards($sub->user_id);
					$card = $cards[0];
				}

				//if there is still no charge-able card return.
				if(!$card) {
					JFactory::getApplication()->enqueueMessage(AxsLanguage::text("AXS_NO_CARD_ON_FILE", "You have no card on file that can be charged."), 'error');
				}

				$trxnParams = AxsPayment::getTransactionParams();

				$trxnParams->user_id = $sub->user_id;
		        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
		        $trxnParams->type_id = $sp->id;
		        $trxnParams->amount = $amount;
		        $trxnParams->date = date("Y-m-d H:i:s");
		        $trxnParams->card = $card;
		        $trxnParams->initiator = AxsPayment::$initiators['user'];
		        $trxnParams->description = $plan->title;

				//make charge and get status
				$trxn = AxsPayment::newTransaction($trxnParams);
				$trxn->charge();
				if($trxn->status == "SUC") {
					$sp->status = "PAID";
					$sp->save();
					//if the sub period is for the current time, set the status to active for the sub.
					$now = new DateTime();
					if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
						$sub->status = "ACT";
						$sub->save();
					}
					$dispatcher->trigger("onReprocessSuccess", array(&$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
					JFactory::getApplication()->enqueueMessage(AxsLanguage::text("AXS_TRANSACTION_SUCCESSFUL", "Transaction was successful!"));
				} else {
					JFactory::getApplication()->enqueueMessage(AxsLanguage::text("AXS_TRANSACTION_FAILED_AGAIN", "Transaction failed again."), 'error');
				}
			} else {
				$sp->status = "PAID";
				$sp->save();
				//if the sub period is for the current time, set the status to active for the sub.
				$now = new DateTime();
				if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
					$sub->status = "ACT";
					$sub->save();
				}
				$dispatcher->trigger("onReprocessSuccess", array(&$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
				JFactory::getApplication()->enqueueMessage(AxsLanguage::text("AXS_TRANSACTION_SUCCESSFUL", "Transaction was successful!"));
			}
		} else {
			JFactory::getApplication()->enqueueMessage(AxsLanguage::text("AXS_WE_DONT_SUPPORT_PRORATED", "We don't support reprocessing prorated yet."), 'error');
		}

		//send them back to the page.
		$url = JRoute::_('index.php?option=com_converge&view=cards', false);
		JFactory::getApplication()->redirect($url);
	}

	public function verifyPromo() {
		$promo_code = $this->input->get('promo', '');
		$plan = AxsPayment::getPlanById($this->input->get('plan_id', 0));
		$planParams = json_decode($plan->params);
		$dispatcher = JEventDispatcher::getInstance();

		if($planParams->tovuti_builder) {
			$initial_amount = $this->input->get('initial_amount', '');
			$recurring_amount = $this->input->get('recurring_amount', '');
			$eventRes = $dispatcher->trigger('verifyPromo', array($promo_code, $plan, null, null, $initial_amount, $recurring_amount));
		} else {
			$eventRes = $dispatcher->trigger('verifyPromo', array($promo_code, $plan, null, null));
		}


		echo json_encode($eventRes[0]);

		//store that someone entered a promo code that may / may not still be valid but DOES exist in system
		$data = new stdClass();
		$data->user_id = JFactory::getUser()->id;
		$data->trxn_id = 0;
		$data->type = AxsPayment::$trxn_types['subscription'];
		$dispatcher->trigger('storeActivity', array($data));
	}

	//AJAX CALL
	public function checkPlanPasscode() {
		$plan_id = JRequest::getVar('plan');
		$code = JRequest::getVar('code');

		//The plan is encrypted so they can't put in a code for another plan.
		$plan_id = AxsEncryption::decrypt($plan_id, AxsKeys::getKey('subscription'));

		$db = JFactory::getDBO();
		$conditions = array(
			$db->quoteName('plan_id') . '=' . (int)$plan_id,
			$db->quoteName('code') . '=' . $db->quote($code),
			$db->quoteName('active') . '=1'
		);

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_subscription_plan_codes'))
			->where($conditions)
			->setLimit(1);

		$db->setQuery($query);

		$results = $db->loadObject();

		if ($results) {

			$expire = strtotime($results->expire);

			if ($expire < 0) {
				$expire = null;
			}

			if ($expire) {
				if ($expire <= strtotime('now')) {
					echo "expired";
					return;
				}
			}

			//Check if the number of users is limited
			if (!$results->userlimit) {
				//It is limited

				//if the number of uses is null, set it to 0 for the math.
				if ($results->inuse == null) {
					$results->inuse = 0;
				}

				//The number of uses left must be greater than 0
				if (($results->maximum - $results->inuse) > 0) {
					echo "true";
				} else {
					echo "full";
				}
			} else {
				//There is no limit.  They havea  code. It's good.
				echo "true";
			}
		} else {
			//This passcode doesn't match any for the plan.
			echo "false";
		}
	}
}
