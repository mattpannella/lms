<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;
include_once 'components/shared/controllers/common.php';

$doc = JFactory::getDocument();
$localizationVars = array(
	'JSAVE' => AxsLanguage::text('JSAVE', 'Save'),
	'AXS_UPDATE' => AxsLanguage::text('AXS_UPDATE', 'Update'),
	'AXS_CONVERGE_ERROR_TRYING_TO_GET_CARD' => AxsLanguage::text('AXS_CONVERGE_ERROR_TRYING_TO_GET_CARD', 'There was an error trying to get your card.'),
	'AXS_CARD_REQUIRED' => AxsLanguage::text("AXS_CARD_REQUIRED", "Card Required"),
	'AXS_EXPIRATION_DATE_REQUIRED' => AxsLanguage::text("AXS_EXPIRATION_DATE_REQUIRED", "Expiration Date Required"),
	'AXS_FIRST_NAME_REQUIRED' => AxsLanguage::text("AXS_FIRST_NAME_REQUIRED", "First name Required"),
	'AXS_LAST_NAME_REQUIRED' => AxsLanguage::text("AXS_LAST_NAME_REQUIRED", "Last name Required"),
	'AXS_ADDRESS_REQUIRED' => AxsLanguage::text("AXS_ADDRESS_REQUIRED", "Address Required"),
	'AXS_ZIP_REQUIRED' => AxsLanguage::text("AXS_ZIP_REQUIRED", "Zip Required"),
	'AXS_CITY_REQUIRED' => AxsLanguage::text("AXS_CITY_REQUIRED", "City Required"),
	'AXS_STATE_REQUIRED' => AxsLanguage::text("AXS_STATE_REQUIRED", "State Required"),
	'AXS_ERROR_SETTING_PERMISSIONG_FOR_CARD' => AxsLanguage::text("AXS_ERROR_SETTING_PERMISSIONG_FOR_CARD", "Error trying to set permissions for this card."),
	'AXS_ERROR_GETTING_CARD_PERMISSIONS' => AxsLanguage::text("AXS_ERROR_GETTING_CARD_PERMISSIONS", "There was an error trying to get card permissions."),
	'AXS_CARD_UPDATE_SUCCESSFUL' => AxsLanguage::text("AXS_CARD_UPDATE_SUCCESSFUL", "Card update successful!"),
	'AXS_AN_ERROR_OCCURRED' => AxsLanguage::text("AXS_AN_ERROR_OCCURRED", "An Error occurred."),
	'AXS_EDIT_CARD_PERMISSIONS' => AxsLanguage::text("AXS_EDIT_CARD_PERMISSIONS", "Edit Card Permissions"),
	'AXS_CARD_SETTINGS_SAVED' => AxsLanguage::text("AXS_CARD_SETTINGS_SAVED", "Card Settings Saved!"),
	'AXS_DELETE_CARD_CONFIRMATION_MESSAGE' => AxsLanguage::text("AXS_DELETE_CARD_CONFIRMATION_MESSAGE", "Are you sure you want to delete this card?"),
	'AXS_CARD_DELETED_SUCCESSFULLY' => AxsLanguage::text("AXS_CARD_DELETED_SUCCESSFULLY", "Card deleted successfully!"),
	'AXS_ERROR_DELETING_YOUR_CARD' => AxsLanguage::text("AXS_ERROR_DELETING_YOUR_CARD", "There was an error deleting your card."),
	'AXS_ERROR_ADDING_CARD_CHECK_INFORMATION' => AxsLanguage::text("AXS_ERROR_ADDING_CARD_CHECK_INFORMATION", "There was an error trying to add your card, please make sure all the information is correct."),
	'AXS_ERROR_PLEASE_REFRESH_PAGE' => AxsLanguage::text("AXS_ERROR_PLEASE_REFRESH_PAGE", "There was an error. Refresh the page and try again please."),
	'AXS_SETTINGS_SAVED' => AxsLanguage::text("AXS_SETTINGS_SAVED", "Settings saved."),
);
$doc->addScriptDeclaration("const AXS_CARDS_LOCALIZATION_VARS = " . json_encode($localizationVars) . ";");
$doc->addScript('components/com_converge/assets/js/cards.js?v=5');
$doc->addStyleSheet('components/com_converge/assets/css/cards.css');

$user_id = JFactory::getUser()->id;

$famIds = AxsExtra::getFamilyMembers($user_id);
$profileData = AxsExtra::getUserProfileData($user_id);
$countries = AxsExtra::getAvailableCountries();

$userAllCards = AxsPayment::getUserAllCards($user_id, $famIds);

$subs = AxsPayment::getUserSubscriptions($user_id);
//TODO: eventually show event trxns too...  $eventTrxns = AxsPayment::getUserEventPurchases($user_id);
$courses = AxsPayment::getUserCoursePurchases($user_id);

$subs = AxsPayment::getUserSubscriptions($user_id);
$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$contact = AxsBrands::getContact();
$contactInfo = '';
/* $contactInfo = '
	<address>
		<strong>'.$contact->title.'</strong>
		<br>
		'.$contact->address.'
		<br>
		'.$contact->city.', '.$contact->state.' '.$contact->zip.'
		<br>
		'.$contact->phone.'
	</address>
'; */

?>
<style>
	@media print {
		body * {
			visibility: hidden;
		}
		.print-area, .print-area * {
			visibility: visible;
		}
		.print-area {
			position: absolute;
			left: 0;
			top: 0;
		}

	}
</style>
<div class="container">
	<center><div style="margin: 30px 0; font-size: 30px;"><?php echo AxsLanguage::text("AXS_PAYMENT_SETTINGS", "Payment Settings") ?></div></center>
	<?php foreach($subs as $s) {
		$latestPeriod = $s->periods[count($s->periods)-1];
		if($latestPeriod->status == 'NONE' || $latestPeriod->status == 'PART') {
			$show = true;
			foreach($latestPeriod->trxns as $t) {
				if($t->status == 'REF') {
					 $show = false;
					break;
				}
			}
			if($show) { ?>
				<div id="missedPayment" style="background-color: white; border: 2px solid gray; border-radius: 10px; padding: 0 20px; margin-bottom: 20px;">
					<h3><?php echo AxsLanguage::text("AXS_YOU_MISSED_A_PAYMENT", "You missed a payment") ?></h3>
					<table class="table">
						<tr>
							<th><?php echo AxsLanguage::text("AXS_ITEM", "Item") ?></th>
							<th><?php echo AxsLanguage::text("AXS_AMOUNT", "Amount") ?></th>
							<th><?php echo AxsLanguage::text("AXS_SETTINGS", "Settings") ?></th>
							<th><?php echo AxsLanguage::text("AXS_RETRY", "Retry") ?></th>
						</tr>
						<tr>
							<td><?php $plan = $s->getPlan(); echo $plan->title; ?></td>
							<td><?php echo $currency_symbol.$s->original_amount; ?></td>
							<td><button id="updatePlanSettings" class="btn btn-primary"><?php echo AxsLanguage::text("AXS_UPDATE_SETTINGS", "Update Settings") ?></button></td>
							<td><a href="/index.php?option=com_converge&task=subscriptions.retryPayment&sub_id=<?php echo $s->id; ?>" id="retryPayment" class="btn btn-success"><?php echo AxsLanguage::text("AXS_RETRY_PAYMENT", "Retry Payment") ?></a></td>
						</tr>
					</table>
				</div>
			<?php } ?>
		<?php }
	} ?>

	<?php
		$subscriptions_active = AxsDbAccess::checkAccess('subscriptions');
	?>

	<ul id="mytabs" class="nav nav-tabs">
		<li class="active"><a data-toggle="pill" href="#cards"><?php echo AxsLanguage::text("AXS_CARD_MANAGER", "Card Manager") ?></a></li>
		<?php if ($subscriptions_active) {?>
			<li><a data-toggle="pill" href="#rewards-settings"><?php echo AxsLanguage::text("AXS_MY_RECURRING_PAYMENTS_SETTINGS", "My Recurring Payments Settings") ?></a></li>
		<?php } ?>
		<li><a data-toggle="pill" href="#card-transactions"><?php echo AxsLanguage::text("AXS_MY_CREDIT_CARD_TRANSACTIONS", "My Credit Card Transactions") ?></a></li>
	</ul>
	<div class="tab-content">

		<div class="tab-pane active" id="cards">
			<table class="table table-bordered table-striped table-responsive">
				<tr>
					<th><?php echo AxsLanguage::text("AXS_CARD_OWNER", "Card Owner") ?></th>
					<th><?php echo AxsLanguage::text("AXS_CARD_NUMBER", "Card #") ?></th>
					<th><?php echo AxsLanguage::text("AXS_DELETE_CARD", "Delete Card") ?></th>
				</tr>
				<?php $cardCount = count($userAllCards);
				if($cardCount) { ?>
					<?php foreach($userAllCards as $c) {
						foreach($c->family_access as $uid) {
							$profile = ginProfile($uid);
						}
						$canEdit = $user_id == $c->user_id ? '' : 'disabled'; //can only edit cards that are theirs.
						$profile = ginProfile($c->user_id);
						$cardOwner = ($user_id == $c->user_id) ? 'My Card' : $profile->firstname.' '.$profile->lastname;
						?>
						<tr>
							<td><?php echo $cardOwner; ?></td>
							<td><?php echo AxsLanguage::text("AXS_ENDING_IN", "Ending in") ?> <?php echo sprintf('%04d', $c->cardNumber); ?></td>
							<td><button class="deleteCard btn btn-danger" data-card="<?php echo $c->cardNumber; ?>" data-cardid="<?php echo $c->id; ?>"  <?php echo $canEdit ? $canEdit : ($cardCount == 1 ? 'disabled title="You can\'t delete your only card on file."' : ''); ?>><span class="lizicon lizicon-bin"></span> <?php echo AxsLanguage::text("JACTION_DELETE", "Delete") ?></button></td>
						</tr>
					<?php } ?>
				<?php } else { ?>
					<tr><td colspan="4"><?php echo AxsLanguage::text("AXS_YOU_HAVE_NO_CARDS_ON_FILE", "You have no cards on file.") ?></td></tr>
				<?php } ?>
				<tr>
					<td colspan="4"><button class="addCard btn btn-success" data-toggle="modal" data-target="#cardModal"><span class="lizicon lizicon-plus"></span> <?php echo AxsLanguage::text("AXS_ADD_NEW_CARD", "Add New Card") ?></button></td>
				</tr>
			</table>
		</div>

		<?php if ($subscriptions_active) { ?>
		<div class="tab-pane fade" id="rewards-settings">
			<table class="table table-bordered table-striped table-responsive">
				<tr>
					<th><?php echo AxsLanguage::text("AXS_YOUR_ACTIVE_SUBSCRIPTIONS", "Your active subscriptions") ?></th>
					<th><?php echo AxsLanguage::text("AXS_TERM_LENGTH", "Term Length") ?></th>
					<th><?php echo AxsLanguage::text("AXS_REWARD_POINTS_TO_USE", "Rewards Points to use") ?></th>
					<th><?php echo AxsLanguage::text("AXS_CARD_TO_CHARGE", "Card to charge<") ?></th>
					<th><?php echo AxsLanguage::text("JSAVE", "Save") ?></th>
				</tr>
				<?php foreach($subs as $s) { ?>
					<tr>
						<td><?php echo $s->getPlan()->title; ?></td>
						<td><?php echo $s->getPeriodLengthName(); ?></td>
						<td><input type="text" class="usePoints form-control" value="<?php echo $s->points; ?>" data-subid="<?php echo $s->id; ?>"/></td>
						<td>
							<select class="useCard form-control" data-subid="<?php echo $s->id; ?>">
								<?php foreach($userAllCards as $c) { ?>
									<option value="<?php echo $c->id; ?>" <?php if($c->id == $s->card_id) echo 'selected'?>><?php echo AxsLanguage::text("AXS_ENDING_IN", "Ending in") ?> <?php echo sprintf('%04d', $c->cardNumber); ?></option>
								<?php } ?>
							</select>
						</td>
						<td><button class="savePlan btn btn-primary" data-subid="<?php echo $s->id; ?>"><span class="lizicon lizicon-checkmark"></span> <?php echo AxsLanguage::text("AXS_SAVE_CHANGES", "Save Changes") ?></button></td>
					</tr>
				<?php } ?>
			</table>
		</div>
		<?php } ?>

		<div class="tab-pane fade" id="card-transactions">
			<div class="panel-group" id="accordion">
				<?php for($i=1; $i<=count($subs); $i++) { $s = $subs[$i-1]; ?>
					<div class="panel panel-default  <?php echo $i == 1 ? 'print-area' : ''; ?>">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="expand pull-left" data-toggle="collapse" href="#collapse<?php echo $i; ?>" data-parent="#accordion"><span class="lizicon <?php echo $i == 1 ? 'lizicon-minus' : 'lizicon-plus'; ?>"></span></a>
								<?php echo $s->getPlan()->title; ?>
							</h4>
						</div>
						<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : ''; ?>">
							<div class="panel-body">
								<button class="btn btn-primary pull-right" style="margin-bottom: 10px;" onclick="window.print()"><span class="lizicon lizicon-printer"></span> <?php echo AxsLanguage::text("JGLOBAL_PRINT", "Print") ?></button>
								<div class="container">
								    <div class="row">
								        <div class="well col-xs-12 col-sm-12 col-md-8  col-md-offset-2">
								            <div class="row">
								                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
								                    <?php echo $contactInfo; ?>
								                </div>
								            </div>
								            <div class="row">
								                <div class="text-center">
								                    <h1><?php echo AxsLanguage::text("AXS_RECEIPT", "Receipt") ?></h1>
								                </div>
								                </span>
								                <table class="table table-hover">
								                    <thead>
								                        <tr>
								                            <th><?php echo AxsLanguage::text("AXS_PRODUCT", "Product") ?></th>
								                            <th><?php echo AxsLanguage::text("AXS_CARD", "Card") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("AXS_AMOUNT", "Amount") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("JDATE", "Date") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("JSTATUS", "Status") ?></th>
								                        </tr>
								                    </thead>
								                    <tbody>
								                    	<?php
															$count = 0;
															$s->periods = is_array($s->periods) ? array_reverse($s->periods) : [];
															foreach($s->periods as $p) {
																$p->trxns = is_array($p->trxns) ? array_reverse($p->trxns) : [];
																$count += count($p->trxns);

																foreach($p->trxns as $t):
																?>
																	<tr>
											                            <td class="col-md-9 text-left"><em><?php echo $s->getPlan()->title; ?></em></h4></td>
											                            <td class="col-md-1" style="text-align: center"><?php echo $t->card->cardNumber; ?></td>
											                            <td class="col-md-1 text-center"><?php echo $currency_symbol.$t->amount; ?></td>
											                            <td class="col-md-1 text-center"><?php echo AxsExtra::format_date($t->date); ?></td>
											                            <td class="col-md-1 text-right"><?php echo $t->getStatusName(); ?></td>
											                        </tr>
																<?php
																endforeach;
															}
															if(!$count) { ?>
																<tr><td colspan="4"><?php echo AxsLanguage::text("AXS_YOU_DONT_HAVE_TRANSACTIONS_SUBSCRIPTION", "You don't have any transactions for this subscription.") ?></td></tr>
														<?php } ?>
								                    </tbody>
								                </table>
								            </div>
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
				<?php foreach($courses as $c) { $i++; ?>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="expand pull-left" data-toggle="collapse" href="#collapse<?php echo $i; ?>" data-parent="#accordion"><span class="lizicon <?php echo $i == 1 ? 'lizicon-minus' : 'lizicon-plus'; ?>"></span></a>
								<?php echo $c->getCourse()->title; ?>
							</h4>
						</div>
						<div id="collapse<?php echo $i; ?>" class="panel-collapse collapse <?php echo $i == 1 ? 'in' : ''; ?>">
							<div class="panel-body">
								<?php if(!empty($c->payment) && get_class($c->payment) == 'Transaction') { ?>
									<button class="btn btn-primary pull-right" style="margin-bottom: 10px;" onclick="window.print()"><span class="lizicon lizicon-printer"></span> <?php echo AxsLanguage::text("JGLOBAL_PRINT", "Print") ?></button>
								<div class="container">
								    <div class="row">
								        <div class="well col-xs-12 col-sm-12 col-md-8  col-md-offset-2">
								            <div class="row">
								                <div class="col-xs-6 col-sm-6 col-md-6 text-left">
								                    <?php echo $contactInfo; ?>
								                </div>
								            </div>
								            <div class="row">
								                <div class="text-center">
								                    <h1><?php echo AxsLanguage::text("AXS_RECEIPT", "Receipt") ?></h1>
								                </div>
								                </span>
								                <table class="table table-hover">
								                    <thead>
								                        <tr>
								                            <th><?php echo AxsLanguage::text("AXS_PRODUCT", "Product") ?></th>
								                            <th><?php echo AxsLanguage::text("AXS_CARD", "Card") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("AXS_AMOUNT", "Amount") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("JDATE", "Date") ?></th>
								                            <th class="text-center"><?php echo AxsLanguage::text("JSTATUS", "Status") ?></th>
								                        </tr>
								                    </thead>
								                    <tbody>
														<tr>
								                            <td class="col-md-9 text-left"><em><?php echo $c->getCourse()->title; ?></em></h4></td>
								                            <td class="col-md-1" style="text-align: center"><?php echo $c->payment->card; ?></td>
								                            <td class="col-md-1 text-center"><?php echo $currency_symbol.$c->payment->amount; ?></td>
								                            <td class="col-md-1 text-center"><?php echo AxsExtra::format_date($c->payment->date); ?></td>
								                            <td class="col-md-1 text-right"><?php echo $c->payment->getStatusName(); ?></td>
								                        </tr>
								                    </tbody>
								                </table>
								            </div>
								        </div>
								    </div>
								</div>
								<?php } else {
									//TODO: this is for pplan junk
								} ?>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="vertical-alignment-helper">
			<div class="modal-dialog vertical-align-center">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?php echo AxsLanguage::text("JLIB_HTML_BEHAVIOR_CLOSE", "Close") ?></span></button>
						<h4 class="modal-title" id="myModalLabel"><?php echo AxsLanguage::text("AXS_ADD_NEW_CARD", "Add New Card") ?></h4>
					</div>
					<div class="modal-body" id="myModalBody"></div>
					<div class="modal-footer">
						<button type="button" class="saveCard btn btn-success"><?php echo AxsLanguage::text("JAPPLY", "Save") ?></button>
						<button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo AxsLanguage::text("JCANCEL", "Cancel") ?></button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="cardModal" tabindex="-1" role="dialog" aria-labelledby="cardModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span class="sr-only"><?php echo AxsLanguage::text("AXS_CLOSE", "Close") ?></span></button>
                    <h4 class="modal-title" id="cardModalLabel"><?php echo AxsLanguage::text("AXS_ADD_NEW_CARD", "Add New Card") ?></h4>
                </div>
                <div class="modal-body" id="cardModalBody">
                    <form id="saveCardForm" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $profileData->user_id; ?>" data-gtw="<?php echo $brand->billing->gateway_type; ?>">
                    <h4><?php echo AxsLanguage::text("AXS_CARD_INFO", "Card Info") ?></h4>

                    <?php if($brand->billing->gateway_type == 'stripe') {
                        $stripeParams = new stdClass();
                        $stripeParams->form_id = 'saveCardForm';
                        $stripeForm = AxsPayment::buildStipePaymentForm($stripeParams);
                        echo $stripeForm;
                    } else { ?>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_CREDIT_CARD_NUMBER", "Credit Card Number") ?><span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="card" value="">
                        </div>
                    </div>
                    <div class="form-group" style="display: block;">
                        <label for="month" class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_EXPIRATION_DATE", "Expiration Date") ?><span class="required">*</span></label>
                        <div class="col-md-8">
							<select name="exp_month" id="exp_month" class="input-small form-control" style="display: inline-block; width: 165px;">
								<?php
									for ($i = 1; $i <= 13; $i++) {
										if ($i < 10) {
											$i = "0" . $i;
										}
										?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
										<?
									}
								?>
							</select>
							/
							<select name="exp_year" id="exp_year" class="input-small form-control" style="display: inline-block; width: 165px;">
								<?php
									$cur_year = (int)date("Y");

									for ($i = $cur_year; $i <= $cur_year + 10; $i++) {
										?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
										<?php
									}
								?>
							</select>
						</div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_CVV_CODE", "CVV Code") ?><span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="cvv" value="">
                        </div>
                    </div>
                    <hr>
                    <h4><?php echo AxsLanguage::text("AXS_BILLING_ADDRESS", "Billing Address") ?></h4>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_FIRST_NAME", "First Name") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="first" class="form-control" value="<?php echo $profileData->first; ?>"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_LAST_NAME", "Last Name") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="last" class="form-control" value="<?php echo $profileData->last; ?>"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_ADDRESS", "Address") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="addr" class="form-control" value="<?php echo $profileData->address; ?>" maxlength="30"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_CITY", "City") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="city" class="form-control" value="<?php echo $profileData->city; ?>" maxlength="30"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_STATE", "State") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="state" class="form-control" value="<?php echo $profileData->state; ?>" maxlength="30"></div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_ZIP_CODE", "Zip Code") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="zip" class="form-control" value="<?php echo $profileData->zip; ?>" maxlength="9"></div>
                    </div>
                    <div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_COUNTRY", "Country") ?><span class="required">*</span></label>
						<div class="col-md-8">
							<select name="country" class="input-small form-control" required>
								<?php foreach($countries as $c) { ?>
									<option value="<?php echo $c; ?>"><?php echo $c; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-md-4 control-label"><?php echo AxsLanguage::text("AXS_PHONE", "Phone") ?><span class="required">*</span></label>
                        <div class="col-md-8"><input type="text" name="phone" class="form-control" value="<?php echo $profileData->phone; ?>"></div>
                    </div>
                    <?php } ?>
                </form>
                </div>
                <div class="modal-footer">

                    <button type="button" id="add-card-button" class="saveCard btn btn-success" <?php echo $brand->billing->gateway_type == 'stripe' ? 'disabled="disabled"' : ''; ?>><?php echo AxsLanguage::text("JSAVE", "Save") ?> now</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo AxsLanguage::text("JCANCEL", "Cancel") ?></button>
                </div>
            </div>
        </div>
    </div>

	<form id="saveCardPermissionsForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $user_id; ?>">
		<h4><?php echo AxsLanguage::text("AXS_FAMILY_MEMBER_ACCESS", "Family member access") ?>:</h4>
		<table class="table table-bordered table-striped table-responsive">
			<tr>
				<th><?php echo AxsLanguage::text("AXS_PROFILE", "Profile") ?></th>
				<th><?php echo AxsLanguage::text("AXS_NAME", "Name") ?></th>
				<th><?php echo AxsLanguage::text("AXS_MEMBER_ID", "Member ID") ?></th>
				<th><?php echo AxsLanguage::text("AXS_CAN_USE_CARD", "Can Use Card") ?></th>
			</tr>
			<?php $famIds = array_diff($famIds, array($user_id)); if(count($famIds)) { ?>
				<?php foreach($famIds as $fm) { $profile = ginProfile($fm); ?>
					<tr>
						<td><img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;"></td>
						<td><?php echo $profile->firstname.' '.$profile->lastname; ?></td>
						<td><?php echo $fm; ?></td>
						<td><input type="checkbox" name="<?php echo $fm; ?>" /></td>
					</tr>
				<?php } ?>
			<?php } else { ?>
				<tr><td colspan="4"><?php echo AxsLanguage::text("AXS_YOU_HAVE_NO_FAMILY_MEMBERS_SIGNED_UP", "You don't have any family members signed up so you cannot set permissions for this card.") ?></td></tr>
			<?php } ?>
		</table>
	</form>

</div>
