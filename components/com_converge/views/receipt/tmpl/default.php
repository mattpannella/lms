<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/8/16
 * Time: 1:44 PM
 */

defined('_JEXEC') or die;

$sub_id = $this->input->get('sub_id', 0);
$subParams = AxsPayment::getSubscriptionParams();
$subParams->id = $sub_id;
$sub = AxsPayment::newSubscription($subParams);
$sub->getSubscriptionWithId(false);

$user = JFactory::getUser();
$app = JFactory::getApplication();

//refresh the user groups in the session
JFactory::getSession()->set('user', new JUser($user->id));
AxsActions::storeAction($user->id,'login');	
$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$encrypt = AxsEncryption::encrypt($user->id, 'interuser5');
setcookie('int_usr', $encrypt,0,'/');

if($user->id != $sub->user_id) {
	$message = AxsLanguage::text("AXS_YOU_DONT_HAVE_ACCESS_TO_RECEIPT", "You don't have access to view this receipt.");
	$app->enqueueMessage($message, 'error');
} else {
	$plan = $sub->getPlan();

	$params = json_decode($plan->params);
	if ($params->redirect_on_signup) {
		if ($params->redirect_on_signup_link != "") {
			header("Location: " . $params->redirect_on_signup_link);
		}
	}

	if($sub->periods[0]->status == "PAID") {		
		$showTrxn = true;
	} else {
		$showTrxn = false;
	} ?>
	<style>
		@media print {
			body * {
				visibility: hidden;
			}
			.print-area, .print-area * {
				visibility: visible;
			}
			.print-area {
				position: absolute;
				left: 0;
				top: 0;
			}
		}
	</style>
	<div class="container print-area">
		<div style="width: 100%;">
			<h1 style="float: left;"><?php echo AxsLanguage::text("AXS_YOU_ARE_NOW_SUBSCRIBED_TO", "You are now subscribed to") ?> <?php echo $plan->title; ?></h1>
			<button style="float: right; margin-top: 20px;" class="btn btn-primary" onclick="window.print()"><?php echo AxsLanguage::text("AXS_PRINT_RECEIPT", "Print receipt") ?></button>
		</div>
		<table class="table .table-hover">
			<tr><td><?php echo AxsLanguage::text("AXS_SIGNUP_DATE", "Signup date") ?></td><td><?php echo AxsExtra::format_date($sub->periods[0]->start); ?></td></tr>
			<?php if($showTrxn) { ?>
				<tr><td><?php echo AxsLanguage::text("AXS_AMOUNT", "Amount") ?></td><td><?php echo $currency_symbol.$sub->periods[0]->trxns[0]->amount; ?></td></tr>
				<tr><td><?php echo AxsLanguage::text("AXS_CARD_USED", "Card Used") ?></td><td><?php echo AxsLanguage::text("AXS_ENDING_IN", "Ending in") ?> <?php echo $sub->periods[0]->trxns[0]->card; ?></td></tr>
				<tr><td><?php echo AxsLanguage::text("AXS_NEXT_CHARGE_DATE", "Next charge date") ?></td><td><?php echo AxsExtra::format_date($sub->periods[0]->end); ?></td></tr>
			<?php } ?>
		</table>
	</div>
<?php }