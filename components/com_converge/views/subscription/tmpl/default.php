<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 6/28/16
 * Time: 2:16 PM
 */

defined('_JEXEC') or die();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

unset($_SESSION['aws_creation']);

$db = JFactory::getDbo();
$query = $db->getQuery(true);

$app = JFactory::getApplication();
$user_id = JFactory::getUser()->id;
$cards = AxsPayment::getUserAllCards($user_id);

//first grab the brand
$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$contact = AxsBrands::getContact();

//now check for a get param of plan_id=id
$input = $app->input;
$plan_id = $input->get('plan_id', 0, 'INT');
$plan_encrypted = AxsEncryption::encrypt($plan_id, AxsKeys::getKey('subscription'));
?>

<script>
	var plan_id = "<?php echo $plan_encrypted; ?>";
</script>

<?php

//brand_req is set if it is coming from a single plan view menu item.
$brand_req = $input->get('brand_req', 0, 'INT');	//1 = No Brand Check

//The $_SESSION['sub_plan_list'] is only set if it is coming from a category listing page of plans.
if (isset($_SESSION['sub_plan_list'])) {

	//IF the ID is set, it's from a page that allows skipping the brand check.
	if (isset($_SESSION['sub_plan_list'][$plan_id]) && $_SESSION['sub_plan_list'][$plan_id]) {
		$any_cat = 1;
	} else {
		$any_cat = 0;
	}
}

$conditions = array(
	$db->quoteName('plan.published') . '=1',
	$db->quoteName('cat.published') . '=1',
	$db->quoteName('plan.id') . '=' . (int)$plan_id
);

if ($any_cat || $brand_req) {
	$skip = 1;
}

if (!$skip) {
	$conditions[]= $db->quoteName('cat.brand_id') . '=' . (int)$brand->id;
}

//grab the plan
$query
	->select('plan.*, cat.params as cat_params')
	->from('axs_pay_subscription_plans AS plan')
	->leftJoin('axs_pay_subscription_categories AS cat ON plan.cat_id = cat.id')
	->where($conditions);

$db->setQuery($query);
$plan = $db->loadObject();

if (!is_object($plan)) {
	$message = AxsLanguage::text("AXS_SUBSCRIPTION_PLAN_IS_INVALID", "This subscription plan is invalid.");
	//return them to the plan page
	$app->enqueueMessage($message, 'error');
	$url = JRoute::_('index.php?option=com_converge&view=plans', false);
	$app->redirect($url);
}

//jdump($plan);
$params = json_decode($plan->params);
$cat_params = json_decode($plan->cat_params);

//check to see if they can sign up for this plan
$subs = AxsPayment::getUserSubscriptions($user_id);
foreach($subs as $s) {
	if ($s->plan_id == $plan->id  && ($s->status == "ACT" || $s->status == "GRC") && !$cat_params->multi_purchase) {
		$message = AxsLanguage::text("AXS_YOU_ARE_ALREADY_SUBSCRIBED", "You are already subscribed to this plan.");
		//return them to the plan page
		$app->enqueueMessage($message, 'error');
		$url = JRoute::_('index.php?option=com_converge&view=plans', false);
		$app->redirect($url);
		break;
	}
}

//Check to see if there is a passcode required for this plan

$usepasscode = false;
if ($params->usepasscodes == "yes") {
	$usepasscode = true;

	//Get the passcodes
	$conditions = array(
		$db->quoteName('plan_id') . '=' . $db->quote($plan_id),
		$db->quoteName('active') . '= 1'
	);

	$query
		->clear()
		->select('*')
		->from($db->quoteName('axs_subscription_plan_codes'))
		->where($conditions);

	$db->setQuery($query);
	$passcodes = $db->loadObjectList();

	//Make sure there's an active and available code.  If not, registration for this plan is full.
	$activeCode = false;
	foreach ($passcodes as &$code) {
		if ($code->active == 1) {



			if ($code->userlimit == "0") {
				//If none are used, it's null.
				if (!$code->inuse) {
					$code->inuse = 0;
				}

				$code->codesleft = $code->maximum - $code->inuse;

				if ($code->codesleft > 0) {
					$activeCode = true;
				}
			} else {
				$activeCode = true;
			}
		}
	}
}

//grab the custom registration fields linked to this plan
$field_ids = json_decode($plan->reg_fields);
$fields = array();

if (is_countable($field_ids) && count($field_ids)) {
	$ids = implode(', ', $field_ids);
	$query = "SELECT * FROM joom_community_fields WHERE id IN ($ids) ORDER BY ordering;";
	$db->setQuery($query);
	$fields = $db->loadObjectList();
}

if (($plan->default_initial_amount > 0) || ($plan->default_amount > 0)) {
	$params->require_address = 1;
}

?>

<style>
	.plan-title {
		color: black;
	}
	.sub-heading {
		font-size: 20px;
		color: black;
	}
	.control-label {
		color: black;
	}
	.required {
		color: red;
	}
	hr {
		border-color: #a6a6a6;
	}
	#registration .input-group {
		padding: 0 15px;
	}

	.passswordMismatchMessage {
		color: red;
		display: none;
	}

	<?php
		if ($usepasscode) {
	?>
			#registration {
				display:none;
			}

			.enter_code {
				background-color: rgb(200,200,200);
				padding: 20px;
				border-radius: 8px;
			}

			.error_messages {
				background-color: rgb(255,220,220);
				padding: 10px;
				border-radius: 4px;
				font-size: 16px;
				margin-top: 8px;
				display: inline-block;
			}
	<?php
		}
	?>
</style>

<?php

	$saas_view = $params->tovuti_builder;
	if ($saas_view === "1") {
		$saas_view = true;
	} else {
		$saas_view = false;
	}

	$show_title = true;
	$hide_subscription = false;

	if ($saas_view) {

		$dispatcher = JEventDispatcher::getInstance();
		$saasSetup = $dispatcher->trigger('setupSaas');

		if ($saasSetup) {
			$show_title = false;
			$hide_subscription = true;
		}
	}

	if ($show_title) {
?>
		<h1 class="plan-title">
			<?php echo $plan->title; ?>
		</h1>
<?php
	}

	if ($usepasscode) {
		if ($activeCode) {
			$error = $params->error;

?>
			<div class="enter_code">

				<?php
					if ($params->requirement_text) {
						echo $params->requirement_text;
					} else {
						echo AxsLanguage::text("AXS_PASSCODE_REQUIRED_TO_SIGN_UP_PLAN", "A passcode is required to sign up for this plan.");
					}
				?>
				<br/>
				<hr/>
				<br/>
				<div class="form-group">
					<label class="col-md-4 control-label" style="text-align: right;"><?php echo AxsLanguage::text("AXS_PASSCODE", "Passcode") ?>: <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="text" required="true" id="user_passcode" class="form-control" name="user_passcode">
					</div>
					<div class="col-md-4">
						<span id="user_passcode_button" class="btn btn-primary"><i class="fa fa-key"></i> <?php echo AxsLanguage::text("AXS_GET_ACCESS", "Get Access") ?></span>
					</div>
				</div>
				<br>
				<div class='clearfix'></div>
				<div id="invalid_passcode" class="error_messages" style="display:none;">
					<?php
						if ($error->invalid) {
							echo $error->invalid;
						} else {
							echo AxsLanguage::text("AXS_THIS_IS_AN_INVALID_PASSCODE", "This is an invalid passcode.");
						}
					?>
				</div>
				<div id="full_passcode" class="error_messages" style="display:none;">
					<?php
						if ($error->full) {
							echo $error->full;
						} else {
							echo AxsLanguage::text("AXS_THIS_PASSCODE_HAS_NO_MORE_USES_LEFT", "This passcode has no more uses left.");
						}
					?>
				</div>
				<div id="expired_passcode" class="error_messages" style="display:none;">
					<?php
						if ($error->expired) {
							echo $error->expired;
						} else {
							echo AxsLanguage::text("AXS_THIS_PASSCODE_HAS_EXPIRED", "This passcode has expired.");
						}
					?>
				</div>

				<script>

					var plan_id = "<?php echo $plan_encrypted; ?>";

					var typecheckTimeout = null;
					var typecheck = function(val) {
						if (typecheckTimeout) {
							clearTimeout(typecheckTimeout);
						}
						typecheckTimeout = setTimeout(
							function() {

								$.ajax({
									type: 'GET',
									data: {
										format: 'raw',
										plan: plan_id,
										code: val,
										option: 'com_converge',
										task: 'subscriptions.checkPlanPasscode'
									},
									url: "/index.php?",
									success:
										function(response) {
											console.log(response);

											$(".error_messages").hide(250);
											$("#registration").hide(250);

											switch (response) {
												case "true":
													$("#registration").show(250);
													$("#subscriptioncode").val(val);
													break;
												case "full":
													$("#registration").hide(250);
													$("#full_passcode").show(250);
													break;
												case "expired":
													$("#registration").hide(250);
													$("#expired_passcode").show(250);
													break;
												case "false":
													$("#registration").hide(250);
													$("#invalid_passcode").show(250);
													break;
											}
										}
								});

							}, 1000
						);
					}

					$("#user_passcode").on(
						'keyup paste change',
						function() {
							typecheck(this.value);
						}
					);

					$("#user_passcode_button").click(function(){
						typecheck($("#user_passcode").val());
					});
				</script>
			</div>
<?php
		} else {
?>
			<div class="no_codes">
				<?php
					if ($params->noaccess) {
						echo $params->noaccess;
					} else {
						echo AxsLanguage::text("AXS_PLAN_REGISTRATION_NOT_AVAILBALE", "Registration for this plan is currently not available");
					}
				?>
			</div>
<?php
		}
	}
?>

<form id="registration" class="form form-horizontal hide-on-build" data-gtw="<?php echo $brand->billing->gateway_type; ?>" method="post" enctype="multipart/form-data" autocomplete="on">
	<div id="appForm" style="display:none;"></div>
	<div id="serverForm" style="display:none;"></div>
	<div class="form-group">
		<input id="subscriptioncode" type="hidden" class="form-control" name="subscriptioncode"/>
	</div>
	<div
		id="registrationForm"
		<?php
			if ($hide_subscription) {
		?>
				style="display:none;"
		<?php
			}
		?>
	>
		<?php
			if (!$user_id) { //display additional registration fields
		?>
				<div class="sub-heading"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_ACCOUNT_INFORMATION', 'Account Information'); ?></div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_FIRST_NAME', 'First Name'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="text" required="true" class="form-control" name="first" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_LAST_NAME', 'Last Name'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="text" required="true" class="form-control" name="last" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_EMAIL', 'Email'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="email" required="true" class="form-control" name="email" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_USERNAME', 'Username'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="text" required="true" class="form-control" name="username" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_PASSWORD', 'Password'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="password" required="true" class="form-control" name="password" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CONFIRM_PASSWORD', 'Confirm password'); ?> <span class="required">*</span></label>
					<div class="col-md-4">
						<input type="password" required="true" class="form-control" name="password2" />
						<div class="passswordMismatchMessage"></div>
					</div>
				</div>

				<?php
					if ($params->include_address || $plan->default_initial_amount > 0 || $plan->default_amount > 0) {
				?>

						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_ADDRESS', 'Address'); ?> <?php echo $params->require_address ? ' <span class="required">*</span>' : ''; ?></label>
							<div class="col-md-4">
								<input type="text" required="true" class="form-control" name="address" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CITY', 'City'); ?> <?php echo $params->require_address ? ' <span class="required">*</span>' : ''; ?></label>
							<div class="col-md-4">
								<input type="text" required="true" class="form-control" name="city" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_STATE', 'State'); ?> <?php echo $params->require_address ? ' <span class="required">*</span>' : ''; ?></label>
							<div class="col-md-4">
								<input type="text" required="true" class="form-control" name="state" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_COUNTRY', 'Country'); ?> <?php echo $params->require_address ? ' <span class="required">*</span>' : ''; ?></label>
							<div class="col-md-4">
								<select required="true" class="form-control" name="country">
									<?php $countries = AxsExtra::getAvailableCountries();
									foreach($countries as $c) {
										echo '<option value="'.$c.'" '.($c == "United States" ? "selected" : "").'>'.$c.'</option>';
									} ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_ZIP', 'Zip'); ?> <?php echo $params->require_address ? ' <span class="required">*</span>' : ''; ?></label>
							<div class="col-md-4">
								<input required="true" type="text" class="form-control" name="zip" />
							</div>
						</div>
				<?php
					}
				?>
		<?php
			} else {
		?>
				<div class="sub-heading">
					<?php echo AxsLanguage::text('COM_SUBSCRIPTION_ACCOUNT_INFORMATION', 'Account Information'); ?>
				</div>
		<?php
			}

			//TODO: check if the rewards program is turned on or off, show.
			$dispatcher = JEventDispatcher::getInstance();
			$res = $dispatcher->trigger('onGetRefid');
			$refid = $res[0];

			if ($brand->billing->rewards_active && !$user_id && !$refid) {
		?>
				<div class="form-group">
					<label class="col-md-4 control-label" title="<?php echo AxsLanguage::text("AXS_ENTER_AFFILIATE_CODE_FROM_REFERRER", "Enter the affiliate code of the person who referred you.") ?>">
						<?php echo AxsLanguage::text("COM_SUBSCRIPTION_AFFILIATE_CODE", "Sponsor's Affiliate Code"); ?>
					</label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="refid" />
					</div>
				</div>
		<?php
			}

			foreach ($fields as $field) {
			$html = AxsHTML::getField($field);
			$fieldName = '';
			$fieldParams = json_decode($field->params);
			if($fieldParams->registration_text) {
				$fieldName = $fieldParams->registration_text;
			} else {
				$fieldName = $field->name;
			}
		?>
				<div class="form-group">
					<label class="col-md-4 control-label">
						<?php echo AxsLanguage::text($fieldName, $fieldName);  echo $field->required ? ' <span class="required">*</span>' : ''; ?>
					</label>
					<div class="col-md-4">
						<?php echo $html; ?>
					</div>
				</div>
		<?php
			}

			if (($plan->default_initial_amount > 0) || ($plan->default_amount > 0) || $saas_view) {

				if (!$saas_view) {
					$plan_initial_amount = $plan->default_initial_amount;
					$plan_default_amount = $plan->default_amount;
				} else {
					$plan_initial_amount = 0;
					$plan_default_amount = $plan->default_amount;
				}
		?>
				<hr>
				<div class="sub-heading"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_PAYMENT_INFORMATION', 'Payment Information'); ?></div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_INITIAL_SIGNUP', 'Initial Price'); ?></label>
					<div class="col-md-4 input-group">
						<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
						<input id="initial_amount" type="text" class="form-control" readonly value="<?php echo $plan_initial_amount; ?>" name="initial_amount" />
					</div>
				</div>
				<div class="form-group" <?php if($plan_default_amount == 0) { echo 'style="display:none;"';  } ?> >
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_SUBSEQUENT_DUES', 'Recurring Dues'); ?></label>
					<div class="col-md-4 input-group">
						<span class="input-group-addon"><?php echo $currency_symbol; ?></span>
						<input id="default_amount" type="text" class="form-control" readonly value="<?php echo $plan_default_amount; ?>" name="recurring_amount" />
					</div>
				</div>
				<div class="form-group" id="promoGroup">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_PROMO_CODE', 'Promo Code'); ?></label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="promo" />
						<span id="promoHelpText" class="help-block"></span>
					</div>
					<div class="col-md-4">
						<button id="applyPromo" type="button" class="btn btn-success" data-planid="<?php echo $plan_id; ?>">
							<?php echo AxsLanguage::text('COM_SUBSCRIPTION_APPLY', 'Apply'); ?></button>
					</div>
				</div>
			<div class="payment_information">
				<?php
					if ($user_id && !empty($cards)) {
				?>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_USE_CARD_ON_FILE', 'Card on File'); ?> <span class="required">*</span></label>
							<div class="col-md-4">
								<select class="form-control" name="card_on_file" id="card_on_file">
									<option value="0">
										--<?php echo AxsLanguage::text('COM_SUBSCRIPTION_SELECT_CARD', 'choose card'); ?>--
									</option>
									<?php
										foreach($cards as $c) {
									?>
											<option value="<?php echo $c->id; ?>">
												<?php echo $c->cardNumber; ?>
											</option>
									<?php
										}
									?>
								</select>
							</div>
						</div>
						<div id="new-card-div" class="form-group">
							<div class="col-md-offset-4 col-md-4" style="text-align: center;">
								<p><?php echo AxsLanguage::text('COM_SUBSCRIPTION_USE_NEW_CARD', 'or use a new card'); ?></p>
							</div>
						</div>
				<?php
					}
				?>

				<?php
					if($brand->billing->gateway_type == 'stripe') {
	                    $stripeForm = AxsPayment::buildStipePaymentForm();
	            ?>
	                    <div class="form-group">
	                    	<label class="col-md-2 control-label">
	                    	</label>
	                    	<div class="col-md-7" style="padding-left: 35px;">
	                    		<?php echo $stripeForm; ?>
	                    	</div>
	                    </div>
	            <?php
	                } else {
				?>

				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CARD_NUMBER', 'Credit Card Number'); ?></label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="card" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CARD_EXPIRATION_DATE', 'Expiration date'); ?></label>
					<div class="col-md-4">
						<select name="exp_month" class="form-control" style="width: 48%; display: inline;">
							<?php
								for ($i = 1; $i <= 12; $i++) {
									if ($i < 10) {
										$i = "0" . $i;
									}
							?>
										<option value="<?php echo $i;?>"><?php echo $i;?></option>
							<?
								}
							?>
						</select> /
						<select name="exp_year" class="form-control" style="width: 48%; display: inline;">
							<?php
								$cur_year = (int)date("Y");

								for ($i = $cur_year; $i <= $cur_year + 10; $i++) {
							?>
										<option value="<?php echo $i;?>"><?php echo $i;?></option>
							<?php
								}
							?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CVV_CODE', 'CVV Code'); ?></label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="cvv" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_FIRST_NAME', 'First Name'); ?></label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="billing_first" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_LAST_NAME', 'Last Name'); ?></label>
					<div class="col-md-4">
						<input type="text" class="form-control" name="billing_last" />
					</div>
				</div>

				<?php
					//Check that there is account billing information to use.
					$user = AxsExtra::getUserProfileData($user_id);
					$have_billing = true;

					if (!$user->country || !$user->state || !$user->city || !$user->address || !$user->zip) {
						$have_billing = false;
					}

					if ($have_billing) {
				?>
						<div class="form-group">
							<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_BILLING_ADDRESS_SAME', 'Billing address is same as account address'); ?></label>
							<div class="col-md-4">
								<input type="checkbox" name="same_billing" autocomplete="off"/>
							</div>
						</div>
				<?php
					}
				?>

				<div id="billing-address">
					<div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_ADDRESS', 'Address'); ?></label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="billing_address" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_CITY', 'City'); ?></label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="billing_city" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_STATE', 'State'); ?></label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="billing_state" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_COUNTRY', 'Country'); ?></label>
						<div class="col-md-4">
							<select class="form-control" name="billing_country">
								<?php
									$countries = AxsExtra::getAvailableCountries();
									foreach($countries as $c) {
								?>
										<option value="<?php echo $c; ?>" <?php if ($c == "United States") { echo "selected"; } ?>>
											<?php echo $c; ?>
										</option>
								<?php
									}
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_ZIP', 'Zip'); ?></label>
						<div class="col-md-4">
							<input type="text" class="form-control" name="billing_zip" />
						</div>
					</div>
				</div>
				<?php } //End stripe if statement ?>
			</div>

		<?php } ?>
		<div class="form-group">
			<div class="col-md-4"></div>
			<div>
		<?php
			AxsBrands::renderAgreementCheckbox($submit_button_id = 'subscribe');
			$plan_password = ConvergeControllerSubscriptions::$plan_password;
		?>
		</div>
		</div>

		<input type="hidden" name="option" value="com_converge" />
		<input type="hidden" name="view" value="subscription" />
		<input type="hidden" name="task" value="subscribe" />
		<input id="form_server_settings" type="hidden" name="server_settings" />
		<input type="hidden" name="plan_id" value="<?php echo AxsEncryption::encrypt("$plan_id:valid", $plan_password); ?>" />

		<div class="col-md-offset-4" style="margin-bottom: 15px;">
			<?php

			$isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);

			if (!$user_id && $params->captcha && !$isIOS) {
				$captcha_plugin = JFactory::getConfig()->get('captcha');
				if ($captcha_plugin != '0') {
				 	$captcha = JCaptcha::getInstance($captcha_plugin);
				 	$field_id = 'recapt_1';
				 	echo $captcha->display($field_id, $field_id, 'g-recaptcha');
				}
			}
			?>
		</div>

			<?php
				if ($params->button_text) {
					$text = $params->button_text;
				} else {
					$text = AxsLanguage::text('COM_SUBSCRIPTION_REGISTER', 'Process Subscription');
				}

                $requireLegalAgreements = (
                    ($brand->legal->require_terms_of_use && $brand->legal->terms_of_use) || ($brand->legal->require_member_agreement && $brand->legal->member_agreement)
                );
			?>
		<input type="submit" id="subscribe" class="col-md-offset-4 btn btn-primary" value="<?php echo $text; ?>"
		<?php if($requireLegalAgreements) { echo "disabled"; } ?>/>
		</br></br>
	</div>

	<div id="saasSteps"></div>
</form>

<script>
	(function($) {
		$(function() {
			$('input[name=same_billing]').change(function() {
				$('#billing-address').slideToggle();
			});

			$('#applyPromo').click(function() {
				var button = $(this);
				button.html('<img src="../images/ajax-loader.gif" /> Verifying');
				$.ajax({
					url: '/index.php?option=com_converge&task=subscriptions.verifyPromo&format=raw',
					data: {
						promo: $('input[name=promo]').val(),
						plan_id: button.data('planid'),
						initial_amount: $('input[name=initial_amount]').val(),
						recurring_amount: $('input[name=recurring_amount]').val()
					},
					type: 'post'
				}).done(function(result) {
					try {
						result = JSON.parse(result);
						console.log(result)
						$('input[name=initial_amount]').val(result.initial_amount);
						$('input[name=recurring_amount]').val(result.recurring_amount);
						if(result.recurring_amount == 0 && result.initial_amount == 0) {
							$('.payment_information').hide();
						} else {
							$('.payment_information').show();
						}
						$('#promoHelpText').html(result.message);
						$('#promoGroup').removeClass().addClass('form-group has-' + result.result);
					} catch(e) {
						$('#promoHelpText').html("<?php echo AxsLanguage::text("AXS_PROMO_CODE_IS_NOT_VALID", "Promo Code is not valid.") ?>");
						$('#promoGroup').removeClass().addClass('form-group has-error');
					}
					button.html('Apply');
				}).fail(function() {
					$('#promoHelpText').html("<?php echo AxsLanguage::text("AXS_PROMO_CODE_IS_NOT_VALID", "Promo Code is not valid.") ?>");
					$('#promoGroup').removeClass().addClass('form-group has-error');
					button.html('Apply');
				});
			});


			function processSubscription() {
				var defaultText = "<?php echo AxsLanguage::text("AXS_PROCESS_SUBSCRIPTION", "Process Subscription") ?>";
				var subscribe_button = this;
				subscribe_button.disabled = true;
				$("#subscribe").val("<?php echo AxsLanguage::text("AXS_PROCESSING", "Processing") ?>...");

				//if (createServerSettings !== undefined) {
				if (typeof(createServerSettings) !== "undefined") {
					createServerSettings();
					$("#form_server_settings").val(btoa(JSON.stringify(server_settings)));
				}

				var form = $("#registration").serialize();

				// Non unique url variables need to have the suffix []
				// in order to be parsed correctly as arrays by PHP $_REQUEST
				// https://www.php.net/manual/en/faq.html.php#faq.html.arrays
				var seen = {};
				var notUnique = {};
				form.replace('?', '').split('&').forEach(urlVar => {
					var key = urlVar.split('=')[0]
					if (seen[key] == true && !key.includes('%5B%5D')) {
						notUnique[key] = true;
					}
					seen[key] = true;
				});
				for (nonUniqueUrlKey in notUnique) {
					form = form.replaceAll(nonUniqueUrlKey, nonUniqueUrlKey + '[]');
				}
				//console.log(createServerSettings);

				$.ajax({
			        type: 'POST',
			        url: '/index.php?option=com_converge&task=subscriptions.subscribe&format=raw',
			        data: form,
			        success: function(response) {
			        	console.log(response);
			        	try {
			        		var response = JSON.parse(response);
			        		console.log(response);
			        		if (response.success !== "undefined") {
				        		if (response.success === false) {
				        			alert(response.message.replaceAll('<br><hr>','\n'));
				        			subscribe_button.disabled = false;
				        			$("#subscribe").val(defaultText);

				        			var recap = document.getElementById("recapt_1");
				        			var iframe = recap.getElementsByTagName("iframe")[0];
				        			iframe.src = iframe.src;

				        		} else {
				        			if (typeof(buildServer) === "undefined") {
				        				//This is for a normal subscription that redirects to a receipt.
				        				window.location = response.url;
				        			} else {
				        				//This is for an AWS site builder subscription and the server should be built next.
				        				buildServer(response.sub_id);
				        				$(window).scrollTop(100);
				        			}
				        		}
				        	}
			        	} catch (e) {
			        		console.log(response);
			        		subscribe_button.disabled = false;
			        		$("#subscribe").val(defaultText);
			        	}
			        }
			    });
			}

			function stripeTokenHandler(token) {
	            // Insert the token ID into the form so it gets submitted to the server
	            var form = document.getElementById('registration');
	            var hiddenInput = document.createElement('input');
	            hiddenInput.setAttribute('type', 'hidden');
	            hiddenInput.setAttribute('name', 'stripeToken');
	            hiddenInput.setAttribute('value', token.id);
	            form.appendChild(hiddenInput);
	            var hiddenInputLastFour = document.createElement('input');
	            hiddenInputLastFour.setAttribute('type', 'hidden');
	            hiddenInputLastFour.setAttribute('name', 'last4');
	            hiddenInputLastFour.setAttribute('value', token.card.last4);
	            form.appendChild(hiddenInputLastFour);
	            processSubscription();
	        }

			$(document).on('click', '#subscribe', function(e) {
				e.preventDefault();
	            var gateway = $('#registration').data('gtw');
	            var initial_amount = $('input[name=initial_amount]').val();
				var recurring_amount = $('input[name=recurring_amount]').val();
				var card_on_file = $('#card_on_file').val();
	            if(gateway == 'stripe' && (initial_amount > 0 || recurring_amount > 0) && (!card_on_file || card_on_file == 0)) {
	                stripe.createToken(card).then(function(result) {
	                    if (result.error) {
	                      // Inform the user if there was an error.
	                      var errorElement = document.getElementById('card-errors');
	                      errorElement.textContent = result.error.message;
	                    } else {
	                        console.log(result);
	                      // Send the token to your server.
	                       stripeTokenHandler(result.token);
	                    }
	                });
	            } else {
	               processSubscription();
	            }
	        });

			$('input[name="password2"').keyup(function() {
				var pass2 = $(this).val();
				var pass1 = $('input[name="password"').val();
				if(pass2 != pass1) {
					var passswordMismatchMessage = "<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH_MESSAGE", "Passwords do not match.") ?>";
					$('.passswordMismatchMessage').text(passswordMismatchMessage);
					$('.passswordMismatchMessage').fadeIn(300);
				} else {
					$('.passswordMismatchMessage').fadeOut(300);
					$('.passswordMismatchMessage').text('');
				}
			})
		});
	})(jQuery);
</script>