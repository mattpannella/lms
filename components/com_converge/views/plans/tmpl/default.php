<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

// No direct access
defined('_JEXEC') or die;

$app = JFactory::getApplication();
$user = JFactory::getUser();
$app->getDocument()->addStyleSheet('components/com_converge/assets/css/plans.css?v=4');
$app->getDocument()->addScript('components/com_converge/assets/js/plans.js');

//first grab the brand
$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$moduleCount = 0;
//now check for a get param of cat=id

$input = $app->input;
$cat = $input->get('cat', 0, 'INT');
$default_view = TRUE;

//Get the category IDs.
$cat_id = JRequest::getVar('cat_id');
if ($cat_id) {
	$cat_id = implode(",", $cat_id);
} elseif($module_settings->subscriptions_category) {
	$moduleCount++;
	$cat_id = implode(",", $module_settings->subscriptions_category);
}

$db = JFactory::getDbo();

//now pull the sub plans
if ($cat_id) {

	$query = $db->getQuery(true);

	$conditions = array(
		$db->quoteName('plans.published') . '= 1',
		$db->quoteName('plans.private') . '= 0',
		$db->quoteName('cat.published') . '= 1',
		//$db->quoteName('cat.brand_id') . '=' . (int)$brand->id,
		//$db->quoteName('cat.id') . '=' . (int)$cat
		"FIND_IN_SET(" . $db->quoteName('cat.id') . ", " . $db->quote($cat_id) . ")"
	);

	$query
		->select('plans.*, cat.title AS cat_title, cat.description AS cat_description, cat.params AS cat_params')
		->from('axs_pay_subscription_plans AS plans')
		->leftJoin('axs_pay_subscription_categories AS cat ON plans.cat_id = cat.id')
		->where($conditions)
		->order('plans.ordering ASC');

	$db->setQuery($query);
	$subPlans = $db->loadObjectList();
} else {

	$query = $db->getQuery(true);

	$conditions = array(
		$db->quoteName('plans.published') . '= 1',
		$db->quoteName('plans.private') . '= 0',
		$db->quoteName('cat.published') . '= 1',
		$db->quoteName('cat.brand_id') . '=' . (int)$brand->id
	);

	$query
		->select('plans.*, cat.title AS cat_title, cat.description AS cat_description, cat.params AS cat_params')
		->from('axs_pay_subscription_plans AS plans')
		->leftJoin('axs_pay_subscription_categories AS cat ON plans.cat_id = cat.id')
		->where($conditions)
		->order('plans.ordering ASC');
	$db->setQuery($query);
	$subPlans = $db->loadObjectList();
}

// Get the subscription categories in order
$query = $db->getQuery(true);
$db->setQuery(trim("
	SELECT *
	FROM axs_pay_subscription_categories
	ORDER BY ordering ASC
"));
$cats = $db->loadAssocList('id');

// Add the subscription plans to their respective categories
foreach ($subPlans as $subplan) {
	if (array_key_exists($subplan->cat_id, $cats)) {
		if (!is_array($cats[$subplan->cat_id]['plans'])) {
			$cats[$subplan->cat_id]['plans'] = array();
		}
		array_push($cats[$subplan->cat_id]['plans'], $subplan);
	}
}

// Now see if the current user is subscribed to any plans
$subs = AxsPayment::getUserSubscriptions($user->id);

if(!function_exists('getPeriodLengthName') ) {
	function getPeriodLengthName($subTermLength) {

		if (!$subTermLength) {
			return AxsLanguage::text("AXS_LIFETIME", "Lifetime");
	    }

	    $length = substr($subTermLength, 0, -1);
	    $term = substr($subTermLength, -1);

	    $postfix = $length > 1 ? 's' : '';

	    if ($length < 10) {
			switch($length) {
				case 1:
					$length = '';
					break;
				case 2:
					$length = AxsLanguage::text("AXS_TWO", "Two");
					break;
				case 3:
					$length = AxsLanguage::text("AXS_THREE", "Three");
					break;
				case 4:
					$length = AxsLanguage::text("AXS_FOUR", "Four");
					break;
				case 5:
					$length = AxsLanguage::text("AXS_FIVE", "Five");
					break;
				case 6:
					$length = AxsLanguage::text("AXS_SIX", "Six");
					break;
				case 7:
					$length = AxsLanguage::text("AXS_SEVEN", "Seven");
					break;
				case 8:
					$length = AxsLanguage::text("AXS_EIGHT", "Eight");
					break;
				case 9:
					$length = AxsLanguage::text("AXS_NINE", "Nine");
					break;
			}
	    }

	    switch($term) {
			case 'M':
				$term = AxsLanguage::text("AXS_MONTH", "Month");
				break;
			case 'D':
				$term = AxsLanguage::text("AXS_DAY", "Day");
				break;
			case 'Y':
				$term = AxsLanguage::text("JYEAR", "Year");
	    }

	    return AxsLanguage::text("AXS_PER", "per") .' ' .$length .  ' ' . $term . $postfix;

	}
}

foreach ($cats as $id => $sps) {

	$subscribed = false;
	$sub_id = 0;
	$plan_id = 0;

	foreach ($sps['plans'] as $plan) {
		foreach ($subs as $sub) {
			if ($plan->id == $sub->plan_id && $sub->status == "ACT") {
				$sub_id = $sub->id;
				$plan_id = $plan->id;
				$subscribed = true;
				break;
			}
		}
	}

	$plansCount = count($sps['plans']);
	switch ($plansCount) {
		case 1:
			$cols = "col-sm-12";
			break;

		case 2:
			$cols = "col-md-6 col-sm-12";
			break;

		case 3:
			$cols = "col-lg-4 col-md-6 col-sm-12";
			break;

		case 4:
			$cols = "col-md-3 col-sm-12";
			break;

		default:
			$cols = "col-lg-4 col-md-6 col-sm-12";
			break;
	}

	$topCatParams = json_decode($sps['plans'][0]->cat_params);

	if($topCatParams->background_color) {
		$categoryStyle = "background-color: $topCatParams->background_color";
	}

	?>
	<?php if($brand->site_details->tovuti_root_styles &&
			$brand->site_details->root_primary_color &&
			$brand->site_details->root_accent_color  &&
			$brand->site_details->root_base_color) { ?>
		<style>
			.cat-title-card {
				color: #fff;
			}
		</style>
	<?php } ?>
			
		<div class="<?php echo ($topCatParams->show_title) ? 'cat-row--pull-top' : '' ;?> row" style="<?php echo $categoryStyle; ?>">
			<?php if($topCatParams->show_title) : ?>
				<div class="cat-title-card">
					<div class="cat-title"><?php echo $sps['plans'][0]->cat_title; ?></div>
					<div class="cat-description"><?php echo $sps['plans'][0]->cat_description; ?></div>
				</div>
			<?php endif; ?>

			<?php
				foreach ($sps['plans'] as $plan) {
					$planParams = json_decode($plan->params);
					$cat_params = json_decode($plan->cat_params);

					$titleStyle 	= '';
					$pricingStyle 	= '';
					$imageStyle 	= '';
					$iconStyle 		= ' text-align: center;';

					if($planParams->plan_title_background_color) {
						$titleStyle .= " background-color: $planParams->plan_title_background_color ;";
					}

					if($planParams->plan_title_text_color) {
						$titleStyle .= " color: $planParams->plan_title_text_color ;";
					}

					if($planParams->plan_pricing_background_color) {
						$pricingStyle .= " background-color: $planParams->plan_pricing_background_color ;";
					}

					if($planParams->plan_pricing_text_color) {
						$pricingStyle .= " color: $planParams->plan_pricing_text_color ;";
					}

					if($planParams->plan_image_border_color && $planParams->plan_image_border_size) {
					 	$imageStyle .= " border: $planParams->plan_image_border_color {$planParams->plan_image_border_size}px solid; ";
					 	$iconStyle  .= " border: $planParams->plan_image_border_color {$planParams->plan_image_border_size}px solid; ";
					}

					if($planParams->plan_icon_background_color) {
					 	$iconStyle .= " background-color: $planParams->plan_icon_background_color; ";
					}

					if($planParams->plan_icon_color) {
					 	$iconStyle .= " color: $planParams->plan_icon_color; ";
					}


			?>

					<div class="<?php echo $cols; ?>">
						<div class="plan">
							<div class="plan-header" style="<?php echo $titleStyle; ?>">
								<?php
									if($planParams->plan_image_type == 'image' && $planParams->plan_image) {
										switch ($planParams->plan_image_style) {
										 	case 'rounded':
										 		$imageStyle .= " border-radius: 10px; ";
										 	break;

										 	case 'circle':
										 		$imageStyle .= " border-radius: 50%; ";
										 	break;

										 }
								?>
										<div class="plan-image-wrap">
											<img src="<?php echo $planParams->plan_image; ?>" class="plan-image" style="<?php echo $imageStyle; ?>" />
										</div>
								<?php
									}
								?>

								<?php
									if($planParams->plan_image_type == 'icon' && $planParams->plan_icon) {
										switch ($planParams->plan_image_style) {
										 	case 'rounded':
										 		$iconStyle .= " border-radius: 10px; ";
										 	break;

										 	case 'circle':
										 		$iconStyle .= " border-radius: 50%; ";
										 	break;
										 }
								?>
										<div class="plan-icon-wrap" style="<?php echo $iconStyle; ?>">
											<span class="<?php echo $planParams->plan_icon; ?> plan-icon"></span>
										</div>
										<script>
											function iconResize() {
												var iconWidth = $('.plan-icon-wrap').width();
												var fontSize  = Math.round((60/100) * iconWidth);
												$('.plan-icon-wrap').height(iconWidth);
												$('.plan-icon').css({"font-size":fontSize+"px","line-height":iconWidth+"px"});
											}

											iconResize();

											$(window).resize(function(){
												iconResize();
											})
										</script>
								<?php
									}
								?>

								<div class="plan-title"><?php echo $plan->title; ?></div>
							</div>
							<div class="plan-price" style="<?php echo $pricingStyle; ?>">
								<?php
									if ($planParams->tovuti_builder) {
										echo $planParams->tovuti_builder_price_text;
									} else {
										if ($planParams->amount_pricing_text && $planParams->use_pricing_text) {
											echo $planParams->amount_pricing_text;
										} else {
											echo $currency_symbol.$plan->default_initial_amount;
										}
								?>
										<div class="plan-price-subtext">
											<?php
												if ($plan->pricing_text != '') {
											?>
													<div><?php echo $plan->pricing_text; ?></div>
											<?php
												} else {
											?>
													<div>
														<?php
															if (($plan->default_amount > 0) && ($plan->default_amount != $plan->default_initial_amount)) {
																echo $currency_symbol.$plan->default_amount.' ';

															}

															echo getPeriodLengthName($plan->default_sub_length);
														?>
													</div>
											<?php
												}
											?>
										</div>
								<?php
									}
								?>
							</div>
							<div class="plan-description">
								<?php
									if($plan->description_type == 'list') {
										$list = json_decode($plan->description_list);
										$items = count($list->checked);
								?>
										<ul class="plan-container">
											<?php
												for ($i=0; $i<$items; $i++) {
													$checked = $list->checked[$i];
													$text = $list->text[$i];
													$sidetext = $list->sidetext[$i];
													$subtext = $list->subtext[$i];
											?>
													<li>
														<span class="lizicon <?php echo $checked ? 'lizicon-checkbox-checked' : 'lizicon-checkbox-unchecked'; ?>"></span>
														<span style="font-size: 12pt; color: #000; margin-left: 5px;">
															<strong><?php echo $text; ?> <span style="font-size: 10pt;"><em><?php echo $sidetext; ?></em></span></strong>
														</span>

														<?php if($subtext) { ?>
															<br>
															<span style="font-size: 8pt;"><?php echo $subtext; ?></span>
														<?php } ?>

													</li>
											<?php
												}
											?>
										</ul>
								<?php
									} else {
										echo $plan->description_html;
									}
								?>
							</div>
							<div class="plan-subscribe">
								<?php

									if ($cat_id) {

										if (!isset($_SESSION['sub_plan_list'])) {
											$_SESSION['sub_plan_list'] = array();
										}

										$_SESSION['sub_plan_list'][$plan->id] = true;
									}

									if ($subscribed && !$cat_params->multi_purchase) {

										if ($plan->id == $plan_id) {

											$url = "index.php?option=com_converge&view=subscriptions&task=cancelSubscription&sub_id=$sub_id";

											$href = JRoute::_($url);
											$button_style = "btn-danger";

											if ($planParams->custom_cancel_text) {
												$text = $planParams->custom_cancel_text;
											} else {
												$text = AxsLanguage::text("AXS_CANCEL_SUBSCRIPTION", "Cancel Subscription");
											}

										} else {

											$href = JRoute::_('index.php?option=com_converge&view=subscription&plan_id=' . $plan->id);

											$button_style = "btn-primary";
											$text = AxsLanguage::text("AXS_CHANGE_TO_THIS_SUBSCRIPTION", "Change to this Subscription");
										}
									} else {

										if ($planParams->button_link) {
											$href = $planParams->button_link;
										} else {
											$href = JRoute::_('index.php?option=com_converge&view=subscription&plan_id='.$plan->id);
										}

										if ($planParams->button_text) {
											$text = $planParams->button_text;
										} else {
											$text = AxsLanguage::text("AXS_SUBSCRIBE", "Subscribe");
										}

										$button_style = "btn-primary";
									}
								?>
								<a href="<?php echo $href; ?>" class="plan-subscribe-button btn <?php echo $button_style;?>">
									<?php echo $text; ?>
								</a>
							</div>
						</div>
					</div>
			<?php
				}
			?>
		</div>
<?php } ?>