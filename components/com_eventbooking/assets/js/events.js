var thumbnail_type = null;
var thumb_image = null;
var default_list = null;
var thumb_preview = null;

jQuery(document).ready(
	function() {
		//Make the following fields required
		var location_list = document.getElementById("location_id");
		var start_date = document.getElementById("event_date");

		//location_list.setAttribute("required", "true");
		start_date.setAttribute("required", "true");


		//Remove any empty location names
		for (var i = 0; i < location_list.length; i++) {
			if (location_list[i].innerHTML == "") {
				location_list.remove(i);
			}
		}

		var selectLists = document.getElementsByTagName("select");
		for (var i = 0; i < selectLists.length; i++) {
			let name = selectLists[i].getAttribute("name");
			$(selectLists[i]).css({
				"font-size":"14px"
			});
			switch (name) {
				case "event_date_hour":
				case "event_end_date_hour":
				case "registration_start_hour":

					$(selectLists[i]).css({
						"min-width":"80px"
					});

					for (var j = 0; j < selectLists[i].length; j++) {
						var opt = selectLists[i][j];
						if (opt.value <= 11 && opt.value > 0) {
							opt.innerHTML = opt.value + " AM";
						} else if (opt.value > 12) {
							opt.innerHTML = (opt.value - 12) + " PM";
						} else if (opt.value == 12) {
							opt.innerHTML = "12 PM";
						} else if (opt.value == 0) {
							opt.innerHTML = "12 AM";
						}
					}
					break;
			}
		}

		thumbnail_type = document.getElementById("thumbnail_image_type");
		thumb_image = document.getElementById("thumb_image");
		default_list = document.getElementById("default_image_list");
		thumb_preview = document.getElementById("thumb_preview");

		jQuery(thumbnail_type).change(
			function() {
				switch (this.value) {
					case "upload":

						let current_image = thumb_preview.getAttribute("src");
						console.log(current_image);
						console.log(current_image == "/");
						if (current_image == "/") {
							thumb_image.setAttribute("required", "true");
						}
						break;
					case "default":
						thumb_image.removeAttribute("required");
						break;
				}		
			}
		);

		$(".icon-calendar").each(
			function() {
				this.className = "lizicon-calendar";
			}
		);

		$("#thumb_image").change(
			function() {
				if (this.files && this.files[0]) {
					var reader = new FileReader();
					
					reader.onload = function(e) {
						$("#thumb_preview").attr('src', e.target.result);
					}

					reader.readAsDataURL(this.files[0]);
				}
			}
		);

		$("#thumbnail_image_type").change(
			function() {
				//console.log($("#thumbnail_image_type").val());
				var type = $("#thumbnail_image_type").val();

				$(".image_select").hide(250);
				$("#" + type + "_image_select").show(250);
			}
		);

		$(".default_image_preview").click(
			function() {
				$(".default_image_selected").each(
					function() {
						$(this).removeClass("default_image_selected");
					}
				);

				$(this.parentNode).addClass("default_image_selected");
				$("#default_image").val(this.getAttribute('value'));
				$("#thumb_preview").attr('src', this.getAttribute('value'));
			}
		);

		var priceFix = null;

		$("#individual_price").on('change paste keyup',
			function() {

				var _this = this;
				var original = parseFloat(this.value);

				if (priceFix) {
					clearTimeout(priceFix);
				}

				priceFix = setTimeout(
					function() {

						var newValue = original.toFixed(2);
						if (newValue === "NaN") {
							newValue = 0;
						}

						_this.value = newValue;
					}, 1000
				);

			}
		);
	}
);
