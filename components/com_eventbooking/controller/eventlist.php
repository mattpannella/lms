<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class EventBookingControllerEventlist extends EventbookingController {
	

	public function onWishlistAdd() {
		$course_id = $this->input->get('course_id', 0);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$columns = array (
			'user_id',
			'course_id',
			'date'
		);
		$values = array (
			(int)JFactory::getUser()->id,
			(int)$course_id,
			$db->q(date("Y-m-d H:i:s"))
		);
		$query->insert($db->qn('#__splms_course_wishlist'))
			->columns($db->qn($columns))
			->values(implode(',', $values));
		$db->setQuery(str_replace('INSERT', 'INSERT IGNORE', $query));
		$db->execute();

		echo 'success';
	}

	public function onWishlistRemove() {
		$course_id = $this->input->get('course_id', 0);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array (
			$db->qn('user_id').'='.(int)JFactory::getUser()->id,
			$db->qn('course_id').'='.(int)$course_id
		);
		$query->delete($db->qn('#__splms_course_wishlist'))
			->where($conditions);
		$db->setQuery($query);
		$db->execute();

		echo 'success';
	}
}