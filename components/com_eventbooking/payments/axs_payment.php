<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/18/16
 * Time: 12:34 PM
 */

defined('_JEXEC') or die;

class axs_payment extends RADPayment {
	/**
	 * Constructor
	 *
	 * @param \Joomla\Registry\Registry $params
	 * @param array                     $config
	 */
	public function __construct($params, $config = array('type' => 1)) {
		parent::__construct($params, $config);
	}

	public function processPayment($row, $data, $newUser = false) {
		$cardParams = AxsPayment::getCardParams();
		$brand = AxsBrands::getBrand();
		if ($data['card_on_file']) {			
			$cardParams->id = $data['card_on_file'];
			$card = AxsPayment::newCard($cardParams);
			$card->getCardWithId();
			$saveCard = false;
		} else {
			if($brand->billing->gateway_type == 'stripe') {
				$cardParams->user_id = $row->user_id;
				$cardParams->stripeToken = $data['stripeToken'];
				$cardParams->cardNumber = $data['last4'];
				$user = JFactory::getUser($cardParams->user_id);
				$cardParams->email = $user->email;
				$cardParams->description = $user->name;
			} else { 
				$cardParams->email = $data['email'];

				// Payment information section of the form
			    $cardParams->cardNumber = $data['x_card_num'];
			    $cardParams->expire = str_pad($data['exp_month'], 2, '0', STR_PAD_LEFT) . substr($data['exp_year'], 2, 2);
			    $cardParams->cvv = $data['x_card_code'];

				// Billing address section of the form
				$cardParams->firstName = $data['billing_first'];
			    $cardParams->lastName = $data['billing_last'];
			    $cardParams->address = $data['billing_address'];
	        	$cardParams->city = $data['billing_city'];
	        	$cardParams->state = $data['billing_state'];
				$cardParams->zip = $data['billing_zip'];
	        	$cardParams->country = $data['billing_country'];
	        	$cardParams->phone = $data['billing_pay_phone'];
				
			    $cardParams->user_id = $row->user_id;		    
		    }

			$card = AxsPayment::newCard($cardParams);
			$saveCard = true;
		}

		$trxnParams = AxsPayment::getTransactionParams();

		$trxnParams->user_id = $row->user_id;
        $trxnParams->type = AxsPayment::$trxn_types['event'];
        $trxnParams->type_id = $row->event_id;
        $trxnParams->amount = $row->amount;
        $trxnParams->date = date("Y-m-d H:i:s");
        $trxnParams->card = $card;
        $trxnParams->initiator = AxsPayment::$initiators['user'];
        $trxnParams->description = $_SERVER['SERVER_NAME'] . " " . $row->registration_code;
        $trxnParams->saveCard = $saveCard;

		$trxn = AxsPayment::newTransaction($trxnParams);
		$trxn->charge();
		if($trxn->status == 'SUC') {

			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onAfterCompleteEventPurchase', array($row));

			$input  = JFactory::getApplication()->input;
			$task   = $input->getCmd('task');
			$Itemid = $input->getInt('Itemid', EventbookingHelper::getItemid());

			if ($task == 'process') {
				$paymentSuccessUrl = JRoute::_('index.php?option=com_eventbooking&view=payment&layout=complete&Itemid=' . $Itemid, false, false);
			} else {
				if (JPluginHelper::isEnabled('system', 'cache')) {
					$paymentSuccessUrl = JRoute::_('index.php?option=com_eventbooking&view=complete&Itemid=' . $Itemid . '&pt=' . time(), false, false);
				} else {
					$paymentSuccessUrl = JRoute::_('index.php?option=com_eventbooking&view=complete&Itemid=' . $Itemid, false, false);
				}
			}

			$this->onPaymentSuccess($row, $trxn->trxn_id, $trxn->id);
			header("location: " . $paymentSuccessUrl);			
			//JFactory::getApplication()->redirect($paymentSuccessUrl);
		} else {
			//Payment failure, display error message to users
			$input = JFactory::getApplication()->input;
			$id = $input->getInt('id', 0);
			$task = $input->getCmd('task');
			$Itemid = $input->getInt('Itemid', EventbookingHelper::getItemid());

			if ($task == 'process') {
				$paymentFailureUrl = JRoute::_('index.php?option=com_eventbooking&view=failure&Itemid=' . $Itemid, false, false);
			} else {
				$paymentFailureUrl = JRoute::_('index.php?option=com_eventbooking&view=failure&id=' . $id . '&Itemid=' . $Itemid, false, false);
			}

			$session = JFactory::getSession();
			$session->set('omnipay_payment_error_reason', $trxn->message);
			$user = JFactory::getUser($row->user_id);
			if($newUser) {
				$user->delete();
			}			
			$row->delete();
			 
			//JFactory::getApplication()->redirect($paymentFailureUrl);
			header("location: " . $paymentFailureUrl);
		}
	}
}