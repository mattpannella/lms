<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class EventbookingHelperEventlist {
	public $search;
	public $catSlug;
	public $catData;
	public $catId;
	public $viewType;
	public $categoryAndSubcategories;

	static $loaded = array();

	public function __construct() {
		$app = JFactory::getApplication();
		$this->search = $app->input->get('search', '', 'STRING');
		$this->catSlug = $app->input->get('cat', '', 'STRING');
		$this->catId = $app->input->get('id', '', 'INT');
		$jinput = JFactory::getApplication()->input;
		$type = $jinput->get('type', 0);
		$this->categoryAndSubcategories = array();
		if($type && $type == 'myevents') {
			$this->viewType = 1;
		} else {
			$this->viewType = 0;
		}
	}

	public function getItemList($search = null, $upcoming = true) {

		if (!$search) {
			$search = $this->search;
		}

		if ($search) {
			$events = $this->getEventsBySearch($search, $upcoming);
		} else if (!$this->catId && !$this->catSlug) {
			$events = $this->getAllEvents($upcoming);
		} else if ($this->catId) {
			$this->catData = $this->getCategoryById($this->catId);
			$events = $this->getEventsByCatId($this->catId, $upcoming);
		} else if ($this->catSlug) {
			$this->catData = $this->getCategoryBySlug($this->catSlug);
			if (is_object($this->catData)) {
				$events = $this->getEventsByCatId($this->catData->id, $upcoming);
			} else {
				$events = $this->getAllEvents($upcoming);
			}
		}

		return $events;
	}

	public function getDateSpan($eventId) {
		if (isset(static::$loaded[__METHOD__][$eventId])) {
			return static::$loaded[__METHOD__][$eventId];
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions[] = $db->quoteName('id').'='.$db->quote($eventId).' OR '.$db->quoteName('parent_id').'='.$db->quote($eventId);
		$query->select('MIN(event_date) as start, MAX(event_end_date) as end')
			->from($db->quoteName('#__eb_events'))
			->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObject();
		$output = '';
		if($result->start) {
			$start = date("D M d, Y", strtotime($result->start));
			$output .= $start;
		}

		if($result->end != '0000-00-00 00:00:00') {
			$end = date("D M d, Y", strtotime($result->end));
			if($end != $start) {
				$output .=  " - ".$end;
			}
		}

		static::$loaded[__METHOD__][$eventId] = $output;

		return $output;
	}



	public function getAllEvents($upcoming = true) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$user_id = JFactory::getUser()->id;
		$levels = JAccess::getAuthorisedViewLevels($user_id);
		$levelsList = implode(',',$levels);
		$conditions = array (
			$db->qn('e.published').' = '.(int)1,
			
		);

		$current_language_tag = explode('-', AxsLanguage::getCurrentLanguage()->get('tag'))[0];
		if ($current_language_tag != 'en') {
			$conditions []= 'CHAR_LENGTH(e.alias_' . $current_language_tag . ')' . ' > 0';
			$conditions []= 'CHAR_LENGTH(e.title_' . $current_language_tag . ')' . ' > 0';
		}

		$conditions[] = $db->quoteName("e.parent_id")." = 0 ";
		$conditions[] = $db->quoteName("e.access")." IN (".$levelsList.") ";
		$leftJoin="";
		if ($upcoming) {
			$leftJoin='LEFT JOIN (SELECT id, published,parent_id FROM joom_eb_events'
					. ' WHERE event_date >= CURDATE() OR event_end_date >= CURDATE() AND published = 1) AS b'
					. ' ON b.parent_id = e.id';
			$conditions[] = '(' . $db->qn('e.event_date') . " >= CURDATE() OR " . $db->qn('e.event_end_date') . " >= CURDATE() OR b.id IS NOT NULL )";
		}

		/*if ($this->viewType) {
			$courses = implode(",", AxsLMS::getUserAllCourses($user_id, true));
			if($courses) {
				$userCourses = "id IN ($courses)";
				array_push($conditions, $userCourses);
			}
		}*/

		$db->setQuery('SELECT DISTINCT e.id, e.title, e.published, price_text, e.access, e.event_date, e.event_end_date, e.thumb, e.individual_price'
		. ' FROM joom_eb_events AS e ' . $leftJoin
		. ' WHERE ' . implode(" AND ",str_replace("\'","",$conditions))
		. ' ORDER BY e.event_date ASC;'
		);

		return $db->loadObjectList();
	}

	public function getEventsBySearch($search, $upcoming = true) {
		$sig = md5(serialize(array($search, $upcoming)));
		if (isset(static::$loaded[__METHOD__][$sig])) {
			return static::$loaded[__METHOD__][$sig];
		}
		$this->search = json_decode(base64_decode($search));
		/*
		if ($this->search->cat) {
			$this->catData = $this->getCategoryById($this->search->cat);
		}
		*/

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$user_id = JFactory::getUser()->id;
		$levels = JAccess::getAuthorisedViewLevels($user_id);
		$levelsList = implode(',',$levels);
		$conditions = array (
			$db->qn('e.published').'='.(int)1,
		);

		$current_language_tag = explode('-', AxsLanguage::getCurrentLanguage()->get('tag'))[0];
		if ($current_language_tag != 'en') {
			$conditions []= 'CHAR_LENGTH(e.alias_' . $current_language_tag . ')' . ' > 0';
			$conditions []= 'CHAR_LENGTH(e.title_' . $current_language_tag . ')' . ' > 0';
		}

		$conditions[] = $db->quoteName("e.parent_id")." = 0";
		$conditions[] = $db->quoteName("e.access")." IN (".$levelsList.")";

		if ($upcoming) {
			$conditions[] = '(' . $db->qn('e.event_date') . " >= CURDATE() OR " . $db->qn('e.event_end_date') . ">= CURDATE() OR EXISTS ( SELECT b.id FROM `#__eb_events`as b WHERE `b`.`published`=1 AND `b`.`parent_id` = `e`.`id` AND (`b`.`event_date` >= CURDATE() OR `b`.`event_end_date`>= CURDATE() ) ) )";
		}
		/*

		if($this->viewType) {
			$courses = implode(",", AxsLMS::getUserAllCourses($user_id, true);
			if ($courses) {
				$userCourses = "splms_course_id IN ($courses)";
				array_push($conditions, $userCourses);
			} else {
				$userCourses = "splms_course_id = 0";
				array_push($conditions, $userCourses);
			}
		}
		*/

		if ($this->search->title) {
			$search_terms = $this->search->title;

			//if it's in quotes, do an exact match.
			preg_match_all('/\"([\w ]+)\"/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = "(".$db->qn('e.title')." LIKE '%$q[1]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/\"([\w ]+)\"/', '', $search_terms);

			//if it has two words separated by an "and or + symbol" it must have both. as in "troy and bob" or "troy + bob"
			preg_match_all('/(\w+)( +and +| *\+ *)(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('e.title')." LIKE '%$q[1]%' AND ".$db->qn('e.title')." LIKE '%$q[3]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+)( +and +| *\+ *)(\w+)/', '', $search_terms);

			//if it has two words separated by an "_ symbol" it can have either (or). as in "troy _ bob" or "troy_bob"
			preg_match_all('/(\w+) *_ *(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('e.title')." LIKE '%$q[1]%' OR ".$db->qn('e.title')." LIKE '%$q[2]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+) *_ *(\w+)/', '', $search_terms);

			//all remaining words
			$search_terms = str_replace(',', ' ', $search_terms);
			$search_terms = preg_replace('/\s+/',',',trim($search_terms));
			$searchArray = explode(',', $search_terms);
			foreach($searchArray as $term) {
				$conditions[] = $db->qn('e.title')." LIKE '%$term%'";
			}
		}

		if ($this->search->desc) {
			$search_terms = $this->search->desc;

			//if it's in quotes, do an exact match.
			preg_match_all('/\"([\w ]+)\"/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = "(".$db->qn('e.description')." LIKE '%$q[1]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/\"([\w ]+)\"/', '', $search_terms);

			//if it has two words separated by an "and or + symbol" it must have both. as in "troy and bob" or "troy + bob"
			preg_match_all('/(\w+)( +and +| *\+ *)(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('e.description')." LIKE '%$q[1]%' AND ".$db->qn('e.description')." LIKE '%$q[3]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+)( +and +| *\+ *)(\w+)/', '', $search_terms);

			//if it has two words separated by an "_ symbol" it can have either (or). as in "troy _ bob" or "troy_bob"
			preg_match_all('/(\w+) *_ *(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('e.description')." LIKE '%$q[1]%' OR ".$db->qn('e.description')." LIKE '%$q[2]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+) *_ *(\w+)/', '', $search_terms);

			//all remaining words
			$search_terms = str_replace(',', ' ', $search_terms);
			$search_terms = preg_replace('/\s+/',',',trim($search_terms));
			$searchArray = explode(',', $search_terms);
			foreach($searchArray as $term) {
				$conditions[] = $db->qn('e.description')." LIKE '%$term%'";
			}
		}

		if ($this->search->cat) {
			if(!is_array($this->search->cat)) {
				$this->catId = $this->search->cat;
				$this->catData = $this->getCategoryById($this->catId);
				array_push($this->categoryAndSubcategories, $this->search->cat);
				self::getAllCategoryChildren($this->search->cat);

			}
			else{
				foreach ($this->search->cat as $cat) {
					array_push($this->categoryAndSubcategories, $cat);
					self::getAllCategoryChildren($cat);
				}
			}


			$catIDs = implode(',', $this->categoryAndSubcategories);
			$conditions[] = "c.category_id IN ($catIDs)";

		}
		if (isset($this->search->prev) && !$this->search->prev) {
			$conditions[] = $db->qn('e.event_end_date') . ' >= NOW()';
		}

		if (isset($this->search->feat) && $this->search->feat) {
			$conditions[] = $db->qn('e.featured') . '=1';
		}

		$query->select('*,e.id as id')
			->from($db->qn('#__eb_events', 'e'))
			->join('LEFT', $db->quoteName('#__eb_event_categories', 'c') . ' ON (' . $db->quoteName('e.id') . ' = ' . $db->quoteName('c.event_id') . ')')
			->where($conditions)
			->group('e.id')
			->order('e.event_date ASC');

		if (isset($this->search->lim) && $this->search->lim > 0) {
			$query->setLimit($this->search->lim);
		}
		$db->setQuery($query);
		$results = $db->loadObjectList();

		static::$loaded[__METHOD__][$sig] = $results;

		return $results;
	}

	public function getEventsByCatId($id, $upcoming = true) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$user_id = JFactory::getUser()->id;
		$levels = JAccess::getAuthorisedViewLevels($user_id);
		$levelsList = implode(',',$levels);

		$conditions = array (
			$db->qn('e.published').'='.(int)1,
		);

		$current_language_tag = explode('-', AxsLanguage::getCurrentLanguage()->get('tag'))[0];
		if ($current_language_tag != 'en') {
			$conditions []= 'CHAR_LENGTH(e.alias_' . $current_language_tag . ')' . ' > 0';
			$conditions []= 'CHAR_LENGTH(e.title_' . $current_language_tag . ')' . ' > 0';
		}

		$conditions[] = $db->quoteName("e.parent_id")." = 0";
		$conditions[] = $db->quoteName("e.access")." IN (".$levelsList.")";

		if ($upcoming) {
			$conditions[] = '(' . $db->qn('e.event_date') . " >= CURDATE() OR " . $db->qn('e.event_end_date') . ">= CURDATE() OR EXISTS ( SELECT b.id FROM `#__eb_events`as b WHERE `b`.`published`=1 AND `b`.`parent_id` = `e`.`id` AND (`b`.`event_date` >= CURDATE() OR `b`.`event_end_date`>= CURDATE() ) ) )";
		}

		$this->catId = $id;
		array_push($this->categoryAndSubcategories, $id);
		self::getAllCategoryChildren($id);

		$catIDs = implode(',', $this->categoryAndSubcategories);
		$conditions[] = "c.category_id IN ($catIDs)";

		$query->select('*,e.id as id')
			->from($db->qn('#__eb_events', 'e'))
			->join('LEFT', $db->quoteName('#__eb_event_categories', 'c') . ' ON (' . $db->quoteName('e.id') . ' = ' . $db->quoteName('c.event_id') . ')')
			->where($conditions)
			->group('e.id')
			->order('e.event_date ASC');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function getEventById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('published').'='.(int)1,
			$db->qn('id').'='.(int)$id
		);
		$query->select('*')
			->from($db->qn('#__eb_events'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getCategoryBySlug($slug) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('published').'='.(int)1,
			$db->qn('alias') .'='. $db->q($slug)
		);
		$query->select('*')
			->from($db->qn('#__eb_categories'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getCategoryById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('published').'='.(int)1,
			$db->qn('id').'='.(int)$id
		);
		$query->select('*')
			->from($db->qn('#__eb_categories'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getAllCategories() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('published').'='.(int)1
		);
		$query->select('*')
			->from($db->qn('#__eb_categories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function getAllCategoryChildren($id) {

		$result = $this->checkSubCategories($id);
		if($result) {
			foreach ($result as $cat) {
				array_push($this->categoryAndSubcategories,$cat->id);
				$subresult = $this->checkSubCategories($cat->id);
				if($subresult) {
					$this->getAllCategoryChildren($cat->id);
				}
			}
		}
	}

	public function checkSubCategories($id) {
		if (isset(static::$loaded[__METHOD__][$id])) {
			return static::$loaded[__METHOD__][$id];
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('published').'='.(int)1
		);
		$conditions[] = $db->qn('parent').'='.(int)$id;

		$query->select('*')
			->from($db->qn('#__eb_categories'))
			->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObjectList();

		static::$loaded[__METHOD__][$id] = $result;

		return $result;
	}

	public function getCategorySelectList($id) {
		if (isset(static::$loaded[__METHOD__][$id])) {
			return static::$loaded[__METHOD__][$id];
		}
		$db          = JFactory::getDbo();
		$query       = $db->getQuery(true);

		$config      = EventbookingHelper::getConfig();
		$fieldSuffix = EventbookingHelper::getFieldSuffix();
		$user = JFactory::getUser();

		$query->select("id, parent AS parent_id, access, show_on_submit_event_form, name" . $fieldSuffix . " AS title")
			->from('#__eb_categories')
			->where('published = 1')
			->order('name' . $fieldSuffix);

		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$children = array();

		$userAccessLevels = $user->getAuthorisedViewLevels();

		$isAdmin = $user->authorise('core.admin', 'com_eventbooking');

		if (!empty($categories)) {
			$parentId = -1;

			// first pass - collect children
			foreach ($categories as $category) {
				if($isAdmin || in_array($category->access, $userAccessLevels)) {
					$parentId   = $category->parent_id;
					$list = @$children[$parentId] ? $children[$parentId] : array();
					array_push($list, $category);
					$children[$parentId] = $list;
				}
			}
		}

		$list    = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);

		$options = array();
		$options[] = JHtml::_('select.option', '', AxsLanguage::text('COM_EB_AXS_EVENTS_FILTER_SELECT_CATEGORY','Select a Category'), 'value', 'text');

		foreach ($list as $listItem) {
			if ($isAdmin || $listItem->show_on_submit_event_form) {
				$options[] = JHtml::_('select.option', $listItem->id, '&nbsp;&nbsp;&nbsp;' . $listItem->treename, 'value', 'text');
			}
		}

		if ($item->id) {
			$query->clear();
			$query
				->select('category_id')
				->from('#__eb_event_categories')
				->where('event_id=' . $item->id)
				->where('main_category=1');
			$db->setQuery($query);

			$mainCategoryId = $db->loadResult();

			$query->clear();
			$query
				->select('category_id')
				->from('#__eb_event_categories')
				->where('event_id=' . $item->id)
				->where('main_category=0');
			$db->setQuery($query);

			$additionalCategories = $db->loadColumn();
		} else {
			$mainCategoryId       = 0;
			$additionalCategories = array();
		}

		$lists['main_category_id'] = JHtml::_('select.genericlist', $options, 'cat',
			array(
				'option.text.toHtml' => false,
				'option.text'        => 'text',
				'option.value'       => 'value',
				'list.attr'          => 'class="form-select flex-md-fill mx-md-1 ms-md-0 mb-1 mb-md-0"',
				'list.select'        => $id, ));

		static::$loaded[__METHOD__][$id] = $lists['main_category_id'];

		return $lists['main_category_id'];
	}

}
