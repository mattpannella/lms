<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
abstract class EventbookingHelperHtml
{
	public static function getCalendarSetupJs($fields)
	{
		$firstDay   = JFactory::getLanguage()->getFirstDay();
		$config     = EventbookingHelper::getConfig();
		$dateFormat = $config->date_field_format ? $config->date_field_format : '%Y-%m-%d';
		$output     = array();
		foreach ($fields as $field)
		{
			$output[] = 'Calendar.setup({
			// Id of the input field
			inputField: "' . $field . '",
			// Format of the input field
			ifFormat: "' . $dateFormat . '",
			// Trigger for the calendar (button ID)
			button: "' . $field . '_img",
			// Alignment (defaults to "Bl")
			align: "Tl",
			singleClick: true,
			firstDay: ' . $firstDay . '
			});';
		}

		return implode("\n", $output);
	}

	/**
	 * Build category dropdown
	 *
	 * @param int    $selected
	 * @param string $name
	 * @param string $attr Extra attributes need to be passed to the dropdown
	 *
	 * @return string
	 */
	public static function buildCategoryDropdown($selected, $name = "parent", $attr = null)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id, parent AS parent_id, name AS title')
			->from('#__eb_categories')
			->where('published=1');
		$db->setQuery($query);
		$rows     = $db->loadObjectList();
		$children = array();
		if ($rows)
		{
			// first pass - collect children
			foreach ($rows as $v)
			{
				$pt   = $v->parent_id;
				$list = @$children[$pt] ? $children[$pt] : array();
				array_push($list, $v);
				$children[$pt] = $list;
			}
		}
		$list      = JHtml::_('menu.treerecurse', 0, '', array(), $children, 9999, 0, 0);
		$options   = array();
		$options[] = JHtml::_('select.option', '0', JText::_('EB_SELECT_CATEGORY'));
		foreach ($list as $item)
		{
			$options[] = JHtml::_('select.option', $item->id, '&nbsp;&nbsp;&nbsp;' . $item->treename);
		}

		return JHtml::_('select.genericlist', $options, $name,
			array(
				'option.text.toHtml' => false,
				'option.text'        => 'text',
				'option.value'       => 'value',
				'list.attr'          => 'class="inputbox" ' . $attr,
				'list.select'        => $selected,));
	}

	/**
	 * Function to render a common layout which is used in different views
	 *
	 * @param string $layout
	 * @param array  $data
	 *
	 * @return string
	 * @throws Exception
	 */
	public static function loadCommonLayout($layout, $data = array())
	{
		$app       = JFactory::getApplication();
		$themeFile = str_replace('/tmpl', '', $layout);

		// This line was added to keep B/C with template override code, don't remove it
		if (strpos($layout, 'common/') === 0 && strpos($layout, 'common/tmpl') === false)
		{
			$layout = str_replace('common/', 'common/tmpl/', $layout);
		}

		$deviceType = EventbookingHelper::getDeviceType();

		$paths = array($layout);

		if ($deviceType != 'desktop')
		{
			$paths[] = JPATH_THEMES . '/' . $app->getTemplate() . '/html/com_eventbooking/' . str_replace('.php', '.' . $deviceType . '.php', $themeFile);
			$paths[] = JPATH_ROOT . '/components/com_eventbooking/view/' . str_replace('.php', '.' . $deviceType . '.php', $layout);
		}

		$paths[] = JPATH_THEMES . '/' . $app->getTemplate() . '/html/com_eventbooking/' . $themeFile;
		$paths[] = JPATH_ROOT . '/components/com_eventbooking/view/' . $layout;

		$path = '';

		foreach ($paths as $possiblePath)
		{
			if (JFile::exists($possiblePath))
			{
				$path = $possiblePath;
				break;
			}
		}

		if (empty($path))
		{
			throw new RuntimeException(JText::sprintf('The given common layout %s does not exist', $layout));
		}

		// Start an output buffer.
		ob_start();
		extract($data);
		// Load the layout.
		include $path;

		// Get the layout contents.
		$output = ob_get_clean();

		return $output;
	}

	/**
	 * Get label of the field (including tooltip)
	 *
	 * @param        $name
	 * @param        $title
	 * @param string $tooltip
	 *
	 * @return string
	 */
	public static function getFieldLabel($name, $title, $tooltip = '')
	{
		$label = '';
		$text  = $title;

		// Build the class for the label.
		$class = !empty($tooltip) ? 'hasTooltip hasTip' : '';

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="' . $name . '-lbl" for="' . $name . '" class="' . $class . '"';

		// If a description is specified, use it to build a tooltip.
		if (!empty($tooltip))
		{
			$label .= ' title="' . JHtml::tooltipText(trim($text, ':'), $tooltip, 0) . '"';
		}

		$label .= '>' . $text . '</label>';

		return $label;
	}

	/**
	 * Get bootstrapped style boolean input
	 *
	 * @param $name
	 * @param $value
	 *
	 * @return string
	 */
	public static function getBooleanInput($name, $value)
	{
		$html = array();

		// Start the radio field output.
		$html[] = '<fieldset id="' . $name . '" class="btn-group btn-group-yesno border rounded">';

		// Yes Option
		$checked = ($value == 1) ? ' checked="checked"' : '';
		$html[]  = '<input class="btn-check" type="radio" id="' . $name . '0" name="' . $name . '" value="1"' . $checked . ' />';
		$html[]  = '<label class="btn btn-outline-primary" for="' . $name . '0">' . JText::_('JYES') . '</label>';


		// No Option
		$checked = ($value == 0) ? ' checked="checked"' : '';
		$html[]  = '<input class="btn-check" type="radio" id="' . $name . '1" name="' . $name . '" value="0"' . $checked . ' />';
		$html[]  = '<label class="btn btn-outline-secondary" for="' . $name . '1">' . JText::_('JNO') . '</label>';

		// End the radio field output.
		$html[] = '</fieldset>';

		return implode($html);
	}

	/**
	 * Get available fields tags using in the email messages & invoice
	 *
	 * @param bool $defaultTags
	 *
	 * @return array|string
	 */
	public static function getAvailableMessagesTags($defaultTags = true)
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('name')
			->from('#__eb_fields')
			->where('published = 1')
			->order('ordering');
		$db->setQuery($query);

		if ($defaultTags)
		{
			$fields = array('registration_detail', 'date', 'event_title', 'event_date', 'event_end_date', 'short_description', 'description', 'total_amount', 'tax_amount', 'discount_amount', 'late_fee', 'payment_processing_fee', 'amount', 'location', 'number_registrants', 'invoice_number', 'transaction_id', 'id', 'payment_method','virtual_meeting_link');
		}
		else
		{
			$fields = array();
		}

		$fields = array_merge($fields, $db->loadColumn());

		$fields = array_map('strtoupper', $fields);
		$fields = '[' . implode('], [', $fields) . ']';

		return $fields;
	}

	/**
	 * Get URL to add the given event to Google Calendar
	 *
	 * @param $row
	 *
	 * @return string
	 */
	public static function getAddToGoogleCalendarUrl($row)
	{
		$params = json_decode($row->params);
		if($params->time_zone) {
			$timezone = $params->time_zone;
		} else {
			$timezone = null;
		}
		$eventData = self::getEventDataArray($row,$timezone);

		$queryString['title']       = "text=" . $eventData['title'];
		$queryString['dates']       = "dates=" . $eventData['dates'];
		if($params->time_zone) {
			$queryString['timezone']    = "ctz=".$params->time_zone;
		}

		if($params->virtual_meeting && $params->calendar_location == 'virtual') {
			$user = JFactory::getUser();
			$encryptedParamsLink = '';
			if($params->auto_checkin && $user->id) {
				$encodedLink = new stdClass();
				$encodedLink->eventId = $row->id;
				$encodedLink->userId = $user->id;
				$encodedLink->userName = $user->name;
				$encodedLink->auto_checkin = true;
				$key = AxsKeys::getKey('lms');
				$encryptedParams = base64_encode(AxsEncryption::encrypt($encodedLink, $key));
				$encryptedParamsLink = "&ev=$encryptedParams";
			}
			$baseURL = AxsLMS::getUrl();
			$virtualMeeting = '/join-virtual-classroom?guest_vcid='.$params->virtual_meeting.$encryptedParamsLink;
			$virtualMeetingLink = '<a href="'.$baseURL.$virtualMeeting.'" target="_blank">Click Here to Join Meeting</a>';
			$queryString['location'] = "location=" . urlencode($baseURL.$virtualMeeting);
		} else {
			$queryString['location'] = "location=" . $eventData['location'];
		}
		$queryString['trp']         = "trp=false";
		$queryString['websiteName'] = "sprop=" . $eventData['sitename'];
		$queryString['websiteURL']  = "sprop=name:" . $eventData['siteurl'];
		$queryString['details']     = "details=" . $eventData['details'];

		return "http://www.google.com/calendar/event?action=TEMPLATE&" . implode("&", $queryString);
	}

	/**
	 * Get URL to add the given event to Yahoo Calendar
	 *
	 * @param $row
	 *
	 * @return string
	 */
	public static function getAddToYahooCalendarUrl($row)
	{
		$params = json_decode($row->params);
		if($params->time_zone) {
			$timezone = $params->time_zone;
		} else {
			$timezone = null;
		}
		$eventData = self::getEventDataArray($row,$timezone);

		$urlString['title']      = "title=" . $eventData['title'];
		$urlString['st']         = "st=" . $eventData['st'];
		$urlString['et']         = "et=" . $eventData['et'];
		$urlString['rawdetails'] = "desc=" . $eventData['details'];
		if($params->virtual_meeting && $params->calendar_location == 'virtual') {
			$baseURL = AxsLMS::getUrl();
			$virtualMeeting = '/join-virtual-classroom?guest_vcid='.$params->virtual_meeting;
			$virtualMeetingLink = '<a href="'.$baseURL.$virtualMeeting.'" target="_blank">Click Here to Join Meeting</a>';
			$urlString['location']   = "in_loc=" . urlencode($baseURL.$virtualMeeting);
		} else {
			$urlString['location']   = "in_loc=" . $eventData['location'];
		}

		return "http://calendar.yahoo.com/?v=60&view=d&type=20&" . implode("&", $urlString);
	}

	/**
	 * Get event data
	 *
	 * @param $row
	 *
	 * @return mixed
	 */
	public static function getEventDataArray($row,$timezone = null)
	{
		$db           = JFactory::getDbo();
		$query        = $db->getQuery(true);
		$config       = JFactory::getConfig();
		$dateFormat   = "Ymd\THis\Z";
		if($timezone) {
			$eventDate    = JFactory::getDate($row->event_date, new DateTimeZone($timezone));
			$eventEndDate = JFactory::getDate($row->event_end_date, new DateTimeZone($timezone));
		} else {
			$eventDate    = JFactory::getDate($row->event_date);
			$eventEndDate = JFactory::getDate($row->event_end_date);
		}


		$data['title']    = urlencode($row->title);
		$data['dates']    = $eventDate->format($dateFormat) . "/" . $eventEndDate->format($dateFormat);
		$data['st']       = $eventDate->format($dateFormat);
		$data['et']       = $eventEndDate->format($dateFormat);
		$data['duration'] = abs(strtotime($row->event_end_date) - strtotime($row->event_date));

		// Get location data
		$query->select('a.*')
			->from('#__eb_locations AS a')
			->innerJoin('#__eb_events AS b ON a.id=b.location_id')
			->where('b.id=' . $row->id);

		$db->setQuery($query);
		$rowLocation = $db->loadObject();
		if ($rowLocation)
		{
			$locationInformation   = array();
			$locationInformation[] = $rowLocation->name;
			if ($rowLocation->address)
			{
				$locationInformation[] = $rowLocation->address;
			}
			if ($rowLocation->city)
			{
				$locationInformation[] = $rowLocation->city;
			}
			if ($rowLocation->state)
			{
				$locationInformation[] = $rowLocation->state;
			}
			if ($rowLocation->zip)
			{
				$locationInformation[] = $rowLocation->zip;
			}
			if ($rowLocation->country)
			{
				$locationInformation[] = $rowLocation->country;
			}
			$data['location'] = implode(', ', $locationInformation);
		}
		else
		{
			$data['location'] = '';
		}

		$data['sitename']   = urlencode($config->get('sitename'));
		$data['siteurl']    = urlencode(JUri::root());
		$data['rawdetails'] = urlencode($row->description);
		$data['details']    = htmlspecialchars_decode($row->description);
		if (strlen($data['details']) > 100)
		{
			$data['details'] = JString::substr($data['details'], 0, 100) . ' ...';
		}

		$data['details'] = urlencode($data['details']);

		return $data;
	}

	/**
	 * Filter and only return the available options for a quantity field
	 *
	 * @param array $values
	 * @param array $quantityValues
	 * @param int   $eventId
	 * @param int   $fieldId
	 * @param bool  $multiple
	 * @param array $multilingualValues
	 *
	 * @return array
	 */
	public static function getAvailableQuantityOptions(&$values, $quantityValues, $eventId, $fieldId, $multiple = false, $multilingualValues = array())
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// First, we need to get list of registration records of this event
		$query->select('id')
			->from('#__eb_registrants')
			->where('event_id = ' . $eventId)
			->where('(published = 1 OR (payment_method LIKE "os_offline%" AND published NOT IN (2,3)))');
		$db->setQuery($query);
		$registrantIds = $db->loadColumn();
		if (count($registrantIds))
		{
			$registrantIds = implode(',', $registrantIds);
			if ($multiple)
			{
				$fieldValuesQuantity = array();
				$query->clear();
				$query->select('field_value')
					->from('#__eb_field_values')
					->where('field_id = ' . $fieldId)
					->where('registrant_id IN (' . $registrantIds . ')');
				$db->setQuery($query);
				$rowFieldValues = $db->loadObjectList();
				if (count($rowFieldValues))
				{
					foreach ($rowFieldValues as $rowFieldValue)
					{
						$fieldValue = $rowFieldValue->field_value;
						if ($fieldValue)
						{
							if (is_string($fieldValue) && is_array(json_decode($fieldValue)))
							{
								$selectedOptions = json_decode($fieldValue);
							}
							else
							{
								$selectedOptions = array($fieldValue);
							}

							foreach ($selectedOptions as $selectedOption)
							{
								if (isset($fieldValuesQuantity[$selectedOption]))
								{
									$fieldValuesQuantity[$selectedOption]++;
								}
								else
								{
									$fieldValuesQuantity[$selectedOption] = 1;
								}
							}
						}
					}
				}
			}

			for ($i = 0, $n = count($values); $i < $n; $i++)
			{
				$value = trim($values[$i]);
				if ($multiple)
				{
					$total = isset($fieldValuesQuantity[$value]) ? $fieldValuesQuantity[$value] : 0;
				}
				else
				{
					$query->clear();
					$query->select('COUNT(*)')
						->from('#__eb_field_values')
						->where('field_id = ' . $fieldId)
						->where('registrant_id IN (' . $registrantIds . ')');

					if (!empty($multilingualValues))
					{
						$allValues = array_map(array($db, 'quote'), $multilingualValues[$i]);
						$query->where('field_value IN (' . implode(',', $allValues) . ')');
					}
					else
					{
						$query->where('field_value=' . $db->quote($value));
					}

					$db->setQuery($query);
					$total = $db->loadResult();
				}

				if ($total && !empty($quantityValues[$i]) && $quantityValues[$i] <= $total)
				{
					unset($values[$i]);
				}
			}
		}

		return $values;
	}

	/**
	 * Helper method to prepare meta data for the document
	 *
	 * @param \Joomla\Registry\Registry $params
	 *
	 * @param null                      $item
	 */
	public static function prepareDocument($params, $item = null)
	{
		$document         = JFactory::getDocument();
		$siteNamePosition = JFactory::getConfig()->get('sitename_pagetitles');
		$pageTitle        = $params->get('page_title');
		if ($pageTitle)
		{
			if ($siteNamePosition == 0)
			{
				$document->setTitle($pageTitle);
			}
			elseif ($siteNamePosition == 1)
			{
				$document->setTitle(JFactory::getConfig()->get('sitename') . ' - ' . $pageTitle);
			}
			else
			{
				$document->setTitle($pageTitle . ' - ' . JFactory::getConfig()->get('sitename'));
			}
		}

		if (!empty($item->meta_keywords))
		{
			$document->setMetaData('keywords', $item->meta_keywords);
		}
		elseif ($params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $params->get('menu-meta_keywords'));
		}

		if (!empty($item->meta_description))
		{
			$document->setMetaData('description', $item->meta_description);
		}
		elseif ($params->get('menu-meta_description'))
		{
			$document->setDescription($params->get('menu-meta_description'));
		}

		if ($params->get('robots'))
		{
			$document->setMetadata('robots', $params->get('robots'));
		}
	}

	/**
	 * Function to add dropdown menu
	 *
	 * @param string $vName
	 */
	public static function renderSubmenu($vName = 'dashboard')
	{
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__eb_menus')
			->where('published = 1')
			->where('menu_parent_id = 0')
			->order('ordering');
		$db->setQuery($query);
		$menus = $db->loadObjectList();
		$html  = '';
		$html .= '<ul id="mp-dropdown-menu" class="nav nav-tabs">';

		$currentLink = 'index.php' . JUri::getInstance()->toString(array('query'));
		for ($i = 0; $n = count($menus), $i < $n; $i++)
		{
			$menu = $menus[$i];

			$hasAccess = true;
			if (!JFactory::getUser()->authorise('core.admin')) {
				//Restrict access for non super users
				switch ($menu->menu_name) {
					case "EB_PAYMENT_PLUGINS":
					case "EB_TOOLS":
						$hasAccess = false;
						break;
				}
			}

			//task=reset_urls
			//task=update
			//task=share_translation

			if (!$hasAccess) {
				continue;
			}

			$query->clear();
			$query->select('*')
				->from('#__eb_menus')
				->where('published = 1')
				->where('menu_parent_id = ' . intval($menu->id))
				->order('ordering');
			$db->setQuery($query);
			$subMenus = $db->loadObjectList();
			if (!count($subMenus))
			{
				$liClass = 'nav-item';
				$anchorClass = 'nav-link';
				if ($menu->menu_link == $currentLink)
				{
					$anchorClass .= ' active';
				}
				$html .= '<li class="' . $liClass . '"><a class="' . $anchorClass .'" href="' . $menu->menu_link . '"> ' . JText::_($menu->menu_name) .
					'</a></li>';
			}
			else
			{
				$liClass = 'nav-item dropdown';
				$anchorClass = 'nav-link dropdown-toggle';
				for ($j = 0; $m = count($subMenus), $j < $m; $j++)
				{
					$subMenu = $subMenus[$j];
					if ($subMenu->menu_link == $currentLink)
					{
						$anchorClass .= ' active';
						break;
					}
				}

				if (!$hasAccess) {
					continue;
				}
				$html .= '<li class="' . $liClass . '">';
				$html .= '<a id="drop_' . $menu->id . '" href="#" data-bs-toggle="dropdown" role="button" class="' . $anchorClass . '"> ' .
					JText::_($menu->menu_name) . ' <b class="caret"></b></a>';
				$html .= '<ul aria-labelledby="drop_' . $menu->id . '" role="menu" class="dropdown-menu" id="menu_' . $menu->id . '">';
				for ($j = 0; $m = count($subMenus), $j < $m; $j++)
				{
					$subMenu = $subMenus[$j];

					$hasAccess = true;
					if ($subMenu->menu_name == "EB_IMPORT_TEMPLATE") {
						//TOVUTI - This function doesn't work.
						continue;
					}

					$class = 'dropdown-item';
					if ($subMenu->menu_link == $currentLink)
					{
						$class .= ' bg-light';
					}
					$html .= '<li><a class="' . $class . '" href="' . $subMenu->menu_link .
						'" tabindex="-1"> ' . AxsLanguage::text($subMenu->menu_name, $subMenu->menu_name) . '</a></li>';
				}
				$html .= '</ul>';
				$html .= '</li>';
			}
		}
		$html .= '</ul>';
		echo $html;
	}

	public static function getMediaInput($value, $fieldName = 'image')
	{
		JHtml::_('jquery.framework');
		$field = JFormHelper::loadFieldType('Media');

		$element = new SimpleXMLElement('<field />');
		$element->addAttribute('name', $fieldName);
		$element->addAttribute('class', 'readonly input-large');
		$element->addAttribute('preview', 'tooltip');
		$element->addAttribute('directory', 'events');

		$form = JForm::getInstance('sample-form', '<form> </form>');
		$field->setForm($form);
		$field->setup($element, $value);

		return $field->input;
	}
}
