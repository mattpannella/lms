<?php
/**
 * @package        	Joomla
 * @subpackage		Event Booking
 * @author  		Tuan Pham Ngoc
 * @copyright    	Copyright (C) 2010 - 2016 Ossolution Team
 * @license        	GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die ;
?>
<div id="eb-registration-complete-page" class="container">
	<h1 class="eb-page-heading"><?php echo JText::_('EB_REGISTRATION_CANCELLED'); ?></h1>
	<div class="eb-message"><?php echo $this->message; ?></div>
</div>