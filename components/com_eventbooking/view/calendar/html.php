<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

class EventbookingViewCalendarHtml extends RADViewHtml
{
	public function display()
	{
		$app    = JFactory::getApplication();
		$active = $app->getMenu()->getActive();
		$params = EventbookingHelper::getViewParams($active, array('calendar'));

		$config           = EventbookingHelper::getConfig();
		$showCalendarMenu = $config->activate_weekly_calendar_view || $config->activate_daily_calendar_view;

		$this->currentDateData  = EventbookingModelCalendar::getCurrentDateData();
		$this->showCalendarMenu = $showCalendarMenu;
		$this->config           = $config;

		#Support Weekly and Daily
		$layout = $this->getLayout();
		if ($layout == 'weekly')
		{
			$this->displayWeeklyView();

			return;
		}
		elseif ($layout == 'daily')
		{
			$this->displayDailyView();

			return;
		}

		$model = $this->getModel();
		$rows  = $model->getData();

		$state = $model->getState();

		$year  = $state->year;
		$month = $state->month;

		$this->data  = EventbookingHelperData::getCalendarData($rows, $year, $month);
		$this->month = $month;
		$this->year  = $year;
		$listMonth   = array(
			AxsLanguage::text("EB_JAN", "January"),
			AxsLanguage::text("EB_FEB", "February"),
			AxsLanguage::text("EB_MARCH", "March"),
			AxsLanguage::text("EB_APR", "April"),
			AxsLanguage::text("EB_MAY", "May"),
			AxsLanguage::text("EB_JUNE", "June"),
			AxsLanguage::text("EB_JULY", "July"),
			AxsLanguage::text("EB_AUG", "August"),
			AxsLanguage::text("EB_SEP", "September"),
			AxsLanguage::text("EB_OCT", "October"),
			AxsLanguage::text("EB_NOV", "November"),
			AxsLanguage::text("EB_DEC", "December"), );

		$options     = array();

		foreach($listMonth as $monthIndex => $monthName) {

			$monthValue = sprintf("%02d", $monthIndex + 1);

			$options[] = JHtml::_('select.option', $monthValue, $monthName);
		}

		// Force all incoming month values to be a zero-padded integer of length 2 (01 = Jan, 11 = Nov)
		$month = sprintf("%02d", $month);

		$this->searchMonth = JHtml::_('select.genericlist', $options, 'month', 'class="input-medium" onchange="submit();" ', 'value', 'text', $month);

		$options           = array();
		for ($i = $year - 3; $i < ($year + 5); $i++)
		{
			$options[] = JHtml::_('select.option', $i, $i);
		}
		$this->searchYear = JHtml::_('select.genericlist', $options, 'year', 'class="input-small" onchange="submit();" ', 'value', 'text', $year);

		EventbookingHelperHtml::prepareDocument($params);

		$fieldSuffix = EventbookingHelper::getFieldSuffix();
		$message     = EventbookingHelper::getMessages();

		if (strlen($message->{'intro_text' . $fieldSuffix}))
		{
			$introText = $message->{'intro_text' . $fieldSuffix};
		}
		else
		{
			$introText = $message->intro_text;
		}

		$this->listMonth = $listMonth;
		$this->params    = $params;
		$this->introText = $introText;

		parent::display();
	}

	/**
	 * Display weekly events
	 */
	protected function displayWeeklyView()
	{
		$config					 = EventbookingHelper::getConfig();
		$model                   = $this->getModel();
		$rows 				     = $model->getEventsByWeek();
		if ($config->process_plugin)
		{
			foreach($rows as $row)
			{
				$row->short_description = JHtml::_('content.prepare', $row->short_description);
			}
		}
		$this->events            = $rows;
		$this->first_day_of_week = $model->getState('date');

		parent::display();
	}

	/**
	 * Display daily events
	 */
	protected function displayDailyView()
	{
		EventbookingHelperJquery::colorbox('eb-colorbox-addlocation');
		$config 	  = EventbookingHelper::getConfig();
		$model        = $this->getModel();
		$rows 		  = $model->getEventsByDaily();
		if ($config->process_plugin)
		{
			foreach($rows as $row)
			{
				$row->short_description = JHtml::_('content.prepare', $row->short_description);
			}
		}
		$this->events = $rows;
		$this->day    = $model->getState('day');

		parent::display();
	}
}
