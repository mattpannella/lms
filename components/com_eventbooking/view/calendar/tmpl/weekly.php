<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
$timeFormat = $this->config->event_time_format ? $this->config->event_time_format : 'g:i a' ;
$width = (int) $this->config->map_width ;
if (!$width)
{
	$width = 800 ;
}
$height = (int) $this->config->map_height ;
if (!$height)
{
	$height = 600 ;
}
EventbookingHelperJquery::colorbox('eb-colorbox-map', $width.'px', $height.'px', 'true', 'false');
$bootstrapHelper = new EventbookingHelperBootstrap($this->config->twitter_bootstrap_version);
$daysInWeek = array(
	0 => AxsLanguage::text("EB_SUNDAY", "Sunday"),
	1 => AxsLanguage::text("EB_MONDAY", "Monday"),
	2 => AxsLanguage::text("EB_TUESDAY", "Tuesday"),
	3 => AxsLanguage::text("EB_WEDNESDAY", "Wednesday"),
	4 => AxsLanguage::text("EB_THURSDAY", "Thursday"),
	5 => AxsLanguage::text("EB_FRIDAY", "Friday"),
	6 => AxsLanguage::text("EB_SATURDAY", "Saturday")
);

$monthsInYear = array(
	1 => AxsLanguage::text("EB_JAN", "January"),
	2 => AxsLanguage::text("EB_FEB", "February"),
	3 => AxsLanguage::text("EB_MARCH", "March"),
	4 => AxsLanguage::text("EB_APR", "April"),
	5 => AxsLanguage::text("EB_MAY", "May"),
	6 => AxsLanguage::text("EB_JUNE", "June"),
	7 => AxsLanguage::text("EB_JUL", "July"),
	8 => AxsLanguage::text("EB_AUG", "August"),
	9 => AxsLanguage::text("EB_SEP", "September"),
	10 => AxsLanguage::text("EB_OCT", "October"),
	11 => AxsLanguage::text("EB_NOV", "November"),
	12 => AxsLanguage::text("EB_DEC", "December")
);
?>
<h1 class="eb-page-heading"><?php echo JText::_('EB_CALENDAR') ; ?></h1>
<div id="extcalendar">
<div style="width: 100%;" class="topmenu_calendar <?php echo $bootstrapHelper->getClassMapping('row-fluid');?>">
	<div class="left_calendar <?php echo $bootstrapHelper->getClassMapping('span7'); ?>">
		<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr>
				<td class="tableh1" colspan="3">
					<table>
						<tr>
							<td align="right" class="today">
								<?php
									$startWeekTime = strtotime("$this->first_day_of_week");
									$endWeekTime = strtotime("+6 day", strtotime($this->first_day_of_week)) ;
									echo $daysInWeek[date('w', $startWeekTime)].'. '.date('d', $startWeekTime).' '.$monthsInYear[date('n', $startWeekTime)].', '.date('Y', $startWeekTime).' - '.$daysInWeek[date('w', $endWeekTime)].'. '.date('d', $endWeekTime).' '.$monthsInYear[date('n', $endWeekTime)].', '.date('Y', $endWeekTime) ;
								?>
							</td>
						</tr>
					</table>
				</td>
	   </table>
	</div>
	<?php
	if ($this->showCalendarMenu)
	{
	?>
		<div class="<?php echo $bootstrapHelper->getClassMapping('span5');?>">
			<?php echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/calendar_navigation.php', array('Itemid' => $this->Itemid, 'config' => $this->config, 'layout' => 'weekly', 'currentDateData' => $this->currentDateData)); ?>
		</div>
	<?php
	}
	?>
</div>

<div class="wraptable_calendar">
<table cellpadding="0" cellspacing="0" width="100%" border="0">

	<tr class="tablec">
		<td class="previousweek">
			<a href="<?php echo JRoute::_("index.php?option=com_eventbooking&view=calendar&layout=weekly&date=".date('Y-m-d',strtotime("-7 day", strtotime($this->first_day_of_week)))."&Itemid=$this->Itemid"); ?>" rel="nofollow">
				<img src="<?php echo JUri::root()?>media/com_eventbooking/assets/images/calendar_previous.png" alt="<?php echo AxsLanguage::text("EB_PREVIOUS_WEEK", "Previous Week") ?>">
			</a>
		</td>
		<td class="currentweek currentweektoday">
			<?php echo AxsLanguage::text("EB_WEEK", "WEEK") ?> <?php echo date('W',strtotime("+6 day", strtotime($this->first_day_of_week)));?>
		</td>
		<td class="nextweek">
			<a class="shajaxLinkNextWeek extcalendar prefetch" href="<?php echo JRoute::_("index.php?option=com_eventbooking&view=calendar&layout=weekly&date=".date('Y-m-d',strtotime("+7 day", strtotime($this->first_day_of_week)))."&Itemid=$this->Itemid"); ?>" rel="nofollow">
				<img src="<?php echo JUri::root()?>media/com_eventbooking/assets/images/calendar_next.png" alt="<?php echo AxsLanguage::text("EB_NEXT_WEEK", "Next Week") ?>">
			</a>
		</td>
	</tr>
	<?php
		foreach ($this->events AS $key => $events)
		{
		?>
		<tr class="tableh2">
			<td class="tableh2" colspan="3">
				<?php
					$time = strtotime("+$key day", strtotime($this->first_day_of_week)) ;
					echo $daysInWeek[date('w', $time)].'. '.date('d', $time).' '.$monthsInYear[date('n', $time)].', '.date('Y', $time) ;
				?>
			</td>
		</tr>
		<tr>
			<?php
				if (!count($events))
				{
				?>
				<td align="center" class="tableb" colspan="3">
						<strong>
							<?php echo AxsLanguage::text("EB_NO_EVENT_ON_THIS_DAY", "There are no events on this day"); ?>
						</strong>
				</td>
				<?php
				}
				else
				{
				?>
				<td class="tableb" align="left" colspan="3">
					 <?php
						foreach ($events as $event)
						{
							$url = JRoute::_(EventbookingHelperRoute::getEventRoute($event->id, 0, $this->Itemid));
					    ?>
						<table width="100%">
							<tr>
								<td class="tablea" width="10%">
									<a href="<?php echo $url; ?>"><?php echo JHtml::_('date', $event->event_date, $timeFormat, null);?></a>
								</td>
								<td class="tableb">
									 <div class="eventdesc">
										<h4><a href="<?php echo $url; ?>"><?php echo $event->title?></a></h4>
										<p class="location-name">
											<a href="<?php echo JRoute::_('index.php?option=com_eventbooking&view=map&location_id='.$event->location_id.'&Itemid='.$this->Itemid.'&tmpl=component&format=html'); ?>" title="<?php echo $event->location_name ; ?>" class="eb-colorbox-map" rel="nofollow">
												<?php echo $event->location_name; ?>
											</a>
										</p>
										<?php echo $event->short_description; ?>
									</div>
								</td>
							</tr>
						</table>
						<?php
						}
					 ?>
				</td>
				<?php
				}
			?>
		</tr>
	<?php
		}
	?>
</table>
</div>
</div>