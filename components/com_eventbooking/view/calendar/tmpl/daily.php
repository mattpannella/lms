<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
$timeFormat = $this->config->event_time_format ? $this->config->event_time_format : 'g:i a' ;
$daysInWeek = array(
		0 => AxsLanguage::text("EB_SUNDAY", "Sunday"),
		1 => AxsLanguage::text("EB_MONDAY", "Monday"),
		2 => AxsLanguage::text("EB_TUESDAY", "Tuesday"),
		3 => AxsLanguage::text("EB_WEDNESDAY", "Wednesday"),
		4 => AxsLanguage::text("EB_THURSDAY", "Thursday"),
		5 => AxsLanguage::text("EB_FRIDAY", "Friday"),
		6 => AxsLanguage::text("EB_SATURDAY", "Saturday")
);

$monthsInYear = array(
		1 => AxsLanguage::text("EB_JAN", "January"),
		2 => AxsLanguage::text("EB_FEB", "February"),
		3 => AxsLanguage::text("EB_MARCH", "March"),
		4 => AxsLanguage::text("EB_APR", "April"),
		5 => AxsLanguage::text("EB_MAY", "May"),
		6 => AxsLanguage::text("EB_JUNE", "June"),
		7 => AxsLanguage::text("EB_JUL", "July"),
		8 => AxsLanguage::text("EB_AUG", "August"),
		9 => AxsLanguage::text("EB_SEP", "September"),
		10 => AxsLanguage::text("EB_OCT", "October"),
		11 => AxsLanguage::text("EB_NOV", "November"),
		12 => AxsLanguage::text("EB_DEC", "December")
);
?>
<h1 class="eb-page-heading"><?php echo AxsLanguage::text("EB_CALENDAR", "Events Calendar"); ?></h1>
<div id="extcalendar">
<div style="width: 100%;" class="topmenu_calendar">
	<div class="left_calendar">
		<strong><?php echo AxsLanguage::text("EB_CHOOSE_DATE", "Choose Date"); ?>:</strong>
		<?php echo JHtml::_('calendar', JRequest::getVar('day', ''),'date', 'date', '%Y-%m-%d'); ?>
		<input type="button" class="btn" value="<?php echo AxsLanguage::text("EB_GO", "Go"); ?>" onclick="gotoDate();" />
	</div>
	<?php
		if ($this->showCalendarMenu)
		{
			echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/calendar_navigation.php', array('Itemid' => $this->Itemid, 'config' => $this->config, 'layout' => 'daily', 'currentDateData' => $this->currentDateData));
		}
	?>
</div>
<div class="wraptable_calendar">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr class="tablec">
		<td class="previousday">
			<a href="<?php echo JRoute::_("index.php?option=com_eventbooking&view=calendar&layout=daily&day=".date('Y-m-d',strtotime("-1 day", strtotime($this->day)))."&Itemid=$this->Itemid");?>" rel="nofollow">
				<img src="<?php echo JUri::root()?>media/com_eventbooking/assets/images/calendar_previous.png" alt="<?php echo AxsLanguage::text("EB_PREVIOUS_DAY", "Previous Day") ?>">
			</a>
		</td>
		<td class="currentday currentdaytoday">
			<?php
				$time = strtotime($this->day) ;
				echo $daysInWeek[date('w', $time)].', '.$monthsInYear[date('n', $time)].' '.date('d', $time).', '.date('Y', $time);
			?>
		</td>
		<td class="nextday">
			<a href="<?php echo JRoute::_("index.php?option=com_eventbooking&view=calendar&layout=daily&day=".date('Y-m-d',strtotime("+1 day", strtotime($this->day)))."&Itemid=$this->Itemid");?>" rel="nofollow">
				<img src="<?php echo JUri::root()?>media/com_eventbooking/assets/images/calendar_next.png" alt="<?php echo AxsLanguage::text("EB_NEXT_DAY", "Next Day") ?>">
			</a>
		</td>
	</tr>

	<tr>
		<td colspan="3">
			<?php
			if (count($this->events))
			{
			?>
			<table cellpadding="0" cellspacing="0" width="100%" border="0">
				<?php
					foreach ($this->events AS $key => $event)
					{
						$url = JRoute::_(EventbookingHelperRoute::getEventRoute($event->id, 0, $this->Itemid));
				?>
					<tr>
						<td class="tablea">
							<a href="<?php echo $url; ?>"><?php echo JHtml::_('date', $event->event_date, $timeFormat, null);?></a>
						</td>
						<td class="tableb">
							<div class="eventdesc">
								<h4><a href="<?php echo $url; ?>"><?php echo $event->title?></a></h4>
								<p class="location-name">
									<a href="<?php echo JRoute::_('index.php?option=com_eventbooking&view=map&location_id='.$event->location_id.'&tmpl=component&format=html'); ?>" title="<?php echo $event->location_name ; ?>" class="eb-colorbox-addlocation" rel="nofollow"><?php echo $event->location_name; ?></a>
							     </p>
								<?php echo $event->short_description; ?>
							</div>
						</td>
					</tr>
				<?php }?>
			</table>
			<?php
			}
			else
			{
				echo '<span class="eb_no_events">'.AxsLanguage::text("EB_NO_EVENTS", "There are no events in the selected category")."</span>";
			}
			?>
		</td>
	</tr>
</table>
</div>
</div>
<script type="text/javascript">
	var url = "<?php echo JRoute::_('index.php?option=com_eventbooking&view=calendar&layout=daily&Itemid='.$this->Itemid.'&day=', false); ?>";
	function gotoDate()
	{
		date = document.getElementById('date');
		if (date.value)
		{
			location.href = url + date.value;
		}
		else
		{
			alert("<?php echo AxsLanguage::text("EB_PLEASE_CHOOSE_DATE", "Please choose the date you want to view event"); ?>");
		}
	}
</script>
