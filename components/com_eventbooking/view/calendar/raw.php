<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

class EventbookingViewCalendarRaw extends RADViewHtml
{
	public function display()
	{
		$currentDateData = EventbookingModelCalendar::getCurrentDateData();

		//Initialize default month and year
		$month = $this->input->getInt('month', 0);
		$year  = $this->input->getInt('year', 0);
		if (!$month)
		{
			$month = $currentDateData['month'];
		}

		if (!$year)
		{
			$year = $currentDateData['year'];
		}

		$model    = new EventbookingModelCalendar(array('remember_states' => false, 'ignore_request' => true));
		$model->setState('month', $month)
			->setState('year', $year);

		$rows        = $model->getData();
		$this->data  = EventbookingHelperData::getCalendarData($rows, $year, $month, true);
		$this->month = $month;
		$this->year  = $year;

		$days     = array();
		$startDay = EventBookingHelper::getConfigValue('calendar_start_date');
		for ($i = 0; $i < 7; $i++)
		{
			$days[$i] = EventbookingHelperData::getDayNameHtmlMini(($i + $startDay) % 7, true);
		}

		$listMonth = array(
			AxsLanguage::text("EB_JAN", "January"),
			AxsLanguage::text("EB_FEB", "February"),
			AxsLanguage::text("EB_MARCH", "March"),
			AxsLanguage::text("EB_APR", "April"),
			AxsLanguage::text("EB_MAY", "May"),
			AxsLanguage::text("EB_JUNE", "June"),
			AxsLanguage::text("EB_JUL", "July"),
			AxsLanguage::text("EB_AUG", "August"),
			AxsLanguage::text("EB_SEP", "September"),
			AxsLanguage::text("EB_OCT", "October"),
			AxsLanguage::text("EB_NOV", "November"),
			AxsLanguage::text("EB_DEC", "December"), );

		$this->days      = $days;
		$this->listMonth = $listMonth;

		parent::display();
	}
}
