<?php
defined('_JEXEC') or die;
$eventParams = json_decode($this->event->params);
$ticketSettings = json_decode($eventParams->ticket_settings);
$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}
?>
<style>
	/* Customize the label (the container) */
	.tovuticheck {
	  display: block;
	  position: relative;
	  padding-left: 35px;
	  margin-bottom: 12px;
	  cursor: pointer;
	  font-size: 22px;
	  -webkit-user-select: none;
	  -moz-user-select: none;
	  -ms-user-select: none;
	  user-select: none;
	  margin-left: 10px;
	}

	/* Hide the browser's default checkbox */
	.tovuticheck input {
	  position: absolute;
	  opacity: 0;
	  cursor: pointer;
	  height: 0;
	  width: 0;
	}

	/* Create a custom checkbox */
	.tovuticheck .checkmark {
	  position: absolute;
	  top: 0;
	  left: 0;
	  height: 25px;
	  width: 25px;
	  background-color: #eee;
	  border: solid 1px #2196F3;
	}

	/* On mouse-over, add a grey background color */
	.tovuticheck:hover input ~ .checkmark {
	  background-color: #ccc;
	}

	/* When the checkbox is checked, add a blue background */
	.tovuticheck input:checked ~ .checkmark {
	  background-color: #2196F3;
	}

	/* Create the checkmark/indicator (hidden when not checked) */
	.tovuticheck .checkmark:after {
	  content: "";
	  position: absolute;
	  display: none;
	}

	/* Show the checkmark when checked */
	.tovuticheck input:checked ~ .checkmark:after {
	  display: block;
	}

	/* Style the checkmark/indicator */
	.tovuticheck .checkmark:after {
	  left: 9px;
	  top: 5px;
	  width: 6px;
	  height: 12px;
	  border: solid white;
	  border-width: 0 3px 3px 0;
	  -webkit-transform: rotate(45deg);
	  -ms-transform: rotate(45deg);
	  transform: rotate(45deg);
	}
</style>
<h3 class="eb-heading">
	<?php 
		if ($ticketSettings->ticket_main_heading) {
			echo $ticketSettings->ticket_main_heading;
		} else {
			echo JText::_('EB_TICKET_INFORMATION');	
		}
	?>
</h3>
<table class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th>
			<?php 
				if ($ticketSettings->ticket_column_heading) {
					echo $ticketSettings->ticket_column_heading;
				} else {
					 echo JText::_('EB_TICKET_TYPE');	
				}
			?>
		</th>
		<?php if ($ticketSettings->show_price_column || !isset($ticketSettings->show_price_column)) { ?>
		<th>
			<?php echo JText::_('EB_PRICE'); ?>
		</th>
		<?php  } ?>
		<?php if ($ticketSettings->show_availability_column || !isset($ticketSettings->show_availability_column)) { ?>
			<th class="center">
				<?php echo AxsLanguage::text("AXS_SPOTS_AVAILABLE", "Spots Available") ?>
			</th>
		<?php } ?>
		<th>
			<?php
				if ($ticketSettings->ticket_select_type == 'quantity' || !isset($ticketSettings->ticket_select_type)) {
					echo JText::_('EB_QUANTITY'); 
				} else {
					echo AxsLanguage::text("JSELECT", "Select");
				}
			?>
		</th>
	</tr>
	</thead>
	<tbody>
	<?php
	foreach ($this->ticketTypes as $ticketType)
	{
	?>
		<tr>
			<td class="eb-ticket-type-title">
				<?php echo $ticketType->title; ?>
				<?php
					if ($ticketType->description)
					{
					?>
						<p class="eb-ticket-type-description"><?php echo $ticketType->description; ?></p>
					<?php
					}
				?>
			</td>
			<?php if ($ticketSettings->show_price_column || !isset($ticketSettings->show_price_column)) { ?>
			<td>
				<?php echo $currency_symbol.$ticketType->price; ?>
			</td>
			<?php } ?>
			<?php
			$available = $ticketType->capacity - $ticketType->registered;

			if ($ticketSettings->show_availability_column || !isset($ticketSettings->show_availability_column)) {
			?>
				<td class="center">
					<?php echo $available; ?>
				</td>
			<?php
			}
			?>
			<td class="center">
				<?php
					if ($available > 0)
					{
						$fieldName = 'ticket_type_' . $ticketType->id;

						if ($ticketType->max_tickets_per_booking > 0)
						{
							$available = min($available, $ticketType->max_tickets_per_booking);
						}

						if ($ticketSettings->ticket_select_type == 'quantity' || !isset($ticketSettings->ticket_select_type)) {
							echo JHtml::_('select.integerlist', 0, $available, 1, $fieldName, 'class="ticket_type_quantity input-small" onchange="calculateIndividualRegistrationFee();"', $this->input->getInt($fieldName, 0));
						} else {
							echo '<label class="tovuticheck">
									  <input type="checkbox" name="'.$fieldName.'" value="1" class="ticket_type_quantity">
									  <span class="checkmark"></span>
								  </label>';
						}
						
					}
					else
					{
						echo JText::_('EB_NA');
					}
				?>
			</td>
		</tr>
	<?php
	}
	?>
	</tbody>
</table>

<?php if ($ticketSettings->ticket_select_type != 'quantity') { ?>
<script>
	$('.ticket_type_quantity').click(function() {
		<?php if ($ticketSettings->ticket_select_type == 'single') { ?>
		$('.ticket_type_quantity').prop('checked',false);
		$(this).prop('checked',true);
		<?php } ?>
		calculateIndividualRegistrationFee();
	});
</script>
<?php } ?>

