<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

$lang = JFactory::getLanguage();
$lang->load("com_splms");
$brand = AxsBrands::getBrand();

/**
 * Layout variables
 * -----------------
 * @var   EventbookingViewRegisterHtml $this
 * @var   string                       $controlGroupClass
 * @var   string                       $controlLabelClass
 * @var   string                       $controlsClass
 */

if (count($this->methods) > 1)
{
?>
	<div class="<?php echo $controlGroupClass;  ?> payment_information" id="payment_method_container">
		<label class="<?php echo $controlLabelClass; ?>" for="payment_method">
			<?php echo JText::_('EB_PAYMENT_OPTION'); ?>
			<span class="required">*</span>
		</label>
		<div class="<?php echo $controlsClass; ?>">
			<?php
			$method = null;

			for ($i = 0, $n = count($this->methods); $i < $n; $i++)
			{
				$paymentMethod = $this->methods[$i];

				if ($paymentMethod->getName() == $this->paymentMethod)
				{
					$checked = ' checked="checked" ';
					$method  = $paymentMethod;
				}
				else
				{
					$checked = '';
				}
				?>
					<label class="radio">
						<input onclick="changePaymentMethod('individual');" class="validate[required] radio"
							   type="radio" name="payment_method"
							   value="<?php echo $paymentMethod->getName(); ?>" <?php echo $checked; ?> /><?php echo JText::_($paymentMethod->getTitle()); ?>
					</label>
				<?php
			}
			?>
		</div>
	</div>
	<?php
}
else
{
	$method = $this->methods[0];
?>
	<div class="<?php echo $controlGroupClass;  ?> payment_information" id="payment_method_container">
		<label class="<?php echo $controlLabelClass; ?>">
			<?php echo JText::_('EB_PAYMENT_OPTION'); ?>
		</label>

		<div class="<?php echo $controlsClass; ?>">
			<?php echo JText::_($method->getTitle()); ?>
		</div>
	</div>
<?php
}

$cards = AxsPayment::getUserAllCards();
//$cards = null;
if (count($cards)) { ?>

	<div id="card_on_file_group">
		<div class="<?php echo $controlGroupClass;  ?> payment_information">
			<label class="<?php echo $controlLabelClass; ?>" for="card_on_file">
				Card on File<span class="required">*</span>
			</label>

			<div class="<?php echo $controlsClass; ?>">
				<select id="card_on_file" name="card_on_file">
					<option value="0">--select card--</option>
					<?php foreach($cards as $card) { ?>
						<option value="<?php echo $card->id; ?>">Ending in <?php echo $card->cardNumber; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="new_card_button">
				&nbsp
			</label>
			<button type="button" id="use_new_card" class="btn btn-primary" name="new_card_button">Enter New Card</button>
		</div>
	</div>

<?php } ?>

<div id="new_card_group" <?php echo count($cards) ? 'style="display:none;"' : ''; ?> data-gtw="<?php echo $brand->billing->gateway_type; ?>">
	<?php if (count($cards)) { ?>
		<label class="control-label" for="use_card_on_file">
			&nbsp
		</label>
		<div class="control-group">
			<button type="button" id="use_card_on_file" class="btn btn-primary" name="use_card_on_file">Use Card on File</button>
		</div>
	<?php } ?>

	<?php
		if($brand->billing->gateway_type == 'stripe') {
			$stripeParams = new stdClass();
			$stripeParams->event_formId = "adminForm";
            $stripeForm = AxsPayment::buildStipePaymentForm($stripeParams);
    ?>
    <div class="<?php echo $controlGroupClass;  ?> payment_information">
		<div class="<?php echo $controlsClass; ?>">
			<?php echo $stripeForm; ?>
		</div>
	</div>
    <?php
        } else {
	?>
	<div class="<?php echo $controlGroupClass;  ?> payment_information" id="tr_card_number">
		<label class="<?php echo $controlLabelClass; ?>" for="x_card_num">
			<?php echo JText::_('AUTH_CARD_NUMBER'); ?><span class="required">*</span>
		</label>

		<div class="<?php echo $controlsClass; ?>">
			<input type="text" id="x_card_num" name="x_card_num"
				   class="input-large validate[required,creditCard]"
				   value="<?php echo $this->escape($this->input->getAlnum('x_card_num')); ?>" onchange="removeSpace(this);"/>
		</div>
	</div>
	<div class="<?php echo $controlGroupClass;  ?> payment_information" id="tr_exp_date">
		<label class="<?php echo $controlLabelClass; ?>">
			<?php echo JText::_('AUTH_CARD_EXPIRY_DATE'); ?><span class="required">*</span>
		</label>

		<div class="<?php echo $controlsClass; ?>">
			<?php echo $this->lists['exp_month'] . '  /  ' . $this->lists['exp_year']; ?>
		</div>
	</div>
	<div class="<?php echo $controlGroupClass;  ?> payment_information" id="tr_cvv_code">
		<label class="<?php echo $controlLabelClass; ?>" for="x_card_code">
			<?php echo JText::_('AUTH_CVV_CODE'); ?><span class="required">*</span>
		</label>

		<div class="<?php echo $controlsClass; ?>">
			<input
				type="text"
				id="x_card_code"
				name="x_card_code"
				class="input-large validate[required,custom[number]]"
				value="<?php echo $this->escape($this->input->getString('x_card_code')); ?>"
			/>
		</div>
	</div>
	<div class="container-fluid payment_information">
		<div class="col-md-4">
			<h4><?php echo JText::_('COM_SPLMS_AXS_BILLING_ADDRESS') ?></h4>
			<div class="form-group">
				<label for="billing_fname" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_FIRST_NAME') ?></label>
				<input id="billing_fname" name="billing_first" class="form-control required-field">
			</div>
			<div class="form-group">
				<label for="billing_lname" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_LAST_NAME') ?></label>
				<input id="billing_lname" name="billing_last" class="form-control required-field">
			</div>

			<div class="form-group">
				<label for="billing_addr" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_ADDRESS') ?></label>
				<input id="billing_addr" name="billing_address" class="form-control required-field">
			</div>
			<div class="form-group">
				<label for="billing_city" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_CITY') ?></label>
				<input id="billing_city" name="billing_city" class="form-control required-field">
			</div>
			<div class="form-group">
				<label for="billing_state" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_STATE') ?></label>
				<input id="billing_state" name="billing_state" class="form-control required-field">
			</div>
			<div class="form-group">
				<label for="billing_zip" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_ZIP') ?></label>
				<input id="billing_zip" name="billing_zip" class="form-control required-field">
			</div>
			<div class="form-group">
				<label for="billing_cty" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_COUNTRY') ?></label>
				<input id="billing_cty" name="billing_country" class="form-control required-field" value="United States">
			</div>
			<div class="form-group">
				<label for="billing_pay_phone" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_PHONE') ?></label>
				<input id="billing_pay_phone" name="billing_pay_phone" class="form-control">
			</div>
		</div>
	</div>
	<?php } //end of stripe if statemet ?>
</div>

<script>
	$(document).ready(
		function() {
			var card_drop_down = document.getElementById("card_on_file");

			//Check to see if the user is on the single registration page.  If not, the button will not exist.
			var submit_button = document.getElementById("btn-submit");
			if (!submit_button) {
				//Check to see if the user is on the group registration page.
				submit_button = document.getElementById("btn-process-group-billing");
			}

			if (submit_button) {

				submit_button.disabled = true;

				$(card_drop_down).change(
					function() {
						var card = card_drop_down.options[card_drop_down.selectedIndex].value;
						if (card !== 0)  {
							submit_button.disabled = false;
						} else {
							submit_button.disabled = true;
						}
					}
				);

				setInterval(function() {
					var amount = $('#amount').val();
					var new_card_group = document.getElementById("new_card_group");
					var card_on_file_group = document.getElementById("card_on_file_group");
					if ($(new_card_group).is(":visible")) {
						var cc_card_number = document.getElementById("x_card_num");
						var cc_card_code = document.getElementById("x_card_code");
						if ( (!cc_card_number && !cc_card_code) || ( (cc_card_code.value && cc_card_number.value) || (amount == 0) ) ) {
							submit_button.disabled = false;
						} else {
							submit_button.disabled = true;
						}
					}

					if ($(card_on_file_group).is(":visible")) {
						var card = card_drop_down.options[card_drop_down.selectedIndex].value;
						if ( (card !== "0")  || (amount == 0) ) {
							submit_button.disabled = false;
						} else {
							submit_button.disabled = true;
						}
					}

				}, 200);
			}

			// New Card button
			$('#use_new_card').click(function() {
				$('.required-field').attr('required',true);
				//clear card on file
				$('#card_on_file').val("0");
				//hide card on file section
				submit_button.disabled = true;
				$('#card_on_file_group').slideToggle();
				$('#new_card_group').slideToggle();
			});

			// Card on File button
			$('#use_card_on_file').click(function() {
				$('.required-field').attr('required',false);
				//clear card data

				if ($('#card_on_file').val() !== "0") {
					submit_button.disabled = false;
				}

				$('#x_card_num').val('');
				$('#exp_month').val('');
				$('#exp_year').val('');
				$('#x_card_code').val('');

				$('#new_card_group').slideToggle();
				$('#card_on_file_group').slideToggle();
			});
		}
	);

	$("input[name='payment_method']").change(function() {
		if($(this).val() == 'os_offline') {
			$("#card_on_file_group").hide();
			$("#new_card_group").hide();
			$("#btn-submit,#btn-process-group-billing").removeAttr('disabled');
		} else {
			$("#card_on_file_group").show();
			$("#new_card_group").show();
			$("#btn-submit,#btn-process-group-billing").attr('disabled');
		}
	});
</script>
