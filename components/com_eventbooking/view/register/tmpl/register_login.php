<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;
/**
 * Layout variables
 * -----------------
 * @var   string $controlGroupClass
 * @var   string $controlLabelClass
 * @var   string $controlsClass
 */
?>
<!-- <h3 class="eb-heading"><?php echo JText::_('EB_EXISTING_USER_LOGIN'); ?></h3>
<form method="post" action="index.php" name="eb-login-form" id="eb-login-form" autocomplete="off" class="form form-horizontal">
	<div class="<?php echo $controlGroupClass;  ?>">
		<label class="<?php echo $controlLabelClass; ?>" for="username">
			<?php echo  JText::_('EB_USERNAME') ?><span class="required">*</span>
		</label>
		<div class="<?php echo $controlsClass; ?>">
			<input type="text" name="username" id="username" class="input-large validate[required]" value=""/>
		</div>
	</div>
	<div class="<?php echo $controlGroupClass;  ?>">
		<label class="<?php echo $controlLabelClass; ?>" for="password">
			<?php echo  JText::_('EB_PASSWORD') ?><span class="required">*</span>
		</label>
		<div class="<?php echo $controlsClass; ?>">
			<input type="password" id="password" name="password" class="input-large validate[required]" value="" />
		</div>
	</div>
	<div class="<?php echo $controlGroupClass;  ?>">
		<div class="<?php echo $controlsClass; ?>">
			<input type="submit" value="<?php echo JText::_('EB_LOGIN'); ?>" class="button btn btn-primary" />
		</div>
	</div>
	<?php
	if (JPluginHelper::isEnabled('system', 'remember'))
	{
	?>
		<input type="hidden" name="remember" value="1" />
	<?php
	}
	?>
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" id="return_url" value="<?php echo base64_encode(JUri::getInstance()->toString()); ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form> -->
<?php

$uri = JFactory::getURI();
$pageURL = $uri->toString();
$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
$security = AxsSecurity::getSettings();
if($security->twofactor_type == 'yubikey') {
	$sercretKeyInputType = 'password';
} else {
	$sercretKeyInputType = 'text';
}
?>
<h3 class="eb-heading"><?php echo JText::_('EB_EXISTING_USER_LOGIN'); ?></h3>
<div class="event-tovuti-login-page">
	<div class="event-tovuti-login-form">
		<div id="event-login-error-message" style="display: none;"></div>
		<form action="#" method="post" id="event-login-form" class="form-inline" aria-label="Login Form">

			<div class="userdata">
				<div id="event-form-login-username" class="control-group">
					<div class="controls">
						<input  aria-label="Enter Your Username or Email" id="event-modlgn-username" type="text" name="username" tabindex="0" placeholder="Username" />
					</div>
				</div>
				<div id="event-form-login-password" class="control-group">
					<div class="controls">
						<input aria-label="Enter Your Password" id="event-modlgn-passwd" type="password" name="password"  tabindex="0"  placeholder="Password" />
					</div>
				</div>

				<?php if (count($twofactormethods) > 1) { ?>
					<div id="event-form-login-secretkey" class="control-group" style="display:none;">
						<div class="controls">
							<input id="event-modlgn-secretkey" autocomplete="off" type="<?php echo $sercretKeyInputType; ?>" name="secretkey" tabindex="0" placeholder="<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key") ?>">
						</div>
					</div>
				<?php } ?>
				<div id="event-form-login-submit" class="control-group">
					<div class="controls">
						<button type="submit" aria-label="Click to Login" tabindex="0" name="Submit" class="btn btn-primary login-button">Login</button>
					</div>
				</div>
				<p class="message"><a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=reset"); ?>">
							Forgot your Password?</a> |
							<a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=remind"); ?>">
							Forgot your Username?</a>
						</p>
				<input type="hidden" name="return" id="event-return" value="<?php echo $return; ?>" />

				<?php echo JHtml::_('form.token'); ?>
			</div>

		</form>
	</div>
</div>

<script>
	var pageURL = '<?php echo base64_encode($pageURL); ?>';
	var returnURL = '<?php echo $return; ?>';

	if (returnURL == pageURL) {
		$('#event-return').val(btoa(parent.window.location));
	}

	$("#event-login-form").submit( function(e) {
		e.preventDefault();
		$('.event-tovuti-login-form button').css('background','#777').attr('disabled','true');
		//var data = $(this).serialize();
		var data = {
			username: $("#event-modlgn-username").val(),
			password: $("#event-modlgn-passwd").val(),
			secretkey: $("#event-modlgn-secretkey").val(),
			return: $("#event-return").val()
		}

		if($("#event-login-error-message:visible")) {
			$("#event-login-error-message").hide();
		}

		$.ajax({
			url: '/index.php?option=com_axs&task=popupmodule.loginAuthenticate&format=raw',
			type: 'post',
			data: data
		}).done(
			function(result) {
				//console.log(result);
				var response = JSON.parse(result);
				if(response.secretKeyVerification) {
					$('.event-tovuti-login-form button').css('background','#4CAF50').removeAttr('disabled');
					$("#event-login-error-message").html(response.message);
					if($("#event-login-error-message:hidden")) {
						$("#event-login-error-message").slideToggle();
					}
					if($("#event-form-login-secretkey").is(':hidden')) {
						$('#event-form-login-secretkey').slideToggle();
					}
					return;
				}
				if (response.status == "success") {
					$.ajax({
						url: '/index.php?option=com_users&task=user.login',
						type: 'post',
						data: data
					}).done( function() {
						window.location = response.url;
					});

				} else {
					$('.event-tovuti-login-form button').css('background','#4CAF50').removeAttr('disabled');
					$("#event-login-error-message").html(response.message);
					if($("#event-login-error-message:hidden")) {
						$("#event-login-error-message").slideToggle();
					}
				}
		});
	});
</script>
<h3 class="eb-heading"><?php echo JText::_('EB_NEW_USER_REGISTER'); ?></h3>
