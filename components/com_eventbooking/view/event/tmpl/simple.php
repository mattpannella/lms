<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
$editor = JFactory::getEditor() ;
$format = 'Y-m-d' ;
EventbookingHelperJquery::validateForm();
$bootstrapHelper = new EventbookingHelperBootstrap($this->config->twitter_bootstrap_version);
$controlGroupClass = $bootstrapHelper->getClassMapping('control-group');
$controlLabelClass = $bootstrapHelper->getClassMapping('control-label');
$controlsClass     = $bootstrapHelper->getClassMapping('controls');

$params = json_decode($this->item->params);
$user = JFactory::getUser();
$isAdmin = false;//$user->authorise('core.manage');

$directory = AxsImages::getImagesPath('events') . '/images/defaults/';
$default_image_list = glob("$directory*.{jpg,png,bmp}", GLOB_BRACE);

?>

<style>
	.calendar {
		vertical-align: bottom;
	}

	.title {
		font-size: 14px;
		border-radius: 0px;
		color: #444;
		background: #fff;
		text-transform: none;
	}

	.table {
		width: auto;
	}

	.eb_form_header {
		font-size: 24px;
	}

	.inputbox {
		border: 1px solid #ccc;
		border-radius: 3px;
		font-size: 14px;
		padding: 4px 6px;		
	}

	.default_image_box {
		height: 500px;
		width: 250px;
		overflow-y: scroll;
	}

	.default_image_preview {
		max-width: 200px;
		margin: 4px;
	}

	.default_image_preview:hover {
		box-shadow: 4px 4px 4px #999;
	}

	.default_image_selected {		
		background-color: rgb(255,0,0);
	}	
</style>

<script src="components/com_eventbooking/assets/js/events.js?v=2" type="text/javascript"></script>

<form action="index.php?option=com_eventbooking&view=event" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form form-horizontal" onsubmit="return validateForm()">
	<div id="eb-submit-event-simple" class="row-fluid container">
		<div class="eb_form_header" style="width:100%;">
			<div style="float: left; width: 40%;"><?php echo JText::_('EB_ADD_EDIT_EVENT'); ?></div>
			<div style="float: right; width: 50%; text-align: right;">
				<input id="save_button" type="submit" name="btnSave" value="<?php echo JText::_('EB_SAVE'); ?>" class="btn btn-primary" />
				<input id="cancel_button" type="button" name="btnCancel" value="<?php echo JText::_('EB_CANCEL_EVENT'); ?>" onclick="cancelEvent();" class="btn btn-primary" />
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_TITLE') ; ?></label>
			<div class="<?php echo $controlsClass; ?>">
				<input required="true" type="text" name="title" value="<?php echo $this->item->title; ?>" class="validate[required] input-xlarge" size="70" />
			</div>
		</div>

		<?php /*
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_ALIAS') ; ?></label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="alias" value="<?php echo $this->item->alias; ?>" class="input-xlarge" size="70" />
			</div>			
		</div>
		*/ ?>

		<?php if ($this->lists['main_category_id']) { ?>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_MAIN_EVENT_CATEGORY') ; ?></label>
				<div class="<?php echo $controlsClass; ?>">
					<div style="float: left;"><?php echo $this->lists['main_category_id'] ; ?></div>
				</div>
			</div>
		<?php } ?>

		<?php if ($this->lists['category_id']) { ?>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_ADDITIONAL_CATEGORIES') ; ?></label>
				<div class="<?php echo $controlsClass; ?>">
					<div style="float: left;"><?php echo $this->lists['category_id'] ; ?></div>
					<div style="float: left; padding-top: 25px; padding-left: 10px;"><?php echo AxsLanguage::text("AXS_CTRL_TO_SELECT_MULTIPLE_CATEGORIES", "Press <strong>Ctrl</strong> to select multiple categories") ?></div>
				</div>
			</div>
		<?php } ?>


		<?php 

			if ($default_image_list) {
	 			if ($params->thumbnail_image_type == 'default') { 
					$defaultSelect = "selected"; 
					$defaultStyle = "";
					$uploadStyle = "style='display:none;'";
				} else {
					$uploadSelect = "selected";
					$uploadStyle = "";
					$defaultStyle = "style='display:none;'";
				}

				$imageSelectStyle = "";
			} else {
				$imageSelectStyle = "style='display:none;'";
				$defaultStyle = "style='display:none;'";
				$defaultSelect = "";
				$uploadSelect = "selected";
			}
		?>

		<div class="<?php echo $controlGroupClass; ?>" <?php echo $imageSelectStyle; ?>>
			<label class="<?php echo $controlLabelClass; ?>"><?php echo "Thumbnail Image Type";//JText::_('EB_ADDITIONAL_CATEGORIES') ; ?></label>
			<div class="<?php echo $controlsClass; ?>">
				<div style="float: left;">
					<select id="thumbnail_image_type" name="thumbnail_image_type">
						<option value="upload" <?php echo $uploadSelect; ?>><?php echo AxsLanguage::text("AXS_UPLOAD", "Upload") ?></option>
						<option value="default" <?php echo $defaultSelect; ?>><?php echo AxsLanguage::text("JDEFAULT", "Default") ?></option>
					</select>
				</div>
			</div>
		</div>

		<div id="default_image_select" class="image_select <?php echo $controlGroupClass; ?>" <?php echo $defaultStyle;?>>
			<label class="<?php echo $controlLabelClass; ?>"><?php echo "Select Default Image";//JText::_('EB_ADDITIONAL_CATEGORIES') ; ?></label>
			<input id="default_image" type="hidden" name="default_image" value="<?php echo $params->default_image;?>"/>
			<div id="default_image_list" class="default_image_box">
				<?php

					if ($params->default_image) {
						$image_parts = explode("/", $params->default_image);
						$default_image = $image_parts[count($image_parts) - 1];
					}


					
					foreach ($default_image_list as $image) {

						$image_parts = explode("/", $image);
						$image_name = $image_parts[count($image_parts) - 1];

						if ($image_name == $default_image) {
							$selected = true;
						} else {
							$selected = false;
						}

						?>
							<div <?php if ($selected) { echo "class='default_image_selected'"; } ?>>
								<img src="<?php echo $image;?>" value="<?php echo $image_name;?>" class="default_image_preview"><br>
							</div>
						<?php
					}
				?>
			</div>
		</div>

		<div id="upload_image_select" class="image_select <?php echo $controlGroupClass; ?>" <?php echo $uploadStyle;?>>
			<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_THUMB_IMAGE') ; ?></label>
			<div class="<?php echo $controlsClass; ?>">
				
				<?php
				
					if ($this->item->thumb)  {

						$main_image = JURI::root() . AxsImages::getImagesPath('events') . '/images/' . $this->item->thumb;
						$thumb = JURI::root() . AxsImages::getImagesPath('events') . '/images/thumbs/' . $this->item->thumb;
					}
				?>

				<input 
					id="thumb_image" 
					type="file"
					accept="image/*" 
					<?php 
						if (!$main_image) {
							//A file already exists, so it doesn't need to be uploaded.
					?>
							required="true"
					<?php
						}
					?> 
					name="thumb_image"
					size="60"
				/>
			</div>
			<img id="thumb_preview" style="margin: 10px; max-width: 500px; float: left;" src="<?php echo $main_image;?>"/>
		</div>
		
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_LOCATION') ; ?></label>
			<div class="<?php echo $controlsClass; ?>">
				<?php
				echo $this->lists['location_id'];
				
				if (JFactory::getUser()->authorise('eventbooking.addlocation', 'com_eventbooking')) {
					?>
					<button type="button" class="btn btn-small btn-success eb-colorbox-addlocation" href="<?php echo JRoute::_('index.php?option=com_eventbooking&view=location&layout=popup&tmpl=component&Itemid='.$this->Itemid)?>"><span class="icon-new icon-white"></span><?php echo JText::_('EB_ADD_NEW_LOCATION') ; ?></button>
					<?php
				}
				?>
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo JText::_('EB_REGISTRATION_TIME_ZONE'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<?php
					$timezone = empty($params->time_zone) ? 'America/Boise' : $params->time_zone;
					echo AxsTimezones::getTimezoneList($timezone);
				?>
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo JText::_('EB_EVENT_START_DATE'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<?php echo JHtml::_('calendar', ($this->item->event_date == $this->nullDate) ? '' : JHtml::_('date', $this->item->event_date, $format, null), 'event_date', 'event_date', '%Y-%m-%d', array('class' =>  'validate[required]')) ; ?>
				<?php echo $this->lists['event_date_hour'] . ' ' . $this->lists['event_date_minute']; ?>
			</div>
		</div>
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo JText::_('EB_EVENT_END_DATE'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<?php echo JHtml::_('calendar', ($this->item->event_end_date == $this->nullDate) ? '' : JHtml::_('date', $this->item->event_end_date, $format, null), 'event_end_date', 'event_end_date') ; ?>
				<?php echo $this->lists['event_end_date_hour'] . ' ' . $this->lists['event_end_date_minute']; ?>
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo JText::_('EB_REGISTRATION_START_DATE'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<?php echo JHtml::_('calendar', ($this->item->registration_start_date == $this->nullDate) ? '' : JHtml::_('date', $this->item->registration_start_date, $format, null), 'registration_start_date', 'registration_start_date') ; ?>
				<?php echo $this->lists['registration_start_hour'] . ' ' . $this->lists['registration_start_minute']; ?>
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo JText::_('EB_PRICE'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="number" name="individual_price" id="individual_price" step="0.01" style="height: 30px" class="input-mini" size="10" value="<?php echo $this->item->individual_price; ?>" />
			</div>
		</div>
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo AxsLanguage::text('EB_PRICE_TEXT', 'Custom Pricing Text'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="price_text" id="price_text" value="<?php echo $this->item->price_text; ?>" />
			</div>
		</div>
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<span class="editlinktip hasTip" title="<?php echo JText::_( 'EB_EVENT_CAPACITY' );?>::<?php echo JText::_('EB_CAPACITY_EXPLAIN'); ?>"><?php echo JText::_('EB_CAPACITY'); ?></span>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="event_capacity" id="event_capacity" class="input-mini" size="10" value="<?php echo $this->item->event_capacity; ?>" />
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo AxsLanguage::text('EB_MORE_EXTERNAL_URL', "External Website"); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="website" class="inputbox" size="50" value="<?php if (isset($params->website)) { echo $params->website; } ?>" />
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo AxsLanguage::text('EB_MORE_INFO_TEXT', "More Info Text"); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="external_url_text" class="inputbox" size="50" value="<?php if (isset($params->external_url_text)) { echo $params->external_url_text; } ?>" />
			</div>
		</div>

		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo AxsLanguage::text('EB_MORE_INFO_URL', "More Info URL"); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<input type="text" name="external_url" class="inputbox" size="50" value="<?php if (isset($params->external_url)) { echo $params->external_url; } ?>" />
			</div>
		</div>

		<?php if ($isAdmin) { ?>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>"><?php echo JText::_('EB_REGISTRATION_TYPE'); ?></label>
				<div class="<?php echo $controlsClass; ?>">
					<?php echo $this->lists['registration_type'] ; ?>
				</div>
			</div>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<span class="editlinktip hasTip" title="<?php echo JText::_( 'EB_CUT_OFF_DATE' );?>::<?php echo JText::_('EB_CUT_OFF_DATE_EXPLAIN'); ?>"><?php echo JText::_('EB_CUT_OFF_DATE') ; ?></span>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<?php echo JHtml::_('calendar', ($this->item->cut_off_date == $this->nullDate) ? '' : JHtml::_('date', $this->item->cut_off_date, $format, null), 'cut_off_date', 'cut_off_date') ; ?>
				</div>
			</div>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<?php echo JText::_('EB_NOTIFICATION_EMAILS'); ?>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<input type="text" name="notification_emails" class="inputbox" size="70" value="<?php echo $this->item->notification_emails ; ?>" />
				</div>
			</div>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<?php echo JText::_('EB_PAYPAL_EMAIL'); ?>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<input type="text" name="paypal_email" class="inputbox" size="50" value="<?php echo $this->item->paypal_email ; ?>" />
				</div>
			</div>
			<?php
			if ($this->config->event_custom_field)
			{
				foreach ($this->form->getFieldset('basic') as $field)
				{
				?>
					<div class="<?php echo $controlGroupClass; ?>">
						<label class="<?php echo $controlLabelClass; ?>">
							<?php echo $field->label;?>
						</label>
						<div class="<?php echo $controlsClass; ?>">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php
				}
			}
			?>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<span class="editlinktip hasTip" title="<?php echo JText::_( 'EB_ACCESS' );?>::<?php echo JText::_('EB_ACCESS_EXPLAIN'); ?>"><?php echo JText::_('EB_ACCESS'); ?></span>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<?php echo $this->lists['access']; ?>
				</div>
			</div>
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<span class="editlinktip hasTip" title="<?php echo JText::_( 'EB_REGISTRATION_ACCESS' );?>::<?php echo JText::_('EB_REGISTRATION_ACCESS_EXPLAIN'); ?>"><?php echo JText::_('EB_REGISTRATION_ACCESS'); ?></span>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<?php echo $this->lists['registration_access']; ?>
				</div>
			</div>

			<?php
				if (EventbookingHelper::canChangeEventStatus($this->item->id)) {
			?>
					<div class="<?php echo $controlGroupClass; ?>">
						<label class="<?php echo $controlLabelClass; ?>">
							<?php echo JText::_('EB_PUBLISHED'); ?>
						</label>
						<?php echo $this->lists['published']; ?>
					</div>
			<?php
				}
			?>
			
			<div class="<?php echo $controlGroupClass; ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<?php echo  JText::_('EB_SHORT_DESCRIPTION'); ?>
				</label>
				<div class="<?php echo $controlsClass; ?>">
					<?php echo $editor->display( 'short_description',  $this->item->short_description , '100%', '180', '90', '6' ) ; ?>
				</div>
			</div>

		<?php } ?>
		<div class="<?php echo $controlGroupClass; ?>">
			<label class="<?php echo $controlLabelClass; ?>">
				<?php echo  JText::_('EB_DESCRIPTION'); ?>
			</label>
			<div class="<?php echo $controlsClass; ?>">
				<?php echo $editor->display( 'description',  $this->item->description , '100%', '250') ; ?>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		Eb.jQuery(document).ready(function($){
			$("#adminForm").validationEngine('attach', {
				onValidationComplete: function(form, status){
					if (status == true) {
						form.on('submit', function(e) {
							e.preventDefault();
						});
						return true;
					}
					return false;
				}
			});
		})

		function cancelEvent()
		{
			location.href   ="<?php echo JRoute::_('index.php?option=com_eventbooking&task=event.cancel&Itemid=' . $this->Itemid, false); ?>";
		}

		function validateForm() {
			var save_button = document.getElementById("save_button");
			var cancel_button = document.getElementById("cancel_button");
			save_button.disabled = true;
			cancel_button.disabled = true;

			if (!thumbnail_type) {
				save_button.disabled = false;
				cancel_button.disabled = false;

				return false;
			}


			switch (thumbnail_type.value) {
				case "upload":
					break;
				case "default":
					var selected = default_list.getElementsByClassName("default_image_selected");
					if (selected.length == 0) {
						alert("<?php echo AxsLanguage::text("AXS_PLEASE_SELECT_DEFAULT_PHOTO", "Please select a default photo.") ?>");

						save_button.disabled = false;
						cancel_button.disabled = false;

						return false;
					} else {
						return true;
					}
					break;
			}

			return true;
		}
	</script>
	<input type="hidden" name="option" value="com_eventbooking" />
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="task" value="save" />
	<input type="hidden" name="return" value="<?php echo $this->return; ?>" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>

<style>
	.container .form-group textarea#description {
		width: 100% !important;
	}
</style>
