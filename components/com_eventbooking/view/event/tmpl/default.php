<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */

// no direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_splms/assets/css/course.css');

EventbookingHelperJquery::colorbox('a.eb-modal');
$brand = AxsBrands::getBrand();

$item = $this->item;
$params = json_decode($item->params);
$url = JRoute::_(EventbookingHelperRoute::getEventRoute($item->id, 0, $this->Itemid), false);
$canRegister = EventbookingHelper::acceptRegistration($item);

$socialUrl = JUri::getInstance()->toString(array('scheme', 'user', 'pass', 'host')).JRoute::_(EventbookingHelperRoute::getEventRoute($item->id, 0, $this->Itemid));

if ($this->config->use_https) {
	$ssl = 1;
} else {
	$ssl = 0;
}

/* @var EventbookingHelperBootstrap $bootstrapHelper*/
$bootstrapHelper   = $this->bootstrapHelper;
$iconPencilClass   = $bootstrapHelper->getClassMapping('icon-pencil');
$iconOkClass       = $bootstrapHelper->getClassMapping('icon-ok');
$iconRemoveClass   = $bootstrapHelper->getClassMapping('icon-remove');
$iconDownloadClass = $bootstrapHelper->getClassMapping('icon-download');
$btnClass          = $bootstrapHelper->getClassMapping('btn');
$return = base64_encode(JUri::getInstance()->toString());

if ($item->cut_off_date != JFactory::getDbo()->getNullDate()) {
	$registrationOpen = ($item->cut_off_minutes < 0);
} else {
	$registrationOpen = ($item->number_event_dates > 0);
}

$isMultipleDate = false;

if ($this->config->show_children_events_under_parent_event && $item->event_type == 1) {
	$isMultipleDate = true;
}

$offset = JFactory::getConfig()->get('offset');

$cover_image = $brand->default_settings->event_cover_image;

if (($eventParams->thumbnail_image_type == 'upload' && $item->thumb) || (!$eventParams->thumbnail_image_type && $item->thumb)) {
	if ($this->config->get('show_image_in_event_detail', 1) && $this->config->display_large_image && $item->image && file_exists(JPATH_ROOT . '/' . $item->image)) {

		$cover_image = JUri::base(true) . '/' . $item->image;

	} elseif ($this->config->get('show_image_in_event_detail', 1) && $item->thumb && file_exists(JPATH_ROOT . '/'.AxsImages::getImagesPath('events').'/images/' . $item->thumb)) {

		$cover_image = JUri::base(true).'/'.AxsImages::getImagesPath('events').'/images/'.$item->thumb;

	}

} else if ($params->thumbnail_image_type == 'default' && $params->default_image) {

		$cover_image = JUri::base(true) . '/' .  AxsImages::getImagesPath('events') . '/images/defaults/' . $params->default_image;
}

?>
<style>
	body {
		background-color: #eee;
	}
	.col_event_static {
		width: 115px;
	}
  	.box {
	    background-color: white;
	    color: black;
	    padding: 35px;
	    margin: 15px 0px;
	    box-shadow: 0 1px 3px rgba(34,25,25,0.4);
  	}

	.subheader {
		font-size: 20px;
		margin-top: 5px;
		margin-bottom: 10px;
		font-weight: bold;
		color: #555;
	}

	.subtitle {
		font-size: 20px;
		margin-bottom: 15px;
		font-weight: bold;
	}

	.required {
		color: red;
	}

	.overlay {
		width: 100%;
		height: 100%;
		position: fixed;
		top: 0;
		left: 0;
		background-color: darkgray;
		opacity: 0.85;
		z-index: 20000;
	}

	.price {
		font-size: 30px;
		padding: 15px;
		text-align: center;
		background: #333333;
		color: #FFFFFF;
		margin-bottom: 15px;
		border-radius: 4px;
		-moz-border-radius: 4px;
		-webkit-border-radius: 4px;
	}

	.no-padding {
		padding: 0px;
	}
	.cover-image {
		background-repeat: no-repeat;
		background-position: center;
		background-size: cover;
		width: 100%;
		height: 300px;
	}
	@media(max-width: 768px) {
		.cover-image {
			height: 200px;
		}
	}
	@media(max-width: 991px) {
		.title-spacer {
			margin-top: 100px;
		}
	}
</style>

<?php
	if(!$brand->site_details->tovuti_root_styles) {
		$add_title_spacer_class = true;
	} else {
		$add_title_spacer_class = false;
	}
?>
<div class="container">
	<div id="eb-event-page" class="container <?php if($add_title_spacer_class) { echo "title-spacer"; } ?> eb-category-<?php echo $item->category_id; ?>eb-event<?php if ($item->featured) echo ' eb-featured-event'; ?>" itemscope itemtype="http://schema.org/Event">
		<div class="clearfix col-md-12">
			<h1 class="eb-page-heading">
				<span itemprop="name"><?php echo $item->title; ?></span>
			</h1>
			<div class="subheader" style="float:left;">
			      <?php echo !empty($this->location->city) ? $this->location->city : ''; ?>
			      <?php echo !empty($this->location->state) ? ', '.$this->location->state : ''; ?>
		    </div>
		    <div class="subheader" style="float:right;">
			     <?php
			     if($item->event_date != '0000-00-00 00:00:00') {
			      	echo EventbookingHelperEventlist::getDateSpan($item->id);
			      }
			      ?>
			</div>
		</div>
		<div class="eb-description-details clearfix col-md-12">
			<?php
				if ($cover_image) {
			?>
					<div class="col-md-12 box cover-image" style="background-image: url('<?php echo $cover_image; ?>');">
						<?php if ($this->location != null): ?>
							<div style="width:100%; padding: 10px; bottom: 0px; left: 0px;  position:absolute;  background-color: rgba(0, 1, 1, 0.7);">
								<div class="course-title pull-left" style="color:#fff; font-size:26px;"><?php echo $this->location->name; ?></div>
							</div>
						<?php endif; ?>
					</div>
			<?php
				}
			?>
		</div>

		<div class="col-md-12">
			<?php if($item->description) { ?>
			<div class="box">
				<?php echo $item->description; ?>
			</div>
			<?php } ?>
		</div>
		<?php if($this->location->city || $this->location->state) { ?>
	 	<div class="col-md-12">
	 	<?php } else { ?>
	 	<div class="col-md-12">
	 	<?php } ?>
			<div class="box">

				<?php
					// Facebook, twitter, Gplus share buttons
					if ($this->config->show_fb_like_button) {
						//add this share function after redesign
						//echo $this->loadTemplate('share');
					}
				?>

				<h3 id="eb-event-properties-heading"><?php echo AxsLanguage::text("EB_DETAILS", "Details"); ?></h3>

				<div id="eb-event-info" class="clearfix">
					<?php
					if (!empty($this->items)) {
						echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/events_children.php', array('items' => $this->items, 'config' => $this->config, 'Itemid' => $this->Itemid, 'nullDate' => $this->nullDate, 'ssl' => $ssl, 'viewLevels' => $this->viewLevels, 'categoryId' => $this->item->category_id, 'bootstrapHelper' => $this->bootstrapHelper));
					} else {
						$leftCssClass = 'span8';

						if (empty($this->rowGroupRates)) {
							$leftCssClass = 'span12';
						}

						$layoutData = array(
							'item'           => $this->item,
							'config'         => $this->config,
							'itemParams'	 => $params,
							/*'location'       => isset($this->location) ? $this->location : null,
							'showLocation'   => true,*/
							'isMultipleDate' => false,
							'nullDate'       => $this->nullDate,
							'Itemid'         => $this->Itemid,
						);

						echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/event_properties.php', $layoutData);

						if ($item->activate_waiting_list == 2) {
							$activateWaitingList = $this->config->activate_waitinglist_feature;
						} else {
							$activateWaitingList = $item->activate_waiting_list;
						}

						if (($item->event_capacity > 0) && ($item->event_capacity <= $item->total_registrants) && $activateWaitingList && !@$item->user_registered && $registrationOpen) {
							$waitingList = true;
						} else {
							$waitingList = false;
						}

						if (!$canRegister && $item->registration_type != 3 && $this->config->display_message_for_full_event && !$waitingList && $item->registration_start_minutes >= 0) {
							if (@$item->user_registered) {
								$msg = JText::_('EB_YOU_REGISTERED_ALREADY');
							} elseif (!in_array($item->registration_access, $this->viewLevels)) {
								$loginLink = 'index.php?option=com_users&view=login&return=' . base64_encode(JUri::getInstance()->toString());
								$msg       = str_replace('[LOGIN_LINK]', $loginLink, JText::_('EB_LOGIN_TO_REGISTER'));
							} else {
								$msg = JText::_('EB_NO_LONGER_ACCEPT_REGISTRATION');
							}
							?>
								<div class="text-info eb-notice-message"><?php echo $msg; ?></div>
							<?php
						}

						if (count($this->rowGroupRates)) {
							echo $this->loadTemplate('group_rates');
						}
					}
					?>
				</div>
				<div class="clearfix"></div>

				<?php
					if (!empty($item->ticketTypes)) {
						echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/tickettypes.php', array('ticketTypes' => $item->ticketTypes, 'config' => $this->config));
						?>
							<div class="clearfix"></div>
						<?php
					}

					$ticketsLeft = $item->event_capacity - $item->total_registrants ;

					if ($item->individual_price > 0 || $ticketsLeft > 0) {
					?>
						<div style="display:none;" itemprop="offers" itemscope itemtype="http://schema.org/AggregateOffer">
							<meta itemprop="url" content="<?php echo JUri::getInstance()->toString();?>">
							<?php
							if ($item->individual_price > 0) {
								?>
									<span itemprop="lowPrice"><?php echo EventbookingHelper::formatCurrency($item->individual_price, $this->config, $currency_symbol); ?></span>
								<?php
							}

							if ($ticketsLeft > 0) {
								?>
									<span itemprop="offerCount"><?php echo $ticketsLeft;?></span>
								<?php
							}
							?>
						</div>
					<?php
					}

					if ($this->showTaskBar) {
					?>
						<div class="eb-taskbar clearfix">
							<ul>
								<?php
									$layoutData = array(
										'item'              => $this->item,
										'config'            => $this->config,
										'isMultipleDate'    => $isMultipleDate,
										'canRegister'       => $canRegister,
										'registrationOpen'  => $registrationOpen,
										'waitingList'       => $waitingList,
										'return'            => $return,
										'showInviteFriend'  => true,
										'ssl'               => $ssl,
										'Itemid'            => $this->Itemid,
										'btnClass'          => $btnClass,
										'iconOkClass'       => $iconOkClass,
										'iconRemoveClass'   => $iconRemoveClass,
										'iconDownloadClass' => $iconDownloadClass,
										'iconPencilClass'   => $iconPencilClass,
									);

									echo EventbookingHelperHtml::loadCommonLayout('common/tmpl/buttons.php', $layoutData);
								?>
							</ul>
						</div>
					<?php
					}

					if (count($this->plugins)) {
						echo $this->loadTemplate('plugins');
					}

					if ($this->config->show_social_bookmark) {
						//add this share function after redesign
						//echo $this->loadTemplate('social_buttons', array('socialUrl' => $socialUrl));
					}
				?>
				<div class="clearfix" style="height:30px;"></div>
			</div>
		</div>
		<?php if($this->location->city || $this->location->state) { ?>
		<div class="col-md-12">
			<div class="box">
	        	<h3 id="eb-event-properties-heading">Location</h3>

	        <iframe
	          width="100%"
	          height="300"
	          seamless="seamless"
	          frameborder="0"
	          style="border:0"
	          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC_6XKStjrrBJqzidJyrF6fp_g9u1w_iQ0
	            &q=<?php echo urlencode($this->location->address.','.$this->location->city.','.$this->location->state.','.$this->location->country); ?>" allowfullscreen>
	        </iframe>

	       	<!--
	       	<hr>
	        <span style="font-size: 12pt;"><strong><?php echo $this->location->name; ?></strong></span>
	        <br/>
	        <span style="font-size: 10pt;"><?php echo $this->location->address; ?> <?php echo $this->location->city; ?>, <?php echo $this->location->state; ?> <?php echo $this->location->country; ?></span>
	     	-->
	      </div>
		</div>
		<?php } ?>
		<?php
			//Check if they're exist
			if (isset($params->website) || isset($params->external_url) || isset($params->external_url_text)) {
				//Check if they're set so there isn't a blank box.
				if ($params->website || $params->external_url || $params->external_url_text) {
		?>
					<?php if($this->location->city || $this->location->state) { ?>
				 	<div class="col-md-12">
				 	<?php } else { ?>
				 	<div class="col-md-12">
				 	<?php } ?>
						<div class="box">
							<h3 id="eb-event-properties-heading"><?php echo AxsLanguage::text("AXS_ADDITIONAL_INFO", "Additional Info") ?></h3>
							<table class="table table-bordered table-striped">
								<?php
									if ($params->website) {
										if (strpos($params->website, "http") === 0) {
											$hasHttp = true;
										}

										if (strpos($params->website, "https") === 0) {
											$hasHttps = true;
										}

										if (!$hasHttps && !$hasHttp) {
											$params->website = "http://" . $params->website;
										}
								?>
										<tr>
											<td>
												<a href="<?php echo $params->website;?>">
													<?php echo AxsLanguage::text("COM_EB_COMMON_WEBSITE", "Website") ?>
												</a>
											</td>
										</tr>

								<?php
									}

									if ($params->external_url) {
										$hasHttp = false;
										$hasHttps = false;
										if (strpos($params->external_url, "http") === 0) {
											$hasHttp = true;
										}

										if (strpos($params->external_url, "https") === 0) {
											$hasHttps = true;
										}

										if (!$hasHttps && !$hasHttp) {
											$params->external_url = "http://" . $params->external_url;
										}

								?>
										<tr>
											<td>
												<a href="<?php echo $params->external_url;?>">
													<?php
														if ($params->external_url_text) {
															echo $params->external_url_text;
														} else {
															echo AxsLanguage::text("AXS_MORE_INFO", "More Info");
														}
													?>
												</a>
											</td>
										</tr>
								<?php
									}
								?>
							</table>
						</div>
					</div>

		<?php
				}
			}
		?>

	</div>
</div>

<form name="adminForm" id="adminForm" action="index.php" method="post">
	<input type="hidden" name="option" value="com_eventbooking" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="id" value="" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>

<script language="javascript">
	function cancelRegistration(registrantId) {
		var form = document.adminForm ;
		if (confirm("<?php echo JText::_('EB_CANCEL_REGISTRATION_CONFIRM'); ?>")) {
			form.task.value = 'registrant.cancel' ;
			form.id.value = registrantId ;
			form.submit() ;
		}
	}
</script>