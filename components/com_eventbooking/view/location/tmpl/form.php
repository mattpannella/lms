<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.framework') ;

$bootstrapHelper		= $this->bootstrapHelper;
$controlGroupClass		= $bootstrapHelper->getClassMapping('control-group');
$controlLabelClass		= $bootstrapHelper->getClassMapping('control-label');
$controlsClass			= $bootstrapHelper->getClassMapping('controls');
$span7Class				= $bootstrapHelper->getClassMapping('span7');
$span5Class				= $bootstrapHelper->getClassMapping('span5');

$config = EventbookingHelper::getConfig();
$mapApiKye = $config->get('map_api_key', 'AIzaSyAp7ydJCwvDe68lKTRnklpITFcdKHCDVzs');

if ($this->item->id)
{
	$coordinates = $this->item->lat.','.$this->item->long;
}
else
{
	$http     = JHttpFactory::getHttp();
	$url      = "https://maps.googleapis.com/maps/api/geocode/json?address=" . str_replace(' ', '+', $config->default_country) . "&key=" . $mapApiKye;
	$response = $http->get($url);
	if ($response->code == 200)
	{
		$output_deals = json_decode($response->body);
		$latLng = $output_deals->results[0]->geometry->location;
		$coordinates = $latLng->lat.','.$latLng->lng;
	}
	else
	{
		$coordinates = '37.09024,-95.712891';
	}
}
?>

<style>
	.results_address {
		font-size: 12px;
		cursor: pointer;
		color: gray;
		padding: 5px;
		border: 1px solid rgb(220,220,220);
		width: 300px;

	}

	.results_address:hover {
		background-color: rgb(240,240,240);
	}
</style>

<script src="https://maps.google.com/maps/api/js?key=<?php echo $mapApiKye; ?>" type="text/javascript"></script>
<script type="text/javascript">
	function checkData(pressbutton, button) {

		var form = document.adminForm;

		if (pressbutton == 'cancel') {
			form.task.value = pressbutton;
			form.submit();
			return;
		} else {

			button.disabled = true;

			var failSave = function() {
				form.name.focus();				
				button.disabled = false;
			}

			if (form.name.value == '') {
				alert("<?php echo JText::_('EN_ENTER_LOCATION_NAME'); ?>");
				failSave();
				return;
			}
			if (form.address.value == '') {
				alert("<?php echo JText::_('EN_ENTER_LOCATION_ADDRESS'); ?>");
				failSave();
				return;
			}
			if (form.city.value == '') {
				alert("<?php echo JText::_('EN_ENTER_LOCATION_CITY'); ?>");
				failSave();
				return;
			}
			if (form.zip.value == '') {
				alert("<?php echo JText::_('EN_ENTER_LOCATION_ZIP'); ?>");
				failSave();
				return;
			}
			form.task.value = pressbutton;
			form.submit();
		}
	}

	function deleteLocation() {
		if (confirm("<?php echo JText::_("EB_DELETE_LOCATION_CONFIRM"); ?>"))
		{
			var form = document.adminForm ;
			form.task.value = 'delete';
			form.submit();
		}
	}

	var map;
	var geocoder;
	var marker;

	Joomla.submitbutton = function(pressbutton) {
		var form = document.adminForm;
		if (pressbutton == 'cancel') {
			Joomla.submitform( pressbutton );
			return;
		} else {
			//Should validate the information here
			if (form.name.value == "") {
				alert("<?php echo JText::_('EN_ENTER_LOCATION'); ?>");
				form.name.focus();
				return ;
			}
			Joomla.submitform( pressbutton );
		}
	}

	function fillUserInputFromAddress(full_address) {
		full_address = full_address.split(",");

		var address = "";
		var city = "";
		var state = "";
		var zip = "";
		var country = "";


		if (full_address[0] !== undefined) {
			address = full_address[0].trim();
		}

		if (full_address[1] !== undefined) {
			city = full_address[1].trim();
		}

		if (full_address[2] !== undefined) {
			var state_zip = full_address[2].trim().split(" ");	//the state and zip is returned like "ID 83706" so there is no comma to separate them.
			var state = state_zip[0];
			var zip = state_zip[1];
		}

		if (full_address[3] !== undefined) {
			country = full_address[3].trim();
		}

		if (address === undefined) { address = ""; 	}
		if (city 	=== undefined) { city = ""; 	}
		if (state 	=== undefined) { state = ""; 	}
		if (zip 	=== undefined) { zip = ""; 		}

		document.getElementById("address").value = address;
		document.getElementById("city").value = city;
		document.getElementById("state").value = state;
		document.getElementById("zip").value = zip;
	}

	function initialize() {
		geocoder = new google.maps.Geocoder();
		var mapDiv = document.getElementById('map-canvas');
		// Create the map object
		map = new google.maps.Map(mapDiv, {
				center: new google.maps.LatLng(<?php  echo $coordinates;?>),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				streetViewControl: false
		});
		// Create the default marker icon
		marker = new google.maps.Marker({
			map: map,
			position: new google.maps.LatLng(<?php  echo $coordinates; ?>),
			draggable: true
		});
		// Add event to the marker
		google.maps.event.addListener(marker, 'drag', function() {
			geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						fillUserInputFromAddress(results[0].formatted_address);
						document.getElementById('coordinates').value = marker.getPosition().toUrlValue();
					}
				}
			});
		});
	}

	function getLocationFromAddress() {
		var address = document.getElementById('address').value;
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				marker.setPosition(results[0].geometry.location);
				$('coordinates').value = results[0].geometry.location.lat().toFixed(7) + ',' + results[0].geometry.location.lng().toFixed(7);
			} else {
				alert('We\'re sorry but your location was not found.');
			}
		});
	}

	// Initialize google map
	google.maps.event.addDomListener(window, 'load', initialize);

	// Search for addresses
	function getLocations(term) {
		var content = $('#eventmaps_results');
		address = $('#address').offset();

		$("#eventmaps_results").hide();

		$$('#eventmaps_results div').each(function(el) {
			el.dispose();
		});

		if (term != '') {
			geocoder.geocode( {'address': term }, function(results, status) {

				if (status == 'OK') {					
					results.each(function(item) {
						theli = document.createElement('div');
						theli.className = "results_address";

						theli.addEvent('click', function() {

							$("#eventmaps_results").hide();

							var lat = item.geometry.location.lat().toFixed(7);
							var lng = item.geometry.location.lng().toFixed(7);

							var location = new google.maps.LatLng(lat, lng);
							marker.setPosition(location);
							map.setCenter(location);

							document.getElementById("coordinates").value = lat + "," + lng;							

							var result_text = this.getElementsByClassName("result_text")[0].innerHTML;

							fillUserInputFromAddress(result_text);
						});

						var text = document.createElement('span');
						text.className = "result_text lizicon-location2";
						text.innerHTML = item.formatted_address;
						theli.appendChild(text);

						content.append(theli);
					});					

					$("#eventmaps_results").show();

				}
			});
		}
	}

	function clearLocations() {
		setTimeout( 
			function () {
				$("#eventmaps_results").hide();
			},1000
		);
	}

</script>

<style>
	.mb15 {
		margin-bottom: 15px;
	}
</style>

<h1 class="eb-page-heading"><?php echo JText::_('EB_ADD_EDIT_LOCATION'); ?></h1>
<form action="index.php?option=com_eventbooking&view=location" method="post" name="adminForm" id="adminForm" class="form">
	<div class="row-fluid">
		<div class="<?php echo $span5Class ?>" style="min-width: 500px; padding: 15px;">			
			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_NAME'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="text_area" type="text" name="name" id="name" size="50" maxlength="250" value="<?php echo $this->item->name;?>" />
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_ADDRESS'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="input-xlarge" type="text" name="address" id="address" size="70" autocomplete="off" onkeyup="getLocations(this.value)" onblur="clearLocations();" maxlength="250" value="<?php echo $this->item->address;?>" />
					<?php /*<input class="input-xlarge" type="text" name="address" id="address" size="70" autocomplete="off" maxlength="250" value="<?php echo $this->item->address;?>" />*/?>
					<div id="eventmaps_results" style="display:none;"></div>
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_CITY'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="text_area" type="text" name="city" id="city" size="30" maxlength="250" value="<?php echo $this->item->city;?>" />
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_STATE'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="text_area" type="text" name="state" id="state" size="30" maxlength="250" value="<?php echo $this->item->state;?>" />
				</div>	
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_ZIP'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="text_area" type="text" name="zip" id="zip" size="20" maxlength="250" value="<?php echo $this->item->zip;?>" />
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_COUNTRY'); ?><span class="required">*</span>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<?php echo $this->lists['country'] ; ?>
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_COORDINATES'); ?>
					</label>
				</div>
				
				<div class="col-sm-8 <?php echo $controlsClass; ?>">
					<input class="text_area" type="text" name="coordinates" id="coordinates" size="30" maxlength="250" value="<?php echo $this->item->lat.','.$this->item->long;?>" />
				</div>				
			</div>

			<div class="row-fluid mb15">
				<div class="col-sm-4">
					<label class="<?php echo $controlLabelClass; ?>">
						<?php echo JText::_('EB_PUBLISHED') ; ?>
					</label>
				</div>			
			
				<div class="col-sm-8" style="padding-left: 15px">
		    		<?php echo $this->lists['published']; ?>
		    	</div>			    
			</div>
			
			<div class="mb15">
				<div class="form-actions">
					<input type="button" class="btn btn-primary" name="btnSave" value="<?php echo JText::_('EB_SAVE'); ?>" onclick="checkData('save', this);" />
					<?php
						if ($this->item->id)  {
					?>
							<input type="button" class="btn btn-primary" name="btnSave" value="<?php echo JText::_('EB_DELETE_LOCATION'); ?>" onclick="deleteLocation();" />
					<?php
						}
					?>
					<input type="button" class="btn btn-primary" name="btnCancel" value="<?php echo JText::_('EB_CANCEL_LOCATION'); ?>" onclick="checkData('cancel');" />
				</div>				
			</div>			
		</div>
		<div class="<?php echo $span7Class ?>" style="padding: 15px;">
			<div class="<?php echo $controlGroupClass;  ?>">
				<label class="<?php echo $controlLabelClass; ?>">
					<input type="button" onclick="getLocationFromAddress();" value="<?php echo JText::_('EB_PINPOINT'); ?> &raquo;" />
				</label>
				<div class="clearfix"></div>
				<div>
					<div id="map-canvas" style="width: 95%; height: 400px"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_( 'form.token' ); ?>

</form>