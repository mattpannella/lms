<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
JHtml::_('bootstrap.framework');
JHtml::_('behavior.framework') ;
EventbookingHelperJquery::validateForm();
$bootstrapHelper   = $this->bootstrapHelper;
$controlGroupClass = $bootstrapHelper->getClassMapping('control-group');
$controlLabelClass = $bootstrapHelper->getClassMapping('control-label');
$controlsClass     = $bootstrapHelper->getClassMapping('controls');
$span7Class          = $bootstrapHelper->getClassMapping('span7');
$span5Class          = $bootstrapHelper->getClassMapping('span5');

$config = EventbookingHelper::getConfig();
$mapApiKey = $config->get('map_api_key', 'AIzaSyAp7ydJCwvDe68lKTRnklpITFcdKHCDVzs');
$http     = JHttpFactory::getHttp();
$url      = "https://maps.googleapis.com/maps/api/geocode/json?address=" . str_replace(' ', '+', $config->default_country) . "&key=" . $mapApiKey;
$response = $http->get($url);

$default = '37.09024,-95.712891';

if ($response->code == 200)
{
	$output_deals = json_decode($response->body);
	$latLng = $output_deals->results[0]->geometry->location;

	if ($latLng && isset($latLng->lat) && isset($latLng->lng)) {
		$coordinates = $latLng->lat.','.$latLng->lng;
	} else {
		$coordinates = $default;
	}
}
else
{
	$coordinates = $default;
}
?>

<style>
	.results_address {
		font-size: 12px;
		cursor: pointer;
		color: gray;
		padding: 5px;
		border: 1px solid rgb(220,220,220);
		width: 300px;

	}

	.results_address:hover {
		background-color: rgb(240,240,240);
	}
</style>

<script src="https://maps.google.com/maps/api/js?key=<?php echo $mapApiKey; ?>" type="text/javascript"></script>
<script type="text/javascript">

	function fillUserInputFromAddress(full_address) {
		full_address = full_address.split(",");

		var address = "";
		var city = "";
		var state = "";
		var zip = "";
		var country = "";


		if (full_address[0] !== undefined) {
			address = full_address[0].trim();
		}

		if (full_address[1] !== undefined) {
			city = full_address[1].trim();
		}

		if (full_address[2] !== undefined) {
			var state_zip = full_address[2].trim().split(" ");	//the state and zip is returned like "ID 83706" so there is no comma to separate them.
			var state = state_zip[0];
			var zip = state_zip[1];
		}

		if (full_address[3] !== undefined) {
			country = full_address[3].trim();
		}

		if (address === undefined) { address = ""; 	}
		if (city 	=== undefined) { city = ""; 	}
		if (state 	=== undefined) { state = ""; 	}
		if (zip 	=== undefined) { zip = ""; 		}

		document.getElementById("address").value = address;
		document.getElementById("city").value = city;
		document.getElementById("state").value = state;
		document.getElementById("zip").value = zip;
	}

	var map;
	var geocoder;
	var marker;

	function initialize() {
		geocoder = new google.maps.Geocoder();
		var mapDiv = document.getElementById('map-canvas');
		// Create the map object
		map = new google.maps.Map(mapDiv, {
				center: new google.maps.LatLng(<?php echo $coordinates; ?>),
				zoom: 10,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				streetViewControl: false
		});
		// Create the default marker icon
		marker = new google.maps.Marker({
			map: map,
			position: new google.maps.LatLng(<?php echo $coordinates; ?>),
			draggable: true
		});
		// Add event to the marker
		google.maps.event.addListener(marker, 'drag', function() {
			geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (results[0]) {
						fillUserInputFromAddress(results[0].formatted_address);
						document.getElementById('coordinates').value = marker.getPosition().toUrlValue();
					}
				}
			});
		});
	}
	function getLocationFromAddress() {
		var address = document.getElementById('address').value;
		geocoder.geocode( { 'address': address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				marker.setPosition(results[0].geometry.location);
				$('coordinates').value = results[0].geometry.location.lat().toFixed(7) + ',' + results[0].geometry.location.lng().toFixed(7);
			} else {
				alert('We\'re sorry but your location was not found.');
			}
		});
	}
	// Initialize google map
	google.maps.event.addDomListener(window, 'load', initialize);
	// Search for addresses
	function getLocations(term) {
		
		var content = jQuery('#eventmaps_results');
		var address = jQuery("#address");

		jQuery('#eventmaps_results').hide();

		$$('#eventmaps_results div').each(function(el) {
			el.dispose();
		});

		if (term != '') {
			geocoder.geocode( {'address': term }, function(results, status) {
				if (status == 'OK') {
					results.each(function(item) {

						theli = document.createElement('div');
						theli.className = "results_address";

						theli.addEvent('click', function() {

							jQuery("#eventmaps_results").hide();
							var lat = item.geometry.location.lat().toFixed(7);
							var lng = item.geometry.location.lng().toFixed(7);
							
							var location = new google.maps.LatLng(lat, lng);
							marker.setPosition(location);
							map.setCenter(location);

							document.getElementById("coordinates").value = lat + "," + lng;

							var result_text = this.getElementsByClassName("result_text")[0].innerHTML;

							fillUserInputFromAddress(result_text);
							
						});

						var text = document.createElement('span');
						text.className = "result_text lizicon-location2";
						text.innerHTML = item.formatted_address;
						theli.appendChild(text);

						content.append(theli);
					});
					jQuery('#eventmaps_results').show();
				}
			});
		}
	}
	function clearLocations() {
		setTimeout( function () {
			jQuery('#eventmaps_results').hide();
		},1000);
	}
</script>
<form action="index.php?option=com_eventbooking&view=location" method="post" name="adminForm" id="adminForm" class="form-horizontal">
	<h1 class="eb-page-heading"><?php echo JText::_('EB_ADD_EDIT_LOCATION'); ?></h1>
	<div class="row-fluid">
	    <div  class="<?php echo $span5Class ?>">
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_NAME'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<input class="input-large validate[required]" type="text" name="name" id="name" size="50" maxlength="250" value="" />
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_ADDRESS'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	      	         <input class="input-large validate[required]" type="text" name="address" id="address" size="70" autocomplete="off" onkeyup="getLocations(this.value)" onblur="clearLocations();" maxlength="250" value="<?php echo $this->item->address;?>" />
	    			<ul id="eventmaps_results" style="display:none;"></ul>
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_CITY'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<input class="input-large validate[required]" type="text" name="city" id="city" size="30" maxlength="250" value="" />
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_STATE'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<input class="text_area" type="text" name="state" id="state" size="30" maxlength="250" value="" />
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_ZIP'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<input class="input-large validate[required]" type="text" name="zip" id="zip" size="20" maxlength="250" value="" />
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_COUNTRY'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<?php echo $this->lists['country'] ; ?>
	    		</div>
	    	</div>
	    	<div class="clearfix"></div>
	    	<div class="<?php echo $controlGroupClass;  ?>">
	    		<label class="<?php echo $controlLabelClass; ?>">
	    			<?php echo JText::_('EB_COORDINATES'); ?>
	    			<span class="required">*</span>
	    		</label>
	    		<div class="<?php echo $controlsClass; ?>">
	    			<input class="input-large validate[required]" type="text" name="coordinates" id="coordinates" size="30" maxlength="250" value="" />
	    		</div>
	    	</div>
		    <div class="<?php echo $controlGroupClass;  ?>">
			    <label class="<?php echo $controlLabelClass; ?>">
				    <?php echo JText::_('EB_PUBLISHED') ; ?>
			    </label>
			    <?php echo $this->lists['published']; ?>
		    </div>
		</div>
		<div class="<?php echo $span7Class ?>">
			<div class="<?php echo $controlGroupClass;  ?>">
				<div id="map-canvas" style="width: 95%; height: 350px"></div>
				<br>
			</div>
		</div>
		<div class="row-fluid">
			<button id="save_location" class="btn btn-primary" type="submit"><span class="icon-save"></span><?php echo JText::_('EB_SAVE'); ?></button>
			<input type="button" class="btn btn-info" onclick="getLocationFromAddress();" value="<?php echo JText::_('EB_PINPOINT'); ?> &raquo;" />
		</div>
	</div>
	<div class="clearfix"></div>
	<input type="hidden" name="published" value="1" />
	<input type="hidden" name="Itemid" value="<?php echo $this->Itemid; ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
</form>
<script type="text/javascript">
	(function($){
		$(document).ready(function(){
			$("#adminForm").validationEngine('attach', {
				onValidationComplete: function(form, status) {

					if (status == true) {
						$.ajax({
							type:'POST',
							data: $('#adminForm input[type=\'radio\']:checked, #adminForm input[type=\'checkbox\']:checked, #adminForm input[type=\'text\'], #adminForm input[type=\'hidden\'],  #adminForm select'),
							dataType: 'json',
							url: siteUrl + 'index.php?option=com_eventbooking&task=location.save_ajax',
							beforeSend: function () {
								$('#save_location').prop('disabled',true);
							},
							success : function(json){
								/*$( "#adminForm" ).before( '<div class="alert alert-success"><h4 class="alert-heading">Message</h4><div class="alert-message">'+json['message']+'</div></div>' );*/
								
								var parentJQuery = parent.jQuery;

								//document.getElementById("save_location").removeAttribute("disabled");								

								parentJQuery('#location_id').append(parentJQuery('<option>', {
									value: json['id'],
									text: json['name']
								}));

								parentJQuery('#location_id').val(json['id']);
								
								//parentJQuery.colorbox.close();
								parentJQuery("#cboxClose").click();
								
								
							}
						})						

						return false;
					}

					document.getElementById("save_location").removeAttribute("disabled");

					return false;
				}
			});
		})
	})(jQuery)
</script>