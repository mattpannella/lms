<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once JPATH_ROOT . '/components/com_eventbooking/helper/eventlist.php';

//If $module_settings exists, then it's from a module.  If not, the regular events page.
$blockId = "block-container";
$blockGridStyle = '';
if ($module_settings) {
	$moduleId = $module->id;
	$blockId = "block-$moduleId";
	$event_category = $module_settings->event_category;
	$featured_events_only = $module_settings->featured_events_only;
	$num_to_show = $module_settings->num_to_show;
	$blocksit_columns = $module_settings->blocksit_columns;
	$show_previous = $module_settings->show_previous;
	$show_title_bar = false;
	$show_search = false;
	$load_javascript = $module_settings->load_javascript;

	if ($module_settings->show_title_bar || $module_settings->showtitle) {
		$blockGridStyle = 'margin-bottom:77px';
	}

	if ($module_settings->show_title_bar && $module_settings->showtitle) {
		$blockGridStyle = 'margin-bottom:100px';
	}

	if ($blocksit_columns > $num_to_show) {
		$blocksit_columns = $num_to_show;
	}

	$search = [
		"cat" => $event_category,
		"title" => "",
		"desc" => "",
		"prev" => $show_previous,
		"feat" => $featured_events_only,
		"lim" => $num_to_show
	];

	$search = base64_encode(json_encode($search));


} else {
	$event_category = null;
	$featured_events_only = false;
	$num_to_show = -1;
	$blocksit_columns = -1;
	$show_previous = $default;
	$show_title_bar = true;
	$show_search = true;
	$load_javascript = true;
}

$model = new EventbookingHelperEventlist;
$items = $model->getItemList($search, true);
$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}
$categoryList = $model->getCategorySelectList($model->catId);

if ($model->catData->image) {
	$coverImage = $model->catData->image;

} else {
	$coverImage = $brand->default_settings->event_category_cover_image;
}

if($model->catData->name) {
	$title = $model->catData->name;
} else {
	if ($model->viewType) {
		$title = AxsLanguage::text('COM_EB_AXS_MY_EVENTS', 'My Events');
	} else {
		$title = AxsLanguage::text('COM_EB_AXS_EVENTS', 'Events');
	}
}

?>

<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">


<link href="/templates/axs/bootstrap-utilities/bootstrap-utilities.css" rel="stylesheet">
<link rel="stylesheet" href="/components/com_axs/assets/css/media_v2.css">
<script src="https://kit.fontawesome.com/f06d0be547.js" crossorigin="anonymous"></script>
<style>
	:root {
    --bs-font-sans-serif: 'Inter', "Helvetica Neue", Arial, "Noto Sans", "Liberation Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
}

.fs-7 {
    font-size: 0.8rem!important;
}

.tov-content-container {
    width: auto;
    padding: 0 !important;
    font-family: var(--bs-font-sans-serif);
}

.tov-ml-content {
    width: 100%;
}

.ml-card {
    max-width: 100% !important;
}

@media (min-width: 768px) {
    .ml-card {
        max-width: 48.6% !important;
    }
}

@media (min-width: 992px) {
    .ml-card {
        max-width: 49% !important;
    }
}

@media (min-width: 1200px) {
    .ml-card {
        max-width: 23.98% !important;
    }
}

@media (min-width: 1400px) {
    .ml-card {
        max-width: 24.28% !important;
    }
}

.ml-card-image {
    background-image: url(../img/placeholders/diversity-training-header.jpg);
    background-size: cover;
    background-position: center center;
    min-height: 200px;
}

.tov-page-header {

    background-size: cover;
    background-position: center center;
    min-height: 250px;
    position: relative;
}

@media (max-width: 991.98px) {
    .tov-page-header {
        min-height: auto;
    }
}

.tov-page-header-mask {
    background-color: rgba(0, 0, 0, 0.6);
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    background-attachment: fixed;
}
#searchEvents input, #searchEvents select {
	height: 38px !important;
}
.badge {
	font-size: inherit !important;
    font-weight: inherit !important;
	border-radius: 2px !important;
    padding: 8px !important;
}
.event-title {
	font-size: 20px;
	margin-bottom: 15px;
	margin-top: 15px;
}
.ml-card-image .p-2 {
	padding-top: 0px !important;
    padding-bottom: 0px !important;
    padding: 4px !important;
	padding-left: 8px !important;
}

.btn .badge {
    top: 0px !important;
}

.ml-card .btn {
	background: #f5f5f5;
}

.event-date {
	background-color: var(--primary-color) !important;
}

</style>

<div class="tov-content-container container-fluid">
<?php if ($show_title_bar) { ?>
	<div name="Page Heading" class="tov-page-header d-flex align-items-center p-5" style="background-image: url(<?php echo $coverImage; ?>);">
		<div class="tov-page-header-mask d-flex align-items-center">
			<div class="container text-center">
				<h2 class="fs-1 m-0 text-white"><?php echo $title; ?></h1>
			</div>
		</div>
	</div>
<?php } ?>

<?php if ($show_search) { ?>
	<form class="form-inline d-md-inline-flex w-100 bg-light p-1 p-md-2 shadow" id="searchEvents" style="flex: 2;">
		<div class="d-inline-flex flex-md-fill w-100 mx-md-1 mb-1 mb-md-0" style="flex: 2;">
			<?php echo $categoryList; ?>
		</div>
		<div class="d-inline-flex flex-md-fill w-100 mx-md-1 mb-1 mb-md-0" style="flex: 2;">
			<label class="d-flex bg-light text-body my-auto rounded-start p-2 border border-end-0" style="min-height:38px;"><i class="fad fa-search my-auto h-100"></i></label>
			<input type="text" class="form-control flex-md-fill w-100 rounded-end" style="border-top-left-radius: 0; border-bottom-left-radius: 0;"  placeholder="<?php echo AxsLanguage::text('COM_EB_AXS_EVENTS_LIST_SEARCH', 'Search'); ?>..." id="title" name="title" value="<?php echo $model->search->title; ?>">
		</div>
		<a class="btn btn-outline-primary btn-sm pt-0 d-flex mt-auto w-100" id="submitSearch" style="flex: 1;"><?php echo AxsLanguage::text('JSEARCH_FILTER', 'Search'); ?></a>
	</form>

	<?php if($model->catData->description) { ?>
		<div style="padding: 20px;">
			<p style="margin: 0;"><?php echo $model->catData->description; ?></p>
		</div>
	<?php } ?>
<?php } ?>

	<div name="Events Items" class="tov-ml-content d-inline-flex justify-content-center flex-wrap py-1 py-md-2 px-1">
	<?php
		if (count($items) > 0) {
			foreach ($items as $item) {
				$settings = json_decode($item->params);
				$event_id = $item->id;
				$link = JRoute::_(EventbookingHelperRoute::getEventRoute($event_id));
				$cover_image = $brand->default_settings->event_cover_image;
				$eventParams = json_decode($item->params);

				if (($eventParams->thumbnail_image_type == 'upload' && $item->thumb) || (!$eventParams->thumbnail_image_type && $item->thumb))  {
					$cover_image = JUri::base(true).'/'.AxsImages::getImagesPath('events').'/images/'.$item->thumb;
				} else if ($eventParams->thumbnail_image_type == 'default' && $eventParams->default_image) {
					$cover_image = JUri::base(true) . '/' .  AxsImages::getImagesPath('events') . '/images/defaults/' . $eventParams->default_image;
				}
	?>
		<!-- Event Card: CARD -->
		<div name="Event Card" class="d-flex align-items-start flex-column ml-card text-decoration-none rounded shadow overflow-hidden w-100 w-md-50 mx-md-1 mb-1 mb-md-2">
			<div class="ml-card-image position-relative rounded-top w-100" style="background-image: url(<?php echo $cover_image; ?>);">
				<!-- <div name="Event Featured" class="position-absolute text-body top-0 end-0 px-2 py-1 rounded bg-white mt-2 me-2">
					<i class="fas fa-stars text-primary me-2"></i>Featured
				</div> -->
				<?php if ($item->individual_price > 0 || $item->price_text) { ?>
				<div name="Card Event Details" class="tov-event-details position-absolute bottom-0 start-0 p-3 pt-0">
					<div class="btn btn-light fs-7 p-2 py-1">
						<?php echo " " . AxsLanguage::text('COM_EB_AXS_EVENT_PRICE', 'Price');?>
					 	<span class="badge bg-primary ms-1">

							<?php if ($purchased) { ?>
									<span><?php echo AxsLanguage::text('COM_EB_AXS_EVENT_PURCHASED_STATUS','Purchased');?></span>
							<?php } else {
									if ($item->price_text) {
										echo $item->price_text;
									} elseif ($item->individual_price == 0 && !$item->price_text) {
										echo AxsLanguage::text("COM_EB_AXS_EVENT_FREE_STATUS", "FREE");
									} else {
										echo $currency_symbol.$item->individual_price;
									}
								}
							?>
						</span>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="bottom-blog d-inline-flex justify-content-between align-items-center w-100 bg-dark event-date p-2">
				<div class="bottom-blog-user d-inline-flex justify-content-between align-items-center text-light">
					<i class="fas fa-calendar text-light me-2"></i><span class="blog-author-name fs-7"><?php echo EventbookingHelperEventlist::getDateSpan($event_id); ?></span>
				</div>
			</div>
			<div name="Card Text" class="text-body p-3">
				<div class="event-title"><?php echo $item->title; ?></div>
			</div>
			<!-- <div name="Card Details" class="tov-event-location px-3">
				<p class="text-start fs-7"><i class="fas fa-map-marked-alt me-2"></i>Virtual Meeting</p>
			</div> -->
			<div name="Card Buttons" class="p-3 pt-0 d-flex mt-auto" style="width: 100%;">
				<!-- <div class="btn btn-success flex-fill me-2"><i class="fas fa-paper-plane me-2"></i>Register</div> -->
				<?php if (!$settings->locked || $purchased) { ?>
					<a href="<?php echo $link; ?>" class="btn btn-outline-primary btn-sm pt-0 d-flex mt-auto w-100"><?php echo AxsLanguage::text("AXS_VIEW_DETAILS", "View Details");?></a>
				<?php }  else { ?>
					<a class="btn btn-outline-primary btn-sm pt-0 d-flex mt-auto w-100" disabled>
						<span class="fa fa-lock"></span> <?php echo AxsLanguage::text('COM_EB_AXS_EVENT_VIEW_EVENT_BUTTON_LOCKED','Locked');?>
					</a>
				<?php } ?>

			</div>
		</div>
		<!-- END Event Card: CARD -->
<?php
		} // end foreach
	} else {
?>

		<div style="display: flex; justify-content: center; padding: 20px; margin-top: 20px; margin-bottom: 20px; background-color: white; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">
			<h2 style="margin: 0;">
				<?php echo AxsLanguage::text('COM_EB_AXS_NO_EVENTS_AVAILABLE', 'No Events Available');?>
			</h2>
		</div>


<?php
	}
?>
	</div>
</div>
<?php if ($module_settings) { ?>
<script>
	function resizeCards() {
		var parentWidth = parseInt($('.tov-ml-content').width());
		var cardWidth;
		if(parentWidth <= 600) {
			cardWidth = '90%';
		} else if(parentWidth <= 992) {
			cardWidth = '45%';
		} else {
			cardWidth = '23.98%';
		}
		$('.ml-card').attr('style', 'max-width:'+cardWidth+' !important;');
	}
	resizeCards();
	$(window).resize(function(){resizeCards()})
</script>
<?php } ?>
<?php if($load_javascript) { ?>
<script>
	$(document).ready(
		function() {
			function sendForm() {
				var link = '<?php echo JRoute::_("index.php?option=com_eventbooking&view=eventlist"); ?>'+'?search=';
				var search = {
					'cat': $('#cat').val(),
					'title': $('#title').val(),
					'desc': $('#desc').val()
				};
				search = btoa(JSON.stringify(search));
				window.location.href = link + search;
			}
			$('#submitSearch').click(function(ev) {
				ev.preventDefault();
				sendForm();
			});
			$('#searchEvents').submit(function(ev) {
				ev.preventDefault();
				sendForm();
			});
			$(document).on('change', '#cat', function(){
				sendForm();
			});

		});
</script>
<?php } ?>