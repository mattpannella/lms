$(document).ready(function() {

    var avatars = $(".gallery .avatar");
    var responseElement = $('#responseMessage');

    avatars.click(function(event) {

        responseElement.hide();

        avatars.removeClass('selected');
        $(this).toggleClass('selected');
    });

    $("#setAvatar").click(function() {

        let avatarId = parseInt($(".gallery .avatar.selected").attr('data-avatar-id'));

        avatars.removeClass('selected');

        $.ajax({
            url: '/component/axs?task=avatars.changeUserAvatar&format=raw',
            data: {
                avatar_id: avatarId
            },
            type: 'POST',
            dataType: 'json'
        }).done(function(response) {

            // Display success message by the gallery button
            let postUploadAction = $("#setAvatar").attr('data-avatar-post-upload-action');

            $.ajax({
                url: postUploadAction,
                data: {
                    avatar_id: avatarId
                },
                type: 'POST',
                dataType: 'json'
            }).done(function(postUploadResponse) {

                if(postUploadResponse.error == "true") {

                    responseElement.addClass('error-message');
                    responseElement.removeClass('info-message');

                    responseElement.text(postUploadResponse.msg);
                } else {
                    responseElement.addClass('info-message');
                    responseElement.removeClass('error-message');

                    responseElement.text(response.message);
                }

                responseElement.show();

                location.reload();
            });
        }).fail(function(response) {

            console.log("Failure Response: ", response);
        });
    });
});