var maxStringLength = null;

$(document).ready(
    function() {    
        init_blocksit();

        $("#inspiration_category_select").change(
            function() {
                var category_list = document.forms["inspiration_form"]["category"].value;
                if (category_list != null && category_list != "") {
                    $("#inspiration_category_warning").hide(500);
                }
            }
        );

        $("#inspiration_file").change(
            function() {
                var inspiration_image = document.forms["inspiration_form"]["image"].value;
                if (inspiration_image == "" || inspiration_image == null) {
                    $("#inspiration_picture_warning").show(500);
                    $("#image_preview_area").hide(500);
                } else {
                    $("#inspiration_picture_warning").hide(500);
                    $("#image_preview_area").show(500);
                    getImage(this);
                }
            }
        );

        var textArea = document.getElementById("inspiration_text_area");
        var setting = document.getElementById("public_setting");

        var checkText = function() {
            var textLength = textArea.value.length;
            if (textLength >= maxStringLength) {
                setting.selectedIndex = 1;
            }
        }
        
        if (textArea.addEventListener) {
              textArea.addEventListener('input', function() {
                    // Non IE
                    checkText();
              }, false);
        } else if (textArea.attachEvent) {
              textArea.attachEvent('onpropertychange', function() {
                    // IE-specific event handling code
                    checkText();
              });
        }

        $(setting).change(
            function() {
                var textLength = textArea.value.length;
                if (textLength >= maxStringLength) {
                    setting.selectedIndex = 1;
                    alert("Text with " + maxStringLength + " characters or more can not be set to public.  You have " + textLength + " characters.");
                }
            }
        );

        $(".delete_image_button").click(
            function() {
                if (confirm("Are you sure you want to delete this?\nThis action is permanent.")) {

                    var post_id = this.getAttribute("post_id");

                    var data = {
                        board:      localStorage.getItem("encrypted_board_id"),
                        pid:        post_id
                    }

                    $.ajax({
                        type: 'POST',
                        url: 'index.php?option=com_axs&task=boards.deletePost&format=raw',
                        data: data,
                        success: function(response) {

                            if (response == 'success') {
                                
                                $(".inspiration_container").each(
                                    function() {
                                        var id = this.getAttribute("post_id");
                                        if (id == post_id) {
                                            $(this).remove();
                                        }
                                    }
                                );

                            } else {
                                alert("There was an error when trying to delete the post.");
                            }
                            
                        }
                    });
                }
            }
        );

        $(".inspiration_switch_privacy").click(
            function() {

                var current = this.getAttribute("current");
                var post_id = this.getAttribute("post_id");
                
                var data = {
                    board:  localStorage.getItem("encrypted_board_id"),
                    pid:    post_id,
                    cur:    current
                }

                $.ajax({
                    type: 'POST',
                    url: 'index.php?option=com_axs&task=boards.setPrivacy&format=raw',
                    data: data,
                    success: function(response) {

                        if (response == 'success') {
                            
                            $(".inspiration_switch_privacy").each(
                                function() {
                                    var id = this.getAttribute("post_id");
                                    if (post_id == id) {
                                        var setting = this.getAttribute("current");
                                        if (setting == current) {
                                            $(this).hide();
                                        } else {
                                            $(this).show();
                                        }
                                    }
                                }
                            );
                            
                        } else {
                            alert("There was an error when trying to change the privacy setting.");
                        }
                        
                    }
                });

                
            }
        );
    }
);

function setMaxStringLength(value) {
    maxStringLength = value;
}

function getImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $("#image_preview").attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function init_blocksit() {

         
    $("div#am-btn").click(
        function() {
            $('#am-btn').toggleClass('minus');
            $('#am-btn').toggleClass('plus');
            $('#add-special').toggle(500);
            return false;
        }
    );

    var video_refresh = function () {
        setInterval(
            function() {                
                refreshBlocksit(get_num_columns(), 'div', '#block-container');                
            }, 200
        );
    }

    setTimeout(
        function() {
            $('#block-container').show();
            $("#column_set").show();
            $("#loading").hide();
            video_refresh();
        },
        600
    );
   
}

var num_columns = 0;

function get_num_columns() {
    
    var winWidth = $(window).width();

    var columns = Math.floor(winWidth / 400) + num_columns;

    if (columns < 1) {
        columns = 1;
        num_columns++;
    }

    if (columns > 10) {
        columns = 10;
        num_columns--;
    }

    return columns;
}

function validateForm() {

    var submit_button = document.getElementById('inspiration_submit_button');
    //submit_button.disabled = true;
    var original_submit_button_text = submit_button.value;
    submit_button.value = "Submitting...";

    var inspiration_image = document.forms["inspiration_form"]["image"].value;
    var category_list = document.forms["inspiration_form"]["category"].value;

    var good = true;
    var curPhoto = $("#current_photo").attr("src");
    

    if ((inspiration_image == "" || inspiration_image == null) && (curPhoto == "" || typeof(curPhoto) == 'undefined')) {
        good = false;
        $("#inspiration_picture_warning").show(500);
        $("#image_preview_area").hide(500);
    } else {
        $("#inspiration_picture_warning").hide(500);
    }

    if (category_list == "" || category_list == null) {
        good = false;
        $("#inspiration_category_warning").show(500);
    } else {
        $("#inspiration_category_warning").hide(500);
    }

    if (good == true) {

        function saveData(fileData) {
            var fields = btoa($("#inspiration_form").serialize());
            var actions = $("#inspiration_form").attr("action");

            var data = {
                fields:     fields,
                board:      localStorage.getItem("encrypted_board_id"),
                action:     actions,
                fileData:   fileData
            };

            $.ajax({
                type: 'POST',
                url: 'index.php?option=com_axs&task=boards.saveBoard&format=raw',
                data: data,
                success: function(response) {

                    var params = window.location.search.split("&");
                    var search = "";
     
                    //If a post was updated, we don't want to update it again.  Remove the update tag from the url.
                    for (var i = 0; i < params.length; i++) {
                        if (!params[i].includes("update")) {
                            search += params[i] + "&";
                        }
                    }

                    search = search.slice(0, -1);

                    window.location.href = window.location.pathname + search;
                }
            });
        }

        var input = document.getElementById("inspiration_file");
        var fileData = null;
        if (input.files && input.files[0]) {            
            var reader = new FileReader();
            
            //Wait until the file is loaded to send the data.
            reader.onload = function(e) {
                fileData = e.target.result;
                
                saveData(fileData);
            }

            reader.readAsDataURL(input.files[0]);            
        } else {
            saveData();
        }
        
    } else {
        submit_button.disabled = false;        
        submit_button.value = original_submit_button_text;
    }



    return false;
}
