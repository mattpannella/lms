function getFilters() {
    if(start > 0) {
        var new_start = start + 1;
        start = new_start;
    }
    var sizeLoad	= size;
    var startLoad	= new_start;
    var search = $('input[name="tld_search"]').val();
    var sort = $('select[name="team-filter"]').val();
    var data = {
            team : team,
            sort: sort,
            search: search,
            size : sizeLoad,
            start : startLoad,
            add : add
    };
    return data;
}
function loadItems(forSearch = false) {

    var data = getFilters();

    if (forSearch) {
        $('.team-list').hide();
    }
    $('.ajax-load-more').show();
    $.ajax({
        type: 'POST',
        url: '/index.php?option=com_axs&task=teams.getTeamMembersHTML&format=raw&lang=' + JLANGUAGE.split('-')[0],
        data: data
    }).done(function(response) {
            var result = JSON.parse(response);
            $('.ajax-load-more').hide();
            if(result) {
                if (result.html == "") {
                    // no items returned, we have loaded everything
                    allItemsLoaded = true;
                    //$('.all-items-loaded').show();
                }
                if (forSearch) {
                    $('.team-list').empty();
                }
                $('.team-list').append(result.html);
                $('.team-list').show();
                JCEMediaBox.Popup.create($('.members_popup'));
                start = start + size;
                $('.load-more').show();
            } else {
                $('.load-more').hide();
            }
            if(result.allLoaded) {
                $('.load-more').hide();
            }
    });
}

loadItems();

$('.load-more').click(function(){
    $(this).hide();
    loadItems();
});

$('input[name="tld_search"]').keyup( function(e) {
    if($(this).val().length == 0 || $(this).val().length > 2) {
        start = 0
        loadItems(true);
    }
});

$('.tld_search').click(function(){
    if($(this).val().length == 0 || $(this).val().length > 2) {
        start = 0
        loadItems(true);
    }
});

$('#team-filter').change(function() {
    var filter = $(this).val();
    switch(filter) {
        case "alphabeticalASC":
            start = 0
            loadItems(true);
        break;
        case "alphabeticalDESC":
            start = 0
            loadItems(true);
        break;
        case "coursesMost":
            filterTeam(filter);
        break;
        case "coursesLeast":
            filterTeam(filter);;
        break;
        case "checklistMost":
            filterTeam(filter);
        break;
        case "checklistLeast":
            filterTeam(filter);
        break;
    }

});