
function checkItem(complete,data) {
	$.ajax({
		url: '/index.php?option=com_axs&task=checklists.checkItem&format=raw',
		data: {
			item: data,
			complete: complete
		},
		type: 'POST'
	}).done(function(response){
		var result = JSON.parse(response);
		if(result.success) {
			var list = $('#tovuti-tasklist-list-body-collapse-'+result.checklist_id);
			var item = $('.item-'+result.item_id);
			if(complete) {
				item.removeClass('task-complete');
				item.addClass('task-incomplete');
			} else {
				item.removeClass('task-incomplete');
				item.addClass('task-complete');
			}
			list.find('.progress-bar').attr('aria-valuenow',result.progress).css('width',result.progress+'%');
			list.find('.progress-amount').text(result.progress);
		}
	})
}

$('.checklist-item').click(function() {
	var check = $(this).data('check');
	if(!check) {
		return;
	}
	var complete = 0;
	var data = $(this).data('item');
	if($(this).hasClass('task-complete')) {
		complete = 1;
	}
	checkItem(complete,data);
});

function setFilters() {
	var filterCount = 0;
	$('.tasklist-filters li').each(function() {
		var isActive = $(this).hasClass('active');
		var filter   = $(this).data('filter');
		var filterClass = '';
		if(filter != 'all') {
			var filterClass = '.item-'+filter;
		}
		if(isActive && filterClass) {
			$(filterClass).show();
			filterCount++;
		} else {
			if(filterClass) {
				$(filterClass).hide();
			}
		}
	});
	if(filterCount == 0) {
		$('.filter-all').addClass('active');
		$('.ch-item').show();
	}
}

$('.tasklist-filters li').click(function(){
	var isActive = $(this).hasClass('active');
	var filter   = $(this).data('filter');
	if(isActive) {
		$(this).removeClass('active');
	} else {
		$(this).addClass('active');
		if(filter != 'all') {
			$('.filter-all').removeClass('active');
		}
	}
	setFilters();
});

$('.archive-link').click(function() {
	var section  = $(this).data('section');
	switch(section) {
		case "checklist" :
			var text = LOCALIZATION_VARS.CHECKLIST;
		break;
		case "courses" :
			var text = LOCALIZATION_VARS.COURSES;
		break;
		case "events" :
			var text = LOCALIZATION_VARS.EVENTS;
		break;
	}

	var hideText = LOCALIZATION_VARS.HIDE + " " + text;
	var showText = LOCALIZATION_VARS.SHOW + " " + text;
	var currentText = $(this).text();
	if(currentText.toLowerCase() == showText.toLowerCase()) {
		$('.archive-section-'+section).show();
		$(this).text(hideText);
	} else {
		$('.archive-section-'+section).hide();
		$(this).text(showText);
	}
});

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" });
});