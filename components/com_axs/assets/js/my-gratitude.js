var maxStringLength = null;

$(document).ready(
    function() {

        $("div#am-btn").click(
            function() {
                $('#am-btn').toggleClass('minus');
                $('#am-btn').toggleClass('plus');
                $('#add-special').toggle(500);
                return false;
            }
        );

        $("#grateful_what_text").on("change keyup paste", 
            function() {
                var grateful_for = document.forms["gratitude_form"]["grateful_what"].value;
                if (grateful_for.length > 0) {
                    $("#gratitude_text_warning").hide(500);
                }
            }
        );

        $("#grateful_why_text").on("change keyup paste", 
            function() {                
                var grateful_why = document.forms["gratitude_form"]["grateful_why"].value;
                if (grateful_why.length > 0) {
                    $("#gratitude_why_warning").hide(500);
                }
            }
        );

        $("#gratitude_category_select").change(
            function() {
                var category_list = document.forms["gratitude_form"]["category"].value;
                if (category_list != null && category_list != "") {
                    $("#gratitude_category_warning").hide(500);
                }
            }
        );

        $(".delete_gratitude").click(
            function() {
                if (confirm("Are you sure you want to delete this?\nThis action is permanent.")) {

                    var data = {
                        board:      localStorage.getItem("encrypted_board_id"),
                        pid:        this.getAttribute("post_id")
                    }

                    var edit_container = this.parentNode;

                    $.ajax({
                        type: 'POST',
                        url: 'index.php?option=com_axs&task=boards.deletePost&format=raw',
                        data: data,
                        success: function(response) {
                            if (response == 'success') {
                                var post_container = edit_container.parentNode;
                                $(post_container).remove();
                            } else {
                                alert("There was an error when trying to delete the post.");
                            }
                            
                        }
                    });
                }
            }
        );

        $(".gratitude_switch_privacy").click(
            function() {

                var privacy_button = this;
                var current = privacy_button.getAttribute("current");
                
                var data = {
                    board:  localStorage.getItem("encrypted_board_id"),
                    pid:    privacy_button.getAttribute("post_id"),
                    cur:    current
                }

                $.ajax({
                    type: 'POST',
                    url: 'index.php?option=com_axs&task=boards.setPrivacy&format=raw',
                    data: data,
                    success: function(response) {

                        if (response == 'success') {
                            var container = privacy_button.parentNode;
                
                            var views = container.getElementsByClassName("gratitude_switch_privacy");
                            $(views).each(
                                function() {
                                    if (this.getAttribute("current") == current) {
                                        $(this).hide();
                                    } else {
                                        $(this).show();
                                    }
                                }
                            );
                        } else {
                            alert("There was an error when trying to change the privacy setting.");
                        }
                        
                    }
                });

                
            }
        );

        var textAreaWhat = document.getElementById("grateful_what_text");
        var textAreaWhy = document.getElementById("grateful_why_text");
        var setting = document.getElementById("public_setting");

        var checkText = function() {
            var textLengthWhat = textAreaWhat.value.length;
            var textLengthWhy = 0;
            if (textAreaWhy) {  //Can be disabled.
                textLengthWhy = textAreaWhy.value.length;
            }

            if (textLengthWhat >= maxStringLength || textLengthWhy >= maxStringLength) {
                setting.selectedIndex = 1;
            }
        }
        
        if (textAreaWhat.addEventListener) {
              textAreaWhat.addEventListener('input', function() {
                    // Non IE
                    checkText();
              }, false);
        } else if (textAreaWhat.attachEvent) {
              textAreaWhat.attachEvent('onpropertychange', function() {
                    // IE-specific event handling code
                    checkText();
              });
        }

        if (textAreaWhy) {  //Can be disabled
            if (textAreaWhy.addEventListener) {
                  textAreaWhy.addEventListener('input', function() {
                        // Non IE
                        checkText();
                  }, false);
            } else if (textAreaWhy.attachEvent) {
                  textAreaWhy.attachEvent('onpropertychange', function() {
                        // IE-specific event handling code
                        checkText();
                  });
            }
        }

        $(setting).change(
            function() {
                var textLengthWhat = textAreaWhat.value.length;
                var textLengthWhy = 0;
                if (textAreaWhy) {  //Can be disabled
                    textLengthWhy = textAreaWhy.value.length;
                }

                if (textLengthWhat >= maxStringLength || textLengthWhy >= maxStringLength) {
                    setting.selectedIndex = 1;
                    if (textLengthWhat >= maxStringLength) {
                        if (textLengthWhy >= maxStringLength) {
                            alert("Text with " + maxStringLength + " characters or more can not be set to public.  You have " + textLengthWhat + " characters in your first text field and " + textLengthWhy + " characters in your second text field.");
                        } else {
                            alert("Text with " + maxStringLength + " characters or more can not be set to public.  You have " + textLengthWhat + " characters in your first text field.");
                        }
                    } else {
                        alert("Text with " + maxStringLength + " characters or more can not be set to public.  You have " + textLengthWhy + " characters in your second text field.");
                    }
                }
            }
        );
    }
);

function setMaxStringLength(value) {
    maxStringLength = value;
}

function setThankfulMessage(message) {
    alreadyThankfulMessage = message;
}

function thankful(thankful_id) {
    var checkbox = document.getElementById('check_thanked_' + thankful_id);
    
    checkbox.onchange = function() {
        var state = checkbox.checked;
        
        if (state == true) { 
            
            $.ajax({
                type: 'POST',                
                url: "index.php?option=com_axs&task=boards.gratitudeThankful",
                data: {
                    gid: thankful_id
                }, 
                success: function(response) {
                    //console.log(response);
                }
            });
        } else {
            checkbox.checked = true;
            alert(alreadyThankfulMessage);
        }
    }
}

function uncheck_at_midnight(check_id) {
    var now = new Date();
    var millisTillMidnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 24, 0, 0, 0) - now;
    
    setTimeout(
        function() {
            var checkbox = document.getElementById('check_thanked_' + check_id);
            checkbox.checked = false;
            uncheck_at_midnight(check_id);
        }, millisTillMidnight
    );    
}

 function validateForm() {

    var submit_button = document.getElementById('gratitude_submit_button');
    submit_button.disabled = true;
    var original_submit_button_text = submit_button.value;
    submit_button.value = "Submitting...";

    var form = document.forms["gratitude_form"];

    var grateful_for = form["grateful_what"].value;
    var grateful_why;

    if (form["grateful_why"]) {
        grateful_why = form["grateful_why"].value;
    } else {
        //It's turned off
        grateful_why = false;
    }

    var category_list = form["category"].value;

    var good = true;
    
    if (grateful_for === "" || grateful_for === null) {
        good = false;
        $("#gratitude_text_warning").show(500);
    } else {
        $("#gratitude_text_warning").hide(500);
    }

    if (grateful_why === "" || grateful_why === null) {
        console.log(grateful_why);
        good = false;
        $("#gratitude_why_warning").show(500);
    } else {
        $("#gratitude_why_warning").hide(500);
    }

    if (category_list === "" || category_list === null) {
        good = false;
        $("#gratitude_category_warning").show(500);
    } else {
        $("#gratitude_category_warning").hide(500);
    }

    if (good === false) {
        submit_button.disabled = false;        
        submit_button.value = original_submit_button_text;
    }

    if (good) {
        var fields = btoa($("#gratitude_form").serialize());
        var actions = $("#gratitude_form").attr("action");
        
        var data = {
            fields:     fields,
            board:      localStorage.getItem("encrypted_board_id"),
            action:     actions
        };

        $.ajax({
            type: 'POST',
            url: 'index.php?option=com_axs&task=boards.saveBoard&format=raw',
            data: data,
            success: function(response) {

                var params = window.location.search.split("&");
                var search = "";

                //If a post was updated, we don't want to update it again.
                for (var i = 0; i < params.length; i++) {
                    if (!params[i].includes("update")) {
                        search += params[i] + "&";
                    }
                }

                search = search.slice(0, -1);
                
                window.location.href = window.location.pathname + search;
            }
        });
    }

    return false;
 }