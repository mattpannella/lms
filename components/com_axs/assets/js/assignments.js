$('.tovuti-popup').click(function(ev) {
    ev.preventDefault();
    var url  = $(this).attr('href');
    var type = $(this).data('type');
    var width = $(this).data('width');
    var height = $(this).data('height');
    var params = {"width": width, "height": height}
    jcepopup.open(url, '', '', type, params);
});
function setSelectorData(type,data) {
    var selector= $('.'+type+'_selector');
    var counter = $('#'+type+'_counter');
    var counterText = counter.find('span.counter-text');
    var counterCount = counter.find('span.counter-count');
    var counterS = counter.find('span.counter-s');
    if (Array.isArray(data) === false) {
        var data =  data.split(',');
    }
    var amount  = data.length;
    
    if (amount == 0) {
        counterText.hide();
        counterCount.hide();
        counterS.hide();
    } else {
        counterText.show();
        counterCount.text(amount);
        if(amount > 1) {
            counterS.show();
        } else {
            counterS.hide();
        }
    }
    
    var url = selector.attr('href');
    if (data != '') {
        if(url.includes('&val')) {
            url = url.split('&val')[0];
        }
        selector.attr('href',url+'&val='+btoa(data));
    } else {
        if(url.includes('&val')) {
            url = url.split('&val')[0];
            selector.attr('href',url);
        }
        switch(type) {
            case 'course':
                counter.html('<i class="fa fa-check"></i>');
            break;
            case 'user':
                counter.html('<i class="fa fa-users"></i>');
            break;
            case 'user_events':
                counter.html('<i class="fa fa-users"></i>');
            break;
            case 'event':
                counter.html('<i class="fa fa-calendar-check-o"></i>');
            break;
            default:
                counter.html('<i class="fa fa-check"></i>');
            break;
        }
    }

    $('input[name="'+type+'[]"]').val(data);
}

window.addEventListener("message", getSelectedItems, false);
function getSelectedItems(e) {
    if(e.data.ids) {
        var type    = e.data.element;
        setSelectorData(type,e.data.ids);
        $('#jcemediabox-popup-frame').trigger('click');
    }
}

$('.assignment-form-btn').click(function() {
    $('.assignment-list').hide();
    $('#assignment_events_form').hide();
    $('.assignment_form').show();
});
$('#assign_courses_button_cancel').click(function() {
    $('.assignment_form').hide();
    $('.assignment-list').show();
    document.getElementById('assignment_form').reset();
    $('input[name="encode"]').val(encode);
    setSelectorData('course',"");
    setSelectorData('user',"");
    setSelectorData('group',"");
});
$('.assign-events-form-btn').click(function() {
    $('.assignment-list').hide();
    $('.assignment_form').hide();
    $('#assignment_events_form').show();
});
$('#assign_events_button_cancel').click(function() {
    $('#assignment_events_form').hide();
    $('.assignment-list').show();
    document.getElementById('assignment_events_form').reset();
    $('input[name="encode"]').val(encode);
    setSelectorData('event',"");
    setSelectorData('user_events',"");
});

function getAssignmentList() {
    data = {
        encode : encode,
        language: 'fr-FR'
    }
    $.ajax({
        url: '/index.php?option=com_axs&task=team.getAssignmentList&format=raw&lang=' + JLANGUAGE.split('-')[0],
        type: 'post',
        data: data
    }).done(
        function(result) {
            document.getElementById('assignmentList').innerHTML = result;
    });
}

getAssignmentList();

function toggleAssignmentSelection() {
    var type = $('input[name="assign_to"]:checked').val();
    $('.assign-type').hide();
    switch(type) {
        case 'groups':
            $('.usergroup_checkbox').show();
        break;
        case 'users':
            $('.userlist_checkbox').show();
        break;
    }
}

$('input[name="assign_to"]').change(function() {
    toggleAssignmentSelection();
});

// teamType is defined in assignments.php so we can display certain UI elements based on the team membership type
if(teamType == 'individuals') {
    // If the teamType is "individuals", only show the userlist selector and keep the group selector hidden.
    $('.userlist_checkbox').show();
} else {
    // If the teamType is "groups", show both selectors as normal, depending on which radio button is checked.
    toggleAssignmentSelection();
}

$('#assignment_form').submit(function(ev) {
    ev.preventDefault();

    var data = $('#assignment_form').serialize();

    $.ajax({
        url: '/index.php?option=com_axs&task=team.assignCourses&format=raw',
        type: 'post',
        data: data
    }).done(
        function(result) {
            var response = JSON.parse(result);
            if (response.status == "success") {

                document.getElementById('assignment_form').reset();
                $('input[name="encode"]').val(encode);
                
                // Reset the selectors
                setSelectorData('course',"");
                setSelectorData('user',"");
                setSelectorData('group',"");

                $('.assignment_form').hide();
                setTimeout(function(){
                    getAssignmentList();
                    $('.assignment-list').fadeIn();
                },300)

            } else {
                alert(response.message)
            }
    });
});

$(document).on('click','.assignment-delete-button',function() {
    var encodeValue = $(this).data('encode');
    var data = {
        encode : encodeValue
    }
    if(confirm('Are you sure you want to delete this assignment?')) {
        $.ajax({
            url: '/index.php?option=com_axs&task=team.deleteAssignment&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                var response = JSON.parse(result);
                if (response.status == "success") {
                    $('.assignment_form').hide();
                    setTimeout(function(){
                        getAssignmentList();
                        $('.assignment-list').fadeIn();
                    },300);
                } else {
                    alert(response.message)
                }
        });
    }

});

$(document).on('click','.assignment-edit-button',function() {
    var values = atob($(this).data('values'));
    var encodeValue = $(this).data('encode');

    if(values) {
        values = JSON.parse(values);

        if(values.assign_to) {
            $('input[name="assign_to"]').each(function() {
                if(values.assign_to == $(this).val()) {
                    $(this).attr('checked',true);
                    $(this).trigger('change');
                } else {
                    $(this).attr('checked',false);
                }
            });
        }

        if(values.type) {
            $('input[name="assign_type"]').each(function() {
                if(values.type == $(this).val()) {
                    $(this).attr('checked',true);
                } else {
                    $(this).attr('checked',false);
                }
            });
        }

        if(values.course_ids) {
            setSelectorData('course', values.course_ids);
        }

        if(values.user_ids) {
            setSelectorData('user', values.user_ids);
        }

        if(values.usergroups) {
            setSelectorData('group', values.usergroups);
        }

        $('input[name="title"]').val(values.title);
        $('input[name="encode"]').val(encodeValue)

        if(values.due_date) {
            $('input[name="due_date"]').val(values.due_date);
        }

        $('.assignment-list').hide();
        $('.assignment_form').show();
    }
});

$('#assignment_events_form').submit(function(ev) {
    ev.preventDefault();

    var $submit = $(this).find('[type=submit]');

    if (!$submit.data('submitting')) {
        $submit.data('submitting', 1);
        $submit.val('Saving...');

        var data = $('#assignment_events_form').serialize();

        $.ajax({
            url: '/index.php?option=com_axs&task=team.assignEvents&format=raw',
            type: 'post',
            data: data
        }).done(
          function(result) {
              var response = JSON.parse(result);
              if (response.status == "success") {
                  document.getElementById('assignment_events_form').reset();
                  setSelectorData('event',"");
                  setSelectorData('user_events',"");
                  alert(response.message);
              } else {
                  alert(response.message);
              }
          }).always(function() {
            $submit.removeData('submitting');
            $submit.val('Save');
        });
    }
});
