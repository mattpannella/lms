var maxStringLength = null;

$(document).ready(
    function() {

        $(function() {
            updateStartTime(new Date(), null);
            updateEndTime(new Date(), null);
        });
        
        $('.percentage').easyPieChart( {
            animate: 1000,
            lineWidth: 15,
            barColor: '#31a3fa',
            trackColor:'#999',
            lineCap:'butt',
            scaleColor:'#777',
            onStep: function(value) {
                        this.$el.find('span').text(Math.round(value));
                    },
            onStop: function(value, to) {
                this.$el.find('span').text(Math.round(to));
            }
        });

        $(".goal_type").change(
            function() {                
                update_new_view(null);


                //Remove pre-populated data in case it is filled out and then the form doesn't submit.
                var goal_type = document.forms["declaration_form"]["goal_type"].value;
                var end_date = document.forms["declaration_form"]["end_time"].value;
                var goal_to_reach_text = document.forms["declaration_form"]["goal_to_reach_text"].value;

                if (end_date == 0) {
                    $("#end_calendar").val("");
                }

                if ((goal_to_reach_text == 0) || (goal_to_reach_text == 1)) {
                    $("#goal_to_reach_text").val("");
                }
            }
        );

        $("#declaration_textarea").on("change keyup paste", 
            function() {                
                var declaration_text = document.forms["declaration_form"]["declaration"].value;                
                if (declaration_text.length > 0) {
                    $("#declaration_text_warning").hide(500);
                }
            }
        );

        $("#category_select").change(
            function() {
                var category_list = document.forms["declaration_form"]["category"].value;
                if (category_list != null && category_list != "") {
                    $("#category_list_warning").hide(500);
                }
            }
        );

        $("#progressive_checkbox").change(
            function() {
                check_goal_progressive();

                var setting = document.getElementById("progressive_checkbox").checked;
                if (setting == false) {
                    $("#goal_to_reach_warning").hide(500);
                }
            }
        );

        $("#start_calendar").on("change keyup paste",
            function() {
                var start_date = document.forms["declaration_form"]["start_time"].value;    
                var start_date_valid = isValidDate(start_date);

                if (start_date_valid) {
                    $("#start_date_warning").hide(500);
                }
            }
        );

        $("#end_calendar").on("change keyup paste",
            function() {
                var end_date = document.forms["declaration_form"]["end_time"].value;    
                var end_date_valid = isValidDate(end_date);
                if (end_date_valid) {
                    $("#end_date_warning").hide(500);
                }
            }
        );

        $("#goal_to_reach_text").on("change keyup paste",
            function() {
                var goal_to_reach_text = document.forms["declaration_form"]["goal_to_reach_text"].value;
                if (goal_to_reach_text.length != "" && goal_to_reach_text != null) {
                    $("#goal_to_reach_warning").hide(500);
                }

                if (isNaN(goal_to_reach_text)) {
                    $("#goal_number_only_warning").show(500);
                } else {
                    $("#goal_number_only_warning").hide(500);
                }
            }
        );

        $(".declaration_progress_amount").each(
            function(index, which) {                
                $(which).on("change keyup paste",
                    function() {
                        num = $(which).attr("which");
                        value = $(which).val();

                        if (isNaN(value) || value == 0) {
                            $("#update_warning_" + num).show(500);
                        } else {                            
                            $("#update_warning_" + num).hide(500);
                        }                        
                    }
                );                
            }
        );

        //Fill out required fields so that they'll go through        
        $("#declaration_submit_button").click(
            function() {
                var type = $(".goal_type").val();
                
                if (type == 'goal') {
                    var checked = $("#progressive_checkbox").is(":checked");
                    if (checked == false) {
                        $("#goal_to_reach_text").val(1);
                    }
                } else if (type == 'declaration') {
                    $("#end_calendar").val(0);
                    $("#goal_to_reach_text").val(0);
                }                
            }
        );

        $(".update_progress_button").click(
            function() {
                var goal = parseInt(this.getAttribute("goal"));
                var progress = document.getElementById("declaraction_progress_amount_" + goal).value;

                if (!progress) {
                    $("#update_warning_" + goal).show(250);
                } else {

                    var data = {
                            goal:           goal,
                            progress:       progress,
                        };

                    $.ajax({
                        type: 'POST',
                        url: "index.php?option=com_axs&task=boards.updateProgress&format=raw",
                        data: data,
                        success: function(response) {
                            window.location.reload();
                        }
                    });
                }
            }
        );

        $(".delete_declaration_button").click(
            function() {
                if (confirm("Are you sure you want to delete this?\nThis action is permanent.")) {

                    var post_id= this.getAttribute("post_id");

                    var data = {
                        board:      localStorage.getItem("encrypted_board_id"),
                        pid:        post_id
                    }

                    $.ajax({
                        type: 'POST',
                        url: 'index.php?option=com_axs&task=boards.deletePost&format=raw',
                        data: data,
                        success: function(response) {

                            if (response == 'success') {
                                var containers = document.getElementsByClassName("declaration_view");
                                $(containers).each(
                                    function() {
                                        var id = this.getAttribute("post_id");
                                        if (id == post_id) {
                                            $(this.parentNode).remove();
                                        }
                                    }
                                );

                            } else {
                                alert("There was an error when trying to delete the post.");
                            }
                            
                        }
                    });
                }
            }
        );

        $(".declaration_switch_privacy").click(
            function() {

                var privacy_button = this;
                var current = privacy_button.getAttribute("current");
                
                var data = {
                    board:  localStorage.getItem("encrypted_board_id"),
                    pid:    privacy_button.getAttribute("post_id"),
                    cur:    current
                }

                $.ajax({
                    type: 'POST',
                    url: 'index.php?option=com_axs&task=boards.setPrivacy&format=raw',
                    data: data,
                    success: function(response) {

                        if (response == 'success') {
                            var container = privacy_button.parentNode;
                
                            var views = container.getElementsByClassName("declaration_switch_privacy");
                            $(views).each(
                                function() {
                                    if (this.getAttribute("current") == current) {
                                        $(this).hide();
                                    } else {
                                        $(this).show();
                                    }
                                }
                            );
                        } else {
                            alert("There was an error when trying to change the privacy setting.");
                        }
                        
                    }
                });

                
            }
        );

        $( "div#am-btn" ).click(
            function() {
                $('#am-btn').toggleClass('minus');
                $('#am-btn').toggleClass('plus');
                $('#add-special').toggle( 
                    function() {                        
                        update_new_view(null);
                    }
                );
                return false;
            }
        );

        var textArea = document.getElementById("declaration_textarea");
        var setting = document.getElementById("public_setting");

        var checkText = function() {
            var textLength = textArea.value.length;
            if (textLength >= maxStringLength) {
                setting.selectedIndex = 1;
            }
        }
        
        if (textArea.addEventListener) {
              textArea.addEventListener('input', function() {
                    // Non IE
                    checkText();
              }, false);
        } else if (textArea.attachEvent) {
              textArea.attachEvent('onpropertychange', function() {
                    // IE-specific event handling code
                    checkText();
              });
        }

        $(setting).change(
            function() {
                var textLength = textArea.value.length;
                if (textLength >= maxStringLength) {
                    setting.selectedIndex = 1;
                    alert("Text with " + maxStringLength + " characters or more can not be set to public.  You have " + textLength + " characters.");
                }
            }
        );
    }
);

function setMaxStringLength(value) {
    maxStringLength = value;
}

function validateForm() {

    var submit_button = document.getElementById('declaration_submit_button');
    submit_button.disabled = true;
    var original_submit_button_text = submit_button.value;
    submit_button.value = "Submitting...";

    var goal_type = document.forms["declaration_form"]["goal_type"].value;
    var declaration = document.forms["declaration_form"]["declaration"].value;
    var goal_to_reach_text = document.forms["declaration_form"]["goal_to_reach_text"].value;
    var category_list = document.forms["declaration_form"]["category"].value;
    var start_date = document.forms["declaration_form"]["start_time"].value;
    var end_date = document.forms["declaration_form"]["end_time"].value;
    
    var progressive_checkbox = document.getElementById("progressive_checkbox").checked;

    var start_date_valid = isValidDate(start_date);
    var end_date_valid = isValidDate(end_date);

    var start_date_code = new Date(start_date);
    var end_date_code = new Date(end_date);

    var good = true;

    if (declaration == null || declaration == "") {
        $("#declaration_text_warning").show(500);
        good = false;
    } else {
        $("#declaration_text_warning").hide(500);                
    }

    if (start_date_valid == false) {
        $("#start_date_warning").show(500);
        good = false;
    } else {
        $("#start_date_warning").hide(500);
    }

    if (goal_type == "goal") {
        if (progressive_checkbox) {        
            if (goal_to_reach_text == null || goal_to_reach_text == "") {
                $("#goal_to_reach_warning").show(500);
                good = false;
            } else {
                $("#goal_to_reach_warning").hide(500);
            }

            if (isNaN(goal_to_reach_text)) {
                $("#goal_number_only_warning").show(500);
                good = false;
            } else {
                $("#goal_number_only_warning").hide(500);
            }
        }

        if (end_date_valid == false) {
            $("#end_date_warning").show(500);
            good = false;
        } else {
            $("#end_date_warning").hide(500);
        }

        if (end_date_code < start_date_code) {
            $("#end_date_before_warning").show(500);
            good = false;
        } else {
            $("#end_date_before_warning").hide(500);
        }
    }

    if (category_list == "") {
        $("#category_list_warning").show(500);
        good = false;
    } else {
        $("#category_list_warning").hide(500);
    }

    if (good == false) {
        submit_button.disabled = false;        
        submit_button.value = original_submit_button_text;
    }

    if (good) {
        var fields = btoa($("#declaration_form").serialize());
        var actions = $("#declaration_form").attr("action");
        
        var data = {
            fields:     fields,
            board:      localStorage.getItem("encrypted_board_id"),
            action:     actions
        };

        $.ajax({
            type: 'POST',
            url: 'index.php?option=com_axs&task=boards.saveBoard&format=raw',
            data: data,
            success: function(response) {

                var params = window.location.search.split("&");
                var search = "";

                //If a post was updated, we don't want to update it again.
                for (var i = 0; i < params.length; i++) {
                    if (!params[i].includes("update")) {
                        search += params[i] + "&";
                    }
                }

                search = search.slice(0, -1);

                window.location.href = window.location.pathname + search;
            }
        });
    }

    return false;
}

function validateUpdate(goal_id) {
    var good = true;

    var value = document.getElementById("declaraction_progress_amount_" + goal_id).value;

    var isAlpha = isNaN(value);

    if (isAlpha || value == 0) {
        $("#update_warning_" + goal_id).show(500);
        good = false;
    } else {
        $("#update_warning_" + goal_id).hide(500);
    }

    return good;
}

function show_edit(type) {    
    $(document).ready(
        function() {
            update_new_view(type);
        }
    );    
}

function check_goal_progressive() {
    var checked = $("#progressive_checkbox").is(":checked"); 
    if (checked == true) {
        $("#goal_to_reach").show();
    } else {
        $("#goal_to_reach").hide();
    }
}


function update_new_view(type) {

    if (type == null) {
        type = $(".goal_type").val();
    }

    $('.declaration_form').show(500);

    switch (type) {
        case 'declaration': 
            $("#description_text").html("Declarations are asprirations that do not have a specific time frame.");
            $("#edit_action").html(" Declaration");
            $("#goal_to_reach").hide();
            $("#end_date").hide();
            $("#declaration_accomplishable").show();
            $("#goal_progressive").hide();
                    
            break;
                
        case 'goal':
            $("#description_text").html("Goals are asprirations that you want to accomplish within a certain time.");
            $("#edit_action").html(" Goal");
            $("#goal_to_reach").hide();
            $("#end_date").show();
            $("#declaration_accomplishable").hide();
            $("#goal_progressive").show();

            setTimeout(
                check_goal_progressive,
                100
            );

            break;
    }
}



function updateStartTime(newMinDate, newMaxDate) {    
    $( "#start_calendar" ).datepicker({
        minDate: newMinDate,
        maxDate: newMaxDate,
        onSelect: function(dateText, inst) {
            var reset_end = function() {
                $("#end_calendar").datepicker("destroy");
                updateEndTime(dateText, null);
            } 
            reset_end();
            $("#start_date_warning").hide(500);
        }
    });    
}

function updateEndTime(newMinDate, newMaxDate) {
    $( "#end_calendar" ).datepicker({
        minDate: newMinDate,
        maxDate: newMaxDate,
        onSelect: function(dateText, inst) {
            var reset_start = function() {
                $("#start_calendar").datepicker("destroy");
                updateStartTime(new Date(), dateText);                
            }
            reset_start();
            $("#end_date_warning").hide(500);
        }
    });    
}
	
function fadeIn(id) {
    var elem = document.getElementById(id);
    elem.style.transition = 'display 2s linear 0s';
    elem.style.display = 'block';
}
     
function fadeOut(id) {
    var elem = document.getElementById(id);		
    elem.style.transition = 'display 2s linear 0s';
    elem.style.display = 'none';		
}
	
function isValidDate(dateString) {
    // First check for the pattern
    var regex_date = /^\d{1,2}\/\d{1,2}\/\d{4}$/;

    if(!regex_date.test(dateString)) {
        return false;
    }

    // Parse the date parts to integers
    var parts   = dateString.split("/");
    var month   = parseInt(parts[0], 10);
    var day     = parseInt(parts[1], 10);    
    var year    = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1900 || year > 3000 || month == 0 || month > 12) {
        return false;
    }

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
        monthLength[1] = 29;
    }

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}