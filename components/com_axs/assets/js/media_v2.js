/* var myModal = document.getElementById('myModal')
var myInput = document.getElementById('myInput')

myModal.addEventListener('shown.bs.modal', function() {
    myInput.focus()
}) */

var start = 0;
var size = 40;
var secondsSinceLoadItems = 0;

var reloadDelay = 1000;
if(isMobile) {
    reloadDelay = 2000;
}
var operator = '?';
function removeURLParameter(url, parameter) {
    //prefer to use l.search if you have a location/link object
    var urlparts = url.split('?');
    if (urlparts.length >= 2) {

        var prefix = encodeURIComponent(parameter) + '=';
        var pars = urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i = pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
    }
    return url;
}
var currentUrl = removeURLParameter(window.location.href, 'filter');
if(currentUrl.indexOf('?') != -1) {
    var operator = '&';
}



function getCategories() {
    var cat = $('select[name="category"]').val();
    var ca = cat.split(':');
    var categories = {
        category : ca[0],
        subcategory : ca[1]
    }
    return categories;
}


function getFilters() {
    var sizeLoad	= size;
    var startLoad	= start;
    var categoryObject = getCategories();
    var search = $('input[name="search_terms"]').val().replace(/[^\w.\s]/gi, '');
    var type = $('select[name="media_type"]').val().replace(/[^\w.\s]/gi, '');
    var data = {
            category: parseInt(categoryObject.category),
            subcategory: parseInt(categoryObject.subcategory),
            type: type,
            search: search,
            language: current_language,
            size : sizeLoad,
            start : startLoad
    };
    return data;
}


let allItemsLoaded = false;
function loadItems(setURL = true, forSearch = false) {
    var data = getFilters();
    var filter   = btoa(JSON.stringify(data));
    if(setURL) {
        var url  = currentUrl+operator+"filter="+filter;
        window.history.pushState("", "", url);
    }

    $.ajax({
        type: 'POST',
        url: '/index.php?option=com_axs&task=videos.getMedia&format=raw&lang=' +  JLANGUAGE.split('-')[0],
        data: { filter }
    }).done(function(response) {
            var result = JSON.parse(response);
            /* var container = document.querySelector('.tov-ml-content');
            container.innerHTML = response; */
            unsetLoadingItems();

            if(result) {
                if (result.html == "") {
                    // no items returned, we have loaded everything
                    allItemsLoaded = true;
                    doneLoadingItems();
                }
                $('.ml-title').text(result.title);
                if (forSearch) {
                    $('.tov-ml-content').empty();
                }
                $('.tov-ml-content').append(result.html);
                start = start + size;
            } else {
                $('#load-more-button').hide();
                $('.ajax-load-more').hide();
            }
    });
}

function setLoadingItems() {
    $('#load-more-button').hide();
    $('.ajax-load-more').show();
}

function unsetLoadingItems() {
    $('.ajax-load-more').hide();
    $('#load-more-button').show();
}

function doneLoadingItems() {
    $('.ajax-load-more').hide();
    $('#load-more-button').hide();
    $('.all-items-loaded').show();
}

loadItems();

$('select[name="category"],select[name="media_type"]').change( function() {
    start = 0;
    $('.tov-ml-content').html('');
    loadItems();
});

$('input[name="search_terms"]').keyup( function(e) {
    if($(this).val().length == 0 || $(this).val().length > 2) {
        start = 0;
        loadItems(true, true);
    }
});

$('#load-more-button button').click(function() {
    setLoadingItems();
    loadItems(false);
});

let loadItemsTimerPlaceholder = null;
$(window).scroll(function(){
    if (allItemsLoaded) {
        doneLoadingItems();
        return;
    }
    if( $(window).scrollTop() + $(window).height() >= $(document).height() ) {
        setLoadingItems();
        var seconds = Math.round(Date.now() / 1000);
        var delay = reloadDelay - (seconds - secondsSinceLoadItems);
        secondsSinceLoadItems = seconds;
        clearTimeout(loadItemsTimerPlaceholder);
        loadItemsTimerPlaceholder = setTimeout(() => {
            loadItemsSymbol = loadItems(false);
        }, delay);
    }
});
