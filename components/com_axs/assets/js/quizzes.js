function displayStatusMessage(response) {

    var message = $('#statusMessage');

    message.hide();

    if(response.success) {
        message.css('color', 'green');
    } else {
        message.css('color', 'red');
    }

    if(response.completed) {
        $('.tovuti-question-container').html('<div>' + response.completedMessage + '</div>');
        $('.tovuti-submit-container').hide();

        // Display quiz response statistics
        $('.tovuti-completion-status').html(response.stats);
    } else {
        message.text(response.message || "Could not submit your response.");

        // Load the next question
        $('.tovuti-question-container').html(response.nextQuestion);
        $('.tovuti-submit-container').show();
        message.show();
    }
}

$(".tovuti-submit-container button").click(function(el) {

    // Grab the necessary form parameters and send them to the backend controller via AJAX
    var encryptedQuizData = $("input[name='quiz_data']").val();
    var questionType = $("input[name='question_type']").val();
    var quizResponse = "";

    if(questionType == 'multiplechoice') {
        quizResponse = $(".tovuti-question-container input[type='radio'][name='quiz_response']:checked").val();
    } else {
        quizResponse = $(".tovuti-question-container [name='quiz_response']").val();
    }

    $.post(
        '/index.php?option=com_axs&task=quizzes.submitAnswer&format=raw',
        {
            'quiz': encryptedQuizData,
            'answer': quizResponse
        },
        displayStatusMessage,
        'json'
    ).fail(function(status) {
        console.log(status);
    });
});