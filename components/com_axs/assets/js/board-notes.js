var current_post_id = null;
var current_note_id = null;
var current_board_id = null;

$(document).ready(
    function() {        
        
        $("#notes_close").click(
            function() {
                $("#notes_content").val();
                $("#notes_save").show();
                $("#notes_update").hide();
                $("#notes_update_cancel").hide();
                $("#notes").hide(500);
            }
        );

        $(".add_new_note").click(
            function() {

                var notes_content = document.getElementById("notes_content");
                var notes = document.getElementById("notes");

                var post_id = this.getAttribute("post_id");
                current_post_id = post_id;
                current_note_id = null;

                $("#notes_save").show();
                $("#notes_update").hide();
                $("#notes_update_cancel").hide();
                //$("#new_note_content").val("");
                $("#notes_content").html("");

                $(notes).show(500);
                
                $.ajax({
                    url: 'index.php?option=com_axs&task=boards.refreshNotes&format=raw',
                    data: {
                        pid: post_id
                    },
                    type: 'post',
                    success: function (response) {
                        createNotesList(response);
                    }
                });
            }
        );

        $("#notes_save").click(
            function() {

                $("#notes_save").hide();

                if (!current_post_id || !current_board_id) {
                    alert('Please select a post first.');
                    $("#notes_save").show();
                    return;
                }

                $.ajax({
                    url: 'index.php?option=com_axs&task=boards.postNewNote&format=raw',
                    data: {
                        pid: current_post_id,
                        bid: current_board_id,
                        note: $("#new_note_content").val()
                    },
                    type: 'post',
                    success: function (response) {
                        if (response) {
                            response = JSON.parse(response);

                            switch (response.success) {
                                case true:

                                    current_note_id = response.newEntry.id;
                                    $("#notes_update").show();
                                    $("#notes_update_cancel").show();

                                    createNote(response.newEntry);

                                    break;
                                case false:

                                    switch (response.message) {
                                        case "unauthorised":
                                            alert("You are not authorised to create this post.");
                                            break;
                                        case "empty":
                                            alert("You cannot submit an empty note.");
                                            break;
                                        default:
                                            alert("There was an error saving your note.");
                                            break;
                                    }

                                    $("#notes_save").show();

                                    break;
                            }
                        }
                    }
                });
            }
        );

        $("#notes_update").click(
            function() {
                $("#notes_update").hide();
                $("#notes_update_cancel").hide();

                if (!current_post_id || !current_board_id) {
                    alert('Please select a post first.');
                    return;
                }

                if (!current_note_id) {
                    alert("Please select a note to update first.");
                    return;
                }

                let newNote = $("#new_note_content").val();

                $.ajax({
                    url: 'index.php?option=com_axs&task=boards.updateNote&format=raw',
                    data: {
                        pid: current_post_id,
                        bid: current_board_id,
                        nid: current_note_id,
                        note: newNote
                    },
                    type: 'post',
                    success: function (response) {
                        if (response) {
                            response = JSON.parse(response);

                            switch (response.success) {
                                case true:

                                    var updated_note = document.getElementById("note_" + current_note_id);
                                    var content = updated_note.getElementsByClassName("note_content")[0];

                                    content.innerHTML = newNote;
                                    
                                    break;
                                case false:

                                    switch (response.message) {
                                        case "unauthorised":
                                            alert("You are not authorised to create this post.");
                                            break;
                                        case "empty":
                                            alert("You cannot submit an empty note.");
                                            break;
                                        default:
                                            alert("There was an error saving your note.");
                                            break;
                                    }

                                    break;
                            }

                            $("#notes_update").show();
                            $("#notes_update_cancel").show();
                        }
                    }
                });
            }
        );

        $("#notes_update_cancel").click(
            function() {
                current_note_id = null;
                $("#new_note_content").val("");

                $("#notes_update_cancel").hide();
                $("#notes_update").hide();
                $("#notes_save").show();
            }
        );

        var notes_content = document.getElementById('notes_content');
        var template = document.getElementById("note_template").innerHTML;

        function createNotesList(data) {
            if (!data) {
                return;
            }

            var parsed = JSON.parse(data);
            
            for (let i = 0; i < parsed.length; i++) {
                createNote(parsed[i]);
            }
        }

        function createNote(data) {

            var newEntry = document.createElement('div');
            newEntry.className = "note_field";
            newEntry.innerHTML = template;
            newEntry.id = "note_" + data.id;

            notes_content.appendChild(newEntry);

            let dateField = newEntry.getElementsByClassName('note_date')[0];
            let timeField = newEntry.getElementsByClassName('note_time')[0];
            let noteField = newEntry.getElementsByClassName('note_content')[0];
            let noteEdit = newEntry.getElementsByClassName('note_edit')[0];
            let noteDelete = newEntry.getElementsByClassName('note_delete')[0];

            //console.log(new Date(data.date));
            let date = new Date(data.date);
            let month;
            let ampm;

            switch (date.getMonth()) {
                case 0: month = "Jan"; break;
                case 1: month = "Feb"; break;
                case 2: month = "Mar"; break;
                case 3: month = "Apr"; break;
                case 4: month = "May"; break;
                case 5: month = "Jun"; break;
                case 6: month = "Jul"; break;
                case 7: month = "Aug"; break;
                case 8: month = "Sep"; break;
                case 9: month = "Oct"; break;
                case 10: month = "Nov"; break;
                case 11: month = "Dec"; break;
            }

            //dateField.innerHTML = data.date;
            //
            let hour = date.getHours();

            if (hour == 0) {
                hour += 12;
                ampm = "am";
            } else if (hour < 12 && hour > 0) {
                ampm = "am";
            } else if (hour == 12) {
                ampm = "pm";
            } else {
                hour -= 12;
                ampm = "pm";
            }
            
            dateField.innerHTML = month + " " + date.getDate() + ", " + date.getFullYear();
            timeField.innerHTML = hour + ":" + date.getMinutes() + ampm;

            noteField.innerHTML = data.note;
            
            $(noteEdit).click(
                function() {
                    current_note_id = data.id;
                    $("#new_note_content").val(data.note);
                    $("#notes_save").hide();
                    $("#notes_update").show();
                    $("#notes_update_cancel").show();
                }
            );

            $(noteDelete).click(
                function() {
                    if (confirm("Are you sure you want to delete this note?")) {

                        $.ajax({
                            url: 'index.php?option=com_axs&task=boards.deleteNote&format=raw',
                            data: {
                                pid: current_post_id,
                                bid: current_board_id,
                                nid: data.id
                            },
                            type: 'post',
                            success: function (response) {
                                if (response == "true") {
                                    $("#note_" + data.id).remove();
                                } else {
                                    alert("You are not authorised to delete this post.");
                                }

                                $("#notes_update").show();
                                $("#notes_update_cancel").show();
                                
                            }
                        });
                    }
                }
            );
        }
    }
);

function setBoardId(board_id) {
    current_board_id =  board_id;
}