/**
 * Created by mar on 4/4/16.
 */

(function($){
    $(function() {
        $('body').append($('#myModal')); //needed to fix the bug where modal pops up below background

        //######################################### For deleting code ########################################
        $(document).on('click', '.deleteCode', function() {
            var uid = this.dataset.user;
            var code_id = this.dataset.code;
            var row = $(this).parent().parent();

            if (confirm('Are you sure you want to delete this referral code?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.deleteCode&format=raw',
                    data: {
                        uid: uid,
                        code_id: code_id
                    },
                    type: 'post'
                }).done(function (result) {
                    if (result == 'success') {
                        alert('Code deleted successfully!');
                        row.remove();
                    } else {
                        alert('There was an error deleting your code.');
                    }
                }).fail(function () {
                    alert("An Error occurred.");
                });
            }
        });

        //######################################### For Adding a new code ########################################
        $(document).on('click', '.addCode', function() {
            $('#myModalLabel').text('Add New Code');
            var clone = $('#saveCodeForm').clone().addClass('saveCodeFormClone');
            $('#myModalBody').html(clone);
            $('.saveCodeFormClone').show();
        });

        $(document).on('keyup', '.saveCodeFormClone input[name=code]', function() {
            var group = $('.saveCodeFormClone .code-group');
            var message = $('.saveCodeFormClone .code-message');
            var value =  this.value;

            if(value.length < 4) {
                group.addClass('has-error');
                message.text('Must be four characters or more.');
            } else if(!isNaN(value)) {
                group.removeClass('has-success').addClass('has-error');
                message.text('Codes can not be numbers.');
            } else {
                group.removeClass('has-error').addClass('has-success');
                message.text('');

                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.checkCode&format=raw',
                    data: {
                        code: value
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result != 'success') {
                        group.addClass('has-error');
                        message.text('This referral code is already taken.');
                    }
                }).fail(function() {
                    alert('There was an error trying to check your code.');
                });
            }
        });

        $(document).on('click', '.saveCode', function() {
            var uid = $('.saveCodeFormClone').data('user');
            var code = $('.saveCodeFormClone input[name=code]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.saveCode&format=raw',
                data: {
                    uid: uid,
                    code: code
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    //refresh the page so they can see their new card
                    location.reload();
                } else {
                    alert('There was an error trying to add your code, ' + result);
                }
            }).fail(function() {
                alert('There was an error trying to add your code.');
            });
        });

        //######################################### For loading more rewards ########################################
        $(document).on('click', '#loadMoreRewards', function() {
            $(this).prop('disabled', true);
            $(this).html('<img src="images/ajax-loader.gif" /> Load More');
            var button = $(this);
            var uid = $(this).data('user');
            var page = $(this).data('page');
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.loadMoreRewards&format=raw',
                data: {
                    uid: uid,
                    page: page
                },
                type: 'post'
            }).done(function(result) {
                $(result).insertBefore(button.parent().parent());
                button.html('Load More');
                button.prop('disabled', false);
                button.data('page', page+1);
            }).fail(function() {
                alert('There was an error fetching your rewards.');
                button.html('Load More');
                button.prop('disabled', false);
            });
        });
    });
}(jQuery));