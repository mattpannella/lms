
const badge = $('.moyo-2fa-badge');

$(() => $('#tfa-key').focus());

function saveKey() {
    $('.tfa-button').attr('disabled','true');
    const key = $("#tfa-key").val();
    const securityKey = $("#security-key").val();
    const data = {
        secretKey: key,
        securityKey: securityKey
    }

    $.ajax({
        url: `/index.php?option=com_axs&task=user.${task}&format=raw`,
        type: 'post',
        data: data
    }).done(
        function(result) {
            let response = JSON.parse(result);
            let backups = '';
            if (response.status == "success") {
                badge.removeClass('text-bg-danger');
                badge.addClass('text-bg-success');
                badge.html(`<i class="fa-light fa-check"></i>${enabled}`);
                $('.moyo-2fa-setup').hide();
                $('.moyo-2fa-setup-complete').fadeIn(300);
            } else {
                alert(response.message)
            }
    });
}
$('#moyo-2fa-auth').submit(function(e) {
    e.preventDefault();
    saveKey();
});
$('.moyo-2fa-modal-button').click(function(){
    $('#tfa-key').focus();
});
