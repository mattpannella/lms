/*
*   Justin Lloyd
*   2 - 8 - 2016
*/

var search_terms = null;
var video_category = null;
var video_subcategory = null;
var video_limit = null;
var video_page_limit = null;
var video_page = null;
var current_video_url = null;

$(document).ready(function() {
    setCurrentUrl("#"); //Refresh the URL so that it detects an update.

    setInterval(
        //Run this function every 0.250 seconds.
        function() {

            var curPage = getCurrentUrl();          //Get the URL currently saved
            var curHash = window.location.hash;     //Get all of the content after the # in the browser's current URL.

            //If the browser bar is empty
            if (curHash == "") {
                var newUrl = makeUrl();         //Get a new URL
                if (newUrl != "") {             //Ifhide_on_load it is not empty
                    setUrl();                   //Set the browser bar to the new url
                    setCurrentUrl(newUrl);      //Save the new url
                } else {
                    setCurrentUrl("");          //Otherwise, clear the url from local storage and return.
                    return;
                }

                refresh_videos();               //Refresh the page content.
                return;
            }

            //Only update the url if it has changed to avoid duplicates
            //Checks if the stored url is different than the url in the browser.
            if (curPage != curHash) {

                //split all of the information up and save it
                var info = curHash.split('#');
                for (i = 0; i < info.length; i++) {
                    setting = info[i].split('=');
                    switch (setting[0]) {
                        //case "language": setLanguage(setting[1]); break;
                        case "page": setPage(setting[1]); break;
                        case "limit": setLimit(setting[1]); break;
                        case "category": setCategory(setting[1]); break;
                        case "subcategory": setSubcategory(setting[1]); break;
                        case "searchterms": setSearchTerms(setting[1]); break;
                    }
                }

                setCurrentUrl(curHash);
                refresh_videos();       //Refresh the videos from the server
            }
        }, 250
    );

    /*
    $('.tov-ml-content').on('BlocksItComplete', function() {
        equalheight(this);
    });
    */

    //on window resizes refresh blocksit
    $(window).resize(function() {
        refreshBlocksit(get_num_columns(), 'div', '.tov-ml-content');
    });
});

function equalheight(container) {
  var currentTallest = 0,
    currentRowStart = 0,
    rowDivs = [],
    $el,
    topPosition = 0;

  $(container).children().each(function() {
      var el = $(this);
      el.height('auto');
      topPostion = el.position().top;

      if (currentRowStart != topPostion) {
        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }

        rowDivs.length = 0; // empty the array
        currentRowStart = topPostion;
        currentTallest = el.height();
        rowDivs.push(el);
      } else {

        rowDivs.push(el);
        currentTallest = (currentTallest < el.height()) ? (el.height()) : (currentTallest);
      }

      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
  });
}

function hide_content() {

    $(".hide_on_load").hide();
    $(".show_on_load").show();
}

function stopAudio() {
    $('.axsAudio').trigger('pause');
}

function playAudio(id) {
    $('#audioPlayer_' + id).trigger('play');
}

function set_pagination_amount_menu(limit) {

    //Set the drop down menu for the page limit to the current setting
    $("select#pagination_amount option").each(
        function() {
            this.selected = (this.value == limit);
        }
    );
}

function refresh_videos() {

    hide_content();

    var page = getPage();                   //Get the current page
    var limit = getLimit();                 //Get the current limit per page
    var category = getCategory();           //Get the current category
    var subcategory = getSubcategory();     //Get the current subcategory
    var searchterms = getSearchTerms();     //Get the search terms
    var setColumns = '';                    //Set the number of columns to blank

    set_pagination_amount_menu(limit);

    //console.log(searchterms);

    var data = {
            page: page,
            lim: limit,
            catv: category,
            subv: subcategory,
            search: searchterms,
            lang: current_language          //Defined in default.php
        };

    $.ajax({
        type: 'POST',
        url: '/index.php?option=com_axs&task=videos.getMedia&format=raw&lang=' +  JLANGUAGE.split('-')[0],
        data: data,
        success: function(response) {
            //console.log(response);
            //Can only get a list of all classes, so have to assign to an array index
            var container = document.querySelector('.tov-ml-content');
            container.innerHTML = response;

            //set the title
            var page_title = document.querySelector('#page_title');
            var title = document.querySelector('#title');
            page_title.innerHTML = title.innerHTML;
            container.removeChild(title);

            //re-create the pagination
            var total = document.querySelector('#total');
            var actualTotal = total.innerHTML;
            if(actualTotal > 0) {
              make_pagination(actualTotal);
            } else {

            }
            container.removeChild(total);

            var new_message = document.querySelector('.new_message');
            if (new_message) {
                var message_center = document.querySelector('#message_center');
                message_center.innerHTML = "";
                message_center.appendChild(new_message);
            }

            /*
                On success, hide the loading gif and show all of the content.
            */

            var imgs = container.querySelectorAll('img');
            var count = 0;

            var show_content = function() {
                $(".hide_on_load").show();
                $(".show_on_load").hide();
                refreshBlocksit(get_num_columns(), 'div', '.tov-ml-content');
            }

            setTimeout(function(){
                show_content();
            },700)

            /* if(imgs.length == 0) {
                show_content();
            } else {
                imgs.forEach(function(img) {
                    img.onload = function() {
                        count++;
                        if (count == imgs.length) {
                            show_content();
                        }
                    };
                });
            } */

            var page_limit = getPageLimit();
            var num_videos = 0;

            //Page limit is number of videos / number of pages.
            //All videos = number of pages of -1
            if (page_limit < 0) {
                num_videos = -page_limit;
            } else {
                num_videos = page_limit * getLimit();
            }

            /*

             If there are too many videos, there will be an undesired "juggling"
             effect as all of the videos transition to find their place.
             So, the more videos there are, the shorter the time should be to minimize this.

             20 videos = 1 second transition
             50 videos = 0.5 second transition
             80 videos = 0 second transition

             (80 - 20) / 60 = 60 / 60 = 1
             (80 - 50) / 60 = 30 / 60 = 0.5
             (80 - 80) / 60 = 0 / 60 = 0
            */
            var time = (80 - num_videos) / 60;
            if (time < 0) {
                time = 0;
            }

            setBlocksitTransitionSpeed(time, time);
            $('.tov-ml-content').prepend($('.hasaccess'));
        }
    });
}

function get_num_columns() {
    var winWidth = $('.tov-ml-content').width();
    var columns = Math.floor(winWidth / 400);
    if (columns < 1) {
        columns = 1;
    }
    return columns;
}

function changeCategory(category, subcategory) {
    if (category) {
        setCategory(category);
        if (subcategory) {
            setSubcategory(subcategory);
        } else {
            setSubcategory(0);
        }
    }
}

/*
    Creates the list of buttons at the bottom of the page.
*/
function make_pagination(video_count) {

    var current_page = parseInt(getPage());             //Get the current page.

    var limit = $("#pagination_amount").val();          //The limit is embedded into the HTML from PHP.  Get it and store it into a variable.
    var num_pages = Math.ceil(video_count / limit);     //Get the number of pages.

    setPageLimit(num_pages);                            //Store the page limit

    for (b = 0; b < 2; b++) {

        var button_area;

        //Get the div that the buttons will live in.
        if (b == 0) {
            button_area = document.getElementById("pagination_buttons");
        } else {
            button_area = document.getElementById("pagination_buttons_bottom");
        }

        button_area.className += " pagination";

        $(button_area).html("");                            //Clear it of any previous hrml.

        //Create a div for the list of buttons and add it to the area.

        button_list = document.createElement("div");
        button_list.className += " pagination-list";
        button_area.appendChild(button_list);

        //If there is only 1 page (or less), don't create or show the button area.
        if (num_pages <= 1) {
            $(button_area).hide();
            continue;
        } else {
            $(button_area).show();
        }

        $(button_area).css('margin-top', '10px');

        //A small function that creates a clickable link button
        var make_button = function(className) {
            var button = document.createElement("a");
            if (className != '') {
                button.className += " " + className;
            }

            return button;
        }

        //A button to return to the first page.
        var start_button = make_button("pagenav lizicon-first");
        start_button.title = "First Page";
        $(start_button).click(
            function () {
                changePage(0);
            }
        );

        //A button to back up one page.
        var back_button = make_button("pagenav lizicon-backward2");
        back_button.title = "Previous Page";
        $(back_button).click(
            function() {
                changePage(current_page - 1);
            }
        );

        //If the current page is the first page, gray out the buttons.
        if (current_page == 0) {
            $(start_button).css("background-color", "rgb(80,80,80)");
            $(back_button).css("background-color", "rgb(80,80,80)");
        }

        //Add the buttons.
        button_list.appendChild(start_button);
        button_list.appendChild(back_button);

        //Make a button for each page.
        for (i = 0; i < num_pages; i++) {
            var button_div = make_button("pagenav");
            button_div.textContent = (i + 1);
            button_div.title = "Page: " + (i + 1);

            //If the button is for the current page, highlight it.
            if (i == current_page) {
                $(button_div).css(
                    'background-color', 'rgb(200, 200, 200)'
                );
            }

            //Attach a script that changes the page when clicked on.
            var setClick = function(id, i) {
                return function() {
                    $(button_div).click(
                        function() {
                            changePage(i);
                        }
                    );
                }
            }

            button_list.appendChild(button_div);
            setClick(button_div.id, i)();
        }

        //Create a button to move one page forward.
        var next_button = make_button("pagenav lizicon-forward3");
        next_button.title = "Next Page";
        $(next_button).click(
            function() {
                changePage(current_page + 1);
            }
        );

        //Create a button to move to the end.
        var last_button = make_button("pagenav lizicon-last");
        last_button.title = "Last Page";
        $(last_button).click(
            function () {
                changePage(num_pages - 1);
            }
        );

        //If the page is currently the last one, gray out the buttons.
        if (current_page == num_pages - 1) {
            $(next_button).css("background-color", "rgb(80,80,80)");
            $(last_button).css("background-color", "rgb(80,80,80)");
        }

        //Add them to the area.
        button_list.appendChild(next_button);
        button_list.appendChild(last_button);

    }
}

//Make a text string for the url based on the current settings.
function makeUrl() {
    var url = 'mediawall';

    //url += "#language=" + getLanguage();
    url += "#page=" + getPage();
    url += "#limit=" + getLimit();
    url += "#category=" + getCategory();
    url += "#subcategory=" + getSubcategory();
    url += "#searchterms=" + getSearchTerms();

    return url;
}

//Change the actual browser url to reflect the current settings.
function setUrl() {

    //Get the current URL
    var curPage = getCurrentUrl();

    //Make a new url
    var url = makeUrl();

    //Only change the url if the current and new do NOT match.  Otherwise, the browser history will contain lots of duplicates.
    if (curPage != url) {
        ChangeUrl("video page " + getPage(), url);
        //setCurrentUrl(url);
    }
}

//Change the current page to the newly supplied page.
function changePage(whichPage) {

    //If no page is supplied, make it 0.
    if (!whichPage) {
        whichPage = 0;
    }

    var num_pages = getPageLimit();  //Get the page limit.

    //Do not proceed if a negative page number is supplied, or if the page is past the page limit.
    if ((whichPage < 0) || (whichPage > num_pages - 1)) {
        return;
    }

    //Otherwise, save the new page to local storage and change the url.
    setPage(whichPage);
    setUrl();
}

/*
    Functions to get and set the variables to and from local storage

    category        -       the current category
    subcategory     -       the current subcategory
    limit           -       the number of items listed per page (set by the drop down list)
    page limit      -       the maximum number of pages to list the current content
    language        -       english or french
    page            -       the page number to view
    url             -       the current url of the browser
*/

function getSearchTerms() {
    var terms = search_terms;
    if(!terms) {
        terms = '';
        search_terms = terms;
    }
    return terms;
}

function setSearchTerms(terms) {
    search_terms = terms;
}

function getCategory() {
    var category = video_category;
    if (!category) {
        category = 0;
        video_category = category;
    }

    return category;
}

function setCategory(newCategory) {
    video_category = newCategory;
}

function getSubcategory() {
    var subcategory = video_subcategory;
    if (!subcategory) {
        subcategory = 0;
        video_subcategory = subcategory;
    }

    return subcategory;
}

function setSubcategory(newSubcategory) {
    video_subcategory = newSubcategory;
}

function getLimit() {
    var limit = video_limit;
    if (!limit) {
        limit = 50;
        video_limit = limit;
    }

    return limit;
}

function setLimit(newLimit) {
    video_limit = newLimit;
}

function getPageLimit() {
    var pageLimit = video_page_limit;
    if (!pageLimit) {
        //pageLimit = 50;
        pageLimit = 0;
        video_page_limit = pageLimit;
    }

    return pageLimit;
}

function setPageLimit(newPageLimit) {
    video_page_limit = newPageLimit;
}

function getPage() {
    var page = video_page;
    if (!page) {
        page = 0;
        video_page = page;
    }

    return page;
}

function setPage(newPage) {
    video_page = newPage;
}

function getCurrentUrl() {
    currentPage = current_video_url;

    if (!currentPage) {
        currentPage = makeUrl();
        current_video_url = currentPage;
    }

    return currentPage;
}

function setCurrentUrl(newUrl) {
    current_video_url = newUrl;
}