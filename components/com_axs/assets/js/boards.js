var whats_this_text = null;
var num_columns = 0;
var column_max = 0;
var run_charts_function = false;
var board_type = null;
var first_comments_load = true;
//var board_num = 0;

$(document).ready(function() {
    
    pulse_init();

    var whats_this = document.getElementById("inspiration_info");

    if (whats_this != null && whats_this_text != null) {
        whats_this.appendChild(document.createTextNode(whats_this_text));
    }
});

function set_whats_this(whats_this) {
    whats_this_text = whats_this;
}

function show_content() {

    setInterval(
        function() {
            refreshBlocksit(get_num_columns(), 'div', '#block-container');
        }, 200
    );

    $(".show_on_reload").hide();
    $(".hide_on_reload").show();

    refreshBlocksit(get_num_columns(), 'div', '#block-container');
}

function set_pulse_type(type) {
	board_type = type;	
}

function set_column_max(max) {
	if (max > 0) {
		column_max = max;
	}
}

function hide_content() {
    $(".show_on_reload").show();
    $(".hide_on_reload").hide();
}

function get_num_columns() {
	
	var winWidth = $(window).width();

    var columns = Math.floor(winWidth / 500) + num_columns;

    if (columns < 1) {
        columns = 1;
        num_columns++;
    }

    if (columns > 10) {
        columns = 10;
        num_columns--;
    }

    return columns;
}

function set_has_charts(bool) {
	if (bool == true || bool == false) {
		run_charts_function = bool;
	}
}

function refresh_search(limit, offset, search_string, team) {

    hide_content();

    $.ajax({
        type: 'POST',
        url: "index.php?option=com_axs&task=boards.refreshPosts&format=raw",
        data: {        	
            cat :       search_string,
            limit:      limit,
            offset:     offset,
            type: 		board_type,
            team:       team,
            board: 		localStorage.getItem("encrypted_board_id")
        },
        success: function(response) {

            var board_container = document.getElementById('board_container');

            var tempDiv = document.createElement('div');
            tempDiv.innerHTML = response;

            var new_block_container = $(tempDiv).find('#block-container');
            var count = new_block_container.attr('value');
            
            board_container.innerHTML = response;
                
            make_pagination(count);

            show_content();

            if (first_comments_load) {
                first_comments_load = false;                
            } else {
                refreshComments();
            }
            

            if (run_charts_function) {
            	run_charts();
            }
        }
    });

    $.ajax({
        type: 'POST',
        url: "index.php?option=com_axs&task=boards.getTeamMembers&format=raw",
        data: {
            board:      localStorage.getItem("encrypted_board_id")
        },
        success: function(response) {

            if (!response) {
                return;
            }
            
            let memberData = JSON.parse(response);

            let members = memberData.members;
            let leaders = memberData.leaders;

            let selector = document.getElementById("team_select");
            let team_ids = [];

            if (members) {
                team_ids = Object.keys(members);
            }

            if (team_ids.length > 1) {

                selector.length = 1;
                for (let i = 0; i < team_ids.length; i++) {

                    let id = team_ids[i];

                    let newOption = document.createElement('option');
                    newOption.appendChild(document.createTextNode(members[id].title));                    
                    newOption.value = id;

                    if (team == id) {
                        newOption.setAttribute("selected", "true");
                    }

                    selector.appendChild(newOption);
                }

                $(selector.parentNode).show();
            } else {
                $(selector.parentNode).hide();
            }
        }
    });
}

function run_charts(){
    $('.percentage').easyPieChart( {
        animate: 1000,
        lineWidth: 15,
        barColor: '#31a3fa',
        trackColor:'#999',
        lineCap:'butt',
        scaleColor:'#777',
        onStep: function(value) {
                    this.$el.find('span').text(Math.round(value));
                },
        onStop: function(value, to) {
            this.$el.find('span').text(Math.round(to));
        }
    });
}

function view_post_notes(item) {
    var post = item.getAttribute("post");
    //console.log('view ' + post);

    $.ajax({
        type: 'POST',
        url: "index.php?option=com_axs&task=boards.getPostNotesAdmin&format=raw",
        data: {
            board:      localStorage.getItem("encrypted_board_id"),
            post:       post
        },
        success: function(response) {
            if (!response) {
                return;
            }

            try {
                response = JSON.parse(response);
                console.log(response);

                var notes = document.getElementById("notes_" + post);            
                var container = notes.getElementsByClassName('note_container')[0];
                container.innerHTML = "";

                let table = document.createElement('table');
                let newRow = document.createElement('tr');
                let newDateHead = document.createElement('th');
                let newNoteHead = document.createElement('th');

                table.className = "note_table";

                newDateHead.appendChild(document.createTextNode('Date'));
                newNoteHead.appendChild(document.createTextNode('Note'));
                newDateHead.className = "note_table_date_head";
                newNoteHead.className = "note_table_text_head";

                container.appendChild(table);
                table.appendChild(newRow);
                newRow.appendChild(newDateHead);
                newRow.appendChild(newNoteHead);

                for (var i = 0; i < response.length; i++) {
                    let newRow = document.createElement('tr');
                    let newDate = document.createElement('td');
                    let newNoteContainer = document.createElement('td');
                    let newNote = document.createElement("textarea");

                    newDate.appendChild(document.createTextNode(response[i].date));
                    newNoteContainer.appendChild(newNote);
                    newNote.appendChild(document.createTextNode(response[i].note));
                    newDate.className = "note_table_date";
                    newNote.className = "note_table_text";

                    newRow.appendChild(newDate);
                    newRow.appendChild(newNoteContainer);
                    table.appendChild(newRow);
                }

                $(notes).show(250);
            } catch (err) {
                console.log(err.message);
            }            
        }
    });

}

function delete_post(item) {
    if (confirm("Are you sure you want to delete this post?\nThis action cannot be undone.")) {
        var post = item.getAttribute('post');
        var target = item.getAttribute('target');
        $.ajax({
            type: 'POST',
            url: "index.php?option=com_axs&task=boards.deletePostAdmin&format=raw",
            data: {
                board:      localStorage.getItem("encrypted_board_id"),
                post:       post
            },
            success: function(response) {
                if (response) {
                    $("#" + target).remove();
                }
            }
        });
    }    
}

function admin_close_notes(item) {
    $(item.parentNode).hide(250);
}
