$(function() {
    function sendSurveyData(form) {
        var question_type = $(form).find("input[name='question_type']").val();
        var survey = $(form).find("input[name='survey_data']").val();
        var surveyLessonData = $(form).parent().parent().parent().data('lesson');
        var likertScore = '';
        if(question_type == 'likert' || question_type == 'multiplechoice') {
            var answer = $(form).find("input[name='survey_answer']:checked").val();
        } else {
            var answer = $(form).find("#survey_answer").val();
        }
        if(question_type == 'likert') {
            likertScore = $(form).find("input[name='survey_answer']:checked").data('index');
        }
        if(!answer) {
            alert(NEED_TO_ANSWER_SURVEY_QUESTION_MESSAGE);
        } else {
            $(form).find('.survey_submit_button').hide();
            $(form).find('.ajax-load-more').show();

            if(typeof surveyLessonData !== 'undefined') {
                lessonData = surveyLessonData;
            } else {
                lessonData = '';
            }
            $.ajax({
                type : 'POST',
                url  : '/index.php?option=com_axs&task=surveys.submitAnswer&format=raw',
                data : {
                    survey : survey,
                    answer : answer,
                    lesson : lessonData,
                    likertScore : likertScore
                }
            }).done(function(response) {
                var result = JSON.parse(response);
                console.log(result);
                if(result.message == 'success') {
                    $(form).find('.ajax-load-more').hide();
                    if(result.question && result.status == 'incomplete') {
                        $(form).find('.tovuti_survey_question_container').html(result.question);
                        $(form).find('.survey_submit_button').show();
                    }
                    if(result.status == 'completed') {
                        $(form).find('.tovuti_survey_question_container').html(result.completed_message);
                        if(typeof surveyLessonData !== 'undefined') {
                            if(result.completed && result.activity_id && result.runCheckComplete) {
                                console.log('Checking for Completion...');
                                console.log(result);
                                if(!$('.activity_'+result.activity_id).hasClass('ic_completed_active')) {
                                    $('.activity_'+result.activity_id).text('Completed');
                                    $('.activity_'+result.activity_id).addClass('ic_completed_active');
                                    completedAudio.play();
                                }
                                if($('.required_'+result.activity_id).hasClass('ic_required_active')) {
                                    $('.required_'+result.activity_id).removeClass('ic_required_active');
                                }

                                checkComplete(true);
                            }
                        }
                    }
                } else {
                    alert(ERROR_SUBMITTING_ANSWER_MESSAGE);
                }
            });
        }

    }

    $('.survey_submit_button').click(function(){
        sendSurveyData($(this).parent());
    });
});