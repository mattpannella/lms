<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die();

class AxsModelVideos extends FOFModel {

	public function getCategoryData($ids_only = false) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();
        $user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);
        $conditions[] = $db->qn('enabled').'='.(int)1;
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

        $query
            ->select('*')
            ->from($db->quoteName('media_categories'))
            ->where($conditions)
            ->order('ordering ASC');
        $db->setQuery($query);

        $categories = $db->loadObjectList();

        if($ids_only) {
            $category_ids = array();
            foreach($categories as $category) {
                array_push($category_ids, $category->id);
            }
            return $category_ids;
        } else {
            return $categories;
        }
    }

    public function buildSeachQuery(&$conditions, $search_terms, $media_type) {
        $db = JFactory::getDbo();
        if($search_terms) {

            $search_term = $search_terms;

            //if it's in quotes, do an exact match.
            preg_match_all('/\"([\w ]+)\"/', $search_term, $matches, PREG_SET_ORDER);
            foreach($matches as $q) {
                $conditions[] = "(".$db->qn('title')." LIKE '%$q[1]%' OR ".$db->qn('speaker')." LIKE '%$q[1]%' OR ".$db->qn('tags')." LIKE '%$q[1]%' OR ".$db->qn('description')." LIKE '%$q[1]%')";
            }
            //now remove it from the search terms
            $search_term = preg_replace('/\"([\w ]+)\"/', '', $search_term);

            //if it has two words separated by an "and or + symbol" it must have both. as in "troy and bob" or "troy + bob"
            preg_match_all('/(\w+)( +and +| *\+ *)(\w+)/', $search_term, $matches, PREG_SET_ORDER);
            foreach($matches as $q) {
                $mArray = array();
                $mArray[] = $db->qn('title')." LIKE '%$q[1]%' AND ".$db->qn('title')." LIKE '%$q[3]%'";
                $mArray[] = $db->qn('speaker')." LIKE '%$q[1]%' AND ".$db->qn('speaker')." LIKE '%$q[3]%'";
                $mArray[] = $db->qn('tags')." LIKE '%$q[1]%' AND ".$db->qn('tags')." LIKE '%$q[3]%'";
                $mArray[] = $db->qn('description')." LIKE '%$q[1]%' AND ".$db->qn('description')." LIKE '%$q[3]%'";
                $conditions[] = '('.implode(' OR ', $mArray).')';
            }
            //now remove it from the search terms
            $search_term = preg_replace('/(\w+)( +and +| *\+ *)(\w+)/', '', $search_term);

            //if it has two words separated by an "_ symbol" it can have either (or). as in "troy _ bob" or "troy_bob"
            preg_match_all('/(\w+) *_ *(\w+)/', $search_term, $matches, PREG_SET_ORDER);
            foreach($matches as $q) {
                $mArray = array();
                $mArray[] = $db->qn('title')." LIKE '%$q[1]%' OR ".$db->qn('title')." LIKE '%$q[2]%'";
                $mArray[] = $db->qn('speaker')." LIKE '%$q[1]%' OR ".$db->qn('speaker')." LIKE '%$q[2]%'";
                $mArray[] = $db->qn('tags')." LIKE '%$q[1]%' OR ".$db->qn('tags')." LIKE '%$q[2]%'";
                $mArray[] = $db->qn('description')." LIKE '%$q[1]%' OR ".$db->qn('description')." LIKE '%$q[2]%'";
                $conditions[] = '('.implode(' OR ', $mArray).')';
            }
            //now remove it from the search terms
            $search_term = preg_replace('/(\w+) *_ *(\w+)/', '', $search_term);

            //all remaining words
            $search_term = str_replace(',', ' ', $search_term);
            $search_term = preg_replace('/\s+/',',',trim($search_term));
            $searchArray = explode(',', $search_term);
            foreach($searchArray as $term) {
                $conditions[] = "(".$db->qn('title')." LIKE '%$term%' OR ".$db->qn('speaker')." LIKE '%$term%' OR ".$db->qn('tags')." LIKE '%$term%' OR ".$db->qn('description')." LIKE '%$term%')";
            }
        }
        if($media_type) {
            $conditions[] = $db->qn('media_type').'='.$db->q($media_type);
        }
    }

    public function getMediaCount($search_terms, $language, $category = null, $subcategory = null) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array(
          $db->qn('language') . '=' . $db->q($language),
          $db->qn('archive_date') . '=' . $db->q('0000-00-00 00:00:00')
        );

        if ($category) {
            $conditions[] = "(FIND_IN_SET('$category', category) > 0)";
            if ($subcategory) {
                $conditions[] = "(FIND_IN_SET('$subcategory', subcategory) > 0)";
            }
        }

        $this->buildSeachQuery($conditions, $search_terms);

        $user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

        $accessCategoryConditions = $this->getCategoryConditions();
        if($accessCategoryConditions) {
            array_push($conditions,$accessCategoryConditions);
        }

        $query->select('COUNT(*) as count')
          ->from($db->quoteName('axs_gin_media'));

        if (count($conditions) > 0) {
            $query->where($conditions);
        }

        $db->setQuery($query);
        $res = $db->loadObject();
        return $res->count;
    }

    private function getCategoryConditions() {
        $accessibleCategoriesArray = $this->getCategoryData(true);
        if(!$accessibleCategoriesArray) {
            return;
        }
        $accessibleCategoriesConditions = array();
        foreach($accessibleCategoriesArray as $accessibleCategory) {
            $accessibleCategoriesConditions[] = "(FIND_IN_SET('$accessibleCategory', category) > 0)";
            $accessibleCategoriesConditions2[] = " ( (FIND_IN_SET('$accessibleCategory', subcategory) > 0) OR subcategory = '' OR subcategory = 0 ) ";
        }
        $accessibleCategoriesConditionsList = implode('OR',$accessibleCategoriesConditions);
        $accessibleCategoriesConditionsList2 = implode('OR',$accessibleCategoriesConditions2);
        $conditions = " ( ($accessibleCategoriesConditionsList) AND ($accessibleCategoriesConditionsList2) ) ";
        return $conditions;
    }

    public function getMediaList($search_terms, $language, $offset = 0, $limit = null, $category = null, $subcategory = null,$media_type) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array(
            "(go_live_date = '0000-00-00 00:00:00' OR date(go_live_date)  <= '".date('Y-m-d')."')",
            "(archive_date = '0000-00-00 00:00:00' OR date(archive_date)  > '".date('Y-m-d')."')",
            "(enabled = 1)"
        );
        if($language) {
            $conditions[] = $db->qn('language') . '=' . $db->q($language);
        }
        if ($category) {
            $conditions[] = "(FIND_IN_SET('$category', category) > 0)";
            if ($subcategory) {
                $conditions[] = "(FIND_IN_SET('$subcategory', subcategory) > 0)";
            }
        }

        $this->buildSeachQuery($conditions, $search_terms, $media_type);

        $user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

        $accessCategoryConditions = $this->getCategoryConditions();
        if($accessCategoryConditions) {
            array_push($conditions,$accessCategoryConditions);
        }

        $query
            ->select('*')
            ->from($db->qn('axs_gin_media'))
            ->order('ordering ASC');

        if (count($conditions) > 0) {
            $query->where($conditions);
        }

        if (($offset == null) || ($offset < 0)) {
            $offset = 0;
        }

        if (($limit == null) || ($limit < 0)) {
            $limit = 0;
        }
        //echo $query; //die();
        $db->setQuery($query, $offset, $limit);
        return $db->loadObjectList();
    }

    public function getMedia($search_terms, $language, $offset = 0, $limit = null, $category = null, $subcategory = null, $media_type) {

        $media = $this->getMediaList($search_terms, $language, $offset, $limit, $category, $subcategory,$media_type);

        /* if(!count($media) && $language == 'french') {
	        $media = $this->getMediaList($search_terms, 'english', $offset, $limit, $category, $subcategory);
        } */

        //$total = $this->getMediaCount($search_terms, $language, $category, $subcategory);

        $tmp = new stdClass();
        $tmp->media = $media;
        //$tmp->total = $total;
        return $tmp;
    }
}