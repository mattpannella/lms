<?php

defined('_JEXEC') or die();

require_once "components/shared/controllers/common.php";
require_once "components/shared/controllers/comments.php";
require_once "components/shared/controllers/likes.php";
require_once "components/shared/controllers/rating.php";
require_once "components/shared/controllers/category.php";

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsModelBoards extends FOFModel {

    public function getBoardData($id = null) {

        if (!$id) {
            $id = JRequest::getVar('id');
        }

        $db = JFactory::getDBO();
        
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_board_templates'))
            ->where($db->quoteName('id') . '=' . $db->quote($id));

        $db->setQuery($query);
        $result = $db->loadObject();

        if (!$result) {
            return null;
        }

        $board_data = json_decode($result->data);
        $board_data->categories = json_decode($result->categories);

        return $board_data;
    }

    public function getBoardAccessData($id = null) {

        if (!$id) {
            $id = JRequest::getVar('id');
        }

        if (!$id) {
            /*
                This could possibly be utilized later to make a list of ALL boards.  But for now, exit.
            */
            //JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
            return false;
        }

        //$item = $this->items[0];
        $db = JFactory::getDBO();
        
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_board_templates'))
            ->where($db->quoteName('id') . '=' . $db->quote($id));

        $db->setQuery($query);
        $item = $db->loadObject();

        //If an ID is specified, the only item returned is the one that matches.  If there's no match, it was invalid.

        if (!$item) {
            JError::raiseWarning(404, JText::_('JERROR_LAYOUT_PAGE_NOT_FOUND'));
            return false;
        }
            
        $item->data = json_decode($item->data);

        $settings = $item->data->settings;

        if (isset($settings->access_type)) {
            $access_type = $settings->access_type;
        } else {
            //There is no access type set, so there are no restrictions.
            //return $item;
            $access_type = "none";
        }

        $user = JFactory::getUser();

        $has_board_access = false;
        $has_admin_access = false;
        $has_report_access = false;
        $teammember = null;
        $teamlead = null;

        //Start by setting it to the admin access array merged with the reporting access array
        //Do not merge an array with null, or it will result in null
        if (is_array($settings->admin->access_type)) {
            if (is_array($settings->report->access_type)) {
                $access_list = array_merge($settings->admin->access_type, $settings->report->access_type);
            } else {
                $access_list = $settings->admin->access_type;
            }
        } else {
            if (is_array($settings->report->access_type)) {
                $access_list = $settings->report->access_type;
            } else {
                $access_list = array();
            }
        }
        
        $access_list[]= $access_type;                       //Add the board access type
        $access_list = array_unique($access_list);          //Make it unique

        foreach ($access_list as $access) {
            switch ($access) {
                case "list":
                    //They need to belong to a specific list of users.
                    
                    if ($access == $access_type) {
                        $list = explode(",", $settings->userlist);

                        if (in_array($user->id, $list)) {
                            $has_board_access = true;
                        }
                    }

                    if (in_array($access, $settings->admin->access_type)) {
                        $list = explode(",", $settings->admin->userlist);
                        if (in_array($user->id, $list)) {
                            $has_admin_access = true;
                        }
                    }

                    if (in_array($access, $settings->report->access_type)) {
                        $list = explode(",", $settings->report->userlist);
                        if (in_array($user->id, $list)) {
                            $has_report_access = true;
                        }
                    }
                    
                    break;

                case "access":
                    //They need to have a specific access level.
                    
                    $user_access = $user->getAuthorisedViewLevels();
                    
                    $board_access_levels = $settings->access;
                    $admin_access_levels = $settings->admin->accesslevel;
                    $report_access_levels = $settings->report->accesslevel;

                    foreach ($user_access as $ua) {
                        if ($access == $access_type) {
                            //Check member access
                            if (in_array($ua, $board_access_levels)) {
                                $has_board_access = true;
                            }
                        }

                        if (in_array($access, $settings->admin->access_type)) {
                            //Check admin access
                            if (in_array($ua, $admin_access_levels)) {
                                $has_admin_access = true;
                            }
                        }

                        if (in_array($access, $settings->report->access_type)) {
                            //Check report access
                            if (in_array($ua, $report_access_levels)) {
                                $has_report_access = true;
                            }
                        }
                    }

                    break;

                case "user":
                    $user_access = $user->getAuthorisedGroups();

                    $board_user_groups = $settings->usergroup;
                    $admin_user_groups = $settings->admin->usergroup;
                    $report_user_groups = $settings->report->usergroup;

                    foreach ($user_access as $ua) {
                        if ($access == $access_type) {
                            if (in_array($ua, $board_user_groups)) {
                                $has_board_access = true;
                            }
                        }

                        if (in_array($access, $settings->admin->access_type)) {
                            if (in_array($ua, $admin_user_groups)) {
                                $has_admin_access = true;
                            }
                        }

                        if (in_array($access, $settings->report->access_type)) {
                            if (in_array($ua, $report_user_groups)) {                                
                                $has_report_access = true;
                            }
                        }
                    }

                    break;

                case "teams":

                    if ($access_type !== "teams") {
                        //  This will happen if the admin/report access is set to "Team Leaders" but the board's access
                        //  is not set to teams (so there's no way to know which teams to use).
                        break;
                    }

                    switch ($settings->teamselect) {
                        case "specific":
                            $teamlist = $settings->teamlist;
                            break;
                        default:
                            $teamlist = null;
                            break;
                    }

                    $conditions = array(
                        $db->quoteName('published') . '=1',
                        "(
                            FIND_IN_SET(" . $db->quote($user->id) . ", " . $db->quoteName('user_ids') . ") OR
                            FIND_IN_SET(" . $db->quote($user->id) . ", " . $db->quoteName('team_lead') . ")
                        )"
                    );

                    if ($teamlist) {
                        $conditions []= "FIND_IN_SET(" . $db->quoteName('id') . "," . $db->quote(implode(",", $teamlist)) . ")";
                    }

                    $query->clear();
                    $query
                        ->select('*')
                        ->from($db->quoteName('axs_teams'))
                        ->where($conditions);

                    $db->setQuery($query);
                    $teams = $db->loadObjectList();

                    $teammember = array();          //The teams that the user is a member of
                    $teamlead = array();            //The teams that the user is a leader of

                    $is_leader = false;
                    $is_member = false;

                    foreach ($teams as $team) {
                        $member_ids = explode(",", $team->user_ids);
                        $leader_ids = explode(",", $team->team_lead);

                        if (!$is_leader) {
                            //Only check if false, so it's not set back to false once true.
                            $is_leader = in_array($user->id, $leader_ids);
                        }

                        if (!$is_member) {
                            $is_member = in_array($user->id, $member_ids);
                        }

                        if ($is_leader) {
                            $teamlead[$team->id] = new stdClass();
                            $teamlead[$team->id]->title = $team->title;
                            $teamlead[$team->id]->ids = $leader_ids;
                        }

                        //Let them see the members if they're a member or a leader
                        if ($is_member || $is_leader) {
                            $teammember[$team->id] = new stdClass();
                            $teammember[$team->id]->title = $team->title;
                            $teammember[$team->id]->ids = $member_ids;
                        }
                    }

                    if ($is_member && $access == $access_type) {
                        $has_board_access = true;
                    }

                    if ($is_leader && in_array($access, $settings->admin->access_type)) {
                        $has_admin_access = true;
                    }

                    if ($is_leader && in_array($access, $settings->report->access_type)) {                        
                        $has_report_access = true;
                    }

                    break;

                case "none":
                    $has_board_access = true;

            }
        }

        $editing = JRequest::getVar('edit');    //They don't have access to edit if they're only an admin.
        if ($editing == "1") {
            $editing = true;
        } else {
            $editing = false;
        }
        
        $boardData = new stdClass();
        $boardData->admin = $has_admin_access;
        $boardData->board = $has_board_access;
        $boardData->report = $has_report_access;
        $boardData->editing = $editing;        
        $boardData->item = $item;

        if (!isset($_SESSION['user_board'])) {
            $_SESSION['user_board'] = array();
        }

        if (!isset($_SESSION['user_board'][$id])) {
            $_SESSION['user_board'][$id] = array();            
            $_SESSION['user_board'][$id] = new stdClass();            
        }

        $_SESSION['user_board'][$id]->teammembers = $teammember;
        $_SESSION['user_board'][$id]->teamleaders = $teamlead;
        $_SESSION['user_board'][$id]->admin_access = $has_admin_access;
        $_SESSION['user_board'][$id]->board_access = $has_board_access;
        $_SESSION['user_board'][$id]->report_access = $has_report_access;
        $_SESSION['user_board'][$id]->access_type = $access_type;

        return $boardData;

    }    

    public function showDeclarations($board_id, $limit = 0, $offset = 0, $searchList = null, $team = null) {

        if (!$board_id) {
            return;
        }        

        $model = FOFModel::getTmpInstance("Boards", "AxsModel");
        $data = $model->getBoardData($board_id);

        if (!$data) {
            return;
        }

        $font = $data->font;
        $board = $data->goal;

        $section = "declaration";
        
        $categories = getCategories();

        $search = array();
        foreach ($categories as $cat) {
            $search[$cat->name] = $cat->id;
        }

        $user = JFactory::getUser();
        $user_id = $user->id;
        $date = strtotime('now');

        $db = JFactory::getDBO();

        //Explode the comma separated string into an array.
        if ($searchList) {
            $searchList = explode(",", $searchList);
        }

        //Check if 'friends' is selected and pull it out, since it isn't a category.
        $friends_index = array_search('friends', $searchList);
        if ($friends_index) {
            $friends = getFriends($user_id);
            unset($searchList[$friends_index]);
        }

        $query = $db->getQuery(true);

        $query
            ->select('*, board.id as dec_id')
            ->from($db->quoteName('declaration_board') . ' AS board')
            ->join('inner', '`#__users` AS users ON users.id = board.user_id');

        $conditions = $db->quoteName('board.public') . '=' . $db->quote('yes');
        $conditions .= ' AND ';
        $conditions .= '(' . $db->quoteName('board.deleted') . ' IS NULL OR ' . $db->quoteName('board.deleted') . '="")';
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('board.board_id') . '=' . $db->quote($board_id);
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('users.block') . '=' . $db->quote(0);

        $count = count($searchList);

        if ($count > 0) {
            
            $conditions .= " AND (";

            for ($i = 0; $i < $count - 1; $i++) {
                $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$i]]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$count - 1]]);
            $conditions .= ")";
        }

        if (isset($friends)) {
            $conditions .= " AND (";

            $count = count($friends);

            for ($i = 0; $i < $count - 1; $i++) {
                $conditions .= $db->quoteName('board.user_id') .'=' . $db->quote($friends[$i]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.user_id') . '=' . $db->quote($friends[$count - 1]);
            $conditions .= ")"; 
        }

        $conditions .= self::getTeamConditions($team, $board_id);

        $query->where($conditions);                 
        $query->order($db->quoteName('board.id') . ' DESC');

        $db->setQuery($query);        
        $rows = $db->loadObjectList();

        $declaration_count = count($rows);
        
        $offset = $offset * $limit;
        
        $db->setQuery($query, $offset, $limit);
        $rows = $db->loadObjectList();

        if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {
            $is_admin = $_SESSION['user_board'][$board_id]->admin_access;
        }

        ?>

        <div id="block-container" class="hide_on_reload" style="display:none;" value="<?php echo $declaration_count ?>">

        <?php

        if ($rows != null) {
            foreach ($rows as $row) {
                $goal_id = $row->dec_id;
                $post_user_id = $row->user_id;
                $declaration = $row->declaration; 
                $goal_amount = $row->goal_amount;
                $category = $row->category;
                $excluded_days = $row->excluded_days;
                $start_date = $row->start_date;
                $end_date = $row->end_date;
                $declaration_date = $row->date;
                $status = $row->status;
                $goal_type = $row->goal_type;
                $progress_type = $row->progress_type;                       

                /*
                    $goal_type == 'goal'                        //A goal to accomplish within a certain time frame.     


                        $progress_type == 'progressive'         //A progressive goal that they can work toward.                             ie. lose 20 lbs in 3 months.
                        $progress_type == 'single'              //A goal that is accomplished at once.                                      ie. buy a new car this year

                    $goal_type == 'declaration'                 //Something the person aspires to do

                        $progress_type == 'ongoing'             //A declaration that has no end.                                            ie. never smoke again.
                        $progress_type == 'accomplishable'      //Something that can they can aspire to acoomplish with no time frame.      ie. adopt a baby

                */
                
                $query = $db->getQuery(true);
                $query
                    ->select("sum(update_amount) AS progress")
                    ->from($db->quoteName('declaration_updates'))
                    ->where(array(
                        $db->quoteName('user_id') . '=' . $db->quote($post_user_id),
                        $db->quoteName('goal_id') . '=' . $db->quote($goal_id)
                    ));

                
                $db->setQuery( $query );     
                $updates = $db->loadObjectList();
                
                if ($updates != null) {
                    foreach ($updates as $update) {
                        $progress = $update->progress;
                        $datediff = (strtotime($start_date) - strtotime($end_date));
                        $datediff = floor($datediff / (60 * 60 * 24));
                        $datediff = $datediff - $excluded_days;
                        $now = time();                        
                        $time_left = strtotime($end_date) - $now;
                        $daysLeft = floor($time_left / (60 * 60 * 24));
                    }
                }

                if ($goal_amount != 0) {
                    $percentage = $progress / $goal_amount;
                    $percentage = $percentage * 100;
                } else {
                    $percentage = 0;
                }

                if ($percentage >= 100) {
                    $percentage = 100;
                    $success = true;
                } else {
                    $success = false;
                }
                
                $cUser = CFactory::getUser($post_user_id);

                if ($success == true) {
                    $days_left_status = $board->finished;
                    $title_status = "title_success";
                } else {
                    if ($daysLeft < 0 && $goal_type == 'goal') {                    
                        if ($daysLeft == 1) {
                            $days_left_status = -$daysLeft . " " . $board->time_passed;
                        } else {
                            $days_left_status = -$daysLeft . " " . $board->time_passed_plural;
                        }                       

                        $title_status = "title_behind";
                    } else {                        
                        $days_left_status = $daysLeft;
                        if ($daysLeft == 1) {
                            $days_left_status .= " " . $board->time_left;
                        } else {
                            $days_left_status .= " " . $board->time_left_plural;
                        }
                        $title_status = "";
                    }
                }
                
    ?>
                <div id="declaration_<?php echo $goal_id; ?>" class="block-grid declaration">
                    <?php if ($is_admin) { ?>
                        <div class="admin_section">
                            <span 
                                class="admin_button view_post_notes lizicon-file-text2"
                                post="<?php echo $goal_id;?>" 
                                title="View User Notes"
                                onclick="view_post_notes(this)"
                            ></span>
                            <span 
                                class="admin_button delete_note lizicon-blocked"
                                post="<?php echo $goal_id;?>"
                                target="declaration_<?php echo $goal_id;?>"
                                title="Delete Post"
                                onclick="delete_post(this)"
                            ></span>
                        </div>
                    <?php } ?>
                    <div class="white-bg">
                        <div class="title <?php echo ($title_status); ?>" text-transform:uppercase;>
                            <span style="float:left;">
                                <span class="<?php echo $categories[$category]->icon; ?> small-icon"></span>
                            </span> 
                            <span style="margin-left: 20px;">
                                <?php
                                    if ($goal_type == 'goal') {
                                        echo ($days_left_status);
                                    }
                                ?>                        
                            </span>
                            <span style="float: right; font-size: 14px; color: #fff;">
                                <?php 
                                    //echo date('F d, Y', $declaration_date);
                                    echo date("F d, Y", strtotime($declaration_date));
                                ?>
                            </span>
                        </div>              
                        <div class="pad">

                            <div class="declaration-img">
                                <img
                                    src="<?php echo $cUser->getAvatar(); ?>"
                                    alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>"
                                    data-author="<?php echo $cUser->id;?>"
                                >
                            </div>

                            <?php
                                if ($goal_type == 'goal') {
                                    if ($progress_type == 'progressive') {
                            ?>                            
                                        <div class="progressChart">
                                            <div class="chart">
                                                <div class="percentage" data-percent="<?php echo $percentage;?>">
                                                    <span class="percent">
                                                        <?php echo round($percentage);?>
                                                    </span>
                                                    <sup class="percent">%</sup>                                                    
                                                </div> 
                                            </div>
                                        </div>
                            <?php
                                    } else {
                            ?>
                                        <div class="status_box">
                                            <?php
                                                if ($success == true) {
                                            ?>
                                                    <span class="success_checkmark lizicon-checkmark"></span>
                                            <?php
                                                } 
                                            ?>
                                        </div>
                            <?php
                                    }
                                } else {
                                    if ($progress_type == 'ongoing') {

                                    } else {
                            ?>
                                        <div class="status_box">
                                            <?php
                                                if ($success == true) {
                                            ?>
                                                    <span class="success_checkmark lizicon-checkmark"></span>
                                            <?php
                                                } else {
                                            ?>
                                                    <div style="color: rgb(200, 200, 200); margin: 10px; font-size: 15px;"><?php echo $board->not_finished; ?></div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                            <?php
                                    }
                                }
                            ?>

                            <center>
                                <div id="clr"></div>
                                    <span class='user-name'>
                                        <?php echo $firstName.' '.$lastName . ' ' . $board->user_lead_in; ?>
                                    </span>
                                    <span class="goal-title">
                                        <?php echo $brand->board->my_goal->lead_in . " " . $declaration; //readMore($declaration, 40); ?>
                                    </span>
                                    <br/>   
                            </center>
                        </div>
                        <div id="comments_likes">
                            <?php
                                $like_comment_id = $section . "_" . $goal_id;
                                like_button($like_comment_id);
                                $params = new stdClass();

                                $params->title = $board->comments;
                                $params->button_title = $board->comments_submit;
                                $params->placeholder = $board->comments_placeholder;
                                $params->post_id = $like_comment_id;

                                display_comments($params);
                            ?>
                        </div>
                    </div>
                    <?php if ($is_admin) { ?>
                        <div id="notes_<?php echo $goal_id;?>" class="admin_notes">
                            <span class="lizicon-cross admin_note_close" title="Close" onclick="admin_close_notes(this)"></span>
                            <div class="note_container"></div>
                        </div>
                    <?php } ?>
                </div>
    <?php
            }
        } else {
    ?>
            <div id="block-container">
                <br />
                <br />
                <center>
                    <b style="font-size: 30px; color:rgb(128,128,128);">
                        <?php echo $board->empty_list; ?>
                    </b>
                </center>
                <br />
                <br />
            </div>
    <?php
        }
    ?>
        </div>

<?php
    }

    public function showInspirations($board_id, $limit = 0, $offset = 0, $searchList = null, $team = null) {

        if (!$board_id) {
            return;
        }        

        $model = FOFModel::getTmpInstance("Boards", "AxsModel");
        $data = $model->getBoardData($board_id);

        if (!$data) {
            return;
        }

        $font = $data->font;
        $board = $data->image;

        $categories = getCategories();

        $search = array();
        foreach ($categories as $cat) {
            $search[$cat->name] = $cat->id;
        }

        $user = JFactory::getUser();
        $user_id = $user->id;
        //$date = strtotime('now');

        $db = JFactory::getDBO();

        //Explode the comma separated string into an array.
        if ($searchList) {
            $searchList = explode(",", $searchList);
        }

        //Check if 'friends' is selected and pull it out, since it isn't a category.
        $friends_index = array_search('friends', $searchList);
        if ($friends_index) {
            $friends = getFriends($userId);
            unset($searchList[$friends_index]);
        } 

        $query = $db->getQuery(true);

        //The id from the inspiration board needs to be renamed so it doesn't get confused with joom_users id.
        //Get only users that aren't blocked.
        $query
            ->select('*, board.id AS insp_id')
            ->from($db->quoteName('inspiration_board') . ' AS board')
            ->join('inner', '`#__users` AS users ON users.id = board.user_id');
        
        $conditions = $db->quoteName('board.public') . '=' . $db->quote('yes');
        $conditions .= ' AND ';
        $conditions .= '(' . $db->quoteName('board.deleted') . ' IS NULL OR ' . $db->quoteName('board.deleted') . '="")';
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('users.block') . '=' . $db->quote(0);
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('board.board_id') . '=' . $db->quote($board_id);

        $count = count($searchList);

        if ($count > 0) {
            
            $conditions .= " AND (";

            for ($i = 0; $i < $count - 1; $i++) {
                $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$i]]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$count - 1]]);
            $conditions .= ")";
        }

        if (isset($friends)) {
            $conditions .= " AND (";

            $count = count($friends);

            for ($i = 0; $i < $count - 1; $i++) {
                $conditions .= $db->quoteName('board.user_id') .'=' . $db->quote($friends[$i]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.user_id') . '=' . $db->quote($friends[$count - 1]);
            $conditions .= ")"; 
        }

        $conditions .= self::getTeamConditions($team, $board_id);

        $query
            ->where($conditions)
            ->order($db->quoteName('board.id') . ' DESC');

        $db->setQuery($query);        
        $rows = $db->loadObjectList();

        $inspiration_count = count($rows);

        $offset = $offset * $limit;
        $db->setQuery($query, $offset, $limit);
        $rows = $db->loadObjectList();

        if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {
            $is_admin = $_SESSION['user_board'][$board_id]->admin_access;
        }

        ?>

        <div id="block-container" class="hide_on_reload" style="display:none;" value="<?php echo $inspiration_count ?>">

            <?php

                if ($rows != null) {
                    $count = 0;
                    foreach ($rows as $row) {
                        //echo ($count);

                        $inspiration_id = $row->insp_id;
                        $imageUrl = $row->image;
                        $post_user_id = $row->user_id;
                        $inspiration = $row->inspiration;     
                        $category = $row->category;     
                        $date = $row->date;
                        $public = $row->public;  
                        $rotation = $row->rotation;
                        
                        if ($imageUrl != '') {
                            $image = '<img src="' . AxsImages::getImagesPath('inspiration') . '/' . $imageUrl . '" onerror="this.img=\'images/user.png\'"/>';
                        } else {
                            $image ='';
                        }

                        $cUser = CFactory::getUser($post_user_id);

            ?>
                
                        <div id="inspiration_<?php echo $inspiration_id ?>" class="block-grid">

                            <?php if ($is_admin) { ?>
                                <div class="admin_section">
                                    <span 
                                        class="admin_button view_post_notes lizicon-file-text2"
                                        post="<?php echo $inspiration_id;?>" 
                                        title="View User Notes"
                                        onclick="view_post_notes(this)"
                                    ></span>
                                    <span 
                                        class="admin_button delete_note lizicon-blocked"
                                        post="<?php echo $inspiration_id;?>"
                                        target="inspiration_<?php echo $inspiration_id;?>"
                                        title="Delete Post"
                                        onclick="delete_post(this)"
                                    ></span>
                                </div>
                            <?php } ?>

                            <?php
                           
                                $rating_id = "inspiration_" . $inspiration_id;

                                $hover = array(
                                    $board->rating_1,
                                    $board->rating_2,
                                    $board->rating_3,
                                    $board->rating_4,
                                    $board->rating_5
                                );

                                echo display_ratings($rating_id, $hover); 
                            ?>

                            <div class="inspiration_date" style="position: absolute; right: 15px; top: 15px; color: rgb(180, 180, 180);">
                                <?php
                                    echo date("M d, Y", strtotime($date));
                                ?>
                            </div>

                            <div class="imgholder pic_<?php echo $count?>">
                                <?php 
                                    if ($image != '') { 
                                        echo $image;
                                    } 
                                ?>                
                            </div>
                            <?php /*
                            <script> 
                                rotate_pic("pic_<?php //echo $count?>", <?php //echo $rotation; ?> );
                            </script>
                            */?>

                            
                            <p class="inspired-text">
                                <?php
                                    echo $inspiration;
                                ?>
                            </p>
                            <?php /*
                            <div class="meta">by  <a href="my-profile/<?php echo $alias;?>"><?php echo $firstName.' '.$lastName; ?></a></div>
                            */ ?>
                           
                            <div class="meta">
                                <img
                                    alt="<?php echo CStringHelper::escape($cUser->getDisplayName());?>"
                                    data-author="<?php echo $cUser->id;?>"
                                    src="<?php echo $cUser->getAvatar(); ?>"
                                    class="inspiration-img"
                                >
                                    <?php 
                                        echo CStringHelper::escape($cUser->getDisplayName());
                                    ?>
                                </img>
                            </div>
                            <?php
                                $like_comment_id = "inspiration_" . $inspiration_id;
                                like_button($like_comment_id);
                                $params = new stdClass();

                                $params->title = $board->comments;
                                $params->button_title = $board->comments_submit;
                                $params->placeholder = $board->comments_placeholder;
                                $params->post_id = $like_comment_id;

                                display_comments($params);
                            ?>

                            <?php if ($is_admin) { ?>
                                <div id="notes_<?php echo $inspiration_id;?>" class="admin_notes">
                                    <span class="lizicon-cross admin_note_close" title="Close" onclick="admin_close_notes(this)"></span>
                                    <div class="note_container"></div>
                                </div>
                            <?php } ?>
                        </div>
                    
            <?php 
                        $count++;
                    } 
                } else {
            ?>
                    <div id="block-container">
                        <br />
                        <br />
                        <center>
                            <b style="font-size: 30px; color:rgb(128,128,128);">
                                <?php echo $board->empty_list; ?>
                            </b>
                        </center>
                        <br />
                        <br />
                    </div>
            <?php
                }
            ?>
        </div>
<?php
    }

    public function showGratitudes($board_id, $limit = 0, $offset = 0, $searchList = null, $team = null) {

        if (!$board_id) {
            return;
        }        

        $model = FOFModel::getTmpInstance("Boards", "AxsModel");
        $data = $model->getBoardData($board_id);

        if (!$data) {
            return;
        }

        $font = $data->font;
        $board = $data->affirmation;

        $categories = getCategories();

        $section = 'gratitude';

        $user = JFactory::getUser();
        $user_id = $user->id;
        $date = strtotime('now');

        //Explode the comma separated string into an array.
        if ($searchList) {
            $searchList = explode(",", $searchList);
        }

        //Check if 'friends' is selected and pull it out, since it isn't a category.
        $friends_index = array_search('friends', $searchList);
        if ($friends_index) {
            $friends = getFriends($user_id);
            unset($searchList[$friends_index]);
        } 

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query
            ->select('*, board.id as grat_id')
            ->from($db->quoteName('gratitude_board') . ' AS board')
            ->join('inner', '`#__users` AS users ON users.id = board.user_id');
        
        $conditions = $db->quoteName('board.public') . '=' . $db->quote('yes');
        $conditions .= ' AND ';
        $conditions .= '(' . $db->quoteName('board.deleted') . 'IS NULL OR' . $db->quoteName('board.deleted') . '="")';
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('board.board_id') . '=' . $db->quote($board_id);
        $conditions .= ' AND ';
        $conditions .= $db->quoteName('users.block') . '=' . $db->quote(0);

        $count = count($searchList);

        $search = array();

        foreach ($categories as $cat) {
            $search[$cat->name] = $cat->id;
        }

        if ($count > 0) {
            
            $conditions .= " AND (";

            for ($i = 0; $i < $count - 1; $i++) {                
                $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$i]]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.category') . '=' . $db->quote($search[$searchList[$count - 1]]);
            $conditions .= ")";
        }

        if (isset($friends)) {
            $conditions .= " AND (";

            $count = count($friends);

            for ($i = 0; $i < $count - 1; $i++) {
                $conditions .= $db->quoteName('board.user_id') .'=' . $db->quote($friends[$i]) . ' OR ';
            }

            $conditions .= $db->quoteName('board.user_id') . '=' . $db->quote($friends[$count - 1]);
            $conditions .= ")"; 
        }

        $conditions .= self::getTeamConditions($team, $board_id);

        $query->where($conditions);                 
        $query->order($db->quoteName('board.id') . ' DESC');

        $db->setQuery($query);
        $rows = $db->loadObjectList();

        $gratitude_count = count($rows);
        
        $offset = $offset * $limit;
        $db->setQuery($query, $offset, $limit);
        $rows = $db->loadObjectList();

        $is_admin = false;
        if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {
            $is_admin = $_SESSION['user_board'][$board_id]->admin_access;
        }

        ?>        

        <div id="block-container" class="hide_on_reload" style="display:none;" value="<?php echo $gratitude_count ?>">

            <?php

            if ($rows != null) {
                foreach ($rows as $row) {
                    $gratitude_id = $row->grat_id;
                    $post_user_id = $row->user_id;
                    $what = $row->what;
                    $why = $row->why;
                    $public = $row->public;
                    $date_made = $row->date;
                    $category = $row->category;

                    $date_made = date_create($date_made);
                    $date_made = $date_made->format("F jS, Y");

                    $cUser = CFactory::getUser($post_user_id);

            ?>

                    <div id="gratitude_<?php echo $gratitude_id ?>" class="block-grid" style="padding: 0px; background: rgba(0,0,0,0);">
                        <?php if ($is_admin) { ?>
                            <div class="admin_section">
                                <span 
                                    class="admin_button view_post_notes lizicon-file-text2"
                                    post="<?php echo $gratitude_id;?>" 
                                    title="View User Notes"
                                    onclick="view_post_notes(this)"
                                ></span>
                                <span 
                                    class="admin_button delete_note lizicon-blocked"
                                    post="<?php echo $gratitude_id;?>"
                                    target="gratitude_<?php echo $gratitude_id;?>"
                                    title="Delete Post"
                                    onclick="delete_post(this)"
                                ></span>
                            </div>
                        <?php } ?>
                        <div class="gratitude_block">
                            <div class="gratitude_date">
                                <?php echo $date_made?>
                            </div>
                            <div class="gratitude-icon">
                                <span class="<?php echo $categories[$category]->icon; ?> small-icon"></span>
                            </div> 
                            <div>                                
                                <div class="photo_name">
                                    <?php 
                                        echo CStringHelper::escape($cUser->getDisplayName());
                                    ?> 
                                </div>
                                <div class="gratitude_img">
                                    <img
                                        src="<?php echo $cUser->getAvatar(); ?>"
                                        alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>"
                                        data-author="<?php echo $cUser->id;?>"
                                    >
                                </div>
                                <div class="gratitude_what">
                                    <?php if ($what != "") { ?>
                                        "<?php echo $data->my_affirmation->lead_in . ' ' . $what ?>"
                                    <?php } ?>
                                </div>
                            </div>
                           
                            <div class="gratitude_why">
                                <?php if ($why != "") { ?>
                                    "<?php echo $data->my_affirmation->reason_lead_in . ' ' . $why ?>"
                                <?php } ?>
                            </div>
                            
                            <div id="comment_like">
                                <?php
                                    $like_comment_id = $section . "_" . $gratitude_id;
                                    like_button($like_comment_id);
                                    $params = new stdClass();

                                    $params->title = $board->comments;
                                    $params->button_title = $board->comments_submit;
                                    $params->placeholder = $board->comments_placeholder;
                                    $params->post_id = $like_comment_id;

                                    display_comments($params);
                                ?>
                            </div>
                        </div>
                        <?php if ($is_admin) { ?>
                            <div id="notes_<?php echo $gratitude_id;?>" class="admin_notes">
                                <span class="lizicon-cross admin_note_close" title="Close" onclick="admin_close_notes(this)"></span>
                                <div class="note_container"></div>
                            </div>
                        <?php } ?>
                    </div>

                <?php
                }   
            } else {
            ?>
                <div id="block-container">
                    <br />
                    <br />
                    <center>
                        <b style="font-size: 30px; color:rgb(128,128,128);">
                            <?php echo $board->empty_list; ?>
                        </b>
                    </center>
                    <br />
                    <br />
                </div>
            <?php
            }
            ?>
        </div>
    <?php
        
    }

    private static function getTeamConditions($team, $board_id) {

        $return = "";

        $db = JFactory::getDBO();

        if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {

            if ($_SESSION['user_board'][$board_id]->access_type != "teams") {
                //Only sort by team posts if the board is set to "teams" access.
                return;
            }

            $members = $_SESSION['user_board'][$board_id]->teammembers;
            $leaders = $_SESSION['user_board'][$board_id]->teamleaders;

            $memberslist = array();
            $leaderslist = array();

            $teamlist = array();

            $memberkeys = array_keys($members);
            $leaderkeys = array_keys($leaders);

            foreach ($memberkeys as $key) {
                $teamlist[]= $key;
            }

            foreach ($leaderkeys as $key) {
                $teamlist[]= $key;
            }

            $teamlist = array_unique($teamlist);    //Need to have them unique incase they're both a member and leader of the same team (which would make for 2 entries but only 1 team)

            if (count($teamlist) == 1) {
                //There's only one team they're a member/leader of, only use that one, no matter what team id is passed in. Otherwise, they can never change it.
                
                $oneTeam = true;
            } else {
                $oneTeam = false;
            }
            
            foreach ($members as $team_id => $group) {
                if (is_numeric($team) && $team_id != $team && !$oneTeam) {
                    continue;
                }

                foreach ($group->ids as $member) {
                    $memberslist[]= $member;
                }
            }

            $return .= "AND FIND_IN_SET(" . $db->quoteName('board.user_id') . "," . $db->quote(implode(',', $memberslist)) . ")";
        }

        return $return;
    }
}