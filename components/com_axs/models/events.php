<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 5/9/16
 * Time: 11:09 AM
 */

defined('_JEXEC') or die();

class AxsModelEvents extends FOFModel {

  public function getEventById($id) {
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*')
      ->from($db->qn('axs_events'))
      ->where($db->qn('id').'='.(int)$id);
    $db->setQuery($query);
    $res = $db->loadObject();
    return $res;
  }


  public function getVenueById($id) {
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*')
      ->from($db->qn('axs_events_venues'))
      ->where($db->qn('id').'='.(int)$id);
    $db->setQuery($query);
    $venue = $db->loadObject();
    return $venue;
  }


  public function getCategoryById($id) {
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('*')
      ->from($db->qn('axs_events_categories'))
      ->where($db->qn('id').'='.(int)$id);
    $db->setQuery($query);
    $result = $db->loadObject();
    return $result;
  }
}