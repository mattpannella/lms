<?php

defined('_JEXEC') or die;

class AxsControllerAvatars extends FOFController {

    public function changeUserAvatar() {

        $input = JFactory::getApplication()->input;

        $avatarId = $input->get('avatar_id', null, 'INT');

        $filter = JFilterInput::getInstance();
        $avatarId = $filter->clean($avatarId, 'integer');

        $avatar = AxsAvatars::getAvatarById($avatarId);

        $avatarParams = json_decode($avatar->params);

        $sourcePath = JPATH_ROOT . '/' . $avatar->path;
        $tmpfile = basename($sourcePath);

        $destinationPath = JPATH_ROOT . "/tmp/$tmpfile";

        JFile::copy($sourcePath, $destinationPath);

        $avatarParams->file_metadata->tmp_name = $destinationPath;

        $avatar->params = json_encode($avatarParams);

        $saveResult = AxsAvatars::saveAvatarRecord($avatar);

        $response = new stdClass();

        if($saveResult) {

            $response->status = 'success';
            $response->message = 'Saved Avatar successfully!';
        } else {

            $response->status = 'fail';
            $response->message = 'Failed to save avatar.';
        }

        echo json_encode($response);
    }
}