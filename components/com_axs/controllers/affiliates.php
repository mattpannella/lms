<?php

defined('_JEXEC') or die();

include_once 'components/shared/controllers/common.php';

class AxsControllerAffiliates extends FOFController {
    public $input;

    public function __construct(array $config) {
        parent::__construct($config);
        $this->input = JFactory::getApplication()->input;
    }

    function deleteCode() {
        $user_id = (int)$this->input->get('uid', '0');
        $code_id = (int)$this->input->get('code_id', '0');

        if(AxsExtra::deleteUserAffiliateCode($code_id, $user_id)) {
            echo "success";
        } else {
            echo "error";
        }
    }

    function saveCode() {
        $user_id = (int)$this->input->get('uid', '0');
        $code = $this->input->get('code', '0');

        $res = AxsExtra::setUserAffiliateCode($code, $user_id);
        if($res == 1) {
            echo "success";
        } else {
            echo $res;
        }
    }

    function checkCode() {
        $code = $this->input->get('code', '0');

        if(AxsExtra::getUserFromAffiliateCode($code)) {
            echo "error";
        } else {
            echo "success";
        }
    }
    
    function loadMoreRewards() {
        $page = $this->input->get('page', '1');
        $user_id = $this->input->get('uid', '0');

        $rows = AxsExtra::getUserRewardsTransactions($user_id, $page);
        $htmlrows = '';
        foreach($rows as $tran) {
            if($tran->referee_id != 0) {
                $profile = ginProfile($tran->referee_id);
                $name = $profile->firstname.' '.$profile->lastname;
            } else {
                $name = 'System';
            }

            $amount = 0;
            if($tran->amount > 0) {
                $amount = '+'.$tran->amount.' points';
            } else {
                $amount = $tran->amount.' points';
            }

            $class = $tran->amount > 0 ? 'success' : 'danger';

            $htmlrows .=
           "<tr class='$class'>".
              "<td>".$name."</td>".
              "<td>".AxsExtra::format_date($tran->date)."</td>".
              "<td>".$tran->reason."</td>".
              "<td>".$amount."</td>".
              "<td>".$tran->status."</td>".
           "</tr>";
        }
        echo $htmlrows;
    }
}