<?php

defined('_JEXEC') or die();

require_once 'components/shared/controllers/likes.php';

class AxsControllerLikes extends FOFController {

	public function setLike() {
		
        $date =  strtotime('now');
        $db = JFactory::getDBO();
        $userId = JFactory::getUser()->id;
        $like_id = JRequest::getVar('lid');

        if ($like_id) {
            
            //Check to be sure they haven't already liked this post.
            //If so, they are unliking it.
            
            $data = check_user_like($like_id);
            
            if ($data != null) {
                //There's already an entry
                $query = $db->getQuery(true);
                
                $conditions = array(
                    $db->quoteName('like_id') . '=' . $db->quote($like_id),
                    $db->quoteName('user_id') . '=' . (int)$userId            
                );
                
                $query
                    ->delete($db->quoteName('likes'))
                    ->where($conditions);
                
                $db->setQuery($query);
                $return = $db->execute();
                
            } else {    
                //There's no entry, add it.
                $query = $db->getQuery(true);

                $columns = array(
                    'like_id',
                    'user_id',            
                    'like_date'
                );

                $values = array(
                    $db->quote($like_id),
                    (int)$userId,            
                    (int)$date
                );

                $query
                    ->insert($db->quoteName('likes'))
                    ->columns($db->quoteName($columns))
                    ->values(implode(',', $values));

                $db->setQuery($query);
                $return = $db->execute();
                
            }
            
            create_like_button($like_id);
        }
        
	}	
}