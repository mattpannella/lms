<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die();

class AxsControllerVideos extends FOFController {


	public function trackVideo() {
		$db        = JFactory::getDbo();
		$response  = new stdClass();
		$data      = new stdClass();
		$input     = JFactory::getApplication()->input;
		$user_id   = JFactory::getUser()->id;
		$videoDataEncoded = $input->get('data', '', 'BASE64');
		$response->completed = 0;
		$runChecklistUpdate = false;
		if(!$user_id) {
			$response->message = "No user session found";
		}

		if($videoDataEncoded && $user_id) {
			$videoData = json_decode(base64_decode($videoDataEncoded));
			$data->video_title = $videoData->video_title;
			$data->video_type = $videoData->video_type;
			if($videoData->secure_video_url) {
				$data->video_path = $videoData->secure_video_url;
			} else {
				$data->video_path = $videoData->video;
			}
			$data->user_id = (int)$user_id;
			$data->activity_id = $videoData->activity_id;
			$data->lesson_id = (int)$videoData->lesson_id;
			$data->course_id = (int)$videoData->course_id;
			$data->media_id = (int)$videoData->media_id;
			$data->current_frame = $input->get('current_frame', '', 'STRING');
			$data->time_started = time();
			$data->video_duration = $input->get('video_duration', '', 'STRING');
			$data->total_time = $data->time_stopped - $data->time_started;
			$data->completed = 0;
			$data->date = date('Y-m-d H:i:s');

			$videoTrackingSession = AxsLMS::getVideoTrackingSession($data);

			if($data->activity_id && AxsLMS::canBeRequired($videoData->video_type)) {
				$studentActivity = new stdClass();
				$studentActivity->course_id = $data->course_id;
				$studentActivity->lesson_id = $data->lesson_id;
				$studentActivity->activity_id = $data->activity_id;
				$studentActivity->user_id = $data->user_id;
				$studentActivity->activity_type = 'video';
				$studentActivity->activity_request = $data->video_title;
				$studentActivity->student_response = $data->video_path;
				$studentActivity->date_submitted = date('Y-m-d H:i:s');
				$studentActivity->date_modified = date('Y-m-d H:i:s');
				$studentActivityRow = AxsLMS::getUserActivityByStudentResponse($studentActivity,$studentActivity->student_response);
			} else {
				$studentActivity = false;
			}

			if($videoTrackingSession) {
				$videoTrackingSession->video_title   = $data->video_title;
				$videoTrackingSession->current_frame = $data->current_frame;
				$videoTrackingSession->time_stopped  = time();
				$videoTrackingSession->total_time    = $videoTrackingSession->time_stopped - $videoTrackingSession->time_started;
				$playingTime = (int)$videoTrackingSession->video_duration - (int)$videoTrackingSession->current_frame;
				
                if( ( !$videoTrackingSession->completed  || ($studentActivityRow && !$studentActivityRow->completed) ) && ($playingTime <= 20) ) {

					$videoTrackingSession->completed = 1;
					$response->completed = 1;

					if($studentActivityRow && !$studentActivityRow->completed) {
						$studentActivityRow->completed = 1;
						$response->runCheckComplete = true;
						$response->activity_id = $data->activity_id;
						$studentActivityRow->date_modified = date('Y-m-d H:i:s');
						$db->updateObject('#__splms_student_activities',$studentActivityRow,'id');
					}

                    // Update the checklist activity for watched_video items
                    AxsLMS::markUserChecklistItemsCompletedForItemType($user_id, $data->media_id, 'watched_video');
				}
				$db->updateObject('axs_video_tracking',$videoTrackingSession,'id');
				$response->message .= "Row Updated";
			} else {
				$db->insertObject('axs_video_tracking',$data);
				$response->message = "Row Inserted";
			}

			if($studentActivity && !$studentActivityRow) {
				$db->insertObject('#__splms_student_activities',$studentActivity);
			}
		}

		echo json_encode($response);
	}

  	public function getMedia() {
		$app = JFactory::getApplication();
		$filterEncoded = $app->input->get('filter', '', 'BASE64');
		$filter = json_decode(base64_decode($filterEncoded));
	    $category = AxsSecurity::alphaNumericOnly($filter->category);
	    $subcategory = AxsSecurity::alphaNumericOnly($filter->subcategory);
		$search_terms = AxsSecurity::alphaNumericOnly($filter->search);
		$media_type = AxsSecurity::alphaNumericOnly($filter->type);
		$language = AxsSecurity::alphaNumericOnly($filter->language);
		$user_id = JFactory::getUser()->id;
		ob_start();
		if (isset($category) && isset($subcategory) && isset($language)) {
	    	//Get the model for the videos
	      	$model = $this->getThisModel();

	     	$limit = AxsSecurity::alphaNumericOnly($filter->size);
			$start = AxsSecurity::alphaNumericOnly($filter->start);
	      	$offset = $start;
			$data = $model->getMedia($search_terms, $language, $offset, $limit, $category, $subcategory, $media_type);
		    $category_list = $model->getCategoryData();

		    if(count($data->media)) {

			    foreach($data->media as $item) {
			    	$itemParams = json_decode($item->params);
			    	$itemParams->access = $item->access;

			    	if($itemParams->access_purchase_type) {
			    		// new access check function
				    	$hasAccess = AxsContentAccess::checkContentAccess($user_id,$itemParams);
			    	} else {
			    		// legacy check for access
			    		$hasAccess =  AxsExtra::checkViewLevel($item->access, $user_id);
			    	}

				    //$accessLevelName =  AxsExtra::accessLevelName($item->access);
					$data_size = 1;
					$descriptionLength = 200;
					$shortDescription = explode('<hr id="system-readmore" />', $item->description);

					switch ($item->media_type) {
						case "audio":

							$icon = "fad fa-file-audio";
							$button = '<a class="btn btn-outline-primary btn-sm w-50 flex-fill ginAudioPopup" title="'.$item->title.'" alias="'.$item->source.'" header="'.$item->title.'">'.AxsLanguage::text("AXS_LISTEN", "Listen").'</a>';
							break;
						case "pdf":
							$icon = "fad fa-file-pdf";
							$has_interactive = false;
							if ($item->interactive_source != '') {
								$has_interactive = true;
								$button = '<a href="'.$item->interactive_source.'" target="_blank" class="btn btn-outline-primary btn-sm w-50 flex-fill">'.AxsLanguage::text("AXS_VIEW", "View").'</a>';
							}
							if (strtolower(substr($item->source, -4)) == '.pdf') {
								if(!strpos($item->source,'http')) {
									$item->source = JURI::root().$item->source;
								}
								$button = '<a href="'.$item->source.'" target="_blank" class="btn btn-outline-primary btn-sm w-50 flex-fill">'.AxsLanguage::text("AXS_VIEW", "View").'</a>';
							} elseif($has_interactive == false) {
								$button = '<a class="btn btn-outline-primary btn-sm w-50 flex-fill">'.Jtext::_('AXS_NO_CONTENT_AVAILABLE').'</a>';
							}
							break;
						case "link":
							$icon = "fad fa-external-link-square-alt";
							$has_interactive = false;
							if ($item->interactive_source != '') {
								$has_interactive = true;
								$button = '<a href="'.$item->interactive_source.'" target="_blank" class="btn btn-outline-primary btn-sm w-50 flex-fill">View</a>';
							} else {
								$button = '<a class="btn btn-outline-primary btn-sm w-50 flex-fill">'.Jtext::_('AXS_NO_CONTENT_AVAILABLE').'</a>';
							}
							break;
						case "video":
						default:
							$icon = "fad fa-file-video";
							$button = '<a href="'.JRoute::_('index.php?option=com_axs&view=video&id='.$item->id).'" class="btn btn-outline-primary btn-sm w-50 flex-fill">'.AxsLanguage::text("AXS_WATCH", "Watch").'</a>';
							break;
					}

					if(!$hasAccess) {
						$button = '<span class="btn btn-outline-primary btn-sm w-50 flex-fill me-1 disabled" title="'.AxsLanguage::text("AXS_YOU_DONT_HAVE_ACCESS", "You don't have access").'">'.Jtext::_('AXS_RESTRICTED').'</span>';
					}

					if (!$item->thumbnail_image) {
						$thumbnail_url = '';
						if ($item->media_type == "video" && $itemParams->video_type == "youtube") {
							$thumbnail_url = AxsMedia::getVideoThumbnail($itemParams->youtube_id);
						}
						else if ($item->media_type == "video" && $itemParams->video_type == "vimeo") {
							$thumbnail_url = AxsMedia::getVimeoThumbnailFromId($itemParams->vimeo_id);
						} else {
							$thumbnail_url = 'components/com_axs/assets/images/media-type-'.$item->media_type.'.jpg';
						}
						$item->thumbnail_image = !empty($thumbnail_url) ? $thumbnail_url : $item->thumbnail_image;
					}

					if( !preg_match('/http(s?)\:\/\//i', $item->thumbnail_image) ) {
						$item->thumbnail_image = JURI::root().$item->thumbnail_image;
					}

					?>
					<div name="ml-card" class="d-flex align-items-start flex-column ml-card rounded shadow overflow-hidden w-100 w-md-50 mx-md-1 mb-1 mb-md-2" <?php echo $hasAccess ? 'hasaccess' : ''; ?>" data-size="<?php echo $data_size; ?>">
						<div class="ml-card-image position-relative rounded-top w-100 tov-ml-mt-<?php echo $item->media_type; ?>"

						style="background-image: url(<?php echo $item->thumbnail_image; ?>);"

						alt="<?php echo $item->title; ?>"
						<?php echo AxsAccessibility::image($item->thumbnail_image); ?>
						>
							<div name="Media Type" class="position-absolute top-0 start-0 p-2 rounded bg-white mt-2 ms-2">
								<i class="<?php echo $icon; ?>"></i>
							</div>
						</div>
						<div name="Card Text" class="p-3">
							<h4><?php echo $item->title; ?></h4>
							<span class="d-inline-block text-truncate text-wrap" style="max-width:100%; max-height: 50px;">
							<?php if($shortDescription[0]) { echo $shortDescription[0]  . '<br><br>'; }  ?>
							</span>
						</div>
						<div name="Card Buttons" class="p-2 pt-0 d-flex mt-auto w-100">
							<?php echo $button; ?>
						</div>
					</div>
			<?php
				}
			}
		    foreach($category_list as $cat) {
		    	$title = '';
		    	if($cat->id == $category) {
		    		$title = $cat->title;
		    		break;
			    }
			}
		}
		$html = ob_get_clean();
		$reponse = new stdClass();
		$reponse->html = $html;
		$reponse->title = $title;
		echo json_encode($reponse);

	}
}

