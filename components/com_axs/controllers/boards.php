<?php

defined('_JEXEC') or die();

//include_once 'components/shared/controllers/common.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsControllerBoards extends FOFController {

	public function saveBoard() {

		$key = AxsKeys::getKey('boards');
		
		$fields = base64_decode(JRequest::getVar('fields'));
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);
		$actions = AxsEncryption::decrypt(base64_decode(JRequest::getVar('action')), $key);
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query
			->select($db->quoteName("board_type"))
			->from($db->quoteName('axs_board_templates'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id))
			->limit(1);
		
		$db->setQuery($query);
		$result = $db->loadObject();

		if (!$result) {
			return;
		}
		
		$type = $result->board_type;

		switch ($type) {
			case "affirmation":
				self::saveAffirmation($fields, $board_id, $actions);
				break;
			case "goal":
				self::saveGoal($fields, $board_id, $actions);
				break;
			case "image":
				self::saveImage($fields, $board_id, $actions);
				break;
		}
	}

	private static function getBoardDatabase($board_type) {
		switch ($board_type) {
			case "affirmation": $board = "gratitude_board"; break;
			case "goal": $board = "declaration_board"; break;
			case "image": $board = "inspiration_board"; break;
		}

		return $board;
	}

	private static function checkOwnership($post_id, $board_type) {
		$db = JFactory::getDBO();

		$board = self::getBoardDatabase($board_type);

		//$query = "SELECT board_id, user_id FROM " . $board . " WHERE id=$post_id LIMIT 1";

		$db->setQuery($query);
		$query = $db->getQuery(true);
		$query
			->select(array(
					$db->quoteName('board_id'),
					$db->quoteName('user_id')
				))
			->from($db->quoteName($board))
			->where($db->quoteName('id') . '=' . $db->quote($post_id))
			->limit(1);

		$db->setQuery($query);
		$result = $db->loadObject();

		$user_id = JFactory::getUser()->id;

		if ($user_id != $result->user_id) {
			return false;
			//This is not their post
		} else {
			return true;
		}
	}

	public static function setPrivacy() {

		$key = AxsKeys::getKey('boards');

		$post_id = JRequest::getVar('pid');
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);
		$current = JRequest::getVar('cur');

		$board_type = self::getBoardType($board_id);

		if (!self::checkOwnership($post_id, $board_type)) {
			return false;
		}

		$db = JFactory::getDBO();
		$board = self::getBoardDatabase($board_type);

		switch ($current) {
			case "public": $setting = "no"; break;		//If public, set to no (private)
			case "private": $setting = "yes"; break;	//If private, set to no (public)
			default:
				return false;
		}

		//$query = "UPDATE " . $board . " SET public = '" . $setting . "' WHERE id=" . $post_id;
		$query = $db->getQuery(true);
		$query
			->update($db->quoteName($board))
			->set($db->quoteName('public') . '=' . $db->quote($setting))
			->where($db->quoteName('id') . '=' . $db->quote($post_id));

		$db->setQuery($query);
		$db->execute();

		echo "success";
	}

	public static function deletePost() {

		$key = AxsKeys::getKey('boards');

		$post_id = JRequest::getVar('pid');
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);

		$board_type = self::getBoardType($board_id);

		if (!self::checkOwnership($post_id, $board_type)) {
			//Don't delete a post that isn't theirs.
			return false;
		}

		$date = date("Y-m-d H:m:s");

		$db = JFactory::getDBO();
		$board = self::getBoardDatabase($board_type);
		//$query = "UPDATE " . $board . " SET deleted = '" . $date . "' WHERE id = " . $post_id;

		$query = $db->getQuery(true);
		$query
			->update($db->quoteName($board))
			->set($db->quoteName('deleted') . '=' . $db->quote($date))
			->where($db->quoteName('id') . '=' . $db->quote($post_id));

		$db->setQuery($query);
		$db->execute();

		echo "success";
	}

	private static function getBoardType($board_id) {
		$db = JFactory::getDBO();
		//$query = "SELECT board_type FROM axs_board_templates WHERE id=$board_id";
		$query = $db->getQuery(true);
		$query
			->select($db->quoteName('board_type'))
			->from($db->quoteName('axs_board_templates'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id));

		$db->setQuery($query);
		$data = $db->loadObject();
		if ($data) {
			return $data->board_type;
		}
	}

	private static function getBoardData($board_id, $entry = null) {
		$db = JFactory::getDBO();
		//$query = "SELECT data FROM axs_board_templates WHERE id=$board_id";

		$query = $db->getQuery(true);
		$query
			->select($db->quoteName('data'))
			->from($db->quoteName('axs_board_templates'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id));

		$db->setQuery($query);
		$data = $db->loadObject();
		if ($data) {
			if ($entry) {
				$data = json_decode($data->data);
				return $data->$entry;
			} else {
				return json_decode($data->data);
			}
		}
	}

	private static function saveAffirmation($fields, $board_id, $actions) {
		$user_id = JFactory::getUser()->id;
		$params = array();
		
		parse_str($fields, $params);

		foreach ($params as $key => $val) {
			//Check for empty information
			if (!$val) {
				return false;
			}
		}


		foreach ($actions as $key => $val) {
			//Get all of the set action values
			$$key = $val;
		}

		$db = JFactory::getDBO();

		$data = self::getBoardData($board_id, "my_affirmation");
		$max_length = (int)$data->max_length;
		
		//Make sure they don't go over their character limit, if so, set it to private.
		if (strlen($params["grateful_what"]) > $max_length) {
			$params["public"] = "no";
		}

		if (strlen($params["grateful_why"]) > $max_length) {
			$params["public"] = "no";
		}
		
		//Convert HTML entities

		$params["grateful_what"] = htmlentities($params["grateful_what"], ENT_QUOTES);
		$params["grateful_why"] = htmlentities($params["grateful_why"], ENT_QUOTES);

		if ($params['public'] != 'yes') {
			$params['public'] = 'no';
		}

		if (!self::checkCategory($board_id, $params['category'])) {
			return;
		}
		
		if (isset($add)) {			
    		$query = $db->getQuery(true);

    		$values = array(
			    $db->quote($params['grateful_what']),
			    $db->quote($params['grateful_why']),
			    $db->quote($params['category']),
			    $db->quote($params['public']),
			    (int)$user_id,
			    (int)$board_id,
			    $db->quote(date("Y-m-d H:m:s"))
			);

			$columns = array(
			    'what',
			    'why',
			    'category',
			    'public',
			    'user_id',
			    'board_id',
			    'date'
			);

		    $query
		        ->insert($db->quoteName('gratitude_board'))
		        ->columns($db->quoteName($columns))
		        ->values(implode(',', $values));

    		$db->setQuery($query);
    		$db->execute();
    		return;
		}

		if (isset($update)) {

			$query = $db->getQuery(true);

		    $fields = array(
		        $db->quoteName('what') . '=' . $db->quote($params['grateful_what']),
		        $db->quoteName('why') . '=' . $db->quote($params['grateful_why']),
		        $db->quoteName('category') . '=' . $db->quote($params['category']),
		        $db->quoteName('public') . '=' . $db->quote($params['public'])
		    );

		    $conditions = array(
		        $db->quoteName('id') . '=' . (int)$gratitude,
		        $db->quoteName('user_id') . '=' . (int)$user_id,		//This is to make sure they own it.
		        $db->quoteName('board_id') . '=' . (int)$board_id		//This is to make sure they own it.
		    );

		    $query
		        ->update($db->quoteName('gratitude_board'))
		        ->set($fields)
		        ->where($conditions);

		    $db->setQuery($query);
		    
		    $result = $db->execute();
		}

		
	}

	private static function saveGoal($fields, $board_id, $actions) {
		$user_id = JFactory::getUser()->id;
		$params = array();
		
		parse_str($fields, $params);

		$data = self::getBoardData($board_id, "my_goal");

		foreach ($actions as $key => $val) {
			//Get all of the set action values
			//ie. add, update
			$$key = $val;
		}

		//Validate Goal Data

		$good_data = true;
		$max_length = (int)$data->max_length;
		
		if (strlen($params["declaration"]) == 0) {
			$good_data = false;
		}

		if (strlen($params["declaration"]) > $max_length) {
			//If the post is over the max length, set it to private.
			$params["public"] = "no";
		}

		//Convert html entities
		$params["declaration"] = htmlentities($params["declaration"], ENT_QUOTES);

		//Validate the start time
		if (!self::validateDate($params["start_time"])) {
			$good_data = false;
		}

		if ($params["public"] != "yes") {
			 $params["public"] = "no";
		}

		if (!self::checkCategory($board_id, $params['category'])) {
			return;
		}

		//Goal specific settings.
		switch ($params["goal_type"]) {
			case "declaration":
			//If progress_type_declaration is set, it must be "on"
				if (isset($params["progress_type_declaration"])) {
					if ($params["progress_type_declaration"] != "on") {
						$good_data = false;						
					}

					//Can only be 1
					$params["goal_amount"] = 1;
				} else {
					$params["goal_amount"] = null;
				}

				$params["end_time"] = null;
				break;
			case "goal":
				//Validate the end date.
				if (!self::validateDate($params["end_time"])) {
					$good_data = false;
				}
				
				$start_time = strtotime($params["start_time"]);
				$end_time = strtotime($params["end_time"]);

				if ($start_time && $end_time) {
					if ($end_time < $start_time) {
						$good_data = false;
					}
				} else {
					$good_data = false;
				}

				//If progress_type_goal is set, it must be "on"
				if (isset($params["progress_type_goal"])) {
					if ($params["progress_type_goal"] != "on") {
						$good_data = false;
					} else {
						if (!is_numeric($params["goal_amount"])) {
							//Progress can only be a number.
							$good_data = false;
						}
					}
				} else {
					//If there is no progress, the goal amount can only be 1.
					$params["goal_amount"] = 1;
				}

				break;
			default:
				//Bad data
				$good_data = false;				
		}

		if (!$good_data) {
			//There's bad data.  
			return;
		}
		
		$db = JFactory::getDBO();

		//Format the start and end dates for saving. Should always be a start date
	    $start_date = date("Y-m-d H:m:s", strtotime($params['start_time']));
	    
	    if ($params['end_time']) {
	    	$end_date = date("Y-m-d H:m:s", strtotime($params['end_time']));
	    } else {
	    	$end_date = null;
	    }
		
		if (isset($add)) {

    		if ($params["goal_type"] == 'declaration') {
		        if (isset($params["progress_type_declaration"])) {
		            $progress_type = 'accomplishable';
		        } else {
		            $progress_type = 'ongoing';
		        }
		    } else if ($params["goal_type"] == 'goal') {
		        if (isset($params["progress_type_goal"])) {
		            $progress_type = 'progressive';
		        } else {
		            $progress_type = 'single';
		        }
		    }

		    $query = $db->getQuery(true);

		    $columns = array(
		        'user_id',
		        'board_id',
		        'declaration',
		        'start_date',
		        'end_date',
		        'category',
		        'goal_amount',
		        'public',
		        'goal_type',
		        'progress_type',
		        'date'
		    );		    		    

		    $values = array(
		        (int)$user_id,
		        (int)$board_id,
		        $db->quote($params["declaration"]),
		        $db->quote($start_date),
		        $db->quote($end_date),
		        (int)$params["category"],
		        (int)$params["goal_amount"],
		        $db->quote($params["public"]),
		        $db->quote($params["goal_type"]),
		        $db->quote($progress_type),
		        $db->quote(date("Y-m-d H:m:s"))

		    );

		    $query = $db->getQuery(true);
		    $query
		        ->insert($db->quoteName('declaration_board'))
		        ->columns($db->quoteName($columns))
		        ->values(implode(',', $values));

		    $db->setQuery($query);
		    $db->execute();
		}

		if (isset($update)) {

			if ($params["goal_type"] == 'declaration') {
		        if (isset($params["progress_type_declaration"])) {
		            $progress_type = 'accomplishable';
		        } else {
		            $progress_type = 'ongoing';
		        }
		    } else if ($params["goal_type"] == 'goal') {
		        if (isset($params["progress_type_goal"])) {
		            $progress_type = 'progressive';
		        } else {
		            $progress_type = 'single';
		        }
		    }

		    $query = $db->getQuery(true);

		    $fields = array(
		        $db->quoteName('goal_amount') . '=' . (int)$params['goal_amount'],
		        $db->quoteName('category') . '=' . (int)$params['category'],
		        $db->quoteName('public') . '=' . $db->quote($params['public']),
		        $db->quoteName('declaration') . '=' . $db->quote($params['declaration']),
		        $db->quoteName('start_date') . '=' . $db->quote($start_date),
		        $db->quoteName('end_date') . '=' . $db->quote($end_date),
		        $db->quoteName('goal_type') . '=' . $db->quote($params['goal_type']),
		        $db->quoteName('progress_type') . '=' . $db->quote($progress_type)
		    );

		    $conditions = array(
		        $db->quoteName('id') . '=' . (int)$goal,
		        $db->quoteName('user_id') . '=' . (int)$user_id,		//This is to make sure they own it.
		        $db->quoteName('board_id') . '=' . (int)$board_id	//This is to make sure they own it.
		    );

		    $query
		        ->update($db->quoteName('declaration_board'))
		        ->set($fields)
		        ->where($conditions);
		            
		    $db->setQuery($query);
			$db->execute();
		}
	}

	private static function saveImage($fields, $board_id, $actions) {

		$user_id = JFactory::getUser()->id;
		$params = array();
		
		parse_str($fields, $params);

		$data = self::getBoardData($board_id, "my_goal");

		foreach ($actions as $key => $val) {
			//Get all of the set action values
			//ie. add, update
			$$key = $val;
		}

		$fileData = JRequest::getVar('fileData');

		if ($params["public"] != "yes") {
			$params["public"] = "no";
		}

		if (!self::checkCategory($board_id, $params['category'])) {
			return;
		}

		$max_length = (int)$data->max_length;
		
		if (strlen($params["text"]) > $max_length) {
			$params["public"] = "no";
		}

		$params["text"] = htmlentities($params["text"], ENT_QUOTES);

		$db = JFactory::getDBO();

		if ($add) {

			$fileName = self::saveImageToDisk($fileData);

			if (!$fileName) {
				//Something was wrong with the image.
				echo "bad";
				return;
			}

			$query = $db->getQuery(true);

			$columns = array(
	            'user_id',
	            'board_id',
	            'category',
	            'inspiration',
	            'image',
	            'date',
	            'public'
	        );
				
			$values = array(
	            (int)$user_id,
	            (int)$board_id,
	            (int)$params['category'],
	            $db->quote($params['text']),
	            $db->quote($fileName),
	            $db->quote(date("Y-m-d H:m:s")),
	            $db->quote($params["public"])
	        );

	        $query
	            ->insert($db->quoteName('inspiration_board'))
	            ->columns($db->quoteName($columns))
	            ->values(implode(',', $values));


			$db->setQuery($query);
		    $db->execute();		    
		}

		if ($update) {

			$newFileName = null;

			if ($fileData) {

				//A new image is supplied, delete the old one and upload the new one.

				/*
					$query = "
						SELECT image FROM inspiration_board 
						WHERE id=$inspiration 
						AND board_id=$board_id
						AND user_id=$user_id
						AND (deleted IS NULL OR deleted = '')";
				*/

				$del = $db->quoteName('deleted');

				$conditions = array(
					$db->quoteName('id') . '=' . $db->quote($inspiration),
					$db->quoteName('board_id') . '=' . $db->quote($board_id),
					$db->quoteName('user_id') . '=' . $db->quote($user_id),
					"($del IS NULL OR $del = '')"
				);

				$query = $db->getQuery(true);
				$query
					->select($db->quoteName('image'))
					->from($db->quoteName('inspiration_board'))
					->where($conditions);

				$db->setQuery($query);
				$original = $db->loadObject();
				
				if (!$original) {
					//It's not theirs, don't continue
					return;
				}

				self::deleteImageFromDisk($original->image);				
				$newFileName = self::saveImageToDisk($fileData);
			}

			$query = $db->getQuery(true);

		    $fields = array(		        
		        $db->quoteName('category') . '=' . (int)$params['category'],
		        $db->quoteName('public') . '=' . $db->quote($params['public']),
		        $db->quoteName('inspiration') . '=' . $db->quote($params['text'])
		    );

		    if ($newFileName) {
		    	array_push($fields, $db->quoteName('image') . '=' . $db->quote($newFileName));
		    }

		    $conditions = array(
		        $db->quoteName('id') . '=' . (int)$inspiration,
		        $db->quoteName('user_id') . '=' . (int)$user_id,		//This is to make sure they own it.
		        $db->quoteName('board_id') . '=' . (int)$board_id	//This is to make sure they own it.
		    );

		    $query
		        ->update($db->quoteName('inspiration_board'))
		        ->set($fields)
		        ->where($conditions);

		    $db->setQuery($query);		    
			$db->execute();
		}
	}

	private static function saveImageToDisk($fileData) {
		$imageData = explode(",", $fileData);
			
		$image = imagecreatefromstring(base64_decode($imageData[1]));
		if (!$image) {			
			return;
		}

		$size = getimagesize($fileData);			

		$directory = AxsImages::getImagesPath('inspiration') . '/';
		$fileName = strtotime('now') . rand(0, 99999);

		switch ($size["mime"]) {
			//Acceptable file types
			//imagepng, imagejpeg, and imagegif save the file to its location.
			case "image/png":
				$fileName .= ".png";
				if (!imagepng($image, $directory . $fileName)) {					
					return;
				}

				break;
			case "image/jpeg":
				$fileName .= ".jpg";
				if (!imagejpeg($image, $directory . $fileName)) {
					return;
				}
				
				break;
			case "image/gif":
				$fileName .= ".gif";
				if (!imagegif($image, $directory . $fileName)) {
					return;
				}
				
				break;

				
			default:
				//Bad
				return;
				break;
		}		
		
		include('components/shared/controllers/image-resizer.php');

		/*
			Use the image resizer to load and resave the file to its proper size.
			Try reworking this so it resizes before the initial save.
		*/
		$image = new SimpleImage();
		$image->load($directory . $fileName);
		$image->resizeToWidth(1024);
		$image->save($directory . $fileName);

		return $fileName;
	}

	private static function deleteImageFromDisk($fileName) {
		$directory = AxsImages::getImagesPath('inspiration') . '/';
		return unlink($directory . $fileName);
	}

	private static function validateDate($date) {
		$strtime = strtotime($date);
		if (!$strtime) {
			return false;
		}

		$date = explode("/", $date);
		
		$month = (int)$date[0];
		$day = (int)$date[1];
		$year = (int)$date[2];
		
		$good = checkdate($month, $day, $year);
		
		if (!$good) {
			return false;
		} else {
			return true;
		}
	}

	public static function gratitudeThankful() {
		$grateful_id = JRequest::getVar('gid');


		//Get the last time they were thankful.  Do not update if it has is the same day
        
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        
        $query
            ->select('*')
            ->from($db->quoteName('gratitude_board'))
            ->where($db->quoteName('id') . '=' . (int)$grateful_id);
        
        $db->setQuery($query);
        $data = $db->loadObject();
        
        if ($data != null) {
            $last_thankful = $data->last_thankful;
        }

        //Get only the year, month, and date of the time
        $last = date("Y-m-d", strtotime($last_thankful));
        $now = date("Y-m-d", strtotime('now'));

        if ($last == $now) {
        	//It's the same day.  Don't allow it again.
        	return;
        }        
        
        $query = $db->getQuery(true);
            
        $conditions = array(
            $db->quoteName('id') . '=' . (int)$grateful_id
        );
        
        $fields = array(
            $db->quoteName('times_thankful') . '=' . $db->quoteName('times_thankful') . '+1',
            $db->quoteName('last_thankful') . '=' . $db->quote(date("Y-m-d H:m:s")),
            $db->quoteName('previous_thankful') . '=' . $db->quote($last_thankful)
        );        

        
        
        $query
            ->update($db->quoteName('gratitude_board'))
            ->set($fields)
            ->where($conditions);
    
        $db->setQuery($query);
        $result = $db->execute();        
	}

	public function refreshPosts() {

		$key = AxsKeys::getKey('boards');

		$type = JRequest::getVar('type');
		$limit = JRequest::getVar('limit');
		$offset = JRequest::getVar('offset');
		$category = JRequest::getVar('cat');
		$team = JRequest::getVar('team');

		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);

		$model = FOFModel::getTmpInstance('boards', 'AxsModel');

		switch ($type) {
            case "declaration":
                $model->showDeclarations($board_id, $limit, $offset, $category, $team);
                break;
            case "gratitude":
                $model->showGratitudes($board_id, $limit, $offset, $category, $team);
                break;
            case "inspiration":
                $model->showInspirations($board_id, $limit, $offset, $category, $team);
                break;
        }
	}

	public function updateProgress() {
		//For declaration boards
		$user_id = JFactory::getUser()->id;

		$goal = JRequest::getVar('goal');
		$progress = (int)JRequest::getVar('progress');

		$db = JFactory::getDBO();
		//$query = "SELECT * FROM declaration_board WHERE id=$goal AND user_id=$user_id";

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('declaration_board'))
			->where(array(
				$db->quoteName('id') . '=' . $db->quote($goal),
				$db->quoteName('user_id') . '=' . $db->quote($user_id)
			));

		$db->setQuery($query);
		$post = $db->loadObject();

		if (!$post || (int)$progress == 0) {
			//It's not theirs or the progress is nothing
			echo "invalid";			
			return;
		}

		$goal_type = $post->goal_type;
		$progress_type = $post->progress_type;

		$date = date("Y-m-d H:m:s");

		if ($progress_type == 'progressive') {
	        //Update the progress numerically
	    
	        $columns = array(
	            'goal_id',
	            'user_id',
	            'update_date',
	            'update_amount'
	        );

	        $values = array(
	            $db->quote($goal),
	            $db->quote($user_id),
	            $db->quote($date),
	            $db->quote($progress)
	        );

	        $query = $db->getQuery(true);
	        $query
	            ->insert($db->quoteName('declaration_updates'))
	            ->columns($db->quoteName($columns))
	            ->values(implode(',', $values));

	        $db->setQuery($query);
	        $db->execute();	        

	    } else if ($progress_type == 'single' || $progress_type == 'accomplishable') {
	        //Toggle the progress successful / unsuccessful

	        //check to be sure it exists first
	        
	        $query = "SELECT * FROM declaration_updates WHERE user_id = $user_id AND goal_id = $goal";

	        $query = $db->getQuery(true);
	        $query
	        	->select('*')
	        	->from($db->quoteName('declaration_updates'))
	        	->where(array(
	        		$db->quoteName('user_id') . '=' . $db->quote($user_id),
	        		$db->quoteName('goal_id') . '=' . $db->quote($goal)
	        	));

	        $db->setQuery($query);
	        $update = $db->loadObject();

	        if ($update) {
	            //The goal is already in the database, change it.

	            /*if ($update->update_amount == 0) {
	                $new_setting = 1;
	            } else {
	                $new_setting = 0;
	            }*/
	            if ($progress == 1) {
	            	$new_setting = 1;
	            } else if ($progress == -1) {
	            	$new_setting = 0;
	            }

	            //$query = "UPDATE declaration_updates SET `update_amount`= $new_setting WHERE id=" . $update->id;

	            $query = $db->getQuery(true);
	            $query
	            	->update($db->quoteName("declaration_updates"))
	            	->set($db->quoteName('update_amount') . '=' . $db->quote($new_setting))
	            	->where($db->quoteName('id') . '=' . $db->quote($update->id));

	            $db->setQuery($query);
	            $db->execute();

	        } else {

	        	//The update does not exist.  Add it.

	        	$columns = array(
	                'goal_id',
	                'user_id',
	                'update_date',
	                'update_amount'
	            );

	            $values = array(
	                $db->quote($goal),
	                $db->quote($user_id),
	                $db->quote($date),
	                $db->quote(1)
	            );

	            $query = $db->getQuery(true);
	            $query
	                ->insert($db->quoteName('declaration_updates'))
	                ->columns($db->quoteName($columns))
	                ->values(implode(',', $values));

	            $db->setQuery($query);
	            $db->execute();
	        }
	    }

	    echo "success";
	}

	private static function checkCategory($id, $category) {
		
		if (!$category) {
			return false;
		}

		$db = JFactory::getDBO();
		//$query = "SELECT categories FROM axs_board_templates WHERE id=$id;";
		$query = $db->getQuery(true);
		$query
			->select($db->quoteName('categories'))
			->from($db->quoteName('axs_board_templates'))
			->where($db->quoteName('id') . '=' . $db->quote($id));
		$db->setQuery($query);
		$result = $db->loadObject();

		if (!$result) {
			return false;
		}

		$available = json_decode($result->categories);

		if (!in_array($category, $available)) {
			return false;
		} else {
			return true;
		}
	}

	public static function getTeamMembers() {

		$key = AxsKeys::getKey('boards');
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);

		if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {

			$data = new stdClass();
			$data->members = $_SESSION['user_board'][$board_id]->teammembers;
			$data->leaders = $_SESSION['user_board'][$board_id]->teamleaders;

			echo json_encode($data);
		}
	}

	public static function refreshNotes() {
		$post_id = JRequest::getVar('pid');
		$user_id = JFactory::getUser()->id;

		if (!$user_id) {
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('post_id') . '=' . $db->quote($post_id),
			$db->quoteName('user_id') . '=' . $db->quote($user_id)
		);

		$query
			->select('*')
			->from($db->quoteName('axs_board_user_notes'))
			->where($conditions);

		$db->setQuery($query);

		$results = $db->loadObjectList();

		$return = array();

		foreach ($results as $result) {
			$newNote = new stdClass();
			$newNote->id = $result->id;
			$newNote->date = $result->date;
			$newNote->note = $result->note;

			$return[]= $newNote;			
		}

		echo json_encode($return);
	}

	public static function updateNote() {
		$post_id = JRequest::getVar('pid');
		$board_id = JRequest::getVar('bid');
		$note_id = JRequest::getVar('nid');

		$db = JFactory::getDBO();
		$user_id = JFactory::getUser()->id;

		if (!$user_id) {
			return;
		}

		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('id') . '=' . $db->quote($note_id),
			$db->quoteName('board_id') . '=' . $db->quote($board_id),
			$db->quoteName('post_id') . '=' . $db->quote($post_id),
			$db->quoteName('user_id') . '=' . $db->quote($user_id)
		);

		$query
			->select('*')
			->from($db->quoteName('axs_board_user_notes'))
			->where($conditions)
			->limit(1);
		
		$db->setQuery($query);
		$currentNote = $db->loadObject();

		if (!$currentNote) {
			$return = new stdClass();
			$return->success = false;
			$return->message = "unauthorised";
			echo json_encode($return);
			return;
		}

		//They own the post, save the note to it.

		$note = JRequest::getVar('note');

		if (!$note) {
			$return = new stdClass();
			$return->success = false;
			$return->message = "empty";
			echo json_encode($return);
			return;
		}

		$note = htmlspecialchars($note, ENT_QUOTES);

		$update = date("Y-m-d H:i:s");

		$currentNote->edit_date = date("Y-m-d H:i:s");
		$currentNote->note = $note;

		$db->updateObject('axs_board_user_notes', $currentNote, 'id');
				
		$return = new stdClass();
		$return->success = true;
		$return->message = "success";
		echo json_encode($return);
	}

	public static function postNewNote() {

		$post_id = JRequest::getVar('pid');
		$board_id = JRequest::getVar('bid');

		$db = JFactory::getDBO();
		$user_id = JFactory::getUser()->id;
		
		$check = self::checkPostOwnership($db, $post_id, $board_id, $user_id);
		if ($check !== true) {
			$return = new stdClass();
			$return->success = false;
			$return->message = $check;
			echo json_encode($return);
			return;
		}

		//They own the post, save the note to it.

		$note = JRequest::getVar('note');
		
		if (!$note) {
			$return = new stdClass();
			$return->success = false;
			$return->message = "empty";
			echo json_encode($return);
			return;
		}

		$note = htmlspecialchars($note, ENT_QUOTES);

		$date = date("Y-m-d H:i:s");

		$newNote = new stdClass();
		$newNote->board_id = $board_id;
		$newNote->post_id = $post_id;
		$newNote->user_id = $user_id;
		$newNote->date = $date;
		$newNote->note = $note;

		$db->insertObject('axs_board_user_notes', $newNote);
		$insert_id = $db->insertId();

		$return = new stdClass();
		$return->success = true;
		$return->newEntry = new stdClass();
		$return->newEntry->id = $insert_id;
		$return->newEntry->note = $note;
		$return->newEntry->date = $date;

		echo json_encode($return);

	}

	public static function deleteNote() {
		$post_id = JRequest::getVar('pid');
		$board_id = JRequest::getVar('bid');
		$note_id = JRequest::getVar('nid');

		$user_id = JFactory::getUser()->id;

		if (!$user_id) {
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('id') . '=' . $db->quote($note_id),
			$db->quoteName('board_id') . '=' . $db->quote($board_id),
			$db->quoteName('user_id') . '=' . $db->quote($user_id),
			$db->quoteName('post_id') . '=' . $db->quote($post_id)
		);

		$query
			->delete($db->quoteName("axs_board_user_notes"))
			->where($conditions);

		$db->setQuery($query);
		$db->execute();

		echo "true";
	}

	private static function checkPostOwnership($db, $post_id, $board_id, $user_id) {

		$query = $db->getQuery(true);
		$query
			->select($db->quoteName('board_type'))
			->from($db->quoteName('axs_board_templates'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id));

		$db->setQuery($query);
		$board_type = $db->loadResult();

		if ($board_type) {
			$board = self::getBoardDatabase($board_type);
			if (!$board) {
				return "unauthorised";
			}
		} else {			
			return "unauthorised";
		}

		$conditions = array(
			$db->quoteName('user_id') . '=' . $db->quote($user_id),
			$db->quoteName('board_id') . '=' . $db->quote($board_id),
			$db->quoteName('id') . '=' . $db->quote($post_id)
		);

		$query->clear();
		$query
			->select('id')
			->from($db->quoteName($board))
			->where($conditions);

		$db->setQuery($query);
		$result = $db->loadObject();

		if (!$result) {
			//They don't own this post
			return "unauthorised";
		}

		return true;
	}

	public static function getPostNotesAdmin() {
		$key = AxsKeys::getKey('boards');

		$user_id = JFactory::getUser()->id;
		if (!$user_id) {
			return;
		}
		
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);
		$post_id = JRequest::getVar('post');

		if (self::checkIfBoardAdmin($board_id)) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			$conditions = array(
				$db->quoteName('board_id') . '=' . $db->quote($board_id),
				$db->quoteName('post_id') . '=' . $db->quote($post_id)
			);

			$query
				->select('*')
				->from($db->quoteName('axs_board_user_notes'))
				->where($conditions);

			$db->setQuery($query);

			echo json_encode($db->loadObjectList());
		}
	}

	public static function deletePostAdmin() {
		$key = AxsKeys::getKey('boards');

		$user_id = JFactory::getUser()->id;
		if (!$user_id) {
			return;
		}
		
		$board_id = AxsEncryption::decrypt(base64_decode(JRequest::getVar('board')), $key);
		$post_id = JRequest::getVar('post');

		if (self::checkIfBoardAdmin($board_id)) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			$query
				->select('board_type')
				->from($db->quoteName('axs_board_templates'))
				->where($db->quoteName('id') . '=' . $db->quote($board_id));

			$db->setQuery($query);
			
			$board_type = $db->loadResult();

			$database = self::getBoardDatabase($board_type);

			$query
				->clear()
				->update($db->quoteName($database))
				->set($db->quoteName('deleted') . '=' . $db->quote(date("Y-m-d H:i:s")))
				->where($db->quoteName('id') . '=' . $db->quote($post_id));

			$db->setQuery($query);
			$db->execute();

			echo "true";

			return;
		}
	}

	private static function checkIfBoardAdmin($board_id) {
		if (isset($_SESSION['user_board']) && isset($_SESSION['user_board'][$board_id])) {
			if ($_SESSION['user_board'][$board_id]->admin_access) {
				return true;
			}
		}

		return false;
	}
}