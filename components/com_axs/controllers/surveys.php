<?php

defined('_JEXEC') or die();

class AxsControllerSurveys extends FOFController {

    public function submitAnswer() {
        $response = new stdClass();
        $input  = JFactory::getApplication()->input;
        $answer = $input->get('answer','','STRING');
        $encryptedData = $input->get('survey','','BASE64');
        $encryptedLessonData = $input->get('lesson','','BASE64');
        $likertScore = $input->get('likertScore','','INT');
        $key = AxsKeys::getKey('surveys');
        $surveyData = AxsEncryption::decrypt(base64_decode($encryptedData),$key);
        $date = date('Y-m-d H:i:s');

        $data = new stdClass();
        $data->user_id = JFactory::getUser()->id;
        $data->survey_id = $surveyData->survey_id;
        $data->question_id = $surveyData->question_id;
        $data->question_type = $surveyData->question_type;
        $data->question = $surveyData->question;
        $data->answer = $answer;
        $data->score = $likertScore;
        $data->submitted_date = date("Y-m-d H:i:s");

        $question_number = $surveyData->question_number + 1;
        $surveyObj = AxsSurvey::getSurveyById($surveyData->survey_id);
        $survey = new AxsSurvey($surveyObj);
        $surveyParams = $survey->getParams();

        $questionAmount = count($survey->getQuestions());
        $response->status = 'incomplete';
        $response->completed = 0;

        $surveyProgress = AxsSurvey::getSurveyProgress($data->survey_id,$data->user_id);

        $questionHTML = $survey->loadQuestion($question_number);

        $db = JFactory::getDbo();
        $result = $db->insertObject('axs_survey_responses',$data);


        $progress = round(($question_number / $questionAmount) * 100);

        $progressData = new stdClass();
        $progressData->user_id = $data->user_id;
        $progressData->survey_id = $data->survey_id;
        $progressData->progress = $progress;
        $progressData->date_last_activity = $date;

        if($progress == 100) {
            $response->completed_message = $survey->getCompletedMessage();
            $response->status = 'completed';
            $response->completed = 1;
            $score = AxsSurvey::calculateScores($data->survey_id,$data->user_id);
            $progressData->date_completed = $date;
            $progressData->nps = $score->nps->finalScore;
            $progressData->csat = $score->csat->finalScore;
            $progressData->average_score = $score->totalAverage;
        }


        if($surveyProgress) {
            $surveyProgress->progress = $progress;
            $surveyProgress->date_last_activity = $date;
            if($progress == 100) {
                $surveyProgress->date_completed = $date;
                $surveyProgress->nps = $score->nps->finalScore;
                $surveyProgress->csat = $score->csat->finalScore;
                $surveyProgress->average_score = $score->totalAverage;
            }
            $db->updateObject('axs_survey_progress',$surveyProgress,'id');
        } else {
            $progressData->date_started = $date;
            $db->insertObject('axs_survey_progress',$progressData);
        }

        if($encryptedLessonData) {
            $lessonData = json_decode(base64_decode($encryptedLessonData));
            if($lessonData->activity_id) {
                $response->activity_id = $lessonData->activity_id;
                if($lessonData->activity_required && $response->completed) {
                    $response->runCheckComplete = true;
                }
				$studentActivity = new stdClass();
				$studentActivity->course_id = $lessonData->course_id;
				$studentActivity->lesson_id = $lessonData->lesson_id;
				$studentActivity->activity_id = $lessonData->activity_id;
				$studentActivity->user_id = $data->user_id;
				$studentActivity->activity_type = 'survey';
				$studentActivity->activity_request = $survey->getTitle();
                $studentActivity->student_response = $surveyObj->id;
                $studentActivity->completed = $response->completed;
				$studentActivity->date_submitted = $date;
				$studentActivity->date_modified = $date;
                $studentActivityRow = AxsLMS::getUserActivityByStudentResponse($studentActivity,$studentActivity->student_response);
			} else {
				$studentActivity = false;
			}

            if($studentActivityRow && !$studentActivityRow->completed && $progress == 100) {
                $studentActivityRow->completed = 1;
				$studentActivityRow->date_modified = $date;
                $db->updateObject('#__splms_student_activities',$studentActivityRow,'id');
            }

			if($studentActivity && !$studentActivityRow) {
				$db->insertObject('#__splms_student_activities',$studentActivity);
			}
        }

        if($result) {
            $response->message  = 'success';
            $response->question = $questionHTML;
        } else {
            $response->message = 'error';
        }
        echo json_encode($response);
    }
}