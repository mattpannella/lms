<?php

defined('_JEXEC') or die;

class AxsControllerQuizzes extends FOFController {

    private $quiz;
    private $answer;

    public function submitAnswer() {
        $input = JFactory::getApplication()->input;
        $this->parseInput($input);

        $quizData = $this->quiz->getQuiz();
        $payload = $this->buildPayload();

        $db = JFactory::getDbo();
        $insertSucceeded = $db->insertObject('axs_quiz_responses', $payload);

        $response = [
            'success' => $insertSucceeded,
            'message' => $insertSucceeded ? 'Your response was saved!' : 'Could not save your response. Please try again.'
        ];

        $nextQuestionNumber = $quizData->question_number + 1;

        if($nextQuestionNumber == count($this->quiz->getQuestions())) {

            $response['completed'] = true;
            $response['completedMessage'] = $this->quiz->getCompletedMessage();
            $response['stats'] = $this->quiz->buildQuizStats();
        } else {

            $response['completed'] = false;
            $response['nextQuestion'] = $this->quiz->buildQuestionHtml($nextQuestionNumber);
        }

        echo json_encode($response);
    }

    private function buildPayload() {
        $question = $this->quiz->getCurrentQuestion();

        $payload = new stdClass();
        $payload->quiz_id = $this->quiz->getId();

        // Parse the question data
        $payload->question_id = $question->id;
        $payload->user_id = JFactory::getUser()->id;
        $payload->question_type = $question->type;
        $payload->question_text = $question->text;
        $payload->answer = $this->answer;
        $payload->submitted_date = date('Y-m-d H:i:s');

        return $payload;
    }

    private function parseInput($input) {
        // Grab the encrypted data and decode it using the surveys key
        $encryptedQuiz = $input->get('quiz', '', 'BASE64');
        $key = AxsKeys::getKey('surveys');

        $decryptedQuiz = AxsEncryption::decrypt(base64_decode($encryptedQuiz), $key);

        // Decrypt the encrypted data and store the quizData and answer in this object's corresponding fields
        $this->quiz = new AxsQuiz($decryptedQuiz);
        $this->answer = $input->get('answer', '', 'TEXT');
    }
}
