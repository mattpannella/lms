<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

require_once("libraries/axslibs/builder.php");
require_once("components/com_converge/controllers/subscriptions.php");

defined('_JEXEC') or die();

class AxsControllerServer extends FOFController {

    public function createServer() {

        $init = JRequest::getVar('init');

        if (!isset($_SESSION['aws_creation']) && $init !== "init") {
            //The AWS Creation has not been initialized and there is no init, so something is wrong.  Possibly someone hacking variables.
    		return;
    	}

        switch ($_SESSION['aws_creation']['stage']) {
            case 0:
            case -1:

                $user_id = JFactory::getUser()->id;
                $return = new stdClass();

                //Get the user ID
                //Creating a new subscription automatically logs them in.  IF there is user ID, there is no subscription.
                //Can't give a site to no one.

                if ($user_id == 0) {
                    $return->success = false;
                    $return->message = "User is not currently logged in, or the subscription was unsuccessful.";
                    echo json_encode($return);
                    return;
                } else {

                    //Check if the user is a super admin.  If so, allow them to just build the site.
                    $admin = false;
                    $user = JFactory::getUser();
                    $superadmin = $user->authorise('core.admin');
                    $usergroups = JAccess::getGroupsByUser($user->id);
                    if(in_array(32, $usergroups)) {
                        $admin = true;
                    }
                if ($superadmin || $admin) {
                        $_SESSION['aws_creation']['subscription_id'] = -1;
                        TovutiBuilder::init();
                        $return->success = true;
                        echo json_encode($return);
                        return;
                    }



                    /*
                        This password is stored in the Subscriptions controller: /components/com_converge/controllers/subscriptions.php
                        When the subscription is successful, the id is sent back (encrypted) with AJAX.
                        It's encrypted so attackers can't numerically guess the next ID and can only use the one they have the encrypted
                        text for.

                        The check before a site is launched is done in two ways.
                        First, an AWS site/subscription is pulled if there already exists one with the current USER ID and SUBSCRIPTION ID
                        Next, an AXS subscription is pulled with the SUBSCRIPTION ID
                    */

                    $password = ConvergeControllerSubscriptions::$subscription_password;
                    $subscription_id = AxsEncryption::decrypt(JRequest::getVar('subscription'), $password);

                    $db = JFactory::getDBO();

                    $query = $db->getQuery(true);

                    $conditions = array(
                        $db->quoteName('user_id') . '=' . $db->quote($user_id),
                        $db->quoteName('subscription_id') . '=' . $db->quote($subscription_id)
                    );

                    $query
                        ->select('*')
                        ->from($db->quoteName('axs_aws_subscriptions'))
                        ->where($conditions);

                    //$query = "SELECT * FROM axs_aws_subscriptions WHERE user_id = $user_id AND subscription_id = $subscription_id";
                    $db->setQuery($query);
                    $server_subscription = $db->loadObject();

                    $query = $db->getQuery(true);
                    $query
                        ->select('*')
                        ->from('axs_pay_subscriptions')
                        ->where($db->quoteName('id') . '=' . $db->quote($subscription_id));

                    //$query = "SELECT * FROM axs_pay_subscriptions WHERE id=$subscription_id";
                    $db->setQuery($query);
                    $subscription_plan = $db->loadObject();

                    //var_dump($result);
                    if (!$server_subscription) {

                        //There is no entry for this subscription.  It is a new subscription.
                        if ($subscription_plan->status == "ACT") {
                            //Their subscription was successful so they can make the site.
                            $_SESSION['aws_creation']['subscription_id'] = $subscription_id;
                            TovutiBuilder::init();
                        } else {
                            $return->success = false;
                            $return->message = "The subscription was not successful.  Server build process will not continue.";
                            echo json_encode($return);
                            return;
                        }


                    } else {
                        //A subscription already exists for this user_id and subscription_id
                        //Check if it has completed.
                        if ($server_subscription->completed === "1") {
                            $return->success = false;
                            $return->message = "This subscription already has an active site attached to it.\nIf you are the owner of this site and feel you have received this message in error, please contact support.";
                            echo json_encode($return);
                            return;
                        } else {
                            if ($subscription_plan->status == "ACT") {
                                //Something must have gone wrong before.  The user has an active subscription, but the site never finished.
                                //echo "subscription is active and ready to go.";
                                $_SESSION['aws_creation']['subscription_id'] = $subscription_id;
                                TovutiBuilder::init();

                            } else {
                                $return->success = false;
                                $return->message = "The subscription was not successful.  Server build process will not continue.";
                                echo json_encode($return);
                                return;
                            }
                        }
                    }

                    $return->success = true;
                    echo json_encode($return);
                    return;
                }

                break;
            case 1:

                $settings = JRequest::getVar('settings');

                //$settings is passed by reference, so changes that are made to it in the validation function continue on.
                $validation = TovutiBuilder::validateAndPrice($settings);
                if (!$validation->valid) {
                    $return->success = false;
                    $return->message = $validation->message;
                    TovutiBuilder::updateStatus($validation->message);
                    echo json_encode($return);
                    return;
                } else {
                    $price = $validation->price->total;
                }

                $_SESSION['aws_creation']['server_price'] = $price;

                TovutiBuilder::newAccount($settings);
                echo "success";
                break;
            case 2:
                TovutiBuilder::copyFiles();
                echo "success";

                break;
            case 3:
                TovutiBuilder::saveUserData();
                echo "success";
                break;
            case 4:
                TovutiBuilder::cleanup();
                break;
        }
    }

    public function getCodeLog() {
        if (!isset($_SESSION['aws_creation'])) {
            return;
        } else {
            $return = new stdClass();
            $return->log = $_SESSION['aws_creation']['log'];
            $return->plog = $_SESSION['aws_creation']['plog'];
            $return->code = $_SESSION['aws_creation']['code'];

            echo json_encode($return);
        }
    }

    public function getStage() {

    	if (!isset($_SESSION['aws_creation']) || $_SESSION['aws_creation'] == null || !isset($_SESSION['aws_creation']['stage'])) {
    		$_SESSION['aws_creation']['stage'] = 0;
    	} /*else {
            if (!isset($_SESSION['aws_creation']['stage']) {
                $_SESSION['aws_creation']['stage'] = 0;
            }
        }*/

        //var_dump($_SESSION['aws_creation']);

        echo $_SESSION['aws_creation']['stage'];
    }

    public function update() {

    	if (!isset($_SESSION['aws_creation'])) {
    		return;
    	}

        $db = JFactory::getDBO();

        $code = JRequest::getVar('code');

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_aws_status_update'))
            ->where($db->quoteName('id') . '=' . $db->quote($code));

        //$query = "SELECT status FROM axs_aws_status_update WHERE id= '$code'";
        $db->setQuery($query);
        $result = $db->loadResult();

        echo $result;
    }

    public function validateDomainName() {
    	$name = JRequest::getVar('domain_name');
    	$valid = TovutiBuilder::validateDomainName($name);
    	echo json_encode($valid);
    }

    public function getRootDomainName() {
        $valid = TovutiBuilder::getRootDomainName();
        echo json_encode($valid);
    }

    public function validateAndPrice() {
        $settings = JRequest::getVar('settings');
        $validate = JRequest::getVar('validate');
        if (!$settings) {
            return;
        }

        if ($validate == 'true') {
            $validate = true;
        } else {
            $validate = false;
        }

        $validation = TovutiBuilder::validateAndPrice($settings, $validate);
        echo json_encode($validation);
    }
}

