<?php

defined("_JEXEC") or die();

require_once 'components/com_eventbooking/helper/mail.php';
require_once 'administrator/components/com_eventbooking/libraries/rad/form/form.php';
require_once 'administrator/components/com_eventbooking/libraries/rad/form/field.php';
require_once 'administrator/components/com_eventbooking/libraries/rad/form/field/text.php';
require_once 'components/com_eventbooking/payments/os_payments.php';
require_once 'components/com_eventbooking/helper/html.php';
require_once 'components/com_eventbooking/helper/ics.php';

class AxsControllerTeams extends FOFController {

    public function setDefault() {
        $input = JFactory::getApplication()->input;
        $team_id = $input->get('team_id',0,'INT');
        $response = new stdClass();
        if(!$team_id) {
            $response->status = 'error';
        } else {
            $db = JFactory::getDbo();
            $user = JFactory::getUser();
            $defaultTeamRow = AxsTeamLeadDashboard::getLeaderDefaultTeam($user->id);
            if($defaultTeamRow) {
                $defaultTeamRow->team_id = (int)$team_id;
                $db->updateObject('axs_team_dashboard_defaults',$defaultTeamRow,'id');
            } else {
                $newTeamDefaultRow = new stdClass();
                $newTeamDefaultRow->team_id = (int)$team_id;
                $newTeamDefaultRow->user_id = (int)$user->id;
                $db->insertObject('axs_team_dashboard_defaults',$newTeamDefaultRow);
            }
            $response->status  = 'success';
            $response->team_id = (int)$team_id;
        }
        echo json_encode($response);
    }

    public function removeUser() {
        $response = new stdClass();
        $response->status = 'error';
        $returnUrl = $this->input->get('return', 0, 'STRING');
        $params    = $this->input->get('params', 0, 'BASE64');
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($params), $key);
        $returnUrl = urldecode($encryptedParams->returnUrl);
        //check to make sure they have permission to this team
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);

        if(!$team || $userId != $encryptedParams->user_id) {
            return;
        }

        if($teamParams->allow_removing_users_from_team) {
            if($team->member_type == 'individuals') {
                AxsTeams::removeTeamMembers(array($encryptedParams->edit_user_id), $teamId);
            } else {

                $requiredGroups   = explode(',',$team->user_groups);
                foreach($requiredGroups as $groupId) {
                    JUserHelper::removeUserFromGroup($encryptedParams->edit_user_id,$groupId);
                    AxsActions::trackUserGroup($encryptedParams->edit_user_id,$groupId,'remove');
                }

            }
        }
        JFactory::getApplication()->redirect($returnUrl);
    }

    public function deleteUser() {
        $response = new stdClass();
        $response->status = 'error';
        $returnUrl = $this->input->get('return', 0, 'STRING');
        $params    = $this->input->get('params', 0, 'BASE64');
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($params), $key);
        $returnUrl = urldecode($encryptedParams->returnUrl);
        //check to make sure they have permission to this team
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);

        if(!$team || $userId != $encryptedParams->user_id) {
            return;
        }

        if($teamParams->allow_removing_users_from_system) {
            $user = JFactory::getUser($encryptedParams->edit_user_id);
            $user->delete();
        }
        JFactory::getApplication()->redirect($returnUrl);
    }

    public function deleteAssignment() {
        $db = JFactory::getDbo();
        $table = "#__splms_courses_groups";
        $response = new stdClass();
        $response->status = 'error';
        $encode  = $this->input->get('encode', '', 'BASE64');
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($encode), $key);
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);

        if(!$team || $userId != $encryptedParams->user_id || $encryptedParams->team_id != $encryptedParams->assignment_team_id) {
            return;
        }

        $query        = $db->getQuery(true);
        $conditions[] = $db->quoteName('team_id').'='.$db->quote($encryptedParams->team_id);
        $conditions[] = $db->quoteName('id').'='.$db->quote($encryptedParams->assignment_id);
        $query->delete($db->quoteName($table));
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->execute();
        if($result) {
            $response->status = 'success';
        }
        echo json_encode($response);
        exit;
    }

    public function assignCourses() {
        $db = JFactory::getDbo();
        $table = "#__splms_courses_groups";
        $response = new stdClass();
        $response->status = 'error';
        $message = '';
        $title     = $this->input->get('title', '', 'STRING');
        $course_ids = $this->input->get('course', '', 'ARRAY');
        $type  = $this->input->get('assign_type', 0, 'INT');
        $assign_to  = $this->input->get('assign_to', '', 'STRING');
        $due_date  = $this->input->get('due_date', '', 'STRING');
        $user_ids  = $this->input->get('user', '', 'ARRAY');
        $group_ids  = $this->input->get('group', '', 'ARRAY');
        $encode  = $this->input->get('encode', '', 'BASE64');
        $course_ids = implode(',',$course_ids);
        $user_ids   = implode(',',$user_ids);
        $group_ids  = implode(',',$group_ids);
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($encode), $key);
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$teamId);
        $teamParams = json_decode($team->params);

        if(!$team || $userId != $encryptedParams->user_id || !$teamParams->allow_assigning_courses) {
            return;
        }
        
        //validate
        if(!$title) {
            $response->message = AxsLanguage::text("AXS_ASSIGNMENT_TITLE_REQUIRED", "You must include an Assignment Title");
            echo json_encode($response);
            exit;
        }
        if(!$course_ids) {
            $response->message = AxsLanguage::text("AXS_INCLUDE_ONE_COURSE_REQUIRED", "You must include at least one course");
            echo json_encode($response);
            exit;
        }
        if((!$user_ids && $assign_to == 'users') && (!$group_ids && $assign_to == 'groups') ){
            $response->message = AxsLanguage::text("AXS_INCLUDE_SPECIFIC_USERS_OR_SELECTED_USERGROUPS", "You must include either specific users or selected user groups");
            echo json_encode($response);
            exit;
        }

        $params = new stdClass();
        $params->send_notifications = 1;
        $data   = new stdClass();
        $data->title = AxsSecurity::alphaNumericOnly($title);
        $data->course_ids = $course_ids;
        if($assign_to == 'groups') {
            $data->usergroups = $group_ids;
        } else {
            $data->usergroups = "";
            $data->user_ids = $user_ids;
        }

        $data->free = 1;
        $data->type = (int)$type;
        $data->due_date = $due_date;
        $data->creator_id = $userId;
        $data->team_id = $encryptedParams->team_id;
        $data->item->user_ids = $data->user_ids;
        $data->item->usergroups = $data->usergroups;
        $data->item->course_ids = $data->course_ids;
        $data->params = json_encode($params);

        if($encryptedParams->action == 'edit') {
            $data->id = $encryptedParams->assignment_id;
            $result = $db->updateObject($table,$data,'id');
            $message = "Assignment was successfully updated";
        } else {
            $result = $db->insertObject($table,$data);
            $message = "Assignment was successfully added";
            $data->alert_type = 'course assigned';
            $data->item->user_ids = $data->user_ids;
            $data->item->usergroups = $data->usergroups;
            $data->item->course_ids = $data->course_ids;
            $data->alert_type = 'course assigned';
            
            $alert = new AxsAlerts();
            $alert->runAutoAlert($data->alert_type, $data);
        }
        if($result) {
            $response->status  = 'success';
            $response->message = $message;
        } else {
            $response->status  = 'error';
            $response->message = 'There was a error saving this assignment';
        }

        $alert = new AxsAlerts();
        $alert->runAutoAlert($data->alert_type, $data);

        echo json_encode($response);
        return;
    }

    public function getEventRegistrations($event_id,$user_ids) {
        if(!$event_id || !$user_ids) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $event_cond = $db->quoteName('event_id') . ' = ' . $db->quote($event_id);
        $user_cond = $db->quoteName('user_id') . ' IN (' . implode(',', $user_ids) . ')';
        $conditions[] = $event_cond . ' AND ' . $user_cond;
        $query->select('*');
        $query->from($db->quoteName('#__eb_registrants'));
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadAssocList();
        return array_reduce($results, function($acc, $item) {
            $acc[$item['user_id']] = $item;
            return $acc;
        }, []);
    }

    public function getEventRegistration($event_id,$user_id) {
        if(!$event_id || !$user_id) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions[] = $db->quoteName('event_id').'='.$db->quote($event_id);
        $conditions[] = $db->quoteName('user_id').'='.$db->quote($user_id);
        $query->select('*');
        $query->from($db->quoteName('#__eb_registrants'));
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    private static function getEventRegistrantCountById($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*) as count')
          ->from($db->qn('#__eb_registrants'))
          ->where($db->qn('event_id') . '=' . (int)$id);
        $db->setQuery($query);
        $res = $db->loadObject();
        return $res->count;
    }

    private static function getEventCapacityById($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id, title, event_capacity')
          ->from($db->qn('#__eb_events'))
          ->where($db->qn('id').'='.(int)$id);
        $db->setQuery($query);
        $res = $db->loadObject();
        return $res;
    }

    public function assignEvents() {
        $db = JFactory::getDbo();
        $response = new stdClass();
        $response->status = 'error';

        $event_ids = $this->input->get('event', '', 'ARRAY');
        $event_ids = explode(',', $event_ids[0]);
        $event_ids = array_filter($event_ids);

        $user_ids  = $this->input->get('user_events', '', 'ARRAY');
        $user_ids = explode(',', $user_ids[0]);
        $user_ids = array_filter($user_ids);

        if(empty($event_ids)) {
            $response->message = AxsLanguage::text("AXS_MUST_INCLUDE_AT_LEAST_ONE_EVENT", "You must include at least one event");
            echo json_encode($response);
            exit;
        }
        if(empty($user_ids)){
            $response->message = AxsLanguage::text("AXS_MUST_INCLUDE_AT_LEAST_ONE_USER", "You must include at least one user");
            echo json_encode($response);
            exit;
        }

        $encode  = $this->input->get('encode', '', 'BASE64');
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($encode), $key);
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);

        if(!$team || $userId != $encryptedParams->user_id || !$teamParams->allow_assigning_events) {
            return;
        }

        //validate
        $overCapacity = false;
        foreach ($event_ids as $event_id) {
            $eventData = self::getEventCapacityById($event_id);
            $capacity = $eventData->event_capacity;
            if ($capacity > 0) {
                $registrantCount = self::getEventRegistrantCountById($event_id);
                $userCount = count($user_ids);
                if ($registrantCount + $userCount > $capacity) {
                    $overCapacity = true;
                    // $response->message .= JText::sprintf("Too many users ($userCount) included for event " . $eventData->title . ". Registration is at $registrantCount/$capacity registrants.\n";
                    $response->message .= JText::sprintf("AXS_TOO_MANY_USERS_REGISTERED_FOR_EVENT", $userCount, $eventData->title, $registrantCount, $capacity);
                }
            }
        }
        if ($overCapacity) {
            $response->message .= AxsLanguage::text("AXS_PLEASE_REDUCE_USERS_TRY_AGAIN", "Please reduce the number of users or change events and try again.");
            echo json_encode($response);
            return;
        }

        $result = false;
        foreach($event_ids as $event_id) {
            // this will return an array of registrations with the user id as the key
            // it will help in only running a single query per event to check if the
            // user has already registered for the event
            $registrations = $this->getEventRegistrations($event_id, $user_ids);

            foreach($user_ids as $user_id) {
                // if there isn't a key with the current user id, it means the user is
                // not registered to the event and we can safely insert it into the db
                if (empty($registrations[$user_id])) {
                    $firstName = '';
                    $lastName  = '';
                    $user      = '';
                    $user = JFactory::getUser($user_id);
                    $names = AxsLearnerDashboard::getFirstAndLastName($user_id);
                    $firstName = $names['FIELD_GIVENNAME'];
                    $lastName = $names['FIELD_FAMILYNAME'];

                    if(!$firstName || !$lastName) {
                        $name = explode(' ',$user->name);
                        $firstName = $name[0];
                        unset($name[0]);
                        $lastName  = implode(' ',$name);
                    }

                    $data   = new stdClass();
                    $data->event_id = $event_id;
                    $data->user_id  = $user_id;
                    $data->register_date  = date('Y-m-d H:i:s');
                    $data->number_registrants = 1;
                    $data->registration_code = JUserHelper::genRandomPassword(10);
                    $data->transaction_id = strtoupper(JUserHelper::genRandomPassword());
                    $data->amount = 0;
                    $data->email = $user->email;
                    $data->first_name = $firstName;
                    $data->last_name  = $lastName;
                    $data->published  = 1;
                    $config = EventbookingHelper::getConfig();

                    $db->insertObject("#__eb_registrants", $data);
                    $result = true;
                }
            }
        }

        if($result) {
            $response->status  = 'success';
            $response->message =  AxsLanguage::text(
                "AXS_EVENT_REGISTRATIONS_SUCCESSFULLY_ADDED",
                "Event Registrations were successfully added"
            );
            EventbookingHelper::sendEmails($data, $config);
        } else {
            $response->status  = 'error';
            $response->message = AxsLanguage::text(
                'AXS_ERROR_SAVING_EVENT_REGISTRATIONS',
                'There was an error saving these Event Registrations'
            );
        }
        echo json_encode($response);
        return;
    }

    public function update() {

        $response = new stdClass();
        $response->status = 'error';
        $register_email     = $this->input->get('register_email', 0, 'STRING');
		$register_firstname = $this->input->get('register_firstname', 0, 'STRING');
		$register_lastname  = $this->input->get('register_lastname', 0, 'STRING');
		$register_username  = $this->input->get('register_username', 0, 'STRING');
		$register_password  = $this->input->get('register_password', 0, 'STRING');
        $register_password2 = $this->input->get('register_password2', 0, 'STRING');
        $register_password_reset = $this->input->get('password_reset', 0, 'STRING');
        $register_params    = $this->input->get('params', 0, 'BASE64');
        $register_groups    = $this->input->get('groups', 0, 'ARRAY');

        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($register_params), $key);

        //check to make sure they have permission to this team
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);

        $forbiddenUserGroups = [8];
        $forbiddenUserIds    = [601];
        $allowedGroups = $encryptedParams->team_params->user_groups;

        if(!$team || $userId != $encryptedParams->user_id) {
            return;
        }


        if($team->member_type == 'groups') {
            $validGroupFound = false;
            $requiredGroups   = explode(',',$team->user_groups);
            foreach($register_groups as $gid) {
                if(in_array($gid,$requiredGroups)) {
                    $validGroupFound = true;
                }
            }
            if(!$validGroupFound) {
                $response->error  = "To be a part of this team you must assign at least one of the following groups:";
                $response->error .= '<ul>';
                foreach($requiredGroups as $group) {
                    $response->error .= "<li>".AxsExtra::getUsergroupName($group)."</li>";
                }
                $response->error .= "</ul>";
                echo json_encode($response);
                return;
            }
        }

		if($register_email && $register_firstname && $register_lastname) {
			$data['name'] = $register_firstname.' '.$register_lastname;
			$data['username'] = $register_username;
            $data['email']    = $register_email;
            if($encryptedParams->action == 'register') {
                $data['password'] = $register_password;
                $data['password2'] = $register_password2;
			    $data['activation'] = '0';
                $data['block'] = '0';
            } else {
                if($register_password) {
                    $data['password'] = $register_password;
                    $data['password2'] = $register_password2;
                }
            }
            if($register_password_reset == 'yes') {
                $data['requireReset'] = 1;
            } else {
                $data['requireReset'] = 0;
            }

            if($encryptedParams->action == 'register') {
                $data['groups'][] = 2;
            }

            if($encryptedParams->action == 'edit') {
                if(!$encryptedParams->edit_user_id || in_array($encryptedParams->edit_user_id,$forbiddenUserIds)) {
                    exit;
                }
                $user = JFactory::getUser($encryptedParams->edit_user_id);
                $current_user_groups = JUserHelper::getUserGroups($user->id);
                $message = 'User Successfully Updated';
            } else {
                $user = new JUser;
                $message = 'User Successfully Created';
            }

            if($register_groups && $allowedGroups) {
                //strip all usergroups that are not allowed
                foreach($register_groups as $key => $groupId) {
                    if (!in_array($groupId,$allowedGroups) || in_array($groupId,$forbiddenUserGroups)) {
                        unset($register_groups[$key]);
                    } else {
                        if($encryptedParams->action == 'register') {
                            array_push($data['groups'],$groupId);
                        } else {
                            if(!in_array($groupId,$current_user_groups)) {
                                JUserHelper::addUserToGroup($user->id,$groupId);
                                AxsActions::trackUserGroup($user->id,$groupId,'add');
                            }
                        }
                    }
                }
            }

            if($encryptedParams->action == 'edit') {
                foreach($allowedGroups as $allowedGroupId) {
                    if(!in_array($allowedGroupId,$register_groups) && !in_array($allowedGroupId,$forbiddenUserGroups)) {
                        JUserHelper::removeUserFromGroup($user->id,$allowedGroupId);
                        AxsActions::trackUserGroup($user->id,$allowedGroupId,'remove');
                    }
                }
            }

			//Write to database
			if(!$user->bind($data)) {
				$response->status = "FAIL";
				$response->error = $user->getError();
				echo json_encode($response);
				return;
			}

			if (!$user->save()) {
				$response->status = "FAIL";
				$response->error = $user->getError();
				echo json_encode($response);
				return;
            }

            $data = new stdClass();
            $data->userId = $user->id;
            $data->firstName = $register_firstname;
            $data->lastName = $register_lastname;
            if($encryptedParams->action == 'register') {
                AxsUser::setProfileNameFields($data);
            } else {
                if(!AxsUser::getFirstName($user->id) || !AxsUser::getLastName($user->id)) {
                    AxsUser::setProfileNameFields($data);
                } else {
                    AxsUser::updateProfileNameFields($data);
                }
            }

            if($team->member_type == 'individuals') {
                AxsTeams::addTeamMembers(array($user->id), $teamId);
            }

            $response->status = 'success';
            $response->message = $message;
            echo json_encode($response);
            return;
		}
    }

    public function getAssignmentList() {
        $encode  = $this->input->get('encode', 0, 'BASE64');
        $key = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($encode), $key);
        $userId = JFactory::getUser()->id;
        $teamId = $encryptedParams->team_id;
        $team   = AxsTeamLeadDashboard::getSelectedTeam($userId,$encryptedParams->team_id);
        $teamParams = json_decode($team->params);
        if(!$team || $userId != $encryptedParams->user_id) {
            return;
        }
        $assignments = AxsTeamLeadDashboard::getTeamAssignments($teamId);
        $actionParams = new stdClass();
        $actionParams->team_id = $teamId;
        $actionParams->user_id = $userId;
        $actionParams->assignment_team_id = $teamId;

        ob_start();
            include 'components/com_axs/templates/assignment_list.php';
        $html = ob_get_clean();
        echo $html;
    }

    public function getTeamMembersHTML() {
        $team   = $this->input->get('team', 0, 'BASE64');
        $add    = $this->input->get('add', 0, 'BASE64');
        $start  = $this->input->get('start', 0, 'INT');
        $size   = $this->input->get('size', 0, 'INT');
        $search = $this->input->get('search', 0, 'STRING');
        $sort   = $this->input->get('sort', 0, 'STRING');
        $count  = $start + $size;
        $key    = AxsKeys::getKey('lms');
        $encryptedParams = AxsEncryption::decrypt(base64_decode($team), $key);
        $dashboardParams = $encryptedParams->dashboard_params;
        $reportParams = new stdClass();
        $reportParams->team_id = (int)$encryptedParams->team_id;
        $reportParams->learnerDashboard_id = (int)$encryptedParams->learnerDashboard_id;
        $actionParams = AxsEncryption::decrypt(base64_decode($add), $key);
        $userList     = $encryptedParams->team_members;
        $teamParams   = $encryptedParams->team_params;
        $teamMembers  = AxsTeamLeadDashboard::getTeamMembersAJAX($sort,$search,$userList);
        ob_start();
		include 'components/com_axs/templates/team_members_list.php';
        $html = ob_get_clean();
        $response = new stdClass();
        $response->html = $html;
        echo json_encode($response);
    }
}
