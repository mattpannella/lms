<?php

defined('_JEXEC') or die();

require_once 'components/shared/controllers/rating.php';

class AxsControllerRatings extends FOFController {



	public function updateRatings() {

		$key = AxsKeys::getKey('ratings');

		$data = JRequest::getVar('data');

		$decode = base64_decode($data);
		$decrypt = AxsEncryption::decrypt($decode, $key);

		$rating_id = $decrypt->rating_id;
		$user_rating = $decrypt->count;

		$user_id = JFactory::getUser()->id;

		/*
            Check to see if the user has already rated it.

                If so, update their rating.

                If not, add their rating.

                Either way, update the totals database when done.

        */

		$db = JFactory::getDBO();

		$query = "SELECT * FROM ratings_user WHERE user_id = $user_id and rating_id = '$rating_id' LIMIT 1;";
		

		$db->setQuery($query);
		$result = $db->loadObject();

		$date = strtotime('now');

		$query = $db->getQuery(true);
		if ($result == null) {
            //The user has not rated this before

            $update_type = 'new';
            $previous_rating = -1;

            $columns = array(
                'user_id', 
                'rating_id',
                'user_rating',
                'date'
            );

            $values = array(
                (int)$user_id,
                $db->quote($rating_id),
                (int)$user_rating,
                (int)$date
            );

            $query
            	->insert($db->quoteName('ratings_user'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $db->execute();
        } else {
            //The user is updating their rating

            $update_type = 'update';
            $previous_rating = $result->user_rating;

            $conditions = array(
                $db->quoteName('id') . '=' . (int)$result->id
            );

            $fields = array(
                $db->quoteName('user_rating') . '=' . $db->quote($user_rating)
            );
            
            $query
                ->update($db->quoteName('ratings_user'))
                ->set($fields)
                ->where($conditions);
        
            $db->setQuery($query);
            $db->execute();
        }

        //Update the rating_post table for the new results
        //Check to see if the table already has ratings for the current rating id

        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('rating_id') . '=' . $db->quote($rating_id)
        );

        $query
            ->select('*')
            ->from('ratings_post')
            ->where($conditions);

        $db->setQuery($query);
        $result = $db->loadObject();

        switch((int)$user_rating) {
            case 1: $star_slot = 'stars1'; break;
            case 2: $star_slot = 'stars2'; break;
            case 3: $star_slot = 'stars3'; break;
            case 4: $star_slot = 'stars4'; break;
            case 5: $star_slot = 'stars5'; break;
        }

        $query = $db->getQuery(true);

        if ($result == null) {
            //The entry does not exist, update it with new data

            $columns = array(
                'rating_id',
                $star_slot
            );
            
            $values = array(            
                $db->quote($rating_id),
                1
            );

            $query
                ->insert($db->quoteName('ratings_post'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $db->execute();
        } else {

            $conditions = array(
               $db->quoteName('id') . '=' . (int)$result->id
            );   
            
            if ($update_type == 'new') {            
                //This is a new entry, so simply increment the star rating
                $fields = array(                
                    $db->quoteName($star_slot) . '=' . $db->quoteName($star_slot) . '+1'
                );            
            } else {

                //The user has already rated this post and is changing their rating
                //Decrement the previous rating and add to the new one.

                //Only update if the user actually changed their rating.
                
                switch((int)$previous_rating) {
                    case 1: $star_slot_prev = 'stars1'; break;
                    case 2: $star_slot_prev = 'stars2'; break;
                    case 3: $star_slot_prev = 'stars3'; break;
                    case 4: $star_slot_prev = 'stars4'; break;
                    case 5: $star_slot_prev = 'stars5'; break;
                }
                    
                $fields = array(
                    $db->quoteName($star_slot_prev) . '=' . $db->quoteName($star_slot_prev) . '-1',
                    $db->quoteName($star_slot) . '=' . $db->quoteName($star_slot) . '+1'
                );            
            }

            if ($user_rating != $previous_rating) {
                $query
                    ->update($db->quoteName('ratings_post'))
                    ->set($fields)
                    ->where($conditions);

                $db->setQuery($query);
                $result = $db->execute();
            }
        }

        ob_start();
        	display_ratings($rating_id);
        $newHtml = ob_get_clean();

        $data = new stdClass();
        $data->rating_id = $rating_id;
        $data->html = $newHtml;

        echo json_encode($data);
        
	}	
}