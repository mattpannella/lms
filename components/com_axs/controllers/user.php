<?php

defined('_JEXEC') or die();

class AxsControllerUser extends FOFController {

	public function generateOteps($count = 10) {
		$oteps = array();
		$salt = "0123456789";
		$base = strlen($salt);
		$length = 16;
		for ($i = 0; $i < $count; $i++) {
			$makepass = '';
			$random = JCrypt::genRandomBytes($length + 1);
			$shift = ord($random[0]);
			for ($j = 1; $j <= $length; ++$j) {
				$makepass .= $salt[($shift + ord($random[$j])) % $base];
				$shift += ord($random[$j]);
			}
			$oteps[] = $makepass;
		}
		return $oteps;
	}

	public function saveOtpKey() {
		$key = JFactory::getConfig()->get('secret');
		$response = new stdClass();
        $response->status = 'error';
        $secretKey   = $this->input->get('secretKey', 0, 'STRING');
		$securityKeyEncoded = $this->input->get('securityKey', 0, 'STRING');
		$encryptKey = AxsKeys::getKey('lms');
		$securityKey = AxsEncryption::decrypt(base64_decode($securityKeyEncoded), $encryptKey);
		$userId = JFactory::getUser()->id;

		if(!$securityKey || !$secretKey) {
			$response->message = AxsLanguage::text("AXS_SECURITY_CODE_MISSING", "Security Code or Key Missing");
			echo json_encode($response);
			return;
		}
		$totp = new FOFEncryptTotp(30, 6, 10);
		$code = $totp->getCode($securityKey);
		$check = $code === $secretKey;

		/*
		* If the check fails, test the previous 30 second slot. This allow the
		* user to enter the security code when it's becoming red in Google
		* Authenticator app (reaching the end of its 30 second lifetime)
		*/
		if (!$check) {
			$time = time() - 10;
			$code = $totp->getCode($securityKey, $time);
			$check = $code === $secretKey;
		}

		/*
		* If the check fails, test the next 30 second slot. This allows some
		* time drift between the authentication device and the server
		*/
		if (!$check) {
			$time = time() + 10;
			$code = $totp->getCode($securityKey, $time);
			$check = $code === $secretKey;
		}

		if (!$check) {
			$response->message = AxsLanguage::text("AXS_SECURITY_KEY_INVALID", "Your Security Key was invalid");
			echo json_encode($response);
			return;
		}

		$userObj = AxsUser::getUser($userId);
		$keyParams = new stdClass();
		$keyParams->code = $securityKey;
		$userObj->otpKey = "totp:".json_encode($keyParams);
		$aes = new FOFEncryptAes($key, 256);
		$encryptedOtep	= $this->generateOteps();
		$decryptedOtep = json_encode($encryptedOtep);
		$aes = new FOFEncryptAes($key, 256);
		$userObj->otep = $aes->encryptString($decryptedOtep);
		$db = JFactory::getDbo();
		$result = $db->updateObject('#__users',$userObj,'id');
		if($result) {
			$response->status = 'success';
			$response->backups = $encryptedOtep;
		}
		echo json_encode($response);
	}


	public function saveYubiKey() {
		$key = JFactory::getConfig()->get('secret');
		$response = new stdClass();
        $response->status = 'error';
        $secretKey = $this->input->get('secretKey', 0, 'STRING');
		$userId = JFactory::getUser()->id;
		$userObj = AxsUser::getUser($userId);
		$yubikey = substr($secretKey, 0, -32);
		$keyParams = new stdClass();
		$keyParams->yubikey = $yubikey;
		$userObj->otpKey = "yubikey:".json_encode($keyParams);
		$aes = new FOFEncryptAes($key, 256);
		$encryptedOtep	= $this->generateOteps();
		$decryptedOtep = json_encode($encryptedOtep);
		$aes = new FOFEncryptAes($key, 256);
		$userObj->otep = $aes->encryptString($decryptedOtep);
		$db = JFactory::getDbo();
		$result = $db->updateObject('#__users',$userObj,'id');
		if($result) {
			$response->status = 'success';
			$response->backups = $encryptedOtep;
		}
		echo json_encode($response);
	}

    public function register() {
        $response = new stdClass();
        $response->status = 'error';
        $register_email     = $this->input->get('register_email', 0, 'STRING');
		$register_firstname = $this->input->get('register_firstname', 0, 'STRING');
		$register_lastname  = $this->input->get('register_lastname', 0, 'STRING');
		$register_username  = $this->input->get('register_username', 0, 'STRING');
		$register_password  = $this->input->get('register_password', 0, 'STRING');
		$register_password2 = $this->input->get('register_password2', 0, 'STRING');
		$login_page_id	    = base64_decode($this->input->get('login_page_id', 0, 'STRING'));

		$isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
		if(!$isIOS) {
			$captchaResponse    = $this->input->get('g-recaptcha-response');

			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$res = $dispatcher->trigger('onCheckAnswer',$captchaResponse);

			if(!$res[0]) {
				$response->error = "Invalid Captcha";
				echo json_encode($response);
				return;
			}
		}

		$validatePassword = AxsSecurity::validatePassword($register_password);
		if($validatePassword->success === false) {
			$response->status = "FAIL";
			$response->error = $validatePassword->error;
			echo json_encode($response);
			return;
		}

		if($register_email && $register_firstname && $register_lastname) {
			$data['name'] = $register_firstname.' '.$register_lastname;
			$data['username'] = $register_username;
			$data['email']    = $register_email;
			$data['password'] = $register_password;
			$data['activation'] = '0';
			$data['block'] = '0';
			$data['groups'][] = 2;

			$user = new JUser;
			//Write to database
			if(!$user->bind($data)) {
				$response->status = "FAIL";
				$response->error = $user->getError();
				echo json_encode($response);
				return;
			}

			if (!$user->save()) {
				$response->status = "FAIL";
				$response->error = $user->getError();
				echo json_encode($response);
				return;
			}

			if($user->id) {
				$session = JFactory::getSession();
				$session->set('user',$user);

				$userData = new stdClass();
				$userData->userId = $user->id;
				$userData->firstName = $register_firstname;
				$userData->lastName = $register_lastname;
                AxsUser::setProfileNameFields($userData);
				AxsActions::storeAction($user->id,'login');

				// We want to use the current brand for the site name
				$brand = AxsBrands::getBrand();

				// Define the template replacements
				$replacements = [
					'name' => $user->name,
					'website_name' => $brand->site_title,
					'website_link' => JUri::root()
				];

				// Get the template information from the login page params
				$loginPage = AxsLandingPages::getLoginPage($login_page_id);

				/* Send a registration email to the user if there is a login page configuration present and
				 * the login page is configured to have registration emails sent to newly registered users.
				 */
				if(!empty($loginPage)) {

					if(!empty($loginPage->send_registration_email) && $loginPage->send_registration_email == '1') {

						$userEmailBodyTemplate = $loginPage->registration_email_body;
						$userEmailSubjectTemplate = $loginPage->registration_email_subject;

						AxsUser::sendEmailWithTemplate($user->id, $userEmailSubjectTemplate, $userEmailBodyTemplate, $replacements, false);
					}
				}

                $response->status = 'success';
				echo json_encode($response);
				return;
			}
		}
    }
}