<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die();


class AxsControllerCustomreports extends FOFController {
    
    public function sendSubmission() {
        $message = new stdClass();
        $message->success = false;
        $message->message = "Thank you, your submission was sent";
        $id = $this->input->get('report_id', '', 'INT');
        $report = AxsCustomReports::getCustomReport($id);
        $data   = new stdClass();
        $params = new stdClass();
        $reportParams = json_decode($report->params);
        $fields       = json_decode($reportParams->fields);
        if($reportParams->success_message) {
            $message->message = $reportParams->success_message;
        }
        foreach($fields as $field) {
            
            $params->{$field->parameter} = $this->input->get($field->parameter, '', 'STRING');
        }

        $params->response = $this->input->get('action', '', 'STRING');
        $data->report_id    = $id;
        $data->params       = json_encode($params);
        $data->date         = date('Y-m-d H:i:s');
                
        $db  = JFactory::getDbo();
        $result = $db->insertObject('axs_reports_custom_submissions', $data); 

        if($result) {
            $message->success = true;            
        }
        echo json_encode($message);
    }   
}