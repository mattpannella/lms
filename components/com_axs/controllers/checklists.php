<?php

defined('_JEXEC') or die();

class AxsControllerChecklists extends FOFController {

	public function checkItem() {
		$response = new stdClass();
		$activity = new stdClass();
		$data = self::getItem(JRequest::getVar('item'));
		if($data->completion_type == 'user' || $data->is_admin) {
			$complete = (bool)JRequest::getVar('complete','','INT');		
			$activity->checklist_id = $data->checklist_id;
			$activity->user_id = $data->user_id;
			$activity->item_type = $data->type;
			$activity->item_id = $data->id;
			$activity->completed = 1;
			$activity->date = date("Y-m-d H:i:s");
			$activity->params = json_encode($data);

			if($complete) {
				AxsChecklist::removeChecklistActivity($activity,$activity->user_id);			
			} else {			
				AxsChecklist::addChecklistActivity($activity,$activity->user_id);
			}
			
			$response->checklist_id = $data->checklist_id;
			$response->item_id = $data->id;
			$response->success = true;
			$response->progress = AxsChecklist::getUserChecklistProgress($data->user_id,$data->checklist_id);
			if ($response->progress == 100) {
				AxsNotifications::sendChecklistCompletedAdminEmail($data->user_id, AxsChecklist::getChecklist($data->checklist_id));
			}
		} else {
			$response->success = false;
			$response->error   = "Permission Denied";
		}

		echo json_encode($response);
	}

	private function getItem($data) {
        $dataEncrypted = base64_decode($data);
        $key = AxsKeys::getKey('lms');
        $item = AxsEncryption::decrypt($dataEncrypted, $key);
        return $item;
    }
}