<?php

defined('_JEXEC') or die();

// error_reporting(E_ALL | E_NOTICE);
// ini_set('display_errors', 1);

class AxsControllerShop extends FOFController {

    // ---- Public Functions ----

    public function getShopItems() {
        $results = $this->getShopItemsFromDb();
        if (is_array($results)) {
            foreach ($results as $shop_item) {
                $stock = json_decode($shop_item->stock);
                $shop_item->stock_count = count($stock);
                // we don't need these fields client-side
                unset($shop_item->stock);
                unset($shop_item->hidden);
            }
        }
        echo json_encode($results);
    }

    public function checkout() {
        $input  = JFactory::getApplication()->input;
        $cart = json_decode($input->get('cart', '', 'STRING'));
        
        // retreieve the ids of those items from the db and make a sku list of the most
        // recently added stock units. remove skus from the items in the db
        $id_list = array();
        $item_count_map = array();
        foreach ($cart as $cart_item) {
            $id_list[] = $cart_item->itemData->id;
            $item_count_map[strval($cart_item->itemData->id)] = $cart_item->inCartCount;
        }

        $shop_items = $this->getShopItemsFromDb($id_list);
        $sku_list = array();
        $cart_total = 0.0;
        foreach ($shop_items as $shop_item) {
            if (is_object($shop_item)) {
                $stock_data = json_decode($shop_item->stock);
                for ($i = 0; $i < $item_count_map[$shop_item->id]; $i++) {
                    $sku_obj = array_pop($stock_data);
                    $sku_list[] = $sku_obj->sku;
                    $cart_total += doubleval($shop_item->price);
                }
                $updated_stock_data = json_encode($stock_data);
                $update_successful = $this->updateShopItemDb($shop_item->id, ['stock' => $updated_stock_data]);
                if (!$update_successful) {
                    echo json_encode(["message" => "There was an issue updating shop items"]);
                    return;
                }
            }
        }

        // we wont bother decoding this shipping info for now
        // in the future we need to validate this shipping info and send it off somewhere
        $shipping_info = $input->get('shippingInfo', '', 'STRING');

        $order_data = new stdClass();
        $order_data->user_id = JFactory::getUser()->id;
        $order_data->item_ids_list = json_encode($id_list);
        $order_data->sku_list = json_encode($sku_list);
        $order_data->order_timestamp = date('Y-m-d h:m:s');
        $order_data->shipping_info = $shipping_info;
        $order_data->total_payment = $cart_total;

        $db = JFactory::getDbo();        
        $db->insertObject('axs_shop_orders', $order_data);

        $result = $db->insertid() != 0 
            ? [ "message" => "order creation successful", "success" => "true"] 
            : [ "message" => "order creation failed"];    
        
        echo json_encode($result);
    }

    // ---- Private Functions ----

    private function getShopItemsFromDb($id_list = null) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from('axs_shop_items')
            ->where('is_hidden = 0')    
            ->order('id ASC');

        if (is_array($id_list)) {
            $query->where('id IN (' . implode(",", $id_list) . ')');
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();

        return $results;
    }

    private function updateShopItemDb($id, $data) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $fields = array();
        foreach ($data as $key => $value) {
            $fields[] = $db->quoteName($key) . ' = ' . $db->quote($value);
        }

        $conditions = array(
            $db->quoteName('id') . ' = ' . $db->quote($id), 
        );

        $query->update($db->quoteName('axs_shop_items'))->set($fields)->where($conditions);
        $db->setQuery($query);
        $result = $db->execute();

        return $result;
    }
}