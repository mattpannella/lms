<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 5/6/16
 * Time: 4:59 PM
 */

class AxsControllerEvents extends FOFController {

  function register() {

    //grab general
    $id = $this->input->get('id', '', 'INT');
    $firstname = $this->input->get('firstname', '', 'STRING');
    $lastname = $this->input->get('lastname', '', 'STRING');
    $phone = $this->input->get('phone', '', 'STRING');
    $email = $this->input->get('email', '', 'STRING');

    //grab card info
    $card = $this->input->get('card', '', 'STRING');
    $expire = str_pad($this->input->get('exp_month', '', 'STRING'), 2, '0', STR_PAD_LEFT) . substr($this->input->get('exp_year', '', 'STRING'), 2, 2);
    $cvv = $this->input->get('cvv', '', 'STRING');

    //grab billing info
    $first = $this->input->get('first', '', 'STRING');
    $last = $this->input->get('last', '', 'STRING');
    $address = $this->input->get('addr', '', 'STRING');
    $city = $this->input->get('city', '', 'STRING');
    $state = $this->input->get('state', '', 'STRING');
    $country = $this->input->get('country', '', 'STRING');
    $zip = $this->input->get('zip', '', 'STRING');

    $cardParams = AxsPayment::getCardParams();

    $cardParams->firstName = $first;
    $cardParams->lastName = $last;
    $cardParams->cardNumber = $card;
    $cardParams->expire = $expire;
    $cardParams->cvv = $cvv;
    $cardParams->address = $address;
    $cardParams->zip = $zip;
    $cardParams->city = $city;
    $cardParams->state = $state;
    $cardParams->country = $country;
    $cardParams->phone = "";
    $cardParams->email = "";

    $newcard = AxsPayment::newCard($cardParams);

    //look up how much the item costs.
    $model = FOFModel::getTmpInstance('events', 'AxsModel');
    $event = $model->getEventById($id);

    $resultHandler = function($ret, $card, $saveCard, $userid, $amount, $desc, $original_amount, $item_id, $type, $nextPayDate) use (&$id, &$first, &$last, &$phone, &$email, &$address, &$city, &$state, &$zip, &$country) {

      switch ($ret['result']) {
        case "SUCCESS":
          //if result is success then save in axs_events_registration table.
          $db = JFactory::getDbo();
          $query = $db->getQuery(true);
          $cols = array(
            'event_id',
            'first',
            'last',
            'phone',
            'email',
            'address',
            'city',
            'state',
            'zip',
            'country',
            'date'
          );
          $values = array(
            (int)$id,
            $db->q($first),
            $db->q($last),
            $db->q($phone),
            $db->q($email),
            $db->q($address),
            $db->q($city),
            $db->q($state),
            $db->q($zip),
            $db->q($country),
            $db->q(date("Y-m-d H:i:s"))
          );
          $query->insert($db->qn('axs_events_registration'))
            ->columns($db->quoteName($cols))
            ->values(implode(',', $values));
          $db->setQuery($query);
          $db->execute();
          $event_userid = $db->insertid();

          //we also want to fire the success event so that we can make plugins to handle it.
          //TODO: refactor the email plugin to take stuff from event system.
          //$dispatcher = JEventDispatcher::getInstance();
          //$eventResult = $dispatcher->trigger('onSuccessfulPayment', array($userid, $amount, $desc, $ret['transaction_id']));
          break;
        case "DECLINED":
          break;
        case "FAIL":
          break;
      }

      //let's store this transaction in our DB for later lookup
      $trxn_row_id = AxsPayment::saveTransaction($original_amount, $amount, $card, $desc, $item_id, $type, $ret['transaction_id'], $ret['status'], $nextPayDate, isset($event_userid) ? $event_userid : 0);
      //if the result was anything other than SUCCESS, also store the error code and message.
      if($ret['code'] != 0) {
        AxsPayment::saveTransactionError($trxn_row_id, $ret);
      }

      //display result
      $url = JRoute::_('index.php?option=com_axs&view=event&id='.$id.'&trxn_id='.$trxn_row_id, false);
      JFactory::getApplication()->redirect($url);
    };

    //deprecated AxsPayment::singleTransaction($newcard, 2, $event->id, $event->title, $event->original_amount, $event->discount_amount, null, 0, false, $resultHandler);
  }
}