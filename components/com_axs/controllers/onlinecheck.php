<?php

defined('_JEXEC') or die();

require_once(JPATH_BASE . '/components/com_community/libraries/core.php');

class AxsControllerOnlinecheck extends FOFController {
    function getEveryoneOnline() {    

        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query
            ->select('id')
            ->from($db->quoteName('joom_users'));


        $db->setQuery($query);
        $results = $db->loadObjectList();

        $online = array();
        foreach ($results as $result) {
            //var_dump($result);
            if (CFactory::getUser($result->id)->isOnline()) {
                array_push($online, $result->id);
            }
        }

        echo json_encode($online);    
    }

    function getOnlineFromList() {
        $list = JRequest::getVar('list');

        if (!$list) {
            return null;
        }

        if (!is_array($list)) {
            $list = json_decode($list);
            if (!is_array($list)) {
                return;
            }
        }

        $online = array();
        foreach ($list as $user) {
            if (CFactory::getUser((int)$user)->isOnline()) {
                array_push($online, (int)$user);
            }
        }

        echo json_encode($online);
    }
}