<?php


defined('_JEXEC') or die();
require_once JPATH_LIBRARIES . '/google_translate/vendor/autoload.php';
use Google\Cloud\Translate\V3\TranslationServiceClient;
putenv('GOOGLE_APPLICATION_CREDENTIALS=/var/www/html/libraries/google_translate/river-formula-351323-5bec45ca2e73.json');

class AxsControllerGoogle_translate extends FOFController {

	 function translate() {
         try {
            $projectId = 'river-formula-351323';
            $translationServiceClient = new TranslationServiceClient();
            $formattedParent = $translationServiceClient->locationName($projectId, 'global');
            
            $TRANSLATION_VARS = $this->input->get('TRANSLATION_VARS');
            $contents = array_values($TRANSLATION_VARS);
            $targetLanguage = $this->input->get('target_language');

            try {
                $response = $translationServiceClient->translateText(
                    $contents,
                    $targetLanguage,
                    $formattedParent
                );
                $response_translations = $response->getTranslations();
                $translation_hashes = array_keys($TRANSLATION_VARS);
                $translations = array();
                for ($i = 0; $i < count($response_translations); $i++) {
                    $cur_hash = $translation_hashes[$i];

                    $translations[$cur_hash] = $response_translations[$i]->getTranslatedText();

                    AxsLanguage::storeTranslation(
                        $original = $TRANSLATION_VARS[$cur_hash],
                        $translated = $translations[$cur_hash],
                        $language_tag = $targetLanguage
                    );
                }
                echo json_encode($translations);
            } finally {
                $translationServiceClient->close();
            }
        } catch (Exception $error) {
            echo json_encode(array('error' => $error->getMessage()));
        }
    }
}
