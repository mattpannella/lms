<?php

defined('_JEXEC') or die();

require_once('administrator/components/com_axs/helpers/helper.php');

class AxsControllerSelector extends FOFController {

    public function loadData() {
        $input = JFactory::getApplication()->input;
        
		// $selection can be courses, users, events, media or groups
        $selection     = $input->get('selection', '', 'STRING');

        // The id of the team that this selector is rendering for
        $teamId = $input->get('teamId', '', 'INTEGER');
		$selectedIds = $input->get('selectedIds', '', 'STRING');

		// kendo serverPaging
        $top = $input->get('$top', null);
		$skip = $input->get('$skip', 0);

        // kendo serverSorting
		$serverSort = $input->get('$orderby', null, 'STRING');

        // kendo serverFiltering
		$serverFilter = $input->get('$filter', null, 'STRING');

        $data = self::getList($selection, $teamId, array(), $top, $skip, $serverSort, $serverFilter, $selectedIds);

        echo json_encode($data);
    }

    public function getList($type, $teamId = null, $custom_conditions = array(), $limitRowCount = null, $limitOffset = null, $serverSort = null, $serverFilter = null, $selectedIds = null) {
		$db = JFactory::getDBO();
		if (empty($type)) {
			return false;
		}
		
		// Initialize variables for query building
		$select = '';
		$from = '';
		$join = '';
		$where = array();

		// Initialize conditional array for where clause
		if (!empty($custom_conditions)) {
			foreach ($custom_conditions as $custom_condition) {
				$where[] = $custom_condition;
			}
		}

		$team = AxsTeams::getTeamById($teamId);

		if ($type == 'courses') {
			$select = 'course.splms_course_id AS _select, course.title AS Title, category.title as Category';
			$from = 'joom_splms_courses AS course';
			$join = 'joom_splms_coursescategories AS category ON course.splms_coursescategory_id = category.splms_coursescategory_id';
			$db->setQuery("SELECT JSON_UNQUOTE(JSON_EXTRACT(params, '$.course_ids')) as courseIds FROM axs_teams WHERE id = " . $db->quote($teamId));
			$courseIds = $db->loadObject()->courseIds;
			if (!empty($courseIds)) {
				$where[]= "FIND_IN_SET(course.splms_course_id, (SELECT JSON_UNQUOTE(JSON_EXTRACT(params, '$.course_ids')) FROM axs_teams WHERE id = $teamId))";
			}
			$where[] = 'course.enabled = 1';
		} else if ($type == 'users') {
			$select = 'user.id AS _select, user.name as Name, user.email as Email';
			$from = 'joom_users AS user';
			$where[] = 'block = 0';
			if ($team->member_type == 'groups') {
				$join = 'joom_user_usergroup_map AS map ON map.user_id = user.id';
				$where[]= "FIND_IN_SET(map.group_id, (SELECT user_groups FROM axs_teams WHERE id = $teamId))";
			} else if ($team->member_type == 'individuals') {
				$where []= "FIND_IN_SET(user.id, (SELECT user_ids FROM axs_teams WHERE id = $teamId))";
			}
			
		} else if ($type == 'events') {
			$select = 'event.id AS _select, event.title AS Title, event.event_date as Date_Time, location.name AS Location, JSON_UNQUOTE(JSON_EXTRACT(event.params, "$.time_zone")) AS Timezone, (SELECT SUM(number_registrants) FROM joom_eb_registrants WHERE event_id = event.id) AS Total_Registrants';
			$from = 'joom_eb_events AS event';
			$join = 'joom_eb_locations AS location ON location.id = event.location_id';
			$where[] = 'event.published = 1';
			$where[] = '(event.event_date >= CURDATE() OR event.event_end_date >= CURDATE() )';
			$where[]= 'access IN (' . implode(',', JAccess::getAuthorisedViewLevels(JFactory::getUser()->id)) . ')';
            $where[]= 'registration_access IN (' . implode(',', JAccess::getAuthorisedViewLevels(JFactory::getUser()->id)) . ')';
		} else if ($type == 'groups') {
			$select = 'usergroup.id AS _select, usergroup.title as Title';
			$from = 'joom_usergroups AS usergroup';
			$where[] = "FIND_IN_SET(usergroup.id, (SELECT team.user_groups FROM axs_teams team WHERE team.id = $teamId))";
		}

		if (!empty($serverFilter)) {
			// The server filter needs to be translated from jquery stuff to mysql stuff
			// Multiple columns can be filtered at the same time
			// For a single column the filter will be formatted: startswith(Column_Name, 'query')
			// For a multiple column the filter will be formatted: (startwith(Column_Name1, 'query1') and startswith(Column_Name2), 'query2')
			if (count(explode(' and ', $serverFilter)) > 1) {
				$serverFilter = substr($serverFilter, 1, strlen($serverFilter) - 2);
				$serverFilter = explode(' and ', $serverFilter);
			} else {
				$serverFilter = array($serverFilter);
			}
			
			// For some reason the filter can come back with two different query types, not sure why yet
			// One is substringof(Column_Name, 'query')
			// The second is startswith(tolower('query'), Column_Name)
			// It seems like the second is used more often when there are queries on two columns
			// Yeah, it's insane
			foreach ($serverFilter as &$filter) {
				if (strpos($filter, 'substringof(') !== false) {
					$filter = str_replace('substringof(', '', $filter);
					$filter = substr($filter, 0, strlen($filter) - 1);

					$columnQuery = str_replace("'", '', explode(',', $filter)[0]);
					$columnAlias = str_replace(')', '', explode(',', $filter)[1]);
					$columnAlias = self::translateColumnAlias($type, $columnAlias);
					$where []= $columnAlias . ' LIKE "%' . $columnQuery . '%"';
				}
				if (strpos($filter, 'startswith(tolower(') !== false) {
					$filter = str_replace('startswith(tolower(', '', $filter);
					$filter = substr($filter, 0, strlen($filter) - 1);

					$columnQuery = str_replace("'", '', explode(',', $filter)[1]);
					$columnAlias = str_replace(')', '', explode(',', $filter)[0]);
					$columnAlias = self::translateColumnAlias($type, $columnAlias);
					$where []= $columnAlias . ' LIKE "%' . strtolower($columnQuery) . '%"';
				}
			}
		}

		$query = $db->getQuery(true);
		$query->select($select);
		$query->from($from);
		if (!empty($join)) {
			$query->leftJoin($join);
		}
		$query->where($where);

		// ORDER BY
		$desc = strpos($serverSort, 'desc') !== false;
		if (strpos($serverSort, 'select') !== false) {
			if ($type == 'courses') {
				$serverSort = "splms_course_id";
			} else if ($type == 'users') {
				$serverSort = "user.id";
			} else if ($type == 'events') {
				$serverSort = "event.id";
			} else if ($type == 'groups') {
				$serverSort = "usergroup.id";
			} else {
				$serverSort = "id IN ($selectedIds)";
			}
			if (!empty($selectedIds)) {
				$serverSort .= " IN ($selectedIds)";
			}
			if ($desc) {
				$serverSort .= " desc";
			}
		}
		if (!empty($serverSort)) {
			if (strpos($serverSort, 'desc') !== false) {
				$query->order(str_replace('desc', '', $serverSort) . ' DESC');
			} else {
				$query->order($serverSort . ' ASC');
			}
		} else if ($type == 'events') {
			$query->order('event_date ASC');
		}

		// LIMIT
		if (isset($limitRowCount) && !isset($limitOffset)) {
			$query->setLimit($limitRowCount);
		} else if (isset($limitRowCount) && isset($limitOffset)) {
			$query->setLimit($limitRowCount, $limitOffset);
		}

		if ($type == 'users') {
			$query->group('user.id');
		}

		$db->setQuery($query);
		$data = $db->loadObjectList();
		foreach ($data as $row) {
			$row->select = $row->_select;
		}

		// Get the total count of all rows in the table that match 
		// the conditions we set without the LIMIT
		$query = $db->getQuery(true);
		$query->select($select);
		$query->from($from);
		if (!empty($join)) {
			$query->leftJoin($join);
		}
		$query->where($where);
		if ($type == 'users') {
			$query->group('user.id');
		}
		$db->setQuery('SELECT COUNT(*) AS total FROM (' . $query->__toString() . ') AS data');
		$result = $db->loadObject();
		$total = $result->total;
		
		return array('data' => $data, 'total' => $total);
	}
	
	private static function translateColumnAlias($type, $columnAlias) {
		$translatedAlias = null;
		if ($type == 'courses') {
			if ($columnAlias == 'Title') {
				$translatedAlias = 'course.title';
			}
			if ($columnAlias == 'Category') {
				$translatedAlias = 'category.title';
			}
		} else if ($type == 'users') {
			if ($columnAlias == 'Name') {
				$translatedAlias = 'user.name';
			}
			if ($columnAlias == 'Email') {
				$translatedAlias = 'user.email';
			}
		} else if ($type == 'events') {
			if ($columnAlias == 'Title') {
				$translatedAlias = 'event.title';
			}
			if ($columnAlias == 'Date_Time') {
				$translatedAlias = 'event.event_date';
			}
			if ($columnAlias == 'Location') {
				$translatedAlias = 'location.name';
			}
			if ($columnAlias == 'Timezone') {
				$translatedAlias = '(JSON_EXTRACT(event.params, "$.time_zone"))';
			}
			if ($columnAlias == 'Total_Registrants') {
				$translatedAlias = '(SELECT SUM(number_registrants) FROM joom_eb_registrants WHERE event_id = event.id)';
			}
		}
		return $translatedAlias;
	}

}