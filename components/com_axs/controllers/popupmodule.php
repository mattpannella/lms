<?php

defined('_JEXEC') or die();

class AxsControllerPopupmodule extends FOFController {

	public function loadModule($moduleID = null) {
		if(!$moduleID) {
			$input = JFactory::getApplication()->input;
			$moduleID = (int)$input->get('moduleID');
		}

		$db = JFactory::getDBO();
	    $query = "SELECT * FROM joom_modules WHERE id = $moduleID LIMIT 1";
	    $db->setQuery($query);
	    $result = $db->loadObject();
	    $title = $result->title;
	    $which = $result->module;
	    $module = JModuleHelper::getModule($which, $title);
	    $attribs['style'] = 'xhtml';
	    $loadModule = JModuleHelper::renderModule($module, $attribs);

	    echo $loadModule;
	}

	public function loginAuthenticate() {
		$response = new stdClass();

		$canLoginCheck = AxsSecurity::checkCanLogin();
		if($canLoginCheck->status == 'fail') {
			echo json_encode($canLoginCheck);
			exit;
		}

		$input = JFactory::getApplication()->input;

		$userName = JRequest::getVar('username');
		$password = JRequest::getVar('password');
		$secretkey = JRequest::getVar('secretkey');

		$url 	  = AxsSecurity::cleanInput(JRoute::_(base64_decode(JRequest::getVar('return'))));

		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_users WHERE (username = ".$db->quote($userName)." OR email = ".$db->quote($userName).") LIMIT 1";
		$db->setQuery($query);
		$user = $db->loadObject();


		$response->status  	= "success";
		$response->message 	= "Login Successful";
		$response->url 		= $url;
		$security = AxsSecurity::getSettings();
		if($user->otpKey && $user->otep && !$secretkey && $security->twofactor_enabled) {
			$response->secretKeyVerification = true;
			$response->message = AxsLanguage::text("AXS_SECURITY_KEY_REQUIRED", "Your Security Key is required for authentication");
		}
		if (!$user) {
			$response->status  = "fail";
			$response->message =  JText::_('MOD_LOGIN_ERROR');
		} else {

			$verify = JUserHelper::verifyPassword($password, $user->password, $user->id);

			if (!$verify) {
				$response->status  = "fail";
				$response->message = JText::_('MOD_LOGIN_ERROR');
			}
			if($user->otpKey && $user->otep && $secretkey && $security->twofactor_enabled) {

				require_once JPATH_ADMINISTRATOR . '/components/com_users/models/user.php';
				$model = new UsersModelUser;
				$otpConfig = $model->getOtpConfig($user->id);

				if($otpConfig->method != 'yubikey') {

					$options['otp_config'] = $otpConfig;

					// Load the Joomla! RAD layer
					if (!defined('FOF_INCLUDED')) {
						include_once JPATH_LIBRARIES . '/fof/include.php';
					}

					$credentials['secretkey'] = $secretkey;

					// Try to validate the OTP
					FOFPlatform::getInstance()->importPlugin('twofactorauth');

					$otpAuthReplies = FOFPlatform::getInstance()->runPlugins('onUserTwofactorAuthenticate', array($credentials, $options));

					$check = false;

					if (!empty($otpAuthReplies)) {
						foreach ($otpAuthReplies as $authReply) {
							$check = $check || $authReply;
						}
					}

				} else {
					// Check if the Yubikey starts with the configured Yubikey user string
					$yubikey_valid = $otpConfig->config['yubikey'];
					$yubikey       = substr($secretkey, 0, -32);
					$check = $yubikey === $yubikey_valid;
				}

				if (!$check) {
					$response->status  = "fail";
					$response->message = AxsLanguage::text("AXS_SECURITY_KEY_INVALID", "Your Security Key was invalid");
				}
			}
		}

		if(!$user || !$verify) {
			$data = new stdClass();
			$data->username = $userName;
			if($user->id) {
				$data->user_id = $user->id;
			}
			JPluginHelper::importPlugin('axs');
			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onUserLoginFailure',array((array) $data));
		}

		echo json_encode($response);
	}
}