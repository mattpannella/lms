<?php

defined('_JEXEC') or die();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);


require_once("/var/www/files/creds.php");

class AxsControllerUpdate extends FOFController {

    function session() {        
        $creds = dbCreds::getCreds();

        $options = array();

        $options['driver']   = 'mysqli';
        $options['host']     = $creds->dbhost;
        $options['user']     = $creds->dbuser;
        $options['password'] = $creds->dbpass;
        $options['database'] = $creds->dbname;

        $db = JDatabaseDriver::getInstance($options);

        $data = new stdClass();

        $url = $db->quote($_SERVER['SERVER_NAME']);

        if ($url) {
            $config_query = "SELECT * FROM `axs_dbmanager` WHERE (FIND_IN_SET($url, domains) > 0) AND enabled=1;";
            $db->setQuery($config_query);
            $config = $db->loadObject();

            $params = AxsClients::decryptClientParams($config->dbparams);

            $data->config = $config;
            $data->config->dbuser = $params->dbuser;
            $data->config->dbpass = $params->dbpass;
            $data->config->dbname = $params->dbname;
        }

        $client_id = AxsClients::getClientId();
        if ($client_id) {
            $client_query = "SELECT * FROM `axs_dbmanager` WHERE id = " . $client_id;
            $db->setQuery($client_query);
            $client = $db->loadObject();

            $data->client = $client->params;

            $checklist_query = "SELECT * FROM `axs_admin_checklists` WHERE enabled = 1 AND client_id = " . $client_id;
            $db->setQuery($checklist_query);
            $checklist = $db->loadObject();

            if($checklist) {
                $_SESSION['onboarding_checklist'] = $checklist;
            } else {
                $_SESSION['onboarding_checklist'] = null;
            }
        }

        $cookie_params = AxsClients::getCookieParams();
        $encrypt = AxsEncryption::encrypt($data, $cookie_params->key);

        setcookie($cookie_params->name, $encrypt);
        $_SESSION[$cookie_params->name] = $encrypt;
    }
}