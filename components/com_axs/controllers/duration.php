<?php
defined('_JEXEC') or die();

/**
 * Duration Controller Class
 */
class AxsControllerDuration extends FOFController {

	public function getDurationObject($data,$type = null) {
		$date    		       = date('Y-m-d H:i:s');
		$last_timestamp        = time();
		$durationObj           = new stdClass();
		$durationObj->user_id  = $data->user_id;
		$durationObj->duration = $data->duration;
		if($type == "site") {
			$durationObj->type    = $type;
			$durationObj->item_id = 1;
		} else {
			$durationObj->type    = AxsDuration::getDurationType($data);
			$durationObj->item_id = $data->item_id;
		}
		$durationObj->date    		   = $date;
		$durationObj->last_timestamp   = $last_timestamp;
		return $durationObj;
	}

	/**
     * Store Duration
     *
     * @return void
     *
     */
	public function storeDuration($durationRow,$durationData) {
		$db = JFactory::getDbo();
		if($durationRow) {
			$durationRow->duration = $durationRow->duration + $durationData->duration;
			$durationRow->last_timestamp = $durationData->last_timestamp;
			$db->updateObject('axs_duration',$durationRow,'id');
		} elseif($durationData) {
			$db->insertObject('axs_duration',$durationData);
		}
	}

	/**
     * Track Duration
     *
     * @return void
     *
     */
	public function trackDuration() {
		//return false;
		$user_id = JFactory::getUser()->id;
		if(!$user_id) {
			return false;
		}
		$db = JFactory::getDbo();
		$input = JFactory::getApplication()->input;
		$durationDataEncoded = $input->get('data', '', 'BASE64');
		$duration = $input->get('duration', 0, 'INT');

		if($durationDataEncoded) {
			$durationData = json_decode(base64_decode($durationDataEncoded));
			$durationData->duration = $duration;
			$durationData->user_id = $user_id;
			//get site duration tracking row
			$siteDuration = $this->getDurationObject($durationData,"site");
			$durationSiteRow = AxsDuration::getDurationRow($siteDuration);
			$this->storeDuration($durationSiteRow,$siteDuration);

			if($durationData->view == "lesson") {
				//get lesson duration tracking row
				$lessonDuration = $this->getDurationObject($durationData,$durationData->view);
				$durationLessonRow = AxsDuration::getDurationRow($lessonDuration);
				$this->storeDuration($durationLessonRow,$lessonDuration);
			}

			if($durationData->view == "lesson" || $durationData->view == "course") {
				//get course duration tracking row
				if($durationData->view == "lesson") {
					$durationData->view = "course";
					$durationData->item_id = $durationData->course_id;
				} else {
					$durationData->item_id = $durationData->item_id;
				}
				$courseDuration = $this->getDurationObject($durationData,$durationData->view);
				$durationCourseRow = AxsDuration::getDurationRow($courseDuration);
				$this->storeDuration($durationCourseRow,$courseDuration);
			}
		}
	}
}

