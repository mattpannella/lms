<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

$forbiddenVCIDChars = ";:";
$key = AxsKeys::getKey('lms');
$input = JFactory::getApplication()->input;

// Strip guest_vcid and encryptedParams of any trailing junk
$encryptedParams = $input->get('vcid',0,'STRING');
$encryptedEventParams = $input->get('ev',0,'STRING');
$encryptedEndMeetingCallback = $input->get('end_vcid',0,'STRING');

$guest_vcid = $input->get('guest_vcid','','STRING');
$guest_name = $input->get('guest_name','','STRING');

if(!empty($encryptedParams)) {
	$encryptedParams = rtrim($encryptedParams, $forbiddenVCIDChars);
} elseif(!empty($guest_vcid)) {
	$guest_vcid = rtrim($guest_vcid, $forbiddenVCIDChars);
}

if($encryptedEventParams) {
	$eventParams = AxsEncryption::decrypt(base64_decode($encryptedEventParams), $key);
	$guest_name = $eventParams->userName;
	$guest_id   = $eventParams->userId;
}

$VC_Server = AxsClientData::getClientVirtualMeetingServer();
$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
$virtualMeeting = new AxsVirtualClassroom($setupParams);

if($encryptedEndMeetingCallback) {
	$virtualClassParams = AxsEncryption::decrypt(base64_decode($encryptedEndMeetingCallback), $key);
	$virtualMeeting->endMeetingCallback($virtualClassParams,'checkOutAll');
	exit;
}
if($encryptedParams) {
	$virtualClassParams = AxsEncryption::decrypt(base64_decode($encryptedParams), $key);
	$meeting = $virtualMeeting->getMeetingById($virtualClassParams->meetingId);
	$vcParticipantMaximum = $meeting->maxParticipants;
	$globalMaximumParticipants = AxsClientData::getClientMaxVirtualClassroomParticipants();

	$displayedMaximum = $vcParticipantMaximum;

	if( ( ($vcParticipantMaximum > $globalMaximumParticipants) || ($vcParticipantMaximum == -1 && $globalMaximumParticipants >= 1) ) && ($globalMaximumParticipants >= 1)) {
		$displayedMaximum = $globalMaximumParticipants;
	}

	$meetingParams= new stdClass();
	$meetingParams->meetingId    = $meeting->meetingId;
	$meetingParams->moderatorPW = $meeting->moderatorPW;

	$maxParticipantsReached = false;

	$meetingInfo = $virtualMeeting->getMeetingInfo($meetingParams);
	$liveParticipants = (int)$meetingInfo->participantCount;

	if($liveParticipants == $displayedMaximum) {
		$maxParticipantsReached = true;
	}
	if($maxParticipantsReached) {
	?>
	<div class="maximum-participants-message">
		<p class="text-center h3">
			Maximum participant limit of <strong><?php echo $displayedMaximum; ?></strong> has been reached.
			Please check back later to see if there are any seats available.
		</p>
	</div>
	<?php
	} else {
		$result = $virtualMeeting->getMeeting($virtualClassParams);
		$params = json_decode($result);
		if($params->url) {
			setcookie(base64_encode($virtualClassParams->meetingId).'_u',$virtualClassParams->userId);
			$virtualMeeting->trackAttendance($virtualClassParams,'checkin');
			if($virtualClassParams->eventId && $virtualClassParams->auto_checkin) {
				AxsEvents::eventCheckIn($virtualClassParams->userId,$virtualClassParams->eventId);
			}
			header("Location: $params->url");
		}
	}
} elseif($guest_vcid && $guest_name) {
	if(!$guest_id) {
		$guest_id = AxsSSO::generateUUID();
	}
	if($eventParams->eventId && $eventParams->auto_checkin) {
		AxsEvents::eventCheckIn($eventParams->userId,$eventParams->eventId);
	}
	$meeting  = $virtualMeeting->getMeetingById($guest_vcid);
	$virtualClassParams= new stdClass();
	$virtualClassParams->meetingId    = $meeting->meetingId;
	$virtualClassParams->userId       = $guest_id;
	$virtualClassParams->guestId      = $guest_id;
	$virtualClassParams->userName     = $guest_name;
	$virtualClassParams->userPassword = $meeting->attendeePW;
	$result = $virtualMeeting->getMeeting($virtualClassParams);
	$params = json_decode($result);
	$vcParticipantMaximum = $meeting->maxParticipants;
	$globalMaximumParticipants = AxsClientData::getClientMaxVirtualClassroomParticipants();

	$displayedMaximum = $vcParticipantMaximum;

	if( ( ($vcParticipantMaximum > $globalMaximumParticipants) || ($vcParticipantMaximum == -1 && $globalMaximumParticipants >= 1) ) && ($globalMaximumParticipants >= 1)) {
		$displayedMaximum = $globalMaximumParticipants;
	}

	$meetingParams= new stdClass();
	$meetingParams->meetingId    = $meeting->meetingId;
	$meetingParams->moderatorPW = $meeting->moderatorPW;

	$maxParticipantsReached = false;

	$meetingInfo = $virtualMeeting->getMeetingInfo($meetingParams);
	$liveParticipants = (int)$meetingInfo->participantCount;

	if($liveParticipants == $displayedMaximum) {
		$maxParticipantsReached = true;
	}
	if($maxParticipantsReached) {
	?>
	<div class="maximum-participants-message">
		<p class="text-center h3">
			Maximum participant limit of <strong><?php echo $displayedMaximum; ?></strong> has been reached.
			Please check back later to see if there are any seats available.
		</p>
	</div>
	<?php
	} else {
		if($params->url) {
			$virtualClassParams->userId = '';
			setcookie(base64_encode($virtualClassParams->meetingId).'_g',$guest_id);
			$virtualMeeting->trackAttendance($virtualClassParams,'checkin');
			$guestUrl = $params->url;

		// We need the Zoom guest URL if this is a Zoom meeting
		/* if($meeting->meetingType == 'zoom') {
			$zoomMeeting = AxsZoomMeetings::getZoomMeetingByUUID($meeting->meetingId);
			if($zoomMeeting->join_url)  {
				$guestUrl = $zoomMeeting->join_url;
			} else {
				$guestUrl = $params->url;
			}
		} */
            header("Location: $guestUrl");
		}
	}
} elseif($guest_vcid && !$guest_name) { ?>
<?php
	// We want to display a message to the user above the name entry box that their meeting has a system-wide maximum set, if one is set
	$meeting = $virtualMeeting->getMeetingById($guest_vcid);

	$vcParticipantMaximum = $meeting->maxParticipants;
	$globalMaximumParticipants = AxsClientData::getClientMaxVirtualClassroomParticipants();

	$displayedMaximum = $vcParticipantMaximum;

	if( ( ($vcParticipantMaximum > $globalMaximumParticipants) || ($vcParticipantMaximum == -1 && $globalMaximumParticipants >= 1) ) && ($globalMaximumParticipants >= 1)) {
		$displayedMaximum = $globalMaximumParticipants;
	}

	$meetingParams= new stdClass();
	$meetingParams->meetingId    = $meeting->meetingId;
	$meetingParams->moderatorPW = $meeting->moderatorPW;

	$maxParticipantsReached = false;

	$meetingInfo = $virtualMeeting->getMeetingInfo($meetingParams);
	$liveParticipants = (int)$meetingInfo->participantCount;

	if($liveParticipants == $displayedMaximum) {
		$maxParticipantsReached = true;
	}
?>
	<link rel="stylesheet" type="text/css" href="/components/com_axs/assets/css/virtual_classrooms.css">
	<div class="header-fullwidth" style="
		background-image: url(https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/dashboard-header.jpg);
		background-size:cover;
		background-position:center;
		height: 300px;
		position: relative;">
		<div class="header-fullwidth-overlay" style="background: rgba(0,0,0,.6); width:100%; height:300px; position:relative;">
			<div class="slide-overlay">
				<h2 style="color:#ffffff;font-size:80px;text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.8); font-family: Anton; ">
					JOIN VIRTUAL MEETING
				</h2>
			</div>
		</div>
	</div>

	<?php if(!$maxParticipantsReached) : ?>
		<form action="" method="POST" style="text-align: center; margin: 50px auto 50px auto; max-width: 400px;">
			<div class="form-group">
				<h2>Enter Your Full Name</h2> <br/>
				<input type="text" id="guest_name" name="guest_name" class="form-control" placeholder="Name..." style="font-size: 18px; padding: 10px;"/><br/>
				<?php if($liveParticipants > 0 && $displayedMaximum >= 1) { ?>
				<div>
					<span>There are <?php echo ($displayedMaximum - $liveParticipants); ?> out of <?php echo $displayedMaximum; ?> seats available.</span>
				</div>
				<?php } ?>
				<button type="submit" class="btn btn-success btn-lg"><i class="fa fa-screen"></i> Join Meeting</button>
			</div>
		</form>
	<?php else : ?>
		<div>
			<div class="maximum-participants-message">
				<p class="text-center h3">
					Maximum participant limit of <strong><?php echo $displayedMaximum; ?></strong> has been reached.
					Please check back later to see if there are any seats available.
				</p>
			</div>
		</div>
	<?php endif; ?>
<?php } ?>