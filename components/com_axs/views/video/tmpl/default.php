<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once 'components/shared/controllers/common.php';

$userId = JFactory::getUser()->id;
$brand = AxsBrands::getBrand();
$model = $this->getModel();

$itemParams = json_decode($this->item->params);
$itemParams->access = $this->item->access;

$availableCategories = array_map('intval', $model->getCategoryData(true));
//check main categories
$mainCategoryAccess = false;
if (empty($this->item->category)) {
	$mainCategoryAccess = true;
} else {
	$mainCategories = array_map('intval', explode(',', $this->item->category));
	foreach ($mainCategories as $mainCategoryID) {
		if (in_array($mainCategoryID, $availableCategories)) {
			$mainCategoryAccess = true;
			break;
		}
	}
}

//check subcategories
$subCategoryAccess = false;
if (empty($this->item->subcategory)) {
	$subCategoryAccess = true;
} else {
	$subCategories = array_map('intval', explode(',', $this->item->subcategory));
	foreach ($subCategories as $subCategoryID) {
		if (in_array($subCategoryID, $availableCategories)) {
			$subCategoryAccess = true;
			break;
		}
	}
}

if($mainCategoryAccess && $subCategoryAccess) {
	$hasAccess = true;
} else {
	$hasAccess = false;
}

if($itemParams->access_purchase_type) {
	// new access check function
	$hasContentAccess = AxsContentAccess::checkContentAccess($userId,$itemParams);
	$visibilityAccess = AxsContentAccess::checkMediaVisibilityAccess($userId,$this->item->id);
	if(!$hasContentAccess || !$visibilityAccess) {
		$hasAccess = false;
	}
} else {
	// legacy check for access
	$hasAccess =  AxsExtra::checkViewLevel($this->item->access, $userId);
}

if (!$this->item->brand) {
	$brandName = $brand->site_title;
} else {
	$brandName = $this->item->brand;
}

if ($this->item->cover_image != '') {
	$cover = '/'.$this->item->cover_image;
} else {
	$cover = '/'.$brand->logos->video_watermark;
	$this->item->cover_image = $brand->logos->video_watermark;
	$itemParamsOBJ= json_decode($this->item->params);
	$itemParamsOBJ->cover_image = $brand->logos->video_watermark;
	$jsonencodedItemParam = json_encode($itemParamsOBJ);
	$this->item->params = $jsonencodedItemParam;
}

if (strpos($this->item->source, "/") !== false) {
	$video = '/'.$this->item->source;
} else {
	$video = $this->item->source;
}

$download = substr($video, 1);

if ($this->item->external_source != '') {
	$video = $this->item->external_source;
	if ($this->item->cover_image == '') {
		$cover = "http://img.youtube.com/vi/".$video."/0.jpg";
	}
	$download = "";
}

if ($this->item->media_type == 'secure_file_locker' && !empty($this->item->secure_file))
{
	$course_video = AxsFiles::renderSecureFile($this->item->secure_file);
} 
else if ($this->item->params) 
{
	$course_video = AxsLMS::getVideo($this->item);
}

?>

<link rel="stylesheet" type="text/css" href="components/com_axs/assets/css/media.css">

<div class="container-fluid">
    <div style="margin-bottom: 15px;" >
    	<span class="media-title-white">
            <?php echo $this->item->title; ?>
            <span class="speaker">
                <?php
                    if ($this->item->speaker != '') {
                        echo '<br />with ' . $this->item->speaker;
                    }
                ?>
            </span>
        </span>
    </div>


    <?php
	    if(!$hasAccess) {
			echo "<h2>" . AxsLanguage::text("AXS_DO_NOT_HAVE_PERMISSION_VIEW_RESOURCE", "You do not have permission to view this resource") . "</h2>";
		} else {

	?>

	<?php
		if($course_video) {
			echo $course_video;
		} else {
			$videoParams = array(
				"videoPlaylists" => array(
					"playlist1" => array(
						"videos" => array(
							"video1" => array(
								"url" => $video ,
								"image" => $cover
							)
						)
					)
				)
			);

			echo getVideo($videoParams);
		}
	?>


	<div id="clr"></div>

</div>

<div id="clr"></div>
<br />

<div class="container-fluid">
	<div class="row description">
		<?php echo $this->item->description; ?>
 	</div>
</div>
<?php } ?>