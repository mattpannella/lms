<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 5/6/16
 * Time: 10:52 AM
 */

// No direct access
  defined('_JEXEC') or die;
  $event = $this->getModel();
  $venue = $event->getVenueById($this->item->venue_id);
  $category = $event->getCategoryById($this->item->category);

  
  preg_match_all("!\{{(\w+)\}}!", $this->item->description,  $partnersArray);
  $partners = $partnersArray[0];
  

  if (count($partners) > 0)
  {
    $partnerIds = array();
    foreach($partners as $partner)
    {
      array_push($partnerIds, trim($partner, '{}'));
    }
   
    $description = preg_replace("!\{{(\w+)\}}!", '%%%', $this->item->description);
    $description = explode('%%%', $description);
    if(isset($_COOKIE['partner']))
    {
      $text = array_search($_COOKIE['partner'],$partnerIds);
    }
    else
    {
      $text = 0;
    }
    
    $description = $description[$text];
  }
  else
  {
    $description = $this->item->description;
  }
 ?>

<style>
  .box {
    background-color: white;
    color: black;
    padding: 20px;
    margin: 15px 0px;
    box-shadow: 0 1px 3px rgba(34,25,25,0.4);
   
  }
  .subheader {
    font-size: 20px;
    margin-top: 15px;
  }
  .subtitle {
    font-size: 20px;
    margin-bottom: 15px;
    font-weight: bold;
  }
  .required {
    color: red;
  }
  .overlay {
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    background-color: darkgray;
    opacity: 0.85;
    z-index: 20000;
  }

  .price {
    font-size: 30px;
    padding: 15px;
    text-align: center;
    background: #333333;
    color: #FFFFFF;
    margin-bottom: 15px;
    border-radius: 4px;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
}
</style>

<div class="container">
  <?php if($trxn_id = $this->input->get('trxn_id', '')) 
  { 
    $trxn = AxsPayment::getTransaction($trxn_id);

    if(isset($_COOKIE['partner']))
    {
      
    $partner = preg_replace("/[^0-9]/", "",$_COOKIE['partner']);
    $data = new stdClass();
    $data->id = '';
    $data->partner_id = $partner;
    $data->user_id = JFactory::getUser()->id;
    $data->trxn_id = $trxn_id;
    $data->type_id = 2;
    $data->date = date('Y-m-d H:i:s');
    JPluginHelper::importPlugin('axs');
    $dispatcher = JDispatcher::getInstance();
    $dispatcher->trigger('saveActivityData', array($data));

    }


    ?>
    <div class="box" style="<?php if($trxn->status == "SUC") { echo 'box-shadow: 0px 0px 15px green; background-color: #4cff4c;'; } else { echo 'box-shadow: 0px 0px 15px red; background-color: #ff4c4c;'; } ?>">
      <h1>Your Payment <?php if($trxn->status != "FAIL") echo "was "; echo AxsPayment::getTransactionStatus($trxn->status); ?></h1>
      <?php if($trxn->status == "SUC") {
        //add facebook's success tracking code on successful purchase;
        echo isset($this->item->success_tracking_code) ? $this->item->success_tracking_code : '';
      ?>
        <table style="height: 153px; width: 544px; margin-top: 20px;">
          <tbody>
          <tr>
            <td><strong>Item:</strong></td>
            <td><?php echo $trxn->item; ?></td>
          </tr>
          <tr>
            <td><strong>Date:</strong></td>
            <td><?php echo AxsExtra::format_date($trxn->date, true); ?></td>
          </tr>
          <tr>
            <td><strong>Amount:</strong></td>
            <td>$<?php echo $trxn->amount; ?></td>
          </tr>
          <tr>
            <td><strong>Transaction Id:</strong></td>
            <td><?php echo $trxn->trxn_id; ?></td>
          </tr>
          </tbody>
        </table>
      <?php } else {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('error_message')
              ->from($db->qn('cctransactions_errors'))
          ->where($db->qn('trxn_row_id').'='.(int)$trxn_id);
        $db->setQuery($query);
        $message = $db->loadObject()->error_message;
        ?>
        <h4 style="margin-top: 20px;">Reason: <?php echo $message; ?></h4>
      <?php } ?>
    </div>
  <?php } ?>
  <div class="box">
    <h1 style="font-size: 35px;"><?php echo $this->item->title; ?></h1>
    <div class="subheader">
      <?php echo !empty($venue->city) ? $venue->city : ''; ?>
      <?php echo !empty($category->title) ? ' | '.$category->title : ''; ?>
    </div>
    <hr>
    <?php
      echo !empty($this->item->start) ? date("l F j, Y", strtotime($this->item->start)) : "";
      echo !empty($this->item->end)  ? " - ".date("l F j, Y", strtotime($this->item->end)) : "";
      echo !empty($this->item->start_time) && !empty($this->item->end_time) ? " @ ".$this->item->start_time." - ".$this->item->end_time : "";
    ?>
    <a  href="#register" class="registershortcut btn btn-primary pull-right"><span class="lizicon lizicon-ticket"></span> BUY YOUR TICKET NOW</a>
  </div>
  <?php if (!empty($this->item->image)) { ?>
    <img class="banner_image" src="<?php echo $this->item->image; ?>" style="width: 100%;"/>
  <?php } ?>
  <div class="box">
    <div class="row">
    <?php echo $description; ?>
    </div>
  </div>
  <div style="margin: 0 -15px;">
    <div class="col-sm-6" style="z-index: 20001;">
      <div class="box" id="register">
        <h1 class="subtitle">Register for the Event - 
        <?php echo $this->item->discount_amount>0 ? '$'.$this->item->discount_amount : '$'.$this->item->original_amount; ?></h1>
        <form class="form-horizontal" method="post" action="index.php?option=com_axs&task=events.register">
          <h4>General</h4>
          <div class="form-group">
            <label class="control-label col-sm-4" for="firstname">First Name:<span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="firstname" name="firstname" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="lastname">Last Name:<span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="lastname" name="lastname" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="phone">Phone:<span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="phone" name="phone" required>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-4" for="email">Email:<span class="required">*</span></label>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="email" name="email" required>
            </div>
          </div>
          <hr>
          <h4>Billing Info</h4>
          <div class="form-group">
            <label class="col-md-4 control-label">Credit Card Number<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="card" class="form-control" required>
            </div>
          </div>
          <div class="form-group" style="display: block;">
            <label class="col-md-4 control-label">Expiration Date<span class="required">*</span></label>
            <div class="col-md-8">
              <select name="exp_month" id="exp_month" class="input-small form-control" style="display: inline-block; width: 165px;">
                <?php
                  for ($i = 1; $i <= 12; $i++) {
                    if ($i < 10) {
                      $i = "0" . $i;
                    }
                    ?>
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?
                  }
                ?>
              </select>
              /  
              <select name="exp_year" id="exp_year" class="input-small form-control" style="display: inline-block; width: 165px;">          
                <?php
                  $cur_year = (int)date("Y");

                  for ($i = $cur_year; $i <= $cur_year + 10; $i++) {
                    ?>
                      <option value="<?php echo $i;?>"><?php echo $i;?></option>
                    <?php
                  }
                ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">CVV Code<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="cvv" class="form-control"  required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">First Name<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="first" class="form-control" value="" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Last Name<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="last" class="form-control" value="" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Address<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="addr" class="form-control" value="" maxlength="30" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">City<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="city" class="form-control" value="" maxlength="30" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">State<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="state" class="form-control" value="" maxlength="30" required>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Country<span class="required">*</span></label>
            <div class="col-md-8">
              <select name="country" class="input-small form-control" required>
                <?php $countries = AxsExtra::getAvailableCountries(); foreach($countries as $c) { ?>
                  <option value="<?php echo $c; ?>" <?php if($c == "United States") echo "selected"; ?>><?php echo $c; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-4 control-label">Zip Code<span class="required">*</span></label>
            <div class="col-md-8">
              <input type="text" name="zip" class="form-control" value="" maxlength="9" required>
            </div>
          </div>
          <button type="submit" class="btn btn-primary pull-right">Register</button>
          <div class="clearfix"></div>
          <input type="hidden" name="id" value="<?php echo $this->item->id; ?>">
          <?php echo JHtml::_('form.token'); ?>
        </form>
      </div>
    </div>

    <div class="col-sm-6">
      <div class="box">
        <h1 class="subtitle">Location</h1>
        <iframe
          width="100%" 
          height="350" 
          seamless="seamless""
          frameborder="0" style="border:0"
          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC_6XKStjrrBJqzidJyrF6fp_g9u1w_iQ0
            &q=<?php echo urlencode($venue->title.','.$venue->address.','.$venue->city.','.$venue->state.','.$venue->country); ?>" allowfullscreen>
        </iframe>
        
        <hr>
        <span style="font-size: 12pt;"><strong><?php echo $venue->title; ?></strong></span>
        <br/>
        <span style="font-size: 10pt;"><?php echo $venue->address; ?> <?php echo $venue->city; ?>, <?php echo $venue->state; ?> <?php echo $venue->country; ?></span>
    
        <br/>
        <?php echo $this->item->hotel_description; ?>
        <br/>
        
        <?php if($this->item->hotel_link != '') { ?>
       <center> <a  href="<?php echo $this->item->hotel_link; ?>" target="_blank" class="btn btn-primary"><span class="lizicon lizicon-calendar"></span> BOOK YOUR HOTEL RESERVATIONS</a></center>
        <?php } ?>
      </div>
    </div>
  </div>

</div>
<script>
  $(function() {
    $('.registershortcut').click(function(ev){
      ev.preventDefault();
      var el = jQuery(jQuery(this).attr('href'));

      $('html,body').animate({
        scrollTop: el.offset().top -40
      }, 700);
      $('body').append('<div class="overlay" onclick="removeOverlay();"></div>');
      el.css("box-shadow", "0px 0px 40px gray");
      return false;
    });
  });

  function removeOverlay() {
    $('.overlay').remove();
    $('#register').css('box-shadow', 'none');
  }
</script>