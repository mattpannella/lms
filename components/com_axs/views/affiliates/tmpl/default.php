<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addScript('components/com_axs/assets/js/affiliates.js');

define('GIN_MONTHLY_DUES', 150);

$user_id = JFactory::getUser()->id;

//will get all children of current user, you can pass a user_id if checking a user that isn't the currently logged in user.
$children = AxsExtra::getUserChildren($user_id);

//gets all affiliate levels
$levels = AxsExtra::getAffiliateLevels();

$p = AxsExtra::getUserRewardsPoints($user_id);

//grab first 20 rewards transactions
$trans = AxsExtra::getUserRewardsTransactions($user_id);

$comms = AxsExtra::getUserCommissions($user_id);

$codes = AxsExtra::getUserAffiliateCodes($user_id);

$brand = AxsBrands::getBrand();
?>
<style>
    td, th {
        vertical-align: middle;
        text-align: center;
    }
    .well {
        line-height: 150%;
        border-radius: 0;
    }
</style>
<div class="container">
    <center><div style="margin: 30px 0; font-size: 30px">GIN Rewards</div></center>
    <div class="col-sm-offset-9 col-sm-3" style="font-size: 20px; margin-bottom: 20px; text-transform: none;">My Current GIN Rewards Points: <strong><?php echo $p ? $p : 0; ?></strong></div>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="pill" href="#explained">GIN Rewards Explained</a></li>
        <li><a data-toggle="pill" href="#my-codes">My Referral Codes</a></li>
        <li><a data-toggle="pill" href="#my-data">People I've Referred</a></li>
        <li><a data-toggle="pill" href="#my-commissions">My Commissions</a></li>
        <li><a data-toggle="pill" href="#rewards">My GIN Rewards Transactions</a></li>
    </ul>
    <div class="tab-content" style="display: block; padding: 30px;">
        <div class="tab-pane active" id="explained">
            <video style="margin: 0 auto; width: 90%; display: block;" controls src="http://assets.globalinformationnetwork.com.s3.amazonaws.com/video/promotional/Share_GIN_Bonus_Program_Video.mp4"></video>
            <h3>GIN Rewards Bonus Program Introduction</h3>
            <h4>Presented by: Blaine Athorn</h4>
            <br><br>
            <table class="table table-bordered table-striped table-responsive">
                <tr>
                    <th colspan="2">Your Personal Primary Active Members</th>
                    <th>Affiliate Sales Commission</th>
                    <th>Team Leader Bonus</th>
                    <th>Team Mentoring Bonus</th>
                </tr>
                <?php foreach($levels as $level) { ?>
                    <tr>
                        <td><?php if($level->image) echo "<img src=\"{$level->image}\" width='50px'>"; ?></td>
                        <td><strong><?php if($level->name) echo $level->name.' - '.$level->members_needed.' Members'; ?></strong></td>
                        <td><?php if($level->sales_commision) echo '$'.number_format(sprintf('%0d', $level->sales_commision)).' each'; ?></td>
                        <td><?php if($level->on_monthly_percentage) echo sprintf('%0d', $level->on_monthly_percentage).'% of monthly dues'; ?></td>
                        <td><?php if($level->on_upgrade_percentage) echo sprintf('%0d', $level->on_upgrade_percentage).'% of upgrades'; ?></td>
                    </tr>
                <?php } ?>
                <tr class="danger">
                    <td colspan="2"><strong>Type of Payment</strong>&nbsp;</td>
                    <td>Cash</td>
                    <td>GIN Rewards Points</td>
                    <td>GIN Rewards Points</td>
                </tr>
                <tr class="warning">
                    <td colspan="2"><strong>Who Qualifies</strong>&nbsp;</td>
                    <td>Affiliates and Members</td>
                    <td>Members Only</td>
                    <td>Members Only</td>
                </tr>
            </table>
            <br><br>
            <table class="table table-bordered table-striped table-responsive">
                <tr>
                    <th rowspan="2" colspan="2">Your Personal Primary Active Members</th>
                    <th rowspan="2">Affiliate Sales Commission</th>
                    <th colspan="2">Team Leader Bonus</th>
                    <th rowspan="2">Team Mentoring Bonus <br> based on $10K</th>
                </tr>
                <tr class="info">
                    <th>Monthly</th>
                    <th>Annually</th>
                </tr>
                <?php foreach($levels as $level) { if($level->members_needed > 0) { ?>
                    <tr>
                        <td><?php if($level->image) echo "<img src=\"{$level->image}\" width='50px'>"; ?></td>
                        <td><strong><?php if($level->name) echo $level->name.' - '.$level->members_needed.' Members'; ?></strong></td>
                        <td><?php if($level->sales_commision) echo '$'.number_format(sprintf('%0d', $level->sales_commision*$level->members_needed)); ?></td>
                        <td><?php if($level->on_monthly_percentage) echo number_format(sprintf('%0d', ($level->on_monthly_percentage/100 * GIN_MONTHLY_DUES * AxsExtra::$rewards_quotient * $level->members_needed) )); ?></td>
                        <td><?php if($level->on_monthly_percentage) echo number_format(sprintf('%0d', ($level->on_monthly_percentage/100 * GIN_MONTHLY_DUES * AxsExtra::$rewards_quotient * $level->members_needed*12) )); ?></td>
                        <td><?php if($level->on_upgrade_percentage) echo number_format(sprintf('%0d', ($level->on_upgrade_percentage/100 * 10000) )); ?></td>
                    </tr>
                    <?php } ?>
                <?php } ?>
                <tr class="danger">
                    <td colspan="2"><strong>Type of Payment</strong>&nbsp;</td>
                    <td>Cash</td>
                    <td colspan="2">GIN Rewards Points</td>
                    <td>GIN Rewards Points</td>
                </tr>
                <tr class="warning">
                    <td colspan="2"><strong>Who Qualifies</strong>&nbsp;</td>
                    <td>Affiliates and Members</td>
                    <td colspan="2">Members Only</td>
                    <td>Members Only</td>
                </tr>
            </table>
            <br><br>
            <div class="well"><p>When you enroll 1 person to join GIN, you will receive a $250.00 Affiliate Sales Commission. Additionally, when you enroll another person into GIN, you will receive another&nbsp;<strong>$250</strong>&nbsp;Affiliate Sales Commission, which is 50% of the initial $495 Enrollment Fee. This would give you a total of&nbsp;<strong>$500</strong>&nbsp;in Affiliate Sales Commission from just two (2) GIN enrollments. </p>
                <p>When you have personally enrolled 3 Members into GIN, who are active and in good standing, you are then at the&nbsp;<strong>Bronze</strong>&nbsp;Pin Level. In addition to the $250 cash Affiliate Sales Commission paid for each Member that you enroll in GIN, you will receive a Team Leader Bonus of&nbsp;<strong>20%</strong>&nbsp;of their monthly Membership Dues, paid in GIN Bonus Dollars. Deposited directly into your GIN Back Office, GIN Bonus Dollars are like cash, allowing you to spend within GIN. You can apply those GIN Bonus Dollars to your monthly Membership Dues. You can accrue them and use GIN Bonus Dollars to go to a GIN Major Event or a GIN Adventure. GIN Bonus Dollars are intended for you to experience more of the GIN lifestyle. When you are a&nbsp;<strong>Bronze</strong>&nbsp;Member and have 3+ Members on your team, then you will receive&nbsp;<strong>20%</strong>&nbsp;of their monthly Membership Dues, or $30 GIN Bonus Dollars per Member you have enrolled. </p>
                <p>When you have personally enrolled 5 Members, who are active and in good standing, you then have achieved the&nbsp;<strong>Silver</strong>&nbsp;Pin Level. The&nbsp;<strong>Silver</strong>&nbsp;Level includes the&nbsp;<strong>$250.00 </strong>cash Affiliate Sales Commission that you receive for each Member you enroll in GIN, the Team Leader Bonus of&nbsp;<strong>20%</strong>&nbsp;of their monthly Membership Dues, paid in GIN Bonus Dollars, and&nbsp;<strong>Silver</strong>&nbsp;Members also will receive a&nbsp;<strong>10%</strong>&nbsp;Mentoring Bonus: when someone on your team upgrades to the next Membership Level, you will receive a&nbsp;<strong>10%</strong>&nbsp;bonus on that Upgrade, paid in GIN Bonus Dollars. Note: in order to receive the Mentoring Bonus, you must be at or above the Membership Level to which the team Member is upgrading. </p>
                <p>The&nbsp;<strong>Gold</strong>&nbsp;Pin Level begins when you have personally enrolled 8 Members into GIN, who are active Members in good standing. The&nbsp;<strong>Gold</strong> Level includes the&nbsp;<strong>$250.00</strong>&nbsp;cash Affiliate Sales Commission that you receive for each Member that you enroll in GIN, the Team Leader Bonus of&nbsp;<strong>20%</strong>&nbsp;of their monthly Membership Dues, paid in GIN Bonus Dollars, and&nbsp;<strong>Gold</strong>&nbsp;Members also will receive a&nbsp;<strong>15%</strong>&nbsp;Mentoring Bonus. When someone on your team upgrades to the next Membership Level, you now will receive a&nbsp;<strong>15%</strong>&nbsp;bonus on that upgrade, paid in GIN Bonus Dollars. Note: in order to receive the Mentoring Bonus, you must be at or above the Membership Level at which the team Member is upgrading. </p>
                <p>The next Share GIN Bonus program level is called&nbsp;<strong>Platinum</strong>. The&nbsp;<strong>Platinum</strong>&nbsp;Pin Level begins when you have personally enrolled 10 Members into GIN, who are an active Member in good standing. At&nbsp;<strong>Platinum</strong>, your cash Affiliate Sales Commission for each Member that you enroll in GIN bumps up to&nbsp;<strong>$350</strong>, the Team Leader Bonus now upgrades to&nbsp;<strong>25%</strong>&nbsp;of your team’s monthly Membership Dues, and the Team Mentoring Bonus is enhanced to&nbsp;<strong>20%</strong>&nbsp;of Upgrade revenues, paid in GIN Bonus Dollars. The&nbsp;<strong>Platinum</strong>&nbsp;Pin Level is the Affiliate Level at which you can really enrich your bonuses and get the most out of your GIN Membership. <br>
                    You began collecting on these bonuses as of June 1, 2015. For those of you who already have Members enrolled in GIN (on or before June 1, 2015), credit will be given toward your qualification for the Share GIN Bonus Program. <br>
                    Don’t forget…for each calendar month that you maintain and qualify at a specific Pin Level, you will be paid out on the residual dues for that month in GIN Bonus Dollars, which can be spent within GIN for Membership Dues, Events, Adventures, and to further your own GIN Career!</p>
                <p>Be a part of the GIN movement today.&nbsp; Share GIN to engage, enrich, and expand our amazing club to change lives, one GIN Member at a time. </p>
            </div>
        </div>
        <div class="tab-pane fade" id="my-codes">
            <table class="table table-bordered table-striped table-responsive">
                <tr><th>Referral Code</th><th>Date Created</th><th>Delete</th></tr>
                <?php if(count($codes)) { ?>
                    <?php foreach($codes as $code) { ?>
                        <tr>
                            <td><?php echo $code->code; ?></td>
                            <td><?php echo AxsExtra::format_date($code->date); ?></td>
                            <td><button class="deleteCode btn btn-danger" data-user="<?php echo $code->user_id; ?>" data-code="<?php echo $code->id; ?>"><span class="lizicon lizicon-bin"></span> Delete</button></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="2">You have no referral codes on file.</td></tr>
                <?php } ?>
                <tr>
                    <td colspan="4"><button class="addCode btn btn-success" data-toggle="modal" data-target="#myModal"><span class="lizicon lizicon-plus"></span> Add New Code</button></td>
                </tr>
            </table>
        </div>
        <div class="tab-pane fade" id="my-data">
            <table class="table table-bordered table-striped table-responsive">
                <tr><th colspan="2">User</th><th>Plan</th><th>Email</th><th>Status</th><th>Signed up on</th></tr>
                <?php if(count($children)) { $activeCount = 0; ?>
                    <?php foreach($children as $child) {
                        $profile = AxsExtra::getUserProfileData($child->user_id);
                        $subs = AxsPayment::getUserSubscriptions($child->user_id);
                            if($subs) {
                                foreach($subs as $s) {
                                    $plan = $s->getPlan();
                                    $brandPlans = $brand->site_details->plans_for_active;
                                    if($s->status == "ACT" && in_array($s->plan_id, $brandPlans)) {
                                        $af = true;
                                        $activeCount++;
                                    } else {
                                        $af = false;
                                    }
                                    ?>
                                    <tr class="<?php if($af) { echo 'success'; } else { echo 'danger'; } ?>">
                                        <td><img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;"></td>
                                        <td><a href="my-profile/<?php echo $profile->alias; ?>"><?php echo $profile->first.' '.$profile->last; ?></a></td>
                                        <td><?php echo $plan->title; ?></td>
                                        <td><a href="mailto:<?php echo $profile->email; ?>"><?php echo $profile->email; ?></a></td>
                                        <td><?php echo $s->getStatusName(); ?></td>
                                        <td><?php echo AxsExtra::format_date($child->registerDate); ?></td>
                                    </tr>
                                <?php } ?>
                      <?php }
                            else { ?>    
                                 <tr class="danger">
                                    <td><img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;"></td>
                                    <td><a href="my-profile/<?php echo $profile->alias; ?>"><?php echo $profile->first.' '.$profile->last; ?></a></td>
                                    <td>Affiliate</td>
                                    <td><a href="mailto:<?php echo $profile->email; ?>"><?php echo $profile->email; ?></a></td>
                                    <td>Inactive</td>
                                    <td><?php echo AxsExtra::format_date($child->registerDate); ?></td>
                                </tr>
                      <?php } ?>
                  <?php } ?>
                    <tr>
                        <td colspan="4"><strong>Total Active Members: <?php echo $activeCount; ?></strong></td>
                        <td colspan="3"><strong>Total Affiliates: <?php echo count($children) - $activeCount; ?></strong></td>
                    </tr>
                <?php } else { ?>
                    <tr><td colspan="7">You have not referred anyone.</td></tr>
                <?php } ?>
            </table>
        </div>
        <div class="tab-pane fade" id="my-commissions">
            <table class="table table-bordered table-striped table-responsive">
                <tr><th></th><th>From</th><th>Date</th><th>Reason</th><th>Amount</th><th>Status</th></tr>
                <?php if(count($comms)) { ?>
                    <?php foreach($comms as $comm) {
                        $profile = ginProfile($comm->referee_id);
                        ?>
                        <tr>
                            <td><img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;"></td>
                            <td><?php echo $profile->firstname.' '.$profile->lastname; ?></td>
                            <td><?php echo AxsExtra::format_date($comm->date); ?></td>
                            <td><?php echo $comm->reason; ?></td>
                            <td><?php echo $comm->amount; ?></td>
                            <td><?php echo $comm->status; ?></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="6">No one has contributed points to your account.</td></tr>
                <?php } ?>
            </table>
        </div>
        <div class="tab-pane fade" id="rewards">
            <table class="table table-bordered table-striped table-responsive">
                <tr><th>From</th><th>Date</th><th>Reason</th><th>Amount</th><th>Status</th></tr>
                <?php if(count($trans)) { ?>
                    <?php foreach($trans as $tran) {
                        if($tran->referee_id != 0) {
                            $profile = ginProfile($tran->referee_id);
                            $name = $profile->firstname.' '.$profile->lastname;
                        } else {
                            $name = 'System';
                        }

                        $amount = 0;
                        if($tran->amount > 0) {
                            $amount = '+'.$tran->amount.' points';
                        } else {
                            $amount = $tran->amount.' points';
                        }
                        ?>
                        <tr class="<?php if($tran->amount > 0) echo 'success'; else echo 'danger'; ?>">
                            <td><?php echo $name; ?></td>
                            <td><?php echo AxsExtra::format_date($tran->date); ?></td>
                            <td><?php echo $tran->reason; ?></td>
                            <td><?php echo $amount; ?></td>
                            <td><?php echo $tran->status; ?></td>
                        </tr>
                    <?php }

                    if(count($trans) == 20) { ?>
                        <tr><td colspan="5"><button id="loadMoreRewards" class="btn btn-default" data-user="<?php echo $user_id; ?>" data-page="1">Load More</button></td></tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="5">No one has contributed points to your account.</td></tr>
                <?php } ?>
            </table>
        </div>
    </div>

    <style>
        .modal {
            z-index: 200001;
        }
        /* takes care of scrolling if the window is small */
        .modal-body{
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }
        .required {
            color: red;
        }
    </style>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add New Code</h4>
                    </div>
                    <div class="modal-body" id="myModalBody"></div>
                    <div class="modal-footer">
                        <button type="button" class="saveCode btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <form id="saveCodeForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $user_id; ?>">
        <h4>Enter Code</h4>
        <div class="code-group form-group">
            <label class="col-md-4 control-label">Code<span class="required">*</span></label>
            <div class="col-md-8">
                <input type="text" name="code" class="form-control" value="" maxlength="16">
                <span class="code-message help-block"></span>
            </div>
        </div>
    </form>
</div>
