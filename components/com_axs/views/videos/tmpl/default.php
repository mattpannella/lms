<?php

defined('_JEXEC') or die;

?>


<!-- <script type="text/javascript" src="components/com_axs/assets/js/videos.js?v=4"></script> -->

<!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script> -->
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600&display=swap" rel="stylesheet">

<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/duotone.css" integrity="sha384-bXXzjCj9fg9FACS6tpRWhBsNqQ7j7swH/U3MKTJrZuRbF3ktmj9g/lie7L3CNSTd" crossorigin="anonymous">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.1/css/fontawesome.css" integrity="sha384-RFOcGND+1HYm6OyEAF5WKkoZnwv/cXQpCX6KduKC1vAKAoohkNYWNPhkx4fsH9Fn" crossorigin="anonymous">
<link href="/templates/axs/bootstrap-utilities/bootstrap-utilities.css" rel="stylesheet">
<link rel="stylesheet" href="/components/com_axs/assets/css/media_v2.css">

<style>

    .ajax-load-more {
		font-size:  25px;
		text-transform: uppercase;
		color: #0067ff;
	}
	.ajax-load-more img {
		width: 25px;
	}

	.ajax-load-more {
		margin-top: 40px;
	}
    .ml-card h4 {
        font-size: 20px !important;;
    }
</style>

<?php
$videos = $this->getModel();

$category_list = $videos->getCategoryData();
$cats = array();
foreach($category_list as $cat) {
    if($cat->parent_id == 0) {
        $cats[$cat->id] = $cat;
        $cat->children = array();
    }
}

foreach($category_list as $cat) {
    if($cat->parent_id > 0) {
        $subcats = explode(',', $cat->parent_id);
        foreach($subcats as $subcat) {
            if($cats[$subcat]) {
                $cats[$subcat]->children[] = $cat;
            }
       }
    }
}


$input = JFactory::getApplication()->input;
$category = $input->get('category',0,'INT');

$subcategory = $input->get('subcategory',0,'INT');
$filterEncoded = $input->get('filter',0,'BASE64');
if($filterEncoded) {
    $filter = json_decode(base64_decode($filterEncoded));
    $category = AxsSecurity::alphaNumericOnly($filter->category);
    $subcategory = AxsSecurity::alphaNumericOnly($filter->subcategory);
    $search_terms = AxsSecurity::alphaNumericOnly($filter->search);
    $media_type = AxsSecurity::alphaNumericOnly($filter->type);
}
$category_combo = $category.':'.$subcategory;

?>
<script>
    var isMobile = Boolean(<?php echo AxsMobileHelper::isClientUsingMobile($mobileDeviceList); ?>);
    var current_language = "<?php echo AxsLanguage::getCurrentLanguage()->get('tag');?>";
</script>

<div class="tov-content-container container-fluid">
    <div name="Page Title" class="w-100 bg-dark ml-title-bar text-light p-3">
        <h1 class="fs-5 m-0 ml-title"><?php echo AxsLanguage::text("AXS_MEDIA_LIBRARY", "Media Library") ?></h1>
    </div>
    <div name="Media Library Filtering" class="d-md-inline-flex w-100 bg-light p-1 p-md-2 shadow">

        <select name="category" class="form-select flex-md-fill mx-md-1 ms-md-0 mb-1 mb-md-0" aria-label="Default select example">
            <option value="0:0"><?php echo AxsLanguage::text("AXS_CATEGORY", "Category") ?></option>
            <option value="0:0"><?php echo AxsLanguage::text("AXS_ALL_CATEGORIES", "All Categories") ?></option>
            <?php foreach ($cats as $c) { ?>
                <option class="parentItem" value="<?php echo $c->id; ?>:0" <?php if($category_combo == $c->id.':0') { echo 'selected'; } ?>>
                    <?php echo $c->title; ?>
                </option>

                <?php foreach($c->children as $child) { ?>
                    <option class="subItem" value="<?php echo $c->id; ?>:<?php echo $child->id; ?>" <?php if($category_combo == $c->id.':'.$child->id) { echo 'selected'; } ?>>
                        &nbsp;&nbsp;&nbsp;<?php echo $child->title; ?>
                    </option>
                <?php } ?>
            <?php } ?>
            </select>
        <select name="media_type" class="form-select flex-md-fill mx-md-1 mb-1 mb-md-0" aria-label="Default select example">
                <option value="0" <?php if($media_type == '0') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_MEDIA_TYPE", "Media Type") ?></option>
                <option value="0" <?php if($media_type == '0') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_ALL_MEDIA_TYPES", "All Media Types") ?></option>
                <option value="audio" <?php if($media_type == 'audio') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_AUDIO", "Audio") ?></option>
                <option value="video" <?php if($media_type == 'video') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_VIDEO", "Video") ?></option>
                <option value="pdf" <?php if($media_type == 'pdf') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_PDF", "PDF") ?></option>
                <option value="link" <?php if($media_type == 'link') { echo 'selected'; } ?>><?php echo AxsLanguage::text("AXS_EXTERNAL_WEBSITE", "External Website") ?></option>
            </select>
        <div class=" d-inline-flex flex-md-fill w-100 mx-md-1 mb-1 mb-md-0">
            <label class="d-flex bg-light text-body my-auto rounded-start p-2 border border-end-0" style="min-height:38px;"><i class="fad fa-search my-auto h-100"></i></label>
            <input value="<?php echo $search_terms; ?>" type="text" name="search_terms" class="form-control flex-md-fill w-100 rounded-end" style="border-top-left-radius: 0; border-bottom-left-radius: 0;">
        </div>
    </div>

    <div name="Media Library Items" class="tov-ml-content d-inline-flex justify-content-center flex-wrap py-1 py-md-2 px-1">
    </div>
    <div id="load-more-button" class="text-center py-3" style="width: 100%; clear: both;">
        <button class="btn btn-primary"><?php echo AxsLanguage::text("AXS_LOAD_MORE", "Load More") ?></button>
    </div>
    <div class="ajax-load-more text-center" style="display:none; width: 100%; clear: both;">
        <p><?php echo AxsLanguage::text("AXS_LOADING", "Loading") ?> <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"> <?php echo AxsLanguage::text("HELIX_COMINGSOON_CONTENT", "Content") ?></p>
    </div>
    <div class="all-items-loaded text-center" style="display:none; width: 100%; clear: both;">
        <p><?php echo AxsLanguage::text("AXS_ALL_ITEMS_LOADED", "All Items Loaded") ?></p>
    </div>
</div>
<script src="/components/com_axs/assets/js/media_v2.js?v=7"></script>
