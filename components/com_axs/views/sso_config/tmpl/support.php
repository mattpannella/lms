<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

$app   = JFactory::getApplication();
$input = $app->input;
$id    = $input->get('idp');

if($id == 204568) {
    $sso_config = AxsSSO::getTovutiSupportConfiguration($id);
} else {
    $sso_config = AxsSSO::getSAMLConfiguration($id);
}
$input = JFactory::getApplication()->input;
if(isset($_GET['forward'])) {
    $forward = $input->get('forward',null,'STRING');
} elseif (isset($_GET['Forward'])){
    $forward = $input->get('Forward',null,'STRING');
} elseif (isset($_GET['FORWARD'])){
    $forward = $input->get('FORWARD',null,'STRING');
}

if($forward) {
    setcookie('sso_forward',$forward,time() + (86400 * 30),'/');
}
if($sso_config['type'] == 'OAuth2') {

    AxsSSO::sendOauthRequest($sso_config);
} else {
    AxsSSO::sendSamlRequest($sso_config);
}
