<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die;

$app   = JFactory::getApplication();
$input = $app->input;
$id    = $input->get('idp');
if($id == 204568) {
    $sso_config = AxsSSO::getTovutiSupportConfiguration($id);
} else {
    $sso_config = AxsSSO::getSAMLConfiguration($id);
}

if($_COOKIE['sso_forward']) {
    $sso_config['forward'] = urldecode($_COOKIE['sso_forward']);
    setcookie('sso_forward','',time() - 3600,'/');
}
if($sso_config['type'] == 'OAuth2') {
    $sso_config['code'] = $input->get('code',null,'STRING');
    AxsSSO::getOauthResponse($sso_config);
} else {

    AxsSSO::getSamlResponse($sso_config);
}
