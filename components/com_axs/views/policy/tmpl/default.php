<?php

	defined('_JEXEC') or die();

	$type = JRequest::getVar('type');
	$brand = AxsBrands::getBrand();
	$return = false;

	switch($type) {
		case 'privacy':
			echo $brand->legal->privacy_policy;
			break;
		case 'agreement':
			echo $brand->legal->member_agreement;
			break;
		case 'terms':
			echo $brand->legal->terms_of_use;
			break;
		case 'faq':
			$faq = $brand->site_faq;
			
			if ($faq->type == "1") {
				makeFAQ($faq);
			} else {
				echo $faq->site_faq_free;
			}
			
			break;
		default:
			$return = true;			
	}

	if ($return) {
		header('Location: /');
	}

	function makeFAQ($faq) {		
		?>

		<div class="page-header">
			<h2 itemprop="name">
				Frequently Asked Questions
			</h2>
		</div>
		<br>
		<br>
		<?php echo $faq->faq_heading; ?>
		<br>
		<br>

		<?php
		for ($i = 0; $i < count($faq->faq->faq_question); $i++) {

			$loc = "?option=com_axs&view=policy&type=faq";

			?>

			<div id="faq_question_<?php echo $i?>">
				<b>
					<a href="<?php echo $loc ?>#faq_<?php echo $i?>">
						<?php echo $faq->faq->faq_question[$i] ?>
					</a>
				</b>
			</div>
			<br>
			

			<?php
		}

		?>
		<br>
		<br>
		<br>
		<?php

		for ($i = 0; $i < count($faq->faq->faq_question); $i++) {
			?>

			<div id="faq_<?php echo $i?>">
				<b>
					<?php echo $faq->faq->faq_question[$i]; ?>
				</b>
			</div>
			<br>
			<div id="faq_answer_<?php echo $i?>">
				<?php echo $faq->faq->faq_answer[$i]; ?>				
			</div>
			<br>

			<?php
		}
	}