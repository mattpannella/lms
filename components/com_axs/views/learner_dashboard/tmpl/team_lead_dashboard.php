<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;
$teamParams = $teamLeadDashboard->getTeamParams();
?>

<link href="/templates/axs/css/offcanvas.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/js/offcanvas.js?v=2"></script>
<link href="/templates/axs/elements/lms/team_lead_dashboard/css/normalize.css?v=8" type="text/css" rel="stylesheet">
<link href="/templates/axs/elements/lms/team_lead_dashboard/css/components.css?v=9" type="text/css" rel="stylesheet">
<link href="/templates/axs/elements/lms/team_lead_dashboard/css/team-lead-dashboard.css?v=22" type="text/css" rel="stylesheet">

<style>
	<?php
		if($learnerDashboardParams->team_header_background_color) {
			echo ".tdl-header-container { background-color: ".$learnerDashboardParams->team_header_background_color." !important; }";
		}
	?>
	<?php
		if($learnerDashboardParams->team_header_text_color) {
			echo ".tdl-header-container { color: ".$learnerDashboardParams->team_header_text_color." !important; }";
		}
	?>

	<?php
		if($learnerDashboardParams->team_header_background_image) {
			echo ".tdl-header-container {
				background-image: url(". JUri::base() . '/' . $learnerDashboardParams->team_header_background_image.") !important;
				background-size: cover !important;
			}";
		}
	?>

	<?php
		if( ($learnerDashboardParams->show_team_leaderboard || $learnerDashboardParams->show_team_reports) && !$learnerDashboardParams->show_team_members) {
			echo ".tld-content-column.right { width: 100% !important; }";

		}
	?>

	<?php
		if( (!$learnerDashboardParams->show_team_leaderboard && !$learnerDashboardParams->show_team_reports) && $learnerDashboardParams->show_team_members) {
			echo ".tld-content-column.left { width: 100% !important; }";

		}
	?>
</style>

<div class="tld-container">
    <div class="tld-wrapper">
		<div class="tdl-header-container">
			<div class="tld-header-wrapper">
			<h1 class="tld-heading"><?php echo $header_background_text; ?></h1>
			</div>
		</div>
		<?php echo $teamLeadDashboard->buildTeamDropdownHTML($leadersTeams,$defaultTeamId); ?>

		<div class="tld-content-container">
			<div class="tld-content-wrapper">

				<?php if($learnerDashboardParams->show_team_members) { ?>
				<div class="tld-content-column left tld-members-container">
					<?php echo $teamLeadDashboard->buildTeamMembersHTML(); ?>
				</div>
				<?php } ?>
				<?php if($teamParams->allow_assigning_courses || $teamParams->allow_assigning_events) { ?>
				<div class="tld-content-column left tld-assignment-container" style="display: none;">
					<?php echo $teamLeadDashboard->buildAssignmentManagerHTML(); ?>
				</div>
				<?php } ?>
				<div class="tld-content-column right">
					<?php
						if($learnerDashboardParams->show_team_leaderboard) {
							$teamLeadDashboard->buildTeamLeaderboardHTML();
						}
					?>
					<?php
						if($learnerDashboardParams->show_team_reports) {
							$teamLeadDashboard->buildTeamReportsHTML();
						}
					?>
				</div>
			</div>
		</div>
    </div>
</div>
<script src="/templates/axs/elements/lms/team_lead_dashboard/js/team-lead-dashboard.js?v=9" type="text/javascript"></script>
