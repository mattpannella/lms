<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;
$userId = JFactory::getUser()->id;

if($input->get('action')) {
	$teamEncrypted = $input->get('action','','BASE64');
	$key = AxsKeys::getKey('lms');
    $userManagerParams = AxsEncryption::decrypt(base64_decode($teamEncrypted), $key);

	if($userManagerParams->team_id) {
		$team = AxsTeamLeadDashboard::getSelectedTeam($userId = null,$userManagerParams->team_id);
    } else {
        exit;
    }

    if($userManagerParams->learnerDashboard_id) {
        $learnerDashboard  = AxsLearnerDashboard::getDashboard($userManagerParams->learnerDashboard_id);
    } else {
        exit;
    }

    $teamLeadDashboard = new AxsTeamLeadDashboard($userId,$team,$learnerDashboard);
    echo $teamLeadDashboard->buildAssignmentManagerHTML($userManagerParams);
}