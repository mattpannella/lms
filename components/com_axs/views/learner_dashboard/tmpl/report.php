<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;
$input = JFactory::getApplication()->input;
$userId = JFactory::getUser()->id;

if($input->get('report')) {
	$teamEncrypted = $input->get('report','','BASE64');
	$key = AxsKeys::getKey('lms');
    $reportParams = AxsEncryption::decrypt(base64_decode($teamEncrypted), $key);
	if($reportParams->team_id) {
		$team = AxsTeamLeadDashboard::getSelectedTeam($userId = null,$reportParams->team_id);
    } else {
        exit;
    }

    if($reportParams->learnerDashboard_id) {
        $learnerDashboard  = AxsLearnerDashboard::getDashboard($reportParams->learnerDashboard_id);
    } else {
        exit;
    }

    $teamLeadDashboard = new AxsTeamLeadDashboard($userId,$team,$learnerDashboard);

    switch($reportParams->report) {
        case "attendance":
            echo $teamLeadDashboard->buildAttendanceHTML($reportParams);
        break;
        case "activity":
            echo $teamLeadDashboard->buildActivityHTML($reportParams);
        break;
        case "team_learner_summary":
            echo $teamLeadDashboard->buildTeamLearnerSummaryHTML($reportParams);
        break;
    }

}