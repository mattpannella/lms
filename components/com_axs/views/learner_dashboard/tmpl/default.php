<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;


if(!$learnerDashboard_id) {
	$input = JFactory::getApplication()->input;
	$learnerDashboard_id = $input->get('id',1,'INT');

}
$show_learnerDashboard = $input->get('show_ld',0,'INT');
$learnerDashboard = AxsLearnerDashboard::getDashboard($learnerDashboard_id);
$learnerDashboardParams = json_decode($learnerDashboard->params);
$userId = JFactory::getUser()->id;
$learner_user_id = $userId;
$is_admin = false;
$selectedTeam = false;
if($input->get('params')) {
	$learnerParamsEncrypted = $input->get('params');
	$key = AxsKeys::getKey('lms');
	$learnerDashboard = AxsEncryption::decrypt(base64_decode($learnerParamsEncrypted), $key);
	if($learnerDashboard->user_id && $learnerDashboard->is_admin) {
		$learner_user_id = $learnerDashboard->user_id;
		$is_admin = true;
	}
	if($learnerDashboard->is_admin) {
		$is_admin = true;
	}
}

if($input->get('team')) {
	$teamEncrypted = $input->get('team');
	$key = AxsKeys::getKey('lms');
	$teamId = AxsEncryption::decrypt(base64_decode($teamEncrypted), $key);
	if($teamId) {
		$selectedTeam = AxsTeamLeadDashboard::getSelectedTeam($userId,$teamId);
	}
}

if(!$learner_user_id) {
	echo '<h2 style="text-align: center; padding-top: 100px; padding-bottom: 100px;">Your session has ended. Please login again</h2>';
} else {
	$profile          = CFactory::getUser($learner_user_id);
	$settings 	  	  = json_decode(AxsLMS::getSettings()->params);
	$coverImage       = $profile->getCover();
	$firstName        = AxsLearnerDashboard::getFirstName($learner_user_id);
	if($firstName) {
		$name = $firstName;
	} else {
		$name = $profile->name;
	}
	$header_background_text = AxsLanguage::text("AXS_WELCOME_BACK", "Welcome back") . ', ' . $name;
	$header_message_text_size = 50;
	$header_message_text_font = '';

	if($learnerDashboardParams->show_header_message == 'custom' && $learnerDashboardParams->header_message_text) {
		$header_background_text = str_replace('{name}', $name, $learnerDashboardParams->header_message_text);
	}

	if($learnerDashboardParams->header_message_text_size) {
		$header_message_text_size = $learnerDashboardParams->header_message_text_size;
	}

	if($learnerDashboardParams->header_message_text_font) {
		$header_message_text_font = 'font-family: '.$learnerDashboardParams->header_message_text_font.';';
	}

	if($learnerDashboardParams->show_profile_cover == 'default' && $learnerDashboardParams->cover_default) {
		$coverImage = '/'.$learnerDashboardParams->cover_default;
	}

	$leadersTeams = AxsTeamLeadDashboard::getLeadersTeams($userId,$learnerDashboardParams);

	if($learnerDashboardParams->enable_team_lead_dashboard && !$show_learnerDashboard && !$learnerParamsEncrypted && $leadersTeams) {
		$defaultTeam = AxsTeamLeadDashboard::getLeaderDefaultTeam($userId);
		$defaultTeamId = $defaultTeam->team_id;
		if($selectedTeam) {
			$firstTeam = $selectedTeam;
		} else {
			if($defaultTeam) {
				$defaultTeamObject = AxsTeamLeadDashboard::getSelectedTeam($userId,$defaultTeamId);
			}
			if($defaultTeamObject) {
				$firstTeam = $defaultTeamObject;
			} else {
				$firstTeam = $leadersTeams[0];
			}
		}
		$teamLeadDashboard = new AxsTeamLeadDashboard($userId,$firstTeam,$learnerDashboard);
		require "team_lead_dashboard.php";
	} else {
		$learnerDashboardParams->teams = AxsLearnerDashboard::getTeamUserList($learner_user_id);
		?>
		<div style="margin-top:70px"></div>
		<?php
		require "learner_dashboard.php";
	}
}