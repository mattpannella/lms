<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;
?>
<link href="/templates/axs/css/offcanvas.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="/js/offcanvas.js?v=2"></script>
<link href="/components/com_axs/assets/css/learner_dashboard.css?v=13" type="text/css" rel="stylesheet">
<script src="/templates/axs/js/bootstrap-tooltip.js"></script>

<?php if ($learnerDashboardParams->show_profile_cover && $coverImage) { ?>
	<div class="header-fullwidth" style="
	    background-image: url(<?php echo $coverImage; ?>);
	    background-size:cover;
	    background-position:center;
	    height: 300px;
	    position: relative;">
		<div class="header-fullwidth-overlay" style="background: rgba(0,0,0,.6); width:100%; height:300px; position:relative;">
			<?php if ($learnerDashboardParams->show_header_message && $header_background_text) { ?>
				<div class="slide-overlay">
					<h1 style="color:#ffffff;font-size:<?php echo $header_message_text_size; ?>px; <?php echo $header_message_text_font; ?> text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.8);">
						<?php echo $header_background_text; ?>
					</h1>
				</div>
			<?php } ?>
		</div>
	</div>
<?php } else { ?>
	<div class="clearfix" style="height:100px"></div>
<?php } ?>
<div class="clearfix"></div>
<div class="learner-dashboard-content">

	<?php
	// Render the awards bar HTML
	AxsLearnerDashboard::buildUserAwardBarHTML($learner_user_id, $is_admin, $learnerDashboardParams);

	if ($learnerDashboardParams->show_checklist) {
		if ($learnerDashboardParams->show_courses) {
			$class = 'col-lg-6';
		} else {
			$class = 'col-lg-12 no-right-padding';
		}

	?>
		<div class="<?php echo $class; ?> no-left-padding">
			<?php AxsChecklist::buildUserChecklistHTML($learner_user_id, $is_admin, $learnerDashboardParams); ?>
		</div>
	<?php } ?>
	<?php
	if ($learnerDashboardParams->show_courses) {
		if ($learnerDashboardParams->show_checklist) {
			$class = 'col-lg-6';
		} else {
			$class = 'col-lg-12 no-left-padding';
		}
	?>
		<div class="<?php echo $class; ?> no-right-padding">
			<?php
			//add mainUserID so we can filter based on team lead
			$learnerDashboardParams->mainUserID = $userId;
			AxsLearnerDashboard::buildUserCoursesHTML($learner_user_id, $is_admin, $learnerDashboardParams);
			?>
		</div>
	<?php }
	$class = 'col-lg-12 no-right-padding'; ?>

	<?php
	if ($learnerDashboardParams->show_events || !isset($learnerDashboardParams->show_events)) {
		$class = 'col-lg-12 no-right-padding';
	?>

		<div class="<?php echo $class; ?> no-left-padding">
			<?php AxsLearnerDashboard::buildUserEventsHTML($learner_user_id, $is_admin, $learnerDashboardParams); ?>
		</div>
	<?php } ?>
</div>
<script>
	const LOCALIZATION_VARS = {
		CHECKLIST: "<?php echo AxsLanguage::text("AXS_COMPLETED_LISTS", "Completed Lists") ?>",
		COURSES: "<?php echo AxsLanguage::text("AXS_COMPLETED_COURSES", "Completed Courses") ?>",
		EVENTS: "<?php echo AxsLanguage::text("AXS_COMPLETED_EVENTS", "Past Events") ?>",
		SHOW: "<?php echo AxsLanguage::text("JSHOW", "Show") ?>",
		HIDE: "<?php echo AxsLanguage::text("JHIDE", "Hide") ?>"
	}
</script>
<script src="/components/com_axs/assets/js/learner_dashboard.js?v=8"></script>