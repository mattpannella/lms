<?php

defined('_JEXEC') or die;
$app = JFactory::getApplication();
$input = $app->input;
$step = $input->get('step', '', 'STRING');
$message = $input->get('message', '', 'BASE64');

if(!$step) {
	$step = 'request';
}
$userId = JFactory::getUser()->id;
if ($userId != 0) {
	echo "<center><h1>You are already logged in.</h1></center>";
} else {

	//Defaults
	$backgroundImage = '/components/com_axs/views/login_page/images/tovuti-building.jpg';
	$root_primary_color = "#1e2935";
	$root_accent_color = "#e9882e";
	$root_base_color = "#fff";
	$welcome_text_color = "#fff";
	$login_form_text_color = "#fff";

	$logo = "";
	$login_heading = "Login";
	$favicon = "https://tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/icon.png";
	if ($brand->logos->favicon) {
		$favicon = '/' . $brand->logos->favicon;
	}
	if ($loginPage->logo) {
		$logo = $loginPage->logo;
	}
	if ($loginPage->login_form_heading) {
		$login_heading = $loginPage->login_form_heading;
	}
	if ($loginPage->background_image) {
		$backgroundImage = '/' . $loginPage->background_image;
	}
	if ($loginPage->welcome_text_color) {
		$welcome_text_color = $loginPage->welcome_text_color;
	}
	if ($loginPage->login_form_text_color) {
		$login_form_text_color = $loginPage->login_form_text_color;
	}

	$useRootStyles = false;
	if (!$brand->site_details->root_base_color) {
		$brand->site_details->root_base_color = "#fff";
	}
	if (
		$brand->site_details->tovuti_root_styles &&
		$brand->site_details->root_primary_color &&
		$brand->site_details->root_accent_color
	) {
		$useRootStyles = true;
		$root_primary_color = $brand->site_details->root_primary_color;
		$root_accent_color = $brand->site_details->root_accent_color;
		$root_base_color = $brand->site_details->root_base_color;
	}
	list($r, $g, $b) = sscanf($root_primary_color, "#%02x%02x%02x");
	$button_fullwidth = '';
	if ((!$loginPage->include_password_reset && $loginPage->include_registration) || ($loginPage->include_password_reset && !$loginPage->include_registration)) {
		$button_fullwidth = 'button_fullwidth';
	}
?>

	<!DOCTYPE html>
	<html lang="en-GB">

	<head>
		<meta charset="utf-8">
		<title>Welcome to <?php echo $brand->site_title; ?> | You must login to continue</title>
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="/components/com_axs/views/login_page/css/normalize.css" rel="stylesheet" type="text/css">
		<link href="/components/com_axs/views/login_page/css/components.css" rel="stylesheet" type="text/css">
		<link href="/components/com_axs/views/login_page/css/login-page-1.css?v=8" rel="stylesheet" type="text/css">
		<script src="/media/plg_captcha_recaptcha/js/recaptcha.min.js?69223b1a3b3ee3e2d50524fefb27828f" type="text/javascript"></script>
		<script src="https://www.google.com/recaptcha/api.js?onload=JoomlaInitReCaptcha2&render=explicit&hl=en-GB" type="text/javascript"></script>
		<script src="/js/jquery-3.4.1.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			! function(o, c) {
				var n = c.documentElement,
					t = " w-mod-";
				n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
			}(window, document);
		</script>
		<link href="<?php echo $favicon; ?>" rel="shortcut icon" type="image/x-icon">
		<link href="<?php echo $favicon; ?>" rel="apple-touch-icon">
		<style>
			:root {
				/* Tovuti Platform Default Theme */
				--primary-color: <?php echo $root_primary_color; ?>;
				/* Tovuti Blue */
				--accent-color: <?php echo $root_accent_color; ?>;
				/* Tovuti Orange */
				--base-color: <?php echo $root_base_color; ?>;
				/* Background Base */
				--login_form_text_color: <?php echo $login_form_text_color; ?>;
				--welcome_text_color: <?php echo $welcome_text_color; ?>;
			}

			.tov_login-left_content {

				background-image: url(<?php echo $backgroundImage; ?>);
				background-size: cover;

				<?php if ($loginPage->show_transparent_image_overlay || !isset($loginPage->show_transparent_image_overlay)) : ?>background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8)), to(rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8))), url(<?php echo $backgroundImage; ?>);

				background-image: linear-gradient(180deg, rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8), rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8)), url(<?php echo $backgroundImage; ?>);
				<?php endif; ?>
			}
		</style>
	</head>

	<body>
		<div class="tov_login-container">
			<div class="tov_login-left">
				<div class="tov_login-left_content">
					<?php if ($logo) { ?>
						<img src="/<?php echo $logo; ?>" alt="logo" class="tov_login-logo">
					<?php } ?>
					<div class="tov_login-left-bottom-content">
						<h2 class="tov_login-h2"><?php echo $loginPage->welcome_text_heading; ?></h2>
						<p class="tov_login-p"><?php echo $loginPage->welcome_text_body; ?></p>
					</div>
				</div>
			</div>
			<div class="tov_login-right">
				<main class="tov_login-right_content">
					<div class="tov_login-right-top-content">
						<h1 class="tov_login-h1 login_form_content"><?php echo $login_heading; ?></h1>
						<h1 class="tov_login-h1 password_reset_content" style="display:none;">Reset Password</h1>
						<h1 class="tov_login-h1 registration_form_content" style="display:none;">Register</h1>
						<p class="tov_login-p login_form_content"><?php echo $loginPage->login_form_text; ?></p>
					</div>
					<div class="tov_login-form-block w-form">
						<div class="w-form-done"></div>
						<div class="w-form-fail"></div>
						<?php
						if ($loginPage->include_registration) {
							echo AxsLoginPages::getRegistrationForm($loginPage, $loginPageId);
						}
						?>
						<?php
						if ($loginPage->include_login || !isset($loginPage->include_login)) {
							echo AxsLoginPages::getLoginForm($loginPage);
						}
						?>
						<?php
						if ($loginPage->include_password_reset) {
							echo AxsLoginPages::getPasswordResetForm($loginPage, $step);
						}
						?>
						<div class="tov_login-horizontal-wrapper" style="margin-top: 15px;">
							<?php
							if ($loginPage->include_registration) {
								echo '<a data-wait="Please wait..." id="register_button" aria-label="Create an Account" class="tov_login-button tov_login-button-reset w-button ' . $button_fullwidth . '">Create an Account</a>';
							}
							?>
							<?php
							if ($loginPage->include_login || !isset($loginPage->include_login)) {
								echo '<a data-wait="Please wait..." id="login_return_button" aria-label="Return to Login" class="tov_login-button tov_login-button-reset w-button ' . $button_fullwidth . '" style="display: none;">Return to Login</a>';
							}
							?>
							<?php
							if ($loginPage->include_password_reset) {
								echo '<a data-wait="Please wait..." id="password_reset_button" aria-label="Reset Password" class="tov_login-button tov_login-button-reset w-button ' . $button_fullwidth . '">Reset Password</a>';
							}
							?>
						</div>
						<div class="tov_login-vertical-wrapper sso login_form_content">
							<div class="tov_login-divider"></div>
							<?php if ($loginPage->sso_options) {
								echo AxsLoginPages::getSsoOptions($loginPage);
							} ?>
						</div>
					</div>
				</main>
			</div>
		</div>
		<script>
			function getUrlVars() {
				var vars = {};
				var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m, key, value) {
					vars[key] = value;
				});
				return vars;
			}

			function getUrlParam(parameter, defaultvalue) {
				var urlparameter = defaultvalue;
				if (window.location.href.indexOf(parameter) > -1) {
					urlparameter = getUrlVars()[parameter];
				}
				return urlparameter;
			}

			$("#password_reset_button").click(function() {
				$(".registration_form_content").hide();
				$(".login_form_content").hide();
				$("#login_return_button").show();
				$("#register_button").show();
				$("#password_reset_button").hide();
				$(".password_reset_content").fadeIn(300);
			});

			$("#login_return_button").click(function() {
				$(".registration_form_content").hide();
				$(".password_reset_content").hide();
				$("#login_return_button").hide();
				$("#register_button").show();
				$("#password_reset_button").show();
				$(".login_form_content").fadeIn(300);
			});
			$("#register_button").click(function() {
				$(".login_form_content").hide();
				$(".password_reset_content").hide();
				$("#register_button").hide();
				$("#login_return_button").show();
				$("#password_reset_button").show();
				$(".registration_form_content").fadeIn(300);
			});
			<?php
			if (!$loginPage->include_login && $loginPage->include_registration) {
				echo '$("#register_button").trigger("click");';
			}
			?>
			<?php
			if ($loginPage->include_password_reset && ($step == 'confirm' || $step == 'complete' || $step == 'reset')) {
				echo '$("#password_reset_button").trigger("click");';
			}
			?>

			<?php if ($loginPage->include_password_reset && ($message)) { ?>
				$(".w-form-fail").html("<?php echo urldecode(base64_decode($message)); ?>");
				if ($(".w-form-fail:hidden")) {
					$(".w-form-fail").slideToggle();
				}
			<?php } ?>
		</script>
	</body>

	</html>
<?php } ?>