<?php

require_once(JPATH_ADMINISTRATOR . "/components/com_axs/helpers/helper.php");
echo AxsLoader::loadKendo();

$input = JFactory::getApplication()->input;
$selection     = $input->get('selection', '', 'STRING');
$element       = $input->get('el', '', 'STRING');
$selectedItems = $input->get('val', '', 'STRING');
$teamId = $input->get('teamId', '', 'INTEGER');

if(!$selection) {
	echo "No Selection Group";
	die();
}

$selectionTitle = '';

switch($selection) {
	case 'courses':
		$selectionTitle = AxsLanguage::text('COM_SPLMS_AXS_COURSES', 'Courses');
		$column1 = array(
			'title' => AxsLanguage::text('JGLOBAL_TITLE', 'Title'),
			'field' => 'Title'
		);
		$column2 = array(
			'title' => AxsLanguage::text('AXS_CATEGORY', 'Category'),
			'field' => 'Category'
		);
	break;
	case 'users':
		$selectionTitle = AxsLanguage::text('AXS_Users', 'Users');
		$column1 = array(
			'title' => AxsLanguage::text('AXS_NAME', 'Name'),
			'field' => 'Name'
		);
		$column2 = array(
			'title' => AxsLanguage::text('JGLOBAL_EMAIL', 'Email'),
			'field' => 'Email'
		);
	break;
	case 'events':
		$selectionTitle = AxsLanguage::text('AXS_EVENTS', 'Events');
		$column1 = array(
			'title' => AxsLanguage::text('JGLOBAL_TITLE', 'Title'),
			'field' => 'Title'
		);
		$column2 = array(
			'title' => AxsLanguage::text('AXS_DATE_TIME', 'Date_Time'),
			'field' => 'Date_Time'
		);
		$column3 = array(
			'title' => AxsLanguage::text('AXS_TIMEZONE', 'Timezone'),
			'field' => 'Timezone'
		);
		$column4 = array(
			'title' => AxsLanguage::text('AXS_LOCATION', 'Location'),
			'field' => 'Location'
		);
		$column5 = array(
			'title' => AxsLanguage::text('AXS_TOTAL_REGISTRANTS', 'Total_Registrants'),
			'field' => 'Total_Registrants'
		);
	break;
    case 'groups':
		$selectionTitle = AxsLanguage::text('COM_SPLMS_AXS_COURSES', 'Courses');
		$column1 = array(
			'title' => AxsLanguage::text('JGLOBAL_TITLE', 'Title'),
			'field' => 'Title'
		);
    break;
	default:
		$selectionTitle = AxsLanguage::text('COM_SPLMS_AXS_COURSES', 'Courses');
		$column1 = array(
			'title' => AxsLanguage::text('JGLOBAL_TITLE', 'Title'),
			'field' => 'Title'
		);
		$column2 = array(
			'title' => AxsLanguage::text('AXS_CATEGORY', 'Category'),
			'field' => 'Category'
		);
	break;
}

?>
<style>
	.selectorCheckbox {
		width: 20px !important;
		height: 20px !important;
		margin-left: 10px !important;
	}
	.k-link {
	    font-weight: bold;
	}

	/* .k-widget .k-icon {
        margin-top: 5px !important;
    }
    .k-link .k-icon {
        margin-top: 10px !important;
    } */
	.selectBtn {
	    padding: 7px 15px 7px 15px;
	    border: 1px solid #283848;
	    color: #283848;
	    font-size: 14px;
	    text-align: center;
	    margin-top: 6px;
	    border-radius: 50px;
	    text-decoration: none;
	    background: #fff;
	    transition: ease .3s all;
	    text-shadow: none;
	    cursor: pointer;
	    display: block;
	    float: right;
	}

	.selectBtn:hover {
		color: #fff;
		background: #ea8936;
	}

	.selectTitle {
		color: #fff;
		float: left;
		font-size: 20px;
		line-height: 50px;
		text-transform: capitalize;
	}

	.selectorHeader {
		color: #fff;
		background: #283848;
		padding-left: 20px;
		padding-right: 20px;
		height: 50px;
		position: fixed;
		top: 0px;
		left: 0px;
		box-shadow: 0px 1px 2px 0px rgba(0,0,0,0.8);
		width: 100%;
		z-index: 2;
	}

	.selectorTableContainer {
		margin-top: 50px;
		z-index: 1;
	}

	.k-filtercell > span {
		padding-right: 0px !important;
	}

	.k-pager-wrap .k-icon {
		padding-top: 15px;
	}

	/* LMS-5049 */
	.k-widget.k-dropdown.k-header.k-dropdown-operator,
	.k-header-column-menu,
	.k-button.k-button-icon {
		display:none;
	}
</style>
<div class="selectorHeader">
	<div class="selectTitle"><?php echo AxsLanguage::text("JSELECT", "Select"); ?> <?php echo $selectionTitle ?></div>
	<div class="selectBtn"><i class="icon-plus"></i> <?php echo AxsLanguage::text("JSELECT", "Select"); ?></div>
	<input type="hidden" name="selectedIds" />
	<div class="clearfix"></div>
</div>
<div id="selectorTableContainer" class="selectorTableContainer">
	<table id="selectorTable" class="table table-bordered table-striped table-responsive">
	</table>
</div>

<script>
	// Load list of selected item ids into hidden input
	$('[name="selectedIds"]').val(atob('<?php echo $selectedItems; ?>'));

	// Check all the corresponding checkboxes for the selected items
	function checkSelectedIds() {
		$('.selectorId').each(function() {
			var value = $(this).val();
			var selectedIds = $('[name="selectedIds"]').val();
			if (selectedIds.length > 0) {
			selectedIds = selectedIds.split(',');
			} else {
				selectedIds = [];
			}
			if(selectedIds.indexOf(value) > -1) {
				$(this).prop('checked',true);
			}
		});
	}

	$(document).on('change', '.selectorId', function() {
		var selectedIds = $('[name="selectedIds"]').val();
		if (selectedIds.length > 0) {
			selectedIds = selectedIds.split(',');
		} else {
			selectedIds = [];
		}
		// If we're checking something
		if ($(this).prop('checked') == true) {
			// Add it to selectedIds
			selectedIds.push($(this).val());
		} else {
			// Remove it from selectedIds
			selectedIds = selectedIds.filter(el => el != $(this).val());
		}
		$('[name="selectedIds"]').val(selectedIds.join(','));
	});

	$(document).on('change', '.selector_SelectAll', function() {
		var checked = $(this).prop('checked');
		$('.selectorId').prop('checked', checked);

		var selectedIds = $('[name="selectedIds"]').val();
		if (selectedIds.length > 0) {
			selectedIds = selectedIds.split(',');
		} else {
			selectedIds = [];
		}
		$('.selectorId').each(function() {
			// If we're checking something
			if (checked == true && selectedIds.indexOf($(this).val()) == -1) {
				// Add it to selectedIds
				selectedIds.push($(this).val());
			} else if (checked == false && selectedIds.indexOf($(this).val()) != -1) {
				// Remove it from selectedIds
				selectedIds = selectedIds.filter(el => el != $(this).val());
			}
		});
		$('[name="selectedIds"]').val(selectedIds.join(','));
	});

	$('.selectBtn').click(function() {
		var data = {
			ids: $('[name="selectedIds"]').val(),
			element: '<?php echo $element; ?>'
		}
		window.parent.postMessage(data);
	});


	var kendoOptions = {
		dataSource: {
			type: "odata",
			transport: {
				read: {
					contentType: "application/json",
					url: "index.php?option=com_axs&task=selector.loadData&format=raw&selection=<?php echo $selection ?>&teamId=<?php echo $teamId ?>",
					dataType: 'json',
					cache: true,
					data: function() {
						return {
							selectedIds: $('[name="selectedIds"]').val()
						}
					}
				}
			},
			serverPaging: true,
			serverFiltering: true,
			serverSorting: true,
			pageSize: 20,
			total: function(data) {
              	return data.length;
			},
			schema: {
				type: "json",
				total: function (response) {
					return response.total;
				},
				data: function (response) {
					return response.data;
				}
			}
		},
		dataBound: function(e) {
			// Do this stuff when the data is loaded
			checkSelectedIds();
		},
		sortable: 	true,
		resizable: 	true,
		columnMenu: true,
		scrollable: false,
		filterable: {
			mode: "row"
		},
		pageable: {
			refresh: true,
			pageSizes: true,
			buttonCount: 5,
			pageSizes: [5,10,20,50,100],
		},
		columns: [
			{
				field: 	"select",
				title: 	"<?php echo AxsLanguage::text("JSELECT", "Select"); ?>",
				width: "10px",
				template: '<input type="checkbox" name="ids[]" value="#: select #" class="selectorCheckbox selectorId"/>',
			},
            {
            	field: "<?php echo $column1['field']; ?>",
            	title: "<?php echo $column1['title']; ?>",
				width: "200px",
            	filterable: {
            		operators: {
            			string: {
            				contains: "Contains"
            			}
            		}
            	}
            },
			{
				field: 	"<?php echo $column2['field']; ?>",
				title: 	"<?php echo $column2['title']; ?>",
				width: "200px",
				filterable: {
            		operators: {
            			string: {
            				contains: "Contains"
            			}
            		}
            	}
			}
			<?php if (isset($column3)): ?>
				,{
					field: 	"<?php echo $column3['field']; ?>",
					title: 	"<?php echo $column3['title']; ?>",
					width: "200px",
					filterable: {
						operators: {
							string: {
								contains: "Contains"
							}
						}
					}
				}
			<?php endif; ?>
			<?php if (isset($column4)): ?>
				,{
					field: 	"<?php echo $column4['field']; ?>",
					title: 	"<?php echo $column4['title']; ?>",
					width: "200px",
					filterable: {
						operators: {
							string: {
								contains: "Contains"
							}
						}
					}
				}
			<?php endif; ?>
			<?php if (isset($column5)): ?>
				,{
					field: 	"<?php echo $column5['field']; ?>",
					title: 	"<?php echo $column5['title']; ?>",
					width: "20px",
				}
			<?php endif; ?>
        ]
	}

	$("#selectorTable").kendoGrid(kendoOptions);

	$('.k-filtercell').each(function() {
		var field = $(this).data('field');
		if(field == 'select') {
			var checkbox = '<input type="checkbox" name="selectorCheckbox_SelectAll" class="selectorCheckbox selector_SelectAll"/>';
			$(this).html(checkbox);
		}
	});

</script>