<?php

	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	class AxsViewReport {
		public static function display() {

			$board = self::getBoardData();

			$settings = $board->data->settings;

			$model = FOFModel::getTmpInstance("Boards", "AxsModel");
			$access = $model->getBoardAccessData();

			if (!$access->report) {
				JError::raiseWarning(401, "You are not authorised to view this.");
				return;
			}

			self::makeReport($board);

		}

		private function getBoardData() {
			$id = JRequest::getVar('id');
			$user_id = JFactory::getUser()->id;

			if (!$user_id) {
				JError::raiseWarning(401, "You must log in to view this resource.");
				return;
			}

			$db = JFactory::getDBO();

			$query = $db->getQuery(true);
			$query
				->select('*')
				->from($db->quoteName('axs_board_templates'))
				->where($db->quoteName('id') . '=' . $db->quote($id));

			$db->setQuery($query);

			$result = $db->loadObject();
			$result->data = json_decode($result->data);	//Decode the params

			return $result;
		}

		private function makeReport($board) {

			AxsLoader::loadKendo();

			$board_type = $board->board_type;
			switch ($board_type) {
				case "affirmation": 	$database = "gratitude_board"; 		break;
				case "goal": 			$database = "declaration_board"; 	break;
				case "image": 			$database = "inspiration_board"; 	break;
				default:
					return;
			}

			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query
				->select(
						array(
							'post.*',
							'user.name AS username',
							'user.email AS email',
							'cat.text AS category',
						)
					)
				->from($db->quoteName($database, "post"))
				->join("LEFT", $db->quoteName('joom_users', "user") . " ON " . $db->quoteName('post.user_id') . '=' . $db->quoteName('user.id'))
				->join("LEFT", $db->quoteName('axs_pulse_categories', "cat") . " ON " . $db->quoteName('post.category') . '=' . $db->quoteName('cat.id'))
				->where(
						array(
							$db->quoteName('post.board_id') . '=' . $db->quote($board->id),
							$db->quoteName('post.deleted') . "IS NOT NULL OR " . $db->quoteName('post.deleted') . '=""'
						)
					);

			$db->setQuery($query);

			$posts = $db->loadObjectList();

			if ($board->data->settings->access_type == "teams") {

				$show_teams = true;
							
				if ($board->data->settings->teamselect == "specific") {
					$team_list = implode(",", $board->data->settings->teamlist);
				} else {
					$team_list = null;
				}

				$query->clear();

				$query
					->select('title, user_ids')
					->from($db->quoteName('axs_teams'));

				if ($team_list) {
					$query->where("FIND_IN_SET(" . $db->quoteName('id') . ", " . $db->quote($team_list) . ")");
				}

				$db->setQuery($query);
				$teams = $db->loadObjectList();

				foreach ($teams as $team) {
					$team->user_ids = explode(",", $team->user_ids);
				}

			} else {
				$show_teams = false;
			}

			$id = JRequest::getVar('id');
		?>
			<style>
				.report_note_box {
					margin-bottom: 15px;
					/*width: 300px;*/
				}
				.report_note_datetime {
					font-size: 12px;
					width: 75px;
					/*height: 0px;*/
					color: gray;
				}
				.report_note_date {
					margin-bottom: -10px;
				}
				.report_note_time {

				}
				.report_note_text {
					font-size: 15px;
					/*margin-left: 80px;*/
					/*min-width: 100px;*/
				}
			</style>

			<a href='<?php echo JRoute::_("index.php?option=com_axs&view=boards&id=$id");?>'>
				<div class="btn btn-primary" style="margin-bottom: 10px;">
					Back
				</div>
			</a>

			<table id="boardReport" style="display:none;">
				<thead>
					<tr>
						<?php				
							switch ($board_type) {
								case "affirmation":
									?>
										<th>User Name</th>
										<th>User Email</th>
										<th>Category</th>
										<?php if ($show_teams) { ?>
											<th>Team</th>
										<?php } ?>
										<th><?php echo $board->data->my_affirmation->user_affirmation;?></th>
										<th><?php echo $board->data->my_affirmation->reason;?></th>
										<th>Notes</th>
										<th>Times Checked(<?php echo $board->data->my_affirmation->thankful;?>)</th>
										<th>Last Time Checked(<?php echo $board->data->my_affirmation->thankful;?>)</th>
										<th>Date Created</th>
										<th>Public</th>
									<?php
									break;
								case "goal":
									?>
										<th>User Name</th>
										<th>User Email</th>
										<th>Category</th>
										<?php if ($show_teams) { ?>
											<th>Team</th>
										<?php } ?>
										<th>Goal Type</th>
										<th>Progress Type</th>
										<th>Progress</th>
										<th>Accomplished</th>
										<th>Notes</th>
										<th>Date Created</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Public</th>
									<?php
									break;
								case "image":
									?>
										<th>User Name</th>
										<th>User Email</th>
										<th>Category</th>
										<?php if ($show_teams) { ?>
											<th>Team</th>
										<?php } ?>
										<th>Image</th>
										<th>Text</th>
										<th>Notes</th>
										<th>Date Created</th>
									<?php
									break;
							}
						?>
					</tr>
				</thead>
				<tbody>
					<?php
						foreach ($posts as $post) {

							if ($show_teams) {
								$user_team_list = array();
								foreach ($teams as $team) {
									if (in_array($post->user_id, $team->user_ids)) {
										$user_team_list[] = $team->title;
									}
								}

								if (!$user_team_list) {
									//Only show the data if it's in the team.
									continue;
								}
							}

							//Get the notes for the post.
							
							$conditions = array(
								$db->quoteName('post_id') . '=' . $db->quote($post->id),
								$db->quoteName('user_id') . '=' . $db->quote($post->user_id),
								$db->quoteName('board_id') . '=' . $db->quote($post->board_id)
							);
							
							$query
								->clear()
								->select('*')
								->from($db->quoteName('axs_board_user_notes'))
								->where($conditions);

							$db->setQuery($query);
							$notes = $db->loadObjectList();

							?>
						
							<tr>
								<?php
								switch ($board_type) {
									case "affirmation":
										?>
											<td><?php echo $post->username;?></td>
											<td><?php echo $post->email;?></td>
											<td><?php echo $post->category;?></td>
											<?php if ($show_teams) { ?>
												<td><?php
													echo implode(",", $user_team_list);

												?></td>
											<?php } ?>
											<td><?php echo $post->what;?></td>
											<td><?php echo $post->why;?></td>
											<?php self::getNoteColumn($notes); ?>
											<td><?php echo $post->times_thankful;?></td>
											<td><?php echo $post->last_thankful;?></td>
											<td><?php echo $post->date;?></td>
											<td><?php echo $post->public;?></td>
										<?php
										break;

									case "goal":
										?>
											<td><?php echo $post->username;?></td>
											<td><?php echo $post->email;?></td>
											<td><?php echo $post->category;?></td>
											<?php if ($show_teams) { ?>
												<td><?php
													echo implode(",", $user_team_list);

												?></td>
											<?php } ?>
											
											<td><?php echo ucfirst($post->goal_type); ?></td>
											<td><?php 
												switch ($post->progress_type) {
													case "progressive":		echo "Progressive Goal"; 			break;
													case "single":			echo "One Time Goal"; 				break;
													case "accomplishable":	echo "Accomplishable Declaration";	break;
													case "ongoing":			echo "Ongoing Declaration"; 		break;
												}
												
												?></td>
											<td><?php
												$accomplished = "";
												switch ($post->progress_type) {
													case "ongoing":
														echo "---";
														$accomplished = "---";
														break;
													default:
														$query->clear();
														$query
															->select("SUM(`update_amount`) AS sum")
															->from($db->quoteName('declaration_updates'))
															->where($db->quoteName('goal_id') . '=' . $db->quote($post->id));
														$db->setQuery($query);
														$sum = $db->loadObject();
														
														if (!$sum->sum) {
															$sum->sum = 0;
														}

														switch ($post->progress_type) {
															case "progressive":
																$progress = (int)(($sum->sum * 100) / $post->goal_amount);

																if ($progress >= 100) {
																	$progress = 100;
																	$accomplished = "Yes";
																} else {
																	$accomplished = "No";
																}

																echo $progress . "%";
																break;

															case "single":
															case "accomplishable":
																if ($sum->sum >= $post->goal_amount) {
																	echo "100%";
																	$accomplished = "Yes";
																} else {
																	echo "0%";
																	$accomplished = "No";
																}
																break;
														}
														
														
												}
											?></td>
											<td><?php echo $accomplished;?></td>
											<?php self::getNoteColumn($notes);?>
											<td><?php echo $post->date;?></td>
											<td><?php echo $post->start_date;?></td>
											<td><?php echo $post->end_date;?></td>
											<td><?php echo $post->public;?></td>

										<?php
										break;
									case "image":
										?>
											<td><?php echo $post->username;?></td>
											<td><?php echo $post->email;?></td>
											<td><?php echo $post->category;?></td>
											<?php if ($show_teams) { ?>
												<td><?php
													echo implode(",", $user_team_list);

												?></td>
											<?php } ?>
											<td><img width="200px" src="<?php echo AxsImages::getImagesPath('inspiration') . '/' . $post->image;?>"/></td>
											<td><?php echo $post->inspiration;?></td>
											<?php self::getNoteColumn($notes); ?>
											<td><?php echo $post->date;?></td>
										<?php
										break;
								}
							?>
							</tr>
						
						<?php
						}
					?>
				</tbody>
			</table>

			<script>

				var kendoOptions = {
					toolbar: ["pdf", "excel"],
	    			sortable: true,
	    			groupable: true,
	    			columnMenu: true,
		            resizable: true,
		            filterable: {
		                mode: "row",
		                operators: {
	                        string: {
	                            contains: "Contains"
	                        }
	                    }
		            },
	    			height: 800,
	    			pageable: {
	    				buttonCount: 8,
	    				input: true,
	    				pageSize: 20,
	    				pageSizes: true,
	    				refresh: true,
	    				message: {
	    					empty: "There are no entries to display"
	    				}
	    			}
	    		}

	    		$("#boardReport").kendoGrid(kendoOptions);
	    		$("#boardReport").show();
	    		
				
			</script>
		<?php
		
		}

		private static function getNoteColumn($notes) {
			?>

				<td>
					<?php
						foreach ($notes as $note) {

							$date = date("Y-m-d", strtotime($note->date));
							$time = date("h:i:sa", strtotime($note->date));



					?>
							<div class="report_note_box">
								<div class="report_note_datetime">
									<div class="report_note_date"><?php echo $date;?></div>
									<div class="report_note_time"><?php echo $time;?></div>
								</div>
								<div class="report_note_text">
									<?php echo $note->note;?>
								</div>
							</div>
					<?php
						}
					?>
					<br>
				</td>

			<?php
		}
	}

	AxsViewReport::display();

	