<?php

	include_once("/components/com_axs/controllers/boards.php");

	$controller = new AxsControllerBoards();
	
	$id = JRequest::getVar('id');
	$edit = JRequest::getVar('edit');
	$delete = JRequest::getVar('delete');
	$update = JRequest::getVar('update');
	$make_public = JRequest::getVar('make_public');
	$make_private = JRequest::getVar('make_private');
	$params = JRequest::getVar('params');

	$model = FOFModel::getTmpInstance("Boards", "AxsModel");

	$data = $model->getBoardAccessData($id);

	//If there's no data, or if they're not an admin and they're not allowed on the board, return
	if (!$data || (!$data->admin && !$data->board)) {
		JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		return;
	} else if (!$data->board && $data->editing) {
		//They're trying to go to post a new message and they're not a board member (just an admin)
		JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		return;
	}

	$item = $data->item;

	$key = AxsKeys::getKey('boards');

    ?>

    	<script>
    		var encrypted_board_id = "<?php echo base64_encode(AxsEncryption::encrypt($id, $key)); ?>";
    		localStorage.setItem("encrypted_board_id", encrypted_board_id);
    	</script>

    <?php

    //We now have the board and they have access
	
	$board = "";

	switch ($item->board_type) {
		case "affirmation":
			if ($edit) {
				$board = "affirmation_edit";
			} else {
				$board = "affirmation";
			}
			break;
		case "goal":
			if ($edit) {
				$board = "goal_edit";
			} else {
				$board = "goal";
			}
			break;
		case "image":
			if ($edit) {
				$board = "image_edit";
			} else {
				$board = "image";
			}
			break;
	}

	if ($board) {
		include_once("components/com_axs/includes/boards/" . $board . ".php");
	}



