<?php defined('_JEXEC') or die;

defined('_JEXEC') or die;
$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_axs/assets/css/2fa.css');
$user = JFactory::getUser();
$security = AxsSecurity::getSettings();
require_once JPATH_ADMINISTRATOR . '/components/com_users/models/user.php';
$model = new UsersModelUser;
$otpConfig = $model->getOtpConfig($user->id);
$alreadySetup = false;

switch($security->twofactor_type) {
	case 'yubikey':
		require_once(JPATH_BASE . '/components/com_axs/templates/yubikey.php');
	break;
	case 'otp':
		require_once(JPATH_BASE . '/components/com_axs/templates/otp.php');
	break;
	default:
		echo "<h3 style='text-align:center'>Authentication Method Not Supported</h3>";
	break;
}
$security = AxsSecurity::getSettings();
