<?php  
 
    defined( '_JEXEC' ) or die( 'Restricted access' );
    
    //error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	require_once "components/shared/controllers/rating.php";
    require_once "components/shared/controllers/common.php";
    require_once "components/shared/controllers/comments.php";
    require_once "components/shared/controllers/likes.php";
    require_once "components/shared/models/pulse.php";
    require_once "components/shared/controllers/category.php";
    
?>

<link rel="stylesheet" type="text/css" href="components/com_axs/assets/css/inspiration.css">
<link rel="stylesheet" type="text/css" href="components/com_axs/assets/css/my-inspiration.css">
<link rel="stylesheet" type="text/css" href="components/shared/includes/css/blocksit.css?v=2">

<script type="text/javascript" src="components/shared/includes/js/blocksit.js?v=2"></script>
<script type="text/javascript" src="components/com_axs/assets/js/my-inspiration.js"></script>
<script type="text/javascript" src="components/shared/includes/js/image.js"></script>

<?php

	$db  = JFactory::getDBO();

	$board_id = JRequest::getVar('id');

    $model = FOFModel::getTmpInstance("Boards", "AxsModel");

    $access = $model->checkAccess($board_id);
    if (!$access) {
        return;
    }

    $data = $model->getBoardData($board_id);

    $user_id = JFactory::getUser()->id;

    if (!$user_id) {
        header("Location: /");
    }

    if (!$data) {
        return;
    }

    $font = $data->font;
    $board = $data->my_image;    
?>

<?php    
    if ($board->background_color) {
?>
        <style>
            body {
                background-color:   <?php echo $board->background_color; ?>;
            }
        </style>

<?php
    }

    if ($board->text_color) {
?>
        <style>
            .large-white {
                color:      <?php echo $board->text_color; ?>;
            }

            #column_set {
                color:      <?php echo $board->text_color; ?> !important;
            }

            #loading {
                color:      <?php echo $board->text_color; ?> !important;
            }

            .columnChange {
                color:      <?php echo $board->text_color; ?> !important;
            }

            .edit-title {
                color:      <?php echo $board->text_color; ?> !important;
            }

            .declare-tbl {
                color:      <?php echo $board->text_color; ?> !important;
            }
        </style>
<?php
    }
?>

<div class="large-white">
    <?php echo $board->board_name; ?>
</div>

<?php
    $backRoute = JRoute::_("index.php?option=com_axs&view=board&id=$board_id");    
?>

<a href="<?php echo $backRoute; ?>">
    <button class="btn btn-primary" style="margin: 20px 0px 20px 0px;">
        <?php echo $board->back_button; ?>
    </button>
</a>

<script>
    $(document).ready(
        function() {
            setMaxStringLength(<?php echo $board->max_length; ?>);
            setBoardId(<?php echo $board_id; ?>);
        }
    );
</script>

<?php
    if ($update) {
        
        $update = (int)$update; //Strip bad data (ie. SQL injection)        
        $query = "
            SELECT * FROM inspiration_board 
            WHERE id=$update 
            AND user_id=$user_id 
            AND board_id=$board_id 
            AND (deleted IS NULL OR deleted = '')";

        $db->setQuery($query);
        $edit_post = $db->loadObject();
        if (!$edit_post) {
            $update = null;
        }        
    }
?>

<div id="right-links">
    <div id="am-btn" class="plus"
         <?php
            if ($update != '') {
                echo 'style = "display:none;"';
            } 
         ?>>
        <?php echo $board->new_button; ?>
    </div>
</div>

<div id="add-special"
     <?php 
        if (!isset($update)) {
            $new_inspiration = true;
            echo 'style="display:none;"';
        } else {
            $new_inspiration = false;
        }
     ?>>
    
    <?php
        
        if ($update != '') {
            $params = array(
                "update" => 1,
                "inspiration" => $update
            );
        } else {
            $params = array(
                "add" => 1
            );
        }

        $key = AxsKeys::getKey('boards');        
        $params = base64_encode(AxsEncryption::encrypt($params, $key));
    ?>

    <style>
        .warning {
            font-size:          20px;
            color:              rgb(200,200,200);
            border-radius:      10px;
            border:             1px solid red;
            background-color:   rgb(200, 40, 40);
            padding:            5px;
            box-shadow:         5px 5px 2px rgb(0,0,0);
        }
    </style>

    <form id="inspiration_form" method="post" action="<?php echo $params; ?>" enctype="multipart/form-data" onsubmit="return validateForm()">
        
        <div class="edit-title" style="text-align:left;">
            <?php 
                if ($new_inspiration) {
                    echo $board->new_image;
                } else {
                    echo $board->edit_image;
                }
            ?>
        </div>

        <br />

        <table border="0" class="declare-tbl">
            <tr id="inspiration_picture">
                <td align="right" valign="top">
                    <?php
                        if ($edit_post->image != '') {
                            $imagePath = AxsImages::getImagesPath('inspiration') . '/' . $edit_post->image;
                            $imageThumb = '<img id="current_photo" src="' . $imagePath .'"" style="width:90px;"/>';
                            echo $imageThumb;
                        } else {
                            echo '<strong>Upload Image</strong>';
                        }
                    ?>
                </td>
                <td align="left" >
                    <input id="inspiration_file" type="file" name="image" value="<?php echo $edit_post->image; ?>" />
                </td>
            </tr>

            <tr id="image_preview_area" style="display:none;">
                <td align="right" valign="top">
                    Preview
                </td>
                <td align="left">
                    <img id="image_preview" src="" width="256"/>
                </td>
            </tr>

            <tr id="inspiration_picture_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_no_image; ?>
                    </span>
                </td>
            </tr>

            <tr id="inspiration_text">
                <td align="right" valign="top">
                    <strong>
                        <?php echo $board->user_text; ?>
                    </strong> 
                </td>
                <td align="left" >
                    <textarea id="inspiration_text_area" class="text-dark-grey" name="text" cols="50" rows="10"><?php 
                        if ($edit_post->inspiration) {
                            echo $edit_post->inspiration;
                        }
                    ?></textarea> 
                </td>
            </tr>

            <tr id="inspiration_category">
                <td align="right" valign="top">
                    Category: 
                </td>
                <td align="left" >
                 <select id="inspiration_category_select" name="category" style="width:150px;" class="text-dark-grey">
                        <option value="">--select one--</option>
                        <?php 
                            $categories = getCategories($data->categories);

                            foreach ($categories as $cat) {
                                ?>
                                    <option value="<?php echo $cat->id; ?>" <?php if ($edit_post->category == $cat->id) { echo 'selected'; } ?>> <?php echo $cat->text;?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr id="inspiration_category_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_no_category; ?>
                    </span>
                </td>
            </tr>

            <tr>
                <td align="right" valign="top">
                    Public:
                </td>
                <td align="left" >
                    <select id="public_setting" name="public" style="width:80px;" class="text-dark-grey">
                        <option value="yes" <?php if ($edit_post->public == 'yes') { echo 'selected'; } ?>> Yes </option>
                        <option value="no"  <?php if ($edit_post->public == 'no' ) { echo 'selected'; } ?>> No  </option>
                    </select>
                    <br />
                    <br />
                </td>
            </tr>
            
            <tr>
                <td align="right" valign="top"></td>
                <td align="left" >
                    <?php
                        if ($new_inspiration) {
                            $submit_value = $board->new_submit;
                        } else {
                            $submit_value = $board->edit_submit;
                        }
                        
                    ?>
                    <input id="inspiration_submit_button" type="submit" class="btn btn-primary" name="save" value="<?php echo $submit_value; ?>" />
                    &nbsp;&nbsp;&nbsp;
                    <?php
                        $cancelUrl = JRoute::_("index.php?option=com_axs&view=board&id=$board_id&edit=1");
                    ?>
                    <a href="<?php echo $cancelUrl; ?>" class="btn btn-primary" <?php if (!$update) { echo 'style="display:none;"'; } ?>>Cancel</a>
                </td>
            </tr>
            
        </table>
    </form>
</div>

<div id="loading" style="font-size: 40px; color:rgb(128, 128, 128);">
    <img src="images/block_loader.gif" width="200" height="200">
    <?php echo $board->loading; ?>
</div>

<?php 
    include JPATH_BASE . "/components/shared/models/column_set.php"; 
    include(JPATH_BASE . "/components/com_axs/includes/boards/notes.php");
?>
	
<div id="block-container" style="display:none;">
    <?php	
        $query = $db->getQuery(true);
        $conditions = array(
            $db->quoteName('user_id') . '=' . (int)$user_id,
            '(' . $db->quoteName('deleted') . '="" OR ' . $db->quoteName('deleted') . 'IS NULL)',
            $db->quoteName('board_id') . '=' . (int)$board_id
    	);

    	$query
    		->select('*')
    		->from($db->quoteName('inspiration_board'))
    		->where($conditions)
    		->order($db->quoteName('id') . ' DESC');

    	$db->setQuery( $query );     
    	$rows = $db->loadObjectList();

        if ($rows != null) {
            $count = 0;
            foreach ($rows as $row) {

                $inspiration_id = $row->id;
        		$imageUrl = $row->image;
                //$user_id = $row->user_id;
                $inspirationText = $row->inspiration;     
                $category = $row->category;     
                $date = $row->date;
                $public = $row->public;  
                $rotation = $row->rotation;
               	
        		if ($imageUrl != '') {
                    $image = '<img src="' . AxsImages::getImagesPath('inspiration') . '/'. $imageUrl . '"/>';
        		} else {
                    $image ='';
        		}

              	$profile = ginProfile($user_id);
				$photo = $profile->photo;
				$alias = $profile->alias;
				$firstName = $profile->firstname;
				$lastName = $profile->lastname;
    		
                
            ?> 
    	
    	<div class="block-grid comments inspiration_container <?php echo $category;?>" post_id="<?php echo $inspiration_id;?>">

            <?php
    			$rating_id = "inspiration_" . $inspiration_id;

                $hover = array(
                    $brand->board->image->rating_1,
                    $brand->board->image->rating_2,
                    $brand->board->image->rating_3,
                    $brand->board->image->rating_4,
                    $brand->board->image->rating_5
                );

    			echo display_ratings($rating_id, $hover); 

                $url = "index.php?option=com_axs&view=board&id=$board_id&edit=1";
                $editUrl = JRoute::_($url . "&update=$inspiration_id");
                /*
                $deleteUrl = JRoute::_($url . "&delete=$inspiration_id");
                $privateUrl = JRoute::_($url . "&make_private=$inspiration_id");
                $publicUrl = JRoute::_($url . "&make_public=$inspiration_id");
                */

            ?>
            <div class="edit-icon">
                 
                <span  
                    class="lizicon lizicon-circle-down edit-parent" title="Edit">
                </span>
                <ul class="edit-dropdown">
                    <a href="<?php echo $editUrl; ?>">
                        <li>
                            <span class="lizicon lizicon-cog"></span> 
                            Edit
                        </li>
                    </a>
                    <li>
                        <div class="delete_image_button" post_id="<?php echo $inspiration_id;?>">
                            <span class="lizicon lizicon-cross"></span> Delete
                        </div>
                    </li>
                    <li>
                        <?php 
                            if ($public == 'yes') {
                                $publicStyle = "";
                                $privateStyle = "display:none;";
                            } else {
                                $publicStyle = "display:none;";
                                $privateStyle = "";
                            }
                        ?>
                        <div class="inspiration_switch_privacy" current="public" style="<?php echo $publicStyle;?>" post_id="<?php echo $inspiration_id;?>">
                            <span class="lizicon lizicon-eye-blocked"></span> Make Private
                        </div>
                
                        <div class="inspiration_switch_privacy" current="private" style="<?php echo $privateStyle;?>" post_id="<?php echo $inspiration_id;?>">
                            <span class="lizicon lizicon-eye"></span> Make Public
                        </div>                        
                    </li>
                    <li>
                        <div class="edit_button add_new_note" post_id="<?php echo $inspiration_id;?>">
                            <span class="lizicon-files-empty"></span> Notes
                        </div>
                    </li>

                </ul>
            </div>
            <div class="imgholder pic_<?php echo $count?>">
        		<?php 
                    if ($image != '') { 
                        echo $image;
                    } 
                ?>                
    		</div>
            <script> 
                //rotate_pic("pic_<?php //echo $count?>", <?php //echo $rotation; ?> );
            </script>

    		
    		<p class="inspired-text"><?php echo $inspirationText;  ?></p>
    		<div class="meta">by  <a href="my-profile/<?php echo $alias;?>"><?php echo $firstName.' '.$lastName; ?></a></div>
            <?php
                $like_comment_id = "inspiration_" . $inspiration_id;
                like_button($like_comment_id);
                $params = new stdClass();

                $params->title = $board->comments;
                $params->button_title = $board->comments_submit;
                $params->placeholder = $board->comments_placeholder;
                $params->post_id = $like_comment_id;

                display_comments($params);
            ?>            
    	</div>
        
    <?php 
            $count++;
        } 
    } 
    ?>
</div>
