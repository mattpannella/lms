<?php  
    defined( '_JEXEC' ) or die( 'Restricted access' );

    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);

    $db = JFactory::getDBO();

    $board_id = JRequest::getVar('id');
    $user_id = JFactory::getUser()->id;

    if (!$user_id) {
        header("Location: /");
    }

    if (!$board_id) {
        return;
    }        

    $model = FOFModel::getTmpInstance("Boards", "AxsModel");

    $access = $model->checkAccess($board_id);
    if (!$access) {
        return;
    }

    $data = $model->getBoardData($board_id);

    if (!$data) {
        return;
    }

    $font = $data->font;
    $board = $data->my_goal;

    require_once "components/shared/controllers/common.php";	
	require_once "components/shared/controllers/comments.php";
    require_once "components/shared/controllers/category.php";
?>

<script type="text/javascript" src="components/shared/includes/js/easy-pie-chart.js"></script>
<script type="text/javascript" src="components/com_axs/assets/js/my-declarations.js"></script>

<link type="text/css"  rel="stylesheet" href="components/com_axs/assets/css/my-declarations.css"/>

<div class="large-black">
    <?php echo $board->board_name; ?>
</div>

<script>
    $(document).ready(
        function() {
            setMaxStringLength(<?php echo $board->max_length; ?>);
            setBoardId(<?php echo $board_id; ?>);
        }
    );
</script>

<?php
    $url = "index.php?option=com_axs&view=board&id=$board_id";
    $backRoute = JRoute::_($url);
?>

<a href="<?php echo $backRoute;?>">
    <button class="btn btn-primary"  style="margin: 20px 0px 20px 0px;">
        <?php echo $board->back_button; ?>
    </button>
</a>

<?php
    if ($update) {
        $query = "
            SELECT * FROM declaration_board 
            WHERE id=$update 
            AND user_id=$user_id 
            AND board_id=$board_id 
            AND (deleted IS NULL OR deleted = '')";
        $db->setQuery($query);
        $edit_post = $db->loadObject();
        if (!$edit_post) {
            $update = null;
        }
    }
?>

<div id="right-links">
    <div id="am-btn" class="plus" 
         <?php 
            if ($update != '') { 
                echo 'style = "display:none;"'; 
            } 
         ?>>
        <?php echo $board->new_button; ?>
    </div>
</div>

<div id="add-special"  
    <?php 
        if ($update == '') { 
            echo 'style="display:none;"'; 
        } 
    ?>>
    
    <style>
        .title_text {
            margin-left:    20px;
            font-size:      20px;
        }

        .small_text {
            margin-left:    20px; 
            font-size:      10px; 
            color:          rgb(160, 160, 160);
        }

        .warning {
            font-size:          20px;
            color:              red;            
            border-radius:      10px;
            border:             1px solid red;
            background-color:   rgb(255, 240, 240);
            padding:            5px;
            box-shadow:         5px 5px 2px rgb(128,128,128);
        }
    </style>

    <?php 
        if ($board->background_color != "") {
    ?>
            <style>
                body {
                    background-color: <?php echo $board->background_color; ?>;
                }
            </style>
    <?php
        }

        if ($board->text_color != "") {
    ?>
            <style>
                body {
                    color: <?php echo $board->text_color; ?>;
                }

                .large-black {
                    color: <?php echo $board->text_color; ?>;
                }

                .small_text {
                    color: <?php echo $board->text_color; ?>;   
                }
            </style>
    <?php
        }    
    
        if ($update != '') {
            $params = array(
                "update" => 1,
                "goal" => $update
            );

            $goal_action = "Edit ";
        } else {            
            $params = array(
                "add" => 1
            );

            $goal_action = "New ";            
        }

        $key = AxsKeys::getKey('boards');
        $params = base64_encode(AxsEncryption::encrypt($params, $key));
    ?>

    <form id="declaration_form" method="post" action="<?php echo $params; ?>" class="declaration_form" style="display:none;" onsubmit="return validateForm()">
   
        <?php 
            if (!$update) {
        ?>
                <div class="title_text">        
                    <strong><?php echo $board->declaration_name; ?> Type</strong>
                    <br>
                    <select name="goal_type" class="goal_type" style="width:200px; margin: 20px 0px 20px 0px;" form="declaration_form">
                        <?php if ($board->goal_name) { ?>  
                            <option value="goal"><?php echo $board->goal_name; ?></option>
                        <?php } ?>
                        <?php if ($board->declaration_name) { ?>       
                            <option value="declaration"><?php echo $board->declaration_name; ?></option>
                        <?php } ?>
                    </select>
                    <span id="description_text" class="small_text"></span>
                </div>
        <?php
            } else {
        ?>
                <input name="goal_type" type="hidden" value="<?php echo $edit_post->goal_type; ?>" />
                <script>
                    show_edit(<?php echo "'" . $edit_post->goal_type . "'" ?>);
                </script>
        <?php
            }
        ?> 
        
        <strong>
            <div class="title_text"><?php echo $edit_post->goal_action; ?><span id="edit_action"></span> </div>
        </strong>
        <br />

        <table border="0" class="declare-tbl">
            <tr id="declaration_text">
                <td align="right" valign="top">
                    <strong><?php echo $board->new_lead_in; ?></strong> 
                </td>
                <td align="left" >
                    <?php /* Do not move php tags or white space will be placed inside text area. */?>
                    <textarea id="declaration_textarea" name="declaration" cols="50" rows="5"><?php 
                            if ($update) {
                                echo $edit_post->declaration;
                            }
                    ?></textarea>                    
                </td>                
            </tr>
            <tr id="declaration_text_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_empty_goal; ?>
                    </span>
                </td>
            </tr>
            

            <tr id="declaration_accomplishable">
                <td align="right" valign="top">
                    <?php echo $board->declaration_accomplishable_title; ?>
                </td>
                <td align="left" >
                    <input name="progress_type_declaration" type="checkbox" <?php if ($edit_post->progress_type == 'accomplishable') { echo 'checked'; } ?> />
                    <span class="small_text">
                        <?php echo $board->declaration_accomplishable_text; ?><br>
                        ie: <br>
                        <?php echo $board->declaration_example_accomplishable; ?><br>
                        <?php echo $board->declaration_example_not_accomplishable; ?><br>
                    </span>
                </td>                
            </tr>

            <tr id="goal_progressive">
                <td align="right" valign="top">
                    <?php echo $board->goal_measurable_title; ?>
                </td>
                <td align="left" >
                    <input id="progressive_checkbox" name="progress_type_goal" type="checkbox" <?php if ($edit_post->progress_type == 'progressive') { echo 'checked'; } ?> /> 
                    <span class="small_text">
                        <?php echo $board->goal_measurable_text; ?><br>
                        ie: <br>
                        <?php echo $board->goal_example_measurable; ?><br>
                        <?php echo $board->goal_example_not_measurable; ?><br>
                    </span>
                </td>
            </tr>

            <tr id="goal_to_reach">
                <td align="right" valign="top">
                    <?php echo $board->goal_reach; ?> 
                </td>
                <td align="left" >
                    <!--<input id="goal_to_reach_text" name="goal_amount" value="<?php echo $goal_amount; ?>" type="text" style="width:100px;" 
                            onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 37 || event.charCode == 39"/>-->
                    <input id="goal_to_reach_text" name="goal_amount" value="<?php echo $edit_post->goal_amount; ?>" type="text" style="width:100px;">
                </td>                
            </tr>
            <tr id="goal_to_reach_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_empty_goal_number; ?>
                    </span>
                </td>
            </tr>
            <tr id="goal_number_only_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_invalid_number; ?>
                    </span>
                </td>
            </tr>

            <tr id="category_list">
                <td align="right" valign="top">
                    Category: 
                </td>
                <td align="left" >
                    <select id="category_select" name="category" style="width:100px;" >
                        <option value="">--select one--</option>
                        <?php 
                            $categories = getCategories($data->categories);

                            foreach ($categories as $cat) {
                                ?>
                                    <option value="<?php echo $cat->id; ?>" <?php if ($edit_post->category == $cat->id) { echo 'selected'; } ?>> 
                                        <?php echo $cat->text;?>
                                    </option>
                                <?php
                            }
                        ?>
                    </select>
                </td>                
            </tr>
            <tr id="category_list_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_no_category; ?>
                    </span>
                </td>
            </tr>

            <?php
                if ($edit_post->start_date) {
                    $start_date_text = date("m/d/Y", strtotime($edit_post->start_date));
                } else {
                    $start_date_text = '';
                }

                if ($edit_post->end_date) {
                    $end_date_text = date("m/d/Y", strtotime($edit_post->end_date));
                } else {
                    $end_date_text = '';
                }
            ?>
            <tr id="start_date">
                <td align="right" valign="top">
                    <?php echo $board->start_date; ?> 
                </td>
                <td align="left" valign="top">
                    <input  id="start_calendar" name="start_time" type="text" value="<?php echo $start_date_text; ?>" style="width:100px;" />
                </td>                
            </tr>
            <tr id="start_date_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>                        
                        <?php echo $board->warning_invalid_start_date; ?>
                    </span>
                </td>
            </tr>

            <tr id="end_date">
                <td align="right" valign="top">
                    <?php echo $board->end_date; ?>
                </td>
                <td align="left" valign="top">
                    <input  id="end_calendar" name="end_time" type="text" value="<?php echo $end_date_text; ?>" style="width:100px;" />
                </td>
            </tr>
            <tr id="end_date_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_invalid_end_date; ?>
                    </span>
                </td>
            </tr>
            <tr id="end_date_before_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_invalid_date; ?>
                    </span>
                </td>
            </tr>

            <tr id="public_select">
                <td align="right" valign="top">
                    Public:
                </td>
                <td align="left" >
                    <select id="public_setting" name="public" style="width:80px;">
                        <option value="yes" <?php if($edit_post->public == 'yes') { echo 'selected'; }?> >Yes</option>
                        <option value="no" <?php if($edit_post->public == 'no') { echo 'selected'; }?> >No</option>
                    </select>
                    <br />
                    <br />
                </td>
            </tr>

            <tr>
                <td align="right" valign="top"></td>
                <td align="left" >                    
                    <input id="declaration_submit_button" type="submit" class="btn btn-primary" name="save" value="<?php echo $board->submit; ?>" />
                    &nbsp;&nbsp;&nbsp;
                    <?php
                        $cancelUrl = JRoute::_("index.php?option=com_axs&view=board&id=$board_id&edit=1");
                    ?>
                    <a href="<?php echo $cancelUrl; ?>" class="btn btn-primary" <?php if (!$update) { echo 'style="display:none;"'; } ?>>Cancel</a>
                </td>
            </tr>

        </table>
      
    </form>
</div>

<?php include(JPATH_BASE . "/components/com_axs/includes/boards/notes.php"); ?>

<br />
<br />

<?php

$db = getDatabase();
$query = $db->getQuery(true);

$user_id = JFactory::getUser()->id;

$conditions = array(
    $db->quoteName('user_id') . '=' . (int)$user_id,
    '(' . $db->quoteName('deleted') . '="" OR ' . $db->quoteName('deleted') . 'IS NULL)',
    $db->quoteName('board_id') . '=' . (int)$board_id
);

$query
    ->select('*')
    ->from($db->quoteName('declaration_board'))
    ->where($conditions)
    ->order($db->quoteName('id') . ' DESC');

$db->setQuery( $query );     
$rows = $db->loadObjectList();

if ($rows != null) {
    foreach ($rows as $row) {

        $goal_id = $row->id;
        //$user_id = $row->user_id;
        $declaration = $row->declaration; 
        $goal_amount = $row->goal_amount;
        $category = $row->category;        
        $start_date = $row->start_date;
        $end_date = $row->end_date;
        $public = $row->public;        
        $goal_type = $row->goal_type;
        $progress_type = $row->progress_type;

        $query = "SELECT sum(update_amount) AS progress 
                    FROM declaration_updates 
                    WHERE user_id='$user_id' 
                    AND goal_id='$goal_id'";
        
        $db->setQuery( $query );
        $data = $db->loadObjectList();
        
        if ($data != null) {
            foreach ($data as $datum) {

                $day = 60 * 60 * 24;
	  
                $progress = $datum->progress;	  
                
                $datediff = strtotime($end_date) - strtotime($start_date);
                $datediff = floor($datediff / $day);

                $now = time();

                $time_left = strtotime($end_date) - $now;
                $daysLeft = floor($time_left / $day);
            }
        }

        if (!isset($progress)) {
            $progress = 0;
        }

        if ($goal_amount > 0) {
            $percentage = $progress / $goal_amount;
            $percentage = $percentage * 100;
        } else {
            $percentage = 0;
        }

        if ($progress >= $goal_amount) {
            $success = true;
        } else {
            $success = false;
        }

        if ($percentage > 100) {
            $percentage = 100;            
        } else if ($percentage < -100) {
            $percentage = -100;
        }

        if ($success == true) {
            $view_class = "declaration_view_success";
        } else {
            $view_class = "declaration_view_in_progress";
        }

        ?>

        <div>
            <div class="<?php echo $view_class ?> declaration_view" post_id="<?php echo $goal_id;?>">

                <?php
                    $query = "SELECT text FROM axs_pulse_categories WHERE id = " . $category;
                    $db->setQuery( $query );     
                    $data = $db->loadObject();
                    $category_text = Jtext::_($data->text);
                ?>

                <span class="category_icon <?php echo $categories[$category]->icon; ?>" title="<?php echo $category_text ?>"></span>

                <?php 
                    $show_progress = 0;

                    /*
                        0 - do not show progress
                        1 - show actual progress ( ie. 45 out of 100)
                        2 - show accomplished / unaccomplished
                    */

                    if ($goal_type == 'goal') {
                        if ($progress_type == 'progressive') {
                            $show_progress = 1;
                ?>
                            <div class="progressChart" style="margin: 20px 20px 0px 0px">
                                <div class="chart">
                                    <div class="percentage" data-percent="<?php echo $percentage;?>">
                                        <span>
                                            <?php echo round($percentage);?>
                                        </span>
                                        <sup>%</sup>
                                    </div>  
                                </div>
                            </div>
                <?php
                        } else {
                            $show_progress = 2;            
                        }
                    } else if ($goal_type == 'declaration') {
                        if ($progress_type == 'accomplishable') {
                            $show_progress = 2;            
                        } else {
                            $show_progress = 0;                   
                        }
                    }

                    if ($show_progress == 2) {
                ?>	
                        <div class="status_box">
                            
                            <input name="goal" type="hidden" value="<?php echo $goal_id; ?>" />                        
                            <input class="success_box" title="<?php echo $board->accomplish_button_hover; ?>" type="submit" value="" goal="<?php echo $goal_id;?>"/>
                            <?php
                                if ($success) {
                            ?>
                                    <span class="lizicon-checkmark success_checkmark unsuccess_box" goal="<?php echo $goal_id;?>"></span>
                            <?php
                                }
                            ?>                        
                            <div class="accomplish_text">
                                <?php echo $board->accomplish_button_instructions; ?>
                            </div>
                        </div>
                <?php
                    }
                ?>

                <script>

                    function accomplish(div, progress) {
                        var goal = parseInt(div.getAttribute("goal"));
                        
                        var data = {
                            goal:           goal,
                            progress:       progress
                        };

                        $.ajax({
                            type: 'POST',
                            url: "index.php?option=com_axs&task=boards.updateProgress&format=raw",
                            data: data,
                            success: function(response) {
                                window.location.reload();
                            }
                        });
                    }                

                    $(".success_box").click(
                        function() {
                            accomplish(this, 1);
                        }
                    );
                    
                    $(".unsuccess_box").click(
                        function() {
                            accomplish(this, -1);
                        }
                    );
                                        
                </script>

                
                <div class="declaration_text">                
                    <?php echo $board->lead_in . " " . $declaration ?>
                </div>
                <div id="data_center">
                    <div class="progress_text">
                        <?php                        
                            if ($show_progress > 0) {

                                echo $board->progress . " ";

                                if ($show_progress == 1) {                            
                                    echo $progress . " out of " . $goal_amount;
                                } else if ($show_progress == 2) {
                                    if ($success) {
                                        echo $board->success;
                                    } else {
                                        echo $board->not_finished;
                                    }
                                }
                            } else {
                                echo $board->encouragement;
                            }
                        
                            $br = "<br>";
                            echo ($br . $board->date_made . " " . date("F d, Y", strtotime($start_date)));
                            echo " ";
                            if ($progress_type == 'progressive' || $progress_type == 'single') {
                                echo ($br . $board->end_date . " " . date("F d, Y", strtotime($end_date)));
                                if ($daysLeft >= 0) {
                                    echo ($br . $board->remaining_time . " " . $daysLeft);
                                } else {
                                    echo ($br . $board->passed_time . " " . -$daysLeft);
                                }
                            }
                            if (isset($exists)) {
                                echo ("exists: " . $exists);
                            }
                        ?>
                    </div>
                    <div id="button_center_<?php echo $goal_id; ?>" class="button_center">
                        <div class="update_form_<?php echo $goal_id; ?>">
                            <?php
                                if ($progress_type == 'progressive') {
                            ?>
                                    <strong><?php echo $board->update_field; ?></strong> 
                                    <input 
                                        id="declaraction_progress_amount_<?php echo $goal_id; ?>" 
                                        class="declaration_progress_amount" 
                                        name="progress" 
                                        type="text" 
                                        value="" 
                                        which="<?php echo $goal_id;?>" 
                                        placeholder="<?php echo $board->update_field_placeholder?>" 
                                        style="width:200px;"
                                    />
                                    <div id="clr" style="height:10px;"></div>
                                    <div id="update_warning_<?php echo $goal_id ?>" style="margin-top:20px; display:none;">
                                        <span class="warning">
                                            <span class="lizicon-arrow-up"></span>
                                            <?php echo $board->warning_invalid_update_number; ?>
                                        </span>
                                    </div>
                                    <div id="clr" style="height:10px;"></div>
                                    <button class="btn btn-primary update_progress_button" goal="<?php echo $goal_id;?>">Update</button>
                            <?php
                                }
                            
                                $url = "index.php?option=com_axs&view=board&id=$board_id&edit=1";
                                $editUrl = JRoute::_($url . "&update=$goal_id");
                                
                            ?>

                            <?php 
                                if ($public == 'yes') { 
                                    $publicStyle = "";
                                    $privateStyle = "display:none;";
                                } else {
                                    $publicStyle = "display:none;";
                                    $privateStyle = "";
                                }
                            ?>
                        
                            <span 
                                class="btn btn-primary delete_declaration_button" 
                                post_id="<?php echo $goal_id;?>"
                            >
                                Delete
                            </span>
                            <a 
                                class="btn btn-primary" 
                                href="<?php echo $editUrl; ?>"
                            >
                                Edit <?php echo (ucwords($goal_type)) ?>
                            </a>
                            <span 
                                class="btn btn-primary declaration_switch_privacy" 
                                post_id="<?php echo $goal_id;?>" 
                                current="public" 
                                style="<?php echo $publicStyle; ?>"
                            >
                                Make Private
                            </span>
                            <span 
                                class="btn btn-primary declaration_switch_privacy" 
                                post_id="<?php echo $goal_id;?>" 
                                current="private" 
                                style="<?php echo $privateStyle; ?>"
                            >
                                Make Public
                            </span>
                            <span
                                class="btn btn-primary edit_button add_new_note"                                
                                title="Notes"
                                post_id="<?php echo $goal_id;?>"
                            >
                                Notes
                            </span>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div id="clr" style="margin-bottom:20px; margin-top:20px;"></div>
        </div>
<?php
    }
} else {
    echo "<br /><br /><center><b>" . $board->no_entries . "</b></center><br /><br />";
}

?>