<?php 
    defined( '_JEXEC' ) or die( 'Restricted access' ); 

	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
    
    //$section = 'inspiration';

    require_once "components/shared/models/pulse.php";
    require_once "components/shared/controllers/common.php";
    require_once "components/shared/controllers/comments.php";
    require_once "components/shared/controllers/likes.php";
    require_once "components/shared/controllers/rating.php";
    require_once "components/shared/controllers/category.php";
    
    $model = FOFModel::getTmpInstance("Boards", "AxsModel");
    $data = $model->getBoardData();

    $font = $data->font;
    $board = $data->image;
    $data->board = $board;
    $board_id = JRequest::getVar('id');
?>

<script type="text/javascript" src="components/shared/includes/js/image.js"></script>
<script type="text/javascript" src="components/com_axs/assets/js/boards.js?v=4"></script>

<link rel="stylesheet" type="text/css" href="components/com_axs/assets/css/inspiration.css?v=2">
<link rel="stylesheet" type="text/css" href="components/shared/includes/css/blocksit.css?v=2">

<script type="text/javascript" src="components/shared/includes/js/blocksit.js?v=2"></script>

<script>
    var text = <?php echo json_encode($board->description); ?>;
    set_whats_this (text);

    //Set the page variables for boards.js
    set_pulse_type('inspiration');
    set_column_max(10);
    set_has_charts(false);

    set_current_board("<?php echo $board_id;?>");
</script>

<div class="large-white">
    <?php echo $board->board_name; ?>
</div>

<div>
    <?php
        showPulse($data, $board_id);
    ?>
    <script>
        setPage(0);
    </script>
</div>


<div id="loading" class="show_on_reload" style="font-size: 40px; color:rgb(128, 128, 128);">
    <img src="images/block_loader.gif" width="200" height="200">
    <?php echo $board->loading; ?>
</div>

<?php include "components/shared/models/column_set.php"; ?> 

<div id="board_container"></div>

<br>
<div class="hide_on_reload">
    <div id="pagination_buttons_bottom" style="display:none;"></div>
</div>

<?php    
    if ($board->background_color) {
?>
        <style>
            body {
                background-color:   <?php echo $board->background_color; ?>;
            }
        </style>

<?php
    }

    if ($board->text_color) {
?>
        <style>
            .large-white {
                color:      <?php echo $board->text_color; ?>;
            }

            #column_set {
                color:      <?php echo $board->text_color; ?> !important;
            }

            #loading {
                color:      <?php echo $board->text_color; ?> !important;
            }

            .columnChange {
                color:      <?php echo $board->text_color; ?> !important;
            }

            #pagination_view {
                color:      <?php echo $board->text_color; ?> !important;
            }
        </style>
<?php
    }
?>