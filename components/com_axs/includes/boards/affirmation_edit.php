<?php
    defined( '_JEXEC' ) or die( 'Restricted access' );

    //error_reporting(E_ALL);
    //ini_set('display_errors', 1);    

    $board_id = JRequest::getVar('id');

    $model = FOFModel::getTmpInstance("Boards", "AxsModel");
    $data = $model->getBoardData($board_id);

    $user_id = JFactory::getUser()->id;

    if (!$user_id) {
        header("Location: /");
    }

    if (!$data) {
        return;
    }

    $access = $model->checkAccess($board_id);
    if (!$access) {
        return;
    }

    $font = $data->font;
    $board = $data->my_affirmation;

    if (!$board) {
        return;
    }

    require_once "components/shared/controllers/common.php";
    require_once "components/shared/controllers/comments.php";
    require_once "components/shared/controllers/likes.php";
    require_once "components/shared/controllers/category.php";

?>

<script type="text/javascript" src="components/com_axs/assets/js/my-gratitude.js"></script>

<link type="text/css"  rel="stylesheet" href="components/com_axs/assets/css/my-gratitude.css"/>

<?php    
    if ($board->background_color) {
?>
        <style>
            body {
                background-color:   <?php echo $board->background_color; ?>;
            }
        </style>

<?php
    }

    if ($board->text_color) {
?>
        <style>
            .large-black {
                color:      <?php echo $board->text_color; ?>;
            }

            .grateful_what {
                color:      <?php echo $board->text_color; ?>;
            }

            .grateful_why {
                color:      <?php echo $board->text_color; ?>;
            }

            .words {
                color:      <?php echo $board->text_color; ?>;
            }

            .mid-text {
                color:      <?php echo $board->text_color; ?>;
            }

            .declare-tbl {
                color:      <?php echo $board->text_color; ?>;
            }
        </style>
<?php
    }
?>

<div class="large-black">
    <?php echo $board->board_name;?>
</div>

<script>
    $(document).ready(
        function() {
            setMaxStringLength(<?php echo $board->max_length; ?>);
            setBoardId(<?php echo $board_id; ?>);
        }
    );
</script>

<?php    
    $backRoute = JRoute::_("index.php?option=com_axs&view=board&id=$board_id");
?>

<a href="<?php echo $backRoute;?>">
    <button class="btn btn-primary" style="margin: 20px 0px 20px 0px;">
        <?php echo $board->back_button; ?>
    </button>
</a>

<?php
    if (!isset($update)) {
        $new_gratitude = true;
    } else {
        $new_gratitude = false;

        $update = (int)$update;

        $db = JFactory::getDBO();

        $query = $db->getQuery(true);

        $del = $db->quoteName('deleted');

        $conditions = array(
            $db->quoteName('id') . '=' . $db->quote($update),
            $db->quoteName('user_id') . '=' . $db->quote($user_id),
            $db->quoteName('board_id') . '=' . $db->quote($board_id),
            "($del IS NULL OR $del = '')"
        );        

        $query
            ->select('*')
            ->from($db->quoteName('gratitude_board'))
            ->where($conditions);

        $db->setQuery($query);
        $edit_post = $db->loadObject();

        if (!$edit_post) {
            //Incorrect ID
            $update = null;
            $new_gratitude = true;
        }
    }
?>

<div id="right-links">
    <div id="am-btn" class="plus" 
        <?php 
            if ($update != '') {
                echo 'style = "display:none;"'; 
            }            
        ?>>        
        <?php echo $board->new_button; ?>
    </div>
</div>

<style>
    .warning {
        font-size:          20px;
        color:              red;
        border-radius:      10px;
        border:             1px solid red;
        background-color:   rgb(255, 240, 240);
        padding:            5px;
        box-shadow:         5px 5px 2px rgb(128,128,128);
    }
</style>

<script>
    setThankfulMessage("<?php echo $board->already_thankful_check; ?>");
</script>

<div id="add-special"  
     <?php 
        if ($update == '') { 
            echo 'style="display:none;"'; 
        } 
     ?>>
    
    <?php
        if ($update != '') {
            $params = array(
                "update" => 1,
                "gratitude" => $update
            );
        } else {
            $params = array(
                "add" => 1
            );
        }

        $key = AxsKeys::getKey('boards');
        $params = base64_encode(AxsEncryption::encrypt($params, $key));
    ?>    

    <form id="gratitude_form" method="post" action="<?php echo $params; ?>" onsubmit="return validateForm()">
        
        <div class="mid-text" style="text-align:left;">
            <?php 
                if ($new_gratitude) {
                    echo $board->new_affirmation;
                } else {
                    echo $board->edit_affirmation;
                }
            ?>
        </div>
        <br />

        <table border="0" class="declare-tbl">
            <tr id="gratitude_text">
                <td align="right" valign="top">
                    <strong><?php echo $board->user_affirmation; ?></strong> 
                </td>
                <td align="left" >                   
                    <textarea id="grateful_what_text" name="grateful_what" cols="50" rows="10"><?php echo $edit_post->what;?></textarea>
                </td>
            </tr>
            <tr id="gratitude_text_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_text; ?>
                    </span>
                </td>
            </tr>
            
            <?php if ($board->require_reason) { ?>
                <tr id="gratitude_why">
                    <td align="right" valign="top">
                        <strong><?php echo $board->reason; ?> </strong> 
                    </td>
                    <td align="left" >
                        <textarea id="grateful_why_text" name="grateful_why" cols="50" rows="5"><?php echo $edit_post->why;?></textarea>
                    </td>
                </tr>
                <tr id="gratitude_why_warning" style="display:none;">
                    <td align="right" valign="top"></td>
                    <td align="left">
                        <span class="warning">
                            <span class="lizicon-arrow-up"></span>
                            <?php echo $board->warning_reason; ?>
                        </span>
                    </td>
                </tr>
            <?php } ?>

            <tr id="gratitude_category">
                <td align="right" valign="top">
                    Category: 
                </td>
                <td align="left" >
                    <select id="gratitude_category_select" name="category" style="width:150px;">
                        <option value="">--select one--</option>
                        <?php

                            $categories = getCategories($data->categories);

                            foreach ($categories as $cat) {                                    
                                ?>
                                    <option value="<?php echo $cat->id; ?>" <?php if ($edit_post->category == $cat->id) { echo 'selected'; } ?>> <?php echo $cat->text;?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr id="gratitude_category_warning" style="display:none;">
                <td align="right" valign="top"></td>
                <td align="left">
                    <span class="warning">
                        <span class="lizicon-arrow-up"></span>
                        <?php echo $board->warning_category; ?>
                    </span>
                </td>
            </tr>

            <tr>
                <td align="right" valign="top">
                    On this Date: 
                </td>
                <?php
                    if ($update) {
                ?>
                        <td align="left" valign="top">
                            <?php echo (date('m-d-Y', $edit_post->date)); ?>
                        </td>
                <?php
                    } else {
                ?>
                        <td align="left" valign="top">
                            <?php echo (date('m-d-Y')); ?>
                        </td>
                <?php
                    }
                ?>
            </tr>
            
            <tr>
                <td align="right" valign="top">
                    Public:
                </td>
                <td align="left" >
                    <select id="public_setting" name="public" style="width:80px;">
                        <option value="yes" <?php if($edit_post->public == 'yes') { echo 'selected'; }?> >Yes</option>
                        <option value="no" <?php if($edit_post->public == 'no') { echo 'selected'; }?> >No</option>
                    </select>
                    <br />
                    <br />
                </td>
            </tr>
            
            <tr>
                <td align="right" valign="top"></td>
                <td align="left" >
                    <?php
                        if ($new_gratitude) {
                            $submit_value = $board->new_submit;
                        } else {
                            $submit_value = $board->edit_submit;
                        }
                    ?>
                    <input id="gratitude_submit_button" type="submit" class="btn btn-primary" name="save" value=<?php echo('"' . $submit_value) . '"' ?> />
                    &nbsp;&nbsp;&nbsp;
                    <?php
                        $cancelUrl = JRoute::_("index.php?option=com_axs&view=board&id=$board_id&edit=1");
                    ?>
                    <a href="<?php echo $cancelUrl; ?>" class="btn btn-primary" <?php if (!$update) { echo 'style="display:none;"'; } ?>>Cancel</a>
                </td>
            </tr>
            
        </table>
    </form>
</div>

<?php include(JPATH_BASE . "/components/com_axs/includes/boards/notes.php"); ?>

<div id="gratitude_list">

    <div id="grateful_title_bar">
        <div id="left_title" class="grateful_title">
            <div id="grateful_title_checkbox"><?php echo $board->thankful; ?></div>
        </div>
        <div id="right_title" class="grateful_title">
            <div id="grateful_title_text"><?php echo $board->title_header; ?></div>
            <div id="grateful_title_edit">Edit</div>
        </div>
    </div>

    <?php

        $db = getDatabase();

        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('user_id') . '=' . (int)$user_id,
            '(' . $db->quoteName('deleted') . '="" OR ' . $db->quoteName('deleted') . 'IS NULL)',
            $db->quoteName('board_id') . '=' . (int)$board_id
        );

        $query
            ->select('*')
            ->from($db->quoteName('gratitude_board'))
            ->where($conditions)
            ->order($db->quoteName('id') . ' DESC');

        $db->setQuery( $query );     
        $rows = $db->loadObjectList();    

        if ($rows != null) {
            foreach($rows as $row) {    
                $gratitude_id = $row->id;
                //$user_id = $row->user_id;
                $what = $row->what;
                $why = $row->why;        
                $category = $row->category;        
                $grateful_date = $row->date;
                $public = $row->public;
                
                $query = $db->getQuery(true);
                
                $conditions = array(
                    $db->quoteName('id') . '=' . (int)$gratitude_id
                );
                
                $query
                    ->select('*')
                    ->from($db->quoteName('gratitude_board'))
                    ->where($conditions);
                
                $db->setQuery($query);
                $data = $db->loadObject();
                
                //Check to see if the date has changed.  If so, they can be thankful again.
                if ($data != null) {


                    $last_thankful = $data->last_thankful;

                    //Convert the date down to the day.

                    $last = date("Y-m-d", strtotime($last_thankful));
                    $now = date("Y-m-d", strtotime('now'));

                    //If it's the same as now, it's the same day
                    if ($last != $now) {            
                        $title = $board->thankful_check;
                        $checked = "";            
                    } else {
                        $title = $board->already_thankful_check;
                        $checked = "checked";
                    }
                }

                ?>
                <div>
                    <div id="grateful_checkbox">
                        <input id="check_thanked_<?php echo ($gratitude_id) ?>" title="<?php echo($title) ?>" type="checkbox" <?php echo($checked) ?>>
                    </div>

                    <div id="grateful_text">
                        <span class="grateful_what">
                            <?php echo $board->lead_in . ' "' . $what . '"'; ?>
                            <br>
                            <?php 
                                if ($board->require_reason) {
                                    echo $board->reason_lead_in . ' "' . $why . '"'; 
                                }
                            ?>
                            <hr>
                            <div style="font-size:12px">
                                <?php 
                                    $like_comment_id = "gratitude_" . $gratitude_id;
                                    like_button($like_comment_id);

                                    $params = new stdClass();

                                    $params->title = $board->comments;
                                    $params->button_title = $board->comments_submit;
                                    $params->placeholder = $board->comments_placeholder;
                                    $params->post_id = $like_comment_id;

                                    display_comments($params);
                                ?>
                            </div>
                        </span>
                        <br/>
                    </div>

                    <div id="grateful_edit">
                        <input name="gratitude" type="hidden" value="<?php echo $gratitude_id; ?>" />

                        <?php
                            $url = "index.php?option=com_axs&view=board&id=$board_id&edit=1";
                            $editUrl = JRoute::_($url . "&update=$gratitude_id");

                            if ($public == "yes") {
                                $privateStyle = "display:none;";
                                $publicStyle = "";
                            } else {
                                $privateStyle = "";
                                $publicStyle = "display:none;";
                            }
                        ?>

                        <div id="clr" style="height:10px;"></div>
                        <?php //Leave </span> on the end or spaces will appear on the page.?>
                        <span
                            class="lizicon-files-empty edit_button add_new_note"
                            style="font-size: 30px;"
                            title="Notes"
                            post_id="<?php echo $gratitude_id;?>"
                        >
                        </span>
                        <span 
                            class="lizicon-cross edit_button delete_gratitude" 
                            style="font-size:30px;" 
                            title="Delete" 
                            post_id="<?php echo $gratitude_id;?>"></span>

                        <a class="lizicon-pencil" style="font-size:30px;" title="Edit" href="<?php echo $editUrl;?>"></a>

                        <span 
                            class="lizicon-eye-blocked edit_button gratitude_switch_privacy" 
                            current="public" 
                            style="font-size:30px; <?php echo $publicStyle;?>" 
                            title="Make Private" 
                            post_id="<?php echo $gratitude_id;?>"></span>

                        <span 
                            class="lizicon-eye edit_button gratitude_switch_privacy" 
                            current="private" 
                            style="font-size:30px; <?php echo $privateStyle;?>" 
                            title="Make Public" 
                            post_id="<?php echo $gratitude_id;?>"></span>
                    </div>

                    <script>
                        var gratitude_id = <?php echo $gratitude_id; ?>;
                        uncheck_at_midnight(gratitude_id);
                        thankful(gratitude_id);
                    </script>

                    <div id="clr" style="border-bottom: 1px solid #999; margin-bottom:20px; margin-top:20px;"></div>
                </div>
                
    <?php    
            }
        } else {
            ?>
                <br />
                <br />
                <center>
                    <b>
                        <?php echo $board->empty_list; ?> 
                    </b>
                </center>
                <br />
                <br />
            <?php
        }

    ?>
</div>
       