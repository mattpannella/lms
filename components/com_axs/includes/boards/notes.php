<style>
    #notes {
        border: 1px solid gray;
        min-width: 800px;
        width: 100%;
        min-height: 600px;
    }

    #new_note {
        width: 100%;
        min-height: 240px;
        border: 1px solid gray;
    }

    #notes_controls {
        width: 100%;
        height: 60px;
    }

    #notes_content {
        width: 100%;
        min-height: 300px;
        max-height: 600px;
        border: 1px solid gray;
        overflow-y: scroll;
    }

    .notes_button {
        margin: 10px;
        float: right;
    }

    #new_note_content {
        min-height: 280px;
        width: 96%;
        margin-top: 10px;
        margin-bottom: 10px;
        margin-left: 2%;
    }

    .note_container {
        position: relative;
        min-height: 80px;
        padding: 10px;
        margin: 5px;
        border: 1px solid #999;
        border-radius: 4px;
        box-shadow: 3px 3px 10px #aaa;

    }

    .note_date {
        position: absolute;
        width: 100px;
        font-size: 12px;
        left: 10px;
        top: 5px;
        color: #777;
    }

    .note_time {
        position: absolute;
        width: 100px;
        font-size: 12px;
        left: 10px;
        top: 20px;
        color: #777;
    }

    .note_content {
        margin-left: 120px;
        font-size: 18px;
        color: #333;
    }

    .note_controls {
        position: absolute;
        right: 10px;
        bottom: 10px;
    }
</style>

<div id="notes" style="display: none;">
    <div id="notes_content"></div>
    <div id="new_note">
        <textarea id="new_note_content"></textarea>
    </div>
    <div id="notes_controls">
        <span id="notes_save" class="notes_button btn btn-success">Save</span>
        <span id="notes_update" class="notes_button btn btn-success" style="display:none;">Update</span>
        <span id="notes_update_cancel" class="notes_button btn btn-warning" style="display:none;">Cancel</span>
        <span id="notes_close" class="notes_button btn btn-primary">Close</span>
    </div>    
</div>

<div id="note_template" style="display:none;">
    <div class="note_container">
        <div class="note_date"></div>
        <div class="note_time"></div>
        <div class="note_content"></div>
        <div class="note_controls">
            <span class="note_edit btn btn-primary">Edit</span>
            <span class="note_delete btn btn-danger">Delete</span>
        </div>
    </div>
</div>

<script type="text/javascript" src="components/com_axs/assets/js/board-notes.js"></script>