<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 1/27/16
 * Time: 10:04 AM
 */

defined('_JEXEC') or die();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
//$doc->addStyleSheet(JURI::root(true) . '/components/com_axs/assets/css/main.css');

// Load FOF
include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
    JError::raiseError ('500', 'FOF is not installed');
    return;
}

FOFDispatcher::getTmpInstance('com_axs')->dispatch();