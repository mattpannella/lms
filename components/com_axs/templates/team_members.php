<?php
    $currentUrl = urlencode($_SERVER['REQUEST_URI']);
    $actionParams = new stdClass();
    $actionParams->team_id = (int)$this->getTeam()->id;
    $actionParams->learnerDashboard_id = (int)$this->getDashboard()->id;
    $actionParams->action = 'register';
    $actionParams->returnUrl = $currentUrl;
    $addEncryptedParams = base64_encode(AxsEncryption::encrypt($actionParams, $key));

    $teamMembersParams = new stdClass();
    $teamMembersParams->team_id = (int)$this->getTeam()->id;
    $teamMembersParams->learnerDashboard_id = (int)$this->getDashboard()->id;
    $teamMembersParams->dashboard_params = $this->getDashboardParams();
    $teamMembersParams->team_members = $this->getUserList();
    $teamMembersParams->team_params = $teamParams;
    $teamMembersEncryptedParams = base64_encode(AxsEncryption::encrypt($teamMembersParams, $key));

    $teamCount = count($teamMembers);
    $listSize  = 50;
    if($teamCount > 50) {
        $limitFilters = true;
    } else {
        $limitFilters = false;
    }
?>
<style>

.ajax-load-more {
    font-size:  25px;
    text-transform: uppercase;
    color: #0067ff;
}
.ajax-load-more img {
    width: 25px;
}

.ajax-load-more {
    margin-top: 40px;
}
.ml-card h4 {
    font-size: 20px !important;;
}
.load-more-box {
    width: 100%;
    height: 100px;
}
.load-more {
    margin-top: 24px;
    margin-bottom: 24px;
}
</style>
<div class="tld-team-container">
    <div class="tld-team-wrapper">
        <div class="tld-section-top">
            <span class="tld-team-top-heading"><?php echo AxsLanguage::text("AXS_TEAM_MEMBERS", "Team Members") ?> <span class="tld-team-top-heading-span"><br>(<?php echo $teamCount; ?> <?php echo AxsLanguage::text("AXS_MEMBERS", "Members") ?>)</span></span>
            <div class="tld-search-filtering">
                <div class="tld-search-form-block w-form">
                    <div class="tld-search-form">
                        <div class="tld-search-field-block">
                            <div class="tld-search-icon-block">
                                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_filtering.svg" alt="" class="tld-search-image">
                            </div>
                            <select id="team-filter" name="team-filter" class="tld-select-field w-select">
                                <option value="alphabeticalASC"><?php echo AxsLanguage::text("AXS_SEARCH_FILTERS", "Search Filters") ?></option>
                                <option value="alphabeticalASC"><?php echo AxsLanguage::text("AXS_ALPHABETICAL_ASCENDING", "Alphabetical Ascending") ?></option>
                                <option value="alphabeticalDESC"><?php echo AxsLanguage::text("AXS_ALPHABETICAL_DESCENDING", "Alphabetical Descending") ?></option>
                                <option value="coursesMost" <?php if($limitFilters) { echo 'disabled'; } ?>><?php echo AxsLanguage::text("AXS_MOST_COURSES_COMPLETED", "Most Courses Completed") ?></option>
                                <option value="coursesLeast" <?php if($limitFilters) { echo 'disabled'; } ?>><?php echo AxsLanguage::text("AXS_LEAST_COURSES_COMPLETED", "Least Courses Completed") ?></option>
                                <option value="checklistMost" <?php if($limitFilters) { echo 'disabled'; } ?>><?php echo AxsLanguage::text("AXS_MOST_CHECKLISTS_COMPLETED", "Most Checklists Completed") ?></option>
                                <option value="checklistLeast" <?php if($limitFilters) { echo 'disabled'; } ?>><?php echo AxsLanguage::text("AXS_LEAST_CHECKLISTS_COMPLETED", "Least Checklists Completed") ?></option>
                            </select>
                        </div>
                        <div class="tld-search-field-block">
                            <div class="tld-search-icon-block tld_search">
                                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_search.svg" alt="" class="tld-search-image">
                            </div>
                            <input type="text" name="tld_search" id="tld_search" class="tld-select-field w-input" maxlength="256" placeholder="<?php echo AxsLanguage::text("JSEARCH_FILTER", "Search") ?>">
                        </div>

                    <?php if($teamParams->allow_adding_users) { ?>
                        <div class="tld-search-field-block">
                            <a
                                href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=user_manager&action=<?php echo $addEncryptedParams; ?>"
                                class="tld-select-field w-input jcepopup noicon"
                                data-mediabox="1"
                                data-mediabox-width="800"
                                data-mediabox-height="900"
                                type="iframe"
                            >
                                <i class="fa fa-plus"></i> <?php echo AxsLanguage::text("AXS_ADD_USER", "Add User") ?>
                            </a>
                        </div>
                    <?php } ?>

                    </div>
                </div>
            </div>

        </div>
        <div class="tld-team-content-container">
            <div class="nano">
			    <div class="nano-content tld-team-member-wrapper">
                    <div class="team-list"></div>

                    <div class="ajax-load-more text-center" style="display:none; width: 100%; clear: both;">
                        <p><?php echo AxsLanguage::text("AXS_LOADING", "Loading") ?> <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"></p>
                    </div>
                    <div class="ajax-load-morebutton text-center" style="width: 100%; clear: both;">
                        <a class="btn btn-outline-primary btn-sm w-50 load-more"><?php echo AxsLanguage::text("AXS_LOADMORE", "Load More") ?></a>
                    </div>
                    <div class="all-items-loaded text-center" style="display:none; width: 100%; clear: both;">
                        <p><?php echo AxsLanguage::text("AXS_ALL_ITEMS_LOADED", "All Items Loaded") ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var start = 0;
    var size = <?php echo $listSize; ?>;
    let allItemsLoaded = false;
    const team = '<?php echo $teamMembersEncryptedParams; ?>';
    const add = '<?php echo $addEncryptedParams; ?>';
</script>
<script src="/components/com_axs/assets/js/team_members.js?v=10" type="text/javascript"></script>