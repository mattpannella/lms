<div class="tld-leaderboard-container">
    <div class="tld-team-wrapper">                
        <div class="tld-leaders-container" id="oc-leaders">
            <div class="tld-section-top">
                <div class="tld-team-top-heading" id="oc-leaders-title" style="text-align: center !important; width: 100%;"><?php echo AxsLanguage::text("AXS_LEADERBOARD", "Leaderboard") ?></div>
                
            </div>
            <div class="clearfix"></div>
            <div id="oc-leaders-body">
                <?php
                    $l = 0;
                    $dashboardParams = $this->getDashboardParams();
                    $leaderboardAmount = 8;
                    $leaderboardMax = 20;
                    if($dashboardParams->show_team_leaderboard_amount) {
                        $leaderboardAmount = (int)$dashboardParams->show_team_leaderboard_amount;
                    }
                    if($dashboardParams->show_team_leaderboard_max) {
                        $leaderboardMax = (int)$dashboardParams->show_team_leaderboard_max;
                    }
                    $leaderboardList = $this->getLeaderboardList($leaderboardMax);
                    foreach ($leaderboardList as $leaderboardUser) {
                        $l++;
                        $hideLeaderboardUserClass = '';                        
                        $cUser = CFactory::getUser($leaderboardUser->user_id);
                        if($cUser->id) {
                            $rank = $this->ordinalNumber($l);
                            $hideClass = '';
                            if($l > $leaderboardAmount) {
                                $hideClass = 'hide_ld_avatar';
                            }
                        ?>
                        <div class="tld-leader w-inline-block <?php echo $hideClass; ?>">                   
                            <img
                                class="tld-team-user-avatar"
                                src="<?php echo $cUser->getAvatar(); ?>"
                                alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>"
                                data-author="<?php echo $cUser->id;?>"
                            >
                            <div class="tld-leader-rank">
                                <div><?php echo $rank; ?></div>
                            </div>
                            <?php if($dashboardParams->show_leaderboard_user_points) : ?>
                            <div class="leaderboard-user-points">
                                <?php echo $leaderboardUser->total; ?>pts
                            </div>
                            <?php endif; ?>
                        </div>
                <?php 
                        }
                    }    
                ?>
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_plus-1.svg" data-content="oc-leaders" class="trigger-offcanvas tld-team-user-avatar more">
            </div>
        </div>
    </div>
</div>