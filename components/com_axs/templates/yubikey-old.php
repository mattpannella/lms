<?php
	if($otpConfig->method === 'yubikey') {
		$alreadySetup = true;
	}
?>
<div class="header-fullwidth" style="
	background-image: url(/components/com_axs/assets/images/2fa-bg.jpg);
	background-size:cover;
	background-position:center;
	height: 300px;
	position: relative;">
	<div class="slide-overlay">
		<h2>Setup Your YubiKey Two-factor Authentication</h2>
	</div>
</div>
<div class="container tfa-wrapper">
	<p style="font-size: 18px; font-weight: bold; margin-top: 20px;">The YubiKey reinvented hardware authentication and is 100% purpose built for security. The highest level of phishing defense, yet easy to use, the YubiKey is your key to trust.
	<hr style="padding: 16px; "/>
	</p>
	<div class="row tfa-instructions-wrapper">
		<div class="col-lg-6">
			<img src="/components/com_axs/assets/images/tovuti-lms-and-yubikey.jpg" class="img-responsive" alt="">
		</div>
		<div class="col-lg-6 tfa-text">
			<div class="tfa-instructions">
				<ol class="tfa-list">
					<li>Insert your Yubikey into your USB port</li>
					<li>Set your cursor in the YubiKey Field below</li>
					<li>Touch your Yubikey and click save!</li>
				</ol>
			</div>
            <div class="tfa-fields" <?php if($alreadySetup) { echo 'style="display: none;"'; } ?>>
                <div class="input-group">
                    <span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i> Yubikey</span>
                    <input class="form-control" autocomplete="off" id="tfa-key" type="password" placeholder="Enter Security Key">
                </div>
                <div class="tfa-backups"></div>
                <button class="btn btn-primary tfa-button">Save Key</button>
            </div>
            <div class="tfa-success" <?php if(!$alreadySetup) { echo 'style="display: none;"'; } ?>><i class="fa fa-check"></i>Your Two-factor Athentication is now Active!</div>
		</div>
	</div>
</div>

<script>
	$('#tfa-key').focus();
	function saveKey() {
        $('.tfa-button').attr('disabled','true');
		const key = $("#tfa-key").val();
		if(!key) {
			alert('You need to add the key')
		}
        const data = {
            secretKey: $("#tfa-key").val()
		}

        $.ajax({
            url: '/index.php?option=com_axs&task=user.saveYubiKey&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                let response = JSON.parse(result);
				console.log(response.backups)
				let backups = '';
                if (response.status == "success") {
					$('.tfa-fields').hide();
					$('.tfa-success').fadeIn(700);
					/* response.backups.forEach((backup)=>{
						backups += `${backup} <br>`;
					}) */
					//$('.tfa-backups').html(backups)
                } else {
                   alert('There was an error saving the key')
                }
        });
	}
	$('.tfa-button').click(saveKey)
</script>