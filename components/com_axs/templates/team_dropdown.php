<div class="tld-selector-container">
    <div class="tld-selector-wrapper">
        <div class="tld-selector">
            <div data-hover="" data-delay="0" class="tld-selector-dropdown w-dropdown">
                <div class="tld-selector-toggle w-dropdown-toggle">
                    <div class="tld-selector-textwrap">
                    <div class="tld-selector-text">
                        <span class="tld-selector-textspan"><?php echo AxsLanguage::text("AXS_DASHBOARD", "Dashboard") ?>:</span> 
                        <span class="current_team">
                            <?php echo $this->team->title; ?>
                    </div>
                    </div>
                    <div class="tld-selector-icon team_dropdown">
                        <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_dropdown.svg" alt="" class="tld-dd-icon">
                    </div>
                </div>
                <nav class="tld-dropdown w-dropdown-list" id="learner-dropdown">
                    <input type="text" id="searchQuery" class="joms-input" placeholder="Search" onkeyup="filterDashboardDropdown()">
                        <div class="tld-dropdown-container">
                            <div class="tld-dropdown-item">
                                <a href="<?php echo $urlBase.'?show_ld=1'; ?>" target="_blank" class="tld-dropdown-item-left w-inline-block">
                                    <div class="tld-selector-text">
                                        <span class="tld-selector-textspan"><?php echo AxsLanguage::text("AXS_PERSONAL", "Personal") ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php
                        foreach($teams as $team) {
                    ?>
                        <div class="tld-dropdown-container">
                            <div class="tld-dropdown-item">
                                <a href="<?php echo $urlBase.'?team='.base64_encode(AxsEncryption::encrypt($team->id, $key)); ?>" class="tld-dropdown-item-left w-inline-block">
                                    <div class="tld-selector-text">
                                        <?php echo $team->title; ?>
                                    </div>
                                </a>
                                <a style="cursor:pointer" class="tld-dropdown-item-right w-inline-block tld-set-default-button" data-id="<?php echo $team->id; ?>">
                                    <?php 
                                        $showDefault = false;
                                        if ($defaultTeamId == $team->id) {
                                            $showDefault = true;
                                        }
                                    ?>  
                                    <div id="team_selector_default_<?php echo $team->id; ?>" class="tld-selector-text selected default_team" <?php echo !$showDefault ? 'style="display:none"' : '' ?>>
                                        <?php echo AxsLanguage::text("JDEFAULT", "Default"); ?>
                                    </div>
                                    <div id="team_selector_<?php echo $team->id; ?>" class="tld-selector-text selected set" <?php echo $showDefault ? 'style="display:none"' : '' ?>>
                                        <?php echo AxsLanguage::text("AXS_SET_DEFAULT", "Set Default") ?>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </nav>
            </div>
            <?php if($teamParams->allow_assigning_courses || $teamParams->allow_assigning_events) { ?>
            <div class="tov-ld-tab-container">
                <a href="javascript:void(0)" class="tov-ld-tab-button active w-button members-btn"><?php echo AxsLanguage::text("AXS_TEAM_MEMBERS", "Team Members") ?></a>
                <a href="javascript:void(0)" class="tov-ld-tab-button w-button assignment-btn"><?php echo AxsLanguage::text("AXS_ASSIGNMENTS", "Assignments") ?></a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>