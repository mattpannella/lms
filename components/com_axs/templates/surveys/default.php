<?php
    defined("_JEXEC") or die();
    $document = JFactory::getDocument();
    $document->addScriptDeclaration(trim("
        let NEED_TO_ANSWER_SURVEY_QUESTION_MESSAGE = `" . AxsLanguage::text("AXS_NEED_TO_ANSWER_SURVEY_QUESTION", "You need to answer the survey question before moving forward.") . "`;
        let ERROR_SUBMITTING_ANSWER_MESSAGE = `" . AxsLanguage::text("AXS_ERROR_SUBMITTING_ANSWER", "There was an error submitting your answer.") . "`;
    "));
    $document->addScript("/components/com_axs/assets/js/surveys.js?v=11");
    $survey = $this->survey;
    $title  = $survey->title;
    $number = 0;
    $userId = JFactory::getUser()->id;
    $completed_message = false;
    if($survey->fullwidth) {
        $class = 'container-fluid';
    } else {
        $class = 'container';
    }
    if(!$userId) {
        echo AxsLanguage::text("AXS_MUST_BE_LOGGED_IN_FOR_SURVEY", "You must be logged in to take this survey");
    } else {
        $userSurveyResponses = $this->getUserSurveyResponses($userId, $this->survey->id);
        $userSurveyResponsesCount = count($userSurveyResponses);
        $questionTotalAmount = count($this->questions);
        if($userSurveyResponsesCount >= $questionTotalAmount) {
            $completed_message = $this->getCompletedMessage();
        }

        if(!$completed_message && $userSurveyResponses) {
            $lastQuestionAnsweredIndex = $userSurveyResponsesCount - 1;
            $question_id = $userSurveyResponses[$lastQuestionAnsweredIndex]->question_id;
            $lastQuestionAnswered = $this->getQuestionById($question_id);
            $nextQuestionIndex = $lastQuestionAnswered->index + 1;
            $number = $nextQuestionIndex;
        }
?>

<link rel="stylesheet" type="text/css" href="/components/com_axs/assets/css/surveys.css?v=11">
<div class="<?php echo $class; ?>">
    <div class="row">
        <div class="col-md-12">
            <h2 class="tovuti_survey_title">
                <?php echo $title; ?>
            </h2>
            <div class="tovuti_survey_description">
                <?php echo $survey->description; ?>
            </div>
            <form class="tovuti_survey_question_container">
                <?php
                    if($completed_message) {
                        echo $completed_message;
                    } else {
                        echo $this->loadQuestion($number);
                    }
                ?>
            </form>
            <?php if(!$completed_message) { ?>
            <a class="survey_submit_button btn btn-primary"><?php echo AxsLanguage::text("JSUBMIT", "Submit") ?></a>
            <?php } ?>
            <div class="ajax-load-more" style="display:none;">
                <p>Sending <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"> Answer</p>
            </div>
        </div>
    </div>
</div>
<?php
    } //end of user logged in check
?>