<?php
echo AxsLoader::loadKendo();
if($reportParams->user_id) {
    $attendance = $this->getAttendance($reportParams->user_id);
} else {
    $attendance = $this->getAttendance();
}

$trackParams = new stdClass();
$trackParams->eventName = 'Run Report From Team Lead Dashboard';
$trackParams->action_type = 'Team Lead';
$trackParams->description = '[type: attendance team_id:'.$reportParams->team_id.']';
AxsTracking::sendToPendo($trackParams);
AxsActions::storeAdminAction($trackParams);
?>
<style>
   body {
       background: #fff;
   }
</style>

<table id="attendanceGrid" class="table table-bordered table-striped table-responsive">

    <tr>
        <th><?php echo AxsLanguage::text("AXS_NAME", "Name") ?></th>
        <th><?php echo AxsLanguage::text("AXS_CLASS_SLASH_MEETING", "Class/Meeting") ?></th>
        <th><?php echo AxsLanguage::text("AXS_DATE_ATTENDED", "Date Attended") ?></th>
        <th><?php echo AxsLanguage::text("AXS_TIME_SPEND_IN_CLASS_MEETING", "Time Spent in Class/Meeting") ?></th>
	</tr>
	<?php
        foreach($attendance as $row) {
           /*  $profile = AxsExtra::getUserProfileData($row->user_id);

            if ($profile->photo) {
                $photo = $profile->photo;
            } else {
                $photo = "/images/user.png";
            }

            $user_display  =  "<div  class='comment-img' style='border-radius:50%;'><a target='_blank'  class='student-link' href='/my-profile/" . $profile->alias . "'><img src='" . $photo . "' style='width: 50px;'></a></div>"; */
    ?>
    <tr>
        <td><?php echo $row->name; ?></td>
        <td><?php echo $row->meetingName; ?></td>
        <td><?php echo date('m/d/Y', strtotime($row->date)); ?></td>
        <td><?php if($row->check_out) { echo gmdate('H:i:s', round( strtotime($row->check_out) - strtotime($row->check_in) ) ); } ?></td>
	</tr>
    <?php } ?>
</table>

<script>

var kendoOptions = {
    toolbar: ["pdf", "excel"],
    sortable:   true,
    groupable:  true,
    resizable:  true,
    columnMenu: true,
    filterable: {
        mode: "row"
    },
    /*height:   600,*/
    pageable: {
        buttonCount:    8,
        input:          true,
        pageSize:       20,
        pageSizes:      true,
        refresh:        true,
        message: {
            empty:      "<?php echo AxsLanguage::text("AXS_THERE_ARE_NO_ENTRIES_TO_DISPLAY", "There are no entries to display") ?>"
        }
    },
    columns: [
        {
            field: "Name",
            title: "<?php echo AxsLanguage::text("AXS_NAME", "Name") ?>",
            filterable: {
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
        },
        {
            field: "meetingName",
            title: "<?php echo AxsLanguage::text("AXS_CLASS_SLASH_MEETING", "Class/Meeting") ?>",
            filterable: {
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
        },
        {
            field: "Date",
            type: "date",
            format: "{0:MM/d/yyyy}",
            filterable: {
                ui: "datepicker"
            }
        },
        {
            field: "Duration",
            title: "<?php echo AxsLanguage::text("AXS_TIME_SPEND_IN_CLASS_MEETING", "Time Spent in Class/Meeting") ?>"
        }
    ]
}

var attendanceGrid = $("#attendanceGrid").kendoGrid(kendoOptions);
var grid = attendanceGrid.data("kendoGrid");
var exportFlag = false;
attendanceGrid.data("kendoGrid").bind("excelExport", function (e) {
    if (!exportFlag) {
        e.sender.hideColumn(0);
        e.preventDefault();
        exportFlag = true;
        setTimeout(function () {
            e.sender.saveAsExcel();
        });
    } else {
        e.sender.showColumn(0);
        exportFlag = false;
    }
});
</script>