<form id="tov_remind-form" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.request&form=tovuti_loginpage'); ?>" method="post" class="tov_login-form-block-container password_reset_content" style="display:none;">
    <input type="text" class="tov_login-form-field w-input" maxlength="256" name="jform[email]" data-name="Email" placeholder="Email" id="remind_email">
    <input type="hidden" name="jform[login_page_url]" id="login_page_url" value="<?php echo base64_encode($currentLoginPageUrl); ?>" />
    <?php
        $isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
        if(!$isIOS) {
            $captcha_plugin = JFactory::getConfig()->get('captcha');
            if ($captcha_plugin != '0') {
                $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'recap_1';
                echo $captcha->display($field_id, $field_id, 'g-recaptcha');
            }
        }
        
    ?>
    <div class="tov_login-button-container">
        <div class="tov_login-horizontal-wrapper">
            <input type="submit" value="Reset Password" data-wait="Please wait..." id="reset_password_button" class="tov_login-button w-button">
        </div>
    </div>
    
    <?php echo JHtml::_('form.token'); ?>
</form>