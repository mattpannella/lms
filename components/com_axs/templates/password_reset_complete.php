<p class="tov_login-p password_reset_content"><?php echo AxsLanguage::text("COMPLETE_PASSWORD_RESET_PROCESS_MESSAGE", "To complete the password reset process, please enter a new password.") ?></p>
<form id="tov_remind-form" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.complete&form=tovuti_loginpage'); ?>" method="post" class="tov_login-form-block-container password_reset_content" style="display:none;">
    <div class="passswordMismatchMessage" style="font-weight:bold; font-style:italic;"></div>
    <input type="password" class="tov_login-form-field w-input" maxlength="256" name="jform[password1]" data-name="New Password" placeholder="<?php echo AxsLanguage::text("AXS_NEW_PASSWORD", "New Password") ?>" id="reset_password1" required="required" aria-required="true" aria-invalid="true">
    <input type="password" class="tov_login-form-field w-input" maxlength="256" name="jform[password2]" data-name="Confirm Password" placeholder="<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>" id="reset_password2" required="required" aria-required="true" aria-invalid="true">
    <input type="hidden" name="jform[login_page_url]" id="login_page_url" value="<?php echo base64_encode($currentLoginPageUrl); ?>" />
    <div class="tov_login-button-container">
        <div class="tov_login-horizontal-wrapper">
            <input type="submit" value="Reset Password" data-wait="<?php echo AxsLanguage::text("AXS_PLEASE_WAIT", "Please wait...") ?>" id="reset_password_button" class="tov_login-button w-button">
        </div>
    </div>    
    <?php echo JHtml::_('form.token'); ?>
</form>
<script>
    $('#reset_password2').keyup(function() {
        var pass2 = $(this).val();
        var pass1 = $('#reset_password1').val();
        if(pass2.length > 2) {
            if(pass2 != pass1) {
                var passswordMismatchMessage = "<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH_MESSAGE", "Passwords do not match.") ?>";
                $('.passswordMismatchMessage').text(passswordMismatchMessage);
                $('.passswordMismatchMessage').fadeIn(300);
            } else {
                $('.passswordMismatchMessage').fadeOut(300);
                $('.passswordMismatchMessage').text('');
            }
        }
    });
</script>