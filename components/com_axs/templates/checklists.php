<?php
//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);
$params = json_decode($checklist->params);
$items  = json_decode($params->items);

$activity = new stdClass();
$activity->checklist_id = $checklist->id;
$activity->user_id = $userId;

$progress = AxsChecklist::getUserChecklistProgress($userId, $checklist);

$archiveClass = '';
if ($progress == 100) {
	$archiveClass = 'archive-item archive-section-checklist';
}

if (substr($checklist_text, -1) == 's') {
	$checklist_text = substr($checklist_text, 0, -1);
}
$due_date      = '';
$dueDateParams = AxsChecklist::getChecklistDueDate($userId, $checklist);

if (!is_null($dueDateParams) && $dueDateParams->due_date) {

	$due_date      = date('m/d', strtotime($dueDateParams->due_date));
}
?>

<!-- START TASK LIST CONTAINER -->
<div class="tovuti-tasklist-list <?php echo $archiveClass; ?>">
	<button class="btn btn-light task-collapse" data-toggle="collapse" style="min-width:48px;min-height:48px;display:flex;align-items: center; justify-content:center;" aria-label="Toggle Checklist" type="button" title="Checklist" data-target="#tovuti-tasklist-list-body-collapse-<?php echo $checklist->id; ?>" aria-expanded="false">
		<i class="fa-solid fa-chevron-down" aria-hidden="true" focusable="false"></i><span class="sr-only">Toggle Checklists</span>
	</button>
	<span class="tov-task-list-header-wrap">
		<span class="tasklist-title"><?php echo $checklist_text; ?></span>
		<?php echo $checklist->title; ?>
		<?php if ($due_date) { ?>
			<span class="checklist-has-due-date"><?php echo AxsLanguage::text("AXS_DUE", "Due") ?> <?php echo $due_date; ?></span>
		<?php } ?>
	</span>
	<!-- START TASK LIST BODY -->
	<div class="tovuti-tasklist-list-body card card-body collapse in show" id="tovuti-tasklist-list-body-collapse-<?php echo $checklist->id; ?>">
		<div class="progress">
			<div class="progress-bar progress-bar-success" aria-label="<?php echo $progress; ?>% <?php echo AxsLanguage::text("AXS_COMPLETE", "Complete") ?>" role="progressbar" aria-valuenow="<?php echo $progress; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress; ?>%;">
				<span class="sr-only" style="color:#000000 !important;">
					<span class="progress-amount"><?php echo $progress; ?></span>% <?php echo AxsLanguage::text("AXS_COMPLETE", "Complete") ?>
				</span>
			</div>
		</div>
		<!-- START TASK LIST -->
		<ul class="tovuti-tasklist-list-item">
			<?php

			$isCompleteMap = AxsChecklist::checkIfExistsMap($items,$userId);
			foreach ($items as $index => $item) {

				$filter = $filters->{$item->type};
				$item->checklist_id = $checklist->id;
				$item->user_id      = $userId;
				$item->is_admin     = $is_admin;

				$isComplete         = isset($isCompleteMap[$item->id]);
				$encryptedParams    = base64_encode(AxsEncryption::encrypt($item, $key));

				if ($isComplete) {
					$completeClass = 'task-complete';
				} else {
					$completeClass = 'task-incomplete';
				}

				$tooltip = '';
				$check = 0;
				$toolTipPlacement = 'top';

				if ($index == 0) {
					$toolTipPlacement = 'bottom';
				}

				if ($item->completion_type == 'admin') {
					$tooltip = 'data-toggle="tooltip" data-placement="' . $toolTipPlacement . '" title="' . AxsLanguage::text("AXS_ONLY_MARKED_COMPLETE_ADMIN", "This can only be marked complete by an Administrator") . '"';
				}

				if ($item->completion_type == 'auto') {
					$tooltip = 'data-toggle="tooltip" data-placement="' . $toolTipPlacement . '" title="' . AxsLanguage::text("AXS_CAN_ONLY_MARK_COMPLETE_FINISHED", "This can only be marked complete by finishing the task listed") . '"';
				}

				if ($item->completion_type == 'user') {
					$tooltip = 'data-toggle="tooltip" data-placement="' . $toolTipPlacement . '" title="' . AxsLanguage::text("AXS_MANUALLY_MARK_COMPLETE", "You can manually mark this item completed") . '"';
					$check = 1;
				}

				if ($is_admin) {
					$check = 1;
				}

				if (!$params->show_tooltip) {
					$tooltip = '';
				}
				$item->item_due_dynamic_usergroup = $checklist->usergroup;
				$item_due_date = AxsChecklist::getChecklistItemDueDate($userId, $item);

				if ($item_due_date) {
					$today    = date_create("NOW");
					$due_date = date_create($item_due_date);
					$diff = date_diff($due_date, $today);

					if ($diff->invert && $diff->days <= 7) {
						$filter = 'due-soon';
					}

					$item_due_date = date('m/d', strtotime($item_due_date));
				} else {

					$item_due_date = '';
				}

				$hasLinkClass = '';
				if ($item->link) {
					$hasLinkClass = 'task-has-link';
				}
			?>
				<li class="item-<?php echo $filter; ?> ch-item">
					<div class="<?php echo $completeClass; ?> <?php echo $hasLinkClass; ?> checklist-item item-<?php echo $item->id; ?>" data-item="<?php echo $encryptedParams; ?>" <?php echo $tooltip; ?> data-check="<?php echo $check; ?>">
						<?php if ($item->icon) { ?>
							<span class="task-has-icon <?php echo $item->icon; ?>"></span>
						<?php } ?>
						<?php echo $item->title; ?>
						<?php if ($item_due_date) { ?>
							<span class="task-has-due-date">
								<?php
								echo AxsLanguage::text("AXS_DUE", "Due") . ' ' . $item_due_date;
								?>
							</span>
						<?php } ?>
						<?php if ($item->instructions) { ?>
							<span class="task-instructions"><?php echo $item->instructions; ?></span>
						<?php } ?>
					</div>
					<?php if ($item->link) { ?>
						<a class="task-link" aria-label="Go to <?php echo $item->title; ?>" href="<?php echo $item->link; ?>" target="_blank">
							<i class="fa-solid fa-arrow-right"></i>
						</a>
					<?php } ?>
				</li>
			<?php } ?>
		</ul>
		<!-- END TASK LIST -->
	</div>
	<!-- END TASK LIST BODY -->
</div>
<!-- END TASK LIST CONTAINER -->