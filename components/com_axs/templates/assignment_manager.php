<?php

defined('_JEXEC') or die;

$selectorType         = 'courses';
$selectorName         = 'courses';
$selectorValue        = "";
$selectorValueEncoded = '';
if($selectorValue) {
    $selectorValueEncoded = '&val='.base64_encode($selectorValue);
}
if($this->element['id']) {
    $id = $this->element['id'];
} else {
    $id = rand(0,2000);
}

    require_once "administrator/components/com_axs/templates/selector.php";

?>

