<form id="tov_login-form" action="#" method="post" aria-label="Login Form" data-name="<?php echo AxsLanguage::text("AXS_LOGIN_FORM", "Login Form") ?>" class="login_form_content tov_login-form-block-container">
    <input type="text" class="tov_login-form-field w-input" maxlength="256" name="username" data-name="<?php echo AxsLanguage::text("AXS_USERNAME_EMAIL", "Username or Email") ?>" placeholder="<?php echo AxsLanguage::text("AXS_USERNAME_EMAIL", "Username or Email") ?>" id="login_username">
    <input type="password" class="tov_login-form-field w-input" maxlength="256" name="password" data-name="<?php echo AxsLanguage::text("AXS_PASSWORD", "Password") ?>" placeholder="<?php echo AxsLanguage::text("AXS_PASSWORD", "Password") ?>" id="login_password" required="">
    <?php if (count($twofactormethods) > 1) { ?>
        <input type="<?php echo $sercretKeyInputType; ?>" class="tov_login-form-field w-input" maxlength="256" name="secretkey" data-name="secretkey" placeholder="<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key") ?>" style="display:none;" id="login_secretkey">
    <?php }	?>
    <div class="tov_login-button-container">
        <div class="tov_login-horizontal-wrapper">
            <input type="submit" value="<?php echo AxsLanguage::text("AXS_SUBMIT", "Submit") ?>" data-wait="<?php echo AxsLanguage::text("AXS_PLEASE_WAIT", "Please wait...") ?>" id="login_button" class="tov_login-button w-button">
        </div>
    </div>
    <input type="hidden" name="return" id="login_return" value="<?php echo base64_encode($return); ?>" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script>
    $("#tov_login-form").submit( function(e) {
        e.preventDefault();
        $('#login_button').attr('disabled','true');
        //var data = $(this).serialize();
        var data = {
            username: $("#login_username").val(),
            password: $("#login_password").val(),
            secretkey: $("#login_secretkey").val(),
            return: $("#login_return").val()
        }

        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }

        $.ajax({
            url: '/index.php?option=com_axs&task=popupmodule.loginAuthenticate&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                var response = JSON.parse(result);
                if(response.secretKeyVerification) {
                        $('#login_button').removeAttr('disabled');
                        $(".w-form-fail").html(response.message);
                        if($(".w-form-fail:hidden")) {
                            $(".w-form-fail").slideToggle();
                        }
                        if($("#login_secretkey").is(':hidden')) {
                            $('#login_secretkey').slideToggle();
                        }
                        return;
                    }
                if (response.status == "success") {
                    $.ajax({
                        url: '/index.php?option=com_users&task=user.login',
                        type: 'post',
                        data: data
                    }).done( function() {
                        window.location = response.url;
                    });
                } else {
                    $('#login_button').removeAttr('disabled');
                    $(".w-form-fail").html(response.message);
                    if($(".w-form-fail:hidden")) {
                        $(".w-form-fail").slideToggle();
                    }
                }
        });
    });
</script>