<?php

    for($i = $start; $i <= $count; $i++) {
        if(!$teamMembers[$i]) {
            break;
        }
        $memberID = $teamMembers[$i]->id;
        $user = CFactory::getUser($memberID);
        AxsChecklist::updateAllChecklistsForUser($memberID);
        $userChecklists = AxsChecklist::getUserChecklists($memberID);
        $checklistTotal = count($userChecklists);
        $checklistCompleted = 0;
        foreach($userChecklists as $checklist) {
            $progress = AxsChecklist::getUserChecklistProgress($memberID,$checklist);
            if($progress == 100) {
                $checklistCompleted++;
            }
        }
        $checklistPercentComplete = round( ($checklistCompleted / $checklistTotal) * 100);

        $userCourses = AxsTranscripts::getTranscriptCourses($memberID);
        $courseTotal = count($userCourses);
        $courseCompleted = 0;
        $coursePercentComplete = 0;
        foreach($userCourses as $course) {
            if($course->percentComplete == 100) {
                $courseCompleted++;
            }
        }
        $coursePercentComplete = $courseTotal > 0 ? round( ($courseCompleted / $courseTotal) * 100) : 0;
        $encryptedTranscriptLink       = AxsLMS::getEncryptedTranscriptLink($memberID);
        $encryptedLearnerDashboardLink = AxsLMS::getEncryptedLearnerDashboardLink($memberID, $encryptedParams->learnerDashboard_id);

        $reportParams->user_id = $memberID;

        $reportParams->report = 'attendance';
        $attendanceEncryptedParams = base64_encode(AxsEncryption::encrypt($reportParams, $key));

        $reportParams->report = 'activity';
        $activityeEncryptedParams = base64_encode(AxsEncryption::encrypt($reportParams, $key));



?>
    <div class="tld-team-member-container member-list" data-name="<?php echo $user->name; ?>" data-courses="<?php echo $courseCompleted; ?>" data-checklists="<?php echo $checklistCompleted; ?>">
        <div class="tld-team-user">
                <img
                    class="tld-team-user-avatar"
                    src="<?php echo $user->getAvatar(); ?>"
                >
            <div class="tld-team-username"><?php echo $user->name; ?></div>
        </div>

        <div class="tld-user-progress-box">
            <?php if($dashboardParams->show_checklists_completed) { ?>
            <div class="tld-user-progress-wrapper">
                <div><?php echo sprintf(AxsLanguage::text("AXS_CHECKLISTS_OF_COMPLETED", "Checklists: %s of %s completed"), $checklistCompleted, $checklistTotal); ?></div>
                <div class="tld-progress-bar">
                    <div class="tld-progress-bar-scale" style="width: <?php echo $checklistPercentComplete; ?>%;"></div>
                </div>
            </div>
            <?php } ?>
            <?php if($dashboardParams->show_courses_completed) { ?>
            <div class="tld-user-progress-wrapper">
                <div><?php echo sprintf(AxsLanguage::text("AXS_COURSE_PROGRESS_OF_COMPLETED", "Course Progress: %s of %s completed"), $courseCompleted, $courseTotal); ?></div>
                <div class="tld-progress-bar">
                    <div class="tld-progress-bar-scale" style="width: <?php echo $coursePercentComplete; ?>%;"></div>
                </div>
            </div>
            <?php } ?>
        </div>
        <div class="tld-team-user-options">
            <?php if($dashboardParams->show_learners_dashboard) { ?>
            <a href="<?php echo $encryptedLearnerDashboardLink ?>" target="_blank" class="tld-team-user-options-item w-inline-block">
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_dashboard.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("AXS_DASHBOARD", "Dashboard") ?></p>
            </a>
            <?php } ?>
            <?php if($dashboardParams->show_attendance) { ?>
            <a
                href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=report&report=<?php echo $attendanceEncryptedParams; ?>"
                class="jcepopup noicon tovuti_popup members_popup tld-team-user-options-item w-inline-block"
                data-mediabox="1"
                data-mediabox-width="1500" data-mediabox-height="725"
                type="iframe"
            >
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_attendance.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("AXS_ATTENDANCE", "Attendance") ?></p>
            </a>
            <?php } ?>
            <?php if($dashboardParams->show_activity_report) { ?>
            <a
                href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=report&report=<?php echo $activityeEncryptedParams; ?>"
                class="jcepopup noicon tovuti_popup members_popup tld-team-user-options-item w-inline-block"
                data-mediabox="1"
                type="iframe"
            >
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_activity.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("AXS_ACTIVITY", "Activity") ?></p>
            </a>
            <?php } ?>
            <?php if($dashboardParams->show_transcript) { ?>
            <a href="<?php echo $encryptedTranscriptLink; ?>" target="_blank" class="tld-team-user-options-item w-inline-block">
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_transcript.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("AXS_TRANSCRIPT", "Transcript") ?></p>
            </a>
            <?php } ?>
            <?php if($dashboardParams->show_message_module || !isset($dashboardParams->show_message_module)) { ?>
            <a href="javascript:" onclick="joms.api.pmSend('<?php echo $memberID; ?>');" class="tld-team-user-options-item w-inline-block">
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_message.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("MESSAGE", "Message") ?></p>
            </a>
            <?php } ?>

            <?php
                $showEditIcon = $teamParams->allow_adding_users ||
                    $teamParams->allow_removing_users_from_system ||
                    $teamParams->allow_removing_users_from_team ||
                    $teamParams->allow_managing_users_groups;

                if($showEditIcon) {
                    $actionParams->action = 'edit';
                    $actionParams->edit_user_id = $memberID;
                    $encrytpedEditUser = base64_encode(AxsEncryption::encrypt($actionParams, $key));
            ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=user_manager&action=<?php echo $encrytpedEditUser; ?>"
                    class="tld-team-user-options-item w-inline-block jcepopup noicon"
                    data-mediabox="1"
                    data-mediabox-width="800"
                    data-mediabox-height="900"
                    type="iframe"
                >
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/cog.svg" alt="" class="tld-team-user-option-icon">
                    <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("JACTION_EDIT", "Edit") ?></p>
                </a>
            <?php } ?>
            <!-- <a href="#" class="tld-team-user-options-item w-inline-block">
                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_assign.svg" alt="" class="tld-team-user-option-icon">
                <p class="tld-team-user-option-text">Assign</p>
            </a> -->
        </div>
        <div data-hover="" data-delay="0" class="tld-user-dropdown w-dropdown">
            <div class="tld-user-dd-toggle w-dropdown-toggle">
                <div><?php echo AxsLanguage::text("AXS_USER_OPTIONS", "User Options") ?></div>
                <div class="tld-selector-icon">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_dropdown.svg" alt="" class="tld-dd-icon">
                </div>
            </div>
            <nav class="tld-user-dd-list tld-mobile-options">

                <?php if($dashboardParams->show_learners_dashboard) { ?>
                <a href="<?php echo $encryptedLearnerDashboardLink; ?>" target="_blank" class="tld-dd-list-item w-inline-block">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_dashboard.svg" alt="" class="tld-dd-user-image">
                    <div><?php echo AxsLanguage::text("AXS_DASHBOARD", "Dashboard") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_attendance) { ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=report&report=<?php echo $attendanceEncryptedParams; ?>"
                    class="jcepopup noicon tovuti_popup members_popup tld-dd-list-item w-inline-block"
                    data-mediabox="1"
                    type="iframe">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_attendance.svg" alt="" class="tld-dd-user-image">
                    <div><?php echo AxsLanguage::text("AXS_ATTENDANCE", "Attendance") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_activity_report) { ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=report&report=<?php echo $activityeEncryptedParams; ?>"
                    class="jcepopup noicon tovuti_popup members_popup tld-dd-list-item w-inline-block"
                    data-mediabox="1"
                    type="iframe"
                >
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_activity.svg" alt="" class="tld-dd-user-image">
                    <div><?php echo AxsLanguage::text("AXS_ACTIVITY", "Activity") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_transcript) { ?>
                <a href="<?php echo $encryptedTranscriptLink; ?>" target="_blank" class="tld-dd-list-item w-inline-block">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_transcript.svg" alt="" class="tld-dd-user-image">
                    <div><?php echo AxsLanguage::text("AXS_TRANSCRIPT", "Transcript") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_message_module || !isset($dashboardParams->show_message_module)) { ?>
                <a
                    href="javascript:" onclick="joms.api.pmSend('<?php echo $memberID; ?>');" class="tld-dd-list-item w-inline-block">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_message.svg" alt="" class="tld-dd-user-image">
                    <div><?php echo AxsLanguage::text("MESSAGE", "Message") ?></div>
                </a>
                <?php } ?>
                <?php if($showEditIcon) { ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=user_manager&action=<?php echo $encrytpedEditUser; ?>"
                    class=" tld-dd-list-item jcepopup noicon"
                    data-mediabox="1"
                    data-mediabox-width="800"
                    data-mediabox-height="800"
                    type="iframe"
                >
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/cog.svg" alt=""  class="tld-dd-user-image"">
                    <div><?php echo AxsLanguage::text("JACTION_EDIT", "Edit") ?></div>
                </a>
            <?php } ?>
                <!-- <a href="#" class="tld-dd-list-item w-inline-block">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_assign.svg" alt="" class="tld-dd-user-image">
                    <div>Assign</div>
                </a> -->
            </nav>
        </div>
    </div>
<?php } ?>