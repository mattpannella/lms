<?php

$totp = new FOFEncryptTotp(30, 6, 10);

if ($otpConfig->method === 'totp') {
	// This method is already activated. Reuse the same Security key.
	$secret = $otpConfig->config['code'];
	$alreadySetup = true;
} else {
	// This methods is not activated yet. Create a new Security key.
	$secret = $totp->generateSecret();
}

$key = AxsKeys::getKey('lms');
$securityKey = base64_encode(AxsEncryption::encrypt($secret, $key));
// These are used by Google Authenticator to tell accounts apart
$username = $user->username;
$hostname = JUri::getInstance()->getHost();
$brand = AxsBrands::getBrand();

if($brand->site_title) {
	$appName = $brand->site_title;
} else {
	$appName = $hostname;
}

// This is the URL to the QR code for Google Authenticator
$url = $totp->getUrl($username, $appName, $secret);

?>

<div class="header-fullwidth" style="
	background-image: url(/components/com_axs/assets/images/2fa-bg.jpg);
	background-size:cover;
	background-position:center;
	height: 300px;
	position: relative;">
	<div class="slide-overlay">
		<h2>Setup Your Two-factor Authentication</h2>
	</div>
</div>
<div class="container tfa-wrapper">
	<div class="row tfa-instructions-wrapper">
		<div class="col-lg-6 text-center">
			<img src="<?php echo $url; ?>" alt="">
			<div class="col-lg-12 text-center secret-key-code">
				<?php echo $secret; ?>
			</div>
		</div>
		<div class="col-lg-6 tfa-text" style="padding-top: 30px;">
			<div class="tfa-instructions">
				<ol class="tfa-list">
					<li>Download a supported Two-factor Authenticator App from the Apple Store or Google Play</li>
					<li>Open the Authenticator App and create new account</li>
					<li>Scan the QR Code on this page</li>
					<li>Enter the Security Code in the field below and click Verify Code!</li>
				</ol>
			</div>
			<div class="tfa-fields" <?php if($alreadySetup) { echo 'style="display: none;"'; } ?>>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="fa fa-lock"></i> Security Code</span>
					<input class="form-control" id="tfa-key" type="text" placeholder="Enter Security Code">
				</div>
				<input type="hidden" id="security-key" name="security-key" value="<?php echo $securityKey; ?>" />
				<div class="tfa-backups"></div>
				<button class="btn btn-primary tfa-button">Verify Code</button>
			</div>
			<div class="tfa-success" <?php if(!$alreadySetup) { echo 'style="display: none;"'; } ?>>
				<i class="fa fa-check" style="font-size: 30px;"></i> Your Two-factor Athentication is now Active!
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 text-center">
			<img src="/components/com_axs/assets/images/tovuti-lms-two-factor-authentication-apps.png" style="max-width: 600px; margin-top:40px;" alt="">
		</div>
	</div>
</div>

<script src="/components/com_axs/assets/js/otp.js"></script>