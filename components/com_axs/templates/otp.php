<?php

$totp = new FOFEncryptTotp(30, 6, 10);

if ($otpConfig->method === 'totp') {
	// This method is already activated. Reuse the same Security key.
	$secret = $otpConfig->config['code'];
	$alreadySetup = true;
} else {
	// This methods is not activated yet. Create a new Security key.
	$secret = $totp->generateSecret();
}

$key = AxsKeys::getKey('lms');
$securityKey = base64_encode(AxsEncryption::encrypt($secret, $key));
// These are used by Google Authenticator to tell accounts apart
$username = $user->username;
$hostname = JUri::getInstance()->getHost();
$brand = AxsBrands::getBrand();

if($brand->site_title) {
	$appName = $brand->site_title;
} else {
	$appName = $hostname;
}

// This is the URL to the QR code for Google Authenticator
$url = $totp->getUrl($username, $appName, $secret);

?>
<div class="header-fullwidth" style="
	background-image: url(/components/com_axs/assets/images/security_bg_optimized.jpg);
	background-size:cover;
	background-position:center;
	height: 300px;
	position: relative;">
	<div class="slide-overlay">
	<h2><?php echo AxsLanguage::text("AXS_SECURITY", "Security"); ?></h2>
	</div>
</div>
<main>
	<section class="moyo-content-body">
		<div class="moyo-content-title">
			<h2>2FA</h2>
			<?php if($alreadySetup) { ?>
				<span class="badge rounded-pill text-bg-success moyo-2fa-badge"><i class="fa-light fa-check"></i>
				<?php echo AxsLanguage::text("AXS_SECURITY_ENABLED", "Enabled"); ?>
				</span>
			<?php } else { ?>
				<span class="badge rounded-pill text-bg-danger moyo-2fa-badge"><i class="fa-light fa-xmark"></i>
				<?php echo AxsLanguage::text("AXS_SECURITY_DISABLED", "Disabled"); ?>
				</span>
			<?php } ?>
		</div>
		<div class="moyo-content-wrapper">
			<section class="moyo-content-2fa">
				<div class="card text-center">
					<div class="moyo-card-2fa card-body">
						<h5 class="card-title">
							<?php echo AxsLanguage::text("AXS_SECURITY_AUTHENTICATON", "Authentication Applications"); ?>
						</h5>
						<a href="#" class="btn btn-dark moyo-2fa-modal-button" data-toggle="modal" data-target="#securityModal">
							<i class="fa-light fa-plus-large"></i>
						</a>
						<p class="card-text">
							<?php echo AxsLanguage::text("AXS_SECURITY_CONNECT_APPS", "We connect with these Authentication Apps"); ?>
						</p>
						<div class="moyo-auth-apps">
							<div class="moyo-auth-app">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/2fas_logomark.svg"/>
								</div>
								2FAS
							</div>
							<div class="moyo-auth-app">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/google_authenticator.svg"/>
								</div>
								Google
							</div>
							<div class="moyo-auth-app">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/lastPass_authenticator.svg"/>
								</div>
								LastPass
							</div>
							<div class="moyo-auth-app">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/microsoft_authenticator.svg"/>
								</div>
								Microsoft
							</div>
							<div class="moyo-auth-app">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/twilio_appmark.svg"/>
								</div>
								Authy
							</div>
						</div>
						<div class="moyo-auth-apps">
							<p class="card-text">
							<?php echo AxsLanguage::text("AXS_SECURITY_AVAILABLE_APPS", "These apps are available on these platforms"); ?>:
							</p>
							<div class="moyo-auth-app inline app-store">
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/google-play-badge.png" height="40px"/>
								</div>
								<div class="moyo-auth-logo">
									<img src="/components/com_axs/assets/images/auth-logos/apple-app-store.svg" height="40px"/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	</section>
</main>

<div class="modal modal-2fa fade" tabindex="-1" id="securityModal" aria-labelledby="securityModal" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content text-center">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="moyo-2fa-setup"  <?php if($alreadySetup) { echo 'style="display: none;"'; } ?>>
					<div class="moyo-2fa-progress">
						<div class="moyo-2fa-incomplete"><i class="fa-solid fa-xmark"></i></div>
						<hr class="moyo-2fa-divider">
						<div class="moyo-2fa-complete disabled"><i class="fa-solid fa-check"></i></div>
					</div>
					<form id="moyo-2fa-auth">
						<div class="moyo-2fa-form mb-3">
							<label for="security-key" class="form-label">
							<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key"); ?>
							</label>
							<input type="text" class="form-control" id="tfa-key" name="tfa-key" aria-describedby="security key"/>
							<input type="hidden" id="security-key" name="security-key" value="<?php echo $securityKey; ?>" />
							<div id="auth-code" class="form-text moyo-copy-code"><?php echo $secret; ?></div>
						</div>
						<button type="submit" class="btn btn-primary moyo-2fa-button"><i class="fa-light fa-lock"></i>
							<?php echo AxsLanguage::text("AXS_SECURITY_AUTHENITICATE", "Authenticate"); ?>
						</button>
					</form>
					<div class="moyo-modal-2fa-custom">
						<div class="moyo-modal-2fa-custom-left">
							<img src="<?php echo $url; ?>">
						</div>
						<div class="moyo-modal-2fa-custom-right">
							<ol>
								<li>
									<?php echo AxsLanguage::text("AXS_SECURITY_2FA_STEP_1", "Download a supported Two-factor Authenticator App from the Apple App Store or the Google Play Store"); ?>
								</li>
								<li>
									<?php echo AxsLanguage::text("AXS_SECURITY_2FA_STEP_2", "Open the Authenticator App and create a new account"); ?>
								</li>
								<li>
									<?php echo AxsLanguage::text("AXS_SECURITY_2FA_STEP_3", "Scan the QR code on this page"); ?>
								</li>
								<li>
									<?php echo AxsLanguage::text("AXS_SECURITY_2FA_STEP_4", "Enter the Security Code in the field below and click 'Verify Code'"); ?>
								</li>
							</ol>
						</div>
					</div>
				</div>
				<div class="moyo-2fa-setup-complete"  <?php if(!$alreadySetup) { echo 'style="display: none;"'; } ?>>
					<div class="moyo-2fa-progress">
						<div class="moyo-2fa-incomplete disabled"><i class="fa-solid fa-xmark"></i></div>
						<hr class="moyo-2fa-divider">
						<div class="moyo-2fa-complete"><i class="fa-solid fa-check"></i></div>
					</div>
					<div class="moyo-2fa-form mb-3">
						<label for="setup-complete" class="form-label">
							<?php echo AxsLanguage::text("AXS_SECURITY_AUTHENITICATED", "Authenticated"); ?>
						</label>
						<div id="setup-complete" class="form-text">
							<?php echo AxsLanguage::text("AXS_SECURITY_2FA_SETUP", "Congratulations, you're authenticated with 2FA"); ?>
						</div>
					</div>
				</div>
			</div>
			<img src="/components/com_axs/assets/images/tovuti-lms-two-factor-authentication-apps.png" style="max-width: 600px; margin-top:40px;" alt="">
		</div>
	</div>
</div>
<script>
	const task = "saveOtpKey";
	const enabled = "<?php echo AxsLanguage::text("AXS_SECURITY_ENABLED", "Enabled"); ?>";
</script>
<script src="/components/com_axs/assets/js/otp.js"></script>
