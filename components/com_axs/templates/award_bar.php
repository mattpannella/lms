<?php
$language = AxsLanguage::getCurrentLanguage()->get('tag');
$badges_text = AxsLanguage::text("AXS_BADGES", "Badges");
$certificates_text = AxsLanguage::text("AXS_CERTIFICATES", "Certificates");
$leaderboard_text = AxsLanguage::text("AXS_LEADERBOARD", "Leaderboard");
$teamList = null;
if ($params->leaderboard_team_only) {
    $teamListArray = $params->teams->allTeamsList;
    if ($teamListArray) {
        $teamList = implode(',', $teamListArray);
    } else {
        $teamList = $userId;
    }
}
if ($params->show_badges == 'custom' && $params->badges_text) {
    $badges_text = $params->badges_text;
}

if ($params->show_certificates == 'custom' && $params->certificates_text) {
    $certificates_text = $params->certificates_text;
}

if ($params->show_leaderboard == 'custom' && $params->leaderboard_text) {
    $leaderboard_text = $params->leaderboard_text;
}

$userPoints = AxsLearnerDashboard::getLeaderBoardPointsForUser($userId);

?>
<section class="tovuti-dashboard-belt">
    <?php if ($params->show_badges) {
        if ($params->show_certificates && $params->show_leaderboard) {
            $beltClass = 'col-lg-4';
        } elseif ($params->show_certificates || $params->show_leaderboard) {
            $beltClass = 'col-lg-6';
        } else {
            $beltClass = 'col-lg-12';
        }
    ?>
        <div aria-label="Badges You've Earned" class="<?php echo $beltClass; ?> tovuti-dashboard-belt-column">
            <div class="belt-badges" id="oc-badges">
                <span class="award-bar-title" id="oc-badges-title"><?php echo $badges_text; ?></span>
                <div id="oc-badges-body">
                    <?php foreach (array_slice($badges, 0, 10) as $badge) {
                        $badgeParams = json_decode($badge->params);
                        $badge_image = $badgeParams->badge_image;
                        $badge_tooltip = $badge->title;
                        if ($badgeParams->badge_hover) {
                            $badge_tooltip = $badgeParams->badge_hover;
                        }
                        if (isset($language) && !empty($badgeParams->language_overrides->{$language})) {
                            $overrideParams = $badgeParams->language_overrides->{$language};
                            $badge_image = $overrideParams->badge_image;
                            $badge_tooltip = $overrideParams->badge_hover;
                        }
                    ?>
                        <img src="/<?php echo $badge_image; ?>" alt='<?php echo $badge_tooltip; ?>' class="learner-dashboard-badge" data-toggle="tooltip" data-placement="top" title="<?php echo $badge_tooltip; ?>">
                    <?php } ?>
                    <img src='/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/more.png' class="trigger-offcanvas" alt="" aria-label="View all Badges" data-content="oc-badges">
                </div>
            </div>
        </div>
    <?php } ?>


    <?php if ($params->show_certificates) {
        if ($params->show_badges && $params->show_leaderboard) {
            $beltClass = 'col-lg-4';
        } elseif ($params->show_badges || $params->show_leaderboard) {
            $beltClass = 'col-lg-6';
        } else {
            $beltClass = 'col-lg-12';
        }
    ?>
        <div aria-label="Certificates You've Earned" class="<?php echo $beltClass; ?> tovuti-dashboard-belt-column">
            <div class="belt-badges" id="oc-certificates">
                <span class="award-bar-title" id="oc-certificates-title"><?php echo $certificates_text; ?></span>
                <div id="oc-certificates-body">
                    <?php $c = 0;
                    foreach ($certificates as $award) {
                        $awardParams = json_decode($award->params);
                        if (isset($language) && isset($awardParams->language_overrides->{$language})) {
                            $awardParams = $awardParams->language_overrides->{$language};
                        }

                        $cert = AxsLearnerDashboard::getCertificateById($awardParams->certificate);
                        $certParams = json_decode($cert->params);
                        $certObj = new stdClass();
                        $certObj->certificate_id = $awardParams->certificate;
                        $certObj->award_id = $award->id;
                        $certObj->user_id = $userId;
                        $certObj->language = $language;
                        $key = AxsKeys::getKey('lms');
                        $encryptedCertificate = base64_encode(AxsEncryption::encrypt($certObj, $key));
                        $c++;

                        $tooltip = $award->title;
                        if ($awardParams->badge_hover) {
                            $tooltip = $awardParams->badge_hover;
                        }

                        $certificate_thumbnail = $certParams->certificate_thumbnail;
                        if (!$certificate_thumbnail) {
                            $certificate_thumbnail = "images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/learning-management-system/defaultCertThumbnail.png";
                        }

                    ?>
                        <a href="/certificates?tmpl=component&cert=<?php echo $encryptedCertificate; ?>" class="jcepopup noicon tovuti_popup" data-mediabox="1" orginal="certificate_<?php echo $c; ?>" id="certificate_<?php echo $c; ?>" type="iframe" title="<?php echo $tooltip; ?>" data-mediabox-width="850" data-mediabox-height="725">
                            <img src='/<?php echo $certParams->certificate_thumbnail; ?>' alt='<?php echo $tooltip; ?>' class="learner-dashboard-badge <?php echo $hideAwardClass; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $tooltip; ?>">
                        </a>
                    <?php } ?>
                    <img src="/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/more.png" alt="" aria-label="View all Certificates" data-content="oc-certificates" class="trigger-offcanvas">
                </div>
            </div>
        </div>
    <?php } ?>
    <?php if ($params->show_leaderboard) {
        if ($params->show_badges && $params->show_certificates) {
            $beltClass = 'col-lg-4';
        } elseif ($params->show_badges || $params->show_certificates) {
            $beltClass = 'col-lg-6';
        } else {
            $beltClass = 'col-lg-12';
        }
    ?>
        <div class="<?php echo $beltClass; ?> tovuti-dashboard-belt-column">

            <?php if ($params->show_leaderboard_user_points) : ?>
                <div style="position: absolute; top: -10px; left: 0%;">
                    <span class="tovuti-user-points top-arrow">
                        <span><?php echo AxsLanguage::text("AXS_YOUR_POINTS", "Your Points") ?>: <?php echo $userPoints; ?></span>
                    </span>
                </div>
            <?php endif; ?>

            <div class="belt-badges" id="oc-leaders">
                <span class="award-bar-title" id="oc-leaders-title"><?php echo $leaderboard_text; ?></span>
                <div id="oc-leaders-body">
                    <?php
                    $l = 0;
                    $leaderboardAmount = 10;
                    $leaderboardList = AxsLearnerDashboard::getLeaderboardList($leaderboardAmount, $teamList);
                    foreach ($leaderboardList as $leaderboardUser) {
                        $l++;
                        $hideLeaderboardUserClass = '';
                        $cUser = CFactory::getUser($leaderboardUser->user_id);
                        if ($cUser->id) {
                            $rank = AxsLearnerDashboard::ordinalNumber($l);
                    ?>
                            <div class="leaderboard-img-wrap <?php echo $hideLeaderboardUserClass; ?> learner-dashboard-badge">
                                <img class="student-picture student-picture-<?php echo $cUser->id; ?> student-leaderboard-img" src="<?php echo $cUser->getAvatar(); ?>" alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>" data-author="<?php echo $cUser->id; ?>">
                                <div class="leaderboard-rank">
                                    <?php echo $rank; ?>
                                </div>
                                <?php if ($params->show_leaderboard_user_points) : ?>
                                    <div class="leaderboard-user-points">
                                        <?php echo $leaderboardUser->total; ?>pts
                                    </div>
                                <?php endif; ?>
                            </div>
                    <?php
                        }
                    }
                    ?>
                    <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/more.png" data-content="oc-leaders" class="trigger-offcanvas">
                </div>
            </div>
        </div>
    <?php } ?>
    <div class="clearfix"></div>
</section>
<div class="offcanvas-view"></div>

<script>
    function adjustBadges() {
        $('.belt-badges').each(function() {
            var minWidth = 286;
            var width = $(this).width();
            if (width < minWidth) {
                width = minWidth;
            }
            var max = Math.floor(width / 75) - 1;
            var badges = $(this).find('.learner-dashboard-badge');
            var i = 1;
            badges.hide();
            badges.each(function() {
                if (i <= max) {
                    $(this).show();
                }
                i++;
            });
        });
    }
    $(window).resize(adjustBadges);
    adjustBadges();
</script>