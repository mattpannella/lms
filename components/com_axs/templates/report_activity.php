<?php
echo AxsLoader::loadKendo();

$report = $this->getActivityReport();
$params = json_decode($report->params);
$params->user_selection_type = 'selected';

if($reportParams->user_id) {
    $params->userlist = $reportParams->user_id;
} else {
    $params->userlist = $this->getUserList();
}
$params->users = $params->userlist;
$params->usergroups_selection_type = 'all';

$start_date  = $report->start_date;
$end_date  = $report->end_date;
$rangeText = '';
if($start_date > 0) {
    $start = date('m/d/Y',strtotime($start_date));
    $rangeText .= AxsLanguage::text("JLIB_HTML_START", "Start") . ": " . $start;
}
if($end_date > 0) {
    $end = date('m/d/Y',strtotime($end_date));
    $rangeText .= AxsLanguage::text("JLIB_HTML_END", "End") . ": " . $end;
}
$courseList = $params->courses;
$dataPoints = explode(',',$params->data_points);
$theme = $params->theme;
$defaultDuration = 60;
if($params->default_duration) {
    $defaultDuration = $params->default_duration;
}
$data = array();

if(in_array('Active Time on Site',$dataPoints)) {
    $newSiteVisitDuration = AxsActivityData::getActivityNewSiteVisitDuration($params, $start_date, $end_date);
    $data = array_merge($data,$newSiteVisitDuration);
}
if(in_array('Site Visit Duration',$dataPoints)) {
    $siteVisitDuration = AxsActivityData::getActivitySiteVisitDuration($params,$start_date,$end_date,$defaultDuration);
    $data = array_merge($data,$siteVisitDuration);
}
if(in_array('Course Started',$dataPoints)) {
    $courseStarted = AxsActivityData::getActivityCourseStarted($params,$start_date,$end_date);
    $data = array_merge($data,$courseStarted);
}
if(in_array('Course Completed',$dataPoints)) {
    $courseCompleted = AxsActivityData::getActivityCourseCompleted($params,$start_date,$end_date);
    $data = array_merge($data,$courseCompleted);
}
if(in_array('Lesson Started',$dataPoints)) {
    $lessonStarted = AxsActivityData::getActivityLessonStarted($params,$start_date,$end_date);
    $data = array_merge($data,$lessonStarted);
}
if(in_array('Lesson Completed',$dataPoints)) {
    $lessonCompleted = AxsActivityData::getActivityLessonCompleted($params,$start_date,$end_date);
    $data = array_merge($data,$lessonCompleted);
}
if(in_array('Certificate Awarded',$dataPoints)) {
    $certificateAwarded = AxsActivityData::getActivityCertificateAwarded($params,$start_date,$end_date);
    $data = array_merge($data,$certificateAwarded);
}
if(in_array('Badge Awarded',$dataPoints)) {
    $badgeAwarded = AxsActivityData::getActivityBadgeAwarded($params,$start_date,$end_date);
    $data = array_merge($data,$badgeAwarded);
}
if(in_array('Milestone Achieved',$dataPoints)) {
    $milestoneAchieved = AxsActivityData::getActivityMilestoneAchieved($params,$start_date,$end_date);
    $data = array_merge($data,$milestoneAchieved);
}
if(in_array('Lesson Activity Submitted',$dataPoints)) {
    $activitySubmitted = AxsActivityData::getActivityLessonActivitySubmitted($params,$start_date,$end_date);
    $data = array_merge($data,$activitySubmitted);
}
if(in_array('Watched Video',$dataPoints)) {
    $watchedVideo = AxsActivityData::getActivityWatchedVideo($params,$start_date,$end_date);
    $data = array_merge($data,$watchedVideo);
}
if(in_array('Event Registration',$dataPoints)) {
    $eventRegistration = AxsActivityData::getActivityEventRegistration($params,$start_date,$end_date);
    $data = array_merge($data,$eventRegistration);
}
if(in_array('Quiz or Survey Taken',$dataPoints)) {
    $quizTaken = AxsActivityData::getActivityQuizTaken($params,$start_date,$end_date);
    $data = array_merge($data,$quizTaken);
}
usort($data, function($a, $b) {
    return strtotime($a->date) - strtotime($b->date);
});

// Convert the data source to JSON so Kendo can use it to filter the dates correctly, and so we won't need an HTML
// table structure to be defined prior to building it out.
if(!$data) {
    $data = '[]';
} else {
    $data = json_encode($data);
}

$trackParams = new stdClass();
$trackParams->eventName = 'Run Report From Team Lead Dashboard';
$trackParams->action_type = 'Team Lead';
$trackParams->description = $report->name.' [type:'.$report->report_type.' id:'.$report->id.']';
AxsTracking::sendToPendo($trackParams);
AxsActions::storeAdminAction($trackParams);
?>
<style>
   body {
       background: #fff;
   }
</style>

<div id="activity_report"></div>

<script>
    var reportData = <?php echo $data; ?>;
    var exportingFlag = false;

    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        excelExport: function(event) {
            var sheet = event.workbook.sheets[0];
            var grid = event.sender;

            if (!exportingFlag) {
                grid.hideColumn('short_date');
                grid.showColumn('date');

                event.preventDefault();
                exportingFlag = true;

                setTimeout(function () {
                    grid.saveAsExcel();

                    grid.showColumn('short_date');
                    grid.hideColumn('date');
                });
            } else {
                exportingFlag = false;
            }

            // Cycle through each row and column and replace html tags
            sheet.rows.forEach(row => {
                row.cells.forEach(cell => {
                    if (cell && cell.value) {
                        if(cell.value.toString().indexOf("<br>") >= 0){
                            cell.value = cell.value.replaceAll("<br>", "\n");
                            cell.wrap = true;
                        }
                        if(cell.value.toString().indexOf("<br/>") >= 0){
                            cell.value = cell.value.replaceAll("<br/>", "\n");
                            cell.wrap = true;
                        }
                        if(cell.value.toString().indexOf("<b>") >= 0){
                            cell.value = cell.value.replaceAll("<b>", "");
                            cell.value = cell.value.replaceAll("</b>", "");
                            cell.wrap = true;
                        }
                    }
                });
            });

            // Get the column index of any date type columns,
            // they need to have a format stored alongside the value
            var fields = grid.dataSource.options.fields;
            var columns = grid.columns;
            var dateCells = [];

            fields
                .filter(f => f.field == "date")
                .forEach((field) => {
                    dateCells.push(columns.indexOf(columns.find(c => c.field == field.field)));
                });

            // Update any data type columns with a field type of date
            // with a format
			sheet.rows
                .filter(c => c.type == 'data')
                .forEach((row, idx) => {
                    dateCells.forEach((dateCell, idx) => {
                        var cellIndex = dateCell - 1;

                        row.cells[cellIndex].format = "mm/d/yyyy h:mm AM/PM";
                    });
                });
        },
        dataSource: {
            transport: {
                read: function(e) {
                    e.success(reportData);
                }
            },
            schema: {

                // Parse the given data set into a short date that is
                parse: function(data) {
                    var activities = [];

                    for(var i = 0; i < data.length; i++) {

                        var activity = data[i];
                        var longDate = kendo.parseDate(activity.date);

                        activity.short_date = kendo.toString(longDate, 'MM/d/yyyy');

                        activities.push(activity);
                    }

                    return activities;
                },

                model: {
                    id: 'user_id',

                    fields: {
                        user_id: { editable: false, nullable: false },
                        date: { type: "date" },
                        short_date: { type: "date" }
                    }
                }
            }
        },
        columns: [
            {
                field: "user_id",
                title: "<?php echo AxsLanguage::text("AXS_USER_ID_SPACES", "User ID") ?>",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "name",
                title: "<?php echo AxsLanguage::text("AXS_NAME", "Name") ?>",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "activity_type",
                title: "<?php echo AxsLanguage::text("AXS_ACTIVITY_TYPE", "Activity_Type") ?>",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "activity_data",
                title: "<?php echo AxsLanguage::text("AXS_ACTIVITY_DATA", "Activity Data") ?>",
                encoded: false,
                filterable: {
                    cell: {
                        suggestionOperator: "contains",
                        template: function(templateArgs) {

                            templateArgs.element.kendoAutoComplete({
                                dataSource: templateArgs.dataSource,
                                dataTextField: "activity_data",
                                template: "#= activity_data #",
                                valuePrimitive: true,
                                filter: "contains",
                                placeholder: "Search Activity Data",

                                // We don't need the selected value to appear in the search field - as long as the
                                // report shows the correct data when filtered we should be OK here.
                                change: function(event) {
                                    event.sender.value('');
                                }
                            });
                        }
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                field: "short_date",
                title: "<?php echo AxsLanguage::text("JDATE", "Date") ?>",
                type: "date",
                template: "#= kendo.toString(date, 'MM/d/yyyy h:mm tt') #",
                filterable: {
                    ui: "datepicker"
                }
            },
            {
                field: "date",
                title: "<?php echo AxsLanguage::text("JDATE", "Date") ?>",
                type: "date",
                format: "{0:MM/d/yyyy h:mm tt}",
                hidden: true,
                menu: false // We don't want this to appear in the column select menu in the header
            }
        ]
    }

    var activity_report = $("#activity_report").kendoGrid(kendoOptions);
    var grid = activity_report.data("kendoGrid");
</script>
