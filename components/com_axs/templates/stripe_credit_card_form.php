<?php

$brand = AxsBrands::getBrand();

if($brand->billing->stripe_production_mode) {
    $stripe_public_key = $brand->billing->stripe_prod_public_api_key;
} else {
    $stripe_public_key = $brand->billing->stripe_dev_public_api_key;
}
$hideZipCode = $brand->billing->stripe_hide_zip;
/*if(!$stripe_public_key) {
    return "You are missing your Stripe Public API Key";
}*/

?>
<link rel="stylesheet" type="text/css" href="/components/com_axs/assets/css/stripe.css">

<div class="form-row">
    <label for="card-element">
      <?php echo AxsLanguage::text("AXS_CREDIT_OR_DEBIT_CARD", "Credit or Debit card") ?>
    </label>
    <div id="card-element">
      <!-- A Stripe Element will be inserted here. -->
    </div>

    <!-- Used to display form errors. -->
    <div id="card-errors" role="alert"></div>
    <?php if (!empty($params->form_id == 'saveCardForm')): ?>
    <span id="ajax-loading-animation" style="display: none; float:right;"> <i class="fa fa-spinner fa-spin" aria-hidden="true"> </i> Validating Card </span>
    <?php endif; ?>
  </div>

<script>
    // Create a Stripe client.
    var stripe = Stripe('<?php echo $stripe_public_key; ?>');

    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
      base: {
        color: '#32325d',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
          color: '#aab7c4'
        }
      },
      invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
      }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', { <?php if($hideZipCode) { echo "hidePostalCode: true,"; } ?> style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
      var displayError = document.getElementById('card-errors');
      if (event.error) {
        displayError.textContent = event.error.message;
      }
      else {
        displayError.textContent = '';
      }

      <?php if (!empty($params->event_formId)): ?>
        $(document).find('#btn-submit').hide();
        $(document).find('#ajax-loading-animation').show();
        if (event.complete == true) {
          stripe.createToken(card).then(function(result) {

            if (result.error) {
              console.log(result);
              // Inform the user if there was an error.
              var errorElement = document.getElementById('card-errors');
              errorElement.textContent = result.error.message;
            }
            else {
              console.log(result);
              // Send the token to your server.
              // Insert the token ID into the form so it gets submitted to the server
              var form = document.getElementById('<?php echo $params->event_formId; ?>');
              var hiddenInput = document.createElement('input');
              hiddenInput.setAttribute('type', 'hidden');
              hiddenInput.setAttribute('name', 'stripeToken');
              hiddenInput.setAttribute('value', result.token.id);
              form.appendChild(hiddenInput);
              var hiddenInputLastFour = document.createElement('input');
              hiddenInputLastFour.setAttribute('type', 'hidden');
              hiddenInputLastFour.setAttribute('name', 'last4');
              hiddenInputLastFour.setAttribute('value', result.token.card.last4);
              form.appendChild(hiddenInputLastFour);
              $(document).find('#ajax-loading-animation').hide();
              $(document).find('#btn-submit').show();
            }
          });
        }
      <?php endif; ?>
      <?php if (!empty($params->form_id == 'saveCardForm')): ?>
        $(document).find('.saveCard,.addCardSave').hide();
        $(document).find('#ajax-loading-animation').show();
        if (event.complete == true) {
          stripe.createToken(card).then(function(result) {

            if (result.error) {
              console.log(result);
              // Inform the user if there was an error.
              var errorElement = document.getElementById('card-errors');
              errorElement.textContent = result.error.message;
            }
            else {
              console.log(result);
              // Send the token to your server.
              // Insert the token ID into the form so it gets submitted to the server
              $(document).find('#ajax-loading-animation').hide();
              $(document).find('.saveCard,.addCardSave').removeAttr('disabled');
              $(document).find('.saveCard,.addCardSave').show();
            }
          });
        }
      <?php endif; ?>


    });

    /*
		var iframe = document.createElement('iframe');
	iframe.src = paymentIntent.next_action.redirect_to_url.url;
	iframe.width = 600;
	iframe.height = 400;
	yourContainer.appendChild(iframe);

	window.top.postMessage('3DS-authentication-complete');

	function on3DSComplete() {
	  // Hide the 3DS UI
	  yourContainer.remove();

	  // Check the PaymentIntent
	  stripe.retrievePaymentIntent('{{PAYMENT_INTENT_CLIENT_SECRET}}')
		.then(function(result) {
		  if (result.error) {
			// PaymentIntent client secret was invalid
		  } else {
			if (result.paymentIntent.status === 'succeeded') {
			  // Show your customer that the payment has succeeded
			} else if (result.paymentIntent.status === 'requires_payment_method') {
			  // Authentication failed, prompt the customer to enter another payment method
			}
		  }
		});
	}

	window.addEventListener('message', function(ev) {
	  if (ev.data === '3DS-authentication-complete') {
		on3DSComplete();
	  }
	}, false); */
</script>
