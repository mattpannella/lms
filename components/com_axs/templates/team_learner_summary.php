<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
    $userId = JFactory::getUser()->id;
    if(!$userId) {
        exit;
    }

	$report = $this->getTeamLearnerSummary();
   
    $teamId = $reportParams->team_id;
    $teamArray = array($teamId);
    
	$type = $report->report_type;
	$model = FOFModel::getTmpInstance('Reports', 'ReportsModel');
	$params = json_decode($report->params);
    $params->report_layout = 'grid';
    $params->teams = $teamArray;
    $report->params = json_encode($params);
    $params = json_decode($report->params);
	$layout = $params->report_layout;
	$theme = $params->report_theme;
	$data = $model->getData($report);
	$brand = AxsBrands::getBrand();
	$currency_code = 'USD';
	$currency_symbol = '$';

    $trackParams = new stdClass();
    $trackParams->eventName = 'Run Report From Team Lead Dashboard';
    $trackParams->action_type = 'Team Lead';
    $trackParams->description = $report->name.' [type:'.$report->report_type.' id:'.$report->id.']';
    AxsTracking::sendToPendo($trackParams);
    AxsActions::storeAdminAction($trackParams);

	if($brand->billing->currency_code) {
		$currency_code = $brand->billing->currency_code;
	}
	if($brand->billing->currency_symbol) {
		$currency_symbol = $brand->billing->currency_symbol;
	}
    $items = $data->grid->items;
    $columns = json_decode($data->grid->columns);
    foreach ($columns as $column) {
        switch ($column->title) {
            case 'User ID':
                $column->title = AxsLanguage::text('AXS_USER_ID', 'User ID');
            break;
            case 'Course Category':
                $column->title = AxsLanguage::text('AXS_COURSE_CATEGORY', 'Course Category');
            break;
            case 'Course Completion Timespan':
                $column->title = AxsLanguage::text('AXS_COURSE_COMPLETION_TIMESPAN', 'Course Completion Timespan');
            break;
            case 'Course Start Date':
                $column->title = AxsLanguage::text('AXS_COURSE_START_DATE', 'Course Start Date');
            break;
            case 'Course Completed Date':
                $column->title = AxsLanguage::text('AXS_COURSE_COMPLETED_DATE', 'Course Completed Date');
            break;
        }
    }
    $aggregates = $data->grid->aggregates;
    $schema = $data->grid->schema;
?>
<script>
	var currency_symbol = '<?php echo $currency_symbol; ?>';
</script>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />
<link rel="stylesheet" href="/administrator/components/com_reports/assets/css/reports.css" />
<link rel="stylesheet" href="/administrator/components/com_dashboard/assets/css/dashboard.css?v=2" />
<link rel="stylesheet" href="/administrator/components/com_dashboard/assets/css/data_box.css" />

<script src="/media/kendo/js/jquery.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<?php echo AxsLanguage::getKendoLocalization(); ?>
<script>
	var report_data = <?php echo json_encode($report); ?>;
	var report_type = "<?php echo $layout; ?>";
</script>

<style>
	#spreadsheet,#grid {
		width: 100%;
		height: 700px;
	}
	.activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
	/* Used to fix bootstrap issue */
	.activated {
		background-image: 	none;
		background-color: 	#3276b1;
		border-color: 		#285e8e;
		outline-color: 		initial;
		outline-style: 		initial;
		outline-width: 		0px;
		box-shadow: 		inset 0 3px 5px rgba(0,0,0,0.125);
	}
	#activity-box {
        overflow-y: auto !important;
    }
	td {
		white-space: pre-line;
	}
    .k-grid-pager a {
        max-height: 20px !important;
    }
</style>

<br>
<div id="activity-box"></div>
<div style="width:100%; height: 10px;"></div>

<div id="grid"></div>

<script>
    function createNewLines(text) {
        return text;
    }
    var report = '<?php echo rawurlencode(json_encode($report)); ?>';
    var reportColumns = <?php echo empty($columns) ? '""' : json_encode($columns); ?>;
    <?php /*
    var aggregates = <?php echo $aggregates; ?>;
    var schema = <?php echo $schema; ?>;
    */ ?>

    $(document).ready(
        function () {
            $('#toolbar').html($('#report_tools')).show();
            var fileName = "export_report";

            $("#grid").kendoGrid({
                toolbar: [
                    {
                        name: "excel"
                    },
                    {
                        name: "pdf"
                    }
                ],
                excel: {
                    fileName: fileName+".xlsx",
                    filterable: true,
                    allPages: true
                },
                pdf: {
                    fileName: fileName+".pdf",
                    filterable: true,
                    allPages: true
                },
                dataSource: {
                    data: <?php echo $items; ?>,

                    <?php
                        if ($aggregates) {
                            echo "aggregate: " . $aggregates . ",";
                        }
                        if ($schema) {
                            echo "schema: " . $schema . ",";
                        }
                    ?>

                },
                selectable: "multiple cell",
                pageable: {
                    refresh: true,
                    buttonCount: 10,
                    pageSize: 100,
                    pageSizes:[100,200,500,1000],
                },

                sortable: true,
                groupable: true,
                filterable: {
                    mode: "row"
                },
                columnMenu: true,
                reorderable: true,
                resizable: true,
                allowCopy: true,
                columns: reportColumns,

                excelExport: function(e) {
                    var sheet = e.workbook.sheets[0];
                    for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
                        var row = sheet.rows[rowIndex];
                        for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex ++) {
                            var cell =row.cells[cellIndex];
                            if(cell.value && cell.value.toString().indexOf("<br>") >= 0) {
                                cell.value = cell.value.split("<br>").join("\n");
                                cell.wrap = true;
                            }
                            if(cell.value && cell.value.toString().indexOf("<b>") >= 0) {
                                cell.value = cell.value.split("<b>").join("");
                                cell.value = cell.value.split("</b>").join("");
                                cell.wrap = true;
                            }
                        }
                    }
                }

            });

            $(".clearSelection").click(function () {
                $("#grid").data("kendoGrid").clearSelection();
            });

            var selectRow = function (e) {
                if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
                    var grid = $("#grid").data("kendoGrid"),
                            rowIndex = $("#selectRow").val(),
                            row = grid.tbody.find(">tr:not(.k-grouping-row)").eq(rowIndex);

                    grid.select(row);
                }
            },
                toggleGroup = function (e) {
                    if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
                        var grid = $("#grid").data("kendoGrid"),
                            rowIndex = $("#groupRow").val(),
                            row = grid.tbody.find(">tr.k-grouping-row").eq(rowIndex);

                        if (row.has(".k-i-collapse").length) {
                            grid.collapseGroup(row);
                        } else {
                            grid.expandGroup(row);
                        }
                    }
                };


            $(".selectRow").click(selectRow);
            $("#selectRow").keypress(selectRow);

            $(".toggleGroup").click(toggleGroup);
            $("#groupRow").keypress(toggleGroup);

            $("#view_selection").change(
                function() {
                    var spreadsheet = $("#spreadsheet");
                    var grid = $("#grid");

                    var type = $('input[name="grid_type"]:checked').val();


                    switch (type) {
                        case "grid":
                            grid.show();
                            spreadsheet.hide();
                            break;
                        case "spreadsheet":
                            grid.hide();
                            spreadsheet.show();
                            break;
                    }

                    //Without a refresh, the cells in the excel sheet will not be rendered properly.
                    window.dispatchEvent(new Event('resize'));
                }
            );

            $(document).on('click','.k-grid-SelectAll',
                function() {
                    var grid = document.getElementById("grid");
                    var cells = grid.getElementsByTagName("td");

                    $(cells).each(
                        function() {
                            $(this).addClass('k-state-selected');
                        }
                    );

                }
            );

            <?php if($type == 'education' && ($params->education_report_type == 'survey_summary' || $params->education_report_type == 'team_summary')) { ?>
            var activity = $('#activity-box');
            activity.kendoDialog({
                width: "700px",
                height: "600px",
                title: "Student Activity",
                closable: false,
                modal: true,
                actions: [
                    {
                        text: 'Close',
                        action: closeActivity ,
                        primary: true
                    }
                ]
            });
            activity.data("kendoDialog").close();
            $(document).on('click', '.getSubmission', function(){
                var student_response = $(this).data('download');
                var activity_id      = $(this).data('id');
                var activity_type    = $(this).data('type');
                var user             = $(this).data('user');
                $.ajax({
                    url: '/index.php?option=com_splms&task=studentactivities.getSubmission&format=raw',
                    type: 'post',
                    data: {
                        student_response: student_response,
                        activity_type: activity_type,
                        user: user,
                        activity_id: 'report'
                    }
                }).done(function(response) {
                    $('#activity-box').html(response);
                    $('#activity-box').find('iframe').css('height','360');
                    activity.data("kendoDialog").open();
                });
            });

            function closeActivity() {
                $('#activity-box').html('');
            }
        <?php } ?>
        }
    );
</script>