<form id="tov_registration-form" action="#" method="post" aria-label="Registration Form" data-name="Registration Form" class="registration_form_content tov_login-form-block-container" style="display:none;">
    <input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_FIRST_NAME", "Enter Your First Name") ?>" placeholder="<?php echo AxsLanguage::text("AXS_FIRST_NAME", "First Name") ?>" id="register-firstname" class="tov_login-form-field w-input" type="text" name="register_firstname" tabindex="0" />
    <input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_LAST_NAME", "Enter Your Last Name") ?>" placeholder="<?php echo AxsLanguage::text("AXS_LAST_NAME", "Last Name") ?>" id="register-lastname" type="text" name="register_lastname"  tabindex="0"  class="tov_login-form-field w-input" />
    <input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_EMAIL", "Enter Your Email") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_EMAIL", "Email") ?>" id="register-email" class="tov_login-form-field w-input" type="text" name="register_email" tabindex="0" />
    <input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_USERNAME", "Enter Your Username") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_USERNAME", "Username") ?>" id="register-username" class="tov_login-form-field w-input" type="text" name="register_username" tabindex="0" />
    <div class="clearfix"></div>
    <div class="passswordMismatchMessage"></div>
    <input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_PASSWORD", "Enter Your Password") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_PASSWORD", "Password") ?>" id="register-password" type="password" name="register_password"  tabindex="0"  class="tov_login-form-field w-input" />
    <input aria-label="<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>" placeholder="<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>" id="register-password2" type="password" name="register_password2"  tabindex="0"  class="tov_login-form-field w-input" />

    <div style="margin:7px 0px">
        <?php echo AxsBrands::renderAgreementCheckbox($submit_button_id = 'register_button_submit'); ?>
    </div>

    <input type="hidden" name="login_page_id" value="<?php echo base64_encode($loginPageId); ?>">

    <?php
        $isIOS = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
        if(!$isIOS) {
            $captcha_plugin = JFactory::getConfig()->get('captcha');
            if ($captcha_plugin != '0') {
                $captcha = JCaptcha::getInstance($captcha_plugin);
                $field_id = 'recap_2';
                echo $captcha->display($field_id, $field_id, 'g-recaptcha');
            }
        }
    ?>
    <div class="tov_login-button-container">
        <div class="tov_login-horizontal-wrapper">
            <input type="submit" value="Register" data-wait="<?php echo AxsLanguage::text("AXS_PLEASE_WAIT", "Please wait...") ?>" id="register_button_submit" class="tov_login-button w-button">
        </div>
    </div>
    <?php echo JHtml::_('form.token'); ?>
</form>
<script>
    var pageURL = '<?php echo $return; ?>';

    $('input[name="register_password2"').keyup(function() {
        var pass2 = $(this).val();
        var pass1 = $('input[name="register_password"').val();
        if(pass2.length > 2) {
            if(pass2 != pass1) {
                var passswordMismatchMessage = "<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH_MESSAGE", "Passwords do not match."); ?>";
                $('.passswordMismatchMessage').text(passswordMismatchMessage);
                $('.passswordMismatchMessage').fadeIn(300);
            } else {
                $('.passswordMismatchMessage').fadeOut(300);
                $('.passswordMismatchMessage').text('');
            }
        }
    });

    function validateRegistrationFields() {
        var register_firstname	= $('input[name=register_firstname]').val();
        var register_lastname	= $('input[name=register_lastname]').val();
        var register_email		= $('input[name=register_email]').val();
        var register_username	= $('input[name=register_username]').val();
        var register_password	= $('input[name=register_password]').val();
        var register_password2	= $('input[name=register_password2]').val();
        var response = [];
        response['valid'] = true;
        if(!register_firstname) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_FIRST_NAME", "First Name") ?>";
            response['valid'] = false;
        } else if(!register_lastname) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_LAST_NAME", "Last Name") ?>";
            response['valid'] = false;
        } else if(!register_email) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_EMAIL", "Email") ?>";
            response['valid'] = false;
        } else if(!register_username) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_USERNAME", "Username") ?>";
            response['valid'] = false;
        } else if(!register_password) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_PASSWORD", "Password") ?>";
            response['valid'] = false;
        } else if(!register_password2) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>";
            response['valid'] = false;
        } else if(register_password != register_password2) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_PASSWORDS_DO_NOT_MATCH", "Passwords Do Not Match"); ?>";
            response['valid'] = false;
        }
        return response;
    }

    $('#tov_registration-form').submit(function(ev) {

        ev.preventDefault();
        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }
        $('#register_button_submit').attr('disabled','true');
        var response = validateRegistrationFields();
        if(!response['valid']) {
            $('#register_button_submit').removeAttr('disabled');
            $(".w-form-fail").html(response['message']);
            if($(".w-form-fail:hidden")) {
                $(".w-form-fail").slideToggle();
            }
            return;
        }
        var data = $('#tov_registration-form').serialize();
        /* var data = {
            register_firstname	: $('input[name=register_firstname]').val(),
            register_lastname	: $('input[name=register_lastname]').val(),
            register_email		: $('input[name=register_email]').val(),
            register_username	: $('input[name=register_username]').val(),
            register_password	: $('input[name=register_password]').val(),
            register_password2	: $('input[name=register_password2]').val(),
            captcha         	: $('input[name=g-recaptcha-response]').val()
        } */

        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }

        $.ajax({
            url: '/index.php?option=com_axs&task=user.register&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                var response = JSON.parse(result);
                if (response.status == "success") {
                        window.location = pageURL;
                } else {
                    $('#register_button_submit').removeAttr('disabled');
                    grecaptcha.reset();
                    $(".w-form-fail").html(response.error);
                    if($(".w-form-fail:hidden")) {
                        $(".w-form-fail").slideToggle();
                    }
                }
        });
    });
</script>