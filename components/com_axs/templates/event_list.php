<?php
$events_text = AxsLanguage::text("AXS_EVENTS_AND_CLASSES", "Events and Classes");

if ($params->show_events == 'custom' && $params->events_text) {
	$events_text = $params->events_text;
}

if ($_COOKIE['timezone']) {
	$timezone = filter_var($_COOKIE['timezone'], FILTER_SANITIZE_STRING);
}
$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if ($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if ($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$VC_Server = AxsClientData::getClientVirtualMeetingServer();
$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
$virtualMeeting = new AxsVirtualClassroom($setupParams);

?>
<div class="tovuti-tasklist">
	<button class="btn btn-primary card-collapse" type="button" aria-label="Toggle All Events" data-toggle="collapse" data-target="#tasklist-body-collapse3" aria-expanded="false" aria-controls="tasklist-body-collapse3">
		<span class="fa-solid fa-chevron-down"></span>
	</button>
	<span class="tov-task-list-header-wrap"><?php echo $events_text; ?> <a class="archive-link" data-section="events"><?php echo AxsLanguage::text("AXS_SHOW_PAST_EVENTS", "Show Past Events") ?></a></span>
	<div class="tovuti-tasklist-body-events card card-body collapse in show" id="tasklist-body-collapse3">
		<ul class="tovuti-tasklist-list-item">
			<?php foreach ($events as $event) {
				$day   = date('d', strtotime($event->event_date));
				$month = date('M', strtotime($event->event_date));
				$year  = date('Y', strtotime($event->event_date));
				$eventTimestamp = strtotime($event->event_date);
				$todayTimestamp = strtotime('NOW');
				$eventDate      = date('m/d/Y', $eventTimestamp);
				$eventTime      = date('H:i:s', $eventTimestamp);
				$todayDate      = date('m/d/Y', $todayTimestamp);
				$eventParams = json_decode($event->params);
				$eventTimeZone  = $eventParams->time_zone;
				$eventClass     = 'upcoming-event';
				if ($eventTimestamp < $todayTimestamp) {
					$eventClass = 'past-event';
				}
				if ($timezone) {
					$now = new DateTime('now', new DateTimeZone($timezone));
					$now->setTimeZone(new DateTimeZone($timezone));
					$event_time  =  $now->format('g:i A');
					$event_day   =  $now->format('d');
					$event_month =  $now->format('M');
					$event_year  =  $now->format('Y');
				}

				$link      = '';
				$link_text = '';
				$time      = '';
				$attendees = '';
				if (!$event->virtual) {
					$link = JRoute::_(EventbookingHelperRoute::getEventRoute($event->id));
					$link_text = AxsLanguage::text('AXS_VIEW_DETAILS', 'View Details');
					if ($eventParams->virtual_meeting) {
						$event->virtual = true;
						$eventVirtualMeeting = $virtualMeeting->getMeetingById($eventParams->virtual_meeting);
						$event->settings = $eventVirtualMeeting->settings;
						$event->meetingId = $eventVirtualMeeting->meetingId;
						$event->moderatorPW = $eventVirtualMeeting->moderatorPW;
						$event->attendeePW = $eventVirtualMeeting->attendeePW;
					}
				}
				$virtualClassIsRunning = false;

				if ($event->virtual) {
					$virtualParams    = json_decode($event->settings);
					$virtualClassTime = json_decode($virtualParams->time);
					//$virtualClassIsRunning = $virtualMeeting->isMeetingRunning($event->meetingId);
					$meetingParams = new stdClass();
					$meetingParams->meetingId = $event->meetingId;
					$meetingParams->moderatorPW = $event->moderatorPW;
					//$virtualClass = $virtualMeeting->getMeetingInfo($meetingParams);
					//$attendees = $virtualClass->participantCount;
					if ($virtualClassTime->hour && $virtualParams->time_zone) {
						if ($virtualClassTime->minute == '0') {
							$virtualClassTime->minute = '00';
						}
						$time = $virtualClassTime->hour . ':' . $virtualClassTime->minute . ' ' . $virtualClassTime->ampm;
					}

					$virtualClassParams = new stdClass();
					$virtualClassParams->meetingId = $event->meetingId;
					$virtualClassParams->userId    = $userId;
					//$attendance = $virtualMeeting->getAttendance($virtualClassParams);
					if ($attendance) {
						$event->checked_in = true;
					}
					if ($timezone && $virtualParams->time_zone && $year > 1969) {
						if ($time) {
							$time = ' ' . $time;
						} else {
							$time = $eventTime;
						}
						$newDateTime = new DateTime($eventDate . $time, new DateTimeZone($virtualParams->time_zone));
						$newDateTime->setTimeZone(new DateTimeZone($timezone));
						if ($virtualClassTime->hour) {
							$time  =  $newDateTime->format('g:i A');
						}
						$day   =  $newDateTime->format('d');
						$month =  $newDateTime->format('M');
						$year  =  $newDateTime->format('Y');
					}
				}

				if ($event->virtual && ($year < 1970 || $event_day . $event_month . $event_year == $day . $month . $year)) {

					$isHost = $virtualMeeting->checkHostAccess($userId, $event->meetingId);
					$encodedLink = new stdClass();
					$encodedLink->meetingId = $event->meetingId;
					$encodedLink->userId = $userId;
					$encodedLink->userName = JFactory::getUser($userId)->name;

					if($eventParams->auto_checkin) {
						$encodedLink->eventId = $event->id;
						$encodedLink->auto_checkin = true;
					}
	
					if ($isHost) {
						$encodedLink->userPassword = $event->moderatorPW;
					} else {
						$encodedLink->userPassword = $event->attendeePW;
					}
					$encryptedParams = base64_encode(AxsEncryption::encrypt($encodedLink, $key));
					$link = '/join-virtual-classroom?vcid=' . $encryptedParams;
					$link_text = AxsLanguage::text("AXS_JOIN_CLASS_OR_MEETING", "Join Class or Meeting");
				}

				if ($link) {
					$hasLinkClass = 'task-has-link';
				}

				$archiveClass = '';

				if (strtotime($event_year . '-' . $event_month . '-' . $event_day) > strtotime($year . '-' . $month . '-' . $day) && $year > 1969) {
					$archiveClass = 'archive-item archive-section-events';
				}

				// Different languages have different length words for 'open' so
				// sometimes we need to adjust the font size of the 'open' text
				// to prevent overflow
				$language = AxsLanguage::getCurrentLanguage()->get('tag');
				$openDateStyle = '';
				if ($language == 'es-ES' || $language == 'fr-FR') {
					$openDateStyle = 'font-size:12px !important;';
				}
				
				// Different languages have different length words for 'open' so
				// sometimes we need to adjust the font size of the 'open' text
				// to prevent overflow
				$language = AxsLanguage::getCurrentLanguage()->get('tag');
				$openDateStyle = '';
				if ($language == 'es-ES' || $language == 'fr-FR') {
					$openDateStyle = 'font-size:12px !important;';
				}
				
				// Different languages have different length words for 'open' so
				// sometimes we need to adjust the font size of the 'open' text
				// to prevent overflow
				$language = AxsLanguage::getCurrentLanguage()->get('tag');
				$openDateStyle = '';
				if ($language == 'es-ES' || $language == 'fr-FR') {
					$openDateStyle = 'font-size:12px !important;';
				}


			?>
				<li class="course-item event-item <?php echo $archiveClass; ?>">
					<div class="<?php echo $eventClass; ?> courselist-item <?php echo $hasLinkClass; ?>">
						<div class="date">
							<?php if ($event->event_date && $year > 1969) { ?>
								<span class="day"><?php echo $day; ?></span>
								<span class="month"><?php echo $month; ?></span>
								<span class="year"><?php echo $year; ?></span>
							<?php } else { ?>
								<span class="open_date" style="<?php echo $openDateStyle; ?>"><?php echo AxsLanguage::text("AXS_OPEN_CAPITALIZED", "OPEN") ?></span>
							<?php } ?>
						</div>
						<span class="course-title">
							<?php echo $event->title; ?>
						</span>
					</div>
					<?php if ($link) { ?>
						<a class="task-link" aira-label="Go to <?php echo $event->title; ?>" href="<?php echo $link; ?>">
							<span class="fa fa-arrow-right"></span>
						</a>
					<?php } ?>
					<div class="clearfix"></div>
					<div class="task-instructions course-details">
						<?php if ($link) { ?>
							<a class="event-link" href="<?php echo $link; ?>">
								<?php echo $link_text; ?>
							</a>
							<?php if ($virtualClassIsRunning == 'true') {
								echo '<b> ' . AxsLanguage::text("AXS_RUNNING", "Running") . ': </b> ' . AxsLanguage::text("AXS_YES", "Yes");
							} ?>
							<?php if ($attendees) {
								echo '<b> ' . AxsLanguage::text("AXS_ATTENDEES", "Attendees") . ': </b> ' . $attendees;
							} ?>
						<?php } ?>
						<?php if ($time) {
							echo '<b> ' . AxsLanguage::text("AXS_TIME", "Time") . ': </b>' . $time. '<b> ' . AxsLanguage::text("AXS_TIMEZONE", "Time Zone") . ': </b>'. $eventTimeZone;
						} ?>
						<?php if ($event->virtual) {
							echo '<b> ' . AxsLanguage::text("AXS_VIRTUAL_CLASS_SLASH_MEETING", "Virtual Class/Meeting") . ': </b> ' . AxsLanguage::text("AXS_YES", "Yes");
						} ?>
						<?php if ($event->register_date) {
							echo '<b> ' . AxsLanguage::text("AXS_REGISTRATION_DATE", "Registration Date") . ':</b> ' . AxsLMS::formatDate($event->register_date);
						} ?>
						<?php if ($event->total_amount) {
							echo ' <b> ' . AxsLanguage::text("AXS_PAYMENT", "Payment") . ':</b>  ' . $currency_symbol . $event->total_amount;
						} ?>
						<?php 	/* if($eventClass == 'past-event') {
								echo ' <b> Attended: </b>';
								if($event->checked_in) {
									echo 'Yes';
								} else {
									echo 'No';
								}
							}  */
						?>
					</div>
					<div class="clearfix" style="height: 20px; background: #fff;"></div>
				</li>
			<?php } ?>
		</ul>
	</div>
</div>