<?php
$checklist_text = AxsLanguage::text("AXS_CHECKLISTS", "Checklists");

if ($params->show_checklist == 'custom' && $params->checklist_text) {
	$checklist_text = $params->checklist_text;
}
?>

<div class="tovuti-tasklist">
	<button class="btn btn-primary card-collapse" aria-label="Toggle All Checklists" type="button" data-toggle="collapse" data-target="#tasklist-body-collapse" aria-expanded="false" aria-controls="tasklist-body-collapse">
		<i class="fa-solid fa-chevron-down" aria-hidden="true" focusable="false"></i>
	</button>
	<span class="tov-task-list-header-wrap"><?php echo $checklist_text; ?> <a class="archive-link" data-section="checklist"><?php echo AxsLanguage::text("AXS_SHOW_COMPLETED_LISTS", "Show Completed Lists") ?></a></span>
	<div class="tovuti-tasklist-body card card-body collapse in show" id="tasklist-body-collapse">
		<p><?php echo AxsLanguage::text("AXS_FILTERS", "Filters") ?>:</p>
		<ul class="tasklist-filters">
			<li data-filter="all" class="filter-all active"><?php echo AxsLanguage::text("JALL", "All") ?></li>
			<li data-filter="due-soon" class="filter-due-soon"><?php echo AxsLanguage::text("AXS_DUE_SOON", "Due Soon") ?></li>
			<li data-filter="courses" class="filter-courses"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses") ?></li>
			<li data-filter="awards" class="filter-awards"><?php echo AxsLanguage::text("AXS_AWARDS", "Awards") ?></li>
			<li data-filter="events" class="filter-events"><?php echo AxsLanguage::text("AXS_EVENTS", "Events") ?></li>
			<li data-filter="videos" class="filter-videos"><?php echo AxsLanguage::text("AXS_VIDEOS", "Videos") ?></li>
			<li data-filter="other" class="filter-other"><?php echo AxsLanguage::text("AXS_OTHER", "Other") ?></li>
		</ul>
		<div class="nano">
			<div class="nano-content">
				<?php
				foreach ($userChecklists as $checklist) {
					include 'components/com_axs/templates/checklists.php';
				}
				?>
			</div>
		</div>
	</div>
</div>