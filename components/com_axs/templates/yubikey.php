<?php
	if($otpConfig->method === 'yubikey') {
		$alreadySetup = true;
	}
?>
<div class="header-fullwidth" style="
	background-image: url(/components/com_axs/assets/images/security_bg_optimized.jpg);
	background-size:cover;
	background-position:center;
	height: 300px;
	position: relative;">
	<div class="slide-overlay">
	<h2>YubiKey <i class="fa-light fa-arrow-right-arrow-left"></i> Tovuti</h2>
	</div>
</div>
<main>
	<section class="moyo-content-body">
		<div class="moyo-content-title">
			<h2>2FA</h2>
			<?php if($alreadySetup) { ?>
				<span class="badge rounded-pill text-bg-success moyo-2fa-badge"><i class="fa-light fa-check"></i>
				<?php echo AxsLanguage::text("AXS_SECURITY_ENABLED", "Enabled"); ?>
				</span>
			<?php } else { ?>
				<span class="badge rounded-pill text-bg-danger moyo-2fa-badge"><i class="fa-light fa-xmark"></i>
				<?php echo AxsLanguage::text("AXS_SECURITY_DISABLED", "Disabled"); ?>
				</span>
			<?php } ?>
		</div>
		<div class="moyo-content-wrapper">
			<section class="moyo-content-2fa">
				<div class="card text-center">
					<div class="moyo-card-2fa card-body">
						<h5 class="card-title">
							<?php echo AxsLanguage::text("AXS_SECURITY_YUBIKEY_AUTHENTICATION", "YubiKey Authentication"); ?>
						</h5>
						<a href="#" class="btn btn-dark" style="z-index:100;" data-toggle="modal"
							data-target="#securityModal"><i class="fa-light fa-plus-large"></i></a>
						<a href="#" class="moyo-2fa-link" data-toggle="modal"
							data-target="#yubiModal">
						<?php echo AxsLanguage::text("AXS_SECURITY_WHATIS_YUBIKEY", "What is a YubiKey Security Key?"); ?>
						</a>
						<img class="moyo-yubi-1" src="/components/com_axs/assets/images/auth-images/5series-new-edited-1.png">
						<img class="moyo-yubi-2" src="/components/com_axs/assets/images/auth-images/5series-new-edited-2.png">
						<img class="moyo-yubi-3" src="/components/com_axs/assets/images/auth-images/5series-new-edited-3.png">
						<img class="moyo-yubi-4" src="/components/com_axs/assets/images/auth-images/SKY3-SKY2-overhead-large.png">
					</div>
				</div>
			</section>
		</div>
	</section>
</main>

<div class="modal modal-2fa fade" tabindex="-1" id="securityModal" aria-labelledby="securityModal" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content text-center">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="moyo-2fa-setup"  <?php if($alreadySetup) { echo 'style="display: none;"'; } ?>>
					<div class="moyo-2fa-progress">
						<div class="moyo-2fa-incomplete"><i class="fa-solid fa-xmark"></i></div>
						<hr class="moyo-2fa-divider">
						<div class="moyo-2fa-complete disabled"><i class="fa-solid fa-check"></i></div>
					</div>
					<form id="moyo-2fa-auth">
						<div class="moyo-2fa-form mb-3">
							<label for="security-key" class="form-label">
							<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key"); ?>
							</label>
							<input type="password" class="form-control" id="tfa-key" name="tfa-key" aria-describedby="security key"/>
						</div>
						<button type="submit" class="btn btn-primary moyo-2fa-button"><i class="fa-light fa-lock"></i>
							<?php echo AxsLanguage::text("AXS_SECURITY_AUTHENITICATE", "Authenticate"); ?>
						</button>
					</form>
					<div class="moyo-modal-2fa-custom">
						<ol>
							<li>
								<?php echo AxsLanguage::text("AXS_SECURITY_YUBI_STEP_1", "Insert your Yubikey into your USB por"); ?>
							</li>
							<li>
								<?php echo AxsLanguage::text("AXS_SECURITY_YUBI_STEP_2", "Set your cursor in the YubiKey Field above"); ?>
							</li>
							<li>
								<?php echo AxsLanguage::text("AXS_SECURITY_YUBI_STEP_3", "Touch and hold your Yubikey and click athenticate!"); ?>
							</li>
						</ol>
					</div>
				</div>
				<div class="moyo-2fa-setup-complete"  <?php if(!$alreadySetup) { echo 'style="display: none;"'; } ?>>
					<div class="moyo-2fa-progress">
						<div class="moyo-2fa-incomplete disabled"><i class="fa-solid fa-xmark"></i></div>
						<hr class="moyo-2fa-divider">
						<div class="moyo-2fa-complete"><i class="fa-solid fa-check"></i></div>
					</div>
					<div class="moyo-2fa-form mb-3">
						<label for="setup-complete" class="form-label">
							<?php echo AxsLanguage::text("AXS_SECURITY_AUTHENITICATED", "Authenticated"); ?>
						</label>
						<div id="setup-complete" class="form-text">
							<?php echo AxsLanguage::text("AXS_SECURITY_2FA_SETUP", "Congratulations, you're authenticated with 2FA"); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal modal-2fa fade" tabindex="-1" id="yubiModal" aria-labelledby="yubiModal" aria-hidden="true">
	<div class="modal-dialog modal-lg modal-dialog-centered">
		<div class="modal-content text-center">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="moyo-2fa-setup-complete">
					<div class="moyo-2fa-form mb-3">
						<label for="setup-complete" class="form-label">
							<?php echo AxsLanguage::text("AXS_SECURITY_WHATIS_YUBIKEY", "What is a YubiKey Security Key?"); ?>
						</label>
						<div id="setup-complete" class="form-text text-left">
							<?php echo AxsLanguage::text("AXS_SECURITY_YUBIKEY_DESCRIPTION_TEXT", "The YubiKey is a form of 2 Factor Authentication (2FA) which works as an extra layer of security to your online accounts. With a YubiKey, you simply register it to your account, then when you log in, you must input your login credentials (username+password) and use your YubiKey (plug into USB-port or scan via NFC). Both login credentials and YubiKey are needed at login. This physical layer of protection prevents many account takeovers that can be done virtually.
							<br><br>
							2FA is a method to confirm a user's claimed online identity by using a combination of two different types of factors. Factors used for 2FA include something that you know (e.g. password or PIN), or something that you have (e.g. a security key or phone) or something that you are (e.g. facial recognition). To learn more about Strong Two Factor Authentication, please click here.
							<br><br>
							A single YubiKey has multiple functions for securing your login to email, online services, apps, computers, and even physical spaces. Use any YubiKey feature, or use them all. The versatile YubiKey requires no software installation or battery and therefore it is ready to use directly out of the package. Just login to the service you want to add that extra protection to and register the Key with your account."); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	const task = "saveYubiKey";
	const enabled = "<?php echo AxsLanguage::text("AXS_SECURITY_ENABLED", "Enabled"); ?>";
</script>
<script src="/components/com_axs/assets/js/otp.js"></script>
