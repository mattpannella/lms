<?php
$courses_text = AxsLanguage::text("AXS_COURSE_PROGRESS", "Course Progress");

if ($params->show_courses == 'custom' && $params->courses_text) {
	$courses_text = $params->courses_text;
}

$now = strtotime('now');
?>
<style>
	.assigned-item,
	.recommended-item {
		font-weight: bold;
		display: inline-block;
		padding: 5px 10px 5px 10px;
		border-radius: 20px;
		color: #fff;
	}

	.assigned-item {
		background-color: #da1e28;
	}

	.recommended-item {
		background-color: #198038;
	}
</style>
<div class="tovuti-tasklist">
	<button class="btn btn-primary card-collapse" aria-label="Toggle All Courses" type="button" data-toggle="collapse" data-target="#tasklist-body-collapse2" aria-expanded="false" aria-controls="tasklist-body-collapse2">
		<span class="fa-solid fa-chevron-down"></span>
	</button>
	<span class="tov-task-list-header-wrap"><?php echo $courses_text; ?> <a class="archive-link" data-section="courses"><?php echo AxsLanguage::text("AXS_SHOW_COMPLETED_COURSES", "Show Completed Courses") ?></a>
	</span>
	<div class="tovuti-tasklist-body card card-body collapse in show" id="tasklist-body-collapse2" aria-label="">
		<div class="nano">
			<div class="nano-content">
				<ul class="tovuti-tasklist-list-item">

					<?php
					$user_courses = AxsLMS::getUserAllCourses($userId);
					foreach ($courses as $course) {

						$courseParams = json_decode($course->params);
						$open_date = $courseParams->course_open_date;
						$visible_before_open = $courseParams->course_visible_before_open;
						$close_date = $courseParams->course_close_date;
						$visible_after_close = $courseParams->course_visible_after_close;



						$course_user_data = $user_courses[$course->splms_course_id];
						$wishlisted = $course_user_data[AxsLMS::$userCourseStatuses['Wishlisted']];
						$recommended = $course_user_data[AxsLMS::$userCourseStatuses['Recommended']];
						$assigned = $course_user_data[AxsLMS::$userCourseStatuses['Assigned']];
						$purchased = $course_user_data[AxsLMS::$userCourseStatuses['Purchased']];
						$inProgress = $course_user_data[AxsLMS::$userCourseStatuses['In Progress']];
						$completed = $course_user_data[AxsLMS::$userCourseStatuses['Completed']];

						// Check the course configuration for an open date
						// and course visibile after open setting
						$course_open = true;
						$course_visible = true;

						if ($open_date) {
							if (strtotime($open_date) > $now) {
								$course_open = "not open";
								if (!$visible_before_open) {
									$course_visible = false;
								}
							}
						}
						if ($close_date) {
							if (strtotime($close_date) < $now) {
								$course_open = "closed";
								if (!$visible_after_close) {
									$course_visible = false;
								}
							}
						}

						// Check if the course was assigned by a courseassignment, and
						// it is past the end_date don't show the course
						$not_past_course_assignment_end_date = $assigned->end_date == 0 || strtotime($assigned->end_date) >= $now;

						// Check if the course was assigned by a courseassignment, and
						// it is before the start date, don't show the course.
						$not_before_course_assignment_start_date = $assigned->start_date == 0 || strtotime($assigned->start_date) <= $now;
						if (
							$course_open !== true ||
							!$course_visible ||
							!$not_past_course_assignment_end_date ||
							!$not_before_course_assignment_start_date
						) {
							continue;
						}

						if (!$course->image) {
							$course->image = "/images/SWltRlRIMGZIb2xCcWZEVlFLWXc2QT09OjI0NzM/placeholder.jpg";
						} else {
							$course->image = str_replace('https://tovuti.io/', '', $course->image);
							$course->image = str_replace('https://tovutilms.com/', '', $course->image);
							if (strpos($course->image, 'http') === false) {
								$course->image = '/' . $course->image;
							}
						}
						$completeClass = 'course-in-progress';
						if ($course->status == 'Completed') {
							$completeClass = 'course-completed';
						}
						//Filter based on Teamlead. Do not show Link.
						$mainUserID = $params->mainUserID;

						// Set up the course links
						if ($mainUserID != $userId) {
							$link = false;
						} else {

							// By default, the course link goes to the slugged internal link for the given course
							$link = JRoute::_('index.php?option=com_splms&view=course') . $course->slug;

							// Course has an external link configured - make sure to use that instead
							if (!empty($courseParams->external_link)) {

								$link = $courseParams->external_link;
							}

							$hasLinkClass = 'task-has-link';
						}

						$archiveClass = '';
						if ($course->percentComplete == 100) {
							$archiveClass = 'archive-item archive-section-courses';
						}
					?>
						<li class="course-item <?php echo $archiveClass; ?>">
							<div class="progress">
								<div class="progress-bar progress-bar-success" role="progressbar" aria-label="Checklist Progress" aria-valuenow="<?php echo $course->percentComplete; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $course->percentComplete; ?>%;">
									<span class="sr-only" style="color:#000000 !important;">
										<span class="progress-amount" aria-label="<?php echo $course->percentComplete; ?>% <?php echo AxsLanguage::text("AXS_COMPLETE", "Complete") ?>"><?php echo $course->percentComplete; ?></span>% <?php echo AxsLanguage::text("AXS_COMPLETE", "Complete") ?>
									</span>
								</div>
							</div>
							<div class="<?php echo $completeClass; ?> courselist-item <?php echo $hasLinkClass; ?>">
								<img class="course-preview" alt="Go to <?php echo $course->title; ?>" src="<?php echo $course->image; ?>" />
								<span class="course-title">
									<?php echo $course->title; ?>
								</span>
							</div>
							<?php if ($link) { ?>
								<a class="task-link" href="<?php echo $link; ?>" role="button" aria-label="Go to <?php echo $course->title; ?>">
									<span class="fa fa-arrow-right" role="presentation"></span>
								</a>
							<?php } ?>
							<div class="clearfix"></div>
							<div class="task-instructions course-details">
								<?php if ($course->assigned) {
									echo '<span class="assigned-item">' . AxsLanguage::text("AXS_ASSIGNED", "Assigned") . '</span> ';
								} ?>
								<?php if ($course->recommended) {
									echo '<span class="recommended-item">' . AxsLanguage::text("AXS_RECOMMENDED", "Recommended") . '</span> ';
								} ?>
								<?php if ($course->date_started) {
									echo '<b>' . AxsLanguage::text("AXS_STARTED", "Started") . ':</b> ' . AxsLMS::formatDate($course->date_started);
								} ?>
								<?php if ($course->date_completed) {
									echo ' <b>' . AxsLanguage::text("AXS_COMPLETED", "Completed") . ':</b> ' . AxsLMS::formatDate($course->date_completed);
								} ?>
								<?php echo ' <b>' . AxsLanguage::text("AXS_PROGRESS", "Progress") . ': </b>' . $course->percentComplete . '%'; ?>
								<?php if ($course->date_due && AxsLMS::formatDate($course->date_due)) { ?>
									<span class="task-has-due-date"><?php echo AxsLanguage::text("AXS_DUE", "Due") ?> <?php echo date('m/d', strtotime($course->date_due)); ?></span>
								<?php } ?>
							</div>

							<div class="clearfix" style="height: 20px; background: #fff;"></div>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>