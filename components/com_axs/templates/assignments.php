<?php
    $team = $this->getTeam();

    $actionParams = new stdClass();
    $actionParams->team_id = (int)$team->id;
    $actionParams->learnerDashboard_id = (int)$this->getDashboard()->id;
    $actionParams->action = 'new';
    $actionParams->user_id = JFactory::getUser()->id;
    $addEncryptedParams = base64_encode(AxsEncryption::encrypt($actionParams, $key));

    if($teamParams->course_ids) {
        $courseFilter = base64_encode($teamParams->course_ids);
    }

    if(!empty($teamParams->user_groups)) {
        $groupFilter = base64_encode($team->user_groups);
    }

    $teamMemberType = $team->member_type;

    $id = rand(0,2000);
?>
<link href="/components/com_axs/assets/css/assignments.css?v=8" type="text/css" rel="stylesheet">
<div class="tld-team-container">
    <div class="tld-team-wrapper" style="overflow: visible;">
        <div class="tld-section-top">
            <span class="tld-team-top-heading"><?php echo AxsLanguage::text("AXS_ASSIGNMENTS", "Assignments") ?></span>
            <div class="tld-search-filtering">
                <div class="tld-search-form-block w-form">
                    <div class="tld-search-form">
                        <div class="tld-search-field-block">
                            <div class="tld-search-icon-block tld_search_assignment">
                                <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_search.svg" alt="" class="tld-search-image">
                            </div>
                            <input type="text" name="tld_search_assignment" id="tld_search_assignment" class="tld-select-field w-input" maxlength="256" placeholder="<?php echo AxsLanguage::text("JSEARCH_FILTER", "Search") ?>">
                        </div>
                        <?php if($teamParams->allow_assigning_courses) { ?>
                        <div class="tld-search-field-block">
                            <a href="javascript:void(0)" class="tld-select-field w-input assignment-form-btn">
                                <i class="fa fa-bookmark"></i> <?php echo AxsLanguage::text("AXS_ASSIGN_COURSES", "Assign Courses") ?>
                            </a>
                        </div>
                        <?php } ?>
                        <?php if($teamParams->allow_assigning_events) { ?>
                        <div class="tld-search-field-block">
                            <a href="javascript:void(0)" class="tld-select-field w-input assign-events-form-btn">
                                <i class="fa fa-calendar"></i> <?php echo AxsLanguage::text("AXS_ASSIGN_EVENTS", "Assign Events") ?>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="tld-team-content-container" style="height: auto;">
            <div>
                <?php if($teamParams->allow_assigning_courses) { ?>
			    <div class="tld-team-member-wrapper assignment-list" id="assignmentList"></div>
                <form method="#" action="post" id="assignment_form" class="assignment_form tld-assign-form">
                    <h3><?php echo AxsLanguage::text("AXS_ASSIGN_COURSES", "Assign Courses") ?></h3>
                    <div class="form-group">
                        <input aria-label="<?php echo AxsLanguage::text("JGLOBAL_TITLE", "Title") ?>" placeholder="<?php echo AxsLanguage::text("AXS_ASSIGNMENT_TITLE", "Assignment Title") ?>" id="title" name="title" type="text" class="form-control" value="">
                    </div>

                    <input type="hidden" name="course[]" value=""/>
                    <div class="tld-search-field-block">
                        <div class="tld-search-icon-block tld_search">
                            <span id="course_counter" class="counter-box">
                                <i class="fa fa-check"></i>
                                <span class="counter-text" style="display:none">
                                    <span class="counter-count"></span> <?php echo AxsLanguage::text("AXS_COURSE_LOWERCASE", "course"); ?><span class="counter-s" style="display:none">s</span> <?php echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); ?>
                                </span>
                            </span>
                        </div>

                        <a
                            href="/index.php?option=com_axs&view=selectors&tmpl=component4&selection=courses&el=course&teamId=<?php echo $this->getTeam()->id; ?>"
                            class="tovuti-popup tld-select-field w-input course_selector"
                            data-width="800"
                            data-height="800"
                            data-type="iframe"
                        >
                            <?php echo AxsLanguage::text("AXS_SELECT_COURSES", "Select Courses") ?>
                        </a>
                    </div>
                    <div class="form-group">
                        <b><?php echo AxsLanguage::text("AXS_COURSES_RECOMMENDED_OR_ASSIGNED", "Are these course(s) Recommended or Assigned?") ?></b>
                        <br/>
                        <div class="radio">
                            <label><input type="radio" name="assign_type" value="0" checked><?php echo AxsLanguage::text("AXS_ASSIGNED", "Assigned") ?></label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="assign_type" value="1"><?php echo AxsLanguage::text("AXS_RECOMMENDED", "Recommended") ?></label>
                        </div>
                    </div>

                    <!-- 
                        If the team member type is "individuals", hide the assign_to radio selector, otherwise show it.
                    -->
                    <?php if($team->member_type == 'groups') : ?>
                    
                    <div class="form-group">
                        <b><?php echo AxsLanguage::text("AXS_ASSIGN_TO_SPECIFIC_USERS_OR_USER_GROUPS", "Assign to Specific Users or User Groups?") ?></b>
                        <br/>
                        <div class="radio">
                            <label><input type="radio" name="assign_to" value="groups" checked><?php echo AxsLanguage::text("AXS_USER_GROUPS", "User Groups") ?></label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="assign_to" value="users"><?php echo AxsLanguage::text("AXS_SPECIFIC_USERS", "Specific Users") ?></label>
                        </div>
                    </div>

                    <?php endif; ?>

                    <div class="checkbox assign-type usergroup_checkbox">
                        <b><?php echo AxsLanguage::text("AXS_SELECT_USER_GROUPS", "Select User Groups:") ?></b>
                        <br/><br/>
                        <input type="hidden" name="group[]" value=""/>
                        <div class="tld-search-field-block">
                            <div class="tld-search-icon-block tld_search">
                                <span id="group_counter" class="counter-box">
                                    <i class="fa fa-users"></i>
                                    <span class="counter-text" style="display:none">
                                        <span class="counter-count"></span> <?php echo AxsLanguage::text("AXS_GROUP_LOWERCASE", "group"); ?><span class="counter-s" style="display:none">s</span> <?php echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); ?>
                                    </span>
                                </span>
                            </div>

                            <a
                                href="/index.php?option=com_axs&view=selectors&tmpl=component4&selection=groups&el=group&teamId=<?php echo $this->getTeam()->id; ?>"
                                class="tovuti-popup tld-select-field w-input group_selector"
                                data-width="800"
                                data-height="800"
                                data-type="iframe"
                            >
                                <?php echo AxsLanguage::text("AXS_GROUPS", "Select Groups") ?>
                            </a>
                        </div>
                    </div>

                    <div class="checkbox assign-type userlist_checkbox">
                        <b><?php echo AxsLanguage::text("AXS_SELECT_SPECIFIC_USERS", "Select Specific Users:") ?></b>
                        <br/><br/>
                        <input type="hidden" name="user[]" value=""/>
                        <div class="tld-search-field-block">
                            <div class="tld-search-icon-block tld_search">
                                <span id="user_counter" class="counter-box">
                                    <i class="fa fa-users"></i>
                                    <span class="counter-text" style="display:none">
                                        <span class="counter-count"></span> <?php echo AxsLanguage::text("AXS_USER_LOWERCASE", "user"); ?><span class="counter-s" style="display:none">s</span> <?php echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); ?>
                                    </span>
                                </span>
                            </div>

                            <a
                                href="/index.php?option=com_axs&view=selectors&tmpl=component4&selection=users&el=user&teamId=<?php echo $this->getTeam()->id; ?>"
                                class="tovuti-popup tld-select-field w-input user_selector"
                                data-width="800"
                                data-height="800"
                                data-type="iframe"
                            >
                                <?php echo AxsLanguage::text("AXS_SELECT_USERS", "Select Users") ?>
                            </a>
                        </div>
                    </div>

                    <div class="form-group">
                        <b><?php echo AxsLanguage::text("AXS_WHEN_ARE_THESE_COURSES_DUE_OPTIONAL", "When are these course(s) due? (optional)") ?></b>
                        <br/><br/>
                        <?php echo JHtml::_('calendar', "", 'due_date', 'due_date', '%Y-%m-%d', null); ?>
                    </div>
                    <div class="tld-search-field-block">
                        <button onclick="$('#assignment_form').submit()" id="assign_courses_button_submit" class="tld-select-field w-input form-button-green"><?php echo AxsLanguage::text("JSAVE", "SAVE"); ?></button>
                        <a href="javascript:void(0)" id="assign_courses_button_cancel" class="tld-select-field w-input extra-btn form-button-red">
                            <i class="fa fa-cancel"></i> <?php echo AxsLanguage::text("JCANCEL", "Cancel") ?>
                        </a>
                    </div>
                    <input type="hidden" value="<?php echo $addEncryptedParams; ?>" name="encode"/>
                </form>
                <?php } //end if allow_assigning_courses ?>
                <?php if($teamParams->allow_assigning_events) { ?>
                <form method="#" action="post" id="assignment_events_form" class="assignment_events_form tld-assign-form">
                    <h3><?php echo AxsLanguage::text("AXS_ASSIGN_EVENTS", "Assign Events") ?></h3>

                    <input type="hidden" name="event[]" value=""/>
                    <div class="tld-search-field-block">
                        <div class="tld-search-icon-block tld_search">
                            <span id="event_counter" class="counter-box">
                                <i class="fa fa-calendar-check-o"></i>
                                <span class="counter-text" style="display:none">
                                    <span class="counter-count"></span> <?php echo AxsLanguage::text("AXS_EVENT_LOWERCASE", "event"); ?><span class="counter-s" style="display:none">s</span> <?php echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); ?>
                                </span>
                            </span>
                            
                        </div>

                        <a
                            href="/index.php?option=com_axs&view=selectors&tmpl=component4&selection=events&el=event"
                            class="tovuti-popup tld-select-field w-input event_selector"
                            data-width="1000"
                            data-height="800"
                            data-type="iframe"
                        >
                            <?php echo AxsLanguage::text("AXS_SELECT_EVENTS", "Select Events") ?>
                        </a>
                    </div>


                    <input type="hidden" name="user_events[]" value=""/>
                    <div class="tld-search-field-block" style="margin-bottom: 25px; margin-top: 25px;">
                        <div class="tld-search-icon-block tld_search">
                            <span id="user_events_counter" class="counter-box">
                                <i class="fa fa-users"></i>
                                <span class="counter-text" style="display:none">
                                    <span class="counter-count"></span> <?php echo AxsLanguage::text("AXS_USER_LOWERCASE", "user"); ?><span class="counter-s" style="display:none">s</span> <?php echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); ?>
                                </span>
                            </span>
                        </div>

                        <a
                            href="/index.php?option=com_axs&view=selectors&tmpl=component4&selection=users&el=user_events&teamId=<?php echo $this->getTeam()->id; ?>"
                            class="tovuti-popup tld-select-field w-input user_events_selector"
                            data-width="800"
                            data-height="800"
                            data-type="iframe"
                        >
                            <?php echo AxsLanguage::text("AXS_SELECT_USERS_TO_REGISTER_FOR_THE_EVENTS", "Select Users to Register for the Events") ?>
                        </a>
                    </div>

                    <div class="tld-search-field-block">
                        <button type="button" onclick="$('#assignment_events_form').submit()" id="assign_courses_button_submit" class="tld-select-field w-input form-button-green"><?php echo AxsLanguage::text("JSAVE", "SAVE"); ?></button>
                        <a href="javascript:void(0)" id="assign_events_button_cancel" class="tld-select-field w-input extra-btn form-button-red">
                            <i class="fa fa-cancel"></i> <?php echo AxsLanguage::text("JCANCEL", "Cancel") ?>
                        </a>
                    </div>
                    <input type="hidden" value="<?php echo $addEncryptedParams; ?>" name="encode"/>
                </form>
                <?php } //end if allow_assigning_courses ?>
            </div>
        </div>
    </div>
</div>

<script>
    var encode = '<?php echo $addEncryptedParams; ?>';
    var teamType = '<?php echo $team->member_type; ?>';
</script>
<script src="/components/com_axs/assets/js/assignments.js?v=7" type="text/javascript"></script>
