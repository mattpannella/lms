<?php
    foreach($assignments as $assignment) {
        if($assignment->due_date > 0) {
            $assignment->due_date = date('Y-m-d',strtotime($assignment->due_date));
        } else {
            $assignment->due_date = "";
        }
        if($assignment->user_ids) {
            $assignment->assign_to = 'users';
        } else {
            $assignment->assign_to = 'groups';
        }
?>
    <div class="tld-team-member-container assignment-list-item" data-name="<?php echo $assignment->title; ?>">
        <div class="tld-team-user">
            <div class="tld-team-username"><?php echo $assignment->title; ?></div>
        </div>
        <div class="tld-team-user-options">
            <?php
                $actionParams->action = 'edit';
                $actionParams->assignment_id = $assignment->id;
                $encrytpedEditUser = base64_encode(AxsEncryption::encrypt($actionParams, $key));
            ?>
                <a
                    href="javascript:void(0)"
                    class="tld-team-user-options-item w-inline-block extra-btn assignment-edit-button"
                    data-encode="<?php echo $encrytpedEditUser; ?>"
                    data-values="<?php echo base64_encode(json_encode($assignment)); ?>"
                >
                    <i class="tld-team-user-option-icon tld-custom-icon fa fa-cog"></i>
                    <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("JACTION_EDIT", "Edit") ?></p>
                </a>


            <?php
                if(false) {
                    $actionParams->action = 'edit';
                    $actionParams->edit_user_id = $member->id;
                    $encrytpedEditUser = base64_encode(AxsEncryption::encrypt($actionParams, $key));
            ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=user_manager&action=<?php echo $encrytpedEditUser; ?>"
                    class="tld-team-user-options-item w-inline-block jcepopup noicon extra-btn"
                    data-mediabox="1"
                    data-mediabox-width="800"
                    data-mediabox-height="800"
                    type="iframe"
                >
                    <i class="tld-team-user-option-icon tld-custom-icon fa fa-list-alt"></i>
                    <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("AXS_VIEW_REPORT", "View Report") ?></p>
                </a>
            <?php } ?>

            <?php
                $actionParams->action = 'delete';
                $actionParams->assignment_id = $assignment->id;
                $encrytpedEditUser = base64_encode(AxsEncryption::encrypt($actionParams, $key));
            ?>
                <a
                    href="javascript:void(0)"
                    class="tld-team-user-options-item w-inline-block extra-btn assignment-delete-button"
                    data-encode="<?php echo $encrytpedEditUser; ?>"
                    data-values="<?php echo base64_encode(json_encode($assignment)); ?>"
                >
                    <i class="tld-team-user-option-icon fa fa-trash tld-custom-icon"></i>
                    <p class="tld-team-user-option-text"><?php echo AxsLanguage::text("JACTION_DELETE", "Delete") ?></p>
                </a>

        </div>
        <div data-hover="" data-delay="0" class="tld-user-dropdown w-dropdown">
            <div class="tld-user-dd-toggle w-dropdown-toggle">
                <div><?php echo AxsLanguage::text("AXS_USER_OPTIONS", "User Options") ?></div>
                <div class="tld-selector-icon">
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/icon_dropdown.svg" alt="" class="tld-dd-icon">
                </div>
            </div>
            <nav class="tld-user-dd-list tld-mobile-options">
                <?php if($teamParams->allow_adding_users) { ?>
                <a
                    href="<?php echo JRoute::_('index.php?option=com_axs&view=learner_dashboard&id=1') ?>&layout=user_manager&action=<?php echo $encrytpedEditUser; ?>"
                    class=" tld-dd-list-item jcepopup noicon"
                    data-mediabox="1"
                    data-mediabox-width="500"
                    data-mediabox-height="800"
                    type="iframe"
                >
                    <img src="/templates/axs/elements/lms/team_lead_dashboard/images/cog.svg" alt=""  class="tld-dd-user-image"">
                    <div><?php echo AxsLanguage::text("JGLOBAL_EDIT", "Edit") ?></div>
                </a>
            <?php } ?>
            </nav>
        </div>
    </div>
<?php } ?>