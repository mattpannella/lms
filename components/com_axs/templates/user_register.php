<?php

defined('_JEXEC') or die;

?>

<style>
.form-control {
    width: 100% !important;
    height: 38px !important;
    padding: 8px 12px !important;
    margin-bottom: 10px !important;
    font-size: 14px !important;
    line-height: 1.428571429 !important;
    color: #333333 !important;
    vertical-align: middle !important;
    background-color: #ffffff !important;
    border: 1px #cad8e4 solid !important;
    border-radius: 10px !important;
}
.w-form-done {
	display: none;
	padding: 10px;
	margin-bottom: 10px;
	background-color: lightgreen;
	color: white;
}
.w-form-fail {
	display: none;
	margin-top: 10px;
	padding: 10px;
	margin-bottom: 10px;
	background-color: #ff0000;
	color: white;
}
body {
	font-family: Arial, Helvetica, sans-serif !important;
}

.group_list li {
	font-size: 12px;
	color: #0b7ae6;
}
.radio-inline {
	display: inline-block;
	margin-right: 30px;
}

.rm_button {
    padding: 7px 15px 7px 15px;
    border: 1px solid #ff0000;
    color: #283848;
    font-size: 12px;
    text-align: center;
    margin-top: 10px;
    border-radius: 50px;
    text-decoration: none;
    background: #fff;
    transition: ease .5s all;
    margin: 0px;
    text-shadow: none;
    cursor: pointer;
	display: inline-block;
}

.rm_button:hover {
	background-color: #ff0000;
	color: #fff;
}

.btn-container {
	display: flex;
	justify-content: space-around;
}
</style>

<?php
	$firstName = '';
	$lastName  = '';
	$userName  = '';
	$email 	   = '';

	if($userManagerParams->action == 'edit') {
		if(!$userManagerParams->edit_user_id) {
			exit;
		}
		$title = AxsLanguage::text("AXS_EDIT_USER", "Edit User");
		$userEditObject = JFactory::getUser($userManagerParams->edit_user_id);
		$firstName = AxsUser::getFirstName($userEditObject->id);
		$lastName  = AxsUser::getLastName($userEditObject->id);
		$userName  = $userEditObject->username;
		$email 	   = $userEditObject->email;
		$editUserGroups = JAccess::getGroupsByUser($userEditObject->id);
	} else {
		$title = AxsLanguage::text("AXS_ADD_NEW_USER", "Add New User");
	}

?>

<div class="btn-container">
<?php if($userManagerParams->action == 'edit' && $teamParams->allow_removing_users_from_team) { ?>
	
	<a class="rm_button" href="/index.php?option=com_axs&task=team.removeUser&params=<?php echo $encryptedParams; ?>" target="_parent" onClick="return confirm(`<?php echo AxsLanguage::text("AXS_TEAM_USER_REMOVE_CONFIRM", "Do you really want to remove this user from your team?") ?>`)"><i class="fa fa-user"></i> <?php echo AxsLanguage::text("AXS_REMOVE_FROM_TEAM", "Remove From Team") ?></a>
<?php } ?>

<?php if($userManagerParams->action == 'edit'  && $teamParams->allow_removing_users_from_system) { ?>

	<?php 
		$user = JFactory::getUser();
		$isAdmin = AxsUser::isAdmin($user->id);
		$isSuperUser = AxsUser::isSuperUser($user->id);
		$canDelete = $isAdmin || $isSuperUser;

		$href = '';
		$class = '';
		$onclick = '';
		if ($canDelete) {
			$confirmText = AxsLanguage::text("AXS_COMPLETELY_DELETE_USER_CONFIRM", "Do you really want to completely delete this user? This action cannot be undone and the user will be fully deleted from the system.");
			$onclick = "return confirm(`$confirmText`)";
			$href = "href=\"/index.php?option=com_axs&task=team.deleteUser&params=$encryptedParams\"";
			$class = 'rm_button';
		} else {
			$href = "#";
			$class = 'disabled rm_button';
		}
	?>
	<a class="<?php echo $class ?>" <?php echo $href ?> target="_parent" onClick="<?php echo $onclick ?>"><i class="fa fa-trash"></i> <?php echo AxsLanguage::text("AXS_COMPLETETLY_DELETE_USER", "Completely Delete User") ?></a>
<?php } ?>
</div>
<?php if (!$canDelete  && $teamParams->allow_removing_users_from_system): ?>
	<br />
	<div style="font-size:small;color:lightslategray;float:right">
		<?php echo AxsLanguage::text("AXS_TEAM_LEADS_REQUIRE_ADMIN_PRIVILEDGES", "Team Leads must have administrator priviledges to delete users.<br/>Contact your site administrator.") ?>
	</div>
<?php endif; ?>
<div class="clearfix"></div>
<h1><?php echo $title; ?></h1>
<div class="w-form-done"></div>
<div class="w-form-fail"></div>
<form id="tov_registration-form" action="#" method="post" aria-label="Registration Form" data-name="Registration Form" class="registration_form_content">
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_FIRST_NAME", "Enter Your First Name") ?>" placeholder="<?php echo AxsLanguage::text("AXS_FIRST_NAME", "First Name") ?>" id="register-firstname" name="register_firstname" type="text" class="form-control" value="<?php echo $firstName; ?>">
  	</div>
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_LAST_NAME", "Enter Your Last Name") ?>" placeholder="<?php echo AxsLanguage::text("AXS_LAST_NAME", "Last Name") ?>" id="register-lastname" name="register_lastname" type="text" class="form-control" value="<?php echo $lastName; ?>">
  	</div>
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_EMAIL", "Enter Your Email") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_EMAIL", "Email") ?>" id="register-email"  name="register_email" type="text" class="form-control" value="<?php echo $email; ?>">
  	</div>
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_USERNAME", "Enter Your Username") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_USERNAME", "Username") ?>" id="register-username" type="text" name="register_username" class="form-control" value="<?php echo $userName; ?>">
  	</div>
  	<div class="clearfix"></div>
  	<div class="passswordMismatchMessage"></div>
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_ENTER_YOUR_PASSWORD", "Enter Your Password") ?>" placeholder="<?php echo AxsLanguage::text("JGLOBAL_PASSWORD", "Password") ?>" id="register-password" type="password" name="register_password" class="form-control">
  	</div>
  	<div class="form-group">
    	<input aria-label="<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>" placeholder="<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>" id="register-password2" type="password" name="register_password2" class="form-control">
	</div>
	<hr>
	<div><?php echo AxsLanguage::text("AXS_REQUIRE_PASSWORD_RESET", "Require Password Reset:") ?></div>
	<div class="radio">
		<label class="radio-inline"><input type="radio" name="password_reset" value="yes" <?php if($userManagerParams->action == 'register') { echo 'checked'; } ?>><?php echo AxsLanguage::text("JYES", "Yes") ?></label>
		<label class="radio-inline"><input type="radio" name="password_reset" value="no"
		<?php if($userManagerParams->action == 'edit') { echo 'checked'; } ?>><?php echo AxsLanguage::text("JNO", "No") ?></label>
	</div>
	<hr>
	<?php
		if($teamParams->allow_managing_users_groups && $teamParams->user_groups) {
			if($team->member_type == 'groups') {
				$requiredGroups = explode(',',$team->user_groups);
				echo AxsLanguage::text("AXS_MUST_ASSIGN_AT_LEAST_ONE_GROUP", "To be a part of this team you must assign at least one of the following groups:");
				echo '<ul class="group_list">';
				foreach($requiredGroups as $group) {
					if(AxsExtra::getUsergroupName($group) != "")
					{
						echo "<li>".AxsExtra::getUsergroupName($group)."</li>";
					}
				}
				echo "</ul>";
			}
	?>
	<hr>
	<h3><?php echo AxsLanguage::text("AXS_ASSIGN_USER_GROUPS", "Assign User Groups") ?></h3>
	<table style="margin-bottom:10px;">
		<?php
			$i = 0;
			foreach($teamParams->user_groups as $groupId) {
				$checked = "";
				$groupName = AxsExtra::getUsergroupName($groupId);
				if($groupName=="")
				{
					continue;
				}
				if($userManagerParams->action == 'edit') {
					if(in_array($groupId,$editUserGroups)) {
						$checked = "checked";
					}
				}
				if (count($teamParams->user_groups) > 10 && ($i == 0 || $i == count($teamParams->user_groups) / 2)) {
					?> <td style="width:50%"> <?php
				}
		?>
			<span style="margin-right:50px">
				<?php echo $groupName; ?>
				<input style="min-height:unset !important" aria-label="groups" id="groups" type="checkbox" name="groups[]" value="<?php echo $groupId; ?>" <?php echo $checked; ?>>
			</span>
				<?php
				if (count($teamParams->user_groups) > 10) {
					echo '<br/>';
				}
				if (count($teamParams->user_groups) > 10 && ($i == (count($teamParams->user_groups) / 2 - 1) || $i == count($teamParams->user_groups) - 1)) {
					?> </td> <?php
				}
				$i++;
				} //end foreach
			} //end if
		?>
  	</table>
  	<div class="form-group">
    	<input type="submit" value="<?php echo AxsLanguage::text("JSUBMIT", "Submit") ?>" id="register_button_submit" class="btn btn-default">
  	</div>
	<?php echo JHtml::_('form.token'); ?>
	<div class="form-group">
    	<input type="hidden" name="params" value="<?php echo $encryptedParams; ?>">
  	</div>
</form>
<script>
    $('input[name="register_password2"').keyup(function() {
        var pass2 = $(this).val();
        var pass1 = $('input[name="register_password"').val();
        if(pass2.length > 2) {
            if(pass2 != pass1) {
                var passswordMismatchMessage = "<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH_MESSAGE", "Passwords do not match.") ?>";
                $('.passswordMismatchMessage').text(passswordMismatchMessage);
                $('.passswordMismatchMessage').fadeIn(300);
            } else {
                $('.passswordMismatchMessage').fadeOut(300);
                $('.passswordMismatchMessage').text('');
            }
        }
    });

    function validateRegistrationFields() {
        var register_firstname	= $('input[name=register_firstname]').val();
        var register_lastname	= $('input[name=register_lastname]').val();
        var register_email		= $('input[name=register_email]').val();
        var register_username	= $('input[name=register_username]').val();
        var register_password	= $('input[name=register_password]').val();
        var register_password2	= $('input[name=register_password2]').val();
        var response = [];
        response['valid'] = true;
        
		if(!register_firstname) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_FIRST_NAME", "First Name"); ?>";
            response['valid'] = false;
        } else if(!register_lastname) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_LAST_NAME", "Last Name"); ?>";
            response['valid'] = false;
        } else if(!register_email) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_EMAIL", "Email"); ?>";
            response['valid'] = false;
        } else if(!register_username) {
            response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_USERNAME", "Username"); ?>";
            response['valid'] = false;
		}

		<?php if($userManagerParams->action == 'register') : ?>
			if(!register_password) {
				response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("JGLOBAL_PASSWORD", "Password") ?>";
				response['valid'] = false;
			} else if(!register_password2) {
				response['message'] = "<?php echo AxsLanguage::text("AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field:") . ' ' . AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>";
				response['valid'] = false;
			} else if(register_password != register_password2) {
				response['message'] = "<?php echo AxsLanguage::text("AXS_PASSWORDS_DO_NOT_MATCH", "Passwords Do Not Match") ?>";
				response['valid'] = false;
			}
		<?php endif; ?>
        return response;
    }

    $('#tov_registration-form').submit(function(ev) {
        ev.preventDefault();
        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }
       // $('#register_button_submit').attr('disabled','true');
        var response = validateRegistrationFields();
        if(!response['valid']) {
            $('#register_button_submit').removeAttr('disabled');
            $(".w-form-fail").html(response['message']);
            if($(".w-form-fail:hidden")) {
                $(".w-form-fail").slideToggle();
            }
            return;
        }
        var data = $('#tov_registration-form').serialize();

        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }

        $.ajax({
            url: '/index.php?option=com_axs&task=team.update&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                var response = JSON.parse(result);
                if (response.status == "success") {
					window.top.location.reload();
                } else {
                    $('#register_button_submit').removeAttr('disabled');
                    $(".w-form-fail").html(response.error);
                    if($(".w-form-fail:hidden")) {
                        $(".w-form-fail").slideToggle();
                    }
                }
        });
	});
</script>

