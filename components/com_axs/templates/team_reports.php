<?php

defined('_JEXEC') or die;

$reportParams = new stdClass();
$reportParams->team_id = (int)$this->getTeam()->id;
$reportParams->learnerDashboard_id = (int)$this->getDashboard()->id;
$reportParams->report = 'attendance';
$attendanceEncryptedParams = base64_encode(AxsEncryption::encrypt($reportParams, $key));
$reportParams->report = 'activity';
$activityeEncryptedParams = base64_encode(AxsEncryption::encrypt($reportParams, $key));
$reportParams->report = 'team_learner_summary';
$teamLearnerSummaryEncryptedParams = base64_encode(AxsEncryption::encrypt($reportParams, $key));
$dashboardParams = $this->getDashboardParams();
?>
<div class="tld-leaderboard-container">
    <div class="tld-team-wrapper">
        <div class="tld-section-top">
            <div class="tld-team-top-heading"  style="text-align: center !important; width: 100%;"><?php echo AxsLanguage::text("AXS_REPORTING", "Reporting") ?></div>
        </div>
        <!-- <div class="tld-reporting-container">
            <span class="tld-team-top-heading subheading">Users</span>
            <div class="tld-reporting-options">
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>User Reports</div>
            </a>
            </div>
        </div> -->
        <!--  <div class="tld-reporting-container">
            <span class="tld-team-top-heading subheading">Events</span>
            <div class="tld-reporting-options">
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Registrants</div>
            </a>
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Attendance</div>
            </a>
            </div>
        </div> -->
        <div class="tld-reporting-container">
            <span class="tld-team-top-heading subheading"><?php echo AxsLanguage::text("TPL_MWM_GIN_EDUCATION", "Education") ?></span>
            <div class="tld-reporting-options">
                <?php if($dashboardParams->show_attendance) { ?>
                <a
                    href="<?php echo JRoute::_("?index.php&option=com_axs&view=learner_dashboard&id=1") ?>&layout=report&report=<?php echo $attendanceEncryptedParams; ?>"
                    class="jcepopup noicon tovuti_popup tld-reporting-button w-inline-block"
                    data-mediabox="1"
                    type="iframe"
                >
                    <div><?php echo AxsLanguage::text("AXS_VIRTUAL_CLASS_SLASH_MEETING_ATTENDANCE", "Virtual Class/Meeting Attendance") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_activity_report) { ?>
                <a
                    href="<?php echo JRoute::_("?index.php&option=com_axs&view=learner_dashboard&id=1") ?>&layout=report&report=<?php echo $activityeEncryptedParams; ?>"
                    class="jcepopup noicon tovuti_popup tld-reporting-button w-inline-block"
                    data-mediabox="1"
                    type="iframe"
                >
                    <div><?php echo AxsLanguage::text("AXS_TEAM_ACTIVITY", "Team Activity") ?></div>
                </a>
                <?php } ?>
                <?php if($dashboardParams->show_team_learner_summary_report && $dashboardParams->team_learner_summary_report) { ?>
                <a
                    href="<?php echo JRoute::_("?index.php&option=com_axs&view=learner_dashboard&id=1") ?>&layout=report&report=<?php echo $teamLearnerSummaryEncryptedParams; ?>"
                    class="jcepopup noicon tovuti_popup tld-reporting-button w-inline-block"
                    data-mediabox="1"
                    type="iframe"
                >
                    <div><?php echo AxsLanguage::text("AXS_TEAM_LEARNER_SUMMARY", "Team Learner Summary") ?></div>
                </a>
                <?php } ?>
            <!--  <a href="#" class="tld-reporting-button w-inline-block">
                <div>Learner Progress</div>
            </a>
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Assigned Courses</div>
            </a>
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Badges</div>
            </a>
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Certifications</div>
            </a>
            <a href="#" class="tld-reporting-button w-inline-block">
                <div>Milestones</div>
            </a> -->
            </div>
        </div>
    </div>
</div>