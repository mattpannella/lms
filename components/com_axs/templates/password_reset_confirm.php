<?php 
    $app = JFactory::getApplication();
    $token = $app->input->get('token','','STRING');
?>
<p class="tov_login-p password_reset_content"><?php echo AxsLanguage::text("AXS_VERIFICATION_CODE_SENT", "An email has been sent to your email address. The email contains a verification code, please paste the verification code in the field below to prove that you are the owner of this account.") ?></p>
<form id="tov_remind-form" action="<?php echo JRoute::_('index.php?option=com_users&task=reset.confirm&form=tovuti_loginpage'); ?>" method="post" class="tov_login-form-block-container password_reset_content" style="display:none;">
    <input type="text" class="tov_login-form-field w-input" maxlength="256" name="jform[username]" data-name="Enter Your Username" placeholder="Enter Your Username" id="remind_username" required="required" aria-required="true" aria-invalid="true">
    <input type="text" class="tov_login-form-field w-input" maxlength="256" name="jform[token]" data-name="Verification Code" placeholder="Verification Code" id="remind_code" required="required" aria-required="true" aria-invalid="true" value="<?php if($token) { echo $token; } ?>">
    <input type="hidden" name="jform[login_page_url]" id="login_page_url" value="<?php echo base64_encode($currentLoginPageUrl); ?>" />
    <div class="tov_login-button-container">
        <div class="tov_login-horizontal-wrapper">
            <input type="submit" value="Submit Verification" data-wait="Please wait..." id="reset_password_button" class="tov_login-button w-button">
        </div>
    </div>    
    <?php echo JHtml::_('form.token'); ?>
</form>