<?php
    defined('_JEXEC') or die;

    $quiz = $this->getQuiz();
    $userId = JFactory::getUser()->id;

    $questionNumber = $quiz->question_number;
    $question = $this->getCurrentQuestion();
?>

<?php if(!empty($userId)) : ?>

<link rel="stylesheet" type="text/css" href="/components/com_axs/assets/css/quizzes.css">

<div class="container">
    <div class="row tovuti-quiz-container">
        <div class="col-sm-8 col-md-8 col-lg-8 tovuti-quiz-panel tovuti-quiz-main-panel">
            <div class="tovuti-intro">
                <div class="title">
                    <?php echo $this->quiz->title; ?>
                </div>

                <div class="description">
                    <?php echo $this->quiz->description; ?>
                </div>
            </div>

            <div class="tovuti-question-container">
                <?php echo $this->buildQuestionHtml($questionNumber); ?>
            </div>

            <?php if(!$this->hasQuizBeenCompleted()) : ?>

            <div class="tovuti-submit-container">
                <button class='btn btn-submit btn-primary'>Submit Response</button>
                <div id='statusMessage'></div>
            </div>

            <?php endif; ?>
        </div>
        <div class="col-sm-4 col-md-4 col-lg-4 tovuti-quiz-panel tovuti-quiz-completion-panel">
            <div class="tovuti-completion-status">
                <?php
                    if($this->hasQuizBeenCompleted()) {
                        echo $this->buildQuizStats();
                    }
                ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="/components/com_axs/assets/js/quizzes.js"></script>
<?php endif; ?>