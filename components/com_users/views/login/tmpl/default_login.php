<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
$security = AxsSecurity::getSettings();
if($security->twofactor_type == 'yubikey') {
	$sercretKeyInputType = 'password';
} else {
	$sercretKeyInputType = 'text';
}
?>
<div class="login<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>

	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
		<div class="login-description">
		<?php endif; ?>

		<?php if ($this->params->get('logindescription_show') == 1) : ?>
			<?php echo $this->params->get('login_description'); ?>
		<?php endif; ?>

		<?php if ($this->params->get('login_image') != '') : ?>
			<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JText::_('COM_USERS_LOGIN_IMAGE_ALT'); ?>" />
		<?php endif; ?>

		<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
		</div>
	<?php endif; ?>

	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="form-validate form-horizontal well" id="tov_login-form">
		<div class="w-form-fail" style="display: none; color:red; margin-bottom:10px; font-weight: bold; "></div>
		<fieldset>
			<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
				<?php if (!$field->hidden) : ?>
					<div class="control-group">
						<div class="control-label">
							<?php echo $field->label; ?>
						</div>
						<div class="controls">
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>

			<?php if (count($twofactormethods) > 1) { ?>
				<div class="control-group" id="login_secretkey" style="display:none;">
					<div class="control-label">
						<label id="secretkey-lbl" for="secretkey" aria-invalid="false"><?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key") ?></label>
					</div>
					<div class="controls">
						<input type="<?php echo $sercretKeyInputType; ?>" name="secretkey" id="secretkey" value="" size="25" aria-invalid="false">
					</div>
				</div>
			<?php } ?>

			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
				<div class="control-group">
					<div class="controls"><input id="remember-me" type="checkbox" name="remember" aria-checked="false" class="inputbox" />
						<label for="remember-me" class="control-label"><?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME') ?></label>
					</div>
					<div></div>
				</div>
			<?php endif; ?>

			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-primary">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>
			</div>

			<?php $return = $this->form->getValue('return', '', $this->params->get('login_redirect_url', $this->params->get('login_redirect_menuitem'))); ?>
			<input type="hidden" name="return" id="login_return" value="<?php echo base64_encode($return); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</form>
</div>
<div>
	<p class="forgot-links" style="text-align:center;">
		<a tabindex="0" href="/forgot-password">
			<?php echo AxsLanguage::text("COM_USERS_LOGIN_RESET", "Forgot your password?") ?>
		</a> |
		<a tabindex="0" href="/forgot-username">
			<?php echo AxsLanguage::text("COM_USERS_LOGIN_REMIND", "Forgot your username?") ?>
		</a>
	</p>
	<ul class="nav nav-tabs nav-stacked">
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?></a>
		</li>
		<li>
			<a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?></a>
		</li>
		<?php
		$usersConfig = JComponentHelper::getParams('com_users');
		if ($usersConfig->get('allowUserRegistration')) : ?>
			<li>
				<a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
					<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?></a>
			</li>
		<?php endif; ?>
	</ul>
</div>
<script>
    $("#tov_login-form").submit( function(e) {
        e.preventDefault();
        $('#login_button').attr('disabled','true');
        //var data = $(this).serialize();
        var data = {
            username: $("#username").val(),
            password: $("#password").val(),
            secretkey: $("#secretkey").val(),
            return: $("#login_return").val()
        }

        if($(".w-form-fail:visible")) {
            $(".w-form-fail").hide();
        }

        $.ajax({
            url: '/index.php?option=com_axs&task=popupmodule.loginAuthenticate&format=raw',
            type: 'post',
            data: data
        }).done(
            function(result) {
                var response = JSON.parse(result);
                if(response.secretKeyVerification) {
					$('#login_button').removeAttr('disabled');
					$(".w-form-fail").html(response.message);
					if($(".w-form-fail").is(':hidden')) {
						$(".w-form-fail").slideToggle();
					}
					if($("#login_secretkey").is(':hidden')) {
						$('#login_secretkey').slideToggle();
					}
					return;
                }
                if (response.status == "success") {

                    $.ajax({
                        url: '/index.php?option=com_users&task=user.login',
                        type: 'post',
                        data: data
                    }).done( function() {
                        window.location = "/";
                    });
                } else {
                    $('#login_button').removeAttr('disabled');
                    $(".w-form-fail").html(response.message);
                    if($(".w-form-fail").is(':hidden')) {
                        $(".w-form-fail").slideToggle();
                    }
                }
        });
    });
</script>