<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Category
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/

/** @var KunenaForumCategory $section */
/** @var KunenaForumCategory $category */
/** @var KunenaForumCategory $subcategory */

defined('_JEXEC') or die;

if ($this->config->enableforumjump)
{
	echo $this->subLayout('Widget/Forumjump')->set('categorylist', $this->categorylist);
}

$mmm = 0;
$config = KunenaFactory::getTemplate()->params;

if ($config->get('displayModule'))
{
	echo $this->subLayout('Widget/Module')->set('position', 'kunena_index_top');
}

foreach ($this->sections as $section) :
	$markReadUrl = $section->getMarkReadUrl();
	$count = 0;

	if ($config->get('displayModule'))
	{
		echo $this->subLayout('Widget/Module')->set('position', 'kunena_section_top_' . ++$mmm);
	} ?>

	<div class="kblock">

		<div class="kheader">
			<h2><span><?php echo $this->getCategoryLink($section, $this->escape($section->name), null, 'hasTooltip', true, false); ?></span></h2>

			<?php if (!empty($section->description)) : ?>
				<div class="ktitle-desc"><?php echo $section->displayField('description'); ?></div>
			<?php endif; ?>
		</div>
		
		<div class="kcontainer collapse in section <?php if (!empty($section->class)) : ?>section<?php echo $this->escape($section->class_sfx); ?><?php endif;?>" id="section<?php echo $section->id; ?>">
			<div class="kbody">
      <ul class="nts-kblocktable kblocktable clearfix <?php echo $this->escape($section->class_sfx); ?>">
				<?php if ($section->isSection() && empty($this->categories[$section->id]) && empty($this->more[$section->id])) : ?>
					<li>
						<h4><?php echo JText::_('COM_KUNENA_GEN_NOFORUMS'); ?></h4>
					</li>
				<?php else : ?>

				<?php
					foreach ($this->categories[$section->id] as $category) : ?>
						<?php
							$count++;

							if (is_file( JPATH_ROOT."/media/kunena/category_images/{$category->id}.png" )) {
							  $imgPath = JURI::root(true)."/media/kunena/category_images/{$category->id}.png";
							} else {
							  $imgPath = "";
							}
						?>
						<li class="<?php if($imgPath) echo 'cat-img' ?> category<?php echo $this->escape($category->class_sfx); ?> <?php if($count % 2 == 0) {echo "even"; } else { echo "odd"; } ?>" id="category<?php echo $category->id; ?>">
							<div class="inner clearfix col<?php echo ($count % 2)?'1':'2'; ?>">

              <div class="kshort-left">
                <div class="kcol-first kcol-category-icon">
                  <?php echo $this->getCategoryLink($category, $this->getCategoryIcon($category, null, true, false)); ?>
                </div>

                <!-- Category title -->
                <div class="kthead-title">
                  <span class="cat-title">
                    <?php echo $this->getCategoryLink($category, $category->name, null, KunenaTemplate::getInstance()->tooltips(), true, false); ?>
                  </span>
                </div>
                <!-- // Category title -->

                <div class="kcol-kcattopics">
                  <span class="kcat-topics-number"><?php echo $this->formatLargeNumber($category->getTopics()); ?></span>
                  <span class="kcat-topics"><?php if ($this->formatLargeNumber($category->getTopics()) > 1) echo JText::_('COM_KUNENA_X_TOPICS'); else echo JText::_('COM_KUNENA_X_TOPIC'); ?></span>
                </div>

                <div class="kcol-kcatreplies">
                  <span class="kcat-replies-number"><?php echo $this->formatLargeNumber($category->getPosts()); ?></span>
                  <span class="kcat-replies"><?php if ($this->formatLargeNumber($category->getPosts()) > 1) echo JText::_('COM_KUNENA_POSTS'); else echo JText::_('COM_KUNENA_POST'); ?></span>
                </div>
              </div>

              <?php if($imgPath): ?>
              <div class="cat-bg" style="background-image: url(<?php echo $imgPath; ?>);">
                <span class="mask"></span>
              </div>
              <?php endif ?>

              <?php if (!empty($category->description)) { ?>
                <div class="kthead-desc"><?php echo $category->displayField('description'); ?></div>
              <?php } else { ?>
                <div class="no-desc"></div>
              <?php } ?>

                <?php $last = $category->getLastTopic(); ?>

                <?php if ($last->exists()) :
                  $author = $last->getLastPostAuthor();
                  $time   = $last->getLastPostTime();
                  $this->ktemplate = KunenaFactory::getTemplate();
                  $avatar = $this->config->avataroncat ? $author->getAvatarImage($this->ktemplate->params->get('avatarType'), 'post') : null;
                ?>

              <div class="full-view">
                <div class="khead-child">
                  <?php
                  // Display subcategories
                  if (!empty($this->categories[$category->id])) : ?>
                    <div class="subcat-wrap clearfix">
                      <ul class="inline">

                        <?php foreach ($this->categories[$category->id] as $subcategory) : ?>
                          <li>
                            <?php $totaltopics = $subcategory->getTopics() > 0 ?  JText::plural('COM_KUNENA_X_TOPICS_MORE', $this->formatLargeNumber($subcategory->getTopics())) : JText::_('COM_KUNENA_X_TOPICS_0'); ?>

                            <?php if (KunenaConfig::getInstance()->showchildcaticon) : ?>
                              <?php echo $this->getCategoryLink($subcategory,  $this->getSmallCategoryIcon($subcategory), '', null, true, false) . $this->getCategoryLink($subcategory, '', null, KunenaTemplate::getInstance()->tooltips(), true, false) . '<small class="hidden-xs muted"> ('
                                . $totaltopics . ')</small>';
                            else : ?>
                              <?php echo $this->getCategoryLink($subcategory, '', null, KunenaTemplate::getInstance()->tooltips(), true, false) . '<small class="hidden-xs muted"> ('
                                . $totaltopics . ')</small>';
                            endif;

                            if (($new = $subcategory->getNewCount()) > 0)
                            {
                              echo '<sup class="knewchar">(' . $new . ' ' . JText::_('COM_KUNENA_A_GEN_NEWCHAR') . ')</sup>';
                            }
                            ?>
                          </li>
                        <?php endforeach; ?>

                        <?php if (!empty($this->more[$category->id])) : ?>
                          <li>
                            <?php echo $this->getCategoryLink($category, JText::_('COM_KUNENA_SEE_MORE'), null, null, true, false); ?>
                            <small class="hidden-xs muted">
                              (<?php echo JText::sprintf('COM_KUNENA_X_HIDDEN', (int) $this->more[$category->id]); ?>)
                            </small>
                          </li>
                        <?php endif; ?>

                      </ul>
                    </div>
                  <?php endif; ?>
                </div>

                <div class="kcol-mid kcol-kcatlastpost">
                  <?php if ($avatar) : ?>
                    <span class="klatest-avatar"><?php echo $author->getLink($avatar); ?></span>
                  <?php endif; ?>
                  <span class="klatest-subject ks">
                    <?php echo $this->getLastPostLink($category,null, null, KunenaTemplate::getInstance()->tooltips(), null, false, true) ?>
                  </span>

                  <div class="klatest-subject-by">
                    <?php echo $author->getLink(null, '', '', '', null, $category->id); ?>
                    <span class="post-date"><?php echo $time->toKunena('config_post_dateformat'); ?></span>
                  </div>
                </div>
              </div>


                <?php else : ?>
                  <div class="kcol-knoposts"><?php echo JText::_('COM_KUNENA_X_TOPICS_0'); ?></div>
                <?php endif; ?>

		          </div>
						</li>

					<?php endforeach; ?>
				<?php endif; ?>

				<?php if (!empty($this->more[$section->id])) : ?>
					<li>
						<h4>
							<?php echo $this->getCategoryLink($section, JText::sprintf('COM_KUNENA_SEE_ALL_SUBJECTS')); ?>
							<small>(<?php echo JText::sprintf('COM_KUNENA_X_HIDDEN', (int) $this->more[$section->id]); ?>)</small>
						</h4>
					</li>
				<?php endif; ?>

			</ul>
      </div>
		</div>
	</div>
	<!-- Begin: Category Module Position -->
	<?php
	if ($config->get('displayModule'))
	{
		echo $this->subLayout('Widget/Module')->set('position', 'kunena_section_' . ++$mmm);
	} ?>
	<!-- Finish: Category Module Position -->
<?php endforeach;

if ($config->get('displayModule'))
{
	echo $this->subLayout('Widget/Module')->set('position', 'kunena_index_bottom');
}
