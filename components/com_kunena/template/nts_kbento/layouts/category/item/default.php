<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Category
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

$categoryActions = $this->getCategoryActions();
$cols = empty($this->checkbox) ? 5 : 6;
$this->addStyleSheet('assets/css/rating.css');
?>

<?php if ($this->category->headerdesc) : ?>
	<div class="alert alert-info">
		<a class="close" data-dismiss="alert" href="#"></a>
		<?php echo $this->category->displayField('headerdesc'); ?>
	</div>
<?php endif; ?>

<?php if (!$this->category->isSection()) : ?>

<?php if (!empty($this->topics)) : ?>
<div class="row-actions clearfix">
	<div class="float-l nts-pagination">
			<?php echo $this->subLayout('Widget/Pagination/List')
	->set('pagination', $this->pagination)
	->set('display', true); ?>
	</div>

	<div class="hidden-xs float-r">
			<?php echo $this->subLayout('Widget/Search')
	->set('catid', $this->category->id)
	->setLayout('topic'); ?>
	</div>
</div>
<?php endif; ?>

<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena'); ?>" method="post" id="categoryactions">
	<input type="hidden" name="view" value="topics" />
	<?php echo JHtml::_('form.token'); ?>
	<div>
		<ul class="inline">
			<?php if ($categoryActions) : ?>
				<li>
					<?php echo implode($categoryActions); ?>
				</li>
			<?php endif; ?>
		</ul>
	</div>
	<?php if ($this->topics) : ?>
	<table class="kblocktable">
		<thead>
		<tr>
			<th class="kcol-topic-icon center">
				<a id="forumtop"> </a>
				<a href="#forumbottom"><i class="fa fa-arrow-down hasTooltip"></i></a>
			</th>
			<th class="kcol-topic-title"><?php echo JText::_('COM_KUNENA_GEN_SUBJECT'); ?></th>
			<th class="kcol-topic-replies"><?php echo JText::_('COM_KUNENA_GEN_REPLIES'); ?></th>
			<th class="kcol-topic-views"><?php echo JText::_('COM_KUNENA_GEN_HITS'); ?></th>
			<th class="kcol-topic-lastpost"><?php echo JText::_('COM_KUNENA_GEN_LAST_POST'); ?></th>

			<?php if (!empty($this->topicActions)) : ?>
			<th class="kcol-topic-action text-center"><label><input class="kcheckall" type="checkbox" name="toggle" value="" /></label></th>
			<?php endif; ?>
		</tr>
		</thead>
		<?php endif; ?>

		<?php
			/** @var KunenaForumTopic $previous */
			$previous = null;

			foreach ($this->topics as $position => $topic)
			{
				echo $this->subLayout('Topic/Row')
					->set('topic', $topic)
					->set('spacing', $previous && $previous->ordering != $topic->ordering)
					->set('position', 'kunena_topic_' . $position)
					->set('checkbox', !empty($this->topicActions))
					->setLayout('category');
				$previous = $topic;
			}
		?>
		<tfoot>
		<?php if ($this->topics) : ?>
		<tr>
			<td class="center hidden-xs">
				<a id="forumbottom"> </a>
				<a href="#forumtop" rel="nofollow">
					<span class="divider"></span>
					<i class="glyphicon glyphicon-arrow-up hasTooltip"></i>
				</a>
				<?php // FIXME: $this->displayCategoryActions() ?>
			</td>
			<td colspan="7" class="hidden-xs">
				<div class="form-group">
					<div class="input-group" role="group">
						<div class="input-group-btn">
							<?php if (!empty($this->moreUri))
							{
								echo JHtml::_(
         'kunenaforum.link', $this->moreUri,
		JText::_('COM_KUNENA_MORE'), null, null, 'follow');
							} ?>

							<?php if (!empty($this->topicActions)) : ?>
								<?php echo JHtml::_(
	'select.genericlist', $this->topicActions, 'task',
'class="form-control kchecktask"', 'value', 'text', 0, 'kchecktask'); ?>

								<?php if ($this->actionMove) : ?>
									<?php
									$options = array(JHtml::_('select.option', '0', JText::_('COM_KUNENA_BULK_CHOOSE_DESTINATION')));
									echo JHtml::_(
										'kunenaforum.categorylist', 'target', 0, $options, array(),
										'class="form-control fbs" disabled="disabled"', 'value', 'text', 0,
										'kchecktarget'
									);
									?>
									<button class="btn btn-default" name="kcheckgo" type="submit"><?php echo JText::_('COM_KUNENA_GO') ?></button>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</td>
		</tr>
		<?php endif; ?>
		</tfoot>
		<?php endif; ?>
	</table>
</form>

<?php if ($this->topics) : ?>
	<div class="row-actions clearfix">
		<div class="nts-pagination">
			<?php echo $this->subLayout('Widget/Pagination/List')
		->set('pagination', $this->pagination)
		->set('display', true); ?>
		</div>
	</div>
<?php endif; ?>

<?php if (!empty($this->moderators)) {
	echo $this->subLayout('Category/Moderators')
		->set('moderators', $this->moderators);
}
?>

<?php if ($this->ktemplate->params->get('writeaccess')) : ?>
<div><?php echo $this->subLayout('Widget/Writeaccess')->set('id', $this->category->id); ?></div>
<?php endif; ?>
