<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Message
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

// @var KunenaLayout $this


// @var KunenaForumMessage  $message  Message to reply to.

$message = $this->message;

if (!$message->isAuthorised('reply'))
{
	return;
}

// @var KunenaUser  $author  Author of the message.

$author = isset($this->author) ? $this->author : $message->getAuthor();
// @var KunenaForumTopic  $topic Topic of the message.

$topic = isset($this->topic) ? $this->topic : $message->getTopic();
// @var KunenaForumCategory  $category  Category of the message.

$category = isset($this->category) ? $this->category : $message->getCategory();
// @var KunenaConfig  $config  Kunena configuration.

$config = isset($this->config) ? $this->config : KunenaFactory::getConfig();
// @var KunenaUser  $me  Current user.

$me = isset($this->me) ? $this->me : KunenaUserHelper::getMyself();

$this->addScript('assets/js/edit.js');

if (KunenaFactory::getTemplate()->params->get('formRecover'))
{
	$this->addScript('assets/js/sisyphus.js');
}

// Fixme: can't get the controller working on this
if ($me->canDoCaptcha() )
{
	if (JPluginHelper::isEnabled('captcha'))
	{
		$plugin = JPluginHelper::getPlugin('captcha');
		$params = new JRegistry($plugin[0]->params);

		$captcha_pubkey = $params->get('public_key');
		$catcha_privkey = $params->get('private_key');

		if (!empty($captcha_pubkey) && !empty($catcha_privkey))
		{
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$result = $dispatcher->trigger('onInit', 'dynamic_recaptcha_' . $this->message->id);
			$output = $dispatcher->trigger('onDisplay', array(null, 'dynamic_recaptcha_' . $this->message->id,
				'class="controls g-recaptcha" data-sitekey="' . $captcha_pubkey . '" data-theme="light"'));
			$this->quickcaptchaDisplay = $output[0];
			$this->quickcaptchaEnabled = $result[0];
		}
	}
}

$template = KunenaTemplate::getInstance();
$quick = $template->params->get('quick');
?>

<?php if ($quick == 1) : ?>
<div class="modal fade" id="kreply<?php echo $message->displayField('id'); ?>_form" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display:none;">
	<div class="modal-dialog" role="document">
		<div class="modal-content">

<?php elseif ($quick == 0) : ?>
<div class="qreplyform" id="kreply<?php echo $message->displayField('id'); ?>_form"  style="position: relative; z-index: 1; display: none">
	<div class="panel panel-default" style="width:100% !important;">
		<div class="panel-body">
<?php endif;?>
			<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=topic'); ?>" method="post"
				enctype="multipart/form-data" name="postform" id="postform" class="form-horizontal">
				<input type="hidden" name="task" value="post" />
				<input type="hidden" name="parentid" value="<?php echo $message->displayField('id'); ?>" />
				<input type="hidden" name="catid" value="<?php echo $category->displayField('id'); ?>" />
				<?php if (!$config->allow_change_subject) : ?>
					<input type="hidden" name="subject" value="<?php echo $this->escape($this->message->subject); ?>" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>

				<div class="modal-header">
					<button type="button" class="close kreply-cancel" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h3>
						<?php echo JText::sprintf('COM_KUNENA_REPLYTO_X', $author->getLink()); ?>
					</h3>
				</div>

				<div class="modal-body">
					<div class="container-fluid">
						<?php if (!$me->exists()) : ?>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3" style="padding:7px 10px">
										<label class="col-md-12 control-label" style="padding:0;">
											<?php echo JText::_('COM_KUNENA_GEN_NAME'); ?>:
										</label>
									</div>
									<div class="col-md-9">
										<input type="text" name="authorname" class="form-control" style="max-width: unset !important;margin:7px;" maxlength="35" placeholder="<?php echo JText::_('COM_KUNENA_GEN_NAME'); ?>" value="" required />
									</div>
								</div>
							</div>
						<?php endif; ?>

						<?php if ($config->askemail && !$me->exists()): ?>
							<div class="form-group">
								<div class="row">
									<div class="col-md-3" style="padding:7px 10px">
										<label class="col-md-12 control-label" style="padding:0;">
											<?php echo JText::_('COM_KUNENA_GEN_EMAIL'); ?>:
										</label>
									</div>
									<div class="col-md-9">
										<input type="text" id="email" name="email" placeholder="<?php echo JText::_('COM_KUNENA_TOPIC_EDIT_PLACEHOLDER_EMAIL') ?>" class="inputbox col-md-12 form-control" style="max-width: unset !important;margin:7px;padding-left: 7px;" maxlength="45" value="" required />
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<small><?php echo $config->showemail == '0' ? JText::_('COM_KUNENA_POST_EMAIL_NEVER') : JText::_('COM_KUNENA_POST_EMAIL_REGISTERED'); ?></small>
									</div>
								</div>
							</div>
						<?php endif; ?>

						<div class="form-group">
							<div class="row">
								<div class="col-md-3" style="padding:7px 10px">
									<label for="kanonymous<?php echo intval($message->id); ?>" class="col-md-12 control-label" style="padding:0;">
										<?php echo JText::_('COM_KUNENA_GEN_SUBJECT'); ?>:
									</label>
								</div>
								<div class="col-md-9">
									<input type="text" id="subject" name="subject" class="form-control"
										maxlength="<?php echo $template->params->get('SubjectLengthMessage'); ?>"
										<?php if (!$config->allow_change_subject): ?>disabled<?php endif; ?>
										value="<?php echo $message->displayField('subject'); ?>"
										style="max-width: unset !important;margin:7px;" />
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-3" style="padding:7px 10px">
									<label class="col-md-12 control-label" style="padding:0;">
										<?php echo JText::_('COM_KUNENA_MESSAGE'); ?>:
									</label>
								</div>
								<div class="col-md-9">
									<textarea class="qreply form-control" id="kbbcode-message" name="message" rows="6" cols="60" style="max-width: unset !important;margin:7px;"></textarea>
								</div>
							</div>
						</div>
					</div>
				<?php if (!empty($this->quickcaptchaEnabled)) : ?>
					<div class="control-group">
						<?php echo $this->quickcaptchaDisplay;?>
					</div>
				<?php endif; ?>
				<div class="modal-footer">
					<small><?php echo JText::_('COM_KUNENA_QMESSAGE_NOTE'); ?></small>
					<input type="submit" class="btn btn-primary kreply-submit" name="submit"
					       value="<?php echo JText::_('COM_KUNENA_SUBMIT'); ?>"
					       title="<?php echo (JText::_('COM_KUNENA_EDITOR_HELPLINE_SUBMIT')); ?>" />
					<?php //TODO: remove data on cancel. ?>
					<input type="reset" name="reset" class="btn btn-default kreply-cancel"
						value="<?php echo (' ' . JText::_('COM_KUNENA_CANCEL') . ' ');?>"
						title="<?php echo (JText::_('COM_KUNENA_EDITOR_HELPLINE_CANCEL'));?>" data-dismiss="modal" aria-hidden="true" />
				</div>
				<input type="hidden" id="kurl_emojis" name="kurl_emojis" value="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=topic&layout=listemoji&format=raw') ?>" />
				<input type="hidden" id="kemojis_allowed" name="kemojis_allowed" value="<?php echo $config->disemoticons ?>" />
			</form>
		</div>
	</div>
</div>
