<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Search
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

// FIXME: change into JForm.

// TODO: Add generic form version

JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');

// Load caret.js always before atwho.js script and use it for autocomplete, emojiis...
$this->addScript('assets/js/jquery.caret.js');
$this->addScript('assets/js/jquery.atwho.js');
$this->addStyleSheet('assets/css/jquery.atwho.css');
$this->addScript('assets/js/search.js');

?>

<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=search'); ?>" method="post" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
	<input type="hidden" name="task" value="results" />
	<?php if ($this->me->exists()) : ?>
		<input type="hidden" id="kurl_users" name="kurl_users" value="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=user&layout=listmention&format=raw') ?>" />
	<?php endif; ?>
	<?php echo JHtml::_('form.token'); ?>

	<h1 class="kpage-title">
		<?php echo JText::_('COM_KUNENA_SEARCH_ADVSEARCH'); ?>
	</h1>

	<div class="collapse in" id="search">
	<div class="search-keyword">
			<div class="clearfix">
				<fieldset class="search-col-l">
					<legend><?php echo JText::_('COM_KUNENA_SEARCH_SEARCHBY_KEYWORD'); ?></legend>
					<label>
						<?php echo JText::_('COM_KUNENA_SEARCH_KEYWORDS'); ?>:
						<input type="text" name="query" class="form-control"
						       value="<?php echo $this->escape($this->state->get('searchwords')); ?>" />
					</label>
					<?php $this->displayModeList('mode'); ?>
				</fieldset>

				<?php if (!$this->config->pubprofile && !JFactory::getUser()->guest || $this->config->pubprofile) : ?>
				<fieldset class="search-col-r">
					<legend><?php echo JText::_('COM_KUNENA_SEARCH_SEARCHBY_USER'); ?></legend>
					<label>
						<?php echo JText::_('COM_KUNENA_SEARCH_UNAME'); ?>:
						<input id="kusersearch" type="text" name="searchuser" class="form-control"
						       value="<?php echo $this->escape($this->state->get('query.searchuser')); ?>" />
					</label>
					<br>
					<label>
						<?php echo JText::_('COM_KUNENA_SEARCH_EXACT'); ?>:
						<input type="checkbox" name="exactname" value="1"
							<?php if ($this->state->get('query.exactname')) { echo $this->checked; } ?> />
					</label>
				</fieldset>
				<?php endif; ?>
			</div>
		</div>

		<!-- Sub Header -->
		<div class="sub-head clearfix">
			<h2><?php echo JText::_('COM_KUNENA_SEARCH_OPTIONS'); ?></h2>
			<a class="btn btn-default <?php echo KunenaIcons::collapse();?>" data-toggle="collapse" data-target="#search-options" aria-expanded="false" aria-controls="search-options"></a>
		</div>
		<!-- // Sub Header -->

		<div class="collapse in" id="search-options">
				<div class="krow clearfix">
					<fieldset class="kcol-l">
						<legend><?php echo JText::_('COM_KUNENA_SEARCH_FIND_POSTS'); ?></legend>
							<?php $this->displayDateList('date'); ?>
							<?php $this->displayBeforeAfterList('beforeafter'); ?>
					</fieldset>

					<fieldset class="kcol-r">
						<legend><?php echo JText::_('COM_KUNENA_SEARCH_SORTBY'); ?></legend>
						<?php $this->displaySortByList('sort'); ?>
						<?php $this->displayOrderList('order'); ?>
					</fieldset>
				</div>

				<div class="krow clearfix">
					<div class="kcol-l">
						<fieldset>
							<legend>
								<?php echo JText::_('COM_KUNENA_SEARCH_START'); ?>
							</legend>
							<div class="col-md-6">
							<input type="text" name="limitstart" class="form-control"
							       value="<?php echo $this->escape($this->state->get('list.start')); ?>" size="5" />
							</div>
							<div class="col-md-6">
								<?php $this->displayLimitlist('limit'); ?>
							</div>
						</fieldset>

						<?php if ($this->isModerator) : ?>
						<!-- Added ID to fieldset and classes to items to ensure correct changes made and no issues are caused elsewhere -->
						<fieldset id="forum_search_show">
							<legend>
								<?php echo JText::_('COM_KUNENA_SEARCH_SHOW'); ?>
							</legend>
							<label class="radio forum_search_show_label">
								<input type="radio" name="show" value="0" class="forum_search_show_radio"
									<?php if ($this->state->get('query.show') == 0) { echo 'checked="checked"'; } ?> />
									<span class="forum_search_show_span"><?php echo JText::_('COM_KUNENA_SEARCH_SHOW_NORMAL'); ?></span>
								
							</label>
							<label class="radio forum_search_show_label">
								<input type="radio" name="show" value="1" class="forum_search_show_radio"
									<?php if ($this->state->get('query.show') == 1) { echo 'checked="checked"'; } ?> />
									<span class="forum_search_show_span"><?php echo JText::_('COM_KUNENA_SEARCH_SHOW_UNAPPROVED'); ?></span>
							</label>
							<label class="radio forum_search_show_label">
								<input type="radio" name="show" value="2" class="forum_search_show_radio"
									<?php if ($this->state->get('query.show') == 2) { echo 'checked="checked"'; } ?> />
									<span class="forum_search_show_span"><?php echo JText::_('COM_KUNENA_SEARCH_SHOW_TRASHED'); ?></span>
							</label>
						</fieldset>

						<?php endif; ?>

					</div>

					<fieldset class="kcol-r">
						<legend>
							<?php echo JText::_('COM_KUNENA_SEARCH_SEARCHIN'); ?>
						</legend>
						<?php $this->displayCategoryList('categorylist', 'class="form-control" size="10" multiple="multiple"'); ?>
						<label>
							<input type="checkbox" name="childforums" value="1"
								<?php if ($this->state->get('query.childforums')) { echo 'checked="checked"'; } ?> />
							<?php echo JText::_('COM_KUNENA_SEARCH_SEARCHIN_CHILDREN'); ?>
						</label>
					</fieldset>
				</div>

		</div>

		<div class="center">
			<button type="submit" class="btn btn-primary">
				<?php echo KunenaIcons::search();?><?php echo(' ' . JText::_('COM_KUNENA_SEARCH_SEND') . ' '); ?>
			</button>
			<button type="reset" class="btn btn-default" onclick="window.history.back();">
				<?php echo KunenaIcons::cancel();?><?php echo(' ' . JText::_('COM_KUNENA_CANCEL') . ' '); ?>
			</button>
		</div>
	</div>
</form>
