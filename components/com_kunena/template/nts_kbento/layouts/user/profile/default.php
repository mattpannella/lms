<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.User
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

// @var KunenaUser $user

$user = $this->user;
$this->ktemplate = KunenaFactory::getTemplate();
$avatar = $user->getAvatarImage($this->ktemplate->params->get('avatarType'), 'post');
$show = KunenaConfig::getInstance()->showuserstats;

$activityIntegration = KunenaFactory::getActivityIntegration();
$points = $activityIntegration->getUserPoints($user->userid);
$medals = $activityIntegration->getUserMedals($user->userid);

if ($show)
{
	if (KunenaConfig::getInstance()->showkarma)
	{
		$karma = $user->getKarma();
	}

	$rankImage = $user->getRank($this->category_id, 'image');
	$rankTitle = $user->getRank($this->category_id, 'title');
	$personalText = $user->getPersonalText();
}
?>
<ul class="profilebox">
	<li class="ku-name">
		<span class="user-name"><?php echo $user->getLink(null, null, '', '', null, $this->category_id); ?></span>
	</li>

	<?php if ($avatar) : ?>
	<li class="ku-avatar">
		<?php echo $user->getLink($avatar, null, ''); ?>
		<?php if (isset($this->topic_starter) && $this->topic_starter) : ?>
				<span class="topic-starter"><?php echo JText::_('COM_KUNENA_TOPIC_AUTHOR') ?></span>
		<?php endif;?>
		<?php /*if (!$this->topic_starter && $user->isModerator()) : */?><!--
			<span class="topic-moderator"><?php /*echo JText::_('COM_KUNENA_MODERATOR') */?></span>
		--><?php /*endif;*/?>
	</li>
	<?php endif; ?>

	<?php if ($user->exists()) : ?>
	<!--
	<li class="ku-status">
		<?php echo $this->subLayout('User/Item/Status')->set('user', $user); ?>
	</li>
	-->
	<?php endif; ?>

	<?php if (!empty($rankTitle)) : ?>
	<li class="ku-rank-title">
		<?php echo $this->escape($rankTitle); ?>
	</li>
	<?php endif; ?>

	<?php if (!empty($rankImage)) : ?>
	<li class="ku-rank-img">
		<?php echo $rankImage; ?>
	</li>
	<?php endif; ?>

	<?php if (!empty($personalText)) : ?>
	<li class="ku-text">
		<?php echo $personalText; ?>
	</li>
	<?php endif; ?>
</ul>
<?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_profile_default'); ?>
<?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_topicprofile'); ?>

<?php if ($user->userid > 1) : ?>
		<ul class="profilebox-extra">
			<?php if ($user->posts >= 1) : ?>
			<li class="ku-posts">
				<?php echo JText::_('COM_KUNENA_POSTS') . ' ' . (int) $user->posts; ?>
			</li>
			<?php endif; ?>

			<?php if (!empty($karma) && KunenaConfig::getInstance()->showkarma) : ?>
			<li class="ku-karma">
				<?php echo JText::_('COM_KUNENA_KARMA') . ': ' . $karma; ?>
			</li>
			<?php endif; ?>

			<?php if ($show && isset($user->thankyou) && KunenaConfig::getInstance()->showthankyou) : ?>
			<li class="ku-thanks">
				<?php echo JText::_('COM_KUNENA_MYPROFILE_THANKYOU_RECEIVED') . ' ' . (int) $user->thankyou; ?>
			</li>
			<?php endif; ?>

			<?php if ($show && !empty($points)) : ?>
			<li class="ku-points">
				<?php echo JText::_('COM_KUNENA_AUP_POINTS') . ' ' . $points; ?>
			</li>
			<?php endif; ?>

			<?php if ($show && !empty($medals)) : ?>
			<li class="ku-medals">
				<?php echo implode(' ', $medals); ?>
			</li>
			<?php endif; ?>

			<?php if ($user->gender) :?>
			<li class="ku-gender">
				<?php echo $user->profileIcon('gender'); ?>
			</li>
			<?php endif ?>

			<?php if ($user->birthdate) :?>
			<li class="ku-birthdate">
				<?php echo $user->profileIcon('birthdate'); ?>
			</li>
			<?php endif ?>

			<?php if ($user->location) :?>
			<li class="ku-localtion">
				<?php echo $user->profileIcon('location'); ?>
			</li>
			<?php endif ?>

			<?php if ($user->websiteurl) :?>
			<li class="ku-website">
				<?php echo $user->profileIcon('website'); ?>
			</li>
			<?php endif ?>

<!-- 			<?php if (KunenaFactory::getPrivateMessaging()) :?>
			<li class="ku-private">
				<?php echo $user->profileIcon('private'); ?>
			</li>
			<?php endif ?> -->

			<?php if ($user->email && !$user->hideEmail) :?>
			<li class="ku-email">
				<?php echo $user->profileIcon('email'); ?>
			</li>
			<?php endif ?>

			<?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_topicprofilemore'); ?>
		</ul>
<?php endif;
