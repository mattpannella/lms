<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Topic
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

$cols = !empty($this->actions) ? 5 : 6;
$colspan = !empty($this->actions) ? 5 : 4;
$view = JFactory::getApplication()->input->getWord('view');
$this->ktemplate = KunenaFactory::getTemplate();

$this->addStyleSheet('assets/css/rating.css');
?>

<h2 class="forum-title">
	<?php echo $this->escape($this->headerText); ?>
	<small class="hidden-xs">
		(<?php echo JText::sprintf('COM_KUNENA_X_TOPICS_MORE', $this->formatLargeNumber($this->pagination->total)); ?>)
	</small>

	<?php // ToDo:: <span class="badge badge-success"> <?php echo $this->topics->count->unread; ?/></span> ?>
</h2>

<div class="row-actions clearfix">

	<div class="float-l">
	<?php
		if ($this->config->enableforumjump && !$this->embedded && $this->topics) {
			echo $this->subLayout('Widget/Forumjump')->set('categorylist', $this->categorylist);
		}
	?>
	</div>

	<?php if ($view != 'user') : ?>
	<div id="filter-time" class="float-r">
		<div class="filter-sel">
			<form action="<?php echo $this->escape(JUri::getInstance()->toString()); ?>" id="timeselect" name="timeselect"
				method="post" target="_self" class="form-inline hidden-xs">
					<?php $this->displayTimeFilter('sel'); ?>
			</form>
		</div>
	</div>
	<?php endif; ?>
</div>

<div class="row-actions clearfix">
	<div class="nts-pagination float-l">
		<?php echo $this->subLayout('Widget/Pagination/List')
		->set('pagination', $this->pagination->setDisplayedPages(4))
		->set('display', true);	?>
	</div>

	<div class="hidden-xs float-r">
		<?php echo $this->subLayout('Widget/Search')
		->set('catid', 'all')
		->setLayout('topic'); ?>
	</div>
</div>


<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=topics'); ?>" method="post" name="ktopicsform" id="ktopicsform">
	<?php echo JHtml::_('form.token'); ?>
	<table class="kblocktable">
		<thead>
		<tr>
			<th class="kcol-topic-icon center">
				<a id="forumtop"></a>
				<a href="#forumbottom"><?php echo KunenaIcons::arrowdown(); ?></a>
			</th>
			<th id="recent-list" class="kcol-topic-title"><?php echo JText::_('COM_KUNENA_GEN_SUBJECT'); ?></th>
			<th class="kcol-topic-replies"><?php echo JText::_('COM_KUNENA_GEN_REPLIES'); ?></th>
			<th class="kcol-topic-views"><?php echo JText::_('COM_KUNENA_GEN_HITS');?></th>
			<th class="kcol-topic-lastpost"><?php echo JText::_('COM_KUNENA_GEN_LAST_POST'); ?></th>

			<?php if (!empty($this->actions)) : ?>
				<th class="kcol-topic-action">
					<label>
						<input class="kcheckall" type="checkbox" name="toggle" value="" />
					</label>
				</th>
			<?php endif; ?>
		</tr>
		</thead>

		<tfoot>
		<tr>
			<td class="center hidden-xs">
				<a id="forumbottom"> </a>
				<a href="#forumtop" rel="nofollow">
					<?php echo KunenaIcons::arrowup(); ?>
				</a>
			</td>
			<?php if (empty($this->actions)) : ?>
			<td colspan="<?php echo $colspan; ?>" class="hidden-xs">
			<?php else : ?>
			<td colspan="<?php echo $colspan; ?>">
			<?php endif; ?>
				<?php if (!empty($this->actions) || !empty($this->moreUri)) : ?>
				<div class="form-group">
					<div class="input-group" role="group">
						<div class="input-group-btn">
							<label>
							<?php if (!empty($this->topics) && !empty($this->moreUri)) { echo JHtml::_('kunenaforum.link', $this->moreUri, JText::_('COM_KUNENA_MORE'), null, 'btn btn-primary pull-left', 'follow'); } ?>
							<?php if (!empty($this->actions)) : ?>
								<?php echo JHtml::_('select.genericlist', $this->actions, 'task', 'class="form-control kchecktask" ', 'value', 'text', 0, 'kchecktask'); ?>
								<?php if (isset($this->actions['move'])) :
									$options = array (JHtml::_('select.option', '0', JText::_('COM_KUNENA_BULK_CHOOSE_DESTINATION')));
									echo JHtml::_('kunenaforum.categorylist', 'target', 0, $options, array(), 'class="form-control fbs" disabled="disabled"', 'value', 'text', 0, 'kchecktarget');
								endif;?>
								<button type="submit" name="kcheckgo" class="btn btn-default"><?php echo JText::_('COM_KUNENA_GO') ?></button>
							<?php endif; ?>
							</label>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</td>
		</tr>
		</tfoot>

		<tbody>
		<?php if (empty($this->topics) && empty($this->subcategories)) : ?>
			<tr>
				<td colspan="5" class="center"><?php echo JText::_('COM_KUNENA_VIEW_NO_TOPICS') ?></td>
			</tr>
		<?php else : ?>
			<?php $counter = 2; ?>

			<?php foreach ($this->topics as $i => $topic)
			{
				echo $this->subLayout('Topic/Row')
					->set('topic', $topic)
					->set('position', 'kunena_topic_' . $i)
					->set('checkbox', !empty($this->actions));

				if ($this->ktemplate->params->get('displayModule'))
				{
					echo $this->subLayout('Widget/Module')
						->set('position', 'kunena_topic_' . $counter++)
						->set('cols', $cols)
						->setLayout('table_row');
				}
			} ?>
		<?php endif; ?>
		</tbody>
	</table>
</form>

<!-- Begin: Pagination -->
<div class="row-actions clearfix">
	<div class="nts-pagination">
		<?php echo $this->subLayout('Widget/Pagination/List')
		->set('pagination', $this->pagination->setDisplayedPages(4))
		->set('display', true); ?>
	</div>
</div>
<!-- End: Pagination -->

