<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Template.NTS Kay
 * @subpackage  Layout.Statistics
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;
?>
<!--
<div class="kblock kwhoisonline">
  <div class="kheader">
    <h2 class="btn-link"><span>
      <?php if ($this->usersUrl) : ?>
        <a href="<?php echo $this->usersUrl; ?>">
          <?php echo JText::_('COM_KUNENA_MEMBERS'); ?>
        </a>
      <?php else : ?>
        <?php echo JText::_('COM_KUNENA_MEMBERS'); ?>
      <?php endif; ?>
    </span></h2>

    <span class="ktoggler" data-toggle="collapse" data-target="#kwho"></span>
  </div>

	<div class="collapse in kcontainer" id="kwhoisonline">
    <div class="kbody clearfix">
      <div class="kblocktable">
        <div class="kwhoicon"><i class="fa fa-users"></i></div>
        <ul>
          <li>
            <?php echo JText::sprintf('COM_KUNENA_VIEW_COMMON_WHO_TOTAL', $this->membersOnline); ?>
          </li>

          <?php
          $template = KunenaTemplate::getInstance();
          $direction = $template->params->get('whoisonlineName');

          if ($direction == 'both') : ?>
            <li><?php echo $this->setLayout('both'); ?></li>
          <?php
          elseif ($direction == 'avatar') : ?>
            <li><?php echo $this->setLayout('avatar'); ?></li>
          <?php else : ?>
            <li><?php echo $this->setLayout('name'); ?></li>
          <?php
            endif;
          ?>

          <?php if (!empty($this->onlineList)) : ?>
          <li class="legend">
            <span><?php echo JText::_('COM_KUNENA_LEGEND'); ?>:</span>
            <span class="kwho-admin">
              <?php echo JText::_('COM_KUNENA_COLOR_ADMINISTRATOR'); ?>
            </span>
            <span class="kwho-globalmoderator">
              <?php echo JText::_('COM_KUNENA_COLOR_GLOBAL_MODERATOR'); ?>
            </span>
            <span class="kwho-moderator">
              <?php echo JText::_('COM_KUNENA_COLOR_MODERATOR'); ?>
            </span>
            <span class="kwho-banned">
              <?php echo JText::_('COM_KUNENA_COLOR_BANNED'); ?>
            </span>
            <span class="kwho-user">
              <?php echo JText::_('COM_KUNENA_COLOR_USER'); ?>
            </span>
            <span class="kwho-guest">
              <?php echo JText::_('COM_KUNENA_COLOR_GUEST'); ?>
            </span>
          </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>

	</div>
</div>
-->
