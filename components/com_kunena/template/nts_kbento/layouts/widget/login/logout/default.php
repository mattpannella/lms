<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Template.NTSKBento
 * @subpackage  Layout.Widget
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

$markAllReadUrl = KunenaForumCategoryHelper::get()->getMarkReadUrl();
// FIXME: move announcements logic and pm logic into the template file...
?>
<ul class="logout-nav">
	<li>
		<span class="sm-avatar">
			<?php if ($this->me->status == 0) : ?>
				<?php echo $this->me->getAvatarImage(KunenaFactory::getTemplate()->params->get('avatarType') . ' green', 32, 32); ?>
			<?php elseif ($this->me->status == 1) : ?>
				<?php echo $this->me->getAvatarImage(KunenaFactory::getTemplate()->params->get('avatarType') . ' yellow', 32, 32); ?>
			<?php elseif ($this->me->status == 2) : ?>
				<?php echo $this->me->getAvatarImage(KunenaFactory::getTemplate()->params->get('avatarType') . ' red', 32, 32); ?>
			<?php elseif ($this->me->status == 3) : ?>
				<?php echo $this->me->getAvatarImage(KunenaFactory::getTemplate()->params->get('avatarType') . ' grey', 32, 32); ?>
			<?php endif; ?>
			<b class="caret"></b>
		</span>

		<div class="dropdown-logout" id="nav-menu">

			<div class="user-info center">
				<strong class="user-name"><?php echo $this->me->getLink(null, null, 'nofollow', '', null); ?></strong>
				<a href="<?php echo $this->me->getURL(); ?>" class="user-avatar">
					<?php echo $this->me->getAvatarImage(KunenaFactory::getTemplate()->params->get('avatarType'), 128, 128); ?>
				</a>
				<!--
				<div class="user-status"><?php echo $this->subLayout('User/Item/Status')->set('user', $this->me); ?></div>
				<div class="last-visible">
					<i class="fa fa-clock-o"></i><?php echo $this->me->getLastVisitDate()->toKunena('config_post_dateformat'); ?>
				</div>
				-->
			</div>
			<!--
			<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena'); ?>" method="post" id="status-form" class="form-inline">
				<span>
					<input id="status-online" class="hide" type="radio" value="0" name="status" />
					<label for="status-online" class="btn btn-link">
						<a href="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=user&task=status&status=0&' . JSession::getFormToken() . '=1'); ?>" class="btn btn-link">
							<i class="fa fa-plus green"></i><?php echo JText::_('COM_KUNENA_ONLINE') ?>
						</a>
					</label>
				</span>

				<span>
					<input id="status-away" class="hide" type="radio" value="1" name="status" />
					<label for="status-away" class="btn btn-link">
						<a href="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=user&task=status&status=1&' . JSession::getFormToken() . '=1'); ?>" class="btn btn-link">
							<i class="fa fa-plus yellow"></i><?php echo JText::_('COM_KUNENA_AWAY') ?>
						</a>
					</label>
				</span>

				<span>
					<input id="status-busy" class="hide" type="radio" value="2" name="status" />
					<label for="status-busy" class="btn btn-link">
						<a href="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=user&task=status&status=2&' . JSession::getFormToken() . '=1');
; ?>" class="btn btn-link">
							<i class="fa fa-minus red"></i><?php echo JText::_('COM_KUNENA_BUSY') ?>
						</a>
					</label>
				</span>

				<span>
					<input id="status-invisible" class="hide" type="radio" value="3" name="status" />
					<label for="status-invisible" class="btn btn-link">
						<a href="<?php echo KunenaRoute::_('index.php?option=com_kunena&view=user&task=status&status=3&' . JSession::getFormToken() . '=1'); ?>" class="btn btn-link">
							<i class="fa fa-minus grey"></i><?php echo JText::_('COM_KUNENA_INVISIBLE') ?>
						</a>
					</label>
				</span>

				<input type="hidden" name="view" value="user" />
				<input type="hidden" name="task" value="status" />
				<?php echo JHtml::_('form.token'); ?>
			</form>

			<div class="user-status-text">
				<a data-toggle="modal" data-target="#statusTextModal">
					<i class="fa fa-pencil"></i> <?php echo JText::_('COM_KUNENA_STATUS') ?>
				</a>
			</div>
			-->
			<?php if (!empty($this->announcementsUrl)) : ?>
				<div class="user-announcements">
					<a href="<?php echo $this->announcementsUrl; ?>">
						<i class="fa fa-bullhorn"></i> <?php echo JText::_('COM_KUNENA_ANN_ANNOUNCEMENTS') ?>
					</a>
				</div>
			<?php endif; ?>

			<?php if (!empty($this->pm_link)) : ?>
				<div class="user-inbox">
					<a href="<?php echo $this->pm_link; ?>" class="btn btn-link">
						<i class="fa fa-envelope"></i> <?php echo $this->inboxCount; ?>
					</a>
				</div>
			<?php endif; ?>

			<div class="user-logout">
				<a href="<?php echo $this->me->getUrl(false, 'edit'); ?>">
					<i class="fa fa-cog"></i> <?php echo JText::_('COM_KUNENA_LOGOUTMENU_LABEL_PREFERENCES'); ?>
				</a>
			</div>

			<?php if ($markAllReadUrl) : ?>
				<div class="mark-all-read">
					<a href="<?php echo $markAllReadUrl; ?>">
						<i class="fa fa-check-square-o"></i> <?php echo JText::_('COM_KUNENA_MARK_ALL_READ'); ?>
					</a>
				</div>
			<?php endif ?>

			<?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_logout'); ?>

			<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena'); ?>" method="post" id="logout-form" class="form-inline">
				<div class="user-logout">
					<button class="btn btn-link" name="submit" type="submit">
						<i class="fa fa-sign-out"></i> <?php echo JText::_('COM_KUNENA_PROFILEBOX_LOGOUT'); ?>
					</button>
				</div>
				<input type="hidden" name="view" value="user" />
				<input type="hidden" name="task" value="logout" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<?php echo $this->subLayout('Widget/Module')->set('position', 'kunena_logout_bottom'); ?>
		</div>
	</li>
</ul>
<?php
/*
  Note these have to be outsize the dropdown as z-index stack context is different
from the parent forcing the dropsown to take over z-index calculation */
?>
<form action="<?php echo KunenaRoute::_('index.php?option=com_kunena'); ?>" method="post" id="statusText-form" class="form-inline">
	<?php echo $this->subLayout('Widget/Modal')
	->set('id', 'statusTextModal')
	->set('name', 'status_text')
	->set('label', JText::_('COM_KUNENA_STATUS_MESSAGE'))
	->set('description', JText::_('COM_KUNENA_STATUS_TYP'))
	->set('data', $this->me->status_text)
	->set('form', 'statusText-form'); ?>
	<input type="hidden" name="view" value="user" />
	<input type="hidden" name="task" value="statustext" />
	<?php echo JHtml::_('form.token'); ?>
</form>
<script type='text/javascript'>
	jQuery(document).ready(function ($) {
		$("input[name=status]").change(function () {
			$("#status-form").submit();
		});
	});
</script>
