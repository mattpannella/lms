<?php
/**
 * Kunena Component
 * @package     Kunena.Template.Crypsis
 * @subpackage  Layout.Widget
 *
 * @copyright   (C) 2008 - 2016 Kunena Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die;

$pathway = $this->breadcrumb->getPathway();
$item = array_shift($pathway);

if ($item) : ?>
<div class="breadcrumb">
	<ul itemscope itemtype="http://schema.org/BreadcrumbList">
		<li class="active" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<span><i class="divider fa fa-home"></i></span>
			<a itemprop="item" href="<?php echo $item->link; ?>" rel="nofollow"><?php echo Jtext::_(AxsLanguage::getLocalizationVariable($item->name)); ?></a>
		</li>

		<?php foreach($pathway as $item) : ?>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="<?php echo $item->link; ?>" rel="nofollow"><?php echo Jtext::_(AxsLanguage::getLocalizationVariable($item->name)); ?></a>
		</li>
		<?php endforeach; ?>

	</ul>
</div>
<?php endif; ?>
