<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Template.NTSBento
 * @subpackage  Template
 *
 * @copyright   (C) 2009 - 2016 Nice Theme Store Team. All rights reserved.
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        http://www.9themestore.org
 **/
defined('_JEXEC') or die;

/**
 * NTS KBento template.
 *
 * @since  K4.0
 */
class KunenaTemplateNts_kbento extends KunenaTemplate
{
	/**
	 * List of parent template names.
	 *
	 * This template will automatically search for missing files from listed parent templates.
	 * The feature allows you to create one base template and only override changed files.
	 *
	 * @var array
	 */
	protected $default = array('kbento');

	/**
	 * Relative paths to various file types in this template.
	 *
	 * These will override default files in JROOT/media/kunena
	 *
	 * @var array
	 */
	protected $pathTypes = array(
		'emoticons' => 'media/emoticons',
		'ranks' => 'media/ranks',
		'icons' => 'media/icons',
		'categoryicons' => 'media/category_icons',
		'images' => 'media/images',
		'js' => 'media/js',
		'css' => 'media/css'
	);

	/**
	 * User group initialization.
	 *
	 * @return void
	 */
	protected $userClasses = array(
		'kwho-',
		'admin' => 'kwho-admin',
		'globalmod' => 'kwho-globalmoderator',
		'moderator' => 'kwho-moderator',
		'user' => 'kwho-user',
		'guest' => 'kwho-guest',
		'banned' => 'kwho-banned',
		'blocked' => 'kwho-blocked'
	);

	/**
	 * Logic to load language strings for the template.
	 *
	 * By default language files are also loaded from the parent templates.
	 *
	 * @return void
	 */
	public function loadLanguage()
	{
		$lang = JFactory::getLanguage();
		KunenaFactory::loadLanguage('kunena_tmpl_nts_kbento');

		foreach (array_reverse($this->default) as $template)
		{
			$file = "kunena_tmpl_nts_kbento";
			$lang->load($file, JPATH_SITE) || $lang->load($file, KPATH_SITE) || $lang->load($file, KPATH_SITE . "/template/{$template}");
		}
	}

	/**
	 * Template initialization.
	 *
	 * @return void
	 */
	public function initialize()
	{
		JHtml::_('bootstrap.framework');
		JHtml::_('bootstrap.tooltip', '[data-toggle="tooltip"]');
		JHtml::_('jquery.framework');
		$this->addScript('assets/js/main.js');

		// Compile CSS from LESS files.
		$this->compileLess('assets/less/nts_kbento.less', 'kunena.css');
		$this->addStyleSheet('kunena.css?v=3');

		$this->ktemplate = KunenaFactory::getTemplate();
		$storage = $this->ktemplate->params->get('storage');

		if ($storage)
		{
			$this->addScript('assets/js/localstorage.js');
		}

		$filename = JPATH_SITE . '/components/com_kunena/template/nts_kbento/assets/css/custom.css';
		if (file_exists($filename))
		{
			$this->addStyleSheet('assets/css/custom.css');
		}

		$fontawesome = $this->ktemplate->params->get('fontawesome');

		$doc = JFactory::getDocument();

		// Load Fontawesome
		if ($fontawesome) {
			$doc->addStyleSheet("//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
		}

		// Load Bootstrap
		$bootstrap = $this->ktemplate->params->get('bootstrap');
		if($bootstrap) {
			$doc->addStyleSheet("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
			$this->addScript("//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
		}

		$icons = $this->ktemplate->params->get('icons');
		if ($icons)
		{
			$doc->addStyleSheet("//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css");
		}

		// Load template colors settings
		$styles = <<<EOF
		/* Kunena Custom CSS */
EOF;
		$iconcolor = $this->ktemplate->params->get('IconColor');
		if ($iconcolor) {
			$styles .= <<<EOF
		.layout#kunena [class*="category"] i,
		.layout#kunena .glyphicon-topic,
		.layout#kunena #kwho i.icon-users,
		.layout#kunena#kstats i.icon-bars { color: {$iconcolor}; }
EOF;
		}

		$iconcolornew = $this->ktemplate->params->get('IconColorNew');
		if ($iconcolornew) {
			$styles .= <<<EOF
		.layout#kunena [class*="category"] .icon-knewchar { }
		.layout#kunena sup.knewchar { }
		.layout#kunena .topic-item-unread { border-left-color: {$iconcolornew} !important;}
		.layout#kunena .topic-item-unread .glyphicon { color: {$iconcolornew} !important;}
		.layout#kunena .topic-item-unread i.fa { color: {$iconcolornew} !important;}
EOF;
		}

		$forumHeaderBackground = $this->ktemplate->params->get('forumHeaderBackground');
		$forumHeaderColor = $this->ktemplate->params->get('forumHeaderColor');
		$forumHeaderLinkColor = $this->ktemplate->params->get('forumHeaderLinkColor');

		if ($forumHeaderBackground) {
			$styles .= <<<EOF
			.layout#kunena div.kblock > div.kheader,#Kunena .kblock div.kheader { background-color: {$forumHeaderBackground}; color: {$forumHeaderColor};}
			.layout#kunena div.kblock > div.kheader a,#Kunena .kblock div.kheader a { color: {$forumHeaderLinkColor};}
			.layout#kunena div.kblock .kheader h1, #Kunena div.kblock .kheader h2, #Kunena div.kblock .kheader h3 { color: {$forumHeaderColor};}
		  .layout#kunena div.kwhoisonline .kheader { background-color: {$forumHeaderBackground}; color: {$forumHeaderColor};}
EOF;
		}

		$forumLink = $this->ktemplate->params->get('forumLinkColor');
		$forumLinkHover = $this->ktemplate->params->get('forumLinkColorHover');

		if ($forumLink) {
			$styles .= <<<EOF
			.layout#kunena a {color: {$forumLink};}
			.layout#kunena a:hover, #kunena.layout a:focus, #kunena.layout a:active { color: {$forumLinkHover};}
			.layout#kunena div.kthead-title a:hover { color: {$forumLinkHover}; }
			.layout#kunena .full-view .kthead-child .kcc-subcat a:hover { color: {$forumLinkHover}; }
EOF;
		}

	$forumBtnBg = $this->ktemplate->params->get('forumBtnBg');
	$forumBtnColor = $this->ktemplate->params->get('forumBtnColor');
	$forumBtnHoverBg = $this->ktemplate->params->get('forumBtnHoverBg');
	$forumBtnHoverColor = $this->ktemplate->params->get('forumBtnHoverColor');

	$styles .= <<<EOF
	.layout#kunena .btn, #Kunena button.btn { background-color: {$forumBtnBg}; color: {$forumBtnColor} !important; }
	.layout#kunena .btn:hover, #Kunena #kprofilebox .input span .kbutton:hover { background-color: {$forumBtnHoverBg}; color: {$forumBtnHoverColor}; }
	.layout#kunena .kpost-thankyou .kicon-button { border: 1px solid {$forumBtnHoverBg}; }
	.layout#kunena .kmsg .kbuttonbar-left .kpost-thankyou a.kicon-button:hover { background: {$forumBtnHoverBg} !important; color: {$forumBtnHoverColor};}
EOF;


	$announcementHeader = $this->ktemplate->params->get('announcementHeadercolor');

	if ($announcementHeader) {
		$styles .= <<<EOF
		.layout#kunena div.kannouncement div.kheader { background: {$announcementHeader} !important;}
EOF;
	}

	$announcementBox = $this->ktemplate->params->get('announcementBoxbgcolor');
	if ($announcementBox) {
		$styles .= <<<EOF
		.layout#kunena div#kannouncement { background: {$announcementBox}; }
EOF;
	}

	$inactiveTab = $this->ktemplate->params->get('inactiveTabcolor');
	if ($inactiveTab) {
		$styles .= <<<EOF
		.layout#kunena #ktab a { background-color: {$inactiveTab}; }
EOF;
	}

	$tabColor = $this->ktemplate->params->get('tabColor');
	if ($tabColor) {
		$styles .= <<<EOF
		.layout#kunena .nts-topbar .nav li a { color: {$tabColor}; }
EOF;
	}

	$hoverTabBg = $this->ktemplate->params->get('hoverTabBg');
	if ($hoverTabBg) {
		$styles .= <<<EOF
		.layout#kunena .nts-topbar .nav li a:hover { background-color: {$hoverTabBg} !important; }
EOF;
	}

	$hoverTabcolor = $this->ktemplate->params->get('hoverTabcolor');
	if ($hoverTabcolor) {
		$styles .= <<<EOF
		.layout#kunena .nts-topbar .nav li a:hover { color: {$hoverTabcolor}; }
EOF;
	}

	$activeTabBg = $this->ktemplate->params->get('activeTabBg');
	$activeTab = $this->ktemplate->params->get('activeTabcolor');
	if ($activeTab) {
		$styles .= <<<EOF
		.layout#kunena .nts-topbar .nav li.active a,.layout#kunena .nts-topbar .nav li#current.selected a { background-color: {$activeTabBg} !important; color: #fff; }
EOF;
	}

	$topBorder = $this->ktemplate->params->get('topBordercolor');
	if ($topBorder) {
		$styles .= <<<EOF
		.layout#kunena #ktop { border-color: {$topBorder} !important; }
EOF;
	}


	$toggleButton = $this->ktemplate->params->get('toggleButtoncolor');
	if ($toggleButton) {
		$styles .= <<<EOF
		.layout#kunena #ktop span.ktoggler { background-color: {$toggleButton} !important; }
EOF;
	}

		$iconBg = $this->ktemplate->params->get('kIconBg');
		$iconColor = $this->ktemplate->params->get('kIconColor');
		if ($iconBg || $iconColor) {
			$styles .= <<<EOF
		.layout#kunena table.kblocktable td.kcol-cat-icon a { background-color: {$iconBg}; color: {$iconColor}; }
		.layout#kunena table.kblocktable td.kcol-topic-icon a { background-color: {$iconBg}; color: {$iconColor}; }
		.layout#kunena #kwho table.kblocktable span.kwhoicon, .layout#kunena #kstats table.kblocktable span.kicon {
			background-color: {$iconBg}; color: {$iconColor};
		}
EOF;
		}

		$headerTextColor = $this->ktemplate->params->get('kHeaderColor');
		$headerLinkColor = $this->ktemplate->params->get('kHeaderLinkColor');
		$headerLinkHoverColor = $this->ktemplate->params->get('kHeaderLinkHoverColor');
		if ($headerTextColor || $headerLinkColor || $headerLinkHoverColor) {
			$styles .= <<<EOF
		.layout#kunena div.kheader {  color: {$headerTextColor}; }
		.layout#kunena div.kheader a { color: {$headerLinkColor};  }
		.layout#kunena div.kheader a:hover, .layout#kunena div.kheader a:focus, .layout#kunena div.kheader a:active { color: {$headerLinkHoverColor}; }
EOF;
		}

		$document = JFactory::getDocument();
		$document->addStyleDeclaration($styles);

		// Get custom for template
		$app    = JFactory::getApplication();
		$filePath   = JPATH_SITE.'/templates/'.$app->getTemplate().'/css/nts_kbento_custom.css';
		$customFilePath   = JUri::root(true).'/templates/'.$app->getTemplate().'/css/nts_kbento_custom.css';
		if(file_exists($filePath)) {
			$doc->addStyleSheet($customFilePath);
		}

		parent::initialize();
	}

	/**
	 * @param        $filename
	 * @param   string $group
	 *
	 * @return JDocument
	 */
	public function addStyleSheet($filename, $group = 'forum')
	{
		$filename = $this->getFile($filename, false, '', "media/kunena/cache/{$this->name}/css");

		return JFactory::getDocument()->addStyleSheet(JUri::root(true) . "/{$filename}");
	}

	/**
	 * @param      $link
	 * @param      $name
	 * @param      $scope
	 * @param      $type
	 * @param   null $id
	 *
	 * @return string
	 */
	public function getButton($link, $name, $scope, $type, $id = null)
	{
		$types = array('communication' => 'comm', 'user' => 'user', 'moderation' => 'mod', 'permanent' => 'mod');
		$names = array('unfavorite' => 'favorite', 'unsticky' => 'sticky', 'unlock' => 'lock', 'create' => 'newtopic', 'quickreply' => 'reply', 'quote' => 'quote', 'edit' => 'edit', 'permdelete' => 'delete', 'flat' => 'layout-flat', 'threaded' => 'layout-threaded', 'indented' => 'layout-indented', 'list' => 'reply');

		// Need special style for buttons in drop-down list
		$buttonsDropdown = array('reply', 'quote', 'edit', 'delete', 'subscribe', 'unsubscribe', 'unfavorite', 'favorite', 'unsticky', 'sticky', 'unlock', 'lock', 'moderate', 'undelete', 'permdelete', 'flat', 'threaded', 'indented');

		$text  = JText::_("COM_KUNENA_BUTTON_{$scope}_{$name}");
		$title = JText::_("COM_KUNENA_BUTTON_{$scope}_{$name}_LONG");

		if ($title == "COM_KUNENA_BUTTON_{$scope}_{$name}_LONG")
		{
			$title = '';
		}

		if ($id)
		{
			$id = 'id="' . $id . '"';
		}

		if (in_array($name, $buttonsDropdown))
		{
			return <<<HTML
				<a $id style="" href="{$link}" rel="nofollow" title="{$title}">
				{$text}
				</a>
HTML;
		}
		else
		{
			return <<<HTML
				<a $id style="" href="{$link}" rel="nofollow" title="{$title}">
				<span class="{$name}"></span>
				{$text}
				</a>
HTML;
		}
	}

	/**
	 * @param        $name
	 * @param   string $title
	 *
	 * @return string
	 */
	public function getIcon($name, $title = '')
	{
		return '<span class="kicon ' . $name . '" title="' . $title . '"></span>';
	}

	/**
	 * @param        $image
	 * @param   string $alt
	 *
	 * @return string
	 */
	public function getImage($image, $alt = '')
	{
		return '<img src="' . $this->getImagePath($image) . '" alt="' . $alt . '" />';
	}
}
