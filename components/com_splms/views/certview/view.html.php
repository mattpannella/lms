<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

class SplmsViewCertview extends FOFViewHtml{
	public function display($tpl = null){

		// Return parent display function
		return parent::display($tpl = null);
	}
}