<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsViewCart extends FOFView{

	public function onAdd($tpl = null){
		return $this->onDisplay();
	}

	public function display($tpl = null) {

		$db = JFactory::getDBO();

		//check if the form was submitted... if so add the items to the session
		$session = JFactory::getSession();
		$input = JFactory::getApplication()->input;
		
		$exist_orders = $session->get('splms_orders');				
		$orders = array_map('unserialize', array_unique(array_map('serialize', $exist_orders)));

		//Get all of the courses they already own.
		
		$user_id = JFactory::getUser()->id;
		$query = "SELECT course_id FROM axs_course_purchases WHERE user_id = $user_id AND status = 'PAID'";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$owned = array();
		foreach ($result as $r) {
			$owned[]= (int)$r->course_id;
		}

		//Check for any empty orders, or orders for courses that are already owned.
		foreach ($orders as $key => $order) {
			$remove = false;
			if (is_array($order)) {

				if (count($order) == 0) {
					//The order is empty.
					$remove = true;
				} else {
					//The order is for a course they already own.
					if (in_array($order['product_id'], $owned)) {
						$remove = true;
					}
				}	

			} else {
				//The order is empty.
				$remove = true;
			}

			if ($remove) {
				unset($orders[$key]);
			}
		}

		if(!empty($orders)) {

			foreach ($orders as $key=>$order) {
				if ($order['product_id'] == $to_removed_course_id) {
					unset($orders[$key]);
				}
			}

			$session->set( 'splms_orders', $orders);
			$this->carts = $orders;
		} else {
			$session->set('splms_orders', null);
			$this->carts = array();			
		}

		$this->user = JFactory::getUser();
		jimport('joomla.application.component.helper');
		$this->params = JComponentHelper::getParams('com_splms');
		$this->currency = explode(':', $this->params->get('currency', "USD:$"));

		$this->cards = AxsPayment::getUserAllCards($this->user->id);
		parent::display($tpl);
	}
}