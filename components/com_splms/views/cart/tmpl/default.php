<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//error_reporting(E_ALL);
//ini_set('display_errors', 1); 



?>
<style>
	@media print {
		body * {
			visibility: hidden;
		}
		.print-area, .print-area * {
			visibility: visible;
		}
		.print-area {
			position: absolute;
			left: 0;
			top: 0;
		}
	}
</style>
<?php 

	$cards = array();
	foreach ($this->cards as $card) {
		$newCard = new stdClass();
		$newCard->id = $card->id;
		$newCard->cardNumber = $card->cardNumber;
		$cards[]= $newCard;
	}

	$session = JFactory::getSession();
	$session->set('splms_cards', $cards);

?>
<br/>

<div id="lms_cart">
	<?php		
		echo SplmsHelper::getCartView();
	?>
</div>

