<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

?>

<div id="splms" class="splms view-splms-teacher splms-person">

	<div class="splms-row">
		<div class="splms-col-sm-4">
			<img src="<?php echo $this->item->image ?>" class="splms-person-image splms-img-responsive" alt="<?php echo $this->item->title; ?>">
		</div>

		<div class="splms-col-sm-8">

			<div class="splms-person-details">

				<h3 class="splms-person-title">
					<?php echo $this->item->title; ?>
					<?php if (!empty($this->item->specialist_in)) { ?>
						<small class="splms-person-designation">
							<?php echo AxsLanguage::text("COM_SPLMS_SPECIALIST_IN", "Specialist") . ': '; ?> 
							<?php echo $this->item->specialist_in; ?>
						</small>
					<?php } ?>
				</h3>

				<?php if (!empty($this->item->website)) { ?>
				<p class="splms-teacher-experience">
					<?php AxsLanguage::text("COM_SPLMS_COMMON_EXPERIENCE", "Experience") . ': '; ?>
					<?php echo $this->item->experience; ?>
				</p>
				<?php } ?>

				<p class="splms-tecaher-lessons">
					<?php echo AxsLanguage::text("COM_SPLMS_COMMON_TOTAL", "Total") . ': '; ?>  
                	<?php echo count($this->lessons); ?> 
                	<?php echo AxsLanguage::text("COM_SPLMS_COMMON_LESSONS", "Lesson(s)"); ?>
				</p>

				<?php if (!empty($this->item->email)) { ?>
				<p class="splms-person-email">
					<?php echo AxsLanguage::text("COM_SPLMS_COMMON_EMAIL", "Email") . ': '; ?> 
					<?php echo $this->item->email; ?>
				</p>
				<?php } ?>

				<?php if (!empty($this->item->website)) { ?>
				<p class="splms-person-website">
					<?php echo AxsLanguage::text("COM_SPLMS_COMMON_WEBSITE", "Website") . ': '; ?>
					<a href="<?php echo $this->item->website; ?>" target="_blank"> <?php echo $this->item->website; ?> </a>
				</p>
				<?php } ?>

				<?php if ( (!empty($this->item->social_facebook)) ||
					(!empty($this->item->social_linkedin)) ||
					(!empty($this->item->social_twitter)) ||
					(!empty($this->item->social_gplus))) { ?>

					<ul class="splms-persion-social-icons">
						<?php if (!empty($this->item->social_facebook)) { ?>			                		
						<li class="facebook">
							<a href="http://facebook.com/<?php echo $this->item->social_facebook; ?>" target="_blank"> 
								<i class="splms-icon-facebook"></i>
							</a>
						</li>
						<?php } if (!empty($this->item->social_linkedin)) {?>
						<li class="linkedin">
							<a href="http://linkedin.com/<?php echo $this->item->social_linkedin; ?>" target="_blank"> 
								<i class="splms-icon-linkedin"></i>
							</a>
						</li>
						<?php } if (!empty($this->item->social_twitter)) {?>
						<li class="twitter">
							<a href="http://twitter.com/<?php echo $this->item->social_twitter; ?>" target="_blank"> 
								<i class="splms-icon-twitter"></i>
							</a>
						</li>
						<?php } if (!empty($this->item->social_gplus)) {?>
						<li class="gplus">
							<a href="https://plus.google.com/<?php echo $this->item->social_gplus; ?>" target="_blank"> 
								<i class="splms-icon-google-plus"></i>
							</a>
						</li>
						<?php } ?>
					</ul>
				<?php } ?>

				<div class="splms-person-description">
					<?php echo $this->item->description; ?>
				</div>

				<?php if(isset($this->lessons) && count($this->lessons)) { ?>
					<div class="splms-teacher-lessons">
						<h3><?php echo AxsLanguage::text("COM_SPLMS_LESOSNS_LIST", "Lesson List"); ?></h3>
						<div class="splms-teacher-lessons-list">
							<ul>
								<?php foreach ($this->lessons as $lesson) { ?>

								<?php if ($lesson->lesson_type == 0 || $this->isAuthorised != '' || $this->lessonCourse->price == 0) { ?>
									<li>
										<span>
											<a href="<?php echo $lesson->lesson_url; ?>"><?php echo $lesson->title; ?>
											</a>
										</span>
										<span class="lesson-duration pull-right">
											<i class="splms-icon-time"></i> <?php echo JText::_('COM_SPLMS_COMMON_DURATION') . ': '; ?> <?php echo $lesson->video_duration; ?>
										</span>
									</li>
								<?php } else { ?>
									<li class="splms-lesson-unauthorised">
										<span>
											<i class="splms-icon-lock"></i>
											<?php echo $lesson->title; ?>
										</span>
										<span class="lesson-duration pull-right">
											<i class="splms-icon-time"></i> <?php echo JText::_('COM_SPLMS_COMMON_DURATION') . ': '; ?> 
											<?php echo $lesson->video_duration; ?>
										</span>
									</li>
								<?php } ?>

								<?php } ?>
							</ul>
						</div>
					</div>	
				<?php } ?>
			</div>
		</div>
	</div>	
</div>
