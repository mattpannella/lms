<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

class SplmsViewTeacher extends FOFViewHtml{
	public function display( $tpl = null){

		// Get model
		$model = $this->getModel();
		//get Item
		$this->item = $model->getItem();
		//Load Lesosns Model 
		$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');
		//Load Course Model
		$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');

		// Get image thumb
		$this->user = JFactory::getUser();
		$lmsparams 	= JComponentHelper::getParams('com_splms');
		$this->thumb_size = strtolower($lmsparams->get('course_thumbnail', '480X300'));

		$this->lessons = $lessons_model->getTeacherLessons($this->item->splms_teacher_id);
		//$this->allCourses = SplmsHelper::getAllCourses();

		//Get Currency
		// $this->currency = explode(':', $lmsparams->get('currency', 'USD:$'));
		// $this->currency =  $currency[1];

		foreach ($this->lessons as &$this->lesson) {
			$this->lessonCourse = $courses_model->getCourse($this->lesson->splms_course_id);
			$this->isAuthorised = $courses_model->getIsbuycourse($this->user->id, $this->lessonCourse->splms_course_id);
		}



		return parent::display( $tpl = null );
	}
}