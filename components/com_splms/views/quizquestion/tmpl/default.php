<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

$params =  json_decode($this->item->params);

?>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style>
	.quiz-overlay {
		height: 100vh;
		width: 100%;
		background: rgba(0,0,0,0.85);
		position: absolute;
		left: 0;
		top: 0;
		z-index: 1000;
		border-radius: 20px;
		display: none;
	}

	svg {
	  width: 100px;
	  display: block;
	  margin: 40px auto 0;
	}

	.checkmark {
	    width: 200px;
	    height: 200px;
	    border-radius: 50%;
	    display: block;
	    stroke-width: 5;
	    stroke: #fff;
	    stroke-miterlimit: 10;
	    margin: 10% auto;
	    z-index: 2;
	    opacity: 0;
	}

	.checkmark-circle {
	    stroke-dasharray: 166;
	    stroke-dashoffset: 166;
	    stroke-width: 5;
	    stroke-miterlimit: 10;
	    fill: none;
	}

	.correct-stroke {
		stroke: green;
	}

	.incorrect-stroke {
		stroke: red;
	}

	.checkmark-correct,
	.checkmark-incorrect {
	    transform-origin: 50% 50%;
	    stroke-dasharray: 70;
	    stroke-dashoffset: 70;
	    stroke-linecap: round;
	}

	@keyframes stroke {
	    100% {
	        stroke-dashoffset: 0;
	    }
	}

	@keyframes scale {
	    0%,
	    100% {
	        transform: none;
	    }
	    50% {
	        transform: scale3d(1.1, 1.1, 1);
	    }
	}

	@keyframes fill-red {
	    100% {
	        box-shadow: inset 0px 0px 0px 100px red;
	    }
	}

	@keyframes fill-green {
	    100% {
	        box-shadow: inset 0px 0px 0px 100px green;
	    }
	}

	.correct.fadeIn {
	    opacity: 1;
	    transition: all 0.8s ease;
	    animation: fill-green .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both;
	}

	.incorrect.fadeIn {
	    opacity: 1;
	    transition: all 0.8s ease;
	    animation: fill-red .4s ease-in-out .4s forwards, scale .3s ease-in-out .9s both;
	}

	.checkmark.fadeIn .checkmark-circle {
	    animation: stroke 0.6s cubic-bezier(0.65, 0, 0.45, 1) forwards;
	}

	.checkmark.fadeIn .show-mark {
	    animation: stroke 0.3s cubic-bezier(0.65, 0, 0.45, 1) 0.8s forwards;
	}

	.checkmark-incorrect {
	  stroke: white;
	  fill: transparent;
	  stroke-linecap: round;
	  stroke-width: 5;
	  margin-left: 40%;
	  margin-top: 40%;
	  position: relative;
	}

	.answer-result {
		font-size: 30px;
		text-align: center;
		margin-top: -25px;
		text-transform: uppercase;
		color: white;
		display: none;
	}

	.answer-box {
		font-size: 20px;
		text-align: center;
		color: white;
		display: none;
	}

	.answer {
		font-size: 20px;
		color: white;
	}

	.answer-title {
		font-size: 25px;
		color: green;
		font-weight: bold;
	}

	.modal {
		display: block;
	}

	.courseReturn {
		display: none;
	}

	.quiz-media {
		max-height: 250px;
		max-width: 100%;
	}

	.questionMedia {
		text-align: center;
	}

	.counterTitle {
	  color: #283848;
	  font-weight: 100;
	  font-size: 40px;
	  margin: 40px 0px 20px;
	}

	#clockdiv{
		font-family: sans-serif;
		color: #fff;
		display: inline-block;
		font-weight: 100;
		text-align: center;
		font-size: 30px;
	}

	#clockdiv > div{
		padding: 10px;
		border-radius: 3px;
		background: #283848;
		display: inline-block;
	}

	#clockdiv div > span{
		padding: 15px;
		border-radius: 3px;
		background: #314458;
		display: inline-block;
	}

	.smalltext{
		padding-top: 5px;
		font-size: 16px;
	}

	.counterBox {
		width: 270px;
		margin-right: auto;
		margin-left: auto;
		display: none;
	}

	.error-message {
		color: firebrick;
		font-size: 14px;
		font-weight: bold;
	}

</style>
<div class="counterBox">
	<div class="counterTitle"></div>
	<div id="clockdiv">
		<!-- <div>
			<span class="days"></span>
			<div class="smalltext">Days</div>
		</div> -->
		<div>
			<span class="hours"></span>
			<div class="smalltext"><?php echo AxsLanguage::text("AXS_HOURS", "Hours") ?></div>
		</div>
		<div>
			<span class="minutes"></span>
			<div class="smalltext"><?php echo AxsLanguage::text("AXS_MINUTES", "Minutes") ?></div>
		</div>
		<div>
			<span class="seconds"></span>
			<div class="smalltext"><?php echo AxsLanguage::text("AXS_SECONDS", "Seconds") ?></div>
		</div>
	</div>
</div>

<?php if($params->show_overlay || !isset($params->show_overlay)) { ?>
<div class="quiz-overlay">
	<div id="wrap">
	    <svg class="checkmark" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 52 52">
	        <circle class="checkmark-circle" cx="26" cy="26" r="25" fill="none" />
	        <path class="checkmark-correct" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
	        <path class="checkmark-incorrect" fill="none"  d="M 16,16 L 36,36 M 36,16 L 16,36" />
	    </svg>
	    <div class="clearfix"></div>
	    <div class="answer-result"></div>
	    <div class="clearfix"></div>
	    <div class="answer-box">
	    	<span class="answer-title"><?php echo AxsLanguage::text("COM_SPLMS_CORRECT_ANSWER", "Correct Answer") ?>:</span>
	    	<br/>
	    	<span class="answer"></span>
	    </div>
	    <div class="clearfix"></div>
	    <center>
		    <button tabindex="0" class="btn-lg btn-success continueButton" style="margin-top: 15px;">
		    	<?php echo JText::_('COM_SPLMS_AXS_QUIZ_CONTINUE') . " ";?>
		    	 <i class="fa fa-chevron-circle-right"></i>
		    </button>
		</center>
	</div>
</div>
<?php } ?>

<div id="splms" class="splms view-splms-quiz">
	<div class="splms-row">
		<div class="splms-col-sm-12">

			<div id="quizPasswordGate">
				<h3><?php echo AxsLanguage::text("COM_SPLMS_QUIZ_PASSWORD_REQUIRED_TO_CONTINUE", "This quiz has been password protected and requires a password to continue.") ?></h3>

				<div id="wrap">
					<form id="submitProctorPassword">
						<label for="proctor_password" class="strong"><?php echo AxsLanguage::text("COM_AXS_PLEASE_ENTER_THE_PASSWORD", "Please enter the password:") ?></label>
						<br>
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
							<input id="proctorPassword" type="password" class="form-control" name="proctor_password" placeholder="Password" style="height:100%; padding: 10px;">
						</div>
						<button id="passwordSubmit" class="btn btn-primary" style="margin-top: 10px;"><?php echo AxsLanguage::text("COM_AXS_SUBMIT_PASSWORD", "Submit Password") ?></button>
						<div class="error-message"></div>
					</form>
				</div>
			</div>

			<div class="before-start-quiz">
				<div class="quiz-content">
					<h3><?php echo $this->item->title; ?></h3>
					<p><?php echo $this->item->description; ?></p>
				</div>
                <div class="quiz-progress-reset" style="display:none;">Your quiz progress has been reset due to certain quiz parameters changing.</div>
				<button class="btn btn-lg btn-primary startQuiz startTimer"><?php echo JText::_('COM_SPLMS_AXS_QUIZ_START');?></button>
				<span onclick="parent.location.reload();" class="btn btn-lg btn-default cancelQuiz"><?php echo JText::_('COM_SPLMS_AXS_QUIZ_CANCEL');?></span>
			</div>

			<div class="quizContainer">
				<div class="quizMessage"></div>
				<div class="ques-ans-wrapper">
					<div class="questionMedia"></div>
					<h3 class="question"></h3>
					<ul class="choiceList list-unstyled"></ul>
				</div>

				<button class="btn btn-primary nextButton"><?php echo JText::_('COM_SPLMS_AXS_QUIZ_SUBMIT_ANSWER') . " ";?></button>
				<div class="response"></div>
				<br>
			</div>
		</div>
	</div>

	<div class="lms-result-wrapper">
		<div class="result"></div>
		<br/>
		<div class="completion_message"></div>
		<br/>
		<center>
			<span onclick="parent.location.reload();" class="btn btn-primary courseReturn" style="display: none;">
				<?php echo JText::_('COM_SPLMS_AXS_QUIZ_RETURN_TO_COURSE'); ?>
			</span>
		</center>
	</div>

</div>

<?php
    if(!empty($this->item->duration)) {

        $user = JFactory::getUser();
        $userId = $user->id;

        if(!empty($user)) {
            $currentAttempt = AxsQuiz::getLatestLegacyQuizAttempt($userId, $this->item->splms_quizquestion_id);
            $currentAttemptTimeElapsed = $currentAttempt->elapsed;

            $quizQuestions = json_decode($this->item->list_answers);
            $currentAttemptAnswers = json_decode($currentAttempt->answers);

            $quizTimedOut = !is_null($currentAttemptTimeElapsed) && ($currentAttemptTimeElapsed >= $this->item->duration * 60);

            $quizCompleted = count($quizQuestions->qes_title) == count($currentAttemptAnswers);

            // Reset the time elapsed since we are now starting a new quiz attempt
            if($quizCompleted || $quizTimedOut) {

                $currentAttemptTimeElapsed = 0;
            }
        } else {

            $currentAttemptTimeElapsed = null;
        }
?>
<script>
    var timeElapsed = 0;

    <?php if($currentAttemptTimeElapsed !== null) : ?>
        // Keep track of the number of seconds that have elapsed since the user started the quiz.
        timeElapsed = <?php echo $currentAttemptTimeElapsed; ?>;
    <?php endif; ?>

	function getTimeRemaining(endtime) {
		var t = Date.parse(endtime) - Date.parse(new Date());
		var seconds = Math.floor((t / 1000) % 60);
		var minutes = Math.floor((t / 1000 / 60) % 60);
		var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
		//var days = Math.floor(t / (1000 * 60 * 60 * 24));
		return {
			'total': t,
			//'days': days,
			'hours': hours,
			'minutes': minutes,
			'seconds': seconds
		};
	}

	function initializeClock(id, endtime, stop) {

		var clock = document.getElementById(id);

		//var daysSpan = clock.querySelector('.days');
		var hoursSpan = clock.querySelector('.hours');
		var minutesSpan = clock.querySelector('.minutes');
		var secondsSpan = clock.querySelector('.seconds');

		function updateClock() {
			if(!stop) {
				var t = getTimeRemaining(endtime);

                // Time elapsed increments by one second each time this function is executed.
                timeElapsed++;

				//daysSpan.innerHTML = t.days;
				hoursSpan.innerHTML   = ('0' + t.hours).slice(-2);
				minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
				secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

				if (t.total <= 0) {
				  clearInterval(timeinterval);
				  var answer = jQuery("input[type='radio']:checked").val();

				  sendAnswer(answer,true);
				}
			} else {
                clearInterval(timeinterval);
            }
		}

		var timeinterval = setInterval(updateClock, 1000);
	}

	jQuery('.startTimer').click(function() {
        var now = Date.parse(new Date());
        var duration = <?php echo $this->item->duration; ?>;
        var timeLeft = (duration * 60) - timeElapsed;
        var deadline = new Date(now + (timeLeft * 1000));

		initializeClock('clockdiv', deadline, false);
		jQuery('.counterBox').fadeIn(700);
	});
</script>
<?php }  ?>