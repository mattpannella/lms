<?php


class SplmsViewQuizquestion extends FOFViewHtml{

	public function display($tpl = null){
		$language = AxsLanguage::getCurrentLanguage()->get('tag');
		// Get model
		$model = $this->getModel();
		// get item
		$this->item = $model->getItem();

		// Get login user ID
		$user = JFactory::getUser();
		$userId = $user->id;

        // guest has no access
		if($user->guest) {
			?>
				<p class="alert alert-danger"><?php echo JText::_('COM_SPLMS_QUIZ_LOGIN'); ?></p>
			<?php
			return;
		}

		//Load quiz model
		$quiz_model = FOFModel::getTmpInstance('Quizquestions', 'SplmsModel');
		//if already given quiz
		$this->quiz_results = $quiz_model->getQuizById( $user->id, $this->item->splms_quizquestion_id );

        // NB: Quiz results refer to the answers that have been recorded when a quiz is completed
        // Get the number of attempts by computing the number of sets of quiz results recorded
		$all_results = $quiz_model->getQuizResult( $user->id, $this->item->splms_quizquestion_id );
		$attempts = count($all_results);

		$available_tries = $this->item->tries;

		//if $available_tries is 0, they have unlimited tries.
		if ($available_tries > 0) {

			if ($attempts >= $available_tries) {
				?>
					<p class="alert alert-danger"><?php echo JText::_('COM_SPLMS_AXS_QUIZ_NO_MORE_ATTEMPTS'); ?></p>
				<?php
				return;
			} else if ($attempts > 0) {
				$suffixLocalizationVariable = "COM_SPLMS_AXS_QUIZ_REMAINING_ATTEMPTS_SUFFIX";
				$remainingAttempts = $available_tries - $attempts;
				if ($remainingAttempts == 1) {
					$suffixLocalizationVariable .= '_SINGULAR';
				}
				?>
					<p class="alert alert-warning">
						<?php echo JText::_('COM_SPLMS_AXS_QUIZ_REMAINING_ATTEMPTS_PREFIX') . " " . $remainingAttempts . " " . JText::_($suffixLocalizationVariable); ?>
					</p>
				<?php
			}
		}


		$list_answers_decode 	= json_decode($this->item->list_answers);
		$quizOptions = json_decode($this->item->params);
		$mode = 'quiz';

		if($quizOptions->mode) {
			$mode = $quizOptions->mode;
		}

		if ($list_answers_decode) {
			// Point table
			$qes_titles		= str_replace('"', '', $list_answers_decode->qes_title);
			$media			= str_replace('"', '', $list_answers_decode->media);
			$ans_ones		= str_replace('"', '', $list_answers_decode->ans_one);
			$ans_twos 		= str_replace('"', '', $list_answers_decode->ans_two);
			$ans_threes		= str_replace('"', '', $list_answers_decode->ans_three);
			$ans_fours 		= str_replace('"', '', $list_answers_decode->ans_four);
			$right_anss		= str_replace('"', '', $list_answers_decode->right_ans);

			$quizQuestions = array();
			foreach ($qes_titles as $id => $qes_title) {
				$quizQuestions[] = array(
					'qes_title' 	=> $qes_titles[$id],
					'media' 		=> $media[$id],
					'ans_one' 		=> $ans_ones[$id],
					'ans_two' 		=> $ans_twos[$id],
					'ans_three' 	=> $ans_threes[$id],
					'ans_four' 		=> $ans_fours[$id]
				);

			}

		} // END:: has list_answers

		$quizParams = new stdClass();
		$quizParams->course_id 	= $this->item->splms_course_id;
		$quizParams->lesson_id 	= $this->item->splms_lesson_id;
		$quizParams->quiz_id 	= $this->item->splms_quizquestion_id;
		$key = AxsKeys::getKey('lms');
		$encryptedQuizParams = base64_encode(AxsEncryption::encrypt($quizParams, $key));

        $currentAttempt = AxsQuiz::getLatestLegacyQuizAttempt($userId, $quizParams->quiz_id);
        $currentAnswers = json_decode($currentAttempt->answers);

        $totalQuestions = count($quizQuestions);
        $nextQuestionStep = count($currentAnswers);

        $quizTimeElapsed = !is_null($currentAttempt->elapsed) && ($currentAttempt->elapsed >= $this->item->duration * 60);

        $resetProgress = "false";

        if($totalQuestions <= $nextQuestionStep) {

            $nextQuestionStep = 0;
            $resetProgress = "true";

            AxsQuiz::deleteLegacyQuizAnswers($userId, $quizParams->quiz_id);
        }

        if(($nextQuestionStep == $totalQuestions) || $quizTimeElapsed) {

            $nextQuestionStep = 0;
        }

		$answer1 = trim(addslashes($quizQuestions[$nextQuestionStep]['ans_one']));
		$answer2 = trim(addslashes($quizQuestions[$nextQuestionStep]['ans_two']));
		$answer3 = trim(addslashes($quizQuestions[$nextQuestionStep]['ans_three']));
		$answer4 = trim(addslashes($quizQuestions[$nextQuestionStep]['ans_four']));

		$first_question = addslashes($quizQuestions[$nextQuestionStep]['qes_title']);
		$first_question_options = "[`$answer1`,`$answer2`,`$answer3`,`$answer4`]";

		//get media
		$media = $quizQuestions[$nextQuestionStep]['media'];
		if($media) {
			$mediaHTML = AxsLMS::getQuizQuestionMedia($media);
		} else {
			$mediaHTML = '';
		}

		?>

		<!-- Quiz Questions -->
		<script type="text/javascript">

			var text_please_select_answer_alert = "<?php echo JText::_('COM_SPLMS_AXS_QUIZ_PLEASE_SELECT_ANSWER');?>";
			var text_show_score_prefix = "<?php echo JText::_('COM_SPLMS_AXS_QUIZ_SCORE_SHOW_PREFIX');?>";
			var text_show_score_suffix = "<?php echo JText::_('COM_SPLMS_AXS_QUIZ_SCORE_SHOW_SUFFIX');?>";
			var text_show_score_time_up = "<?php echo JText::_('COM_SPLMS_AXS_QUIZ_TIME_IS_UP');?>";
			var answerResult;
			var mode = "<?php echo $mode; ?>";
            var resetProgress = "<?php echo $resetProgress; ?>";
			$(document).on('ready', function() {

				$('.quizContainer').hide();
				$('.before-start-quiz').hide();
				$('.error-text').hide();

				<?php if(empty($quizOptions->password)) : ?>
					$('#quizPasswordGate').hide();
					$('.before-start-quiz').show();
				<?php endif; ?>

                if(resetProgress == "true") {
                    $('.quiz-progress-reset').show();
                }

                /* When the password is submitted, check it via AJAX and grab the result.
				 * If the password is good, continue the quiz as normal. If not, display "try again" message.
				 */
				$("#passwordSubmit").click(function(event) {

					event.preventDefault();

					var password = $('#proctorPassword').val();

					$.post({
						url: '/index.php?option=com_splms&task=quizquestions.checkPassword&format=raw',
						data: {
							'quiz_id': <?php echo $this->item->splms_quizquestion_id; ?>,
							'password': btoa(password)
						},
						success: function(response) {

							// If the password matched, hide the password form and show the quiz start content
							if(response.success) {

								$('#quizPasswordGate').hide();
								$('.quizContainer').hide();
								$('.before-start-quiz').show();
							} else {

								$('.error-message').show();
								$('.error-message').text(response.message);
							}
						},
						dataType: "json"
					});
				});
			});

			$(document).on("click",".startQuiz", function(){
				$(document).find(".quizContainer").show();
				$(document).find(".before-start-quiz").hide();
                $('.quiz-progress-reset').hide();
			});

			var currentQuestion = <?php echo $nextQuestionStep; ?>;
			var quizOver 		= false;
			var quiz 			= "<?php echo $encryptedQuizParams; ?>";
        	var language 	 	= "<?php echo $language; ?>";

        	var answerAnimation = function(result,answer) {
		        $('.quiz-overlay').fadeIn(200);
        		$('.checkmark-circle').addClass(result+'-stroke');
				$('.checkmark').addClass(result);
				$('.checkmark').css('fill','none');
    			$('.'+result).addClass('fadeIn');
    			$('.checkmark-'+result).addClass('show-mark');
    			$('.answer-result').text(result);
    			$('.answer-result').fadeIn();
    			if(answer && result == 'incorrect') {
    				$('.answer').text(answer);
    				$('.answer-box').fadeIn();
    			}
    			$('.continueButton').fadeIn();
        	}

        	var clearAnimation = function(result) {
        		$('.checkmark-circle').removeClass(result+'-stroke');
				$('.checkmark').removeClass(result);
    			$('.'+result).removeClass('fadeIn');
    			$('.checkmark-'+result).removeClass('show-mark');
    			$('.answer-result').text('');
    			$('.answer-result').fadeOut();
    			$('.answer').text('');
    			$('.answer-box').fadeOut();
    			$('.continueButton').fadeOut();
    			$('.quiz-overlay').fadeOut(200);
        	}

        	var sendAnswer = function(answer, timeExpired = false) {
				var request = {
		            'data'   : {
		            	quiz 		: quiz,
		            	language 	: language,
		            	question 	: currentQuestion,
		            	answer 		: answer,
		            	timeExpired	: timeExpired
		            }
		        };

                // Javascript doesn't like undefined references - only include timeElapsed if the variable is defined
                if(typeof(timeElapsed) !== 'undefined') {

                    request['data'].timeElapsed = timeElapsed;
                }

				$.ajax({
					url    : '/index.php?option=com_splms&task=quizquestions.saveAnswer&format=raw',
		            type   : 'POST',
		            data   : request,
                    dataType: 'json'
                }).done(function (result) {

                    // Re-enable the quiz submit button
                    $('.quizContainer .nextButton').attr('disabled', false);

                    if(result) {
                        if(result.next_question) {
                            var question = result.next_question;
                            var questionMedia = result.next_question_media;
                            var options  = JSON.parse(result.next_question_options);
                            displayCurrentQuestion(questionMedia,question,options);
                        }

                        if(result.quiz_completed) {
                            displayScore(result.correct_count,result.question_count,mode,result.completion_message);
                        }

                        if(result.correct) {
                            answerResult = 'correct';
                        } else {
                            answerResult = 'incorrect';
                        }
                        if(mode == 'quiz') {
                            answerAnimation(answerResult,result.answer);
                        }

                        currentQuestion++;

                        // Reset and hide the quiz error message
                        $('.quizMessage').text('');
                        $('.quizMessage').hide();
                    }
                });
			}

        	$(document).ready(function () {
				var firstQuestionMedia = `<?php echo $mediaHTML; ?>`;
				var firstQuestion = `<?php echo trim($first_question); ?>`;
        		var firstOptions  = <?php echo $first_question_options; ?>;

			    // Display the first question
			    displayCurrentQuestion(firstQuestionMedia,firstQuestion,firstOptions);

			    $('.continueButton').click( function(){
			    	clearAnimation(answerResult);
			    });

			    $(".quizMessage").hide();

			    // On clicking next, display the next question
			    $(".nextButton").on("click", function () {

			        if (!quizOver) {

			            value = $("input[type='radio']:checked").val();

			            if (value == undefined) {
			                $(".quizMessage").text(text_please_select_answer_alert);
			                $(".quizMessage").show();
			            } else {

                            // Disable the submit answer button until sendAnswer is finished
                            $('.quizContainer .nextButton').attr('disabled', true);

			            	sendAnswer(value);
			            }
			        } else {
			            quizOver = false;
			        }
			    });
			});

			// This displays the current question AND the choices
			function displayCurrentQuestion(media = null,question,options) {

			    var question 	  = question;
			    var media 	      = media;
			    var mediaClass    = $(document).find(".questionMedia");
			    var questionClass = $(document).find(".quizContainer .ques-ans-wrapper > .question");
			    var choiceList 	  = $(document).find(".quizContainer .ques-ans-wrapper > .choiceList");
			    var numChoices    = options.length;
			    var options 	  = options;

			    $(document).find(".lms-result-wrapper > .result").removeClass('active');
			    $(document).find(".quizContainer #countdown").show();
			    $(document).find(".nextButton").removeClass("playagain");
			    $(document).find(".quizContainer .ques-ans-wrapper").show();
				$(document).find(".quizContainer .ques-ans-wrapper").show();
				$(mediaClass).html('');

			    // Set the questionClass text to the current question
			    $(questionClass).text(question);

			    if(media) {
			    	$(mediaClass).html(media);
			    }

			    // Remove all current <li> elements (if any)
			    $(choiceList).find("li").remove();

			    var choice;
			    for (i = 0; i < numChoices; i++) {
			        choice = options[i];
			        if(choice){
			        	$('<li><div class="radio"><label><input type="radio" value=' + i + ' name="dynradio" />' + choice + '</label></div></li>').appendTo(choiceList);
			    	}
			    }
			}

			function resetQuiz() {
			    currentQuestion = <?php echo $nextQuestionStep; ?>;
			    hideScore();
			}

			function displayScore(correct_count,question_count,mode,message) {
				<?php if($this->item->duration) { ?>
				initializeClock('clockdiv', null, true);
				$('#clockdiv').hide();
				<?php } ?>
				var text;
				if(mode == 'quiz') {
					text = text_show_score_prefix + " " + correct_count + " " + text_show_score_suffix + " " + question_count;
				}

				if(mode == 'survey' && !message) {
					text = "<?php echo AxsLanguage::text("AXS_THANK_YOU_FOR_TAKING_SURVEY", "Thank you for taking the survey") ?>";
				}

				$('.quizContainer').hide();
				$(document).find(".quizContainer .ques-ans-wrapper").hide();
				if(text) {
					$(document).find(".lms-result-wrapper > .result").text(text);
					$(document).find(".lms-result-wrapper > .result").show().addClass('active');
				}
				if(message) {
					$('.completion_message').html(message);
				}

			    $('.courseReturn').show();

			}

			function hideScore() {
			    $(document).find(".lms-result-wrapper > .result").hide();
			}
			</script>
		<?php

		return parent::display($tpl = null);
	}
}