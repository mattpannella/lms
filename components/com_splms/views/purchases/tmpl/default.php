<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<div id="splms" class="splms splms-view-purchases">
	<?php if ($this->purchases) { ?>
	<!-- START purchased courses list -->
	<table class="table table-bordered table-striped">
	  	<thead>
			<th>#</th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_PURCHASED_COURSE_NAME", "Course Name"); ?></th>
			<th><?php echo AxsLanguage::text("JSTATUS", "Status"); ?></th>
		</thead>

		<?php foreach ($this->purchases as $key=>$purchased_course) { 
			// Check course purchase status
		  	$course_status = ($purchased_course->status == 'STL' || $purchased_course->status == 'SUC') ? '<span class="label label-success">' . JText::_('JENABLED') . '</span>' : '<span class="label label-danger">' . JText::_('COM_SPLMS_PURCHASED_DISABLED_PENDING') . '</span>';
			?>
		<tr>
			<td width="20">
				<?php echo $key + 1; ?>
			</td>

			<td>
				<a href="<?php echo $purchased_course->url; ?>">
					<?php echo $purchased_course->course_name; ?>
				</a>
			</td>
			<td width="100">
				<?php echo $course_status; ?>
			</td>
		</tr>
		<?php } ?>
	</table>
	<!-- END:: purchased courses list -->
	<?php } ?>


	<?php if ($this->quiz_results) { ?>
	<!-- START Quiz Result list -->
	<table class="table table-bordered table-striped">
	  	<thead>
			<th>#</th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_QUIZ_NAME", "Quiz Name"); ?></th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_COURSE_NAME", "Course Name"); ?></th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_QUIZ_RESULT", "Quiz Result"); ?></th>
		</thead>

		<?php foreach ($this->quiz_results as $key=>$quiz_result) { ?>
		<tr>
			<td width="20">
				<?php echo $key + 1; ?>
			</td>

			<td width="30%">
				<?php echo $quiz_result->quiz_name; ?>
			</td>

			<td>
				<a href="<?php echo $quiz_result->course_url; ?>">
					<?php echo $quiz_result->course_name; ?>
				</a>
			</td>

			<td width="120">
				<?php echo $quiz_result->point . '/' .$quiz_result->total_marks . ' (' . round((($quiz_result->point/$quiz_result->total_marks)*100), 2) . '%)'; ?>
			</td>
		</tr>
		<?php } ?>
	</table>
	<!-- END:: Quiz Result list -->
	<?php } ?>


	<?php if ($this->user_certificates) { ?>
	<!-- START Quiz Result list -->
	<table class="table table-bordered table-striped">
	  	<thead>
			<th>#</th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_CERTIFICATE_NAME", "Certificate Name"); ?></th>
			<th><?php echo AxsLanguage::text("COM_SPLMS_COURSE_INSTRUCTOR", "undefined"); ?></th>
		</thead>

		<?php foreach ($this->user_certificates as $key=> $certicate) { ?>
		<tr>
			<td width="20">
				<?php echo $key + 1; ?>
			</td>

			<td width="30%">
				<a href="<?php echo $certicate->certificate_url; ?>">
					<?php echo $certicate->title; ?>
				</a>
			</td>

			<td>
				<?php echo $certicate->instructor; ?>
			</td>
		</tr>
		<?php } ?>
	</table>
	<!-- END:: Quiz Result list -->
	<?php } ?>

</div>