<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsViewPurchases extends FOFView{
	
	public function display($tpl = null){

		$user = JFactory::getUser();
		//Loan Courses model
		$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');
		// load quiz model
		$quiz_model = FOFModel::getTmpInstance('Quizquestions', 'SplmsModel');
		// load certificate model
		$certificate_model = FOFModel::getTmpInstance('Certificates', 'SplmsModel');


		if($user->guest) {
			echo '<p class="alert alert-danger">' . AxsLanguage::text("COM_SPLMS_PURCHASED_QUIZ_LOGIN", "Please login for see your purchased courses list & Quiz Results") . '</p>';
			return;	
		}

		$purchased_course = $courses_model->getPurchasedCourse( $user->id );
		if (empty($purchased_course)) {
			echo '<p class="alert alert-warning">' . AxsLanguage::text("COM_SPLMS_NO_EMPTY_PURCHASED", "Your purchased list is empty") . '</p>';
			//return true;
		}
		$this->purchases = $purchased_course;
		
		// quiz result
		$this->quiz_results = $quiz_model->getQuizResult( $user->id );
		if (empty($this->quiz_results)) {
			echo '<p class="alert alert-warning">' . AxsLanguage::text("COM_SPLMS_EMPTY_QUIZ_RESULT", "Your quiz result is empty") . '</p>';
			//return true;
		}

		// certificates
		$this->user_certificates = $certificate_model->getCerficateById( $user->id );
		//print_r($this->user_certificates);
		if (empty($this->user_certificates)) {
			echo '<p class="alert alert-warning">' . AxsLanguage::text("COM_SPLMS_EMPTY_CERTIFICATE", "Your Certificate result is empty") . '</p>';
			//return true;
		}
		

		parent::display($tpl);
	}

}
