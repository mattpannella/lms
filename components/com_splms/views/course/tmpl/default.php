<?php

defined('_JEXEC') or die();
$document = JFactory::getDocument();
$document->setTitle($this->item->title);

$model = $this->getModel();
$model->setOverrides($this->item);
$course_params = json_decode($this->item->params);
if($course_params->layout){
	$layout = $course_params->layout;
} else {
	$layout = 'dual';
}
include_once($layout.'.php');
