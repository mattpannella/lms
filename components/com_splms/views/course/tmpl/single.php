<?php

defined('_JEXEC') or die();

require_once('assets/getID3/getid3/getid3.php');
require_once('components/shared/controllers/comments.php');
require_once('components/shared/controllers/rating.php');
$getID3 = new getID3;

$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_splms/assets/css/course.css', array('version' => 'auto'));
$doc->addScript('components/com_splms/assets/js/course.js', array('version' => 'auto'));

$user_id = JFactory::getUser()->id;
$accessLevelName =  AxsExtra::accessLevelName($this->item->access);
$hasBoughtCourse = AxsPayment::hasUserPurchasedCourse($this->item->splms_course_id, $this->user->id);
$subs = AxsPayment::getUserSubscriptions($user_id);
$brand = AxsBrands::getBrand();

$isActiveMember = false;

if(!$brand->site_details->plans_for_active) {
		$isActiveMember = true;
} else {
	foreach($subs as $s) {
		$brandPlans = $brand->site_details->plans_for_active;
		if (($s->status == "ACT" || $s->status == "GRC") && in_array($s->plan_id, $brandPlans)) {
			$isActiveMember = true;
			break;
		}
	}
}

$model = $this->getModel();
 
if ($isActiveMember && $hasBoughtCourse && AxsExtra::checkViewLevel($this->item->access, $this->user->id)) {
	$buyButton = false;
	$unlocked = true;
} else { //if they don't have access to view this course
	//check if this course is set for GIN access levels
	if ($this->item->access >= 10 && $this->item->access <= 16) {
		if($isActiveMember && AxsExtra::checkViewLevel($this->item->access - 1, $this->user->id)) {
			//this is a course 1 above their current level, they can upgrade to it.
			$buttonText = $this->coursePrice . ' - ' . AxsLanguage::text("COM_SPLMS_AXS_UPGRADE_NOW", "Upgrade Now"); //Upgrade Now';
			$buyButton = true;
			$unlocked = false;
		} else {
			//this is a course that they are not eligible to upgrade to yet,
			//either because they are not a level below or because they are not active
			$buyButton = false;
			$unlocked = false;
		}
	} else {
		//this course is not a GIN course.
		$buttonText = '$'.$this->item->price.' - ' . AxsLanguage::text("COM_SPLMS_AXS_BUY_NOW", "Buy Now"); //Buy Now';
		$buyButton = true;
		$unlocked = false;
	}
}

//$language = AxsLanguage::getCurrentLanguage();

?>

<div class="row">
	<div class="container">
		<div id="splms" class="splms view-splms-course course-details">
			<div class="splms-course">
				<div class="course-thumbnail">
				
				</div>

				<!-- start course-header -->
				
				<div class="course-header clearfix">
					<h2 class="course-title pull-left"><?php echo $this->item->title; ?></h2>
					<?php if($this->item->available) { ?>
					<div class="apply-now pull-right">
						<?php if (($buyButton) && ($this->item->price != 0)) { ?>
								<button 
									class="addtocart btn btn-primary"
									product-id="<?php echo $this->item->splms_course_id?>"
								>
									<?php echo $buttonText; ?>
								</button>
						<?php } elseif(!$unlocked) { ?>
								<span class="access" ><?php echo AxsLanguage::text("COM_SPLMS_MUST_BE_BLANK_OR_HIGHER", "You must be %s or higher and be an active Member, $accessLevelName") ?></span>
						<?php } else { ?>
							<a id="intro-vid" class="btn btn-primary" href="javascript:void(0);">
								<span id="show-intro-vid">
									<i class="splms-icon"></i><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_SHOW_INTRO_VIDEO", "Show Intro Video") ?>
								</span>
								<span id="hide-intro-vid">
									<i class="splms-icon"></i><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_HIDE_INTRO_VIDEO", "Hide Intro Video") ?>
								</span>
							</a>
						<?php } ?>
					</div>
					<?php } ?>
				</div> 
				
				<!-- end course-header -->

				<div class="splms-course-description">
					<?php echo $this->item->description; ?>					
				</div>

				

				<?php 
					$course_id = $this->item->splms_course_id;
					$user_id = $this->user->id;
					$user_quiz_results = $this->quizresults;
					$quizquestions = $this->quizquestions;
					$accuratethoughts = $this->accuratethoughts;

					$attempts = $model->getAllAttempts($quizquestions, $user_quiz_results);
				?>

				<!-- Has lesson -->
				<?php 
					if(!empty($this->lessons)) { ?>
						<!-- start course-lessons  -->
						<ul class="nav nav-tabs clearfix" style="margin-top: 40px;">	
		    				<?php
		    					$possible = array();

		    					//This determines the order that the tabs are listed.
		    					array_push($possible, "training");		    					
		    					array_push($possible, "additional");
		    					array_push($possible, "webinars");
		    					array_push($possible, "resources");
		    					array_push($possible, "instructions");
		    					array_push($possible, "tests");	    					
		    					//  

		    					$tabs = array();
		    					foreach($this->lessons as $key => $lesson) {
		    						$tabs[$key] = true;	    						
		    					}

		    					$count = 0;
		    					foreach($possible as $tab) {
		    						if ($tabs[$tab]) {
		    							if ($count == 0) {
		    								?>

		    									<li class='active'>
		    								<?php 
		    							} else {
		    								?>
		    									<li>
		    								<?php
		    							}
		    							$count++;

		    							switch ($tab) {
											case "training": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_TRAINING", "Training"); break;
											case "additional": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_ADDITIONAL", "Additional Training"); break;
											case "webinars": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_WEBINARS", "Webinars"); break;
											case "resources": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_RESOURCES", "Resources"); break;
											case "instructions": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_INSTRUCTIONS", "Instructions & Checklists"); break;
											case "tests": $text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_TAB_TESTS", "Training Tests"); break;
		    							}

		    							/*
		    							switch ($language) {
		    								case "english": 
		    								{
				    							switch ($tab) {
				    								case "training": $text = "Training"; break;				    								
				    								case "additional": $text = "Additional Training"; break;
						        					case "webinars": $text = "Webinars"; break;
						        					case "resources": $text = "Resources"; break;
						        					case "instructions": $text = "Instructions & Checklists"; break;
						        					case "tests": $text = "Training Tests"; break;				        					
				    							}
		    								}
		    								break;

		    								case "french": 
		    								{
				    							switch ($tab) {
				    								case "training": $text = "Entraînement"; break;				    						
				    								case "additional": $text = "Formation Complémentaire"; break;
						        					case "webinars": $text = "Webinaires"; break;
						        					case "resources": $text = "Ressources"; break;
						        					case "instructions": $text = "Instructions & Listes de Contrôle"; break;
						        					case "tests": $text = "Tests d'évaluation"; break;				        					
				    							}
		    								}
		    								break;
			    						}
			    						*/

		    							?>
		    								<a data-toggle="pill" href="#<?php echo $tab ?>"><?php echo $text ?></a>
		    								</li>
		    							<?php
		    						}
		    					}
		    					
		    				?>
	    					
	    				</ul>

	    				<div class="tab-content" style="display: block; padding: 30px;">
				    	<?php
					    	$count = 0;
					    	//$filesUpdated = 0;					    	
					    	//$startTime = microtime(true);
					    	foreach ($this->lessons as $key => $thisLesson) {
				    	?>
				    			<?php
				    				if ($key == 'training') {
				    					$active = 'active';
				    				} else {
				    					$active = '';
				    				}
				    			?>

					        	<div class="tab-pane <?php echo $active; ?>" id="<?php echo $key; ?>">
									<div class="course-lessons">
										<ul class="list-unstyled">
						
										<?php 
											$lessonCount = 0;
											foreach ($this->lessons[$key] as $lesson) {

												$lessonCount++;												
												$lesson_id = $lesson->splms_lesson_id;

												$lessonMaterials = json_decode($lesson->teacher_materials);
												$quizRequirements = json_decode($lesson->required_quizzes);

												//Check to see if the quiz requirements for the lesson have been taken.
												$quizPassed = true;
												$quizResults = null;
												if ($quizRequirements != null) {													
													$quizResults = $model->checkQuizPassed($user_id, $quizRequirements);													
													$quizPassed = $quizResults->passed;
													$quizNeededScore = $quizResults->scores->required;													
												}											

												/*
													Check if they've attempted the quiz.
													If they've attempted it, show the score.
													Check if they've used all of their attempts.
													If not, let them take it again.
												*/

												$quizAttempted = false;
												$quizAttemptsLeft = true;
												$attempt = $attempts[$lesson->splms_lesson_id];
												if ($attempt) {
													if ($attempt->tries > 0) {
														$quizAttempted = true;														
													}

													if ($attempt->tries < $attempt->total) {
														$quizAttemptsLeft = true;
													} else {
														$quizAttemptsLeft = false;
													}
												} 

												$videoDuration = 0;
												$audioDuration = 0;
												
												//Does the file need to be updated in the database?
												$update = false;												
												
												//Check all audio files
												for ($i = 0; $i < count($lessonMaterials->audio_file); $i++) {
													//Check if there is an audio file
													if ($lessonMaterials->audio_file[$i] != "") {
														//See if it has a duration
														if ($lessonMaterials->audio_duration[$i] == "") {
															//If not, get it's info, and set update to true
															$audio_file_info = $getID3->analyze($lessonMaterials->audio_file[$i]);
															$duration = $audio_file_info['playtime_seconds'];
															$audioDuration += $duration;											
															$lessonMaterials->audio_duration[$i] = $duration;											
															$update = true;															
														} else {
															//It already has a duration, so use it.
															$audioDuration += $lessonMaterials->audio_duration[$i];
														}
													}
												}

												//Check all video files
												for ($i = 0; $i < count($lessonMaterials->video_file); $i++) {
													//Check if there is an video file
													if ($lessonMaterials->video_file[$i] != "") {
														//See if it has a duration
														if ($lessonMaterials->video_duration[$i] == "") {
															//If not, get it's info, and set update to true
															$video_file_info = $getID3->analyze($lessonMaterials->video_file[$i]);
															$duration = $video_file_info['playtime_seconds'];
															$videoDuration += $duration;
															$lessonMaterials->video_duration[$i] = $duration;
															$update = true;
														} else {
															//It already has a duration, so use it.
															$videoDuration += $lessonMaterials->video_duration[$i];
														}
													}
												}

												if ($update) {
													$model->update_lesson_materials($lesson_id, $lessonMaterials);
												}

												if ($lesson->lesson_type == 0 || $unlocked) {
													$accessible = true;
												} else {
													$accessible = false;
												}

												if ($accessible && $quizPassed) {
								
													$quiz = null;
													$quiz_id = null;
													$quiz_taken = false;
													$accurate_thought = null;

													foreach ($accuratethoughts as $thought) {
														if (($thought->course_id == $course_id) && ($thought->lesson_id == $lesson_id)) {
															$accurate_thought = $thought;
														}
													}

													$highest_score = 0;
													foreach ($quizquestions as $quizzes) {
														if (($quizzes->splms_lesson_id == $lesson_id) && ($quizzes->splms_course_id == $course_id)) {
															$quiz = $quizzes;
															$quiz_id = $quiz->splms_quizquestion_id;
															
															foreach($user_quiz_results as $quiz_results) {
																if ($quiz_results->splms_quizquestion_id == $quiz_id) {
																	if ($quiz_results->point > $highest_score) {
																		$highest_score = $quiz_results->point;
																	}

																	$quiz_taken = true;
																}
															}
														}
													}
										?>

													<li class="lesson_button" style="border-bottom: 1px solid rgb(220, 220, 220);">
														<span onclick="show_lesson(<?php echo $lesson_id?>)" style="width: 100%;">
															<span id="lesson_icon_<?php echo $lesson_id; ?>" style="font-size: 18px; color: rgb(160,160,160);">
																<span class="lizicon-circle-right" style="color:#337ab7; font-size:18px;"></span>															
															</span>
															<span style=" font-size: 20px; " class="lesson-title">
																<?php echo $lessonCount . " - " . $lesson->title; ?>
															</span>
															<span class="pull-right" style="font-size: 15px;">
																<strong>
																	<?php echo AxsLanguage::text("COM_SPLMS_AXS_TOTAL_DURATION", "Total Duration") . ": " . gmdate("H:i:s", $videoDuration + $audioDuration); ?>
																</strong>
															</span>
														</span>
														<div id="lesson_material_<?php echo $lesson_id; ?>" style="display:none;" class="lesson_material">
														<?php											
															for ($whichLesson = 0; $whichLesson < count($lessonMaterials->splms_teacher_id); $whichLesson++) {

																$video_duration = gmdate("H:i:s", (float)$lessonMaterials->video_duration[$whichLesson]);
																$audio_duration = gmdate("H:i:s", (float)$lessonMaterials->audio_duration[$whichLesson]);
																
																$teacher_id = $lessonMaterials->splms_teacher_id[$whichLesson];
																	
																$audio_file = $lessonMaterials->audio_file[$whichLesson];
																$video_file = $lessonMaterials->video_file[$whichLesson];
																$pdf_file = $lessonMaterials->pdf_file[$whichLesson];
																$video_thumb = $lessonMaterials->video_thumb[$whichLesson];

																$teacher = SplmsModelTeachers::getTeacher($teacher_id);
														?>																
				                                                <div class="comment-img">
		                                                            <a href="<?php echo $teacher->url ?>">
				                                                    	<img class="bw" src="<?php echo $teacher->image; ?>">
		                                                            </a>
				                                                </div>
																<div class="teacher-name lesson-blue">
																	<a href="<?php echo $teacher->url ?>">
																		<?php echo $teacher->title; ?>
																	</a>
																	<br/>
				                                                    <span class="sub-heading">
				                                                    	<?php echo $teacher->sub_heading;?>
				                                                    </span>
				                                                    <br/>
		                                                            <span>
					                                                    <?php
					                                                    	$rating_id = "course_" . $course_id . "_lesson_" . $lesson_id . "_teacher_" . $teacher_id;
					                                                    	$rating_text = array(
													                    		AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_1", "It was okay"),
													                    		AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_2", "Made me think"),
													                    		AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_3", "Inspired me"),
													                    		AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_4", "Changed my day"),
													                    		AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_5", "Changed my life")
													                    	);

					                                                    	echo display_ratings($rating_id, $rating_text);
					                                                    ?>
					                                                </span>
				                                                </div>
						                                                
																<div class="lesson-icons lesson-blue">

																	<?php
																		if ($video_file) {
																	?>
					                                                    	<div class="pull-left center pad-right">
					                                                    	<?php if ($media_mode == 'onPage') { ?>
																				<span 
																					class="lizicon-film icon-font" 
																					onclick="toggle_media(<?php echo $lesson_id . ',' . $teacher_id ?>, 'video')">
																				</span>
																			<?php } ?>
																			<span 
																					class="lizicon-film icon-font ginAudioPopup"
																					title="<?php echo $lesson->title; ?>" 
																					alias="<?php echo $video_file; ?>" 
																					header="<?php echo $lesson->title; ?>">
																				</span>
																				<br/>
					                                                        	<span class="comment-date"><?php echo ($video_duration)?></span>
					                                                    	</div>
			                                                    	<?php
			                                                    		}

			                                                    		 if ($audio_file) {
			                                                    	?>
					 														<div class="pull-left center pad-right">
																				<span 
																					class="lizicon-headphones icon-font ginAudioPopup"
																					title="<?php echo $lesson->title; ?>" 
																					alias="<?php echo $audio_file; ?>" 
																					header="<?php echo $lesson->title; ?>">
																				</span>
																				<br/>
					     														<span class="comment-date"><?php echo ($audio_duration)?></span>
					 														</div>

			 														<?php
			 															}

			 															if ($pdf_file) {
			 														?>
					 														<a href="<?php echo $pdf_file?>" target="_blank">
						 														<div class="pull-left center pad-right">
																					<span class="lizicon-file-text2 icon-font"></span>
																					<br/>
																					<span class="comment-date">PDF</span>
						                                                    	</div>
						                                                    </a>
				                                                   	<?php
				                                                   		}
				                                                   	?>
				                                                    <div class="pull-left center pad-right">
				                                                    	<span 
				                                                    		class="lizicon-bubble2 icon-font"
				                                                    		onclick="toggle_media(<?php echo $lesson_id . ',' . $teacher_id ?>, 'comment')">
				                                                    	</span>
				                                                    	<br>
				                                                    	<span class="comment-date">
				                                                    		<?php echo AxsLanguage::text("COM_SPLMS_AXS_LESSON_COMMENT", "Comment"); ?>
				                                                    	</span>
				                                                    </div>
			                                                    </div>
																<div class="clearfix" style="margin-bottom:25px;"></div>

																<?php if ($media_mode == 'onPage') { ?>
																	<div 
						                                               	id="lesson_video_<?php echo $lesson_id . '_' . $teacher_id?>" 
						                                             	style="display:none; width: 100%;"
						                                            >
																		<video width="800" controls preload="none">
																			<source src="<?php echo $video_file; ?>" type="video/mp4">
																		</video>  <a class="btn btn-primary btn-margin" href="/download.php?file=<?php echo urlencode($video_file); ?>"  target="_blank"><span class="lizicon lizicon-download"></span> <?php echo JText::_('COM_SPLMS_AXS_DOWNLOAD');?></a>
																	</div>
																
																	<div 
																		id="lesson_audio_<?php echo $lesson_id . '_' . $teacher_id?>" 
																		style="display:none;"
																	>
																		<audio controls="controls" preload="none">
																	   		<source src="<?php echo $audio_file; ?>" type="audio/mpeg">
																	   	</audio> <a class="btn btn-primary btn-margin"  href="/download.php?file=<?php echo urlencode($audio_file); ?>" target="_blank"><span class="lizicon lizicon-download"></span> <?php echo JText::_('COM_SPLMS_AXS_DOWNLOAD');?></a>
																	</div>
																<?php } ?>

																<div
																	id="lesson_comments_<?php echo $lesson_id . '_' . $teacher_id ?>"
																	style="display:none; width: 100%;"
																>
																	<?php
																		$comment_id = "course_" . $course_id . "_lesson_" . $lesson_id . "_teacher_" . $teacher_id;

																		$params = new stdClass();

											                            $params->title = AxsLanguage::text("COM_SPLMS_AXS_LESSON_COMMENTS_HOVER", "Lesson Comments");
											                            $params->button_title = AxsLanguage::text("COM_SPLMS_AXS_LESSON_COMMENTS_BUTTON", "Comment");
											                            $params->placeholder = AxsLanguage::text("COM_SPLMS_AXS_LESSON_COMMENTS_PLACEHOLDER", "Your comment...");
											                            $params->post_id = $comment_id;

											                            display_comments($params);
																	?>
																</div>

														<?php
															}
														?>
											
															<div id="thoughts_view_<?php echo $lesson_id; ?>" style="display:none;">
																<textarea id="your_accurate_thoughts_<?php echo ($lesson_id);?>" rows="15" cols="75"></textarea>
																<span 
																	class="btn btn-primary" 
																	onclick="save_accurate_thoughts(<?php echo ($user_id . ', ' . $course_id . ', ' . $lesson_id); ?>)"
																	style="margin-left:20px; margin-bottom: 25px;"
																>
																	<?php echo AxsLanguage::text("COM_LMS_SUBMIT", "Submit") ?>
																</span>
															</div>

														<?php
															//Must have a quiz id, or there is no quiz
															//Must not have taken the quiz

															// old if statement 
															//if ($quiz_id && !$quiz_taken) {
															if ($quiz_id) {

																if ($quizAttemptsLeft) {
																	$alias = "//" . $_SERVER['HTTP_HOST'].  "/quiz?view=quizquestion&tmpl=component&id=" . $quiz_id;
														?>
																	<span
																		id="take_quiz_button_<?php echo $quiz_id; ?>"																	
																		alias="<?php echo $alias; ?>"
																		class="quizButton btn btn-primary"
																		data-mediabox-height="450"
																		data-mediabox-width="800"
																	>
																		<?php echo AxsLanguage::text("COM_SPLMS_AXS_LESSON_TAKE_QUIZ", "Take the Quiz") ?>
																	</span>
														<?php
																} else {
														?>
																	<span
																		id="take_quiz_button_<?php echo $quiz_id; ?>"
																		class="quizButton quiz_results"
																		style="background-color: rgb(160,130,130);"
																	>
																		<?php echo AxsLanguage::text("COM_SPLMS_AXS_NO_MORE_QUIZ_ATTEMPTS", "No more quiz attempts allowed"); ?>
																	</span>
														<?php
																}																
															}

															//Only show accurate thoughts if there is no quiz id OR (the quiz has been taken and there is no accurate thought)
															if ((!$quiz_id || $quiz_taken) && !$accurate_thought) {
																$accuratethoughtbutton_style = "";
															} else {
																$accuratethoughtbutton_style = "display:none;";
															}
														?>
															<span 
																id="accurate_thoughts_button_<?php echo $lesson_id; ?>" 
																class="btn btn-primary" 
																onclick="give_thoughts(<?php echo ($lesson_id);?>)"
																style = "<?php echo $accuratethoughtbutton_style;?>"
															>

																<?php /*Your Accurate Thoughts*/ ?>
																<?php echo AxsLanguage::text("COM_SPLMS_LESSON_NOTES", "Lesson Notes") ?>
															</span>
															<br>
														<?php

															if ($quiz_id && $quiz_taken) {
																$quiz_style = "";
															} else {
																$quiz_style = "display:none;";
															}

															if (!$accurate_thought) {
																$accuratethought_style = "display:none;";
															} else {
																$accuratethought_style = "";
															}

															if ($quizAttempted) {
																$score_style = "display: inline-block";
															} else {
																$score_style = "display: none;";
															}
														?>

															<div id="quiz_results_<?php echo ($lesson_id); ?>" class="quiz_results" style="<?php echo $score_style; ?>">
																<?php echo AxsLanguage::text("COM_SPLMS_QUIZ_RESULTS_CAPITALIZED", "QUIZ RESULTS") ?>
																<br>
																<div>
																	<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_SCORE", "Score") ?>: 
																	<span id="quiz_score_<?php echo $quiz_id ?>" style="float: right">
																		<?php 
																			echo (int)(($highest_score * 100) / $quiz_results->total_marks) . "%";
																		?>
																	</span>
																</div>
																<div>
																	Points: 
																	<span id="quiz_points_<?php echo $quiz_id ?>" style="float: right">
																		<?php
																			echo $highest_score . " / " . $quiz_results->total_marks;
																		?>
																	</span>
																</div>
															</div>

															<div 
																id="accurate_thought_<?php echo ($lesson_id); ?>" 
																class="quiz_results" 
																style="<?php echo $accuratethought_style ?>  width: 200px; height:100px;"
															>
																<?php echo AxsLanguage::text("COM_SPLMS_AXS_LESSON_NOTES", "LESSON NOTES"); ?>:
																<br>
																<div id="accurate_thought_text_<?php echo ($lesson_id); ?>">
																	<?php echo $accurate_thought->accurate_thought?>
																</div>
															</div>
														</div>
													</li>
											<?php 			
												} else {
											?>
													<li 
														class="splms-lesson-unauthorised" 
														style="margin-bottom:0px; font-size: 20px; color: rgb(96, 96, 96);" 
														<?php if ($quizPassed == false) { echo "title = " . AxsLanguage::text("COM_SPLMS_HAVE_NOT_PASSED_REQUIRED_QUIZZES", "You have not passed the required quiz(zes).'"); } ?>
													>
														<span>
															<i class="splms-icon-lock"></i>
															<?php echo $lesson->title; ?>
														</span>
														<span class="pull-right" style="font-size: 15px;">
															<strong>
																<?php echo AxsLanguage::text("COM_SPLMS_AXS_TOTAL_DURATION", "Total Duration") . ": " . gmdate("H:i:s", $videoDuration + $audioDuration); ?>
															</strong>
														</span>

														<?php
															//Do not show quiz results if it's not even accessible.
															if ($accessible && !$passed) {
																if (!isset($quizNotPassedCount)) {
																	$quizNotPassedCount = 0;
																} else {
																	$quizNotPassedCount++;
																}

																$listTitle = "quizNotPassedList_" . $quizNotPassedCount;
														?>
																<dir style="margin: 4px 0px 0px 0px; font-size: 12px;">
																	<span id="<?php echo $listTitle?>_down" class="lizicon-circle-right"></span>
																	<span id="<?php echo $listTitle?>_right" class="lizicon-circle-down" style="display:none;"></span>
																	<span style="color: rgb(96, 64, 64);" onclick="toggleQuizList('<?php echo $listTitle ?>')">
																		<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_MUST_PASS_QUIZZES_FOR_ACCESS", "You need to pass the following quizzes to gain access") . ":"; ?>
																	</span>

																	<table id="<?php echo $listTitle ?>" style="display:none; margin-top: 15px;">
																		<?php
																			foreach ($quizResults->scores as $quiz) {
																		?>
																			<tr style="margin: 4px 0px 0px 20px; padding: 2px;">
																		<?php
																				$td = "<td style='padding: 0px 8px 0px 10px;'>";
																				$redSpan = "<span style='color: red;'>";
																				$greenSpan = "<span style='color: green;'>";
																				echo $td . $quiz->title . "</td>";
																				if ($quiz->score == null) {
																					//Not taken
																					echo $td . $redSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_NOT_TAKEN", "Not taken") . ".</span></td>";
																				} else {
																					if ($quiz->score > $quiz->required) {
																						//Passed
																						echo $td . $greenSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_PASSED", "Passed") . "</span></td>";
																					} else {
																						//Failed
																						echo $td . $redSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_SCORE", "Score") . ": " . $quiz->score . "% - " . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_REQUIRED", "Required") . ": " . $quiz->required . "%</span></td>";
																					}
																				}
																		?>
																			</tr>
																		<?php
																			}
																		?>																		
																	</table>
																</dir>
														<?php
															}
														?>
													</li>
										<?php 			
												} // End if else
											} // End foreach											
											
										?>
										</ul>
									</div>
								</div> <!-- end course-lessons -->
				<?php 
							}	 //End foreach							
					}
					//$endTime = microtime(true);
					//$runTime = $endTime - $startTime;
					//echo ("<script>alert('" . $filesUpdated . " file times updated.\\nPage load took " . $runTime . " seconds');</script>");					
				?>
        		</div>
            </div>
        </div>
    </div>
</div>

<script>
function toggleQuizList(id) {
	var div = $("#" + id);
	var down = $("#" + id + "_down");
	var right = $("#" + id + "_right");

	var visible = div.is(":visible");

	if (visible) {
		div.hide(250);
		down.show();
		right.hide();
	} else {
		div.show(250);
		down.hide();
		right.show();
	}
}

$(".addtocart").click(
	function() {
		var which = this.getAttribute("product-id");
		
		$.ajax({
			url: '/index.php?option=com_splms&task=cart.addItem&format=raw',
			data: {
				product_id: which
			},
			type: 'post'
		}).done(function(result) {
			//console.log(result);
			window.location = result;
		}).fail(function() {
			alert("<?php echo AxsLanguage::text("COM_SPLMS_AXS_UNABLE_TO_REMOVE_ITEM", "Unable to remove item.  Please try again.") ?>");
		});
	}
);
</script>
