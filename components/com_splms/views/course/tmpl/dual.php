<?php

/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

defined('_JEXEC') or die();

//require_once('assets/getID3/getid3/getid3.php');
require_once('components/shared/controllers/comments.php');
require_once('components/shared/controllers/rating.php');
require_once('components/com_community/libraries/core.php');
//$getID3 = new getID3;
$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_splms/assets/css/course.css', array('version' => 'auto'));
$doc->addScript('components/com_splms/assets/js/course.js', array('version' => 'auto'));
/*$doc->addScript('components/shared/includes/js/easy-pie-chart.js');*/

$user_id  = JFactory::getUser()->id;
$settings = json_decode(AxsLMS::getSettings()->params);
$language = AxsLanguage::getCurrentLanguage()->get('tag');

//$accessLevelName =  AxsExtra::accessLevelName($this->item->access);
$hasBoughtCourse = AxsPayment::hasUserPurchasedCourse($this->item->splms_course_id, $this->user->id);
$subs = AxsPayment::getUserSubscriptions($user_id);
$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if ($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if ($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

//Check to see if the course is not open yet or if it's expired
$now = strtotime('now');
$course_closed_status = null;
$course_closed = false;
if ($course_params->course_close_date) {
	if ($now > strtotime($course_params->course_close_date)) {
		if (!$course_params->course_visible_after_close) {
			//The course is closed and no longer visible. They've followed an old link.
			echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_NOT_AVAILABLE", "The selected course is not available.");
			return;
		} else {
			$course_closed_status = "closed";
			$course_closed = true;
		}
	}
}

if ($course_params->course_open_date) {
	if ($now < strtotime($course_params->course_open_date)) {
		if (!$course_params->course_visible_before_open) {
			//The course is not open yet and not available for viewing.
			echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_NOT_AVAILABLE", "The selected course is not available.");
			return;
		} else {
			$course_closed_status = "not open";
			$course_closed = true;
		}
	}
}

if (!isset($course_params->lesson_gating)) {
	$course_params->lesson_gating = 2;
}

$coverImage = $course_params->cover_image;
if (!$coverImage && $brand->default_settings->lms_cover_image) {
	$coverImage = $brand->default_settings->lms_cover_image;
}

$width = 'container-fluid';
$display = $course_params->display;
if ($display) {
	if ($display == 'fullwidth') {
		$width = 'container-fluid';
	}
}

if ($course_params->progress_color) {
	$progressColor = $course_params->progress_color;
} else {
	$progressColor = '#31a3fa';
}

$scormAccess = AxsDbAccess::getAccessLevels('scorm');
$contentLibraryAccess = AxsDbAccess::getAccessLevels('bizlibrary');
$globalContentLibraryType = AxsDbAccess::getAccessLevels('bizlibrary_content_selection');

$isActiveMember = true;

if (!$brand->site_details->plans_for_active) {
	$isActiveMember = true;
} else {
	foreach ($subs as $s) {
		$brandPlans = $brand->site_details->plans_for_active;
		if (($s->status == "ACT" || $s->status == "GRC") && in_array($s->plan_id, $brandPlans)) {
			$isActiveMember = true;
			break;
		}
	}
}

$progress = $model->getProgress($this->item->splms_course_id, null, $user_id, 'completed');

$progressList = $model->getProgress($this->item->splms_course_id, null, $user_id, null);
$courseProgress = AxsLMS::getCourseProgress($this->item->splms_course_id, $user_id);

$lessons = $model->getLessons($this->item->splms_course_id, null);
/*$totalLessons = count($lessons);
$completedLessons = count($progress);
$percentage = round(($completedLessons / $totalLessons) * 100);*/
$percentage = round($courseProgress->progress);
$studentList = SplmsModelCourses::getCourseStudents($this->item->splms_course_id);

$studentIdList = array();
foreach ($studentList as $student) {
	$studentIdList[] = $student->user_id;
}

?>
<?php
// check user access for this course
$course_params->access = $this->item->access;
$visibilityAccess = AxsContentAccess::checkVisibilityAccess($user_id, $this->item->splms_course_id);
$registerAccess = AxsContentAccess::checkContentAccess($user_id, $course_params);
$categoryId = $this->item->splms_coursescategory_id;
$categoryVisibilityAccess = AxsContentAccess::checkCategoryAccess($user_id, $categoryId);
$assigned = AxsLMS::getUserAssignedData($user_id, $this->item->splms_course_id, null);
if ($assigned->free) {
	$this->item->price = 0;
}
if (!$visibilityAccess || !$categoryVisibilityAccess) {
	echo AxsLanguage::text("AXS_NO_ACCESS_FOR_YOU", "You don't have access to this course");
} else { ?>

	<?php
	$buttonText = $currency_symbol . $this->item->price . ' - ' . JText::_('COM_SPLMS_AXS_BUY_NOW');

	if ($isActiveMember && ($hasBoughtCourse || $this->item->price == 0) && $visibilityAccess && ($registerAccess || $hasBoughtCourse)) {
		$buyButton = false;
		$unlocked = true;
	}

	if (!$hasBoughtCourse && $this->item->price > 0 && $visibilityAccess && $registerAccess) {
		$buyButton = true;
	}

	$award = '';
	$awardColor = '#31a3fa';
	if ($course_params->award_color) {
		$awardColor = $course_params->award_color;
	}

	if ($course_params->award_type != 'none' || !$course_params->award_type) {
		switch ($course_params->award_type) {
			case 'image':
				$award = '<span><img src="' . $course_params->award_image . '" ' . AxsAccessibility::image($course_params->award_image) . '/></span>';
				break;
			case 'icon':
				$award = '<span class="' . $course_params->award_icon . '"></span>';
				break;
		}
	}

	$minimumStudentsMet = true;
	$numberOfStudents = count($studentList);

	if ($course_params->require_minimum_students == "yes") {
		$currentStudentCount = $numberOfStudents;
		$requiredStudentCount = $course_params->minimum_students;

		if ($currentStudentCount < $requiredStudentCount) {
			$minimumStudentView = $model->getMinimumStudentFailView($course_params, $currentStudentCount);
			$minimumStudentsMet = false;
		}
	}

	?>
	<style>
		body {
			background: #eee;
		}

		.lesson-title {
			font-size: 20px;
			padding: var(--padding-2);
			margin-left: 24px;
			flex: 1;
			line-height: 125%;
		}

		.no-padding {
			padding: 0px;
		}

		.cover-image {
			background-repeat: no-repeat;
			background-position: center;
			background-size: cover;
			width: 100%;
			height: 300px;
		}

		@media(max-width: 768px) {
			.cover-image {
				height: 200px;
			}
		}

		.lms-subtitle {
			font-size: 22px;
			margin-top: -15px;
			margin-left: -15px;
		}

		.indent-level-1 {
			margin-left: 50px !important;
		}

		.indent-level-2 {
			margin-left: 70px !important;
		}

		.indent-level-3 {
			margin-left: 90px !important;
		}

		.indent-level-4 {
			margin-left: 110px !important;
		}

		.indent-level-5 {
			margin-left: 130px !important;
		}

		.course-lessons ul li a {
			width: 100%;
		}

		.list-unstyled {
			margin: 0px;
			padding: 0px;
			grid-auto-columns: 1fr;
			grid-column-gap: 16px;
			grid-row-gap: 16px;
			-ms-grid-columns: 1fr 1fr;
			grid-template-columns: 1fr 1fr;
			-ms-grid-rows: auto auto;
			grid-template-rows: auto auto;
			list-style-type: none;
		}

		.lesson_button {
			position: relative;
			overflow: hidden;
			margin-bottom: 8px;
			padding: 0px;
			border-style: solid;
			border-width: 1px;
			border-color: #ccc;
			border-radius: 8px;
			-webkit-transition: all 200ms ease;
			transition: all 200ms ease;
			color: #5000cf;
		}

		.lesson_button:hover {
			color: #333;
		}

		.test-section {
			width: 764px;
			height: 100%;
			padding: 20px;
		}

		.lesson-thumb {
			overflow: hidden;
			width: 72px;
			height: 72px;
			min-height: 50px;
			min-width: 50px;
			margin: -6px 0px 0px 53px;
			border-radius: 4px;
			background-size: cover;
			background-attachment: scroll;
			display: flex;
			align-items: center;
			justify-content: center;
		}

		.lesson-thumb.no-img {
			background-image: none;
			background-size: auto;
			background-attachment: scroll;
		}

		.body {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-align: start;
			-webkit-align-items: flex-start;
			-ms-flex-align: start;
			align-items: flex-start;
			font-family: Inter, sans-serif;
		}

		.lesson-duration {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			margin-left: 0px;
			padding: 8px;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
			border-style: solid none solid solid;
			border-width: 1px;
			border-color: #ccc;
			border-radius: 4px 0px 0px 4px;
			background-color: #f3f3f3;
			color: #333;
			font-weight: 600;
			text-align: center;
		}

		.duration-p {
			margin-bottom: 0px;
			margin-top: 0px;
		}

		.lesson_button_a {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			-webkit-box-align: start;
			-webkit-align-items: flex-start;
			-ms-flex-align: start;
			align-items: flex-start;
			color: #333;
			text-decoration: none;
		}

		.lesson-thumb-go {
			display: -webkit-box;
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			width: 100%;
			height: 100%;
			-webkit-box-pack: center;
			-webkit-justify-content: center;
			-ms-flex-pack: center;
			justify-content: center;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-flex-align: center;
			align-items: center;
		}

		.image {
			max-height: 24px;
		}

		@media screen and (max-width: 991px) {
			.lesson_button {
				-webkit-box-align: start;
				-webkit-align-items: flex-start;
				-ms-flex-align: start;
				align-items: flex-start;
			}

			.lesson-thumb.no-img {
				background-image: none;
				background-size: auto;
				background-attachment: scroll;
			}

			.lesson-thumb {
				margin: -9px 0px 0px 15px;
			}

		}

		@media screen and (max-width: 767px) {
			.lesson-title {
				font-size: 16px;
			}

			.lesson-duration {
				display: none;
			}

			.lesson-thumb {
				margin: -9px 0px 0px 15px;
			}
		}

		@media screen and (max-width: 479px) {
			.lesson_button {
				-webkit-box-orient: vertical;
				-webkit-box-direction: normal;
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column;
			}

			.test-section {
				padding: 8px;
			}

			.lesson-thumb {
				width: 100% !important;
				height: 250px !important;
				margin: -9px 0px 0px 15px;
				border-bottom-left-radius: 0px;
				border-bottom-right-radius: 0px;
			}

			.lesson-title {
				margin-left: 0px;
				padding: 8px;
				font-size: 24px !important;
				margin-top: 15px;
			}

			.lesson-duration {
				display: -webkit-box;
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;
				width: 100%;
				border-top-style: solid;
				border-bottom-style: none;
				border-left-style: none;
				border-top-left-radius: 0px;
				border-top-right-radius: 0px;
				border-bottom-right-radius: 4px;
			}

			.lesson_button_a {
				-webkit-box-orient: vertical;
				-webkit-box-direction: normal;
				-webkit-flex-direction: column;
				-ms-flex-direction: column;
				flex-direction: column;
			}

			.indent-level-1 {
				margin-left: 0px !important;
			}

			.indent-level-2 {
				margin-left: 0px !important;
			}

			.indent-level-3 {
				margin-left: 0px !important;
			}

			.indent-level-4 {
				margin-left: 0px !important;
			}

			.indent-level-5 {
				margin-left: 0px !important;
			}
		}
	</style>
	<script type="text/javascript">
		function toggleQuizList(id) {
			var div = $("#" + id);
			var down = $("#" + id + "_down");
			var right = $("#" + id + "_right");

			var visible = div.is(":visible");

			if (visible) {
				div.slideToggle(250);
				down.show();
				right.hide();
			} else {
				div.slideToggle(250);
				down.hide();
				right.show();
			}
		}

		function getThumbnail(element) {
			/* Configuration variables */
			var MAX_COLOR = 200; // Max value for a color component
			var MIN_COLOR = 120; // Min value for a color component
			var FILL_CHANCE = 0.5; // Chance of a square being filled [0, 1]
			var SQUARE = 10; // Size of a grid square in pixels
			var GRID = 8; // Number of squares width and height
			var PADDING = SQUARE / 2; // Padding on the edge of the canvas in px
			var SIZE = SQUARE * GRID + PADDING * 2; // Size of the canvas
			var FILL_COLOR = '#ccc'; // canvas background color

			/* Create a temporary canvas */
			function setupCanvas() {
				var canvas = document.createElement('canvas');
				canvas.width = SIZE;
				canvas.height = SIZE;

				// Fill canvas background
				var context = canvas.getContext('2d');
				context.beginPath();
				context.rect(0, 0, SIZE, SIZE);
				context.fillStyle = FILL_COLOR;
				context.fill();
				return canvas;
			}

			/* Fill in a square of the canvas */
			function fillBlock(x, y, color, context) {
				context.beginPath();
				context.rect(PADDING + x * SQUARE, PADDING + y * SQUARE, SQUARE, SQUARE);
				context.fillStyle = 'rgb(' + color.join(',') + ')';
				context.fill();
			}

			/* Generate a random color with low saturation. */
			function generateColor() {
				var rgb = [];
				for (var i = 0; i < 3; i++) {
					var val = Math.floor(Math.random() * 256);
					var minEnforced = Math.max(MIN_COLOR, val);
					var maxEnforced = Math.min(MAX_COLOR, minEnforced);
					rgb.push(maxEnforced);
				}
				return rgb;
			};

			/* Generate a random identicon */
			function generateIdenticon() {
				var canvas = setupCanvas();
				var context = canvas.getContext('2d');
				var color = generateColor(); // Generate custom tile color

				// Iterate through squares on left side
				for (var x = 0; x < Math.ceil(GRID / 2); x++) {
					for (var y = 0; y < GRID; y++) {
						// Randomly fill squares
						if (Math.random() < FILL_CHANCE) {
							fillBlock(x, y, color, context);

							// Fill right side symmetrically
							if (x < Math.floor(GRID / 2)) {
								fillBlock((GRID - 1) - x, y, color, context);
							}
						}
					}
				}
				return canvas.toDataURL();
			}

			// Attach finished identicon to DOM
			var urlDataImage = generateIdenticon(); // Generate identicon
			var image = new Image; // Create new image object
			image.src = urlDataImage; // Assign url data to image
			document.getElementById(element).appendChild(image);
		}
	</script>
	<div class="row mt-3">
		<div class="<?php echo $width; ?>">
			<div id="splms" class="splms view-splms-course course-details">
				<?php
				// If the user completed the course prior to the due date, don't display this message
				//$completed_prior_due_date = $courseProgress->progress == "100" && ($courseProgress->date_completed < $courseProgress->date_due);
				if ($course_params->use_due_date && $courseProgress->date_due && $courseProgress->progress != "100") {

					$today = date("M d, Y", strtotime('now'));
					$due = date("M d, Y", strtotime($courseProgress->date_due));

					$todayTimestamp = strtotime('now');
					$dueTimestamp = strtotime($courseProgress->date_due);

					if ($todayTimestamp < $dueTimestamp) {
						//The course is due at a future date
						$due_date_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_DUE", "This course is due on") . ": " . $due;
						$due_date_color = "#777";
					} else if ($todayTimestamp == $dueTimestamp) {
						//The course is due today.
						$due_date_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_DUE_TODAY", "This course is due today.");
						$due_date_color = "#00E";
					} else {
						$due_date_color = "#F00";
						//The course is past due.
						if ($course_params->due_date_past_completion) {
							//They can still complete the course past the due date.
							$due_date_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_PAST_DUE", "This course is past due.");
						} else {
							//They cannot complete the course past due.
							$due_date_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_PAST_DUE_RESTRICTED", "This course is past due and can no longer be completed.");
							$course_closed = true;
						}
					}
				?>
					<div class="clearfix"></div>

					<span style="font-size: 20px; color: <?php echo $due_date_color; ?>">
						<span class="fa fa-calendar" />
						<?php
						echo $due_date_text;
						?>
					</span>

				<?php
				}
				?>
				<div class="splms-course">
					<?php if ($coverImage) { ?>
						<div class="col-md-12 box cover-image" style="background-image: url('<?php echo $coverImage; ?>');" <?php echo AxsAccessibility::image($coverImage); ?>>
							<div style="width:100%; padding: 10px; bottom: 0px; left: 0px;  position:absolute;  background-color: rgba(0, 1, 1, 0.7);">
								<div class="course-title pull-left" style="color:#fff; font-size:26px;"><?php echo $this->item->title; ?></div>
								<?php if ($settings->show_ratings || !isset($settings->show_ratings)) { ?>
									<div class="pull-right lms" style="margin-top:10px;">
										<?php
										$rating_id = "course_" . $this->item->splms_course_id;
										$rating_text = array(
											AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_1", "It was okay"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_2", "Made me think"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_3", "Inspired me"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_4", "Changed my day"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_5", "Changed my life")
										);
										echo display_ratings($rating_id, $rating_text);
										?>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } else { ?>
						<div style="width:100%; padding: 10px; position:relative;  background-color: rgba(0, 1, 1, 0.7);">
							<div class="course-title pull-left" style="color:#fff; font-size:26px;"><?php echo $this->item->title; ?>

							</div>
							<?php if ($settings->show_ratings || !isset($settings->show_ratings)) { ?>
								<div class="pull-right lms" style="margin-top:10px;">
									<?php
									$rating_id = "course_" . $this->item->splms_course_id;
									$rating_text = array(
										AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_1", "It was okay"),
										AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_2", "Made me think"),
										AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_3", "Inspired me"),
										AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_4", "Changed my day"),
										AxsLanguage::text("COM_SPLMS_AXS_RATING_COURSE_5", "Changed my life")
									);
									echo display_ratings($rating_id, $rating_text);
									?>
								</div>
							<?php } ?>
							<div class="clearfix"></div>
						</div>
					<?php } ?>

					<!-- start course-header -->

					<div class="row">
						<div class="clearfix"></div>
						<div class="col-md-12">

							<?php if ($this->item->available) { ?>
								<div class="pull-right">
									<?php if ($buyButton && $this->item->price != 0 && !$course_closed) { ?>
										<button class="addtocart btn btn-primary" product-id="<?php echo $this->item->splms_course_id ?>">
											<?php echo $buttonText; ?>
										</button>
										<button class="btn btn-primary" onclick="window.location='/index.php?option=com_splms&view=cart'">
											<span class="lizicon-cart"></span>
											<?php echo ' ' . JText::_('COM_SPLMS_AXS_SHOPPING_CART'); ?>
										</button>
									<?php } else if (!$user_id) { ?>
										<span class="access"><?php echo AxsLanguage::text("COM_SPLMS_MUST_LOGIN_TO_PURCHASE", "You must first login to purchase this item.") ?></span>
									<?php } else if ($course_closed) { ?>
										<?php /* DISPLAY NOTHING */ ?>
									<?php } else if (!$unlocked) { ?>
										<span class="access"><?php echo AxsLanguage::text("COM_SPLMS_NO_COURSE_ACCESS", "You Don't Have Access to This Course") ?></span>
									<?php } ?>

								</div>
							<?php } ?>
						</div>
						<div class="clearfix"></div>

						<?php
						if ($course_closed_status) {
						?>
							<div class="col-md-12">
								<?php
								switch ($course_closed_status) {
									case "not open":
										echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_NOT_OPEN_YET", "This course is not available until") . $course_params->course_open_date;
										break;
									case "closed":
										echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_CLOSED", "This course has closed");
										break;
								}
								?>
							</div>
							<div class="clearfix"></div>
						<?php
						} else if (!$minimumStudentsMet) {
						?>
							<div class="col-md-12">
								<?php echo $minimumStudentView; ?>
							</div>
							<div class="clearfix"></div>
						<?php
						}
						?>

						<?php
						$course_video = AxsLMS::getVideo($this->item);
						$descriptionClass = 'col-md-12';
						if ($course_video) {
							$descriptionClass = 'col-md-7';
						?>
							<div class="col-md-5">
								<div class="box no-padding video-content">
									<?php echo $course_video; ?>
								</div>
							</div>
						<?php
						}
						?>
						<?php if ($this->item->description) { ?>
							<div class="course-description <?php echo $descriptionClass; ?>">
								<div class="box" style="min-height:257px;">
									<?php echo $this->item->description; ?>
								</div>
							</div>
						<?php } ?>
					</div>

					<!-- end course-header -->
					<?php if ($course_params->use_library_content && ($course_params->scorm_id || $course_params->bizlibrary_content_id) && $user_id && ($scormAccess || $contentLibraryAccess)) { ?>
						<?php if ($unlocked) { ?>
							<div class="clearfix"></div>
							<div class="row">
								<div class="col-md-12">
									<div class="box">
										<?php
										$scormParams = new stdClass();
										$scormParams->course_id = $this->item->splms_course_id;
										$scormParams->user_id = $user_id;
										$scormParams->language = $language;

										if ($course_params->library_content_type == 'scorm' && $course_params->scorm_id && $scormAccess) {
											$scormParams->scorm_course_id = $course_params->scorm_id;
											$scormParams->scorm_type = 'scormlibrary';
											$scormContent = AxsScormFactory::loadContent($scormParams);
											echo $scormContent;
										}

										if ($course_params->library_content_type == 'bizlibrary' && $course_params->bizlibrary_content_id && $contentLibraryAccess) {

											// Make sure we're actually cleared to display the BizLibrary SCORM content as a secondary layer of permissioning
											$validBizlibraryAccess = true;
											if ($globalContentLibraryType == 'production' && AxsScormFactory::getBizLibraryCourseType($course_params->bizlibrary_content_id) != 'production') {
												$validBizlibraryAccess = false;
											}
											if ($validBizlibraryAccess) {
												$scormParams->scorm_course_id = $course_params->bizlibrary_content_id;
												$scormParams->scorm_type = 'bizlibrary';
												$scormContent = AxsScormFactory::loadContent($scormParams);
												echo $scormContent;
											}
										}
										?>
									</div>
								</div>
							</div>
						<?php } ?>
					<?php } else { ?>
						<div class="clearfix"></div>

						<style>
							.data-view-tab {
								border: 1px solid #777;
								cursor: pointer;
							}

							.data-view-tab:hover {
								background-color: #aaa;
								transition: background-color 0.25s ease;
							}

							.data-view-tab:active {
								background-color: #222;
							}

							.data-view-tab-icon {
								font-size: 14px;
								margin-right: 8px;
								color: gray;
							}

							.data-view-tab-active {
								background-color: #ddd;
							}
						</style>

						<div class="row">
							<div class="col-md-9">
								<div class="box">
									<?php
									if (!$course_closed) {
									?>

										<?php //Only show if the parameter is set, and it's set to true. 
										?>
										<?php if (isset($course_params->show_students) && $course_params->show_students) { ?>
											<div class="btn-lg lms-subtitle pull-left data-view-tab data-view-tab-active" content="lesson">
												<span class="fa fa-clipboard data-view-tab-icon"></span><?php
																										if ($course_params && $course_params->lesson_button_text) {
																											echo $course_params->lesson_button_text;
																										} else {
																											echo AxsLanguage::text("COM_SPLMS_AXS_LESSONS", "Lessons");
																										} ?>
											</div>
											<?php if (!empty($user_id)) : ?>
												<div class="btn-lg lms-subtitle pull-left data-view-tab" content="students" style="margin-left: 20px;">
													<span class="fa fa-users data-view-tab-icon"></span><?php
																										if ($course_params && $course_params->students_button_text) {
																											echo $course_params->students_button_text;
																										} else {
																											echo AxsLanguage::text("COM_SPLMS_AXS_STUDENTS", "Students");
																										} ?>
												</div>
											<?php endif; ?>
										<?php } ?>
										<div class="pull-right data-view-content" content="lesson">
											<input type="text" name="search" id="search" placeholder="<?php echo AxsLanguage::text("COM_SPLMS_AXS_SEARCH_LESSONS", "Search Lessons...") ?>" value="" />
											<span class="btn btn-primary search"><span class="lizicon-search"></span></span>
										</div>
										<div class="clearfix"></div>

										<script>
											$(".data-view-tab").click(
												function() {
													$(".data-view-tab").each(
														function() {
															$(this).removeClass('data-view-tab-active');
														}
													);

													$(this).addClass("data-view-tab-active");
													var content = this.getAttribute("content");
													$(".data-view-content").hide();
													$(".data-view-content").each(
														function() {
															var thisContent = this.getAttribute("content");
															if (thisContent == content) {
																$(this).show();
															}
														}
													);
												}
											);

											setInterval(
												function() {

													var list = '<?php echo json_encode($studentIdList); ?>';
													$.ajax({
														url: '/index.php?option=com_axs&task=onlineCheck.getOnlineFromList&format=raw',
														data: {
															list: list
														},
														success: function(response) {
															$(".student-picture").removeClass("student-online");

															var list = JSON.parse(response);
															for (var i = 0; i < list.length; i++) {
																$(".student-picture-" + list[i]).addClass("student-online");
															}
														}
													});
												}, 10000
											);
										</script>
									<?php
									}
									?>

									<?php
									$course_id = $this->item->splms_course_id;
									$user_id = $this->user->id;
									$user_quiz_results = $this->quizresults;
									$quizquestions = $this->quizquestions;
									$accuratethoughts = $this->accuratethoughts;

									$attempts = $model->getAllAttempts($quizquestions, $user_quiz_results);
									?>

									<!-- Has lesson -->
									<?php
									if (!empty($this->lessons)) {
									?>
										<!-- start course-lessons  -->


										<div class="tab-content data-view-content" content="lesson" style="display: block; padding: 30px;">
											<?php
											$count = 0;
											//$filesUpdated = 0;
											//$startTime = microtime(true);
											$lessonCount = 0;
											$previous_lesson_id = '';
											foreach ($this->lessons as $key => $thisLesson) {
											?>

												<div>
													<div class="course-lessons">
														<ul class="list-unstyled">

															<?php

															foreach ($this->lessons[$key] as $lesson) {
																$lessonCount++;
																$lesson_id = $lesson->splms_lesson_id;
																$lesson_slug = $lesson->slug;

																$lessonParams = json_decode($lesson->params);
																/*if (!$lessonParams->duration) {
																			$lessonParams->duration = "00:00:00";
																		}
*/
																$lessonMaterials = json_decode($lesson->teacher_materials);
																$quizRequirements = json_decode($lesson->required_quizzes);

																//Check to see if the quiz requirements for the lesson have been taken.
																$quizPassed = true;
																$quizResults = null;

																if ($quizRequirements != null) {
																	$quizResults = $model->checkQuizPassed($user_id, $quizRequirements);
																	$quizPassed = $quizResults->passed;
																	$quizNeededScore = $quizResults->scores->required;
																}

																/*
																			Check if they've attempted the quiz.
																			If they've attempted it, show the score.
																			Check if they've used all of their attempts.
																			If not, let them take it again.
																		*/

																$quizAttempted = false;
																$quizAttemptsLeft = true;
																$attempt = $attempts[$lesson->splms_lesson_id];

																if ($attempt) {
																	if ($attempt->tries > 0) {
																		$quizAttempted = true;
																	}

																	if ($attempt->tries < $attempt->total) {
																		$quizAttemptsLeft = true;
																	} else {
																		$quizAttemptsLeft = false;
																	}
																}

																if ($lesson->lesson_type == 0 || $unlocked) {
																	$accessible = true;
																	if ($previous_lesson_id && ($course_params->lesson_gating == 1  || ($settings->lesson_gating == 1 && $course_params->lesson_gating == 2))) {
																		$lessonGating = 1;
																		$previousParams = new stdClass();
																		$previousParams->course_id 	    = $this->item->splms_course_id;
																		$previousParams->lesson_id 	    = $previous_lesson_id;
																		$previousParams->user_id 		= $user_id;
																		$previousParams->language 	    = $language;
																		$previousLessonCompleted = AxsLMS::checkLessonCompletion($previousParams);
																		if (!$previousLessonCompleted) {
																			$accessible = false;
																		}
																	}
																} else {
																	$accessible = false;
																}



																if (!$minimumStudentsMet || $course_closed) {
																	//These will automatically lock all lessons on the course.
																	$accessible = false;
																}

																if ($accessible && $quizPassed) {

																	$quiz = null;
																	$quiz_id = null;
																	$quiz_taken = false;
																	$accurate_thought = null;

																	$highest_score = 0;
																	foreach ($quizquestions as $quizzes) {
																		if (($quizzes->splms_lesson_id == $lesson_id) && ($quizzes->splms_course_id == $course_id)) {
																			$quiz = $quizzes;
																			$quizParams = json_decode($quiz->params);
																			$quiz_id = $quiz->splms_quizquestion_id;

																			foreach ($user_quiz_results as $quiz_results) {
																				if ($quiz_results->splms_quizquestion_id == $quiz_id) {
																					if ($quiz_results->point > $highest_score) {
																						$highest_score = $quiz_results->point;
																					}

																					$quiz_taken = true;
																				}
																			}
																		}
																	}
															?>

																	<li class="lesson_button lessons lesson-list" style="width:100%;">
																		<a href="<?php echo JRoute::_('index.php?view=lesson') . '?id=' . $lesson_id; ?>" class="lesson_button_a">

																			<div id="lesson_id_<?php echo $lesson_id; ?>" class="lesson-thumb lesson-img bw" <?php echo AxsAccessibility::image($lesson->image); ?> <?php if ($lesson->image) {
																																																				$lesson->image = str_replace('https://tovuti.io/', '', $lesson->image);
																																																				$lesson->image = str_replace('https://tovutilms.com/', '', $lesson->image);
																																																				$slash = '/';
																																																				if (strpos($lesson->image, 'http') !== false) {
																																																					$slash = '';
																																																				}
																																																				echo ' style="background-image: url(' . $slash . $lesson->image . ');" ';
																																																			} else {
																																																				//echo 'style="background:#ea8936;"';
																																																			}
																																																			?>>
																				<?php if (!$lesson->image && false) { ?>
																					<script>
																						getThumbnail('lesson_id_<?php echo $lesson_id; ?>');
																					</script>
																				<?php } ?>
																			</div>
																			<div class="<?php echo $lessonParams->indent_level; ?> lesson-title">
																				<?php
																				if ($course_params->show_numbering || $course_params->show_numbering == '') {
																					echo $lessonCount . " - ";
																				}
																				?>
																				<?php echo $lesson->title; ?>
																			</div>
																			<?php if ($lessonParams->duration != '00:00:00' && $lessonParams->duration) { ?>
																				<div class="lesson-duration">
																					<p class="duration-p">
																						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_DURATION", "Duration") . ": " . $lessonParams->duration; ?>
																					</p>
																				</div>
																			<?php } ?>
																		</a>

																	</li>
																<?php
																} else {
																?>
																	<li class="splms-lesson-unauthorised lessons" style="margin-bottom:0px; font-size: 20px; color: rgb(96, 96, 96);" <?php if ($quizPassed == false) {
																																															echo "title = 'You have not passed the required quiz(zes).'";
																																														} ?>>
																		<a class="lesson_button_a w-inline-block">
																			<div class="lesson-thumb locked-lesson" style="background-image: url('<?php
																																					$slash = '/';
																																					if (strpos($lesson->image, 'http') !== false) {
																																						$slash = '';
																																					}
																																					echo $slash . $lesson->image; ?>');" <?php echo AxsAccessibility::image($lesson->image); ?>>
																				<div class="black-fade">
																					<i class="splms-icon-lock"></i>
																				</div>
																			</div>

																			<div class="lesson-title <?php echo $lessonParams->indent_level; ?>">
																				<?php
																				if ($course_params->show_numbering || $course_params->show_numbering == '') {
																					echo $lessonCount . " - ";
																				}
																				?>
																				<?php echo $lesson->title; ?>

																				<?php
																				//Do not show quiz results if it's not even accessible.
																				if ($accessible && !$passed) {
																					if (!isset($quizNotPassedCount)) {
																						$quizNotPassedCount = 0;
																					} else {
																						$quizNotPassedCount++;
																					}

																					$listTitle = "quizNotPassedList_" . $quizNotPassedCount;
																				?>
																					<div class="<?php echo $lessonParams->indent_level; ?>" style="font-size: 12px;">
																						<span id="<?php echo $listTitle ?>_down" class="lizicon-circle-right"></span>
																						<span id="<?php echo $listTitle ?>_right" class="lizicon-circle-down" style="display:none;"></span>
																						<span style="color: rgb(96, 64, 64);" onclick="toggleQuizList('<?php echo $listTitle ?>')">
																							<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_MUST_PASS_QUIZZES_FOR_ACCESS", "You need to pass the following quizzes to gain access") . ":" ?>
																						</span>

																						<table id="<?php echo $listTitle ?>" style="display:none; margin-top: 15px;">
																							<?php
																							foreach ($quizResults->scores as $quiz) {
																							?>
																								<tr style="margin: 0px 0px 0px 20px; padding: 2px;">
																									<?php
																									$td = "<td style='padding: 0px 8px 0px 10px;'>";
																									$redSpan = "<span style='color: red;'>";
																									$greenSpan = "<span style='color: green;'>";
																									echo $td . $quiz->title . "</td>";
																									if ($quiz->score === null) {
																										//Not taken
																										echo $td . $redSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_NOT_TAKEN", "Not taken") . "</span></td>";
																									} else {
																										if ($quiz->score > $quiz->required) {
																											//Passed
																											echo $td . $greenSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_PASSED", "Passed") . "</span></td>";
																										} else {
																											//Failed
																											echo $td . $redSpan . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_SCORE", "Score") . ": " . round($quiz->score) . "% - " . JText::_('COM_SPLMS_AXS_COURSE_QUIZ_REQUIRED') . ": " . $quiz->required . "%</span></td>";
																										}
																									}
																									?>
																								</tr>
																							<?php
																							}
																							?>
																						</table>
																					</div>
																				<?php
																				}
																				?>
																			</div>

																			<?php if ($lessonParams->duration != '00:00:00') { ?>
																				<div class="lesson-duration">
																					<p class="duration-p">
																						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_DURATION", "Duration") . ": " . $lessonParams->duration; ?>
																					</p>
																				</div>
																			<?php } ?>
																		</a>


																	</li>
															<?php
																} // End if else

																$previous_lesson_id = $lesson_id;
															} // End foreach
															?>
														</ul>
													</div>
												</div> <!-- end course-lessons -->
											<?php
											}	//End foreach
											?>
										</div>


										<?php //Only show if the parameter is set, and it's set to true. 
										?>
										<?php if (isset($course_params->show_students) && $course_params->show_students) { ?>
											<div class="data-view-content clearfix" content="students" style="display:none;">
												<?php

												foreach ($studentList as $student) {
													$cUser = CFactory::getUser($student->user_id);

													$online = $cUser->isOnline();
													if ($online) {
														$title = AxsLanguage::text("AXS_ONLINE", "Online");
													} else {
														$title = AxsLanguage::text("AXS_OFFLINE", "Offline");
													}

												?>

													<div style="margin: 15px; float:left;">
														<div style="text-align: center;">
															<img style="margin: 0px 6px" class="student-picture student-picture-<?php echo $cUser->id; ?> student-img-lg <?php if ($online) {
																																												echo "student-online";
																																											} ?>" src="<?php echo $cUser->getThumbAvatar(); ?>" title="<?php echo $title; ?>" alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>" data-author="<?php echo $cUser->id; ?>">
															<br>
															<span class="lesson-blue text-center" style="font-size: 12px; clear:both;">
																<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>
															</span>
														</div>
														<?php /* <div><?php echo CStringHelper::escape($cUser->getDisplayName()); ?></div> */ ?>

													</div>

												<?php

												}

												?>
											</div>
										<?php } ?>
									<?php
									}
									//$endTime = microtime(true);
									//$runTime = $endTime - $startTime;
									//echo ("<script>alert('" . $filesUpdated . " file times updated.\\nPage load took " . $runTime . " seconds');</script>");
									?>

								</div>
							</div>

							<div class="col-md-3">

								<!-- PROGRESS VIEW -->

								<?php if ((!isset($course_params->show_progress) || $course_params->show_progress) && $minimumStudentsMet) { ?>
									<div class="box progress-box">
										<div class="lms-subtitle">
											<?php
											if (!empty($course_params->progress_area_text) && $course_params->progress_area_text != 'Progress' ) {
												echo $course_params->progress_area_text;
											} else {
												echo JText::_('COM_SPLMS_AXS_COURSE_PROGRESS');
											}
											?>
										</div>
										<div id="progress" style="width:100%; margin-top:15px;"></div>

										<?php if ($course_params->show_progress_lessons || !isset($course_params->show_progress_lessons)) { ?>
											<div class="progress-list">
												<hr />
												<?php
												foreach ($progressList as $item) {
													switch ($item->status) {
														case 'completed':
															$icon = '<span title="' . AxsLanguage::text("COM_SPLMS_AXS_COURSE_PROGRESS_HOVER_COMPLETED", "Completed") . '" class="green lizicon-checkmark"></span>';
															break;
														case 'started':
															$icon = '<span title="' . AxsLanguage::text("COM_SPLMS_AXS_COURSE_PROGRESS_HOVER_STARTED", "Started") . '" class="grey lizicon-clock"></span>';
															break;
														case 'failed':
															$icon = '<span title="' . AxsLanguage::text("COM_SPLMS_AXS_COURSE_PROGRESS_HOVER_FAILED", "Failed Attempt") . '" class="red lizicon-warning"></span>';
															break;
													}
													$progressLesson = $model->getLessons(null, $item->lesson_id);

													if ($progressLesson && $item->course_id == $progressLesson->splms_course_id) {
														echo $icon . ' ' . '<strong>' . $progressLesson->title . '</strong>';
														if ($item->status == "completed" || $item->status == "failed") {
															$item_quiz = $model->getUserQuizzes($item->course_id, $item->lesson_id, $user_id, null);
															if ($item_quiz) {
																$quizObj = AxsLMS::getQuiz($item_quiz->id);
																$itemQuizParams = json_decode($quizObj->params);
																$plural = "";

																if ($item_quiz->attempts > 1) {
																	$attempts_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_ATTEMPTS_PLURAL", "attempts");
																} else {
																	$attempts_text = AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_ATTEMPTS_SINGULAR", "attempt");
																}


												?>

																<br />
																<span class="results">
																	<?php
																	if ($itemQuizParams->mode == 'survey') {
																		echo AxsLanguage::text("COM_SPLMS_SURVEY_COMPLETED", "Survey Completed");
																	} else {
																		echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_SCORE", "Score") . ":" . $item_quiz->highscore . '% ' . $item_quiz->breakdown . ' ' . $item_quiz->attempts . ' ' . $attempts_text;
																	}
																	?>
																</span>

												<?php

															}
														}
														echo '<hr />';
													}
												}
												?>
											</div>

										<?php } ?>
									</div>
								<?php } ?>

								<!-- STUDENT TEAM VIEW -->
								<?php //Only show if the course is open and the show students parameter is set, and it's set to true. 
								?>
								<?php if (!$course_closed && isset($course_params->show_students) && $course_params->show_students && !empty($user_id)) { ?>
									<div class="box">
										<div class="lms-subtitle">
											<?php
											//echo JText::_('COM_SPLMS_AXS_COURSE_TEAMS');
											echo $course_params->student_list_text;
											?>
										</div>
										<div id="student_list">
											<?php

											foreach ($studentList as $student) {
												$cUser = CFactory::getUser($student->user_id);

												$online = $cUser->isOnline();
												if ($online) {
													$title = AxsLanguage::text("AXS_ONLINE", "Online");
												} else {
													$title = AxsLanguage::text("AXS_OFFLINE", "Offline");
												}
											?>
												<div style="position: relative">
													<img class="student-picture student-picture-<?php echo $cUser->id; ?> student-img <?php if ($online) {
																																			echo "student-online";
																																		} ?>" src="<?php echo $cUser->getThumbAvatar(); ?>" title="<?php echo $title; ?>" alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>" data-author="<?php echo $cUser->id; ?>">
													<span class="student-name"><?php echo CStringHelper::escape($cUser->getDisplayName()); ?></span>
												</div>

												<br>

											<?php
											}
											?>
										</div>
										<div class="clearfix"></div>
									</div>
								<?php } ?>

								<!-- TEACHER VIEW -->

								<?php
								$teacherList = SplmsModelCourses::getCourseTeachers($this->item->splms_course_id, null);
								if ((!isset($course_params->show_teachers) || $course_params->show_teachers) && $teacherList) {
								?>
									<div class="box">
										<div class="lms-subtitle">
											<?php
											//echo JText::_('COM_SPLMS_AXS_COURSE_TEACHERS');
											echo $course_params->teacher_list_text;
											?>
										</div>
										<?php

										foreach ($teacherList as $teacherItem) {

											$params = json_decode($teacherItem->params);

											if (isset($params->user)) {
												$teacherProfile = AxsExtra::getUserProfileData($params->user);
											} else {
												$teacherProfile = '';
											}

											if ($params->image_type == 'social' && $params->user) {
												$photo = $teacherProfile->photo;
											} else {
												$photo = $teacherItem->image;
											}

											if ($params->profile_type == 'social' && $params->user) {
												//$teacherUrl = '/community/'.$teacherProfile->alias;
												$teacherUrl = JRoute::_('index.php?option=com_community') . $teacherProfile->alias;
											} else {
												$teacherUrl = '';
											}

											$cUser = CFactory::getUser($params->user);
											//$online = $cUser->isOnline();
											if (!$photo) {
												$photo = '/images/user.png';
											}
										?>
											<?php if ($params->image_type == 'social' && $params->user) { ?>
												<img class="teacher-img <?php if ($online) {
																			echo "student-online";
																		} ?>" src="<?php echo $cUser->getThumbAvatar(); ?>" title="<?php echo CTooltip::cAvatarTooltip($cUser); ?>" alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>" data-author="<?php echo $cUser->id; ?>">
											<?php } else { ?>
												<?php if ($photo) { ?>
													<img class="teacher-img" src="<?php echo $photo; ?>">
												<?php } ?>
											<?php } ?>
											<div class="text-center" style="width:100%; clear:both;">
												<?php if ($teacherUrl) { ?>
													<a href="<?php echo $teacherUrl; ?>">
													<?php } ?>
													<?php echo $teacherItem->title; ?>
													<?php if ($teacherUrl) { ?>
													</a>
												<?php } ?>
												<br />
												<span class="sub-heading">
													<?php echo $teacherItem->sub_heading; ?>
												</span>
											</div>
										<?php
										}
										?>
										<div class="clearfix"></div>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="components/shared/includes/js/easy-pie-chart.js"></script>
	<script>
		var text_unable_to_remove_item = "<?php echo JText::_('COM_SPLMS_AXS_COURSE_UNABLE_TO_REMOVE_ITEM'); ?>";

		$(document).ready(function() {
			<?php if ($course_params->show_progress || !isset($course_params->show_progress)) { ?>

				function setWidth() {
					$("#progress").html('');
					var newPieChart = `<div class="percentage" data-percent="<?php echo $percentage; ?>">
		                                <span class="value">
		                                <?php echo round($percentage); ?>
		                                </span>
		                                <span class="percent-sign">%</span>
			                       </div>`;

					$("#progress").html(newPieChart);

					var width = $('#progress').width();
					var lineWidth = width / 8;
					var fontSize = width / 8;
					var barColor = '<?php echo $progressColor; ?>';
					var trackColor = '#ccc';
					var scaleColor = '#aaa';
					var percent = Math.round($('.percentage').data('percent'));

					if (percent >= 100) {
						percent = 100;

						barColor = '<?php echo $progressColor; ?>';
						scaleColor = '<?php echo $progressColor; ?>';
					} else if (percent == 0) {
						var fontColor = scaleColor;
					} else {
						var fontColor = barColor;
					}

					$('.percentage').css('fontSize', fontSize + 'px', 'color', fontColor);
					$('.percentage').css('color', fontColor);
					$('.percentage').easyPieChart({
						animate: 1000,
						lineWidth: lineWidth,
						barColor: barColor,
						trackColor: trackColor,
						lineCap: 'butt',
						scaleColor: scaleColor,
						size: width,
						onStep: function(value) {
							this.$el.find('.value').text(Math.round(value));
						},
						onStop: function(value, to) {
							this.$el.find('.value').text(Math.round(to));
						}
					});

					if (percent == 100) {
						fontSize = width / 2.75;
						//$('.percentage').css('fontSize',fontSize+'px','color',fontColor);
						//$('.value').hide();
						//$('.percent-sign').hide();
						//var award = '<?php echo $award; ?>';
						//$('.percentage').prepend(award);
					}
				}

				setWidth();

				$(window).resize(function() {
					setWidth();
				});
			<?php } ?>

			function search() {
				var keyword = $('#search').val();
				if (keyword.length > 1 || keyword.length == 0) {
					$('.lessons').addClass('hide');
					$('.lessons').each(function() {
						var title = $(this).find('.lesson-title').text();
						if (title.toLowerCase().search(keyword.toLowerCase()) > -1) {
							$(this).removeClass('hide');
						}

					});
				}
			}



			$('#search').keyup(function() {
				search();
			});

			$('.search').click(function() {
				search();
			});

			$(".addtocart").click(
				function() {
					var which = this.getAttribute("product-id");

					$.ajax({
						url: '/index.php?option=com_splms&task=cart.addItem&format=raw',
						data: {
							product_id: which
						},
						type: 'post'
					}).done(function(result) {
						//console.log(result);
						window.location = result;
					}).fail(function() {
						alert(text_unable_to_remove_item);
					});
				}
			);


		});
	</script>
<?php } ?>