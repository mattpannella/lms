<?php

/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsViewCourse extends FOFViewHtml{

	public function display($tpl = null){

		//Load the method jquery script.
		/*JHtml::_('jquery.framework');*/

		// Get model
		$model = $this->getModel();
		// get item
		$this->item = $model->getItem();

		//Load Lessons Model
		$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');
		$quizquestion_model = FOFModel::getTmpInstance('QuizQuestions', 'SplmsModel');
		$accuratethought_model = FOFModel::getTmpInstance('AccurateThoughts', 'SplmsModel');

		//Joomla Component Helper & Get LMS Params
		jimport('joomla.application.component.helper');
		$this->params = JComponentHelper::getParams('com_splms');

		// Add Script
		$doc = JFactory::getDocument();
		$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/splms.js' );

		$this->user = JFactory::getUser();

		//Set getLessons to true to divide lessons by section
		$this->lessons      	= $lessons_model->getLessons($this->item->splms_course_id, true);
		$this->freeLesson   	= $lessons_model->getCourseFreeLesson($this->item->splms_course_id);
		$this->isAuthorised 	= $model->getIsbuycourse($this->user->id, $this->item->splms_course_id);

		$this->quizquestions 	= $quizquestion_model->getCourseQuizzes($this->item->splms_course_id);
		$this->quizresults		= $quizquestion_model->getQuizResult($this->user->id);
		$this->accuratethoughts	= $accuratethought_model->getAccurateThoughts($this->user->id, $this->item->splms_course_id);

		//Get course teacher
		$this->teachers 		= $model->getCourseTeachers( $this->item->splms_course_id );

		// Get Currency
		//$params = JComponentHelper::getParams('com_splms');
		$this->currency = explode(':', $this->params->get('currency', 'USD:$'));

		$this->coursePrice='';
		if ($this->item->price == 0) {
			$this->coursePrice = AxsLanguage::text("COM_SPLMS_FREE", "Free");
		}else{
			$this->coursePrice = $this->currency[1] . $this->item->price;
		}



		return parent::display($tpl = null);
	}
}