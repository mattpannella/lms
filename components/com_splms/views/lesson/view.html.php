<?php

/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsViewLesson extends FOFViewHtml{

	public function display($tpl = null){

		// Get model
		$model = $this->getModel();
		// get item
		$this->item = $model->getItem();

		// Load lesson Model
		$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');
		$teachers_model = FOFModel::getTmpInstance('Teachers', 'SplmsModel');
		//$quizquestion_model = FOFModel::getTmpInstance('QuizQuestions', 'SplmsModel');
		$accuratethought_model = FOFModel::getTmpInstance('AccurateThoughts', 'SplmsModel');

		//Joomla Component Helper & Get LMS Params
		jimport('joomla.application.component.helper');
		$params = JComponentHelper::getParams('com_splms');

		// Get User
		$this->user = JFactory::getUser();

		$this->teacher = $teachers_model->getTeacher($this->item->splms_teacher_id);
		$this->course = $courses_model->getCourse($this->item->splms_course_id);
		$courses_model->setOverrides($this->course);
		$this->isAuthorised = $teachers_model->getIsbuycourse($this->user->id, $this->item->splms_course_id);
		//$this->quizquestions 	= $quizquestion_model->getCourseQuizzes($this->item->splms_course_id);
		//$this->quizresults		= $quizquestion_model->getQuizResult($this->user->id);
		$this->accuratethoughts	= $accuratethought_model->getAccurateThoughts($this->user->id, $this->item->splms_course_id);

		

		$this->teacher_description = strip_tags($this->teacher->description);
		if (strlen($this->teacher_description) > 400) {
		    // truncate string
		    $descriptionCut = substr($this->teacher_description, 0, 340);
		    // make sure it ends in a word so assassinate doesn't become ass...
		    $this->teacher_description = substr($descriptionCut, 0, strrpos($descriptionCut, ' ')).'...';
		    // Show Desription
		    $this->teacher_description = $this->teacher_description;
		}else{
			$this->teacher_description = $teacher->description;
		}




		return parent::display($tpl = null);
	}
}