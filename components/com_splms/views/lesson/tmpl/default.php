<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;
$document = JFactory::getDocument();
$document->setTitle($this->item->title);

$scormAccess = AxsDbAccess::getAccessLevels('scorm');
$contentLibraryAccess = AxsDbAccess::getAccessLevels('bizlibrary');
$globalContentLibraryType = AxsDbAccess::getAccessLevels('bizlibrary_content_selection');

$user_id  = JFactory::getUser()->id;
$language = AxsLanguage::getCurrentLanguage()->get('tag');
$settings = json_decode(AxsLMS::getSettings()->params);
$course_params = json_decode($this->course->params);
$userGroups = JAccess::getGroupsByUser($user_id);

$assigned = AxsLMS::getUserAssignedData($user_id, $this->item->splms_course_id, null);
if ($assigned->free) {
	$this->course->price = 0;
}


$model = $this->getModel();
$otherLessons = $model->getNextAndPrevious($this->item->splms_lesson_id);

// loads next and previous lessons
$next = $otherLessons->next;
$previous = $otherLessons->previous;
$previous_not_required = true;
$lessonGating = 0;

$hasNextLesson = !is_null($next);

if (!isset($course_params->lesson_gating)) {
	$course_params->lesson_gating = 2;
}
if ($course_params->lesson_gating == 1  || ($settings->lesson_gating == 1 && $course_params->lesson_gating == 2)) {
	$lessonGating = 1;
	if ($previous) {
		$previousParams = new stdClass();
		$previousParams->course_id 	    = $this->item->splms_course_id;
		$previousParams->lesson_id 	    = $previous->splms_lesson_id;
		$previousParams->user_id 		= $user_id;
		$previousParams->language 	    = $language;
		$previousLessonCompleted = AxsLMS::checkLessonCompletion($previousParams);
		if (!$previousLessonCompleted) {
			$previous_not_required = false;
		}
	}
}

$isAdmin = false;
$unlockAdmin = false;
$published  = $this->item->enabled;
if (in_array(20, $userGroups) || in_array(32, $userGroups) || in_array(8, $userGroups)) {
	$isAdmin = true;
	$unlockAdmin = JRequest::getVar('unlock');
	if ($unlockAdmin == 1) {
		$unlockAdmin = true;
		$previous_not_required = true;
		$published = true;
	} else {
		$unlockAdmin = false;
	}
}

if (($unlockAdmin || $this->item->lesson_type == 0 || $this->isAuthorised != '' || $this->course->price == 0) && $previous_not_required && $published) {

	require_once('components/shared/controllers/comments.php');
	require_once('components/shared/controllers/rating.php');

	$doc = JFactory::getDocument();
	$doc->addStyleSheet('components/com_splms/assets/css/course.css?v=9');
	$doc->addStyleSheet('components/com_splms/assets/css/lms.css?v=1');





	$width = 'container-fluid';
	$display = $course_params->display;
	if ($display) {
		if ($display == 'fullwidth') {
			$width = 'container-fluid';
		}
	}

	if ($course_params->progress_color) {
		$progressColor = $course_params->progress_color;
	} else {
		$progressColor = '#0a5786';
	}

	$auto_discussion = false;

	if ($course_params->discussion_auto) {
		$auto_discussion = true;
	}


	//$accessLevelName =  AxsExtra::accessLevelName($this->course->access);
	$hasBoughtCourse = AxsPayment::hasUserPurchasedCourse($this->item->splms_course_id, $this->user->id);
	$subs = AxsPayment::getUserSubscriptions($user_id);

	$course_params->access = $this->course->access;
	$visibilityAccess = AxsContentAccess::checkVisibilityAccess($user_id, $this->item->splms_course_id);
	$registerAccess = AxsContentAccess::checkContentAccess($user_id, $course_params);

	$categoryId = $this->course->splms_coursescategory_id;
	$categoryVisibilityAccess = AxsContentAccess::checkCategoryAccess($user_id, $categoryId);

	//create lesson params to pass into activity ajax call
	$activityParams = new stdClass();
	$activityParams->course_id 	= $this->item->splms_course_id;
	$activityParams->lesson_id 	= $this->item->splms_lesson_id;
	$brand = AxsBrands::getBrand();
	$key = AxsKeys::getKey('lms');
	$encryptedActivityParams = base64_encode(AxsEncryption::encrypt($activityParams, $key));

	$isActiveMember = true;

	if (!$brand->site_details->plans_for_active) {
		$isActiveMember = true;
	} else {
		foreach ($subs as $s) {
			$brandPlans = $brand->site_details->plans_for_active;
			if (($s->status == "ACT" || $s->status == "GRC") && in_array($s->plan_id, $brandPlans)) {
				$isActiveMember = true;
				break;
			}
		}
	}



	$lesson_params = json_decode($this->item->params);
	$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');
	$quiz_model = FOFModel::getTmpInstance('QuizQuestions', 'SplmsModel');
	$progress = $courses_model->getProgress($this->item->splms_course_id, $this->item->splms_lesson_id, $user_id, null)[0];

	$lessonParams = new stdClass();
	$lessonParams->course_id 	= $this->item->splms_course_id;
	$lessonParams->lesson_id 	= $this->item->splms_lesson_id;
	$lessonParams->user_id 		= $user_id;
	$lessonParams->status 		= 'started';
	$lessonParams->language 	= $language;

	if (!$progress) {
		AxsLMS::insertLessonStatus($lessonParams);
		$progress = $courses_model->getProgress($this->item->splms_course_id, $this->item->splms_lesson_id, $user_id, null)[0];
	}

	if ($user_id) {
		AxsLMS::courseUpdateLastActivity($user_id, $this->item->splms_course_id, $this->item->splms_lesson_id, $language);
	}

	//Check to see if the minimum student requirement has been met for the course
	$minMet = $courses_model->minimumStudentsMet($this->item->splms_course_id);
	if ($minMet !== true) {
		header("Location: $minMet");
	}


	$lesson_quiz = $quiz_model->getQuizByLesson($this->item->splms_course_id, $this->item->splms_lesson_id);


	if ($previous_not_required && $isActiveMember && ($hasBoughtCourse || $this->course->price == 0) && $visibilityAccess && $categoryVisibilityAccess && ($registerAccess || $hasBoughtCourse)) {
		$unlocked = true;
	} else {
		$unlocked = false;
	}

	//Get attached files
	$files = json_decode($lesson_params->files);
	$filesCount = is_countable($files->title) ? count($files->title) : 0;


	//Get link
	$links = json_decode($lesson_params->links);
	$linksCount = is_countable($links->title) ? count($links->title) : 0;

	//Get student activities
	$studentActivities = json_decode($lesson_params->student_activities);

	if ($unlockAdmin || $next) {
		if (($next->lesson_type == 0 || $unlocked) && $previous_not_required) {
			$next_unlocked = true;
		}
		$next_quizRequirements = json_decode($next->required_quizzes);
		$next_quizPassed = true;
		$next_quizResults = null;
		if ($next_quizRequirements != null) {
			$next_quizResults = $courses_model->checkQuizPassed($user_id, $next_quizRequirements);
			$next_quizPassed = $next_quizResults->passed;
		}
	}

	if ($unlockAdmin || $previous) {
		if ($previous->lesson_type == 0 || $unlocked) {
			$previous_unlocked = true;
		}
		$previous_quizRequirements = $previous->required_quizzes;
		$previous_quizPassed = true;
		$previous_quizResults = null;
		if ($previous_quizRequirements != null) {
			$previous_quizResults = $courses_model->checkQuizPassed($user_id, $previous_quizRequirements);
			$previous_quizPassed = $previous_quizResults->passed;
		}
	}


	$quizRequirements = json_decode($this->item->required_quizzes);

	//Check to see if the quiz requirements for the lesson have been taken.
	$quizPassed = true;
	$quizResults = null;
	if ($quizRequirements != null) {
		$quizResults = $courses_model->checkQuizPassed($user_id, $quizRequirements);
		$quizPassed = $quizResults->passed;
	}

	$c_id = $this->item->splms_course_id;
	$prog = AxsLMS::getCourseProgress($c_id, $user_id, $language);
	$percentage = round($prog->progress);
	$slug = $this->course->slug;

	$link = JRoute::_('index.php?option=com_splms&view=course') . $slug;

	//Check to see if the course itself can still be taken.

	$today = date("Y-m-d", strtotime('now'));
	$due = date("Y-m-d", strtotime($prog->date_due));
	if ($course_params->use_due_date) {
		//The course has a due date.
		if ($due < $today) {
			//The course is past due.  See if they're still allowed to take it.
			if (!$course_params->due_date_past_completion) {
				//The course cannot be taken after the due date.  They have followed an old link, or typed it in by hand.
				//Return them to the course overview.
				header("Location: $link");
			}
		}
	}

	$course_open = $course_params->course_open_date;
	$course_close = $course_params->course_close_date;

	if ($course_open && $course_open > $today) {
		//The course has an open date and it is not open yet.
		header("Location: $link");
	}

	if ($course_close && $course_close < $today) {
		//The course has a close date and it's already closed
		header("Location: $link");
	}

	if ($unlockAdmin || $quizPassed && ($unlocked || $this->item->lesson_type == 0)) {
?>
		<script>
			var language = '<?php echo $language; ?>';
		</script>
		<script type="text/javascript" src="/components/com_splms/assets/js/course.js?v=3"></script>
		<style>
			body {
				background: #eee;
			}

			.no-padding {
				padding: 0px;
			}

			.cover-image {
				background-repeat: no-repeat;
				background-position: center;
				background-size: cover;
				width: 100%;
				height: 300px;
			}

			@media(max-width: 768px) {
				.cover-image {
					height: 200px;
				}
			}

			.lms-subtitle {
				font-size: 22px;
				margin-top: -15px;
				margin-left: -15px;
				color: #777;
				font-weight: 600;
				padding-bottom: 10px;
			}

			.lms-subtitle .fa {
				font-size: 18px;
				margin-right: 10px;
			}

			.lesson-links a {
				font-size: 13px;
			}

			.activity-instructions {
				font-size: 13px;
			}

			.lms-activity {
				padding-bottom: 20px;
			}

			.lms-activity .file {
				visibility: hidden;
				position: absolute;
			}

			.activity-submission .fa {
				font-size: 18px;
				color: #03941f;
			}

			.activity-submission {
				font-size: 18px;
				color: #777;
			}

			.activities-title {
				font-size: 25px;
				color: #555;
				text-align: center;
			}

			.lms-activity input {
				height: 32px;
			}

			@include media-breakpoint-up(sm) {
				.progress {
					width: 100%;
					margin: 0px;
					height: 40px;
					rgba(221, 246, 212, 1);
					box-shadow: var(--box-shadow-inner);
					border: 1px solid rgba(0, 0, 0, .075);
				}
			}


			.progress.active .progress-bar {}

			.progress-bar {
				margin-bottom: 0;
				vertical-align: middle;
				position: relative;
				height: auto;
				background-color: <?php echo $progressColor; ?>;
				box-shadow: var(--box-shadow-inner);
				min-width: 32px;
			}

			.progress,
			.progress-bar {
				border-radius: 100px;
				display: flex;
				align-items: center;
			}

			.course-bar-percentage {
				font-size: 14px;
				font-weight: 400;
				line-height: auto;
				background: rgb(26, 205, 0);
				background: linear-gradient(274deg, var(--bs-success) 0%, var(--bs-dark-success) 100%);
				padding: 7px 0px;
				color: #FFFFFF;
				text-align: center;
				width: 48px;
				height: 32px;
				border-radius: 100px;
				border: 0px solid rgba(221, 246, 212, 1);
				outline: 1px solid white;
				box-shadow: var(--box-shadow-5);
			}

			.course-progress-bar {
				border-radius: 100px;
			}

			.tovuti-nav-button {
				background: #ea8936;
				width: 45px;
				height: 40px;
				border-radius: 50%;
				text-align: center;
				line-height: auto;
				font-weight: bold;
				color: #fff;
				transition: ease all .3s;
				border: 0px solid #ea8936;
				display: flex;
				justify-content: center;
				align-items: center;
				padding: $spacer;

			}

			.tovuti-nav-button:hover {
				background: #eee;
				color: #ea8936;
				cursor: pointer;
			}

			.tovuti-nav-button:focus {
				outline: 2px solid var(--accent-color) !important;
			}

			button.activity-resubmit {
				background-color: #FEFEFE;
				color: #434754;
				border-style: solid;
				border-radius: 5px;
				border-color: #EEEFF2;
				padding: 8px;
				font-size: 16px;
				widtH: 40%;
				min-width: 125px;
				text-align: left;
			}
			button.activity-resubmit i {
				color: #495EF6; /* or this? 0b7ae6 */
				margin: 0 5px;
			}
		</style>
		<script src="https://kit.fontawesome.com/8c1112f2d0.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="templates/axs/bootstrap-utilities/bootstrap-utilities.min.css">
		<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
		<div class="row course-progress-bar">
			<div class="col-lg-12 mt-3  no-padding d-inline-flex position-relative gap-2">
				<?php if ($course_params->show_progress || !isset($course_params->show_progress)) { ?>
					<!--<div class="lms-subtitle"><?php echo JText::_('COM_SPLMS_AXS_COURSE_PROGRESS') ?></div>-->
					<a href=<?php echo $link ?> class="bg-white text-secondary shadow-sm flex-shrink h-100 rounded-pill py-2 px-3 d-flex align-items-center gap-2"> <i class="fa-regular fa-arrow-rotate-left"></i> <?php echo AxsLanguage::text("AXS_COURSE", "Return to Course") ?></a>
					<div class="progress px-1 flex-fill">
						<div class="progress-bar progress-bar-striped active" style="transition: all ease;">
							<div class="course-bar-percentage w-100 ms-auto bg-none"></div>
						</div>
					</div>

				<?php } ?>
			</div>

		</div>

		<div class="row">
			<div class="<?php echo $width; ?>">
				<div id="splms" class="splms view-splms-course course-details">
					<div class="splms-course">
						<?php
						if ($lesson_params->cover_image) {
						?>
							<div class="col-lg-12 box cover-image" style="background-image: url('<?php echo $lesson_params->cover_image; ?>');" <?php echo AxsAccessibility::image($lesson_params->cover_image); ?>>

								<div style="width:100%; padding: 10px; bottom: 0px; left: 0px;  position:absolute;  background-color: rgba(0, 1, 1, 0.7);">
									<div class="course-title pull-left" style="color:#fff; font-size:26px;"><?php echo $this->item->title; ?></div>
									<?php if ($settings->show_ratings || !isset($settings->show_ratings)) { ?>
										<div class="pull-right lms" style="margin-top:10px;">
											<?php
											$rating_id = "lesson_" . $this->item->splms_lesson_id;
											$rating_text = array(
												AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_1", "It was okay"),
												AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_2", "Made me think"),
												AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_3", "Inspired me"),
												AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_4", "Changed my day"),
												AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_5", "Changed my life")
											);
											echo display_ratings($rating_id, $rating_text);
											?>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } else { ?>
							<div style="width:100%; margin-top:20px; padding: 10px; position:relative;  background-color: rgba(0, 1, 1, 0.7);">
								<div class="course-title pull-left" style="color:#fff; font-size:26px;"><?php echo $this->item->title; ?>
								</div>
								<?php if ($settings->show_ratings || !isset($settings->show_ratings)) { ?>
									<div class="pull-right lms" style="margin-top:10px;">
										<?php
										$rating_id = "lesson_" . $this->item->splms_lesson_id;
										$rating_text = array(
											AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_1", "It was okay"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_2", "Made me think"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_3", "Inspired me"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_4", "Changed my day"),
											AxsLanguage::text("COM_SPLMS_AXS_RATING_LESSON_5", "Changed my life")
										);
										echo display_ratings($rating_id, $rating_text);
										?>
									</div>
								<?php } ?>
								<div class="clearfix"></div>
							</div>
						<?php } ?>

						<!-- start course-header -->

						<div class="row">
							<div class="col-lg-12 clearfix">

								<?php
								if ($this->item->available) {
								?>
									<div class="pull-right">

										<?php
										if (($buyButton) && ($this->item->price != 0)) {
										?>
											<button style="margin-bottom:10px;" class="btn btn-primary" onclick="window.location='/index.php?option=com_splms&view=cart'">
												<span class="lizicon-cart"></span>
												&nbsp;<?php echo JText::_('COM_SPLMS_AXS_SHOPPING_CART'); ?>
											</button>
											<button class="addtocart btn btn-primary" style="margin-top:20px;" product-id="<?php echo $this->item->splms_course_id ?>">
												<?php echo $buttonText; ?>
											</button>
										<?php
										} elseif (!$unlocked) {
										?>
											<span class="access"><?php echo AxsLanguage::text("COM_SPLMS_DONT_HAVE_ACCESS_TO_LESSON", "You don't have access to this Lesson") ?></span>
										<?php
										}
										?>

									</div>
								<?php
								}
								?>
							</div>

							<div class="col-lg-9">
								<?php
								$videoParams = $this->item;
								$lesson_video = AxsLMS::getVideo($videoParams);
								if ($lesson_video) {
								?>
									<div class="box no-padding video-content">
										<?php echo $lesson_video; ?>
									</div>
								<?php
								}
								?>

								<?php if ($this->item->description) { ?>
									<div class="box clearfix">
										<?php echo $this->item->description; ?>
									</div>
								<?php } ?>

								<?php if ($lesson_params->use_library_content && ($lesson_params->scorm_id || $lesson_params->bizlibrary_content_id) && $user_id && ($scormAccess || $contentLibraryAccess)) { ?>
									<div class="clearfix"></div>
									<div class="row">
										<div class="col-lg-12">
											<div class="box">
												<?php
												$scormParams = new stdClass();
												$scormParams->lesson_id = $this->item->splms_lesson_id;
												$scormParams->course_id = $this->item->splms_course_id;
												$scormParams->user_id = $user_id;
												$scormParams->language = AxsLanguage::getCurrentLanguage()->get('tag');

												if ($lesson_params->library_content_type == 'scorm' && $lesson_params->scorm_id && $scormAccess) {
													$scormParams->scorm_course_id = $lesson_params->scorm_id;
													$scormParams->scorm_type = 'scormlibrary';
													$scormContent = AxsScormFactory::loadContent($scormParams);
													echo $scormContent;
												}

												if ($lesson_params->library_content_type == 'bizlibrary' && $lesson_params->bizlibrary_content_id && $contentLibraryAccess) {

													// Make sure we're actually cleared to display the BizLibrary SCORM content as a secondary layer of permissioning
													$validBizlibraryAccess = true;

													if ($globalContentLibraryType == 'production' && AxsScormFactory::getBizLibraryCourseType($lesson_params->bizlibrary_content_id) != 'production') {
														$validBizlibraryAccess = false;
													}
													if ($validBizlibraryAccess) {
														$scormParams->scorm_course_id = $lesson_params->bizlibrary_content_id;
														$scormParams->scorm_type = 'bizlibrary';
														$scormContent = AxsScormFactory::loadContent($scormParams);
														echo $scormContent;
													}
												}
												?>
											</div>
										</div>
									</div>
								<?php } ?>

								<?php

								if ($studentActivities) { /*&& $user_id*/
									if ($lesson_params->activity_section_label) {
										echo '<div class="activities-title">' . $lesson_params->activity_section_label . '</div>';
									}

									foreach ($studentActivities as $activity) {
										echo $model->getActivity($activity, $this->item);
									}
								?>
									<script>
										var lesson = '<?php echo $encryptedActivityParams; ?>';
									</script>
									<script src="/components/com_splms/assets/js/studentactivities.js?v=15"></script>
									<div class="clearfix"></div>
								<?php
								}
								?>

								<?php
								$lessonMaterials = json_decode($this->item->teacher_materials);
								$lesson_id = $this->item->splms_lesson_id;
								$course_id = $this->item->splms_course_id;
								$count_lessonMaterials = is_countable($lessonMaterials->splms_teacher_id) ? count($lessonMaterials->splms_teacher_id) : 0;

								if ($count_lessonMaterials) {
									echo '<div class="activities-title">' . AxsLanguage::text("COM_LMS_TEACHER_MATERIALS", "Additional Teacher Materials") . '</div>';
								?>
									<div class="box">
										<?php
										for ($whichLesson = 0; $whichLesson < $count_lessonMaterials; $whichLesson++) {

											$video_duration = gmdate("H:i:s", (float)$lessonMaterials->video_duration[$whichLesson]);
											$audio_duration = gmdate("H:i:s", (float)$lessonMaterials->audio_duration[$whichLesson]);

											$teacher_id = $lessonMaterials->splms_teacher_id[$whichLesson];

											$audio_file = $lessonMaterials->audio_file[$whichLesson];
											$video_file = $lessonMaterials->video_file[$whichLesson];
											$pdf_file = $lessonMaterials->pdf_file[$whichLesson];
											$video_thumb = $lessonMaterials->video_thumb[$whichLesson];

											$teacher = SplmsModelTeachers::getTeacher($teacher_id);

											$params_materials = '';
											$teacherProfile_materials = '';
											$photo_materials = '';
											$teacherUrl_materials = '';
											$params_materials = json_decode($teacher->params);

											if ($params_materials->user) {
												$teacherProfile_materials = AxsExtra::getUserProfileData($params_materials->user);
											}

											if ($params_materials->image_type == 'social' && $params_materials->user) {
												$photo_materials = $teacherProfile_materials->photo;
											} else {
												$photo_materials = $teacher->image;
											}

											if ($params_materials->profile_type == 'social' && $params_materials->user) {
												//$teacherUrl_materials = '/community/'.$teacherProfile_materials->alias;
												$teacherUrl_materials = JRoute::_('index.php?option=com_community') . $teacherProfile_materials->alias;
											} else {
												$teacherUrl_materials = '';
											}

										?>

											<div class="comment-img">
												<?php if ($teacherUrl_materials) { ?>
													<a href="<?php echo $teacherUrl_materials; ?>">
													<?php } ?>
													<img class="bw" src="<?php echo $photo_materials; ?>">
													<?php if ($teacherUrl_materials) { ?>
													</a>
												<?php } ?>
											</div>

											<div class="teacher-name lesson-blue">
												<?php if ($teacherUrl_materials) { ?>
													<a href="<?php echo $teacherUrl_materials; ?>">
													<?php } ?>
													<?php echo $teacher->title; ?>
													<?php if ($teacherUrl_materials) { ?>
													</a>
												<?php } ?>

												<br />
												<span class="sub-heading">
													<?php echo $teacher->sub_heading; ?>
												</span>
												<br />
												<span>
													<?php
													$rating_id = "course_" . $course_id . "_lesson_" . $lesson_id . "_teacher_" . $teacher_id;
													$rating_text = array(
														AxsLanguage::text("COM_SPLMS_AXS_RATING_TEACHER_1", "Needs improvement"),
														AxsLanguage::text("COM_SPLMS_AXS_RATING_TEACHER_2", "Could be better"),
														AxsLanguage::text("COM_SPLMS_AXS_RATING_TEACHER_3", "Okay"),
														AxsLanguage::text("COM_SPLMS_AXS_RATING_TEACHER_4", "Great"),
														AxsLanguage::text("COM_SPLMS_AXS_RATING_TEACHER_5", "Exceptional")
													);
													echo display_ratings($rating_id, $rating_text);
													?>

												</span>
											</div>

											<div class="lesson-icons lesson-blue">

												<?php
												if ($video_file) {
												?>
													<div class="pull-left center pad-right">
														<span class="lizicon-film icon-font ginAudioPopup" title="<?php echo $lesson->title; ?>" alias="<?php echo $video_file; ?>" header="<?php echo $lesson->title; ?>">
														</span>
														<br />
														<span class="comment-date"><?php echo ($video_duration) ?></span>
													</div>
												<?php
												}

												if ($audio_file) {
												?>
													<div class="pull-left center pad-right">
														<span class="lizicon-headphones icon-font ginAudioPopup" title="<?php echo $lesson->title; ?>" alias="<?php echo $audio_file; ?>" header="<?php echo $lesson->title; ?>">
														</span>
														<br />
														<span class="comment-date"><?php echo ($audio_duration) ?></span>
													</div>

												<?php
												}

												if ($pdf_file) {
												?>
													<a href="<?php echo $pdf_file ?>" target="_blank">
														<div class="pull-left center pad-right">
															<span class="lizicon-file-text2 icon-font"></span>
															<br />
															<span class="comment-date"><?php echo AxsLanguage::text("AXS_PDF", "PDF") ?></span>
														</div>
													</a>
												<?php
												}
												?>

												<div class="pull-left center pad-right">
													<span class="lizicon-bubble2 icon-font" onclick="toggle_media(<?php echo $lesson_id . ',' . $teacher_id ?>, 'comment')">
													</span>
													<br>
													<span class="comment-date"><?php echo JText::_('COM_SPLMS_AXS_LESSON_COMMENT'); ?></span>
												</div>
											</div>

											<div class="clearfix" style="margin-bottom:25px;"></div>

											<div id="lesson_comments_<?php echo $lesson_id . '_' . $teacher_id ?>" style="display:none; width: 100%;">
												<?php
												$comment_id = "course_" . $course_id . "_lesson_" . $lesson_id . "_teacher_" . $teacher_id;

												$params = new stdClass();

												$params->title = JText::_('COM_SPLMS_AXS_LESSON_COMMENTS_HOVER');
												$params->button_title = JText::_('COM_SPLMS_AXS_LESSON_COMMENTS_BUTTON');
												$params->placeholder = JText::_('COM_SPLMS_AXS_LESSON_COMMENTS_PLACEHOLDER');
												$params->post_id = $comment_id;

												display_comments($params);
												?>
											</div>

										<?php
										} //end teacher resources for loop
										?>
									</div>
								<?php
								}
								?>
								<?php if ($course_params->discussion_size == 'large' && ($settings->show_discussions || !isset($settings->show_discussions))) { ?>
									<div class="box undocked large-commentbox">
										<div class="lms-subtitle"><span class="fa fa-comments"></span> <?php echo JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION'); ?></div>
										<div class="discussion">
											<div class="clearfix"></div>
											<?php
											$comment_id = "lesson_" . $lesson_id;

											$params = new stdClass();

											$params->title = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_HOVER');
											$params->button_title = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_BUTTON');
											$params->placeholder = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_PLACEHOLDER');
											$params->auto_update = $auto_discussion;
											$params->post_id = $comment_id;

											display_comments($params);
											?>
											<div class="clearfix"></div>
										</div>
									</div>
								<?php } ?>
							</div>
							<aside class="col-lg-3" id="lesson-side">
								<div class="box">
									<div class="lms-subtitle"><span class="fa fa-share-square"></span> <?php echo JText::_('COM_SPLMS_AXS_COURSE_UP_NEXT'); ?></div>
									<?php
									if ($next->image) {
									?>
										<img src="<?php echo $next->image; ?>" class="img-responsive" <?php echo AxsAccessibility::image($next->image); ?> /></br>
										<center><b><span style="font-size:12px;"><?php echo $next->title; ?></span></b></center>
									<?php
									} else {
									?>
										<center><b><?php echo $next->title; ?></b></center>
									<?php
									}
									?>

									<div class="clearfix" style="margin-top: 15px;"></div>

									<?php
									if ($next_quizPassed && $next_unlocked) {
										$nextUrl = JRoute::_('index.php?view=lesson') . '?id=' . $next->splms_lesson_id;
										if ($progress->status != 'completed') {
											$checkCompleteClass = 'checkComplete';
										}
									?>

										<a role="button" aria-label="Next Lesson" href="<?php echo $nextUrl; ?>" class="pull-right tovuti-nav-button <?php echo $checkCompleteClass; ?>" data-lesson="<?php echo $this->item->splms_lesson_id; ?>">
											<?php //echo JText::_('COM_SPLMS_AXS_LESSON_NEXT') . " ";
											?>
											<i class="fa-solid fa-arrow-right-from-line"></i></a>
									<?php
									} else {
									?>
										<span class="pull-right tovuti-nav-button disabled">
											<?php //echo JText::_('COM_SPLMS_AXS_LESSON_NEXT') . " ";
											?><i class="fa-solid fa-lock-keyhole"></i></span>
									<?php
									}
									?>

									<?php
									if ($previous_quizPassed && $previous_unlocked) {
										$prevUrl = JRoute::_('index.php?view=lesson') . '?id=' . $previous->splms_lesson_id;
									?>
										<a role="button" aria-label="Previous Lesson" href="<?php echo $prevUrl; ?>" class="pull-left tovuti-nav-button ">
											<i class="fa-solid fa-arrow-left-from-line"></i>
											<?php //echo " " . JText::_('COM_SPLMS_AXS_LESSON_PREVIOUS');
											?></a>
									<?php
									} else {
									?>
										<span class="pull-left tovuti-nav-button disabled"><i class="fa-solid fa-lock-keyhole"></i><?php //echo " " . JText::_('COM_SPLMS_AXS_LESSON_PREVIOUS');
																																	?></span>
									<?php
									}
									?>

									<div class="clearfix" style="margin-bottom: 20px;"></div>

									<center>
										<?php
										if ($lesson_quiz) {
											$item_quiz = $courses_model->getUserQuizzes($lesson_quiz->course_id, $lesson_quiz->lesson_id, $user_id, $lesson_quiz->splms_quizquestion_id);
											$lesson_quiz_params = json_decode($lesson_quiz->params);

											$button_text = 'COM_SPLMS_AXS_LESSON_TAKE_QUIZ';
											$button_icon = 'lizicon-quill';

											if ($lesson_quiz_params->mode == 'survey') {
												$button_text = 'COM_SPLMS_AXS_LESSON_TAKE_SURVEY';
											}

											if ($lesson_quiz_params->button_text) {
												$button_text = $lesson_quiz_params->button_text;
											}

											if ($lesson_quiz_params->button_icon) {
												$button_icon = $lesson_quiz_params->button_icon;
											}

											if (($item_quiz->attempts < $lesson_quiz->tries) || !$lesson_quiz->tries) {
												$alias = JRoute::_('index.php?option=com_splms&view=quizquestion&tmpl=component&id=' . $lesson_quiz->splms_quizquestion_id);
										?>
												<button id="take_quiz_button_<?php echo $lesson_quiz->splms_quizquestion_id; ?>" alias="<?php echo $alias; ?>" class="quizButton btn btn-default" data-mediabox-height="900" data-mediabox-width="800">
													<span class="<?php echo $button_icon; ?>"></span>
													<?php echo JText::_($button_text); ?>
												</button>
											<?php
											} else {
											?>
												<button id="take_quiz_button_<?php echo $lesson_quiz->splms_quizquestion_id; ?>" class="btn btn-default quiz_results">
													<span class="lizicon-blocked"></span>&nbsp;
													<?php echo AxsLanguage::text("COM_SPLMS_AXS_NO_MORE_QUIZ_ATTEMPTS", "No more quiz attempts allowed"); ?>
												</button>
											<?php
											} /*($lesson_params->use_library_content && ( $lesson_params->scorm_id || $lesson_params->bizlibrary_content_id ) && $user_id && ( $scormAccess || $contentLibraryAccess) )*/
										} else {
											//old mark complete button  -- will remove later
											if ($progress->status != 'completed' && !$next->splms_lesson_id) {
											?>
												<button class="checkComplete btn btn-default completeButton" data-lesson="<?php echo $this->item->splms_lesson_id; ?>">
													<span class="lizicon-checkmark"></span>
													<?php echo AxsLanguage::text("COM_SPLMS_AXS_QUIZ_MARK_COMPLETE", "Mark Completed"); ?>
												</button>
										<?php
											}
										}
										?>

									</center>

									<?php
									if ($progress->status) {
									?>

										<div class="status" style="text-align:center; margin-top:15px;">
											<?php
											switch ($progress->status) {
												case 'completed':
													//$icon = '<span title="Completed" class="green lizicon-checkmark"></span>';
													$statusText = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_COMPLETE_TEXT", "Lesson Completed");
													$statusTitle = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_COMPLETE_HOVER", "Completed");
													$statusIcon = 'lizicon-checkmark';
													$statusColor = 'bs-dark-success';
													break;
												case 'started':
													//$icon = '<span title="Started" class="grey lizicon-clock"></span>';
													$statusText = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_STARTED_TEXT", "Lesson Started");
													$statusTitle = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_STARTED_HOVER", "Started");
													$statusIcon = 'lizicon-clock';
													$statusColor = 'grey';
													break;
												case 'failed':
													//$icon = '<span title="Failed Attempt" class="red lizicon-warning"></span>';
													$statusText = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_FAILED_TEXT", "Failed Attempt");
													$statusTitle = AxsLanguage::text("COM_SPLMS_AXS_QUIZ_FAILED_HOVER", "Failed Attempt");
													$statusIcon = 'lizicon-warning';
													$statusColor = 'bs-danger';
													break;
											}
											?>

											<span title="<?php echo $statusTitle; ?>" class="<?php echo $statusColor . ' ' . $statusIcon; ?>"></span>
											<span class="subtle-text"><?php echo $statusText; ?></span>
											<?php
											if ($item_quiz->attempts > 0) {
												if ($item_quiz) {
													if ($lesson_quiz_params->mode == 'survey') {
														echo '<br/><span class="results">' . AxsLanguage::text("COM_SPLMS_SURVEY_COMPLETED", "Survey Completed") . '</span>';
													} else {
														if ($item_quiz->attempts > 1) {
															$attempts = $item_quiz->attempts . " " . JText::_('COM_SPLMS_AXS_COURSE_QUIZ_ATTEMPTS_PLURAL');
														} else {
															$attempts = $item_quiz->attempts . " " . JText::_('COM_SPLMS_AXS_COURSE_QUIZ_ATTEMPTS_SINGULAR');
														}
											?>
														<br />
														<span class="results" style="font-weight:bold;"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_SCORE", "Score") ?>: <?php echo $item_quiz->highscore; ?>% <?php echo $item_quiz->breakdown; ?> - <?php echo $attempts; ?></span>
														<br />
											<?php

													}
												}
											}
											?>
										</div>
									<?php
									}
									?>
								</div>

								<?php if ($filesCount) { ?>
									<div class="box lesson-links">
										<h6 class="lms-subtitle"><span class="fa fa-download"></span> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_FILES', 'Lesson Files'); ?></h6>
										<?php
										$fileList = '';
										for ($i = 0; $i < $filesCount; $i++) {
											$action = 'download';
											if ($files->action[$i] == 'newtab') {
												$action = 'target="_blank"';
											}
											$fileList .= '<a href="' . $files->source[$i] . '" ' . $action . '>' . $files->title[$i] . '</a><br/> ';
										}
										echo $fileList;
										?>
										<div class="clearfix"></div>
									</div>
								<?php } ?>

								<?php if ($linksCount) { ?>
									<div class="box lesson-links">
										<div class="lms-subtitle"><span class="fa fa-paper-plane-o"></span> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_LINKS', 'Lesson Links'); ?></div>
										<?php
										$linkList = '';
										for ($i = 0; $i < $linksCount; $i++) {
											$linkList .= '<a href="' . $links->url[$i] . '" target="_blank">' . $links->title[$i] . '</a><br/> ';
										}
										echo $linkList;
										?>
										<div class="clearfix"></div>
									</div>
								<?php } ?>

								<?php
								$teacherList = $model->getLessonTeachers($this->item);
								if ((!isset($course_params->show_teachers) || $course_params->show_teachers) && $teacherList) {
								?>
									<div class="box">
										<div class="lms-subtitle">
											<span class="fa fa-graduation-cap"></span>
											<?php

											if ($course_params->teacher_list_text) {
												echo $course_params->teacher_list_text;
											} else {
												echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_TEACHERS", "Teachers");
											}
											?>
										</div>
										<?php
										foreach ($teacherList as $teacherItem) {

											$params = json_decode($teacherItem->params);
											$teacherUrl = null;
											$cUser = null;
											$teacherProfile = null;
											$photo = $teacherItem->image;

											if ($params) {

												if ($params->user) {
													$teacherProfile = AxsExtra::getUserProfileData($params->user);
													$cUser = CFactory::getUser($params->user);
													//$online = $cUser->isOnline();

													if ($params->image_type == "social" && $params->user) {
														$photo = $teacherProfile->photo;
													}

													if ($params->profile_type == 'social' && $params->user) {
														if (!$cUser) {
															$teacherUrl = JRoute::_('index.php?option=com_community') . $teacherProfile->alias;
														}
													}
												}
											}

											if (!$photo) {
												$photo = '/images/user.png';
											}
										?>

											<?php if ($params->image_type == 'social' && $params->user) { ?>
												<img class="teacher-img <?php if ($online) {
																			echo "student-online";
																		} ?>" src="<?php echo $cUser->getThumbAvatar(); ?>" title="<?php echo CTooltip::cAvatarTooltip($cUser); ?>" alt="<?php echo CStringHelper::escape($cUser->getDisplayName()); ?>" data-author="<?php echo $cUser->id; ?>">
											<?php } else { ?>
												<?php if ($photo) { ?>
													<img class="teacher-img" src="<?php echo $photo; ?>">
												<?php } ?>
											<?php } ?>

											<div class="lesson-blue text-center" style="width:100%; clear:both;">
												<?php if ($teacherUrl) { ?>
													<a href="<?php echo $teacherUrl; ?>">
													<?php } ?>
													<?php echo $teacherItem->title; ?>
													<?php if ($teacherUrl) { ?>
													</a>
												<?php } ?>
												<br />
												<span class="sub-heading">
													<?php echo $teacherItem->sub_heading; ?>
												</span>
											</div>
										<?php
										}
										?>
										<div class="clearfix"></div>
									</div>
								<?php } ?>

								<?php if (($course_params->discussion_size == 'small' || !$course_params->discussion_size) && ($settings->show_discussions || !isset($settings->show_discussions))) { ?>
									<div class="box undocked small-commentbox">
										<h6 class="lms-subtitle"><span class="fa fa-comments"></span> <?php echo JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION'); ?></h6>
										<div class="discussion">
											<div class="clearfix"></div>
											<?php
											$comment_id = "lesson_" . $lesson_id;

											$params = new stdClass();

											$params->title = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_HOVER');
											$params->button_title = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_BUTTON');
											$params->placeholder = JText::_('COM_SPLMS_AXS_LESSON_DISCUSSION_PLACEHOLDER');
											$params->auto_update = $auto_discussion;
											$params->post_id = $comment_id;

											display_comments($params);
											?>
											<div class="clearfix"></div>
										</div>
									</div>
								<?php } ?>
							</aside>
						</div>
					</div>
				</div>
			</div>
		</div>
		<audio style="display: none;" id="completedAudio">
			<source src="https://tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/learning-management-system/completed.mp3" type="audio/mpeg">
		</audio>

		<script>
			var text_unable_to_remove_item = "<?php echo JText::_('COM_SPLMS_AXS_LESSON_UNABLE_TO_REMOVE'); ?>";
			//progress bar variables
			var course_prog_start = 0;
			var course_prog = <?php echo $percentage; ?>;
			var completedAudio = document.getElementById('completedAudio');


			<?php if ($course_params->show_progress || !isset($course_params->show_progress)) { ?>

				//progress bar
				function courseProg(course_prog) {
					$(".progress-bar").animate({
						width: course_prog + "%"
					}, {
						duration: 2500,
						complete: function() {
							$(this).removeClass('progress-bar-striped');
						}
					});
					$('.course-bar-percentage').each(function() {
						var $this = $(this);
						jQuery({
							Counter: course_prog_start
						}).animate({
							Counter: course_prog
						}, {
							duration: 2600,
							step: function() {
								$this.text(Math.ceil(this.Counter) + '%');
							}
						});
					});
					course_prog_start = course_prog;
				}
				courseProg(course_prog);
			<?php } ?>

			function checkComplete(updateProgress) {
				var status = false;
				$.ajax({
					url: '/index.php?option=com_splms&task=lessons.checkComplete&format=raw',
					async: false,
					data: {
						course_id: '<?php echo $this->item->splms_course_id; ?>',
						lesson_id: '<?php echo $this->item->splms_lesson_id; ?>',
						language: language
					},
					type: 'post'
				}).done(function(result) {
					var response = JSON.parse(result);
					console.log('checkComplete response : ');
					console.log(response);
					if (response.error == 'User Session Error') {
						alert("<?php echo AxsLanguage::text("AXS_YOUR_SESSION_HAS_ENDED", "Your Session has ended please log in again.") ?>");
					} else {
						if (response.status == 'completed') {
							$('.status').html('');
							$('.status').html('<span title="Completed" class="green lizicon-checkmark"></span><span style="font-style:italic; font-size:12px; color:#777"><?php echo AxsLanguage::text("COM_SPLMS_AXS_QUIZ_COMPLETE_TEXT", "Lesson Completed") ?></span> <br/>');
							if (updateProgress) {
								courseProg(response.percentage);
							}
							status = true;
						}
					}

				}).fail(function() {
					console.log('Not Completed');
				});
				return status;
			}

			function checkCompleteAICC(updateProgress, courseId, lessonId) {
				var status = false;

				$.ajax({
					url: '/index.php?option=com_splms&task=lessons.checkComplete&format=raw',
					async: false,
					data: {
						course_id: courseId,
						lesson_id: lessonId
					},
					type: 'post'
				}).done(function(result) {
					var response = JSON.parse(result);

					if (response.error == 'User Session Error') {
						alert('Your Session has ended please log in again.');
					} else {
						if (response.status == 'completed') {
							$('.status').html('');
							$('.status').html('<span title="Completed" class="green lizicon-checkmark"></span><span style="font-style:italic; font-size:12px; color:#777">Lesson Completed</span> <br/>');
							if (updateProgress) {
								courseProg(response.percentage);
							}
							status = true;
						}
					}
				}).fail(function() {
					console.log('Not Completed');
				});

				return status;
			}

			$('.checkComplete').click(function(e) {
				e.preventDefault();
				var updateProgress = false;
				var lessonGating = <?php echo $lessonGating; ?>;
				var nextLesson = $(this).attr('href');
				if (!nextLesson) {
					updateProgress = true;
				}
				var check = checkComplete(updateProgress);
				if (!nextLesson) {
					if (check) {
						$('.completeButton').remove();
					} else {
						alert("<?php echo AxsLanguage::text("COM_SPLMS_USER_COMPLETE_ALL_REQUIRED_ACTIVITIES", "You need to complete all required activities before you can go to the next lesson.") ?>");
					}
				} else {
					if (!lessonGating || check) {
						window.location.href = nextLesson;
					} else {
						alert("<?php echo AxsLanguage::text("COM_SPLMS_USER_COMPLETE_ALL_REQUIRED_ACTIVITIES", "You need to complete all required activities before you can go to the next lesson.") ?>");
					}
				}
			});

			window.addEventListener("message", receiveMessage, false);

			function receiveMessage(response) {
				if (!response.origin.includes("<?php echo $_SERVER['HTTP_HOST']; ?>")) {
					return;
				}
				if (typeof response.data !== 'string') {
					return;
				}
				if (!response.data.includes("activity_id")) {
					return;
				}
				var data = JSON.parse(response.data);
				if (data.completed) {
					console.log('Checking for Completion...');
					let activity = $('.activity_' + data.activity_id)
					if (!activity.hasClass('ic_completed_active')) {
						activity.find('.incomplete-message').hide();
						activity.find('.completed-message').show();
						activity.addClass('ic_completed_active');
						completedAudio.play();
					}
					let requiredActivity = $('.required_' + data.activity_id);
					if (requiredActivity.hasClass('ic_required_active')) {
						requiredActivity.removeClass('ic_required_active');
					}

					checkComplete(true);
				}
			}


			$(".addtocart").click(
				function() {
					var which = this.getAttribute("product-id");

					$.ajax({
						url: '/index.php?option=com_splms&task=cart.addItem&format=raw',
						data: {
							product_id: which
						},
						type: 'post'
					}).done(function(result) {
						//console.log(result);
						window.location = result;
					}).fail(function() {
						alert(text_unable_to_remove_item);
					});
				}
			);
		</script>

	<?php
	} else {
	?>
		<center>
			<h2><?php echo AxsLanguage::text("COM_SPLMS_AXS_LESSON_LOCKED", "This lesson is locked."); ?></h2>
			<?php if (!$user_id) { ?>
				<br />
				<a class="popModule btn-lg btn-primary" href="javascript:void(0)" title="287"><i class="fa fa-lock"></i> <?php echo AxsLanguage::text("COM_SPLMS_LOGIN_TO_VIEW_LESSON", "Login To View Lesson") ?></a>
			<?php } ?>
		</center>
	<?php
	}
} else {
	?>
	<center>
		<h2><?php echo AxsLanguage::text("COM_SPLMS_AXS_LESSON_NO_ACCESS", "You do not have access to this lesson."); ?></h2>
	</center>
<?php
}

if (!$hasNextLesson) {
?>
	<script>
		window.checkComplete(true);
	</script>
<?php
}
?>