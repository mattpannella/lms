<?php
defined('_JEXEC') or die;

$fields = AxsUser::getProfileFields();
$fieldArray = array();
foreach($fields as $field) {
	$fieldArray['['.$field->name.']'] = $field->id;
}

$input = JFactory::getApplication()->input;
$input->get('cert');
$certParamsEncrypted = $_GET['cert'];
$key = AxsKeys::getKey('lms');
$cert = AxsEncryption::decrypt(base64_decode($certParamsEncrypted), $key);
$hideDownloadButton = $_GET['HideDownloadButton'];
$certificate_id = $cert->certificate_id;
$award_id = $cert->award_id;
$cert_user_id = $cert->user_id;
$award = AxsLMS::getUserAward($award_id);
$awardParams = json_decode($award->params);

$languageOverrides = AxsLanguage::getLanguageOverrides($awardParams,$cert->language);

if($languageOverrides) {
	$certificate_id = $languageOverrides->certificate;
}


$certificate = AxsLMS::getUserCertificate($award_id,$cert_user_id,$certificate_id);

$awardEarned = AxsLMS::getUserAwardEarned($award_id,$cert_user_id);
$certificateParams = json_decode($certificate->params);
$cert_user = CFactory::getUser($cert_user_id);
$certificateTitle = str_replace(' ', '_', $award->title);
$certificateTitle = str_replace('"', '', $award->title);
$expiration = '';


$date_issued = date("M d, Y",strtotime($awardEarned->date_earned));
if($awardEarned->date_expires && $awardEarned->date_expires > 0) {
	$expiration = date("M d, Y",strtotime($awardEarned->date_expires));
} elseif ($awardParams->expiration_amount && $awardEarned->date_expires > 0) {
	$expiration = date("M d, Y",strtotime($awardEarned->date_earned. " + ".$awardParams->expiration_amount." ".$awardParams->expiration_type));
}

$certificate->params = AxsUser::mergeFields($cert_user_id,$date_issued,$expiration,$fieldArray,$certificateParams);

?>

<link rel="stylesheet" href="/templates/axs/css/bootstrap.css?v=5">
<script type="text/javascript" src="/js/html2canvas2.js?v=2"></script>
<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poller+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Chonburi" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
<link rel="stylesheet" href="/administrator/components/com_splms/assets/css/certificate_builder.css?v=2">

<style type="text/css">
	.certificate-container {
		width: 100%;
		position: relative;
		z-index: 10;
	}
	.modal {
		display: inherit !important;
	}

	.certificate-actions {
		padding-top: 10px;
		padding-bottom: 10px;
		margin-bottom: 10px;
		width:100%;
		clear: both;
		text-align: center;
		/*border-bottom: solid 1px #999;*/
		border-radius: 5px;
		background: #16304b;
	}
	.cert-badge-icon {
	    bottom: 22%;
	    left: 8.1%;
	}
	.cert-badge-image {
	    bottom: 23%;
	    left: 8.1%;
	}
	.cert-badge-image img {
	    max-height:100%;
	}
	#certificate-container-print,
	#certificate-print {
		position: absolute;
		top:0px;
		left:0px;
		z-index: 0;
	}
	#certificate-print-cover {
		position: absolute;
		top:0px;
		left:0px;
		z-index: 1;
	}

	.cert-id-text {
		position: absolute;
		bottom: 10px;
		right: 10px;
	}

	<?php
        if($certificateParams->custom_css) {
            echo $certificateParams->custom_css;
        }
    ?>
</style>

<a id="certificate-image" style="display:none;"></a>
<?php
	if(!$hideDownloadButton){
?>
<div class="certificate-actions">
	<a class="btn btn-lg btn-light download-image"><i class="fa fa-download"></i> <?php echo AxsLanguage::text("COM_SPLMS_AXS_DOWNLOAD_CERTIFICATE", "Download Certificate") ?></a>
</div>
<?php
	}
?>
<div id="certificate-container">
    <div id="certificate">
        <div class="cert-title">
            <div class="cert-title-top"></div>
            <div class="cert-title-bottom"></div>
        </div>
        <div class="cert-heading"></div>
        <div class="cert-name"></div>
        <div class="cert-footer"></div>

        <div class="cert-badge">
            <i id="cert-badge-icon" class="" style="display: none;"></i>
            <img id="cert-badge-image" class="" style="display: none;"/>
        </div>
        <div class="cert-logo">
            <div class="cert-signature-1">
                <div class="cert-signature"></div>
                <div class="cert-signature-text"></div>
            </div>
            <div class="cert-signature-2">
                <div class="cert-signature"></div>
                <div class="cert-signature-text"></div>
            </div>
            <img class="cert-logo-image" src="" style="width: 20%;display: none;"/>
		</div>
		<?php if($certificateParams->show_certificate_id) {  ?>
			<div class="cert-id-text"><?php echo str_pad($awardEarned->award_id,4,'0',STR_PAD_LEFT); ?></div>
		<?php } ?>
    </div>
</div>


<script>

var defaults = {
	"background_image" : "/administrator/components/com_splms/assets/images/certificates/backgrounds/certificate-background-1.jpg",
	"header_top_text" : "Certificate",
	"header_top_font" : "Chonburi",
	"header_top_font_size" : 100,
	"header_top_color" : "#fcd25a",
	"header_bottom_text" : "of Completion",
	"header_bottom_font" : "Chonburi",
	"header_bottom_font_size" : 40,
	"header_bottom_color" : "#fff",
	"introduction_text" : "This Certificate<br/> is proudly presented<br/> for honorable achievement to",
	"introduction_font" : "Verdana",
	"introduction_font_size" : 40,
	"introduction_color" : "#222",
	"recipient_name_font" : "Pinyon Script",
	"recipient_name_font_size" : 80,
	"recipient_name_color" : "#222",
	"recipient_name" : "<?php echo $cert_user->name; ?>",
	"sub_text" : "Has demonstrated competence and understanding of the materials and successfully completed the requirements to be awarded this certification.",
	"sub_text_font" : "Verdana",
	"sub_text_font_size" : 20,
	"sub_text_color" : "#222",
	"footer_area_date_issued" : "<?php echo $date_issued; ?>",
	"footer_area_date_issued2" : "Date Issued",
	"footer_area_date_expires" : "<?php echo $expiration; ?>",
	"footer_area_date_expires2" : "Date Expires",
	"footer_area_text" : "Signature",
	"footer_area_left_text_signature" : "Bruce Lee",
	"footer_area_text_bottom_font_size" : 20,
	"footer_area_left_font" : "Pinyon Script",
	"footer_area_left_font_size" : 32,
	"footer_area_left_color" : "#222",
	"footer_area_right_text_signature" : "Chuck Norris",
	"footer_area_right_font" : "Pinyon Script",
	"footer_area_right_font_size" : 32,
	"footer_area_right_color" : "#222",
	"logo_image" : "",
	"seal_icon" : "",
	"seal_icon_font_size" : 180,
	"seal_overlay" : 55,
	"seal_overlay_color" : "#f5f7c6",
	"seal_overlay_dropshadow" : 1,
	"certificate_id_font_size" : 20
};

var overides = <?php echo json_encode($certificate->params); ?>;

switch(overides.footer_area_right_type) {
	case 'signature':
		defaults.footer_area_right_text_bottom = defaults.footer_area_text;
	break;

	case 'date_issued':
		defaults.footer_area_right_text_top    = defaults.footer_area_date_issued;
		defaults.footer_area_right_text_bottom = defaults.footer_area_date_issued2;
	break;

	case 'date_expires':
		defaults.footer_area_right_text_top    = defaults.footer_area_date_expires;
		defaults.footer_area_right_text_bottom = defaults.footer_area_date_expires2;
	break;
}

if(overides.seal_type) {
	defaults.seal_type = overides.seal_type;
}

switch(overides.footer_area_left_type) {
	case 'signature':
		defaults.footer_area_left_text_bottom = defaults.footer_area_text;
	break;

	case 'date_issued':
		defaults.footer_area_left_text_top    = defaults.footer_area_date_issued;
		defaults.footer_area_left_text_bottom = defaults.footer_area_date_issued2;
	break;

	case 'date_expires':
		defaults.footer_area_left_text_top = defaults.footer_area_date_expires;
		defaults.footer_area_left_text_bottom = defaults.footer_area_date_expires2;
	break;
}


if(overides.background_type == 'custom') {
	if(overides.background_image) {
		defaults.background_image = overides.background_image;
	}
}

if(overides.header_top == 'custom') {
	if(overides.header_top_text) {
		defaults.header_top_text = overides.header_top_text;
	}
	if(overides.header_top_font) {
		defaults.header_top_font = overides.header_top_font;
	}
	if(overides.header_top_font_size) {
		defaults.header_top_font_size = overides.header_top_font_size;
	}
	if(overides.header_top_color) {
		defaults.header_top_color = overides.header_top_color;
	}
}

if(overides.recipient_name == 'custom') {
	if(overides.recipient_name_font) {
		defaults.recipient_name_font = overides.recipient_name_font;
	}
	if(overides.recipient_name_font_size) {
		defaults.recipient_name_font_size = overides.recipient_name_font_size;
	}
	if(overides.recipient_name_color) {
		defaults.recipient_name_color = overides.recipient_name_color;
	}
}

if(overides.certificate_id_font_size) {
		defaults.certificate_id_font_size = overides.certificate_id_font_size;
}

if(overides.header_bottom == 'custom') {
	if(overides.header_bottom_text) {
		defaults.header_bottom_text = overides.header_bottom_text;
	}
	if(overides.header_bottom_font) {
		defaults.header_bottom_font = overides.header_bottom_font;
	}
	if(overides.header_bottom_font_size) {
		defaults.header_bottom_font_size = overides.header_bottom_font_size;
	}
	if(overides.header_bottom_color) {
		defaults.header_bottom_color = overides.header_bottom_color;
	}
}

if(overides.sub_area == 'custom') {
	if(overides.sub_text) {
		defaults.sub_text = overides.sub_text;
	}
	if(overides.sub_text_font) {
		defaults.sub_text_font = overides.sub_text_font;
	}
	if(overides.sub_text_font_size) {
		defaults.sub_text_font_size = overides.sub_text_font_size;
	}
	if(overides.sub_text_color) {
		defaults.sub_text_color = overides.sub_text_color;
	}
}

if(overides.introduction == 'custom') {
	if(overides.introduction_text) {
		defaults.introduction_text = overides.introduction_text;
	}
	if(overides.introduction_font) {
		defaults.introduction_font = overides.introduction_font;
	}
	if(overides.introduction_font_size) {
		defaults.introduction_font_size = overides.introduction_font_size;
	}
	if(overides.introduction_color) {
		defaults.introduction_color = overides.introduction_color;
	}
}

if(overides.footer_area_left == 'custom') {
	if(overides.footer_area_left_text_top) {
		defaults.footer_area_left_text_top = overides.footer_area_left_text_top;
	}
	if(overides.footer_area_left_text_bottom) {
		defaults.footer_area_left_text_bottom = overides.footer_area_left_text_bottom;
	}
	if(overides.footer_area_left_font) {
		defaults.footer_area_left_font = overides.footer_area_left_font;
	}
	if(overides.footer_area_left_font_size) {
		defaults.footer_area_left_font_size = overides.footer_area_left_font_size;
	}
	if(overides.footer_area_left_color) {
		defaults.footer_area_left_color = overides.footer_area_left_color;
	}
}


if(overides.footer_area_right == 'custom') {
	if(overides.footer_area_right_text_top) {
		defaults.footer_area_right_text_top = overides.footer_area_right_text_top;
	}
	if(overides.footer_area_right_text_bottom) {
		defaults.footer_area_right_text_bottom = overides.footer_area_right_text_bottom;
	}
	if(overides.footer_area_right_font) {
		defaults.footer_area_right_font = overides.footer_area_right_font;
	}
	if(overides.footer_area_right_font_size) {
		defaults.footer_area_right_font_size = overides.footer_area_right_font_size;
	}
	if(overides.footer_area_right_color) {
		defaults.footer_area_right_color = overides.footer_area_right_color;
	}
}

if(overides.logo_image) {
	defaults.logo_image = overides.logo_image;
}

if(overides.seal_icon) {
	defaults.seal_icon = overides.seal_icon;
}

if(overides.seal_image) {
	defaults.seal_image = overides.seal_image;
}

if(overides.seal_overlay_dropshadow) {
	defaults.seal_overlay_dropshadow = overides.seal_overlay_dropshadow;
}

if(overides.seal_overlay) {
	defaults.seal_overlay = overides.seal_overlay;
}

if(overides.seal_overlay_color) {
	defaults.seal_overlay_color = overides.seal_overlay_color;
}


function buildCertificate(ratio) {
	var containerWidth = jQuery('#certificate-container').width();
	if(!ratio) {
		var ratio = 1650 / containerWidth;
	}
	var certWidth  = 1650 / ratio;
	var certHeight = 1275 / ratio;
	jQuery('#certificate').css({'width':certWidth, 'height':certHeight});
	jQuery('#certificate').css("backgroundImage", "url("+defaults.background_image+")");
	jQuery('.cert-title-top').html(defaults.header_top_text);
	jQuery('.cert-title-top').css('fontFamily',defaults.header_top_font);
	jQuery('.cert-title-top').css('fontSize',defaults.header_top_font_size / ratio +'px');
	jQuery('.cert-title-top').css('color',defaults.header_top_color);
	jQuery('.cert-title-bottom').html(defaults.header_bottom_text);
	jQuery('.cert-title-bottom').css('fontFamily',defaults.header_bottom_font);
	jQuery('.cert-title-bottom').css('fontSize',defaults.header_bottom_font_size / ratio +'px');
	jQuery('.cert-title-bottom').css('color',defaults.header_bottom_color);
	jQuery('.cert-heading').html(defaults.introduction_text);
	jQuery('.cert-heading').css('fontFamily',defaults.introduction_font);
	jQuery('.cert-heading').css('fontSize',defaults.introduction_font_size / ratio +'px');
	jQuery('.cert-heading').css('color',defaults.introduction_color);
	jQuery('.cert-name').css('fontFamily',defaults.recipient_name_font);
	jQuery('.cert-name').css('fontSize',defaults.recipient_name_font_size / ratio +'px');
	jQuery('.cert-name').css('color',defaults.recipient_name_color);
	jQuery('.cert-name').html(defaults.recipient_name);
	jQuery('.cert-footer').html(defaults.sub_text);
	jQuery('.cert-footer').css('fontFamily',defaults.sub_text_font);
	jQuery('.cert-footer').css('fontSize',defaults.sub_text_font_size / ratio +'px');
	jQuery('.cert-footer').css('color',defaults.sub_text_color);
	jQuery('.cert-signature-2 > .cert-signature').html(defaults.footer_area_left_text_top);
	jQuery('.cert-signature-2 > .cert-signature-text').html(defaults.footer_area_left_text_bottom);
	jQuery('.cert-signature-text').css('fontSize',defaults.footer_area_text_bottom_font_size / ratio +'px');
	jQuery('.cert-id-text').css('fontSize',defaults.certificate_id_font_size / ratio +'px');
	jQuery('.cert-signature-text').css('bottom','-'+ 40 / ratio +'px');
	jQuery('.cert-signature-2 > .cert-signature').css('fontFamily',defaults.footer_area_left_font);
	jQuery('.cert-signature-2 > .cert-signature').css('fontSize',defaults.footer_area_left_font_size / ratio +'px');
	jQuery('.cert-signature-2 > .cert-signature').css('color',defaults.footer_area_left_color);
	jQuery('.cert-signature-1 > .cert-signature').html(defaults.footer_area_right_text_top);
	jQuery('.cert-signature-1 > .cert-signature-text').html(defaults.footer_area_right_text_bottom);
	jQuery('.cert-signature-1 > .cert-signature').css('fontFamily',defaults.footer_area_right_font);
	jQuery('.cert-signature-1 > .cert-signature').css('fontSize',defaults.footer_area_right_font_size / ratio +'px');
	jQuery('.cert-signature-1 > .cert-signature').css('color',defaults.footer_area_right_color);
	jQuery('.cert-badge').css('fontSize',defaults.seal_icon_font_size / ratio +'px');
	jQuery('.cert-badge').css('opacity',defaults.seal_overlay / 100);
	jQuery('.cert-badge').css('color',defaults.seal_overlay_color);
	jQuery('#cert-badge-image').attr('src',defaults.seal_image);
	jQuery('#cert-badge-icon').addClass(defaults.seal_icon);

	if(defaults.seal_type == 'image') {
		jQuery('.cert-badge').removeClass('cert-badge-icon');
		jQuery('.cert-badge').addClass('cert-badge-image');
		jQuery('#cert-badge-icon').hide();
		jQuery('#cert-badge-image').show();
	}

	if(defaults.seal_type == 'icon') {
		jQuery('.cert-badge').removeClass('cert-badge-image');
		jQuery('.cert-badge').addClass('cert-badge-icon');
		jQuery('#cert-badge-image').hide();
		jQuery('#cert-badge-icon').show();
	}

	if(defaults.logo_image) {
		jQuery('.cert-logo-image').attr('src',defaults.logo_image);
		jQuery('.cert-logo-image').show();
	}


	if(defaults.seal_overlay_dropshadow) {
		jQuery('.cert-badge').addClass('cert-badge-icon-shadow');
	}
}

jQuery(window).resize(function(){
	buildCertificate();
});

buildCertificate();

jQuery(document).ready(function() {
	jQuery(".download-image").click(function(){
		buildCertificate(1);
		html2canvas(jQuery("#certificate")[0]).then(function(canvas) {
		      //onrendered: function(canvas) {
				jQuery('#certificate-image').html("");
				jQuery('#certificate-image').append(canvas);
				jQuery('#certificate-image').attr('href', canvas.toDataURL("image/png"));
				jQuery('#certificate-image').attr('download',"<?php echo $certificateTitle; ?>.png");
				jQuery('#certificate-image')[0].click();
				jQuery('#certificate-image').html("");
		    //}
		});
		buildCertificate();
	});
});

</script>
