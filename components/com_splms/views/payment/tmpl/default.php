<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$notify_url 	= JURI::base() . 'index.php?option=com_splms&view=payment&task=notify';
$return_success = JURI::base() . 'index.php?option=com_splms&view=payment&task=success';
$return_cancel  = JURI::base() . 'index.php?option=com_splms&view=payment&task=cancel';
?>

<table border="1" width="100%">
	<tr>
		<td><?php echo AxsLanguage::text("AXS_PRODUCT_NAME", "Product Name") ?></td>
		<td><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_PRICE", "Price") ?></td>
		<td><?php echo AxsLanguage::text("JLIB_RULES_ACTION", "Action") ?></td>
	</tr>

	<?php foreach ($this->carts as $item) { ?>
		<tr>
			<td><?php echo $item['product_name']; ?></td>
			<td><?php echo $item['price']; ?></td>
			<td>
				<form name="order_remove" method="post">
					<input type="hidden" name="product_id" value="<?php echo $item['product_id']; ?>" />
					<input type="submit" name="remove" />
				</form>
			</td>
		</tr>
	<?php } ?>
</table>

<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" >
	<input type="hidden" name="cmd" value="_cart">
	<input type="hidden" name="upload" value="1">
	<input type="hidden" name="business" value="rifat3280-facilitator@gmail.com">
	
	<?php
	$i = 1;
	foreach ($this->carts as $item) { ?>
			<div id="item_<?php echo $i; ?>" class="itemwrap">
			<input type="hidden" name="item_name_<?php echo $i; ?>" value="<?php echo $item['product_name'] ?>">
			<input type="hidden" name="item_number_<?php echo $i; ?>" value="<?php echo $item['product_id'] ?>">
			<input type="hidden" name="quantity_<?php echo $i; ?>" value="1">
			<input type="hidden" name="amount_<?php echo $i; ?>" value="<?php echo $item['price'] ?>">
			</div>
		<?php 
			$i++;
	}
	
	?>
	
	<input type="hidden" name="invoice" value="<?php echo time().rand( 1000 , 9999 ); ?>">

	<input type="hidden" name="currency_code" value="USD">
	<!-- Specify the Pages for Successful payment & failed Payment -->
	<input type="hidden" name="notify_url" value="<?php echo $return_success; ?>"/>
	<input type="hidden" name="return" value="<?php echo $return_success; ?>"/>
	<input type="hidden" name="cancel_return" value="<?php echo $return_cancel; ?>"/>
	<!-- Display the payment button. -->
	<!-- Saved buttons display an appropriate button image. -->
	<input type="image" name="submit" border="0"
	src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
	alt="PayPal - The safer, easier way to pay online">
	<img alt="" border="0" width="1" height="1"
	src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>
<!-- Paypal Form ends -->