<?php
 // error_reporting(E_ALL);
 // ini_set('display_errors', 1);
// No direct access
defined('_JEXEC') or die();

//$date = JFactory::getDate();


// // Now time
// $now = JFactory::getDate();
// $nowDate = JFactory::getDate()->format('Y-m-d h-i-s');

// echo $nowDate; // Current date and time

require_once('assets/getID3/getid3/getid3.php');

$getID3 = new getID3;

?>

<script type="text/javascript" src="/components/com_splms/assets/js/course.js?v=3"></script>

<div class="row">
	<div class="container">
		<div id="splms" class="splms view-splms-course course-details">
			<div class="splms-course">
				<div class="course-thumbnail">
					    <div class="embed-responsive embed-responsive-16by9">
							<video controls id="course-intro-video" class="embed-responsive-item">
				        		<source src="<?php echo $this->src; ?>" type="video/mp4">
				    		</video>
						</div>
						
						<?php if (1 == 2) {?>
							<div class="lesson-video">			
								<div id="splms-lesson-video-<?php echo $id; ?>" class="lesson-video">
									<?php if ($this->id == 'remote-host') { ?>									
						                <!-- player markup -->
								        <div id="MainWrapper"></div>
								        <div id="playlist_list">
								             <!-- local tracks -->
								             <div id="playlist" data-address="playlist">
								                 <div class="playlistNonSelected" data-address="local1" data-type="local" data-mp4="<?php echo $this->src; ?>" data-preview="<?php echo JURI::root() . $this->item->image; ?>" data-thumb="<?php echo JURI::root() . $this->item->image; ?>">
								                 </div>
								             </div>
								         
								         </div>
									<?php } else{ ?>
									<div class="embed-responsive embed-responsive-16by9">
										<iframe src="<?php echo $this->src; ?>" width="100%" height="400" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
									</div>
									<?php } ?>								
								</div>
							</div>
						<?php } else { if($this->item->image) { ?>
							<div class="course-banner-img">
								<img class="splms-img-responsive" src="<?php echo $this->item->image; ?>" alt="<?php echo $this->item->title; ?>">
							</div>
						<?php } }?>

						<?php
						if ($this->item->price == 0) {
							echo '<span class="splms-badge-free">' . JText::_('COM_SPLMS_FREE') . '</span>';
						} ?>
				</div>

				<!-- start course-header -->
				<div class="course-header clearfix">
					<h2 class="course-title pull-left"><?php echo $this->item->title; ?></h2>
					<div class="apply-now pull-right">
						<?php if (($this->item->price != 0) && ($this->isAuthorised == '') ) { ?>

								<button 
									class="addtocart btn btn-primary"
									product-id="<?php echo $this->item->splms_course_id?>"
								>
									<i class="splms-icon"></i>
									<?php echo $this->coursePrice; ?> - <?php echo JText::_('COM_SPLMS_BUY_NOW'); ?>
								</button>
							</form>
						<?php }elseif ($this->isAuthorised != '') { ?>
							<a id="intro-vid" class="btn btn-primary" href="javascript:void(0);">
								<span id="show-intro-vid">
									<i class="splms-icon"></i><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_SHOW_INTRO_VIDEO", "Show Intro Video" ?>
								</span>
								<span id="hide-intro-vid">
									<i class="splms-icon"></i><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_HIDE_INTRO_VIDEO", "Hide Intro Video" ?>
								</span>
							</a>
							<a class="btn btn-primary" href="javascript:void(0);">
								<i class="splms-icon"></i>
								<?php echo JText::_('COM_SPLMS_PURCHASED'); ?>
							</a>
						<?php } ?>
					</div>
				</div> <!-- end course-header -->

				<div class="splms-course-description">
					<?php echo $this->item->description; ?>
				</div>

				<?php
					$course_id = $this->item->splms_course_id;
					$user_id = $this->user->id;
					$user_quiz_results = $this->quizresults;
					$accurate_thoughts = $this->accuratethoughts;
				?>

				<!-- Has lesson -->
				<?php if(!empty($this->lessons)) {?>
				<!-- start course-lessons  -->

				<!--<div class="course-lessons">-->
				<div class="course-lessons">
					<ul class="list-unstyled">

						<style>
							.icon-font {
								font-size:		32px;
								margin-right: 	10px;                                                    
							}

							.file-info {
								font-size: 		12px;                                                    
								margin-right: 	50px;
							}

	                        .pad-right {                                                    
								margin-right: 	50px;
							}

							.teacher-name {
								font-size:		15px; 
								font-weight: 	bold; 								
								min-height: 	60px; 
								margin-right:	50px; 
								float:			left; 
							}
	                                                
							.lesson-icons {
								float:			right; 								
								min-height: 	60px; 
								margin-right:	50px;
							}
	                                                
							.sub-heading {
								font-size:		12px; 
								color: 			#999;
							}

							.lesson-blue {
								color:			#337ab7;
							}

							.quiz_results {
								width:			150px;
								height:			50px;
								border-radius:	5px;
								color:			white;
								background-color: 	#337ab7;
								padding:		10px 10px 10px 10px;
								margin-top:		15px;
							}

							
						</style>

						<?php foreach ($this->lessons as $lesson) { ?>

							<?php

								$lesson_id = $lesson->splms_lesson_id;

								$lessonMaterials = SplmsModelCourses::getTeacherMaterialArray($lesson_id);
								$videoDuration = 0;
								$audioDuration = 0;

								foreach($lessonMaterials as $lessonMaterial) {
									$videoFileInfo = $getID3->analyze($lessonMaterial['video_file']);
									$audioFileInfo = $getID3->analyze($lessonMaterial['audio_file']);

									$videoDuration += $videoFileInfo['playtime_seconds'];
									$audioDuration += $audioFileInfo['playtime_seconds'];									
								}						
							?>

							<?php if ($lesson->lesson_type == 0 || $this->isAuthorised != '' || $this->item->price == 0) { ?>

								<?php
									$quiz = null;
									$quiz_id = null;
									$quiz_taken = false;
									//echo (var_dump($quiz_result));
									foreach ($this->quizquestions as $quizzes) {										
										if (($quizzes->splms_lesson_id == $lesson_id) && ($quizzes->splms_course_id == $course_id)) {
											$quiz = $quizzes;
											$quiz_id = $quiz->splms_quizquestion_id;

											foreach($user_quiz_results as $quiz_results) {
												//echo (var_dump($quiz_results));//->splms_quizquestion_id));
												if ($quiz_results->splms_quizquestion_id == $quiz_id) {
													$quiz_taken = true;
												}
											}
										}
									}									
								?>

								<li>
									<span style="font-size:20px; color:rgb(96,96,96);" onclick="show_lesson(<?php echo $lesson_id?>)">
										<!--<a href="<?php echo $lesson->lesson_url; ?>">-->
											<?php echo $lesson->title; ?>
										<!--</a>-->
									</span>							
									<span class="pull-right">
										<strong>
											<?php echo AxsLanguage::text("AXS_TOTAL_DURATION", "Total Duration") . ': '  . gmdate("H:i:s", $videoDuration + $audioDuration); ?>
										</strong>										
									</span>
									<div id="lesson_material_<?php echo $lesson_id; ?>" style="display:none;" class="lesson_material">
										<?php
										
											foreach($lessonMaterials as $lessonMaterial) {

												$videoFileInfo = $getID3->analyze($lessonMaterial['video_file']);
												$audioFileInfo = $getID3->analyze($lessonMaterial['audio_file']);

												$teacher_id = $lessonMaterial['teacher_id'];;
												$audio_file = $lessonMaterial['audio_file'];;
												$video_file = $lessonMaterial['video_file'];
												$pdf_file = $lessonMaterial['pdf_file'];
												$video_thumb = $lessonMaterial['video_thumb'];
												$video_duration = $videoFileInfo['playtime_string'];
												$audio_duration = $audioFileInfo['playtime_string'];

												$teacher = SplmsModelTeachers::getTeacher($teacher_id);

												echo (var_dump($accurate_thoughts));

										?>												

												<ul>
													<li>
														<a href="<?php echo $teacher->url ?>">
		                                                    <div class="comment-img">
		                                                         <img src="<?php echo $teacher->image; ?>">
		                                                    </div>                                                    
															<div class="teacher-name lesson-blue"><?php echo $teacher->title; ?><br/>
		                                                         <span class="sub-heading"><?php echo $teacher->sub_heading;?></span>
		                                                    </div>
		                                                </a>
														<div class="lesson-icons lesson-blue">
	                                                    	<div class="pull-left center pad-right">
																<span 
																	class="lizicon-film icon-font" 
																	onclick="toggle_media(<?php echo $lesson_id . ',' . $teacher_id ?>, 'video')">
																</span>
																<br/>
	                                                        	<span class="comment-date"><?php echo ($video_duration)?></span>
	                                                    	</div>
	 														<div class="pull-left center pad-right">
																<span 
																	class="lizicon-bullhorn icon-font"
																	onclick="toggle_media(<?php echo $lesson_id . ',' . $teacher_id ?>, 'audio')">
																</span>
																<br/>
	     														<span class="comment-date"><?php echo ($audio_duration)?></span>
	 														</div>
	 														<a href="<?php echo $lessonMaterial['pdf_file']?>" target="_blank">
		 														<div class="pull-left center pad-right">
																	<span class="lizicon-file-text2 icon-font"></span>
																	<br/>
																	<span class="comment-date">PDF</span>
		                                                    	</div>
		                                                    </a>														
	                                                    </div>                                                    
													</li>
												</ul>
												<div 
	                                               	id="lesson_video_<?php echo $lesson_id . '_' . $teacher_id?>" 
	                                             	style="display:none; width: 100%;"
	                                             	
	                                            >	                                            	
													<video width="800" controls>
														<source src="<?php echo $lessonMaterial['video_file']; ?>" type="video/mp4">
													</video>												
												</div>
												<div 
													id="lesson_audio_<?php echo $lesson_id . '_' . $teacher_id?>" 
													style="display:none;"													
												>														
													<audio controls>
												   		<source src="<?php echo $lessonMaterial['audio_file']; ?>" type="audio/mpeg">
												   	</audio>
												</div>

										<?php
											}
										?>
										
										<div id="thoughts_view_<?php echo $lesson_id; ?>" style="display:none;">											
											<textarea id="your_accurate_thoughts_<?php echo ($lesson_id);?>" rows="15" cols="75"></textarea>
											<span 
												class="btn btn-primary" 
												onclick="save_accurate_thoughts(<?php echo ($user_id . ', ' . $course_id . ', ' . $lesson_id); ?>)"
												style="margin-left:20px; margin-bottom: 25px;"
											>
												<?php echo AxsLanguage::text("COM_LMS_SUBMIT", "Submit") ?>
											</span>
										</div>

										<?php
										
											if ($quiz_id && !$quiz_taken) {
										?>
											<a 
												href="quiz?view=quizquestion&id=<?php echo $quiz_id; ?>"
												target="_blank"
												type="iframe"
												class="jcepopup noicon"
												data-mediabox-height="450"
												data-mediabox-width="800"
											>
												<span class="btn btn-primary">
													<?php echo AxsLanguage::text("COM_SPLMS_TAKE_THE_QUIZ_DURATION", "Take the Quiz - Duration:") . ' ' . $quiz->duration ?>
												</span>
											</a>
										<?php
											}

											if (!$quiz_id || $quiz_taken) {
										?>										
												<span class="btn btn-primary" onclick="give_thoughts(<?php echo ($lesson_id);?>)">
													<?php echo AxsLanguage::text("COM_SPLMS_AXS_YOUR_ACCURATE_THOUGHTS", "Your Accurate Thoughts") ?>
												</span>
												<br>
										<?php 											
											}

											if ($quiz_id && $quiz_taken) {
												$quiz_style = "";
											} else {
												$quiz_style = "display:none;";
											}
										?>

										<div id="quiz_results_<?php echo ($lesson_id); ?>" class="quiz_results" style="<?php echo $quiz_style ?>">
											<?php echo AxsLanguage::text("COM_SPLMS_QUIZ_RESULTS_CAPITALIZED", "QUIZ RESULTS") ?>
											<br>
											<?php echo AxsLanguage::text("COM_SPLMS_QUIZ_YOU_SCORED", "You scored:") ?> <?php echo $quiz_results->point . " / " . $quiz_results->total_marks ?>											
										</div>

										<div 
											id="quiz_view_<?php echo ($lesson_id); ?>"
											style="margin-top: 50px; width:100%; height: 500px;border: 1px solid; border-radius: 20px; display:none;"
											class="lesson-blue"											
										>

											<?php



												/*$quiz_list = json_decode($quiz->list_answers);

												$questions = array();
												$answers = array();
												$question_options = array();

												$questions = $quiz_list->qes_title;
												$answers = $quiz_list->right_ans;
												
												$question_options = array();

												array_push($question_options, $quiz_list->ans_one);
												array_push($question_options, $quiz_list->ans_two);
												array_push($question_options, $quiz_list->ans_three);
												array_push($question_options, $quiz_list->ans_four);*/

												/*$b = "<br>";

												for ($i = 0; $i < count($questions); $i++) {

													echo ($questions[$i] . $b);
													
													for ($j = 0; $j < count($question_options); $j++) {
														if ($j == $answers[$i]) {
															echo (" * ");
														} else {
															echo (" - ");
														}
														echo ($question_options[$j][$i] . $b);
													}

												}*/												

												//echo (json_encode($questions) . ", " . json_encode($question_options) . ", " . json_encode($answers));
											?>

											<script>
												//create_quiz(<?php echo (json_encode($questions) . ", " . json_encode($question_options) . ", " . json_encode($answers)); ?>);
												//create_quiz(1, 2, 3);
											</script>
										</div>
									</div>
								</li>
							<?php } else {?>
								<li class="splms-lesson-unauthorised" style="margin-bottom:20px;">
									<span>
								<i class="splms-icon-lock"></i>
										<?php echo $lesson->title; ?>
									</span>
									<span class="pull-right">
										<?php echo JText::_('COM_SPLMS_COMMON_DURATION') . JText::_(': '); ?>
										<?php echo $duration; ?>
									</span>
								</li>
							<?php } // end else ?>
						<?php } // END:: foreach ?>
					</ul>
				</div> <!-- end course-lessons -->
				<?php } // END:: has lesson ?>

				<!-- Has teacher -->
				<?php if (!empty($this->teachers)) {?>
				<div class="splms-course-teachers">
					<h3><?php echo JText::_('COM_SPLMS_MEET_OUR_COURSE_TEACHER'); ?></h3>
					<div class="splms-row">
						<?php foreach ($this->teachers as $teacher) { ?>
							<div class="splms-col-sm-3">
								<div class="splms-course-teacher">
									<a href="<?php echo $teacher->url; ?>"><img src="<?php echo $teacher->image; ?>" alt="<?php echo $teacher->title; ?>"></a>
									<h4><a href="<?php echo $teacher->url; ?>"><?php echo $teacher->title; ?></a></h4>
									<small><?php echo $teacher->specialist_in; ?></small>
								</div>
							</div>
						<?php } ?>			
					</div>
				</div>
				<?php } ?>
				<!-- END::  teacher -->
            </div>
        </div>
    </div>
</div>

<script>
	$(".addtocart").click(
		function() {
			var which = this.getAttribute("product-id");
			
			$.ajax({
				url: '/index.php?option=com_splms&task=cart.addItem&format=raw',
				data: {
					product_id: which
				},
				type: 'post'
			}).done(function(result) {
				//console.log(result);
				window.location = result;
			}).fail(function() {
				alert("Unable to remove item.  Please try again.");
			});
		}
	);
</script>