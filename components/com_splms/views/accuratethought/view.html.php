<?php

/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsViewCourse extends FOFViewHtml{

	public function display($tpl = null){

		//Load the method jquery script.
		JHtml::_('jquery.framework');

		// Get model
		$model = $this->getModel();
		// get item
		$this->item = $model->getItem();

		//Load Lessons Model
		$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');
		$quizquestion_model = FOFModel::getTmpInstance('QuizQuestions', 'SplmsModel');
		$accuratethought_model = FOFModel::getTmpInstance('AccurateThoughts', 'SplmsModel');

		//Joomla Component Helper & Get LMS Params
		jimport('joomla.application.component.helper');
		$this->params = JComponentHelper::getParams('com_splms');

		// Add Script
		$doc = JFactory::getDocument();
		$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/splms.js' );

		$this->user = JFactory::getUser();

		$this->lessons      	= $lessons_model->getLessons($this->item->splms_course_id);		
		$this->freeLesson   	= $lessons_model->getCourseFreeLesson($this->item->splms_course_id);
		$this->isAuthorised 	= $model->getIsbuycourse($this->user->id, $this->item->splms_course_id);
		
		$this->quizquestions 	= $quizquestion_model->getCourseQuizzes($this->item->splms_course_id);
		$this->quizresults		= $quizquestion_model->getQuizResult($this->user->id);
		$this->accuratethoughts	= $accuratethought_model->getAccurateThoughts($this->user->id, $this->item->splms_course_id);

		//Get course teacher
		$this->teachers 		= $model->getCourseTeachers( $this->item->splms_course_id );

		// Get Currency
		//$params = JComponentHelper::getParams('com_splms');
		$this->currency = explode(':', $this->params->get('currency', 'USD:$'));

		$this->coursePrice='';
		if ($this->item->price == 0) {
			$this->coursePrice = AxsLanguage::text("COM_SPLMS_FREE", "Free");
		}else{
			$this->coursePrice = $this->currency[1] . $this->item->price;
		}

		// Video Player
		
		if (isset($this->item->video_url) && $this->item->video_url) {
		//Video Check host URL
			$video = parse_url($this->item->video_url);

			switch($video['host']) {
				case 'youtu.be':
					$this->id = trim($video['path'],'/');
					$this->src = '//www.youtube.com/embed/' . $this->id;
				break;
				
				case 'www.youtube.com':
				case 'youtube.com':
					parse_str($video['query'], $query);
					$this->id = $query['v'];
					$this->src = '//www.youtube.com/embed/' . $this->id;
				break;
				
				case 'vimeo.com':
				case 'www.vimeo.com':
					$this->id = trim($video['path'],'/');
					$this->src = "//player.vimeo.com/video/{$this->id}";
				break;
				default:
					$this->id = 'remote-host';
					$this->src = $this->item->video_url;
			}
	
			if ($this->id == 'do not use') {
				$doc = JFactory::getDocument();

				// Load Player CSS files
				$doc->addStylesheet( JURI::root(true) . '/components/com_splms/assets/css/video-player/tooltipster.css' );
				$doc->addStylesheet( JURI::root(true) . '/components/com_splms/assets/css/video-player/videoGallery_noPlaylist.css' );

				// Load JS files
				$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/video-player/captionator.js' );
				$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/video-player/jquery.tooltipster.js' );
				$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/video-player/jquery.videoGallery.min.js' );

				?>
				<script type="text/javascript">
					var hap_player;
					// LMS Player Settings
					var video_logo 				= "<?php echo JURI::root() . $this->params->get('videologo'); ?>";
					var video_logo_x_offset 	= "<?php echo $this->params->get('video_logo_x_offset'); ?>";
					var video_logo_y_offset		= "<?php echo $this->params->get('video_logo_y_offset'); ?>";
					var video_logo_tooltip_text = "<?php echo $this->params->get('video_logo_tooltip_text'); ?>";
					var video_logo_url 			= "<?php echo $this->params->get('video_logo_url'); ?>";
					jQuery(document).ready(function($) {
						var vplp_settings = {
							/* media_id: unique string for player identification (if multiple player instances were used, then strings need to be different!) */
							media_id:'player1',
							/* use_deeplink: true, false */
							use_deeplink:false,
							/*active_playlist: Active playlist to start with. If no deeplink is used, enter element 'id' attribute, or if deeplink is used enter element data-address attribute. */
							active_playlist:'playlist',
							/*active_item: Active video to start with. Enter number, -1 = no video loaded, 0 = first video, 1 = second video etc */
							active_item: 1,
							
							/*auto_hide_controls: auto hide player controls on mouse out: true/false. Defaults to false on mobile. */
							auto_hide_controls: false,
							/*controls_timeout: time after which controls and playlist hides in fullscreen if screen is inactive, in miliseconds. */
							controls_timeout:3000,
							/*default_volume: 0-1 */

							default_volume:0.5,
							/*auto_play: true/false (defaults to false on mobile)*/
							auto_play: false,
							/*random_play: true/false */
							random_play:false,
							/* looping_on: on playlist end rewind to beginning (last item in playlist) */
							looping_on: false,
							/*auto_advance_to_next_video: true/false (use this to loop video) */
							auto_advance_to_next_video: false,
							/*auto_open_description: true/false  */
							auto_open_description:false,
							/*use_live_preview: true/false (if true, you need small videos for preview for local videos, otherwise thumbnails). Defaults to false on mobile. */
							use_live_preview: true,
							
							/* show_controls_in_advert: true/false (show controls while video advert plays)  */
							show_controls_in_advert:false,
							/* disable_seekbar_in_advert: true/false (disable seekbar while video advert plays)  */
							disable_seekbar_in_advert:false,
							/* show_skip_button_in_advert: true/false (show skip button while video advert plays)  */
							show_skip_button_in_advert: false,
							advert_skip_btn_text:'SKIP AD >',
							advert_skip_video_text:'You can skip to video in',
							
							/* context_menu_type: disabled, custom, default */
							context_menu_type:'custom',
							/* context_menu_text: Custom text link in context menu. Leave empty for none.  */
							context_menu_text:'',
							/* context_menu_link: url link, leave empty for none  */
							context_menu_link:'',
							/* context_menu_target: _blank/_parent (opens in new/same window)  */
							context_menu_target:'',
							
							logo_path: video_logo,
						    logo_position: 'tl',
						    logo_x_offset: video_logo_x_offset,
						    logo_y_offset: video_logo_x_offset,
						    logo_tooltip_text: video_logo_tooltip_text,
						    logo_url: video_logo_url,
						    logo_target: '_blank',
							
							/*aspect_ratio: video aspect ratio (0 = original, 1 = fit inside, 2 = fit outside). Defaults to 1 on mobile! */
							aspect_ratio: 2,
							/*playlist_orientation: vertical/horizontal  */
							playlist_orientation:'vertical',
							/*playlist_type: list/wall/wall_popup */
							playlist_type:'list',
							show_playlist:false,
							/*scroll_type: scroll/buttons  */
							scroll_type:'buttons',
							/*wall_path: folder replacement path for the wall data */
							wall_path:'/wall/',
							yt_app_id:'AIzaSyA9jm0xJOYX0KD1nuijTbhO4OFC4gYQnb0',
							use_share: false,
							/*fs_app_id: facebook application id (if you use facebook share, https://developers.facebook.com/apps) */
							fs_app_id:'',
							/*dropdown_id: id attribute of the element with holds the dropdown */
							dropdown_id:'#hap_drop',
							/*playlistList: dom element which holds list of playlists */
							playlist_list:'#playlist_list',
							buttons_url: {thumbnailPreloaderUrl: 'data/loading.gif'},
							/* auto_reuse_mail_for_download: true/false. download backup for ios, save email after client first enters email address and auto send all emails to the same address */
							auto_reuse_mail_for_download: false,
							use_tooltips:true,
							tooltip_titles: {
							   /* tooltip titles */
							   remove: 'Remove',/* remove button */
							   detail: 'View detail',/* lightbox button */
							   link: 'External link',/* external link button */
							   action: 'External action'}/* empty action button */
						};
						
						hap_player = $('#MainWrapper').videoGallery(vplp_settings);
					});
		        </script>
		<?php
			} // has video URL

		}

		return parent::display($tpl = null);
	}
}