<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Get Columns
$columns = $this->params->get('columns', 3);

?>

<div id="splms" class="splms view-splms-courses">
	<?php if(count($this->items)) { ?>
	<div class="splms-courses-list">

	<?php foreach(array_chunk($this->items, $columns) as $this->items) { ?>
		<div class="splms-row">

		<?php foreach ($this->items as $key=>$this->item) { ?>
			<div class="splms-col-sm-<?php echo round(12/$columns); ?>">
				<div class="splms-course splms-match-height">
					<div class="splms-common-overlay-wrapper">

	            		<img src="<?php echo $this->item->thumb; ?>" class="splms-course-img splms-img-responsive" alt="<?php echo $this->item->title; ?>">

	            		<?php if ($this->item->price == 0) {
	            			echo '<span class="splms-badge-free">' . AxsLanguage::text("COM_SPLMS_FREE", "Free") . '</span>';
	            		} ?>

						<div class="splms-common-overlay">
	                		<div class="splms-vertical-middle">
	                			<div>
	            					<a href="<?php echo $this->item->url; ?>" class="splms-readmore btn btn-default">
	            						<?php echo $this->params->get('readmore_text', AxsLanguage::text("COM_SPLMS_DETAILS", "Details")); ?>
	            					</a>
	                			</div>
	                		</div>
						</div>
					</div>

					<div class="splms-course-info">
						<h3 class="splms-courses-title">
							<a href="<?php echo $this->item->url; ?>">
								<?php echo $this->item->title; ?>
							</a>
						</h3>

						<!-- Has teacher -->
						<?php if (!empty($this->item->teachers)) { ?>
						<div class="splms-course-teachers">
							<span><?php echo AxsLanguage::text("COM_SPLMS_BY", "By"); ?></span>
								
							<?php foreach ($this->item->teachers as $teacher) {
								// Get Last Item
								$last_item = end($this->item->teachers);
							?>
							<a href="<?php echo $teacher->url; ?>" class="splms-teacher-name">
								<strong>
									<?php echo $teacher->title; ?>
								</strong>
								<?php echo ($teacher == $last_item) ? '' : ', '; ?>
							</a>
							<?php } // END:: foreach ?>
						</div>
						<?php } // END:: has teahcer ?>

						<p class="splms-course-short-info"><?php echo $this->item->short_description; ?></p>
						<div class="splms-course-meta">
							<?php echo $this->item->course_price; ?>
							<?php echo ' . '; ?>
							<?php echo count($this->item->lessons); ?>
							<?php echo AxsLanguage::text("COM_SPLMS_COMMON_LESSONS", "Lesson(s)"); ?>
							<?php echo ' . '; ?>
							<?php echo count($this->item->total_attachments); ?>
							<?php echo AxsLanguage::text("COM_SPLMS_COMMON_ATTACHMENTS", "Attachment(s)"); ?>
						</div>
					</div> <!-- /.splms-course-info -->
				</div> <!-- /.splms-course -->
			</div> <!-- /.splms-col-sm -->
		<?php } // END:: foreach ?>

	</div> <!-- /.splms-row -->
	<?php } // END::array_chunk ?>

	</div> <!-- /.splms-courses-list -->
	<?php } // END::has items ?>

	<?php if ($this->params->get('hide_pagination') == 0) { //Pagination ?>
		<?php if ($this->pagination->get('pages.total') >1) { ?>
			<div class="pagination">
				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
		<?php } ?>
	<?php } ?>
	
</div>