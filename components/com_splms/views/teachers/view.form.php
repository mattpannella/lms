<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

class SplmsViewTeachers extends FOFViewForm{
	//display function
	public function display( $tpl = null ){
		//get model
		$model = $this->getModel();
		//get items
		$this->items = $model->getItemList();

		// Load lesson Model
		$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');

		foreach ($this->items as $this->item) {
			// Generate URL
			$this->item->url = JRoute::_('index.php?option=com_splms&view=teacher&id='.$this->item->splms_teacher_id.':'.$this->item->slug . SplmsHelper::getItemid('teachers')); 
			  //Get Teachers Lessons
			$this->item->teacher_total_lessons= count($lessons_model->getTeacherLessons($this->item->splms_teacher_id));
			
		}

		return parent::display( $tpl = null );
	}
}