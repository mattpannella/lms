<?php
defined('_JEXEC') or die;

require_once ('modules/mod_award/helper.php');
JHtml::_('stylesheet', 'components/com_splms/assets/css/transcripts.css', array('version' => 'auto'));
JHtml::_('script', 'https://kit.fontawesome.com/a10f21bffc.js');
JHtml::_('stylesheet', 'modules/mod_award/assets/css/badges.css', array('version' => 'auto'));
JHtml::_('script', 'media/tovuti/js/social-share.js');
JHtml::_('script', 'media/kendo/js/kendo.all.min.js');
JHtml::_('script', 'js/html2canvas.js');

$isOnMobileDevice = AxsMobileHelper::isClientUsingMobile();

$input = JFactory::getApplication()->input;
$transcript_user_id = JFactory::getUser()->id;
if($input->get('params')) {
	$transcriptParamsEncrypted = $input->get('params');
	$key = AxsKeys::getKey('lms');
	$transcript = AxsEncryption::decrypt(base64_decode($transcriptParamsEncrypted), $key);
	if($transcript->user_id) {
		$transcript_user_id = $transcript->user_id;
	}
}
$profile = CFactory::getUser($transcript_user_id);
JFactory::getDocument()->setTitle(AxsLanguage::text("AXS_TRANSCRIPT", "Transcript") . " - " . $profile->name);

$settings 	  	  = json_decode(AxsLMS::getSettings()->params);
$badges 	      = ModAwardHelper::makeBadges($transcript_user_id);
$certificates     = ModAwardHelper::makeCertificates($transcript_user_id);
$profileImage     = $profile->getAvatar();
$coverImage       = $profile->getCover();
$showProfileImage = false;
$showCertificates = false;
$showBadges 	  = false;
$showEvents 	  = false;
$showCourses 	  = false;
$cover 			  = null;
$certificatesText = AxsLanguage::text("AXS_CERTIFICATES", "Certificates");
$certificatesIcon = 'lizicon-newspaper';
$badgesText 	  = AxsLanguage::text("AXS_BADGES", "Badges");
$badgesIcon  	  = 'lizicon-trophy1';
$coursesText 	  = AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses");
$coursesIcon 	  = 'lizicon-school';
$eventsText		  = AxsLanguage::text("AXS_EVENTS", "Events");
$eventsIcon 	  = 'lizicon-calendar';

$userPoints = AxsLearnerDashboard::getLeaderBoardPointsForUser($transcript_user_id);

$encryptedTranscriptLink = AxsLMS::getEncryptedTranscriptLink($transcript_user_id);

if($settings->transcripts_show_profile_cover || !isset($settings->transcripts_show_profile_cover)) {
	$cover = '/'.$settings->transcripts_cover_default;
	if ($settings->transcripts_show_profile_cover == 1) {
		$cover = $coverImage;
	}
}

if($settings->transcripts_show_profile_picture || !isset($settings->transcripts_show_profile_picture)) {
	$showProfileImage = true;
}

if($settings->transcripts_show_badges || !isset($settings->transcripts_show_badges)) {
	$showBadges = true;
	if($settings->transcript_badges_text && $settings->transcripts_show_badges == 'custom') {
		$badgesText = $settings->transcript_badges_text;
	}
	if(($settings->transcript_badges_icon && $settings->transcript_badges_icon != 'null') && $settings->transcripts_show_badges == 'custom') {
		$badgesIcon = $settings->transcript_badges_icon;
	}
}

if($settings->transcripts_show_certificates || !isset($settings->transcripts_show_certificates)) {
	$showCertificates = true;
	if($settings->transcript_certificates_text && $settings->transcripts_show_certificates == 'custom') {
		$certificatesText = $settings->transcript_certificates_text;
	}
	if(($settings->transcript_certificates_icon && $settings->transcript_certificates_icon != 'null') && $settings->transcripts_show_certificates == 'custom') {
		$certificatesIcon = $settings->transcript_certificates_icon;
	}
}

if($settings->transcripts_show_courses || !isset($settings->transcripts_show_courses)) {
	$showCourses = true;
	if($settings->transcript_courses_text && $settings->transcripts_show_courses == 'custom') {
		$coursesText = $settings->transcript_courses_text;
	}
	if(($settings->transcript_courses_icon && $settings->transcript_courses_icon != 'null') && $settings->transcripts_show_courses == 'custom') {
		$coursesIcon = $settings->transcript_courses_icon;
	}

	$options = array(
		'show_full_course_data' => (bool)$settings->transcripts_full_course_data,
		'collapse' => (bool)$settings->transcripts_collapse,
	);
	$courses = AxsTranscripts::renderCourses($transcript_user_id, $options);
}

if($settings->transcripts_show_events || !isset($settings->transcripts_show_events)) {
	$showEvents = true;
	if($settings->transcript_events_text && $settings->transcripts_show_events == 'custom') {
		$eventsText = $settings->transcript_events_text;
	}
	if( ($settings->transcript_events_icon && $settings->transcript_events_icon != 'null')  && $settings->transcripts_show_events == 'custom') {
		$eventsIcon = $settings->transcript_events_icon;
	}
	$events = AxsTranscripts::renderEvents($transcript_user_id);
}

?>
<style>
@media print {
    <?php echo AxsTranscripts::getPrinterFriendlyStyleRules(); ?>
}
</style>

<div id="pdf_render_area">
	<div class="container-fluid" style="max-width: 1300px;">
		<div class="row prevent-split">
			<div class="col-md-12">
				<div class="col-md-12 box cover-image" style="background-image: url('<?php echo $cover; ?>');">
					<div class="cover-overlay">
						<?php if($showProfileImage) { ?>
						<div class="transcript-profile-img">
                            <img src="<?php echo $profileImage; ?>" alt="profile picture">
                        </div>
                    	<?php } ?>
						<div class="pull-left">
							<span class="transcript-name"><?php echo $profile->name; ?></span>
							<?php if($settings->show_user_points) { ?>
							<div style="display: inline-block; position: relative; margin-left: 10px;">
								<span style="vertical-align: text-bottom;" class="tovuti-user-points left-arrow"><?php echo AxsLanguage::text("AXS_YOUR_POINTS", "Your Points") ?>: <?php echo $userPoints; ?></span>
							</div>
							<?php } ?>
						</div>

						<div id="transcriptButtons" class="pull-right">
							<?php if(!$isOnMobileDevice) : ?>
							<button id="downloadTranscriptButton" class="btn btn-primary">
								<span class="fa fa-download"></span> <?php echo AxsLanguage::text("AXS_TRANS_DOWNLOAD_TRANSCRIPT", "Download Transcript") ?>
							</button>
							<?php endif; ?>

							<button data-toggle="modal" data-target="#copyTranscriptLinkModal" class="btn btn-primary">
								<span class="fa fa-share-alt"></span> <?php echo AxsLanguage::text("AXS_SHARE", "Share") ?>
							</button>

						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="badges-certs-container">
			<?php if($badges && $showBadges) { ?>
			<div class="row prevent-split transcript-badges">
				<div class="col-md-12">
					<div class="tov-trans-card-header">
						<span class="transcript-icon <?php echo $badgesIcon; ?>"></span> <?php echo $badgesText; ?>
					</div>
					<div class="tov-trans-card-body">
						<div class="tov-trans-card-course">
							<?php echo $badges;?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>

			<?php if($certificates && $showCertificates) { ?>
			<div class="row prevent-split transcript-certs">
				<div class="col-md-12">
					<div class="tov-trans-card-header">
						<span class="transcript-icon <?php echo $certificatesIcon; ?>"></span>
						<?php echo $certificatesText; ?>
					</div>
					<div class="tov-trans-card-body">
						<div class="tov-trans-card-course">
							<?php echo $certificates;?>
						</div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php if($showEvents) { ?>
		<div class="row prevent-split">
			<div class="col-md-12">
				<div class="tov-trans-card-header">
					<span class="transcript-icon <?php echo $eventsIcon; ?>"></span> <?php echo $eventsText; ?>
				</div>
				<div class="tov-trans-card-body">
					<?php echo $events;?>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($showCourses) { ?>
		<div class="row prevent-split">
			<div class="col-md-12">
				<div class="tov-trans-card-header">
					<i class="<?php echo $coursesIcon ?> tov-trans-me-3"></i><?php echo $coursesText; ?>
					<?php if ((bool)$settings->transcripts_collapse == true && (bool)$settings->transcripts_full_course_data == true): ?>
						<span id="toggleCollapseAllButton" data-toggle="tooltip" data-placement="top" title="Expand All" class="pull-right" style="cursor:pointer">
							<span class="fa fa-angle-double-down"></span>
							<span class="fa fa-angle-double-up" style="display:none"></span>
						</span>
					<?php endif; ?>
				</div>
				<div class="tov-trans-card-body">
					<?php echo $courses;?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>

<div id="copyTranscriptLinkModal" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document" style="margin-top:150px">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title"><?php echo AxsLanguage::text("AXS_TRANS_COPY_TRANSCRIPT_LINK", "Copy Transcript Link") ?></h4>
			</div>
			<div class="modal-body">
				<div>
					<?php echo AxsLanguage::text("AXS_COPY_ENCRYPTED_TRANSCRIPT_LINK", "Copy Your Encrypted Transcript Link Below") ?>
				</div>
				<div class="copy-message"></div>
				<div class="input-group">
					<input
						id="encryptedTranscriptLink"
						value="<?php echo $encryptedTranscriptLink; ?>"
						aria-label="Transcript Link"
						class="form-control"
						type="text"
					>
					<span class="input-group-addon encryptedTranscriptLink-button">
						<i class="lizicon-shield"></i>  <?php echo AxsLanguage::text("AXS_COPY", "Copy") ?>
					</span>	
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo AxsLanguage::text("AXS_CLOSE", "Close") ?></button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script>
var member_badge_container_height_original 		 = $('#member_badge_container').height();
var member_certificate_container_height_original = $('#member_certificate_container').height();

function setImagesContainerHeight() {
	var width = $('.badges-certs-container').width();
	var widthPercent = width / 1184;
	if(widthPercent < 0 ) {
		widthPercent = 1;
	}
	var member_badge_container_height 		= $('#member_badge_container').height();
	var member_certificate_container_height = $('#member_certificate_container').height();
	$('#member_badge_container').height(member_badge_container_height / widthPercent);
	$('#member_certificate_container').height(member_certificate_container_height / widthPercent);
}

function downloadPDF() {
	setImagesContainerHeight();
	$("<style id='stylesForPrint'>")
		.prop("type", "text/css")
		.html(`<?php echo AxsTranscripts::getPrinterFriendlyStyleRules(); ?>`)
		.appendTo("head");

    kendo.drawing.drawDOM($("#pdf_render_area"), {
        avoidLinks: true,
		paperSize: "Letter",
		multiPage: true,
		//forcePageBreak: '.transcript-certs',
		margin: {
			left: 30,
			top: 15,
			right: 30,
			bottom: 15
		},
		scale: .55
    }).then(function(group) {
		$('#member_badge_container').height(member_badge_container_height_original);
		$('#member_certificate_container').height(member_certificate_container_height_original);
		$('#stylesForPrint').remove();
		return kendo.drawing.exportPDF(group);
	}).done(function(data) {
		kendo.saveAs({
			dataURI: data,
			fileName: `<?php echo JFactory::getDocument()->getTitle() ?>`
		});
	});
}

$('#downloadTranscriptButton').click(function(){
	downloadPDF();
});

$("#encryptedTranscriptLink").click(function() {
	this.select();
	document.execCommand('copy');
	$('.copy-message').text("<?php echo AxsLanguage::text("AXS_LINK_COPIED", "Link Copied") ?>!")
});

$(".encryptedTranscriptLink-button").click(function() {
	$("#encryptedTranscriptLink").select();
	document.execCommand('copy');
	$('.copy-message').text("<?php echo AxsLanguage::text("AXS_LINK_COPIED", "Link Copied") ?>!")
});

$("#shareTranscriptButton").click(function() {
	var right = $(".share-transcript").css('right');

	if(right == '0px') {

		if(window.screen.availWidth <= 500) {
			$(".share-transcript").css('right','-100%');
		} else {
			$(".share-transcript").css('right','-310px');
		}
	} else {
		$(".share-transcript").css('right','0px');
	}
});

var expanded = false;
$("#toggleCollapseAllButton").click(function() {
	let self = $(this);
	let upicon = self.find('.fa-angle-double-up');
	let downicon = self.find('.fa-angle-double-down');
	$(".tov-trans-course-expand-btn").each(function(idx, value) {
		let self = $(value);
		updateExpandButton(self, expanded);
	});
	if (expanded) {
		$('.collapse').collapse('hide');
		expanded = false;
		upicon.hide();
		downicon.show();
		self.attr('data-original-title', "<?php echo AxsLanguage::text("AXS_EXPAND_ALL", "Expand All") ?>")
	} else {
		$('.collapse').collapse('show');
		expanded = true;
		downicon.hide();
		upicon.show();
		self.attr('data-original-title', "<?php echo AxsLanguage::text("AXS_COLLAPSE_ALL", "Collapse All") ?>")
	}
});

$(function () {
  	$('[data-toggle="tooltip"]').tooltip()
})

function updateExpandButton(self, expanded_local) {
	let upicon = self.find('.fa-chevron-square-up');
	let downicon = self.find('.fa-chevron-square-down');
	if (!expanded_local) {
		buttonsExpanded[self.attr('id')] = true;
		upicon.show();
		downicon.hide();
		self.html(self.html().replace('expand', 'collapse'));
	} else {
		buttonsExpanded[self.attr('id')] = false;
		upicon.hide();
		downicon.show();
		self.html(self.html().replace('collapse', 'expand'));
	}
}

var buttonsExpanded = {};
$(".tov-trans-course-expand-btn").click(function() {
	let self = $(this);
	updateExpandButton(self,  buttonsExpanded[self.attr('id')]);
});
</script>