<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;
$document = JFactory::getDocument();

$title = $document->getTitle();
if(!$title) {
	$document->setTitle(AxsLanguage::text("COM_SPLMS_AXS_CATEGORIES", "Categories"));
}

$model = $this->getModel(); ?>
<link rel="stylesheet" type="text/css" href="components/shared/includes/css/blocksit.css?v=2">
<script type="text/javascript" src="components/shared/includes/js/blocksit.js?v=2"></script>
<link href="/templates/axs/elements/lms/courses-list/css/normalize.css" rel="stylesheet" type="text/css">
<link href="/templates/axs/elements/lms/courses-list/css/components.css?v=31" rel="stylesheet" type="text/css">
<link href="/templates/axs/elements/lms/courses-list/css/tovuti-course-list.css?v=41" rel="stylesheet" type="text/css">
<style>
	body {
		background-color: #eee;
	}
	.course-titlebox h2 {
		font-size: 15px;
	}
	.course-titlebox {
		position: relative;
		min-height: 48px;
	}
	.category_container {
		overflow: hidden; 
		background-color: white; 
		border-radius: 2px; 
		box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
	}
	.course-card-img {
    	border-radius: 0px;
	}
	.course-card-view-button {
		margin-bottom: 10px;
	}
	.course-card-title {
    	width: 100%;
	}
</style>

<h1>
	<?php echo JText::_('COM_SPLMS_AXS_COURSE_COURSE_CATEGORIES'); ?>
</h1>
<div id="block-container">
	<?php

		$language = AxsLanguage::getCurrentLanguage();
		$db = JFactory::getDBO();
		$query = "SELECT sef FROM joom_languages WHERE lang_code = '" . $language->get('tag') . "'";
		$db->setQuery($query);
		//$sef = $db->loadObject()->sef;
		$result = $db->loadObject();

		if ($result) {
			$sef = $result->sef;
		} else {
			$sef = null;
		}

		foreach ($this->items as $item) {
			$courses = $model->getCoursesByCategory($item->splms_coursescategory_id);
			$count = count($courses);
			$link = JRoute::_('index.php?option=com_splms&view=courses&cat=' . $item->slug);

			$params = json_decode($item->params);
			if ($params && $sef) {
				$overrides = $params->override->$sef;
				if ($overrides->use == "1") {
					if ($overrides->title) {
						$item->title = $overrides->title;
					}

					if ($overrides->description) {
						$item->description = $overrides->description;
					}
				}
			}

	?>
			<div class="block-grid category_container" link="<?php echo $link; ?>">

				<?php
					if ($item->image) {
				?>
				<a href="<?php echo $link; ?>">
					<div class="course-card-img" style="background-image: url('/<?php echo $item->image; ?>');"></div>
				</a>
						
				<?php
					}
				?>
			
				<div style="padding-left: 20px; padding-right: 20px;">
					<div class="course-titlebox">						
						<h3 class="course-card-title">
						<a href="<?php echo $link; ?>">
							<?php echo $item->title; ?>
						</a>
						</h3>
					</div>
					<p>
						<?php echo $item->description; ?>						
					</p>
					<?php if(!empty($item->children)) { ?>
					<select
						class="subCat search-form-select w-select"
					>
						<option value="">
							<?php echo AxsLanguage::text("AXS_SELECT_SUBCATEGORY", "Select Subcategory") ?>
						</option>
						
						<?php foreach($item->children as $cat_child) { ?>
							<option class="subItem" value="<?php echo JRoute::_('index.php?option=com_splms&view=courses&cat=' . $cat_child->slug); ?>">
								<?php echo $cat_child->title; ?>
							</option>
						<?php }	?>						
					</select>
					<?php } ?>
					<div class="course-card-details-row">
						<div class="course-card-icon card-icon-dark"></div>
						<div class="course-card-details-text"><?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES', 'Courses'); ?></div>
						<div class="course-card-details-count"><?php echo $count; ?></div>
					</div>
					<a class="course-card-view-button w-inline-block" href="<?php echo $link; ?>">
						<h5 class="tovuti-course-card-view-button-h5">
							<?php echo AxsLanguage::text("COM_SPLMS_VIEW_COURSES", "View Courses") ?>
						</h5>
					</a>
				</div>
			</div>
	<?php 
		} 
	?>
</div>

<script>
	(function($) {
		$(function() {
			var container = $('#block-container');
			//initial call to set up blocksit.
			refreshBlocksit();

			function get_num_columns() {
				var winWidth = container.width();
				var columns = Math.floor(winWidth / 350);
				if (columns < 1) {
					columns = 1;
				}
				return columns;
			}

			function refreshBlocksit() {
				var columns = get_num_columns();

				container.BlocksIt({
					numOfCol: columns,
					offsetX: 15,
					offsetY: 15,
					blockElement: '.block-grid'
				});
			}

			$(window).resize(refreshBlocksit);

			$('.subCat').change(function() {
				var url = $(this).val();
				if(url) {
					window.location.href = url;
				}				
			});

		});
	})(jQuery);
</script>
