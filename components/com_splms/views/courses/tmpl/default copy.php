<?php
// No direct access
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$document = JFactory::getDocument();
$document->setTitle(AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses"));
$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if ($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if ($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}
require_once "components/shared/controllers/rating.php";

$model = $this->getModel();
$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');
$settings = json_decode(AxsLMS::getSettings()->params);

$userId = JFactory::getUser()->id;
$categoryId = $model->catData->splms_coursescategory_id;
$visibilityAccess = AxsContentAccess::checkCategoryAccess($userId, $categoryId);
$coverImage = '';

if (!$visibilityAccess && $model->catData->splms_coursescategory_id) {
	echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_CATEGORY_NO_ACCESS", "You don't have access to this category");
} else {
?>

	<link href="/templates/axs/elements/lms/courses-list/css/normalize.css" rel="stylesheet" type="text/css">
	<link href="/templates/axs/elements/lms/courses-list/css/components.css?v=3" rel="stylesheet" type="text/css">
	<link href="/templates/axs/elements/lms/courses-list/css/tovuti-course-list.css?v=39" rel="stylesheet" type="text/css">
	<script type="text/javascript">
		! function(o, c) {
			var n = c.documentElement,
				t = " w-mod-";
			n.className += t + "js", ("ontouchstart" in o || o.DocumentTouch && c instanceof DocumentTouch) && (n.className += t + "touch")
		}(window, document);
	</script>
	<div class="relative-container">
		<div class="courses-hero-header">
			<h1 class="course-hero-header-h1"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses") ?></h1>
		</div>
		<div class="course-listing-container">
			<div class="course-listing-search">
				<div class="tovuti-course-archive-search-form w-form">
					<form id="searchForm" class="tovuti-search">
						<select id="filter" name="filter" class="search-form-select w-select">
							<option value="block-grid"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_VIEW_ALL', 'View All'); ?> </option>
							<option value="purchased"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_PURCHASED', 'Purchased'); ?> </option>
							<option value="wishlisted"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_BOOKMARKED', 'Bookmarked') ?> </option>
							<option value="recommended"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_RECOMMENDED', 'Recommended'); ?> </option>
							<option value="assigned"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_ASSIGNED', 'Assigned'); ?> </option>
							<option value="inProgress"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_IN_PROGRESS', 'In Progress'); ?> </option>
							<option value="completed"> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_COMPLETED', 'Completed'); ?> </option>
						</select><select id="cat" name="cat" class="search-form-select w-select">
							<option value="0">
								<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_SELECT_CATEGORY', 'select a category'); ?>
							</option>
							<?php
							$cats = $model->getAllCategories();
							foreach ($cats as $cat) {
							?>
								<option value="<?php echo $cat->splms_coursescategory_id; ?>" <?php
																								if ($model->catData && $cat->splms_coursescategory_id == $model->catData->splms_coursescategory_id) {
																									echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected");
																								}
																								?>>
									<?php echo $cat->title; ?>
								</option>
							<?php
							}
							?>
						</select><input type="text" class="search-form-field w-input" maxlength="256" id="title" name="title" value="<?php echo $model->search->title; ?>" placeholder="<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_LIST_SEARCH', 'Search'); ?>..." /><input type="submit" value="<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_LIST_SEARCH', 'Search'); ?>" class="search-form-button search-form-button-submit w-button" />
						<a class="search-form-button search-form-button-shopping w-button" href="<?php echo JRoute::_('index.php?option=com_splms&view=cart'); ?>">
							<?php echo AxsLanguage::text('COM_SPLMS_CART', 'Cart') ?>
						</a>
					</form>
				</div>
			</div>
			<?php
			if (count($this->items)) {
				$user_courses = AxsLMS::getUserAllCourses($userId);
			?>
				<?php

				$now = strtotime('now');

				foreach ($this->items as $item) {

					$model->setOverrides($item);	//Set the multilingual overrides

					$params = json_decode($item->params);

					if ($params->external_link) {
						$link = $params->external_link;
						$target = '_blank';
					} else {
						$link = JRoute::_('index.php?option=com_splms&view=course') . $item->slug;
						$target = '';
					}

					$lessons = $lessons_model->getLessons($item->splms_course_id);
					$teachers = $model->getCourseTeachers($item->splms_course_id);
					$course_user_data = $user_courses[$item->splms_course_id];
					$wishlisted = $course_user_data[AxsLMS::$userCourseStatuses['Wishlisted']];
					$recommended = $course_user_data[AxsLMS::$userCourseStatuses['Recommended']];
					$assigned = $course_user_data[AxsLMS::$userCourseStatuses['Assigned']];
					$purchased = $course_user_data[AxsLMS::$userCourseStatuses['Purchased']];
					$inProgress = $course_user_data[AxsLMS::$userCourseStatuses['In Progress']];
					$completed = $course_user_data[AxsLMS::$userCourseStatuses['Completed']];
					$showTopBar = $recommended || $assigned || $purchased || $inProgress || $completed;

					$open_date = $params->course_open_date;
					$visible_before_open = $params->course_visible_before_open;
					$close_date = $params->course_close_date;
					$visible_after_close = $params->course_visible_after_close;

					if (!$item->image) {
						$item->image = "https://tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg";
					}


					//Check if the course is currently available
					$course_open = true;
					$course_visible = false;

					if ($open_date) {
						if (strtotime($open_date) > $now) {
							$course_open = "not open";
							if ($visible_before_open) {
								$course_visible = true;
							}
						}
					}

					if ($close_date) {
						if (strtotime($close_date) < $now) {
							$course_open = "closed";
							if ($visible_after_close) {
								$course_visible = true;
							}
						}
					}

					if ($course_open !== true && !$course_visible) {
						continue;
					}

				?>
					<?php include("components/com_splms/templates/card_view_1.php"); ?>
			<?php
				} //end of foreach loop of courses
			} //end of check if there are any courses
			?>
		</div>
	</div>
	<?php if ($model->catData) { //display the category header and description 
		if ($model->catData->image) {
			$coverImage = $model->catData->image;
		} elseif (!$model->catData->image && $brand->default_settings->lms_category_cover_image) {
			$coverImage = $brand->default_settings->lms_category_cover_image;
		}
	?>
		<div class="cat-card">
			<div class="cat-titlebar">
				<?php if ($coverImage) { ?>
					<img src="<?php echo $coverImage; ?>" class="cat-titleimg" <?php echo AxsAccessibility::image($coverImage); ?> />
				<?php } ?>
				<div class="cat-box">
					<h2><?php echo $model->catData->title; ?></h2>
				</div>
			</div>

		</div>
	<?php } else { //display the courses header
		if ($brand->default_settings->lms_category_cover_image) {
			$coverImage = $brand->default_settings->lms_category_cover_image;
		} else {
			$coverImage = '';
		}
	?>
		<div class="cat-card">
			<div class="cat-titlebar">
				<?php if ($coverImage) { ?>
					<img src="/<?php echo $coverImage; ?>" class="cat-titleimg" <?php echo AxsAccessibility::image($coverImage); ?> />
				<?php } ?>
				<div class="cat-box">
					<h2>
						<?php
						if ($model->viewType) {
							echo AxsLanguage::text('COM_SPLMS_AXS_MY_COURSES', 'My Courses');
						} else {
							echo AxsLanguage::text('COM_SPLMS_AXS_COURSES', 'Courses');
						}
						?>

					</h2>
				</div>
			</div>
		</div>
	<?php } ?>



	<?php if (count($this->items)) {
		$user_courses = AxsLMS::getUserAllCourses($userId);
	?>
		<div id="block-container" style="margin-top: 10px; margin-bottom: 20px; display:none;">
			<?php

			$now = strtotime('now');

			foreach ($this->items as $item) {

				$model->setOverrides($item);	//Set the multilingual overrides

				$params = json_decode($item->params);

				if ($params->external_link) {
					$link = $params->external_link;
					$target = '_blank';
				} else {
					$link = JRoute::_('index.php?option=com_splms&view=course') . $item->slug;
					$target = '';
				}

				$lessons = $lessons_model->getLessons($item->splms_course_id);
				$teachers = $model->getCourseTeachers($item->splms_course_id);
				$course_user_data = $user_courses[$item->splms_course_id];
				$wishlisted = $course_user_data[AxsLMS::$userCourseStatuses['Wishlisted']];
				$recommended = $course_user_data[AxsLMS::$userCourseStatuses['Recommended']];
				$assigned = $course_user_data[AxsLMS::$userCourseStatuses['Assigned']];
				$purchased = $course_user_data[AxsLMS::$userCourseStatuses['Purchased']];
				$inProgress = $course_user_data[AxsLMS::$userCourseStatuses['In Progress']];
				$completed = $course_user_data[AxsLMS::$userCourseStatuses['Completed']];
				$showTopBar = $recommended || $assigned || $purchased || $inProgress || $completed;

				$open_date = $params->course_open_date;
				$visible_before_open = $params->course_visible_before_open;
				$close_date = $params->course_close_date;
				$visible_after_close = $params->course_visible_after_close;

				if (!$item->image) {
					$item->image = "https://tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg";
				}


				//Check if the course is currently available
				$course_open = true;
				$course_visible = false;

				if ($open_date) {
					if (strtotime($open_date) > $now) {
						$course_open = "not open";
						if ($visible_before_open) {
							$course_visible = true;
						}
					}
				}

				if ($close_date) {
					if (strtotime($close_date) < $now) {
						$course_open = "closed";
						if ($visible_after_close) {
							$course_visible = true;
						}
					}
				}

				if ($course_open !== true && !$course_visible) {
					continue;
				}

			?>
				<div class="block-grid course-card 
					<?php
					if ($recommended) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_RECOMMENDED", "Recommended");
					}
					if ($wishlisted) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_WISHLISTED", "Wishlisted");
					}
					if ($assigned) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_ASSIGNED", "Assigned");
					}
					if ($completed) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_COMPLETED", "Completed");
					}
					if ($inProgress) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_IN_PROGRESS", "In Progress");
					}
					if ($purchased) {
						echo " " . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_PURCHASED", "Purchased");
					}
					?>">

					<style>
						.video-overlay {
							cursor: pointer;
							position: absolute;
							top: 0;
							right: 0;
							width: 100%;
							height: 100%;
							display: flex;
							justify-content: center;
							align-items: center;
							font-size: 50px;
							color: white;
							text-shadow: 0 2px 2px rgba(0, 0, 0, 0.2);
						}

						.course {
							padding-left: 20px;
							padding-right: 20px;
							padding-bottom: 20px;
						}
					</style>

					<div class="course-topbar">
						<?php
						//if the user is logged in, let them wishlist the course, otherwise don't display
						if ($userId) {
							if ($wishlisted) { ?>
								<div class="course-wishlist">
									<span data-wishlisted="1" data-course-id="<?php echo $item->splms_course_id; ?>" title="<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_WISHLIST_HOVER_TITLE', 'Wishlist'); ?>" class="wishlist fa fa-heart" style="color: #ff4349; cursor: pointer;" data-toggle="tooltip"></span>
								</div>
							<?php
							} else {
							?>
								<div class="course-wishlist">
									<span data-wishlisted="0" data-course-id="<?php echo $item->splms_course_id; ?>" title="<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_WISHLIST_HOVER_TITLE', 'Wishlist'); ?>" class="wishlist fa fa-heart-o" style="color: white; cursor: pointer;" data-toggle="tooltip"></span>
								</div>
							<?php
							}
						}

						if ($showTopBar) {
							?>
							<div class="topbar-wrap">
								<?php
								$topBarHTML = '';
								if ($assigned) {
									$due_date = '';
									$now = new DateTime();

									if ($assigned->due_date != '0000-00-00 00:00:00') {
										$due = new DateTime($assigned->due_date);
										$diff = $now->diff($due);
										$days = (int)$diff->format('%R%a');
										$due_date = $days . " ";
										if ($days >= 0) {
											if ($days == 1) {
												$due_date .= AxsLanguage::text('COM_SPLMS_AXS_COURSE_DAY_LEFT', 'day left');
											} else {
												$due_date .= AxsLanguage::text('COM_SPLMS_AXS_COURSE_DAYS_LEFT', 'days left');
											}
										} else {
											$days = $days * -1;

											if ($days == 1) {
												$due_date .= AxsLanguage::text('COM_SPLMS_AXS_COURSE_DAY_PAST_DUE', 'day past due');
											} else {
												$due_date .= AxsLanguage::text('COM_SPLMS_AXS_COURSE_DAYS_PAST_DUE', 'days past due');
											}
										}
									}

									$topBarHTML = '<span class="fa fa-calendar-check-o"></span> ' . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_ASSIGNED", "Assigned") . ' ';
								} else if ($recommended) {
									$topBarHTML = '<span style="color: gold;" class="fa fa-star"></span> ' . AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_RECOMMENDED', 'Recommended');
								}

								if ($purchased) {
									$topBarHTML .= ($topBarHTML ? ' - ' : '') . AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_PURCHASED', 'Purchased');
								}

								if ($completed) {
									$topBarHTML .= ($topBarHTML ? ' - ' : '') . '<span style="color: gold;" class="fa fa-trophy"></span> ' . AxsLanguage::text('COM_SPLMS_AXS_COURSE_100_COMPLETE', '100% Complete!');
								} else if ($inProgress) {
									$topBarHTML .= ($topBarHTML ? ' - ' : '') . '<span class="fa fa-clock-o"></span> ' . AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_IN_PROGRESS", "In Progress") . ' ' . $inProgress->percentComplete . '%';
								}

								echo $topBarHTML;
								?>
							</div>
						<?php
						}
						?>
					</div>

					<?php
					$video = false; //AxsLMS::getVideo($item);
					if (!$video) {
					?>
						<div style="height: 225px; overflow-y: hidden; position: relative;">
							<?php if (!$params->locked || $purchased) { ?>
								<a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
								<?php } ?>
								<img src="<?php echo $item->image; ?>" style="width: 100%; min-height: 225px;" <?php echo AxsAccessibility::image($item->image); ?> />
								<?php if (!$params->locked || $purchased) { ?>
								</a>
							<?php } ?>
							<?php if ($settings->show_ratings || !isset($settings->show_ratings)) { ?>
								<div class="pull-right" style="position: absolute; z-index: 10; bottom: 5px; left: 5px;">
									<?php display_ratings("course_" . $item->splms_course_id); ?>
								</div>
							<?php } ?>
						</div>
					<?php
					} else {
					?>
						<div style="position: relative;">
							<?php
							echo $video;
							?>
							<div class="video-overlay hidden-xs hidden-sm" data-toggle="0">
								<p class="video-enlarge"><span class="lizicon lizicon-enlarge2"></span></p>
							</div>
						</div>
					<?php
					}
					?>

					<div class="course">
						<div class="course-titlebox">
							<h2 style="margin-top: 10px;"><?php echo $item->title; ?></h2>
							<p><?php echo $item->short_description; ?></p>
						</div>

						<div style="display: flex; justify-content: center; flex-wrap: wrap;">
							<table class="table">
								<?php if ($settings->show_teachers_count) { ?>
									<tr>
										<td><span class="lizicon lizicon-users"></span><?php echo " " . AxsLanguage::text('COM_SPLMS_AXS_COURSE_TEACHERS', 'Teachers'); ?></td>
										<td><?php echo count($teachers); ?></td>
									</tr>
								<?php } ?>
								<?php if ($settings->show_lessons_count) { ?>
									<tr>
										<td><span class="lizicon lizicon-books"></span><?php echo " " . AxsLanguage::text('COM_SPLMS_AXS_COURSE_LESSONS', 'Lessons'); ?></td>
										<td><?php echo count($lessons); ?></td>
									</tr>
								<?php } ?>
								<?php if ($settings->show_pricing) { ?>
									<tr>

										<?php
										$owned = false;
										$given = false;

										if ($purchased) {
											$owned = true;
										} else {
											if ($assigned && $assigned->free) {
												$title = AxsLanguage::text('COM_SPLMS_AXS_ASSIGNED_COURSE', 'Assigned Course');
												$given = true;
											} else if ($recommended && $recommended->free) {
												$title = AxsLanguage::text('COM_SPLMS_AXS_RECOMMENDED_COURSE', 'Recommended Course');
												$given = true;
											}
										}

										ob_start();
										?>

										<td>
											<span class="lizicon lizicon-price-tag"></span><?php echo " " . AxsLanguage::text('COM_SPLMS_AXS_COURSE_PRICE', 'Price'); ?>
										</td>
										<?php
										$tableData = ob_get_clean();

										if ($owned) {
											echo $tableData;
										?>
											<td>
												<span><?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_PURCHASED_STATUS', 'PURCHASED'); ?></span>
											</td>
											<?php
										} else if ($course_open == "1") {
											if ($given) {
												echo $tableData;
											?>
												<td>
													<span data-toggle="tooltip" title="<?php echo $title; ?>"><span style="text-decoration: line-through;"><?php echo $currency_symbol . $item->price; ?></span><?php echo " " . AxsLanguage::text('COM_SPLMS_AXS_COURSE_FREE_STATUS', 'FREE'); ?></span>
												</td>
											<?php
											} else {
												echo $tableData;
											?>
												<td>
													<?php
													if ($item->price != '0.00') {
														echo $currency_symbol . $item->price;
													} else {
														echo AxsLanguage::text("COM_SPLMS_FREE", "Free");
													}
													?>
												</td>
											<?php
											}
										} else {
											?>
											<td colspan="2">
												<span><?php
														if ($course_open == "not open") {
															echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_NOT_OPEN_YET', 'This course is not available until ') . $open_date;
														} else if ($course_open == "closed") {
															echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_CLOSED', 'This course has closed');
														} else {
															echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_NOT_AVAILABLE', 'The selected course is not available');
														}

														?></span>
											</td>
										<?php
										}


										?>
									</tr>
								<?php } ?>
							</table>
							<?php if (!$params->locked || $purchased) { ?>
								<a class="btn btn-primary" href="<?php echo $link; ?>" target="<?php echo $target; ?>">
									<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_VIEW_COURSE_BUTTON', 'View Course'); ?>
								</a>
							<?php } else { ?>
								<button class="btn btn-primary" disabled>
									<span class="fa fa-lock"></span> <?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSE_VIEW_COURSE_BUTTON_LOCKED', 'Locked'); ?>
								</button>
							<?php } ?>
						</div>
					</div>
				</div>
			<?php
			}
			?>
		</div>

		<script>
			(function($) {
				$(function() {

					var text_wishlist_add_error = "<?php echo AxsLanguage::text('COM_SPLMS_AXS_WISHLIST_ERROR_ADDING', 'Error wishlisting this course'); ?>";
					var text_wishlist_remove_error = "<?php echo AxsLanguage::text('COM_SPLMS_AXS_WISHLIST_ERROR_REMOVE', 'Error removing course from wishlist.'); ?>";

					$('#filter').change(function() {
						var filter = $('#filter').val();
						$('.block-grid').hide();
						$('.' + filter).show();
						refreshBlocksit();
					});
					$('#block-container').show();
					var container = $('#block-container');
					//initial call to set up blocksit.
					refreshBlocksit();

					function get_num_columns() {
						var winWidth = container.width();
						var columns = Math.floor(winWidth / 350);
						if (columns < 1) {
							columns = 1;
						}
						return columns;
					}

					function refreshBlocksit() {
						var columns = get_num_columns();

						container.BlocksIt({
							numOfCol: columns,
							offsetX: 15,
							offsetY: 15,
							blockElement: '.block-grid'
						});
					}

					$(window).resize(refreshBlocksit);

					$('#searchForm').submit(function(ev) {
						ev.preventDefault();
						var link = '<?php echo JRoute::_('index.php?option=com_splms&view=courses&search='); ?>';
						var search = {
							'cat': this.cat.value,
							'title': this.title.value,
							'desc': this.desc.value
						};
						search = btoa(JSON.stringify(search));
						window.location.href = link + search;
					});

					$('.video-overlay').click(function() {
						var toggle = $(this).data('toggle');
						if (!toggle) {
							$(this).parent().parent().data('size', 2);
							$(this).data('toggle', 1);
							refreshBlocksit();
							$(this).animate({
								width: '50px',
								height: '50px',
								"font-size": '16px'
							}, 400, function() {
								$(this).find('.lizicon').removeClass().addClass('lizicon lizicon-shrink2');
							});
						} else {
							$(this).parent().parent().data('size', 1);
							$(this).data('toggle', 0);
							refreshBlocksit();
							$(this).animate({
								width: '100%',
								height: '100%',
								"font-size": '50px'
							}, 400, function() {
								$(this).find('.lizicon').removeClass().addClass('lizicon lizicon-enlarge2');
							});
						}
					});

					var wishlistOnHover = function() {
						$(this).removeClass('fa-heart-o').addClass('fa-heart');
					};

					var wishlistOffHover = function() {
						$(this).removeClass('fa-heart').addClass('fa-heart-o');
					};

					//for all courses that are not wishlisted, set the hover handler.
					$('.wishlist[data-wishlisted="0"]').hover(wishlistOnHover, wishlistOffHover);

					$('.wishlist').click(function() {
						if ($(this).data('wishlisted')) {
							//they are removing from their wishlist
							$(this).data('wishlisted', 0);
							$(this).hover(wishlistOnHover, wishlistOffHover);
							$(this).css('color', 'white');
							$.ajax({
								method: "POST",
								url: "/index.php?option=com_splms&task=courses.onWishlistRemove&format=raw",
								data: {
									course_id: $(this).data('course-id')
								}
							}).done(function(result) {
								if (result != 'success') {
									alert(text_wishlist_remove_error);
								}
							}).fail(function() {
								alert(text_wishlist_remove_error);
							});
						} else {
							//they are adding it to their wishlist
							$(this).data('wishlisted', 1);
							$(this).unbind('mouseenter', wishlistOnHover);
							$(this).unbind('mouseleave', wishlistOffHover);
							$(this).css('color', '#ff4349');
							$.ajax({
								method: "POST",
								url: "/index.php?option=com_splms&task=courses.onWishlistAdd&format=raw",
								data: {
									course_id: $(this).data('course-id')
								}
							}).done(function(result) {
								if (result != 'success') {
									alert(text_wishlist_add_error);
								}
							}).fail(function() {
								alert(text_wishlist_add_error);
							});
						}
					});

					//$('.has-tooltip').tooltip();
					$('[data-toggle="tooltip"]').tooltip();
				});
			})(jQuery);
		</script>

	<?php } else { ?>

		<div style="display: flex; justify-content: center; padding: 20px; margin-top: 20px; margin-bottom: 20px; background-color: white; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">
			<h2 style="margin: 0;">
				<?php echo AxsLanguage::text('COM_SPLMS_AXS_NO_COURSES_AVAILABLE', 'Sorry, no courses are available.'); ?>
			</h2>
		</div>

		<script>
			(function($) {
				$(function() {
					$('#searchForm').submit(function(ev) {
						ev.preventDefault();
						var link = '<?php echo JRoute::_("index.php?option=com_splms&view=courses&search="); ?>';
						var search = {
							'cat': this.cat.value,
							'title': this.title.value,
							'desc': this.desc.value
						};
						search = btoa(JSON.stringify(search));
						window.location.href = link + search;
					});

				});
			})(jQuery);
		</script>
	<?php
	} ?>
<?php
} // end of access else statement
?>
