<?php
// No direct access
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$document = JFactory::getDocument();
$title = $document->getTitle();
if(!$title) {
	$document->setTitle(AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses"));
}

$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}
require_once "components/shared/controllers/rating.php";

$model = $this->getModel();
$lessons_model = FOFModel::getTmpInstance('Lessons', 'SplmsModel');
$settings = json_decode(AxsLMS::getSettings()->params);
$totalPages = $model->total / $model->limit;
if($totalPages > 1) {
	$totalPages = ceil($totalPages);
} else {
	$totalPages = 0;
}

switch($settings->card_design) {
	case "default":
		$cardTemplate = "components/com_splms/templates/card_view_1.php";
	break;
	case "modern":
		$cardTemplate = "components/com_splms/templates/card_view_2.php";
	break;
	case "minimalistic":
		$cardTemplate = "components/com_splms/templates/card_view_3.php";
	break;
	default:
		$cardTemplate = "components/com_splms/templates/card_view_1.php";
	break;
}

$userId = JFactory::getUser()->id;
$categoryId = $model->catData->splms_coursescategory_id;
$visibilityAccess = AxsContentAccess::checkCategoryAccess($userId,$categoryId);

$coverImage = '/components/com_axs/views/login_page/images/tovuti-building.jpg';

$root_primary_color = "#1e2935";
$root_accent_color = "#e9882e";
$root_base_color = "#fff";

$useRootStyles = false;
if (!$brand->site_details->root_base_color) {
	$brand->site_details->root_base_color = "#fff";
}
if (
	$brand->site_details->tovuti_root_styles &&
	$brand->site_details->root_primary_color &&
	$brand->site_details->root_accent_color
) {
	$useRootStyles = true;
	$root_primary_color = $brand->site_details->root_primary_color;
	$root_accent_color = $brand->site_details->root_accent_color;
	$root_base_color = $brand->site_details->root_base_color;
}
list($r, $g, $b) = sscanf($root_primary_color, "#%02x%02x%02x");
list($ar, $ag, $ab) = sscanf($root_accent_color, "#%02x%02x%02x");
if(!$visibilityAccess && $model->catData->splms_coursescategory_id) {
	echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_CATEGORY_NO_ACCESS", "You don't have access to this category");
} else {

if ($model->catData) { //display the category header and description
	if($model->catData->image) {
		$coverImage = $model->catData->image;
	} elseif(!$model->catData->image && $brand->default_settings->lms_category_cover_image) {
		$coverImage = $brand->default_settings->lms_category_cover_image;
	}
} else { //display the courses header
	if ($brand->default_settings->lms_category_cover_image) {
		$coverImage = $brand->default_settings->lms_category_cover_image;
	}
}

if ($settings->course_carousel_enabled == "1" && count($this->items) > 1) { ?>
	<!-- add the slick jquery plugin for the carousel-->
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<?php
	// If there are less slides than the default
	// slides to show
	$slidesToShow = 4;
	if (count($this->items) <= 4) {
		// Adjust number of slides to show so the carousel
		// still gets activated. If the number of slides equals slides to show
		// slick wont activate the carousel...
		$slidesToShow = count($this->items) - 1;
	}
	$document->addScriptDeclaration(trim("
		jQuery(() => {
			jQuery('.course-listing-container')
				.slick({
					infinite: true,
					slidesToShow: $slidesToShow,
					slidesToScroll: 1,
					dots: true,
					nextArrow: `<button type='button' class='slick-next'>left</button>`,
					prevArrow: `<button type='button' class='slick-prev'>right</button>`,
					responsive:[
						{
							breakpoint: 1200,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
							}
						},
						{
							breakpoint: 576,
							settings: 'unslick'
						}
					]
				});
		});
	"));
	$document->addStyleDeclaration(trim("
		/* add margin so arrows can be visible */
		.course-listing-container {
			margin: 0px 50px;
		}
		/* the default setting of top:50% causes the prev/next arrows */
		/* to jump around on page resize, so we set it to a px value here */
		.slick-prev, .slick-next {
			top: 250px !important;
		}
		.slick-prev {
			left: -35px !important;
		}
		/* increase size of arrows and set their color to black */
		.slick-prev:before {
			color:black !important;
			font-size: 30px !important;
		}
		.slick-next:before {
			margin-right: 8px;
			color:black !important;
			font-size: 30px !important;
		}
	"));
	// Only modern and minimal course designs require
	// display:flex for their container div. Default uses display:block
	if ($settings->card_design == 'modern' || $settings->card_design == 'minimalistic') {
		$document->addStyleDeclaration(trim("
			/* elements inside course cards require flex for proper layout*/
			.slick-initialized .slick-slide {
				display: flex !important;
			}
		"));
	}
}
?>

<script src="https://cdn.rawgit.com/botmonster/jquery-bootpag/master/lib/jquery.bootpag.min.js"></script>
<script type="text/javascript">!function(o,c){var n=c.documentElement,t=" w-mod-";n.className+=t+"js",("ontouchstart"in o||o.DocumentTouch&&c instanceof DocumentTouch)&&(n.className+=t+"touch")}(window,document);</script>
<style>
	:root {
		/* Tovuti Platform Default Theme */
		--primary-color: <?php echo $root_primary_color; ?>;
		/* Tovuti Blue */
		--accent-color: <?php echo $root_accent_color; ?>;
		/* Tovuti Orange */
		--base-color: <?php echo $root_base_color; ?>;

	}
	.courses-hero-header {
		background-color: #1e2935;
		background-image: -webkit-gradient(linear, left top, left bottom, from(rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8)), to(rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8))), url('/<?php echo $coverImage; ?>');
		background-image: linear-gradient(180deg, rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8), rgba(<?php echo $r; ?>, <?php echo $g; ?>, <?php echo $b; ?>, 0.8)), url('/<?php echo $coverImage; ?>');
	}
    .parentItem {
        font-weight: bold;
    }

</style>
</style>

<?php
	if($r && $g && $b && $root_primary_color && $root_accent_color) {
		$cardLinearGradient = "linear-gradient(45deg, #1e2935 23%, rgba($r, $g, $b, 0.63) 59%, #1e2935 )";
	}
?>
<link href="/templates/axs/elements/lms/courses-list/css/normalize.css" rel="stylesheet" type="text/css">
<link href="/templates/axs/elements/lms/courses-list/css/components.css?v=31" rel="stylesheet" type="text/css">
<link href="/templates/axs/elements/lms/courses-list/css/tovuti-course-list.css?v=41" rel="stylesheet" type="text/css">
<div class="relative-container">
	  <div class="courses-hero-header">
		<h1 class="course-hero-header-h1"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES", "Courses") ?></h1>
	  </div>
	  <div class="course-listing-search">
		  <div class="tovuti-course-archive-search-form w-form">
			<form
			  id="searchForm"
			  class="tovuti-search"
			>
				<select
					id="filter"
					name="filter"
					class="search-form-select w-select"
					>
					<option value="">		<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_VIEW_ALL", "View All"); ?>	</option>
					<option value="assigned"
						<?php if ($model->search->filter == "assigned") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_ASSIGNED", "Assigned"); ?>
					</option>
					<option value="recommended"
						<?php if ($model->search->filter == "recommended") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_RECOMMENDED", "Recommended"); ?>
					</option>
					<option value="purchased"
						<?php if ($model->search->filter == "purchased") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_PURCHASED", "Purchased"); ?>
					</option>
					<option value="wishlisted"
						<?php if ($model->search->filter == "wishlisted") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_FILTER_BOOKMARKED", "Bookmarked") ?>
					</option>
					<option value="inProgress"
						<?php if ($model->search->filter == "inProgress") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_IN_PROGRESS', "In Progress"); ?>
					</option>
					<option value="completed"
						<?php if ($model->search->filter == "completed") {echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected"); } ?>>
						<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_COMPLETED', "Completed"); ?>
					</option>
				</select>
				<?php $languageCode = AxsLanguage::getCurrentLanguageCode(); ?>
			  	<select
				  	id="cat"
				  	name="cat"
					class="search-form-select w-select"
				>
					<option value="0">
						<?php echo AxsLanguage::text('COM_SPLMS_AXS_COURSES_FILTER_SELECT_CATEGORY', "select a category");?>
					</option>

					<?php
						$cats = $model->getAllCategories();
						foreach($cats as $cat) {
					?>
							<option class="parentItem" value="<?php echo $cat->splms_coursescategory_id; ?>"
								<?php
									if ($model->catData && $cat->splms_coursescategory_id == $model->catData->splms_coursescategory_id) {
										echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected");
									}
								?>
							>
								<?php
									$category_title = $cat->title;
									if($languageCode == "en" || $languageCode == "es" || $languageCode == "fr"){
										$lang_params = json_decode($cat->params);
										if(isset($lang_params->override->$languageCode->title) && $lang_params->override->$languageCode->title != ""){
											$category_title = $lang_params->override->$languageCode->title;
										}										
									}
									echo $category_title; 
								?>
							</option>
							<?php
								if(!empty($cat->children)) {
									foreach($cat->children as $cat_child) {
							?>
										<option class="subItem" value="<?php echo $cat_child->splms_coursescategory_id; ?>"
											<?php
												if ($model->catData && $cat_child->splms_coursescategory_id == $model->catData->splms_coursescategory_id) {
													echo AxsLanguage::text("AXS_SELECTED_LOWERCASE", "selected");
												}
											?>
										>
										&nbsp;--&nbsp;
										<?php
											$category_child_title = $cat_child->title;
											if($languageCode == "en" || $languageCode == "es" || $languageCode == "fr"){
												$child_lang_params = json_decode($cat_child->params);
												if(isset($child_lang_params->override->$languageCode->title) && $child_lang_params->override->$languageCode->title != ""){
													$category_child_title = $child_lang_params->override->$languageCode->title;
												}										
											}
										 	echo $category_child_title; 
										?>
										</option>
							<?php
									}
								}
							?>
					<?php
						}
					?>
				</select>
				<input
					type="text"
					class="search-form-field w-input"
					maxlength="256"
					id="search_term"
					name="search_term"
					value="<?php echo AxsSecurity::alphaNumericOnly($model->search->title); ?>"
					placeholder="<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_LIST_SEARCH", "Search"); ?>..."
			  	/>
				<input
					type="submit"
					value="<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSES_LIST_SEARCH", "Search");?>"
					class="search-form-button search-form-button-submit w-button"
			  	/>
				<a class="search-form-button search-form-button-shopping w-button" href="<?php echo JRoute::_('index.php?option=com_splms&view=cart'); ?>">
					<?php echo AxsLanguage::text("AXS_CART", "Cart") ?>
				</a>
			</form>
		</div>
	</div>
	<div class="course-listing-container">
		<?php
			$rendered_courses_count = 0;
			if (count($this->items)):
				$user_courses = AxsLMS::getUserAllCourses($userId);
				$now = strtotime('now');

				foreach($this->items as $item):

					// Set the multilingual overrides
					$model->setOverrides($item);

					// Provide a default image
					if(!$item->image) {
						$item->image = "https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg";
					}

					$lessons = $lessons_model->getLessons($item->splms_course_id);
					$teachers = $model->getCourseTeachers($item->splms_course_id);
					$course_user_data = $user_courses[$item->splms_course_id];
					$wishlisted = $course_user_data[AxsLMS::$userCourseStatuses['Wishlisted']];
					$recommended = $course_user_data[AxsLMS::$userCourseStatuses['Recommended']];
					$assigned = $course_user_data[AxsLMS::$userCourseStatuses['Assigned']];
					$purchased = $course_user_data[AxsLMS::$userCourseStatuses['Purchased']];
					$inProgress = $course_user_data[AxsLMS::$userCourseStatuses['In Progress']];
					$completed = $course_user_data[AxsLMS::$userCourseStatuses['Completed']];
					$showTopBar = $recommended || $assigned || $purchased || $inProgress || $completed;

					$params = json_decode($item->params);

					if($params->external_link) {
						$link = $params->external_link;
						$target = '_blank';
					} else {
						$link = JRoute::_('index.php?option=com_splms&view=course').$item->slug;
						$target = '';
					}

					$open_date = $params->course_open_date;
					$visible_before_open = $params->course_visible_before_open;
					$close_date = $params->course_close_date;
					$visible_after_close = $params->course_visible_after_close;

					// Check the course configuration for an open date
					// and course visibile after open setting
					$course_open = true;
					$course_visible = true;

					if ($open_date) {
						if (strtotime($open_date) > $now) {
							$course_open = "not open";
							if (!$visible_before_open) {
								$course_visible = false;
							}
						}
					}
					if ($close_date) {
						if (strtotime($close_date) < $now) {
							$course_open = "closed";
							if (!$visible_after_close) {
								$course_visible = false;
							}
						}
					}

					// Check if the course was assigned by a courseassignment, and
					// it is past the end_date don't show the course
					$not_past_course_assignment_end_date = $assigned->end_date == 0 || strtotime($assigned->end_date) >= $now;

					// Check if the course was assigned by a courseassignment, and
					// it is before the start date, don't show the course.
					$not_before_course_assignment_start_date = $assigned->start_date == 0 || strtotime($assigned->start_date) <= $now;

					if (
						$course_visible &&
						$not_past_course_assignment_end_date &&
						$not_before_course_assignment_start_date
					) {
						include($cardTemplate);
						$rendered_courses_count += 1;
					}
				endforeach;
			?>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p id="pagination"></p>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php if ($rendered_courses_count == 0): ?>
		<div style="display: flex; justify-content: center; padding: 20px; margin-top: 20px; margin-bottom: 20px; background-color: white; border-radius: 2px; box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);">
			<h2 style="margin: 0;">
				<?php echo AxsLanguage::text('COM_SPLMS_AXS_NO_COURSES_AVAILABLE', 'Sorry, no courses are available');?>
			</h2>
		</div>
	<?php endif; ?>
	<script>
		var operator = '?';
		function removeURLParameter(url, parameter) {
			//prefer to use l.search if you have a location/link object
			var urlparts = url.split('?');
			if (urlparts.length >= 2) {

				var prefix = encodeURIComponent(parameter) + '=';
				var pars = urlparts[1].split(/[&;]/g);

				//reverse iteration as may be destructive
				for (var i = pars.length; i-- > 0;) {
					//idiom for string.startsWith
					if (pars[i].lastIndexOf(prefix, 0) !== -1) {
						pars.splice(i, 1);
					}
				}

				return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
			}
			return url;
		}
		var currentUrl = removeURLParameter(window.location.href, 'page');
		if(currentUrl.indexOf('?') != -1) {
			var operator = '&';
		}
		$('#pagination').bootpag({
			total: <?php echo $totalPages; ?>,
			page: <?php echo $model->page; ?>,
			maxVisible: 5,
			leaps: false,
			firstLastUse: true,
			first: '<i class="fa fa-fast-backward"></i>',
			next: '<i class="fa fa-forward"></i>',
			prev: '<i class="fa fa-backward"></i>',
			last: '<i class="fa fa-fast-forward"></i>',
			wrapClass: 'pagination',
			activeClass: 'active',
			disabledClass: 'disabled'
		});

		$('#pagination').on("page", function(event, num){
			window.location.href = currentUrl+operator+"page="+num;
		});

		$('#searchForm').submit(function(ev) {
			ev.preventDefault();
			var link = '<?php echo JRoute::_("index.php?option=com_splms&view=courses&search="); ?>';
			var search = {
				'cat': $('#cat').val().replace(/[^\w.\s]/gi, ''),
				'title': $('#search_term').val().replace(/[^\w.\s]/gi, ''),
				'filter': $('#filter').val().replace(/[^\w.\s]/gi, '')
			};
			search = btoa(JSON.stringify(search));
			window.location.href = link + search;
		});

		var text_wishlist_add_error = "<?php echo AxsLanguage::text('COM_SPLMS_AXS_WISHLIST_ERROR_ADDING', 'Error wishlising this course.');?>";
		var text_wishlist_remove_error = "<?php echo AxsLanguage::text('COM_SPLMS_AXS_WISHLIST_ERROR_REMOVE', 'Error removing this course from wishlist.');?>";

		$('#filter').change(function(){
			var filter = $('#filter').val();
			var link = '<?php echo JRoute::_("index.php?option=com_splms&view=courses&search="); ?>';
			var search = {
				'cat': $('#cat').val().replace(/[^\w.\s]/gi, ''),
				'title': $('#search_term').val().replace(/[^\w.\s]/gi, ''),
				'filter': filter.replace(/[^\w.\s]/gi, '')
			};
			search = btoa(JSON.stringify(search));
			window.location.href = link + search;
			/* $('.cardBox').hide();
			$('.'+filter).show(); */
		});

		$('.wishlist').click(function(e) {
			e.preventDefault();
			if ($(this).data('wishlisted')) {
				//they are removing from their wishlist
				$(this).data('wishlisted', 0);
				$(this).find('.bookmarked').hide();
				$.ajax({
					method: "POST",
					url: "/index.php?option=com_splms&task=courses.onWishlistRemove&format=raw",
					data: {
						course_id: $(this).data('course-id')
					}
				}).done(function(result) {
					if (result != 'success') {
						alert(text_wishlist_remove_error);
					}
				}).fail(function() {
					alert(text_wishlist_remove_error);
				});
			} else {
				//they are adding it to their wishlist
				$(this).data('wishlisted', 1);
				$(this).find('.bookmarked').show();
				$.ajax({
					method: "POST",
					url: "/index.php?option=com_splms&task=courses.onWishlistAdd&format=raw",
					data: {
						course_id: $(this).data('course-id')
					}
				}).done(function(result) {
					if (result != 'success') {
						alert(text_wishlist_add_error);
					}
				}).fail(function() {
					alert(text_wishlist_add_error);
				});
			}
		});

		//$('.has-tooltip').tooltip();
		$('[data-toggle="tooltip"]').tooltip();
	</script>
	<!-- <script src="/js/jquery-3.4.1.min.js"></script> -->
	<!-- <script src="/templates/axs/elements/lms/courses-list/css/tovuti-course-list.js" type="text/javascript"></script> -->
<?php } //end of category permission checker ?>