<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No Direct Access
defined('_JEXEC') or die;

$columns = $this->params->get('columns', '3');

?>

<div id="splms" class="splms view-splms-quiz-list">
	<div class="splms-row">
	<?php foreach ($this->items as $item) { ?>
		<div class="splms-col-sm-6">
			<div class="quiz-item-wrapper">
				<div class="quiz-banner">
					<img class="img-responsive" src="<?php echo $item->image; ?>" alt="<?php echo $item->title; ?>">
				</div>
				<div class="quiz-description">
					<h3 class="quiz-title"><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></h3>
					<?php echo $item->description; ?>
					<h4 class="quiz-course-name"><?php echo $item->cat_name; ?></h4>
					<h4 class="quiz-duration"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_DURATION", "Duration") ?>: <?php echo $item->duration; ?> Sec </h4>
					<a href="<?php echo $item->url; ?>" class="btn btn-primary"><?php echo AxsLanguage::text("COM_SPLMS_QUIZ_PLAY", "Play this quiz") ?></a>
				</div>
			</div>
		</div>

	<?php } ?>
	</div> <!-- /.splms-row -->
	
</div> <!-- /.splms -->
