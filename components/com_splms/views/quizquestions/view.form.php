<?php

// Load jQuery Library

$doc = JFactory::getDocument();

class SplmsViewQuizquestions extends FOFViewForm{

	public function display($tpl = null){
		// Get model
		$model = $this->getModel();
		$this->items = $model->getItemList();
		// Load Lessons model
		$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');

		//$tournament_model 	= FOFModel::getTmpInstance('Tournaments', 'SpsoccerModel');
		
		foreach ($this->items as &$this->item) {
			$this->item->url 	= JRoute::_('index.php?option=com_splms&view=quizquestion&id=' . $this->item->splms_quizquestion_id . ':' . $this->item->slug . SplmsHelper::getItemid('quizquestions'));
			$this->item->cat_name = $courses_model->getCourse($this->item->splms_course_id)->title;
			
		}

		return parent::display($tpl = null);
	}

}