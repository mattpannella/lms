<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsModelCoursescategories extends FOFModel {

	public function getAllChildrenCategoryIds($id) {
		if(!$id) {
			return false;
		}
		$user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);
        
		$conditions[] = 'enabled = 1';
		$conditions[] = 'parent_id = '.(int)$id;
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('splms_coursescategory_id as id')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$categories = $db->loadColumn();
		return $categories;
	}

	public function getCoursesByCategory($category_id) {
		if(!$category_id) {
			return false;
		}
		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('enabled')." = 1";
		$children = $this->getAllChildrenCategoryIds($category_id);		
		if($children) {
			$childrenList = implode(',',$children);
			$conditions[] = '('.$db->qn('splms_coursescategory_id') .' IN ('.$childrenList.') OR '.$db->qn('splms_coursescategory_id') .'='.(int)$category_id.')';
		} else {
			$conditions[] = $db->qn('splms_coursescategory_id') .'='.(int)$category_id;
		}		
		// Create a new query object.
		$query = $db->getQuery(true);
		 
		// Select all records from the user profile table where key begins with "custom.".
		// Order it by the ordering field.
		$query->select('*');
		$query->from($db->quoteName('#__splms_courses'));
		$query->where($conditions);
		$query->order('ordering ASC');
		 
		// Reset the query using our newly populated query object.
		$db->setQuery($query);
		 
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();
		//$result = $db->loadObject();

		/*
		foreach ($results as &$result) {
			$result->url = JRoute::_('index.php?option=com_splms&view=course&id='.$result->splms_course_id.':'.$result->slug . self::getItemid('courses'));
		}
		*/

		return $results;
	}

	public function getItemList($overrideLimits = false, $group = '') {
		$user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);
        
        $conditions[] = 'enabled = 1';
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

		$conditions []= AxsBrands::getCourseCategoryBrandConditions();

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$categoriesOutput = array();
		$category_ids = array();
		
		foreach($categories as $category) {
			if(!$category->parent_id) {
				$categoriesOutput[$category->splms_coursescategory_id] = $category;
				$categoriesOutput[$category->splms_coursescategory_id]->children = array();
				array_push($category_ids, $category->splms_coursescategory_id);
			}
		}
		foreach($categories as $categoryChild) {
			if($categoryChild->parent_id) {
				if(array_key_exists($categoryChild->parent_id,$categoriesOutput)) {
					array_push($categoriesOutput[$categoryChild->parent_id]->children,$categoryChild);
					array_push($category_ids, $categoryChild->splms_coursescategory_id);
				}					
			}
		}
		return $categoriesOutput;        
	}
}