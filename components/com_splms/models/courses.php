<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsModelCourses extends FOFModel {
	public $search;
	public $catSlug;
	public $catData;
	public $viewType;
	public $limit;
	public $total;
	public $page;
	public $offset;

	// Request level cache
	static $loaded = array();

	public function __construct(array $config) {
		$app = JFactory::getApplication();
		$this->search = $app->input->get('search', '', 'STRING');
		$this->catSlug = $app->input->get('cat', '', 'STRING');

		$settings = json_decode(AxsLMS::getSettings()->params);
		if($settings->max_courses) {
			$this->limit = (int)$settings->max_courses;
		} else {
			$this->limit = 32;
		}

		$page = (int)$app->input->get('page', '', 'INT');
		if(!$page) {
			$page   = 1;
			$offset = 0;
		} else {
			$offset = ($page - 1) * $this->limit;
		}
		$this->page   = $page;
		$this->offset = $offset;
		$jinput = JFactory::getApplication()->input;
		$type = $jinput->get('type', 0);

		if ($type && $type == 'mycourses') {
			$this->viewType = 1;
		} else {
			$this->viewType = 0;
		}

		parent::__construct($config);
	}

	public function &getItemList($overrideLimits = false, $group = '') {
		if($this->search) {
			$courses = $this->getCoursesBySearch($this->search);
		} else if($this->catSlug) {
			$this->catData = $this->getCategoryBySlug($this->catSlug);
			if(is_object($this->catData)) {
				$courses = $this->getCoursesByCatId($this->catData->splms_coursescategory_id);
			} else {
				JFactory::getApplication()->enqueueMessage(AxsLanguage::text("COM_SPLMS_AXS_CATEGORY_NOT_AVAILABLE", "The specified category is not available!"), "error");
				$courses = $this->getAllCourses();
			}
		} else {
			$courses = $this->getAllCourses();
		}
		return $courses;
	}



	private function getCategoryConditions() {
	        $accessibleCategoriesArray = $this->getAllCategories(true);
	        if(!$accessibleCategoriesArray) {
	        	return;
	        }
	        $accessibleCategoriesConditions = array();
	        foreach($accessibleCategoriesArray as $accessibleCategory) {
	            $accessibleCategoriesConditions[] = "(FIND_IN_SET('$accessibleCategory', splms_coursescategory_id) > 0)";
	        }
	        $accessibleCategoriesConditionsList = implode('OR',$accessibleCategoriesConditions);

			$conditions = [];

	        $conditions []= " ($accessibleCategoriesConditionsList) ";

			//Get Brand Specific Conditions
			$conditions []= AxsBrands::getCourseCategoryBrandConditions();

	        return implode(' AND ', $conditions);
    }

	public function getAllCourses() {
		$user_id = JFactory::getUser()->id;
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array (
			$db->qn('enabled').'='.(int)1
		);


		if ($this->viewType) {
			$courses = implode(",", AxsLMS::getUserAllCourses($user_id, true));

			if ($courses) {
				$userCourses = "splms_course_id IN ($courses)";
				array_push($conditions, $userCourses);
			} else {
				$userCourses = "splms_course_id = 0";
				array_push($conditions, $userCourses);
			}
		}

        // Get the currently used brand
        $brand = AxsBrands::getCurrentBrandId();

        // Get normal visibility access conditions
		$accessConditions = AxsContentAccess::getAccessConditions($user_id);

		if($accessConditions) {
			$conditions []= $accessConditions;
		}

		$accessCategoryConditions = $this->getCategoryConditions();

        if($accessCategoryConditions) {
			$conditions []= $accessCategoryConditions;
        }

		$query->select('*')
			->from($db->qn('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC')
			->setLimit($this->limit,$this->offset);

		$db->setQuery($query);
		$result = $db->loadObjectList();

		$query = $db->getQuery(true);
		$query->select('COUNT(*) as total')
			->from($db->qn('#__splms_courses'))
			->where($conditions);
		$db->setQuery($query);
		$count = $db->loadObject();
		$this->total = $count->total;
		return $result;
	}


	public function getCoursesBySearch($search) {
		$this->search = json_decode(base64_decode($search));

		if ($this->search->cat) {
			$this->search->cat = AxsSecurity::alphaNumericOnly($this->search->cat);
			$this->catData = $this->getCategoryById($this->search->cat);
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$user_id = JFactory::getUser()->id;
		$conditions = array (
			$db->qn('enabled').'='.(int)1
		);

		if ($this->viewType) {

			$courses = implode(",", AxsLMS::getUserAllCourses($user_id, true));

			if ($courses) {
				$userCourses = "splms_course_id IN ($courses)";
				array_push($conditions, $userCourses);
			} else {
				$userCourses = "splms_course_id = 0";
				array_push($conditions, $userCourses);
			}
		}

		if($this->search->title) {
			$this->search->title = AxsSecurity::alphaNumericOnly($this->search->title);
			$search_terms = $this->search->title;

			//if it's in quotes, do an exact match.
			preg_match_all('/\"([\w ]+)\"/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = "(".$db->qn('title')." LIKE '%$q[1]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/\"([\w ]+)\"/', '', $search_terms);

			//if it has two words separated by an "and or + symbol" it must have both. as in "troy and bob" or "troy + bob"
			preg_match_all('/(\w+)( +and +| *\+ *)(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('title')." LIKE '%$q[1]%' AND ".$db->qn('title')." LIKE '%$q[3]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+)( +and +| *\+ *)(\w+)/', '', $search_terms);

			//if it has two words separated by an "_ symbol" it can have either (or). as in "troy _ bob" or "troy_bob"
			preg_match_all('/(\w+) *_ *(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('title')." LIKE '%$q[1]%' OR ".$db->qn('title')." LIKE '%$q[2]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+) *_ *(\w+)/', '', $search_terms);

			//all remaining words
			$search_terms = str_replace(',', ' ', $search_terms);
			$search_terms = preg_replace('/\s+/',',',trim($search_terms));
			$searchArray = explode(',', $search_terms);
			foreach($searchArray as $term) {
				$conditions[] = $db->qn('title')." LIKE '%$term%'";
			}
		}

		if($this->search->desc) {
			$search_terms = $this->search->desc;

			//if it's in quotes, do an exact match.
			preg_match_all('/\"([\w ]+)\"/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = "(".$db->qn('description')." LIKE '%$q[1]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/\"([\w ]+)\"/', '', $search_terms);

			//if it has two words separated by an "and or + symbol" it must have both. as in "troy and bob" or "troy + bob"
			preg_match_all('/(\w+)( +and +| *\+ *)(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('description')." LIKE '%$q[1]%' AND ".$db->qn('description')." LIKE '%$q[3]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+)( +and +| *\+ *)(\w+)/', '', $search_terms);

			//if it has two words separated by an "_ symbol" it can have either (or). as in "troy _ bob" or "troy_bob"
			preg_match_all('/(\w+) *_ *(\w+)/', $search_terms, $matches, PREG_SET_ORDER);
			foreach($matches as $q) {
				$conditions[] = $db->qn('description')." LIKE '%$q[1]%' OR ".$db->qn('description')." LIKE '%$q[2]%'";
			}
			//now remove it from the search terms
			$search_terms = preg_replace('/(\w+) *_ *(\w+)/', '', $search_terms);

			//all remaining words
			$search_terms = str_replace(',', ' ', $search_terms);
			$search_terms = preg_replace('/\s+/',',',trim($search_terms));
			$searchArray = explode(',', $search_terms);
			foreach($searchArray as $term) {
				$conditions[] = $db->qn('description')." LIKE '%$term%'";
			}
		}

		if($this->search->cat) {
			$children = $this->getAllChildrenCategoryIds($this->search->cat);
			if($children) {
				$childrenList = implode(',',$children);
				$conditions[] = '('.$db->qn('splms_coursescategory_id') .' IN ('.$childrenList.') OR '.$db->qn('splms_coursescategory_id') .'='.(int)$this->search->cat.')';
			} else {
				$conditions[] = $db->qn('splms_coursescategory_id') .'='.(int)$this->search->cat;
			}
		}

		$accessConditions = AxsContentAccess::getAccessConditions($user_id);

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$accessCategoryConditions = self::getCategoryConditions();
        if($accessCategoryConditions) {
            array_push($conditions,$accessCategoryConditions);
		}

		if($this->search->filter) {
			$this->search->filter = AxsSecurity::alphaNumericOnly($this->search->filter);
			$courseFilterList = AxsLMS::getUserAllCourses($user_id,true,$this->search->filter);
			if($courseFilterList) {
				$courseFilterIds = implode(',',$courseFilterList);
				$conditions[] = $db->qn('splms_course_id') .' IN ('.$courseFilterIds.')';
			} else {
				$conditions[] = $db->qn('splms_course_id') .' = 0';
			}
		}

		$query->select('*')
			->from($db->qn('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC')
			->setLimit($this->limit,$this->offset);
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$query = $db->getQuery(true);
		$query->select('COUNT(*) as total')
			->from($db->qn('#__splms_courses'))
			->where($conditions);
		$db->setQuery($query);
		$count = $db->loadObject();
		$this->total = $count->total;

		return $result;
	}

	public function getCoursesByCatId($id) {
		$db = JFactory::getDbo();
		$user_id = JFactory::getUser()->id;
		$children = $this->getAllChildrenCategoryIds($id);
		$query = $db->getQuery(true);
		$conditions[] = $db->qn('enabled').'= 1';
		if($children) {
			$childrenList = implode(',',$children);
			$conditions[] = '('.$db->qn('splms_coursescategory_id') .' IN ('.$childrenList.') OR '.$db->qn('splms_coursescategory_id') .'='.(int)$id.')';
		} else {
			$conditions[] = $db->qn('splms_coursescategory_id') .'='.(int)$id;
		}

		if ($this->viewType) {
			$courses = implode(",", AxsLMS::getUserAllCourses($user_id, true));

			if ($courses) {
				$userCourses = "splms_course_id IN ($courses)";
				array_push($conditions, $userCourses);
			} else {
				$userCourses = "splms_course_id = 0";
				array_push($conditions, $userCourses);
			}
		}

		$accessConditions = AxsContentAccess::getAccessConditions($user_id);

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query->select('*')
			->from($db->qn('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC')
			->setLimit($this->limit,$this->offset);
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$query = $db->getQuery(true);
		$query->select('COUNT(*) as total')
			->from($db->qn('#__splms_courses'))
			->where($conditions);
		$db->setQuery($query);
		$count = $db->loadObject();
		$this->total = $count->total;
		return $result;
	}

	public function getCourseById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn('splms_course_id').'='.(int)$id
		);

		$accessConditions = AxsContentAccess::getAccessConditions();

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query->select('*')
			->from($db->qn('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getCategoryBySlug($slug) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		if(is_numeric($slug)) {
			$column = 'splms_coursescategory_id';
		} else {
			$column = 'slug';
		}
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn($column) .'='. $db->q($slug)
		);

		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getCategoryById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn('splms_coursescategory_id').'='.(int)$id
		);
		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getAllChildrenCategories($id) {
		if(!$id) {
			return false;
		}
		$user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);

		$conditions[] = 'enabled = 1';
		$conditions[] = 'parent_id = '.(int)$id;
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		return $categories;
	}

	public function getAllChildrenCategoryIds($id) {
		if(!$id) {
			return false;
		}
		$user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);

		$conditions[] = 'enabled = 1';
		$conditions[] = 'parent_id = '.(int)$id;
        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('splms_coursescategory_id as id')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$categories = $db->loadColumn();
		return $categories;
	}

	public function getAllCategories($ids_only = false) {
		$user_id = JFactory::getUser()->id;
        $accessConditions = AxsContentAccess::getAccessConditions($user_id);

        $conditions[] = 'enabled = 1';

		$conditions []= AxsBrands::getCourseCategoryBrandConditions();

        if($accessConditions) {
            array_push($conditions,$accessConditions);
        }

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$categoriesOutput = array();
		$category_ids = array();

		foreach($categories as $category) {
			if(!$category->parent_id) {
				$categoriesOutput[$category->splms_coursescategory_id] = $category;
				$categoriesOutput[$category->splms_coursescategory_id]->children = array();
				array_push($category_ids, $category->splms_coursescategory_id);
			}
		}
		foreach($categories as $categoryChild) {
			if($categoryChild->parent_id) {
				if(array_key_exists($categoryChild->parent_id,$categoriesOutput)) {
					array_push($categoriesOutput[$categoryChild->parent_id]->children,$categoryChild);
					array_push($category_ids, $categoryChild->splms_coursescategory_id);
				}
			}
		}

		if($ids_only) {
			return $category_ids;
		} else {
			return $categoriesOutput;
		}

	}

	public function getCourseStudents($course) {

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query
			->select('*')
			->from($db->quoteName('joom_splms_courses'))
			->where($db->quoteName('splms_course_id') . '=' . (int)$course);

		$db->setQuery($query);
		$result = $db->loadObject();

		if ((float)$result->price == 0) {
			$free = true;
		} else {
			$free = false;
		}

		$query->clear();

		if (!$free) {

			//The course is not free, get the list from the students who have purchased it.

			$conditions = array(
				$db->quoteName('purchases.course_id') . '=' . (int)$course,
				$db->quoteName('purchases.status') . '=' . $db->quote("PAID"),
				$db->quoteName('values.field_id') . '= 20'		//Last name, for alphabetical order
			);

			$query
				->select('purchases.user_id')
				->from($db->quoteName('axs_course_purchases', 'purchases'))
				->join('LEFT', $db->quoteName('joom_community_fields_values','values') . ' ON (' . $db->quoteName('purchases.user_id') . '=' . $db->quoteName('values.user_id') . ')')
				->join('RIGHT', $db->quoteName('joom_users','user') . ' ON (' . $db->quoteName('purchases.user_id') . '=' . $db->quoteName('user.id') . ')')
				->where($conditions)
				->order($db->quoteName('values.value') . ' ASC');

			$db->setQuery($query);
			return $db->loadObjectList();
		} else {
			//The course is free.  Get the list from the course progress table

			$conditions = array(
				$db->quoteName('progress.course_id') . '=' . (int)$course,
				$db->quoteName('values.field_id') . '= 20'
			);

			$conditions[] = 'progress.archive_date IS NULL';

			$query
				->select('progress.user_id')
				->from($db->quoteName('joom_splms_course_progress', 'progress'))
				->join('LEFT', $db->quoteName('joom_community_fields_values', 'values') . ' ON (' . $db->quoteName('progress.user_id') . '=' . $db->quoteName('values.user_id') . ')')
				->join('RIGHT', $db->quoteName('joom_users','user') . ' ON (' . $db->quoteName('progress.user_id') . '=' . $db->quoteName('user.id') . ')')
				->where($conditions)
				->order($db->quoteName('values.value') . ' ASC')
				->group($db->quoteName('progress.user_id'));

			$db->setQuery($query);
			return $db->loadObjectList();
		}
	}

	// Get Course Teachers by Course ID
	public function getCourseTeachers($course) {

		$sig = md5(serialize($course));
		if (isset(static::$loaded[__METHOD__][$sig])) {
			return static::$loaded[__METHOD__][$sig];
		}
		$lessons = self::getLessons($course,null);
		$ids = array();
		foreach ($lessons as $lesson) {
			$ids[] = $lesson->splms_teacher_id;
		}

		$ids = array_unique($ids);
		$ids = implode(',',$ids);
		if ($ids) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$conditions = array(
				$db->qn('enabled').'='.(int)1,
				$db->qn('splms_teacher_id') . ' IN (' . $ids . ')'
			);
			$query->select('*')
				->from($db->qn('#__splms_teachers'))
				->where($conditions)
				->order('ordering ASC');
			$db->setQuery($query);
			$items = $db->loadObjectList();
		} else {
			$items = array();
		}

		static::$loaded[__METHOD__][$sig] = $items;
		
		return $items;
	}

	public function getCourseTeachersByLessons($lessons) {
		$ids = array();
		foreach ($lessons as $lesson) {
			$ids[] = $lesson->splms_teacher_id;
		}

		$ids = array_unique($ids);
		$ids = implode(',',$ids);

		if ($ids) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$conditions = array (
				$db->qn('enabled').'='.(int)1,
				$db->qn('splms_teacher_id') . ' IN (' . $ids . ')'
			);
			$query->select('*')
				->from($db->qn('#__splms_teachers'))
				->where($conditions)
				->order('ordering ASC');
			$db->setQuery($query);
			$items = $db->loadObjectList();
		} else {
			$items = array();
		}
		return $items;
	}

	// Get Course By Course id
	public static function getCourse($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Select all records from the user profile table where key begins with "custom.".
		// Order it by the ordering field.

		$conditions = array(
			$db->quoteName('enabled') . '=' . $db->quote(1),
			$db->quoteName('splms_course_id') .'='. $db->quote($id)
		);

		$accessConditions = AxsContentAccess::getAccessConditions();

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query
			->select('*')
			->from($db->quoteName('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$result = $db->loadObject();

		$result->url = JRoute::_('index.php?option=com_splms&view=course&id='.$result->splms_course_id.':'.$result->slug . SplmsHelper::getItemid('courses'));

		return $result;
	}

	/**
	 * @param $user_id
	 * @param $course_id
	 *
	 * @return bool
	 * @deprecated
	 */
	public static function getIsbuycourse($user_id, $course_id) {


		return AxsExtra::hasBoughtItem($course_id, $user_id);
	}

	/**
	 * @param $user_id
	 *
	 * @return mixed
	 * @deprecated
	 */

	public function getUserPurchasedCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
		$query = "SELECT * FROM axs_course_purchases WHERE status = 'PAID' AND user_id = '$user_id'";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$courseIDs = array();
		foreach ($results as $id) {
			array_push($courseIDs, $id->course_id);
		}
		$result = array_unique($courseIDs);
		return $result;
	}

	public function getUserWishlistCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
		$query = "SELECT * FROM joom_splms_course_wishlist WHERE user_id = '$user_id'";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$courseIDs = array();
		foreach ($results as $id) {
			array_push($courseIDs, $id->course_id);
		}
		$result = array_unique($courseIDs);
		return $result;
	}

	public function getUserAssignedRecommendedCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
		$courseIDs = array();
		//get courses that are recommended or assigned by usergroup
		$query = "SELECT a.* FROM `#__splms_courses_groups` a LEFT JOIN `#__user_usergroup_map` b ON b.group_id IN (a.usergroups) WHERE (a.course_ids != '' AND a.course_ids IS NOT NULL) AND  (b.user_id=$user_id OR FIND_IN_SET($user_id, a.user_ids)) GROUP BY a.id;";
		$db->setQuery($query);
		$course_groups = $db->loadObjectList();
		foreach($course_groups as $cg) {
			$course_ids = explode(',', $cg->course_ids);
			foreach($course_ids as $cid) {
				array_push($courseIDs, $cid);
			}
		}
		$result = array_unique($courseIDs);
		return $result;
	}

	public function getUserCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
		$courseIDs = array();
		$wishlisted = $this->getUserWishlistCourses($user_id);
		$purchased = $this->getUserPurchasedCourses($user_id);
		$assigned = $this->getUserAssignedRecommendedCourses($user_id);
		if($wishlisted){
			$courseIDs = array_merge($courseIDs, $wishlisted);
		}

		if($purchased){
			$courseIDs = array_merge($courseIDs, $purchased);
		}

		if($assigned){
			$courseIDs = array_merge($courseIDs, $assigned);
		}

		$result = array_unique($courseIDs);
		$result = implode(",", $result);
		return $result;
	}

	public static function getPurchasedCourse($user_id) {
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);
		$query->select('user_id AS order_user_id, item_id AS splms_course_id, status');
		$query->from($db->quoteName('cctransactions'));
		$query->where($db->quoteName('user_id')." = ".$db->quote($user_id));
		$query->where($db->qn('type') . '=' . (int)0);
		$query->order('date DESC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach ($results as &$result) {
			$result->course_info = self::getCourse($result->splms_course_id);

			$result->course_name = $result->course_info->title;
			$result->url  = $result->course_info->url;
		}

		return $results;
	}

	/*
	public static function getTeacherMaterials($lesson_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$language = self::getLanguage();

		$conditions = array(
			$db->quoteName('splms_lesson_id') . '=' . $db->quote($lesson_id),
			$db->quoteName('language') . '=' . $db->quote($language)
		);

		$query
			->select('teacher_materials')
			->from($db->quoteName('#__splms_lessons'))
			->where($conditions);

		$db->setQuery($query);

		$results = $db->loadObjectList();

		if ($results) {
			$result = json_decode($results[0]->teacher_materials);

		}

		return $result;
	}

	public static function getTeacherMaterialArray($lesson_id) {

		$teacherList = SplmsModelCourses::getTeacherMaterials($lesson_id);

		if ($teacherList) {
			// Point table

			$teacher_id = $teacherList->splms_teacher_id;
			$audio_file = $teacherList->audio_file;
			$video_file = $teacherList->video_file;
			$pdf_file = $teacherList->pdf_file;
			$video_thumb = $teacherList->video_thumb;
			$audio_duration = $teacherList->audio_duration;
			$video_duration = $teacherList->video_duration;

			$teacherArray = array();
			$teacherCount = count($teacher_id);

			for ($i = 0; $i < $teacherCount; $i++) {

				$teacherArray[] = array(
					'teacher_id' 		=> $teacher_id[$i],
					'audio_file' 		=> $audio_file[$i],
					'video_file' 		=> $video_file[$i],
					'pdf_file'			=> $pdf_file[$i],
					'video_thumb' 		=> $video_thumb[$i],
					'video_duration'	=> $video_duration[$i],
					'audio_duration'	=> $audio_duration[$i]
				);
			}
		}

		return $teacherArray;

	}
	*/


	// build query
	public function buildQuery($overrideLimits = false){

		// BEGIN:: Query
		$table = $this->getTable();
		$tableName = $table->getTableName();
		$tableKey = $table->getKeyName();

		$db = $this->getDbo();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName($tableName));

		//Frontend
		if(FOFPlatform::getInstance()->isFrontend()) {
			// Get Params
			$app = JFactory::getApplication();
			$params = $app->getMenu()->getActive()->params; // get the active item
			//Enabled
			$query
				->where($db->quoteName('enabled') . "=" . $db->quote('1'))
				->order('ordering ASC');
		}

		return $query;

	}

	public function update_lesson_materials($lesson_id, $teacher_materials) {

		$language = AxsLanguage::getCurrentLanguage()->get('tag');

		//if($language == 'french') {
           $teacher_materials = json_encode($teacher_materials, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
       	/*} else {
           $teacher_materials = json_encode($teacher_materials);
       	}*/

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$fields = array(
			$db->quoteName('teacher_materials') . '=' . $db->quote($teacher_materials)
		);

		$conditions = array(
			$db->quoteName('splms_lesson_id') . '=' . $db->quote($lesson_id)
		);

		$query
			->update($db->quoteName('#__splms_lessons'))
			->set($fields)
			->where($conditions);

		$db->setQuery($query);
		$db->execute();
	}



	public function checkQuizPassed($userId, $quizRequirements) {

		$quizCount = count($quizRequirements->quiz_id);

		/*
			Loop through all of the required courses and check that they have
			been taken, and have been passed.
		*/

		$passed = true;

		$quizResults = array();

		for ($i = 0; $i < $quizCount; $i++) {

			$quizId = $quizRequirements->quiz_id[$i];

			$db = JFactory::getDBO();
			$query = "SELECT a.point AS points, a.total_marks AS total, b.title AS title, b.params
					FROM #__splms_quizresults AS a
					INNER JOIN #__splms_quizquestions AS b
					WHERE a.splms_quizquestion_id = $quizId
					AND b.splms_quizquestion_id = a.splms_quizquestion_id
					AND a.user_id = $userId
					AND a.archive_date IS NULL";

			$db->setQuery($query);
			$results = $db->loadObjectList();
			$params = json_decode($results[0]->params);
			$requiredScore = (int)$params->required_score;
			$final = new stdClass();
			$final->required = $requiredScore;

			$quizzesTaken = count($results);
			if ($quizzesTaken == 0) {
				/*
					The user has not taken the quiz.
				*/

				$query = "SELECT title
					FROM #__splms_quizquestions
					WHERE splms_quizquestion_id = $quizId";

				$db->setQuery($query);
				$results = $db->loadObjectList();

				if (count($results) == 0) {
					$final->title = "Unknown Quiz";
				} else {
					$final->title = $results[0]->title;
				}

				$final->score = null;
				$passed = false;
			} else {

				$highestScore = 0;

				/*
					If the user has attempted the quiz multiple times,
					get their highest score.
				*/

				foreach($results as $result) {

					$quizScore = $result->points / $result->total;
					if ($quizScore > $highestScore) {
						$highestScore = $quizScore;
					}
				}

				$final->title = $results[0]->title;
				$final->score = ($highestScore * 100);
				$final->required = $requiredScore;

				//Did they pass?
				if (($highestScore * 100) < $requiredScore) {
					$passed = false;
				}
			}

			array_push($quizResults, $final);
		}

		/*
			All quizzes have been successfully passed.
		*/

		$results = new stdClass();
		$results->passed = $passed;
		$results->scores = $quizResults;

		return $results;
	}

	public function getAllAttempts($quizzes, $results) {

		$attempts = array();
		foreach($quizzes as $quiz) {
			$count = 0;
			foreach($results as $result) {
				if ($result->splms_quizquestion_id == $quiz->splms_quizquestion_id) {
					$count++;
				}
			}

			$attempt = new stdClass();
			$attempt->tries = $count;
			$attempt->total = $quiz->tries;

			$attempts[$quiz->splms_lesson_id] = $attempt;
		}

		return $attempts;
	}

	public function getLessons($course,$lesson) {


		$language = AxsLanguage::getCurrentLanguage();
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('*')
			->from($db->qn('#__splms_lessons'));

		if ($course) {
		 	$query->where($db->qn('splms_course_id').'='.(int)$course);
		}

		if ($lesson) {
			$query->where($db->qn('splms_lesson_id').'='.(int)$lesson);
		}

		$query->where($db->qn('enabled').'= 1')
			  ->where($db->qn('language').'='.$db->q($language->get('tag')));

		$db->setQuery($query);
		if ($lesson) {
			$result = $db->loadObject();
		} else {
			$result = $db->loadObjectList();
		}


		return $result;
	}

	public function getProgress($course,$lesson,$user,$status) {
		$language = AxsLanguage::getCurrentLanguage()->get('tag');
		if (isset($_SESSION['current_language'])) {
			$language = $_SESSION['current_language'];
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->qn('#__splms_lesson_status') . ' AS s')
			->innerJoin($db->qn('#__splms_lessons') . 'AS l ON l.splms_lesson_id = s.lesson_id ');

		if ($course) {
			$query->where($db->qn('course_id').'='.(int)$course);
		}

		if ($lesson) {
			$query->where($db->qn('lesson_id').'='.(int)$lesson);
		}

		if ($status) {
			$query->where($db->qn('status').'='.$db->q($status));
		}

		$query
			->where('s.archive_date IS NULL')
			->where($db->qn('s.user_id').'='.(int)$user)
			->where($db->qn('s.language').'='.$db->q($language))
			->group($db->qn('s.user_id').','.$db->qn('s.course_id').','.$db->qn('s.lesson_id'))
			->order($db->qn('l.ordering'). 'ASC');

		$db->setQuery($query);

		$result = $db->loadObjectList();
		return $result;
	}

	public function getUserQuizzes($course,$lesson,$user,$quiz) {
		return AxsLMS::getAllUserQuizzes($course,$lesson,$user,$quiz);
	}

	public function getMinimumStudentFailView($params, $current) {

		$required = $params->minimum_students;

		$percentage = ($current / $required) * 100;

		if ($percentage <= 25) {
			$color = "#FF0000";	//Red
		} else if ($percentage > 25 && $percentage <= 50) {
			$color = "#FF9900";	//Orange
		} else if ($percentage > 50 && $percentage <= 75) {
			$color = "#64C800";	//Light Green
		} else {
			$color = "#00C800";	//Green
		}

		$progressColor = "$color";

		ob_start();

		?>

			<div id="minimumStudentFailView" class="box container-fluid">
				<div class="row">
					<div id="studentsNeeded" class="col-md-2"></div>
					<div id="studentsNeededInfo" class="col-md-9">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12" style="margin-bottom: 12px;">
									<?php
										if ($params->minimum_students_not_met_text == "") {
											echo JText::_('COM_SPLMS_AXS_CURRENT_STUDENTS_NOT_MET');
										} else {
											echo $params->minimum_students_not_met_text;
										}
									?>
								</div>
								<div class="col-md-1"></div>
								<div class="currentStudentsBox studentsBox col-md-4">
									<div class="studentsBoxIcon">
										<span class="lizicon-users"></span>
									</div>
									<div class="studentsBoxData">
										<?php echo $current; ?>
									</div>
									<div class="studentsBoxTitle">
										<?php echo JText::_('COM_SPLMS_AXS_CURRENT_STUDENTS_COUNT');?>
									</div>
								</div>
								<div class="col-md-1"></div>
								<div class="requiredStudentsBox studentsBox col-md-4">
									<div class="studentsBoxIcon">
										<span class="lizicon-community"></span>
									</div>
									<div class="studentsBoxData">
										<?php echo $required; ?>
									</div>
									<div class="studentsBoxTitle">
										<?php echo JText::_('COM_SPLMS_AXS_MINIMUM_STUDENTS_REQUIRED');?>
									</div>
								</div>
								<div class="col-md-1"></div>

							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>

			<script type="text/javascript" src="components/shared/includes/js/easy-pie-chart.js"></script>
			<script>
				$(document).ready(
					function() {
						function setMinStudentPieWidth() {
							/*
							var newPieChart = `<div class="percentage" data-percent="<?php echo $percentage;?>">
	                                <span class="value">
	                                <?php echo round($percentage);?>
	                                </span>
	                                <span class="percent-sign">%</span>
		                       </div>`;
		                    */

		                    var newPieChart = '<div class="percentage" data-percent="<?php echo $percentage;?>"></div>';
							var progressColor = "<?php echo $progressColor;?>";

					    	$("#studentsNeeded").html(newPieChart);

							var width = $('#studentsNeeded').width();
							console.log(width);
							var lineWidth = width / 4;
							var fontSize = width / 8;
							var barColor = progressColor;
							var trackColor = '#ccc';
							var scaleColor = '#aaa';
							var percent = Math.round($('.percentage').data('percent'));
							console.log(percent);

							if (percent >= 100) {
								percent = 100;

								barColor = progressColor;
								scaleColor = progressColor;
							} else if (percent == 0) {
								var fontColor = scaleColor;
							} else {
								var fontColor = barColor;
							}

							$('.percentage').css('fontSize',fontSize+'px','color',fontColor);
							$('.percentage').css('color',fontColor);
							$('.percentage').easyPieChart( {
							    animate: 1000,
							    lineWidth: lineWidth,
							    barColor: barColor,
							    trackColor: trackColor,
							    lineCap:'butt',
							    scaleColor: scaleColor,
							    size: width,
							    onStep: function(value) {
							    	this.$el.find('.value').text(Math.round(value));
							            },
							    onStop: function(value, to) {
							        this.$el.find('.value').text(Math.round(to));
							    }
						    });

						    if(percent == 100) {
						    	fontSize = width / 2.75;
						    }
						}

						setMinStudentPieWidth();

						$(window).resize(function() {
							console.log("resizing");
							setMinStudentPieWidth();
						});
					}
				);
			</script>

		<?php

		return ob_get_clean();
	}

	public static function minimumStudentsMet($course_id) {
		$db = JFactory::getDBO();
		$query = "SELECT params, slug FROM joom_splms_courses WHERE `splms_course_id` = $course_id";
		$db->setQuery($query);
		$result = $db->loadObject();
		if ($result) {
			$params = json_decode($result->params);
		} else {
			$url = JRoute::_('index.php?option=com_splms&view=course');
			return $url;
		}

		if ($params->require_minimum_students == "yes") {
			$current = count(self::getCourseStudents($course_id));

			if ($current < (int)$params->minimum_students) {
				$url = JRoute::_('index.php?option=com_splms&view=course&id='.$course_id.':'.$result->slug . SplmsHelper::getItemid('courses'));
				return $url;
			} else {
				return true;
			}
		} else {
			return true;
		}
	}

	public static function setOverrides(&$item) {



		$language = AxsLanguage::getCurrentLanguage();

		$sef = AxsLanguage::getSefByLangCode($language->get('tag'));

		$course_params = json_decode($item->params);

		//$coures_params = $this->item->params;

		$overrides = $course_params->override->$sef;

		if ($overrides->use == 0) {
			return;
		}

		if ($overrides->title) {
			$item->title = $overrides->title;
		}

		if ($overrides->description) {
			$item->description = $overrides->description;
		}

		if ($overrides->minimum_students_not_met_text) {
			$course_params->minimum_students_not_met_text = $overrides->minimum_students_not_met_text;
		}

		if ($overrides->scorm_content) {
			$course_params->scorm_content = $overrides->scorm_content;
		}

		if ($overrides->scorm_start_button) {
			$course_params->scorm_start_button = $overrides->scorm_start_button;
		}

		if ($overrides->scorm_id) {
			$course_params->scorm_id = $overrides->scorm_id;
		}

		if ($overrides->lesson_button_text) {
			$course_params->lesson_button_text = $overrides->lesson_button_text;
		}

		if ($overrides->students_button_text) {
			$course_params->students_button_text = $overrides->students_button_text;
		}

		if ($overrides->progress_area_text) {
			$course_params->progress_area_text = $overrides->progress_area_text;
		}

		if ($overrides->student_list_text) {
			$course_params->student_list_text = $overrides->student_list_text;
		}

		if ($overrides->teacher_list_text) {
			$course_params->teacher_list_text = $overrides->teacher_list_text;
		}

		if ($overrides->video_type != 'none') {
			$course_params->video_type = $overrides->video_type;

			if ($overrides->video_url) {
				$item->video_url = $overrides->video_url;
			}

			if ($overrides->youtube_id) {
				$course_params->youtube_id = $overrides->youtube_id;
			}

			if ($overrides->screencast_id) {
				$course_params->screencast_id = $overrides->screencast_id;
			}

			if ($overrides->vimeo_id) {
				$course_params->vimeo_id = $overrides->vimeo_id;
			}

			if ($overrides->facebook_url) {
				$course_params->facebook_url = $overrides->facebook_url;
			}
		}

		$item->params = json_encode($course_params);

	}

}
