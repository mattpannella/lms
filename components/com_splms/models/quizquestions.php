<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsModelQuizquestions extends FOFModel {

	// Get Player by player ID
	public function getQuizResult($user_id, $quiz_id = null) {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select($db->quoteName(array('a.splms_quizresult_id', 'a.point', 'a.total_marks', 'a.splms_quizquestion_id', 'a.splms_course_id')));
		$query->select($db->quoteName('b.title', 'quiz_name'));
		$query->select($db->quoteName('c.title', 'course_name'));
		$query->select($db->quoteName('c.slug', 'course_slug'));
    	$query->from($db->quoteName('#__splms_quizresults', 'a'));
    	$query->join('LEFT', $db->quoteName('#__splms_quizquestions', 'b') . ' ON (' . $db->quoteName('a.splms_quizquestion_id') . ' = ' . $db->quoteName('b.splms_quizquestion_id') . ')');
    	$query->join('LEFT', $db->quoteName('#__splms_courses', 'c') . ' ON (' . $db->quoteName('a.splms_course_id') . ' = ' . $db->quoteName('c.splms_course_id') . ')');
    	$query->where($db->quoteName('a.user_id')." = ".$db->quote($user_id));
    	$query->where($db->quoteName('a.enabled')." = 1");
		$query->where('a.archive_date IS NULL');

    	if ($quiz_id != null) {
    		$query->where($db->quoteName('b.splms_quizquestion_id') . " = " . $db->quote($quiz_id));
    	}

    	$db->setQuery($query);
		$results = $db->loadObjectList();

		foreach ($results as &$result) {
			$result->course_url = JRoute::_('index.php?option=com_splms&view=course&id='.$result->splms_course_id.':'.$result->course_slug . SplmsHelper::getItemid('courses'));
		}

		return $results;
	}

	public static function getCourseQuizzes($course_id) {
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('splms_course_id') . '=' . $db->quote($course_id)
		);

		$query
			->select('*')
			->from('#__splms_quizquestions')
			->where($conditions);			

		$db->setQuery($query);

		$result = $db->loadObjectList();
		return $result;
	}

	// Get users quiz by ID
	public static function getQuizById($user_id, $quiz_id) {
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);

		$query->select($db->quoteName(array('splms_quizresult_id', 'point', 'total_marks', 'splms_quizquestion_id', 'splms_course_id')));
		$query->from($db->quoteName('#__splms_quizresults'));
		$query->where($db->quoteName('enabled')." = 1");
		$query->where($db->quoteName('user_id')." = ".$db->quote($user_id));
		$query->where($db->quoteName('splms_quizquestion_id')." = ".$db->quote($quiz_id));
		$query->where('archive_date IS NULL');
		$query->setLimit(1);
		$query->order('ordering ASC');
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$result = $db->loadObject();

		return $result;
	}

	public static function getQuizByLesson($course,$lesson) {
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);

		$query->select('*');
		$query->from($db->quoteName('#__splms_quizquestions'));
		if($lesson) {
			$query->where($db->quoteName('splms_lesson_id')." = ".$db->quote($lesson));
		}
		if($course) {
			$query->where($db->quoteName('splms_course_id')." = ".$db->quote($course));
		}
		$query->where($db->quoteName('enabled')." = 1");
		
		$query->setLimit(1);
		$query->order('ordering ASC');
		$db->setQuery($query);
		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$result = $db->loadObject();

		return $result;
	}

	// Insert Quiz Result
	public function insertQuizResult($params) {
		//print_r($user_id);
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Insert columns.
		$columns = array(
			'user_id', 
			'splms_quizquestion_id', 
			'splms_course_id', 
			'splms_lesson_id', 
			'total_marks', 
			'point',
			'date',
			'answers'
		);

		// Insert values.
		$values = array(
			$db->q($params->user_id),
			$db->q($params->quiz_id),
			$db->q($params->course_id),
			$db->q($params->lesson_id),
			$db->q($params->total_marks),
			$db->q($params->q_result),
			$db->q(date('Y-m-d H:i:s')),
			$db->q($params->answers)
		);

		// Prepare the insert query.
		$query
			->insert($db->quoteName('#__splms_quizresults'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));

		// Set the query using our newly populated query object and execute it.
		$db->setQuery($query);
		$db->execute();
	}
}