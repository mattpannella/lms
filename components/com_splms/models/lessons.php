<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */


defined('_JEXEC') or die();

class SplmsModelLessons extends FOFModel {

	protected static $message = '';
	protected static $files = array();

	// Request-level cache
	static $loaded = array();

	// Get Lessons by course ID

	public function getActivity($activity,$lesson) {
		$icon = '';
		$formDisplay = '';
		$submissionDisplay = '';
		$submission = '';
		$settings = json_decode(AxsLMS::getSettings()->params);
		$lessonParams = new stdClass();
		$lessonParams->lesson_id               = $lesson->splms_lesson_id;
		$lessonParams->activity_id             = $activity->id;
		$lessonParams->course_id               = $lesson->splms_course_id;
		$lessonParams->activity_title          = $activity->title;
		$lessonParams->activity_required       = $activity->required;
		$lessonParams->activity_required_score = $activity->required_score;
		$encodedLessonParams = base64_encode(json_encode($lessonParams));
		$activity->lesson = json_encode($lesson);
		if($activity->icon) {
			$icon = '<span class="fa '.$activity->icon.'"></span>';
		}

		$activityTemplate = 'upload';
		$embedFieldTypes  = ['powerpoint','pdf'];
		$videoPlayerFieldTypes  = ['video'];
		$customEmbedFieldTypes  = ['embed'];
		$audioFieldTypes  = ['audio'];
        $textFieldTypes   = ['youtube','vimeo','facebook','screencast','link','textbox','esignature'];
        $videoFieldTypes  = ['youtube','vimeo','facebook','screencast','mp4'];
		$interactiveFieldTypes  = ['interactive'];
		$questionSetFieldTypes  = ['survey'];

        if(in_array($activity->type, $textFieldTypes)) {
            $activityTemplate = 'textfield';
        }

        if(in_array($activity->type, $interactiveFieldTypes)) {
            $activityTemplate = 'interactive';
        }

        if(in_array($activity->type, $embedFieldTypes)) {
            $activityTemplate = 'embedfield';
        }

        if(in_array($activity->type, $customEmbedFieldTypes)) {
            $activityTemplate = 'custom_embed';
        }

        if(in_array($activity->type, $videoPlayerFieldTypes)) {
            $activityTemplate = 'videofield';
        }

        if(in_array($activity->type, $audioFieldTypes)) {
            $activityTemplate = 'audiofield';
		}

		if(in_array($activity->type, $questionSetFieldTypes)) {
            $activityTemplate = 'survey';
        }

        if($activityTemplate == 'textfield') {
        	if(in_array($activity->type, $videoFieldTypes)) {
	            $textFieldIcon = 'fa-video-camera';
	            $textFieldPlaceholder = AxsLanguage::text("COM_LMS_COPY_PASTE_VIDEO", "Copy & Paste Video...");
	        } elseif($activity->type == 'link') {
	        	$textFieldIcon = 'fa-link';
	        	$textFieldPlaceholder = AxsLanguage::text("COM_LMS_COPY_PASTE_URL", "Copy & Paste URL...");
	        } elseif($activity->type == 'esignature') {
	        	$textFieldIcon = 'fa-pencil';
	        	$textFieldPlaceholder = JFactory::getUser()->name;
	        } else {
	        	$textFieldIcon = 'fa-pencil';
	        	$textFieldPlaceholder = AxsLanguage::text("COM_LMS_WRITE_SOMETHING", "Write Something...");
	        }

        }
        $params = new stdClass();
        $params->student_id = JFactory::getUser()->id;
		$params->activity_id = $activity->id;
		$params->lesson_id = $lesson->splms_lesson_id;
		$params->course_id = $lesson->splms_course_id;
        $activityList = AxsLMS::newActivityList($params);
		$studentActivity = $activityList->getActivities($params, 1);
		$studentActivityObject = $studentActivity;
		$canBeRequired = false;

		if($activityTemplate == 'interactive' && $activity->interactive_id && $activity->required) {
			$content_type = AxsLMS::getInteractiveLibrary((int)$activity->interactive_id);
			if(AxsLMS::canBeRequired($content_type)) {
				$canBeRequired = true;
			}
		} elseif($activity->type == 'video' && $activity->required) {
			$content_type = $activity->video_type;
			if(AxsLMS::canBeRequired($content_type)) {
				$canBeRequired = true;
			}
		} elseif($activity->required) {
            if(AxsLMS::canBeRequired("submitted_".$activity->type)) {
                $canBeRequired = true;
            }
        }

		if($activity->required && $canBeRequired) {
			// makes the required button red
			$requiredClass  = "ic_required_active";

			// show incomplete message with red background by default
			$incompleteText = AxsLanguage::text("AXS_INCOMPLETE", "Incomplete");
			$incompleteStyle = "";
			$completedText  = Axslanguage::text("AXS_COMPLETED", "Completed");
			$completedStyle = "style='display:none'";

			// if the activity has been completed prior to pageload

			if($studentActivityObject->completed) {

				// remove the red background from the required button
				$requiredClass  = "";

				// change incomplete message to complete
				// and make the background green
				$completedClass = "ic_completed_active";
				$incompleteStyle = "style='display:none'";
				$completedStyle = "";

			}
			$requiredBox = trim('
				<div class="ic_required_container">
					<div class="ic_required ' . $requiredClass . ' required_' . $activity->id . '">
						' . AxsLanguage::text("COM_SPLMS_AXS_COURSE_QUIZ_REQUIRED", "Required") . '
					</div>
					<div class="ic_completed ' . $completedClass . ' activity_' . $activity->id . '">
						<span class="completed-message" ' . $completedStyle . '>' . $completedText . '</span>
						<span class="incomplete-message" ' . $incompleteStyle . '>' . $incompleteText . '</span>
					</div>
				</div>
				<div class="clearfix"></div>
			');

		}

		if($studentActivity) {
			$formDisplay = 'style= "display: none;"';
			$pendingMessage = '';
			if($activity->required && $activity->required_approval && !$studentActivityObject->completed) {
                $pendingMessage = '</br><span style="color:red; font-size: 13px;">*<i> '. AxsLanguage::text("COM_LMS_PENDING_APPROVAL", "Pending Approval For Completion") .'</i></span>';
            }

			switch ($activityTemplate) {
				case 'textfield':
					$submission  = AxsLanguage::text("COM_LMS_SUBMISSION_SAVED_SUCCESSFULLY", "Your Submission Saved Successfully");
					$submission .= $pendingMessage;
				break;

				case 'upload':
					$submission  = AxsLanguage::text("COM_LMS_ACTIVITY_SUCCESS", "Your Submission Uploaded Successfully");
					$submission .= $pendingMessage;
				break;
			}

		} else {
			$submissionDisplay = 'style= "display: none;"';
		}

		include 'components/com_splms/templates/activity_container.php';
	}

	public static function getLessons($course_id, $divideBySection = false) {

		$teachers_model = FOFModel::getTmpInstance('Teachers', 'SplmsModel');

		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);

		// Select all records from the user profile table where key begins with "custom.".
		// Order it by the ordering field.

		$language = AxsLanguage::getCurrentLanguage()->get('tag');

		$conditions = array(
			$db->quoteName('enabled') . '=' . $db->quote(1),
			$db->quoteName('splms_course_id') . '=' . $db->quote($course_id),
			$db->quoteName('language') . '=' . $db->quote($language)
			//$db->quoteName('section') . '=' . $db->quote('Training Tests')
		);

		$query
			->select('*')
			->from($db->quoteName('#__splms_lessons'))
			->where($conditions)
			->order('ordering ASC');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		foreach ($results as &$result) {
			$result->teacher_name = $teachers_model->getTeacher($result->splms_teacher_id);
			$result->teacher_url  = JRoute::_('index.php?option=com_splms&view=teacher&id='. $result->splms_teacher_id);
			$result->lesson_url = JRoute::_('index.php?option=com_splms&view=lesson&id='.$result->splms_lesson_id.':'.$result->slug . SplmsHelper::getItemid('courses'));
		}

		if ($divideBySection) {

			$sectionResults = array();
			for ($i = 0; $i < count($results); $i++) {
				$section = $results[$i]->section;

				if (!isset($sectionResults[$section])) {
					$sectionResults[$section] = array();
				}

				array_push($sectionResults[$section], $results[$i]);

			}

			return $sectionResults;
		}

		return $results;
	}

	public function getNextAndPrevious($lesson) {
		$db = JFactory::getDbo();
		$query = "SELECT * FROM joom_splms_lessons WHERE splms_lesson_id = '$lesson'";
		$db->setQuery($query);
		$currentLesson = $db->loadObject();

		$query = "SELECT * FROM joom_splms_lessons WHERE splms_course_id = '$currentLesson->splms_course_id' AND enabled = 1 AND ordering > '$currentLesson->ordering' AND language = '$currentLesson->language' ORDER BY ordering ASC LIMIT 1";
		$db->setQuery($query);
		$nextLesson = $db->loadObject();

		$query = "SELECT * FROM joom_splms_lessons WHERE splms_course_id = '$currentLesson->splms_course_id' AND enabled = 1 AND ordering < '$currentLesson->ordering' AND language = '$currentLesson->language' ORDER BY ordering DESC LIMIT 1";
		$db->setQuery($query);
		$previousLesson = $db->loadObject();

		$result = new stdClass();
		$result->next = $nextLesson;
		$result->previous = $previousLesson;

		return $result;
	}

	public function getLessonTeachers($lesson) {

		$ids = array();

		$ids[] = $lesson->splms_teacher_id;
		if($lesson->teacher_materials){
			$ids = array_merge($ids, json_decode($lesson->teacher_materials)->splms_teacher_id);
		}

		$ids = array_unique($ids);
		$ids = implode(',',$ids);

		if ($ids) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$conditions = array(
				$db->qn('enabled').'='.(int)1,
				$db->qn('splms_teacher_id') . ' IN (' . $ids . ')'
			);
			$query->select('*')
				->from($db->qn('#__splms_teachers'))
				->where($conditions)
				->order('ordering DESC');
			$db->setQuery($query);
			$items = $db->loadObjectList();
		} else {
			$items = array();
		}
		return $items;
	}


	// Get Free Lessons of course by course ID
	public static function getCourseFreeLesson($course_id) {
		$db = JFactory::getDbo();
		// Create a new query object.
		$query = $db->getQuery(true);

		// Select all records from the user profile table where key begins with "custom.".
		// Order it by the ordering field.

		$language = AxsLanguage::getCurrentLanguage()->get('tag');

		$conditions = array(
			$db->quoteName('enabled') . '=' . $db->quote(1),
			$db->quoteName('lesson_type') . '=' . $db->quote(0),
			$db->quoteName('splms_course_id') . '=' . $db->quote($course_id),
			$db->quoteName('language') . '=' . $db->quote($language)
		);

		$query
			->select('*')
			->from($db->quoteName('#__splms_lessons'))
			->where($conditions)
			->setLimit(1)
			->order('ordering ASC');

		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		return $results;
	}

	// Get Lessons By Teacher ID (Teachers Lessons)
	public static function getTeacherLessons($teacher_id) {

		$courses_model = FOFModel::getTmpInstance('Courses', 'SplmsModel');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('enabled') . '=' . $db->quote(1)
		);

		$query
			->select('*')
			->from($db->quoteName('#__splms_lessons'))
			->where($conditions)
			->order('ordering DESC');

		// Reset the query using our newly populated query object.
		$db->setQuery($query);

		// Load the results as a list of stdClass objects (see later for more options on retrieving data).
		$results = $db->loadObjectList();

		$lesson_list = array();
		foreach ($results as $result) {

			$teacherList = SplmsModelCourses::getTeacherMaterialArray($result->splms_lesson_id);

			$in_list = false;
			foreach($teacherList as $list) {
				if ($teacher_id == $list['teacher_id']) {
					$in_list = true;
				}
			}

			if ($in_list) {
				$result->lesson_url = JRoute::_('index.php?option=com_splms&view=lesson&id='.$result->splms_lesson_id.':'.$result->slug . SplmsHelper::getItemid('courses'));
				$result->get_course_info = $courses_model->getCourse($result->splms_course_id);

				array_push($lesson_list, $result);
			}
		}
		return $lesson_list;
	}

	// build query
	public function buildQuery($overrideLimits = false){

		// BEGIN:: Query
		$table = $this->getTable();
		$tableName = $table->getTableName();
		$tableKey = $table->getKeyName();
		$db = $this->getDbo();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName($tableName));

		//Frontend
		if(FOFPlatform::getInstance()->isFrontend()) {
			// Get Params
			$app = JFactory::getApplication();
			$params = $app->getMenu()->getActive()->params; // get the active item
			//Enabled

			$conditions = array(
				$db->quoteName('enabled') .'='. $db->quote(1)
			);

			$query
				->where($conditions)
				->order('ordering DESC');

		}

		return $query;
	}

	// check file type
	protected static function fileExtensionCheck($file, $allowed){
		$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		if(in_array( strtolower($ext), $allowed) ) {
			return true;
		}
		return false;
	}

	protected function onBeforeSave(&$model, &$data){

		$jinput = JFactory::getApplication()->input;
		$file = $jinput->files->get('attachment');

		$lesson_id = $model['splms_lesson_id'];

		if ( $file['name']!= '' && $file['name']) {

			if ( self::fileExtensionCheck($file, array('png', 'jpg', 'pdf', 'zip') )) {
				self::$files['attachment'] = $file;
			} else {
				throw new RuntimeException(JText::_('INVALID_FILE_TYPE'), 1);
			}

			// has acchatment before
			$exist_attachment = '';
			$exist_attachment = self::existAttachment($lesson_id)->attachment;

			// delete attachment
			if ($exist_attachment != '') {
				JFile::delete(JPATH_ROOT. '/media/com_splms/lessons/attachments/' .$exist_attachment);
			}
		}

		// unset attachment
		unset($model['attachment']);

		return true;
	}

	protected function onAfterSave(&$model){

		$app = JFactory::getApplication();
		$jinput = JFactory::getApplication()->input;
		// Get Lesson ID
		$lesson_id = $model->splms_lesson_id;
		// Get attachment file

		$attachment_file = (self::$files) ? self::$files['attachment'] : '' ;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// has file
		if ($attachment_file != '') {

			$folder_path = JPATH_ROOT . '/media/com_splms/lessons/attachments/' . $lesson_id;

			if (!JFolder::exists( $folder_path )) {
				JFolder::create($folder_path);
			}

			// Cleans the name of teh file by removing weird characters
			$filename = JFile::makeSafe($attachment_file['name']);

			$src = $attachment_file['tmp_name'];
			//JFolder::create($folder_path);

			if (JFile::upload($src, $folder_path. '/' .$attachment_file['name'])) {
				// Fields to update.
				$fields = array(
				    $db->quoteName('attachment') . ' = ' . $db->quote('/' . $lesson_id . '/' . $attachment_file['name'] )
				);

				$conditions = array(
					$db->quoteName('splms_lesson_id') . ' = ' . $db->quote($lesson_id)
				);
				//$query->update($db->quoteName('#__splms_lessons'))->set('attachment')->where();
				$query
					->update($db->quoteName('#__splms_lessons'))
					->set($fields)
					->where($conditions);

				$db->setQuery($query);
				$result = $db->execute();

			} else {
				return JFactory::getApplication()->enqueueMessage('Couldn\'t update file ', 'error');
			}
		} // END:: has file


		return true;

	}
}