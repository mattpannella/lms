<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsModelTeachers extends FOFModel {

	static $loaded = array();

	// Get teachers
	public static function getTeacher($id) {

		if (isset(static::$loaded[__METHOD__][$id])) {
			if (static::$loaded[__METHOD__][$id] == 'null') {
				return null;
			}
			return static::$loaded[__METHOD__][$id];
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Order it by the ordering field.

		$conditions = array(
			$db->quoteName('enabled') . '=' . $db->quote(1),
			$db->quoteName('splms_teacher_id') . '=' . $db->quote($id)
		);

		$query
			->select('*')
			->from($db->quoteName('#__splms_teachers'))
			->where($conditions)
			->order('ordering DESC');

		$db->setQuery($query);
		$result = $db->loadObject();

		if ($result != '') {
			$result->url = JRoute::_('index.php?option=com_splms&view=teacher&id='.$result->splms_teacher_id.':'.$result->slug . SplmsHelper::getItemid('teachers'));
		}

		if ($result == null) {
			static::$loaded[__METHOD__][$id] = 'null';
		} else {
			static::$loaded[__METHOD__][$id] = $result;
		}

		return $result;
	}

	// build query
	public function buildQuery($overrideLimits = false){

		// BEGIN:: Query
		$table = $this->getTable();
		$tableName = $table->getTableName();
		$tableKey = $table->getKeyName();
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('*')->from($db->qn($tableName));
		//Frontend
		if(FOFPlatform::getInstance()->isFrontend()) {
			// Get Params
			$app = JFactory::getApplication();
			$params   = $app->getMenu()->getActive()->params; // get the active item
			//Enabled
			$query->where($db->qn('enabled')." = ".$db->quote('1'));

			// ordering
			$query->order('ordering DESC');

		}

		// Call the behaviors
		//$this->modelDispatcher->trigger('onAfterBuildQuery', array(&$this, &$query));

		return $query;

	}

}