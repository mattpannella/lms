<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsModelAccuratethoughts extends FOFModel {

	public static function getAccurateThoughts($user_id, $course_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('course_id') . '=' . $db->quote($course_id),
			$db->quoteName('user_id') . '=' . $db->quote($user_id)
		);

		$query
			->select('*')
			->from($db->quoteName('#__splms_accuratethoughts'))
			->where($conditions);
			
		$db->setQuery($query);

		$results = $db->loadObjectList();
		return $results;
	}
}