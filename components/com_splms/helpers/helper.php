<?php

defined('_JEXEC') or die;

class SplmsHelper {

	static $loaded = array();

	public static function getItemid($view = 'courses') {

		$sig = md5(serialize(array($view)));

		if (isset(static::$loaded[__METHOD__][$sig]))
		{
			return static::$loaded[__METHOD__][$sig];
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select($db->quoteName(array('id')));
		$query->from($db->quoteName('#__menu'));
		$query->where($db->quoteName('link') . ' LIKE '. $db->quote('%option=com_splms&view='. $view .'%'));
		$query->where($db->quoteName('published') . ' = '. $db->quote('1'));
		$db->setQuery($query);
		$result = $db->loadResult();

		if(is_countable($result) && count($result) > 0) {
			static::$loaded[__METHOD__][$sig] = '&Itemid=' . $result;

			return '&Itemid=' . $result;
		}

		static::$loaded[__METHOD__][$sig] = 'not found';

		return;
	}

	public static function getCartView() {

		$session = JFactory::getSession();
		$orders = $session->get('splms_orders');
		$cards = $session->get('splms_cards');
		$brand = AxsBrands::getBrand();
		//Set Defaults for currency
		$currency_code = 'USD';
		$currency_symbol = '$';
        if($brand->billing->currency_code) {
            $currency_code = $brand->billing->currency_code;
		}
		if($brand->billing->currency_symbol) {
            $currency_symbol = $brand->billing->currency_symbol;
        }
		$db = JFactory::getDBO();

		if (count($orders) > 0) {

			//Create a query to get the information on all of the items being purchased.
			$cond = array();
			foreach ($orders as $order) {
				$cond[]= $db->quoteName("splms_course_id") . "=" . (int)$order['product_id'];
			}

			$query = $db->getQuery(true);
			$query
				->select('splms_course_id AS product_id,price,title AS product_name')
				->from($db->quoteName('joom_splms_courses'))
				->where($cond, 'or');

			$db->setQuery($query);
			$carts = $db->loadObjectList();
		} else {
			$carts = null;
		}

		$user = JFactory::getUser();

		$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
        $security = AxsSecurity::getSettings();
        if($security->twofactor_type == 'yubikey') {
            $sercretKeyInputType = 'password';
        } else {
            $sercretKeyInputType = 'text';
        }


		ob_start();

		?>
		<br/>
		<style>
			body {
				background-color: #eee !important;
			}

			.checkout-box {
				position: relative;
				border-radius: 10px;
				background-color: #fff;
				-webkit-box-shadow: 0 10px 30px 0 rgba(0,0,0,.1);
				box-shadow: 0 10px 30px 0px rgba(0,0,0,.1);
				padding: 20px 20px 20px 20px;
				margin-bottom: 25px;
			}

			.checkout-box input {
				margin-bottom: 15px;
			}

			.bl_button {
				padding: 7px 15px 7px 15px;
				border: 1px solid #283848;
				font-size: 14px;
				text-align: center;
				margin-top: 10px;
				border-radius: 50px;
				text-decoration: none;
				transition: ease .5s all;
				margin: 0px;
				text-shadow: none;
				cursor: pointer;
				display: inline-block;
				color: #283848;
				background: #fff;
				margin-bottom: 20px;
				height: 36px;
			}

			.bl_button:hover,
			.bl_button_active {
				color: #fff;
				background: #283848;
			}

			.passswordMismatchMessage {
				color: red;
				display: none;
			}
		</style>
		<div class="container">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<h2><?php echo AxsLanguage::text("COM_SPLMS_ORDER_CHECKOUT", "Checkout") ?></h2>
				<div id="splms" class="splms splms-view-cart">
					<?php
						if (empty($carts) || $carts == null) {
					?>
							<h3>
								<?php echo AxsLanguage::text("COM_SPLMS_NO_ITEM_IN_CART", "Your cart is empty"); ?>
							</h3>
					<?php
						} else {
					?>		<div class="checkout-box">
								<table id="purchaselist" class="table table-striped">
									<tr>
										<th><?php echo AxsLanguage::text("COM_SPLMS_CART_COURSE_NAME", "Item"); ?></th>
										<th><?php echo AxsLanguage::text("COM_SPLMS_CART_COURSE_PRICE", "Price"); ?></th>
									</tr>

									<?php
										$items = $carts;
										$total = 0;
										foreach ($items as $item) {
									?>
											<tr>
												<td><?php echo $item->product_name; ?></td>
												<td>
													<span>
														<?php echo $currency_symbol; ?>
														<span
															data-id="<?php echo $item->product_id?>"
															class="itemPrice"
														>
															<?php echo $item->price; $total += (float)$item->price; ?>
													</span>
													</span>

													<button
														class="btn btn-danger btn-xs btn-remove-course pull-right"
														which="<?php echo $item->product_id?>"
														helper="<?php echo SplmsHelper::getItemid('cart')?>"
													>
														<i class="splms-icon-error"></i>
													</button>
												</td>
											</tr>

									<?php
										}
									?>

									<tr>
										<td><?php echo AxsLanguage::text("COM_SPLMS_COMMON_TOTAL", "Total") ?>:</td>
										<?php setlocale(LC_MONETARY,"en_US"); ?>
										<td><?php echo $currency_symbol; ?><span id="totalPrice"><?php echo number_format($total, 2); ?></span></td>
									</tr>
								</table>
							</div>
							<?php if(!$user->id) { ?>
							<div class="row">
								<div class="col-md-6">
									<a
										id="login-box-button"
										class="bl_button bl_button_active"
										style="width:100%;"
									><?php echo AxsLanguage::text("COM_SPLMS_CART_ALREADY_HAVE_ACCOUNT", "Already have an account") ?></a>
								</div>
								<div class="col-md-6">
									<a
										id="register-box-button"
										class="bl_button"
										style="width:100%;"
									><?php echo AxsLanguage::text("COM_SPLMS_CART_CREATE_ACCOUNT", "Create Account") ?></a>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="checkout-box">
								<?php
								$uri = JFactory::getURI();
								$pageURL = base64_encode($uri->toString());
								?>

								<div class="cart-tovuti-login-box">
									<div class="cart-tovuti-login-form">
										<div id="cart-login-error-message" style="display: none;"></div>
										<form action="#" method="post" id="cart-login-form" aria-label="Login Form">
											<div class="userdata form-group">
													<div class="col-md-6">
														<label
															for="username"
															class="control-label"
														><?php echo AxsLanguage::text("JGLOBAL_USERNAME", "Username") ?></label>
														<input
															id="cart-modlgn-username"
															aria-label="<?php echo AxsLanguage::text("AXS_ENTER_USERNAME_OR_EMAIL", "Enter Your Username or Email") ?>"
															class="form-control"
															type="text"
															name="username"
															tabindex="0"
														/>
													</div>

													<div class="col-md-6">
														<label for="password" class="control-label"><?php echo AxsLanguage::text("JGLOBAL_PASSWORD", "Password") ?></label>
														<input
															id="cart-modlgn-passwd"
															aria-label="<?php echo AxsLanguage::text("AXS_ENTER_PASSWORD", "Enter Your Password") ?>"
															type="password"
															name="password"
															tabindex="0"
															class="form-control"
														/>
													</div>
													<div class="clearfix" style="margin-bottom:20px;"></div>
													<?php if (count($twofactormethods) > 1) { ?>
														<div class="col-md-12" id="secretkey-wrap" style="display:none;">
															<input type="<?php echo $sercretKeyInputType; ?>" aria-label="<?php echo AxsLanguage::text("JGLOBAL_SECRETKEY", "Security Key"); ?>" class="form-control" name="cart-modlgn-secretkey" data-name="cart-modlgn-secretkey"  id="cart-modlgn-secretkey" placeholder="<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key") ?>">
														</div>
													<?php }	?>
													<div class="col-md-6">
														<button style="width:100%;" type="submit" aria-label="Click to Login" tabindex="0" name="Submit" class="btn btn-success login-button"><?php echo AxsLanguage::text("JLOGIN", "Login") ?></button>
													</div>
													<div class="col-md-6" style="font-size: 13px;">
															<a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=reset"); ?>">
																<?php echo AxsLanguage::text("AXS_FORGOT_PASSWORD", "Forgot your password?") ?>
															</a>
															<br/>
															<a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=remind"); ?>">
																<?php echo AxsLanguage::text("AXS_FORGOT_USERNAME", "Forgot your username?") ?>
															</a>
													</div>
													<div class="clearfix"></div>
												<input
													id="cart-return"
													value="<?php echo $pageURL; ?>"
													type="hidden"
													name="return"
												/>
												<?php echo JHtml::_('form.token'); ?>
											</div>
										</form>
									</div>
								</div>

								<div class="cart-tovuti-register-box" style="display: none;">
									<div class="cart-tovuti-register-form">
										<div id="cart-register-error-message" style="display: none;"></div>
										<?php if(!$user->id) { ?>
										<form id="payment-form" data-gtw="<?php echo $brand->billing->gateway_type; ?>">
										<?php } ?>
											<div class="form-group">
												<div class="col-md-6">
													<label for="register_firstname" class="control-label"><?php echo AxsLanguage::text("AXS_FIRST_NAME", "First Name"); ?></label>
													<input  aria-label="Enter Your First Name" id="cart-register-firstname" class="form-control" type="text" name="register_firstname" tabindex="0" />
												</div>
												<div class="col-md-6">
													<label for="register_lastname" class="control-label"><?php echo AxsLanguage::text("AXS_LAST_NAME", "Last Name"); ?></label>
													<input aria-label="Enter Your Last Name" id="cart-register-lastname" type="text" name="register_lastname"  tabindex="0"  class="form-control" />
												</div>
												<div class="clearfix"></div>
												<div class="col-md-6">
													<label for="register_email" class="control-label"><?php echo AxsLanguage::text("AXS_EMAIL", "Email"); ?></label>
													<input aria-label="Enter Your Email" id="cart-register-email" class="form-control" type="text" name="register_email" tabindex="0" />
												</div>
												<div class="col-md-6">
													<label for="register_username" class="control-label"><?php echo AxsLanguage::text("AXS_USERNAME", "Username"); ?></label>
													<input  aria-label="Enter Your Username" id="cart-register-username" class="form-control" type="text" name="register_username" tabindex="0" />
												</div>
												<div class="clearfix"></div>
												<div class="passswordMismatchMessage"></div>
												<div class="col-md-6">
													<label for="register_password" class="control-label"><?php echo AxsLanguage::text("AXS_PASSWORD", "Password"); ?></label>
													<input aria-label="Enter Your Password" id="cart-register-password" type="password" name="register_password"  tabindex="0"  class="form-control" />
												</div>
												<div class="col-md-6">
													<label for="register_password2" class="control-label"><?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password"); ?></label>
													<input aria-label="Confirm Password" id="cart-register-password2" type="password" name="register_password2"  tabindex="0"  class="form-control" />
												</div>
												<div style="margin-left:17px">
												<?php echo AxsBrands::renderAgreementCheckbox($submit_button_id = 'payment-form-submit'); ?>
												</div>
												<div class="clearfix"></div>
											</div>
									</div>
								</div>

								<script>
									var pageURL = '<?php echo base64_encode($pageURL); ?>';

									$('input[name="register_password2"').keyup(function() {
										var pass2 = $(this).val();
										var pass1 = $('input[name="register_password"').val();
										if(pass2.length > 2) {
											if(pass2 != pass1) {
												var passswordMismatchMessage = '<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH", "Passwords do not match.") ?>';
												$('.passswordMismatchMessage').text(passswordMismatchMessage);
												$('.passswordMismatchMessage').fadeIn(300);
											} else {
												$('.passswordMismatchMessage').fadeOut(300);
												$('.passswordMismatchMessage').text('');
											}
										}
									});

									$('#login-box-button').click(function(){
										$('.cart-tovuti-register-box').hide();
										$('.cart-tovuti-login-box').show();
										$('#pay-info').hide();
										$('.bl_button_active').removeClass('bl_button_active');
										$(this).addClass('bl_button_active');
									});

									$('#register-box-button').click(function(){
										$('.cart-tovuti-login-box').hide();
										$('.cart-tovuti-register-box').show();
										$('#pay-info').show();
										$('.bl_button_active').removeClass('bl_button_active');
										$(this).addClass('bl_button_active');
									});

									$("#cart-login-form").submit( function(e) {
										e.preventDefault();
										$('.cart-tovuti-login-form button').css('background','#777').attr('disabled','true');
										//var data = $(this).serialize();
										var data = {
											username: $("#cart-modlgn-username").val(),
											password: $("#cart-modlgn-passwd").val(),
											secretkey: $("#cart-modlgn-secretkey").val(),
											return: $("#cart-return").val()
										}
										if($("#cart-login-error-message:visible")) {
											$("#cart-login-error-message").hide();
										}

										$.ajax({
											url: '/index.php?option=com_axs&task=popupmodule.loginAuthenticate&format=raw',
											type: 'post',
											data: data
										}).done(
											function(result) {
												var response = JSON.parse(result);
												if(response.secretKeyVerification) {
													$('.cart-tovuti-login-form button')
													.css('background','#4CAF50')
													.removeAttr('disabled');
													$("#cart-login-error-message")
														.html(response.message);
													if ($("#cart-login-error-message:hidden")) {
														$("#cart-login-error-message").slideToggle();
													}
													if($("#secretkey-wrap").is(':hidden')) {
														$('#secretkey-wrap').slideToggle();
													}
													return;
												}
												if (response.status == "success") {
													if(response.secretKeyVerification) {
														$('.cart-tovuti-login-form button')
														.css('background','#4CAF50')
														.removeAttr('disabled');
														$("#cart-login-error-message")
															.html(response.message);
														if ($("#cart-login-error-message:hidden")) {
															$("#cart-login-error-message").slideToggle();
														}
														$('#secretkey-wrap').slideToggle();
														return;
													}
													$('#secretkey-wrap').slideToggle();
													return;
												}
												if (response.status == "success") {
													$.ajax({
														url: '/index.php?option=com_users&task=user.login',
														type: 'post',
														data: data
													}).done( function() {
														window.location = response.url;
													});

												} else {
													$('.cart-tovuti-login-form button')
														.css('background','#4CAF50')
														.removeAttr('disabled');
													$("#cart-login-error-message")
														.html(response.message);
													if ($("#cart-login-error-message:hidden")) {
														$("#cart-login-error-message").slideToggle();
													}
												}
										});
									});
								</script>
								<div class="clearfix"></div>
							</div>
							<?php } ?>

							<div class="checkout-box" id="pay-info" <?php if(!$user->id) { echo 'style="display:none;"'; } ?>>
								<div class="tovuti-row">
									<div id="converge-module">
										<?php if($user->id) { ?>
										<form id="payment-form" data-gtw="<?php echo $brand->billing->gateway_type; ?>">
										<?php } ?>
											<div class="form-group" id="promoGroup">
												<div class="col-md-12">
													<label for="promo" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_PROMO_CODE'); ?></label>
												</div>
												<div class="col-md-8">
													<input type="text" class="form-control" name="promo" />
												</div>
												<div class="col-md-4">
													<button style="width: 100%" id="applyPromo" type="button" class="btn btn-success" data-courseid="<?php echo $item->product_id; ?>">
														<!-- <i class="fa fa-tag"></i> -->
														<?php echo AxsLanguage::text("COM_SPLMS_AXS_APPLY", "Apply"); ?>
													</button>
												</div>
												<div class="clearfix"></div>
												<span id="promoHelpText" class="help-block"></span>
											</div>
											<div class="col-md-12" id="payment_information">
												<?php if($cards) { ?>
												<div class="form-group">
													<label for="card_on_file" class="control-label">
														<?php echo AxsLanguage::text("COM_SPLMS_AXS_USE_CARD_ON_FILE", "Use a card on file"); ?>
													</label>
													<select id="card_on_file" name="card_on_file" class="form-control">
														<option value="">--<?php echo AxsLanguage::text("COM_SPLMS_AXS_SELECT_CARD", "select card"); ?>--</option>
														<?php foreach($cards as $card) { ?>
															<option value="<?php echo $card->id; ?>">
																<?php echo AxsLanguage::text("COM_SPLMS_AXS_ENDING_IN_PRETEXT", "Ending in") . ' ' . $card->cardNumber; ?>
															</option>
														<?php } ?>
													</select>
												</div>
												<p><?php echo AxsLanguage::text("AXS_OR", "or") ?></p>
												<div class="form-group">
													<div class="">
														<button id="card-drop-down" class="btn btn-default" type="button">
															<?php echo AxsLanguage::text("COM_SPLMS_AXS_USE_NEW_CARD", "Use a new card"); ?>
														</button>
													</div>
												</div>
												<?php } ?>
												<div id="new-card-fields" <?php if($cards) { echo 'style="display: none;"'; } ?>>

													<?php
														if($brand->billing->gateway_type == 'stripe') {
															$stripeForm = AxsPayment::buildStipePaymentForm();
													?>
															<div class="form-group">

																<div>
																	<?php echo $stripeForm; ?>
																</div>
															</div>
													<?php
														} else {
													?>
													<div class="form-group">
														<label for="card-number" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_CARD_NUMBER", "Card Number");?></label>
														<input id="card-number" name="cardNumber" class="form-control">
													</div>
													<div class="form-group">
														<label for="exp-date" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_CARD_EXPIRATION_DATE", "Expiration Date");?></label>
														<div class="col-md-9">
															<select name="exp_month" id="exp_month" class="form-control" style="width:40%; display:inline-block;">
																<?php
																	for ($i = 1; $i <= 12; $i++) {
																		if ($i < 10) {
																			$month = "0" . $i;
																		} else {
																			$month = $i;
																		}
																?>
																	<option value="<?php echo $i; ?>"><?php echo $month; ?></option>
																<?php
																	}
																?>
															</select>
															/ <select name="exp_year" id="exp_year" class="form-control" style="width:50%; display:inline-block;">
																<?php
																	$year = (int)date("Y");

																	for ($y = $year; $y <= $year + 10; $y++) {
																?>
																	<option value="<?php echo $y;?>">
																		<?php echo $y;?>
																	</option>
																<?php
																	}
																?>
															</select>
														</div>
													</div>
													<div class="form-group">
														<label for="cvv-code" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_CVV_CODE", "CVV code") ?></label>
														<input id="cvv-code" name="cvv" class="form-control">
													</div>
													<h4><?php echo AxsLanguage::text("COM_SPLMS_AXS_BILLING_ADDRESS", "Billing Address") ?></h4>
													<div class="form-group">
														<label for="fname" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_FIRST_NAME", "First Name") ?></label>
														<input id="fname" name="first" class="form-control">
													</div>
													<div class="form-group">
														<label for="lname" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_LAST_NAME", "Last Name") ?></label>
														<input id="lname" name="last" class="form-control">
													</div>

													<div class="form-group">
														<label for="addr" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_ADDRESS", "Address") ?></label>
														<input id="addr" name="address" class="form-control">
													</div>
													<div class="form-group">
														<label for="city" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_CITY", "City") ?></label>
														<input id="city" name="city" class="form-control">
													</div>
													<div class="form-group">
														<label for="state" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_STATE", "State") ?></label>
														<input id="state" name="state" class="form-control">
													</div>
													<div class="form-group">
														<label for="zip" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_ZIP", "Zip") ?></label>
														<input id="zip" name="zip" class="form-control">
													</div>
													<div class="form-group">
														<label for="cty" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_COUNTRY", "Country") ?></label>
														<input id="cty" name="country" class="form-control" value="United States">
													</div>
													<div class="form-group">
														<label for="phone" class="control-label"><?php echo AxsLanguage::text("COM_SPLMS_AXS_PHONE", "Phone") ?></label>
														<input id="phone" name="phone" class="form-control">
													</div>
													<?php  } //end of stripe if statement ?>
													<?php
													/*
													<div class="form-group">
														<label for="email" class="control-label"><?php echo JText::_('COM_SPLMS_AXS_EMAIL') ?></label>
														<input id="email" name="email" class="form-control">
													</div>
													*/
													?>

												</div>

												<div class="clearfix"></div>
											</div>
											<div class="form-group">
												<div class="col-md-7">
													<button style="width: 100%;" id="payment-form-submit" type="submit" class="btn btn-success btn-lg">
														<?php echo AxsLanguage::text("COM_SPLMS_CART_PURCHASE_COURSES", "Purchase Courses"); ?>
													</button>
												</div>
												<div class="col-md-5">
													<a
														style="width: 100%;"
														href="<?php echo JRoute::_('index.php?option=com_splms&view=courses'); ?>"
														class="btn btn-lg btn-default"
													><?php echo AxsLanguage::text("COM_SPLMS_CART_CONTINUE_SHOPPING", "Continue Shopping") ?></a>
												</div>
												<div class="clearfix"></div>
											</div>

											<input type="hidden" name="task" value="ajax">
											<input type="hidden" name="format" value="raw">
											<div class="clearfix"></div>
										</form>
									</div>
								</div>
							<div class="clearfix"></div>
						</div>
					<?php
						}
					?>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>


		<script>

			var text_promo_code_not_valid = "<?php echo JText::_('COM_SPLMS_AXS_PROMO_CODE_NOT_VALID')?>";
			var text_print_receipt = "<?php echo JText::_('COM_SPLMS_AXS_PRINT_RECEIPT')?>";
			var text_transaction_successful = "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_SUCCESSFUL');?>";
			var text_transaction_failed = "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_FAILED');?>";
			var text_transaction_declined = "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_DECLINED');?>";
			var text_transaction_retry =  "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_RETRY');?>";
			var text_transaction_id = "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_ID');?>";
			var text_purchase_date = "<?php echo JText::_('COM_SPLMS_AXS_PURCHASE_DATE');?>";
			var text_begin_course = "<?php echo JText::_('COM_SPLMS_AXS_BEGIN_COURSE');?>";
			var text_unable_to_remove_alert = "<?php echo JText::_('COM_SPLMS_AXS_UNABLE_TO_REMOVE_ITEM');?>";
			var text_transaction_fail_reason = "<?php echo JText::_('COM_SPLMS_AXS_TRANSACTION_FAILED_REASON');?>";
			var currency_symbol = "<?php echo $currency_symbol; ?>";

			$('#applyPromo').click(function() {
				var button = $(this);
				var coursePriceElements = $('.itemPrice');
				var courseids = [];

				$.each(coursePriceElements, function(index, coursePrice) {

					courseids.push($(coursePrice).data('id'));
				});

				button.html('<img src="../images/ajax-loader.gif" /> Verifying');
				$.ajax({
					url: '/index.php?option=com_splms&task=cart.verifyPromo&format=raw',
					data: {
						promo: $('input[name=promo]').val(),
						course_ids: courseids
					},
					type: 'post'
				}).done(function(results) {
					try {
						var verificationResults = JSON.parse(results);
						var promoVerificationStatus = 'error';
						var totalDifference = 0;

						// Update the item amounts and verification message
						$.each(verificationResults, function(index, verificationResult) {
							var promoCodeInvalidForAll = true;
							var itemPriceElement = $(".itemPrice[data-id=" + index + "]");

							if(verificationResult.result == 'success') {

								var originalAmount = itemPriceElement.text();

								itemPriceElement.html(verificationResult.item_amount.toFixed(2));
								$('#promoHelpText').html(verificationResult.message);
								promoVerificationStatus = verificationResult.result;

								totalDifference += originalAmount - verificationResult.item_amount;

								promoCodeInvalidForAll = false;
							}

							if(promoCodeInvalidForAll) {

								$('#promoHelpText').html(verificationResult.message);
								promoVerificationStatus = verificationResult.result;
							}
						});

						// Remove any commas so we can use this number in calculations
						var totalPrice = $('#totalPrice').text().replace(',','');
						var newPrice = totalPrice - totalDifference;

						if(newPrice == 0) {
							$('#payment_information').hide();
						} else {
							$('#payment_information').show();
						}

						$('#totalPrice').text((newPrice).toFixed(2));

						$('#promoGroup').removeClass().addClass('form-group has-' + promoVerificationStatus);
					} catch(e) {
						$('#promoHelpText').html(text_promo_code_not_valid);
						$('#promoGroup').removeClass().addClass('form-group has-error');
					}
					button.html('Apply');
				}).fail(function() {
					$('#promoHelpText').html(text_promo_code_not_valid);
					$('#promoGroup').removeClass().addClass('form-group has-error');
					button.html('Apply');
				});
			});

			$('#card-drop-down').click(function() {
				$('#new-card-fields').slideToggle();
			});

			function processPayment() {
				$('#payment-form-submit').prop('disabled', true);
				var value = $("#payment-form").serialize();
				$.ajax({
					data : value,
					type: 'POST'
				}).done(function (data) {
					data = JSON.parse(data);
					switch(data.status) {
						case "SUC":

							var newDiv = document.createElement('div');
							newDiv.className = "print-area";

							var header = document.createElement('h1');
							header.appendChild(document.createTextNode(text_transaction_successful));

							var receiptButton = document.createElement('button');
							receiptButton.appendChild(document.createTextNode(text_print_receipt));
							$(receiptButton).css({
								"float" : "right",
								"margin-top": "20px"
							});
							receiptButton.className = "btn btn-primary";
							receiptButton.setAttribute("onclick", "window.print()");

							var newTable = document.createElement('table');
							newTable.className = "table";
							var tableBody = document.createElement('tbody');



							newTable.appendChild(tableBody);

							var tableRow = document.createElement('tr');
							var tableData = document.createElement('td');

							tableData.appendChild(document.createTextNode(text_transaction_id));
							tableRow.appendChild(tableData);

							tableData = document.createElement('td');
							tableData.appendChild(document.createTextNode(data.id));
							tableRow.appendChild(tableData);

							newTable.appendChild(tableRow);

							tableRow = document.createElement('tr');
							tableData = document.createElement('td');

							tableData.appendChild(document.createTextNode('Amount'));
							tableRow.appendChild(tableData);

							tableData = document.createElement('td');
							tableData.appendChild(document.createTextNode(currency_symbol+data.amount));
							tableRow.appendChild(tableData);

							newTable.appendChild(tableRow);

							tableRow = document.createElement('tr');
							tableData = document.createElement('td');

							tableData.appendChild(document.createTextNode(text_purchase_date));
							tableRow.appendChild(tableData);

							tableData = document.createElement('td');
							tableData.appendChild(document.createTextNode(currDate()));
							tableRow.appendChild(tableData);

							newTable.appendChild(tableRow);

							var beginCourseButton = document.createElement('button');
							beginCourseButton.appendChild(document.createTextNode(text_begin_course));
							beginCourseButton.id = "back-to-course";
							beginCourseButton.className = "btn btn-primary pull-right btn-lg";
							beginCourseButton.setAttribute("onclick", "history.go(-1)");

							$(beginCourseButton).css({
								"margin-bottom": "30px"
							});

							newDiv.appendChild(header);
							newDiv.appendChild(receiptButton);
							newDiv.appendChild(newTable);
							newDiv.appendChild(beginCourseButton);
							document.getElementById("converge-module").innerHTML = "";
							document.getElementById("converge-module").appendChild(newDiv);
							document.getElementById("purchaselist").innerHTML = "";

							break;

						case "FAIL":
							alert(data.error.replaceAll("<br><hr>","\n"))
							console.log(data); console.log('failed');
						break;
						case "DEC":

							if (data.status === "FAIL") {
								var header_text = text_transaction_failed;
							} else if (data.status === "DEC") {
								var header_text = text_transaction_declined;
							}

							var newDiv = document.createElement('div');
							var header = document.createElement('h1');
							header.appendChild(document.createTextNode(header_text));

							var newTable = document.createElement('table');
							newTable.className = "table";
							var tableBody = document.createElement('tbody');
							newTable.appendChild(tableBody);

							var tableRow = document.createElement('tr');
							var tableData = document.createElement('td');

							tableData.appendChild(document.createTextNode(text_transaction_fail_reason));
							tableRow.appendChild(tableData);
							tableData = document.createElement('td');
							tableData.appendChild(document.createTextNode(data.message));

							var retryButton = document.createElement('button');
							retryButton.id = "back-to-course";
							retryButton.className = "btn btn-primary pull-right btn-lg";
							retryButton.appendChild(document.createTextNode(text_transaction_retry));
							$(retryButton).css({
								"margin-bottom" : "30px"
							});
							retryButton.setAttribute("onclick", "location.reload()");

							newDiv.appendChild(header);
							newDiv.appendChild(newTable);
							newDiv.appendChild(retryButton);

							document.getElementById("converge-module").innerHTML = "";
							document.getElementById("converge-module").appendChild(newDiv);

							/*$('#converge-module').html(
								'<h1>Transaction Failed</h1>' +
								'<table class="table">' +
								'<tbody>' +
								'<tr><td>Reason</td><td>'+data.message+'</td></tr>' +
								'</tbody>' +
								'</table>' +
								'<button id="back-to-course" class="btn btn-primary pull-right btn-lg" style="margin-bottom: 30px;" onclick="location.reload();">Retry</button>'
							);*/
							break;

					}
					$('#payment-form-submit').prop('disabled', false);
				}).fail(function() {
					alert(text_failed_to_process_transaction);
					$('#payment-form-submit').prop('disabled', false);
				});
			}

			function stripeTokenHandler(token) {
	            // Insert the token ID into the form so it gets submitted to the server
	            var form = document.getElementById('payment-form');
	            var hiddenInput = document.createElement('input');
	            hiddenInput.setAttribute('type', 'hidden');
	            hiddenInput.setAttribute('name', 'stripeToken');
	            hiddenInput.setAttribute('value', token.id);
	            form.appendChild(hiddenInput);
	            var hiddenInputLastFour = document.createElement('input');
	            hiddenInputLastFour.setAttribute('type', 'hidden');
	            hiddenInputLastFour.setAttribute('name', 'last4');
	            hiddenInputLastFour.setAttribute('value', token.card.last4);
	            form.appendChild(hiddenInputLastFour);
	            processPayment();
	        }

			function validateRegistrationFields() {
				var register_firstname	= $('input[name=register_firstname]').val();
				var register_lastname	= $('input[name=register_lastname]').val();
				var register_email		= $('input[name=register_email]').val();
				var register_username	= $('input[name=register_username]').val();
				var register_password	= $('input[name=register_password]').val();
				var register_password2	= $('input[name=register_password2]').val();
				var response = [];
				response['valid'] = true;
				const missingRegistrationText = '<?php echo AxsLanguage::text("COM_SPLMS_AXS_MISSING_REGISTRATION_FIELD", "Missing Registration Field: ") ?>';
				if(!register_firstname) {
					const firstNameStr = '<?php echo AxsLanguage::text("AXS_FIRST_NAME", "First Name") ?>';
					response['message'] = missingRegistrationText + firstNameStr;
					response['valid'] = false;
				} else if(!register_lastname) {
					const lastNameStr = '<?php echo AxsLanguage::text("AXS_LAST_NAME", "Last Name") ?>';
					response['message'] = missingRegistrationText + lastNameStr;
					response['valid'] = false;
				} else if(!register_email) {
					const emailStr = '<?php echo AxsLanguage::text("AXS_EMAIL", "Email") ?>';
					response['message'] = missingRegistrationText + emailStr;
					response['valid'] = false;
				} else if(!register_username) {
					const usernameStr = '<?php echo AxsLanguage::text("AXS_USERNAME", "Username") ?>';
					response['message'] = missingRegistrationText + usernameStr;
					response['valid'] = false;
				} else if(!register_password) {
					const passwordStr = '<?php echo AxsLanguage::text("AXS_PASSWORD", "Password") ?>';
					response['message'] = missingRegistrationText + passwordStr;
					response['valid'] = false;
				} else if(!register_password2) {
					const confirmPasswordStr = '<?php echo AxsLanguage::text("AXS_CONFIRM_PASSWORD", "Confirm Password") ?>';
					response['message'] = missingRegistrationText + confirmPasswordStr;
					response['valid'] = false;
				} else if(register_password != register_password2) {
					response['message'] = '<?php echo AxsLanguage::text("AXS_PASSWORD_MISMATCH", "Passwords do not match.") ?>';
					response['valid'] = false;
				}
				return response;
			}

			$('#payment-form').submit(function(ev) {
				ev.preventDefault();
				if($('.cart-tovuti-register-box').is(':visible')) {
					var response = validateRegistrationFields();
					if(!response['valid']) {

						alert(response['message']);
						return;
					}
				}
	            var gateway = $('#payment-form').data('gtw');
	            var amount = parseFloat($('#totalPrice').text());
				var card_on_file = $('#card_on_file').val();
	            if(gateway == 'stripe' && amount > 0 && (card_on_file == 0 || card_on_file == undefined)) {
	                stripe.createToken(card).then(function(result) {
						console.log(result)
	                    if (result.error) {
	                      // Inform the user if there was an error.
	                      var errorElement = document.getElementById('card-errors');
	                      errorElement.textContent = result.error.message;
	                    } else {
	                        //console.log('success '+result);
	                      // Send the token to your server.
	                       stripeTokenHandler(result.token);
	                    }
	                });
	            } else {
	                processPayment();
	            }
	        });

			$(".btn-remove-course").click(
				function() {

					this.disabled = true;
					var which = this.getAttribute("which");
					$.ajax({
						url: '/index.php?option=com_splms&task=cart.removeItem&format=raw',
						data: {
							product_id: which
						},
						type: 'post'
					}).done(function(result) {
						//console.log(result);
						$("#lms_cart").html(result);
					}).fail(function() {
						alert(text_unable_to_remove_alert);
					});
				}
			);

			function currDate() {
				var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!
				var yyyy = today.getFullYear();
				if(dd<10) {
					dd='0'+dd
				}
				if(mm<10) {
					mm='0'+mm
				}
				return mm+'/'+dd+'/'+yyyy;
			}
		</script>

		<?php

		return ob_get_clean();
	}

}