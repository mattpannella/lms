<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsControllerAccuratethoughts extends FOFController{

	public function __construct($config = array()){
		parent::__construct($config);
	}

	public function saveAccurateThought() {
		
		$thoughts = JRequest::getVar('thoughts');
		$course_id = JRequest::getVar('cid');
		$lesson_id = JRequest::getVar('lid');
		$user_id = JRequest::getVar('uid');

		if (isset($thoughts) && isset($course_id) && isset($lesson_id) && isset($user_id)) {
			
			$db = JFactory::getDbo();

			$date = strtotime('now');

			$columns = array(
				'user_id',
				'course_id',
				'lesson_id',
				'accurate_thought',
				'date'
			);

			$values = array(
				(int)$user_id,
				(int)$course_id,
				(int)$lesson_id,
				$db->quote($thoughts),
				$date
			);

			$query = $db->getQuery(true);

			$query
				->insert($db->quoteName("#__splms_accuratethoughts"))
				->columns($db->quoteName($columns))
				->values(implode(',', $values));

			$db->setQuery($query);
			$db->execute();

			echo ($thoughts);
		} 
	}
}