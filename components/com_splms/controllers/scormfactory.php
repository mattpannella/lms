<?php

defined('_JEXEC') or die();

class SplmsControllerScormfactory extends FOFController {

	public function saveData() {
		$db     = JFactory::getDbo();
		$table  = "axs_scorm_learner_data";
		$column = "id";

		$response = new stdClass();
		$user_id = JFactory::getUser()->id;

		if(!$user_id) {
			$response->status  = "error";
			$response->message = AxsLanguage::text("AXS_SESSION_ENDED", "Your session has ended. Please login again."); 
		} else {
			$course_id 		  = $this->input->get('course_id', 0);
			$lesson_id 		  = $this->input->get('lesson_id', 0);
			$scorm_content_id = $this->input->get('scorm_course_id', '', 'STRING');
			$language         = $this->input->get('language', '', 'STRING');
			$cmiData          = $this->input->get('cmiData', '', 'BASE64');
			$learner_data     = urldecode(base64_decode($cmiData));
			$data = new stdClass();
			$data->user_id 			= $user_id;
			$data->course_id 		= $course_id;
			$data->lesson_id 		= $lesson_id;
			$data->scorm_content_id = $scorm_content_id;
			$data->modified_date    = date('Y-m-d H:i:s');

			if(!$language) {
				$language = 'en-GB';
			}

			$row = AxsScormFactory::getLearnerScormData($data);

			$scormData   = json_decode($learner_data);

			if($scormData->completion_status == 'completed' || $scormData->core->lesson_status == 'passed' || $scormData->completion_status == 'passed' || $scormData->core->lesson_status == 'completed') {
				$scormStatus = 'completed';
			} else {
	            $scormStatus = 'started';
	        }

	        if($scormData->success_status == 'failed') {
	            $scormStatus = 'failed';
	        }

	        if($scormData->success_status) {
	            $success = $scormData->success_status;
	        }

			$data->data = $learner_data;

			if($row) {
				$data->id = $row->id;
				$result = $db->updateObject($table,$data,$column);
			} else {
				$result = $db->insertObject($table,$data);
			}

			if($result) {
				$response->progress = $scormStatus;
				$response->status  = "success";
				$response->message = AxsLanguage::text("AXS_DATA_SAVED", "Data Saved");
			} else {
				$response->progress = $scormStatus;
				$response->status  = "error";
				$response->message = AxsLanguage::text("AXS_DATA_SAVED_ERROR", "There was an issue saving your data");
			}

			$data->scorm_course_id = $scorm_content_id;
	        $data->success  = $success;
	        $data->status   = $scormStatus;
			$data->language = $language;
			if($data->lesson_id) {
				$response->runCheckComplete = true;
	            //AxsLMS::insertLessonStatus($data);
	        } else {
	            AxsLMS::courseProgress_ScormContent($data);
	        }

		}
		echo json_encode($response);
	}
}