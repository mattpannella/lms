<?php

defined('_JEXEC') or die();

class SplmsControllerLessons extends FOFController {

	public function insertLessonStatus($params) {
		AxsLMS::insertLessonStatus($params);
	}

	public function checkComplete() {
		$params = new stdClass();
		$message = new stdClass();
		$input = JFactory::getApplication()->input;
		$params->user_id = JFactory::getUser()->id;
		if($params->user_id) {
			$params->course_id = $input->get('course_id', '', 'STRING');
			$params->lesson_id = $input->get('lesson_id', '', 'STRING');
			$params->language  = $_SESSION['current_language'];
			$currentLessonStatus = AxsLMS::getLessonStatus($params);
			if($currentLessonStatus->status == "completed") {
				$message->status = "completed";
			}
			$checkRequirements = AxsLMS::checkIfRequirementCompleted($params);
			$message->checkIfRequirementCompleted .= $checkRequirements;
			if($checkRequirements == "completed") {
				$params->status = "completed";
				$message->status = "completed";
				$params->check_lesson_status = $params->status;
				$params->check_lesson_language = $params->language;
				AxsLMS::insertLessonStatus($params);
				$message->percentage = round(AxsLMS::getCourseProgress($params->course_id,$params->user_id,$params->language)->progress);
			}
		} else {
			$message->error = 'User Session Error';
		}

		echo json_encode($message);
		return true;
	}

	/** 
	 * If an activity is complete and configured to require
	 * admin approval, send a notification email to the admin configured to approve it
	 */
	public function sendAdminApprovalMessageIfConfigured() {
		$input = JFactory::getApplication()->input;
		$activity_id = $input->get('activity_id', -1, 'STRING');
		$lesson_id = $input->get('lesson_id', -1, 'INT');

		$lesson = AxsLMS::getLessonById($lesson_id);
		$lessonParams = json_decode($lesson->params);
		$student_activities = json_decode($lessonParams->student_activities);
		$activity = null;
		foreach($student_activities as $key => $value) {
			if ($value->id == $activity_id) {
				$activity = $value;
				break;
			}
		}
		if ($activity->required_approval == "1" && $lessonParams->send_approval_notification == "1") {
			AxsNotifications::sendActivityAdminApprovalEmail($lesson, $activity);
		}
	}

}
