<?php

defined('_JEXEC') or die();

class SplmsControllerCart extends FOFController{
    private $model;

    public function __construct($config = array()) {
        parent::__construct($config);
        $viewName = $this->input->getString('view', $this->default_view);
        $this->model = $this->getModel($viewName);
	}

	public function setProfileNameFields($data) {
		if(!$data->userId || !$data->firstName || !$data->lastName) {
			return false;
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_GIVENNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$firstNameID = $db->loadObject();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_FAMILYNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$lastNameID = $db->loadObject();

		if($firstNameID->id) {
			$query = "INSERT IGNORE INTO joom_community_fields_values(`user_id`, `field_id`, `value`, `access`) VALUES (
				".$db->quote($data->userId).",
				".$db->quote($firstNameID->id).",
				".$db->quote($data->firstName).",
				40
				)";
			$db->setQuery($query);
			$db->execute();
		}

		if($lastNameID->id) {
			$query = "INSERT IGNORE INTO joom_community_fields_values(`user_id`, `field_id`, `value`, `access`) VALUES (
				".$db->quote($data->userId).",
				".$db->quote($lastNameID->id).",
				".$db->quote($data->lastName).",
				40
				)";
			$db->setQuery($query);
			$db->execute();
		}
		return true;
	}


    public function ajax() {

    	$card_on_file = $this->input->get('card_on_file', 0);
	    $brand = AxsBrands::getBrand();
	    $session = JFactory::getSession();
	    $orders = $session->get('splms_orders');


	    $cardNum = $this->input->get('cardNumber', 0);
	    $expm = $this->input->get('exp_month', 0);
	    $expy = $this->input->get('exp_year', 0);
	    $cvv = $this->input->get('cvv', 0);
	    $first = $this->input->get('first', 0);
	    $last = $this->input->get('last', 0);
	    $addr = $this->input->get('address', 0);
	    $zip = $this->input->get('zip', 0);
	    $city = $this->input->get('city', 0);
	    $state = $this->input->get('state', 0);
	    $ctry = $this->input->get('country', 0);
	    $phone = $this->input->get('phone', 0);
	    $email = $this->input->get('email', 0);
		$promo_code = $this->input->get('promo', 0);

		$register_email     = $this->input->get('register_email', 0, 'STRING');
		$register_firstname = $this->input->get('register_firstname', 0, 'STRING');
		$register_lastname  = $this->input->get('register_lastname', 0, 'STRING');
		$register_username  = $this->input->get('register_username', 0, 'STRING');
		$register_password  = $this->input->get('register_password', 0, 'STRING');
		$register_password2 = $this->input->get('register_password2', 0, 'STRING');

		$newUser = false;
		$user = JFactory::getUser();

		if($register_email && $register_firstname && $register_lastname && $user->id == 0) {
			$data['name'] = $register_firstname.' '.$register_lastname;
			$data['username'] = $register_username;
			$data['email']    = $register_email;
			$data['password'] = $register_password;
			$data['activation'] = '0';
			$data['block'] = '0';
			$data['groups'][] = 2;

			$error = new stdClass();
			$validatePassword = AxsSecurity::validatePassword($register_password);
			if($validatePassword->success === false) {
				$error->status = "FAIL";
				$error->error = $validatePassword->error;
				echo json_encode($error);
				return;
			}

			$user = new JUser;
			//Write to database
			if(!$user->bind($data)) {
				$error->status = "FAIL";
				$error->error = $user->getError();
				echo json_encode($error);
				return;
			}

			if (!$user->save()) {
				$error->status = "FAIL";
				$error->error = $user->getError();
				echo json_encode($error);
				return;
			}

			if($user->id) {
				$user_id = $user->id;
				$session = JFactory::getSession();
				$session->set('user',$user);
				$userData = new stdClass();
				$userData->userId = $user->id;
				$userData->firstName = $register_firstname;
				$userData->lastName = $register_lastname;
				self::setProfileNameFields($userData);
				$newUser = true;
			}
		}

		$user_id = JFactory::getUser()->id;

	    $dispatcher = JEventDispatcher::getInstance();

	    $total = 0;
	    $purchase_desc = "";

	    $db = JFactory::getDBO();

		if (count($orders) > 0) {

			//Create a query to get the information on all of the items being purchased.
			$cond = array();
			foreach ($orders as $order) {
				$cond[]= $db->quoteName("splms_course_id") . "=" . (int)$order['product_id'];
			}

			$query = $db->getQuery(true);
			$query
				->select('splms_course_id AS product_id,price,title AS product_name')
				->from($db->quoteName('joom_splms_courses'))
				->where($cond, 'or');

			$db->setQuery($query);
			$orders = $db->loadObjectList();
		}

		//Validate that they have not already purchased the course.
	    foreach ($orders as $order) {

	    	$item_id = $order->product_id;

	    	if ($purchase_desc == "") {
	    		$purchase_desc = $order->product_name;
	    	} else {
	    		$purchase_desc .= ", " . $order->product_name;
	    	}

	    	$amount = $order->price;

	    	$course = AxsExtra::getCourseById($item_id);
	    	$eventRes = $dispatcher->trigger('verifyPromo', array($promo_code, null, $course, null));
	    	$res = $eventRes[0];

	    	if ($res->result == 'success') {
	    		$amount = $res->item_amount;
	    	}

	    	if ($amount > 0) {
	    		$total += $amount;
	    	}
	    }

	    if($total > 0) {
	    	//check if they are paying with a card on file
		    $cardParams = AxsPayment::getCardParams();

	      	if ($card_on_file != '') {
	      		$cardParams->id = $card_on_file;
	      		$card = AxsPayment::newCard($cardParams);
				$card->getCardWithId();
				$saveCard = false;

	      	} else {
		      	//must be paying with a new card
		      	if($brand->billing->gateway_type == 'stripe') {
					$cardParams->user_id = $user_id;
					$cardParams->stripeToken = $this->input->get('stripeToken', 0, 'STRING');
					$cardParams->cardNumber = $this->input->get('last4', 0);
					$user = JFactory::getUser($cardParams->user_id);
					$cardParams->email = $user->email;
					$cardParams->description = $user->name;
				} else {
			      	$cardParams->firstName = $first;
			      	$cardParams->lastName = $last;
			      	$cardParams->cardNumber = $cardNum;
			      	$cardParams->expire = str_pad($expm, 2, '0', STR_PAD_LEFT) . substr($expy, 2, 2);
					$cardParams->cvv = $cvv;
			      	$cardParams->address = $addr;
			      	$cardParams->zip = $zip;
			      	$cardParams->city = $city;
			      	$cardParams->state = $state;
			      	$cardParams->country = $ctry;
			      	$cardParams->phone = $phone;
			      	$cardParams->email = $email;
			      	$cardParams->user_id = $user_id;
			     }
				 /* echo json_encode($cardParams);
				 echo json_encode($user); die(); */
		      	$card = AxsPayment::newCard($cardParams);
		      	$saveCard = true;
	      	}

	      	$trxnParams = AxsPayment::getTransactionParams();

	      	$trxnParams->user_id = $user_id;
	        $trxnParams->type = AxsPayment::$trxn_types['course'];
	        $trxnParams->amount = $total;
	        $trxnParams->date = date("Y-m-d H:i:s");
	        $trxnParams->card = $card;
	        $trxnParams->initiator = AxsPayment::$initiators['user'];
	        $trxnParams->description = $purchase_desc;
	        $trxnParams->saveCard = $saveCard;

	      	//send transaction
	      	$trxn = AxsPayment::newTransaction($trxnParams);
	      	$trxn->charge(false); //false so we don't save it right away
			if ($trxn->status == 'FAIL') {
				$error = new stdClass();
				$error->status = "FAIL";
				$error->error = $trxn->message;
				echo json_encode($error);
				return;
			}
	    }

      	$id_list = "";

      	if ($trxn->status == 'SUC') {
      		foreach ($orders as $order) {
	    		$item_id = $order->product_id;

	    		if ($id_list == "") {
	    			$multiple = false;
	    			$id_list = $item_id;
	    		} else {
	    			$multiple = true;
	    			$id_list .= "," . $item_id;
	    		}

	    		$courseParams = AxsPayment::getCoursePurchaseParams();
	    		$courseParams->user_id = $user_id;
	    		$courseParams->course_id = $item_id;
	    		$courseParams->date = date('Y-m-d H:i:s');
	    		$courseParams->status = "PAID";

          		$coursePurchase = AxsPayment::newCoursePurchase($courseParams);
          		$coursePurchase->save();

          		$dispatcher->trigger('onCoursePurchase', array($coursePurchase));
          	}

          	$dispatcher->trigger('onCoursePromo', array($trxn));

          	$trxn->type_id = $coursePurchase->id;
          	if ($multiple) {
          		$trxn->mult_type_id = $id_list;
          	}
          	$trxn->save();

          	if($trxn->status == 'SUC') {
          		$dispatcher->trigger('onCoursePurchaseCommision', array($trxn));
          	}

          	//Clear the cart
      		$session->set('splms_orders', null);
      	}

      	if($total == 0) {
      		foreach ($orders as $order) {
	    		$item_id = $order->product_id;

	    		if ($id_list == "") {
	    			$multiple = false;
	    			$id_list = $item_id;
	    		} else {
	    			$multiple = true;
	    			$id_list .= "," . $item_id;
	    		}

	    		$courseParams = AxsPayment::getCoursePurchaseParams();
	    		$courseParams->user_id = $user_id;
	    		$courseParams->course_id = $item_id;
	    		$courseParams->date = date('Y-m-d H:i:s');
	    		$courseParams->status = "PAID";

          		$coursePurchase = AxsPayment::newCoursePurchase($courseParams);
          		$coursePurchase->save();

          		$dispatcher->trigger('onCoursePurchase', array($coursePurchase));
          	}

          	$trxn = new stdClass();
          	$trxn->status = "SUC";
			$trxn->id = AxsLMS::createUniqueID();
			$trxn->amount = $total;
			$dispatcher->trigger('onCoursePromo', array($trxn));
          	//Clear the cart
      		$session->set('splms_orders', null);
		  }

		  if($courseParams->status = "PAID") {
			//send to auto alerts
			$alertParams = new stdClass();
			$alertParams->user_id = $user_id;
			$alertParams->course_id = $item_id;
			$alertParams->amount = $total;
			$alertParams->date = date("m-d-Y");
			$alerts = new AxsAlerts($alertParams);
			$alerts->runAutoAlert('course purchase',$alertParams);
		  }

      	echo json_encode($trxn);
    }

	public function verifyPromo() {
		$promo_code = $this->input->get('promo', '');
		$courseIds = $this->input->get('course_ids');

		// If at least one course has a verified promo code, consider it a good code
		$results = [];

		foreach($courseIds as $courseId) {
			$course = AxsExtra::getCourseById($courseId);

			$dispatcher = JEventDispatcher::getInstance();
			$verifiedPromo = $dispatcher->trigger('verifyPromo', array($promo_code, null, $course, null));

			$results[$courseId] = $verifiedPromo[0];
		}

		echo json_encode($results);

		//store that someone entered a promo code that may / may not still be valid but DOES exist in system
		$data = new stdClass();
		$data->user_id = JFactory::getUser()->id;
		$data->trxn_id = 0;
		$data->type = AxsPayment::$trxn_types['course'];
		$dispatcher->trigger('storeActivity', array($data));
	}

    public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
        return parent::getModel($name, $prefix, $config);
    }

    public function removeItem() {

    	$product_id = $this->input->get('product_id', '');
    	$session = JFactory::getSession();
    	$orders = $session->get('splms_orders');

    	foreach ($orders as $key => $order) {
    		if ((int)$order['product_id'] == (int)$product_id) {
    			unset($orders[$key]);
    		}
    	}

    	if (count($orders) > 0) {
    		$session->set('splms_orders', $orders);
    	} else {
    		$session->set('splms_orders', null);
    	}

    	echo SplmsHelper::getCartView();
    }

    public function addItem() {
    	$product_id = $this->input->get('product_id', '');
    	$session = JFactory::getSession();
    	$orders = $session->get('splms_orders');

    	$orders[]= array("product_id" => $product_id);
    	$session->set('splms_orders', $orders);

    	echo JRoute::_('index.php?option=com_splms&view=cart');

    }
}