<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die();

class SplmsControllerQuizquestions extends FOFController {

	private function getAnswerSet($index) {

        if(is_null($index)) {
            return null;
        }

		switch ($index) {
			case 0:
				$answerArray = 'ans_one';
			break;

			case 1:
				$answerArray = 'ans_two';
			break;

			case 2:
				$answerArray = 'ans_three';
			break;

			case 3:
				$answerArray = 'ans_four';
			break;
		}

		return $answerArray;
	}

	public function saveAnswer() {
		// Load Quiz model
		$params   = new stdClass();
		$response = new stdClass();

		$response->quiz_completed 		 = false;
		$response->next_question 		 = false;
		$response->next_question_options = false;

		$quiz_model = FOFModel::getTmpInstance('quizquestions', 'SplmsModel');

        $input = JFactory::getApplication()->input;
		$data = $input->post->get('data', NULL, 'ARRAY');

		$params->user_id 	 = JFactory::getUser()->id;
		$params->language 	 = $data['language'];
		$params->answer 	 = $data['answer'];
		$params->question 	 = $data['question'];
		$timeExpired         = $data['timeExpired'] != 'false';

        // Save the time elapsed for more accurate timed quiz tracking which will prevent users from resetting the
        // quiz timer by leaving and returning to the quiz.
        $timeElapsed         = $data['timeElapsed'];

        if(!$params->user_id) {
			echo 'Please log in to take this quiz.';
			return true;
		}

        // Decrypt the encoded quiz parameters
		$key = AxsKeys::getKey('lms');
		$encryptedQuizParams = base64_decode($data['quiz']);
		$decryptedQuizParams = AxsEncryption::decrypt($encryptedQuizParams, $key);

		$params->quiz_id 	 = $decryptedQuizParams->quiz_id;
		$params->course_id   = $decryptedQuizParams->course_id;
		$params->lesson_id 	 = $decryptedQuizParams->lesson_id;

        AxsQuiz::saveLegacyQuizAnswer($params->user_id, $params->quiz_id, $params->question, $params->answer, $timeElapsed);

		// Get the set of answers associated with this quiz - only necessary when continuing a quiz
        $currentAttempt = AxsQuiz::getLatestLegacyQuizAttempt($params->user_id, $params->quiz_id);
        $userAnswers = json_decode($currentAttempt->answers);

        // Add the new questionID:answer pair
        $userAnswers[$params->question] = $params->answer;

		$quiz 			= AxsLMS::getQuiz($params->quiz_id);
		$quiz_params 	= json_decode($quiz->params);
		$questions 		= json_decode($quiz->list_answers);

        $answerCount 	= count($userAnswers);
		$questionCount 	= count($questions->right_ans);

		if($userAnswers[$params->question] == $questions->right_ans[$params->question]) {
			$response->correct = true;
		} else {
			$response->correct = false;
		}

		//get correct answer text from array
		$answerArray = $this->getAnswerSet($questions->right_ans[$params->question]);

		if($quiz_params->show_answers) {
			$response->answer = $questions->{$answerArray}[$params->question];
		} else {
			$response->answer = false;
		}

        // This condition covers the case where a quiz is finished
		if( $params->user_id && ( ($answerCount == $questionCount) || $timeExpired) ) {

			$correctCount = 0;
			$answerSet = [];

			for($questionNumber = 0; $questionNumber < $questionCount; $questionNumber++) {
				$answerBlock  = $this->getAnswerSet($questions->right_ans[$questionNumber]);
				$studentAnswerBlock = $this->getAnswerSet($userAnswers[$questionNumber]);

				$answerObject = new stdClass();
				$answerObject->question = $questions->qes_title[$questionNumber];

                if(!is_null($studentAnswerBlock)) {

                    $answerObject->answer   = $questions->{$studentAnswerBlock}[$questionNumber];

                    if($userAnswers[$questionNumber] == $questions->right_ans[$questionNumber]) {
                        $answerObject->correct = true;
                        $correctCount++;
                    } else {
                        $answerObject->correct  	 = false;
                        $answerObject->correctAnswer = $questions->{$answerBlock}[$questionNumber];
                    }
                } else {

                    $answerObject->correct = false;
                    $answerObject->answer = null;

                    $answerObject->correctAnswer = $questions->{$answerBlock}[$questionNumber];
                }

				$answers = json_encode($answerObject);

				array_push($answerSet, $answers);
			}

			$params->total_marks = $questionCount;
			$params->q_result 	 = $correctCount;
			$params->answers 	 = json_encode($answerSet);
            $requiredScore       = (int)$quiz_params->required_score;

			$percentScored = (int)(($correctCount / $questionCount) * 100);
            $quizPassed = false;

			if ($percentScored >= $requiredScore) {

				$params->status = 'completed';
                $quizPassed = true;
			} else {

                $params->status = 'failed';
			}

			if (!$data['status'] && $params->total_marks) {
				$quiz_model->insertQuizResult($params);
			} else {
				$params->status = $input->get('status', '', 'STRING');
			}

			$params->status = AxsLMS::checkIfRequirementCompleted($params);
			if($params->status != "incomplete") {
				AxsLMS::insertLessonStatus($params);
				$progress = round(AxsLMS::getCourseProgress($params->course_id,$params->user_id,$params->language)->progress);
			}

			$response->status              = $params->status;
			$response->progress 	       = $progress;
			$response->quiz_completed 	   = true;
			$response->question_count 	   = $questionCount;
			$response->correct_count	   = $correctCount;
			$response->percentage          = $percentScored;
			$response->percentage_required = $requiredScore;
			$message = '';

			if($quiz_params->passed_message && $quizPassed) {
				$message .= $quiz_params->passed_message;
			}
			elseif($quiz_params->failed_message && !$quizPassed) {
				$message .= $quiz_params->failed_message;
			}
			elseif($quiz_params->completion_message) {
				$message .= $quiz_params->completion_message;
			}
			else {
				$message = false;
			}

            // Regardless of what else happens, we need to delete the temporary quiz answer tracking row(s) when a quiz
            // is completed. Any future retakes will start with a blank slate.
            AxsQuiz::deleteLegacyQuizAnswers($params->user_id, $params->quiz_id);

			$response->completion_message = $message;

		} else {
			$answer1 = $questions->ans_one[$params->question + 1];
			$answer2 = $questions->ans_two[$params->question + 1];
			$answer3 = $questions->ans_three[$params->question + 1];
			$answer4 = $questions->ans_four[$params->question + 1];

			$next_question_options = [$answer1,$answer2,$answer3,$answer4];

			//get media
			$media = $questions->media[$params->question + 1];
			if($media) {
				$mediaHTML = AxsLMS::getQuizQuestionMedia($media);
			} else {
				$mediaHTML = '';
			}

			$response->next_question_media   = $mediaHTML;
			$response->next_question 		 = $questions->qes_title[$params->question + 1];
			$response->next_question_options = json_encode($next_question_options);
		}

		echo json_encode($response);
		return true;
	}

	public function submit_result() {
		// Load Quiz model
		$params = new stdClass();
		$quiz_model = FOFModel::getTmpInstance('quizquestions', 'SplmsModel');

		$status = 'started';
		$input = JFactory::getApplication()->input;
		$data = $input->post->get('data', NULL, 'ARRAY');

		$params->user_id 	 = JFactory::getUser()->id;
		$params->quiz_id 	 = $data['quiz_id'];
		$params->course_id   = $data['course_id'];
		$params->lesson_id 	 = $data['lesson_id'];
		$params->total_marks = $data['total_marks'];
		$params->q_result 	 = $data['q_result'];
		$params->language 	 = $data['language'];

		if($params->user_id) {
			/*$quiz_params = json_decode(AxsLMS::getQuiz($params->quiz_id)->params);

			$percentScored = (int)(($params->q_result / $params->total_marks) * 100);

			if ($percentScored >= (int)$quiz_params->required_score) {
				$params->status = 'completed';
			} else {
				$params->status = 'failed';
			}*/


			$quiz_model->insertQuizResult($params);

			$params->status = AxsLMS::checkIfRequirementCompleted($params);

			if($params->status != "incomplete") {
				AxsLMS::insertLessonStatus($params);
				$result = round(AxsLMS::getCourseProgress($params->course_id,$params->user_id,$params->language)->progress);
				echo $result;
			}


			return true;
		} else {
			echo AxsLanguage::text("AXS_USER_SESSION_ERROR", "User Session Error");
			return true;
		}


	}

	public function check_results() {
		$course_id = JRequest::getVar('cid');
		$lesson_id = JRequest::getVar('lid');
		$user_id   = JRequest::getVar('uid');
		$quiz_id   = JRequest::getVar('qid');
		$language  = JRequest::getVar('language');

		//echo ("Course: " . $course_id . " Lesson: " . $lesson_id . " User: " . $user_id . " Quiz: " . $quiz_id);

		if (isset($course_id) && isset($lesson_id) && isset($user_id) && isset($quiz_id)) {

			$db = JFactory::getDbo();

			$query = $db->getQuery(true);

			$conditions = array(
				$db->quoteName('user_id') 				. '=' . $db->quote($user_id),
				$db->quoteName('splms_quizquestion_id') . '=' . $db->quote($quiz_id),
				$db->quoteName('splms_course_id') 		. '=' . $db->quote($course_id),
				$db->quoteName('splms_lesson_id') 		. '=' . $db->quote($lesson_id),
				'archive_date IS NULL'
			);

			$query
				->select('*')
				->from ($db->quoteName('#__splms_quizresults'))
				->where($conditions);

			$db->setQuery($query);

			$results = $db->loadObjectList();

			if ($results) {

				$result = json_encode($results[0]);
				echo $result;
			}
		}
	}

	public function checkPassword() {
		$input = JFactory::getApplication()->input;

		// Clean up the password input and store it
		$givenPassword = htmlentities(trim(base64_decode($input->get('password'))));
		$quizId = (int)$input->get('quiz_id');

		// Check the given password against the one associated with the given quiz
		$quiz = AxsLMS::getQuiz($quizId);
		$quizParams = json_decode($quiz->params);

		$decryptedQuizPassword = AxsEncryption::decrypt($quizParams->password, AxsKeys::getKey('surveys'));

		$response = array();

		if($decryptedQuizPassword == $givenPassword) {

			// Passwords match, so the response object will return success
			$response['success'] = true;
		} else {

			$response['success'] = false;
			$response['message'] = AxsLanguage::text("AXS_INCORRECT_PASSWORD", 'You have entered an incorrect password. Please check your input and try again.');
		}

		echo json_encode($response);
	}
}