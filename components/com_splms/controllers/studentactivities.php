<?php

defined('_JEXEC') or die();

class SplmsControllerStudentactivities extends FOFController {

    public function getSubmission() {
        $item = new stdClass();
        $item->student_response = JRequest::getVar('student_response');
        $item->activity_type    = JRequest::getVar('activity_type');
        $item->activity_id      = JRequest::getVar('activity_id');
        $item->user             = JRequest::getVar('user');

        $key = AxsKeys::getKey('lms');
        $tokenParams = new stdClass();
        $tokenParams->timestamp = time();
        $tokenParams->interactive_id = $item->activity_id;
        $tokenParams->user_id = $item->user;
        $tokenParams->isAdmin = true;
        $encryptedToken = base64_encode(AxsEncryption::encrypt($tokenParams, $key));

        $item->encryptedToken = $encryptedToken;

        $html = AxsLMS::parseActivitySubmission($item);
        echo $html;
    }

    public function getPreview($ic_id = null) {
        $input = JFactory::getApplication()->input;
        $ic_id = $input->get('ic_id','','INT');

        $key = AxsKeys::getKey('lms');
        $tokenParams = new stdClass();
        $tokenParams->timestamp = time();
        $tokenParams->interactive_id = $ic_id;
        $tokenParams->user_id = JFactory::getUser()->id;
        $encryptedToken = base64_encode(AxsEncryption::encrypt($tokenParams, $key));

        $html = '<iframe src="/?tmpl=ic_embed&embed_id='.$ic_id.'&token=' . $encryptedToken . '" style="width: 100%; height: 400px;" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        echo $html;
    }

    public function updateActivity($params = null) {
        $response = new stdClass();
        $data = new stdClass();
        $data->id = JRequest::getVar('id');
        $data->teacher_comment = JRequest::getVar('teacher_comment');
        $data->grade = JRequest::getVar('grade');
        $completed = JRequest::getVar('completed');
        if($completed == "Yes") {
            $data->completed = 1;
        } else {
            $data->completed = 0;
        }
        $activity = AxsLMS::newActivity($data);
        $result = $activity->save();
        if($result) {
            $response->status = 'success';
            $response->message = AxsLanguage::text("COM_LMS_ACTIVITY_UPDATED", "Activity Updated");
            if($completed == "Yes") {
                $activity = AxsLMS::getActivityById($data->id);
                $lesson   = AxsLMS::getLessonById($activity->lesson_id);
                $params = new stdClass();
                $params->user_id   = $activity->user_id;
                $params->course_id = $activity->course_id;
                $params->lesson_id = $activity->lesson_id;
                $params->language  = $lesson->language;
                AxsLMS::checkComplete($params);
            }
        } else {
            $response->status = 'failed';
            $response->message = AxsLanguage::text("AXS_ERROR_SAVING_PLEASE_REFRESH", "There was an error saving. Please refresh your page and try again.");
        }
        echo json_encode($response);
    }

    public function submitActivity() {
        $Jsession = JFactory::getSession();
        $user_id = JFactory::getUser()->id;
        $response = new stdClass();

        if($user_id) {
            $lesson   = self::getLesson(JRequest::getVar('lesson'));
            $activity = self::getLesson(JRequest::getVar('activity'));
            if($activity->allow_resubmit) {
                $existing = AxsLMS::getStudentActivityByActivityId($user_id, $activity->id);
                if($existing) {
                    $existing->archive_date = date('Y-m-d H:i:s');
                    $existing = AxsLMS::newActivity($existing);
                    $existing->save();
                }
            }
            $data = new stdClass();
            $data->course_id = $lesson->course_id;
            $data->lesson_id = $lesson->lesson_id;
            $data->date_submitted = date("Y-m-d H:i:s");
            $data->activity_id = $activity->id;
            $data->user_id = $user_id;
            $data->activity_type = $activity->type;
            $data->activity_request = htmlentities($activity->title);

            $activityType = $data->activity_type;
            $externalVideoSources = ['youtube','vimeo','facebook','screencast'];
            $fileUploadSources = ['mp3','mp4','upload'];

            if(in_array($activityType, $externalVideoSources)) {
                $activityType = 'externalvideo';
            }

            if(in_array($activityType, $fileUploadSources)) {
                $activityType = 'upload';
            }

            switch ($activityType) {
                case 'textbox':
                     $data->student_response = htmlentities(JRequest::getVar('textbox'));
                break;

                case 'link':
                     $data->student_response = urlencode(JRequest::getVar('textbox'));
                break;

                case 'upload':
                     $data->file = $_FILES;
                break;

                case 'externalvideo':
                     $data->student_response = htmlentities(JRequest::getVar('textbox'));
                break;

                default:
                     $data->student_response = htmlentities(JRequest::getVar('textbox'));
                break;
            }


            if ($activityType == "esignature") {
                $db = JFactory::getDbo();
                $query = "SELECT id, name FROM joom_users WHERE id = " . $db->quote($user_id) . " AND block = 0";
                $db->setQuery($query);
                $user = $db->loadObject();

                $sessionUserId = JFactory::getUser()->id;

                if (trim(strtolower($user->name)) != trim(strtolower($data->student_response))) {
                    $response = new stdClass();
                    $response->message = AxsLanguage::text("AXS_NAME_DOESNT_MATCH", "Signature does not match user name");
                    echo json_encode($response);
                    return false;
                }

                if ($user_id != $sessionUserId || empty($Jsession) || empty($user)) {
                    $response = new stdClass();
                    $response->message = AxsLanguage::text("COM_LMS_SUBMISSION_ERROR_NO_USER","You are not permitted to do the esignature of another user.");
                    echo json_encode($response);
                    return false;
                }
                
                $data->user_agent = $_SERVER['HTTP_USER_AGENT'];
            }

            if (!$activity->required_approval) {
                $data->completed = 1;
            } else {
                $data->completed = 0;
            }
            $newActivity = AxsLMS::newActivity($data);
            $response = $newActivity->submit();

            if($data->completed) {
                $response->completed = 1;
            } else {
                $response->completed = 0;
                if($activity->required && $activity->required_approval) {
                    $response->message =
                        $response->message
                        . '</br><span style="color:red; font-size: 13px;">*<i>'
                        . AxsLanguage::text("COM_LMS_PENDING_APPROVAL", "Pending Approval For Completion")
                        . '</i></span>';
                }
            }

            $response->activity_id = $data->activity_id;
            $response->lesson_id = $data->lesson_id;
            echo json_encode($response);
        } else {
            $response = new stdClass();
            $response->message = AxsLanguage::text("COM_LMS_SUBMISSION_ERROR_NO_USER","You must be logged in to continue. Try refreshing your page and logging in.");
            echo json_encode($response);
        }
    }

    private function getLesson($lesson) {
        $lessonEncrypted = base64_decode($lesson);
        $key = AxsKeys::getKey('lms');
        $lesson = AxsEncryption::decrypt($lessonEncrypted, $key);
        return $lesson;
    }

    private function getActivity($activity) {
        $activityEncrypted = base64_decode($activity);
        $key = AxsKeys::getKey('lms');
        $lesson = AxsEncryption::decrypt($activityEncrypted, $key);
        return $activity;
    }

    public function ajax_getUserActivityArchive() {

        $contentId = filter_input(INPUT_GET, 'content_id', FILTER_VALIDATE_INT);
        $userId = filter_input(INPUT_GET, 'user_id', FILTER_VALIDATE_INT);
        $versionNumber = filter_input(INPUT_GET, 'version', FILTER_VALIDATE_INT);

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query = "SELECT * FROM ic_h5p_contents_user_data_archive WHERE `content_id` = $contentId AND `user_id` = $userId AND `version_number` = $versionNumber";

        $db->setQuery($query);

        $versionData = $db->loadObject();

        $response = new stdClass();

        $response->archive_time = AxsLMS::formatDate($versionData->archived_at);

        $activityData = $versionData->data;

        if($activityData == 'RESET' || empty($activityData)) {

            $activityData = 'none';

            $response->answers_template = '<h3>No archived user response data exists for this activity.</h3>';
        } else {

            $response->answers_template = $this->getArchivedActivityDataHTML($contentId, $userId, $versionNumber);
        }

        echo json_encode($response);
    }

    private function getArchivedActivityDataHTML($contentId, $userId, $versionNumber) {

        $key = AxsKeys::getKey('lms');
        $tokenParams = new stdClass();
        $tokenParams->timestamp = time();
        $tokenParams->interactive_id = $contentId;
        $tokenParams->user_id = $userId;
        $tokenParams->version_number = $versionNumber;
        $tokenParams->isAdmin = true;
        $encryptedToken = base64_encode(AxsEncryption::encrypt($tokenParams, $key));

        $templateHTML = AxsLMS::getInteractiveContentResults($contentId, $userId, $encryptedToken, $versionNumber);

        return $templateHTML;
    }
}