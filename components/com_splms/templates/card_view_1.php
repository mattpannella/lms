<?php
    $topBarExtraClass = '';
    $topBarText = '';

    if ($completed) {
        $topBarExtraClass = 'course-card-topbar-completed';
        $topBarText       = AxsLanguage::text("AXS_COMPLETED", "Completed");
    }
    if ($inProgress) {
        $topBarText       = AxsLanguage::text("AXS_IN_PROGRESS", "In Progress");
    }
?>
<div class="cardBox course-card-v1
<?php
    if ($recommended) 	{ echo " recommended"; 	}
    if ($wishlisted) 	{ echo " wishlisted"; 	}
    if ($assigned) 		{ echo " assigned"; 	}
    if ($completed) 	{ echo " completed"; 	}
    if ($inProgress) 	{ echo " inProgress"; 	}
	if ($purchased) 	{ echo " purchased"; 	}
?>
">
    <div class="course-card-topbar <?php echo $topBarExtraClass; ?>">
        <?php if ($inProgress) { ?>
            <div class="course-card-icon"></div>
            <div class="course-card-topbar-text"><?php echo $topBarText; ?></div>
			<div class="course-progress-container">
			    <div
				    style="width: <?php echo $inProgress->percentComplete.'%'; ?>;height:15px"
				    class="course-progress-bar"
                ></div>
            </div>
        <?php } ?>
        <?php if ($completed) { ?>
            <div class="course-card-icon"></div>
            <div class="course-card-topbar-text"><?php echo $topBarText; ?></div>
        <?php } ?>
	</div>
	<?php if (!$params->locked || $purchased) { ?>
		<a href="<?php echo $link; ?>" target="<?php echo $target; ?>">
	<?php } ?>
          <div class="course-card-img"
            <?php if($item->image) {
				$item->image = str_replace('https://tovuti.io/','/',$item->image);
				$item->image = str_replace('https://www.tovuti.io/','/',$item->image);
				$item->image = str_replace('https://tovutilms.com/','/',$item->image);
			?>
                style="background-image: url('<?php echo $item->image; ?>');"
            <?php } ?>
          >
			<?php if ($assigned && !$completed) {
				$due_date = '';
				$now = strtotime('now');
				$dt = strtotime($assigned->due_date);
				if ($assigned->due_date != '0000-00-00 00:00:00' && $dt > $now) {
					$due_date = AxsLanguage::text("AXS_DUE", "Due") . ' ' .date("m/d",strtotime($assigned->due_date));
				}
				if($dt < $now && $assigned->due_date != '0000-00-00 00:00:00' && $assigned->due_date) {
					$due_date = AxsLanguage::text("AXS_PAST_DUE", "Past Due");
				}
			?>
			<div
			  style="-webkit-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)"
			  class="course-card-assigned"
			>
			  <div class="course-card-icon"></div>
			  <h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_ASSIGNED", "In Assigned") ?> <?php echo $due_date; ?></h6>
			</div>
			<?php } ?>
			<?php if (!$assigned && $recommended && !$completed) { ?>
			<div
			  style="-webkit-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-moz-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);-ms-transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0);transform:translate3d(0PX, 0PX, 0) scale3d(1, 1, 1) rotateX(0) rotateY(0) rotateZ(0) skew(0, 0)"
			  class="course-card-recommended"
			>
			  <div class="course-card-icon"></div>
			  <h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_RECOMMENDED", "Recommended") ?></h6>
			</div>
			<?php } ?>
		  </div>
		<?php if (!$params->locked || $purchased) { ?>
		  </a>
		<?php } ?>
		  <div class="tovuti-course-card-title">
			<h3 class="course-card-title">
			<?php echo AxsActivityData::truncate($item->title,100); ?>
			</h3>
		  </div>
		  <div class="course-card-details">
		  	<?php if($settings->show_teachers_count) { ?>
			<div class="course-card-details-row">
			  <div class="course-card-icon card-icon-dark"></div>
			  <div class="course-card-details-text">
				<?php
					if ($params && $params->teacher_list_text && $params->teacher_list_text != 'Teachers') {
						echo $params->teacher_list_text;
					} else {
						echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_TEACHERS", "Teachers");
					}
				?>
			  </div>
			  <div class="course-card-details-count teachers">
				  <?php echo count($teachers); ?>
			  </div>
			</div>
			<?php } ?>
			<?php if($settings->show_lessons_count) { ?>
			<div class="course-card-details-row">
			  <div class="course-card-icon card-icon-dark"></div>
			  <div class="course-card-details-text">
				<?php
					if ($params && $params->lesson_button_text && $params->lesson_button_text != 'Lessons') {
						echo $params->lesson_button_text;
					} else {
						if(count($lessons) != 1){
							echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_LESSONS", "Lessons");
						}
						else{
							echo AxsLanguage::text("AXS_LESSON", "Lesson");
						}
					}
				?>
			  </div>
			  <div class="course-card-details-count"><?php echo count($lessons); ?></div>
			</div>
			<?php } ?>
			<?php if($settings->show_pricing) { ?>
			<div class="course-card-details-row">
			  <div class="course-card-icon card-icon-dark"></div>
			  <div class="course-card-details-text"><?php echo JText::_('COM_SPLMS_AXS_COURSE_PRICE');?></div>
			  <div class="course-card-details-count">
			  	<?php
					if($item->price != '0.00') {
						echo $currency_symbol.$item->price;
					} else {
						echo AxsLanguage::text("AXS_FREE", "Free");
					}
				?>
			  </div>
			</div>
			<?php } ?>
		  </div>

		  <?php if (!$params->locked || $purchased) { ?>
			<a
			class="course-card-view-button w-inline-block"
			href="<?php echo $link; ?>"
			target="<?php echo $target; ?>">
			<h5 class="tovuti-course-card-view-button-h5">
			  <?php echo AxsLanguage::text("COM_SPLMS_COURSE_EXPLORE", "Explore this Course") ?>
			</h5>
			<img
			  src="https://uploads-ssl.webflow.com/5dea7c7f31edea8ab1bb721a/5deaa34b247ca5e66aca1b8d_white-arrow.svg"
			  alt=""
			  class="tovuti-course-card-view-button-icon"
		  /></a>
		<?php }  else { ?>

		<button class="btn btn-primary" disabled>
			<h5 class="tovuti-course-card-view-button-h5">
			<span class="fa fa-lock"></span> <?php echo JText::_('COM_SPLMS_AXS_COURSE_VIEW_COURSE_BUTTON_LOCKED');?>
			</h5>
		</button>
		<?php } ?>
		</div>