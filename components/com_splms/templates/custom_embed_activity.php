<div class="box lms-activity">
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>
	<div class="lms-custom-embed">
		<?php echo $activity->embed_code; ?>
	</div>
</div>