<div class="box lms-activity" data-lesson="<?php echo $encodedLessonParams; ?>">
	<?php if($requiredBox) { echo $requiredBox; } ?>
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>	
	<?php
		$surveyObject = AxsSurvey::getSurveyById($activity->survey);
		$surveyObject->fullwidth = true;
		$survey = new AxsSurvey($surveyObject);
		$survey->getTemplate();
	?>
</div>