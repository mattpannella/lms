<?php
foreach($events as $event):
	if(!$event->image) {
		$event->image = "https://www.tovuti.io/images/SWltRlRIMGZIb2xCcWZEVlFLWXc2QT09OjI0NzM/placeholder.jpg";//"/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg";
	} else {
		$event->image = '/'.$event->image;
	}
?>
	<div class="tov-trans-card-course">
		<div class="tov-trans-course-columns">
			<div class="tov-trans-course-column tov-trans-course-column-first">
				<div class="tov-trans-course-img">
					<?php if (isset($event->image) && !empty($event->image) && $event->image != '/'): ?>
						<img style="max-width:100%" src="<?php echo $event->image; ?>" />
					<?php else: ?>
						<svg><rect width="100%" height="100%" fill="<?php echo AxsTranscripts::getColorFromStr($event->title) ?>"/></svg>
					<?php endif; ?>
				</div>
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_NAME", "Name") ?></p>
					<p class="tov-trans-detail-data"><?php echo $event->title; ?></p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_ATTENDED", "Attended") ?></p>
					<p class="tov-trans-detail-data">
					<td style="text-align: center; font-size: 20px;">
						<?php if($event->checked_in) {
							echo '<badge class="tov-trans-course-badge tov-trans-badge-completed">Attended</badge>';
						} else {
							echo '<badge class="tov-trans-course-badge tov-trans-badge-not-completed">Not Attended</badge>';
						} ?>
					</td>
				</p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("JDATE", "Date") ?></p>
					<p class="tov-trans-detail-data">
					<?php
						if (date('Y',strtotime($event->event_date)) > 1969) {
							echo AxsLMS::formatDate($event->event_date);
						} else {
							echo AxsLanguage::text("AXS_OPEN_CAPITALIZED", "OPEN");
						}
						if (
							$event->event_end_date != '0000-00-00 00:00:00'
							&& date('Y',strtotime($event->event_date)) > 1969
							&& AxsLMS::formatDate($event->event_end_date) != AxsLMS::formatDate($event->event_date)
						) {
							echo ' - '.AxsLMS::formatDate($event->event_end_date);
						}
					?>
					</p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_REGISTRATION_DATE", "Registration Date") ?></p>
					<p class="tov-trans-detail-data">
					<?php echo AxsLMS::formatDate($event->register_date); ?>

					</p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_PAYMENT", "Payment") ?></p>
					<p class="tov-trans-detail-data"><?php echo $currency_symbol.$event->total_amount; ?></p>
				</div>
			</div>
		</div>
	</div>
<?php
endforeach;