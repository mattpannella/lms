<div class="box lms-activity video-content">
	<?php if($requiredBox) { echo $requiredBox; } ?>
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>
	<?php
		$activity_video = '';
		if($activity->file_location == 'secure_file_locker' && $activity->secure_file) {
			$activity->video_url = "lockedfiles/mp4/".AxsFiles::getEncodedFile($activity->secure_file);
			$activity->secure_video_url = $activity->secure_file;
		}
		$activity_video = AxsLMS::getVideo($activity);
		echo $activity_video;
	?>
</div>