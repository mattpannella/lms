<div class="box lms-activity">
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>
	<?php
		$base = JUri::base(); 
		if($activity->file_location == 'secure_file_locker' && $activity->secure_file) {
			$file = $base."lockedfiles/file/".AxsFiles::getEncodedFile($activity->secure_file);
		} else {
			$file = $activity->audio_file;
		}
	?>
	<audio style="width:100%;" src="<?php echo $file; ?>" controls> </audio>
</div>