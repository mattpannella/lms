<?php 
	$columns = 12;
	if($activity->content_width) {
		$columns = $activity->content_width;
	}
	echo '<div class="col-md-'.$columns.' no-padding">';
	if($activityTemplate != 'textfield' && $activityTemplate != 'upload') {				
		include 'components/com_splms/templates/'.$activityTemplate.'_activity.php';
	} else {
	$key = AxsKeys::getKey('lms');
	$encryptedParams = base64_encode(AxsEncryption::encrypt($activity, $key));
?>

<div class="box lms-activity" data-lesson="<?php echo $encodedLessonParams; ?>">
	<?php if($requiredBox) { echo $requiredBox; } ?>
	<div class="row">
		<div class="col-md-6">
		<?php if($activity->title) { ?>
			<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
				<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
			</div>
		<?php } ?>
		</div>
		<div class="col-md-6">
			<div id="submission_<?php echo $activity->id; ?>" class="activity-submission" <?php echo $submissionDisplay; ?>>
				<span id="submission_<?php echo $activity->id; ?>-uploading-message" style="display:none">
					<span class="fa fa-spinner fa-spin"></span> <?php echo AxsLanguage::text("AXS_UPLOADING", "Uploading") ?>...	
				</span>
				<span id="submission_<?php echo $activity->id; ?>-upload-success-icon" <?php if(!$submission) {echo 'style="display:none"';} ?>>
					<span class="fa fa-check-circle-o"></span>
					<span class="upload-success-text"><?php echo $submission; ?></span>
				</span>
				<?php if($activity->allow_resubmit) {?>
				<div class="col-md-12 mt-4">
					<button class="activity-resubmit" data-activityid="<?php echo $activity->id; ?>"><i class="far fa-history"></i> <?php echo AxsLanguage::text("COM_LMS_LESSON_TRYAGAIN", "Try again.")?></button>
					<div>
						<small class="text-danger"><?php echo AxsLanguage::text("COM_LMS_ACTIVITY_TRYAGAIN_WARNING","Caution: Uploading a new answer archives the previous answer")?>
						</small>
					</div>
				</div>	
				<?php } ?>				
			</div>
			<form id="form_<?php echo $activity->id; ?>" method="post" enctype="multipart/form-data" class="activity-form" <?php echo $formDisplay; ?>>
				<div class="form-group">
				    <?php include 'components/com_splms/templates/'.$activityTemplate.'_activity.php';?>
				</div>
				<input type="hidden" name="activity" value="<?php echo $encryptedParams; ?>"/>
			</form>
		</div>
	</div>
</div>
<?php } 

	echo '</div>';

?>