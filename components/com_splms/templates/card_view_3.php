<?php
    $topBarExtraClass = '';
    $topBarText = '';

    if ($completed) {
        $topBarExtraClass = 'course-card-topbar-completed';
        $topBarText       = AxsLanguage::text("AXS_COMPLETED", "Completed");
    }
    if ($inProgress) {
        $topBarText       = AxsLanguage::text("AXS_IN_PROGRESS", "In Progress");
	}
	$alert = '';
	if ($params->locked && !$purchased) {
		$link = '#';
		$alertText = AxsLanguage::text("COM_SPLMS_AXS_COURSE_IS_LOCKED", "This Course is Locked");
		$alert = "onClick='() => alert(``)}";
	}
	$bookmarkedData = 0;
	$bookmarkedClass = 'style="display:none"';
?>


<a href="<?php echo $link; ?>" <?php echo $alert; ?> target="<?php echo $target; ?>" class="cardBox course-card-v3 w-inline-block
<?php
    if ($recommended) 	{ echo " recommended"; 	}
    if ($wishlisted) 	{
		echo " wishlisted";
		$bookmarkedData = 1;
		$bookmarkedClass = '';
	}
    if ($assigned) 		{ echo " assigned"; 	}
    if ($completed) 	{ echo " completed"; 	}
    if ($inProgress) 	{ echo " inProgress"; 	}
	if ($purchased) 	{ echo " purchased"; 	}
?>
"
<?php if($item->image) {
	$item->image = str_replace('https://tovuti.io/','/',$item->image);
	$item->image = str_replace('https://www.tovuti.io/','/',$item->image);
	$item->image = str_replace('https://tovutilms.com/','/',$item->image);
?>
	style="background-image: url('<?php echo $item->image; ?>');"
<?php } ?>
>

	<div class="course-card-v3-top">
		<?php if ($assigned && !$completed) {
			$due_date = '';
			$now = strtotime('now');
			$dt = strtotime($assigned->due_date);
			if ($assigned->due_date != '0000-00-00 00:00:00' && $dt > $now) {
				$due_date = AxsLanguage::text("AXS_DUE", "Due") . ' ' .date("m/d",strtotime($assigned->due_date));
			}
			if($dt < $now && $assigned->due_date != '0000-00-00 00:00:00' && $assigned->due_date) {
				$due_date = AxsLanguage::text("AXS_PAST_DUE", "Past Due");
			}
		?>
		<div class="course-card-assigned v3">
			<div class="course-card-icon"></div>
			<h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_ASSIGNED", "In Assigned") ?> <?php echo $due_date; ?></h6>
		</div>
		<?php } ?>
		<?php if (!$assigned && $recommended && !$completed) { ?>
		<div class="course-card-recommended v3">
			<div class="course-card-icon"></div>
			<h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_RECOMMENDED", "Recommended") ?></h6>
		</div>
		<?php } ?>
		<div class="course-card-bookmark wishlist v3" data-wishlisted="<?php echo $bookmarkedData; ?>" data-course-id="<?php echo $item->splms_course_id; ?>">
			<div class="bookmark-stacked">
			<img src="/templates/axs/elements/lms/courses-list/images/bookmark-1.svg" alt="" class="bookmark-icon">
			<img src="/templates/axs/elements/lms/courses-list/images/bookmarked-1.svg" <?php echo $bookmarkedClass; ?> alt="" class="bookmark-icon bookmarked">
			</div>
		</div>

	</div>
	<div class="course-card-v3-bottom">
		<?php if($settings->show_lessons_count) { ?>
		<div class="course-card-details-row v3">
			<div class="course-card-details-text light"><?php echo count($lessons); ?> <?php
					if ($params && $params->lesson_button_text && $params->lesson_button_text != 'Lessons') {
						echo $params->lesson_button_text;
					} else {
						if(count($lessons) != 1){
							echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_LESSONS", "Lessons");
						}
						else{
							echo AxsLanguage::text("AXS_LESSON", "Lesson");
						}
					}
				?></div>
		</div>
		<?php } ?>
		<h3 class="course-card-title light v3"><?php echo AxsActivityData::truncate($item->title,200); ?></h3>
			<?php if ($inProgress ||$completed) {
				if($completed) {
					$percentComplete = 100;
				} else {
					$percentComplete = $inProgress->percentComplete;
				}
			?>
				<div class="course-progress-container v3">
					<div
						style="width: <?php echo $percentComplete.'%'; ?>;height:15px"
						class="course-progress-bar"
					></div>
				</div>
			<?php } ?>
	</div>
</a>