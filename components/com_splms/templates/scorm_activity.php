<?php
	$scorm_temp_id = AxsLMS::createUniqueID();
	// the fullscreen button doesn't work on mobile
	$isMobileBrowser = AxsMobileHelper::isClientUsingMobile();
	//if it is on a mobile change height to 60 % of the vertical height of the device to prevent scrolling issues - mainly for iOS
	if($isMobileBrowser) {
		$height = '60vh';
	}
	if($params->scorm_type == 'bizlibrary') {
		$scolling = 'scrolling="no"';
	}
?>

<?php if (!$isMobileBrowser) : ?>
	<a class="btn btn-default fullscreen-btn" data-scorm="<?php echo $scorm_temp_id; ?>"><i class="lizicon-enlarge"></i> FullScreen</a>
<?php endif; ?>

<iframe
	src="/?tmpl=scormplayer&params=<?php echo $encryptedScormParams; ?>"
	style="width: 100%; overflow: auto; height: <?php echo $height; ?>;"
	frameborder="0"
	class="scorm_course"
	<?php echo $scolling; ?>
	id="scorm_frame_<?php echo $scorm_temp_id; ?>"
	allow="autoplay; fullscreen"
	allowfullscreen="allowfullscreen">
</iframe>

<?php if (!$isMobileBrowser) : ?>
	<script>
		$('.fullscreen-btn').click(function() {
			var scorm_id = '#scorm_frame_' + $(this).data('scorm');
			var scorm_element = document.querySelector(scorm_id);
			if (scorm_element.requestFullscreen) {
				scorm_element.requestFullscreen();
			} else if (scorm_element.webkitRequestFullscreen) { /* Safari */
				scorm_element.webkitRequestFullscreen();
			} else if (scorm_element.msRequestFullscreen) { /* IE11 */
				scorm_element.msRequestFullscreen();
			}
		})
	</script>
<?php endif ?>
