<div class="input-group col-lg-12">
	<span class="input-group-addon">
		<i class="fa <?php echo $textFieldIcon; ?>"></i>
	</span>
	<input
		data-id="activity_<?php echo $activity->id; ?>" 
		class="activity_<?php echo $activity->id; ?> form-control input-lg activity_file" 
		placeholder="<?php echo $textFieldPlaceholder; ?>"
		name="textbox" 
		type="text" 
		required 
	/>				    	
	<span class="input-group-btn">
		<button 
			class="activity-submit btn btn-primary input-lg" 
			data-id="activity_<?php echo $activity->id; ?>" 
			data-task="youtube" 
			type="submit"
		><i class="fa fa-check-circle"></i> <?php echo AxsLanguage::text("COM_LMS_SUBMIT", "Submit"); ?></button>
	</span>
</div>