<div class="input-group col-lg-12">
	<span class="input-group-addon">
		<i class="fa fa-file"></i>
	</span>
	<input 
		data-id="activity_<?php echo $activity->id; ?>" 
		class="activity_<?php echo $activity->id; ?> form-control input-lg activity_file" 
		placeholder="<?php echo AxsLanguage::text("COM_LMS_UPLOAD_PLACEHOLDER","Upload File"); ?>"
		name="filename"  
		type="text" 
		readonly 
	/>
	<input 
		required 
		id="activity_<?php echo $activity->id; ?>" 
		type="file" 
		name="file" 
		class="file"
	/>				    	
	<span class="input-group-btn">
	<button 
		class="browse btn btn-primary input-lg" 
		data-id="activity_<?php echo $activity->id; ?>" 
		type="button"
	>
		<i class="fa fa-search"></i> <?php echo AxsLanguage::text("COM_LMS_BROWSE","Browse"); ?>
	</button>
	</span>
</div>				    				    
<button 
	class="activity-submit upload-activity btn btn-success input-md" 
	style="width: 100%; margin-top: 10px; display: none;" 
	data-id="activity_<?php echo $activity->id; ?>" 
	data-task="uploadActivity"
	type="submit"
>
	<i class="fa fa-upload"></i> <?php echo AxsLanguage::text("COM_LMS_UPLOAD","Upload Your File"); ?>
</button>