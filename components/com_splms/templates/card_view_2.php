<?php

	if(!$cardLinearGradient) {
		$cardLinearGradient = "linear-gradient(45deg, #1e2935 23%, rgba(30, 41, 53, 0.63) 59%, #1e2935)";
	}

    $topBarExtraClass = '';
    $topBarText = '';

    if ($completed) {
        $topBarExtraClass = 'course-card-topbar-completed';
        $topBarText       = AxsLanguage::text("AXS_COMPLETED", "Completed");
    }
    if ($inProgress) {
        $topBarText       = AxsLanguage::text("AXS_IN_PROGRESS", "In Progress");
	}
	$alert = '';
	if ($params->locked && !$purchased) {
		$link = '#';
		$alert = 'onClick="function(){alert(\'This Course is Locked\')}"';
	}
	$bookmarkedData = 0;
	$bookmarkedClass = 'style="display:none"';
?>


<a href="<?php echo $link; ?>" <?php echo $alert; ?> target="<?php echo $target; ?>" class="cardBox course-card-v2 w-inline-block
<?php
    if ($recommended) 	{ echo " recommended"; 	}
    if ($wishlisted) 	{
		echo " wishlisted";
		$bookmarkedData = 1;
		$bookmarkedClass = '';
	}
    if ($assigned) 		{ echo " assigned"; 	}
    if ($completed) 	{ echo " completed"; 	}
    if ($inProgress) 	{ echo " inProgress"; 	}
	if ($purchased) 	{ echo " purchased"; 	}
?>
"
<?php if($item->image) {
	$item->image = str_replace('https://tovuti.io/','/',$item->image);
	$item->image = str_replace('https://www.tovuti.io/','/',$item->image);
	$item->image = str_replace('https://tovutilms.com/','/',$item->image);
?>
	style="background-image: <?php echo $cardLinearGradient; ?>,url('<?php echo $item->image; ?>');"
<?php } ?>
>

	<div class="course-card-v2-top">
		<h4 class="course-category-h4"><?php echo $model->getCategoryById($item->splms_coursescategory_id)->title; ?></h4>
		<h3 class="course-card-title light v2">
			<?php echo AxsActivityData::truncate($item->title,200); ?>
		</h3>
	</div>
	<div class="course-card-v2-bottom">
		<div class="course-card-topbar <?php echo $topBarExtraClass; ?> v2">
			<?php if ($inProgress) { ?>
				<div class="course-card-icon"></div>
				<div class="course-card-topbar-text"><?php echo $topBarText; ?></div>
				<div class="course-progress-container">
					<div
						style="width: <?php echo $inProgress->percentComplete.'%'; ?>;height:15px"
						class="course-progress-bar"
					></div>
				</div>
			<?php } ?>
			<?php if ($completed) { ?>
				<div class="course-card-icon tovuti-spacer-20"></div>
				<div class="course-card-topbar-text v2"><?php echo $topBarText; ?></div>
			<?php } ?>
		</div>
	</div>
	<div class="course-card-details v2">
		<?php if($settings->show_teachers_count) { ?>
		<div class="course-card-details-row v2">
			<div class="course-card-icon"></div>
			<div class="course-card-details-text light">
				<?php
					if ($params && $params->teacher_list_text && $params->teacher_list_text != 'Teachers') {
						echo $params->teacher_list_text;
					} else {
						echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_TEACHERS", "Teachers");
					}
				?>
			</div>
			<div class="course-card-details-count teachers">
				<?php echo count($teachers); ?>
			</div>
		</div>
		<?php } ?>
		<?php if($settings->show_lessons_count) { ?>
		<div class="course-card-details-row" v2>
			<div class="course-card-icon"></div>
			<div class="course-card-details-text light">
				<?php
					if ($params && $params->lesson_button_text && $params->lesson_button_text != 'Lessons') {
						echo $params->lesson_button_text;
					} else {
						if(count($lessons) != 1){
							echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_LESSONS", "Lessons");
						}
						else{
							echo AxsLanguage::text("AXS_LESSON", "Lesson");
						}
					}
				?>
			</div>
			<div class="course-card-details-count"><?php echo count($lessons); ?></div>
		</div>
		<?php } ?>
		<?php if($settings->show_pricing) { ?>
		<div class="course-card-details-row v2">
			<div class="course-card-icon"></div>
			<div class="course-card-details-text light">
				<?php echo AxsLanguage::text("COM_SPLMS_AXS_COURSE_PRICE", "Price");?>
			</div>
			<div class="course-card-details-count">
			<?php
				if($item->price != '0.00') {
					echo $currency_symbol.$item->price;
				} else {
					echo AxsLanguage::text("AXS_FREE", "Free");
				}
			?>
			</div>
		</div>
		<?php } ?>
	</div>
	<div class="course-card-bookmark wishlist" data-wishlisted="<?php echo $bookmarkedData; ?>" data-course-id="<?php echo $item->splms_course_id; ?>">
      	<div class="bookmark-stacked">
		  <img src="/templates/axs/elements/lms/courses-list/images/bookmark-1.svg" alt="" class="bookmark-icon">
		  <img src="/templates/axs/elements/lms/courses-list/images/bookmarked-1.svg" <?php echo $bookmarkedClass; ?> alt="" class="bookmark-icon bookmarked">
		</div>
    </div>
	<?php if ($assigned && !$completed) {
		$due_date = '';
		$now = strtotime('now');
		$dt = strtotime($assigned->due_date);
		if ($assigned->due_date != '0000-00-00 00:00:00' && $dt > $now) {
			$due_date = AxsLanguage::text("AXS_DUE", "Due") . ' ' .date("m/d",strtotime($assigned->due_date));
		}
		if($dt < $now && $assigned->due_date != '0000-00-00 00:00:00' && $assigned->due_date) {
			$due_date = AxsLanguage::text("AXS_PAST_DUE", "Past Due");
		}
	?>
	<div class="course-card-assigned v2">
		<div class="course-card-icon"></div>
		<h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_ASSIGNED", "In Assigned") ?> <?php echo $due_date; ?></h6>
	</div>
	<?php } ?>
	<?php if (!$assigned && $recommended && !$completed) { ?>
	<div class="course-card-recommended v2">
		<div class="course-card-icon"></div>
		<h6 class="tovuti-course-card-badge-h6"><?php echo AxsLanguage::text("AXS_RECOMMENDED", "Recommended") ?></h6>
	</div>
	<?php } ?>
</a>