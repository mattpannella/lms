<?php
	$key = AxsKeys::getKey('lms');
	$tokenParams = new stdClass();
	$tokenParams->timestamp = time();
	$tokenParams->interactive_id = $activity->interactive_id;
	$tokenParams->user_id = JFactory::getUser()->id;
	$encryptedToken = base64_encode(AxsEncryption::encrypt($tokenParams, $key));
?>
<div class="box lms-activity">
	<?php if($requiredBox) { echo $requiredBox; } ?>
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>

	<iframe src="/?tmpl=ic_embed&embed_id=<?php echo $activity->interactive_id; ?>&lesson=<?php echo $encodedLessonParams; ?>&token=<?php echo $encryptedToken; ?>" style="width: 100%;" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
	<script src="/components/com_interactivecontent/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>
</div>