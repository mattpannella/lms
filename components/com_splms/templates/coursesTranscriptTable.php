
<?php
/**
 * Required variables from AxsTranscripts::getTranscriptCoursesTableData() scope:
 * $user_id = encoded in url param for transcripts page
 * $courses = all the courses that the user has a course_progress entry for
 * $lessons = all the lessons with for $courses
 * $lesson_status_started = hashmap of lesson_id->lesson_status db row with 'started' status
 * $lesson_status_completed = hashmap of lesson_id->lesson_status db row with 'completed' status
 * $activities = all the activities for $lessons by the user with id = $user_id
 * $collapse = if set to true, enable collapse areas and buttons
*/

/**
 * Renders html markup for a collapse row for the given lesson with
 * all the activities for the lesson
 *
 * @param array $lesson_activities  The activities for this lesson
 *
 * @return string
 */
$renderActivitiesRow = function($lesson_activities) {
	ob_start();
	foreach ($lesson_activities as $lesson_activity): ?>
		<div class="tov-trans-lessons-wrapper">
			<div class="tov-trans-lesson-indicator">
				<i class="fas fa-caret-right"></i>
			</div>
			<div class="tov-trans-course-columns">
				<div class="tov-trans-course-column tov-trans-course-column-first activity-type">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-type <?php echo $lesson_activity->type ?>">
							<i class="<?php echo AxsTranscripts::getActivityIcon($lesson_activity->type) ?> tov-trans-me-2"></i><?php echo ucfirst($lesson_activity->type) ?>
						</p>
						<p class="tov-trans-detail-activity"><?php echo $lesson_activity->title ?></p>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("JSTATUS", "Status") ?></p>
						<badge class="tov-trans-course-badge tov-trans-badge-<?php echo str_replace(" ", "-", strtolower($lesson_activity->completed)); ?>"
						><?php echo AxsTranscripts::getStatusTranslation($lesson_activity->completed) ?></badge>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<?php if (!empty($lesson_activity->date)): ?>
							<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_DATE_SUBMITTED", "Date Submitted") ?></p>
							<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($lesson_activity->date) ?></p>
						<?php endif; ?>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<?php if (!empty($lesson_activity->grade)): ?>
							<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_SCORE", "Score") ?></p>
							<p class="tov-trans-detail-data">
                                <?php echo $lesson_activity->grade; ?>%
                            </p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php
	endforeach;
	return ob_get_clean();
};


/**
 * Renders html markup for a collapse row for the given course with
 * all the lessons in the course
 *
 * @param array $course_lessons  An array of lessons to render
 *
 * @return string
 */
$renderLessonsTable = function($course_lessons)
	use (
		$renderActivitiesRow,
		$lesson_statuses_completed,
		$lesson_statuses_started,
		$activities,
		$quizresults,
		$collapse
	)
{
	ob_start();
	foreach ($course_lessons as $lesson):
		$date_started = isset($lesson_statuses_started[$lesson->splms_lesson_id]) ? $lesson_statuses_started[$lesson->splms_lesson_id]['date'] : "";
		$date_completed = isset($lesson_statuses_completed[$lesson->splms_lesson_id]) ? $lesson_statuses_completed[$lesson->splms_lesson_id]['date'] : "";

		$lesson_status = isset($lesson_statuses_started[$lesson->splms_lesson_id]) ? "In Progress" : "Not Started";
		$lesson_status = isset($lesson_statuses_completed[$lesson->splms_lesson_id]) ? "Completed" : $lesson_status;

		$params = json_decode($lesson->params);
		$lesson_activities = AxsTranscripts::processLessonActivities(json_decode($params->student_activities), $quizresults, $activities, $lesson->splms_lesson_id);
		?>
		<div class="tov-trans-lessons-wrapper">
			<div class="tov-trans-course-columns">
				<div class="tov-trans-course-column tov-trans-course-column-first lessons">
					<div class="tov-trans-course-img lesson">
						<?php if (isset($lesson->image) && !empty($lesson->image) && $lesson->image != '/'): ?>
							<img style="max-width:100%" src="<?php echo $lesson->image; ?>"/>
						<?php else: ?>
							<svg><rect width="100%" height="100%" fill="<?php echo AxsTranscripts::getColorFromStr($lesson->course_id + 'lesson')  ?>"/></svg>
						<?php endif; ?>
					</div>
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_LESSON", "Lesson") ?></p>
						<p class="tov-trans-detail-data"><?php echo $lesson->title ?></p>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("JSTATUS", "Status") ?></p>
						<badge class="tov-trans-course-badge tov-trans-badge-<?php echo str_replace(" ", "-", strtolower($lesson_status)) ?>"><?php echo AxsTranscripts::getStatusTranslation($lesson_status) ?></badge>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_DATE_STARTED", "Date Started") ?></p>
						<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($date_started) ?></p>
					</div>
				</div>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_DATE_COMPLETED", "Date Completed") ?></p>
						<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($date_completed) ?></p>
					</div>
				</div>
				<?php if ($collapse == "collapse"): ?>
					<div class="tov-trans-course-expand">
						<button
							class="tov-trans-course-expand-btn lessons btn-sm"
							id="expandButtonLesson-<?php echo $lesson->splms_lesson_id ?>"
							data-target="#lessonActivities-<?php echo $lesson->splms_lesson_id ?>"
							data-toggle="collapse"
							aria-expanded="false"
							aria-controls="lessonActivities-<?php echo $lesson->splms_lesson_id ?>"
							type="button"
						><?php echo sprintf(AxsLanguage::text("AXS_TRANS_LESSON_CONTAINS_ACTIVITIES", "This lesson contains %s activities, click here to expand"), count($lesson_activities)) ?>
						<i class="fas fa-chevron-square-down tov-trans-hide-mobile" data-target="#lessonActivities-<?php echo $lesson->splms_lesson_id ?>" data-toggle="collapse"></i>
						<i class="fas fa-chevron-square-up tov-trans-hide-mobile" style="display:none" data-target="#lessonActivities-<?php echo $lesson->splms_lesson_id ?>" data-toggle="collapse"></i>
						</button>
					</div>
				<?php endif; ?>
				<?php if (!empty($lesson_activities)): ?>
					<div id="lessonActivities-<?php echo $lesson->splms_lesson_id?>" class="<?php echo $collapse ?> tov-trans-activity-items">
						<?php echo $renderActivitiesRow($lesson_activities); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	<?php endforeach;
	return ob_get_clean();
};

foreach($courses as $course):
    if (empty($course->title)) {
      continue;
    }
?>
	<div class="tov-trans-card-course">
		<div class="tov-trans-course-columns">
			<div class="tov-trans-course-column tov-trans-course-column-first">
				<div class="tov-trans-course-img">
					<?php if (isset($course->image) && !empty($course->image) && $course->image != '/'): ?>
						<img style="max-width:100%" src="<?php echo $course->image; ?>" />
					<?php else: ?>
						<svg><rect width="100%" height="100%" fill="<?php echo AxsTranscripts::getColorFromStr($course->splms_course_id) ?>"/></svg>
					<?php endif; ?>
				</div>
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_COURSE", "Course") ?></p>
					<p class="tov-trans-detail-data"><?php echo $course->title; ?></p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("JSTATUS", "Status") ?></p>
					<badge class="tov-trans-course-badge tov-trans-badge-<?php echo str_replace(" ", "-", strtolower($course->status)); ?>"><?php echo AxsTranscripts::getStatusTranslation($course->status); ?></badge>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_PROGRESS", "Progress") ?></p>
					<p class="tov-trans-detail-data"><?php echo $course->percentComplete . '%'; ?></p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("COM_SPLMS_QUIZ_AVG", "Quiz Avg.") ?></p>
					<p class="tov-trans-detail-data"><?php echo $course->score . '%'; ?></p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_ENROLLED", "Enrolled") ?></p>
					<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($course->date_started); ?></p>
				</div>
			</div>
			<div class="tov-trans-course-column">
				<div class="tov-trans-course-col-details">
					<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_COMPLETED", "Completed") ?></p>
					<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($course->date_completed); ?></p>
				</div>
			</div>
			<?php if (isset($course->params->use_due_date) && $course->params->use_due_date == true): ?>
				<div class="tov-trans-course-column">
					<div class="tov-trans-course-col-details">
						<p class="tov-trans-detail-header"><?php echo AxsLanguage::text("AXS_DUE", "Due") ?></p>
						<p class="tov-trans-detail-data"><?php echo AxsLMS::formatDate($course->params->due_static_date); ?></p>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php
			$course_lessons = array_filter($lessons, function($l) use ($course) { return $l->splms_course_id == $course->splms_course_id; });
		?>
		<!-- if lessons is null, show_full_course_data is false -->
		<?php if ($lessons != null && $collapse == "collapse"): ?>
			<div class="tov-trans-course-expand">
				<button
					id="expandButtonCourse-<?php echo $course->splms_course_id ?>"
					class="tov-trans-course-expand-btn btn-sm"
					data-toggle="collapse"
					data-target="#courseLessons-<?php echo $course->splms_course_id ?>"
					aria-expanded="false"
					aria-controls="courseLessons-<?php echo $course->splms_course_id ?>"
					type="button"
				>
					<?php echo sprintf(AxsLanguage::text("AXS_TRANS_COURSE_LESSONS_COUNT", "This course contains %s lessons, click here to expand"), count($course_lessons)) ?>
					<i class="fas fa-chevron-square-down tov-trans-hide-mobile" data-toggle="collapse" data-target="#courseLessons-<?php echo $course->splms_course_id ?>"></i>
					<i class="fas fa-chevron-square-up tov-trans-hide-mobile" data-toggle="collapse" data-target="#courseLessons-<?php echo $course->splms_course_id ?>" style="display:none"></i>
				</button>
			</div>
		<?php endif; ?>
		<div id="courseLessons-<?php echo $course->splms_course_id?>" class="<?php echo $collapse ?>">
			<?php echo $renderLessonsTable($course_lessons); ?>
		</div>

	</div>
<?php
endforeach;
