<div class="box lms-activity">
	<?php if($activity->title) { ?>
		<div class="lms-subtitle"><?php echo $icon; ?> <?php echo $activity->title; ?>
			<div class="activity-instructions"><?php echo $activity->instructions; ?></div>
		</div>
	<?php } ?>
	<?php
		$embedUrl = '';
		$base = JUri::base();
		$random = rand(100,100000);
		$mobileDeviceList = ['iPad','iPhone'];
		$isAppleDevice = AxsMobileHelper::isClientUsingMobile($mobileDeviceList);
		$androidMobileDeviceList = ['Android'];
		$isAndroidDevice = AxsMobileHelper::isClientUsingMobile($androidMobileDeviceList);
		$linkStart = '';
		$linkEnd   = '';
		$officeViewer = "https://view.officeapps.live.com/op/embed.aspx?src=";
		$googleViewer = "https://docs.google.com/gview?embedded=true&url=";
		$googleViewerPage = "https://drive.google.com/viewerng/viewer?embedded=true&url=";
		if($activity->file_location == 'secure_file_locker' && $activity->secure_file) {
			$file = "lockedfiles/file/".AxsFiles::getEncodedFile($activity->secure_file);
		} else {
			$file = $activity->file;
		}
		switch ($activity->type) {
		 	case 'powerpoint':
				$embedUrl = $officeViewer.$base.$file;
		 		break;

			 case 'pdf':
				$embedUrl = $base . $file . '#toolbar=0';
				if($settings->allow_pdf_download) {
					$embedUrl = $base . $file;
				}
				if($isAppleDevice) {
					$linkStart = '<a href="'.$embedUrl.'" target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '';
					$linkEnd   = '</a>';
				} elseif($settings->show_open_pdf_link) {
					$linkStart = '<a href="'.$embedUrl.'" target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '</a>';
					$linkEnd   = '';
				}
				if($isAndroidDevice) {
					$linkStart = '<a href="'.$googleViewerPage . $base . $file.'" target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '</a>';
					$linkEnd   = '';
					$embedUrl = $googleViewer . $base . $file;
				}
		 		break;
		}
	?>
	<?php echo $linkStart; ?>
	<div class="embed-responsive embed-responsive-16by9">
	    <embed class="embed-responsive-item" src="<?php echo $embedUrl; ?>" style="width: 100%;" scrolling="auto" frameborder="0"></embed>
	</div>
	<?php echo $linkEnd; ?>
</div>