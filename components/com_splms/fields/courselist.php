<?php

    /**
    * @author    JoomShaper http://www.joomshaper.com
    * @copyright Copyright (C) 2010 - 2013 JoomShaper
    * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2
    */

    defined('JPATH_BASE') or die;

    // Requred Componenet and module helper
    //require_once JPATH_ROOT . '/modules/mod_sp_soccer_recent_results/helper.php';

    jimport('joomla.form.formfield');
    jimport('joomla.filesystem.folder');
    jimport('joomla.filesystem.file');
    
    class JFormFieldCourselist extends JFormField {

        protected $type = 'courselist';


        protected function getInput(){

            // Get Tournaments
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            // Select all records from the user profile table where key begins with "custom.".
            $query->select($db->quoteName(array('splms_course_id', 'title' )));
            $query->from($db->quoteName('#__splms_courses'));
            $query->where($db->quoteName('enabled')." = 1");
            $query->order('ordering ASC');

            $db->setQuery($query);  
            $results = $db->loadObjectList();
            $course_list = $results;


            foreach($course_list as $course){
                $options[] = JHTML::_( 'select.option', $course->splms_course_id, $course->title );

                //print_r($tournament);
            }
            
            return JHTML::_('select.genericlist', $options, $this->name, '', 'value', 'text', $this->value);
        }
    }
