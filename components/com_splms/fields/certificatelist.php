<?php

    /**
    * @author    JoomShaper http://www.joomshaper.com
    * @copyright Copyright (C) 2010 - 2013 JoomShaper
    * @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPLv2
    */

    defined('JPATH_BASE') or die;

    // Requred Componenet and module helper
    //require_once JPATH_ROOT . '/modules/mod_sp_soccer_recent_results/helper.php';

    jimport('joomla.form.formfield');
    jimport('joomla.filesystem.folder');
    jimport('joomla.filesystem.file');
    
    class JFormFieldCertificatelist extends JFormField {

        protected $type = 'certificatelist';


        protected function getInput(){

            // Get Certificates
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select($db->quoteName(array('b.title', 'a.splms_certificate_id', 'a.userid', 'a.instructor', 'c.name')));
            $query->from($db->quoteName('#__splms_certificates', 'a'));
            $query->join('LEFT', $db->quoteName('#__splms_courses', 'b') . ' ON (' . $db->quoteName('a.splms_course_id') . ' = ' . $db->quoteName('b.splms_course_id') . ')');
            $query->join('LEFT', $db->quoteName('#__users', 'c') . ' ON (' . $db->quoteName('a.userid') . ' = ' . $db->quoteName('c.id') . ')');
            $query->where($db->quoteName('a.enabled')." = 1");
            $query->order('a.ordering DESC');
            $db->setQuery($query);
            $results = $db->loadObjectList();

            $c_list = $results;


            foreach($c_list as $certificate){
                $options[] = JHTML::_( 'select.option', $certificate->splms_certificate_id, $certificate->title . ' ( ' . $certificate->name . ' )' );

                //print_r($tournament);
            }
            
            return JHTML::_('select.genericlist', $options, $this->name, '', 'value', 'text', $this->value);
        }
    }
