<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

require_once JPATH_COMPONENT . '/helpers/helper.php';

JHtml::_('jquery.framework');
$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$view = $app->input->get('view');

if($view != 'courses') {
	// Include JS
	$doc->addScript( JURI::root(true) . '/components/com_splms/assets/js/splms.js' );

	// Include CSS files
	$doc->addStylesheet( JURI::root(true) . '/components/com_splms/assets/css/font-splms.css' );
	$doc->addStylesheet( JURI::root(true) . '/components/com_splms/assets/css/splms.css?v=2' );
}


// Load FOF
include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
	JError::raiseError ('500', 'FOF is not installed');
	return;
}

FOFDispatcher::getTmpInstance('com_splms')->dispatch();