<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();



function SplmsBuildRoute(&$query) {

	$segments = array();

	$app		= JFactory::getApplication();
	$menu		= $app->getMenu();


	// We need a menu item.  Either the one specified in the query, or the current active one if none specified
	if (empty($query['Itemid']))
	{
		$menuItem = $menu->getActive();
		$menuItemGiven = false;
	}
	else
	{
		$menuItem = $menu->getItem($query['Itemid']);
		$menuItemGiven = true;
	}

	// Check again
	if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_splms')
	{
		$menuItemGiven = false;
		unset($query['Itemid']);
	}

	if (isset($query['view']))
	{
		$view = $query['view'];
	}
	else
	{
		// We need to have a view in the query or it is an invalid URL
		return $segments;
	}

	// Are we dealing with an article or category that is attached to a menu item?
	if (($menuItem instanceof stdClass)
			&& $menuItem->query['view'] == $query['view']
			&& isset($query['id'])
			&& $menuItem->query['id'] == (int) $query['id']) {

		unset($query['view']);
		unset($query['id']);
		return $segments;
	}

	//Replace with menu
	$mview = (empty($menuItem->query['view'])) ? null : $menuItem->query['view'];

	//Cart
	if ($view == 'cart' || $view == 'courses' || $view == 'purchases') {
		if($mview != $view) {
			$segments[] = $view;
		}
		unset($query['view']);
	}

	//
	if ($view == 'course' || $view == 'coursescategory' || $view == 'lesson' || $view == 'teacher' || $view == 'event' || $view == 'eventcategory' || $view == 'speaker') {
		
		$segments[] = $view;
		
		//Remove ID
		$id_slug = explode(':', $query['id']);
		if(count($id_slug)>=1) {
			$segments[] = $id_slug[1];
		} else {
			$segments[] = $query['id'];
		}

		unset($query['view']);
		unset($query['id']);
	}

	$total = count($segments);

	for ($i = 0; $i < $total; $i++)
	{
		$segments[$i] = str_replace(':', '-', $segments[$i]);
	}

	return $segments;

}


function SplmsParseRoute($segments) {

	$total = count($segments);
	$vars = array();

	for ($i = 0; $i < $total; $i++)
	{
		$segments[$i] = preg_replace('/-/', ':', $segments[$i], 1);
	}

	// Get the active menu item.
	$app	= JFactory::getApplication();
	$menu	= $app->getMenu();
	$item	= $menu->getActive();

	// Count route segments
	$count = count($segments);

	if ($count > 1) {
		$vars['view']	= $view = $segments[0];

		switch ($vars['view']) {

			case 'course':
				$table = 'splms_courses';
				break;

			case 'coursescategory':
				$table = 'splms_coursescategories';
				break;

			case 'lesson':
				$table = 'splms_lessons';
				break;

			case 'teacher':
				$table 	= 'splms_teachers';
				break;

			case 'event':
				$table 	= 'splms_events';
				break;

			case 'eventcategory':
				$table 	= 'splms_eventcategories';
				break;

			case 'speaker':
				$table 	= 'splms_speakers';
				break;
	
		}

		//Retrive ID
		$slug = preg_replace('/:/', '-', $segments[1]);

		$db = JFactory::getDbo();
		$dbQuery = $db->getQuery(true)
			->select( $db->quotename( 'splms_' . $view . '_id' ) )
			->from('#__' . $table)
			->where( $db->quotename('slug') . '=' . $db->quote($slug));
		$db->setQuery($dbQuery);
		$id = $db->loadResult();

		$vars['id']		= $id;		
	} else {
		$vars['view']	= $view = $segments[0];
	}

	return $vars;
}