jQuery(function($){
  google.maps.event.addDomListener(window, 'load', function(){

    var latlng = new google.maps.LatLng($('#splms-event-map').data('lat'), $('#splms-event-map').data('lng'));
    var mapOptions = {
      zoom: 15,
      center: latlng,
      scrollwheel: false
    };
    var map = new google.maps.Map(document.getElementById('splms-event-map'), mapOptions);
    var marker = new google.maps.Marker({position: latlng, map: map});
    map.setMapTypeId(google.maps.MapTypeId.ROADMAP);

  });

});