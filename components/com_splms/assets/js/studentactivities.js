$(document).on('click', '.activity_file, .browse', function(e) {
	e.preventDefault();
	var id = $(this).data('id');
	var file = $('#'+id);
	var formID = id.split('_')[1];
	file.trigger('click');
});

$(document).on('change', '.file', function() {
	var id = $(this).attr('id');
	var formID = id.split('_')[1];
		$('#form_'+formID).find('.upload-activity').show();
		$('.'+id).val($(this).val().replace(/C:\\fakepath\\/i, ''));

});

function sendAdminApprovalMessageIfConfigured(lesson_id, activity_id) {
	$.ajax({
		url: '/index.php?option=com_splms&task=lessons.sendAdminApprovalMessageIfConfigured&format=raw',
		async: false,
		data: {
			lesson_id,
			activity_id
		},
		type: 'post'
	});
}

function sendData(data, url, formID) {
	if(data) {
		// show the container div with
		// upload in progress message preloaded
		let submission = $('#submission_' + formID);
		submission.show();
		submission.find('.uploading-message').show();

		// hide form
		let form = $('#form_' + formID);
		form.hide();

		$.ajax({
			url  : url,
			data : data,
			enctype: 'multipart/form-data',
	        processData: false,
	        contentType: false,
			type : 'post'
		}).then(function (response) {
			let result = JSON.parse(response);
			// console.log(result);
			if (result.status == 'success') {
				// hide the upload in progress message
				submission.find('#submission_' + formID +'-uploading-message').hide();
				// show success icon
				submission.find('#submission_' + formID +'-upload-success-icon').show();
				// display user message
				submission.find('#submission_' + formID +'-upload-success-icon>span.upload-success-text').text(result.message);

				sendAdminApprovalMessageIfConfigured(result.lesson_id, result.activity_id);

				if (result.completed && result.activity_id) {

					let activity = $('.activity_' + result.activity_id)
					if (!activity.hasClass('ic_completed_active')) {
						activity.find('.incomplete-message').hide();
						activity.find('.completed-message').show();
						activity.addClass('ic_completed_active');
						completedAudio.play();
					}
					let requiredActivity = $('.required_' + result.activity_id);
					if (requiredActivity.hasClass('ic_required_active')) {
						requiredActivity.removeClass('ic_required_active');
					}

					checkComplete(true);
				}
			} else {
				$('#submission_' + formID).hide();
				$('#form_' + formID).show();
				alert(result.message);
			}
		});
	}
}

$('.activity-form').submit(function(e) {
	e.preventDefault();
});

function validateField(e) {
	var response = false;
	$(e).find('input').each( function() {
		if($(this)[0].hasAttribute('required') && $(this).val() == '') {
			response = $(this).attr('name')+' is required';
		}
	});
	return response;
}

$('.activity-submit').click( function(e) {
	e.preventDefault();
	var id 	   = $(this).data('id');
	var task   = $(this).data('task');
	var formID = id.split('_')[1];
	var data   = new FormData($('#form_'+formID)[0]);
	var error  = validateField($('#form_'+formID));
	if(!error) {
		data.append('lesson',lesson);
		var url  = '/index.php?option=com_splms&task=studentactivities.submitActivity&format=raw';
		sendData(data, url, formID);
	} else {
		alert(error);
	}
});


$('.activity-resubmit').click( function(e) {
	var id = $(this).data('activityid');
	$('#submission_' + id).hide();
	$('#form_' + id).show();
	let activity = $('.activity_' + id)
	activity.find('.incomplete-message').show();
	activity.find('.completed-message').hide();
	activity.removeClass('ic_completed_active');
	let submission = $('#submission_' + id);
	submission.find('#submission_' + id +'-upload-success-icon').hide();
	submission.find('#submission_' + id +'-upload-success-icon>span.upload-success-text').empty();
	let requiredActivity = $('.required_' + id);
	requiredActivity.addClass('ic_required_active');
});

