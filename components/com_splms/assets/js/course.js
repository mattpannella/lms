jQuery(document).ready(function() {
	jQuery(".showWords").hide();
	jQuery(".wordsText").show();

	jQuery('#show-intro-vid').click(function () {
		if (jQuery(this).text().trim() == 'Show Intro Video') {
			jQuery(this).text('Hide Intro Video');
		} else {
			jQuery(this).text('Show Intro Video');
		}
		jQuery('#intro-video').slideToggle();
	});
});

function show_lesson(lesson_id) {
	jQuery(".lesson_material").hide(500);

	var currently_selected = jQuery(".lesson_icon");

	currently_selected.html("");
	var newSpan = document.createElement("span");
	newSpan.setAttribute('class', "lizicon-circle-down");
	currently_selected.append(newSpan);
	currently_selected.attr('class', '');

	var is_visible = jQuery("#lesson_material_" + lesson_id).is(":visible");
	if (!is_visible) {		
		jQuery("#lesson_material_" + lesson_id).show(500);

		var newly_selected = jQuery("#lesson_icon_" + lesson_id);

		newly_selected.html("");
		newSpan = document.createElement("span");
		newSpan.setAttribute('class', "lizicon-circle-right");
		newly_selected.append(newSpan);
		newly_selected.attr('class', "lesson_icon");
	}
}

function toggle_media(media_id, teacher_id, type) {
	var video_div = jQuery("#lesson_video_" + media_id + "_" + teacher_id);
	var audio_div = jQuery("#lesson_audio_" + media_id + "_" + teacher_id);
	var comment_div = jQuery("#lesson_comments_" + media_id + "_" + teacher_id);

	var video_visible = video_div.is(":visible");
	var audio_visible = audio_div.is(":visible");
	var comment_visible = comment_div.is(":visible");

	switch (type) {
		case 'video':
			video_visible = !video_visible;
			audio_visible = false;		
			comment_visible = false;
			break;

		case 'audio':
			audio_visible = !audio_visible;
			video_visible = false;
			comment_visible = false;
			break;

		case 'comment':
			comment_visible = !comment_visible;
			video_visible = false;
			audio_visible = false;
			break;
	}

	var toggle_speed = 400;

	if (video_visible) {
		video_div.show(toggle_speed);
	} else {
		video_div.hide(toggle_speed);
	}
	    
    if (audio_visible) {
		audio_div.show(toggle_speed);
	} else {
		audio_div.hide(toggle_speed);
	}

	if (comment_visible) {
		comment_div.show(toggle_speed);
	} else {
		comment_div.hide(toggle_speed);
	}
}

function take_quiz(user_id, course_id, lesson_id, quiz_id) {

	var taken = false;
	var result;

	var change_view = function() {
		//jQuery("#take_quiz_button_" + quiz_id).hide(500);
		jQuery("#quiz_results_" + lesson_id).show(500);
		jQuery("#quiz_score_" + quiz_id).html("" + Math.floor((result['point'] * 100) / result['total_marks']) + "%");
		jQuery("#quiz_points_" + quiz_id).html(result['point'] + " / " + result['total_marks']);
		jQuery("#accurate_thoughts_button_" + lesson_id).show(500);
	}

	var check = function () {
		jQuery.ajax({
	        type: 'POST',
	        url: 'index.php?option=com_splms&view=quizquestions&task=check_results&format=raw',
	        data: { 
	        	uid 	 : user_id,
	        	cid 	 : course_id,
	        	lid 	 : lesson_id,
	            qid 	 : quiz_id,
	            language : language
	        },
	        success: function(response) {
	            if (response != '') {
	            	taken = true;	            	
	            	result = JSON.parse(response);
	            }
	        }
	    });

		var runCheck = function () {
			check();
		}

		if (!taken) {
			setTimeout(runCheck, 2000);
		} else {
			change_view();
		}
	}

	check();
}

function give_thoughts(lesson_id) {
	jQuery("#thoughts_view_" + lesson_id).slideToggle(500);
}

function save_accurate_thoughts(user_id, course_id, lesson_id) {
	
	var thoughts = jQuery("#your_accurate_thoughts_" + lesson_id).val();	

	jQuery.ajax({
        type: 'POST',
        url: 'index.php?option=com_splms&view=accuratethought&task=saveAccurateThought&format=raw',
        data: { 
        	uid :  		user_id,
        	cid : 		course_id,
        	lid : 		lesson_id,
            thoughts : 	thoughts
        },
        success: function(response) {

            jQuery("#accurate_thought_" + lesson_id).show(500);
            jQuery("#accurate_thought_text_" + lesson_id).html(response);

            jQuery("#accurate_thoughts_button_" + lesson_id).hide(500);
            jQuery("#thoughts_view_" + lesson_id).hide(500);
            
        }
    });
}


/*function create_quiz(questions, options, answers, current_question) {

	if (current_question === undefined) {		
		current_question = 0;
	}

	//alert(questions + "\n" + options + "\n" + answers + "\n" + current_question);

	var output_div = document.createElement("div");
	var options_div = document.createElement("div");
	
	output_div.id = "quiz_question_" + current_question;
	options_div.id = "quiz_options_" + current_question;

	output_div.appendChild(document.createTextNode(questions[current_question]));
	output_div.appendChild(document.createElement("br"));

	var num_questions = options.length;

	for (i = 0; i < num_questions; i++) {
		//options_div.appendChild(document.createTextNode(options[i][current_question]));
		//options_div.appendChild(document.createElement("br"));

		var radio = document.createElement("input");
		radio.type = "radio";
		radio.name = "question_name_" + current_question;
		radio.value = "question_value_" + i;
		radio.id = "question_radio_" + i;

		var label = document.createElement("label");
		label.appendChild(radio);
		label.appendChild(document.createTextNode(options[i][current_question]));		
		
		options_div.appendChild(label);
		options_div.appendChild(document.createElement("br"));
	}

	jQuery(output_div).css("font-size", "30px");
	jQuery(output_div).css("margin-left", "30px");
	jQuery(output_div).css("margin-top", "30px");

	jQuery(options_div).css("font-size", "20px");
	jQuery(options_div).css("margin-left", "10px");
	jQuery(options_div).css("margin-top", "30px");

	output_div.appendChild(options_div);

	var submit_button = document.createElement("button");	
	submit_button.className = "btn btn-primary";
	submit_button.appendChild(document.createTextNode("Submit"));

	output_div.appendChild(submit_button);


	jQuery(submit_button).click(
		function() {
			var choice;
			var correct = answers[current_question];
			for (i = 0; i < num_questions; i++) {

				var radio = document.getElementById("question_radio_" + i);
				if (radio.checked) {
					choice = i;
				}
			}

			if (choice == correct) {
				var rightText = "\nYou were correct!";
			} else {
				var rightText = "\nYou are incorrect.";
			}
			alert("You chose: " + choice + "\nCorrect: " + correct + rightText);
			create_quiz(questions, options, answers, current_question + 1);
		}
	);


	jQuery('#quiz_view_1').html(output_div);
}*/
