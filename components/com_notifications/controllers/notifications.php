<?php
defined('_JEXEC') or die();

class NotificationsControllerNotifications extends FOFController {

	public function saveAction(){
		$message = 'failed';
		$input  = JFactory::getApplication()->input;
    	$action 	= $input->post->get('action');
        $encryptedData = base64_decode($action);
        $key = AxsKeys::getKey('notifications');
        $data = AxsEncryption::decrypt($encryptedData, $key);
    	$userId = JFactory::getUser()->id;
    	if($data->action && $data->notification_id && $userId) {
    		$data->user_id = $userId;
    		$data->date_created = date('Y-m-d H:i:s');
    		$db = JFactory::getDBO();
    		$insert = $db->insertObject('axs_notifications_tracking', $data);
    		if($insert){
    			$message = "success";
    		}
    	} else {
            if($data->notification_id) {
                if($_COOKIE['notifications']) {
                    $notificationsArray = explode(',',$_COOKIE['notifications']);                
                } else {
                    $notificationsArray = array();
                }
                array_push($notificationsArray, $data->notification_id);
                $notifications = implode(',', $notificationsArray);
                setcookie("notifications", $notifications, time()+31556926 ,'/');
            }
        }

    	echo $message;  	
	}
}