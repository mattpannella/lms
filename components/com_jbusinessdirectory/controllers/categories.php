<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class JBusinessDirectoryControllerCategories extends JControllerLegacy
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	 
	function __construct()
	{
		parent::__construct();
	}

	function displayCategories(){
		
		parent::display();
	}
	
	function getCategories(){
		$input = JFactory::getApplication()->input;
		$keyword = $input->get('term',null,'STRING');
		$type = $input->get('type', CATEGORY_TYPE_BUSINESS, 'INT');

		//dmp($keyword);
		if(empty($keyword)){
			JFactory::getApplication()->close();
		}
		
		$categoriesModel = $this->getModel("Categories");
		
		$categoriesList = $categoriesModel->getCategoriesList($keyword, $type);
		header('Content-Type: application/json');
		echo $categoriesList;
		
		JFactory::getApplication()->close();
		}

	/**
	 * Get's the id, type, level and parentId of a category and calls the respective method in the model
	 * The level is used to determine the order the select box is shown on the view
	 * The parentId is used to fetch all the subcategories in order to populate the drop down list
	 * The category id is used to determine which category is selected by default inside this drop down list
	 * The type is used to fetch only the categories of that type
	 */
	function getSubcategoriesByParentIdAjax(){
		$parentId = JRequest::getVar('parentId', null);
		$type = JRequest::getVar('categoryType');
		$level = JRequest::getVar('level');
		$catId = JRequest::getVar('categoryId', null);

		$model = $this->getModel('Categories');
		$result = $model->getSubcategoriesByParentIdAjax($parentId, $type, $level, $catId);

		/* Send as JSON */
		header("Content-Type: application/json", true);
		echo json_encode($result);
		exit;
	}

	function getAllParentsByIdAjax(){
		$id = JRequest::getVar('categoryId', null);

		$model = $this->getModel('Categories');
		$result = $model->getAllParentsById($id);

		/* Send as JSON */
		header("Content-Type: application/json", true);
		echo json_encode($result);
		exit;
	}
}