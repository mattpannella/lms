<?php

/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );



class JBusinessDirectoryControllerEvent extends JControllerLegacy
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	 
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Gets all the data about the tickets that are being booked 
	 * On success, it redirects to the event guest details page for the user to input further details for the booking.
	 */
	function reserveTickets(){
				
		$eventModel = $this->getModel("Event");
		$result = $eventModel->reserveTickets();
		$eventId = JRequest::getVar('eventId');
		
		if($result){
			$this->setRedirect(JRoute::_('index.php?option=com_jbusinessdirectory&view=eventguestdetails&layout=edit', false));
		}else{
			$this->setRedirect(JRoute::_('index.php?option=com_jbusinessdirectory&view=event&eventId='.$eventId, false));
		}
			
	}

	function checkEventsAboutToExpire(){
		$model = $this->getModel('event');
		$model->checkEventsAboutToExpire();
	}
}