<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';

JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/companies-list-view-blue.css');
JHtml::_('stylesheet', 'https://fonts.googleapis.com/css?family=Raleway:700');

$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
$enableSEO = $appSettings->enable_seo;
$enablePackages = $appSettings->enable_packages;
$enableRatings = $appSettings->enable_ratings;
$enableNumbering = $appSettings->enable_numbering;
$user = JFactory::getUser();

$total_page_string = $this->pagination->getPagesCounter();
$current_page = substr($total_page_string,5,1);
$limitStart = JFactory::getApplication()->input->get('limitstart');
if(empty($limitStart) || ($current_page == 1) || $total_page_string==null ) {
    $limitStart = 0;
}
$showData = !($user->id==0 && $appSettings->show_details_user == 1);
?>

<div id="results-container" itemscope itemtype="http://schema.org/ItemList" <?php echo $this->appSettings->search_view_mode?'style="display: none"':'' ?> class="compact-list">
    <?php
    if(!empty($this->companies)){
        foreach($this->companies as $index=>$company){
            ?>
            <div class="result result-blue <?php echo isset($company->featured) && $company->featured==1?"featured":"" ?>">

                <div class="business-container" itemprop="itemListElement" itemscope itemtype="http://schema.org/Organization">

                    <div class="business-details business-details-blue row-fluid">
                        <?php if(isset($company->packageFeatures) && in_array(SHOW_COMPANY_LOGO,$company->packageFeatures) || !$enablePackages){ ?>
                            <div class="company-image company-image-blue span2" itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
                                <a href="<?php echo JBusinessUtil::getCompanyLink($company)?>">
                                    <?php if(isset($company->logoLocation) && $company->logoLocation!=''){?>
                                        <img title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.$company->logoLocation ?>" itemprop="contentUrl" />
                                    <?php }else{ ?>
                                        <img title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>" itemprop="contentUrl" />
                                    <?php } ?>
                                </a>
                            </div>
                        <?php } ?>

                        <div class="company-info <?php echo isset($company->packageFeatures) && in_array(SHOW_COMPANY_LOGO,$company->packageFeatures) || !$enablePackages && isset($company->logoLocation) && $company->logoLocation!=''?'span10':'span12'?>">
                            <div class="row-fluid">
                                <div class="span8 span8-blue">
                                    <h3 class="business-name">
                                        <a href="<?php echo JBusinessUtil::getCompanyLink($company)?>"><?php echo $enableNumbering? "<span>".($index + $limitStart + 1).". </span>":""?><span itemprop="name"><?php echo $company->name."&nbsp;&nbsp;" ?> </span><?php if(isset($company->slogan) && strlen($company->slogan)>2){?>
                                                <span class="business-slogan"><?php echo $company->slogan ?> </span>
                                            <?php }?></a>
                                    </h3>
                                    <div class="content-box">
                                        <?php if(!empty($company->short_description)){ ?>
                                            <div class="classification">
                                                <div class="categories short-desc">
                                                    <?php echo $company->short_description ?>
                                                </div>
                                            </div>
                                        <?php } ?>


                                        <?php if(!empty($company->address)){ ?>
                                            <div class="company-address">
                                                <i class="dir-icon-map-marker light-blue-marker"></i> <?php echo JBusinessUtil::getAddressText($company) ?>
                                            </div>
                                        <?php } ?>

                                    </div>

                                </div>

                                <div class="span4 span4-blue">

                                    <div class="business-info">
                                        <div class="company-rating" <?php echo !$enableRatings? 'style="display:none"':'' ?>>
                                            <div style="display:none" class="thanks-rating tooltip">
                                                <div class="arrow">�</div>
                                                <div class="inner-dialog">
                                                    <a href="javascript:void(0)" class="close-button"><?php echo JText::_('LNG_CLOSE') ?></a>
                                                    <p>
                                                        <strong><?php echo JText::_('LNG_THANKS_FOR_YOUR_RATING') ?></strong>
                                                    </p>
                                                    <p><?php echo JText::_('LNG_REVIEW_TXT') ?></p>
                                                    <p class="buttons">
                                                        <a  onclick="" class="review-btn track-write-review no-tracks" href=""><?php echo JText::_("LNG_WRITE_A_REVIEW")?></a>
                                                        <a href="javascript:void(0)" class="close-button">X <?php echo JText::_('LNG_NOT_NOW') ?></a>
                                                    </p>
                                                </div>
                                            </div>

                                            <div style="display:none" class="rating-awareness tooltip">
                                                <div class="arrow">�</div>
                                                <div class="inner-dialog">
                                                    <a href="javascript:void(0)" class="close-button" onclick="jQuery(this).parent().parent().hide()"><?php echo JText::_('LNG_CLOSE') ?></a>
                                                    <strong><?php echo JText::_('LNG_INFO') ?></strong>
                                                    <p>
                                                        <?php echo JText::_('LNG_YOU_HAVE_TO_BE_LOGGED_IN') ?>
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="rating">
                                                <p class="rating-average" title="<?php echo $company->averageRating?>" id="<?php echo $company->id?>" style="display: block;"></p>
                                            </div>

                                        </div>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="content-box">
                                        <?php if(!empty($company->website)){ ?>
                                            <div class="website-right">
                                                <i class="dir-icon-home"></i><a href="<?php echo $company->website ?>"><?php echo $company->website ?></a>
                                            </div>
                                        <?php } ?>
                                        <?php if(!empty($company->email)){ ?>
                                            <div class="company-right">
                                                <i class="dir-icon-envelope"></i><a href="mailto:<?php echo $company->email ?>"><?php echo $company->email ?></a>
                                            </div>
                                        <?php } ?>

                                        <?php if(!empty($company->categories)){?>

                                            <div class="categories-right">
                                                <?php
                                                $categories = explode('#|',$company->categories);

                                                foreach($categories as $i=>$category) {
                                                    $category = explode("|", $category);
                                                    ?>
                                                    <?php if(isset($category[3]) && !empty($category[3])){ ?>

                                                        <div class="aio-icon-component">
                                                            <a class="aio-icon-box-link" href="<?php echo JBusinessUtil::getCategoryLink($category[0], $category[2]) ?>">
                                                                <div class="aio-icon-box">
                                                                    <div class="aio-icon-top">
                                                                        <h4 style="color:<?php echo $category[4]; ?>;">
                                                                            <i class="dir-icon-<?php echo $category[3]; ?>"></i>
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="result-actions">
                    <ul>
                        <li> </li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <?php

        }
    }
    ?>
</div>