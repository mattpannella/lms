<?php // no direct access
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::_('script', 'components/com_jbusinessdirectory/assets/js/jquery-ui.js');
?>

<div id="categories-container" class="categories-accordion">
	<ul id="categories-accordion" class="categories-accordion1">
	<?php foreach($categories as $category){
		if(!is_array($category)){
			$category = array($category);
			$category["subCategories"] = array();
		}
		if(isset($category[0]->name)){		
	?>
		<li class="accordion-element">
			<div>
				<!-- div class="category-img-container">
					<img alt="" src="<?php echo JURI::root().PICTURES_PATH.$category[0]->imageLocation ?>">
				</div--> 
				<h3>
					<a href="<?php echo $category[0]->link ?>"><?php echo $category[0]->name ?>
						<?php if($appSettings->show_total_business_count) { ?>
							<span class="numberCircle"> <?php echo $category[0]->nr_listings ?></span>
						<?php } ?>
					</a>
				</h3>
			</div>
			<div>
				<ul class="category-list">
					<?php
						$i=1; 
						foreach($category["subCategories"] as $cat){ 
					?>
					<li>
						<a class="categoryLink" title="<?php echo $cat[0]->name?>" alt="<?php echo $cat[0]->name?>" 
							href="<?php echo $category[0]->link ?>"
						>
							<?php echo $cat[0]->name?>
						</a>
					</li> 
				<?php } ?>
				</ul>
			</div>
		</li>
	<?php 
		}
	} 
	?>
	</ul>
</div>
<div class="clear"></div>
<script>

	jQuery(document).ready(function(){
		jQuery(".categories-accordion1" ).each( function () {
			jQuery(this).accordion({
				heightStyle: "content",
				active: "false",
				event: "click hoverintent"
			});
		});
	});

</script>