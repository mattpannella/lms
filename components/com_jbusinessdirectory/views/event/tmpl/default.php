<?php // no direct access
/**
* @copyright	Copyright (C) 2008-2009 CMSJunkie. All rights reserved.
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
* See the GNU General Public License for more details.
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

defined('_JEXEC') or die('Restricted access');

$document = JFactory::getDocument();
$config = new JConfig();

$uri = JURI::getInstance();
$url = $uri->toString( array('scheme', 'host', 'port', 'path'));

$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();

require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';

$title = stripslashes($this->event->name)." | ".$config->sitename;
$description = !empty($this->event->short_description)?$this->event->short_description:$appSettings->meta_description;
$keywords = $appSettings->meta_keywords;

if(!empty($this->event->meta_title))
	$title = stripslashes($this->event->meta_title)." | ".$config->sitename;

if(!empty($this->event->meta_description))
	$description = $this->event->meta_description;

if(!empty($this->event->meta_keywords))
	$keywords = $this->event->meta_keywords;

$document->setTitle($title);
$document->setDescription($description);
$document->setMetaData('keywords', $keywords);

if(!empty($this->event->pictures)){
	$document->addCustomTag('<meta property="og:image" content="'.JURI::root().PICTURES_PATH.$this->event->pictures[0]->picture_path .'" /> ');
}
$document->addCustomTag('<meta property="og:type" content="website"/>');
$document->addCustomTag('<meta property="og:url" content="'.$url.'"/>');
$document->addCustomTag('<meta property="og:site_name" content="'.$config->sitename.'"/>');

$menuItemId="";
if(!empty($appSettings->menu_item_id)){
	$menuItemId = "&Itemid=".$appSettings->menu_item_id;
}
?>

<?php require_once JPATH_COMPONENT_SITE."/include/fixlinks.php"; ?>
<?php require_once JPATH_COMPONENT_SITE."/include/social_share.php"; ?>

<div> <a href="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=events'); ?>"><?php echo JText::_("BACK") ?></a></div>
<div id="event-container" class="event-container row-fluid" itemscope itemtype="http://schema.org/Event">
	<div class="row-fluid">
		<?php if(!empty($this->event->pictures)){?>
			<div id="event-image-container" class="event-image-container span6">
				<?php 
						$this->pictures = $this->event->pictures;
						require_once JPATH_COMPONENT_SITE.'/include/image_gallery.php'; 
						
					?>
			</div>
		<?php } ?>
		<div id="event-content" class="event-content span6">
			<h2 itemprop="name">
				<?php echo $this->event->name?>
			</h2>
				<div class="event-details">
						<div class="event-location" itemprop="location" itemscope itemtype="http://schema.org/Place">
							<i class="dir-icon-map-marker dir-icon-large"></i><span itemprop="name"><?php echo JBusinessUtil::getLocationText($this->event)?></span>
							<span style="display:none;" itemprop="address"><?php echo JBusinessUtil::getLocationText($this->event)?></span>
						</div>
						
						<?php if(!empty($this->event->price) && intval($this->event->price)!=0){?>
							<div class="event-price">
								<?php echo JText::_("LNG_PRICE")?>: <strong><?php echo JBusinessUtil::getPriceFormat($this->event->price, $this->event->currency_id) ?></strong>
							</div>
						<?php } ?>

						<div class="event-type">
							<?php echo JText::_("LNG_TYPE")?>: <strong><?php echo $this->event->eventType?></strong>
						</div>

						<?php if(!empty($this->event->categories)){?>
							<div class="event-categories">
								<div style="float:left"><?php echo JText::_('LNG_CATEGORIES')?>:&nbsp;</div>
								<ul class="event-categories">
									<?php
									$categories = explode('#',$this->event->categories);
									foreach($categories as $i=>$category){
										$category = explode("|", $category);
										?>
										<li> <a rel="nofollow" href="<?php echo JBusinessUtil::getEventCategoryLink($category[0], $category[2]) ?>"><?php echo $category[1]?></a><?php echo $i<(count($categories)-1)? ',&nbsp;':'' ?></li>
										<?php
									}
									?>
								</ul>
							</div>
						<?php } ?>

						<?php if(!empty($this->event->contact_phone) || !empty($this->event->contact_email)) { ?>
							<div class="event-contact">
								<strong><?php echo JText::_('LNG_CONTACT_DETAILS') ?></strong><br/>
								<?php if(!empty($this->event->contact_phone)) { ?><i class="dir-icon-phone"></i><a href="tel:<?php echo $this->event->contact_phone ?>"><?php echo $this->event->contact_phone ?></a><br/> <?php } ?>
								<?php if(!empty($this->event->contact_email)) { ?><i class="dir-icon-envelope"></i> <?php echo $this->event->contact_email ?><br/> <?php } ?>
							</div>
						<?php } ?>
				</div>

				<div class="event-dates-details">
					<table>
						<tr>
							<td>
								<div class="event-date">
									<strong><?php echo JText::_('LNG_EVENT_DATES') ?></strong><br/>
									<i class="dir-icon-calendar"></i> <?php $dates = JBusinessUtil::getDateGeneralFormat($this->event->start_date).(!empty($this->event->start_date) && $this->event->start_date!=$this->event->end_date && $this->event->show_end_date?" - ".JBusinessUtil::getDateGeneralFormat($this->event->end_date):""); echo $dates; ?>
									<?php echo (empty($dates) || ($this->event->show_start_time==0  && $this->event->show_end_time==0))?"":"," ?>
									<?php echo ($this->event->show_start_time?JBusinessUtil::convertTimeToFormat($this->event->start_time):"")." ".(!empty($this->event->end_time)&&$this->event->show_end_time?JText::_("LNG_UNTIL"):"")." ".($this->event->show_end_time?JBusinessUtil::convertTimeToFormat($this->event->end_time):""); ?>
								</div>
							</td>
						</tr>
						<?php if(!empty($this->event->doors_open_time) && $this->event->show_doors_open_time) { ?>
						<tr>
							<td>
								<strong><?php echo JText::_('LNG_EVENT_DOORS_OPEN') ?></strong><br/>
								<i class="dir-icon-clock-o"></i><span itemprop="doorTime"><?php echo JBusinessUtil::convertTimeToFormat($this->event->doors_open_time) ?></span>
							</td>
						</tr>
						<?php } ?>

						<?php if(!JBusinessUtil::emptyDate($this->event->booking_open_date) || !JBusinessUtil::emptyDate($this->event->booking_close_date)){?>
							<?php if(!(!JBusinessUtil::emptyDate($this->event->booking_close_date) xor $this->event->booking_open_date <= $this->event->booking_close_date)) {?>
								<tr>
									<td>
										<div class="event-date">
											<strong><?php echo JText::_('LNG_BOOKING_DATES') ?></strong><br/>
											<i class="dir-icon-calendar"></i>
											<?php echo $this->event->dates ?>
											<?php echo (empty($this->event->dates) || (empty($this->event->booking_open_time) && empty($this->event->booking_close_time)))?"":"," ?>
											<?php echo JBusinessUtil::convertTimeToFormat($this->event->booking_open_time)." ".(!empty($this->event->booking_close_time)?JText::_("LNG_UNTIL"):"")." ".(JBusinessUtil::convertTimeToFormat($this->event->booking_close_time)); ?>
										</div>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</table>
				</div><br/>

	          	<div class="company-details" itemprop="organizer" itemscope itemtype="http://schema.org/Organization">
					<table>
						<tr>
							<td><strong><?php echo JText::_('LNG_COMPANY_DETAILS') ?></strong></td>
						</tr>
						<tr>
							<td><a itemprop="url" href="<?php echo JBusinessUtil::getCompanyLink($this->event->company)?>"><span itemprop="name"><?php echo $this->event->company->name?></span></a></td>
						</tr>
						<tr>
							<td><i class="dir-icon-map-marker dir-icon-large"></i><span itemprop="address"><?php echo JBusinessUtil::getAddressText($this->event->company) ?></span></td>
						</tr>
						<?php if(!empty($this->event->company->phone)){?>
						<tr>
							<td><i class="dir-icon-phone dir-icon-large"></i> <a href="tel:<?php  echo $this->event->company->phone; ?>"><span itemprop="telephone"><?php  echo $this->event->company->phone; ?></span></a></td>
						</tr>
						<?php } ?>
						<?php if(!empty($this->event->company->website)){?>
							<tr>
								<td><a target="_blank" href="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=companies&task=companies.showCompanyWebsite&companyId='.$this->event->company->id) ?>"><i class="dir-icon-link "></i>  <?php echo JText::_('LNG_WEBSITE')?></a></td>
							</tr>
						<?php } ?>
					</table>
				</div>

			</div>
		</div>

	<div class="classification">
		<?php
			$renderedContent = AttributeService::renderAttributesFront($this->eventAttributes,false, array());
			echo $renderedContent;
		?>
	</div>
	<div class="event-description" itemprop="description">
		<?php echo $this->event->description?>
	</div>

	<span style="display:none;" itemprop="startDate"><?php echo JBusinessUtil::getDateGeneralFormat($this->event->start_date) ?></span>
	<span style="display:none;" itemprop="endDate"><?php echo JBusinessUtil::getDateGeneralFormat($this->event->end_date) ?></span>
	<span style="display:none;" itemprop="image" itemscope itemtype="http://schema.org/ImageObject">
		<?php if(isset($this->event->picture_path) && $this->event->picture_path!=''){?>
			<img  alt="<?php echo $this->event->name ?>" src="<?php echo JURI::root()."/".PICTURES_PATH.$this->event->picture_path?>" itemprop="contentUrl">
		<?php }else{?>
			<img title="<?php echo $this->event->name?>" alt="<?php echo $this->event->name?>" src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>" itemprop="contentUrl">
		<?php } ?>
	</span>
	<div class="clear"></div>
</div>

<script>


// starting the script on page load
jQuery(document).ready(function(){
	jQuery("img.image-prv").click(function(e){
		jQuery("#image-preview").attr('src', this.src);	
	});
});		

</script>