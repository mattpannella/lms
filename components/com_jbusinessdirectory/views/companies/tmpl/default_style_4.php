<?php /*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC' ) or die('Restricted access' );

require_once 'header.php';
require_once JPATH_COMPONENT_SITE.'/classes/attributes/attributeservice.php';

$menuItemId="";
if(!empty($this->appSettings->menu_item_id)){
	$menuItemId = "&Itemid=".$this->appSettings->menu_item_id;
}
?>

<?php require_once JPATH_COMPONENT_SITE."/include/social_share.php"?>
<?php require_once 'breadcrumbs.php';?>
<div class="clear"></div>

<div id="one-page-container" class="one-page-container style4" itemscope itemtype="http://schema.org/LocalBusiness">
	<div class="company-name">
		<h1 itemprop="name">
			<?php  echo !empty($this->company->name)?$this->company->name:"" ; ?>
		</h1>
	</div>
	<div class="row-fluid" style="margin-bottom: 0">
		<?php 
				if((isset($this->package->features) && in_array(IMAGE_UPLOAD,$this->package->features) || !$appSettings->enable_packages)
					&& !empty($this->pictures)){ 
		?>
			<?php $showImage = true;?>
			<div id="company-info" class="company-info span7">
				<?php require_once JPATH_COMPONENT_SITE.'/include/image_gallery.php'; ?>
			</div>
		<?php }?>
		
		<div class="contact-information <?php echo !empty($showImage)?"span5":"span12" ?>" >
		
			<div class="company-info-container">
			    <strong class="title"><?php echo JText::_("LNG_FIND_OUT_MORE")?></strong>
			   
			   <?php if($showData && (isset($this->package->features) && in_array(CONTACT_FORM, $this->package->features) || !$appSettings->enable_packages)) { ?>
				<?php if(!empty($this->companyContacts) && !empty($this->companyContacts[0]->contact_name)) { ?>
					   <strong><?php echo count($this->companyContacts)>1?JText::_('LNG_CONTACT_PERSONS'):JText::_('LNG_CONTACT_PERSON'); ?></strong>
					   <div style="line-height:18px;">
						   <?php require_once 'company_contact.php'; ?>
					   </div>
					<?php } ?>
			   <?php } ?>

				<strong><?php echo JText::_('LNG_ADDRESS') ?>:</strong>
				<span class="company-address" itemprop="address">
						<?php echo JBusinessUtil::getAddressText($this->company) ?>
				</span>
				
				<?php if($showData && (isset($this->package->features) && in_array(PHONE, $this->package->features) || !$appSettings->enable_packages)) { ?>
					<div class="comany-contact-details">
						<?php if(!empty($company->phone)) { ?>
							<span>
								<strong><?php echo JText::_('LNG_PHONE') ?>: </strong> <a href="tel:<?php  echo $company->phone; ?>"><span itemprop="telephone"><?php  echo $company->phone; ?></span></a>
							</span><br/>
						<?php } ?>
						<?php if(!empty($company->fax)) { ?>
							<span>
								<strong><?php echo JText::_('LNG_FAX') ?>: </strong><span itemprop="faxNumber"><?php  echo $company->fax; ?></span>
							</span><br/>
						<?php } ?>
						<?php if(!empty($company->mobile)) { ?>
							<span>
								<strong><?php echo JText::_('LNG_MOBILE') ?>: </strong><a href="tel:<?php  echo $company->mobile; ?>"><span itemprop="telephone"> <?php  echo $company->mobile; ?></span></a>
							</span><br/>
						<?php } ?>
					</div>
				<?php } ?>
				
				<?php require_once 'listing_social_networks.php'; ?>
				
				<div class="clear"></div>
				<div class="company-links">
					<ul class="features-links">
						<?php if($showData && (isset($this->package->features) && in_array(WEBSITE_ADDRESS,$this->package->features) || !$appSettings->enable_packages) && !empty($company->website)){ ?>
							<li>
								<a itemprop="url" class="website" title="<?php echo $this->company->name?> Website" target="_blank" onclick="increaseWebsiteClicks(<?php echo $company->id ?>)" href="<?php echo $company->website ?>"><?php echo JText::_('LNG_WEBSITE') ?></a>
							</li>
						<?php }?>
						<?php if((isset($this->package->features) && in_array(CONTACT_FORM,$this->package->features) || !$appSettings->enable_packages) && !empty($company->email)){ ?>
							<li>
								<a class="email" href="javascript:contactCompany(<?php echo $showData?"0":"1"?>)" ><?php echo JText::_('LNG_CONTACT_COMPANY'); ?></a>
							</li>
						<?php } ?>
							<li>
								<a href="javascript:showReportAbuse()" style="padding:0px;"><?php echo JText::_('LNG_REPORT_LISTING'); ?></a>
							</li>
					</ul>
				</div>
		
				<div class="clear"></div>
				
				<div class="rating-info">
					<div class="company-info-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" <?php echo !$appSettings->enable_ratings? 'style="display:none"':'' ?>>
					 <span style="display:none">
						 <span style="display:none" itemprop="itemReviewed" itemscope itemtype="http://schema.org/LocalBusiness"><span itemprop="name"><?php echo $this->company->name?></span></span>
						 <span itemprop="ratingValue"><?php echo $this->company->averageRating?></span>  <span itemprop="worstRating">0</span><span itemprop="bestRating">5</span>
					</span>
						<span  style="display:none" itemprop="ratingCount"><?php echo count($this->reviews)?></span>
			    
						<div class="company-info-average-rating">
							<div class="rating">
								<div id="rating-average" title="<?php echo $this->company->averageRating?>"></div>
							</div>
							<p class="rating-text">
							 <?php echo JText::_('LNG_AVG_OF') ?> <span id="average-rating-count"> <span id="rateNumber<?php echo $this->company->id?>"> <?php echo $this->ratingCount ?> </span> <?php echo JText::_('LNG_RATINGS') ?></span></p>
						</div>
						<div class="company-info-user-rating">
							<div class="rating">
								<div id="rating-user"></div>
							</div>
							<p class="rating-text">  <span id="user-rating-count" > <?php echo JText::_('LNG_YOUR_RATING') ?></span></p>
						</div>
					</div>
					
					<div class="company-info-review" <?php echo !$appSettings->enable_reviews? 'style="display:none"':'' ?>>
						<div class="review-count">
							<?php if(count($this->reviews)){ ?> 
		   					    <a href="#reviews"><?php echo count($this->reviews)?> <?php echo JText::_('LNG_REVIEWS') ?></a>
								&nbsp;|&nbsp;
								<a href="javascript:void(0)" onclick="addNewReview(<?php echo ($appSettings->enable_reviews_users && $user->id ==0) ?"1":"0"?>)"> <?php echo JText::_('LNG_WRITE_REVIEW') ?></a>
							<?php } else{ ?>
								<a href="javascript:void(0)" onclick="addNewReview(<?php echo ($appSettings->enable_reviews_users && $user->id ==0) ?"1":"0"?>)"><?php echo JText::_('LNG_BE_THE_FIRST_TO_REVIEW') ?></a>
							<?php }?>
							
							<?php if($this->appSettings->enable_bookmarks) { ?>
								<?php if(!empty($company->bookmark)){?>
									<a href="javascript:showUpdateBookmarkDialog(<?php echo $user->id==0?"1":"0"?>)"  title="<?php echo JText::_("LNG_UPDATE_BOOKMARK")?>" class="bookmark "><i class="dir-icon-heart"></i></a>
								<?php }else{?>
									<a href="javascript:addBookmark(<?php echo $user->id==0?"1":"0"?>)" title="<?php echo JText::_("LNG_ADD_BOOKMARK")?>" class="bookmark "><i class="dir-icon-heart-o"></i></a>
								<?php } ?>
								
							<?php } ?>
						</div>
					</div>
				</div>
				
				<?php if($showData){?>
					<div class="company-map">
						<a href="javascript:showMap()" title="Show Map">
							<?php 
								$key="";
								if(!empty($appSettings->google_map_key))
									$key="&key=".$appSettings->google_map_key;
							?>
							<?php 
								if((isset($this->package->features) && in_array(GOOGLE_MAP,$this->package->features) || !$appSettings->enable_packages ) 
												&& !empty($this->company->latitude) && !empty($this->company->longitude)){ 
									echo '<img src="http://maps.googleapis.com/maps/api/staticmap?center='.$this->company->latitude.','.$this->company->longitude.'&zoom=13&size=300x106&markers=color:blue|'.$this->company->latitude.','.$this->company->longitude.$key.'&sensor=false">';
								}
							?>
						</a> 	
						
						<div class="clear"></div>
						<?php if((!isset($this->company->userId) || $this->company->userId == 0) && $appSettings->claim_business){ ?>
						<div class="claim-container" id="claim-container">
							
							<a href="javascript:void(0)" onclick="claimCompany(<?php echo $user->id==0?"1":"0"?>)">
								<div class="claim-btn">
									<?php echo JText::_('LNG_CLAIM_COMPANY')?>
								</div>
							</a>
						</div>
						<?php  } ?>
					</div>
				<?php } ?>
			</div>
			<div class="clear"></div>
			
		</div>
	</div>
	
	
	<div class="clear"></div>

	<div id="company-map-holder" style="display:none" class="company-cell">
		<div class="search-toggles">
			<span class="button-toggle">
				<a title="" class="" href="javascript:hideMap()"><?php echo JText::_("LNG_CLOSE_MAP")?></a>
			</span>
			<div class="clear"></div>
		</div>
		<h2><?php echo JText::_("LNG_BUSINESS_MAP_LOCATION")?></h2>
		
		<?php 
			if(isset($this->company->latitude) && isset($this->company->longitude) && $this->company->latitude!='' && $this->company->longitude!='')
				require_once 'map.php';
		?>
	</div>
	
	<div class="company-menu">
		<nav>
			<a id="business-link" href="javascript:showDetails('company-business');" class="active"><?php echo JText::_("LNG_BUSINESS_DETAILS")?></a>
		
			<?php 
				if((isset($this->package->features) && in_array(VIDEOS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->videos) && count($this->videos)>0){	
			?>
					<a id="videos-link" href="javascript:showDetails('company-videos');" class=""><?php echo JText::_("LNG_VIDEOS")?></a>
			<?php } ?>
			
			<?php 
				if((isset($this->package->features) && in_array(COMPANY_OFFERS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->offers) && count($this->offers) && $appSettings->enable_offers){
			?>
					<a id="offers-link" href="javascript:showDetails('company-offers');" class=""><?php echo JText::_("LNG_OFFERS")?></a>
			<?php } ?>
			
			<?php 
				if((isset($this->package->features) && in_array(COMPANY_EVENTS,$this->package->features) || !$appSettings->enable_packages)
					&& isset($this->events) && count($this->events) && $appSettings->enable_events){
			?>
					<a id="events-link" href="javascript:showDetails('company-events');" class=""><?php echo JText::_("LNG_EVENTS")?></a>
			<?php } ?>
			
			<?php if($appSettings->enable_reviews){ ?>
				<a id="reviews-link" href="javascript:showDetails('company-reviews');" class=""><?php echo JText::_("LNG_REVIEWS")?></a>
			<?php }?>
			
		</nav>
	</div>

	<div id="company-details" class="company-cell">
		<?php if(isset($this->company->slogan) && strlen($this->company->slogan)>2){?>
			<p class="business-slogan"><?php echo  $this->company->slogan ?> </p>
		<?php }?>
			
		<dl>
			<?php if(!empty($this->company->typeName)){?>
				<dt><?php echo JText::_('LNG_TYPE')?>:</dt>
				<dd><?php echo $this->company->typeName?></dd>
			<?php } ?>
			
			<?php if(!empty($this->company->categories)){?>
				<dt><?php echo JText::_('LNG_CATEGORIES')?>:</dt>
				<dd>
					<ul>
						<?php 
							$categories = explode('#',$this->company->categories);
							foreach($categories as $i=>$category){
								$category = explode("|", $category);
								?>
									<li> <a rel="nofollow" href="<?php echo JBusinessUtil::getCategoryLink($category[0], $category[2]) ?>"><?php echo $category[1]?></a><?php echo $i<(count($categories)-1)? ',&nbsp;':'' ?></li>
								<?php 
							}
						?>
					</ul>
				</dd>
			<?php } ?>

			<?php if(!empty($this->company->keywords)){?>
				<dt><?php echo JText::_('LNG_KEYWORDS')?>:</dt>
				<dd>
					<ul>
						<?php 
						$keywords =  explode(',', $this->company->keywords);
						for($i=0; $i<count($keywords); $i++) { ?>
							<li>
								<a  href="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=search&searchkeyword='.$keywords[$i]) ?>"><?php echo $keywords[$i]?><?php echo $i<(count($keywords)-1)? ',&nbsp;':'' ?></a>
							</li>
						<?php 
						} ?>
					</ul>
				</dd>
			<?php } ?>
			
			<?php if(!empty($this->company->description)){?>
				<dt><?php echo JText::_("LNG_GENERAL_INFO")?></dt>
				<dd><div id="dir-listing-description" class="dir-listing-description"><?php echo JHTML::_("content.prepare", $this->company->description);?></div></dd>
			<?php }?>
			
			<?php if((isset($this->package->features) && in_array(CUSTOM_TAB,$this->package->features) || !$appSettings->enable_packages)
					   && !empty($this->company->custom_tab_name)){ ?>
				<dt><?php echo $this->company->custom_tab_name ?></dt>
				<dd><?php echo  $this->company->custom_tab_content;	?>&nbsp;</dd>
			<?php } ?>
			
			<?php if(!empty($this->company->locations)){ ?>
				<dt><?php echo JText::_("LNG_COMPANY_LOCATIONS")?></dt>
				<dd><?php require_once 'locations.php';?></dd>
			<?php } ?>
			
			<?php if((isset($this->package->features) && in_array(OPENING_HOURS,$this->package->features) || !$appSettings->enable_packages)
				&& !empty($this->company->business_hours)){ ?>
				<dt><?php echo JText::_("LNG_OPENING_HOURS")?></dt>
				<dd><?php require_once 'business_hours.php'; ?>	</dd>
			<?php } ?>
			
			<?php if($showData && $appSettings->enable_attachments && (isset($this->package->features) && in_array(ATTACHMENTS, $this->package->features) || !$appSettings->enable_packages)) { ?>
				<?php if(!empty($this->company->attachments)) { ?>
					<dt><?php echo JText::_("LNG_ATTACHMENTS")?></dt>
					<dd>
						<div class="attachments">
							<ul>
								<?php foreach($this->company->attachments as $attachment) { ?>	
									<li><a target="_blank" href="<?php echo JURI::root()."/".ATTACHMENT_PATH.$attachment->path?>"><?php echo !empty($attachment->name)?$attachment->name:basename($attachment->path)?></a> </li>
								<?php } ?>
							</ul>
						</div>
					</dd>
				<?php } ?>
			<?php } ?>
		</dl>
			
		<div class="classification">
			<?php 
				$renderedContent = AttributeService::renderAttributesFront($this->companyAttributes, $appSettings->enable_packages, $this->package->features);
				echo $renderedContent;
			?>
		</div>
			
	</div>
	<div class="clear"></div>
					
	
	<?php 
		if((isset($this->package->features) && in_array(VIDEOS,$this->package->features) || !$appSettings->enable_packages)
							&& isset($this->videos) && count($this->videos)>0){
	?>			
		<div id="company-videos" class="company-cell">
			<h2><?php echo JText::_("LNG_VIDEOS")?></h2>
			<?php  require_once 'companyvideos.php';?>
		</div>
	<?php }	?>
	
	<?php 
	if((isset($this->package->features) && in_array(COMPANY_OFFERS,$this->package->features) || !$appSettings->enable_packages)
			&& isset($this->offers) && count($this->offers) && $appSettings->enable_offers){
	?>
		<div id="company-offers" class="company-cell" itemprop="hasOfferCatalog" itemscope itemtype="http://schema.org/OfferCatalog">
			<h2><?php echo JText::_("LNG_COMPANY_OFFERS")?></h2>
			<?php require_once 'companyoffers.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>
	
	<?php 
	if((isset($this->package->features) && in_array(COMPANY_EVENTS,$this->package->features) || !$appSettings->enable_packages)
			&& isset($this->events) && count($this->events) && $appSettings->enable_events){
	?>
		<div id="company-events" class="company-cell">
			<h2><?php echo JText::_("LNG_COMPANY_EVENTS")?></h2>
			<?php require_once 'events.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>
	
	<?php 
	if($appSettings->enable_reviews){
	?>
		<div id="company-reviews" class="company-cell">
		<h2><?php echo JText::_("LNG_BUSINESS_REVIEWS")?></h2>
			<?php require_once 'reviews.php';?>
		</div>
		<div class="clear"></div>
	<?php } ?>
	
	<form name="tabsForm" action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory'.$menuItemId) ?>" id="tabsForm" method="post">
	 	 <input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
		 <input type="hidden" name="task" value="companies.displayCompany" /> 
		 <input type="hidden" name="tabId" id="tabId" value="<?php echo $this->tabId?>" /> 
		 <input type="hidden" name="view" value="companies" /> 
		 <input type="hidden" name="layout2" id="layout2" value="" /> 
		 <input type="hidden" name="companyId" value="<?php echo $this->company->id?>" />
		 <input type="hidden" name="controller"	value="<?php echo JRequest::getCmd('controller', 'J-BusinessDirectory')?>" />
	</form>
</div>
<script>

function showMap(){
	jQuery("#company-map-holder").show();
	<?php if((isset($this->package->features) && in_array(GOOGLE_MAP,$this->package->features) || !$appSettings->enable_packages ) 
			&& !empty($this->company->latitude) && !empty($this->company->longitude)){ ?>
		loadScript();
	<?php }	?>
}

function hideMap(){
	jQuery("#company-map-holder").hide();
}

function readMore(){
	jQuery("#general-info").removeClass("collapsed");
	jQuery(".read-more").hide();
}

function showDetails(identifier){
	var ids = ["company-details","company-videos" ,"company-offers","company-events","company-reviews"];
	var pos = ids.indexOf(identifier); 
	
	jQuery(".company-menu a").each(function(){
		jQuery(this).removeClass("active");
	});

	jQuery("#"+identifier+"-link").addClass("active");
	
	for(var i=0;i<pos;i++){
		jQuery("#"+ids[i]).slideUp();
	}

	for(var i=pos;i<ids.length;i++){
		jQuery("#"+ids[i]).slideDown();
	}
}
</script>
<?php require_once 'company_util.php'; ?>