<?php if(!empty($this->companyContacts)) { ?>
	<?php foreach($this->companyContacts as $contact) { ?>
		<?php if(!empty($contact->contact_name)){?>
			<div id="contact-person-details<?php echo $contact->id?>" class="contact-person-details">
	    		<div class="contact-person-name" onclick="jQuery('#contact-person-details<?php echo $contact->id ?>').toggleClass('open')"><?php echo $contact->contact_name?> (+)</div>
	    		<ul>
				    <?php if(!empty($contact->contact_email)) { ?>
				        <i class="dir-icon-envelope"></i> <?php echo $contact->contact_email; ?>
				        <br/>
				    <?php }?>
		   			 <?php if(!empty($contact->contact_phone)) { ?>
					     <i class="dir-icon-mobile-phone"></i> <a href="tel:<?php  echo $contact->contact_phone; ?>"><?php  echo $contact->contact_phone; ?></a>
					     <br/>
			    	<?php } ?>
				    <?php if(!empty($contact->contact_fax)) {?>
				        <i class="dir-icon-fax"></i> <?php echo $contact->contact_fax?>
				        <br/>
				    <?php }?>
		        </ul>
	     	</div>
	     <?php } ?>
	<?php } ?>
<?php } ?>