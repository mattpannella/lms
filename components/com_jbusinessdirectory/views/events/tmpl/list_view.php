<div id="events-list-view"> 
	<ul class="event-list" itemscope itemtype="http://schema.org/ItemList">
	<?php 
		if(isset($this->events) && count($this->events)>0){
			foreach ($this->events as $event){ ?>
				<li itemscope itemprop="itemListElement" itemtype="http://schema.org/Event">
					<div class="event-box row-fluid <?php echo !empty($event->featured)?"featured":"" ?>">
						<div class="event-img-container span3">
							<a class="event-image" itemprop="image" itemscope itemtype="http://schema.org/ImageObject"
								href="<?php echo JBusinessUtil::getEventLink($event->id, $event->alias) ?>">
								
								<?php if(isset($event->picture_path) && $event->picture_path!=''){?>
									<img  alt="<?php echo $event->name ?>" src="<?php echo JURI::root()."/".PICTURES_PATH.$event->picture_path?>" itemprop="contentUrl">
								<?php }else{?>
									<img title="<?php echo $event->name?>" alt="<?php echo $event->name?>" src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>" itemprop="contentUrl">
								<?php } ?>
							</a>
						</div>
						<div class="event-content span8">
							<div class="event-subject" itemprop="url">
								<a title="<?php echo $event->name?>"
									href="<?php echo JBusinessUtil::getEventLink($event->id, $event->alias) ?>"><span itemprop="name"><?php echo $event->name?></span>
								</a>
							</div>
							
							<?php if(JBusinessUtil::getLocationText($event)!=""){?>
								<div class="event-location" itemprop="location" itemscope itemtype="http://schema.org/Place">
									<i class="dir-icon-map-marker dir-icon-large"></i><span itemprop="address"><span itemprop="name"><?php echo JBusinessUtil::getLocationText($event)?></span></span>
								</div>
							<?php } ?>

							<div class="event-date">
								<i class="dir-icon-calendar"></i>
								<?php $dates = JBusinessUtil::getDateGeneralFormat($event->start_date).(!empty($event->start_date) && $event->start_date!=$event->end_date && $event->show_end_date?" - ".JBusinessUtil::getDateGeneralFormat($event->end_date):""); echo $dates; ?>
								<?php echo (empty($dates) || ($event->show_start_time==0  && $event->show_end_time==0))?"":"," ?>
								<?php echo ($event->show_start_time?JBusinessUtil::convertTimeToFormat($event->start_time):"")." ".(!empty($event->end_time)&&$event->show_end_time?JText::_("LNG_UNTIL"):"")." ".($event->show_end_time?JBusinessUtil::convertTimeToFormat($event->end_time):""); ?>
							</div>

							<?php if(!empty($event->eventType)){?>
								<div class="event-type">
									<?php echo JText::_("LNG_TYPE")?>: <strong><?php echo $event->eventType?></strong>
								</div>
							<?php } ?>

							<?php if(!empty($event->categories)){?>
								<div class="event-categories">
									<div style="float:left"><?php echo JText::_('LNG_CATEGORIES')?>:&nbsp;</div>
									<ul class="event-categories">
										<?php
										$categories = explode('#',$event->categories);
										foreach($categories as $i=>$category){
											$category = explode("|", $category);
											?>
											<li> <a rel="nofollow" href="<?php echo JBusinessUtil::getEventCategoryLink($category[0], $category[2]) ?>"><?php echo $category[1]?></a><?php echo $i<(count($categories)-1)? ',&nbsp;':'' ?></li>
											<?php
										}
										?>
									</ul>
								</div>
							<?php } ?>

							<div class="event-desciption" itemprop="description">
								<?php echo $event->short_description ?>
							</div>
						</div>
						<?php if(isset($event->featured) && $event->featured==1){ ?>
							<div class="featured-text">
								<?php echo JText::_("LNG_FEATURED")?>
							</div>
						<?php } ?>
					</div>
					<span style="display:none;" itemprop="startDate"><?php echo JBusinessUtil::getDateGeneralFormat($event->start_date) ?></span>
					<span style="display:none;" itemprop="endDate"><?php echo JBusinessUtil::getDateGeneralFormat($event->end_date) ?></span>
					<span style="display:none;" itemprop="organizer" itemscope itemtype="http://schema.org/Organization"><span itemprop="name"><?php echo $event->companyName ?></span></span>
					<div class="clear"></div>
				</li>
			<?php }
		} ?>
	</ul>
	<div class="clear"></div>
</div>