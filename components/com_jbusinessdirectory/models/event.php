<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.modelitem');
JTable::addIncludePath(DS.'components'.DS.JRequest::getVar('option').DS.'tables');
require_once( JPATH_COMPONENT_ADMINISTRATOR.'/library/category_lib.php');

class JBusinessDirectoryModelEvent extends JModelItem
{ 
	
	function __construct()
	{
		parent::__construct();
		$this->appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
		
		$this->eventId = JRequest::getVar('eventId');
	}

	/**
	 * Returns a Table object, always creating it
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 */
	public function getTable($type = 'Event', $prefix = 'JTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	function getEvent(){
		$eventsTable = JTable::getInstance("Event", "JTable");
		$event =  $eventsTable->getActiveEvent($this->eventId);
		
		if(empty($event))
			return $event;
		
		$event->pictures = $eventsTable->getEventPictures($this->eventId);
		$eventsTable->increaseViewCount($this->eventId);
		
		$companiesTable = JTable::getInstance("Company", "JTable");
		$company = $companiesTable->getCompany($event->company_id);
		$event->company=$company;
 
		if($this->appSettings->enable_multilingual){
			JBusinessDirectoryTranslations::updateEntityTranslation($event, EVENT_DESCRIPTION_TRANSLATION);
		}

		$dates = '';
		if(!JBusinessUtil::emptyDate($event->booking_open_date) && !JBusinessUtil::emptyDate($event->booking_close_date) && $event->booking_open_date!=$event->booking_close_date){
			$dates = JBusinessUtil::getDateGeneralFormat($event->booking_open_date).' - '.JBusinessUtil::getDateGeneralFormat($event->booking_close_date);
		}
		else if($event->booking_open_date==$event->booking_close_date){
			$dates = JBusinessUtil::getDateGeneralFormat($event->booking_open_date);
		}
		else if(!JBusinessUtil::emptyDate($event->booking_open_date)){
			$dates = JText::_('LNG_STARTING_FROM').' '.JBusinessUtil::getDateGeneralFormat($event->booking_open_date);
		}
		else if(!JBusinessUtil::emptyDate($event->booking_close_date)){
			$dates = JText::_('LNG_UNTIL').' '.JBusinessUtil::getDateGeneralFormat($event->booking_close_date);
		}

		$event->dates = $dates;
		
		return $event;
	}

	function getEventAttributes(){
		$attributesTable = $this->getTable('EventAttributes');
		return  $attributesTable->getEventAttributes($this->eventId);
	}

	function getEventTickets(){
		$ticketsTable = $this->getTable("EventTickets");
		$eventTickets = $ticketsTable->getTicketsByEvent($this->eventId);

		$ticketsBooked = EventBookingService::getNuberOfAvailableTickests($this->eventId);
		$result = array();
		foreach($eventTickets as &$eventTicket){
			if(empty($eventTicket->max_booking) || ($eventTicket->max_booking > $eventTicket->quantity)){
				$eventTicket->max_booking = $eventTicket->quantity;
			}
			
			if(isset($ticketsBooked[$eventTicket->id]) && $eventTicket->max_booking > ($eventTicket->quantity - $ticketsBooked[$eventTicket->id])){
				$eventTicket->max_booking = $eventTicket->quantity - $ticketsBooked[$eventTicket->id];
			}
			
			if($eventTicket->max_booking < 0){
				$eventTicket->max_booking = 0;
			}
			
			if($eventTicket->max_booking > 0){
				$result[] = $eventTicket;
			}
		}
		
		if($this->appSettings->enable_multilingual){
			JBusinessDirectoryTranslations::updateEventTicketsTranslation($result);
		}

		return $result;
	}

	
	function reserveTickets(){
		EventUserDataService::initializeUserData();
		
		$eventsTable = JTable::getInstance("Event", "JTable");
		$event =  $eventsTable->getEvent($this->eventId);
		
		$ticket_quantity = JFactory::getApplication()->input->get('ticket_quantity');
		$ticket_id = JRequest::getVar('ticket_id');
		$tickets = array();
		$totalTickets = 0;
		foreach ($ticket_quantity as $key => $val) {
			if($val>0){
				$ticket = new stdClass();
				$ticket->id = $ticket_id[$key];
				$ticket->quantity = $val;
				$tickets[] = $ticket;
				$totalTickets+=$val;
			}
		}
		
		$ticketsBooked = EventBookingService::getNuberOfAvailableTickests($this->eventId);
		
		$totalTicketsBooked = 0;
		foreach($ticketsBooked as $t){
			$totalTicketsBooked+=$t;
		}
		
		if($totalTickets>($event->total_tickets-$totalTicketsBooked)){
			JError::raiseWarning(500, JText::_('LNG_TICKET_MAX_NUMBER_EXCEED'));
			return false;
		}
		
		if($totalTickets>($event->total_tickets-$totalTicketsBooked)){
			JError::raiseWarning(500, JText::_('LNG_TICKET_MAX_NUMBER_EXCEED'));
			return false;
		}
		
		if(empty($tickets)){
			JError::raiseWarning(500, JText::_('LNG_NO_TICKET_SELECTED'));
			return false;
		}
		
		$result = EventUserDataService::reserveTickests($this->eventId, $tickets);
		
		return $result;
	}

	/**
	 * Get the events that are about to expire and send an email to the event owners
	 */
	function checkEventsAboutToExpire(){
		$eventTable = $this->getTable("Event");
		$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
		$nrDays = $appSettings->expiration_day_notice;
		$events = $eventTable->getEventsAboutToExpire($nrDays);
		foreach($events as $event){
			echo "sending expiration e-mail to: ".$event->name;
			$result = EmailService::sendEventExpirationEmail($event, $nrDays);
			if($result)
				$eventTable->updateExpirationEmailDate($event->id);
		}
		exit;
	}
}
?>

