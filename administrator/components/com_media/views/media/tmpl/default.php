<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_media
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$user  = JFactory::getUser();
$input = JFactory::getApplication()->input;
$lang  = JFactory::getLanguage();
$style = JFactory::getApplication()->getUserStateFromRequest('media.list.layout', 'layout', 'thumbs', 'word');

if (DIRECTORY_SEPARATOR == '\\')
{
	$base = str_replace(DIRECTORY_SEPARATOR, "\\\\", COM_MEDIA_BASE);
}
else
{
	$base = COM_MEDIA_BASE;
}

JFactory::getDocument()->addScriptDeclaration(
	"
		var basepath = '" . $base . "';
		var viewstyle = '" . $style . "';
	"
);

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.framework');
JHtml::_('script', 'media/mediamanager.min.js', array('version' => 'auto', 'relative' => true));
JHtml::_('script', 'media/mediaelement-and-player.js', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'media/mediaelementplayer.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'system/mootree.css', array('version' => 'auto', 'relative' => true));

if ($lang->isRtl())
{
	JHtml::_('stylesheet', 'system/mootree_rtl.css', array('version' => 'auto', 'relative' => true));
}

$uploadFormActionValue = 
	JUri::base()
	. "index.php?option=com_media&amp;task=file.upload&amp;tmpl=component&amp;"
	. $this->session->getName() 
	. '=' 
	. $this->session->getId()
	. '&amp;'
	. JSession::getFormToken()
	. '=1&amp;format=html';
?>
<div class="row">

	<div id="j-main-container" class="ms-3 col-sm-12">
		<?php echo $this->loadTemplate('navigation'); ?>
		<?php if (($user->authorise('core.create', 'com_media')) and $this->require_ftp) : ?>
			<form 
				id="ftpForm"
				name="ftpForm"
				action="index.php?option=com_media&amp;task=ftpValidate" 
				method="post"
			>
				<fieldset title="<?php echo JText::_('COM_MEDIA_DESCFTPTITLE'); ?>">
					<legend><?php echo JText::_('COM_MEDIA_DESCFTPTITLE'); ?></legend>
					<?php echo JText::_('COM_MEDIA_DESCFTP'); ?>
					<label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label>
					<input type="text" id="username" name="username" size="70" value="" />

					<label for="password"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
					<input type="password" id="password" name="password" size="70" value="" />
				</fieldset>
			</form>
		<?php endif; ?>

		<form 
			id="mediamanager-form"
			name="adminForm"
			action="index.php?option=com_media" 
			method="post" 
			enctype="multipart/form-data"
		>
			<input
				name="task"
				value=""
				type="hidden"
			/>
			<input
				id="cb1"
				name="cb1"
				value="0" 
				type="hidden"
			/>
			<input
				id="folder"
				name="folder"
				value="<?php echo $this->state->folder; ?>" 
				class="update-folder"
				type="hidden"
			/>
		</form>

		<?php if ($user->authorise('core.create', 'com_media')) : ?>
			<!-- File Upload Form -->
			<div id="collapseUpload" class="collapse">
				<form 
					id="uploadForm"
					action="<?php echo $uploadFormActionValue ?>" 
					name="uploadForm" 
					method="post" 
					enctype="multipart/form-data"
				>
					<div id="uploadform" class="uploadform p-2">
						<fieldset 
							id="upload-noflash"
							class="actions"
						>
								<label
									for="upload-file"
								>
									<?php echo JText::_('COM_MEDIA_UPLOAD_FILE'); ?>
								</label>
								<input
									id="upload-file"
									name="Filedata[]"
									type="file"
									multiple
									required
								/> 
								<button 
									id="upload-submit" 
									class="btn btn-primary"
								>
									<span class="icon-upload icon-white"></span> <?php echo JText::_('COM_MEDIA_START_UPLOAD'); ?>
								</button>
								<p class="help-block">
									<?php $cMax    = (int) $this->config->get('upload_maxsize'); ?>
									<?php $maxSize = JUtility::getMaxUploadSize($cMax . 'MB'); ?>
									<?php echo JText::sprintf('JGLOBAL_MAXIMUM_UPLOAD_SIZE_LIMIT', JHtml::_('number.bytes', $maxSize)); ?>
								</p>
						</fieldset>
						<input
							id="folder"
							name="folder"
							value="<?php echo $this->state->folder; ?>"
							class="update-folder"
							type="hidden"
						/>
						<?php JFactory::getSession()->set('com_media.return_url', 'index.php?option=com_media'); ?>
					</div>
				</form>
			</div>
			<div id="collapseFolder" class="collapse">
				<form
					id="folderForm"
					class="form-inline"
					name="folderForm"
					action="index.php?option=com_media&amp;task=folder.create&amp;tmpl=<?php echo $input->getCmd('tmpl', 'index'); ?>"
					method="post"
				>
						<div class="path">
							<input 
								id="folderpath"
								class="update-folder"
								type="text"
								readonly 
							/>
							<input 
								id="foldername"
								name="foldername"
								type="text"
								required 
							/>
							<input 
								id="folderbase"
								name="folderbase" 
								value="<?php echo $this->state->folder; ?>"
								class="update-folder"
								type="hidden"  
							/>
							<button class="btn btn-primary" type="submit">
								<span class="icon-folder-open"></span> <?php echo JText::_('COM_MEDIA_CREATE_FOLDER'); ?>
							</button>
						</div>
						<?php echo JHtml::_('form.token'); ?>
				</form>
			</div>
		<?php endif; ?>

		<form 
			id="folderForm"
			name="folderForm"
			method="post"
			action="index.php?option=com_media&amp;task=folder.create&amp;tmpl=<?php echo $input->getCmd('tmpl', 'index'); ?>"   
		>
			<div id="folderview">
				<div class="view">
					<iframe
						id="folderframe"
						name="folderframe"
						class="thumbnail"
						src="index.php?option=com_media&amp;view=mediaList&amp;tmpl=component&amp;folder=<?php echo $this->state->folder; ?>"
						style="width:100%; height:60vh"
						scrolling="auto"
					></iframe>
				</div>
				<?php echo JHtml::_('form.token'); ?>
			</div>
		</form>
	</div>
</div>

<?php

	// Shared modal markup
	$modalTitle = JText::_('COM_MEDIA_PREVIEW');
	$closeBtnText = JText::_('JLIB_HTML_BEHAVIOR_CLOSE');
	$modalFooter = "<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>$closeBtnText</button>";

	// Image Preview Modal
	$imagePreviewModalBody = "
		<div 
			id='image' 
			style='text-align:center;'>
			<img 
				id='imagePreviewSrc'
				src='../media/jui/img/alpha.png' 
				alt='preview' 
			/>
		</div>
	";
	echo JHtml::_(
		'bootstrap.renderModal',
		'imagePreview',
		array(
			'title' => $modalTitle,
			'footer' => $modalFooter,
			'size' => 'xl'
		),
		$imagePreviewModalBody
	);

	// Video Preview Modal
	$videoPreviewModalBody = "
		<div id='videoPlayer' style='z-index: -100;'><video id='mejsPlayer' style='max-width: 100%;'/></div>
	";
	echo JHtml::_(
		'bootstrap.renderModal',
		'videoPreview',
		array(
			'title' => $modalTitle,
			'footer' => $modalFooter,
			'size' => 'xl'
		),
		$videoPreviewModalBody
	);

?>
