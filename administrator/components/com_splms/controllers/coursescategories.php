<?php

defined('_JEXEC') or die();

class SplmsControllerCoursescategories extends FOFController {
    
    public function remove() {
        $cid = $this->input->get('cid');
        if (!$cid) {
            return;
        }
        $selectionCount = count($cid);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('COUNT(*) as count');
        $query->from('#__splms_coursescategories');
        $db->setQuery($query);
        $result = $db->loadobject();
        if($result->count <= 1 || $selectionCount == $result->count) {
            $url = 'index.php?option=com_splms&view=coursescategories';
            $app = JFactory::getApplication();
            $msg = 'You must have at least one category';
            $app->redirect($url, $msg, $type = 'message');
        } else {
            $query = $db->getQuery(true);
            $catIds = implode(',', $cid);
            $query = "DELETE FROM joom_splms_coursescategories WHERE (FIND_IN_SET(splms_coursescategory_id, '$catIds') > 0)";
            $db->setQuery($query);
            $db->execute();
            $url = 'index.php?option=com_splms&view=coursescategories';
            $app = JFactory::getApplication();
            $msg = 'The selected categories were deleted';
            $app->redirect($url, $msg, $type = 'message');
        }
    } 
}