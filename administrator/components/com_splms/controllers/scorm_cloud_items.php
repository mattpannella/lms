<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

defined('_JEXEC') or die();

class SplmsControllerScorm_cloud_items extends FOFController {

    public function remove() {
        $db = JFactory::getDbo();
        $model = $this->getThisModel();
        if (!$model->getId()) {
            $model->setIDsFromRequest();
        }
        $ids = $model->getIds();
        $scormLMS = new AxsScormLMS();
        $courseService = $scormLMS->ScormService->getCourseService();       
        foreach($ids as $id) {
            $cid = AxsScormLMS::getCourse($id)->scorm_course_id;$courseService->DeleteCourse($cid);
            $db->setQuery("DELETE FROM joom_splms_scorm_library WHERE id = $id;");
            $db->execute();
        }
        //now redirect them back to list view
        $url = JRoute::_('index.php?option=com_splms&view=scormitems', false);
        JFactory::getApplication()->redirect($url);
    }
}