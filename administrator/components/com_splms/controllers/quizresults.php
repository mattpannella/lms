<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/3/16
 * Time: 12:06 PM
 */

defined('_JEXEC') or die();

class SplmsControllerQuizresults extends FOFController {

	/**
	 * Responds with quiz questions, quiz results, and lesson data 
	 * for the given course purchase row id
	 * 
	 * @return void
	*/
	function courseProgress() {
		$course_purchase_row_id = $this->input->get('course_purchase_row_id', 0, 'INT');

		$db  = JFactory::getDbo();
		$db->setQuery("SELECT b.*, c.title as lesson_title, d.title as quiz_title
			FROM axs_course_purchases a 
			JOIN joom_splms_quizresults b ON a.course_id=b.splms_course_id 
			JOIN joom_splms_lessons c ON b.splms_lesson_id=c.splms_lesson_id AND a.user_id=b.user_id
			JOIN joom_splms_quizquestions d ON b.splms_quizquestion_id=d.splms_quizquestion_id
			WHERE a.id = $course_purchase_row_id AND b.archive_date IS NULL;");
		$results = $db->loadObjectList();

		$res = new stdClass();
		$res->quiz_results = $results;
		echo json_encode($res);
	}

}