<?php

defined('_JEXEC') or die();

class SplmsControllerLessons extends FOFController {

	 function getLessons() {
        $input = JFactory::getApplication()->input;
	 	$parent = $input->get('parent','','STRING');
        $table  = $input->get('table','','STRING');
        $column = $input->get('column','','STRING');
	 	$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
            ->select('*')
            ->from($db->quoteName($table))
            ->where($column.'='.$parent)
            ->order('ordering ASC');

        $db->setQuery($query);
        $result = $db->loadObjectList();

	 	$html = '<option value="">--Select--</option>';
        foreach($result as $lesson) {
            $html .= '<option value="'.$lesson->splms_lesson_id.'" selected>'.$lesson->title.'</option>';
        }
        echo $html;
    }

    function getActivities(){
        $input = JFactory::getApplication()->input;
	 	$lesson = $input->get('lesson','','INT');
        $activities = AxsLMS::getActivityList($lesson);

        $html = '<option value="">--Select--</option>';
        foreach($activities as $activity) {
            if(!$activity->title) {
                $activity->title = $activity->type;
            }
            $html .= '<option value="'.$lesson.':'.$activity->id.'" selected>'.$activity->title.'</option>';
        }
        echo $html;
   }
}