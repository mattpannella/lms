<?php

defined('_JEXEC') or die();

require_once(JPATH_ADMINISTRATOR . "/components/com_award/helpers/helper.php");

class SplmsControllerCourseprogress extends FOFController {

    /**
     * Wrapper function to handle course completion requests from JavaScript
     *
     * @return void
     */
    public function completeCourse() {
        $input = JFactory::getApplication()->input;

        $userId = $input->get('user_id');
        $courseId = $input->get('course_id');
        $startDate = $input->get('start_date');
        $completeDate = $input->get('complete_date');

        if(!$completeDate) {
            $completeDate = null;
        } else {
            $completeDate = date('Y-m-d H:i:s',strtotime($completeDate));
        }

        $result = $this->markCourseCompleted($userId, $courseId, $startDate, $completeDate);

        echo json_encode($result);
    }

    /**
     * Wrapper function to handle course progress archive button action
     *
     * @return void
     */
    public function archiveCourseProgress() {
        $input = JFactory::getApplication()->input;

        $userId = $input->get('user_id', null, 'INT');
        $courseId = $input->get('course_id', null, 'INT');

        $result = AxsLMS::archiveCourseProgress($userId, $courseId);

        echo json_encode($result);
    }

    /**
     * markCourseCompleted
     * Marks a course as complete for a specific user
     *
     * @param int $userId
     * @param int $courseId
     * @param string $startDate
     * @param string $completeDate
     * @return boolean Course successfully marked as complete
     */
    private function markCourseCompleted($userId, $courseId, $startDate, $completeDate) {
        $result = AxsLMS::setCourseCompleted($userId, $courseId, $startDate, $completeDate);

        // Now assign any awards for the course
        $awardCheckParams = new stdClass();
        $awardCheckParams->requirement_type = 'course';
        $awardCheckParams->course_id = $courseId;
        $award_ids = AxsLMS::getAvailableBadges($awardCheckParams);
        if($award_ids) {
            BadgeHelper::updateBadges($award_ids, $userId, null, $completeDate);
        }

        return $result;
    }

    /**
     * Adds a course progress entry to keep track of course progress on a per-user basis
     *
     * @return void
     */
    public function addCourseProgressEntry() {
        $input = JFactory::getApplication()->input;

        $userId = $input->get('user_id');
        $courseId = $input->get('course_id');
        $startDate = $input->get('start_date');
        $completeDate = $input->get('complete_date');

        if(empty($startDate)) {
            $startDate = !empty($completeDate) ? $completeDate : date('Y-m-d H:i:s');
        }

        if(!$completeDate) {
            $completeDate = null;
        } else {
            $completeDate = date('Y-m-d H:i:s',strtotime($completeDate));
        }

        $addNewCourseResult = json_encode(AxsLMS::courseProgress_addNewCourse($userId, $courseId, $startDate, $completeDate));

        // We're adding a complete date, so mark the course completed
        if(!empty($completeDate) && $addNewCourseResult) {
            $this->markCourseCompleted($userId, $courseId, $startDate, $completeDate);
        }

        echo $addNewCourseResult;
    }
}