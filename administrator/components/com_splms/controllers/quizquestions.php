<?php

defined('_JEXEC') or die();

class SplmsControllerQuizquestions extends FOFController {

	public static function importQuestions()  {
		$input = JFactory::getApplication()->input;
		$file = $_FILES["csv_file"];
		$filename = $file["name"];
		$directory = '../tmp/';
		$name = explode(".", $filename);            
        $fileType = strtolower($name[count($name) -1]);
		$columns = array();
		$message = 'Error Importing';
		$status = "failed";
        if(isset($file) && $file['error'] == 0) {
            //check file type
            $filetype = $file['type'];
            $zipFileTypes = [
				'application/vnd.ms-excel'
			];
	
            if($fileType == 'csv') {
				$newFileName = $name[0]."-".time()."-".rand(1,time()).'.'.$name[1];
				$importFile = $directory.$newFileName;
				if(move_uploaded_file($file['tmp_name'], $importFile)) {
						$bom = "\xef\xbb\xbf";				
						$fileHandle = fopen($importFile, "r");
						if (fgets($fileHandle, 4) !== $bom) {
							// BOM not found - rewind pointer to start of file.
							rewind($fileHandle);
						}
						$header = fgetcsv($fileHandle, 0, ",");
						$questions = array();
						$media = array();
						$answer_one = array();
						$answer_two = array();
						$answer_three = array();
						$answer_four = array();
						$correct = array();
						$i = 0;
						
						while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {			
							$import = array();              	
							$import = array_combine($header, $row);
							$import_question = "";
							$import_answer_one = "";
							$import_answer_two = "";
							$import_answer_three = "";
							$import_answer_four = "";
							$import_correct = ""; 
							if($import['question']) {
								$import_question = $import['question'];
							} else {
								$import_question = "";
							}
							if($import["answer_one"]) {
								$import_answer_one = $import["answer_one"];
							} else {
								$import_answer_one = "";
							}
							if($import["answer_two"]) {
								$import_answer_two = $import["answer_two"];
							} else {
								$import_answer_two = "";
							}
							if($import["answer_three"]) {
								$import_answer_three = $import["answer_three"];
							} else {
								$import_answer_three = "";
							}
							if($import["answer_four"]) {
								$import_answer_four = $import["answer_four"];
							} else {
								$import_answer_four = "";
							}
							if($import["correct"]) {
								$import_correct = (int)$import["correct"] - 1;
							}				 else {
								$import_correct  = 0;
							}
							array_push($questions,$import_question);
							array_push($media,"");
							array_push($answer_one,$import_answer_one);  
							array_push($answer_two,$import_answer_two);     		
							array_push($answer_three,$import_answer_three);  
							array_push($answer_four,$import_answer_four);
							array_push($correct,$import_correct);
							$i++;
						}
						$questionData = new stdClass();
						$questionData->qes_title = $questions;
						$questionData->media = $media;
						$questionData->ans_one = $answer_one;
						$questionData->ans_two = $answer_two;
						$questionData->ans_three = $answer_three;
						$questionData->ans_four = $answer_four;
						$questionData->right_ans = $correct;
						$status = "success";
						$message = "$i questions imported";
						unlink($importFile);
				}
			} else {
				$message = 'Error Importing: File must be a CSV';
				$status = "failed";
			}
        } else {
			$message = 'Error Importing: File could not upload';
			$status = "failed";
		}

		$response = new stdClass();
		$response->status = $status;
		$response->message = $message;
    	$response->questionData = base64_encode(json_encode($questionData));
    	echo json_encode($response);
	}

}