<?php

defined('_JEXEC') or die();

class SplmsControllerLms extends FOFController {

	public function getStudents() {
	 	$params = new stdClass();
        $params->course_id = JRequest::getVar('id');
        if(JRequest::getVar('lang')) {
            $params->language  = JRequest::getVar('lang');
        }
        $studentList = AxsLMS::newStudentList($params);
        $students = $studentList->getStudents($params);
        $data = json_encode($students);

        echo $data;
    }

    public function getStudentLessons() {
        $params = new stdClass();
        $params->course_id = JRequest::getVar('id');

        if(JRequest::getVar('lang')) {
            $language = JRequest::getVar('lang');
            $params->language = $language;
        }

        $startedLessons = AxsLMS::getStudentLessons($params);

        foreach($startedLessons as &$lesson) {

            $course = new stdClass();
            $course->user_id = (int)$lesson->user;
            $course->course_id = (int)$params->course_id;
            $course->language = $language;

            $completedStatus = AxsLMS::getLessonCompletedList($course, $lesson->lesson_id);

            /* There will always be just one lesson status array for a single lesson ID that is returned
             * If the result is not null, that means a record with the 'completed' status was found
             * and we need to use that status instead of the 'started' status, for the given parameters.
             */
            if(!empty($completedStatus)) {

                $lesson->status = 'completed';
            }
        }

        $data = json_encode($startedLessons);

        echo $data;
    }

    public function markCoursePaid($id = null) {
        if(!$id) {
            $id = JRequest::getVar('id');
        }

        if($id) {
            $db = JFactory::getDbo();
            $query = "UPDATE axs_course_purchases SET status = 'PAID' WHERE id = $id LIMIT 1";
            $db->setQuery($query);
            $db->execute();
            echo "success";
        } else {
            echo "No Course ID Provided";
        }
    }

    public function deleteCoursePurchase($id = null) {
        if(!$id) {
            $id = JRequest::getVar('id');
        }
        if($id) {
            $db = JFactory::getDbo();
            $query = "DELETE FROM axs_course_purchases WHERE id = $id LIMIT 1";
            $db->setQuery($query);
            $db->execute();
            echo "success";
        } else {
            echo "No Course ID Provided";
        }
    }

}