<?php

defined('_JEXEC') or die();

class SplmsControllerContentlibraries extends FOFController {

	public function getLibraryItems($params = null) {
        if(empty($params)) {
            $input = JFactory::getApplication()->input;
            $params = new stdClass();

            $params->topic      = $input->get('topic', '', 'STRING');
            $params->category   = $input->get('category', '', 'STRING');
            $params->keyword    = $input->get('keyword', '', 'STRING');
            $params->size       = $input->get('size', '', 'STRING');
            $params->sort       = $input->get('sort', '', 'STRING');
            $params->start      = $input->get('start', '', 'STRING');
            $params->language   = $input->get('bl_language', '', 'STRING');
            $params->content_type_filter     = $input->get('content_type_filter', '', 'STRING');
        }

	 	echo AxsScormFactory::loadLibraryListings($params);
    }
}