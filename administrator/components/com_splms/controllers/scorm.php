<?php


defined('_JEXEC') or die();
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
class SplmsControllerScorm extends FOFController {

	public function buildScormUploader() {
        ob_start();
        require 'components/com_splms/templates/scormuploader.php';
        echo ob_get_clean();
    }

    // @todo Convert this into a multi-zip processor that handles multiple zip files within a zip file
    public function processFile() {
        $file = $_FILES["filedata"];
        $result = $this->upload($file);
        echo json_encode($result);
    }

    /* This function grabs the crs file information - its format is different than all other AICC files
     * Each line has its own information, whereas the other files have the standard delimited record set
     * with header information on the first line.
     */
    private static function readCRSFile($crsFile, $courseName) {

        // Organize data according to sections - these contain terms in brackets
        $courses = [];

        if(!empty($crsFile)) {
            $rows = file($crsFile);

            if (is_array($rows)) {

                $courses[$courseName] = new stdClass();

                foreach ($rows as $row) {
                    if (preg_match("/^(.+)=(.+)$/", $row, $matches)) {
                        switch (strtolower(trim($matches[1]))) {
                            case 'course_id':
                                $courses[$courseName]->id = trim($matches[2]);
                            break;
                            case 'course_title':
                                $courses[$courseName]->title = trim($matches[2]);
                            break;
                            case 'version':
                                $courses[$courseName]->version = 'AICC_'.trim($matches[2]);
                            break;
                        }
                    }
                }
            }
        }

        return $courses;
    }

    private static function readAUFile($auFile, $courseName) {

        $auData = [];

        if(!empty($auFile)) {

            $auData[$courseName] = new stdClass();
            $fileData = file($auFile);

            $rowCount = !empty($fileData) ? count($fileData) : 0;

            if($rowCount > 1) {

                $headers = explode(',', $fileData[0]);

                // rowCount should always be 2
                $dataRow = explode("\",", $fileData[1]);

                foreach($headers as $index => $header) {

                    $trimmedHeader = trim(strtolower($header), "\"");
                    $auData[$courseName]->$trimmedHeader = $dataRow[$index];
                }
            }
        }

        return $auData;
    }

    /*
        foreach ($ids as $courseid => $id) {
            if (!isset($courses[$courseid])) {
                $courses[$courseid] = new stdClass();
            }

            if (isset($id->des)) {
                $contents = $id->des->get_content();
                $rows = explode("\r\n", $contents);
                $columns = scorm_get_aicc_columns($rows[0]);
                $regexp = scorm_forge_cols_regexp($columns->columns);
                for ($i = 1; $i < count($rows); $i++) {
                    if (preg_match($regexp, $rows[$i], $matches)) {
                        for ($j = 0; $j < count($columns->columns); $j++) {
                            $column = $columns->columns[$j];
                            if (!isset($courses[$courseid]->elements[substr(trim($matches[$columns->mastercol + 1]), 1 , -1)])) {
                                $courses[$courseid]->elements[substr(trim($matches[$columns->mastercol + 1]), 1 , -1)] = new stdClass();
                            }
                            $courses[$courseid]->elements[substr(trim($matches[$columns->mastercol + 1]), 1 , -1)]->$column = substr(trim($matches[$j + 1]), 1, -1);
                        }
                    }
                }
            }
            if (isset($id->cst)) {
                $contents = $id->cst->get_content();
                $rows = explode("\r\n", $contents);
                $columns = scorm_get_aicc_columns($rows[0], 'block');
                $regexp = scorm_forge_cols_regexp($columns->columns, '(.+)?,');
                for ($i = 1; $i < count($rows); $i++) {
                    if (preg_match($regexp, $rows[$i], $matches)) {
                        for ($j = 0; $j < count($columns->columns); $j++) {
                            if ($j != $columns->mastercol) {
                                $element = substr(trim($matches[$j + 1]), 1 , -1);
                                if (!empty($element)) {
                                    $courses[$courseid]->elements[$element]->parent = substr(trim($matches[$columns->mastercol + 1]), 1, -1);
                                }
                            }
                        }
                    }
                }
            }
            if (isset($id->ort)) {
                $contents = $id->ort->get_content();
                $rows = explode("\r\n", $contents);
                $columns = scorm_get_aicc_columns($rows[0], 'course_element');
                $regexp = scorm_forge_cols_regexp($columns->columns, '(.+)?,');
                for ($i = 1; $i < count($rows); $i++) {
                    if (preg_match($regexp, $rows[$i], $matches)) {
                        for ($j = 0; $j < count($matches) - 1; $j++) {
                            if ($j != $columns->mastercol) {
                                $courses[$courseid]->elements[substr(trim($matches[$j + 1]), 1, -1)]->parent = substr(trim($matches[$columns->mastercol + 1]), 1, -1);
                            }
                        }
                    }
                }
            }
            if (isset($id->pre)) {
                $contents = $id->pre->get_content();
                $rows = explode("\r\n", $contents);
                $columns = scorm_get_aicc_columns($rows[0], 'structure_element');
                $regexp = scorm_forge_cols_regexp($columns->columns, '(.+),');
                for ($i = 1; $i < count($rows); $i++) {
                    if (preg_match($regexp, $rows[$i], $matches)) {
                        $elementid = trim($matches[$columns->mastercol + 1]);
                        $elementid = trim(trim($elementid, '"'), "'"); // Remove any quotes.

                        $prereq = trim($matches[2 - $columns->mastercol]);
                        $prereq = trim(trim($prereq, '"'), "'"); // Remove any quotes.

                        $courses[$courseid]->elements[$elementid]->prerequisites = $prereq;
                    }
                }
            }
            if (isset($id->cmp)) {
                $contents = $id->cmp->get_content();
                $rows = explode("\r\n", $contents);
            }
        }
        */

    private static function getAICCMetaData($path) {
        /* We need to build out an object with the following public properties:
         * Schema (schema): Overall format of the AICC course according to the spec
         * Version (version): Version of the course
         * Title (title): Course title
         * Launch File (launch_file): File that will be opened when launching the course content
         */

        // Get the AICC files from the temporary path that had been extracted
        $structureFiles = scandir($path, SCANDIR_SORT_NONE);
        $aiccMetaData = new stdClass();

        // We want to read in any files ending in .crs, .au, .des, .cst, .pre and sometimes .ort
        $validFileExtensions = ['crs', 'au', 'des', 'cst', 'pre', 'ort'];

        $aiccMetaData->schema = "AICC";

        if(!empty($structureFiles)) {

            foreach($structureFiles as $index => $file) {

                $fullPath = $path . DIRECTORY_SEPARATOR . $file;
                $ext = strtolower(substr($file, strrpos($file, '.') + 1));
                $courseName = null;

                if(in_array($ext, $validFileExtensions)) {

                    // Extract the common name of the course fileset
                    $courseName = substr($file, 0, strrpos($file, '.'));

                    switch($ext) {

                        case 'crs': {

                            // Parse the crs file
                            $crsData = self::readCRSFile($fullPath, $courseName);

                            $aiccMetaData->version = $crsData[$courseName]->version;
                            $aiccMetaData->title = $crsData[$courseName]->title;

                            break;
                        }

                        case 'au': {

                            $auData = self::readAUFile($fullPath, $courseName);

                            // Set this to true if we are grabbing data from an external AICC resource
                            $aiccMetaData->external = false;

                            $fileName = trim($auData[$courseName]->file_name, "\"");
                            $webLaunch = !empty($auData[$courseName]->web_launch) ? trim($auData[$courseName]->web_launch, "\"") : '';

                            if(!empty($fileName)) {

                                // If the filename is a URL, attach the web launch information to it as params
                                if(preg_match('/https?.*/', $fileName)) {

                                    $launchURL = $fileName . '?' . $webLaunch;
                                    $aiccMetaData->external = true;
                                } else {

                                    $launchURL = $fileName;
                                }
                            }

                            $aiccMetaData->launch_file = $launchURL;

                            break;
                        }

                        default: {break;}
                    }
                } else {
                    continue;
                }
            }
        }

        return $aiccMetaData;
    }

    public static function getScormMetaData($manifestFile) {
        if(!$manifestFile) {
            return false;
        }
        $xmlFile = simplexml_load_file($manifestFile, 'SimpleXMLElement', LIBXML_NOCDATA);
        $xmlJson = json_encode($xmlFile);
        $xml = json_decode($xmlJson);
        if(!$xml) {
            return false;
        }
        $data = new stdClass();
        $metadata = "";
        if(empty($xml->metadata->schema)) {
            if(empty($xml->resources->metadata->schema)) {
                if(is_array($xml->resources->resource)) {
                    foreach($xml->resources->resource as $r) {
                        foreach($r as $key => $value) {
                            if($key == "metadata") {
                                $metadata = $value;
                                break;
                            }
                            if($metadata) {
                                break;
                            }
                        }
                    }
                } else {
                    $metadata = $xml->resources->resource->metadata;
                }
            } else {
                $metadata = $xml->resources->metadata;
            }
        } else {
            $metadata = $xml->metadata;
        }
        $data->schema      = $metadata->schema;
        $data->version     = $metadata->schemaversion;
        if(empty($metadata->schema) || empty($metadata->schemaversion) ) {
            // look in organization object
            if($xml->organizations->organization->metadata->schema) {
                $data->schema = $xml->organizations->organization->metadata->schema;
            }
            if($xml->organizations->organization->metadata->schemaversion) {
                $data->version = $xml->organizations->organization->metadata->schemaversion;
            }
        }
        $parameters = '';
        if(is_array($xml->organizations->organization->item)) {
            $identifierref = $xml->organizations->organization->item[0]->{"@attributes"}->identifierref;
            if($xml->organizations->organization->item[0]->{"@attributes"}->parameters) {
                $parameters = $xml->organizations->organization->item[0]->{"@attributes"}->parameters;
            }
        } else {
            $identifierref = $xml->organizations->organization->item->{"@attributes"}->identifierref;
            if($xml->organizations->organization->item->{"@attributes"}->parameters) {
                $parameters = $xml->organizations->organization->item->{"@attributes"}->parameters;
            }
        }

        if(is_array($xml->resources->resource)) {
            foreach($xml->resources->resource as $resource) {
                if($resource->{"@attributes"}->href && $resource->{"@attributes"}->identifier == $identifierref) {
                    $data->launch_file = $resource->{"@attributes"}->href.$parameters;
                }
            }
        } else {
           $data->launch_file = $xml->resources->resource->{"@attributes"}->href.$parameters;
        }
        $data->title = $xml->organizations->organization->title;
        return $data;
    }

    private static function upload($file) {
        //Build json response
        require 'administrator/components/com_splms/helpers/allowedFileTypes.php';
        $response = new stdClass();
        $response->message = "Your SCORM Package Failed to Upload";
        $response->status = "failed";
        if(isset($file) && $file['error'] == 0) {
            //check file type
            $filetype = $file['type'];
            $zipFileTypes = [
                'application/zip',
                'application/x-zip-compressed',
            ];

            if(in_array($filetype, $zipFileTypes)) {
                //build directory path
                $symlinkPath = AxsImages::getImagesPath().'/';
                $activityPath = 'scorm';
                $base = 'lmsfiles/';
                $pathArray = explode('/', $base.$activityPath);
                $pathCount = is_countable($pathArray) ? count($pathArray) : 0;
                $fullPath = $symlinkPath.$base.$activityPath.'/';

                $date = strtotime('now');

                for($i = 0; $i < $pathCount; $i++) {
                    if(!isset($filePath)) {
                        $filePath = $symlinkPath.'/'.$pathArray[$i];
                    } else {
                        $filePath = $filePath.'/'.$pathArray[$i];
                    }
                    if(!file_exists($filePath)) {
                        $response->message = "Step 3";
                        mkdir($filePath);
                        chmod($filePath, 0777);
                    }
                }

                $uploaddir = $fullPath;
                $uploadFile = basename($file['name']);
                $fileurl    = rand(0, 99999);
                $uploadFile = str_replace(' ', '-', $uploadFile);
                $newName = $uploaddir . $fileurl . $uploadFile;
                if(move_uploaded_file($file['tmp_name'], $newName)) {
                    $zip = new ZipArchive;
                    $zipFile = $zip->open($newName);
                    $uniqueName = AxsLMS::createUniqueID();
                    $uniquePath = $uploaddir.$uniqueName;

                    if($zipFile) {
                        $continue = true;
                        $response->files = '';

                        $isAICC = self::isAICCFile($zip);

                        if(!$isAICC) {

                            // Check to see if this is a valid SCORM package
                            if($zip->locateName('imsmanifest.xml') === FALSE) {

                                $continue = false;
                                $response->message = "Your SCORM Package is Missing the Manifest File or is an Unsupported Version";
                                unlink($newName);
                                return $response;
                            }

                            $fileNotAllowed = false;

                            // Check for allowed files - if one file is not allowed, stop processing the archive
                            for ($i = 0; $i < $zip->numFiles; $i++) {
                                $archive = $zip->getNameIndex($i);
                                if(strpos($archive,'.')) {
                                    $fileNameArray = explode('.',$archive);
                                    $index = count($fileNameArray) - 1;
                                    $filename = $fileNameArray[$index];
                                    if(strpos($filename,'/')) {
                                        $fileNameDirectory = explode('/',$filename);
                                        $dir_index = count($fileNameDirectory) - 1;
                                        $filename = $fileNameDirectory[$dir_index];
                                    }

                                    if(!array_key_exists(strtolower($filename), $allowedFileTypes) && $filename) {
                                        $fileNotAllowed = true;;
                                        break;
                                    }
                                }
                            }

                            if($fileNotAllowed) {

                                $continue = false;
                                $response->files .= $archive."\n";
                                $response->message = "Your SCORM Package Contains Files That Are Not Allowed";
                                unlink($newName);
                                return $response;
                            }
                        }

                        if($continue) {
                            $zip->extractTo($uniquePath);
                        }

                        $zip->close();
                        unlink($newName);
                    }

                    // Get Metadata
                    if($isAICC) {
                        $scormData = self::getAICCMetaData($uniquePath);
                    } else {
                        $scormData = self::getScormMetaData($uniquePath.'/imsmanifest.xml');
                    }
                    if(!empty($scormData)) {
                        if(!$scormData->schema) {
                            $response->message = "The Schema of Your SCORM Package is Missing";
                            return $response;
                        }
                        if(!$scormData->version) {
                            $response->message = "The Version of Your SCORM Package is Missing";
                            return $response;
                        }
                        if(!$scormData->title) {
                            $response->message = "The Title of Your SCORM Package is Missing";
                            return $response;
                        }
                        if((($isAICC && $scormData->external) || !$isAICC) && !$scormData->launch_file) {
                            $response->message = "The Launch File of Your SCORM Package is Missing";
                            return $response;
                        }

                        if(!$isAICC) {

                            if($scormData->schema != "ADL SCORM") {
                                $response->message = "This is an Unsupported SCORM Type";
                                return $response;
                            }

                            if(
                                $scormData->version != '1.2' &&
                                strpos($scormData->version, '2004') === false &&
                                strpos(strtolower($scormData->version), 'cam 1.3') === false
                            ) {
                                $response->message = "This is an Unsupported SCORM Version";
                                return $response;
                            }
                        }
                    } else {
                        $response->message = "Your SCORM Package is Corrupted or is an Unsupported Version";
                        return $response;
                    }

                    if($continue) {
                        $response->schema      = $scormData->schema;
                        $response->version     = $scormData->version;
                        $response->title       = $scormData->title;
                        $response->launch_file = $scormData->launch_file;
                        $response->directory   = $uniquePath;
                        $response->fullPath    = $uniquePath.'/'.$scormData->launch_file;
                        $response->external    = $scormData->external;
                        $response->params      = self::encodeParams($response);
                        $response->status      = "success";
                        $response->message     = "Your SCORM Package Uploaded Successfully";
                    }
                }
            } else {
                $response->message = "Error: File Type Not Allowed (must be a SCORM Zip file)";
            }
        }
        return $response;
    }

    private static function encodeParams($response) {
        unset($response->status);
        unset($response->message);
        $key = AxsKeys::getKey('lms');
        $encryptedParams = base64_encode(AxsEncryption::encrypt($response, $key));
        return $encryptedParams;
    }

    private static function isAICCFile($zipFile) {

        $isAICC = false;

        // Check the zip file for a valid .crs file - this is an indicator of an AICC archive
        for ($i = $zipFile->numFiles; $i >= 0; $i--) {
            $fileName = $zipFile->getNameIndex($i);

            $ext = substr($fileName, strrpos($fileName, '.') + 1);

            // .crs is an AICC file extension that is the starting point for AICC course information parsing
            if (strtolower($ext) == 'crs') {

                $isAICC = true;
                break;
            }
        }

        return $isAICC;
    }
}