<?php


defined('_JEXEC') or die();
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
class SplmsControllerScorm_cloud extends FOFController {

	function buildScormUploader() {
	 	$courseInfo = new stdClass();
        $courseInfo->courseID = uniqid();
        $scormLMS = new AxsScormLMS($courseInfo);
        $course = $scormLMS->getCourseSetup('upload');
                    
        ob_start();
        require 'components/com_splms/templates/scormuploader.php';
        echo ob_get_clean();       
    }

    function getCourseMetaData() {
        $courseInfo = new stdClass();
        $courseInfo->courseID = JRequest::getVar('courseid');
        $scormLMS = new AxsScormLMS($courseInfo);
        $courseService = $scormLMS->ScormService->getCourseService();
        $metadata = $courseService->GetCourseDetail($scormLMS->courseID);

        echo $metadata;
    }

    function getCoursePropertyEditor() {
        $courseInfo = new stdClass();
        $courseInfo->courseID = JRequest::getVar('courseid');
        $scormLMS = new AxsScormLMS($courseInfo);
        $courseService = $scormLMS->ScormService->getCourseService();
        $propsEditorUrl = $courseService->GetPropertyEditorUrl($courseInfo->courseID,Null,Null);

        echo $propsEditorUrl;
    }

    function storeProgress($params) {
        $db = JFactory::getDbo();             
        $reg = JRequest::getVar('reg');
        $dataEncrypted = json_decode(base64_decode($reg));
        //$key = AxsKeys::getKey('lms');
        //$registrationRowID = AxsEncryption::decrypt($dataEncrypted, $key);
        $registrationRowID = $dataEncrypted;
        $params = new stdClass();
        $params->id = $registrationRowID; 
        $registration = AxsScormLMS::getScormRegistration($params);
        $xml = simplexml_load_string($_POST['data']);
        $status = (string)$xml->complete;
        $success = (string)$xml->success;
        if($status == 'complete') {
            $scormStatus = 'completed';
        } else {
            $scormStatus = 'started';
        }

        if($success == 'failed') {
            $scormStatus = 'failed';
        }

        $data = new stdClass();
        $data->user_id = $registration->user_id;
        $data->course_id = $registration->course_id;
        $data->scorm_course_id = $registration->scorm_course_id;
        $data->registration_id = $registration->registration_id;
        $data->language = $registration->language;
        $data->success = $success;
        $data->status = $scormStatus;
        $data->lesson_id = $registration->lesson_id;

        if($registration->lesson_id) {
            AxsLMS::insertLessonStatus($data);
        } else {
            AxsLMS::courseProgress_ScormContent($data);
        }        
        
    }

    function createCourseRegistration($params, $startButton = true) {
        $db = JFactory::getDbo();

        if($params) {
            $scormData = $params;
        } else {
            $data = JRequest::getVar('data');
            $dataEncrypted = json_decode(base64_decode($data));
            //$key = AxsKeys::getKey('lms');
            //$scormData = AxsEncryption::decrypt($dataEncrypted, $key);
            $scormData = $dataEncrypted;
        }
        
        $user = JFactory::getUser();
        if($scormData->scorm_course_id && $user) {

            //Check if registration already exist for this user and this course or lesson
            $regCheck = AxsScormLMS::getScormRegistration($scormData); 
            if(!$regCheck) {
                $regId = uniqid(rand(), true);
                $profile = AxsExtra::getUserProfileData($user->id);        
                $courseId = $scormData->scorm_course_id;
                $scormData->user_id = $user->id;
                $scormData->registration_id = $regId;
                $scormData->date_created = date('Y-m-d H:i:s');
                $insertRow = $db->insertObject('joom_splms_scorm_registrations', $scormData);

                //Get the last inserted ID and encrypt it to send into the end point for progress
                $id = $db->insertid();
                //$encryptedReg = AxsEncryption::encrypt($id, $key);
                //$regEncrypted = base64_encode($encryptedReg);
                $regEncrypted = base64_encode($id);
                if($insertRow) {
                    $learnerId = $user->email;
                    $learnerFirstName = $profile->first;
                    $learnerLastName = $profile->last;
                    $courseInfo = new stdClass();
                    $courseInfo->courseID = $courseId;
                    $scormLMS = new AxsScormLMS($courseInfo);
                    $regService = $scormLMS->ScormService->getRegistrationService();                    
                    $resultsPostbackUrl = JUri::base().'index.php?option=com_splms&task=scorm.storeProgress&reg='.$regEncrypted;
                    $regService->CreateRegistration($regId, $courseId, $learnerId, $learnerFirstName, $learnerLastName, null, null, 'xml', $resultsPostbackUrl);
                }
            }
        }
        if(JRequest::getVar('url')) {
            $url = JRequest::getVar('url');
        } else {
            $url = $_SERVER['HTTP_REFERER'];
        }
        JFactory::getApplication()->redirect($url);
    }
}