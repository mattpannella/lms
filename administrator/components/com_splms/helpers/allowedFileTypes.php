<?php

$allowedFileTypes = array(
	'gz' => array(
		'name' => '3Compressed File',
		'type' => 'application/gz'
	),
	'x3d' => array(
		'name' => '3D Crossword Plugin',
		'type' => 'application/vnd.hzn-3d-crossword'
	),
	'3gp' => array(
		'name' => '3GP',
		'type' => 'video/3gpp'
	),
	'3g2' => array(
		'name' => '3GP2',
		'type' => 'video/3gpp2'
	),
	'mseq' => array(
		'name' => '3GPP MSEQ File',
		'type' => 'application/vnd.mseq'
	),
	'pwn' => array(
		'name' => '3M Post It Notes',
		'type' => 'application/vnd.3m.post-it-notes'
	),
	'plb' => array(
		'name' => '3rd Generation Partnership Project - Pic Large',
		'type' => 'application/vnd.3gpp.pic-bw-large'
	),
	'psb' => array(
		'name' => '3rd Generation Partnership Project - Pic Small',
		'type' => 'application/vnd.3gpp.pic-bw-small'
	),
	'pvb' => array(
		'name' => '3rd Generation Partnership Project - Pic Var',
		'type' => 'application/vnd.3gpp.pic-bw-var'
	),
	'tcap' => array(
		'name' => '3rd Generation Partnership Project - Transaction Capabilities Application Part',
		'type' => 'application/vnd.3gpp2.tcap'
	),
	'abw' => array(
		'name' => 'AbiWord',
		'type' => 'application/x-abiword'
	),
	'ace' => array(
		'name' => 'Ace Archive',
		'type' => 'application/x-ace-compressed'
	),
	'acc' => array(
		'name' => 'Active Content Compression',
		'type' => 'application/vnd.americandynamics.acc'
	),
	'acu' => array(
		'name' => 'ACU Cobol',
		'type' => 'application/vnd.acucobol'
	),
	'atc' => array(
		'name' => 'ACU Cobol',
		'type' => 'application/vnd.acucorp'
	),
	'adp' => array(
		'name' => 'Adaptive differential pulse-code modulation',
		'type' => 'audio/adpcm'
	),
	'aab' => array(
		'name' => 'Adobe (Macropedia) Authorware - Binary File',
		'type' => 'application/x-authorware-bin'
	),
	'aam' => array(
		'name' => 'Adobe (Macropedia) Authorware - Map',
		'type' => 'application/x-authorware-map'
	),
	'aas' => array(
		'name' => 'Adobe (Macropedia) Authorware - Segment File',
		'type' => 'application/x-authorware-seg'
	),
	'air' => array(
		'name' => 'Adobe AIR Application',
		'type' => 'application/vnd.adobe.air-application-installer-package+zip'
	),
	'swf' => array(
		'name' => 'Adobe Flash',
		'type' => 'application/x-shockwave-flash'
	),
	'fxp' => array(
		'name' => 'Adobe Flex Project',
		'type' => 'application/vnd.adobe.fxp'
	),
	'pdf' => array(
		'name' => 'Adobe Portable Document Format',
		'type' => 'application/pdf'
	),
	'ppd' => array(
		'name' => 'Adobe PostScript Printer Description File Format',
		'type' => 'application/vnd.cups-ppd'
	),
	'dir' => array(
		'name' => 'Adobe Shockwave Player',
		'type' => 'application/x-director'
	),
	'xdp' => array(
		'name' => 'Adobe XML Data Package',
		'type' => 'application/vnd.adobe.xdp+xml'
	),
	'xfdf' => array(
		'name' => 'Adobe XML Forms Data Format',
		'type' => 'application/vnd.adobe.xfdf'
	),
	'aac' => array(
		'name' => 'Advanced Audio Coding (AAC)',
		'type' => 'audio/x-aac'
	),
	'ahead' => array(
		'name' => 'Ahead AIR Application',
		'type' => 'application/vnd.ahead.space'
	),
	'azw' => array(
		'name' => 'Amazon Kindle eBook format',
		'type' => 'application/vnd.amazon.ebook'
	),
	'ami' => array(
		'name' => 'AmigaDE',
		'type' => 'application/vnd.amiga.ami'
	),
	'atx' => array(
		'name' => 'Antix Game Player',
		'type' => 'application/vnd.antix.game-component'
	),
	'aw' => array(
		'name' => 'Applixware',
		'type' => 'application/applixware'
	),
	'les' => array(
		'name' => 'Archipelago Lesson Player',
		'type' => 'application/vnd.hhe.lesson-player'
	),
	'less' => array(
		'name' => 'Less File',
		'type' => 'text/plain'
	),
	'swi' => array(
		'name' => 'Arista Networks Software Image',
		'type' => 'application/vnd.aristanetworks.swi'
	),
	'ac' => array(
		'name' => 'Attribute Certificate',
		'type' => 'application/pkix-attr-cert'
	),
	'aif' => array(
		'name' => 'Audio Interchange File Format',
		'type' => 'audio/x-aiff'
	),
	'avi' => array(
		'name' => 'Audio Video Interleave (AVI)',
		'type' => 'video/x-msvideo'
	),
	'aep' => array(
		'name' => 'Audiograph',
		'type' => 'application/vnd.audiograph'
	),
	'dxf' => array(
		'name' => 'AutoCAD DXF',
		'type' => 'image/vnd.dxf'
	),
	'dwf' => array(
		'name' => 'Autodesk Design Web Format (DWF)',
		'type' => 'model/vnd.dwf'
	),
	'bak' => array(
		'name' => 'Backup',
		'type' => 'image/bmp'
	),
	'bmp' => array(
		'name' => 'Bitmap Image File',
		'type' => 'image/bmp'
	),
	'mpm' => array(
		'name' => 'Blueice Research Multipass',
		'type' => 'application/vnd.blueice.multipass'
	),
	'bmi' => array(
		'name' => 'BMI Drawing Data Interchange',
		'type' => 'application/vnd.bmi'
	),
	'btif' => array(
		'name' => 'BTIF',
		'type' => 'image/prs.btif'
	),
	'rep' => array(
		'name' => 'BusinessObjects',
		'type' => 'application/vnd.businessobjects'
	),
	'cdxml' => array(
		'name' => 'CambridgeSoft Chem Draw',
		'type' => 'application/vnd.chemdraw+xml'
	),
	'css' => array(
		'name' => 'Cascading Style Sheets (CSS)',
		'type' => 'text/css'
	),
	'cur' => array(
		'name' => 'Cursor File',
		'type' => 'text/css'
	),
	'cdx' => array(
		'name' => 'ChemDraw eXchange file',
		'type' => 'chemical/x-cdx'
	),
	'cml' => array(
		'name' => 'Chemical Markup Language',
		'type' => 'chemical/x-cml'
	),
	'csml' => array(
		'name' => 'Chemical Style Markup Language',
		'type' => 'chemical/x-csml'
	),
	'cdbcmsg' => array(
		'name' => 'CIM Database',
		'type' => 'application/vnd.contact.cmsg'
	),
	'cla' => array(
		'name' => 'Claymore Data Files',
		'type' => 'application/vnd.claymore'
	),
	'c4g' => array(
		'name' => 'Clonk Game',
		'type' => 'application/vnd.clonk.c4group'
	),
	'sub' => array(
		'name' => 'Close Captioning - Subtitle',
		'type' => 'image/vnd.dvb.subtitle'
	),
	'c11amc' => array(
		'name' => 'ClueTrust CartoMobile - Config',
		'type' => 'application/vnd.cluetrust.cartomobile-config'
	),
	'c11amz' => array(
		'name' => 'ClueTrust CartoMobile - Config Package',
		'type' => 'application/vnd.cluetrust.cartomobile-config-pkg'
	),
	'ras' => array(
		'name' => 'CMU Image',
		'type' => 'image/x-cmu-raster'
	),
	'dae' => array(
		'name' => 'COLLADA',
		'type' => 'model/vnd.collada+xml'
	),
	'csv' => array(
		'name' => 'Comma-Seperated Values',
		'type' => 'text/csv'
	),
	'cpt' => array(
		'name' => 'Compact Pro',
		'type' => 'application/mac-compactpro'
	),
	'cgm' => array(
		'name' => 'Computer Graphics Metafile',
		'type' => 'image/cgm'
	),
	'ice' => array(
		'name' => 'CoolTalk',
		'type' => 'x-conference/x-cooltalk'
	),
	'cmx' => array(
		'name' => 'Corel Metafile Exchange (CMX)',
		'type' => 'image/x-cmx'
	),
	'xar' => array(
		'name' => 'CorelXARA',
		'type' => 'application/vnd.xara'
	),
	'cmc' => array(
		'name' => 'CosmoCaller',
		'type' => 'application/vnd.cosmocaller'
	),
	'cpio' => array(
		'name' => 'CPIO Archive',
		'type' => 'application/x-cpio'
	),
	'clkx' => array(
		'name' => 'CrickSoftware - Clicker',
		'type' => 'application/vnd.crick.clicker'
	),
	'clkk' => array(
		'name' => 'CrickSoftware - Clicker - Keyboard',
		'type' => 'application/vnd.crick.clicker.keyboard'
	),
	'clkp' => array(
		'name' => 'CrickSoftware - Clicker - Palette',
		'type' => 'application/vnd.crick.clicker.palette'
	),
	'clkt' => array(
		'name' => 'CrickSoftware - Clicker - Template',
		'type' => 'application/vnd.crick.clicker.template'
	),
	'clkw' => array(
		'name' => 'CrickSoftware - Clicker - Wordbank',
		'type' => 'application/vnd.crick.clicker.wordbank'
	),
	'wbs' => array(
		'name' => 'Critical Tools - PERT Chart EXPERT',
		'type' => 'application/vnd.criticaltools.wbs+xml'
	),
	'cif' => array(
		'name' => 'Crystallographic Interchange Format',
		'type' => 'chemical/x-cif'
	),
	'cmdf' => array(
		'name' => 'CrystalMaker Data Format',
		'type' => 'chemical/x-cmdf'
	),
	'cu' => array(
		'name' => 'CU-SeeMe',
		'type' => 'application/cu-seeme'
	),
	'cww' => array(
		'name' => 'CU-Writer',
		'type' => 'application/prs.cww'
	),
	'cmp' => array(
		'name' => 'CustomMenu',
		'type' => 'application/vnd.yellowriver-custom-menu'
	),
	'uva' => array(
		'name' => 'DECE Audio',
		'type' => 'audio/vnd.dece.audio'
	),
	'uvi' => array(
		'name' => 'DECE Graphic',
		'type' => 'image/vnd.dece.graphic'
	),
	'uvh' => array(
		'name' => 'DECE High Definition Video',
		'type' => 'video/vnd.dece.hd'
	),
	'uvm' => array(
		'name' => 'DECE Mobile Video',
		'type' => 'video/vnd.dece.mobile'
	),
	'uvu' => array(
		'name' => 'DECE MP4',
		'type' => 'video/vnd.uvvu.mp4'
	),
	'uvp' => array(
		'name' => 'DECE PD Video',
		'type' => 'video/vnd.dece.pd'
	),
	'uvs' => array(
		'name' => 'DECE SD Video',
		'type' => 'video/vnd.dece.sd'
	),
	'uaa' => array(
		'name' => 'DECE Video',
		'type' => 'video/vnd.dece.video'
	),
	'dvi' => array(
		'name' => 'Device Independent File Format (DVI)',
		'type' => 'application/x-dvi'
	),
	'seed' => array(
		'name' => 'Digital Siesmograph Networks - SEED Datafiles',
		'type' => 'application/vnd.fdsn.seed'
	),
	'dtb' => array(
		'name' => 'Digital Talking Book',
		'type' => 'application/x-dtbook+xml'
	),
	'res' => array(
		'name' => 'Digital Talking Book - Resource File',
		'type' => 'application/x-dtbresource+xml'
	),
	'ait' => array(
		'name' => 'Digital Video Broadcasting',
		'type' => 'application/vnd.dvb.ait'
	),
	'svc' => array(
		'name' => 'Digital Video Broadcasting',
		'type' => 'application/vnd.dvb.service'
	),
	'eol' => array(
		'name' => 'Digital Winds Music',
		'type' => 'audio/vnd.digital-winds'
	),
	'djvu' => array(
		'name' => 'DjVu',
		'type' => 'image/vnd.djvu'
	),
	'dtd' => array(
		'name' => 'Document Type Definition',
		'type' => 'application/xml-dtd'
	),
	'mlp' => array(
		'name' => 'Dolby Meridian Lossless Packing',
		'type' => 'application/vnd.dolby.mlp'
	),
	'wad' => array(
		'name' => 'Doom Video Game',
		'type' => 'application/x-doom'
	),
	'dpg' => array(
		'name' => 'DPGraph',
		'type' => 'application/vnd.dpgraph'
	),
	'dra' => array(
		'name' => 'DRA Audio',
		'type' => 'audio/vnd.dra'
	),
	'dfac' => array(
		'name' => 'DreamFactory',
		'type' => 'application/vnd.dreamfactory'
	),
	'dts' => array(
		'name' => 'DTS Audio',
		'type' => 'audio/vnd.dts'
	),
	'dtshd' => array(
		'name' => 'DTS High Definition Audio',
		'type' => 'audio/vnd.dts.hd'
	),
	'dwg' => array(
		'name' => 'DWG Drawing',
		'type' => 'image/vnd.dwg'
	),
	'geo' => array(
		'name' => 'DynaGeo',
		'type' => 'application/vnd.dynageo'
	),
	'es' => array(
		'name' => 'ECMAScript',
		'type' => 'application/ecmascript'
	),
	'mag' => array(
		'name' => 'EcoWin Chart',
		'type' => 'application/vnd.ecowin.chart'
	),
	'mmr' => array(
		'name' => 'EDMICS 2000',
		'type' => 'image/vnd.fujixerox.edmics-mmr'
	),
	'rlc' => array(
		'name' => 'EDMICS 2000',
		'type' => 'image/vnd.fujixerox.edmics-rlc'
	),
	'exi' => array(
		'name' => 'Efficient XML Interchange',
		'type' => 'application/exi'
	),
	'mgz' => array(
		'name' => 'EFI Proteus',
		'type' => 'application/vnd.proteus.magazine'
	),
	'epub' => array(
		'name' => 'Electronic Publication',
		'type' => 'application/epub+zip'
	),
	'nml' => array(
		'name' => 'Enliven Viewer',
		'type' => 'application/vnd.enliven'
	),
	'xpr' => array(
		'name' => 'Express by Infoseek',
		'type' => 'application/vnd.is-xpr'
	),
	'xif' => array(
		'name' => 'eXtended Image File Format (XIFF)',
		'type' => 'image/vnd.xiff'
	),
	'xfdl' => array(
		'name' => 'Extensible Forms Description Language',
		'type' => 'application/vnd.xfdl'
	),
	'emma' => array(
		'name' => 'Extensible MultiModal Annotation',
		'type' => 'application/emma+xml'
	),
	'ez2' => array(
		'name' => 'EZPix Secure Photo Album',
		'type' => 'application/vnd.ezpix-album'
	),
	'ez3' => array(
		'name' => 'EZPix Secure Photo Album',
		'type' => 'application/vnd.ezpix-package'
	),
	'fst' => array(
		'name' => 'FAST Search & Transfer ASA',
		'type' => 'image/vnd.fst'
	),
	'fvt' => array(
		'name' => 'FAST Search & Transfer ASA',
		'type' => 'video/vnd.fvt'
	),
	'fbs' => array(
		'name' => 'FastBid Sheet',
		'type' => 'image/vnd.fastbidsheet'
	),
	'fe_launch' => array(
		'name' => 'FCS Express Layout Link',
		'type' => 'application/vnd.denovo.fcselayout-link'
	),
	'f4v' => array(
		'name' => 'Flash Video',
		'type' => 'video/x-f4v'
	),
	'flv' => array(
		'name' => 'Flash Video',
		'type' => 'video/x-flv'
	),
	'fpx' => array(
		'name' => 'FlashPix',
		'type' => 'image/vnd.fpx'
	),
	'npx' => array(
		'name' => 'FlashPix',
		'type' => 'image/vnd.net-fpx'
	),
	'flx' => array(
		'name' => 'FLEXSTOR',
		'type' => 'text/vnd.fmi.flexstor'
	),
	'fli' => array(
		'name' => 'FLI/FLC Animation Format',
		'type' => 'video/x-fli'
	),
	'ftc' => array(
		'name' => 'FluxTime Clip',
		'type' => 'application/vnd.fluxtime.clip'
	),
	'fdf' => array(
		'name' => 'Forms Data Format',
		'type' => 'application/vnd.fdf'
	),
	'f' => array(
		'name' => 'Fortran Source File',
		'type' => 'text/x-fortran'
	),
	'mif' => array(
		'name' => 'FrameMaker Interchange Format',
		'type' => 'application/vnd.mif'
	),
	'fm' => array(
		'name' => 'FrameMaker Normal Format',
		'type' => 'application/vnd.framemaker'
	),
	'fh' => array(
		'name' => 'FreeHand MX',
		'type' => 'image/x-freehand'
	),
	'fsc' => array(
		'name' => 'Friendly Software Corporation',
		'type' => 'application/vnd.fsc.weblaunch'
	),
	'fnc' => array(
		'name' => 'Frogans Player',
		'type' => 'application/vnd.frogans.fnc'
	),
	'ltf' => array(
		'name' => 'Frogans Player',
		'type' => 'application/vnd.frogans.ltf'
	),
	'ddd' => array(
		'name' => 'Fujitsu - Xerox 2D CAD Data',
		'type' => 'application/vnd.fujixerox.ddd'
	),
	'xdw' => array(
		'name' => 'Fujitsu - Xerox DocuWorks',
		'type' => 'application/vnd.fujixerox.docuworks'
	),
	'xbd' => array(
		'name' => 'Fujitsu - Xerox DocuWorks Binder',
		'type' => 'application/vnd.fujixerox.docuworks.binder'
	),
	'oas' => array(
		'name' => 'Fujitsu Oasys',
		'type' => 'application/vnd.fujitsu.oasys'
	),
	'oa2' => array(
		'name' => 'Fujitsu Oasys',
		'type' => 'application/vnd.fujitsu.oasys2'
	),
	'oa3' => array(
		'name' => 'Fujitsu Oasys',
		'type' => 'application/vnd.fujitsu.oasys3'
	),
	'fg5' => array(
		'name' => 'Fujitsu Oasys',
		'type' => 'application/vnd.fujitsu.oasysgp'
	),
	'bh2' => array(
		'name' => 'Fujitsu Oasys',
		'type' => 'application/vnd.fujitsu.oasysprs'
	),
	'spl' => array(
		'name' => 'FutureSplash Animator',
		'type' => 'application/x-futuresplash'
	),
	'fzs' => array(
		'name' => 'FuzzySheet',
		'type' => 'application/vnd.fuzzysheet'
	),
	'g3' => array(
		'name' => 'G3 Fax Image',
		'type' => 'image/g3fax'
	),
	'gmx' => array(
		'name' => 'GameMaker ActiveX',
		'type' => 'application/vnd.gmx'
	),
	'gtw' => array(
		'name' => 'Gen-Trix Studio',
		'type' => 'model/vnd.gtw'
	),
	'txd' => array(
		'name' => 'Genomatix Tuxedo Framework',
		'type' => 'application/vnd.genomatix.tuxedo'
	),
	'ggb' => array(
		'name' => 'GeoGebra',
		'type' => 'application/vnd.geogebra.file'
	),
	'ggt' => array(
		'name' => 'GeoGebra',
		'type' => 'application/vnd.geogebra.tool'
	),
	'gdl' => array(
		'name' => 'Geometric Description Language (GDL)',
		'type' => 'model/vnd.gdl'
	),
	'gex' => array(
		'name' => 'GeoMetry Explorer',
		'type' => 'application/vnd.geometry-explorer'
	),
	'gxt' => array(
		'name' => 'GEONExT and JSXGraph',
		'type' => 'application/vnd.geonext'
	),
	'g2w' => array(
		'name' => 'GeoplanW',
		'type' => 'application/vnd.geoplan'
	),
	'g3w' => array(
		'name' => 'GeospacW',
		'type' => 'application/vnd.geospace'
	),
	'gsf' => array(
		'name' => 'Ghostscript Font',
		'type' => 'application/x-font-ghostscript'
	),
	'bdf' => array(
		'name' => 'Glyph Bitmap Distribution Format',
		'type' => 'application/x-font-bdf'
	),
	'gtar' => array(
		'name' => 'GNU Tar Files',
		'type' => 'application/x-gtar'
	),
	'texinfo' => array(
		'name' => 'GNU Texinfo Document',
		'type' => 'application/x-texinfo'
	),
	'gnumeric' => array(
		'name' => 'Gnumeric',
		'type' => 'application/x-gnumeric'
	),
	'kml' => array(
		'name' => 'Google Earth - KML',
		'type' => 'application/vnd.google-earth.kml+xml'
	),
	'kmz' => array(
		'name' => 'Google Earth - Zipped KML',
		'type' => 'application/vnd.google-earth.kmz'
	),
	'gqf' => array(
		'name' => 'GrafEq',
		'type' => 'application/vnd.grafeq'
	),
	'gif' => array(
		'name' => 'Graphics Interchange Format',
		'type' => 'image/gif'
	),
	'gv' => array(
		'name' => 'Graphviz',
		'type' => 'text/vnd.graphviz'
	),
	'h261' => array(
		'name' => 'H.261',
		'type' => 'video/h261'
	),
	'h263' => array(
		'name' => 'H.263',
		'type' => 'video/h263'
	),
	'h264' => array(
		'name' => 'H.264',
		'type' => 'video/h264'
	),
	'h265' => array(
		'name' => 'H.265',
		'type' => 'video/h265'
	),
	'hdf' => array(
		'name' => 'Hierarchical Data Format',
		'type' => 'application/x-hdf'
	),
	'rip' => array(
		'name' => 'Hit\'n\'Mix',
		'type' => 'audio/vnd.rip'
	),
	'html' => array(
		'name' => 'HyperText Markup Language (HTML)',
		'type' => 'text/html'
	),
	'htm' => array(
		'name' => 'HyperText Markup Language (HTML)',
		'type' => 'text/html'
	),
	'irm' => array(
		'name' => 'IBM DB2 Rights Manager',
		'type' => 'application/vnd.ibm.rights-management'
	),
	'sc' => array(
		'name' => 'IBM Electronic Media Management System - Secure Container',
		'type' => 'application/vnd.ibm.secure-container'
	),
	'ics' => array(
		'name' => 'iCalendar',
		'type' => 'text/calendar'
	),
	'icc' => array(
		'name' => 'ICC profile',
		'type' => 'application/vnd.iccprofile'
	),
	'ico' => array(
		'name' => 'Icon Image',
		'type' => 'image/x-icon'
	),
	'igl' => array(
		'name' => 'igLoader',
		'type' => 'application/vnd.igloader'
	),
	'ief' => array(
		'name' => 'Image Exchange Format',
		'type' => 'image/ief'
	),
	'ivp' => array(
		'name' => 'ImmerVision PURE Players',
		'type' => 'application/vnd.immervision-ivp'
	),
	'ivu' => array(
		'name' => 'ImmerVision PURE Players',
		'type' => 'application/vnd.immervision-ivu'
	),
	'rif' => array(
		'name' => 'IMS Networks',
		'type' => 'application/reginfo+xml'
	),
	'3dml' => array(
		'name' => 'In3D - 3DML',
		'type' => 'text/vnd.in3d.3dml'
	),
	'spot' => array(
		'name' => 'In3D - 3DML',
		'type' => 'text/vnd.in3d.spot'
	),
	'igs' => array(
		'name' => 'Initial Graphics Exchange Specification (IGES)',
		'type' => 'model/iges'
	),
	'i2g' => array(
		'name' => 'Interactive Geometry Software',
		'type' => 'application/vnd.intergeo'
	),
	'cdy' => array(
		'name' => 'Interactive Geometry Software Cinderella',
		'type' => 'application/vnd.cinderella'
	),
	'js' => array(
		'name' => 'JavaScript',
		'type' => 'application/javascript'
	),
	'js~' => array(
		'name' => 'JavaScript',
		'type' => 'application/javascript'
	),
	'js.template' => array(
		'name' => 'JavaScript',
		'type' => 'application/javascript'
	),
	'template' => array(
		'name' => 'JavaScript',
		'type' => 'application/javascript'
	),
	'json' => array(
		'name' => 'JavaScript Object Notation (JSON)',
		'type' => 'application/json'
	),
	'jpm' => array(
		'name' => 'JPEG 2000 Compound Image File Format',
		'type' => 'video/jpm'
	),
	'".jpeg, .jpg"' => array(
		'name' => 'JPEG Image',
		'type' => 'image/jpeg'
	),
	'jpeg' => array(
		'name' => 'JPEG Image',
		'type' => 'image/jpeg'
	),
	'jpg' => array(
		'name' => 'JPEG Image',
		'type' => 'image/jpeg'
	),
	'".DS_Store, .DS_store"' => array(
		'name' => 'JPEG Image',

		'type' => 'image/jpeg'
	),
	'DS_Store' => array(
		'name' => 'Mac OS File',
		'type' => ''
	),
	'ds_store' => array(
		'name' => 'Mac OS File',
		'type' => ''
	),
	'DS_store' => array(
		'name' => 'Mac OS File',
		'type' => ''
	),
	'jpg_' => array(
		'name' => 'JPEG Image',
		'type' => 'image/jpeg'
	),
	'jpgv' => array(
		'name' => 'JPGVideo',
		'type' => 'video/jpeg'
	),
	'ktz' => array(
		'name' => 'Kahootz',
		'type' => 'application/vnd.kahootz'
	),
	'mmd' => array(
		'name' => 'Karaoke on Chipnuts Chipsets',
		'type' => 'application/vnd.chipnuts.karaoke-mmd'
	),
	'karbon' => array(
		'name' => 'KDE KOffice Office Suite - Karbon',
		'type' => 'application/vnd.kde.karbon'
	),
	'chrt' => array(
		'name' => 'KDE KOffice Office Suite - KChart',
		'type' => 'application/vnd.kde.kchart'
	),
	'kfo' => array(
		'name' => 'KDE KOffice Office Suite - Kformula',
		'type' => 'application/vnd.kde.kformula'
	),
	'flw' => array(
		'name' => 'KDE KOffice Office Suite - Kivio',
		'type' => 'application/vnd.kde.kivio'
	),
	'kon' => array(
		'name' => 'KDE KOffice Office Suite - Kontour',
		'type' => 'application/vnd.kde.kontour'
	),
	'kpr' => array(
		'name' => 'KDE KOffice Office Suite - Kpresenter',
		'type' => 'application/vnd.kde.kpresenter'
	),
	'ksp' => array(
		'name' => 'KDE KOffice Office Suite - Kspread',
		'type' => 'application/vnd.kde.kspread'
	),
	'kwd' => array(
		'name' => 'KDE KOffice Office Suite - Kword',
		'type' => 'application/vnd.kde.kword'
	),
	'htke' => array(
		'name' => 'Kenamea App',
		'type' => 'application/vnd.kenameaapp'
	),
	'kia' => array(
		'name' => 'Kidspiration',
		'type' => 'application/vnd.kidspiration'
	),
	'kne' => array(
		'name' => 'Kinar Applications',
		'type' => 'application/vnd.kinar'
	),
	'sse' => array(
		'name' => 'Kodak Storyshare',
		'type' => 'application/vnd.kodak-descriptor'
	),
	'lasxml' => array(
		'name' => 'Laser App Enterprise',
		'type' => 'application/vnd.las.las+xml'
	),
	'latex' => array(
		'name' => 'LaTeX',
		'type' => 'application/x-latex'
	),
	'lbd' => array(
		'name' => 'Life Balance - Desktop Edition',
		'type' => 'application/vnd.llamagraphics.life-balance.desktop'
	),
	'lbe' => array(
		'name' => 'Life Balance - Exchange Format',
		'type' => 'application/vnd.llamagraphics.life-balance.exchange+xml'
	),
	'license' => array(
		'name' => 'License',
		'type' => 'text/plain'
	),
	'jam' => array(
		'name' => 'Lightspeed Audio Lab',
		'type' => 'application/vnd.jam'
	),
	'0.123' => array(
		'name' => 'Lotus 1-2-3',
		'type' => 'application/vnd.lotus-1-2-3'
	),
	'apr' => array(
		'name' => 'Lotus Approach',
		'type' => 'application/vnd.lotus-approach'
	),
	'pre' => array(
		'name' => 'Lotus Freelance',
		'type' => 'application/vnd.lotus-freelance'
	),
	'nsf' => array(
		'name' => 'Lotus Notes',
		'type' => 'application/vnd.lotus-notes'
	),
	'org' => array(
		'name' => 'Lotus Organizer',
		'type' => 'application/vnd.lotus-organizer'
	),
	'scm' => array(
		'name' => 'Lotus Screencam',
		'type' => 'application/vnd.lotus-screencam'
	),
	'scss' => array(
		'name' => 'Sass',
		'type' => 'text/plain'
	),
	'lwp' => array(
		'name' => 'Lotus Wordpro',
		'type' => 'application/vnd.lotus-wordpro'
	),
	'lvp' => array(
		'name' => 'Lucent Voice',
		'type' => 'audio/vnd.lucent.voice'
	),
	'm3u' => array(
		'name' => 'M3U (Multimedia Playlist)',
		'type' => 'audio/x-mpegurl'
	),
	'm4v' => array(
		'name' => 'M4v',
		'type' => 'video/x-m4v'
	),
	'hqx' => array(
		'name' => 'Macintosh BinHex 4.0',
		'type' => 'application/mac-binhex40'
	),
	'map' => array(
		'name' => 'Map File',
		'type' => 'application/octet-stream'
	),
	'mgp' => array(
		'name' => 'MapGuide DBXML',
		'type' => 'application/vnd.osgeo.mapguide.package'
	),
	'mrc' => array(
		'name' => 'MARC Formats',
		'type' => 'application/marc'
	),
	'mrcx' => array(
		'name' => 'MARC21 XML Schema',
		'type' => 'application/marcxml+xml'
	),
	'mxf' => array(
		'name' => 'Material Exchange Format',
		'type' => 'application/mxf'
	),
	'nbp' => array(
		'name' => 'Mathematica Notebook Player',
		'type' => 'application/vnd.wolfram.player'
	),
	'ma' => array(
		'name' => 'Mathematica Notebooks',
		'type' => 'application/mathematica'
	),
	'mathml' => array(
		'name' => 'Mathematical Markup Language',
		'type' => 'application/mathml+xml'
	),
	'mbox' => array(
		'name' => 'Mbox database files',
		'type' => 'application/mbox'
	),
	'mc1' => array(
		'name' => 'MedCalc',
		'type' => 'application/vnd.medcalcdata'
	),
	'mscml' => array(
		'name' => 'Media Server Control Markup Language',
		'type' => 'application/mediaservercontrol+xml'
	),
	'cdkey' => array(
		'name' => 'MediaRemote',
		'type' => 'application/vnd.mediastation.cdkey'
	),
	'mwf' => array(
		'name' => 'Medical Waveform Encoding Format',
		'type' => 'application/vnd.mfer'
	),
	'mfm' => array(
		'name' => 'Melody Format for Mobile Platform',
		'type' => 'application/vnd.mfmp'
	),
	'msh' => array(
		'name' => 'Mesh Data Type',
		'type' => 'model/mesh'
	),
	'mads' => array(
		'name' => 'Metadata Authority  Description Schema',
		'type' => 'application/mads+xml'
	),
	'mets' => array(
		'name' => 'Metadata Encoding and Transmission Standard',
		'type' => 'application/mets+xml'
	),
	'mods' => array(
		'name' => 'Metadata Object Description Schema',
		'type' => 'application/mods+xml'
	),
	'meta4' => array(
		'name' => 'Metalink',
		'type' => 'application/metalink4+xml'
	),
	'potm' => array(
		'name' => 'Micosoft PowerPoint - Macro-Enabled Template File',
		'type' => 'application/vnd.ms-powerpoint.template.macroenabled.12'
	),
	'docm' => array(
		'name' => 'Micosoft Word - Macro-Enabled Document',
		'type' => 'application/vnd.ms-word.document.macroenabled.12'
	),
	'dotm' => array(
		'name' => 'Micosoft Word - Macro-Enabled Template',
		'type' => 'application/vnd.ms-word.template.macroenabled.12'
	),
	'mcd' => array(
		'name' => 'Micro CADAM Helix D&D',
		'type' => 'application/vnd.mcd'
	),
	'flo' => array(
		'name' => 'Micrografx',
		'type' => 'application/vnd.micrografx.flo'
	),
	'igx' => array(
		'name' => 'Micrografx iGrafx Professional',
		'type' => 'application/vnd.micrografx.igx'
	),
	'es3' => array(
		'name' => 'MICROSEC e-Szign¢',
		'type' => 'application/vnd.eszigno3+xml'
	),
	'mdb' => array(
		'name' => 'Microsoft Access',
		'type' => 'application/x-msaccess'
	),
	'asf' => array(
		'name' => 'Microsoft Advanced Systems Format (ASF)',
		'type' => 'video/x-ms-asf'
	),
	'exe' => array(
		'name' => 'Microsoft Application',
		'type' => 'application/x-msdownload'
	),
	'cil' => array(
		'name' => 'Microsoft Artgalry',
		'type' => 'application/vnd.ms-artgalry'
	),
	'cab' => array(
		'name' => 'Microsoft Cabinet File',
		'type' => 'application/vnd.ms-cab-compressed'
	),
	'ims' => array(
		'name' => 'Microsoft Class Server',
		'type' => 'application/vnd.ms-ims'
	),
	'application' => array(
		'name' => 'Microsoft ClickOnce',
		'type' => 'application/x-ms-application'
	),
	'clp' => array(
		'name' => 'Microsoft Clipboard Clip',
		'type' => 'application/x-msclip'
	),
	'mdi' => array(
		'name' => 'Microsoft Document Imaging Format',
		'type' => 'image/vnd.ms-modi'
	),
	'eot' => array(
		'name' => 'Microsoft Embedded OpenType',
		'type' => 'application/vnd.ms-fontobject'
	),
	'xls' => array(
		'name' => 'Microsoft Excel',
		'type' => 'application/vnd.ms-excel'
	),
	'xlam' => array(
		'name' => 'Microsoft Excel - Add-In File',
		'type' => 'application/vnd.ms-excel.addin.macroenabled.12'
	),
	'xlsb' => array(
		'name' => 'Microsoft Excel - Binary Workbook',
		'type' => 'application/vnd.ms-excel.sheet.binary.macroenabled.12'
	),
	'xltm' => array(
		'name' => 'Microsoft Excel - Macro-Enabled Template File',
		'type' => 'application/vnd.ms-excel.template.macroenabled.12'
	),
	'xlsm' => array(
		'name' => 'Microsoft Excel - Macro-Enabled Workbook',
		'type' => 'application/vnd.ms-excel.sheet.macroenabled.12'
	),
	'chm' => array(
		'name' => 'Microsoft Html Help File',
		'type' => 'application/vnd.ms-htmlhelp'
	),
	'crd' => array(
		'name' => 'Microsoft Information Card',
		'type' => 'application/x-mscardfile'
	),
	'lrm' => array(
		'name' => 'Microsoft Learning Resource Module',
		'type' => 'application/vnd.ms-lrm'
	),
	'mvb' => array(
		'name' => 'Microsoft MediaView',
		'type' => 'application/x-msmediaview'
	),
	'mny' => array(
		'name' => 'Microsoft Money',
		'type' => 'application/x-msmoney'
	),
	'pptx' => array(
		'name' => 'Microsoft Office - OOXML - Presentation',
		'type' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
	),
	'sldx' => array(
		'name' => 'Microsoft Office - OOXML - Presentation (Slide)',
		'type' => 'application/vnd.openxmlformats-officedocument.presentationml.slide'
	),
	'ppsx' => array(
		'name' => 'Microsoft Office - OOXML - Presentation (Slideshow)',
		'type' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow'
	),
	'potx' => array(
		'name' => 'Microsoft Office - OOXML - Presentation Template',
		'type' => 'application/vnd.openxmlformats-officedocument.presentationml.template'
	),
	'xlsx' => array(
		'name' => 'Microsoft Office - OOXML - Spreadsheet',
		'type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
	),
	'xltx' => array(
		'name' => 'Microsoft Office - OOXML - Spreadsheet Teplate',
		'type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template'
	),
	'docx' => array(
		'name' => 'Microsoft Office - OOXML - Word Document',
		'type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
	),
	'dotx' => array(
		'name' => 'Microsoft Office - OOXML - Word Document Template',
		'type' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template'
	),
	'obd' => array(
		'name' => 'Microsoft Office Binder',
		'type' => 'application/x-msbinder'
	),
	'thmx' => array(
		'name' => 'Microsoft Office System Release Theme',
		'type' => 'application/vnd.ms-officetheme'
	),
	'onetoc' => array(
		'name' => 'Microsoft OneNote',
		'type' => 'application/onenote'
	),
	'pya' => array(
		'name' => 'Microsoft PlayReady Ecosystem',
		'type' => 'audio/vnd.ms-playready.media.pya'
	),
	'pyv' => array(
		'name' => 'Microsoft PlayReady Ecosystem Video',
		'type' => 'video/vnd.ms-playready.media.pyv'
	),
	'ppt' => array(
		'name' => 'Microsoft PowerPoint',
		'type' => 'application/vnd.ms-powerpoint'
	),
	'ppam' => array(
		'name' => 'Microsoft PowerPoint - Add-in file',
		'type' => 'application/vnd.ms-powerpoint.addin.macroenabled.12'
	),
	'sldm' => array(
		'name' => 'Microsoft PowerPoint - Macro-Enabled Open XML Slide',
		'type' => 'application/vnd.ms-powerpoint.slide.macroenabled.12'
	),
	'pptm' => array(
		'name' => 'Microsoft PowerPoint - Macro-Enabled Presentation File',
		'type' => 'application/vnd.ms-powerpoint.presentation.macroenabled.12'
	),
	'ppsm' => array(
		'name' => 'Microsoft PowerPoint - Macro-Enabled Slide Show File',
		'type' => 'application/vnd.ms-powerpoint.slideshow.macroenabled.12'
	),
	'mpp' => array(
		'name' => 'Microsoft Project',
		'type' => 'application/vnd.ms-project'
	),
	'pub' => array(
		'name' => 'Microsoft Publisher',
		'type' => 'application/x-mspublisher'
	),
	'scd' => array(
		'name' => 'Microsoft Schedule+',
		'type' => 'application/x-msschedule'
	),
	'xap' => array(
		'name' => 'Microsoft Silverlight',
		'type' => 'application/x-silverlight-app'
	),
	'stl' => array(
		'name' => 'Microsoft Trust UI Provider - Certificate Trust Link',
		'type' => 'application/vnd.ms-pki.stl'
	),
	'cat' => array(
		'name' => 'Microsoft Trust UI Provider - Security Catalog',
		'type' => 'application/vnd.ms-pki.seccat'
	),
	'vsd' => array(
		'name' => 'Microsoft Visio',
		'type' => 'application/vnd.visio'
	),
	'wm' => array(
		'name' => 'Microsoft Windows Media',
		'type' => 'video/x-ms-wm'
	),
	'wma' => array(
		'name' => 'Microsoft Windows Media Audio',
		'type' => 'audio/x-ms-wma'
	),
	'wax' => array(
		'name' => 'Microsoft Windows Media Audio Redirector',
		'type' => 'audio/x-ms-wax'
	),
	'wmx' => array(
		'name' => 'Microsoft Windows Media Audio/Video Playlist',
		'type' => 'video/x-ms-wmx'
	),
	'wmd' => array(
		'name' => 'Microsoft Windows Media Player Download Package',
		'type' => 'application/x-ms-wmd'
	),
	'wpl' => array(
		'name' => 'Microsoft Windows Media Player Playlist',
		'type' => 'application/vnd.ms-wpl'
	),
	'wmz' => array(
		'name' => 'Microsoft Windows Media Player Skin Package',
		'type' => 'application/x-ms-wmz'
	),
	'wmv' => array(
		'name' => 'Microsoft Windows Media Video',
		'type' => 'video/x-ms-wmv'
	),
	'wvx' => array(
		'name' => 'Microsoft Windows Media Video Playlist',
		'type' => 'video/x-ms-wvx'
	),
	'wmf' => array(
		'name' => 'Microsoft Windows Metafile',
		'type' => 'application/x-msmetafile'
	),
	'trm' => array(
		'name' => 'Microsoft Windows Terminal Services',
		'type' => 'application/x-msterminal'
	),
	'doc' => array(
		'name' => 'Microsoft Word',
		'type' => 'application/msword'
	),
	'wri' => array(
		'name' => 'Microsoft Wordpad',
		'type' => 'application/x-mswrite'
	),
	'wps' => array(
		'name' => 'Microsoft Works',
		'type' => 'application/vnd.ms-works'
	),
	'xbap' => array(
		'name' => 'Microsoft XAML Browser Application',
		'type' => 'application/x-ms-xbap'
	),
	'xps' => array(
		'name' => 'Microsoft XML Paper Specification',
		'type' => 'application/vnd.ms-xpsdocument'
	),
	'mid' => array(
		'name' => 'MIDI - Musical Instrument Digital Interface',
		'type' => 'audio/midi'
	),
	'mj2' => array(
		'name' => 'Motion JPEG 2000',
		'type' => 'video/mj2'
	),
	'mpga' => array(
		'name' => 'MPEG Audio',
		'type' => 'audio/mpeg'
	),
	'mxu' => array(
		'name' => 'MPEG Url',
		'type' => 'video/vnd.mpegurl'
	),
	'mpeg' => array(
		'name' => 'MPEG Video',
		'type' => 'video/mpeg'
	),
	'm21' => array(
		'name' => 'MPEG-21',
		'type' => 'application/mp21'
	),
	'mp4a' => array(
		'name' => 'MPEG-4 Audio',
		'type' => 'audio/mp4'
	),
	'm4a' => array(
		'name' => 'MPEG-4 Audio',
		'type' => 'audio/mp4'
	),
	'mp4' => array(
		'name' => 'MPEG-4 Video',
		'type' => 'video/mp4'
	),
	'mp3' => array(
		'name' => 'MP3 Audio',
		'type' => 'audio/mp3'
	),
	'mp4' => array(
		'name' => 'MPEG4',
		'type' => 'application/mp4'
	),
	'mbz' => array(
		'name' => 'Moodle File',
		'type' => 'application/mbz'
	),
	'm3u8' => array(
		'name' => 'Multimedia Playlist Unicode',
		'type' => 'application/vnd.apple.mpegurl'
	),
	'mus' => array(
		'name' => 'MUsical Score Interpreted Code Invented  for the ASCII designation of Notation',
		'type' => 'application/vnd.musician'
	),
	'msty' => array(
		'name' => 'Muvee Automatic Video Editing',
		'type' => 'application/vnd.muvee.style'
	),
	'mxml' => array(
		'name' => 'MXML',
		'type' => 'application/xv+xml'
	),
	'ngdat' => array(
		'name' => 'N-Gage Game Data',
		'type' => 'application/vnd.nokia.n-gage.data'
	),
	'n-gage' => array(
		'name' => 'N-Gage Game Installer',
		'type' => 'application/vnd.nokia.n-gage.symbian.install'
	),
	'ncx' => array(
		'name' => 'Navigation Control file for XML (for ePub)',
		'type' => 'application/x-dtbncx+xml'
	),
	'nc' => array(
		'name' => 'Network Common Data Form (NetCDF)',
		'type' => 'application/x-netcdf'
	),
	'nlu' => array(
		'name' => 'neuroLanguage',
		'type' => 'application/vnd.neurolanguage.nlu'
	),
	'dna' => array(
		'name' => 'New Moon Liftoff/DNA',
		'type' => 'application/vnd.dna'
	),
	'nnd' => array(
		'name' => 'NobleNet Directory',
		'type' => 'application/vnd.noblenet-directory'
	),
	'nns' => array(
		'name' => 'NobleNet Sealer',
		'type' => 'application/vnd.noblenet-sealer'
	),
	'nnw' => array(
		'name' => 'NobleNet Web',
		'type' => 'application/vnd.noblenet-web'
	),
	'rpst' => array(
		'name' => 'Nokia Radio Application - Preset',
		'type' => 'application/vnd.nokia.radio-preset'
	),
	'rpss' => array(
		'name' => 'Nokia Radio Application - Preset',
		'type' => 'application/vnd.nokia.radio-presets'
	),
	'n3' => array(
		'name' => 'Notation3',
		'type' => 'text/n3'
	),
	'edm' => array(
		'name' => 'Novadigm\'s RADIA and EDM products',
		'type' => 'application/vnd.novadigm.edm'
	),
	'edx' => array(
		'name' => 'Novadigm\'s RADIA and EDM products',
		'type' => 'application/vnd.novadigm.edx'
	),
	'ext' => array(
		'name' => 'Novadigm\'s RADIA and EDM products',
		'type' => 'application/vnd.novadigm.ext'
	),
	'gph' => array(
		'name' => 'NpGraphIt',
		'type' => 'application/vnd.flographit'
	),
	'ecelp4800' => array(
		'name' => 'Nuera ECELP 4800',
		'type' => 'audio/vnd.nuera.ecelp4800'
	),
	'ecelp7470' => array(
		'name' => 'Nuera ECELP 7470',
		'type' => 'audio/vnd.nuera.ecelp7470'
	),
	'ecelp9600' => array(
		'name' => 'Nuera ECELP 9600',
		'type' => 'audio/vnd.nuera.ecelp9600'
	),
	'oda' => array(
		'name' => 'Office Document Architecture',
		'type' => 'application/oda'
	),
	'ogg' => array(
		'name' => 'Ogg',
		'type' => 'application/ogg'
	),
	'ogx' => array(
		'name' => 'Ogg',
		'type' => 'application/ogg'
	),
	'oga' => array(
		'name' => 'Ogg Audio',
		'type' => 'audio/ogg'
	),
	'ogv' => array(
		'name' => 'Ogg Video',
		'type' => 'video/ogg'
	),
	'dd2' => array(
		'name' => 'OMA Download Agents',
		'type' => 'application/vnd.oma.dd2+xml'
	),
	'oth' => array(
		'name' => 'Open Document Text Web',
		'type' => 'application/vnd.oasis.opendocument.text-web'
	),
	'opf' => array(
		'name' => 'Open eBook Publication Structure',
		'type' => 'application/oebps-package+xml'
	),
	'qbo' => array(
		'name' => 'Open Financial Exchange',
		'type' => 'application/vnd.intu.qbo'
	),
	'oxt' => array(
		'name' => 'Open Office Extension',
		'type' => 'application/vnd.openofficeorg.extension'
	),
	'osf' => array(
		'name' => 'Open Score Format',
		'type' => 'application/vnd.yamaha.openscoreformat'
	),
	'weba' => array(
		'name' => 'Open Web Media Project - Audio',
		'type' => 'audio/webm'
	),
	'webm' => array(
		'name' => 'Open Web Media Project - Video',
		'type' => 'video/webm'
	),
	'odc' => array(
		'name' => 'OpenDocument Chart',
		'type' => 'application/vnd.oasis.opendocument.chart'
	),
	'otc' => array(
		'name' => 'OpenDocument Chart Template',
		'type' => 'application/vnd.oasis.opendocument.chart-template'
	),
	'odb' => array(
		'name' => 'OpenDocument Database',
		'type' => 'application/vnd.oasis.opendocument.database'
	),
	'odf' => array(
		'name' => 'OpenDocument Formula',
		'type' => 'application/vnd.oasis.opendocument.formula'
	),
	'odft' => array(
		'name' => 'OpenDocument Formula Template',
		'type' => 'application/vnd.oasis.opendocument.formula-template'
	),
	'odg' => array(
		'name' => 'OpenDocument Graphics',
		'type' => 'application/vnd.oasis.opendocument.graphics'
	),
	'otg' => array(
		'name' => 'OpenDocument Graphics Template',
		'type' => 'application/vnd.oasis.opendocument.graphics-template'
	),
	'odi' => array(
		'name' => 'OpenDocument Image',
		'type' => 'application/vnd.oasis.opendocument.image'
	),
	'oti' => array(
		'name' => 'OpenDocument Image Template',
		'type' => 'application/vnd.oasis.opendocument.image-template'
	),
	'odp' => array(
		'name' => 'OpenDocument Presentation',
		'type' => 'application/vnd.oasis.opendocument.presentation'
	),
	'otp' => array(
		'name' => 'OpenDocument Presentation Template',
		'type' => 'application/vnd.oasis.opendocument.presentation-template'
	),
	'ods' => array(
		'name' => 'OpenDocument Spreadsheet',
		'type' => 'application/vnd.oasis.opendocument.spreadsheet'
	),
	'ots' => array(
		'name' => 'OpenDocument Spreadsheet Template',
		'type' => 'application/vnd.oasis.opendocument.spreadsheet-template'
	),
	'odt' => array(
		'name' => 'OpenDocument Text',
		'type' => 'application/vnd.oasis.opendocument.text'
	),
	'odm' => array(
		'name' => 'OpenDocument Text Master',
		'type' => 'application/vnd.oasis.opendocument.text-master'
	),
	'ott' => array(
		'name' => 'OpenDocument Text Template',
		'type' => 'application/vnd.oasis.opendocument.text-template'
	),
	'ktx' => array(
		'name' => 'OpenGL Textures (KTX)',
		'type' => 'image/ktx'
	),
	'sxc' => array(
		'name' => 'OpenOffice - Calc (Spreadsheet)',
		'type' => 'application/vnd.sun.xml.calc'
	),
	'stc' => array(
		'name' => 'OpenOffice - Calc Template (Spreadsheet)',
		'type' => 'application/vnd.sun.xml.calc.template'
	),
	'sxd' => array(
		'name' => 'OpenOffice - Draw (Graphics)',
		'type' => 'application/vnd.sun.xml.draw'
	),
	'std' => array(
		'name' => 'OpenOffice - Draw Template (Graphics)',
		'type' => 'application/vnd.sun.xml.draw.template'
	),
	'sxi' => array(
		'name' => 'OpenOffice - Impress (Presentation)',
		'type' => 'application/vnd.sun.xml.impress'
	),
	'sti' => array(
		'name' => 'OpenOffice - Impress Template (Presentation)',
		'type' => 'application/vnd.sun.xml.impress.template'
	),
	'sxm' => array(
		'name' => 'OpenOffice - Math (Formula)',
		'type' => 'application/vnd.sun.xml.math'
	),
	'sxw' => array(
		'name' => 'OpenOffice - Writer (Text - HTML)',
		'type' => 'application/vnd.sun.xml.writer'
	),
	'sxg' => array(
		'name' => 'OpenOffice - Writer (Text - HTML)',
		'type' => 'application/vnd.sun.xml.writer.global'
	),
	'stw' => array(
		'name' => 'OpenOffice - Writer Template (Text - HTML)',
		'type' => 'application/vnd.sun.xml.writer.template'
	),
	'otf' => array(
		'name' => 'OpenType Font File',
		'type' => 'application/x-font-otf'
	),
	'osfpvg' => array(
		'name' => 'OSFPVG',
		'type' => 'application/vnd.yamaha.openscoreformat.osfpvg+xml'
	),
	'psd' => array(
		'name' => 'Photoshop Document',
		'type' => 'image/vnd.adobe.photoshop'
	),
	'prf' => array(
		'name' => 'PICSRules',
		'type' => 'application/pics-rules'
	),
	'pic' => array(
		'name' => 'PICT Image',
		'type' => 'image/x-pict'
	),
	'pnm' => array(
		'name' => 'Portable Anymap Image',
		'type' => 'image/x-portable-anymap'
	),
	'pbm' => array(
		'name' => 'Portable Bitmap Format',
		'type' => 'image/x-portable-bitmap'
	),
	'pcf' => array(
		'name' => 'Portable Compiled Format',
		'type' => 'application/x-font-pcf'
	),
	'pfr' => array(
		'name' => 'Portable Font Resource',
		'type' => 'application/font-tdpfr'
	),
	'pgn' => array(
		'name' => 'Portable Game Notation (Chess Games)',
		'type' => 'application/x-chess-pgn'
	),
	'pgm' => array(
		'name' => 'Portable Graymap Format',
		'type' => 'image/x-portable-graymap'
	),
	'png' => array(
		'name' => 'Portable Network Graphics (PNG)',
		'type' => 'image/png'
	),
	'ppm' => array(
		'name' => 'Portable Pixmap Format',
		'type' => 'image/x-portable-pixmap'
	),
	'pskcxml' => array(
		'name' => 'Portable Symmetric Key Container',
		'type' => 'application/pskc+xml'
	),
	'pml' => array(
		'name' => 'PosML',
		'type' => 'application/vnd.ctc-posml'
	),
	'ai' => array(
		'name' => 'PostScript',
		'type' => 'application/postscript'
	),
	'pfa' => array(
		'name' => 'PostScript Fonts',
		'type' => 'application/x-font-type1'
	),
	'ptid' => array(
		'name' => 'Princeton Video Image',
		'type' => 'application/vnd.pvi.ptid1'
	),
	'pls' => array(
		'name' => 'Pronunciation Lexicon Specification',
		'type' => 'application/pls+xml'
	),
	'properties' => array(
		'name' => 'Properties',
		'type' => 'application/properties'
	),
	'str' => array(
		'name' => 'Proprietary P&G Standard Reporting System',
		'type' => 'application/vnd.pg.format'
	),
	'ei6' => array(
		'name' => 'Proprietary P&G Standard Reporting System',
		'type' => 'application/vnd.pg.osasli'
	),
	'dsc' => array(
		'name' => 'PRS Lines Tag',
		'type' => 'text/prs.lines.tag'
	),
	'psf' => array(
		'name' => 'PSF Fonts',
		'type' => 'application/x-font-linux-psf'
	),
	'qps' => array(
		'name' => 'PubliShare Objects',
		'type' => 'application/vnd.publishare-delta-tree'
	),
	'qxd' => array(
		'name' => 'QuarkXpress',
		'type' => 'application/vnd.quark.quarkxpress'
	),
	'esf' => array(
		'name' => 'QUASS Stream Player',
		'type' => 'application/vnd.epson.esf'
	),
	'msf' => array(
		'name' => 'QUASS Stream Player',
		'type' => 'application/vnd.epson.msf'
	),
	'ssf' => array(
		'name' => 'QUASS Stream Player',
		'type' => 'application/vnd.epson.ssf'
	),
	'qam' => array(
		'name' => 'QuickAnime Player',
		'type' => 'application/vnd.epson.quickanime'
	),
	'qfx' => array(
		'name' => 'Quicken',
		'type' => 'application/vnd.intu.qfx'
	),
	'qt' => array(
		'name' => 'Quicktime Video',
		'type' => 'video/quicktime'
	),
	'rar' => array(
		'name' => 'RAR Archive',
		'type' => 'application/x-rar-compressed'
	),
	'ram' => array(
		'name' => 'Real Audio Sound',
		'type' => 'audio/x-pn-realaudio'
	),
	'rmp' => array(
		'name' => 'Real Audio Sound',
		'type' => 'audio/x-pn-realaudio-plugin'
	),
	'rsd' => array(
		'name' => 'Really Simple Discovery',
		'type' => 'application/rsd+xml'
	),
	'rm' => array(
		'name' => 'RealMedia',
		'type' => 'application/vnd.rn-realmedia'
	),
	'bed' => array(
		'name' => 'RealVNC',
		'type' => 'application/vnd.realvnc.bed'
	),
	'mxl' => array(
		'name' => 'Recordare Applications',
		'type' => 'application/vnd.recordare.musicxml'
	),
	'musicxml' => array(
		'name' => 'Recordare Applications',
		'type' => 'application/vnd.recordare.musicxml+xml'
	),
	'rnc' => array(
		'name' => 'Relax NG Compact Syntax',
		'type' => 'application/relax-ng-compact-syntax'
	),
	'rdz' => array(
		'name' => 'RemoteDocs R-Viewer',
		'type' => 'application/vnd.data-vision.rdz'
	),
	'rdf' => array(
		'name' => 'Resource Description Framework',
		'type' => 'application/rdf+xml'
	),
	'rp9' => array(
		'name' => 'RetroPlatform Player',
		'type' => 'application/vnd.cloanto.rp9'
	),
	'jisp' => array(
		'name' => 'RhymBox',
		'type' => 'application/vnd.jisp'
	),
	'rtf' => array(
		'name' => 'Rich Text Format',
		'type' => 'application/rtf'
	),
	'rtx' => array(
		'name' => 'Rich Text Format (RTF)',
		'type' => 'text/richtext'
	),
	'link66' => array(
		'name' => 'ROUTE 66 Location Based Services',
		'type' => 'application/vnd.route66.link66+xml'
	),
	'".rss, .xml"' => array(
		'name' => 'RSS - Really Simple Syndication',
		'type' => 'application/rss+xml'
	),
	'shf' => array(
		'name' => 'S Hexdump Format',
		'type' => 'application/shf+xml'
	),
	'st' => array(
		'name' => 'SailingTracker',
		'type' => 'application/vnd.sailingtracker.track'
	),
	'svg' => array(
		'name' => 'Scalable Vector Graphics (SVG)',
		'type' => 'image/svg+xml'
	),
	'movie' => array(
		'name' => 'SGI Movie',
		'type' => 'video/x-sgi-movie'
	),
	'mov' => array(
		'name' => 'QuickTime',
		'type' => 'video/quicktime'
	),
	'rgb' => array(
		'name' => 'Silicon Graphics RGB Bitmap',
		'type' => 'image/x-rgb'
	),
	'slt' => array(
		'name' => 'SimpleAnimeLite Player',
		'type' => 'application/vnd.epson.salt'
	),
	'aso' => array(
		'name' => 'Simply Accounting',
		'type' => 'application/vnd.accpac.simply.aso'
	),
	'imp' => array(
		'name' => 'Simply Accounting - Data Import',
		'type' => 'application/vnd.accpac.simply.imp'
	),
	'twd' => array(
		'name' => 'SimTech MindMapper',
		'type' => 'application/vnd.simtech-mindmapper'
	),
	'csp' => array(
		'name' => 'Sixth Floor Media - CommonSpace',
		'type' => 'application/vnd.commonspace'
	),
	'saf' => array(
		'name' => 'SMAF Audio',
		'type' => 'application/vnd.yamaha.smaf-audio'
	),
	'mmf' => array(
		'name' => 'SMAF File',
		'type' => 'application/vnd.smaf'
	),
	'spf' => array(
		'name' => 'SMAF Phrase',
		'type' => 'application/vnd.yamaha.smaf-phrase'
	),
	'teacher' => array(
		'name' => 'SMART Technologies Apps',
		'type' => 'application/vnd.smart.teacher'
	),
	'svd' => array(
		'name' => 'SourceView Document',
		'type' => 'application/vnd.svd'
	),
	'rq' => array(
		'name' => 'SPARQL - Query',
		'type' => 'application/sparql-query'
	),
	'srx' => array(
		'name' => 'SPARQL - Results',
		'type' => 'application/sparql-results+xml'
	),
	'gram' => array(
		'name' => 'Speech Recognition Grammar Specification',
		'type' => 'application/srgs'
	),
	'grxml' => array(
		'name' => 'Speech Recognition Grammar Specification - XML',
		'type' => 'application/srgs+xml'
	),
	'ssml' => array(
		'name' => 'Speech Synthesis Markup Language',
		'type' => 'application/ssml+xml'
	),
	'skp' => array(
		'name' => 'SSEYO Koan Play File',
		'type' => 'application/vnd.koan'
	),
	'sgml' => array(
		'name' => 'Standard Generalized Markup Language (SGML)',
		'type' => 'text/sgml'
	),
	'sdc' => array(
		'name' => 'StarOffice - Calc',
		'type' => 'application/vnd.stardivision.calc'
	),
	'sda' => array(
		'name' => 'StarOffice - Draw',
		'type' => 'application/vnd.stardivision.draw'
	),
	'sdd' => array(
		'name' => 'StarOffice - Impress',
		'type' => 'application/vnd.stardivision.impress'
	),
	'smf' => array(
		'name' => 'StarOffice - Math',
		'type' => 'application/vnd.stardivision.math'
	),
	'sdw' => array(
		'name' => 'StarOffice - Writer',
		'type' => 'application/vnd.stardivision.writer'
	),
	'sgl' => array(
		'name' => 'StarOffice - Writer  (Global)',
		'type' => 'application/vnd.stardivision.writer-global'
	),
	'sm' => array(
		'name' => 'StepMania',
		'type' => 'application/vnd.stepmania.stepchart'
	),
	'sit' => array(
		'name' => 'Stuffit Archive',
		'type' => 'application/x-stuffit'
	),
	'sitx' => array(
		'name' => 'Stuffit Archive',
		'type' => 'application/x-stuffitx'
	),
	'sdkm' => array(
		'name' => 'SudokuMagic',
		'type' => 'application/vnd.solent.sdkm+xml'
	),
	'xo' => array(
		'name' => 'Sugar Linux Application Bundle',
		'type' => 'application/vnd.olpc-sugar'
	),
	'au' => array(
		'name' => 'Sun Audio - Au file format',
		'type' => 'audio/basic'
	),
	'wqd' => array(
		'name' => 'SundaHus WQ',
		'type' => 'application/vnd.wqd'
	),
	'sis' => array(
		'name' => 'Symbian Install Package',
		'type' => 'application/vnd.symbian.install'
	),
	'smi' => array(
		'name' => 'Synchronized Multimedia Integration Language',
		'type' => 'application/smil+xml'
	),
	'xsm' => array(
		'name' => 'SyncML',
		'type' => 'application/vnd.syncml+xml'
	),
	'bdm' => array(
		'name' => 'SyncML - Device Management',
		'type' => 'application/vnd.syncml.dm+wbxml'
	),
	'xdm' => array(
		'name' => 'SyncML - Device Management',
		'type' => 'application/vnd.syncml.dm+xml'
	),
	'sv4cpio' => array(
		'name' => 'System V Release 4 CPIO Archive',
		'type' => 'application/x-sv4cpio'
	),
	'sv4crc' => array(
		'name' => 'System V Release 4 CPIO Checksum Data',
		'type' => 'application/x-sv4crc'
	),
	'sbml' => array(
		'name' => 'Systems Biology Markup Language',
		'type' => 'application/sbml+xml'
	),
	'tsv' => array(
		'name' => 'Tab Seperated Values',
		'type' => 'text/tab-separated-values'
	),
	'ts' => array(
		'name' => 'iPhone Segment',
		'type' => 'video/MP2T'
	),
	'tiff' => array(
		'name' => 'Tagged Image File Format',
		'type' => 'image/tiff'
	),
	'txt' => array(
		'name' => 'Text File',
		'type' => 'text/plain'
	),
	'ttf' => array(
		'name' => 'TrueType Font',
		'type' => 'application/x-font-ttf'
	),
	'ttl' => array(
		'name' => 'Turtle (Terse RDF Triple Language)',
		'type' => 'text/turtle'
	),
	'uoml' => array(
		'name' => 'Unique Object Markup Language',
		'type' => 'application/vnd.uoml+xml'
	),
	'unityweb' => array(
		'name' => 'Unity 3d',
		'type' => 'application/vnd.unity'
	),
	'ufd' => array(
		'name' => 'Universal Forms Description Language',
		'type' => 'application/vnd.ufdl'
	),
	'uri' => array(
		'name' => 'URI Resolution Services',
		'type' => 'text/uri-list'
	),
	'utz' => array(
		'name' => 'User Interface Quartz - Theme (Symbian)',
		'type' => 'application/vnd.uiq.theme'
	),
	'ustar' => array(
		'name' => 'Ustar (Uniform Standard Tape Archive)',
		'type' => 'application/x-ustar'
	),
	'uu' => array(
		'name' => 'UUEncode',
		'type' => 'text/x-uuencode'
	),
	'vcs' => array(
		'name' => 'vCalendar',
		'type' => 'text/x-vcalendar'
	),
	'vcf' => array(
		'name' => 'vCard',
		'type' => 'text/x-vcard'
	),
	'vcd' => array(
		'name' => 'Video CD',
		'type' => 'application/x-cdlink'
	),
	'vsf' => array(
		'name' => 'Viewport+',
		'type' => 'application/vnd.vsf'
	),
	'wrl' => array(
		'name' => 'Virtual Reality Modeling Language',
		'type' => 'model/vrml'
	),
	'vcx' => array(
		'name' => 'VirtualCatalog',
		'type' => 'application/vnd.vcx'
	),
	'vtt' => array(
		'name' => 'WebVTT',
		'type' => 'text/vtt'
	),
	'mts' => array(
		'name' => 'Virtue MTS',
		'type' => 'model/vnd.mts'
	),
	'vtu' => array(
		'name' => 'Virtue VTU',
		'type' => 'model/vnd.vtu'
	),
	'vis' => array(
		'name' => 'Visionary',
		'type' => 'application/vnd.visionary'
	),
	'viv' => array(
		'name' => 'Vivo',
		'type' => 'video/vnd.vivo'
	),
	'ccxml' => array(
		'name' => 'Voice Browser Call Control',
		'type' => '"application/ccxml+xml,"'
	),
	'vxml' => array(
		'name' => 'VoiceXML',
		'type' => 'application/voicexml+xml'
	),
	'src' => array(
		'name' => 'WAIS Source',
		'type' => 'application/x-wais-source'
	),
	'wbxml' => array(
		'name' => 'WAP Binary XML (WBXML)',
		'type' => 'application/vnd.wap.wbxml'
	),
	'wbmp' => array(
		'name' => 'WAP Bitamp (WBMP)',
		'type' => 'image/vnd.wap.wbmp'
	),
	'wav' => array(
		'name' => 'Waveform Audio File Format (WAV)',
		'type' => 'audio/x-wav'
	),
	'davmount' => array(
		'name' => 'Web Distributed Authoring and Versioning',
		'type' => 'application/davmount+xml'
	),
	'woff' => array(
		'name' => 'Web Open Font Format',
		'type' => 'application/x-font-woff'
	),
	'woff2' => array(
		'name' => 'Web Open Font Format',
		'type' => 'application/x-font-woff'
	),
	'wspolicy' => array(
		'name' => 'Web Services Policy',
		'type' => 'application/wspolicy+xml'
	),
	'webp' => array(
		'name' => 'WebP Image',
		'type' => 'image/webp'
	),
	'wtb' => array(
		'name' => 'WebTurbo',
		'type' => 'application/vnd.webturbo'
	),
	'wgt' => array(
		'name' => 'Widget Packaging and XML Configuration',
		'type' => 'application/widget'
	),
	'hlp' => array(
		'name' => 'WinHelp',
		'type' => 'application/winhlp'
	),
	'htc' => array(
		'name' => 'HTC',
		'type' => 'application/htc'
	),
	'info' => array(
		'name' => 'Info',
		'type' => 'plain/text'
	),
	'wml' => array(
		'name' => 'Wireless Markup Language (WML)',
		'type' => 'text/vnd.wap.wml'
	),
	'wmls' => array(
		'name' => 'Wireless Markup Language Script (WMLScript)',
		'type' => 'text/vnd.wap.wmlscript'
	),
	'wmlsc' => array(
		'name' => 'WMLScript',
		'type' => 'application/vnd.wap.wmlscriptc'
	),
	'wpd' => array(
		'name' => 'Wordperfect',
		'type' => 'application/vnd.wordperfect'
	),
	'stf' => array(
		'name' => 'Worldtalk',
		'type' => 'application/vnd.wt.stf'
	),
	'wsdl' => array(
		'name' => 'WSDL - Web Services Description Language',
		'type' => 'application/wsdl+xml'
	),
	'xbm' => array(
		'name' => 'X BitMap',
		'type' => 'image/x-xbitmap'
	),
	'xpm' => array(
		'name' => 'X PixMap',
		'type' => 'image/x-xpixmap'
	),
	'xwd' => array(
		'name' => 'X Window Dump',
		'type' => 'image/x-xwindowdump'
	),
	'db' => array(
		'name' => 'Microsoft Access Database',
		'type' => 'application/msaccess'
	),
	'der' => array(
		'name' => 'X.509 Certificate',
		'type' => 'application/x-x509-ca-cert'
	),
	'fig' => array(
		'name' => 'Xfig',
		'type' => 'application/x-xfig'
	),
	'xhtml' => array(
		'name' => 'XHTML - The Extensible HyperText Markup Language',
		'type' => 'application/xhtml+xml'
	),
	'xml' => array(
		'name' => 'XML - Extensible Markup Language',
		'type' => 'application/xml'
	),
	'xsd' => array(
		'name' => 'XML - Extensible Markup Language',
		'type' => 'application/xml'
	),
	'xdf' => array(
		'name' => 'XML Configuration Access Protocol - XCAP Diff',
		'type' => 'application/xcap-diff+xml'
	),
	'xenc' => array(
		'name' => 'XML Encryption Syntax and Processing',
		'type' => 'application/xenc+xml'
	),
	'xer' => array(
		'name' => 'XML Patch Framework',
		'type' => 'application/patch-ops-error+xml'
	),
	'rl' => array(
		'name' => 'XML Resource Lists',
		'type' => 'application/resource-lists+xml'
	),
	'rs' => array(
		'name' => 'XML Resource Lists',
		'type' => 'application/rls-services+xml'
	),
	'rld' => array(
		'name' => 'XML Resource Lists Diff',
		'type' => 'application/resource-lists-diff+xml'
	),
	'xslt' => array(
		'name' => 'XML Transformations',
		'type' => 'application/xslt+xml'
	),
	'xop' => array(
		'name' => 'XML-Binary Optimized Packaging',
		'type' => 'application/xop+xml'
	),
	'xpi' => array(
		'name' => 'XPInstall - Mozilla',
		'type' => 'application/x-xpinstall'
	),
	'xspf' => array(
		'name' => 'XSPF - XML Shareable Playlist Format',
		'type' => 'application/xspf+xml'
	),
	'xul' => array(
		'name' => 'XUL - XML User Interface Language',
		'type' => 'application/vnd.mozilla.xul+xml'
	),
	'xyz' => array(
		'name' => 'XYZ File Format',
		'type' => 'chemical/x-xyz'
	),
	'zir' => array(
		'name' => 'Z.U.L. Geometry',
		'type' => 'application/vnd.zul'
	),
	'zmm' => array(
		'name' => 'ZVUE Media Manager',
		'type' => 'application/vnd.handheld-entertainment+xml'
	),
	'zaz' => array(
		'name' => 'Zzazz Deck',
		'type' => 'application/vnd.zzazz.deck+xml'
	),
	'cst' => array(
		'name' => 'cst File',
		'type' => 'application/cst'
	),
	'crs' => array(
		'name' => 'crs File',
		'type' => 'application/crs'
	),
	'des'=> array(
		'name' => 'des File',
		'type' => 'application/des'
	),
	'srt'=> array(
		'name' => 'srt File',
		'type' => 'application/srt'
	),

);