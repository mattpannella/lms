<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class SplmsHelper {
	// Get Orders
	public static function getOrders() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('type') . '=' . (int)0,
			'('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').')'
		);
		$query->select('COUNT(id)');
		$query->from($db->quoteName('cctransactions'));
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadResult();
		return $results;
	}

	// Get Orders
	public static function getCourses() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(splms_course_id)');
		$query->from($db->quoteName('#__splms_courses'));
		$query->where($db->quoteName('enabled')." = 1");
		$query->order('ordering DESC'); 
		$db->setQuery($query);
		$results = $db->loadResult();

		return $results;
	}

	// Get Orders
	public static function getLessons() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('COUNT(splms_lesson_id)');	 
		$query->from($db->quoteName('#__splms_lessons'));
		$query->where($db->quoteName('enabled')." = 1");
		$query->order('ordering DESC'); 
		$db->setQuery($query); 
		$results = $db->loadResult();

		return $results;
	}

	// Get Orders
	public static function getUsers() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);	 
		$query->select('COUNT(id)');
		$query->from($db->quoteName('#__users'));
		$query->where($db->quoteName('block')." = 0");
		$db->setQuery($query);
		$results = $db->loadResult();

		return $results;
	}


	//Get total sales
	public static function getTotalSales() {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('type') . '=' . (int)0,
			'('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').')'
		);
		$query->select('SUM(amount)');
		$query->from($db->quoteName('cctransactions'));
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadResult();

		return round($results,2);
	}

	//Get total sales by day
	public static function getSales($day, $month, $year) {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('type') . '=' . (int)0,
			'('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').')'
		);
		$query->select('SUM(amount)');
		$query->from($db->quoteName('cctransactions'));
		$query->where('DAY(date) = ' . $day);
		$query->where('MONTH(date) = ' . $month);
		$query->where('YEAR(date) = ' . $year);
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadResult();

		return $results;
	}

	//Orders List
	public static function getOrdersList() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('type') . '=' . (int)0,
			'('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').')'
		);
		$query->select('a.id AS splms_order_id, a.item_id AS splms_course_id, a.date AS created_on, a.amount AS order_payment_price, b.title, b.splms_course_id');
		$query->from($db->quoteName('cctransactions', 'a'));
		$query->join('LEFT', $db->quoteName('#__splms_courses', 'b') . ' ON (' . $db->quoteName('a.item_id') . ' = ' . $db->quoteName('b.splms_course_id') . ')');
		$query->where($conditions);
		$query->setLimit('5');
		$query->order('a.date DESC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		return $results;
	}

	public static function getCoursesList() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
			 
		$query->select($db->quoteName(array('splms_course_id', 'title', 'created_on', 'price')));
		$query->from($db->quoteName('#__splms_courses'));
		$query->where($db->quoteName('enabled')." = 1");
		$query->setLimit('5');
		$query->order('ordering DESC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		return $results;
	}

}
