<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

$doc = JFactory::getDocument();
$doc->addStylesheet( JURI::root(true) . '/administrator/components/com_splms/assets/css/splms.css' );

// Load FOF
include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
	JError::raiseError ('500', 'FOF is not installed');
	
	return;
}

FOFDispatcher::getTmpInstance('com_splms')->dispatch();