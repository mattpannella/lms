<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsTableCertificate extends FOFTable
{
	public function check() {

		$result = true;

		//You can also use $stamp = strtotime ("now"); But I think date("Ymdhis") is easier to understand.
		$today = JFactory::getDate();
		$today = JHtml::_('date', $today, 'Ymd');
		$rand = strtoupper(substr(uniqid(sha1(time())),0,4));
		$uniqueno = $rand .$today;

		if(empty($this->certificate_no)) {
			// Auto-fetch a alias
			$this->certificate_no = $uniqueno;
		} else {
			// if has certificate no
			$this->certificate_no = $this->certificate_no;
		}


		return $result;
	}

}