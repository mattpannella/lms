<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsTableCourse extends FOFTable
{
	public function check() {

		$result = true;

		// if(empty($this->alias)) {
		// 	// Auto-fetch a alias
		// 	$this->alias = JFilterOutput::stringURLSafe($this->title);
		// } else {
		// 	// Make sure nobody adds crap characters to the alias
		// 	$this->alias =JFilterOutput::stringURLSafe($this->alias);
		// }

		// $existingAlias = FOFModel::getTmpInstance('Courses','SplmsModel')
		// 	->alias($this->alias)
		// 	->getList(true);

		// if(!empty($existingAlias)) {
		// 	$count = 0;
		// 	$k = $this->getKeyName();
		// 	foreach($existingAlias as $item) {
		// 		if($item->$k != $this->$k) $count++;
		// 	}
		// 	if($count) {
		// 		$this->setError(JText::_('COM_SPLMS_ALIAS_ERR_SLUGUNIQUE'));
		// 		$result = false;
		// 	}
		// }

		//Generate Thumbnails
		if($result) {
			jimport('joomla.application.component.helper');
			$params = JComponentHelper::getParams('com_splms');

			$thumb = $params->get('course_thumbnail', '480X300');
			$thumb_small = $params->get('course_thumbnail_small', '100X60');

			if(!isset($this->image) && $this->image) {
				jimport( 'joomla.filesystem.file' );
				jimport( 'joomla.filesystem.folder' );
				jimport( 'joomla.image.image' );
				$image = JPATH_ROOT . '/' . $this->image;
				$sizes = array($thumb, $thumb_small);
				$image = new JImage($image);
				$image->createThumbs($sizes, 5);
			}
		}

		return $result;
	}

}