<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die();

class SplmsTableEvent extends FOFTable
{
	public function check() {

		$result = true;

		if (is_array($this->splms_speaker_id))
		{
			if (!empty($this->splms_speaker_id))
			{
				$this->splms_speaker_id = json_encode($this->splms_speaker_id);
			}
		}
		if (is_null($this->splms_speaker_id) || empty($this->splms_speaker_id))
		{
			$this->splms_speaker_id = '';
		}


		// if(empty($this->alias)) {
		// 	// Auto-fetch a alias
		// 	$this->alias = JFilterOutput::stringURLSafe($this->title);
		// } else {
		// 	// Make sure nobody adds crap characters to the alias
		// 	$this->alias =JFilterOutput::stringURLSafe($this->alias);
		// }

		// $existingAlias = FOFModel::getTmpInstance('Events','SplmsModel')
		// 	->alias($this->alias)
		// 	->getList(true);

		// if(!empty($existingAlias)) {
		// 	$count = 0;
		// 	$k = $this->getKeyName();
		// 	foreach($existingAlias as $item) {
		// 		if($item->$k != $this->$k) $count++;
		// 	}
		// 	if($count) {
		// 		$this->setError(JText::_('COM_SPLMS_ALIAS_ERR_SLUGUNIQUE'));
		// 		$result = false;
		// 	}
		// }

		//Generate Thumbnails
		if($result && $this->image !='') {

			jimport('joomla.application.component.helper');
			$params = JComponentHelper::getParams('com_splms');

			$thumb = $params->get('event_thumbnail', '480X300');
			$thumb_small = $params->get('event_thumbnail_small', '100X60');

			if(!is_null($this->image)) {

				jimport( 'joomla.filesystem.file' );
				jimport( 'joomla.filesystem.folder' );
				jimport( 'joomla.image.image' );
				$image = JPATH_ROOT . '/' . $this->image;
				$sizes = array($thumb, $thumb_small);
				$image = new JImage($image);
				$image->createThumbs($sizes, 5);
			}
		}

		return $result;
	}

	public function onAfterLoad(&$result) {
		// Convert plan to an array
		if(!is_array($this->splms_speaker_id)) {
			if(!empty($this->splms_speaker_id)) {
				$this->splms_speaker_id = json_decode($this->splms_speaker_id, true);
			}
		}
		if(is_null($this->splms_speaker_id) || empty($this->splms_speaker_id)) {
			$this->splms_speaker_id = array();
		}

		return parent::onAfterLoad($result);
	}


}