<?php
    echo AxsLoader::loadKendo();

    $db = JFactory::getDBO();
    $query = "SELECT lang_code, title_native AS title FROM joom_languages WHERE published = 1";
    $db->setQuery($query);
    $languages = $db->loadObjectList();

    $current_site_language = AxsLanguage::getCurrentLanguage()->get('tag');

?>

<link href="components/com_dashboard/assets/css/data_box.css" rel="stylesheet" type="text/css" />
<style>
    #student_list .control-label {
        display:none;
    }
    #student_list .controls {
        margin-left: 0px;
    }
    .fa {
        margin-right: 5px;
    }
    a.student-link:hover {
        text-decoration: none;
    }
    .stats_bar {
        border-radius: 5px;
        margin-bottom: 20px;
    }
</style>

<div class='row'>
    <div class="col-md-3 totals-widget p-2">
        <div class='stats_bar shadowed' style="background-color: #155eef;">
            <div class="totals-inner">
                <div class="totals-icon" style="margin-top: 10px;"><span class="fa fa-users"></span></div>
                <div class="totals-data" id="totalStudents">0</div>
                <div class="totals-footer">
                    Total Students
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 totals-widget p-2">
        <div class='stats_bar shadowed' style="background-color: #b42318;">
            <div class="totals-inner">
                <div class="totals-icon" style="margin-top: 10px;"><span class="fa fa-trophy"></span></div>
                <div class="totals-data" id="averageCompletion">0</div>
                <div class="totals-footer">
                    Average Completion Rate
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 totals-widget p-2">
        <div class='stats_bar shadowed' style="background-color: #b54708;">
            <div class="totals-inner">
                <div class="totals-icon" style="margin-top: 10px;"><span class="fa fa-money"></span></div>
                <div class="totals-data" id="totalRevenue">0</div>
                <div class="totals-footer">
                    Total Revenue
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 totals-widget p-2">
        <div class='stats_bar shadowed' style="background-color: #027a48;">
            <div class="totals-inner">
                <div class="totals-icon" style="margin-top: 10px;"><span class="fa fa-comments"></span></div>
                <div class="totals-data" id="totalDiscussion"><?php if($totalComments) { echo $totalComments; } else { echo '0'; } ?></div>
                <div class="totals-footer">
                    Total Discussions
                </div>
            </div>
        </div>
    </div>
</div>

<script id="selectLanguage" type="text/x-kendo-tmpl">
    <select id="studentListLanguageSelector" style="padding:2px;">
        <?php foreach ($languages as $language) : ?>
            <option
                value="<?php echo $language->lang_code;?>"
            <?php
                if ($language->lang_code == $current_site_language) {
                    echo " selected ";
                }
            ?>>
                <?php echo $language->title; ?>
            </option>
        <?php endforeach; ?>
    </select>
</script>

<script id="profileTemplate" type="text/x-kendo-tmpl">
    <span class='student-image' style='padding: 0px'>
        <span style='margin: 0px 5px;'>
            <img src='#=profile.photo#' style='width: 50px;' class='img-circle'>
        </span>
    </span>
    <a class='student-link btn btn-default' target='_blank' href='/my-profile/#=profile.alias#'><i class="fa fa-vcard-o"></i> Profile</a>
</script>

<script id="expandBtn" type="text/x-kendo-tmpl">
    <span class="k-button k-button-icontext collapseAll" style="display: none;"><i class="fa fa-compress"></i> Collapse All</span>
    <span class="k-button k-button-icontext expandAll"><i class="fa fa-expand"></i> Expand All</span>
</script>

<div id="grid"></div>

<script>
    var language = '<?php echo $current_site_language; ?>';
    // This function binds the footer totals with the top box widgets
    function onDataBound() {
        var percentage = jQuery('#average-progress').text();
        var students   = jQuery('#total-students').text();
        var revenue   = jQuery('#total-revenue').text();

        jQuery('#totalStudents').text(students);
        jQuery('#averageCompletion').text(percentage);
        jQuery('#totalRevenue').text(revenue);
    };

    function loadGrid(language) {
        var element = jQuery("#grid").kendoGrid({
            toolbar: [
                {
                    text:"",
                    template: kendo.template(jQuery("#selectLanguage").html())
                },
                "pdf",
                "excel",
                {
                    text:"",
                    template: kendo.template(jQuery("#expandBtn").html())
                }
            ],
            dataSource: {
                transport: {
                    read: {
                        url: "/index.php?option=com_splms&task=lms.getStudents&format=raw&id=<?php echo $params->course_id; ?>&lang="+language,
                        dataType: "json"
                    }
                },
                schema: {
                    model: {
                        fields: {
                            user: {
                                editable: true
                            },
                            profile: {
                                editable: false
                            },
                            name: {
                                editable: false
                            },
                            progress: {
                                type: "number"
                            },
                            amount_paid: {
                                type: "number"
                            },
                            date_started: {
                                type: "date"
                            },
                            date_completed: {
                                type: "date"
                            },
                            comments: {
                                type: "number"
                            }
                        }
                    }
                },
                aggregate: [
                    { field: "progress", aggregate: "average" },
                    { field: "name", aggregate: "count" },
                    { field: "amount_paid", aggregate: "sum" }
                ]
            },
            sortable: true,
            columnMenu: true,
            pageable: true,
            resizable: true,
            filterable: {
                mode: "row"
            },
            pageable: {
                buttonCount: 50,
                pageSize: 50,
                pageSizes: true,
                refresh: true,
                message: {
                    empty: "There are no entries to display"
                }
            },
            groupable: true,
            detailInit: detailInit,
            dataBound: function() {
                this.expandRow(this.tbody.find("tr.k-master-row").first());
                onDataBound();
            },
            columns: [
                {
                    field: "user",
                    title: "ID",
                    sortable: false,
                    filterable: false,
                    width: "20px"
                },
                {
                    field: "profile",
                    title: "Profile",
                    template: kendo.template(jQuery("#profileTemplate").html()),
                    filterable: false,
                    sortable: false,
                    width: "70px"
                },
                {
                    field: "name",
                    title: "Name",
                    width: "70px",
                    filterable: {
                        operators: {
                            string: {
                                contains: "Contains"
                            }
                        }
                    },
                    aggregates: ["count"],
                    footerTemplate: "Total Students: <span id='total-students'>#= kendo.toString(count, 'n0') #</span>"
                },
                {
                    field: "progress",
                    title: "Progress",
                    width: "50px",
                    type: "number",
                    format: "{0:n0}%",
                    aggregates: ["average"],
                    footerTemplate: "Average: <span id='average-progress'>#= kendo.toString(average, 'n0') #%</span>"
                },
                {
                    field: "amount_paid",
                    title: "Paid Amount",
                    width: "50px",
                    type: "number",
                    format: "${0:n2}",
                    template: `#if(amount_paid === null) {
                                    # $#= kendo.toString(0, 'n2') #  #
                                } else {
                                    # $#= kendo.toString(amount_paid, 'n2') #  #
                                }#`,
                    aggregates: ["sum"],
                    footerTemplate: `#if(sum === null) {
                                        # Total Revenue: <span id='total-revenue'>$#= kendo.toString(0, 'n2') #</span>#
                                    } else {
                                        # Total Revenue: <span id='total-revenue'>$#= kendo.toString(sum, 'n2') #</span>#
                                    }#`
                },
                {
                    field: "date_started",
                    title: "Date Started",
                    width: "50px",
                    type: 'date',
                    format: "{0:MM/d/yyyy}",
                    filterable: {
                        ui: "datepicker"
                    }
                },
                {
                    field: "date_completed",
                    title: "Date Completed",
                    width: "50px",
                    type: 'date',
                    format: "{0:MM/d/yyyy}",
                    filterable: {
                        ui: "datepicker"
                    }
                },
                {
                    field: "comments",
                    title: "Comments",
                    width: "50px",
                    type: "number"
                }
            ]
        });
    };

    loadGrid(language);

    jQuery('#studentListLanguageSelector').change( function(e) {

        language = e.originalEvent.target.value;
        console.log(language)

        let courseStudentTable = jQuery("#grid").data("kendoGrid");

        courseStudentTable.dataSource.transport.options.read.url = '/index.php?option=com_splms&task=lms.getStudents&format=raw&id=<?php echo $params->course_id; ?>&lang='+language;

        courseStudentTable.dataSource.read();
    });

    function detailInit(e) {
        jQuery("<div/>").appendTo(e.detailCell).kendoGrid({
            dataSource: {
                transport: {
                    read: {
                        url: "/index.php?option=com_splms&task=lms.getStudentLessons&format=raw&id=<?php echo $params->course_id; ?>&lang="+language,
                        dataType: "json"
                    }
                },
                schema: {
                    model: {
                        fields: {
                            ordering: {
                                type: "number"
                            }
                        }
                    }
                },
                filter: {
                    field: "user",
                    operator: "eq",
                    value: e.data.user
                },
                sort: {
                    field: "ordering",
                    dir: "asc"
                }
            },
            sortable: true,
            columnMenu: true,
            pageable: true,
            resizable: true,
            filterable: {
                mode: "row",
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
            pageable: {
                buttonCount: 25,
                pageSize: 25,
                pageSizes: true,
                refresh: true,
                message: {
                    empty: "There are no entries to display"
                }
            },
            groupable: true,
            columns: [
                {
                    field: "lesson_title",
                    title: "Lesson",
                    width: "400px",
                    filterable: {
                        multi: true
                    }
                },
                {
                    field: "status",
                    title: "Status",
                    width: "100px"

                },
                {
                    field: "status_date",
                    title: "Date",
                    width: "120px",
                    type: 'date',
                    format: "{0:MM/d/yyyy}",
                    filterable: {
                        ui: "datepicker"
                    }
                }
            ]
        });
    }

    jQuery(document).on('click', '.student-image', function() {
        jQuery(this).parent().parent().find(".k-icon").trigger("click");
    });

    jQuery(document).on('click', '.collapseAll', function() {
        jQuery("#grid").find(".k-icon.k-i-collapse").trigger("click");
        jQuery('.collapseAll').hide();
        jQuery('.expandAll').show();
    });

    jQuery(document).on('click', '.expandAll', function() {
        jQuery("#grid").find(".k-icon.k-i-expand").trigger("click");        
        jQuery('.expandAll').hide();
        jQuery('.collapseAll').show();
    });
</script>