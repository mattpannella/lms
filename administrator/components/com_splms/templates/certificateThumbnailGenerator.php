<script type="text/javascript" src="/js/html2canvas.js"></script>
<input name="certificate_thumbnail_temp" id="certificate_thumbnail_temp" type="hidden"/>
<img class="cert-thumb" style="display: none;" width="300"/> <br/>
<span id="certificate-image" style="display:none;"></span>
<span class="btn btn-success create-thumbnail"><i class="fa fa-camera-retro"></i> Create Thumbnail</span>
<a class="btn btn-success download-image" style="display: none;"><i class="fa fa-download"></i> Download</a>

<script>
jQuery(document).ready(function() {
    jQuery(".create-thumbnail").click(function() {        
        html2canvas(jQuery("#certificate"), {
                onrendered: function(canvas) {                
                jQuery('#certificate-image').html("");
                jQuery('#certificate-image').append(canvas);
                var image = canvas.toDataURL("image/png");
                jQuery('#certificate_thumbnail_temp').val(image);
                jQuery('.cert-thumb').attr('src', image);
                jQuery('.download-image').attr('href', image);
                jQuery('.download-image').attr('download','Certificate.png');
                jQuery('.cert-thumb').fadeIn(400);
                jQuery('.download-image').fadeIn(400);
            }
        });
    });
}); 
</script>