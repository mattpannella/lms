<?php
	foreach($items->data as $item) {
		if(!$item->attributes->archived) {
?>

	<div class="bl_card">
		<div class="bl_image_container">
			<img class="bl_image" src="<?php echo AxsScormFactory::getCoursePoster($item->attributes->code); ?>"/>
		</div>
		<div class="bl_content">
			<div class="bl_title">
				<?php echo $item->attributes->title; ?>
			</div>

		</div>
		<div class="bl_attributes">
			<?php //if(in_array("Mobile Friendly",$item->attributes->technology) ) { echo '<span class="fa fa-mobile"></span> Mobile Friendly'; } ?>
		</div>
		<div class="bl_button_container">
			<span 
				class="btn btn-light btn-sm bl_preview" 
				data-bs-toggle="modal" 
				data-bs-target="#previewModal" 
				data-title="<?php echo str_replace('"','', $item->attributes->title); ?>" 
				data-code="<?php echo trim($item->attributes->code); ?>" 
				style="float:left; margin:10px;"
			>
				<i class="icon-screen"></i> Preview
			</span>
			<span 
				class="btn btn-secondary btn-sm bl_select" 
				data-image="<?php echo AxsScormFactory::getCoursePoster($item->attributes->code); ?>" 
				data-title="<?php echo str_replace('"','', $item->attributes->title); ?>" 
				data-code="<?php echo trim($item->attributes->code); ?>" 
				style="float:right; margin:10px;"
			>
				<i class="icon-plus"></i> Select
			</span>
		</div>
		<?php 
		$content_type = $item->attributes->content_type;
		$badgeClass = '';
		if ($content_type == 'Video Lesson') {
			$badgeClass = 'bg-light text-dark';
		} else if ($content_type == 'Video Course') {
			$badgeClass = 'bg-secondary';
		} else if ($content_type == 'eLearning') {
			$badgeClass = 'bg-warning text-dark';
		} else if ($content_type == 'Interactive Video') {
			$badgeClass = 'bg-info';
		} else {
			$badgeClass = 'text-dark';
		}
		?>
		<div title="Content Type: <?php echo $content_type ?>" class="bl_content_type badge <?php echo $badgeClass ?>"><?php echo $content_type ?></div>
	</div>
<?php
		}
	}
?>