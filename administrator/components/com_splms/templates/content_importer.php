
<link type="text/css" rel="stylesheet" href="/administrator/components/com_splms/assets/css/content_importer.css?v=8"/>
<link rel="stylesheet" href="/administrator/components/com_axs/assets/css/offCanvas.css" type="text/css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<?php
	$footerBtnTextLocalized = JText::_('JCANCEL');
	$previewModalBody = "<div id='previewFrame'></div>";
	echo JHtml::_(
		'bootstrap.renderModal',
		'previewModal',
		array(
			'footer' => "<button class='btn btn-danger' type='button' data-bs-dismiss='modal'>$footerBtnTextLocalized</button>",
			'size' => 'lg'
		),
		$previewModalBody
	)
?>
<div class="row ms-2 mb-3">
	<div class="col-auto mt-3">
		<select id="bl_category" class="bl_search_fields" name="bl_category">
			<option value="">--Select Category--</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<select id="bl_topic" class="bl_search_fields" name="bl_topic">
			<option value="">--Select Topic--</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<input type="text" name="bl_keyword" id="bl_keyword" placeholder="Keyword..." />
			<span class="btn btn-primary" id="bl_submit"><i class="icon-search"></i> Search</span>
		</div>
	</div>
</div>
<div class="row ms-2 mb-3">
	<div class="col-auto mt-3">
		<select id="bl_language" class="bl_search_fields"  name="bl_language" style="width:125px;">
			<option value="english">English</option>
			<option value="spanish">Spanish</option>
			<option value="german">German</option>
			<option value="french">French</option>
			<option value="japanese">Japanese</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<label class="input-group-text" for="bl_sort"><i class="icon-filter"></i></label>
			<select id="bl_sort" class="input-group-select"  name="bl_sort" style="width:175px;" default="popularity">
				<option value="popularity">Most Popular</option>
				<option value="created">Date Created</option>
				<option value="title">Title</option>
				<option value="avg_rating">Rating</option>
				<option value="duration">Duration</option>
			</select>
		</div>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<label class="input-group-text" for="bl_content_type"><i class="icon-filter"></i></label>
			<select id="bl_content_type" class="input-group-select"  name="bl_content_type" style="width:175px;" default="All">
				<option value="All">All</option>
				<option value="Video Lesson">Video Lesson</option>
				<option value="Video Course">Video Course</option>
				<option value="Interactive Video">Interactive Video</option>
				<option value="eLearning">eLearning</option>
			</select>
		</div>
	</div>
	<div class="col-auto mt-3">
		<div class="content-list btn btn-primary">
			Import
			<span class="content-list-count count-btn">0</span>
			Content Items
		</div>
	</div>
</div>

<div class="bl_items_container">
	<!-- bizlibrary items are rendered inside this div -->
</div>

<div class="clearfix"></div>
<div class="bl_items_container"></div>
<div class="ajax-load-more text-center" style="display:none; width: 100%; clear: both;">
    <p>Loading <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"> Content</p>
</div>

<div class="clearfix"></div>

<span class="bl_button scrollToTopBtn"><i class="icon-arrow-up-4"></i>Top</span>

<div class="offCanvasContainer">
	<div class="offCanvasContainer_Header">
		<div class="offCanvasContainer_CloseBtn"><i class="fa fa-remove"></i></div>
		<div class="offCanvas-icon">
			<i class="lizicon-dvr"></i>
		</div>
		<div class="offCanvas-title">
			Content Builder
		</div>
		<div class="offCanvas-fullButton build-btn" style="display:none;">
			<i class="fa fa-cloud-download"></i>
			Import <span class="content-list-count">0</span> Items
		</div>
		<div class="offCanvas-successButton" style="display:none;">
			<i class="fa fa-check"></i>
			Import was Successful!
		</div>
		<div class="contentImportFormWrapper">
			<div class="contentImportForm">
				<select name="import_type" id="import_type" style="width: 100%;">
					<option value="">Select Import Type</option>
					<option value="courses">Individual Courses</option>
					<option value="lessons">One Course With Lessons</option>
				</select>

				<input type="text" id="import_course_name" name="import_course_name" placeholder="Course Name..." value="" style="width: 100%;"/>

				<input type="text" id="import_category_name" name="import_category_name" placeholder="Category Name..." value="" style="width: 100%;"/>

				<?php echo AxsContentImporter::getCourseCategorySelect(); ?>

				<?php echo AxsContentImporter::getCourseSelect(); ?>

				<div class="bl_button_orange" id="build_content">
					<i class="icon-download"></i> Import
				</div>

			</div>
		</div>
	</div>
	<div class="nano has-scrollbar">
		<div class="nano-content">
			<div class="offCanvas-body">
				<ul id="content-items-list">
				</ul>
			</div>
		</div>
	</div>
</div>

<script>
	var items = [];
	var start = 0;
	var size  = 20;
	var course_type = 'existing';
	var category_type = 'existing';

	function topScroll() {
	  document.body.scrollTop = 0;
	  document.documentElement.scrollTop = 0;
	}

	function loadPreview(code) {
		var src = "/media/scorm/html/bizlibrary_preview.html?lb=i&code="+code;
		var iframe = `<iframe src="`+src+`" id="bl_preview_frame"  frameborder="0" scrolling="auto" style="width:100%; height: 60vh; sandbox="allow-same-origin allow-presentation allow-scripts allow-forms allow-popups" allow="autoplay; fullscreen" allowfullscreen="allowfullscreen""></iframe>`;
		jQuery('#previewFrame').html(iframe);

	}

	function loadSelectedTitles() {
		html = '';
		for(var i = 0; i < items.length; i++) {
			html += `<li class="ui-state-default">
						<div
							class="selected-item"
							data-code="`+items[i].code+`"
							data-title="`+items[i].title+`"
							data-image="`+items[i].image+`"
						>
							<div class="selected-item-thumbnail">
								<img src="`+items[i].image+`"/>
							</div>
							`+items[i].title+`
							<div class="clearfix"></div>
							<div class="remove-item" data-code="`+items[i].code+`">
								<i class="fa fa-trash"></i>
							</div>
						</div>
					</li>`;
		}
		jQuery('.content-list-count').text(items.length);
		jQuery('#content-items-list').html(html);
		if(items.length > 0) {
			jQuery('.offCanvas-fullButton').show();
			jQuery('.offCanvas-successButton').hide();
		} else {
			jQuery('.offCanvas-fullButton').hide();
			jQuery('.contentImportFormWrapper').hide();
		}
	}

	function checkItem(code) {
		for(var i = 0; i < items.length; i++) {
			if(items[i].code == code) {
				return true;
			}
		}
	}

	function checkIfSelected() {
		jQuery('.bl_select').each(function() {
			var code = jQuery(this).data('code');
			if(checkItem(code)) {
				jQuery(this).addClass('selectBtn_active');
			} else {
				jQuery(this).removeClass('selectBtn_active');
			}
		});
	}

	function loadItems() {
		var topic		= jQuery('#bl_topic').val();
		var category	= jQuery('#bl_category').val();
		var keyword		= jQuery('#bl_keyword').val();
		var sizeLoad	= size;
		var startLoad	= start;
		var sort		= jQuery('#bl_sort').val();
		var language	= jQuery('#bl_language').val();
		var content_type_filter = jQuery('#bl_content_type').val();

		if (content_type_filter != 'All') {
			content_type_filter = `content_type:'${content_type_filter}'`;
		} else {
			content_type_filter = '';
		}

		if(keyword) {
			topic    = '';
			category = '';
		}
		var data = {
			'topic'   		: topic,
			'category'  	: category,
			'keyword'  		: keyword,
			'size'  		: sizeLoad,
			'sort'  		: sort,
			'start'  		: startLoad,
			'bl_language'  	: language,
			'content_type_filter' : content_type_filter
		}
		jQuery.ajax({
			url: '/index.php?option=com_splms&task=contentlibraries.getLibraryItems&format=raw',
			data: data,
			type: 'POST',
			beforeSend: function() {
				jQuery('.ajax-load-more').show();
			}
		}).done(function(response){
			start = start + 20;
			jQuery('.ajax-load-more').hide();
			jQuery('.bl_items_container').css('opacity','0');
			jQuery('.bl_items_container').append(response);
			jQuery('.bl_items_container').css('opacity','1');
			checkIfSelected();
		});
	}

	function getTopicOptions(category) {
		jQuery.getJSON('/media/scorm/categories.json', function(data) {
			var options = '<option value="">--Select Topic--</option>';
			for(var i = 0; i < data[category].length; i++) {
				options += '<option value="'+data[category][i]+'">'+data[category][i]+'</option>';
			}
			jQuery('#bl_topic').html(options);
		});
	}

	function getCategoryOptions() {
		jQuery.getJSON('/media/scorm/categories.json', function(data) {
			var options = '<option value="">--Select Category--</option>';
			jQuery.each(data, function(key, value) {
				options += '<option value="'+key+'">'+key+'</option>';
			});
			jQuery('#bl_category').html(options);
		});
	}

	function removeItem(code) {
		for(var i = 0; i < items.length; i++) {
			if(items[i].code == code) {
				items.splice(i,1);
			}
		}
	}

	function buildCourses() {
		var import_type = jQuery('#import_type').val();
		var import_course_name = jQuery('#import_course_name').val();
		var import_category_name = jQuery('#import_category_name').val();
		var import_category = jQuery('#import_category').val();
		var import_course = jQuery('#import_course').val();
		var data = {
			contentItems : items,
			import_type : import_type,
			import_course_name : import_course_name,
			import_category_name : import_category_name,
			import_category : import_category,
			import_course : import_course,
			course_type : course_type,
			category_type : category_type
		}
		jQuery.ajax({
			url: '/index.php?option=com_axs&task=content_libraries.importContent&format=raw',
			data: data,
			type: 'POST'
		}).done(function(response){
			var result = JSON.parse(response);
			console.log(response)
			if(result.status== 'success') {
				items = [];
				loadSelectedTitles();
				checkIfSelected();
				jQuery('.offCanvas-fullButton').hide();
				jQuery('.contentImportFormWrapper').hide();
				jQuery('.offCanvas-successButton').show();
			}
		});
	}

	function setImportFormFields() {
		var type = jQuery('#import_type').val();
		switch(type) {
			case 'courses':
				jQuery('#import_category').show();
				jQuery('#import_course').hide();
				jQuery('#import_course_name').hide();
				jQuery('#import_category_name').hide();
				jQuery('#add_category').css('display','inline-block');
				jQuery('#build_content').css('display','inline-block');
				jQuery('#add_course').hide();
			break;
			case 'lessons':
				jQuery('#import_course').show();
				jQuery('#import_category').hide();
				jQuery('#import_course_name').hide();
				jQuery('#import_category_name').hide();
				jQuery('#add_category').hide();
				jQuery('#add_course').css('display','inline-block');
				jQuery('#build_content').css('display','inline-block');
			break;
			default:
				jQuery('#import_course').hide();
				jQuery('#import_category').hide();
				jQuery('#import_course_name').hide();
				jQuery('#import_category_name').hide();
				jQuery('#add_category').hide();
				jQuery('#add_course').hide();
				jQuery('#build_content').hide();
			break;
		}
	}

	function orderItems() {
		itemsNew = [];
		jQuery('.selected-item').each(function(){
			var itemData = {
				'code'  : jQuery(this).data('code'),
				'title' : jQuery(this).data('title'),
				'image' : jQuery(this).data('image')
			}
			itemsNew.push(itemData);
		});
		items = itemsNew;
		console.log(items);
	}

	jQuery(document).on('click','.content-list',function() {
		if(jQuery('.offCanvasContainer').css('right') == '0px') {
			jQuery('.offCanvasContainer').css('right','-350px');
		} else {
			jQuery('.offCanvasContainer').css('right','0px');
		}
	});

	jQuery(document).on('click','.offCanvasContainer_CloseBtn',function(){
		jQuery('.offCanvasContainer').css('right','-350px');
	});

	jQuery('.build-btn').click(function() {
		jQuery('.contentImportFormWrapper').slideToggle(300)
		//buildCourses();
	});

	jQuery('#build_content').click(function() {
		buildCourses();
	});

	jQuery(document).on('click','.remove-item', function(){
		var code = jQuery(this).data('code');
		removeItem(code);
		loadSelectedTitles();
		checkIfSelected();
	})

	jQuery(function() {
    	jQuery("#content-items-list").sortable( {
			update: function(event, ui) {
				orderItems();
			}
		});
    	jQuery("#content-items-list").disableSelection();
  	});

	jQuery(document).on('change','#bl_category', function() {
		start = 0;
		jQuery('.bl_items_container').html('');
		var value = jQuery(this).val();
		jQuery('#bl_topic').val('');
		getTopicOptions(value);
		loadItems();
	});

	jQuery(document).on('change','#bl_topic,#bl_sort,#bl_content_type,#bl_size,#bl_language', function(){
		start = 0;
		jQuery('.bl_items_container').html('');
		loadItems();
	});

	jQuery(document).on('click','#bl_submit',function() {
		start = 0;
		jQuery('.bl_items_container').html('');
		loadItems();
	});

	jQuery(document).on('click', '.bl_select', function() {
		var code  = jQuery(this).data('code');
		var title = jQuery(this).data('title');
		var image = jQuery(this).data('image');
		if(jQuery(this).hasClass('selectBtn_active')) {
			jQuery(this).removeClass('selectBtn_active');
			removeItem(code)
		} else {
			jQuery(this).addClass('selectBtn_active');
			var itemData = {
				'code'  : code,
				'title' : title,
				'image' : image
			}
			items.push(itemData);
		}
		loadSelectedTitles();
		console.log(items)
	});

	jQuery(document).on('click', '.bl_preview', function(){
		var code  = jQuery(this).data('code');
		var title = jQuery(this).data('title');
		loadPreview(code);
	});

	jQuery(window).scroll(function(){
		if( jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height() ) {
			loadItems();
		}

		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		    jQuery(".scrollToTopBtn").show();
		  } else {
		    jQuery(".scrollToTopBtn").hide();
		  }
	});

	jQuery(".scrollToTopBtn").click(function(){
		topScroll();
	});

	jQuery("#bl_keyword").keyup( function(e) {
		var code = e.which;
	    if(code==13) {
	        start = 0;
			jQuery('.bl_items_container').html('');
			loadItems();
	    }
	});

	jQuery('#import_type').change(function(){
		setImportFormFields();
	});

	jQuery('#add_course').click(function(){
		if(jQuery('#import_course').is(':visible')) {
			course_type = 'new';
			jQuery('#import_course_name').show();
			jQuery('#import_course').hide();
			jQuery('#import_category').show();
			jQuery(this).html('<i class="icon-list"></i> Use Existing Course');
		} else {
			course_type = 'existing';
			jQuery('#import_course_name').hide();
			jQuery('#import_course').show();
			jQuery('#import_category').hide();
			jQuery(this).html('<i class="icon-plus"></i> Create New Course');
		}
	});
	jQuery('#add_category').click(function(){
		if(jQuery('#import_category').is(':visible')) {
			category_type = 'new';
			jQuery('#import_category_name').show();
			jQuery('#import_category').hide();
			jQuery(this).html('<i class="icon-list"></i> Use Existing Category');
		} else {
			category_type = 'existing';
			jQuery('#import_category_name').hide();
			jQuery('#import_category').show();
			jQuery(this).html('<i class="icon-plus"></i> Create New Category');
		}
	});

	getCategoryOptions();
	loadItems();
</script>
