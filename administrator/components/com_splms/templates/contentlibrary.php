<style>
	.bl_card {
		float: left;
		margin: 15px;
		position: relative;
		box-shadow: 0px 1px 3px 0px  rgba(0,0,0,0.5);
		width: 300px;
		height: 330px;
	}

	.bl_image_container {
		height: 165px;
		overflow-y: hidden;
    	position: relative;
	}

	.bl_image {
		object-fit: cover;
		width: 100%;
    	min-height: 165px;
	}

	.bl_attributes {
		font-size: 11px;
		padding-left: 10px;
		padding-right: 10px;
	}

	.bl_content {
		width: 100%;
		padding: 10px;
		border-bottom: 1px solid #eee;
		padding-bottom: 10px;
		margin-bottom: 10px;
	}

	.bl_fields {
		float: left;
		width: 25%;
	}

	.fullWidthField {
		width:100%;
		margin-left: 0px !important;
	}

	.bl_title {
		font-size: 13px;
		font-weight: bold;
		text-align: center;
		width: 100%;

	}

	.bl_button {
	    padding: 7px 15px 7px 15px;
	    border: 1px solid #283848;
	    color: #283848;
	    font-size: 12px;
	    text-align: center;
	    margin-top: 10px;
	    border-radius: 50px;
	    text-decoration: none;
	    background: #fff;
	    transition: ease .5s all;
	    margin: 0px;
	    text-shadow: none;
	    cursor: pointer;
	    display: block;
	}

	.bl_button:hover {
		color: #fff;
		background: #283848;
	}

	.bl_button_container {
		position: absolute;
		bottom: 26px;
		width: 100%;
		transition: all ease .7s;
	}

	.bl_content_type {
		position: absolute;
		bottom: 6px;
		width: 94%;
		left:10px;
		font-size:small;
	}

	.bl_selected_title {
		color: #428bca;
		font-size: 25px;
		font-weight: bold;
		width: 100%;
		clear: both;
		margin-bottom: 20px;
		border-bottom: solid 1px #ddd;
		padding-bottom: 10px;
	}

	#bl_preview_frame {
		width: 0px;
		height: 0px;
		overflow: hidden;
	}

	.scrollToTopBtn {
		position: fixed;
		z-index: 10;
		right: 0px;
		bottom: 5px;
		color: #fff;
		background: #283848;

	}

	.selectBtn_active {
		color: #fff;
		background: #283848;
	}

	.scrollToTopBtn:hover {
		color: #283848;
		background: #fff;

	}

	.ajax-load-more {
		font-size:  25px;
		text-transform: uppercase;
		color: #0067ff;
	}
	.ajax-load-more img {
		width: 25px;
	}

	.ajax-load-more {
		margin-top: 40px;
	}

	.selectLabel {
		font-size: 20px;
		color: #777;
	}

</style>

<?php
	$cancelButtonTextLocalized = JText::_('JCANCEL');
	echo JHtml::_(
		'bootstrap.renderModal',
		'previewModal',
		array(
			'size' => 'lg',
			'footer' => "
				<button class='btn btn-danger' type='button' data-bs-dismiss='modal'>
					$cancelButtonTextLocalized
				</button>
			"
		),
		'<iframe
			src=""
			id="bl_preview_frame"
			frameborder="0"
			scrolling="auto"
			style="width:100%; height: 60vh;"
			sandbox="allow-same-origin allow-presentation allow-scripts allow-forms allow-popups"
			allow="autoplay; fullscreen"
			allowfullscreen="allowfullscreen"
		></iframe>'
	)
?>
<div class="bl_selected_title">
</div>
<div class="row ms-2 mb-3">
	<div class="col-auto mt-3">
		<select id="bl_category" class="bl_search_fields" name="bl_category">
			<option value="">--Select Category--</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<select id="bl_topic" class="bl_search_fields" name="bl_topic">
			<option value="">--Select Topic--</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<input type="text" name="bl_keyword" id="bl_keyword" placeholder="Keyword..." />
			<span class="btn btn-primary" id="bl_submit"><i class="icon-search"></i> Search</span>
		</div>
	</div>
</div>
<div class="row ms-2 mb-3">
	<div class="col-auto mt-3">
		<select id="bl_language" class="bl_search_fields"  name="bl_language" style="width:125px;">
			<option value="english">English</option>
			<option value="spanish">Spanish</option>
			<option value="german">German</option>
			<option value="french">French</option>
			<option value="japanese">Japanese</option>
		</select>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<label class="input-group-text" for="bl_sort"><i class="icon-filter"></i></label>
			<select id="bl_sort" class="input-group-select"  name="bl_sort" style="width:175px;" default="popularity">
				<option value="popularity">Most Popular</option>
				<option value="created">Date Created</option>
				<option value="title">Title</option>
				<option value="avg_rating">Rating</option>
				<option value="duration">Duration</option>
			</select>
		</div>
	</div>
	<div class="col-auto mt-3">
		<div class="input-group">
			<label class="input-group-text" for="bl_content_type"><i class="icon-filter"></i></label>
			<select id="bl_content_type" class="input-group-select"  name="bl_content_type" style="width:175px;" default="All">
				<option value="Video Lesson">Video Lesson</option>
				<option value="Video Course">Video Course</option>
				<option value="Interactive Video">Interactive Video</option>
				<option value="eLearning">eLearning</option>
				<option value="All">All</option>
			</select>
		</div>
	</div>
</div>
<div class="clearfix"></div>
<div class="bl_items_container"></div>
<div class="ajax-load-more text-center" style="display:none; width: 100%; clear: both;">
    <p>Loading <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"> Content</p>
</div>
<span class="bl_button scrollToTopBtn"><i class="icon-arrow-up-4"></i>Top</span>

<script>
	jQuery('#bizlibrary_content_id').parent().parent().find('.control-label').hide();
	jQuery('#bizlibrary_content_title').parent().parent().find('.control-label').hide();
	jQuery('.bl_search').parent().parent().find('.control-label').hide();
	jQuery('.bl_search').parent().addClass('fullWidthField');
	var start = 0;
	var size = 20;

	function setDefaultContentTypeOptionForPage() {
		<?php
		$option =  JFactory::getApplication()->input->get('option');
		$view = JFactory::getApplication()->input->get('view');
		$content_type_filter = '';
		if ($option == 'com_splms' && $view == 'lesson') {
			?> jQuery('#bl_content_type').val('Video Lesson'); <?php
		} else if ($option == 'com_splms' && $view == 'course') {
			?> jQuery('#bl_content_type').val('Video Course'); <?php
		}
		?>
	}


	function topScroll() {
	  document.body.scrollTop = 0;
	  document.documentElement.scrollTop = 0;
	}


	function loadPreview(code) {
		var src = "/media/scorm/html/bizlibrary_preview.html?lb=i&code="+code;
		document.getElementById('bl_preview_frame').src = src;

	}

	function loadSelectedTitle() {
		var title = jQuery('#bizlibrary_content_title').val();
		if(title) {
			text = '<span class="selectLabel">Selected Content:</span> '+title;
			jQuery('.bl_selected_title').html(text);
		}

	}

	function loadItems() {
		var topic		= jQuery('#bl_topic').val();
		var category	= jQuery('#bl_category').val();
		var keyword		= jQuery('#bl_keyword').val();
		var sizeLoad	= size;
		var startLoad	= start;
		var sort		= jQuery('#bl_sort').val();
		var language	= jQuery('#bl_language').val();
		var content_type_filter = jQuery('#bl_content_type').val();

		if (content_type_filter != 'All') {
			content_type_filter = `content_type:'${content_type_filter}'`;
		} else {
			content_type_filter = '';
		}

		if(keyword) {
			topic    = '';
			category = '';
		}
		var data = {
			'topic'   		: topic,
			'category'  	: category,
			'keyword'  		: keyword,
			'size'  		: sizeLoad,
			'sort'  		: sort,
			'start'  		: startLoad,
			'bl_language'  	: language,
			'content_type_filter'  : content_type_filter,
		}
		jQuery.ajax({
			url: '/index.php?option=com_splms&task=contentlibraries.getLibraryItems&format=raw',
			data: data,
			type: 'POST',
			beforeSend: function() {
				jQuery('.ajax-load-more').show();
			}
		}).done(function(response){
			start = start + 20;
			jQuery('.ajax-load-more').hide();
			jQuery('.bl_items_container').css('opacity','0');
			jQuery('.bl_items_container').append(response);
			jQuery('.bl_items_container').css('opacity','1');
		});
	}

	function getTopicOptions(category) {
		jQuery.getJSON('/media/scorm/categories.json', function(data) {
			var options = '<option value="">--Select Topic--</option>';
			for(var i = 0; i < data[category].length; i++) {
				options += '<option value="'+data[category][i]+'">'+data[category][i]+'</option>';
			}
			jQuery('#bl_topic').html(options);
		});
	}

	function getCategoryOptions() {
		jQuery.getJSON('/media/scorm/categories.json', function(data) {
			var options = '<option value="">--Select Category--</option>';
			jQuery.each(data, function(key, value) {
				options += '<option value="'+key+'">'+key+'</option>';
			});
			jQuery('#bl_category').html(options);
		});
	}

	jQuery(document).on('change','#bl_category', function() {
		start = 0;
		jQuery('.bl_items_container').html('');
		var value = jQuery(this).val();
		jQuery('#bl_topic').val('');
		getTopicOptions(value);
		loadItems();
	});

	jQuery(document).on('change','#bl_topic,#bl_sort,#bl_size,#bl_language,#bl_content_type', function(){
		start = 0;
		jQuery('.bl_items_container').html('');
		loadItems();
	});

	jQuery(document).on('click','#bl_submit',function() {
		start = 0;
		jQuery('.bl_items_container').html('');
		loadItems();
	});

	jQuery(document).on('click', '.bl_select', function() {
		var code  = jQuery(this).data('code');
		var title = jQuery(this).data('title');
		jQuery('.selectBtn_active').removeClass('selectBtn_active');
		jQuery('#bizlibrary_content_id').val(code);
		jQuery('#bizlibrary_content_title').val(title);
		jQuery(this).addClass('selectBtn_active');
		loadSelectedTitle();
		topScroll();
	});

	jQuery(document).on('click', '.bl_preview', function(){
		var code  = jQuery(this).data('code');
		var title = jQuery(this).data('title');
		loadPreview(code);
	});

	jQuery(window).scroll(function(){
		if( jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height() ) {
			loadItems();
		}

		if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
		    jQuery(".scrollToTopBtn").show();
		  } else {
		    jQuery(".scrollToTopBtn").hide();
		  }
	});

	jQuery(".scrollToTopBtn").click(function(){
		topScroll();
	});

	jQuery("#bl_keyword").keyup( function(e) {
		var code = e.which;
	    if(code==13) {
	        start = 0;
			jQuery('.bl_items_container').html('');
			loadItems();
	    }
	});

	loadSelectedTitle();
	getCategoryOptions();
	setDefaultContentTypeOptionForPage();
	loadItems();
</script>
