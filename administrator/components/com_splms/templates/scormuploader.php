<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="/libraries/axslibs/scorm/assets/css/uploader.css">

<form action="" method="post" id="scormUploadForm" enctype="multipart/form-data">
    <div id="scorm-uploader-message"></div>    
    <div id="scorm-uploader">
        <div class="input-group">
            <span class="input-group-text"><i class="fa fa-file"></i></span>
            <input type="text" data-id="scorm_upload" readonly  class="scorm_upload form-control input-md scorm_file" name="filename"  placeholder="Upload SCORM Package" style="max-width:500px"/>
            <input id="scorm_upload" style="display: none;" type="file" name="filedata"  class="file"/>                       
            <button class="browse btn btn-primary input-md" data-id="scorm_upload" style="border-bottom-right-radius: 0.25rem; border-top-right-radius: 0.25rem;"><i class="fa fa-search"></i> Browse</button>
            <button class="scorm-submit upload-scorm btn btn-success input-md" style="margin-left: 10px; border-radius: 5px; display: none;"  data-task="uploadActivity"><i class="fa fa-upload"></i> Upload</button>
        </div>
    </div>
</form>
<div><b>Only SCORM 1.2, SCORM 2004 and AICC packages are supported.</b></div>
<script>
	var sendURL = '/index.php?format=raw&option=com_splms&task=scorm.processFile&format=raw';
</script>
<script src="/libraries/axslibs/scorm/assets/js/uploader.js"></script>