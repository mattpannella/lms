<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- <link rel="stylesheet" type="text/css" href="/libraries/axslibs/question/assets/css/uploader.css"> -->

<form action="" method="post" id="questionUploadForm" enctype="multipart/form-data">
    <div id="question-uploader-message" style="font-size: 18px;color: #47a447;"></div>
    <div id="question-uploader">
        <div class="input-group col-md-4">
            <span class="input-group-addon"><i class="fa fa-file"></i></span>
            <input type="text" data-id="question_upload" readonly  class="question_upload form-control input-md question_file" name="filename"  placeholder="Upload Question CSV"/>
            <input id="question_upload" style="display: none;" type="file" name="csv_file"  class="file"/>
            <span class="input-group-btn">
                <span class="browse btn btn-primary input-md" data-id="question_upload" style="border-bottom-left-radius: 0; border-top-left-radius: 0;"><i class="fa fa-search"></i> Browse</span>
            </span>
            <span class="input-group-btn">
            <span class="question-submit upload-question btn btn-success input-md" style="margin-left: 10px; border-radius: 5px; display: none;"  data-task="uploadActivity"><i class="fa fa-upload"></i> Upload</span>
            </span>
        </div>
    </div>
</form>
<div><b>Upload a CSV file with your questions and answers. <a href="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/Question_Import_Template.csv" download>First download the CSV template here</a></b></div>
<script>
	var sendURL = '/administrator/index.php?format=raw&option=com_splms&task=quizquestions.importQuestions&format=raw';
</script>
<script src="/administrator/components/com_splms/assets/js/question_importer.js"></script>