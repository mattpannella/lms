<?php echo AxsLoader::loadKendo(); ?>
<style>
    #student_submissions .control-label {
        display:none;
    }
    #student_submissions .controls {
        margin-left: 0px;
    }
    .fa {
        margin-right: 5px;
    }
    #activity-box {
        overflow-y: auto !important;
    }
</style>
<div id="activity-box"></div>
<table id="activitiesGrid" class="table table-bordered table-striped table-responsive">

    <tr>
        <th>id</th>
    	<th>Profile</th>
        <th><?php echo AxsLanguage::text("AXS_NAME", "Name") ?></th>
    	<th>Activity</th>
    	<th>Submission</th>
    	<th>Comment</th>
    	<th>Score</th>
        <th>Completed</th>
    	<th><?php echo AxsLanguage::text("AXS_DATE", "Date") ?></th>
        <th>Action</th>
	</tr>


    <?php
        $baseUrl = JURI::base();
    	foreach ($lessonActivities as $activity) {
            $response = '';
            if($activity->activity_type) {
                switch ($activity->activity_type) {
                    case 'textbox':
                        $response = $activity->student_response;
                    break;

                    case 'link':
                        $link = urldecode($activity->student_response);

                        if(!preg_match('/https?:\/\//', $link)) {
                            $link = '//'.$link;
                        }

                        $response = '<a class="k-button k-button-icontext" href="'.$link.'" target="_blank"><i class="fa fa-paper-plane"></i> View Link</a>';
                    break;

                    case 'upload':
                        $response = '<a class="k-button k-button-icontext" href="/'.$activity->student_response.'" download><i class="fa fa-download"></i> Download File</a>';
                    break;

                    case 'mp3':
                        $response = '<span class="k-button k-button-icontext getSubmission" data-id="'.$activity->id.'" data-download="'.$activity->student_response.'" data-user="'.$activity->user_id.'" data-type="'.$activity->activity_type.'"><i class="fa fa-headphones"></i> Listen</span>';
                    break;

                    case 'interactive':
                        $response = '<span class="k-button k-button-icontext getSubmission" data-id="'.$activity->id.'" data-download="'.$activity->student_response.'" data-user="'.$activity->user_id.'" data-type="'.$activity->activity_type.'" data-content-type="'.$activity->activity_request.'"><i class="fa fa-external-link"></i> View Results</span>';
                    break;

                    case 'video':
                        $response = '<span class="k-button k-button-icontext getSubmission" data-id="'.$activity->id.'" data-download="'.$activity->student_response.'" data-user="'.$activity->user_id.'" data-type="'.$activity->activity_type.'"><i class="fa fa-external-link"></i> View Results</span>';
                    break;

                    case 'survey':
                        $response = '<span class="k-button k-button-icontext getSubmission" data-id="'.$activity->id.'" data-download="'.$activity->student_response.'" data-user="'.$activity->user_id.'" data-type="'.$activity->activity_type.'"><i class="fa fa-external-link"></i> View Results</span>';
                    break;
                    
                    case 'esignature':
                        $response = $activity->student_response;
                    break;

                    default:
                        $response = '<span class="k-button k-button-icontext getSubmission" data-id="'.$activity->id.'" data-download="'.$activity->student_response.'" data-user="'.$activity->user_id.'" data-type="'.$activity->activity_type.'"><i class="fa  fa-video-camera"></i> Watch</span>';
                    break;

                }
            }
        	$profile = AxsExtra::getUserProfileData($activity->user_id);

        	if ($profile->photo) {
				$photo = $profile->photo;
			} else {
				$photo = "/images/user.png";
			}

        	$user_display  =  "<div  class='comment-img' style='border-radius:50%;'><a target='_blank'  class='student-link' href='/my-profile/" . $profile->alias . "'><img src='" . $photo . "' style='width: 50px;'></a></div>";
        ?>
    <tr>
        <td><?php echo $activity->id; ?></td>
    	<td><?php echo $user_display; ?></td>
        <td><?php echo $profile->first . ' ' . $profile->last; ?></td>
    	<td><?php echo $activity->activity_request; ?></td>
    	<td><?php echo $response; ?></td>
    	<td><?php echo $activity->teacher_comment; ?></td>
    	<td><?php echo $activity->grade; ?></td>
        <td><?php if($activity->completed) { echo "Yes"; } else { echo "No"; } ?></td>
    	<td><?php echo date("m/d/Y", strtotime($activity->date_submitted)); ?></td>
        <td></td>
	</tr>

    <?php    }
    ?>
</table>

<div id="activity-editor" style="display: none;">
    <form id="activity-form" class="form-horizontal" role="form" method="post">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label"><?php echo AxsLanguage::text('COM_LMS_COMMENT', 'Comment'); ?></label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="teacher_comment" name="teacher_comment" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label  for="email" class="col-sm-2 control-label">Score</label>
            <div class="col-sm-10">
                <input type="score" class="form-control" id="grade" name="grade" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label  for="email" class="col-sm-2 control-label">Completed</label>
            <div class="col-sm-10">
                <select type="completed" class="form-control" id="completed" name="completed">
                    <option value="No">No</option>
                    <option value="Yes">Yes</option>
                </select>
            </div>
        </div>
        <input type="hidden" name="id" value=""/>
    </form>
</div>

<script>
    jQuery(document).ready(function () {

        let row;
        let id;

        function initialize() {
            let activitiesGrid = jQuery("#activitiesGrid");
            activitiesGrid = activitiesGrid.kendoGrid(kendoOptions);
            let grid = activitiesGrid.data("kendoGrid");
            grid.hideColumn('id');

            var exportFlag = false;
            activitiesGrid.data("kendoGrid").bind("excelExport", function (e) {
            if (!exportFlag) {
                    e.sender.hideColumn('Profile');
                    e.sender.hideColumn('Submission');
                    e.preventDefault();
                    exportFlag = true;
                    setTimeout(function () {
                        e.sender.saveAsExcel();
                    });
                } else {
                    e.sender.showColumn('Profile');
                    e.sender.showColumn('Submission');
                    exportFlag = false;
                }
            });
        }

        function updateActivity(data) {
            let comment = jQuery('#teacher_comment').val();
            let grade   = jQuery('#grade').val();
            let completed   = jQuery('#completed').val();
            row.set("Comment", comment);
            row.set("Score", grade);
            row.set("Completed", completed);
            jQuery('#activity-editor').hide();
            dialog.data("kendoDialog").close();
            sendData();
        }

        jQuery(document).on('click', '.getSubmission', function(){
            var student_response = jQuery(this).data('download');
            var activity_id      = jQuery(this).data('id');
            var activity_type    = jQuery(this).data('type');
            var user             = jQuery(this).data('user');
            var content_type     = jQuery(this).data('content-type');

            jQuery.ajax({
                url: '/index.php?option=com_splms&task=studentactivities.getSubmission&format=raw',
                type: 'post',
                data: {
                    student_response: student_response,
                    activity_type: activity_type,
                    user: user,
                    activity_id: activity_id
                }
            }).done(function(response) {
                jQuery('#activity-box').html(response);
                jQuery('#activity-box').find('iframe').css('height','360');

                var spinner = jQuery('.version-details-box .spinner');
                spinner.hide();

                // If we're working with interactive content, display the archive versions if they exists
                if(activity_type === 'interactive') {
                    // Set up the version link handler to bind to the opened dialog
                    activity.data("kendoDialog").bind('open', function() {

                    let archiveLinks = jQuery('.splms-archived-versions a.archive-version-link');

                    if(archiveLinks !== null) {
                        archiveLinks.ready(function() {

                            archiveLinks.click(function(event) {

                                event.preventDefault();

                                spinner.show();

                                let target = event.originalEvent.target;
                                let version = jQuery(target).data('version');
                                let contentId = jQuery(target).data('content-id');
                                let userId = user;

                                // Load the selected version's information and display it
                                jQuery.get({
                                    'url': '/index.php?option=com_splms&task=studentactivities.ajax_getUserActivityArchive&format=raw',
                                    'data': {
                                        'version': version,
                                        'content_id': contentId,
                                        'user_id': userId,
                                    },
                                    'dataType': 'json'
                                }).done(function(result) {

                                    spinner.hide();

                                    // Display the results
                                    jQuery('.version-details-box .answers').html(result.answers_template);
                                });
                            });
                        });
                    }
                    });
                }


                activity.data("kendoDialog").open();
            });
        });

        function sendData() {
            let comment     = jQuery('#teacher_comment').val();
            let grade       = jQuery('#grade').val();
            let completed   = jQuery('#completed').val();
            jQuery.ajax({
                url: '/index.php?option=com_splms&task=studentactivities.updateActivity&format=raw',
                type: 'post',
                data: {
                    id: id,
                    teacher_comment: comment,
                    grade: grade,
                    completed: completed

                }
            }).done(function(response) {
                let result = JSON.parse(response)
                //alert(result.message);
            });
        }

        function openDialog(data) {
            row = data;
            id  = row.get("id");
            jQuery('#teacher_comment').val(row.get("Comment"));
            jQuery('#grade').val(row.get("Score"));
            jQuery('#completed').val(row.get("Completed"));
            jQuery('#activity-editor').show();
            jQuery('#activity-editor').show();
            dialog.data("kendoDialog").open();
        }

        function closeDialog() {
            jQuery('#activity-editor').hide();
        }

        function closeActivity() {
            jQuery('#activity-box').html('');
        }

        let kendoOptions = {
            toolbar: [
                "pdf",
                "excel"
            ],
            dataSource: {
                schema: {
                    model: {
                        fields: {
                             id: {
                                editable: false
                            },
                            Profile: {
                                editable: false
                            },
                            Name: {
                                editable: false
                            },
                            Activity: {
                                editable: false
                            },
                            Submission: {
                                editable: false
                            },
                            Comment: {
                                editable: true
                            },
                            Score: {
                                editable: true
                            },
                            Completed: {
                                editable: true
                            },
                            Date: {
                                type: "date"
                            }
                        }
                    }
                }
            },
            columns: [
                {
                    field: "id",
                    filterable: false
                },
                {
                    field: "Profile",
                    filterable: false,
                     width: "100px"
                },
                {
                    field: "Name",
                    filterable: {
                        multi:true,
                        operators: {
                            string: {
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Activity",
                    filterable: {
                        multi:true,
                        operators: {
                            string: {
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Submission",
                    filterable: false
                },
                {
                    field: "Comment",
                    class: "comment"
                },
                {
                    field: "Score",
                    filterable: {
                        multi:true,
                        operators: {
                            string: {
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Completed",
                    filterable: {
                        multi:true,
                        operators: {
                            string: {
                                contains: "Contains"
                            }
                        }
                    }
                },
                {
                    field: "Date",
                    type: "date",
                    format: "{0:MM/d/yyyy}",
                    filterable: {
                        ui: "datepicker"
                    }
                },
                {
                    command: [
                        {
                            name: "Edit",
                            iconClass: "fa fa-edit",
                            click: function(e) {
                                let tr = jQuery(e.target).closest("tr");
                                let data = this.dataItem(tr);
                                openDialog(data);
                            }
                        }
                    ]
                }

            ],
            editable: "popup",
            sortable: true,
            columnMenu: true,
            reorderable: true,
            resizable: true,
            groupable: true,
            filterable: {
                mode: "row"
            },
            pageable: {
                buttonCount: 8,
                input: true,
                pageSize: 20,
                pageSizes: true,
                refresh: true,
                message: {
                    empty: "There are no entries to display"
                }
            }
        };

        let dialog = jQuery('#activity-editor');
        dialog.kendoDialog({
            width: "640px",
            height: "500px",
            title: "Edit Submission",
            closable: true,
            modal: true,
            actions: [
                {
                    text: 'Update',
                    action: updateActivity,
                    primary: true
                },
                {
                    text: 'Cancel',
                    action: closeDialog
                }
            ]
        });

        let activity = jQuery('#activity-box');
        activity.kendoDialog({
            width: "50%",
            height: "700px",
            title: "Student Activity",
            closable: false,
            modal: true,
            actions: [
            {
                text: 'Close',
                action: closeActivity,
                primary: true
            }
        ]
        });

        initialize();
    });
</script>