<link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poller+One" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Chonburi" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Pinyon+Script" rel="stylesheet">
<style>
    .cert-badge-icon {
	    bottom: 22%;
	    left: 8.1%;
	}
	.cert-badge-image {
	    bottom: 23%;
	    left: 8.1%;
	}
	.cert-badge-image img {
	    max-height:100%;
    }

    .cert-id-text {
		position: absolute;
		bottom: 10px;
		right: 10px;
	}
    #certificate-wrapper {
        display: none;
    }
    
    <?php
        $input = JFactory::getApplication()->input;
        $certificate_id = $input->get('id','','INT');

		if($certificate_id) {
            $certificate = AxsLMS::getCertificateById((int)$certificate_id);
            $certificateParams = json_decode($certificate->params);
            $certificate_id_text = $certificate_id;
		} else {
            $certificate_id_text = 258;
        }
        if($certificateParams->custom_css) {
            echo $certificateParams->custom_css;
        }
    ?>
</style>
<div id="certificate-wrapper">
    <div id="certificate">
        <div class="cert-title">
            <div class="cert-title-top"></div>
            <div class="cert-title-bottom"></div>
        </div>
        <div class="cert-heading"></div>
        <div class="cert-name">Recipient Name</div>
        <div class="cert-footer"></div>

        <div class="cert-badge">
            <i id="cert-badge-icon" class="lizicon-diamond" style="display: none;"></i>
            <img id="cert-badge-image" class="" style="display: none;"/>
        </div>          
        <div class="cert-logo">
            <div class="cert-signature-1">
                <div class="cert-signature">
                </div>
                <div class="cert-signature-text"></div>
            </div>
            <div class="cert-signature-2">
                <div class="cert-signature">
                </div>
                <div class="cert-signature-text"></div>
            </div>
            <img class="cert-logo-image" src="" style="width: 20%;"/>
        </div> 
        <div class="cert-id-text" style="display: none;"><?php echo str_pad($certificate_id_text,4,'0',STR_PAD_LEFT); ?></div>         
    </div>
</div>