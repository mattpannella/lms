// If we are editing a lesson that exists already, append the Preview Lesson button to the toolbar.
jQuery(document).ready(function() {
	var urlParams = new URLSearchParams(window.location.search);
	var id = urlParams.get('id');
	if (!id) {
		return;
	}
	jQuery('.btn-toolbar').append(`
		<div class="btn-wrapper" id="toolbar-preview">
			<a href="/lesson-preview?id=${id}&unlock=1" target="_blank" class="btn btn-sm btn-primary ms-1"><i class="fa fa-rocket"></i> Preview Lesson</a>
		</div>`);

	jQuery('.interactive_content_list').each((idx, el) => {
		checkInteractiveContentOptionForRequirements(getGroupId(el));
	});

	jQuery('.video_type').each((idx, el) => {
		checkVideoTypeOptionForRequirements(getGroupId(el));
	});

	jQuery('.file_location').each((idx, el) => {
		checkActivityTypeAndFileLocationForRequirements(getGroupId(el));
	});

	jQuery('.activity_type').each((idx, el) => {
		checkActivityTypeForRequirements(getGroupId(el));
	});

});

jQuery(document).on('change', '.interactive_content_list', function() {
	checkInteractiveContentOptionForRequirements(getGroupId(this));
});

jQuery(document).on('change', '.video_type', function() {
	checkVideoTypeOptionForRequirements(getGroupId(this));
});

jQuery(document).on('change', '.file_location', function() {
	checkActivityTypeAndFileLocationForRequirements(getGroupId(this));
});

jQuery(document).on('change', '.activity_type', function() {
	checkActivityTypeForRequirements(getGroupId(this));
	checkAdminApprovalSelectForRequirements(getGroupId(this));
});

jQuery(document).on('change', '.requiredcheck', function() {
	checkAdminApprovalSelectForRequirements(getGroupId(this));
});

function checkActivityTypeForRequirements(groupId) {
	var activityType = jQuery('#' + groupId +'__type').val();

	if (activityType == 'video') {
		checkVideoTypeOptionForRequirements(groupId);
		return;
	}
	if (activityType == 'interactive') {
		checkInteractiveContentOptionForRequirements(groupId);
	}

	if (activityType == 'survey') {
		jQuery('#'+groupId+'__required-row').fadeIn();
	}

	if (['powerpoint', 'pdf', 'audio'].includes(activityType)) {
		jQuery('#' + groupId +'__file_location-row').fadeIn();
		checkActivityTypeAndFileLocationForRequirements(groupId);
	} else {
		jQuery('#' + groupId +'__file_location-row').fadeOut();
		jQuery('#'+groupId+'__secure_file-row').fadeOut();
		jQuery('#'+groupId+'__file-row').fadeOut();
		jQuery('#'+groupId+'__audio_file-row').fadeOut();
		jQuery('#'+groupId+'__video_url-row').fadeOut();
	}
}

function checkVideoTypeOptionForRequirements(groupId) {
	var videoType = jQuery('#' + groupId +'__video_type').val();
	if (['mp4', 'youtube', 'vimeo'].includes(videoType)) {
		jQuery('#'+groupId+'__required-row').fadeIn();
		checkAdminApprovalSelectForRequirements(groupId);
	} else {
		jQuery('#'+groupId+'__required-row').fadeOut();
		jQuery('#'+groupId+'__required-row').parent().find('#'+groupId+'__admin_id-row').hide();
	}

	if (videoType == 'mp4') {
		jQuery('#'+groupId+'__file_location-row').fadeIn();
		checkActivityTypeAndFileLocationForRequirements(groupId);
	} else {
		jQuery('#'+groupId+'__file_location-row').fadeOut();
		jQuery('#'+groupId+'__secure_file-row').fadeOut();
		jQuery('#'+groupId+'__file-row').fadeOut();
		jQuery('#'+groupId+'__audio_file-row').fadeOut();
		jQuery('#'+groupId+'__video_url-row').fadeOut();
	}
}

function checkAdminApprovalSelectForRequirements(groupId) {
	var activityType = jQuery('#'+groupId+'__type').val();
	var adminApprovalRequiredSelect = jQuery('#'+groupId+'__required_approval-row').find('select');
	if (adminApprovalRequiredSelect.val() == '1' && !['audio', 'video', 'survey', 'embed', 'interactive', 'pdf', 'powerpoint'].includes(activityType)) {
		jQuery('#'+groupId+'__admin_id-row').show();
	} else {
		jQuery('#'+groupId+'__admin_id-row').hide();
	}
};

function checkActivityTypeAndFileLocationForRequirements(groupId) {
	var fileLocation = jQuery('#' + groupId +'__file_location').val();
	if (fileLocation == 'secure_file_locker') {
		jQuery('#'+groupId+'__secure_file-row').fadeIn();

		jQuery('#'+groupId+'__file-row').fadeOut();
		jQuery('#'+groupId+'__audio_file-row').fadeOut();
		jQuery('#'+groupId+'__video_url-row').fadeOut();
	} else {
		jQuery('#'+groupId+'__secure_file-row').fadeOut();

		var activityType = jQuery('#'+groupId+'__type').val();
		if (activityType == "powerpoint" || activityType == "pdf") {
			jQuery('#'+groupId+'__file-row').fadeIn();
			jQuery('#'+groupId+'__audio_file-row').fadeOut();
			jQuery('#'+groupId+'__video_url-row').fadeOut();
		} else if (activityType == "audio") {
			jQuery('#'+groupId+'__file-row').fadeOut();
			jQuery('#'+groupId+'__audio_file-row').fadeIn();
			jQuery('#'+groupId+'__video_url-row').fadeOut();
		} else if (activityType == "video" || activityType == "mp4") {
			jQuery('#'+groupId+'__file-row').fadeOut();
			jQuery('#'+groupId+'__audio_file-row').fadeOut();
			jQuery('#'+groupId+'__video_url-row').fadeIn();
		} else {
			jQuery('#'+groupId+'__file-row').fadeOut();
			jQuery('#'+groupId+'__audio_file-row').fadeOut();
			jQuery('#'+groupId+'__video_url-row').fadeOut();
		}
	}
}

function checkInteractiveContentOptionForRequirements(groupId) {
	var interactiveContentSelect = document.getElementById(groupId + '__interactive_id');

	var canRequire = interactiveContentSelect.options[interactiveContentSelect.selectedIndex].getAttribute('data-requirement');
	if (canRequire == 'true') {
		jQuery('#'+groupId+'__required-row').fadeIn();
		checkAdminApprovalSelectForRequirements(groupId);
	} else {
		jQuery('#'+groupId+'__required-row').fadeOut();
		jQuery('#'+groupId+'__admin_id-row').fadeOut();
	}

	var canScore = interactiveContentSelect.options[interactiveContentSelect.selectedIndex].getAttribute('data-scored');
	if (canScore == 'true') {
		jQuery('#'+groupId+'__required_score-row').fadeIn();
	} else {
		jQuery('#'+groupId+'__required_score-row').fadeOut();
	}
}

function getGroupId(element) {
	if (typeof element.parent != 'function') {
		element = jQuery(element);
	}
	let parent = element.parent();
	while (!parent.attr('data-group')) {
		parent = parent.parent();
	}
	return parent.attr('data-base-name') + '_' + parent.attr('data-group');
}

jQuery(document).on('subform-row-add', (ev, row) => {
	jQuery(row).find('.interactive_content_list').each((idx, el) => {
		checkInteractiveContentOptionForRequirements(getGroupId(el));
	});

	jQuery(row).find('.video_type').each((idx, el) => {
		checkVideoTypeOptionForRequirements(getGroupId(el));
	});

	jQuery(row).find('.file_location').each((idx, el) => {
		checkActivityTypeAndFileLocationForRequirements(getGroupId(el));
	});

	jQuery(row).find('.activity_type').each((idx, el) => {
		checkActivityTypeForRequirements(getGroupId(el));
	});
});