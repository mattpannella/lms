function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


jQuery(document).ready( function() {
	var id = getUrlVars()['id'];

	if(id){
		var previewButton = `<div class="btn-wrapper" id="toolbar-preview">
								<a href="/course-preview?id=`+id+`" target="_blank" class="btn btn-primary btn-sm ms-1"><i class="fa fa-rocket"></i> Preview Course</a>
							 </div>`;
							 jQuery('.btn-toolbar').append(previewButton);
	}

});

jQuery(document).ready(
	function() {

		var open_date = document.getElementById("course_open_date");
		var close_date = document.getElementById("course_close_date");

		//FOF calendar does not work like regular calendars.

		var open_date_value = open_date.value;
		var close_date_value = close_date.value;

		var date_error = function(type) {
			switch (type) {
				case "open":
					alert("Starting date cannot be after ending date.");
					break;
				case "close":
					alert("Ending date cannot be after starting date.");
			}
		}

		var check_date = function(type) {
			var open = new Date(open_date_value);
			var close = new Date(close_date_value);

			if (open_date.value == "") {
				open_date.value = null;
			}

			if (close_date.value == "") {
				close_date.value = null;
			}
			
			if (open > close) {
				date_error(type);

				switch (type) {
					case "open":

						open_date_value = close_date_value;
						open_date.value = open_date_value;

						break;
					case "close":

						close_date_value = open_date_value;
						close_date.value = close_date_value;

						break;
				}
				
			}
		}

		setInterval(
			function() {

				var type = null;

				if (open_date.value != open_date_value) {
					open_date_value = open_date.value;

					type = "open";
				}

				if (close_date.value != close_date_value) {
					close_date_value = close_date.value;

					type = "close";
				}

				check_date(type);	
			}, 100
		);
	}
);