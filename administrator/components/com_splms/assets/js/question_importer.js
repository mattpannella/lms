$(document).on('click', '.question_file, .browse', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var file = $('#'+id);
    file.trigger('click');
    file.change( function() {
        $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
        $('.question-submit').show();
    }); 
});

function sendData(data, url) {
    if(data) {
        var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
        var loading     = '<span class="fa fa-spinner fa-spin"></span> Uploading...';
        var building    = '<span class="fa fa-cog fa-spin"></span> Importing Questions...';
        var error       = '<span style="color: red;"><b>There was an error uploading your questions.</b></span>';
        $('#question-uploader').hide();
        $('#question-uploader-message').show();
        $('#question-uploader-message').html(loading);
        $.ajax({
            url  : url,
            data : data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type : 'post'
        }).done( function(response) {
            var result = JSON.parse(response);
            if(result) {
                var message = '<span style="color: red;"><b>'+result.message+'</b></span>';
                if(result.status == 'success') {
                    window.parent.getQuestions(result);
                    var message = '<span class="fa fa-check-circle-o"></span> '+result.message;
                    $('#question-uploader-message').html(message);
                } else {
                    $('#question-uploader-message').html(message);
                    $('#question-uploader').show();
                }
            } else {
                $('#question-uploader-message').html(error);
                $('#question-uploader').show();
            }
        });
    }
}

$('.question-submit').click( function(e) {
    e.preventDefault();
    var url  = sendURL;                    
	var data   = new FormData($('#questionUploadForm')[0]);
    sendData(data, url);                   
});