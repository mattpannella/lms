function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

jQuery(document).ready( function() {
	var id = getUrlVars()['id'];

	if(!id){
		var add = `<div class="btn-wrapper" id="toolbar-new">
					<a href="/administrator/index.php?option=com_splms&view=interactiveitems&ic=add" class="btn btn-sm button-new btn-success">
					<span class="icon-new icon-white" aria-hidden="true"></span>
					New</a>
				   </div>`;

		var all = `<div class="btn-wrapper">
					<a href="/administrator/index.php?option=com_splms&view=interactiveitems&ic=all" class="btn btn-sm">
					<span class="icon-list" aria-hidden="true"></span>
					View All Content</a>
				   </div>`;

		var results = `<div class="btn-wrapper">
						<a href="/administrator/index.php?option=com_splms&view=interactiveitems&ic=results" class="btn btn-sm">
						<span class="icon-bars" aria-hidden="true"></span>
						Results</a>
					   </div>`;			

		jQuery('.btn-toolbar').append(add+all+results);
	}

});