jQuery(document).ready( function() {
		
	var certificate = jQuery('#certificate-wrapper');

	var defaults = {
		"background_image" : "/administrator/components/com_splms/assets/images/certificates/backgrounds/certificate-background-1.jpg",
		"header_top_text" : "Certificate",
		"header_top_font" : "Chonburi",
		"header_top_font_size" : 100,
		"header_top_color" : "#fcd25a",
		"header_bottom_text" : "of Completion",
		"header_bottom_font" : "Chonburi",
		"header_bottom_font_size" : 40,
		"header_bottom_color" : "#fff",
		"introduction_text" : "This Certificate<br/> is proudly presented<br/> for honorable achievement to",
		"introduction_font" : "Verdana",
		"introduction_font_size" : 40,
		"introduction_color" : "#222",
		"recipient_name_font" : "Pinyon Script",
		"recipient_name_font_size" : 80,
		"recipient_name_color" : "#222",
		"sub_text" : "Has demonstrated competence and understanding of the materials and successfully completed the requirements to be awarded this certification.",
		"sub_text_font" : "Verdana",
		"sub_text_font_size" : 20,
		"sub_text_color" : "#222",
		"footer_area_date_issued" : "Nov. 27th 2018",
		"footer_area_date_issued2" : "Date Issued",
		"footer_area_date_expires" : "July 4th 2019",
		"footer_area_date_expires2" : "Date Expires",
		"footer_area_text" : "Signature",
		"footer_area_left_text_signature" : "Bruce Lee",
		"footer_area_text_bottom_font_size" : 20,
		"footer_area_left_font" : "Pinyon Script",
		"footer_area_left_font_size" : 32,
		"footer_area_left_color" : "#222",
		"footer_area_right_text_signature" : "Chuck Norris",
		"footer_area_right_font" : "Pinyon Script",
		"footer_area_right_font_size" : 32,
		"footer_area_right_color" : "#222",
		"logo_image" : "/administrator/components/com_splms/assets/images/certificates/your_logo.png",
		"seal_icon" : "lizicon-diamond",
		"seal_icon_font_size" : 180,
		"seal_overlay" : 55,
		"seal_overlay_color" : "#f5f7c6",
		"certificate_id_font_size" : 20
	};
	var containerWidth = jQuery('#certificate_configuration').width();

	var ratio =  1650 / containerWidth;
	if(containerWidth < 980) {
		var certWidth  = 725 / ratio;
		var certHeight = 561 / ratio;
		ratio = ratio * 2.5;
	} else if(containerWidth < 1350) {
		var certWidth  = 725 / ratio;
		var certHeight = 561 / ratio;
		ratio = ratio * 2.5;
	} else {
		var certWidth  = 725;
		var certHeight = 561;
		ratio = 2.5;
	}

	function cleanInput(string) {
		return string.replace(/<script[^>]*>.*<\/script>/gm, '');
	}
	
	jQuery('#certificate').css({'width':certWidth, 'height':certHeight});

	jQuery('#certificate_configuration').prepend(certificate);
	
	//Change the backgound image
	jQuery('.background_image').on('change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#background_type1').is(':checked')) {
			jQuery('#certificate').css("backgroundImage", "url(/"+value+")");
		} else {
			jQuery('#certificate').css("backgroundImage" , "url("+defaults.background_image+")");
		}
	});

	// Design the Header Top Text
	jQuery('.header_top_text').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_top1').is(':checked')) {
			jQuery('.cert-title-top').html(cleanInput(value));
		} else {
			jQuery('.cert-title-top').html(defaults.header_top_text);
		}
	});

	jQuery('.header_top_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_top1').is(':checked')) {
			jQuery('.cert-title-top').css('fontFamily',value);
		} else {
			jQuery('.cert-title-top').css('fontFamily',defaults.header_top_font);
		}
	});

	jQuery('.header_top_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_top1').is(':checked')) {
			jQuery('.cert-title-top').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-title-top').css('fontSize',defaults.header_top_font_size / ratio +'px');
		}
	});

	jQuery('.header_top_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_top1').is(':checked')) {
			jQuery('.cert-title-top').css('color',value);
		} else {
			jQuery('.cert-title-top').css('color',defaults.header_top_color);
		}
	});
	//End Section

	// Design the Header Bottom Text
	jQuery('.header_bottom_text').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_bottom1').is(':checked')) {
			jQuery('.cert-title-bottom').html(value);
		} else {
			jQuery('.cert-title-bottom').html(defaults.header_bottom_text);
		}
	});

	jQuery('.header_bottom_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_bottom1').is(':checked')) {
			jQuery('.cert-title-bottom').css('fontFamily',value);
		} else {
			jQuery('.cert-title-bottom').css('fontFamily',defaults.header_bottom_font);
		}
	});

	jQuery('.header_bottom_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_bottom1').is(':checked')) {
			jQuery('.cert-title-bottom').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-title-bottom').css('fontSize',defaults.header_bottom_font_size / ratio +'px');
		}
	});

	jQuery('.header_bottom_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#header_bottom1').is(':checked')) {
			jQuery('.cert-title-bottom').css('color',value);
		} else {
			jQuery('.cert-title-bottom').css('color',defaults.header_bottom_color);
		}
	});

	//End Section

	// Design the Introduction Text
	jQuery('.introduction_text').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#introduction1').is(':checked')) {
			jQuery('.cert-heading').html(cleanInput(value));
		} else {
			jQuery('.cert-heading').html(defaults.introduction_text);
		}
	});

	jQuery('.introduction_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#introduction1').is(':checked')) {
			jQuery('.cert-heading').css('fontFamily',value);
		} else {
			jQuery('.cert-heading').css('fontFamily',defaults.introduction_font);
		}
	});

	jQuery('.introduction_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#introduction1').is(':checked')) {
			jQuery('.cert-heading').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-heading').css('fontSize',defaults.introduction_font_size / ratio +'px');
		}
	});

	jQuery('.introduction_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#introduction1').is(':checked')) {
			jQuery('.cert-heading').css('color',value);
		} else {
			jQuery('.cert-heading').css('color',defaults.introduction_color);
		}
	});

	//End Section

	// Design the Recpient Text
	jQuery('.recipient_name_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#recipient_name1').is(':checked')) {
			jQuery('.cert-name').css('fontFamily',value);
		} else {
			jQuery('.cert-name').css('fontFamily',defaults.recipient_name_font);
		}
	});

	jQuery('.recipient_name_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#recipient_name1').is(':checked')) {
			jQuery('.cert-name').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-name').css('fontSize',defaults.recipient_name_font_size / ratio +'px');
		}
	});

	jQuery('.recipient_name_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#recipient_name1').is(':checked')) {
			jQuery('.cert-name').css('color',value);
		} else {
			jQuery('.cert-name').css('color',defaults.recipient_name_color);
		}
	});

	//End Section

	// Design the Sub Text
	jQuery('.sub_text').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#sub_area1').is(':checked')) {
			jQuery('.cert-footer').html(cleanInput(value));
		} else {
			jQuery('.cert-footer').html(defaults.sub_text);
		}
	});

	jQuery('.sub_text_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#sub_area1').is(':checked')) {
			jQuery('.cert-footer').css('fontFamily',value);
		} else {
			jQuery('.cert-footer').css('fontFamily',defaults.sub_text_font);
		}
	});

	jQuery('.sub_text_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#sub_area1').is(':checked')) {
			jQuery('.cert-footer').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-footer').css('fontSize',defaults.sub_text_font_size / ratio +'px');
		}
	});

	jQuery('.sub_text_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#sub_area1').is(':checked')) {
			jQuery('.cert-footer').css('color',value);
		} else {
			jQuery('.cert-footer').css('color',defaults.sub_text_color);
		}
	});

	//End Section
	
	// Design the Footer Area Left Text
	jQuery('.cert-badge').css('fontSize',defaults.seal_icon_font_size / ratio +'px');
	jQuery('.cert-signature-text').css('fontSize',defaults.footer_area_text_bottom_font_size / ratio +'px');
	jQuery('.cert-signature-text').css('bottom','-'+ 40 / ratio +'px');
	jQuery('.footer_area_left_text_top').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_left1').is(':checked')) {			
			jQuery('.cert-signature-2 > .cert-signature').html(cleanInput(value));
		} else {

			if(jQuery('#footer_area_left_type0').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature').html(defaults.footer_area_left_text_signature);
			}

			if(jQuery('#footer_area_left_type1').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature').html(defaults.footer_area_date_issued);
			}

			if(jQuery('#footer_area_left_type2').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature').html(defaults.footer_area_date_expires);
			}			
		}
	});

	jQuery('.footer_area_left_text_bottom').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_left1').is(':checked')) {			
			jQuery('.cert-signature-2 > .cert-signature-text').html(cleanInput(value));			
		} else {

			if(jQuery('#footer_area_left_type0').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature-text').html(defaults.footer_area_text);
			}

			if(jQuery('#footer_area_left_type1').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature-text').html(defaults.footer_area_date_issued2);
			}

			if(jQuery('#footer_area_left_type2').is(':checked')) {
				jQuery('.cert-signature-2 > .cert-signature-text').html(defaults.footer_area_date_expires2);
			}			
		}
	});

	jQuery('.footer_area_left_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_left1').is(':checked')) {
			jQuery('.cert-signature-2 > .cert-signature').css('fontFamily',value);
		} else {
			jQuery('.cert-signature-2 > .cert-signature').css('fontFamily',defaults.footer_area_left_font);
		}
	});

	jQuery('.footer_area_left_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_left1').is(':checked')) {
			jQuery('.cert-signature-2 > .cert-signature').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-signature-2 > .cert-signature').css('fontSize',defaults.footer_area_left_font_size / ratio +'px');
		}
	});


	jQuery('.certificate_id_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(jQuery('#show_certificate_id0').is(':checked')) {
			jQuery('.cert-id-text').show();
		} else {
			jQuery('.cert-id-text').hide();
		}
		jQuery('.cert-id-text').css('fontSize',value / ratio +'px');
		
	});

	jQuery('.footer_area_left_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_left1').is(':checked')) {
			jQuery('.cert-signature-2 > .cert-signature').css('color',value);
		} else {
			jQuery('.cert-signature-2 > .cert-signature').css('color',defaults.footer_area_left_color);
		}
	});

	//End Section

	// Design the Footer Area Left Text
	jQuery('.footer_area_right_text_top').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_right1').is(':checked')) {			
			jQuery('.cert-signature-1 > .cert-signature').html(cleanInput(value));
		} else {

			if(jQuery('#footer_area_right_type0').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature').html(defaults.footer_area_right_text_signature);
			}

			if(jQuery('#footer_area_right_type1').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature').html(defaults.footer_area_date_issued);
			}

			if(jQuery('#footer_area_right_type2').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature').html(defaults.footer_area_date_expires);
			}			
		}
	});

	jQuery('.footer_area_right_text_bottom').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_right1').is(':checked')) {			
			jQuery('.cert-signature-1 > .cert-signature-text').html(cleanInput(value));
		} else {

			if(jQuery('#footer_area_right_type0').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature-text').html(defaults.footer_area_text);
			}

			if(jQuery('#footer_area_right_type1').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature-text').html(defaults.footer_area_date_issued2);
			}

			if(jQuery('#footer_area_right_type2').is(':checked')) {
				jQuery('.cert-signature-1 > .cert-signature-text').html(defaults.footer_area_date_expires2);
			}			
		}
	});

	jQuery('.footer_area_right_font').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_right1').is(':checked')) {
			jQuery('.cert-signature-1 > .cert-signature').css('fontFamily',value);
		} else {
			jQuery('.cert-signature-1 > .cert-signature').css('fontFamily',defaults.footer_area_right_font);
		}
	});

	jQuery('.footer_area_right_font_size').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_right1').is(':checked')) {
			jQuery('.cert-signature-1 > .cert-signature').css('fontSize',value / ratio +'px');
		} else {
			jQuery('.cert-signature-1 > .cert-signature').css('fontSize',defaults.footer_area_right_font_size / ratio +'px');
		}
	});

	jQuery('.footer_area_right_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value && jQuery('#footer_area_right1').is(':checked')) {
			jQuery('.cert-signature-1 > .cert-signature').css('color',value);
		} else {
			jQuery('.cert-signature-1 > .cert-signature').css('color',defaults.footer_area_right_color);
		}
	});

	//End Section

	// Logo
	jQuery('.logo_image').on('change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value) {			
			jQuery('.cert-logo-image').attr('src','/'+value);
		} else {
			jQuery('.cert-logo-image').attr('src',defaults.logo_image);
		}
	});
	//End Section

	jQuery('#cert-badge-icon').css('fontSize',defaults.seal_icon_font_size / ratio +'px');
	// Seal
	jQuery('.seal_type').on('change buildCertificate', function() {
		if(jQuery('.seal_type').val() == 'image') {
			jQuery('.cert-badge').removeClass('cert-badge-icon');
			jQuery('.cert-badge').addClass('cert-badge-image');
			jQuery('#cert-badge-icon').hide();
			jQuery('#cert-badge-image').show();			
		}

		if(jQuery('.seal_type').val() == 'icon') {
			jQuery('.cert-badge').removeClass('cert-badge-image');
			jQuery('.cert-badge').addClass('cert-badge-icon');
			jQuery('#cert-badge-image').hide();
			jQuery('#cert-badge-icon').show();
		}
	});

	jQuery('.seal_image').on('change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value) {			
			jQuery('#cert-badge-image').attr('src','/'+value);
		}
	});

	jQuery('.icon_box,.empty_icon_button').on('click', function() {
		var value = jQuery(this).attr('icon_name');
		jQuery('#cert-badge-icon').attr('class','');
		if(value != 'null') {		
			jQuery('#cert-badge-icon').addClass('lizicon-'+value);
		} else {
			jQuery('#cert-badge-icon').addClass('lizicon-'+defaults.seal_icon);
		}
	});

	jQuery('#seal_icon').on('buildCertificate', function() {		
		var value = jQuery(this).val();
		if(value != 'null' && value != '') {
			jQuery('#cert-badge-icon').attr('class','');		
			jQuery('#cert-badge-icon').addClass(value);
		} else {
			jQuery('#cert-badge-icon').addClass(defaults.seal_icon);
		}
	});

	jQuery('.seal_overlay').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value) {		
			jQuery('.cert-badge').css('opacity',value / 100);
		} else {
			jQuery('.cert-badge').css('opacity',defaults.seal_overlay / 100);
		}
	});

	jQuery('.seal_overlay_color').on('keyup change buildCertificate', function() {
		var value = jQuery(this).val();
		if(value) {		
			jQuery('.cert-badge').css('color',value);
		} else {
			jQuery('.cert-badge').css('color',defaults.seal_overlay_color);
		}
	});

	jQuery('.seal_overlay_dropshadow').on('keyup change buildCertificate', function() {
		if(jQuery('#seal_overlay_dropshadow0').is(':checked')) {
			jQuery('.cert-badge').addClass('cert-badge-icon-shadow');
		} else {
			jQuery('.cert-badge').removeClass('cert-badge-icon-shadow');
		}
	});

	//End Section

	function buildCert() {
		jQuery('input,select,textarea').trigger('buildCertificate');
	}

	jQuery('input[name="background_type"],input[name="header_top"], input[name="header_bottom"], input[name="introduction"], input[name="recipient_name"], input[name="sub_area"], input[name="footer_area_left"], input[name="footer_area_left_type"], input[name="footer_area_right"], input[name="footer_area_right_type"], input[name="logo_image"], input[name="seal_type"],input[name="show_certificate_id"]').on('change', function() {
		buildCert();
	});

	buildCert();
	jQuery('#certificate-wrapper').show();
	jQuery('.control-label').each(function() {
		var label = jQuery(this).attr('for');
		if(label == 'certificate_builder') {
			jQuery(this).hide();
		}
	});


	jQuery('.merge_field').click(function() {
		let item = jQuery(this).data('item');
		let copyField = document.getElementById('copy_field');
		copyField.value = item;
		copyField.select();
		document.execCommand("copy");
	})

});