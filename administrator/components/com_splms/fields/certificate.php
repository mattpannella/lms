<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('JPATH_PLATFORM') or die;

class JFormFieldCertificate extends JFormField {

	protected $type = 'certificate';
	
	protected function getInput() {
		
		ob_start();
			require 'components/com_splms/templates/certificate.php';
		return ob_get_clean();
	}
}