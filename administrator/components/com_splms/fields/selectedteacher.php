<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldSelectedteacher extends FOFFormFieldText {
	protected $type = 'selectedteacher';

	public function getRepeatable() {
		if($this->value) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->quoteName('title'));
			$query->from('#__splms_teachers');
			$query->where($db->quoteName('splms_teacher_id').'='.$db->quote($this->value));
			$db->setQuery($query);
			$teacher = $db->loadObject();
			return $teacher->title;
		} else {
			return '';
		}
	}
}