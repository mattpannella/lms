<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldSelectedcourse extends FOFFormFieldText {
	protected $type = 'selectedcourse';

	public function getRepeatable() {
		if($this->value) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->quoteName('title'));
			$query->from('#__splms_courses');
			$query->where($db->quoteName('splms_course_id').'='.$db->quote($this->value));
			$db->setQuery($query);
			$course = $db->loadObject();
			return $course->title;
		} else {
			return '';
		}
	}
}
