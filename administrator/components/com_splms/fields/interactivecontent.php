<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldInteractivecontent extends JFormField
{

	protected $type = 'interactivecontent';

	private function getAllInteractiveConent() {
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('content.*,library.name AS content_type')
			  ->from('ic_h5p_contents AS content')
			  ->join('LEFT','ic_h5p_libraries AS library ON library.id = content.library_id')
			  ->order('title ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

	protected function getInput() {

		$required  = $this->required ? ' required aria-required="true"' : '';
		$interactiveContentList = self::getAllInteractiveConent();
		$output  = '<div class="ui-widget">';
		$output = '<select class="interactive_content_list combobox" data-includeclasses="interactive_content_list" name="' . $this->name . '" id="' . $this->id . '" '. $required .'>';
		$output .= '<option value="">--Select Item--</option>';
		foreach($interactiveContentList as $option) {
			if(AxsLMS::canBeRequired($option->content_type)) {
				$canBeRequired = 'data-requirement="true"';
			} else {
				$canBeRequired = "";
			}
			if(AxsLMS::canBeScored($option->content_type)) {
				$canBeScored = 'data-scored="true"';
			} else {
				$canBeScored = "";
			}
			if($this->value == $option->id) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$output .= '<option value="'.$option->id.'" '.$canBeRequired.' '.$canBeScored.' data-library="'.$option->library_id.'" '.$selected.'>'.$option->title.'</option>';
		}
		$output .= '</select>';
		$output .= '</div>';

		return $output;

	}
}
