<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldgetlessonorder extends JFormField {

	protected $type = 'getlessonorder';

	protected function getInput() {
		$required  = $this->required ? ' required aria-required="true"' : '';

		$output = '
			<input
				id="' . $this->id . '"
				name="' . $this->name . '"
				value="' . $this->value . '" '. $required .'
				type="number"
			></input>
			<script>
				function getOrder(parent){
					jQuery.ajax({
						type: "POST",
						url: "index.php",
						data: {
							"option": "com_axs",
							"task": "videos.getOrder",
							"format": "raw",
							"parent": parent,
							"table": "joom_splms_lessons",
							"column": "splms_course_id",
						}
					}).done(function(response) {
						var data = JSON.parse(response);
						console.log(data)
						var order = data.order;
						jQuery("#ordering").val(order)
					});
				};

				var parent = jQuery(".parent_category").val();
				var currentOrder = jQuery(".ordering").val();

				if (currentOrder == 0){
					getOrder(parent);
				}

				jQuery(".parent_category").change(function() {
					var parent = jQuery(".parent_category").val();
					getOrder(parent);
				});
			</script>
		';

		return $output;
	}
}
