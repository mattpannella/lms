<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldlessons extends JFormField
{

	protected $type = 'lessons';

	protected function getInput()
	{
		$required  = $this->required ? ' required aria-required="true"' : '';





		$output = '
		<select class="lessons"  name="' . $this->name . '" id="' . $this->id . '" table="' . $this->table. '" '. $required .'>
		<option value="">Loading Lessons...</option>
		</select>';
		$output .= "
				<script>

				   function getOrder(parent,change){

				   		jQuery.ajax({

			   	 			type:       'POST',
   							url:        'index.php',
  						 	data:       	{
       										'option': 	'com_splms',
       										'task': 	'lessons.getLessons',
       										'format':   'raw',
       										'parent':    parent,
       										'table':    'joom_splms_lessons',
       										'column':    'splms_course_id',

   											},

	                        success: function(response)
	                        {
	                        	jQuery('#splms_lesson_id').html(response);

	                        	var lesson = '".$this->value."';
							    if (lesson && !change) {
							    	jQuery('#splms_lesson_id').val(lesson);

							    	jQuery('#splms_lesson_id > option[value=lesson]').attr('selected','selected');
							    } else {
							    	jQuery('#splms_lesson_id')[0].selectedIndex = 0;
							    }
	                        }
	                    });

	                };

			        var parent = jQuery('#splms_course_id').val();

			        var currentLesson = jQuery('#splms_lesson_id').val();

				    jQuery('#splms_course_id').change(function(){

				    	var parent = jQuery('#splms_course_id').val();
				    	getOrder(parent,true);
				    });

				    getOrder(parent,false);

			    </script>";

		return $output;

	}
}
