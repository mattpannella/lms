<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldScormselector extends JFormField
{

	protected $type = 'scormselector';

	protected function getInput() {

		$required  = $this->required ? ' required aria-required="true"' : '';
		$scormItems = AxsScormFactory::getScormItems();
		$output  = '<div class="ui-widget">';
		$output .= '<select class="scormItems  combobox"  name="' . $this->name . '" class="' . $this->class . '" id="' . $this->id . '" '. $required .'>';
		$output .= '<option value="">--Select File--</option>';
		foreach($scormItems as $option) {
			if($this->value == $option->id) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$output .= '<option value="'.$option->id.'" '.$selected.'>'.$option->title.'</option>';
		}
		$output .= '</select>';
		$output .= '</div>';
		return $output;

	}
}
