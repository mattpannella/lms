<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/2/16
 * Time: 3:57 PM
 */
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('JPATH_PLATFORM') or die;

class FOFFormFieldAnswers extends FOFFormFieldText {
	protected $type = 'answers';

	private function getAnswers($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__splms_quizresults')
			->where('archive_date IS NULL AND ' . $db->qn('splms_quizresult_id').'='.(int)$id)
			->limit(1);
		$db->setQuery($query);
		return $db->loadObject();
	}

	private function getQuiz($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__splms_quizquestions')
			->where($db->qn('splms_quizquestion_id').'='.(int)$id)
			->limit(1);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function getInput($elementName = null, $values = null) {
		$app = JFactory::getApplication();
		$id = $app->input->get('id');
		if($id) {
			$quizResult = self::getAnswers($id);
			$answerList = $quizResult->answers;
			$quiz = self::getQuiz($quizResult->splms_quizquestion_id);
			$quizParams = json_decode($quiz->params);
		}
		if(!$id && !$answerList) {

            $message = "No results found..."; 
			return $message;
		} else {			

            $answers = json_decode($answerList);
			$result  = '';

            foreach($answers as $answer) {

                $item = json_decode($answer);
                $studentAnswer = is_null($item->answer) ? '(No Answer)' : $item->answer;

				if($item->correct || $quizParams->mode == 'survey') {
					$answerType = '<span style="color: green; font-weight: bold;"><i class="fa fa-check-circle"></i> Answer: </span> ';
					$correctAnswer = '';
				} else {
					$answerType = '<span style="color: red; font-weight: bold;"><i class="fa fa-times-circle"></i> Answer: </span> ';
					$correctAnswer = '<span style="color: green; font-weight: bold; font-style: italic;"><i class="fa fa-check-circle"></i> Correct Answer: '.$item->correctAnswer.'</span> ';
				}

                $result .= '<hr/>';
				$result .= '<b>Question:</b> '.$item->question.'<br/>';
				$result .= $answerType.$studentAnswer.'<br/>';

				if($correctAnswer) {
					$result .= $correctAnswer;
				}
			}

			return $result;
		}
	}		
}
