<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/2/16
 * Time: 3:57 PM
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldUsers extends FOFFormFieldText {
	protected $type = 'users';

	public function getInput($elementName = null, $values = null) {

		if (!$elementName) {
			$elementName = $this->name;
		}

		if (!$values) {
			$values = $this->value;
		}

		if ($values) {
			$db = JFactory::getDbo();
			$query = "SELECT * FROM #__users WHERE id IN ($values);";
			$db->setQuery($query);
			$users = $db->loadObjectList();
			$tmp_users = array();

			foreach ($users as $user) {
				$tmp = new stdClass();
				$tmp->value = $user->id;
				$tmp->text = $user->name;
				$tmp_users[] = $tmp;
			}

			$items = json_encode($tmp_users);
	    } else {
			$items = '[]';
	    }

		
		$modalFooterButtonId = $elementName . '_modal_add';
		$modalFooter = "
			<button 
				id='$modalFooterButtonId' 
				data-bs-dismiss='modal'
				class='btn btn-success'
				type='button' 
			>Add</button>
			<button 
				class='btn btn-danger' 
				data-bs-dismiss='modal'
				type='button'
			>Cancel</button>
		";

		$modalBodyIframeId = $elementName . '_iframe';
		$modalBody = "
			<iframe 
				id='$modalBodyIframeId' 
				src='index.php?option=com_users&view=axsusers&layout=modalselect&tmpl=component&field=<?php echo $elementName; ?>' 
				style='width: 100%; height: 600px; border: none; padding-top: 20px'>
			</iframe>
		";

		$modalId = $elementName . '_modal';
		echo JHtml::_(
			'bootstrap.renderModal', 
			$modalId,
			array(
				'title' => 'Add User',
				'footer' => $modalFooter,
				'size' => 'lg'
			),
			$modalBody
		);
	    ob_start(); ?>

		<script>
			var selectedElement;
      		(function($) {
        		$(function() {

        			var element = $('input[name=<?php echo $elementName; ?>]');

					element.tagsinput({
						itemValue: 'value',
						itemText: 'text'
					});

					var items = <?php echo $items; ?>;
					items.forEach(function(item) {
						element.tagsinput('add', item);
					});

					var tagsContainer = $('input[name=<?php echo $elementName; ?>]').parent().find('.bootstrap-tagsinput');

					tagsContainer.css({
						'position': 'relative',
						'padding-right': '70px',
						'min-width': '200px',
						'min-height': '38px'
					});

					tagsContainer.find('input').hide();

					//create user button
					var userButton = document.createElement('BUTTON');
					userButton.setAttribute("data-bs-toggle", "modal");
					userButton.setAttribute("data-bs-target", "#<?php echo $elementName; ?>_modal");
					userButton.setAttribute("type", "button");
					userButton.className = "btn btn-primary";

					$(userButton).css({
						"position": "absolute",
						"top": "0",
						"right": "0",
						"height": "100%",
						"border-top-left-radius": "0",
						"border-bottom-left-radius": "0",
					});

					var spanPlus = document.createElement('SPAN');
					spanPlus.className = "icon icon-plus-2";

					var spanUser = document.createElement('SPAN');
					spanUser.className = "icon icon-user";
					spanUser.style = "font-size:1.2em";

					userButton.appendChild(spanPlus);
					userButton.appendChild(spanUser);

					tagsContainer.append(userButton);

					$(document).on(
						'click',
						'#<?php echo $elementName; ?>_modal_add',
						function() {
							selectedElement = $('input[name=<?php echo $elementName; ?>]');
							var iframe = $('#<?php echo $elementName; ?>_iframe').get(0);
							iframe.contentWindow.postMessage('getSelected', '*');
						}
					);

					$(window).on(
						'message',
						function(e) {
							if (typeof e.originalEvent.data != "string") {
								return;
							}
							var data = JSON.parse(e.originalEvent.data);
							if (data.eventName && data.eventName == "saveUsers") {
								data.usersData.forEach(
									function(item) {
										selectedElement.tagsinput('add', item);
									}
								);
							}
						}
					);

				});
			})(jQuery);

		</script>

		<?php

		$inlineHTML = ob_get_clean();

		$document = JFactory::getApplication()->getDocument();
		$document->addStyleSheet('/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.css');
		$document->addScript('/node_modules/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js');
		$input = '<input type="text" name="'.$elementName.'" id="'.$elementName.'" aria-invalid="false">';

		return $input . $inlineHTML;
	}


	public function getRepeatable() {

	}
}
