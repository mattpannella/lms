<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldScormuploader extends JFormField {

    protected $type = 'scormuploader';

    public function getInput() {
        ob_start();
        ?>
            <script>
                jQuery('#scorm_params').parent().parent().find('.control-label').hide();
            </script>
            <div id="scorm-uploader-message"></div>            
            <iframe style="width: 100%; height: 100px;" frameborder="0" scrolling="no" src="/administrator/components/com_splms/templates/scormuploader.php"></iframe>
            <script>
                function getSCORMData(response) {
                   console.log(response)
                   jQuery('#title').val(response.title);
                   jQuery('#version').val(response.version);
                   jQuery('#scorm_params').val(response.params);
                }            
            </script>           
        <?php
        
        $html = ob_get_clean();
        return $html;            
    }
}