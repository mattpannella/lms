<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldScormpackage extends JFormField {

    protected $type = 'scormpackage';

    public function getInput() {
        
        ob_start();
        ?>
            <div id="scorm-uploader-message"></div>            
            <iframe style="width: 100%; height: 40px;" frameborder="0" scrolling="no" src="index.php?tmpl=component&option=com_splms&task=scorm.buildScormUploader"></iframe>
            <script>
                function toggleEditorLabel(state) {
                    $('.control-label').each( function() {
                        if($(this).attr('for') == 'scorm_properties_editor') {
                            if(state == 'hide') {
                                $(this).hide();
                            }
                            if(state == 'show') {
                                $(this).show();
                            }
                        }
                    });
                }

                function loadEditor() {
                    var id =  $('#courseid').val();
                    $.ajax({
                        url  : 'index.php?tmpl=component&option=com_splms&task=scorm.getCoursePropertyEditor&format=raw&courseid='+id,
                        type : 'post'
                    }).done( function(response) {
                        var url = response;
                        $('#scorm-properties').attr('src',url);
                        $('#scorm-properties').show();
                        toggleEditorLabel('show')
                    });
                }

                function sendCourseID(response) {
                    var xmlDoc = $.parseXML(response);
                    var xml = $( xmlDoc );

                    console.log(xml);
                    var course   = xml.find("course");
                    var title    = course.attr("title");
                    var courseid = course.attr("id");
                    $('#courseid').val(courseid);
                    if($('#title').val() == '') {                        
                        $('#title').val(title);
                    }
                    loadEditor();
                }
            
            </script>
        <?php
        
        $html = ob_get_clean();
        return $html;
            
    }
}