<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('JPATH_PLATFORM') or die;

class JFormFieldScormproperties extends JFormField {

    protected $type = 'scormproperties';

    public function getInput() {
        
        ob_start();
        ?>           
            <iframe style="width: 80%; height: 450px; display: none;" id="scorm-properties" frameborder="0" scrolling="no" ></iframe>
            <script>                
                if($('#courseid').val()) {
                    loadEditor();                    
                }
                toggleEditorLabel('hide');
            </script>
        <?php
        
        $html = ob_get_clean();
        return $html;
    }    
}