<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldlessonactivities extends JFormField
{

	protected $type = 'lessonactivities';

	protected function getInput()
	{
		$required  = $this->required ? ' required aria-required="true"' : '';





		$output = '
		<select class="team_lesson_activity"  name="' . $this->name . '" id="' . $this->id . '" table="' . $this->table. '" '. $required .'>
		<option value="">Loading Lesson Activities...</option>
		</select>';
		$output .= "
				<script>

				   function getAssigment(lesson,change){
				   		jQuery.ajax({

			   	 			type:       'POST',
   							url:        'index.php',
  						 	data:       	{
       										'option': 	'com_splms',
       										'task': 	'lessons.getActivities',
       										'format':   'raw',
       										'lesson':    lesson

   											},

	                        success: function(response)
	                        {
	                        	jQuery('#team_lesson_activity').html(response);
	                        	var team_lesson_activity = '".$this->value."';
							    if (team_lesson_activity && !change) {
							    	jQuery('#team_lesson_activity').val(team_lesson_activity);
							    	jQuery('#team_lesson_activity > option[value=team_lesson_activity]').attr('selected','selected');
							    } else {
							    	jQuery('#team_lesson_activity')[0].selectedIndex = 0;
							    }
	                        }
	                    });

	                };

				    jQuery('#splms_lesson_id').change(function() {
				    	var lesson = jQuery('#splms_lesson_id').val();
				    	getAssigment(lesson,true);
				    });

					setTimeout(function() {
						var currentLesson = jQuery('#splms_lesson_id').val();
						getAssigment(currentLesson,false);
					},2000);


			    </script>";

		return $output;

	}
}
