<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldQuestionimporter extends JFormField {

    protected $type = 'questionimporter';

    public function getInput() {
        ob_start();
        ?>
            <input type="hidden" value="" name="<?php echo $this->name; ?>" id="questionImportData"/>
            <div id="question-import-message"></div>            
            <iframe style="width: 100%; height: 100px;" frameborder="0" scrolling="no" src="/administrator/components/com_splms/templates/question_importer.php"></iframe>
            <script>
                function getQuestions(response) {
                    if(response.questionData) {
                        jQuery('#questionImportData').val(response.questionData);
                        jQuery('#list_answers_button').hide();
                    }                   
                }            
            </script>           
        <?php
        
        $html = ob_get_clean();
        return $html;            
    }
}