<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('JPATH_PLATFORM') or die;

class JFormFieldCertificatethumbnailgenerator extends JFormField {

	protected $type = 'certificatethumbnailgenerator';
	
	protected function getInput() {
		
		ob_start();
			require 'components/com_splms/templates/certificateThumbnailGenerator.php';
		return ob_get_clean();
	}
}