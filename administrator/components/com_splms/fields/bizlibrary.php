<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldBizlibrary extends JFormField {

	protected $type = 'bizlibrary';
	
	protected function getInput() {
		ob_start();
		require 'components/com_splms/templates/contentlibrary.php';
		return ob_get_clean();
	}
}