<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');


class JFormFieldStackedRadio extends JFormFieldRadio
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'stackedradio';


		/**
	 * Method to get a control group with label and input.
	 *
	 * @param   array  $options  Options to be passed into the rendering of the field
	 *
	 * @return  string  A string containing the html for the control group
	 *
	 * @since   3.2
	 */
	public function renderField($options = array())
	{

		$rowId = $this->id . '-row';
		$options['rowId'] = $rowId;
		
		if ($this->hidden)
		{
			return $this->getInput();
		}

		if (!isset($options['class']))
		{
			$options['class'] = '';
		}

		$options['rel'] = '';

		if (empty($options['hiddenLabel']) && $this->getAttribute('hiddenLabel'))
		{
			$options['hiddenLabel'] = true;
		}

		if ($this->showon)
		{
			
			JFactory::getDocument()->addScriptDeclaration("allShowOnFieldsIds.push('" . $rowId  . "')");
			

			$options['rel']           = ' data-showon=\'' .
				json_encode(JFormHelper::parseShowOnConditions($this->showon, $this->formControl, $this->group)) . '\'';
			$options['showonEnabled'] = true;

			$showonarr = array();
			
			foreach (preg_split('%\[AND\]|\[OR\]%', $this->showon) as $showonfield)
			{
				$showon   = explode(':', $showonfield, 2);
				$showonarr[] = array(
					'field'  => $showon[0],
					'values' => explode(',', $showon[1]),
					'op'     => (preg_match('%\[(AND|OR)\]' . $showonfield . '%', $this->showon, $matches)) ? $matches[1] : ''
				);
			}

			
			$showonEvalResult = true;
			foreach ($showonarr as $condition) {
				
				$conditionEvalResult = false;
				if ($this->form->getField($condition['field'])->value == $condition['values'][0]) {
					$conditionEvalResult = true;
				}

				switch ($condition['op']) {
					case "";
						$showonEvalResult = $conditionEvalResult;
					break;
					case "AND";
						$showonEvalResult = $conditionEvalResult && $showonEvalResult;
					break;
					case "OR";
						$showonEvalResult = $conditionEvalResult || $showonEvalResult;
					break;
				}
			}

			if ($showonEvalResult == false) {
				$options['show'] = false;
			}

		}
		$layoutdata = $this->getLayoutData();
		$data = array(
			'input'   => $this->getInput(),
			'label'   => $this->getLabel(),
			'options' => $options,
			'description' => $layoutdata['description']
		);
		$renderer = $this->getRenderer('axs.form.renderfield-stacked');

		return $renderer->render($data);
	}

}
