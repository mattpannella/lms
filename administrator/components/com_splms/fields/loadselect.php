<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldLoadSelect extends JFormField {

	protected $type = 'loadselect';

	public function getInput() {
		if($this->name == 'loadselectframework') {
			$output  = '<link rel="stylesheet" href="/media/select_search/css/jquery-ui-1-12-1.css">';
			$output .= '<link rel="stylesheet" href="/media/select_search/css/selector.css">';
			$output .= '<script src="/media/select_search/js/jquery-ui-1-12-1.js"></script>';
			$output .= '<script src="/media/select_search/js/selector.js"></script>';
			return $output;
		} else {
			ob_start();
			require 'components/com_splms/templates/select_search.php';
			return ob_get_clean();
		}
	}
}