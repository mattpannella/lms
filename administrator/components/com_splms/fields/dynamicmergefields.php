<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/2/16
 * Time: 3:57 PM
 */
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('JPATH_PLATFORM') or die;

class FOFFormFieldDynamicmergefields extends FOFFormFieldText {
	protected $type = 'dynamicmergefields';

	
	public function getInput($elementName = null, $values = null) {
		$customFields = AxsUser::getProfileFields();
		$output = '<link href="/administrator/components/com_splms/assets/css/custom_fields.css" rel="stylesheet" type="text/css" />';
		$output .= '<b>Click any item to copy it to your clipboard and then you can paste it into your certificate design in custom text fields.</b><br>';
		$output .= 'Copied Text: <input type="text" id="copy_field" readonly="true" name="copy_field"><br>';
		$output .= '<div class="custom_fields">';
		foreach($customFields as $field) {
			$output .= '<div class="custom_fields_item"> 
							<span class="btn btn-sm btn-default merge_field border" data-item="['.$field->name.']"> 
								<i class="fa fa-ellipsis-v"></i> '.$field->name.'
							</span>
						</div>';
		}
		$output .= '</div>';
		return $output;
	}		
}
