<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('JPATH_PLATFORM') or die;

class JFormFieldStudentlist extends JFormField {

	protected $type = 'studentlist';
	
	protected function getInput() {
		if(!JRequest::getVar('id')) {
			return;
		}
		$params = new stdClass();
        $params->course_id = JRequest::getVar('id');
        $params->totals = TRUE;
        $comments = AxsLMS::getComments($params);
		$totalComments = is_countable($comments) ? count($comments) : 0;

		ob_start();
			require 'components/com_splms/templates/students.php';
		return ob_get_clean();
	}
}