<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldParentcategory extends JFormField
{

	protected $type = 'parentcategory.php';

	private function getParentCategories($id = null) {
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('parent_id')." IS NULL OR ".$db->quoteName('parent_id')." = 0";
		if($id) {
			$conditions[] = $db->quoteName('splms_coursescategory_id')." != ".(int)$id;
		}		
        $query = $db->getQuery(true);
        $query->select("*");
		$query->from("#__splms_coursescategories");
		$query->where($conditions);
        $query->order('title ASC');
		$db->setQuery($query);
		$results = $db->loadobjectList();
		return $results;
	}

	private function getChildrenCategories($id = null) {
		if(!$id) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('parent_id')." = ".(int)$id;	
        $query = $db->getQuery(true);
        $query->select("*");
		$query->from("#__splms_coursescategories");
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadobjectList();
		return $results;
	}
	
	protected function getInput() {
		$app = JFactory::getApplication();
		$id = $app->input->get('id');
		$parentCategories = $this->getParentCategories($id);
		$hasChildren = $this->getChildrenCategories($id);
		if($hasChildren) {
			return '<b>This category is parent to other categories and cannot be parented</b>';
		}
		$required  = $this->required ? ' required aria-required="true"' : '';
		$output = '<select class="parent_categories"  name="' . $this->name . '" class="' . $this->class . '" id="' . $this->id . '" '. $required .'>';
		$output .= '<option value="">--Select--</option>';
		foreach($parentCategories as $option) {
			if($this->value == $option->splms_coursescategory_id) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$output .= '<option value="'.$option->splms_coursescategory_id.'" '.$selected.'>'.$option->title.'</option>';
		}
		$output .= '</select>';		
		$output .= '<br>Leave blank if you want this to be a top level category';
		return $output;

	}
}
