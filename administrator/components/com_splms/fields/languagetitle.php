<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldLanguagetitle extends JFormField {

	public function getInput() {
		return "Input";
	}

	public function getRepeatable() {      
      $language = JLanguage::getInstance($this->item->language);
      return $language->get('name');
   }
}