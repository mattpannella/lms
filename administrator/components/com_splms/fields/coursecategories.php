<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldCoursecategories extends JFormField
{

	protected $type = 'coursecategories.php';


	private function getCategories() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->order('title ASC');
		$db->setQuery($query);
		$categories = $db->loadObjectList();
		$categoriesOutput = array();
		
		foreach($categories as $category) {
			if(!$category->parent_id) {
				$categoriesOutput[$category->splms_coursescategory_id] = $category;
				$categoriesOutput[$category->splms_coursescategory_id]->children = array();
			}
		}
		foreach($categories as $categoryChild) {
			if($categoryChild->parent_id) {
				if(array_key_exists($categoryChild->parent_id,$categoriesOutput)) {
					array_push($categoriesOutput[$categoryChild->parent_id]->children,$categoryChild);
				}					
			}
		}
		return $categoriesOutput;
	}
	
	protected function getInput() {
		$app = JFactory::getApplication();
		$id = $app->input->get('id');
		$parentCategories = $this->getCategories();
		$required  = $this->required ? ' required aria-required="true"' : '';
		$output = '<select class="parent_categories"  name="' . $this->name . '" class="' . $this->class . '" id="' . $this->id . '" '. $required .'>';
		$output .= '<option  value="">--Select--</option>';
		foreach($parentCategories as $option) {
			if($this->value == $option->splms_coursescategory_id) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$output .= '<option class="parentItem" value="'.$option->splms_coursescategory_id.'" '.$selected.'>'.$option->title.'</option>';
			if(!empty($option->children)) {
				foreach($option->children as $optionChild) {
					if($this->value == $optionChild->splms_coursescategory_id) {
						$selected = "selected";
					} else {
						$selected = "";
					}
					$output .= '<option class="subItem"  value="'.$optionChild->splms_coursescategory_id.'" '.$selected.'>&nbsp;--&nbsp;'.$optionChild->title.'</option>';
				}
			}
		}
		$output .= '</select>';
		return $output;

	}
}
