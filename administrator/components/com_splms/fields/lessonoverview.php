<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldLessonOverview extends JFormField {

	
	
	protected function getInput() {

		$course_id = JRequest::getVar('id');

		if(!$course_id) {
			return;
		}

		$db = JFactory::getDBO();

		$query = "SELECT splms_lesson_id AS id, title, language FROM joom_splms_lessons WHERE splms_course_id = $course_id AND enabled = 1 ORDER BY ordering";
		$db->setQuery($query);
		$lessons = $db->loadObjectList();

		$query = "SELECT lang_code, title_native AS title FROM joom_languages WHERE published = 1";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		/*$langData = array();
		foreach ($languages as $lang) {
			$langData[$lang->lang_code] = $lang->title;
		}*/

		$lessonData = array();
		
		//Get a list of all of the IDs so we don't have to run a query for each one.
		$ids = array();
		$comment_ids = array();
		foreach ($lessons as $lesson) {
			array_push($ids, $lesson->id);			
			array_push($comment_ids, "lesson_" . $lesson->id);

			$data = new stdClass();

			$data->title = $lesson->title;
			$data->comments = 0;
			$data->activities = 0;
			$data->started = 0;
			$data->failed = 0;
			$data->completed = 0;
			$data->language = $lesson->language;

			$lessonData[$lesson->id] = $data;
		
		}

		//Comments

		$query = "SELECT post_id, COUNT(post_id) AS count FROM comments WHERE FIND_IN_SET(post_id, '" . implode(",", $comment_ids) . "') AND deleted IS NULL GROUP BY post_id";
		$db->setQuery($query);
		$comments = $db->loadObjectList();

		foreach ($comments as $comment) {
			$lesson_id = str_replace("lesson_", "", $comment->post_id);			
			$lessonData[$lesson_id]->comments = $comment->count;
		}

		//Activities

		$query = "SELECT lesson_id, COUNT(lesson_id) AS count FROM joom_splms_student_activities WHERE archive_date IS NULL AND FIND_IN_SET(lesson_id, '" . implode(",", $ids) . "') GROUP BY lesson_id";
		$db->setQuery($query);
		$activities = $db->loadObjectList();

		foreach ($activities as $activity) {
			$lessonData[$activity->lesson_id]->activities = $activity->count;
		}

		//Status (started, failed, completed)

		$query = "SELECT language, status, lesson_id, COUNT(status) AS count FROM joom_splms_lesson_status WHERE archive_date IS NULL AND FIND_IN_SET(lesson_id, '" . implode(",", $ids) . "') GROUP BY status, lesson_id";
		$db->setQuery($query);
		$statuses = $db->loadObjectList();

		foreach ($statuses as $status) {
			$lessonData[$status->lesson_id]->{$status->status} = $status->count;
		}

		$current_site_language = AxsLanguage::getCurrentLanguage()->get('tag');

		ob_start();

		?>

		<style>
			#lesson_overview .control-label { 
	        	display:none; 
		    }

		    #lesson_overview .controls {
		        margin-left: 0px;
		    }
		</style>

		<script>
			var languages = [];
		</script>

		<select id="gridLanguageSelector" style="margin: 8px 0px;">
			<?php
				foreach ($languages as $language) {
			?>
					<option 
						value="<?php echo $language->lang_code;?>"
					<?php
						if ($language->lang_code == $current_site_language) {
							echo " selected ";
						}
					?>>
						<?php echo $language->title; ?>
					</option>
			<?php
				}
			?>
		</select>


		<?php
			
			foreach ($languages as $language) {

				if ($language->lang_code != $current_site_language) {
					$style = 'style="display:none;"';					
				} else {
					$style = "";
				}
		?>
				<script>
					languages.push("<?php echo $language->lang_code;?>");
				</script>

				<div id="lessonOverviewContainer_<?php echo $language->lang_code;?>" class="lessonOverviewContainer" <?php echo $style;?>>
					<table id="lessonOverviewTable_<?php echo $language->lang_code;?>" class="table table-bordered table-striped table-responsive">
						
						<?php

							foreach ($lessonData as $lesson) {

								if ($lesson->language == $language->lang_code) {

									if ($lesson->completed > 0) {
										$rate = 100 * (float)($lesson->completed / $lesson->started);
									} else {
										$rate = 0;
									}
						?>
									<tr>
										<td><?php echo $lesson->title; ?></td>
										<td><?php echo (int)$lesson->started; ?></td>
										<td><?php echo (int)$lesson->completed; ?></td>
										<td><?php echo (int)$rate; ?></td>
										<td><?php echo (int)$lesson->failed; ?></td>
										<td><?php echo (int)$lesson->comments; ?></td>
										<td><?php echo (int)$lesson->activities; ?></td>

									</tr>
						<?php
								}
							}
						?>

					</table>
				</div>
		<?php
			}
		?>

		<script>

			jQuery(document).ready(
				function() {
					var kendoOptions = {
						toolbar: ["pdf", "excel"],
		    			sortable: 	true,
		    			groupable: 	true,
		    			resizable: 	true,
		    			columnMenu: true,
		    			filterable: {
		    				mode: "row"
		    			},
		    			/*height: 	600,*/
		    			pageable: {
		    				buttonCount: 	8,
		    				input: 			true,
		    				pageSize: 		20,
		    				pageSizes: 		true,
		    				refresh: 		true,
		    				message: {
		    					empty: 		"There are no entries to display"
		    				}
		    			},
		    			columns: [
			                {
			                	field: "title",
			                	title: "Lesson Name",
			                	width: "500px",
			                	filterable: {
			                		operators: {
			                			string: {
			                				contains: "Contains"
			                			}
			                		}
			                	}
			                },
							{
								field: 	"started",
								title: 	"Total Started",
								type: 	"number"
							},
							{
								field: 	"completed",
								title: 	"Total Completed",
								type: 	"number"
							},
							{
								field: 	"rate",
								title: 	"Completion Rate",
								type: 	"number",
								format: "{0:n0}%"
							},
							{
								field: 	"failed",
								title: 	"Failed Attempts",
								type: 	"number"
							},
							{
								field: 	"comments",
								title: 	"Number of Comments",
								type: 	"number"
							},
							{
								field: 	"activities",
								title: 	"Activities Uploaded",
								type: 	"number"
							}
			                
			            ]
		    		}

		    		for (var i = 0; i < languages.length; i++) {
		    			let x = jQuery("#lessonOverviewTable_" + languages[i]).kendoGrid(kendoOptions);
		    		}

		    		jQuery("#gridLanguageSelector").change(
		    			function() {
		    				var show = this.value;

		    				jQuery(".lessonOverviewContainer").hide();
		    				jQuery("#lessonOverviewContainer_" + show).show();

		    			}
		    		);

		    	}
		    );
	    </script>
	
		<?php

		return ob_get_clean();
	}
}