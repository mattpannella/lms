<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldSecurefiles extends JFormField
{

	protected $type = 'securefiles';

	protected function getInput() {

		$required  = $this->required ? ' required aria-required="true"' : '';
		$lockerFiles = AxsFiles::getLockerFiles();
		$output  = '<div class="ui-widget">';
		$output .= '<select class="secure_file_list combobox"  name="' . $this->name . '" class="' . $this->class . '" id="' . $this->id . '" '. $required .'>';
		$output .= '<option value="">--Select File--</option>';
		foreach($lockerFiles as $option) {
			if($this->value == $option->file) {
				$selected = "selected";
			} else {
				$selected = "";
			}
			$output .= '<option value="'.$option->file.'" '.$selected.'>'.$option->file.'</option>';
		}
		$output .= '</select>';
		$output .= '</div>';
		return $output;

	}
}
