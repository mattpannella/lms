<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldActivitieslist extends JFormField {

	protected $type = 'activitieslist';
	
	protected function getInput() {
		$params = new stdClass();
        $params->lesson_id = JRequest::getVar('id');
        if(!$params->lesson_id) {
        	return;
        }
        $activityList = AxsLMS::newActivityList($params);
		$lessonActivities = $activityList->getActivities();
		ob_start();
		require 'components/com_splms/templates/activities.php';
		return ob_get_clean();
	}
}