<?php

echo AxsLoader::loadKendo();
function getResultsData() {
    $db = JFactory::getDbo();
    $conditions = array();
    $query = $db->getQuery(true);
    $query = '
    SELECT
        qr.splms_quizresult_id AS Id,
        u.name AS User,
        u.email AS Email,
        q.title AS Quiz,
        (SELECT title FROM joom_splms_lessons WHERE  splms_lesson_id = qr.splms_lesson_id) AS Lesson,
        (SELECT title FROM joom_splms_courses WHERE  splms_course_id = qr.splms_course_id) AS Course,
        ROUND(qr.point / qr.total_marks * 100) AS Score,
        DATE_FORMAT(qr.date, "%Y-%m-%d") AS Date
    FROM
        joom_splms_quizresults AS qr
            INNER JOIN
        joom_users AS u ON qr.user_id = u.id
            INNER JOIN
        joom_splms_quizquestions AS q ON qr.splms_quizquestion_id = q.splms_quizquestion_id
    WHERE qr.archive_date IS NULL
    ORDER BY id DESC;';
    $db->setQuery($query);
    $results = $db->loadObjectList();
    return json_encode($results);
}
$data = getResultsData();

if(!$data) {
    $date = '[]';
}
?>
<style>
    #loading_icon {
        font-size: 30px;
        color: #007aff;
        width: 100%;
        padding-top: 150px;
        text-align: center;
    }
</style>

<form action="index.php?option=com_splms&view=quizresults" method="post" id="adminForm" name="adminForm">
	<div id="j-main-container">
	<div id="loading_icon">
        <span class="fa fa-spinner fa-spin"></span> Loading...
    </div>
	<div id="grid"></div>
	<div>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>

<script>
    var fileName = "quiz_results";
    var kendoOptions = {
        toolbar: [
            {
                name: "excel"
            }
        ],
        excel: {
            fileName: fileName+".xlsx",
            filterable: true,
            allPages: true
        },
        sortable:   true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        columnMenu: true,
        reorderable: true,
        resizable: true,
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100],
            refresh:        true,
            message: {
                empty:      "There are no entries to display"
            }
        },
        dataSource: {
            data:  <?php echo $data; ?>
        },
        columns: [
			{
                field: "Id",
                title: "Delete",
                sortable: false,
				filterable: false,
                template: '<input type="checkbox" id="cb#: Id #" name="cid[]" value="#: Id #" onclick="Joomla.isChecked(this.checked);">',
				width: '100px'

            },
            {
                field: "User",
                title: "User",
                template: '<a href="index.php?option=com_splms&view=quizresult&id=#: Id #">#: User #</a>',
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                field: "Email",
                title: "Email",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Quiz",
                title: "Quiz",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Lesson",
                title: "Lesson",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Course",
                title: "Course",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Score",
                title: "Score",
                type: "number",
                format: "{0:#.##\\'%'}",

            },
            {
                field: "Date",
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            }
        ]
    }

    var quiz_results = jQuery("#grid").kendoGrid(kendoOptions);
    var exportFlag = false;
    var grid = quiz_results.data("kendoGrid").bind("excelExport", function (e) {
          if (!exportFlag) {
            e.sender.hideColumn(0);
            e.preventDefault();
            exportFlag = true;
            setTimeout(function () {
              e.sender.saveAsExcel();
            });
          } else {
            e.sender.showColumn(0);
            exportFlag = false;
          }
        });
    jQuery('#loading_icon').hide();
</script>