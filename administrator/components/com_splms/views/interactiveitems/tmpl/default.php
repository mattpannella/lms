<?php

$ic = $_GET['ic']; 
 
switch ($ic) {
 	case 'add':
 		$layout = "h5p_new";
 	break;

 	case 'results':
 		$layout = "h5p_results";
 	break;
 	
 	default:
 		$layout = "h5p";
 	break;
}

?>
<style>.subhead { text-shadow: none; }</style>
<script src="/administrator/components/com_splms/assets/js/interactive.js?v=2"></script>

<iframe 
	id="ic-content-container" 
	src="/interactive/content/wp-admin/admin.php?page=<?php echo $layout; ?>" 
	style="width: 100%; min-height: 100vh;" 
	frameborder="0" 
	scrolling="no" >
</iframe>

<script>
	window.addEventListener('message', function(e) {
		// message passed can be accessed in "data" attribute of the event object
		var scroll_height = e.data;

		document.getElementById('ic-content-container').style.height = scroll_height + 'px'; 
	} , false);
</script>