<form>
    <fieldset
        name="activity"
        label="Type of Content or Activity"
    >
        <field
            name="id"
            type="hidden"
        />

        <field
            name="type"
            type="list"
            label="Activity Type"
            class="activity_type"
         >
            <option value="powerpoint">PowerPoint Presentation</option>
            <option value="pdf">PDF Document</option>
            <option value="video">Video Player</option>
            <option value="audio">Audio Player</option>
            <option value="interactive">Interactive Content</option>
            <option value="survey">Survey</option>
            <option value="embed">Custom Embed</option>
            <option value="textbox">Text Box</option>
            <option value="upload">File Upload</option>
            <option value="mp4">MP4 Video Upload</option>
            <option value="mp3">MP3 Audio Upload</option>
            <option value="youtube">Submit YouTube Video</option>
            <option value="vimeo">Submit Vimeo Video</option>
            <option value="facebook">Submit Facebook Video</option>
            <option value="screencast">Submit Screencast Video</option>
            <option value="link">Submit External Link</option>
            <option value="esignature">E-signature</option>
        </field>
        <field
            name="content_width"
            label="Content Column Width"
            description="All rows are 12 columns wide. \nEach set of columns that adds up to 12 or less will be on the same row."
            type="integer"
            first="1"
            last="12"
            step="1"
            default="12"
        />
        <field
            name="use_expiration"
            type="list"
            default="0"
            label="Use Expiration Reporting"
        >
            <option value="0">No</option>
            <option value="1">Yes</option>
        </field>
        <field
            name="custom_id"
            type="text"
            label="Custom Id"
            description="This is an optional Id for expiration reporting."
            showon="use_expiration:1"
        />
        <field
            name="expiration_date"
            type="calendar"
            label="Expiration Date"
            description="This is for expiration reporting only. The content will not expire."
            showon="use_expiration:1"
        />
        <field
            name="allow_resubmit"
            type="stackedradio"
            label="Allow Resubmissions"
            description="Previous submissions are archived and not easily retrievable"
            default="0"
            showon="type:embed[OR]type:textbox[OR]type:esignature[OR]type:upload[OR]type:mp4[OR]type:mp3[OR]type:youtube[OR]type:facebook[OR]type:vimeo[OR]type:screencast[OR]type:link"
         >
            <option value="1">Yes</option>
            <option value="0">No</option>
        </field>
    </fieldset>
    <fieldset
        name="content"
        label="Content Settings"
    >
        <field
            name="icon"
            type="icon"
            class="data_dropdowns"
            label="Icon"


        />
        <field
            name="title"
            type="text"
            label="Title"

        />
        <field
            name="instructions"
            type="textarea"
            label="Instructions"

        />

        <field
            name="survey"
            type="sql"
            translate="false"
            query="SELECT * FROM axs_surveys WHERE enabled = 1"
            key_field="id"
            label="Survey"
            value_field="title"
            showon="type:survey"
        />

        <field
            name="interactive_id"
            label="Interactive Content"
            type="interactivecontent"
            class="interactive_content"
            showon="type:interactive"

        />

        <field
            name="embed_code"
            type="textarea"
            description="Copy and paste your custom embed code"
            label="Embed Code&lt;br&gt;(Max 5000 Characters)"
            class="widthFull"
            rows="8"
            maxlength="5000"
            showon="type:embed"

        />

         <field
            name="video_type"
            class="video_type"
            type="list"
            label="Video Type"
            showon="type:video"
         >
            <option value="">--Select--</option>
            <option value="mp4">MP4</option>
            <option value="youtube">YouTube</option>
            <option value="screencast">Screencast</option>
            <option value="facebook">Facebook</option>
            <option value="vimeo">Vimeo</option>
        </field>

        <field
            name="file_location"
            type="list"
            label="File Location"
            class="file_location"
            showon="type:powerpoint[OR]type:pdf[OR]type:audio[OR]type:video[AND]video_type:mp4"
            default="default_location"
         >
            <option value="default_location">Default File Folders</option>
            <option value="secure_file_locker">Secure File Locker</option>
        </field>

        <field
            name="secure_file"
            label="Secure File Locker"
            type="securefiles"
            class="secure_file_locker"

        />

        <field
            name="file"
            type="mediajce"
            mediatype="files"
            filter="none"
            label="Default File Folders"
            class="default_file_location"
        />

        <field
            name="audio_file"
            type="mediajce"
            mediatype="files"
            filter="none"
            label="Default File Folders"
            showon="type:audio"
            description="File must be MP3 Format"
        />

        <field
            name="video_url"
            type="mediajce"
            mediatype="files"
            filter="none"
            label="Default File Folders"
            showon="type:video[AND]video_type:mp4"
        />

        <field
            name="screencast_id"
            type="textarea"
            filter="raw"
            label="Screencast Direct Link or Embed Code"
            class="widthFull"
            rows="8"
            showon="type:video[AND]video_type:screencast"

        />

        <field
            name="youtube_id"
            type="text"
            class="inputbox"
            label="YouTube Video URL or ID"
            labelclass="splms-label splms-label-main"
            showon="video_type:youtube[AND]type:video"

        />

        <field
            name="facebook_url"
            type="text"
            class="inputbox"
            label="Facebook Video URL"
            labelclass="splms-label splms-label-main"
            showon="video_type:facebook[AND]type:video"

        />

        <field
            name="vimeo_id"
            type="text"
            class="inputbox"
            label="Vimeo URL or ID"
            labelclass="splms-label splms-label-main"
            showon="video_type:vimeo[AND]type:video"

        />

        <field
            name="required"
            type="list"
            class="requiredcheck"
            label="Required For Completion"
            showon="type:video[OR]type:textbox[OR]type:survey[OR]type:upload[OR]type:mp4[OR]type:mp3[OR]type:youtube[OR]type:vimeo[OR]type:facebook[OR]type:screencast[OR]type:link[OR]type:interactive[OR]type:esignature"
        >
            <option value="1">Yes</option>
            <option value="0">No</option>
        </field>

        <field
            name="required_approval"
            type="list"
            class="requiredcheck"
            label="Requires Admin Approval for Completion"
            default="0"
            showon="type:textbox[OR]type:upload[OR]type:mp4[OR]type:mp3[OR]type:youtube[OR]type:vimeo[OR]type:facebook[OR]type:screencast[OR]type:link[OR]type:esignature"
        >
            <option value="0">No</option>
            <option value="1">Yes</option>
        </field>

        <field
            name="required_score"
            type="number"
            class="required_score"
            label="Required Minimum Score (Percentage %)"
            showon="type:interactive"
        />
    </fieldset>
</form>