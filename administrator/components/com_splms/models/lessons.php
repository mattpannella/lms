<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
class SplmsModelLessons extends FOFModel {

	public function copy() {
        
        $ids = $this->getIds();
        $table  = "#__splms_lessons";
        $column = "splms_lesson_id";
        $db = JFactory::getDbo();

        foreach($ids as $id) {

            if($id) {
                $query = $db->getQuery(true);
                $query
                    ->select('*')
                    ->from($db->quoteName($table))
                    ->where($column.'='.$id)
                    ->limit(1);
                $db->setQuery($query);
                $data = $db->loadObject();

                $data->title .= " (Copy)";    
                $data->$column = '';
                $data->enabled = 0;
                $db->insertObject($table,$data);
            }
        }

        $url = JRoute::_('index.php?option=com_splms&view=lessons', false);
        JFactory::getApplication()->redirect($url);
    }


	public function save($data) {
		if(is_array($data)) {
			$this->sanitizeData($data);
			// Turn the student activity array to a json encoded string and then decode it to set the unique ids
			$activities = json_decode(json_encode($data['student_activities']));

			foreach ($activities as $activity) {
				if (!$activity->id) {
					$activity->id = AxsLMS::createUniqueID();
				}
				if ($activity->youtube_id) {
					$url = urldecode(rawurldecode($activity->youtube_id));
					preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
					if ($matches[1]) {
						$activity->youtube_id = $matches[1];
					}
				}
				if ($activity->vimeo_id) {
					$id = AxsMedia::getVimeoVideoIdFromUrl($activity->vimeo_id);
					//$id = str_replace("https://vimeo.com/", '',$activity->vimeo_id);
					$activity->vimeo_id = $id;
				}

				if ($activity->screencast_id) {
					preg_match('/src="([^"]+)"/', $activity->screencast_id, $src);

					if ($src[1]) {
		   				$id = $src[1];
		   			} else {
		   				$id = $activity->screencast_id;
		   			}

					$activity->screencast_id = $id;
				}

				if ($activity->facebook_url) {
					$url = urlencode($activity->facebook_url);
					$activity->facebook_url = $activity->facebook_url;
				}

				$expirationData = new stdClass();
				$info 			= new stdClass();
				$title = "";
				if($activity->title) {
					$title = $data['title'].' ('.$activity->title.')';
				} else {
					$title = $data['title'].' ('.$activity->type.')';
				}
				$expirationData->content_type    = 'lesson activity';
				$expirationData->content_id      = $activity->id;
				$expirationData->custom_id       = $activity->custom_id;
				$expirationData->title           = $title;
				$expirationData->expiration_date = $activity->expiration_date;
				$expirationData->data            = "";
				$expirationData->params          = "";
				$expirationObject = new AxsContentExpiration($expirationData);
				if($activity->use_expiration && ($activity->custom_id || $activity->expiration_date)) {
					$expirationObject->store();
				} else {
					if($expirationObject->getRow()) {
						$expirationObject->delete();
					}
				}
			}
			$params = new stdClass();
			$params->video_type = $data['video_type'];
			$params->student_activities = json_encode($activities);
			$params->files = $data['files'];
			$params->links = $data['links'];
			$params->duration = $data['duration'];
			$params->cover_image = $data['cover_image'];
			$params->layout = $data['layout'];
			$params->quiz_required_enabled = $data['quiz_required_enabled'];
			$params->indent_level = $data['indent_level'];
			$params->interactive_id = $data['interactive_id'];
			$params->activity_section_label = $data['activity_section_label'];
			$params->award_points = $data['award_points'];
			$params->points_category = $data['points_category'];
			$params->points = $data['points'];
			$params->use_library_content = $data['use_library_content'];
			$params->library_content_type = $data['library_content_type'];
			$params->scorm_id = $data['scorm_id'];
			$params->bizlibrary_content_title = $data['bizlibrary_content_title'];
			$params->bizlibrary_content_id = $data['bizlibrary_content_id'];
			$params->scorm_required = $data['scorm_required'];
			$params->send_approval_notification = $data['send_approval_notification'];
			$params->approval_admin_id = $data['approval_admin_id'];



			if ($data['youtube_id']) {
				$url = urldecode(rawurldecode($data['youtube_id']));
				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
				if ($matches[1]) {
					$params->youtube_id = $matches[1];
				} else {
					$params->youtube_id = $data['youtube_id'];
				}
			}

			if ($data['vimeo_id']) {
				//$id = str_replace("https://vimeo.com/", '',$data['vimeo_id']);
				$id = AxsMedia::getVimeoVideoIdFromUrl($data['vimeo_id']);
				$params->vimeo_id = $id;
			}

			if ($data['screencast_id']) {
				preg_match('/src="([^"]+)"/', $data['screencast_id'], $src);

				if ($src[1]) {
	   				$id = $src[1];
	   			} else {
	   				$id = $data['screencast_id'];
	   			}

				$params->screencast_id = $id;
			}

			if ($data['facebook_url']) {
				$url = urlencode($data['facebook_url']);
				$params->facebook_url = $data['facebook_url'];
			}

			$quizIds = '';

			if ($data['quiz_required_enabled'] && $data['required_quizzes']) {
				$quizIds = $data['required_quizzes'];
			}
			
			$data['required_quizzes'] = $quizIds;
			$data['params'] = json_encode($params);
			$data['access_level'] = json_encode($data['access_level']);

		}

		$success = parent::save($data);

		if(is_array($data)) {
			//After the lesson is saved, update the progress for the course
			//AxsLMS::courseProgress_updateLesson($data['splms_lesson_id'], $data['splms_course_id'], $data['language']);
		}

		return $success;
	}

	/**
	 * Sanitize input data before inserting
	 *
	 * @param $data
	 */
	protected function sanitizeData(&$data) {
		
		$data['title'] = trim($data['title']);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);
			$this->_formData['duration'] = $params->duration;
			$this->_formData['indent_level'] = $params->indent_level;
			$this->_formData['quiz_required_enabled'] = $params->quiz_required_enabled;
			$this->_formData['files'] = $params->files;
			$this->_formData['links'] = $params->links;
			$this->_formData['student_activities'] = $params->student_activities;
			$this->_formData['video_type'] = $params->video_type;
			$this->_formData['cover_image'] = $params->cover_image;
			$this->_formData['layout'] = $params->layout;
			$this->_formData['youtube_id'] = $params->youtube_id;
			$this->_formData['screencast_id'] = $params->screencast_id;
			$this->_formData['vimeo_id'] = $params->vimeo_id;
			$this->_formData['facebook_url'] = $params->facebook_url;
			$this->_formData['activity_section_label'] = $params->activity_section_label;
			$data = $this->_formData;

			$data['access_level'] = json_decode($data['access_level']);
			$data['interactive_id'] = $params->interactive_id;

			$data['use_library_content'] = $params->use_library_content;
			$data['library_content_type'] = $params->library_content_type;
			$data['scorm_id'] = $params->scorm_id;
			$data['bizlibrary_content_title'] = $params->bizlibrary_content_title;
			$data['bizlibrary_content_id'] = $params->bizlibrary_content_id;
			$data['scorm_required'] = $params->scorm_required;
			$data['award_points'] = $params->award_points;
			$data['points_category'] = $params->points_category;
			$data['points'] = $params->points;
			$data['send_approval_notification'] = $params->send_approval_notification;
			$data['approval_admin_id'] = $params->approval_admin_id;

			return $data;
		}
	}

	public function delete() {
		//The lesson has been deleted.  Remove it from course progress
		//AxsLMS::courseProgress_deleteLesson($this->get('id'));

		parent::delete();
	}

	public function populateState()
	{
		$this->context = 'com_splms.admin.lessons';

		$value = $this->getUserStateFromRequest($this->context . '.filter_order', 'filter_order', 'splms_lesson_id', 'cmd');

		$this->setState('filter_order', $value);

		$value = $this->getUserStateFromRequest($this->context . '.filter_order_Dir', 'filter_order_Dir', 'asc', 'word');

		$this->setState('filter_order_Dir', $value);
	}
}
