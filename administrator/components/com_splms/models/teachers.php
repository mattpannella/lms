<?php
class SplmsModelTeachers extends FOFModel {	
	public function save($data) {
		if(is_array($data)) {
			$params = new stdClass();
			$params->user = $data['user'];
			$params->profile_type = $data['profile_type'];
			$params->image_type = $data['image_type'];
			$data['params'] = json_encode($params);
		}
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);
			$this->_formData['duration'] = $params->duration;
			$this->_formData['profile_type'] = $params->profile_type;
			$this->_formData['image_type'] = $params->image_type;
			$this->_formData['user'] = $params->user;
			$data = $this->_formData;
			return $data;
		}
	}
}
