<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

class SplmsModelCourses extends FOFModel {

	public function copy() {

        $ids = $this->getIds();

        $table  = "#__splms_courses";
        $column = "splms_course_id";
        $db = JFactory::getDbo();

        foreach($ids as $id) {

            if($id) {
                $query = $db->getQuery(true);
                $query
                    ->select('*')
                    ->from($db->quoteName($table))
                    ->where($column.'='.$id)
                    ->limit(1);
                $db->setQuery($query);
                $data = $db->loadObject();

                $data->title .= " (Copy)";    
                $data->$column = '';
                $data->enabled = 0;
                $db->insertObject($table,$data);
            }
        }        

        $url = JRoute::_('index.php?option=com_splms&view=courses', false);
        JFactory::getApplication()->redirect($url);
    }


	public function save($data) {

		if(is_array($data)) {

			$this->sanitizeData($data);

			$params = new stdClass();

            $params->brand = null;

            // Set up the brand options, if any are selected. Existing courses may not have this option set at all.
            if(!empty($data['brand_visibility']) && $data['brand_visibility'] == 'specific') {
                
                if(!empty($data['brand'])) {
                    $params->brand = implode(',', $data['brand']);
                }
            }

			if(!empty($data['accesslevel'])) {
				$data['accesslevel'] = implode(',', $data['accesslevel']);
			}

			if(!empty($data['usergroup'])) {
				$data['usergroup'] = implode(',', $data['usergroup']);
			}

			if(!empty($data['access'])) {
				$data['access'] = implode(',', $data['access']);
			}

			// If the user specified a minimum student requirement the course as free
			if ($data['require_minimum_students'] == "yes" && intval($data['price'] == 0)) {
				// Invalidate this save, courses can't be free and also have a minimum student requirement
				$explanation = AxsLanguage::text(
					'AXS_SPLMS_NO_MIN_STUDENTS_FOR_FREE_COURSE',
					'Free courses cannot have a minimum student requirement. Please make this a paid course or remove the minimum student requirement.'
				);
				JFactory::getApplication()->enqueueMessage($explanation, 'error');
				return false;
			}

            $params->brand_visibility = $data['brand_visibility'];
			$params->access_purchase_type = $data['access_purchase_type'];
			$params->usergroup_purchase_register = implode(',', $data['usergroup_purchase_register']);
			$params->userlist_purchase_register = $data['userlist_purchase_register'];
			$params->lesson_gating = $data['lesson_gating'];
			$params->locked = $data['locked'];
			$params->display = $data['display'];
			$params->global_settings = $data['global_settings'];
			$params->progress_color = $data['progress_color'];
			$params->discussion_size = $data['discussion_size'];
			$params->discussion_auto = $data['discussion_auto'];
			$params->show_teachers = $data['show_teachers'];
			$params->show_students = $data['show_students'];
			$params->show_progress = $data['show_progress'];
			$params->show_progress_lessons = $data['show_progress_lessons'];
			$params->video_type = $data['video_type'];
			$params->award_points = $data['award_points'];
			$params->points_category = $data['points_category'];
			$params->points = $data['points'];
			$params->cover_image = $data['cover_image'];
			$params->layout = $data['layout'];
			$params->use_library_content = $data['use_library_content'];
			$params->library_content_type = $data['library_content_type'];
			$params->scorm_id = $data['scorm_id'];
			$params->scorm_start_button = $data['scorm_start_button'];
			$params->bizlibrary_content_title = $data['bizlibrary_content_title'];
			$params->bizlibrary_content_id = $data['bizlibrary_content_id'];
			$params->external_link = $data['external_link'];
			$params->lesson_button_text = $data['lesson_button_text'];
			$params->students_button_text = $data['students_button_text'];
			$params->progress_area_text = $data['progress_area_text'];
			$params->student_list_text = $data['student_list_text'];
			$params->teacher_list_text = $data['teacher_list_text'];
			$params->require_minimum_students = $data['require_minimum_students'];
			$params->minimum_students = $data['minimum_students'];
			$params->minimum_students_not_met_text = $data['minimum_students_not_met_text'];
			$params->notify_admin = $data['notify_admin'];
			$params->admin_id = $data['admin_id'];

			if ($data['course_use_due_date'] == "1") {
				$params->use_due_date = true;
			} else {
				$params->use_due_date = false;
			}

			if ($data['course_due_date_past_completion'] == "1") {
				$params->due_date_past_completion = true;
			} else {
				$params->due_date_past_completion = false;
			}

			$params->due_date_type = $data['course_due_date_type'];
			$params->due_static_date = $data['course_due_static_date'];
			$params->due_dynamic_number = $data['course_due_dynamic_number'];
			$params->due_dynamic_unit = $data['course_due_dynamic_unit'];

			if ($data['course_open_date'] == "0000-00-00 00:00:00") {
				$data['course_open_date'] = null;
			}

			if ($data['course_close_date'] == "0000-00-00 00:00:00") {
				$data['course_close_date'] = null;
			}

			$params->course_open_date = $data['course_open_date'];
			$params->course_close_date = $data['course_close_date'];

			if ($data['course_visible_before_open'] == "1") {
				$params->course_visible_before_open = true;
			} else {
				$params->course_visible_before_open = false;
			}

			if ($data['course_visible_after_close'] == "1") {
				$params->course_visible_after_close = true;
			} else {
				$params->course_visible_after_close = false;
			}

			if (isset($data['youtube_id'])){
				$url = urldecode(rawurldecode($data['youtube_id']));

				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
				if ($matches[1]) {
					$params->youtube_id = $matches[1];
				} else {
					$params->youtube_id = $data['youtube_id'];
				}

			}

			if (isset($data['vimeo_id'])) {
				$vimeo_id = AxsMedia::getVimeoVideoIdFromUrl($data['vimeo_id']);
				$params->vimeo_id = $vimeo_id;
			}

			if (isset($data['screencast_id'])) {
				preg_match('/src="([^"]+)"/', $data['screencast_id'], $src);

				if ($src[1]) {
	   				$screencast_id = $src[1];
	   			} else {
	   				$screencast_id = $data['screencast_id'];
	   			}

				$params->screencast_id = $screencast_id;
			}

			if (isset($data['facebook_url'])) {
				$url = urlencode($data['facebook_url']);
				$params->facebook_url = $data['facebook_url'];
			}

			//Multilingual Overrides

			$languages = self::getLanguages();
			$params->override = new stdClass();

			foreach ($languages as $language) {
				$sef = $language->sef;
				$override = new stdClass();

				$override->use = $data['lang_use_override_' . $sef];
				$override->title = $data['lang_title_' . $sef];
				$override->video_type = $data['lang_video_type_' . $sef];
				$override->youtube_id = $data['lang_youtube_id_' . $sef];
				$override->screencast_id = $data['lang_screencast_id_' . $sef];
				$override->vimeo_id = $data['lang_vimeo_id_' . $sef];
				$override->facebook_url = $data['lang_facebook_url_' . $sef];
				$override->video_url = $data['lang_video_url_' . $sef];
				$override->description = $data['lang_description_' . $sef];
				$override->minimum_students_not_met_text = $data['lang_minimum_students_not_met_text_' . $sef];
				$override->scorm_content = $data['lang_scorm_content_' . $sef];
				$override->scorm_id = $data['lang_scorm_id_' . $sef];
				$override->scorm_start_button = $data['lang_scorm_start_button_' . $sef];
				$override->lesson_button_text = $data['lang_lesson_button_text_' . $sef];
				$override->students_button_text = $data['lang_students_button_text_' . $sef];
				$override->progress_area_text = $data['lang_progress_area_text_' . $sef];
				$override->student_list_text = $data['lang_student_list_text_' . $sef];
				$override->teacher_list_text = $data['lang_teacher_list_text_' . $sef];

				$params->override->$sef = $override;
			}

			$params->show_numbering = $data['show_numbering'];
			$data['params'] = json_encode($params);

			//For the schedules only

			$id = $data['splms_course_id'];

			//$params->title = $data['title'];
			//$params->$url = JRoute::_("/index.php?option=com_splms&view=course&id=" . $id);
			//$params->image = $data['cover_image'];

			AxsLMS::updateCourseSchedules($data);
		}

		return parent::save($data);
	}

	/**
	 * Sanitize input data before inserting
	 *
	 * @param $data
	 */
	protected function sanitizeData(&$data) {
		
		$data['title'] = trim($data['title']);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$params = json_decode($data['params']);

            // Populate the brand visibility options
            $data['brand_visibility'] = $params->brand_visibility;
			$data['brand'] = explode(',', $params->brand);

			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);
			$data['access'] = explode(',', $data['access']);

			if(!$params->access_purchase_type) {
				$data['access_purchase_type'] = 'access';
			} else {
				$data['access_purchase_type'] = $params->access_purchase_type;
			}

			$data['usergroup_purchase_register'] = explode(',', $params->usergroup_purchase_register);
			$data['userlist_purchase_register'] = $params->userlist_purchase_register;
			$data['lesson_gating'] = $params->lesson_gating;
			$data['display'] = $params->display;
			$data['global_settings'] = $params->global_settings;
			$data['progress_color'] = $params->progress_color;
			$data['discussion_size'] = $params->discussion_size;
			$data['discussion_auto'] = $params->discussion_auto;
			$data['show_teachers'] = $params->show_teachers;
			$data['show_students'] = $params->show_students;
			$data['show_progress'] = $params->show_progress;
			$data['show_progress_lessons'] = $params->show_progress_lessons;
			$data['show_numbering'] = $params->show_numbering;
			$data['locked'] = $params->locked;
			$data['use_library_content'] = $params->use_library_content;
			$data['library_content_type'] = $params->library_content_type;
			$data['scorm_id'] = $params->scorm_id;
			$data['scorm_start_button'] = $params->scorm_start_button;
			$data['bizlibrary_content_title'] = $params->bizlibrary_content_title;
			$data['bizlibrary_content_id'] = $params->bizlibrary_content_id;
			$data['external_link'] = $params->external_link;

			$data['require_minimum_students'] = $params->require_minimum_students;
			$data['minimum_students'] = $params->minimum_students;
			$data['minimum_students_not_met_text'] = $params->minimum_students_not_met_text;
			$data['admin_id'] = $params->admin_id;
			$data['notify_admin'] = $params->notify_admin;

			$data['lesson_button_text'] = $params->lesson_button_text;
			$data['students_button_text'] = $params->students_button_text;
			$data['progress_area_text'] = $params->progress_area_text;
			$data['student_list_text'] = $params->student_list_text;
			$data['teacher_list_text'] = $params->teacher_list_text;

			$data['video_type'] = $params->video_type;
			$data['cover_image'] = $params->cover_image;
			$data['layout'] = $params->layout;
			$data['youtube_id'] = $params->youtube_id;
			$data['screencast_id'] = $params->screencast_id;
			$data['vimeo_id'] = $params->vimeo_id;
			$data['facebook_url'] = $params->facebook_url;
			$data['award_points'] = $params->award_points;
			$data['points_category'] = $params->points_category;
			$data['points'] = $params->points;

			if ($params->use_due_date) {
				$data['course_use_due_date'] = "1";
			} else {
				$data['course_use_due_date'] = "0";
			}

			if ($params->due_date_past_completion) {
				$data['course_due_date_past_completion'] = "1";
			} else {
				$data['course_due_date_past_completion'] = "0";
			}

			$data['course_due_date_type'] = $params->due_date_type;
			$data['course_due_static_date'] = $params->due_static_date;
			$data['course_due_dynamic_number'] = $params->due_dynamic_number;
			$data['course_due_dynamic_unit'] = $params->due_dynamic_unit;

			$data['course_open_date'] = $params->course_open_date;
			$data['course_close_date'] = $params->course_close_date;

			if ($params->course_visible_before_open) {
				$data['course_visible_before_open'] = "1";
			} else {
				$data['course_visible_before_open'] = "0";
			}

			if ($params->course_visible_after_close) {
				$data['course_visible_after_close'] = "1";
			} else {
				$data['course_visible_after_close'] = "0";
			}

			$overrides = $params->override;

			if(!empty($overrides)) {
				foreach ($overrides as $key => $override) {
					$data['lang_use_override_' . $key] = $override->use;
					$data['lang_title_' . $key] = $override->title;
					$data['lang_description_' . $key] = $override->description;
					$data['lang_minimum_students_not_met_text_' . $key] = $override->minimum_students_not_met_text;
					$data['lang_video_type_' . $key] = $override->video_type;
					$data['lang_youtube_id_' . $key] = $override->youtube_id;
					$data['lang_screencast_id_' . $key] = $override->screencast_id;
					$data['lang_vimeo_id_' . $key] = $override->vimeo_id;
					$data['lang_facebook_url_' . $key] = $override->facebook_url;
					$data['lang_video_url_' . $key] = $override->video_url;
					$data['lang_scorm_id_' . $key] = $override->scorm_id;
					$data['lang_scorm_content_' . $key] = $override->scorm_content;
					$data['lang_scorm_start_button_' . $key] = $override->scorm_start_button;
					$data['lang_lesson_button_text_' . $key] = $override->lesson_button_text;
					$data['lang_students_button_text_' . $key] = $override->students_button_text;
					$data['lang_progress_area_text_' . $key] = $override->progress_area_text;
					$data['lang_student_list_text_' . $key] = $override->student_list_text;
					$data['lang_teacher_list_text_' . $key] = $override->teacher_list_text;
				}
			}

			return $data;
		}
	}

	private static function getLanguages() {
		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_languages WHERE published = 1";
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public function delete() {
		//The course is permanently deleted, so remove any progress from the course since it is useless without a course id.
		AxsLMS::courseProgress_deleteCourse($this->get('id'));

		parent::delete();
	}

	public function populateState()
	{
		$this->context = 'com_splms.admin.courses';

		$value = $this->getUserStateFromRequest($this->context . '.filter_order', 'filter_order', 'splms_course_id', 'cmd');

		$this->setState('filter_order', $value);

		$value = $this->getUserStateFromRequest($this->context . '.filter_order_Dir', 'filter_order_Dir', 'asc', 'word');

		$this->setState('filter_order_Dir', $value);
	}
}