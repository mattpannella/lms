<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

class SplmsModelCertificates extends FOFModel {

	public function &getItemList($overrideLimits = false, $group = '') {
		$this->blacklistFilters(array('id'));
		$query = $this->buildQuery($overrideLimits);
		if (!$overrideLimits)
		{
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
			$this->list = $this->_getList((string) $query, $limitstart, $limit, $group);
		}
		else
		{
			$this->list = $this->_getList((string) $query, 0, 0, $group);
		}	

		return $this->list;
	}

	public function saveThumbnail($fileData,$title) {
		require('../components/shared/controllers/image-resizer.php');
		$imageData = explode(",", $fileData);
		$title = preg_replace("/[^A-Za-z0-9 ]/", '', $title);
		$title = str_replace(' ', '-', $title).'-';
			
		$image = imagecreatefromstring(base64_decode($imageData[1]));
		if (!$image) {		
			return;
		}

		$size = getimagesize($fileData);			
		$serverPath = str_replace('administrator','',getcwd());
		$clientPath = AxsImages::getImagesPath().'/certificates/';
		$directory = $serverPath.$clientPath;
		if(!file_exists($directory)) {
			mkdir($directory);
		}
		$fileName = $title.strtotime('now') . rand(0, 99999);
		switch ($size["mime"]) {
			//Acceptable file types
			//imagepng, imagejpeg, and imagegif save the file to its location.
			case "image/png":
				$fileName .= ".png";
				if (!imagepng($image, $directory . $fileName)) {						
					return;
				}
			break;

			case "image/jpeg":
				$fileName .= ".jpg";
				if (!imagejpeg($image, $directory . $fileName)) {	
					return;
				}				
			break;

			case "image/gif":
				$fileName .= ".gif";
				if (!imagegif($image, $directory . $fileName)) {	
					return;
				}				
			break;
				
			default:
				//Bad
				return;
			break;
		}

		$image = new SimpleImage();
		$image->load($directory . $fileName);
		$image->resizeToWidth(500);
		$image->save($directory . $fileName);

		return $clientPath.$fileName;
	}

	public function save($data) {

		$params = new stdClass();
		$data['title'] 					    = AxsSecurity::cleanInput($data['title']);
		$data['description'] 			    = AxsSecurity::cleanInput($data['description']);
		$params->template 					= AxsSecurity::cleanInput($data['template']);
		$params->background_type 			= AxsSecurity::cleanInput($data['background_type']);
		$params->background_image 			= AxsSecurity::cleanInput($data['background_image']);
		$params->header_top 				= AxsSecurity::cleanInput($data['header_top']);
		$params->header_top_text 			= AxsSecurity::cleanInput($data['header_top_text']);
		$params->header_top_font 			= AxsSecurity::cleanInput($data['header_top_font']);
		$params->header_top_font_size 		= AxsSecurity::cleanInput($data['header_top_font_size']);
		$params->header_top_color 			= AxsSecurity::cleanInput($data['header_top_color']);

		$params->header_bottom 				= AxsSecurity::cleanInput($data['header_bottom']);
		$params->header_bottom_text 		= AxsSecurity::cleanInput($data['header_bottom_text']);
		$params->header_bottom_font 		= AxsSecurity::cleanInput($data['header_bottom_font']);
		$params->header_bottom_font_size 	= AxsSecurity::cleanInput($data['header_bottom_font_size']);
		$params->header_bottom_color 		= AxsSecurity::cleanInput($data['header_bottom_color']);

		$params->introduction 				= AxsSecurity::cleanInput($data['introduction']);
		$params->introduction_text 			= AxsSecurity::cleanInput($data['introduction_text']);
		$params->introduction_font 			= AxsSecurity::cleanInput($data['introduction_font']);
		$params->introduction_font_size 	= AxsSecurity::cleanInput($data['introduction_font_size']);
		$params->introduction_color 		= AxsSecurity::cleanInput($data['introduction_color']);

		$params->recipient_name 			= AxsSecurity::cleanInput($data['recipient_name']);
		$params->recipient_name_font 		= AxsSecurity::cleanInput($data['recipient_name_font']);
		$params->recipient_name_font_size 	= AxsSecurity::cleanInput($data['recipient_name_font_size']);
		$params->recipient_name_color 		= AxsSecurity::cleanInput($data['recipient_name_color']);

		$params->sub_area 					= AxsSecurity::cleanInput($data['sub_area']);
		$params->sub_text 					= AxsSecurity::cleanInput($data['sub_text']);
		$params->sub_text_font 				= AxsSecurity::cleanInput($data['sub_text_font']);
		$params->sub_text_font_size 		= AxsSecurity::cleanInput($data['sub_text_font_size']);
		$params->sub_text_color 			= AxsSecurity::cleanInput($data['sub_text_color']);

		$params->footer_area_left_type 		= AxsSecurity::cleanInput($data['footer_area_left_type']);
		$params->footer_area_left 			= AxsSecurity::cleanInput($data['footer_area_left']);
		$params->footer_area_left_text_top  = AxsSecurity::cleanInput($data['footer_area_left_text_top']);
		$params->footer_area_left_text_bottom  = AxsSecurity::cleanInput($data['footer_area_left_text_bottom']);
		$params->footer_area_left_font 		= AxsSecurity::cleanInput($data['footer_area_left_font']);
		$params->footer_area_left_font_size = AxsSecurity::cleanInput($data['footer_area_left_font_size']);
		$params->footer_area_left_color 	= AxsSecurity::cleanInput($data['footer_area_left_color']);

		$params->footer_area_right_type 	= AxsSecurity::cleanInput($data['footer_area_right_type']);
		$params->footer_area_right 			= AxsSecurity::cleanInput($data['footer_area_right']);
		$params->footer_area_right_text_top = AxsSecurity::cleanInput($data['footer_area_right_text_top']);
		$params->footer_area_right_text_bottom 	= AxsSecurity::cleanInput($data['footer_area_right_text_bottom']);
		$params->footer_area_right_font 	= AxsSecurity::cleanInput($data['footer_area_right_font']);
		$params->footer_area_right_font_size= AxsSecurity::cleanInput($data['footer_area_right_font_size']);
		$params->footer_area_right_color 	= AxsSecurity::cleanInput($data['footer_area_right_color']);

		$params->logo_image 				= AxsSecurity::cleanInput($data['logo_image']);
		$params->seal_type 					= AxsSecurity::cleanInput($data['seal_type']);
		$params->seal_image 				= AxsSecurity::cleanInput($data['seal_image']);
		$params->seal_icon 					= AxsSecurity::cleanInput($data['seal_icon']);
		$params->seal_overlay_color 		= AxsSecurity::cleanInput($data['seal_overlay_color']);
		$params->seal_overlay 				= AxsSecurity::cleanInput($data['seal_overlay']);
		$params->seal_overlay_dropshadow	= AxsSecurity::cleanInput($data['seal_overlay_dropshadow']);
		$params->custom_css 				= AxsSecurity::cleanInput($data['custom_css']);
		$params->show_certificate_id 	    = AxsSecurity::cleanInput($data['show_certificate_id']);
		$params->certificate_id_font_size 	= AxsSecurity::cleanInput($data['certificate_id_font_size']);
		


		if($data['certificate_thumbnail_temp']) {
			$image = self::saveThumbnail($data['certificate_thumbnail_temp'],$data['title']);
			if($image) {
				$params->certificate_thumbnail = AxsSecurity::cleanInput($image);
			}
		} else {
			$params->certificate_thumbnail = AxsSecurity::cleanInput($data['certificate_thumbnail']);
		}
		

		$data['params'] = json_encode($params);
		
		return parent::save($data);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$params = json_decode($data['params']);
			
			$data['template']					= $params->template;
			$data['background_type'] 			= $params->background_type;
			$data['background_image'] 			= $params->background_image;
			$data['header_top']					= $params->header_top;
			$data['header_top_text']			= $params->header_top_text;
			$data['header_top_font']			= $params->header_top_font;
			$data['header_top_font_size']		= $params->header_top_font_size;
			$data['header_top_color']			= $params->header_top_color;

			$data['header_bottom']				= $params->header_bottom;
			$data['header_bottom_text']			= $params->header_bottom_text;
			$data['header_bottom_font']			= $params->header_bottom_font;
			$data['header_bottom_font_size']	= $params->header_bottom_font_size;
			$data['header_bottom_color']		= $params->header_bottom_color;

			$data['introduction']				= $params->introduction;
			$data['introduction_text']			= $params->introduction_text;
			$data['introduction_font']			= $params->introduction_font;
			$data['introduction_font_size']		= $params->introduction_font_size;
			$data['introduction_color']			= $params->introduction_color;

			$data['recipient_name']				= $params->recipient_name;
			$data['recipient_name_font']		= $params->recipient_name_font;
			$data['recipient_name_font_size']	= $params->recipient_name_font_size;
			$data['recipient_name_color']		= $params->recipient_name_color;

			$data['sub_area']					= $params->sub_area;
			$data['sub_text']					= $params->sub_text;
			$data['sub_text_font']				= $params->sub_text_font;
			$data['sub_text_font_size']			= $params->sub_text_font_size;
			$data['sub_text_color']				= $params->sub_text_color;

			$data['footer_area_left_type']		= $params->footer_area_left_type;
			$data['footer_area_left']			= $params->footer_area_left;
			$data['footer_area_left_text_top']	= $params->footer_area_left_text_top;
			$data['footer_area_left_text_bottom'] = $params->footer_area_left_text_bottom;
			$data['footer_area_left_font']		= $params->footer_area_left_font;
			$data['footer_area_left_font_size']	= $params->footer_area_left_font_size;
			$data['footer_area_left_color']		= $params->footer_area_left_color;
			
			$data['footer_area_right_type']		= $params->footer_area_right_type;
			$data['footer_area_right']			= $params->footer_area_right;
			$data['footer_area_right_text_top']	= $params->footer_area_right_text_top;
			$data['footer_area_right_text_bottom'] = $params->footer_area_right_text_bottom;
			$data['footer_area_right_font']		= $params->footer_area_right_font;
			$data['footer_area_right_font_size']= $params->footer_area_right_font_size;
			$data['footer_area_right_color']	= $params->footer_area_right_color;

			$data['logo_image']					= $params->logo_image;
			$data['seal_type']					= $params->seal_type;
			$data['seal_image']					= $params->seal_image;
			$data['seal_icon']					= $params->seal_icon;
			$data['seal_overlay_color']			= $params->seal_overlay_color;
			$data['seal_overlay']				= $params->seal_overlay;
			$data['seal_overlay_dropshadow']	= $params->seal_overlay_dropshadow;
			$data['certificate_thumbnail']		= $params->certificate_thumbnail;
			$data['custom_css'] 			    = $params->custom_css;
			$data['show_certificate_id'] 		= $params->show_certificate_id;
			$data['certificate_id_font_size']   = $params->certificate_id_font_size;

			return $data;
		}
	}

}