<?php
class SplmsModelCoursescategories extends FOFModel {	
	public function save($data) {
		if(is_array($data)) {
			$languages = self::getLanguages();
			$params = new stdClass();

			$params->override = new stdClass();

			$params->lang_select = $data['lang_select'];
			
			foreach ($languages as $language) {
				$sef = $language->sef;
				$override = new stdClass();

				$override->use = $data['lang_use_override_' . $sef];
				$override->title = $data['lang_title_' . $sef];			
				$override->description = $data['lang_description_' . $sef];

				$params->override->$sef = $override;
			}
			
			$params->brand = null;

            // Set up the brand options, if any are selected. Existing courses may not have this option set at all.
            if(!empty($data['brand_visibility']) && $data['brand_visibility'] == 'specific') {
                
                if(!empty($data['brand'])) {
                    $params->brand = implode(',', $data['brand']);
                }
            }

            $params->brand_visibility = $data['brand_visibility'];

			$data['params'] = json_encode($params);

			if(!empty($data['accesslevel'])) {
				$data['accesslevel'] = implode(',', $data['accesslevel']);
			}

			if(!empty($data['usergroup'])) {
				$data['usergroup'] = implode(',', $data['usergroup']);
			}
		}
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;
		if (empty($data)) {
			return array();
		} else {

			$languages = self::getLanguages();

			$params = json_decode($data['params']);

			$data['lang_select'] = $params->lang_select;

			$overrides = $params->override;

			$lang_code = substr($data['lang_select'], 0, 2);

			// Set language specific data
			$data['lang_use_override_' . $lang_code] = $overrides->$lang_code->use;
			$data['lang_title_' . $lang_code] = $overrides->$lang_code->title;
			$data['lang_description_' . $lang_code] = $overrides->$lang_code->description;

            // Populate the brand visibility options
            $data['brand_visibility'] = $params->brand_visibility;
			$data['brand'] = explode(',', $params->brand);

			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);
			
			return $data;
		}
	}

	private static function getLanguages() {
		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_languages WHERE published = 1";
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
