<?php

defined('_JEXEC') or die;

class SplmsModelCourseassignments extends FOFModel {

	public function save($data) {
        $coursesElementName = $data['full_selector_name'];

        $data['course_ids'] = !empty($data["$coursesElementName"]) ? implode(',' , $data["$coursesElementName"]) : null;
		$data['usergroups'] = !empty($data['usergroups']) ? implode(',' , $data['usergroups']) : null;

		$params = new stdClass();
		$params->send_notifications = $data['send_notifications'];
		$params->assignment_filter_type = $data['assignment_filter_type'];

		//if assignment_filter_type is groups then we need to clear any user ids or it will still use the user ids as the filter in all of the checks for assigned users
		if($params->assignment_filter_type == 'groups') {
			$data['user_ids'] = "";
		}

		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data['usergroups'] = explode(',', $data['usergroups']);
			$params = json_decode($data['params']);
			$data['send_notifications'] = $params->send_notifications;

			if(!isset($params->assignment_filter_type) && $data['user_ids']) {
				$data['assignment_filter_type'] ='users';
			} else {
				$data['assignment_filter_type'] = $params->assignment_filter_type;
			}

			return $data;
		}
	}
}