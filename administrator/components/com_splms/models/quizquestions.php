<?php

defined('_JEXEC') or die;

class SplmsModelQuizquestions extends FOFModel {
	public function save($data) {
		if (is_array($data)) {
			$params = new stdClass();
			$params->required_score = $data['required_score'];
			$params->show_answers = $data['show_answers'];
			$params->mode = "quiz";
			$params->button_text = $data['button_text'];
			$params->button_icon = $data['button_icon'];
			$params->completion_message = $data['completion_message'];
			$params->show_overlay = $data['show_overlay'];
			$params->passed_message = $data['passed_message'];
			$params->failed_message = $data['failed_message'];

			$password = htmlentities(trim($data['password']));

			// Encrypt the password
			if (!empty($password)) {
				$params->password = AxsEncryption::encrypt($password, AxsKeys::getKey('surveys'));
			} else {
				$params->password = null;
			}
			
			if ($params->mode == 'survey') {
				$params->required_score = 0;
				$data['tries'] = 0;
				$params->show_answers = 0;
			}

			if ($data['questionimportdata']) {
				$list_answers = base64_decode($data['questionimportdata']);
				if($list_answers) {
					$data['list_answers'] = $list_answers;
				}
			}
			$data['params'] = json_encode($params);
		}

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);

			$this->_formData['required_score'] = $params->required_score;
			$this->_formData['show_answers'] = $params->show_answers;
			$this->_formData['button_text'] = $params->button_text;
			$this->_formData['completion_message'] = $params->completion_message;
			$this->_formData['button_icon'] = $params->button_icon;
			$this->_formData['show_overlay'] = $params->show_overlay;
			$this->_formData['passed_message'] = $params->passed_message;
			$this->_formData['failed_message'] = $params->failed_message;

			if(!empty($params->password)) {

				// Decrypt the password
				$this->_formData['password'] = AxsEncryption::decrypt($params->password, AxsKeys::getKey('surveys'));
			}

			return $this->_formData;

		}
	}
}