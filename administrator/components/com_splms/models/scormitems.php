<?php

defined('_JEXEC') or die;

class SplmsModelScormitems extends FOFModel {
	public function save($data) {
		$key = AxsKeys::getKey('lms');
		$scorm_paramsEncoded = base64_decode($data['scorm_params']);
        $scorm_params = AxsEncryption::decrypt($scorm_paramsEncoded, $key);
		$params = new stdClass();
		$params->title        = $data['title'];
		$params->version      = $data['version'];
		$params->frame_height = $data['frame_height'];
		$params->schema       = $scorm_params->schema;
		$params->directory    = $scorm_params->directory;
		$params->fullPath     = $scorm_params->fullPath;
		$params->launch_file  = $scorm_params->launch_file;
		$params->external	  = $scorm_params->external;
		$params->scorm_params = $data['scorm_params'];
		$params->open_in_new_window = $data['open_in_new_window'];
		$params->name_format = $data['name_format'];
		$params->use_expiration = $data['use_expiration'];
		$params->custom_id = $data['custom_id'];
		$params->expiration_date = $data['expiration_date'];

		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$params               = json_decode($data['params']);
			$data['version']      = $params->version;
			$data['scorm_params'] = $params->scorm_params;
			$data['frame_height'] = $params->frame_height;
			$data['open_in_new_window'] = $params->open_in_new_window;
			$data['name_format'] = $params->name_format;
			$data['use_expiration'] = $params->use_expiration;
			$data['custom_id'] = $params->custom_id;
			$data['expiration_date'] = $params->expiration_date;
			return $data;
		}
	}
}