<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);

*/
class SplmsModelSettings extends FOFModel {

	private function getData() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("axs_lms_settings");
		$query->where('id=1');
		$query->limit(1);
		$db->setQuery($query);
		$data = $db->loadObject();
		return $data;
	}

	public function save($data) {
		$settings = new stdClass();
		$params = new stdClass();
		$video = new stdClass();
		$virtual_classroom = new stdClass();
		$params->lesson_gating = $data['lesson_gating'];
		$video->allow_downloads = $data['allow_downloads'];
		$video->restart_button = $data['restart_button'];
		$video->rewind_button = $data['rewind_button'];
		$video->fast_forward_button = $data['fast_forward_button'];
		$video->progress_button = $data['progress_button'];
		$video->current_time = $data['current_time'];
		$video->duration = $data['duration'];
		$video->mute_button = $data['mute_button'];
		$video->volume_slider = $data['volume_slider'];
		$video->captions = $data['captions'];
		$video->settings = $data['settings'];
		$video->pip = $data['pip'];
		$video->fullscreen_option = $data['fullscreen_option'];
		$video->play_button = $data['play_button'];
		$video->small_play_button = $data['small_play_button'];
		$video->default_video_cover = $data['default_video_cover'];
		$virtual_classroom->record = $data['record'];
		$virtual_classroom->default_layout = $data['default_layout'];
		$virtual_classroom->muteOnStart = $data['muteOnStart'];
		$virtual_classroom->allowModsToUnmuteUsers = $data['allowModsToUnmuteUsers'];
		$virtual_classroom->webcamsOnlyForModerator = $data['webcamsOnlyForModerator'];
		$virtual_classroom->lockSettingsDisableCam = $data['lockSettingsDisableCam'];
		$virtual_classroom->lockSettingsDisableMic = $data['lockSettingsDisableMic'];
		$virtual_classroom->lockSettingsDisablePrivateChat = $data['lockSettingsDisablePrivateChat'];
		$virtual_classroom->lockSettingsDisablePublicChat = $data['lockSettingsDisablePublicChat'];
		$virtual_classroom->lockSettingsDisableNote = $data['lockSettingsDisableNote'];
		$virtual_classroom->welcome = $data['welcome'];
		$virtual_classroom->moderatorOnlyMessage = $data['moderatorOnlyMessage'];
		$virtual_classroom->useCustomLogo = $data['useCustomLogo'];
		$virtual_classroom->customLogo = $data['customLogo'];
		$params->virtual_classroom = json_encode($virtual_classroom);
		$params->video = json_encode($video);
		$params->show_teachers_count = $data['show_teachers_count'];
		$params->show_lessons_count  = $data['show_lessons_count'];
		$params->show_pricing        = $data['show_pricing'];
		$params->show_discussions    = $data['show_discussions'];
		$params->show_ratings        = $data['show_ratings'];
		$params->transcripts_show_badges = $data['transcripts_show_badges'];
		$params->transcript_badges_text = $data['transcript_badges_text'];
		$params->transcript_badges_icon = $data['transcript_badges_icon'];
		$params->transcripts_show_courses = $data['transcripts_show_courses'];
		$params->transcript_courses_text = $data['transcript_courses_text'];
		$params->transcript_courses_icon = $data['transcript_courses_icon'];
		$params->transcripts_show_events = $data['transcripts_show_events'];
		$params->transcript_events_text = $data['transcript_events_text'];
		$params->transcript_events_icon = $data['transcript_events_icon'];
		$params->transcripts_full_course_data = $data['transcripts_full_course_data'];
		$params->transcripts_collapse = $data['transcripts_collapse'];
		$params->transcripts_show_certificates = $data['transcripts_show_certificates'];
		$params->transcript_certificates_text = $data['transcript_certificates_text'];
		$params->transcript_certificates_icon = $data['transcript_certificates_icon'];
		$params->transcripts_show_profile_cover = $data['transcripts_show_profile_cover'];
		$params->transcripts_cover_default = $data['transcripts_cover_default'];
		$params->transcripts_show_profile_picture = $data['transcripts_show_profile_picture'];
		$params->card_design = $data['card_design'];
		$params->show_user_points = $data['show_user_points'];
		$params->max_courses = $data['max_courses'];
		$params->allow_pdf_download = $data['allow_pdf_download'];
		$params->show_open_pdf_link = $data['show_open_pdf_link'];
		$params->course_carousel_enabled = $data['course_carousel_enabled'];


		$settings->id = 1;
		$settings->params = json_encode($params);
		$db = JFactory::getDbo();
		$success = $db->updateObject("axs_lms_settings",$settings,"id");

		$tracking = new stdClass();
		$tracking->eventName = 'LMS Settings Edit';
		$tracking->description = 'LMS Settings';
		AxsTracking::sendToPendo($tracking);
		$tracking->action_type ="admin";
		AxsActions::storeAdminAction($tracking);
		return $success;
	}

	public function loadFormData() {
		$settings = self::getData();
		$params = json_decode($settings->params);
		$video  = json_decode($params->video);
		$virtual_classroom = json_decode($params->virtual_classroom);
		$data['lesson_gating'] = $params->lesson_gating;
		$data['allow_downloads'] = $video->allow_downloads;
		$data['restart_button'] = $video->restart_button;
		$data['rewind_button'] = $video->rewind_button;
		$data['fast_forward_button'] = $video->fast_forward_button;
		$data['progress_button'] = $video->progress_button;
		$data['current_time'] = $video->current_time;
		$data['duration'] = $video->duration;
		$data['mute_button'] = $video->mute_button;
		$data['volume_slider'] = $video->volume_slider;
		$data['captions'] = $video->captions;
		$data['settings'] = $video->settings;
		$data['pip'] = $video->pip;
		$data['fullscreen_option'] = $video->fullscreen_option;
		$data['play_button'] = $video->play_button;
		$data['small_play_button'] = $video->small_play_button;
		$data['default_video_cover'] = $video->default_video_cover;
		$data['show_teachers_count'] = $params->show_teachers_count;
		$data['show_lessons_count']  = $params->show_lessons_count;
		$data['show_pricing']        = $params->show_pricing;
		$data['show_discussions']    = $params->show_discussions;
		$data['show_ratings']        = $params->show_ratings;
		$data['transcripts_collapse'] = $params->transcripts_collapse;
		$data['transcripts_full_course_data'] = $params->transcripts_full_course_data;
		$data['transcripts_show_badges'] = $params->transcripts_show_badges;
		$data['transcript_badges_text'] = $params->transcript_badges_text;
		$data['transcript_badges_icon'] = $params->transcript_badges_icon;
		$data['transcripts_show_courses'] = $params->transcripts_show_courses;
		$data['transcript_courses_text'] = $params->transcript_courses_text;
		$data['transcript_courses_icon'] = $params->transcript_courses_icon;
		$data['transcripts_show_events'] = $params->transcripts_show_events;
		$data['transcript_events_text'] = $params->transcript_events_text;
		$data['transcript_events_icon'] = $params->transcript_events_icon;
		$data['transcripts_show_certificates'] = $params->transcripts_show_certificates;
		$data['transcript_certificates_text'] = $params->transcript_certificates_text;
		$data['transcript_certificates_icon'] = $params->transcript_certificates_icon;
		$data['transcripts_show_profile_cover'] = $params->transcripts_show_profile_cover;
		$data['transcripts_cover_default'] = $params->transcripts_cover_default;
		$data['transcripts_show_profile_picture'] = $params->transcripts_show_profile_picture;
		$data['record'] = $virtual_classroom->record;
		$data['default_layout'] = $virtual_classroom->default_layout;
		$data['muteOnStart'] = $virtual_classroom->muteOnStart;
		$data['allowModsToUnmuteUsers'] = $virtual_classroom->allowModsToUnmuteUsers;
		$data['webcamsOnlyForModerator'] = $virtual_classroom->webcamsOnlyForModerator;
		$data['lockSettingsDisableCam'] = $virtual_classroom->lockSettingsDisableCam;
		$data['lockSettingsDisableMic'] = $virtual_classroom->lockSettingsDisableMic;
		$data['lockSettingsDisablePrivateChat'] = $virtual_classroom->lockSettingsDisablePrivateChat;
		$data['lockSettingsDisablePublicChat'] = $virtual_classroom->lockSettingsDisablePublicChat;
		$data['lockSettingsDisableNote'] = $virtual_classroom->lockSettingsDisableNote;
		$data['welcome'] = $virtual_classroom->welcome;
		$data['moderatorOnlyMessage'] = $virtual_classroom->moderatorOnlyMessage;
		$data['useCustomLogo'] = $virtual_classroom->useCustomLogo;
		$data['customLogo'] = $virtual_classroom->customLogo;
		$data['card_design'] = $params->card_design;
		$data['show_user_points'] = $params->show_user_points;
		$data['max_courses'] = $params->max_courses;
		$data['allow_pdf_download'] = $params->allow_pdf_download;
		$data['show_open_pdf_link'] = $params->show_open_pdf_link;
		$data['course_carousel_enabled'] = $params->course_carousel_enabled;
		
		return $data;
	}
}
