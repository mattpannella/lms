-- Create syntax for TABLE '#__splms_courses'
CREATE TABLE IF NOT EXISTS `#__splms_courses` (
  `splms_course_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `course_sub_title` varchar(55) NOT NULL,
  `splms_coursescategory_id` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) NOT NULL,
  `ref_url` varchar(255) NOT NULL,
  `featured_course` tinyint(1) NOT NULL DEFAULT '0',
  `price` float(8,2) NOT NULL,
  `download` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`splms_course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_coursescategories'
CREATE TABLE IF NOT EXISTS `#__splms_coursescategories` (
  `splms_coursescategory_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT '',
  `featured` tinyint(1) NOT NULL,
  `show` tinyint(1) NOT NULL,
  `icon` varchar(55) NOT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `asset_id` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_coursescategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_eventcategories'
CREATE TABLE IF NOT EXISTS `#__splms_eventcategories` (
  `splms_eventcategory_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `asset_id` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_eventcategory_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_events'
CREATE TABLE IF NOT EXISTS `#__splms_events` (
  `splms_event_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `image` varchar(255) DEFAULT '',
  `price` int(11) DEFAULT NULL,
  `splms_speaker_id` varchar(50) DEFAULT '',
  `splms_eventcategory_id` bigint(20) NOT NULL,
  `event_start_date` date NOT NULL,
  `event_end_time` time NOT NULL,
  `event_time` time NOT NULL,
  `event_end_date` date NOT NULL,
  `event_address` text,
  `enabled` tinyint(1) NOT NULL,
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map` text,
  PRIMARY KEY (`splms_event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_lessons'
CREATE TABLE IF NOT EXISTS `#__splms_lessons` (
  `splms_lesson_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL DEFAULT '',
  `description` text,
  `short_description` varchar(255) DEFAULT '',
  `splms_teacher_id` int(11) NOT NULL,
  `video_url` varchar(255) DEFAULT '',
  `vdo_thumb` text NOT NULL,
  `ordering` int(10) unsigned NOT NULL,
  `splms_course_id` int(11) unsigned NOT NULL,
  `video_duration` varchar(50) DEFAULT '',
  `attachment` varchar(255) DEFAULT '',
  `lesson_type` tinyint(1) NOT NULL DEFAULT '1',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_lesson_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_orders'
CREATE TABLE IF NOT EXISTS `#__splms_orders` (
  `splms_order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_user_id` bigint(20) NOT NULL,
  `splms_course_id` bigint(20) NOT NULL,
  `order_price` float(17,2) NOT NULL,
  `order_payment_id` varchar(255) NOT NULL,
  `invoice_id` varchar(255) NOT NULL,
  `order_payment_method` varchar(255) NOT NULL DEFAULT '',
  `order_payment_price` float NOT NULL,
  `order_duration` varchar(55) NOT NULL DEFAULT '',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` bigint(20) DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_discount_code` varchar(255) DEFAULT '',
  `order_discount_price` decimal(17,5) DEFAULT NULL,
  `order_discount_tax` decimal(17,5) DEFAULT NULL,
  `order_payment_currency` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`splms_order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_quizquestions'
CREATE TABLE IF NOT EXISTS `#__splms_quizquestions` (
  `splms_quizquestion_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `splms_course_id` int(11) DEFAULT NULL,
  `list_answers` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_quizquestion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_quizresults'
CREATE TABLE IF NOT EXISTS `#__splms_quizresults` (
  `splms_quizresult_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `point` int(11) DEFAULT NULL,
  `total_marks` int(11) DEFAULT NULL,
  `user_id` text NOT NULL,
  `splms_quizquestion_id` int(11) DEFAULT NULL,
  `splms_course_id` int(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`splms_quizresult_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_speakers'
CREATE TABLE IF NOT EXISTS `#__splms_speakers` (
  `splms_speaker_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '',
  `slug` varchar(50) NOT NULL DEFAULT '',
  `designation` varchar(50) DEFAULT '',
  `description` text,
  `image` varchar(255) DEFAULT '',
  `website` varchar(50) DEFAULT '',
  `email` varchar(100) DEFAULT '',
  `social_facebook` varchar(50) DEFAULT '',
  `social_twitter` varchar(50) DEFAULT '',
  `social_gplus` varchar(50) DEFAULT '',
  `social_linkedin` varchar(50) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_speaker_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Create syntax for TABLE '#__splms_teachers'
CREATE TABLE IF NOT EXISTS `#__splms_teachers` (
  `splms_teacher_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT '',
  `website` varchar(50) DEFAULT '',
  `email` varchar(100) DEFAULT '',
  `experience` varchar(255) NOT NULL,
  `specialist_in` text NOT NULL,
  `social_facebook` varchar(100) DEFAULT '',
  `social_twitter` varchar(100) DEFAULT '',
  `social_gplus` varchar(100) DEFAULT '',
  `social_linkedin` varchar(100) DEFAULT '',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `ordering` int(10) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` bigint(20) NOT NULL DEFAULT '0',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `locked_by` bigint(20) NOT NULL DEFAULT '0',
  `locked_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`splms_teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

