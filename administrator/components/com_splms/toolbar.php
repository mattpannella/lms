<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

class SplmsToolbar extends FOFToolbar{

	function onBrowse() {
		// JToolBarHelper::preferences('com_splms');
		parent::onBrowse();
	}

	function onCpanelsBrowse() {
		JToolBarHelper::title(JText::_('COM_SPLMS_NAME_CPANEL_TITLE_DASHBOARD'), 'SPLMS');
	}

    function onLessonsBrowse() {
        JToolBarHelper::title('Lessons');
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Lesson(s)');
    }

	function onLessonsEdit() {
		JToolBarHelper::title('Lessons: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy','Duplicate Lesson');
		JToolbarHelper::cancel();
	}

	function onSettingsBrowse() {
		JToolBarHelper::title('Settings');
		JToolbarHelper::save('save','Save','save');
		JToolbarHelper::cancel();
	}

	function onQuizresultsBrowse() {
		JToolBarHelper::title('Quiz Results');
		JToolbarHelper::deleteList();

	}

    function onCoursesBrowse() {
        JToolBarHelper::title('Courses');
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Course(s)');
    }

	function onCoursesEdit() {
		JToolBarHelper::title('Courses: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy','Duplicate Course');
		JToolbarHelper::cancel();
	}

	function onInteractiveitemsBrowse() {
		JToolBarHelper::title('Interactive Content');
	}

    function onQuizQuestionsBrowse() {
        JToolBarHelper::title('Quizzes');
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Quiz(zes)');
    }

    function onQuizQuestionsEdit() {
        JToolBarHelper::title('Quizzes: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy','Duplicate Quiz');
		JToolbarHelper::cancel();
    }

    function onCertificatesBrowse() {
        JToolBarHelper::title('Certificate Designer');
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Certificate(s)');
    }

    function onCertificatesEdit() {
		JToolBarHelper::title('Certificate Designer');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy','Duplicate Certificate');
		JToolbarHelper::cancel();
	}
}