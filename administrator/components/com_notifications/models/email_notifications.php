<?php

defined('_JEXEC') or die;

class NotificationsModelEmail_Notifications extends FOFModel {
	public function save($data) {

		$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : null;

        $params = new stdClass();
        $params->recipients_bind_conditions = $data['recipients_bind_conditions'];
        $params->report_bind_conditions = $data['report_bind_conditions'];
        $params->recipients_conditions = json_encode($data['recipients_conditions']);
        $params->report_conditions = json_encode($data['report_conditions']);
        $params->report_binding = json_encode($data['report_binding']);
        $params->report_type = $data['report_type'];
        $params->report_courses_select = $data['report_courses_select'];
        $params->report_courses = !empty($data['report_courses']) ? implode(',', $data['report_courses']) : null;
        $params->report_courses_status_types = !empty($data['report_courses_status_types']) ? implode(',', $data['report_courses_status_types']) : null;
        $params->member_access_type = $data['member_access_type'];
        $params->member_userlist = $data['member_userlist'];
        $params->additional_emails = $data['additional_emails'];
        $params->member_usergroup = !empty($data['member_usergroup']) ? implode(',', $data['member_usergroup']) : null;


        $data['interval_number'] = (int)$data['interval_number'];
        if ($data['interval_number'] < 1) {
            $data['interval_number'] = 1;
        }

        $params->interval_number = $data['interval_number'];
        $params->interval_type = $data['interval_type'];
        
        if ($data['schedule_use_day_of_the_week'] == "1") {
            $params->schedule_use_day_of_the_week = true;
        } else {
            $params->schedule_use_day_of_the_week = false;
        }

        if ($data['schedule_use_day_of_the_month'] == "1") {
            $params->schedule_use_day_of_the_month = true;
        } else {
            $params->schedule_use_day_of_the_month = false;
        }

        /*
        if ($data['schedule_use_week_of_the_month'] == "1") {
            $params->schedule_use_week_of_the_month = true;
        } else {
            $params->schedule_use_week_of_the_month = false;
        }
        */

        if ($data['schedule_use_month_of_the_year'] == "1") {
            $params->schedule_use_month_of_the_year = true;
        } else {
            $params->schedule_use_month_of_the_year = false;
        }

        $params->schedule_day_of_the_week = $data['schedule_day_of_the_week'];
        $params->schedule_day_of_the_month = $data['schedule_day_of_the_month'];
        $params->schedule_week_of_the_month = $data['schedule_week_of_the_month'];
        $params->schedule_month_of_the_year = $data['schedule_month_of_the_year'];

        if (!$data['start_date']) {
            $data['start_date'] = "2000-01-01 00:00:00";
        }

        if (!$data['end_date']) {
            $data['end_date'] = "3000-12-31 23:59:59";
        }
        
        if ($data['include_date'] == "1") {
            $params->include_date = true;
        } else {
            $params->include_date = false;
        }

        $data['send_time'] = self::convertTime($data['send_time'], true);

        $data['params'] = json_encode($params);
        return parent::save($data);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data = $data;
            $data['usergroup'] = explode(',', $data['usergroup']);
            $params = json_decode($data['params']);

            $data['recipients_bind_conditions'] = $params->recipients_bind_conditions;
            $data['report_bind_conditions'] = $params->report_bind_conditions;
            $data['recipients_conditions'] = $params->recipients_conditions;
            $data['report_conditions'] = $params->report_conditions;
            $data['report_type'] = $params->report_type;
            $data['report_courses_select'] = $params->report_courses_select;
            $data['report_courses'] = explode(',', $params->report_courses);
            $data['report_courses_status_types'] = explode(',', $params->report_courses_status_types);
            $data['member_access_type'] = $params->member_access_type;
            $data['member_userlist'] = $params->member_userlist;
            $data['additional_emails'] = $params->additional_emails;
            $data['member_usergroup'] = explode(',', $params->member_usergroup);
            $data['interval_number'] = $params->interval_number;
            $data['interval_type'] = $params->interval_type;
            $data['report_binding'] = $params->report_binding;

            if ($params->schedule_use_day_of_the_week) {
                $data['schedule_use_day_of_the_week'] = "1";
            } else {
                $data['schedule_use_day_of_the_week'] = "0";
            }

            if ($params->schedule_use_day_of_the_month) {
                $data['schedule_use_day_of_the_month'] = "1";
            } else {
                $data['schedule_use_day_of_the_month'] = "0";
            }

            /*
            if ($params->schedule_use_week_of_the_month) {
                $data['schedule_use_week_of_the_month'] = "1";
            } else {
                $data['schedule_use_week_of_the_month'] = "0";
            }
            */

            if ($params->schedule_use_month_of_the_year) {
                $data['schedule_use_month_of_the_year'] = "1";
            } else {
                $data['schedule_use_month_of_the_year'] = "0";
            }

            $data['schedule_day_of_the_week'] = $params->schedule_day_of_the_week;
            $data['schedule_day_of_the_month'] = $params->schedule_day_of_the_month;
            $data['schedule_week_of_the_month'] = $params->schedule_week_of_the_month;
            $data['schedule_month_of_the_year'] = $params->schedule_month_of_the_year;

            if ($data['start_date'] == "2000-01-01 00:00:00") {
                $data['start_date'] = "";
            }
            if ($data['end_date'] == "3000-12-31 23:59:59") {
                $data['end_date'] = "";
            }

            if ($params->include_date) {
                $data['include_date'] = "1";
            } else {
                $data['include_date'] = "0";
            }

            $data['send_time'] = self::convertTime($data['send_time'], false);
            //$data['include_date'] = $params->include_date;

            return $data;
		}
	}

    private function convertTime($send_time, $encode) {

        if ($encode) {
            
            $time = json_decode($send_time);

            $string = "";

            if ($time->hour < 10) {
                $string .= "0";                
            }
                
            $string .= $time->hour . ":";

            if ($time->minute < 10) {
                $string .= "0";
            }
                
            $string .= $time->minute . ":00 " . strtoupper($time->ampm);

            return $string;

        } else {
            $time = new stdClass();

            $time->hour = (int)substr($send_time, 0, 2);
            $time->minute = (int)substr($send_time, 3, 2);
            $time->ampm = strtolower(substr($send_time, 9, 2));

            return $time;
        }


    }
}