<?php

defined('_JEXEC') or die;

class NotificationsModelPopup_Notifications extends FOFModel {


	public function save($data) {

		$buttons = json_decode(json_encode($data['buttons']));
		foreach ($buttons as $button) {
			if (!$button->id) {
				$button->id = AxsLMS::createUniqueID();
			}
		}

		$data['accesslevel'] = !empty($data['accesslevel']) ? implode(',', $data['accesslevel']) : 0;
		$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : 0;

		$params = new stdClass();
		$params->geofence = $data['geofence'];
		$params->radius_amount = $data['radius_amount'];
		$params->radius_unit = $data['radius_unit'];
		$params->latitude = $data['latitude'];
		$params->longitude = $data['longitude'];
		$params->address = $data['address'];
		$params->display_type = $data['display_type'];
		$params->display_title = $data['display_title'];
		$params->display_always = $data['display_always'];
		$params->display_delay_buttons = $data['display_delay_buttons'];
		$params->buttons = json_encode($buttons);
		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {

			$data = $data;
			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);
			$params = json_decode($data['params']);
			$data['geofence'] = $params->geofence;

			$geofield = new stdClass();
			$geofield->radius = $params->radius_amount;			
			$geofield->address = $params->address;
			$geofield->latitude = $params->latitude;
			$geofield->longitude = $params->longitude;
			$data['geofield'] = $geofield;
			$data['radius_unit'] = $params->radius_unit;

			$data['display_type'] = $params->display_type;
			$data['display_title'] = $params->display_title;
			$data['display_always'] = $params->display_always;
			$data['display_delay_buttons'] = $params->display_delay_buttons;
			$data['buttons'] = $params->buttons;
			return $data;
			
		}
	}
}