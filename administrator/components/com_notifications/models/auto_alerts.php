<?php

defined('_JEXEC') or die;

class NotificationsModelAuto_Alerts extends FOFModel {
	public function save($data) {

        $params = new stdClass();

        // Get Brand information
        $brandId = $data['brand_id'];
        $notificationType = $data['alert_type'];

        if($notificationType == 'new user welcome email') {

            /* We are setting the alert to apply to all brands, so make sure that there are no other new user
             * welcome email notifications enabled; this is because we are sticking to the 1:1 correspondence of 
             * brand to welcome email notification
             */

            // NB: This notification type will always be 'new user welcome email'
            $newUserWelcomeNotifications = AxsNotifications::getNotificationsByType($notificationType);
            $newUserWelcomeNotifications = array_filter($newUserWelcomeNotifications, function($el) use ($data) {
                return $el->id != $data['id'];
            });
            $reject = false;

            // If we are setting this auto_alert to global
            if ($brandId == '0') {
                // Check that no other new user welcome email notifications exist
                $reject = count($newUserWelcomeNotifications) > 0;
            } else {
                // Check if there is another notification configured for this brand
                foreach ($newUserWelcomeNotifications as $notification) {
                    if ($notification->brand_id == $brandId) {
                        $reject = true;
                    }
                }
            }

            // There are other new user welcome email notifications, so a global setting shouldn't apply here
            if($reject) {

                $totalNotifications = count($newUserWelcomeNotifications);

                if($totalNotifications > 0) {

                    JFactory::getApplication()->enqueueMessage('You can only create one new user welcome email notification per brand.<br />Please select another brand and try again, or edit the existing notification for the selected brand.', 'error');

                    return false;
                }
            }

            // Get any notifications that exist for this brandId and alert_type that DON'T match the current notification ID.
            $notificationsForBrand = AxsNotifications::getNotificationsForBrand($brandId, $notificationType, false);

            $notification = $notificationsForBrand[0];
            // If this is an existing or new notification and there are existing notifications for this brand
            if(($notification->id != $data['id'] || empty($data['id'])) && count($notificationsForBrand) > 0) {
                // Reject the form submission
                JFactory::getApplication()->enqueueMessage('You can only create one new user welcome email notification per brand.<br />Please select another brand and try again, or edit the existing notification for the selected brand.', 'error');

                return false;
            }
        }

        $coursesElementName = $data['full_selector_name'];
        $data['courses'] = implode(',',$data["$coursesElementName"]);
        $data['certificates_badges'] = implode(',',$data['certificates_badges']);
        $data['subscription_plans'] = implode(',',$data['subscription_plans']);
        $params->subject = $data['subject'];
        $params->subscription_registration_message = $data['subscription_registration_message'];
        $params->course_registration_message = $data['course_registration_message'];
        $params->course_purchase_message = $data['course_purchase_message'];
        $params->course_assigned_message = $data['course_assigned_message'];
        $params->course_recommended_message = $data['course_recommended_message'];
        $params->course_due_message = $data['course_due_message'];
        $params->certificate_expiration_message = $data['certificate_expiration_message'];
        $params->assignment_due_message = $data['assignment_due_message'];
        $params->course_assignments = $data['course_assignments'];

        // Welcome email configuration fields
        $params->welcome_email_subject = $data['welcome_email_subject'];
        $params->welcome_email_body = $data['welcome_email_body'];
        $data['params']  = json_encode($params);

        return parent::save($data);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {

            $params = json_decode($data['params']);
            $data['certificates_badges'] = explode(',',$data['certificates_badges']);
            $data['subscription_plans'] = explode(',',$data['subscription_plans']);
            $data['subject'] = $params->subject;
            $params = $this->getEmailTemplate($params);
            $data['subscription_registration_message'] = $params->subscription_registration_message;
            $data['course_registration_message'] = $params->course_registration_message;
            $data['course_purchase_message'] = $params->course_purchase_message;
            $data['course_assigned_message'] = $params->course_assigned_message;
            $data['course_recommended_message'] = $params->course_recommended_message;
            $data['course_due_message'] = $params->course_due_message;
            $data['certificate_expiration_message'] = $params->certificate_expiration_message;
            $data['assignment_due_message'] = $params->assignment_due_message;
            $data['course_assignments'] = $params->course_assignments;

            // Load the welcome email data
            $data['welcome_email_subject'] = $params->welcome_email_subject;
            $data['welcome_email_body'] = $params->welcome_email_body;

            return $data;
		}
	}

    private function getEmailTemplate($params) {
        if(empty($params)) {
            $params = new stdClass();
        }

        if(empty($params->course_registration_message)) {
            $params->course_registration_message = "
            Hi [name], <br/><br/>
            You have successfully been registered or for [course].";
        }
        if(empty($params->course_purchase_message)) {
            $params->course_purchase_message = "
            Hi [name], <br/><br/>
            Your payment was successful for the following course:
            <br/><br/>
            <b>Title:</b> [course]
            <br/><br/>
            <b>Amount:</b> [course_purchase_amount]
            <br/><br/>
            <b>Date:</b> [course_purchase_date]";
        }
         if(empty($params->subscription_registration_message)) {
            $params->subscription_registration_message = "
            Hi [name], <br/><br/>
            You have successfully been registered or for [subscription].";
        }
        if(empty($params->course_assigned_message)) {
            $params->course_assigned_message = "
            Hi [name], <br/><br/>
            You have been assigned to take the following courses: <br/>
            [courselist]";
        }
        if(empty($params->course_recommended_message)) {
            $params->course_recommended_message = "
            Hi [name], <br/><br/>
            You have been recommended to take the following courses: <br/>
            [courselist]";
        }
        if(empty($params->course_due_message)) {
            $params->course_due_message = "
            Hi [name], <br/><br/>
            [course] is due on: [course_due_date].";
        }
        if(empty($params->certificate_expiration_message)) {
            $params->certificate_expiration_message = "
            Hi [name], <br/><br/>
            Your [certificate] is going to expire on: [expiration_date].";
        }
        if(empty($params->assignment_due_message)) {
            $params->assignment_due_message = "
            Hi [name], <br/><br/>
            Course assignment [assignment_title] is due on: [assignment_due_date]. <br/>
            This assignment includes the following courses:
            [courselist]";
        }

        // Default welcome email template
        
        // Get the default subject, if any
        $legacyUserSettings = AxsUser::getGlobalUserSettings();

        if(!isset($params->welcome_email_subject) || empty($params->welcome_email_subject)) {

            if(!empty($legacyUserSettings->subject)) {

                $params->welcome_email_subject = $legacyUserSettings->subject;
            } else {
                
                $params->welcome_email_subject = 'New User Details';
            }
        }

        if(!isset($params->welcome_email_body) || empty($params->welcome_email_body)) {

            if(!empty($legacyUserSettings->body)) {
                $params->welcome_email_body = $legacyUserSettings->body;
            } else {
                $params->welcome_email_body = "Hello [name],<br /><br />
                You have been added as a User to [website_name] by an Administrator.<br /><br />
                This email contains your username and password to log in to [website_link]<br /><br />
                Username: [username]<br />
                Password: [password]<br /><br /><br />
                Please do not respond to this message as it is automatically generated and is for information purposes only.";
            }            
        }

        return $params;
    }
}