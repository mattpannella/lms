<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldgeofence extends JFormField
{

	protected $type = 'geofence';
	
	protected function getInput() {
		ob_start();
		require 'components/com_notifications/templates/geofence.php';
		return ob_get_clean();
	}

}