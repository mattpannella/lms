<?php

defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class JFormFieldCommunityFields extends JFormField {

	protected function getInput() {

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('#__community_fields'))
			->where($db->quoteName('published') . '=1');

		$db->setQuery($query);
		$results = $db->loadObjectList();

		$nameArray = array();
		$splitter = "&&&&";
		foreach ($results as $result) {
			$nameArray[$result->id] = $result->id . $splitter . JText::_($result->name);
		}

		sort($nameArray, SORT_STRING);

		ob_start();

		?>
			<select multiple="true" style="height: 200px;">
		<?php
				foreach ($nameArray as $name) {
					//echo $name . "<br>";
					$data = explode($splitter, $name);
					?>
						<option value="<?php echo $data[0];?>"><?php echo $data[1];?></option>
					<?php
				}
			?>
			</select>
		<?php
		/*
		?>
			<select>

				<?php
					foreach ($nameArray as $name) {
				?>

				<?php
					}
				?>
			</select>
		<?php
		*/


		return ob_get_clean();
	}

}
