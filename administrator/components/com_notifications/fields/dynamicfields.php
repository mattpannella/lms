<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldDynamicfields extends JFormField {

	protected function getInput() {
		ob_start();

		?>
			<style>
				.dynamic_fields {
					word-spacing: 15px;
				}	
			</style>
			<span class="dynamic_fields">
				[name] [username] [subscription] [course] [course_purchase_amount] [course_purchase_date] [courselist] [certificate] [badge] [assigned_start_date] [assigned_end_date] [assigned_due_date] [course_due_date] [expiration_date] [assignment_title] [assignment_due_date]
			</span>
			<br/>
			<b>Use these placeholders in your email messages to pull in dynamic data, such as the user's name or the course title.</b>

		<?php

		return ob_get_clean();
	}
}