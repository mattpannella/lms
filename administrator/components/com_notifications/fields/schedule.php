<?php

defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class JFormFieldSchedule extends JFormField {



	public function getRepeatable() {

		$id = $this->item->get('id');

		ob_start();

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName("axs_notifications_emails"))
			->where($db->quoteName("id") . '=' . $db->quote($id))
			->limit(1);
			
		$db->setQuery($query);

		$result = $db->loadObject();

		switch ($result->schedule_type) {
			case "date":
				echo "Date: " . date("Y-m-d", strtotime($result->send_date));
				break;
			case "schedule":
			case "interval":
				$range = "<table><tr><td>Start:</td>";
				//echo $result->start_date;
				if ($result->start_date == "2000-01-01 00:00:00") {
					$range .= "<td>Now</td>";
				} else {
					$range .= "<td>" . date("Y-m-d", strtotime($result->start_date)) . "</td>";
				}

				$range .= "</tr><tr><td>End:</td>";

				//echo $result->end_date;
				if ($result->end_date == "3000-12-31 23:59:59") {
					$range .= "<td>Indefinite</td>";
				} else {
					$range .= "<td>" . date("Y-m-d", strtotime($result->end_date)) . "</td>";
				}

				$range .= "</tr></table>";
				
				echo $range;
		}

		return ob_get_clean();
	}

}
