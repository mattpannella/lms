<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldAlertinterval extends JFormField {

	private function getRow($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName("axs_auto_alerts"))
			->where($db->quoteName("id") . '=' . $db->quote($id))
			->limit(1);				
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	protected function getInput() {
		$input = JFactory::getApplication()->input;
        $id = $input->get('id', null, 'INT');
		if($id) {
			$result = self::getRow($id);
			$interval_amount = $result->interval_amount;
			$interval_type   = $result->interval_type;
			$interval_time   = $result->interval_time;
		}

		ob_start();

		?>
			<div class="row">
				<div class="col-auto">
					<input type="number" name="interval_amount" style="width:60px;" value="<?php if($interval_amount) { echo $interval_amount; } else { echo '5'; }?>"/>
				</div>
				<div class="col-auto">
					<select name="interval_type" style="width:90px;">					
						<option value="days" <?php if($interval_type == 'days') { echo 'selected'; }?>>Days</option>
						<option value="weeks" <?php if($interval_type == 'weeks') { echo 'selected'; }?>>Weeks</option>
						<option value="months" <?php if($interval_type == 'months') { echo 'selected'; }?>>Months</option>
						<option value="years" <?php if($interval_type == 'years') { echo 'selected'; }?>>Years</option>
					</select>
				</div>
				<div class="col-auto">
					<select name="interval_time" style="width:100px;">
						<option value="before" <?php if($interval_time == 'before') { echo 'selected'; }?>>Before</option>
						<option value="after" <?php if($interval_time == 'after') { echo 'selected'; }?>>After</option>
					</select>
				</div>
			</div>

		<?php

		return ob_get_clean();
	}

	public function getRepeatable() {
		$id = $this->item->get('id');
		if($id) {
			$result = self::getRow($id);
			$interval_amount = $result->interval_amount;
			$interval_type   = $result->interval_type;
			$interval_time   = $result->interval_time;
			if($interval_amount == 1) {
				$interval_type = str_replace('s', '', $interval_type);
			}
			return $interval_amount.' '.$interval_type.' '.$interval_time; 
		}
	}
}