<?php

defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class JFormFieldInterval extends JFormField {



	public function getRepeatable() {
		$days = [
			1 => 'Monday',
			2 => 'Tuesday',
			3 => 'Wednesday',
			4 => 'Thursday',
			5 => 'Friday',
			6 => 'Saturday',
			7 => 'Sunday'
		];

		$id = $this->item->get('id');

		ob_start();

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName("axs_notifications_emails"))
			->where($db->quoteName("id") . '=' . $db->quote($id))
			->limit(1);
			
		$db->setQuery($query);

		$result = $db->loadObject();

		$params = json_decode($result->params);
		$num = $params->interval_number;
		$type = ucfirst($params->interval_type);

		$weekDays = [];
		switch ($result->schedule_type) {
			case "date":
				echo "Single";
				break;
			case "schedule":
				if ($params->schedule_use_day_of_the_week == false && $params->schedule_use_day_of_the_month == false) {
					echo "Daily";
				} else if ($params->schedule_use_day_of_the_week == true && $params->schedule_use_day_of_the_month == false) {
					foreach($params->schedule_day_of_the_week as $dayOfWeek){
						$weekDays []= $days[$dayOfWeek];
					}
										
					echo "Weekly (" . implode(', ', $weekDays) . ")";
				} else if ($params->schedule_use_day_of_the_week == false && $params->schedule_use_day_of_the_month == true) {
					echo "Monthly";
				} else {
					echo "Other";
				}
				break;
			case "interval":
				if ($num > 1) {
					echo "$num $type";
				} else {
					echo $num . " " . substr($type, 0, -1);
				}
		}

		return ob_get_clean();
	}

}
