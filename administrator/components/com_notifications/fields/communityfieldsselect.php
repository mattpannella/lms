<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldcommunityfieldsselect extends JFormField
{

	protected $type = 'communityfieldsselect';

	public function getCommunityFields() {
		$db = JFactory::getDbo();
		$query = 
		"SELECT * 
			FROM joom_community_fields 
			WHERE published = 1 
			AND type != 'group' 
			ORDER BY ordering ASC
		";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}
	
	protected function getInput() {

		$lang = JFactory::getLanguage();
		$lang->load("com_community", JPATH_SITE, "en-GB", true);

		if ($this->required) {
			$required = ' required aria-required="true"';
		} else {
			$required = '';
		}

		ob_start();
		?>

		<select 
			name="<?php echo $this->name; ?>" 
			id="<?php echo $this->id; ?>"			
			<?php echo $required;?>
		>

		<?php
			$options = '';
			$fields = self::getCommunityFields();
			foreach($fields as $field) {
                $selected = '';
                if ($this->value == $field->id) {
                    $selected = 'selected';
                }
				$options .= '<option value="'.$field->id.'" '. $selected .'>'.JText::_($field->name).'</option>';
			}

			echo $options;
		?>
			

		</select>

		<?php

		return ob_get_clean();

	}
}