<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class NotificationsToolbar extends FOFToolbar {
	function onMessagesBrowse() {
		JToolBarHelper::addNew();
		JToolBarHelper::editList('editItem');
		JToolBarHelper::publishList();
		JToolBarHelper::unpublishList();
		JToolBarHelper::deleteList();
		JToolBarHelper::title('Notifications');
	}

	function onEmail_NotificationsEdit() {
		JToolBarHelper::title("Email Notifications: Edit");
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}
}