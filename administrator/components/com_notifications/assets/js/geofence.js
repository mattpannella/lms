function initMap() {
    var radiusAmount = null;
    var circle  = new google.maps.Circle();
    var zoom    = 12;
    var radius  = null;
    var unit    = null;
    var lat     = null;
    var lng     = null;

    function setLatNLng() {
        lat = parseFloat(jQuery('#latitude').val());
        lng = parseFloat(jQuery('#longitude').val());
    }

    setLatNLng();

    if(!lat || !lng) {
        var lat = 32.8506028;
        var lng = -117.27142630000003;
    }
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: lat, lng: lng},
      zoom: 12
    });

    function getRadius() {
        radius  = parseFloat(jQuery('#radius_amount').val());
        unit    = jQuery('#radius_unit').val();

        switch(unit) {
            case 'mi': 
                radiusAmount = radius * 1609.344;
                radiusText   = 'Mile';
                zoom         = 12;
            break;

            case 'km': 
                radiusAmount = radius * 1000;
                radiusText   = 'Kilometer';
                zoom         = 12;
            break;

            case 'm': 
                radiusAmount = radius;
                radiusText   = 'Meter';
                zoom         = 15;
            break;

            case 'ft': 
                radiusAmount = radius * 0.3048;
                radiusText   = 'Feet';
                zoom         = 15;
            break;

            default: 
                radiusAmount = radius * 1609.344;
                radiusText   = 'Mile';
                zoom         = 12;
            break;

        }
        jQuery('.radius_unit_text').text(radiusText);
        map.setZoom(zoom);
        return radiusAmount;
    }

    function setCircle(lat,lng) {
        circle.setMap(null);
        circle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                draggable:false,
                center: {lat: lat, lng: lng},
                radius: getRadius()
            });
    }

    if( lat && lng ) {
        setCircle(lat,lng);
    }

    jQuery('#radius_amount').bind('keyup change', function() {
        setLatNLng();
        setCircle(lat,lng);
    });

    jQuery('#radius_unit').bind('keyup change', function() {
        setLatNLng();
        setCircle(lat,lng);
    });

    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var types = document.getElementById('type-selector');
    //var strictBounds = document.getElementById('strict-bounds-selector');

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

    var autocomplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
      	map: map,
      	anchorPoint: new google.maps.Point(0, -29)
    });

    function setLocation() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        jQuery('#latitude').val(lat);
        jQuery('#longitude').val(lng);

        if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
        map.setZoom(12);
        } else {
        map.setCenter(place.geometry.location);
        map.setZoom(12);  // Why 17? Because it looks good.
        }
        var radius = jQuery('#radius_amount').val();
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        setCircle(lat,lng);
        var address = '';
        if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
        }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    }

    autocomplete.addListener('place_changed', function() {
        setLocation();
    });

 
}