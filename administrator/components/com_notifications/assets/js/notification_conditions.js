
jQuery(document).ready(
	function() {

		add_gender_toggle();

		jQuery(".group-add").click(
			function() {
				add_gender_toggle();
			}
		);

	}
);


/*
	This function is necessary because, unlike all of the other custom community fields, Gender uses a language file text.
	So, male is COM_COMMUNITY_MALE and female is COM_COMMUNITY_FEMALE.
	Since the conditions are checked by string comparision, no one is going to be typing COM_COMMUNITY_MALE into the field.

	This makes it so that if the Gender field is selected, it swaps out the text field for a drop down list that contains male
	and female.  Then, depending on which one is currently selected, the actual (hidden) input field is updated with the 
	visible input's value.
*/

function add_gender_toggle() {
	//On an update, this is called before the divs are actually created, so there needs to be a small delay.
	setTimeout(
		function() {

			//Add it to any new buttons too.
			jQuery(".group-add").click(
				function() {
					add_gender_toggle();
				}
			);

			jQuery(".subform-repeatable-group").each(
				function() {

					//Don't allow it to be added more than once.
					if (this.getAttribute("has_gender_toggle") == "true") {
						return;
					} else {
						this.setAttribute("has_gender_toggle", "true");
					}

					//var name = "recipients_conditions";
					var names = ["recipients_conditions", "report_conditions"];
					//console.log(this.getAttribute("data-base-name"));
					//console.log(names.indexOf(this.getAttribute("data-base-name")));

					var index = names.indexOf(this.getAttribute("data-base-name"));

					//if (this.getAttribute("data-base-name") == name) {
					if (index >= 0) {

						let name = names[index];

						//If the ids are created on page load, they have two underscores.  If they're created on page load, they have one.
						
						let group = this.getAttribute("data-group");
						var id_pre = name + "_" + group + "_";

						let field = this.getElementById(id_pre + "field");
						if (!field) {
							field = this.getElementById(id_pre + "_field");
						}

						if (!field.getAttribute('value_field')) {

							let value = this.getElementById(id_pre + "value");
							if (!value) {
								value = this.getElementById(id_pre + "_value");
							}

							field.setAttribute('value_field', value.id);

							let value_parent = value.parentNode;

							let new_input = document.createElement('input');
							new_input.type = "text";
							new_input.setAttribute("aria-invalid", false);

							value_parent.appendChild(new_input);

							let select_list = document.createElement('select');
							let male_option = document.createElement('option');
							let female_option = document.createElement('option');

							select_list.appendChild(male_option);
							select_list.appendChild(female_option);
							value_parent.appendChild(select_list);

							male_option.value = "COM_COMMUNITY_MALE";
							female_option.value = "COM_COMMUNITY_FEMALE";

							male_option.appendChild(document.createTextNode("Male"));
							female_option.appendChild(document.createTextNode("Female"));

							function changeFields(val, first) {
								if (val == 2) {
									jQuery(new_input).hide();
									jQuery(select_list).show();
									if (first) {
										select_list.value = value.value;
									} else {
										value.value = select_list.value;
									}
								} else {
									jQuery(new_input).show();
									jQuery(select_list).hide();
									if (first) {
										new_input.value = value.value;
									} else {
										value.value = new_input.value;
									}
								}
							}
							
							jQuery(value).hide();

							changeFields(field.value, true);
							jQuery(field).change(
								function() {
									changeFields(field.value, false);
								}
							);

							jQuery(select_list).change(
								function() {
									value.value = select_list.value;
								}
							);

							jQuery(new_input).on("keyup paste change",
								function() {
									value.value = new_input.value;
								}
							);
						}
					}
				}
			);
		}, 200
	);

	/*
		Even though the interval time is designated an integer, and will get a "red border" if a decimal isn't used,
		it will still allow one and will save and load it as such.  This will wait until the user has stopped typing and then
		return it to a whole number.
	*/
	jQuery("#interval_number").on("keyup paste change",
		function() {
			var _this = this;
			setTimeout(
				function() {
					_this.value = parseInt(_this.value);

				}, 1000
			);
		}
	);
}