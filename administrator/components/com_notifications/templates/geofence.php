<link href="/administrator/components/com_notifications/assets/css/geofence.css" rel="stylesheet" type="text/css" />

<div class="pac-card" id="pac-card">
	<div>
		<div id="geofence-title">
		Geofence System
		</div>
	</div>
	<div id="pac-container">
		<div class="d-flex" style="padding: 10px;">
			<div class="input-group float-start">
				<input type="number" class="form-control" id="radius_amount" name="radius_amount" value="<?php echo $this->value->radius; ?>">
				<span class="input-group-text bg-primary text-white"> <i class="fa fa-bullseye"></i> <span class="radius_unit_text">Mile</span>   Radius</span>
			</div>
			<div class="input-group float-start">
				<input id="pac-input" class="form-control geofence_address" name="address" type="text" placeholder="Enter a location" value="<?php echo $this->value->address; ?>">
				<span class="input-group-text bg-primary text-white"> <i class="fa fa-map-marker" style="margin-left:10px;"></i>   Address</span>
			</div>
		</div>
	</div>
	<div id="map"></div>
	<div id="infowindow-content">
		<img src="" width="16" height="16" id="place-icon">
		<span id="place-name"  class="title"></span><br>
		<span id="place-address"></span>
	</div>
</div>
<input id="latitude" name="latitude" type="hidden" value="<?php echo $this->value->latitude; ?>">
<input id="longitude" name="longitude" type="hidden" value="<?php echo $this->value->longitude; ?>">  

<script src="/administrator/components/com_notifications/assets/js/geofence.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAp7ydJCwvDe68lKTRnklpITFcdKHCDVzs&libraries=places,drawing&callback=initMap" async defer></script>