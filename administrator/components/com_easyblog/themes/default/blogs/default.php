<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<form action="index.php?option=com_easyblog" method="post" name="adminForm" id="adminForm" data-grid-eb>
	<?php
		$filters = [
			$this->getFilterState($filter_state),
			$this->getFilterCategory($filter_category),
			$this->getFilterBlogger($filterBlogger)
		];
		echo $this->html('filter.box', $search, $filters, $pagination->getLimitBox());
	?>
	<div class="panel-table">
		<table class="app-table app-table-middle table table-striped table-eb" data-table-grid>
			<thead>
				<tr>
					<th width="1%" class="nowrap hidden-phone center text-center">
						<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);">
					</th>
					<th class="text-start">
						<?php echo JHTML::_('grid.sort', JText::_('COM_EASYBLOG_BLOGS_BLOG_TITLE'), 'a.title', $orderDirection, $order ); ?>
					</th>
					<?php if( !$browse ): ?>
					<th class="nowrap hidden-phone center text-center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_FEATURED' ); ?>
					</th>
					<th class="nowrap hidden-phone center text-center">
						<?php echo JText::_( 'COM_EASYBLOG_STATUS' ); ?>
					</th>
					<th class="nowrap hidden-phone center text-center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_FRONTPAGE' ); ?>
					</th>
					<th class="nowrap hidden-phone center text-center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_HITS' ); ?>
					</th>

					<!-- <th width="10%" class="nowrap hidden-phone center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_CONTRIBUTED_IN' ); ?>
					</th>
					<th width="2%" class="nowrap hidden-phone center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_AUTOPOSTING' ); ?>
					</th> -->
					<th width="1%" class="nowrap hidden-phone text-center">
						<?php echo JText::_( 'COM_EASYBLOG_BLOGS_NOTIFY' ); ?>
					</th>
					<th width="20%" class="nowrap center text-center hidden-phone">
						<?php echo JHTML::_('grid.sort', 'COM_EASYBLOG_DATE', 'a.created', $orderDirection, $order ); ?>
					</th>
					<th width="1%" nowrap="nowrap center text-center">
						<?php echo JHTML::_('grid.sort', 'COM_EASYBLOG_ID', 'a.id', $orderDirection, $order ); ?>
					</th>
					<?php endif; ?>
				</tr>
			</thead>
			<tbody>
				<?php if( $blogs ): ?>
					<?php $i = 0; ?>

					<?php foreach ($blogs as $row): ?>
						<tr data-item data-id="<?php echo $row->id;?>" data-title="<?php echo $row->title;?>">
                           	<td class="center text-center align-middle hidden-iphone" valign="top">
						   		<?php echo $this->html('grid.id', $i++ , $row->id); ?>
                           	</td>
                           	<td class="nowrap has-context">
                              	<div style="max-width: 280px; overflow: hidden; white-space: nowrap; font-size: 14px; font-weight: bold; text-overflow: ellipsis;">
									<?php if ($row->isFromFeed()): ?>
										<i class="fa fa-rss-square"
											data-bs-toggle="tooltip"
											data-title="<?php echo JText::_('COM_EASYBLOG_BLOG_POST_IS_IMPORTED_FROM_FEEDS', true); ?>"
											data-placement="bottom"
										></i>&nbsp;
									<?php endif; ?>

									<?php if ($browse): ?>
										<a href="javascript:void(0);" data-post-title><?php echo $row->title;?></a>
									<?php else: ?>
										<a href="<?php echo $row->getEditLink();?>" target="_blank" data-eb-composer><?php echo $row->title; ?></a>
									<?php endif; ?>
                              </div>
                              <div style="max-width: 280px; white-space: initial; text-overflow: ellipsis;">
							  	<span class="me-2">
									<i class="fa fa-user text-muted"></i>&nbsp;
									<?php if( !$browse ): ?>
										<a href="<?php echo JRoute::_('index.php?option=com_easyblog&c=user&id=' . $row->created_by . '&task=edit'); ?>"><?php echo JFactory::getUser($row->created_by)->name; ?></a>
									<?php else: ?>
										<?php echo JFactory::getUser($row->created_by)->name; ?>
									<?php endif; ?>
								</span>

								<?php foreach ($row->getCategories() as $category): ?>
									<span class="me-2">
										<i class="fa fa-folder text-muted"></i>&nbsp; <?php echo $category->getTitle(); ?>
									</span>
								<?php endforeach; ?>

								<span class="me-2">
									<i class="fa fa-flag text-muted"></i>&nbsp;
									<?php if ($row->language=='*' || empty( $row->language) ): ?>
										<?php echo JText::alt('JALL', 'language'); ?>
									<?php else: ?>
										<?php echo $this->escape($this->getLanguageTitle($row->language)); ?>
									<?php endif; ?>
								</span>

								<?php if ($row->ip): ?>
									<span class="me-2">
										<i class="fa fa-desktop"></i> <?php echo $row->ip;?>
									</span>
								<?php endif; ?>

								<?php if ($row->locked): ?>
									<span class="me-2">
										<i class="fa fa-lock text-muted" data-bs-toggle="tooltip" data-title="<?php echo JText::_('COM_EASYBLOG_BLOG_POST_IS_LOCKED');?>" data-placement="bottom"></i> <?php echo JText::_('COM_EASYBLOG_POST_LOCKED');?>
									</span>
								<?php endif; ?>

								<div class="text-nowrap">
									<?php echo JText::_('COM_EASYBLOG_BLOGS_CONTRIBUTED_IN') . ' : ';?> <?php echo $row->contributionDisplay;?>
								</div>

								<div class="text-nowrap">
									<?php echo JText::_('COM_EASYBLOG_BLOGS_AUTOPOSTING') . ' : ';?>
									<?php if ($row->isPublished() && $centralizedConfigured): ?>
										<?php foreach ($consumers as $consumer): ?>
											<a class="btn btn-social btn-<?php echo $consumer->type;?> btn-sm text-center <?php echo $consumer->isShared($row->id) ? ' is-sent' : '';?>"
												href="javascript:void(0);"
												data-post-autopost
												data-id="<?php echo $row->id;?>"
												data-type="<?php echo $consumer->type;?>"
												data-bs-toggle="tooltip"
												data-original-title="<?php echo $consumer->isShared($row->id) ? JText::sprintf('COM_EASYBLOG_AUTOPOST_SHARED', $consumer->type) : JText::sprintf('COM_EASYBLOG_AUTOPOST_NOT_SHARED_YET', $consumer->type);?>"
											>
												<i class="fa fa-<?php echo $consumer->type;?> fa-14"></i>
											</a>
										<?php endforeach; ?>
									<?php else: ?>
										<div data-bs-toggle="tooltip" data-original-title="<?php echo (! $centralizedConfigured) ? JText::_('COM_EASYBLOG_AUTOPOST_NOT_AVAILABLE_BECAUSE_MISSING_CONFIGURATION') : JText::_('COM_EASYBLOG_AUTOPOST_NOT_AVAILABLE_BECAUSE_UNPUBLISHED', true ); ?>">
											<?php echo JText::_('COM_EASYBLOG_NOT_AVAILABLE'); ?>
										</div>
									<?php endif; ?>
								</div>
                              </div>
                           	</td>
						   	<?php if ( !$browse ): ?>
								<td class="nowrap hidden-phone center text-center align-middle">
									<?php echo $this->html('grid.featured', $row, 'blogs', 'featured', array('blogs.feature', 'blogs.unfeature')); ?>
									</td>
									<td class="nowrap hidden-phone center text-center align-middle">
										<?php echo $this->html('grid.published', $row, 'blogs', 'published'); ?>
									</td>
									<td class="nowrap hidden-phone center text-center align-middle">
										<?php echo $this->html('grid.published', $row, 'blogs', 'frontpage', array('blogs.setFrontpage', 'blogs.removeFrontpage')); ?>
								</td>
								<td class="nowrap text-center align-middle hidden-phone ">
										<?php echo $row->hits;?>
								</td>
								<td class="center text-center align-middle hidden-phone small">
									<a class="btn btn-default bg-light text-dark border border-1 px-3 py-2 btn-sm"
											data-notify-item
											data-blog-id="<?php echo $row->id;?>"
											data-bs-toggle="tooltip"
											data-title="<?php echo JText::_('COM_EASYBLOG_BLOGS_NOTIFY_TOOLTIP');?>"
										>
											<i class="fa fa-envelope fa-14 me-0"></i>
										</a>
								</td>
								<td class="text-center align-middle">
									<div style="white-space: inherit">
										<?php echo EB::date($row->created)->format(JText::_('DATE_FORMAT_LC1')); ?>
									</div>
								</td>
								<td class="text-center align-middle">
										<?php echo $row->id; ?>
								</td>
						   <?php endif; ?>
                        </tr>
						<?php $i++; ?>
					<?php endforeach; ?>
				<?php else: ?>
				<tr>
					<td colspan="15" class="empty">
						<?php echo JText::_('COM_EASYBLOG_BLOGS_NO_ENTRIES');?>
					</td>
				</tr>
				<?php endif; ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="15" class="text-center">
						<?php echo $pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>

	<?php if( $browse ){ ?>
	<input type="hidden" name="tmpl" value="component" />
	<?php } ?>

	<input type="hidden" name="autopost_type" value="" />
	<input type="hidden" name="autopost_selected" value="" />
	<input type="hidden" name="move_category_id" value="" data-move-category />
	<input type="hidden" name="move_author_id" value="" data-move-author />
	<input type="hidden" name="browse" value="<?php echo $browse;?>" />
	<input type="hidden" name="browseFunction" value="<?php echo $browseFunction;?>" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="view" value="blogs" />
	<input type="hidden" name="task" value="" data-table-grid-task />
	<input type="hidden" name="filter_order" value="<?php echo $order; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $orderDirection; ?>" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
