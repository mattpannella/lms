<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

defined('_JEXEC') or die('Unauthorized Access');

$iframeUrl = JRoute::_('index.php?option=com_easyblog&view=tags&tmpl=component&browse=1&browsefunction=insertTag');
$modalParams = array(
    'selector' => $id . '_modal',
    'params' => array(
        'iframeUrl' => $iframeUrl,
        'title' => 'Select a Tag',
        'size' => 'lg'
    )
);
echo JLayoutHelper::render('joomla.modal.main', $modalParams);
?>

<span class="input-group">
    <input 
        id="<?php echo $id;?>_name"
        value="<?php echo $title; ?>"
        data-tag-title
        type="text"
        readonly
    />
    <button
        id="<?php echo $id;?>_open_modal_btn"
        class="btn btn-primary"
        data-bs-target="#<?php echo $id;?>_modal"
        data-bs-toggle="modal"
        type="button"
    >
        <i class="icon-users"></i> <?php echo JText::_('COM_EASYBLOG_MENU_SELECT_TAG'); ?>
    </button>
</span>

<input 
    id="<?php echo $id;?>_id" 
    name="<?php echo $name;?>" 
    value="<?php echo $value;?>" 
    data-tag-id 
    type="hidden" 
/>

<script type="text/javascript">
    EasyBlog.ready(function($) {
        window.insertTag = function(id, name) {
            $('[data-tag-id]').val(id);
            $('[data-tag-title]').val(name);

            var modal = document.getElementById("<?php echo $id ?>_modal");
            modal = bootstrap.Modal.getInstance(modal);
            modal.hide();
        }
    });
</script>
