<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
    <?php foreach ($fieldsets as $fieldset): ?>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_($fieldset->label); ?></b>
                <div class="card-text"><?php echo JText::_($fieldset->info);?></div>
            </div>

            <div class="card-body">
                <?php foreach ($fieldset->fields as $field): ?>
                  <div class="row form-group d-flex mb-4">
                    <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                        <?php echo $this->html(
                            'grid.label',
                            $field->name,
                            JText::_($field->attributes->label),
                            JText::_($field->attributes->label. '_DESC')
                        ); ?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <?php
                            $defaultVal = $field->attributes->default;
                            $fieldType = ($this->input->get('view', '', 'cmd') == 'settings' && $field->attributes->type == 'textext') ? 'text' : $field->attributes->type;
                        ?>
                        <?php echo $this->output('admin/form/field.' . $fieldType, array('field' => $field, 'default' => $defaultVal, 'prefix' => $prefix)); ?>
                    </div>
                  </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
