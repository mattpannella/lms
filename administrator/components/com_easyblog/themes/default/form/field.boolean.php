<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

$booleanKey = ($prefix) ? $prefix . $field->attributes->name : 'params[' . $field->attributes->name . ']';
$checked = $params->get($prefix . $field->attributes->name, $default);

krsort($field->options, SORT_NUMERIC);
?>
<div class="btn-group"
   role="group"
   aria-label="Yes/No Option"
   data-content=""
   data-original-title=""
   data-placement=""
>
<?php foreach ($field->options as $option) {
    if ($skipEmpty && ($option->value < 0)) { continue; }

    $className = "btn-outline-primary";
    $toggleValue = $option->value;
    $label = JText::_('COM_EASYBLOG_GRID_YES');

    if ($option->value == -1) {
        $className = 'btn-outline-secondary';
        $label = JText::_('COM_EASYBLOG_GRID_INHERIT');
    } else if ($option->value == 0) {
        $className = 'btn-outline-secondary';
        $label = JText::_('COM_EASYBLOG_GRID_NO');
    }
?>
  <input type="radio"
         class="btn-check"
         name="<?php echo $booleanKey ; ?>"
         id="<?php echo $booleanKey; ?>-<?php echo $option->value; ?>"
         autocomplete="off"
         value="<?php echo $option->value; ?>"
      <?php echo $checked == $option->value ? 'checked' : ''; ?> />

  <label class="btn <?php echo $className; ?>"
         for="<?php echo $booleanKey; ?>-<?php echo $option->value; ?>">
      <?php echo $label; ?>
  </label>
<?php } ?>
</div>

