<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2013 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
?>
<div class="btn-group"
     role="group"
     aria-label="Yes/No Option"
     data-content="<?php echo isset( $tips[ 'content' ] ) ? JText::_( $tips[ 'content' ] ) : '';?>"
     data-original-title="<?php echo isset( $tips[ 'title' ] ) ? JText::_( $tips[ 'title' ] ) : '';?>"
     data-placement="<?php echo isset( $tips[ 'placement' ] ) ? $tips[ 'placement' ] : 'right';?>"
>
  <input type="radio"
         class="btn-check"
         name="<?php echo $name ;?>"
         id="yes-<?php echo empty( $id ) ? $name : $id; ?>"
         autocomplete="off"
         value="1"
      <?php echo $attributes; ?>
      <?php echo $checked ? 'checked' : ''; ?> />
  <label class="btn btn-outline-primary"
         for="yes-<?php echo empty( $id ) ? $name : $id; ?>">
      <?php echo $onText; ?>
  </label>

  <input type="radio"
         class="btn-check"
         name="<?php echo $name ;?>"
         id="no-<?php echo empty( $id ) ? $name : $id; ?>"
         autocomplete="off"
         value="0"
      <?php echo $attributes; ?>
      <?php echo !$checked ? 'checked' : ''; ?> />
  <label class="btn btn-outline-secondary"
         for="no-<?php echo empty( $id ) ? $name : $id; ?>">
      <?php echo $offText; ?>
  </label>
</div>
