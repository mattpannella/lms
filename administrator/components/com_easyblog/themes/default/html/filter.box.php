<?php
defined('_JEXEC') or die('Unauthorized Access');
?>
	<div class="form-inline row align-items-center mb-4">
		<div class="col-sm-12 col-md-3">
			<?php echo $search; ?>
		</div>
		<div class="col-sm-12 col-md-8">
			<div class="row">
                <?php foreach ($dropdowns as $dropdown): ?>
				<div class="col-sm-12 col-md-4 px-1">
					<?php echo $dropdown; ?>
				</div>
                <?php endforeach; ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-1">
			<div class="form-group float-end text-end">
				<?php echo $pagination; ?>
			</div>
		</div>
	</div>