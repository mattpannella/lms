<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

if ($task == 'blogs.unfeature') {
    $icon_class = 'fa-star';
    $badge_class = 'text-warning';
}

if ($task == 'blogs.feature') {
    $icon_class = 'fa-star';
}

if ($class == 'publish') {
    $icon_class = 'fa-check';
    $badge_class .= ' text-success';
}

if ($class == 'unpublish') {
    $icon_class = 'fa-times';
    $badge_class .= ' text-danger';
}

if ($class == 'trash') {
    $icon_class = 'fa-trash-o';
}

if ($class == 'scheduled') {
    $icon_class = 'fa-clock-o';
}

if ($class == 'archived') {
    $icon_class = 'fa-inbox';
}
?>
<button type="button" class="btn eb-state-<?php echo $class; ?> <?php echo $badge_class; ?>"
	<?php echo $allowed ? ' data-table-grid-publishing' : '';?>
	data-task="<?php echo $task;?>"
    data-bs-toggle="tooltip"
    title="<?php echo $tooltip;?>"
	<?php echo !$allowed ? ' disabled="disabled"' : ''; ?>
>
    <i style="font-size:x-large" class="fa <?php echo $icon_class; ?>"></i>
</button>

