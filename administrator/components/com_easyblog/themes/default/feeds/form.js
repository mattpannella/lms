
EasyBlog.ready(function($){

    window.insertTag = function( id , name )
    {
    	$( '#item_team' ).val( id );
    	$( '#team_name' ).html( '<span>' + name + '</span>');

    	EasyBlog.dialog().close();
    }

    window.insertMember = function( id , name )
    {
        $( '#item_creator' ).val( id );
        $( '#author_name' ).html( '<span>' + name + '</span>');

        EasyBlog.dialog().close();
    }

    window.insertCategory = function( id , name )
    {
    	$( '#item_category' ).val( id );
    	$( '#category_name' ).html( name );
        
    	EasyBlog.dialog().close();
    }

    $('[data-browse-categories]').on('click', function() {
        EasyBlog.dialog({
            content: EasyBlog.ajax('admin/views/categories/browse')
        });
    });

    $('[data-browse-bloggers]').on('click', function() {
        EasyBlog.dialog({
            content:  EasyBlog.ajax('admin/views/bloggers/browse')
        });
    });

    $('[data-browse-teams]').on('click', function() {
        EasyBlog.dialog({
            content: EasyBlog.ajax('admin/views/teamblogs/browse')
        });
    });
});