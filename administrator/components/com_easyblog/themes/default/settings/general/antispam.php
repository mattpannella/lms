<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
  <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_ANTI_SPAM');?></b>

                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_ANTI_SPAM_INFO');?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'main_post_min', 'COM_EASYBLOG_SETTINGS_ENABLE_MINIMUM_CONTENT_LENGTH');
                ?>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_post_length',
                          JText::_('COM_EASYBLOG_SETTINGS_MINIMUM_CONTENT_LENGTH'),
                          JText::_('COM_EASYBLOG_SETTINGS_MINIMUM_CONTENT_LENGTH_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="form-inline">
                            <div class="row form-group d-flex mb-4">
                                <div class="input-group">
                                    <input type="text" name="main_post_length" value="<?php echo $this->escape( $this->config->get( 'main_post_length' ) );?>" class="form-control text-center" />
                                    <span class="input-group-text"><?php echo JText::_( 'COM_EASYBLOG_CHARACTERS' );?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_CONTENT_FILTERING');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_CONTENT_FILTERING_INFO');?></div>
            </div>

            <div class="card-body">
                <p><?php echo JText::_('COM_EASYBLOG_SETTINGS_BLOCKED_WORDS_INFO'); ?></p>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_blocked_words',
                          JText::_('COM_EASYBLOG_SETTINGS_BLOCKED_WORDS'),
                          JText::_('COM_EASYBLOG_SETTINGS_BLOCKED_WORDS_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <textarea  name="main_blocked_words"><?php echo $this->config->get( 'main_blocked_words' ); ?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
