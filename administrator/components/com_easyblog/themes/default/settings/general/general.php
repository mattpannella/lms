<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
    // echo $this->html('settings.select', 'main_login_provider', 'COM_EASYBLOG_SETTINGS_WORKFLOW_LOGIN_PROVIDER', '', [
    //     'easysocial' => JText::_( 'COM_EASYBLOG_EASYSOCIAL' ),
    //     'joomla' => JText::_( 'COM_EASYBLOG_JOOMLA' ),
    //     'cb' => JText::_( 'COM_EASYBLOG_CB' ),
    //     'jomsocial' => JText::_( 'COM_EASYBLOG_JOMSOCIAL' ),
    // ]);
    // echo $this->html('settings.smalltext', 'main_meta_autofilldescription_length', 'COM_EASYBLOG_SETTINGS_WORKFLOW_METADATA_AUTO_FILL_DESCRIPTION_CHARACTER_LIMIT', 'COM_EASYBLOG_SETTINGS_WORKFLOW_METADATA_AUTO_FILL_DESCRIPTION_CHARACTER_LIMIT_DESC', 'COM_EASYBLOG_CHARACTERS');
    // echo $this->html('settings.toggle', 'main_nonblogger_profile', 'COM_EASYBLOG_SETTINGS_WORKFLOW_DISPLAY_NON_BLOGGER_PROFILE');
    // echo $this->html('settings.text', 'layout_exclude_bloggers', 'COM_EASYBLOG_SETTINGS_LAYOUT_EXCLUDE_USERS_FROM_BLOGGER_LISTINGS');

?>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_GENERAL_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_GENERAL_INFO'); ?></div>
            </div>

            <div class="card-body">
                <div class="row form-group d-flex mb-4">
                    <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                        <?php echo $this->html(
                            'grid.label',
                            'main_title',
                            JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_TITLE'),
                            JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_TITLE_DESC')
                        ); ?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <input type="text" name="main_title"  value="<?php echo $this->escape($this->config->get('main_title'));?>" />
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                    <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                        <?php echo $this->html(
                            'grid.label',
                            'main_description',
                            JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_DESCRIPTION'),
                            JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_BLOG_DESCRIPTION_DESC')
                        ); ?>
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <textarea name="main_description" rows="5"  cols="35"><?php echo $this->config->get('main_description');?></textarea>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_pagetitle_autoappend',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_APPEND_BLOG_TITLE'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_APPEND_BLOG_TITLE_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                      <?php echo $this->html('grid.boolean', 'main_pagetitle_autoappend', $this->config->get('main_pagetitle_autoappend')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'sitename_position',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_SITENAME_POSITION'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_SITENAME_POSITION_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <select name="sitename_position" >
                            <option value="default"<?php echo $this->config->get('sitename_position') == 'default' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_SITENAME_POSITION_DEFAULT');?></option>
                            <option value="after"<?php echo $this->config->get('sitename_position') == 'after' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_SITENAME_POSITION_AFTER');?></option>
                        </select>

                        <div>
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_SITENAME_POSITION_NOTE'); ?>
                    </div>
                    </div>


                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_pagetitle_autoappend_entry',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_APPEND_BLOG_TITLE_ENTRY_VIEW'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SEO_AUTOMATIC_APPEND_BLOG_TITLE_ENTRY_VIEW_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_pagetitle_autoappend_entry', $this->config->get('main_pagetitle_autoappend_entry')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_login_read',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_REQUIRE_LOGIN_TO_READ_FULL'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_REQUIRE_LOGIN_TO_READ_FULL_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_login_read', $this->config->get('main_login_read'));?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_copyrights',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_COPYRIGHTS'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_COPYRIGHTS_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_copyrights', $this->config->get('main_copyrights')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_password_protect',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_PASSWORD_PROTECTION'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_PASSWORD_PROTECTION_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_password_protect', $this->config->get('main_password_protect')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_microblog',
                          JText::_('COM_EASYBLOG_SETTINGS_MICROBLOG_ENABLE_MICROBLOG'),
                          JText::_('COM_EASYBLOG_SETTINGS_MICROBLOG_ENABLE_MICROBLOG_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_microblog', $this->config->get('main_microblog'));?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_multi_language',
                          JText::_('COM_EASYBLOG_SETTINGS_GENERAL_ENABLE_MULTI_LANGUAGE_POSTS'),
                          JText::_('COM_EASYBLOG_SETTINGS_GENERAL_ENABLE_MULTI_LANGUAGE_POSTS_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_multi_language', $this->config->get('main_multi_language')); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_CALENDAR');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_CALENDAR_INFO'); ?></div>
            </div>

            <div class="card-body">
                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_start_of_week',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_CALENDAR_START_OF_WEEK'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_CALENDAR_START_OF_WEEK_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <select name="main_start_of_week" >
                            <option value="monday"<?php echo $this->config->get('main_start_of_week') == 'monday' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_CALENDAR_MONDAY'); ?></option>
                            <option value="sunday"<?php echo $this->config->get('main_start_of_week') == 'sunday' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_CALENDAR_SUNDAY'); ?></option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_HITS');?></b>
                <div class="card-text"><?php echo JText::_( 'COM_EASYBLOG_SETTINGS_WORKFLOW_HITS_DESC' ); ?></div>
            </div>

            <div class="card-body">
                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_hits_session',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_SESSION_TRACKING_HITS'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_SESSION_TRACKING_HITS_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <?php echo $this->html('grid.boolean', 'main_hits_session', $this->config->get('main_hits_session')); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_OPTIONS'); ?></b>
                <div class="card-text"><?php echo JText::_( 'COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_INFO' );?></div>
            </div>

            <div class="card-body">
                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_newblogonfrontpage',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_NEW_ENTRY_ON_FRONTPAGE'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_NEW_ENTRY_ON_FRONTPAGE_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7" >
                        <?php echo $this->html('grid.boolean', 'main_newblogonfrontpage', $this->config->get('main_newblogonfrontpage')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_defaultallowcomment',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_ALLOW_COMMENT'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_ALLOW_COMMENT_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7" >
                        <?php echo $this->html('grid.boolean', 'main_defaultallowcomment', $this->config->get('main_defaultallowcomment')); ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_blogprivacy',
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_BLOG_PRIVACY'),
                          JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_DEFAULT_BLOG_PRIVACY_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7" >
                        <?php
                            $nameFormat = EasyBlogHelper::getHelper('privacy')->getOptions();
                            $showdet = JHTML::_('select.genericlist', $nameFormat, 'main_blogprivacy', '', 'value', 'text', $this->config->get('main_blogprivacy' ) );
                            echo $showdet;
                        ?>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_sendemailnotifications',
                          JText::_('COM_EASYBLOG_BLOGS_BLOG_SEND_NOTIFICATION_EMAILS'),
                          JText::_('COM_EASYBLOG_BLOGS_BLOG_SEND_NOTIFICATION_EMAILS_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7" >
                        <?php echo $this->html('grid.boolean', 'main_sendemailnotifications', $this->config->get('main_sendemailnotifications')); ?>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
