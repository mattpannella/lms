<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SUBSCRIPTIONS_TITLE');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SUBSCRIPTIONS_INFO'); ?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_sitesubscription',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_SITE_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_SITE_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_sitesubscription', $this->config->get('main_sitesubscription'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_subscription',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_BLOG_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_BLOG_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_subscription', $this->config->get('main_subscription'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_bloggersubscription',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_BLOGGER_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_BLOGGER_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_bloggersubscription', $this->config->get('main_bloggersubscription'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_categorysubscription',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_CATEGORY_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_CATEGORY_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_categorysubscription', $this->config->get('main_categorysubscription'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_teamsubscription',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_TEAM_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENABLE_TEAM_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_teamsubscription', $this->config->get('main_teamsubscription'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_allowguestsubscribe',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_GUEST_TO_SUBSCRIBE'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_GUEST_TO_SUBSCRIBE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_allowguestsubscribe', $this->config->get('main_allowguestsubscribe'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_registeronsubscribe',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_GUEST_REGISTRATION_DURING_SUBSCRIBE'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_GUEST_REGISTRATION_DURING_SUBSCRIBE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_registeronsubscribe', $this->config->get('main_registeronsubscribe'));?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_AGREEMENT');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_AGREEMENT_INFO'); ?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_subscription_agreement',
                  JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_REQUIRE_USER_TO_AGREE'),
                  JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_REQUIRE_USER_TO_AGREE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_subscription_agreement', $this->config->get('main_subscription_agreement'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_subscription_agreement_message',
                  JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_REQUIRE_USER_TO_AGREE'),
                  JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_REQUIRE_USER_TO_AGREE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<textarea name="main_subscription_agreement_message" ><?php echo $this->config->get('main_subscription_agreement_message');?></textarea>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_SUBSCRIPTIONS_FEATURE_TITLE');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_SUBSCRIPTIONS_INFO'); ?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_subscription_confirmation',
                  JText::_('COM_EASYBLOG_SETTINGS_NOTIFY_USER_SUBSCRIPTIONS_CONFIRMATION'),
                  JText::_('COM_EASYBLOG_SETTINGS_NOTIFY_USER_SUBSCRIPTIONS_CONFIRMATION_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_subscription_confirmation', $this->config->get('main_subscription_confirmation'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_subscription_admin_notification',
                  JText::_('COM_EASYBLOG_SETTINGS_NOTIFY_ADMIN_NEW_SUBSCRIPTIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_NOTIFY_ADMIN_NEW_SUBSCRIPTIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_subscription_admin_notification', $this->config->get('main_subscription_admin_notification'));?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
