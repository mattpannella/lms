<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row form-horizontal">
	<div class="col-lg-6">

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_INFO');?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4" data-composer-editors>
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_editor',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SELECT_DEFAULT_EDITOR'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SELECT_DEFAULT_EDITOR_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('form.editors', 'layout_editor', $this->config->get('layout_editor'), true); ?>
					</div>
				</div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_permalink',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COMPOSER_ALLOW_EDITING_PERMALINK'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COMPOSER_ALLOW_EDITING_PERMALINK_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_permalink', $this->config->get('layout_composer_permalink')); ?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_fields',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_FIELDS'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_FIELDS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_fields', $this->config->get('layout_composer_fields')); ?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_dashboardseo',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_SEO'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_SEO_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_dashboardseo', $this->config->get('layout_dashboardseo')); ?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_history',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_REVISIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_REVISIONS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_history', $this->config->get('layout_composer_history')); ?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_tags',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COMPOSER_ENABLE_TAGS'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COMPOSER_ENABLE_TAGS_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_tags', $this->config->get('layout_composer_tags')); ?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'max_tags_allowed',
                  JText::_('COM_EASYBLOG_LAYOUT_DASHBOARD_MAX_TAGS_ALLOWED'),
                  JText::_('COM_EASYBLOG_LAYOUT_DASHBOARD_MAX_TAGS_ALLOWED_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<div class="row">
							<div class="col-lg-3">
								<input type="text" name="max_tags_allowed" class="form-control text-center" value="<?php echo $this->config->get('max_tags_allowed', '' );?>" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_TEMPLATES');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_TEMPLATES_DESC');?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'composer_templates',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_ENABLE_TEMPLATES'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_ENABLE_TEMPLATES_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'composer_templates', $this->config->get('composer_templates')); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_TITLE'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_INFO');?></div>
			</div>

			<div class="card-body">
				<p><?php echo JText::sprintf('COM_EASYBLOG_SETTINGS_AUTOFILL_REQUIRES_API_KEY', '/administrator/index.php?option=com_easyblog&view=settings&layout=system');?></p>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_autofill_tags',
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_TAGS_ENABLE'),
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_TAGS_ENABLE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_autofill_tags', $this->config->get('main_autofill_tags'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_autofill_keywords',
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_KEYWORDS_ENABLE'),
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOFILL_KEYWORDS_ENABLE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
							<?php echo $this->html('grid.boolean', 'main_autofill_keywords', $this->config->get('main_autofill_keywords'));?>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-lg-6">

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_INFO');?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_language',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_LANGUAGE_SELECTION'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_LANGUAGE_SELECTION_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_language', $this->config->get('layout_composer_language'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_creationdate',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_CREATION_DATE'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_CREATION_DATE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_creationdate', $this->config->get('layout_composer_creationdate'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_publishingdate',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_PUBLISHING_DATE'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_PUBLISHING_DATE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_publishingdate', $this->config->get('layout_composer_publishingdate'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_unpublishdate',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_UNPUBLISHING_DATE'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_UNPUBLISHING_DATE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_unpublishdate', $this->config->get('layout_composer_unpublishdate'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_privacy',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_PRIVACY_SECTION'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_PRIVACY_SECTION_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_privacy', $this->config->get('layout_composer_privacy'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_composer_comment',
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_COMMENT'),
                  JText::_('COM_EASYBLOG_SETTINGS_COMPOSER_POST_OPTIONS_COMMENT_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'layout_composer_comment', $this->config->get('layout_composer_comment'));?>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_TITLE'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_INFO');?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_autodraft',
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_ENABLE'),
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_ENABLE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_autodraft', $this->config->get('main_autodraft'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_autodraft_interval',
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_INTERVAL'),
                  JText::_('COM_EASYBLOG_SETTINGS_AUTOSAVE_INTERVAL_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<div class="form-inline">
							<div class="row form-group d-flex mb-4">
								<div class="input-group">
									<input type="text" size="10" name="main_autodraft_interval" class="form-control text-center" value="<?php echo $this->config->get('main_autodraft_interval', '0' );?>" />
									<span class="input-group-text"><?php echo JText::_('COM_EASYBLOG_SECONDS');?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE_INFO');?></div>
			</div>

			<div class="card-body">
				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_keepalive',
                  JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE_ENABLE'),
                  JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE_ENABLE_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<?php echo $this->html('grid.boolean', 'main_keepalive', $this->config->get('main_keepalive'));?>
					</div>
				</div>

				<div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_keepalive_interval',
                  JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE_INTERVAL'),
                  JText::_('COM_EASYBLOG_SETTINGS_KEEPALIVE_INTERVAL_DESC')
              ); ?>
          </div>
					<div class="col-sm-12 col-md-7">
						<div class="form-inline">
							<div class="row form-group d-flex mb-4">
								<div class="input-group">
									<input type="text" size="10" name="main_keepalive_interval" class="form-control text-center" value="<?php echo $this->config->get('main_keepalive_interval', '0' );?>" />
									<span class="input-group-text"><?php echo JText::_('COM_EASYBLOG_SECONDS');?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
