<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
        <div class="card-header">
            <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_TITLE'); ?></b>
            <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_INFO'); ?></div>
        </div>

        <div class="card-body">
            <?php
                echo $this->html('settings.toggle', 'main_comment', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_COMMENT');
            ?>
          <div class="row form-group d-flex mb-4">
            <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                <?php echo $this->html(
                    'grid.label',
                    'comment_sort',
                    JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_SORTING'),
                    JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_SORTING_DESC')
                ); ?>
            </div>
            <div class="col-sm-12 col-md-7">
                <?php
                    $listLength = array();
                    $listLength[] = JHTML::_('select.option', 'desc', JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_SORTING_OPTIONS_DESCENDING'));
                    $listLength[] = JHTML::_('select.option', 'asc', JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_SORTING_OPTIONS_ASCENDING'));
                    echo JHTML::_('select.genericlist', $listLength, 'comment_sort', 'class="form-control input-box"', 'value', 'text', $this->config->get('comment_sort' , 'desc'));
                ?>
            </div>
          </div>
            <?php
                echo $this->html('settings.toggle', 'comment_bbcode', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_BBCODE');
                echo $this->html('settings.toggle', 'comment_likes', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_LIKES');
                echo $this->html('settings.toggle', 'main_allowguestviewcomment', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_GUEST_VIEW_COMMENT');
                echo $this->html('settings.toggle', 'comment_registeroncomment', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_GUEST_REGISTRATION_WHEN_COMMENTING');
                echo $this->html('settings.toggle', 'comment_autotitle', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_AUTO_TITLE_IN_REPLY');
                echo $this->html('settings.smalltext', 'comment_maxthreadedlevel', 'COM_EASYBLOG_SETTINGS_COMMENTS_THREADED_LEVEL');
                echo $this->html('settings.toggle', 'comment_autohyperlink', 'COM_EASYBLOG_COMMENTS_ENABLE_AUTO_HYPERLINKS');
                echo $this->html('settings.toggle', 'comment_autosubscribe', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_AUTOSUBSCRIBE');
                echo $this->html('settings.toggle', 'comment_allowlogin', 'COM_EASYBLOG_SETTINGS_COMMENTS_ALLOW_LOGIN_LINK');
            ?>
        </div>
      </div>
  	</div>

  <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="card">
          <div class="card-header">
              <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_REQUIREMENTS_TITLE'); ?></b>
              <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_REQUIREMENTS_DESC');?></div>
          </div>

          <div class="card-body">
              <?php
                  echo $this->html('settings.toggle', 'comment_show_title', 'COM_EASYBLOG_SETTINGS_COMMENTS_SHOW_TITLE');
                  echo $this->html('settings.toggle', 'comment_requiretitle', 'COM_EASYBLOG_SETTINGS_COMMENTS_REQUIRE_TITLE');
                  echo $this->html('settings.toggle', 'comment_show_email', 'COM_EASYBLOG_SETTINGS_COMMENTS_SHOW_EMAIL');
                  echo $this->html('settings.toggle', 'comment_require_email', 'COM_EASYBLOG_SETTINGS_COMMENTS_REQUIRE_EMAIL');
                  echo $this->html('settings.toggle', 'comment_show_website', 'COM_EASYBLOG_SETTINGS_COMMENTS_SHOW_WEBSITE');
                  echo $this->html('settings.toggle', 'comment_require_website', 'COM_EASYBLOG_SETTINGS_COMMENTS_REQUIRE_WEBSITE');
              ?>
          </div>
      </div>

      <div class="card">
          <div class="card-header">
            <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_TNC_TITLE'); ?></b>
            <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_REQUIREMENTS_DESC');?></div>
          </div>

          <div class="card-body">
            <?php
                echo $this->html('settings.toggle', 'comment_tnc', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_TERMS');
                echo $this->html('settings.textarea', 'comment_tnctext', 'COM_EASYBLOG_SETTINGS_COMMENTS_TERMS_TEXT', '', '', '', 15);
            ?>
          </div>
      </div>
	</div>
</div>
