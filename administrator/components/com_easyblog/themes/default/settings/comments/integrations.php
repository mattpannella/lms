<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Restricted access');
?>
<div class="row form-horizontal">
    <div class="col-sm-12 col-md-6 col-lg-6">

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_OTHER_COMMENT_TITLE'); ?></b>
                <div class=card-text><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_OTHER_COMMENT_DESC'); ?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'main_comment_multiple', 'COM_EASYBLOG_SETTINGS_COMMENTS_MULTIPLE_SYSTEM');
                    echo $this->html('settings.toggle', 'comment_easyblog', 'COM_EASYBLOG_SETTINGS_COMMENTS_BUILTIN_COMMENTS');
                ?>
            </div>
        </div>

       <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_KOMENTO'); ?></b>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_komento',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_KOMENTO'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_KOMENTO_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php if ($komento): ?>
                      <?php echo $this->html('grid.boolean', '', $this->config->get('comment_komento')); ?>
                    <?php else: ?>
                      <div class="form-control-static"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_KOMENTO_NOT_INSTALLED'); ?></div>
                    <?php endif; ?>
                </div>
              </div>
            </div>
        </div>

       <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_EASYDISCUSS'); ?></b>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_easydiscuss',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_EASYDISCUSS'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_EASYDISCUSS_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php if ($easydiscuss): ?>
                        <?php echo $this->html('grid.boolean', '', $this->config->get('comment_easydiscuss')); ?>
                    <?php else: ?>
                      <div class="form-control-static"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_EASYDISCUSS_NOT_INSTALLED'); ?></div>
                    <?php endif; ?>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_DISQUS'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_disqus', 'COM_EASYBLOG_SETTINGS_COMMENTS_DISQUS');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_disqus_code',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_DISQUS_CODE'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_DISQUS_CODE_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="input-group">
                    <input type="text"  value="<?php echo $this->config->get('comment_disqus_code');?>" id="comment_disqus_code" name="comment_disqus_code" />
                    <a href="http://stackideas.com/docs/easyblog/administrators/comments/integrating-with-disqus" class="btn btn-default">
                      <i class="fa fa-life-ring"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_HYPERCOMMENTS'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_hypercomments', 'COM_EASYBLOG_SETTINGS_COMMENTS_HYPERCOMMENTS');
                    echo $this->html('settings.text', 'comment_hypercomments_widgetid', 'COM_EASYBLOG_SETTINGS_COMMENTS_HYPERCOMMENTS_WIDGETID');
                ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_EASYSOCIAL_COMMENTS'); ?></b>
            </div>

            <div class="card-body">
              <?php echo $this->html('settings.toggle', 'comment_easysocial', 'COM_EASYBLOG_SETTINGS_COMMENTS_EASYSOCIAL_COMMENTS'); ?>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_FACEBOOK_COMMENTS'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_facebook', 'COM_EASYBLOG_SETTINGS_COMMENTS_FACEBOOK_COMMENTS');
                    echo $this->html('settings.select', 'comment_facebook_colourscheme', 'COM_EASYBLOG_SETTINGS_COMMENTS_FACEBOOK_COLOUR_SCHEME', '', [
                        'light' => JText::_('COM_EASYBLOG_SETTINGS_SOCIALSHARE_FACEBOOK_LIKE_THEMES_LIGHT'),
                        'dark' => JText::_('COM_EASYBLOG_SETTINGS_SOCIALSHARE_FACEBOOK_LIKE_THEMES_DARK'),
                    ]);
                ?>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_INTENSE_DEBATE'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_intensedebate', 'COM_EASYBLOG_SETTINGS_COMMENTS_INTENSE_DEBATE');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_intensedebate_code',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_INTENSE_DEBATE_CODE'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_INTENSE_DEBATE_CODE_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="input-group">
                    <input type="text"  value="<?php echo $this->config->get('comment_intensedebate_code');?>" id="comment_intensedebate_code" name="comment_intensedebate_code" />
                    <a href="http://stackideas.com/docs/easyblog/administrators/comments/integrating-with-intense-debate" class="btn btn-default">
                      <i class="fa fa-life-ring"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_COMPOJOOM'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_compojoom', 'COM_EASYBLOG_SETTINGS_COMMENTS_COMPOJOOM');
                ?>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_JCOMMENT'); ?></b>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_jcomments',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_JCOMMENT'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_JCOMMENT_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php if ($jComment): ?>
                        <?php echo $this->html('grid.boolean', 'comment_jcomments', $this->config->get('comment_jcomments')); ?>
                    <?php else: ?>
                      <div class="form-control-static"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_JCOMMENT_NOT_FOUND'); ?></div>
                    <?php endif; ?>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RSCOMMENTS'); ?></b>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_rscomments',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RSCOMMENTS'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RSCOMMENTS_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php if ($jComment): ?>
                        <?php echo $this->html('grid.boolean', 'comment_rscomments', $this->config->get('comment_rscomments')); ?>
                    <?php else: ?>
                      <div class="form-control-static"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RSCOMMENTS_NOT_FOUND'); ?></div>
                    <?php endif; ?>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_LIVEFYRE'); ?></b>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_livefyre', 'COM_EASYBLOG_SETTINGS_COMMENTS_LIVEFYRE');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'comment_livefyre_siteid',
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_LIVEFYRE_SITEID'),
                        JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_LIVEFYRE_SITEID_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="input-group">
                    <input type="text"  value="<?php echo $this->config->get('comment_livefyre_siteid');?>" id="comment_livefyre_siteid" name="comment_livefyre_siteid" />
                    <a href="http://stackideas.com/docs/easyblog/administrators/comments/integrating-with-livefyre" target="_blank" class="btn btn-default">
                      <i class="fa fa-life-ring"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
