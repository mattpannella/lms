<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
        	<div class="card-header">
	            <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_AKISMET_INTEGRATIONS_TITLE'); ?></b>
	            <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_AKISMET_INTEGRATIONS_DESC'); ?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_akismet', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_AKISMET');
                    echo $this->html('settings.text', 'comment_akismet_key', 'COM_EASYBLOG_SETTINGS_COMMENTS_AKISMET_API_KEY');
                ?>
            </div>
        </div>

        <div class="card">
       		<div class="card-header">
	            <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_BUILTIN_CAPTCHA'); ?></b>
	            <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_BUILTIN_CAPTCHA_DESC'); ?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'comment_captcha', 'COM_EASYBLOG_SETTINGS_COMMENTS_CAPTCHA');
                    echo $this->html('settings.toggle', 'comment_captcha_registered', 'COM_EASYBLOG_SETTINGS_COMMENTS_CAPTCHA_REGISTERED');
                ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">
      <div class="card">
        <div class="card-header">
            <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_INTEGRATIONS_TITLE'); ?></b>
            <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_INTEGRATIONS_DESC'); ?></div>
          </div>

          <div class="card-body">
              <?php
                  echo $this->html('settings.toggle', 'comment_recaptcha', 'COM_EASYBLOG_SETTINGS_COMMENTS_ENABLE_RECAPTCHA');
                  echo $this->html('settings.text', 'comment_recaptcha_public', 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_PUBLIC_KEY');
                  echo $this->html('settings.text', 'comment_recaptcha_private', 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_PRIVATE_KEY');
                  echo $this->html('settings.select', 'main_login_provider', 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_THEME', '', [
                      'light' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_THEME_LIGHT' ),
                      'dark' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_THEME_DARK' ),
                  ]);
                  echo $this->html('settings.select', 'comment_recaptcha_lang', 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE', '', [
                      'en' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_ENGLISH' ),
                      'ru' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_RUSSIAN' ),
                      'fr' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_FRENCH' ),
                      'de' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_GERMAN' ),
                      'nl' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_DUTCH' ),
                      'pt' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_PORTUGUESE' ),
                      'tr' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_TURKISH' ),
                      'es' => JText::_( 'COM_EASYBLOG_SETTINGS_COMMENTS_RECAPTCHA_LANGUAGE_SPANISH' ),
                  ]);
              ?>
          </div>
      </div>
    </div>
</div>
