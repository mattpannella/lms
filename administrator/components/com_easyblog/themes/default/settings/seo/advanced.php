<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Restricted access');
$mainRouting = ($this->config->get('main_routing') == 'currentactive') ? 'default' : $this->config->get('main_routing');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ADVANCE_SETTINGS_TITLE');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ADVANCE_SETTINGS_INFO');?></div>
			</div>

			<div class="card-body">
        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'main_routing',
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR'),
                  JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="list-group">

              <div class="list-group-item">
                <div class="radio">
                  <input type="radio" name="main_routing" value="default" id="defaultRouting" data-routing-type <?php echo $mainRouting == 'default' ? ' checked="checked"' : '';?> style="margin-top: 2px;" />
                  <label for="defaultRouting">
                    <b class="list-group-item-heading"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_DEAULT');?></b>
                    <p class="list-group-item-text">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_DEAULT_DESC');?>
                    </p>
                  </label>
                </div>

              </div>

              <div class="list-group-item">
                <div class="radio">
                  <input type="radio" name="main_routing" value="menuitemid" id="useMenuRouting" data-routing-type <?php echo $mainRouting == 'menuitemid' ? ' checked="checked"' : '';?> style="margin-top: 2px;" />
                  <label for="useMenuRouting">
                    <b class="list-group-item-heading"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_USE_MENUITEM');?></b>
                    <p class="list-group-item-text">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_USE_MENUITEM_DESC');?>
                    </p>
                  </label>

                  <div class="row mt-10">
                    <div class="col-sm-8">
                      <div class="input-group">
                        <span class="input-group-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ENTER_MENU_ID');?></span>
                        <input type="text" name="main_routing_itemid" class="form-control text-center" value="<?php echo $this->config->get('main_routing_itemid' );?>" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="list-group-item">
                  <?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_NOTE'); ?>
              </div>
            </div>
          </div>
        </div>
        <div class="alert alert-warning mt-20">
            <?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_ROUTING_BEHAVIOR_DEPRECATED_NOTE'); ?>
        </div>
      </div>
		</div>
	</div>

	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_SEO_ENTRY_ROUTING_VIEW');?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_SEO_ENTRY_ROUTING_VIEW_INFO');?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.select', 'main_routing_entry', 'COM_EASYBLOG_SETTINGS_SEO_ENTRY_SELECT_ROUTING_VIEW', '', [
                  'categories' => JText::_( 'COM_EASYBLOG_SETTINGS_SEO_ENTRY_ROUTING_CATEGORY' ),
                  'blogger' => JText::_( 'COM_EASYBLOG_SETTINGS_SEO_ENTRY_ROUTING_AUTHOR' ),
                  'teamblog' => JText::_( 'COM_EASYBLOG_SETTINGS_SEO_ENTRY_ROUTING_TEAMBLOG' ),
              ]);
          ?>
			</div>
		</div>
	</div>
</div>
