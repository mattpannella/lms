<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

    $tabs = [
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_SEO_SUBTAB_GENERAL'), 'file' => 'general'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_SEO_SUBTAB_ADVANCED_SETTINGS'), 'file' => 'advanced'],
    ];
?>
<form method="post" action="<?php echo JRoute::_('index.php');?>" id="adminForm">
    <div class="app-tabs">
        <ul  role="tablist" class="app-tabs-list list-unstyled nav nav-tabs">
            <?php foreach ($tabs as $index => $tab): ?>
              <li class="nav-item" role="presentation">
                <button class="nav-link<?php echo $index === 0 ? ' active' : ''; ?>"
                        id="<?php echo $tab['file']; ?>-tab"
                        data-bs-toggle="tab"
                        data-bs-target="#<?php echo $tab['file']; ?>-content"
                        type="button"
                        role="tab"
                        aria-controls="<?php echo $tab['file']; ?>-content"
                        aria-selected="<?php echo $index === 0 ? 'true' : 'false'; ?>"
                        data-form-tabs>
                    <?php echo $tab['title']; ?>
                </button>
              </li>
            <?endforeach; ?>
        </ul>
    </div>

    <div class="tab-content">
        <?php foreach ($tabs as $index => $tab): ?>
          <div id="<?php echo $tab['file']; ?>-content"
               class="tab-pane fade<?php echo $index === 0 ? ' show active' : ''; ?>"
               role="tabpanel"
               aria-labelledby="<?php echo $tab['file']; ?>-tab"
          >
            <div id="<?php echo $tab['file']; ?>" class="tab-pane<?php echo $index === 0 ? ' active in' : ''; ?>">
                <?php echo $this->output('admin/settings/seo/' . $tab['file']); ?>
            </div>
          </div>
        <?php endforeach; ?>

    <?php echo $this->html('form.action'); ?>
    <input type="hidden" name="page" value="seo" />
    <input type="hidden" name="activeTab" value="<?php echo $activeTab;?>" data-settings-active />
</form>
