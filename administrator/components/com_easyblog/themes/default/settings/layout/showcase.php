<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_FEATURED_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_FEATURED_INFO');?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.select', 'cover_featured_size', 'COM_EASYBLOG_SETTINGS_POST_COVER_SIZE', '', [
                  'small' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_SMALL' ),
                  'thumbnail' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_THUMBNAIL' ),
                  'medium' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_MEDIUM' ),
                  'large' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_LARGE' ),
                  'original' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_ORIGINAL' ),
              ]);
              echo $this->html('settings.toggle', 'cover_featured_crop', 'COM_EASYBLOG_SETTINGS_POST_COVER_CROP_COVER');
              echo $this->html('settings.toggle', 'cover_photo_legacy', 'COM_EASYBLOG_SETTINGS_POST_COVER_PHOTO_LEGACY');
          ?>

        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_featured_width',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_featured_width" value="<?php echo $this->config->get('cover_featured_width', 200);?>" data-cover-featured-width />
              <span class="input-group-text ps-2">pixels</span>
            </div>
          </div>
        </div>

        <div class="row form-group mb-4 <?php echo !$this->config->get('cover_featured_crop') ? 'd-none' : 'd-flex'; ?>" data-cover-featured-height>
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_featured_height',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_featured_height" value="<?php echo $this->config->get('cover_featured_height', 200);?>" />
              <span class="input-group-text ps-2">pixels</span>
            </div>
          </div>
        </div>

        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_featured_alignment',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <select name="cover_featured_alignment"  style="width: 50%;">
              <option value="left"<?php echo $this->config->get('cover_featured_alignment') == 'left' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_LEFT');?></option>
              <option value="right"<?php echo $this->config->get('cover_featured_alignment') == 'right' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_RIGHT');?></option>
            </select>
          </div>
        </div>
			</div>
		</div>
	</div>

	<div class="col-lg-6">
	</div>
</div>
