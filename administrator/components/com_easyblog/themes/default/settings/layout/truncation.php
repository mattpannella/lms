<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">

    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOMATED_TRUNCATION_COMPOSER_CONTENT');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOMATED_TRUNCATION_COMPOSER_CONTENT_INFO'); ?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'composer_truncation_enabled', 'COM_EASYBLOG_SETTINGS_AUTOMATED_TRUNCATION_COMPOSER_ENABLE');
                    echo $this->html('settings.smalltext', 'composer_truncation_chars', 'COM_EASYBLOG_SETTINGS_TRUNCATION_COMPOSER_MAX_CHARS', 'COM_EASYBLOG_SETTINGS_TRUNCATION_COMPOSER_MAX_CHARS_DESC');
                    echo $this->html('settings.toggle', 'composer_truncation_readmore', 'COM_EASYBLOG_SETTINGS_TRUNCATION_COMPOSER_DISPLAY_READMORE_WHEN_NECESSARY');
                ?>

                <?php $mediaTypes   = array('image', 'video', 'audio'); ?>

                <?php
                    foreach ($mediaTypes as $media) {
                        echo $this->html('settings.select', 'composer_truncate_' . $media . '_position', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_' . strtoupper($media) . '_POSITIONS', '', [
                            'top' => JText::_( 'COM_EASYBLOG_TOP_OPTION' ),
                            'bottom' => JText::_( 'COM_EASYBLOG_BOTTOM_OPTION' ),
                            'hidden' => JText::_( 'COM_EASYBLOG_DO_NOT_SHOW_OPTION' ),
                        ]);

                        echo $this->html('settings.smalltext', 'composer_truncate_' . $media . '_limit', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_' . strtoupper($media) . '_LIMITS', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_' . strtoupper($media) . '_LIMITS_DESC');
                    }
                ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOMATED_TRUNCATION_NORMAL_CONTENT');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_AUTOMATED_TRUNCATION_NORMAL_CONTENT_INFO'); ?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'layout_blogasintrotext', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_BLOG_CONTENT_AS_INTROTEXT');
                    echo $this->html('settings.select', 'main_truncate_type', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_BLOG_TYPE', '', [
                        'chars' => JText::_( 'COM_EASYBLOG_BY_CHARACTERS' ),
                        'words' => JText::_( 'COM_EASYBLOG_BY_WORDS' ),
                        'paragraph' => JText::_( 'COM_EASYBLOG_BY_PARAGRAPH' ),
                        'break' => JText::_( 'COM_EASYBLOG_BY_BREAK' ),
                    ]);

                    echo $this->html('settings.smalltext', 'layout_maxlengthasintrotext', 'COM_EASYBLOG_SETTINGS_LAYOUT_MAX_LENGTH_OF_BLOG_CONTENT_AS_INTROTEXT', 'COM_EASYBLOG_SETTINGS_LAYOUT_MAX_LENGTH_OF_BLOG_CONTENT_AS_INTROTEXT_DESC');
                    echo $this->html('settings.smalltext', 'main_truncate_maxtag', 'COM_EASYBLOG_SETTINGS_LAYOUT_MAX_LENGTH_TAGS', 'COM_EASYBLOG_SETTINGS_LAYOUT_MAX_LENGTH_TAGS_DESC');
                    echo $this->html('settings.toggle', 'main_truncate_ellipses', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_ADD_ELLIPSES');
                    echo $this->html('settings.toggle', 'layout_respect_readmore', 'COM_EASYBLOG_SETTINGS_LAYOUT_SHOW_READMORE');

                    $mediaTypes   = array('image', 'video', 'audio', 'gallery');
                    foreach ($mediaTypes as $media) {
                        echo $this->html('settings.select', 'main_truncate_' . $media . '_position', 'COM_EASYBLOG_SETTINGS_LAYOUT_TRUNCATE_' . strtoupper($media) . '_POSITIONS', '', [
                            'top' => JText::_( 'COM_EASYBLOG_TOP_OPTION' ),
                            'bottom' => JText::_( 'COM_EASYBLOG_BOTTOM_OPTION' ),
                            'hidden' => JText::_( 'COM_EASYBLOG_DO_NOT_SHOW_OPTION' ),
                        ]);
                    }
                ?>
            </div>
        </div>
    </div>
</div>
