<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

    $superUser = JFactory::getUser()->authorise('core.admin');

    $tabs = [
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_GENERAL'), 'file' => 'general'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_LIST_OPTION'), 'file' => 'listings'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_CATEGORIES_OPTION'), 'file' => 'categories'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_CATEGORY_OPTION'), 'file' => 'category'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_AUTHOR_OPTION'), 'file' => 'author'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_TAG_OPTION'), 'file' => 'tag'],
        ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_POST_OPTION'), 'file' => 'posts'],
    ];

    if ($superUser) {
      $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_PAGINATION'), 'file' => 'pagination'];
      $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_AVATARS'), 'file' => 'avatars'];
    }

    $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_TRUNCATION'), 'file' => 'truncation'];
    $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_TOOLBAR'), 'file' => 'toolbar'];
    $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_POST_COVER'), 'file' => 'cover'];
    $tabs[] = ['title' => JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_SUBTAB_SHOWCASE'), 'file' => 'showcase'];

?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
    <div class="app-tabs">
        <ul id="tab" role="tablist" class="app-tabs-list list-unstyled nav nav-tabs">
            <?php foreach ($tabs as $index => $tab): ?>
              <li class="nav-item" role="presentation">
                <button class="nav-link<?php echo $index === 0 ? ' active' : ''; ?>"
                        id="<?php echo $tab['file']; ?>-tab"
                        data-bs-toggle="tab"
                        data-bs-target="#<?php echo $tab['file']; ?>-content"
                        type="button"
                        aria-controls="<?php echo $tab['file']; ?>-content"
                        aria-selected="<?php echo $index === 0 ? 'true' : 'false'; ?>"
                        data-form-tabs>
                    <?php echo $tab['title']; ?>
                </button>
              </li>
            <?php endforeach; ?>
        </ul>
    </div>

    <div class="tab-content">
        <?php foreach ($tabs as $index => $tab): ?>
          <div id="<?php echo $tab['file']; ?>-content"
               class="tab-pane fade<?php echo $index === 0 ? ' show active' : ''; ?>"
               role="tabcard"
               aria-labelledby="<?php echo $tab['file']; ?>-tab"
          >
            <?php echo $this->output('admin/settings/layout/' . $tab['file']); ?>
          </div>
        <?php endforeach; ?>
    </div>

    <?php echo $this->html('form.action'); ?>
    <input type="hidden" name="page" value="layout" />
    <input type="hidden" name="activeTab" value="<?php echo $activeTab;?>" data-settings-active />
</form>
