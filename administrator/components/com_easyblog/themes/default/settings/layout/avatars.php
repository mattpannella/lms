<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATARS_TITLE');?></b>
			</div>
			<div class="card-body">
          <?php
              echo $this->html('settings.toggle', 'layout_avatar', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_AVATARS');
              echo $this->html('settings.toggle', 'layout_categoryavatar', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_CATEGORY_AVATARS');
              echo $this->html('settings.toggle', 'layout_teamavatar', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_TEAMBLOG_AVATARS');
          ?>
			</div>
		</div>
	</div>

	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_TITLE');?></b>
			</div>
			<div class="card-body">
          <?php
              echo $this->html('settings.toggle', 'layout_avatar_link_name', 'COM_EASYBLOG_SETTINGS_LAYOUT_LINK_AUTHOR_NAME');
          ?>
        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'layout_avatarIntegration',
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS'),
                  JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
              <?php
                  $nameFormat = array();
                  $avatarIntegration[] = JHTML::_('select.option', 'default', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_DEFAULT'));
                  $avatarIntegration[] = JHTML::_('select.option', 'easysocial', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_EASYSOCIAL'));
                  $avatarIntegration[] = JHTML::_('select.option', 'jfbconnect', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_JFBCONNECT'));
                  $avatarIntegration[] = JHTML::_('select.option', 'communitybuilder', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_CB'));
                  $avatarIntegration[] = JHTML::_('select.option', 'gravatar', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_GRAVATAR'));
                  $avatarIntegration[] = JHTML::_('select.option', 'jomsocial', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_JOMSOCIAL'));
                  $avatarIntegration[] = JHTML::_('select.option', 'kunena', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_KUNENA'));
                  $avatarIntegration[] = JHTML::_('select.option', 'k2', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_K2'));
                  $avatarIntegration[] = JHTML::_('select.option', 'phpbb', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_PHPBB'));
                  $avatarIntegration[] = JHTML::_('select.option', 'mightytouch', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_MIGHTYREGISTRATION'));
                  $avatarIntegration[] = JHTML::_('select.option', 'anahita', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_ANAHITA'));
                  $avatarIntegration[] = JHTML::_('select.option', 'jomwall', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_JOMWALL'));
                  $avatarIntegration[] = JHTML::_('select.option', 'easydiscuss', JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_AVATAR_INTEGRATIONS_EASYDISCUSS'));
                  echo JHTML::_('select.genericlist', $avatarIntegration, 'layout_avatarIntegration', ' data-avatar-source', 'value', 'text', $this->config->get('layout_avatarIntegration' , 'default' ) );
              ?>
          </div>
        </div>

        <?php
            echo $this->html('settings.text', 'layout_phpbb_path', 'COM_EASYBLOG_SETTINGS_LAYOUT_PHPBB_PATH');
        ?>
			</div>
		</div>
	</div>
</div>
