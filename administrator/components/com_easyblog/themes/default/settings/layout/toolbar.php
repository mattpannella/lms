<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">

	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TOOLBAR_GENERAL');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TOOLBAR_GENERAL_INFO');?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.toggle', 'layout_headers', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_BLOG_HEADER');
              echo $this->html('settings.toggle', 'layout_header_description', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_BLOG_DESCRIPTIONS_HEADER');
              echo $this->html('settings.toggle', 'layout_headers_respect_author', 'COM_EASYBLOG_SETTINGS_LAYOUT_HEADER_RESPECT_AUTHOR');
              echo $this->html('settings.toggle', 'layout_headers_respect_teamblog', 'COM_EASYBLOG_SETTINGS_LAYOUT_HEADER_RESPECT_TEAMBLOG');
              echo $this->html('settings.toggle', 'layout_toolbar', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_BLOG_TOOLBAR');
          ?>
			</div>
		</div>

		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_TOOLBAR_FRONTEND'); ?></b>
				<div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_TOOLBAR_FRONTEND_INFO'); ?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.toggle', 'layout_latest', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_LATEST_POST');
              echo $this->html('settings.toggle', 'layout_option_toolbar', 'COM_EASYBLOG_SETTINGS_LAYOUT_BUTTON_IN_TOOLBAR');
              echo $this->html('settings.toggle', 'layout_categories', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_CATEGORIES');
              echo $this->html('settings.toggle', 'layout_tags', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_TAGS');
              echo $this->html('settings.toggle', 'layout_bloggers', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_BLOGGERS');
              echo $this->html('settings.toggle', 'layout_search', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_SEARCH');
              echo $this->html('settings.toggle', 'layout_teamblog', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_TEAMBLOG');
              echo $this->html('settings.toggle', 'layout_archives', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_ARCHIVES');
              echo $this->html('settings.toggle', 'layout_calendar', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_CALENDAR');
              echo $this->html('settings.toggle', 'layout_login', 'COM_EASYBLOG_SETTINGS_LAYOUT_ENABLE_LOGIN');
              echo $this->html('settings.toggle', 'toolbar_editprofile', 'COM_EASYBLOG_SETTINGS_LAYOUT_TOOLBAR_SHOW_EDIT_PROFILE');
              echo $this->html('settings.toggle', 'toolbar_teamrequest', 'COM_EASYBLOG_SETTINGS_LAYOUT_TOOLBAR_SHOW_TEAM_REQUEST');
              echo $this->html('settings.toggle', 'toolbar_logout', 'COM_EASYBLOG_SETTINGS_LAYOUT_TOOLBAR_SHOW_LOGOUT');
          ?>
			</div>
		</div>
	</div>

	<div class="col-sm-12 col-md-6 col-lg-6">

		<div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD'); ?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_INFO');?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'layout_enabledashboardtoolbar', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_ENABLE');
                    echo $this->html('settings.toggle', 'layout_dashboardhome', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_HOME');
                    echo $this->html('settings.toggle', 'layout_dashboardmain', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_STATS');
                    echo $this->html('settings.toggle', 'layout_dashboardblogs', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_ENTRIES');
                    echo $this->html('settings.toggle', 'layout_dashboardcomments', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_COMMENTS');
                    echo $this->html('settings.toggle', 'layout_dashboardcategories', 'COM_EASYBLOG_SETTINGS_TOOLBAR_DASHBOARD_CATEGORIES');
                    echo $this->html('settings.toggle', 'layout_dashboardtags', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_TAGS');
                    echo $this->html('settings.toggle', 'layout_dashboardteamrequest', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_TEAM_REQUEST');
                    echo $this->html('settings.toggle', 'layout_dashboardnewpost', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_NEW_POST');
                    echo $this->html('settings.toggle', 'layout_dashboardsettings', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_SETTINGS');
                    echo $this->html('settings.toggle', 'layout_dashboardlogout', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_LOGOUT');
                ?>
            </div>
		</div>
	</div>
</div>
