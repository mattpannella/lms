<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DISPLAY_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DISPLAY_INFO');?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'layout_blogger_breadcrumb', 'COM_EASYBLOG_LAYOUT_BREADCRUMB_BLOGGER');
                ?>
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_nameformat',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DISPLAY_NAME_FORMAT'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_DISPLAY_NAME_FORMAT_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_( 'select.option' , 'name' , JText::_( 'COM_EASYBLOG_REAL_NAME_OPTION' ) );
                        $listLength[] = JHTML::_('select.option', 'nickname', JText::_( 'COM_EASYBLOG_NICKNAME_OPTION' ) );
                        $listLength[] = JHTML::_('select.option', 'username', JText::_( 'COM_EASYBLOG_USERNAME_OPTION' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_nameformat', '', 'value', 'text', $this->config->get('layout_nameformat' , 'name'));
                    ?>
                </div>
              </div>
              <?php
                    echo $this->html('settings.toggle', 'main_categories_hideempty', 'COM_EASYBLOG_SETTINGS_WORKFLOW_HIDE_EMPTY_CATEGORIES');
                    echo $this->html('settings.toggle', 'layout_zero_as_plural', 'COM_EASYBLOG_LAYOUT_ZERO_AS_PLURAL');
                    echo $this->html('settings.toggle', 'layout_responsive', 'COM_EASYBLOG_LAYOUT_ENABLE_RESPONSIVE_LAYOUT');
                    echo $this->html('settings.toggle', 'layout_dashboard_biography_editor', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ALLOW_HTML_FOR_BIOGRAPHY');
                ?>
            </div>
        </div>

    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_ORDERING');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_ORDERING_INFO');?></div>
            </div>
            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_postorder',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_( 'select.option' , 'modified' , JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_OPTIONS_LAST_MODIFIED' ) );
                        $listLength[] = JHTML::_('select.option', 'latest', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_OPTIONS_LATEST' ) );
                        $listLength[] = JHTML::_('select.option', 'alphabet', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_OPTIONS_ALPHABET' ) );
                        $listLength[] = JHTML::_('select.option', 'popular', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_OPTIONS_HITS' ) );
                        $listLength[] = JHTML::_('select.option', 'published', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_ORDERING_PUBLISHED' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_postorder', '', 'value', 'text', $this->config->get('layout_postorder' , 'latest'));
                    ?>
                </div>
              </div>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_postsort',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_SORTING'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_SORTING_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_('select.option', 'desc', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_SORTING_OPTIONS_DESCENDING' ) );
                        $listLength[] = JHTML::_('select.option', 'asc', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_POSTS_SORTING_OPTIONS_ASCENDING' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_postsort', 'class="form-control input-box"', 'value', 'text', $this->config->get('layout_postsort' , 'desc'));
                    ?>
                </div>
              </div>
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_teamblogsort',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TEAMBLOG_LISTING_POSTS_SORTING'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TEAMBLOG_LISTING_POSTS_SORTING_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_('select.option', 'desc', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TEAMBLOG_LISTING_POSTS_SORTING_OPTIONS_DESCENDING' ) );
                        $listLength[] = JHTML::_('select.option', 'asc', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TEAMBLOG_LISTING_POSTS_SORTING_OPTIONS_ASCENDING' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_teamblogsort', 'class="form-control input-box"', 'value', 'text', $this->config->get('layout_teamblogsort' , 'desc'));
                    ?>
                </div>
              </div>
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_bloggerorder',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_( 'select.option' , 'featured' , JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_OPTIONS_FEATURED' ) );
                        $listLength[] = JHTML::_('select.option', 'latest', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_OPTIONS_LATEST' ) );
                        $listLength[] = JHTML::_('select.option', 'alphabet', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_OPTIONS_ALPHABET' ) );
                        $listLength[] = JHTML::_('select.option', 'latestpost', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_OPTIONS_LATESTPOST' ) );
                        $listLength[] = JHTML::_('select.option', 'active', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGERS_ORDERING_OPTIONS_ACTIVE' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_bloggerorder', '', 'value', 'text', $this->config->get('layout_bloggerorder' , 'latest'));
                    ?>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_TAGSTYLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_TAGSTYLE_INFO');?></div>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'layout_tagstyle',
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE'),
                        JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_( 'select.option' , '1' , JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE_STYLE1' ) );
                        $listLength[] = JHTML::_('select.option', '2', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE_STYLE2' ) );
                        $listLength[] = JHTML::_('select.option', '3', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE_STYLE3' ) );
                        $listLength[] = JHTML::_('select.option', '4', JText::_( 'COM_EASYBLOG_SETTINGS_LAYOUT_TAG_STYLE_STYLE4' ) );
                        echo JHTML::_('select.genericlist', $listLength, 'layout_tagstyle', '', 'value', 'text', $this->config->get('layout_tagstyle' , '1'));
                    ?>
                </div>
              </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_CUSTOM_FIELDS');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_CUSTOM_FIELDS_INFO');?></div>
            </div>

            <div class="card-body">
              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'custom_field_date_format',
                        JText::_('COM_EASYBLOG_FIELD_DATE_FORMAT'),
                        JText::_('COM_EASYBLOG_FIELD_DATE_FORMAT_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $listLength = array();
                        $listLength[] = JHTML::_('select.option', 'l, d F Y' , JText::_('DATE_FORMAT_LC1'));
                        $listLength[] = JHTML::_('select.option', 'l, d F Y H:i', JText::_('DATE_FORMAT_LC2'));
                        $listLength[] = JHTML::_('select.option', 'd F Y', JText::_('DATE_FORMAT_LC3'));
                        echo JHTML::_('select.genericlist', $listLength, 'custom_field_date_format', '', 'value', 'text', $this->config->get('custom_field_date_format' , JText::_('DATE_FORMAT_LC1')));
                    ?>
                  <span><a href="http://php.net/manual/en/function.date.php" target="_blank"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_CUSTOM_FIELD_DATE_NOTICE'); ?></a></span>
                </div>
              </div>
            </div>
        </div>

    </div>
</div>
