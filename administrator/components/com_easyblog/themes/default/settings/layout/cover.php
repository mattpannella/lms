<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_LISTING_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_LISTING_INFO');?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.select', 'cover_size', 'COM_EASYBLOG_SETTINGS_POST_COVER_SIZE', '', [
                  'small' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_SMALL' ),
                  'thumbnail' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_THUMBNAIL' ),
                  'medium' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_MEDIUM' ),
                  'large' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_LARGE' ),
                  'original' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_ORIGINAL' ),
              ]);
              echo $this->html('settings.toggle', 'cover_crop', 'COM_EASYBLOG_SETTINGS_POST_COVER_CROP_COVER');
          ?>
        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_width',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_width" value="<?php echo $this->config->get('cover_width', 260);?>" <?php echo $this->config->get('cover_width_full') ? ' disabled="disabled"' : '';?> data-cover-width />
              <span class="input-group-text ps-2">pixels</span>
              <div class="checkbox">
                <input type="checkbox" id="cover-width-full" value="1" name="cover_width_full" <?php echo $this->config->get('cover_width_full') ? ' checked="checked"' : '';?> data-cover-full-width />
                <label for="cover-width-full">
                    <?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_USE_FULL_WIDTH');?>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="row form-group mb-4 <?php echo !$this->config->get('cover_crop') ? 'd-none' : 'd-flex'; ?>" data-cover-height>
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_height',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_height" value="<?php echo $this->config->get('cover_height', 260);?>" />
              <span class="input-group-text ps-2">pixels</span>
            </div>
          </div>
        </div>

        <div class="row form-group mb-4 <?php echo $this->config->get('cover_width_full') ? 'd-none' : 'd-flex'; ?>">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_alignment',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <select name="cover_alignment"  style="width: 50%;">
              <option value="left"<?php echo $this->config->get('cover_alignment') == 'left' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_LEFT');?></option>
              <option value="right"<?php echo $this->config->get('cover_alignment') == 'right' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_RIGHT');?></option>
              <option value="center"<?php echo $this->config->get('cover_alignment') == 'center' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_CENTER');?></option>
              <option value="none"<?php echo $this->config->get('cover_alignment') == 'none' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_NONE');?></option>
            </select>
          </div>
        </div>
			</div>
		</div>
	</div>

	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="card">
			<div class="card-header">
				<b><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_ENTRY_TITLE');?></b>
        <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_LAYOUT_COVER_ENTRY_INFO');?></div>
			</div>

			<div class="card-body">
          <?php
              echo $this->html('settings.select', 'cover_size_entry', 'COM_EASYBLOG_SETTINGS_POST_COVER_SIZE', '', [
                  'small' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_SMALL' ),
                  'thumbnail' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_THUMBNAIL' ),
                  'medium' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_MEDIUM' ),
                  'large' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_LARGE' ),
                  'original' => JText::_( 'COM_EASYBLOG_SETTINGS_POST_COVER_ORIGINAL' ),
              ]);
              echo $this->html('settings.toggle', 'cover_crop_entry', 'COM_EASYBLOG_SETTINGS_POST_COVER_CROP_COVER');
          ?>
        <div class="row form-group d-flex mb-4">
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_width_entry',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_WIDTH_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_width_entry" value="<?php echo $this->config->get('cover_width_entry', 260);?>" <?php echo $this->config->get('cover_width_entry_full') ? ' disabled="disabled"' : '';?> data-cover-width-entry />
              <span class="input-group-text ps-2">pixels</span>
              <div class="checkbox">
                <input type="checkbox" id="cover-width-full-entry" value="1" name="cover_width_entry_full" <?php echo $this->config->get('cover_width_entry_full') ? ' checked="checked"' : '';?> data-cover-full-width-entry />
                <label for="cover-width-full-entry">
                    <?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_USE_FULL_WIDTH');?>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div class="row form-group mb-4 <?php echo !$this->config->get('cover_crop_entry') ? 'd-none' : 'd-flex'; ?>" data-cover-height-entry>
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_height_entry',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_HEIGHT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <div class="input-group align-items-center">
              <input type="text"  name="cover_height_entry" value="<?php echo $this->config->get('cover_height_entry', 260);?>" />
              <span class="input-group-text ps-2">pixels</span>
            </div>
          </div>
        </div>

        <div class="row form-group mb-4 <?php echo $this->config->get('cover_width_entry_full') ? 'd-none' : 'd-flex'; ?>" data-cover-alignment-entry>
          <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
              <?php echo $this->html(
                  'grid.label',
                  'cover_alignment_entry',
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT'),
                  JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGNMENT_DESC')
              ); ?>
          </div>
          <div class="col-sm-12 col-md-7">
            <select name="cover_alignment_entry"  style="width: 50%;">
              <option value="left"<?php echo $this->config->get('cover_alignment_entry') == 'left' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_LEFT');?></option>
              <option value="right"<?php echo $this->config->get('cover_alignment_entry') == 'right' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_RIGHT');?></option>
              <option value="center"<?php echo $this->config->get('cover_alignment_entry') == 'center' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_CENTER');?></option>
              <option value="none"<?php echo $this->config->get('cover_alignment_entry') == 'none' ? ' selected="selected"' : '';?>><?php echo JText::_('COM_EASYBLOG_SETTINGS_POST_COVER_ALIGN_NONE');?></option>
            </select>
          </div>
        </div>
			</div>
		</div>
	</div>
</div>
