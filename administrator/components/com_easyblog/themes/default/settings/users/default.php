<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Restricted access');
?>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="row">
    <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_USERS');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_WORKFLOW_USERS_INFO'); ?></div>
            </div>

            <div class="card-body">
              <?php
                  echo $this->html('settings.toggle', 'main_joomlauserparams', 'COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_JOOMLA_USER_PARAMETERS');
                  echo $this->html('settings.toggle', 'main_dashboard_editaccount', 'COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_EDIT_ACCOUNT');
                  echo $this->html('settings.select', 'main_login_provider', 'COM_EASYBLOG_SETTINGS_WORKFLOW_LOGIN_PROVIDER', '', [
                      'easysocial' => JText::_( 'COM_EASYBLOG_EASYSOCIAL' ),
                      'joomla' => JText::_( 'COM_EASYBLOG_JOOMLA' ),
                      'cb' => JText::_( 'COM_EASYBLOG_CB' ),
                      'jomsocial' => JText::_( 'COM_EASYBLOG_JOMSOCIAL' ),
                  ]);
                  echo $this->html('settings.toggle', 'main_nonblogger_profile', 'COM_EASYBLOG_SETTINGS_WORKFLOW_DISPLAY_NON_BLOGGER_PROFILE');
                  echo $this->html('settings.text', 'layout_exclude_bloggers', 'COM_EASYBLOG_SETTINGS_LAYOUT_EXCLUDE_USERS_FROM_BLOGGER_LISTINGS');
                  echo $this->html('settings.text', 'layout_exclude_categories', 'COM_EASYBLOG_SETTINGS_LAYOUT_EXCLUDE_CATEGORIES_FROM_FRONTPAGE');
                  echo $this->html('settings.toggle', 'main_blogprivacy_override', 'COM_EASYBLOG_SETTINGS_WORKFLOW_ALLOW_BLOGGER_TO_SWITCH');
                  echo $this->html('settings.toggle', 'main_bloggerlistingoption', 'COM_EASYBLOG_SETTINGS_LAYOUT_BLOGGER_LISTINGS_OPTION');
              ?>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-md-6 col-lg-6">

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_USERS_AUTOMATION');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_USERS_AUTOMATION_INFO'); ?></div>
            </div>

            <div class="card-body">
              <?php echo $this->html('settings.toggle', 'main_autofeatured', 'COM_EASYBLOG_SETTINGS_WORKFLOW_AUTOMATIC_FEATURE_BLOG_POST'); ?>
            </div>
        </div>

    </div>
</div>
 <?php echo $this->html('form.action'); ?>
    <input type="hidden" name="page" value="users" />
    <input type="hidden" name="activeTab" value="<?php echo $activeTab;?>" data-settings-active />
</form>
