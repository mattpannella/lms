<?php
/**
* @package      EasyBlog
* @copyright    Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license      GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
  <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_UPLOADER_TITLE');?></b>
                <div class="card-text">
                    <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_UPLOADER_INFO');?>
                </div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.buttontext', 'main_media_extensions', 'COM_EASYBLOG_SETTINGS_MEDIA_ALLOWED_EXTENSIONS', '', 'data-reset-extensions', 'COM_EASYBLOG_RESET_DEFAULT');
                    echo $this->html('settings.smalltext', 'main_upload_image_size', 'COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_MAX_FILESIZE', '', 'COM_EASYBLOG_MEGABYTES');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'main_upload_image_size',
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_MAX_FILESIZE'),
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_MAX_FILESIZE_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="input-group align-items-center">
                        <input type="text" name="main_upload_image_size" class="form-control text-center" value="<?php echo $this->config->get('main_upload_image_size', '0' );?>">
                        <span class="input-group-text ps-2"><?php echo JText::_('COM_EASYBLOG_MEGABYTES');?></span>
                      </div>
                      <div><?php echo JText::sprintf( 'COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_UPLOAD_PHP_MAXSIZE' , ini_get( 'upload_max_filesize') ); ?></div>
                      <div><?php echo JText::sprintf( 'COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_UPLOAD_PHP_POSTMAXSIZE' , ini_get( 'post_max_size') ); ?></div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'main_image_quality',
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY'),
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY_DESC')
                      ); ?>
                  </div>
                  <div class="col-sm-12 col-md-7">
                        <?php
                            $options = array();

                            for( $i = 0; $i <= 100; $i += 10 )
                            {
                                $message    = $i;
                                $message    = $i == 0 ? JText::sprintf( 'COM_EASYBLOG_LOWEST_QUALITY_OPTION' , $i ) : $message;
                                $message    = $i == 50 ? JText::sprintf( 'COM_EASYBLOG_MEDIUM_QUALITY_OPTION' , $i ) : $message;
                                $message    = $i == 100 ? JText::sprintf( 'COM_EASYBLOG_HIGHEST_QUALITY_OPTION' , $i ) : $message;
                                $options[]  = JHTML::_('select.option', $i , $message );
                            }

                            echo JHTML::_('select.genericlist', $options, 'main_image_quality', '', 'value', 'text', $this->config->get('main_image_quality' ) );
                        ?>
                        <div class="help-block">
                          <?php echo JText::_( 'COM_EASYBLOG_SETTINGS_WORKFLOW_IMAGE_UPLOAD_QUALITY_HINTS' );?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php /*
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_STORAGE_TITLE');?></b>
                <div class="card-text">
                    <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_STORAGE_INFO');?>
                </div>
            </div>

            <div class="card-body">
                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_ARTICLE_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_ARTICLE_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_ARTICLE_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                        <input type="text" name="main_articles_path"  value="<?php echo $this->config->get('main_articles_path', 'images/easyblog_articles/' );?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_IMAGE_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                    	<input type="text" name="main_image_path"  value="<?php echo $this->config->get('main_image_path', 'images/easyblog_images/' );?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_SHARED_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_SHARED_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_SHARED_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                    	<input type="text" name="main_shared_path"  value="<?php echo $this->config->get('main_shared_path', 'media/com_easyblog/shared/' );?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_AVATAR_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_AVATAR_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_AVATAR_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                    	<input type="text" name="main_avatarpath"  value="<?php echo $this->config->get('main_avatarpath', 'images/eblog_avatar/' );?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_CATEGORY_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_CATEGORY_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_CATEGORY_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                    	<input type="text" name="main_categoryavatarpath"  value="<?php echo $this->config->get('main_categoryavatarpath', 'images/eblog_cavatar/' );?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="page_title" class="col-md-5">
                        <?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_TEAMBLOG_PATH'); ?>

                        <i data-html="true" data-placement="top" data-title="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_TEAMBLOG_PATH'); ?>"
                            data-content="<?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_TEAMBLOG_PATH_DESC');?>" data-eb-provide="popover" class="fa fa-question-circle float-end"></i>
                    </label>

                    <div class="col-md-7">
                    	<input type="text" name="main_teamavatarpath"  value="<?php echo $this->config->get('main_teamavatarpath', 'images/eblog_tavatar/' );?>" />
                    </div>
                </div>
            </div>
        </div>
        */?>
	</div>

  <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_ORIGINAL_IMAGE_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_ORIGINAL_IMAGE_DESC');?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.toggle', 'main_resize_original_image', 'COM_EASYBLOG_SETTINGS_MEDIA_RESIZE_ORIGINAL_IMAGE');
                    echo $this->html('settings.smalltext', 'main_original_image_width', 'COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_WIDTH', '', 'COM_EASYBLOG_PIXELS');
                    echo $this->html('settings.smalltext', 'main_original_image_height', 'COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_HEIGHT', '', 'COM_EASYBLOG_PIXELS');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'main_original_image_quality',
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY'),
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $options = array();

                        for( $i = 0; $i <= 100; $i += 10 )
                        {
                            $message	= $i;
                            $message	= $i == 0 ? JText::sprintf( 'COM_EASYBLOG_LOWEST_QUALITY_OPTION' , $i ) : $message;
                            $message	= $i == 50 ? JText::sprintf( 'COM_EASYBLOG_MEDIUM_QUALITY_OPTION' , $i ) : $message;
                            $message	= $i == 100 ? JText::sprintf( 'COM_EASYBLOG_HIGHEST_QUALITY_OPTION' , $i ) : $message;
                            $options[]	= JHTML::_('select.option', $i , $message );
                        }

                        echo JHTML::_('select.genericlist', $options, 'main_original_image_quality', '', 'value', 'text', $this->config->get('main_original_image_quality' ) );
                    ?>
                    <div class="help-block">
                        <?php echo JText::_( 'COM_EASYBLOG_SETTINGS_WORKFLOW_IMAGE_UPLOAD_QUALITY_HINTS' );?>
                    </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_THUMBNAILS_TITLE');?></b>
                <div class="card-text"><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_THUMBNAILS_DESC');?></div>
            </div>

            <div class="card-body">
                <?php
                    echo $this->html('settings.smalltext', 'main_image_thumbnail_width', 'COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_WIDTH', '', 'COM_EASYBLOG_PIXELS');
                    echo $this->html('settings.smalltext', 'main_image_thumbnail_height', 'COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_HEIGHT', '', 'COM_EASYBLOG_PIXELS');
                ?>

              <div class="row form-group d-flex mb-4">
                <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                    <?php echo $this->html(
                        'grid.label',
                        'main_image_thumbnail_quality',
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY'),
                        JText::_('COM_EASYBLOG_SETTINGS_MEDIA_QUALITY_DESC')
                    ); ?>
                </div>
                <div class="col-sm-12 col-md-7">
                    <?php
                        $options = array();

                        for( $i = 0; $i <= 100; $i += 10 )
                        {
                            $message	= $i;
                            $message	= $i == 0 ? JText::sprintf( 'COM_EASYBLOG_LOWEST_QUALITY_OPTION' , $i ) : $message;
                            $message	= $i == 50 ? JText::sprintf( 'COM_EASYBLOG_MEDIUM_QUALITY_OPTION' , $i ) : $message;
                            $message	= $i == 100 ? JText::sprintf( 'COM_EASYBLOG_HIGHEST_QUALITY_OPTION' , $i ) : $message;
                            $options[]	= JHTML::_('select.option', $i , $message );
                        }

                        echo JHTML::_('select.genericlist', $options, 'main_image_thumbnail_quality', '', 'value', 'text', $this->config->get('main_original_image_quality' ) );
                    ?>
                  <div class="help-block">
                      <?php echo JText::_( 'COM_EASYBLOG_SETTINGS_WORKFLOW_IMAGE_UPLOAD_QUALITY_HINTS' );?>
                  </div>
                </div>
              </div>
            </div>
        </div>
	</div>
</div>
