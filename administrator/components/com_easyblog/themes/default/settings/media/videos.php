<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_SETTINGS_MEDIA_VIDEOS_TITLE');?></b>
            </div>

            <div class="card-body">
                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'max_video_width',
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_WIDTH'),
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_WIDTH_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="row">
                          <div class="col-sm-12">
                            <div class="input-group">
                              <input type="text" name="max_video_width" class="form-control text-center " value="<?php echo $this->config->get('max_video_width' );?>" />
                              <span class="input-group-text"><?php echo JText::_('COM_EASYBLOG_PIXELS');?></span>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row form-group d-flex mb-4">
                  <div class="col-sm-12 col-md-5 d-flex justify-content-between mb-md-0 mb-2">
                      <?php echo $this->html(
                          'grid.label',
                          'max_video_height',
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_HEIGHT'),
                          JText::_('COM_EASYBLOG_SETTINGS_MEDIA_MAXIMUM_HEIGHT_DESC')
                      ); ?>
                  </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <input type="text" name="max_video_height" class="form-control text-center w-50" value="<?php echo $this->config->get('max_video_height' );?>" />
                                    <span class="input-group-text"><?php echo JText::_('COM_EASYBLOG_PIXELS');?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
