<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="row">
  <div class="col-sm-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <b><?php echo JText::_('COM_EASYBLOG_INTEGRATIONS_FLICKR_TITLE');?></b>
                <p class="card-text">
    				<?php echo JText::_('COM_EASYBLOG_INTEGRATIONS_FLICKR_INFO');?>
    			</p>
            </div>

            <div class="card-body">
    			<div style="line-height:28px;height:28px;">
    				<a href="http://www.flickr.com/services/apps/create/" target="_blank"><?php echo JText::_('COM_EASYBLOG_FLICKR_CREATE_APP');?></a>

    				<?php echo JText::_('COM_EASYBLOG_OR'); ?>

    				<a href="http://stackideas.com/docs/easyblog/administrators/integrations/integrating-with-flickr" target="_blank"><?php echo JText::_('COM_EASYBLOG_FLICKR_VIEW_DOC');?></a>
    			</div>

    			<?php
              echo $this->html('settings.toggle', 'layout_media_flickr', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_ENABLE_FLICKR');
              echo $this->html('settings.text', 'integrations_flickr_api_key', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_FLICKR_API_KEY', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_FLICKR_API_KEY_DESC');
              echo $this->html('settings.text', 'integrations_flickr_secret_key', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_FLICKR_SECRET_KEY', 'COM_EASYBLOG_SETTINGS_LAYOUT_DASHBOARD_FLICKR_SECRET_KEY_DESC');
          ?>
		</div>
	</div>
</div>
