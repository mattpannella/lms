<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');
?>
<div class="container-nav d-none">
	<a class="nav-sidebar-toggle" data-bp-toggle="collapse" data-bs-target=".app-sidebar-collapse">
		<i class="fa fa-bars"></i>
		<span><?php echo JText::_('COM_EASYBLOG_MOBILE_MENU');?></span>
	</a>
	<a class="nav-subhead-toggle" data-bp-toggle="collapse" data-bs-target=".subhead-collapse">
		<i class="fa fa-cog"></i>
		<span><?php echo JText::_('COM_EASYBLOG_MOBILE_OPTIONS');?></span>
	</a>
</div>
<style>
	.accordion-button[data-sidebar-item]:after {
		background-image: none;
	}
	.accordion-button {
		background-color: #fff !important;
	}
</style>
<div class="app-sidebar-nav accordion" data-sidebar>
	<?php foreach ($menus as $menu): ?>
		<?php
			$id = 'nav-' . strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $menu->view), '-'));
		?>

		<div class="accordion-item">
			<div class="accordion-header">
				<?php if (!empty($menu->childs)): ?>
					<a href="javascript:void(0);"
						<?php if ($menu->view == $view): ?>
							class="accordion-button"
							aria-expanded="true"
						<?php else: ?>
							class="accordion-button collapsed"
							aria-expanded="false"
						<?php endif; ?>
						data-bs-toggle="collapse"
						data-bs-target="#<?php echo $id; ?>"
						aria-controls="<?php echo $id; ?>"
						data-sidebar-parent
					>
						<?php if (!empty($menu->icon)): ?>
							<i class="fa <?php echo $menu->icon; ?>"></i>
						<?php endif; ?>

						<span class="ms-2">
							<?php echo JText::_($menu->title); ?>
						</span>

						<?php if (!empty($menu->counter)): ?>
							<span class="badge"><?php echo $menu->counter;?></span>
						<?php endif; ?>
					</a>
				<?php else: ?>
					<a href="<?php echo $menu->link; ?>" class="text-dark accordion-button" data-sidebar-item>
						<?php if (!empty($menu->icon)): ?>
							<i class="fa <?php echo $menu->icon; ?>"></i>
						<?php endif; ?>

						<span class="ms-2">
							<?php echo JText::_($menu->title); ?>
						</span>

						<?php if (!empty($menu->counter)): ?>
							<span class="badge"><?php echo $menu->counter;?></span>
						<?php endif; ?>
					</a>
				<?php endif; ?>

			</div>

		<?php if (!empty($menu->childs)): ?>
			<div class="accordion-collapse collapse<?php echo $menu->view == $view ? ' show' : ''; ?>" id="<?php echo $id; ?>" data-sidebar-child>
				<?php foreach ($menu->childs as $child): ?>
					<div
						<?php if ($layout == $child->url->layout): ?>
							class="p-2 ms-4 childItem active"
						<?php else: ?>
							class="p-2 ms-4 childItem"
						<?php endif; ?>
					>
						<a href="<?php echo $child->link;?>">
							<?php if (!empty($child->icon)): ?>
								<i class="fa <?php echo $child->icon; ?>"></i>
							<?php endif; ?>
							<?php echo JText::_($child->title); ?>

							<?php if (!empty($child->counter)): ?>
								<span class="badge"><?php echo $child->counter; ?></span>
							<?php endif ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
		</div>
	<?php endforeach; ?>
</div>
