<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined('_JEXEC') or die('Unauthorized Access');

class EasyBlogAclHelper
{
	/**
	 * generate sql used for blogger retrieval
	 *
	 * @since	5.0
	 * @access	public
	 * @param
	 * @return string
	 */

	public static function genIsbloggerSQL($column = 'a.id')
	{
		$db = EB::db();

		$aclQuery = "inner join `joom_user_usergroup_map` as up ON up.`user_id` = $column
									inner join `joom_easyblog_acl_group` as ag ON ag.`content_id` = up.`group_id` 
									inner join `joom_easyblog_acl` as acl on ag.`acl_id` = acl.`id` 
									where acl.`action` = 'add_entry' and ag.`type` = 'group' and ag.`status` = 1";

		return $aclQuery;
	}
}
