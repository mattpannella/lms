<?php


defined('_JEXEC') or die();

class ReportsControllerReports extends FOFController {

	function apply_and_redirect($key = null, $urlVar = null) {
		parent::save($key, $urlVar);
		$id = (int)JRequest::getVar('id');
		if ($id) {
			$this->setRedirect("index.php?option=com_reports&view=report&layout=report&id=" . $id);
		}
	}

	function get_data() {
		$id = (int)JRequest::getVar('report');
		$db = JFactory::getDbo();
		$query = "SELECT * FROM axs_reports WHERE id = $id";
		$db->setQuery($query);
		$report = $db->loadObject();
		if($report->report_type == 'external') {
			echo $this->external_data($report);
		} else {
			// convert date columns to EST
			$data = json_decode($this->local_data($report), true);
			for ($i = 0; $i < count($data); $i++) {
				$columns = array_keys($data[$i]);
				foreach ($columns as $column) {
					if (DateTime::createFromFormat('Y-m-d H:i:s', $data[$i][$column]) !== false) {
						$data[$i][$column] = JHtml::_('date', $data[$i][$column], 'Y-m-d H:i:s');
					}
				}
			}

			echo json_encode($data);
		}
	}

	function local_data($report) {
		$model = $this->getModel();
		$data = $model->getData($report);
		return $data->grid->items;
	}

	function external_data($report) {
		if (JRequest::getVar('url')) {
			$url = urldecode(JRequest::getVar('url'));
		} else {
			$params = json_decode($report->params);
			$url = $params->external_url;
		}

		$auth = JRequest::getVar('auth');
		
		$curl = curl_init($url);
		if ($auth) {
			$auth = base64_decode($auth);
			$auth = json_decode($auth);
			//echo (json_encode($auth->type));
			switch ($auth->type) {
				case "password":
					$username = $auth->username;
					$password = $auth->password;

					$creds = "" . $auth->username . ":" . $auth->password;

					curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
					curl_setopt($curl, CURLOPT_USERPWD, $creds); //Your credentials goes here

					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
					curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

					break;
				case "token":
					$token = $auth->token;
					break;
			}
		} 
		
		curl_setopt($curl, CURLOPT_TIMEOUT, 5);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($curl);
		curl_close($curl);
		if($data[0] == "[") {
			$result = $data;
		} else {
			$result = '['.$data.']';
		}
		if(JRequest::getVar('url')) {
			echo $result;
		} else {
			return $result;
		}
	}

	/*
		Updates an existing spreadsheet.
	*/
	function save_spreadsheet() {
		$report_id = (int)JRequest::getVar('report_id');
		$spreadsheet_id = (int)JRequest::getVar('view_id');
		$type = JRequest::getVar('type');
		$snapshot = JRequest::getVar('snapshot');

		$db = JFactory::getDBO();

		$query = "SELECT snapshot FROM axs_spreadsheets WHERE id=$spreadsheet_id AND report_id=$report_id";
		$db->setQuery($query);
		$result = $db->loadObject();

		if (is_countable($result) && count($result) > 0) {
			$file = json_decode($result->snapshot)->spreadsheet;
		}

		$snapshot = $this->save_to_local_file($snapshot, $type, $file);

		if (!$snapshot) {
			echo "failed\n";
			return;
		}

		$query = $db->getQuery(true);

		$fields = array(
			$db->quoteName('snapshot') . '=' . $db->quote($snapshot)
		);

		$conditions = array(
			$db->quoteName('report_id') . '=' . (int)$report_id,
			$db->quoteName('id') . '=' . (int)$spreadsheet_id
		);

		$query
			->update($db->quoteName('axs_spreadsheets'))
			->set($fields)
			->where($conditions);

		$db->setQuery($query);
		$db->execute();

		echo "success";
	}

	function load_spreadsheet() {
		$report_id = (int)JRequest::getVar('report_id');
		$spreadsheet_id = (int)JRequest::getVar('view_id');

		$query = "SELECT snapshot FROM axs_spreadsheets WHERE report_id = $report_id AND id = $spreadsheet_id";
		$db = JFactory::getDBO();
		$db->setQuery($query);

		$snapshot = $this->get_from_local_file($db->loadObject()->snapshot);

		echo $snapshot;
	}

	/*
		Saves a new spreadsheet.
	*/
	function save_as_spreadsheet() {
		$report_id = JRequest::getVar('report_id');
		$snapshot = JRequest::getVar('snapshot');
		$type = JRequest::getVar('type');
		$name = JRequest::getVar('name');

		$db = JFactory::getDBO();

		$snapshot = $this->save_to_local_file($snapshot, $type);
		if (!$snapshot) {
			echo "failed\n";
			return;
		}

		$query = $db->getQuery(true);
		$columns = array(
			'report_id', 
			'name', 
			'snapshot'
		);

		$values = array(
			(int)$report_id,
			$db->quote($name),
			$db->quote($snapshot)
		);

		$query
			->insert($db->quoteName('axs_spreadsheets'))
			->columns($db->quoteName($columns))
			->values(implode(',', $values));

		$db->setQuery($query);
		$db->execute();

		echo $db->insertid();
	}

	function delete_spreadsheet() {

		$report_id = (int)JRequest::getVar('report_id');
		$spreadsheet_id = (int)JRequest::getVar('view_id');

		$db = JFactory::getDBO();
		$query = "UPDATE axs_spreadsheets SET delete_date = CURRENT_TIMESTAMP WHERE report_id = $report_id AND id = $spreadsheet_id";

		$db->setQuery($query);
		$db->execute();
	}	

	function get_all_names() {
		//Get the names of all of the spreadsheets for the given report.
		$report_id = (int)JRequest::getVar('report_id');

		$db = JFactory::getDBO();
		$query = "SELECT name, id FROM axs_spreadsheets WHERE report_id = $report_id AND delete_date IS NULL ORDER BY name ASC";
		$db->setQuery($query);
		$results = $db->loadObjectList();

		echo json_encode($results);
	}

	function check_overwrite() {
		//Check to see if a report with the selected name already exists. 
		//If so, return true.  If not, return false.

		$report_id = JRequest::getVar('report_id');
		$name = JRequest::getVar('name');

		$db = JFactory::getDBO();

		$report_id = (int)$report_id;
		$name = $db->quote($name);

		$query = "SELECT id FROM axs_spreadsheets WHERE report_id = $report_id AND name = $name AND delete_date IS NULL";
		$db->setQuery($query);

		$result = $db->loadObjectList();

		if (count($result) > 0) {
			echo $result[0]->id;
		} else {
			echo "-1";
		}
	}

	function purge_deleted() {

		$db = JFactory::getDBO();

		// Remove all entries that are older than 30 days.

		$query = "DELETE FROM axs_spreadsheets WHERE delete_date < (NOW() - INTERVAL 30 DAY)";
		$db->setQuery($query);
		$db->execute();

		//Get a list of all of the remaining snapshots.

		$query = "SELECT snapshot FROM axs_spreadsheets";
		$db->setQuery($query);
		$results = $db->loadObjectList();

		// Get a list of all of the reports in the spreadsheets folder.

		$files = array();
		$dir = $this->getSpreadsheetFileLocation();
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if ($file != "." && $file != "..") {	//Skip directory listings
						$fileObj = new stdClass();
						$fileObj->file = $file;
						$fileObj->inUse = false;

						// Loop through all of the spreadsheets and see if the report is being used.  If so, flag it as in use.
						foreach ($results as $result) {
							$snapshot = json_decode($result->snapshot);
							if (isset($snapshot->spreadsheet)) {
								if ($snapshot->spreadsheet == $file) {
									$fileObj->inUse = true;
								}
							}
						}						
						array_push($files, $fileObj);
					}
				}
				closedir($dh);
			}
		}

		// Loop through all of the files and delete any that are not in use.
		foreach ($files as $file) {
			if (!$file->inUse) {
				//Delete the file.
				unlink($this->getSpreadsheetFileLocation() . $file->file);
			}
		}		
	}

	private function get_from_local_file($snapshot) {
		
		$final = new stdClass();

		$snapshot = json_decode($snapshot);

		if (isset($snapshot->spreadsheet) && isset($snapshot->key)) {
			$key = base64_decode($snapshot->key);

			$dest = $this->getSpreadsheetFileLocation() . $snapshot->spreadsheet;

			$zip = new ZipArchive();

			if ($zip->open($dest) !== TRUE) {
				echo "failed";
				return null;
			}

			$data = $zip->getFromName('data');
			$zip->close();

			$spreadsheet = AxsEncryption::decrypt($data, $key);
			$final->spreadsheet = $spreadsheet;
		}

		if (isset($snapshot->grid)) {
			$final->grid = $snapshot->grid;
		}

		return json_encode($final);
	}

	private function save_to_local_file($snapshot, $type, $report_name = null) {

		$snapshot = json_decode($snapshot);
		
		if ($type == 'grid') {
			$final = new stdClass();
			$final->grid = $snapshot->grid;
			return json_encode($final);
		}

		if ($report_name == null) {
			$time = strtotime('now');
			$rand = rand(100000, 999999);
			$report_name = "report_" . $time . "_" . $rand;
		}

		$key = AxsEncryption::createRandomKey();

		$final = new stdClass();

		if ($type == 'both') {
			$final->grid = $snapshot->grid;
		}

		$data = $snapshot->spreadsheet;

		$final->spreadsheet = $report_name;
		$final->key = base64_encode($key);		

		//The location needs to exist if the above is not active.

		$dest = $this->getSpreadsheetFileLocation() . $report_name;		
		$data = AxsEncryption::encrypt($data, $key);

		$zip = new ZipArchive();

		if ($zip->open($dest, ZipArchive::CREATE) !== TRUE) {
			return false;
		}

		$zip->addFromString('data', $data);		
		$zip->close();

		$final->filesize = filesize($dest);

		if ($final->filesize == false) {			
			return false;
		}

		return json_encode($final);
	}

	function getSpreadsheetFileLocation() {
		$location = "/var/www/files/users/" . AxsClients::getClientFolder() . "/spreadsheets/";
		if (!is_dir($location)) {
			//Check if the location already exists.  If not, create it.
			mkdir ($location, 0775, true);
		}
		return $location;
	}
}
