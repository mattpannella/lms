<?php


defined('_JEXEC') or die();

class ReportsControllerGantts extends FOFController {

	function saveChart() {
		$id = (int)JRequest::getVar('id');
		$data = JRequest::getVar('data');
		$data_dependencies = JRequest::getVar('data_dependencies');
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$fields = array();

		if($data) {
			array_push($fields, $db->quoteName('data') . '=' . $db->quote($data));
		}

		if($data_dependencies) {
			array_push($fields, $db->quoteName('data_dependencies') . '=' . $db->quote($data_dependencies));
		}

		$conditions = array(
			$db->quoteName('id') . '=' . $id
		);

		$query
			->update($db->quoteName('axs_gantt_charts'))
			->set($fields)
			->where($conditions);

		$db->setQuery($query);
		$result = $db->execute();

		if($result) {
			echo "success";
		} else {
			echo "error";
		}
				
	}
}