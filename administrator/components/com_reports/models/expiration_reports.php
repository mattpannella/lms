<?php
defined('_JEXEC') or die;

class ReportsModelExpiration_reports extends FOFModel {


	public function save($data) {
		$params = new stdClass();
		$data['name'] = AxsSecurity::alphaNumericOnly(AxsSecurity::cleanInput($data['name']));
        $params->theme = $data['theme'];
		$params->content_types = implode(',',$data['content_types']);
        $data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['theme'] = $params->theme;
			$data['content_types'] = explode(',',$params->content_types);
			return $data;
		}
	}
}