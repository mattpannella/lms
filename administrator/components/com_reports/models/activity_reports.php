<?php
defined('_JEXEC') or die;

class ReportsModelActivity_reports extends FOFModel {


	public function save($data) {
		$params = new stdClass();
		$data['name'] = AxsSecurity::alphaNumericOnly(AxsSecurity::cleanInput($data['name']));
        $params->theme = $data['theme'];
        $params->course_selection_type = !empty($data['course_selection_type']) ? $data['course_selection_type'] : null;
		$params->courses = !empty($data['courses']) ? implode(',',$data['courses']) : null;
		$params->user_selection_type = $data['user_selection_type'];
        $params->userlist = $data['userlist'];
        $params->usergroups_selection_type = $data['usergroups_selection_type'];
        $params->usergroup_filter = !empty($data['usergroup_filter']) ? implode(',',$data['usergroup_filter']) : null;
        $params->certificates_selection_type = $data['certificates_selection_type'];
        $params->certificates = !empty($data['certificates']) ? implode(',',$data['certificates']) : null;
        $params->badges_selection_type = $data['badges_selection_type'];
		$params->badges = !empty($data['badges']) ? implode(',',$data['badges']) : null;
		$params->include_archived_activity = $data['include_archived_activity'];
		$params->milestones_selection_type = $data['milestones_selection_type'];
        $params->milestones = !empty($data['milestones']) ? implode(',',$data['milestones']) : null;
		$params->data_points = !empty($data['data_points']) ? implode(',',$data['data_points']) : null;
		$params->default_duration = round($data['default_duration']);
        $data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['theme'] = $params->theme;
	        $data['course_selection_type'] = $params->course_selection_type;
			$data['courses'] = $params->courses;
			$data['user_selection_type'] = $params->user_selection_type;
	        $data['userlist'] = $params->userlist;
	        $data['usergroups_selection_type'] = $params->usergroups_selection_type;
	        $data['usergroup_filter'] = explode(',',$params->usergroup_filter);
	        $data['milestones_selection_type'] = $params->milestones_selection_type;
	        $data['milestones'] = explode(',',$params->milestones);	        
	        $data['certificates_selection_type'] = $params->certificates_selection_type;
	        $data['certificates'] = explode(',',$params->certificates);
	        $data['badges_selection_type'] = $params->badges_selection_type;
	        $data['badges'] = explode(',',$params->badges);
			$data['include_archived_activity'] = $params->include_archived_activity;
			$data['data_points'] = explode(',',$params->data_points);
			$data['default_duration'] = round($params->default_duration);
			return $data;
		}
	}
}