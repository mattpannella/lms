<?php

defined('_JEXEC') or die;

class ReportsModelCustom_reports extends FOFModel {

	public function createUniqueID() {
		$timestamp = strtotime('now');
		$uniqueID = $timestamp.self::randomString();
		return $uniqueID;
	}

	public function save($data) {
		$buttons = json_decode(json_encode($data['buttons']));
		foreach ($buttons as $button) {
			if (!$button->id) {
				$button->id = AxsLMS::createUniqueID();
			}
		}		
		$params = new stdClass();
		$params->report_theme = $data['report_theme'];
		$params->buttons = json_encode($buttons);
		$params->fields = json_encode($data['fields']);
		$params->success_message = $data['success_message'];
		$params->button_size = $data['button_size'];
		$params->button_position = $data['button_position'];
		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	private function createEmbedCode($data) {
		$embed       = '';
		$fields      = json_decode($data['fields']);		
		$id 	 	 = $data['id'];
		$buttonsCode = self::buildButtons($data,$id);
		$fieldsArray = new stdClass();
		if($buttonsCode) {
			$embed .= $buttonsCode."\n";
		}
		
		$embed .= '<script type="text/javascript" src="/js/get-params.js"></script>'."\n";
		$embed .= '<script>'."\n";		
		if($fields) {
			foreach($fields as $field) {
				$embed .= 'var '.$field->parameter.' = ';
				if($field->decode_url) {
					$embed .= 'decodeURI(';
				}

				$embed .= 'getUrlParam("'.$field->parameter.'", "'.$field->default.'")';

				if(!$field->decode_url) {
					$embed .= ';'."\n";
				}

				if($field->decode_url) {
					$embed .= ');'."\n";
				}
				$fieldsArray->{$field->parameter} = $field->parameter;
			}				
		}
		if($fieldsArray) {
			$embed .= 'var data = '. str_replace('"', '', json_encode($fieldsArray)) .'';
		}			
		$embed .= '</script>'."\n";			
	
		$embed .= '<script type="text/javascript" src="/js/custom-report.js"></script>'."\n";
		return $embed;
	}

	private function buildButtons($data,$id) {
		$buttons = json_decode($data['buttons']);
		switch ($data['button_position']) {
			case 'center':
				$style = "text-align:center;";
			break;

			case 'right':
				$style = "float: right;";
			break;
			
			default:
				$style = "";
			break;
		}

		switch ($data['button_size']) {
			case 'small':
				$size = "-sm";
			break;

			case 'large':
				$size = "-lg";
			break;

			default:
				$size = "";
			break;
		}		
		$output  = '<div class="button_container button_container_'.$id.'" style="width:100%; clear:both; '.$style.'">';
		foreach ($buttons as $item) {
			$params = NULL;
			$encryptedParams = NULL;
			// build the params to be encrypted and passed into button html
			$params = new stdClass();
			$params->notification_id = $id;
			$params->action = $item->button_action;
			$output .= ' <button class="btn'.$size.' btn-'.$item->button_color.' customReportButton" data-id="'.$id.'" data-action="'.$item->button_action.'" data-url="'.$item->button_url.'">'."\n".' <i class="lizicon '.$item->button_icon.'"></i> '.$item->button_text."\n".' </button> ';
		}
		$output .= '</div>';
		return $output;
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {			
			$params          			= json_decode($data['params']);
			$data['fields']  			= $params->fields;
			$data['buttons'] 	  		= $params->buttons;
			$data['report_theme'] 		= $params->report_theme;
			$data['success_message'] 	= $params->success_message;
			$data['button_size']		= $params->button_size;
			$data['button_position']	= $params->button_position;
			$data['embed'] 				= self::createEmbedCode($data);
			return $data;
		}
	}
}