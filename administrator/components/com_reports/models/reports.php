<?php
defined('_JEXEC') or die;

 //error_reporting(E_ALL);
 //ini_set('display_errors', 1);
class ReportsModelReports extends FOFModel {

    private static $params = null;

    public function buildGridData($items, $columns, $report, $aggregates = null, $schema = null) {

        $gridColumns = [];

        foreach ($columns as $column) {
            $newColumn = [];

            foreach ($column as $key => $value) {
                $newColumn[$key] = $value;
            }

        	array_push($gridColumns, $newColumn);
        }

        $data = new stdClass();
        $data->grid = new stdClass();

       	$data->grid->columns = json_encode($gridColumns);
        $data->grid->items = json_encode($items);

        if ($aggregates) {
            $data->grid->aggregates = json_encode($aggregates);
        } else {
            $data->grid->aggregates = null;
        }

        if ($schema) {
            //Build the schema in the required format.
            $newSchema = new stdClass();
            $newSchema->model = new stdClass();
            $newSchema->model->fields = new stdClass();
            foreach ($schema as $s) {
                $key = $s->field;
                $newSchema->model->fields->$key = ["type" => $s->schema];
            }
            $data->grid->schema = json_encode($newSchema);
        } else {
            $data->grid->schema = null;
        }

        return $data;
    }

    public function buildSpreadsheetData($items, $columns, $report) {

        $extraRows = 20;
	  	$rows = [];
        $headers = [ "cells" => [] ];
        $columnWidths = [];

        foreach ($columns as $column) {

        	if ($column->width) {
        		$width = $column->width;
        	} else {
        		$width = 150;
        	}

        	$newColumn = [
				"value" => $column->title,
				"textAlign" => "center",
				"background" => "rgb(236,239,241)",
				"bold" => "true",
				"color" => "black"
			];

		 	$newColumnWidth = [
	        	"width" => $width
	        ];

	        array_push($columnWidths, $newColumnWidth);
        	array_push($headers["cells"], $newColumn);
        }

        array_push($rows, $headers);

        $colArray = array();
        foreach ($columns as $item){
        	array_push($colArray, $item);
        }

        $numColumns = count($colArray);
        $numRows = count($items) + $extraRows;
        $lastColumnLetter = self::getColumnLetter($numColumns);

        $min = 0;
        $max = $numRows;

        for ($r = $min; $r < $max; $r++){
        	$newCell = [ "cells" => [] ];

			for ($c = 0; $c < $numColumns; $c++) {

                $val = $items[$r]->{$colArray[$c]->field};

                if (is_numeric($val)) {
                    $val = (float)$val;
                }

				$cell = [
		        	"value" => $val,
		            "textAlign" => "left",
		            "background" =>"rgb(255,255,255)",
		            "bold" => "false",
		            "color" => "black"
		        ];

			 	array_push($newCell["cells"], $cell);
			}

			array_push($rows, $newCell);
		}

	    $sheets = [
	    	"rows" => $numRows,
			"sheets" => [
				[
					"name" => $report->name,
					"filter" => [
						"ref" => "A1:".$lastColumnLetter.$numRows
					],
					"rows" => $rows,

					"columns" => $columnWidths
				]
	        ]
        ];

        $data = json_encode($sheets, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);

        return $data;
    }

    private function getCustomFieldData($params) {

        $lang = JFactory::getLanguage();
        $lang->load("com_community", JPATH_SITE, "en-GB", true);

        $data = array();

        if (isset($params->use_custom_fields) && $params->use_custom_fields) {

            $fields = json_decode($params->custom_fields);

            $db = JFactory::getDBO();
            $query = $db->getQuery(true);

            $query
                ->select('*')
                ->from($db->quoteName('#__community_fields'))
                ->where('FIND_IN_SET(id, "' . implode(",", $fields) . '")')
                ->order('ordering ASC');
            $db->setQuery($query);
            $results = $db->loadObjectList();

            $names = array();

            foreach ($results as &$result) {
                $data[$result->id] = new stdClass();
                $data[$result->id]->name = JText::_($result->name);
                $data[$result->id]->type = $result->type;
                $data[$result->id]->params = $result->params;
            }

            /*foreach ($results as $result) {
                $names[$result->id] = JText::_($result->name);
            }

            foreach ($fields as $field) {
                $query->clear();
                $query
                    ->select('*')
                    ->from($db->quoteName('#__community_fields_values'))
                    ->where($db->quoteName('field_id') . '=' . $db->quote($field));

                $db->setQuery($query);
                $results = $db->loadObjectList();

                $data[$names[$field]] = $results;
            }*/

            //return $results;
        }

        return $data;
    }

    private static function getCustomFieldSQL(&$fields, &$columns, $params, $report) {
        $db = JFactory::getDbo();
        $custom_data = self::getCustomFieldData($params);

        if ($custom_data) {

            switch ($report->report_type) {
                case "store":
                    //The store data gets its ID from elsewhere.
                    $user_id = "orders.customer_id";
                    break;
                default:
                    $user_id = "user.id";
                    break;
            }

            foreach ($custom_data as $key => $value) {
                $column = new stdClass;

                $fieldname = "custom_$key";

                $column->field = $fieldname;
                $column->title = $value->name;
                $column->format = "";
                $column->filter = "";
                $column->width = 150;
                $column->type = "";
                $column->filterable = "";
                $column->aggregates = "";
                $column->footerTemplate = "";

                $columns->$fieldname = $column;

                $field_value = "";

                if ($key != 2 && ($value->type != "date" && $value->type != "birthdate")) {
                    $field_value = "
                        (SELECT value
                            FROM joom_community_fields_values
                            WHERE user_id = $user_id AND field_id = $key
                            LIMIT 1) AS $fieldname,";
                } else if ($key != 2 && ($value->type == "date" || $value->type == "birthdate")) {
                    // translate custom field value based on the user-specified date format
                    $custom_field_parameters = json_decode($value->params);
                    $date_format = stripcslashes($custom_field_parameters->date_format);
                    // translate joomlas custom date format to a format
                    // that works with mysql 5.6 date_format()
                    // see https://dev.mysql.com/doc/refman/5.6/en/date-and-time-functions.html#function_date-format
                    if(!$date_format) {
                        $date_format = 'Y-m-d';
                    }

                    $date_format = str_replace(
                        array('m', 'd', 'y', 'M', 'D', 'Y', 'F', 'l'),
                        array('%m', '%d', '%y', '%b', '%a', '%Y', '%M', '%W'),
                        $date_format
                    );

                    $field_value = "
                        (SELECT DATE_FORMAT(
                            (SELECT value
                                FROM joom_community_fields_values
                                WHERE user_id = $user_id AND field_id = $key
                                LIMIT 1),
                            ".$db->quote($date_format).")) AS ".$db->quote($fieldname).",";
                } else {
                    $field_value = "
                        (SELECT
                            CASE
                                WHEN value='COM_COMMUNITY_MALE' THEN 'Male'
                                WHEN value='COM_COMMUNITY_FEMALE' THEN 'Female'
                            END AS value
                            FROM joom_community_fields_values
                            WHERE user_id = $user_id AND field_id = 2 LIMIT 1)
                            AS $fieldname,";
                }


                $fields .= $field_value;
            }
        }
    }

    public function getEducationProgressData($layout, $params, $report, $settings) {
        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];
        $getCertificates = false;

        if (count($settings) == 0) {
            return null;
        }

        self::getCustomFieldSQL($fields, $columns, $params, $report);

        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";

            switch ($setting) {

                // User data fields
                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                break;

                // Course data fields
                case "category":
                    $column_title = "Category";
                    $fields .= "(SELECT cat.title FROM joom_splms_coursescategories AS cat JOIN joom_splms_courses AS course WHERE course.splms_coursescategory_id = cat.splms_coursescategory_id AND progress.course_id = course.splms_course_id) AS category,";
                break;

                case "course":
                    $column_title = "Course";
                    $fields .= "(SELECT course.title FROM joom_splms_courses AS course WHERE course.splms_course_id = progress.course_id) AS course,";
                    $filter = new stdClass();
                    $operators = new stdClass();
                    $string = new stdClass();
                    $string->contains = "Contains";
                    $operators->string = $string;
                    $filter->operators = $operators;
                    $column_filterable = $filter;
                break;
                case "course_score":
                    $column_title = "Score";
                    $column_type = "number";
                    $column_format = "{0:n0}%";
                    $fields .= "MAX(progress.score) AS course_score,";
                break;
                case "course_progress":
                    $column_title = "Progress";
                    $column_type = "number";
                    $column_format = "{0:n0}%";
                    $fields .= "MAX(progress.progress) AS course_progress,";
                break;
                case "course_due_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Due Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "CASE WHEN progress.date_due IS NOT NULL
                                THEN
                                MAX(progress.date_due)
                                ELSE 'Open'
                                END  AS course_due_date,";
                break;
                case "course_start_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Start Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "MAX(progress.date_started)  AS $setting,";
                break;
                case "course_completed_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Completed Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "MAX(progress.date_completed)  AS $setting,";
                break;
                case "course_last_activity_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Last Activity Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "MAX(progress.date_last_activity)  AS $setting,";
                break;
                case "archive_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Archive Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "progress.archive_date AS $setting,";
                break;

                // Certificate fields
                case "certificate_name":
                    
                    $filter = new stdClass();
                    $operators = new stdClass();
                    $string = new stdClass();

                    $column_title = 'Certificate Name';
                    $column_width = 200;
                    $column_type = 'string';

                    $string->contains = "Contains";
                    $operators->string = $string;
                    $filter->operators = $operators;
                    $column_filterable = $filter;

                    $getCertificates = true;

                    $fields .= "awards.title AS $setting,";
                break;
                case "certificate_earned_date":
                    $filter = new stdClass();
                    $filter->ui = 'datepicker';

                    $column_title = 'Certificate Earned Date';
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:M/d/yyyy}";
                    $column_filterable = $filter;
                    
                    $schemaObject = new stdClass();
                    $schemaObject->field = $setting;
                    $schemaObject->schema = '';

                    $schema[] = $schemaObject;

                    $getCertificates = true;

                    $fields .= "DATE(awards_earned.date_earned) AS $setting,";

                break;
                case "certificate_expiry_date":
                    $filter = new stdClass();
                    $filter->ui = 'datepicker';

                    $column_title = 'Certificate Expiry Date';
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:M/d/yyyy}";
                    $column_filterable = $filter;

                    $getCertificates = true;

                    $fields .= "DATE(awards_earned.date_expires) AS $setting,";
                break;
                default: break;
            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }

            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $tables = "
            FROM joom_splms_course_progress AS progress
            JOIN joom_users AS user ON progress.user_id = user.id
        ";

        if($getCertificates) {
            $tables .= "
                LEFT JOIN axs_awards_earned AS awards_earned ON progress.user_id = awards_earned.user_id
                INNER JOIN axs_awards AS awards ON awards_earned.badge_id = awards.id
            ";

            $conditions[] = "awards.type = 'certificate'";
            $conditions[] = 'awards.enabled = 1';
            $conditions[] = 'user.id = awards_earned.user_id';
        }

        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $conditions[]= 'progress.archive_date IS NULL';
        }

        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE progress.user_id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        if ($params->include_archived_activity == "1") {
            $order = " GROUP BY archive_date, progress.user_id, progress.course_id ORDER BY progress.id DESC";
        } else {
            $order = " GROUP BY progress.user_id, progress.course_id ORDER BY progress.id DESC";
        }

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEducationAssignedData($layout, $params, $report, $settings) {

        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $include_quiz_results_table = false;
        $include_lesson_status_table = false;
        $include_course_purchases_table = false;
        $include_courses_table = false;
        $include_lessons_table = false;

        //$start_date = $params->education_start_date;
        //$end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            $column_template = "";
            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;

                case "category":
                    $column_title = "Category";
                    $fields .= "(SELECT cat.title FROM joom_splms_coursescategories AS cat JOIN joom_splms_courses AS course WHERE course.splms_coursescategory_id = cat.splms_coursescategory_id AND progress.course_id = course.splms_course_id) AS category,";
                    $include_quiz_results_table = true;

                    break;

                case "assignment":
                    $column_title = "Assignment";
                    $fields .= "assignment.title AS $setting,";
                    $filter = new stdClass();
                    $operators = new stdClass();
                    $string = new stdClass();
                    $string->contains = "Contains";
                    $operators->string = $string;
                    $filter->operators = $operators;
                    $column_filterable = $filter;

                    break;

                case "courses":
                    $column_title = "Courses";
                    $filter = new stdClass();
                    $operators = new stdClass();
                    $string = new stdClass();
                    $column_width = 200;
                    $string->contains = "Contains";
                    $operators->string = $string;
                    $filter->operators = $operators;
                    $column_filterable = $filter;
                    $column_template = '#=createNewLines(courses)#';
                    break;

                case "assignment_progress":
                    $column_title = "Progress";
                    $column_type = "number";
                    $column_format = "{0:n0}%";
                    $fields .= "assignment.course_ids AS course_list,";
                    $fields .= "user.id AS userId,";
                    $fields .= "assignment.id as assignment_progress,";
                    break;
                case "assignment_due_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Due Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "CASE WHEN assignment.due_date IS NOT NULL
                                    THEN
                                    assignment.due_date
                                    ELSE 'Open'
                                    END  AS $setting,";
                    break;
                case "assignment_start_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Start Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "assignment.start_date  AS $setting,";
                    break;
                case "assignment_end_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "End Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "assignment.end_date  AS $setting,";
                    break;
                case "assignment_last_activity_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Last Activity Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "assignment.id  AS $setting,";
                    break;
                case "assignment_completed_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Completed Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "assignment.id  AS $setting,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if($column_template) {
                $column->template = $column_template;
            }

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        /*if ($start_date) {
            if ($end_date) {
                $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
            } else {
                $where = " WHERE per.start >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE per.end <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }    */

        //$items = array();

        $query = "SELECT * FROM joom_splms_courses_groups WHERE id = ".$db->quote($params->assignment);
        $db->setQuery($query);
        $results = $db->loadObjectList();

        foreach($results as $result) {
            if($result->user_ids) {
                $assignedUsers = explode(',',$result->user_ids);

            } elseif($result->usergroups) {
                $groupUsers = AxsAlerts::getUsersFromGroupList($result->usergroups);
                foreach($groupUsers as $user) {
                    $uid = (int)$user->user_id;
                    $assignedUsers[] = $uid;
                }
            }
        }

        $userList = implode(',',$assignedUsers);
        if(!$userList) {
            return false;
        }
        $tables = "FROM joom_users as user";
        $tables .= " JOIN joom_splms_courses_groups AS assignment ON assignment.id = ".$db->quote($params->assignment);
        $conditions[] = "user.id IN ($userList)";
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        //$order = " GROUP BY progress.user_id, progress.course_id";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        foreach($items as &$item) {
            $progress = '';
            $progress = AxsLMS::getAssignmentProgress($item->course_list,$item->userId);
            $courses = AxsLMS::getCourseListProgress($item->course_list,$item->userId);
            $courseList = '';

            foreach($courses->courseList as $key => $value) {
                if(!$value) {
                    $value = 0;
                };
                $courseList .= $key.': <b>'.round($value)."%</b><br>";
            }
            $item->courses = $courseList;
            $item->assignment_progress = $progress->percentageComplete;
            $item->assignment_last_activity_date = $progress->last_activity_date;
            $item->assignment_completed_date = $progress->completed_date;
        }

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;

    }

    public function getEducationSurvey_summaryData($layout, $params, $report, $settings) {
        $db = JFactory::getDbo();
        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $start_date = $params->education_start_date;
        $end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            $column_template = "";
            switch ($setting) {

                //User Info
                case "survey_name":
                    $column_title = "Survey";
                    $fields .= "surveyRow.title AS survey_name,";
                    break;

                case "results_button":
                    $column_title = "View Results";
                    $fields .= "survey.user_id AS surveyUser,survey.survey_id AS surveyId,";
                    $column_template = '<span class="k-button k-button-icontext getSubmission" data-id="#: surveyId #" data-download="#: surveyId #" data-user="#: surveyUser #" data-type="survey"><i class="fa fa-external-link"></i> View Results</span>';
                    break;

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;
                case "nps":
                    $column_title = "NPS";
                    $column_type = "number";
                    $fields .= "survey.nps AS nps,";
                    $column_aggregates = array("average");
                    $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    break;
                case "csat":
                    $column_title = "CSAT Score";
                    $column_type = "number";
                    $fields .= "survey.csat AS csat,";
                    $column_aggregates = array("average");
                    $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    break;
                case "csat_average":
                    $column_title = "CSAT Average";
                    $column_type = "number";
                    $fields .= "survey.average_score AS csat_average,";
                    $column_aggregates = array("average");
                    $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    break;
                /* case "progress":
                    $column_title = "Progress";
                    $column_type = "number";
                    $fields .= "survey.progress AS progress,";
                    $column_aggregates = array("average");
                    $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    break;
                case "date_started":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Started";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.date_started AS date_started,";
                    break; */
                case "date_completed":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Completed";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.date_completed AS date_completed,";
                    break;
                /* case "date_last_activity":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Submitted";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.date_last_activity AS date_last_activity,";
                    break;   */
                case "archive_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Archive Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.archive_date AS $setting,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if($column_template) {
                $column->template = $column_template;
            }

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        if ($start_date) {
            if ($end_date) {
                $conditions[] = "( DATE(survey.date_completed) >= DATE('$start_date') AND DATE(survey.date_completed) <= DATE('$end_date') )";
            } else {
                $conditions[] = "( DATE(survey.date_completed) >= DATE('$start_date') )";
            }
        } else {
            if ($end_date) {
                $conditions[] = "( DATE(survey.date_completed) <= DATE('$end_date') )";
            }
        }

        if($params->survey_summary && $params->survey_summary != 'all') {
            $surveyList = implode(',',$params->survey_summary);
            $conditions[] = "survey.survey_id IN ($surveyList)";
        }

        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $conditions []= 'survey.archive_date IS NULL';
        }

        $conditions[] = "survey.progress = 100";
        $tables  = "FROM axs_survey_progress AS survey ";
        $tables .= " JOIN joom_users as user ON user.id = survey.user_id ";
        $tables .= " JOIN axs_surveys as surveyRow ON survey.survey_id = surveyRow.id ";

        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        $order = " ORDER BY survey.date_completed DESC";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $items = array_values($items);
        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEducationTeam_summaryData($layout, $params, $report, $settings) {
        $db = JFactory::getDbo();
        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $showCourseTimespanDuration = in_array('course_duration',$settings);
        $showDurationInCourse       = in_array('course_duration_time',$settings);

        if($showCourseTimespanDuration && $params->splms_course_id) {
            if(!in_array('date_course_started',$settings)) {
                $settings[] = 'date_course_started';
            }
            if(!in_array('date_course_completed',$settings)) {
                $settings[] = 'date_course_completed';
            }
        }

        $quiz_score = false;
        $checklistUserList = false;
        $assignment = false;

        $teams = $params->teams;
        $checklist_id = $params->team_checklist;

        if(is_null($teams) || (!is_countable($teams) && count($teams) == 0)) {
            return;
        }

        if($params->team_lesson_activity) {
            $team_lesson_activity = explode(':',$params->team_lesson_activity);
            $team_lesson   = $team_lesson_activity[0];
            $team_activity = $team_lesson_activity[1];
        }

        if(in_array('quiz_score',$settings) && $params->team_quiz) {
            $quiz_score = true;
        }

        if(in_array('course_assignment_progress',$settings) && $params->team_course_assignment) {
            $assignment = true;
            $assignmentObj = AxsLMS::getAssignmentById($params->team_course_assignment);
        }


        if($checklist_id) {
            $checklist = AxsChecklist::getChecklist($checklist_id);
            if($checklist) {
                $checklistUserList = array();
                $checklistUsers = AxsChecklist::getChecklistUsers($checklist);
                foreach($checklistUsers as $checklistUser) {
                    array_push($checklistUserList,$checklistUser);
                }
            }
        }

        $allUsers = array();
        $users    = array();
        $tld      = count($teams) == 1;

        // Get the team object associated with the selected teams and create the user data for them
        foreach($teams as $team) {
            $teamRow = AxsTeams::getTeamById($team);
            $teamObj = new AxsTeamLeadDashboard(null,$teamRow,null);
            $teamLeaders = $teamObj->getTeamLeaders(true);
            $teamUserList = $teamObj->getUserList();

            $users[$team]['leaders'] = $teamLeaders;

            if(!is_null($teamUserList)) {

                $users[$team]['users'] = explode(',',$teamUserList);
            } else {

                $users[$team]['users'] = array();
            }

            $users[$team]['teamObj'] =  $teamObj;
            $allUsers = array_merge($allUsers,$users[$team]['users']);
        }

        $allUsers = array_unique($allUsers);
        $userList = implode(',',array_map('intval', $allUsers));

        if($params->team_survey) {
            $survey = (int)$params->team_survey;
            $surveyCondition = " AND survey.survey_id = ".$db->quote($survey)." AND survey.progress = 100";
        } else {
            $surveyCondition = '';
        }

        $surveyCondition .= " AND survey.archive_date IS NULL";

        if($params->splms_course_id) {
            $course_id = (int)$params->splms_course_id;
            $course = AxsLMS::getCourseById($course_id);
            $courseCategory = AxsLMS::getCourseCategoryById($course->splms_coursescategory_id);
        }

        // @todo Optimize this report data query - currently it stands at ~13s execution time
        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            $column_template = "";
            $notFound = false;


            switch ($setting) {

                case "team_name":
                    if(!$teams) {
                        $notFound = true;
                    } else {
                        $column_title = "Team";
                        $fields .= "'' AS team_name,";
                        $filter = new stdClass();
                        $operators = new stdClass();
                        $string = new stdClass();
                        $string->contains = "Contains";
                        $operators->string = $string;
                        $filter->operators = $operators;
                        $column_filterable = $filter;
                    }
                    break;
                case "team_leads":
                    if(!$teams) {
                        $notFound = true;
                    } else {
                        $column_title = "Team Leads";
                        $fields .= "'' AS team_leads,";
                        $filter = new stdClass();
                        $operators = new stdClass();
                        $string = new stdClass();
                        $string->contains = "Contains";
                        $operators->string = $string;
                        $filter->operators = $operators;
                        $column_filterable = $filter;
                    }
                    break;
                case "checklist_progress":
                    if(!$params->team_checklist) {
                        $notFound = true;
                    } else {
                        $column_title = "Checklist Progress";
                        $column_type = "number";
                        $column_format = "{0:n0}%";
                        $fields .= "0 AS checklist_progress,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #%";
                    }
                    break;
                case "quiz_score":
                    if(!$params->team_quiz) {
                        $notFound = true;
                    } else {
                        $column_title = "Quiz Score";
                        $column_type = "number";
                        $column_format = "{0:n0}";
                        $fields .= "'' AS quiz_score,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #%";
                    }
                    break;
                case "lesson_activity":
                    if(!$team_activity) {
                        $notFound = true;
                    } else {
                        $column_title = "Activity Status";
                        $fields .= "(SELECT IF(completed=1,'Completed','Not Completed') AS status FROM #__splms_student_activities AS activity WHERE activity.course_id = ".$db->quote($course_id)." AND activity.lesson_id = ".$db->quote($team_lesson)." AND activity.activity_id = ".$db->quote($team_activity)." AND activity.user_id = user.id AND archive_date IS NULL LIMIT 1) AS lesson_activity,";
                        $filter = new stdClass();
                        $operators = new stdClass();
                        $string = new stdClass();
                        $string->contains = "Contains";
                        $operators->string = $string;
                        $filter->operators = $operators;
                        $column_filterable = $filter;
                    }
                    break;
                case "course_name":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $column_title = "Course";
                        $fields .= $db->quote($course->title)." AS course_name,";
                        $filter = new stdClass();
                        $operators = new stdClass();
                        $string = new stdClass();
                        $string->contains = "Contains";
                        $operators->string = $string;
                        $filter->operators = $operators;
                        $column_filterable = $filter;
                    }
                    break;
                case "course_category":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $courseCategoryName = !empty($courseCategory) ? $courseCategory->title : '';
                        $column_title = "Course Category";
                        $fields .= $db->quote($courseCategoryName)." AS course_category,";
                        $filter = new stdClass();
                        $operators = new stdClass();
                        $string = new stdClass();
                        $string->contains = "Contains";
                        $operators->string = $string;
                        $filter->operators = $operators;
                        $column_filterable = $filter;
                    }
                    break;
                case "course_score":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $column_title = "Score";
                        $column_type = "number";
                        $column_format = "{0:n0}";
                        $fields .= "(SELECT COALESCE(MAX(progress.score),'') FROM joom_splms_course_progress AS progress WHERE progress.course_id = ".$db->quote($course_id)." AND progress.user_id = user.id AND archive_date IS NULL) AS course_score,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #%";
                    }
                    break;
                case "course_progress":
                    $column_title = "Progress";
                    $column_type = "number";
                    $column_format = "{0:n0}%";
                    $fields .= "(SELECT COALESCE(MAX(progress.progress),0) FROM joom_splms_course_progress AS progress WHERE progress.course_id = ".$db->quote($course_id)." AND progress.user_id = user.id AND archive_date IS NULL) AS course_progress,";
                    break;
                case "date_course_started":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $filter = new stdClass();
                        $filter->ui = "datepicker";
                        $column_title = "Course Start Date";
                        $column_width = 200;
                        $column_type = "date";
                        $column_format = "{0:MM/d/yyyy}";
                        $column_filterable = $filter;
                        $fields .= "(SELECT MAX(progress.date_started) FROM joom_splms_course_progress AS progress WHERE progress.course_id = ".$db->quote($course_id)." AND progress.user_id = user.id AND archive_date IS NULL) AS $setting,";
                    }
                    break;
                case "date_course_completed":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $filter = new stdClass();
                        $filter->ui = "datepicker";
                        $column_title = "Course Completed Date";
                        $column_width = 200;
                        $column_type = "date";
                        $column_format = "{0:MM/d/yyyy}";
                        $column_filterable = $filter;
                        $fields .= "(SELECT MAX(progress.date_completed) FROM joom_splms_course_progress AS progress WHERE progress.course_id = ".$db->quote($course_id)." AND progress.user_id = user.id AND archive_date IS NULL) AS $setting,";
                    }
                    break;
                case "course_duration":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $column_title = "Course Completion Timespan";
                        $fields .= "'' AS course_duration,";
                    }
                    break;
                case "course_duration_time":
                    if(!$course_id) {
                        $notFound = true;
                    } else {
                        $column_title = "Duration in Course";
                        $fields .= "(SELECT duration.duration FROM axs_duration AS duration WHERE duration.item_id = ".$db->quote($course_id)." AND duration.type = 'course' AND duration.user_id = user.id LIMIT 1) AS $setting,";
                    }
                    break;
                case "course_assignment_progress":
                    if(!$params->team_course_assignment) {
                        $notFound = true;
                    } else {
                        $column_title = "Assigned Courses Progress";
                        $column_type = "number";
                        $column_format = "{0:n0}%";
                        $fields .= "0 AS course_assignment_progress,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #%";
                    }
                    break;

                //User Info
                case "survey_name":
                    if(!$params->team_survey) {
                        $notFound = true;
                    } else {
                        $column_title = "Survey";
                        $fields .= "surveyRow.title AS survey_name,";
                    }
                    break;

                case "results_button":
                    if(!$params->team_survey) {
                        $notFound = true;
                    } else {
                        $column_title = "View Results";
                        $fields .= "survey.user_id AS surveyUser,survey.survey_id AS surveyId,";
                        $column_template = '<span class="k-button k-button-icontext getSubmission" data-id="#: surveyId #" data-download="#: surveyId #" data-user="#: surveyUser #" data-type="survey"><i class="fa fa-external-link"></i> View Results</span>';
                    }
                    break;

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;
                case "nps":
                    if(!$params->team_survey) {
                        $notFound = true;
                    } else {
                        $column_title = "NPS";
                        $column_type = "number";
                        $fields .= "survey.nps AS nps,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    }
                    break;
                case "csat":
                    if(!$params->team_survey) {
                        $notFound = true;
                    } else {
                        $column_title = "CSAT Score";
                        $column_type = "number";
                        $fields .= "survey.csat AS csat,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    }
                    break;
                case "csat_average":
                    if(!$params->team_survey) {
                        $notFound = true;
                    } else {
                        $column_title = "CSAT Average";
                        $column_type = "number";
                        $fields .= "survey.average_score AS csat_average,";
                        $column_aggregates = array("average");
                        $column_footerTemplate = "Average: #= kendo.toString(average, 'n2') #";
                    }
                    break;
                default:
                    $notFound = true;
                break;
            }


            if($notFound) {
                continue;
            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if($column_template) {
                $column->template = $column_template;
            }

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        if(!in_array('user_id',$settings)) {
            $fields .= "user.id AS user_id,";
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $conditions[] = "user.id IN ($userList)";

        $tables  = " FROM joom_users as user";
        if($params->team_survey) {
            $tables .= " LEFT OUTER JOIN axs_survey_progress AS survey ON user.id = survey.user_id".$surveyCondition;
            $tables .= " LEFT OUTER JOIN axs_surveys as surveyRow ON survey.survey_id = surveyRow.id ";
        }
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        $query = "
            SELECT
            $fields
            $tables
            $where
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        $courseIds = trim($assignmentObj->course_ids, ',');

        foreach($items as &$item) {

            $userTeamList = array();
            $userTeamLeaderList = array();

            foreach($teams as $team) {
                if (in_array($item->user_id, $users[$team]['users'])) {
                    array_push($userTeamList, $users[$team]['teamObj']->getTeam()->title);
                    array_push($userTeamLeaderList, $users[$team]['leaders']);
                }
            }

            // Save an implode if we're only using one team (TLD)
            if($tld) {
                $item->team_name = $userTeamList[0];
            } else {
                $item->team_name = implode(',',$userTeamList);
            }

            $item->team_leads = implode(',',$userTeamLeaderList);

            if($checklistUserList && in_array($item->user_id,$checklistUserList)) {
                $item->checklist_progress = AxsChecklist::getUserChecklistProgress($item->user_id,$checklist);
            }

            if($quiz_score){
                $quizResult = AxsLMS::getAllUserQuizzes(null,null,$item->user_id,(int)$params->team_quiz);
                if($quizResult) {
                    $item->quiz_score = $quizResult->highscore;
                } else {
                    $item->quiz_score = '';
                }
            }

            if($assignment && $courseIds) {

                $assignmentProgress = AxsLMS::getAssignmentProgress($courseIds, $item->user_id);

                if($assignmentProgress) {
                    $item->course_assignment_progress = $assignmentProgress->percentageComplete;
                }
            }

            if($item->date_course_completed && $showCourseTimespanDuration) {
                $courseStart     = date_create($item->date_course_started);
                $courseCompleted = date_create($item->date_course_completed);
                $difference      = date_diff($courseStart, $courseCompleted);
                $duration = '';
                if($difference->days) {
                    $duration .= $difference->days."d ";
                }
                if($difference->h) {
                    $duration .= $difference->h."h ";
                }
                if($difference->i) {
                    $duration .= $difference->i."m ";
                }
                if($difference->s) {
                    $duration .= $difference->s."s ";
                }
                $item->course_duration = $duration;
            }

            if($item->course_duration_time && $showDurationInCourse) {
                $item->course_duration_time = gmdate('H:i:s', $item->course_duration_time);
            }
        }

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = $this->buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = $this->buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEducationSurveyData($layout, $params, $report, $settings) {
        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $start_date = $params->education_start_date;
        $end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    //$column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    //$column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    //$column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;
                case "question":
                    $column_title = "Question";
                    $fields .= "survey.question AS question,";
                    break;
                case "answer":
                    $column_title = "Answer";
                    $fields .= "survey.answer AS answer,";
                    break;
                case "submitted_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Submitted";
                    //$column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.submitted_date AS submitted_date,";
                    break;
                case "point_value":
                    $column_title = "Point Value";
                    //$column_width = 150;
                    $column_type = "number";
                    $fields .= "survey.score AS point_value,";
                    break;
                case "archive_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Archive Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "survey.archive_date AS $setting,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;


            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        if ($start_date) {
            if ($end_date) {
                $conditions[] = "( DATE(survey.submitted_date) >= DATE('$start_date') AND DATE(survey.submitted_date) <= DATE('$end_date') )";
            } else {
                $conditions[] = "( DATE(survey.submitted_date) >= DATE('$start_date') )";
            }
        } else {
            if ($end_date) {
                $conditions[] = "( DATE(survey.submitted_date) <= DATE('$end_date') )";
            }
        }

        $conditions[] = "survey.survey_id = ".$db->quote($params->survey);

        $tables = "FROM axs_survey_responses AS survey ";
        $tables .= " JOIN joom_users as user ON user.id = survey.user_id ";

        //$conditions[] = "user.id IN ($userList)";
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $conditions []= 'survey.archive_date IS NULL';
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        $order = " ORDER BY survey.user_id";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $items = array_values($items);
        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEducationPoint_totalsData($layout, $params, $report, $settings) {
        $db = JFactory::getDbo();
        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $start_date = $params->education_start_date;
        $end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            switch ($setting) {
                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    //$column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    //$column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;

                case "points":
                    $column_title = "Points";
                    $column_type = "number";
                    $fields .= "points.points AS points,";
                    $column_aggregates = array("sum");
                    $column_footerTemplate = "Total: #= kendo.toString(sum, 'n2') #";
                    break;

                case "category":
                    $column_title = "Category";
                    $fields .= "category.title AS category,";
                    break;

                case "date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Earned";
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "points.date AS date,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;


            if ($column_aggregates) {
                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        if ($start_date) {
            if ($end_date) {
                $conditions[] = "( DATE(points.date) >= DATE('$start_date') AND DATE(points.date) <= DATE('$end_date') )";
            } else {
                $conditions[] = "( DATE(points.date) >= DATE('$start_date') )";
            }
        } else {
            if ($end_date) {
                $conditions[] = "( DATE(points.date) <= DATE('$end_date') )";
            }
        }


        $tables = "FROM axs_points AS points ";
        $tables .= " JOIN  joom_users as user ON user.id = points.user_id ";
        $tables .= " JOIN  axs_points_categories as category ON points.category_id = category.id ";

        //$conditions[] = "user.id IN ($userList)";
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        $order = " ORDER BY points.date DESC";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";
        $db->setQuery($query);
        $items = $db->loadObjectList();
        $items = array_values($items);
        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEducationQuizData($layout, $params, $report, $settings) {

        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $showAnswers = in_array('answer',$settings);
        $showCorrect = in_array('correct',$settings);
        $showQuestion = in_array('question',$settings);
        $include_quiz_results_table = false;
        $include_lesson_status_table = false;
        $include_course_purchases_table = false;
        $include_courses_table = false;
        $include_lessons_table = false;

        //$start_date = $params->education_start_date;
        //$end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";

            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;

                case "question":
                    $column_title = "Question";
                    break;
                case "answer":
                    $column_title = "Answer";
                    break;
                case "correct":
                    $column_title = "Correct";
                    break;
                case "date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "quiz.date  AS date,";
                    break;
                case "archive_date":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Archive Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "quiz.archive_date AS $setting,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;


            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        /*if ($start_date) {
            if ($end_date) {
                $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
            } else {
                $where = " WHERE per.start >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE per.end <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }    */

        //$items = array();
        $query = "SELECT * FROM joom_splms_quizresults WHERE splms_quizquestion_id = ".$db->quote($params->quiz);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $allUserIds = array();
        foreach($results as $result) {
            if($result->user_id) {
                $allUserIds[] = (int)$result->user_id;
            }
        }

        $userList = implode(',',$allUserIds);
        if(!$userList) {
            return false;
        }

        $tables = "FROM joom_users as user";
        $tables .= " JOIN joom_splms_quizresults AS quiz ON  user.id = quiz.user_id AND quiz.splms_quizquestion_id = ".$db->quote($params->quiz);
        $conditions[] = "user.id IN ($userList)";
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $conditions []= 'quiz.archive_date IS NULL';
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        $query = "
            SELECT
             quiz.answers as temp_answers,
            $fields
            $tables
            $where
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $itemsCount = count($items);
        for($i = 0; $i < $itemsCount; $i++) {
            $answers = '';
            $answers = json_decode($items[$i]->temp_answers);
            foreach($answers as $ans) {
                $answer = json_decode($ans);
                $newItem = clone $items[$i];
                $newItem->question = $answer->question;
                $newItem->answer   = $answer->answer;
                if($answer->correct) {
                    $newItem->correct  = 'Yes';
                } else {
                    $newItem->correct  = 'No';
                }
                unset($newItem->answers);
                unset($newItem->temp_answers);
                $items[] = $newItem;
            }
            unset($items[$i]);
            unset($items[$i]->temp_answers);
        }

        $items = array_values($items);
        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;

    }

    public function getEducationCertificatesData($layout, $params, $report, $settings) {

        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        //$start_date = $params->education_start_date;
        //$end_date = $params->education_end_date;

        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
/* <option value="user_id">User ID</option>
			<option value="email">User Email</option>
			<option value="groups">User Groups</option>
			<option value="award">Award (badge/cert/milestone)</option>
			<option value="award_type">Award Type</option>
			<option value="date_earned">Date Issued</option>
			<option value="date_expires">Date Expires</option> */
            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;
                case "award":
                    $column_title = "Award";
                    $fields .= "award.title AS award,";
                    break;
                case "award_type":
                    $column_title = "Award Type";
                    $fields .= "award.type AS award_type,";
                    break;
                case "date_earned":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Issued";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "award_earned.date_earned AS date_earned,";
                    break;
                case "date_expires":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";
                    $column_title = "Date Expires";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "award_earned.date_expires AS date_expires,";
                    break;
            }



            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }



            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;


            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        /*if ($start_date) {
            if ($end_date) {
                $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
            } else {
                $where = " WHERE per.start >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE per.end <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }    */

        //$items = array();
        $award_type = $params->award_type;
        if($params->{$award_type}[0] == 'all' || !$params->{$award_type} || $award_type == 'all') {
            $awardList = '';
            $awardListJoined = '';
        } else {
            $awardIds = implode(',', $db->quote($params->{$award_type}));
            $awardList = ' badge_id IN ('.$awardIds.')';
            $awardListJoined = ' AND award_earned.badge_id IN ('.$awardIds.')';
        }

        $tables = "FROM axs_awards_earned AS award_earned ";
        $tables .= " JOIN  joom_users as user ON user.id = award_earned.user_id ";
        $tables .= " LEFT JOIN axs_awards AS award ON award.id = award_earned.badge_id";
        if($awardListJoined && $awardIds) {
            $conditions[] = 'award_earned.badge_id IN ('.$awardIds.')';
        }
        if($award_type != 'all') {
            $conditions[] = 'award.type = '.$db->quote($award_type);
        }
        //$conditions[] = "user.id IN ($userList)";
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        //$order = " GROUP BY progress.user_id, progress.course_id";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $items = array_values($items);
        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;

    }

    public function getEducationAverageData($layout, $params, $report, $settings) {

        $db = JFactory::getDbo();

        $fields = "";
        $where = "";
        $columns = new stdClass();
        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        //$start_date = $params->education_start_date;
        //$end_date = $params->education_end_date;

        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";

            switch ($setting) {

                case "question":
                    $column_title = "Question";
                    $fields .= "quiz.point AS question,";
                    break;
                case "correct":
                    $column_title = "Correct";
                    $column_type = "number";
                    $fields .= "quiz.point AS correct,";
                    break;
                case "Incorrect":
                    $column_title = "Wrong";
                    $column_type = "number";
                    $fields .= "quiz.point AS incorrect,";
                    break;
                case "total":
                    $column_title = "Total Attempts";
                    $column_type = "number";
                    $fields .= "quiz.point AS total,";
                    break;
                case "average":
                    $column_title = "Average Correct";
                    $column_type = "number";
                    $column_format = "{0:n0}%";
                    $fields .= "quiz.point AS average,";
                    break;
            }


            if(!$column_width) {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = $column_format;
            $column->type = $column_type;
            $column->filterable = $column_filterable;
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;


            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }


        /*if ($start_date) {
            if ($end_date) {
                $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
            } else {
                $where = " WHERE per.start >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE per.end <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }    */

        //$items = array();
        $query = "SELECT * FROM joom_splms_quizresults WHERE splms_quizquestion_id = ".$db->quote($params->quiz_average);
        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $query .= ' AND archive_date IS NULL';
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $allUserIds = array();
        foreach($results as $result) {
            if($result->user_id) {
                $allUserIds[] = (int)$result->user_id;
            }
        }

        $userList = implode(',',$allUserIds);
        if(!$userList) {
            return false;
        }

        $tables = "FROM joom_splms_quizresults AS quiz JOIN joom_users as user ON quiz.user_id = user.id ";
        $conditions[] = " user.id IN ($userList) AND quiz.splms_quizquestion_id = ".$db->quote($params->quiz_average);
        if($params->use_group_filter && $params->usergroup_filter) {
            $groupFilter = implode(',',$params->usergroup_filter);
            $conditions[] = "EXISTS(SELECT * FROM joom_user_usergroup_map as groups WHERE user.id = groups.user_id AND groups.group_id IN ($groupFilter))";
        }

        if ($params->include_archived_activity == null || $params->include_archived_activity == "0") {
            $conditions []= 'archive_date IS NULL';
        }

        $limit = "";
        if($conditions) {
            $where = ' WHERE ' . implode(' AND ',$conditions);
        }

        //$order = " GROUP BY progress.user_id, progress.course_id";

        $query = "
            SELECT
            *
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();
        $itemsCount = count($items);
        $questions = array();
        $questionsSet = array();
        for($i = 0; $i < $itemsCount; $i++) {
            $answers = '';
            $answers = json_decode($items[$i]->answers);
            $a = 0;
            foreach($answers as $ans) {
                $answer = json_decode($ans);
                if(!in_array($answer->question, $questionsSet)) {
                    $questionsSet[] = $answer->question;
                    $totals = new stdClass();
                    $totals->question = $answer->question;
                    $totals->correct = 0;
                    $totals->incorrect = 0;
                    $totals->total = 0;
                    $totals->average = 0;
                    $questions[$a] = $totals;
                }
                $questions[$a]->total++;
                if($answer->correct) {
                    $questions[$a]->correct++;
                } else {
                    $questions[$a]->incorrect++;
                }
                $questions[$a]->average = round( ($questions[$a]->correct / $questions[$a]->total) * 100 );
                $a++;
            }
        }

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = ReportsModelReports::buildGridData($questions, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = ReportsModelReports::buildSpreadsheetData($questions, $columns, $report);
        }

        return $result;

    }

    public function getUsersData($layout, $params, $report) {

        $db = JFactory::getDbo();

        $fields = "";
        $columns = new stdClass();

        $aggregates = null;

        $settings = $params->users_settings;

        if (count($settings) == 0) {
        	return null;
        }

        $include_subscriptions = false;
        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
        	$column = new stdClass();
        	$field_id = "";
            $column_width = "";

        	switch ($setting) {
        		case "id":
        			$column_title = "ID";
        			$fields .= "user." . $setting . ",";
        			break;
        		case "name":
        			$column_title = "Name";
        			$fields .= "user." . $setting . ",";
        			break;
        		case "username":
        			$column_title = "User Name";
        			$column_width = 200;
        			$fields .= "user." . $setting . ",";
        			break;
        		case "email":
        			$column_title = "Email";
        			$column_width = 200;
        			$fields .= "user." . $setting . ",";
        			break;
        		case "registerDate":
        			$column_title = "Register Date";
        			$fields .= "user." . $setting . ",";
        			break;
        		case "lastvisitDate":
        			$column_title = "Last Visit Date";
        			$fields .= "user." . $setting . ",";
        			break;

        		case "city":
        			$column_title = "City";
        			if ($field_id == "") { $field_id = 10; }
        			break;
        		case "state":
        			$column_title = "State";
        			if ($field_id == "") { $field_id = 9; }
        			break;
                case "address":
                    $column_title = "Address";
                    if ($field_id == "") { $field_id = 8; }
                    break;
        		case "zip":
        			$column_title = "Zip";
        			if ($field_id == "") { $field_id = 23; }
        			break;
        		case "country":
        			$column_title = "Country";
        			if ($field_id == "") { $field_id = 11; }
        			break;

        		case "groups":
        			$column_title = "Groups";
        			$column_width = 200;
        			$fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
        			break;

        		case "active":
        			$column_title = "Active";
        			$include_subscriptions = true;
        			$fields .= "CASE WHEN sub.status = 'ACT' THEN 'Active' WHEN sub.status = 'INAC' THEN 'Inactive' WHEN sub.status IS NULL THEN 'Inactive' WHEN sub.status = 'GRC' then 'Grace Period' END AS $setting,";
        			break;

        		case "rewards":
        			$column_title = "Rewards Points";
        			$fields .= "(SELECT points FROM axs_rewards_data WHERE user_id = user.id) AS $setting,";
        			break;
        		case "memberType":
        			$column_title = "Membership Type";
        			$include_subscriptions = true;
        			//$fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM axs_pay_subscription_plans AS f JOIN axs_pay_subscriptions AS g WHERE f.id = g.plan_id AND g.user_id = a.id) AS $setting,";
        			$fields .= "(SELECT title FROM axs_pay_subscription_plans WHERE id = sub.plan_id) AS $setting,";
        			break;
        	}

        	if ($field_id != "") {
        		$fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
        	}

            if ($column_width == "") {
                $column_width = 100;
            }

        	$column = new stdClass;
	        $column->field = $setting;
	        $column->title = $column_title;
	        $column->format = "";
	        $column->filter = "";
	        $column->width = $column_width;

        	$columns->$setting = $column;
        }



        //Take off the last comma on $joom_fields
        if ($fields != "") {
        	$fields = rtrim($fields, ",");
        }

        $tables = "FROM joom_users AS user";
        if ($include_subscriptions) {
        	$tables .= " LEFT JOIN axs_pay_subscriptions AS sub ON user.id = sub.user_id";
        }

        $limit = "";

        $query = "
			SELECT
			$fields
			$tables
			ORDER BY user.id ASC
			$limit
		";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
        	$result = self::buildGridData($items, $columns, $report, $aggregates);

        }

        if ($layout == 'spreadsheet') {
        	$result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getNotificationData($layout, $params, $report) {
        $db = JFactory::getDbo();

        $fields = "";
        $columns = new stdClass();

        $aggregates = null;
        $settings = $params->notifications_settings;

        if (count($settings) == 0) {
            return null;
        }

        $include_subscriptions = false;
        self::getCustomFieldSQL($fields, $columns, $params, $report);
        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_type = "";
            $column_format = "";
            $column_filterable = "";
            $column_schema = "";

            switch ($setting) {
                case "id":
                    $column_title = "ID";
                    $fields .= "user." . $setting . ",";
                    break;
                case "name":
                    $column_title = "Name";
                    $fields .= "user." . $setting . ",";
                    break;
                case "title":
                    $column_title = "Notification";
                    $column_width = 200;
                    $fields .= "notifications." . $setting . ",";
                    break;
                case "action":
                    $column_title = "Action";
                    $column_width = 200;
                    $fields .= "actions." . $setting . ",";
                    break;
                case "date_created":
                    $filter = new stdClass();
                    $filter->ui = "datepicker";

                    $column_title = "Date";
                    $column_width = 200;
                    $column_type = "date";
                    $column_format = "{0:MM/d/yyyy}";
                    $column_filterable = $filter;
                    $fields .= "(DATE_FORMAT(actions." . $setting . ",'%m/%d/%Y')) as date_created,";
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    break;
                case "registerDate":
                    $column_title = "Register Date";
                    $fields .= "user." . $setting . ",";
                    break;
                case "lastvisitDate":
                    $column_title = "Last Visit Date";
                    $fields .= "user." . $setting . ",";
                    break;

                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "address":
                    $column_title = "Address";
                    if ($field_id == "") { $field_id = 8; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;

                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;

                case "active":
                    $column_title = "Active";
                    $include_subscriptions = true;
                    $fields .= "CASE WHEN sub.status = 'ACT' THEN 'Active' WHEN sub.status = 'INAC' THEN 'Inactive' WHEN sub.status IS NULL THEN 'Inactive' WHEN sub.status = 'GRC' then 'Grace Period' END AS $setting,";
                    break;

                case "rewards":
                    $column_title = "Rewards Points";
                    $fields .= "(SELECT points FROM axs_rewards_data WHERE user_id = user.id) AS $setting,";
                    break;
                case "memberType":
                    $column_title = "Membership Type";
                    $include_subscriptions = true;
                    //$fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM axs_pay_subscription_plans AS f JOIN axs_pay_subscriptions AS g WHERE f.id = g.plan_id AND g.user_id = a.id) AS $setting,";
                    $fields .= "(SELECT title FROM axs_pay_subscription_plans WHERE id = sub.plan_id) AS $setting,";
                    break;
            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }

            if ($column_width == "") {
                $column_width = 100;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->type = $column_type;
            $column->format = $column_format;
            $column->filterable = $column_filterable;
            $column->width = $column_width;

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }


        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $tables  = " FROM axs_notifications_tracking AS actions";
        $tables .= " LEFT JOIN axs_notifications AS notifications ON notifications.id = actions.notification_id";
        $tables .= " LEFT JOIN joom_users AS user ON user.id = actions.user_id";

        if ($include_subscriptions) {
            $tables .= " LEFT JOIN axs_pay_subscriptions AS sub ON user.id = sub.user_id";
        }


        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            ORDER BY actions.id DESC
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates);

        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getTransactionsData($layout, $params, $report) {
        $db = JFactory::getDbo();

        $fields = "";
        $columns = new stdClass();

        $aggregates = [];
        $schema = [];

        $settings = $params->transactions_settings;

        if (count($settings) == 0) {
            return null;
        }

        $start_date = $params->transactions_start_date;
        $end_date = $params->transactions_end_date;

        $include_subscriptions = false;
        $include_user_table = false;

        //Create an array of all the transaction statuses.
        $statuses = array();
        foreach (Transaction::$statuses as $key => $value) {
            $statuses[$key] = Transaction::getStatusName($key);
        }

        $initiators = array();
        foreach (AxsPayment::$initiators as $key => $value) {
            $initiators[$value] = ucwords($key);
        }

        $trxn_types = array();
        foreach (AxsPayment::$trxn_types as $key => $value) {

            //This converts the status keys to proper titles
            //ie. "failed_signup" becomes "Failed Signup"
            $key = ucwords(str_replace("_", " ", $key));

            $trxn_types[$value] = $key;
        }
        self::getCustomFieldSQL($fields, $columns, $params, $report);

        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";

            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "User Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "User City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "User State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "address":
                    $column_title = "User Address";
                    if ($field_id == "") { $field_id = 8; }
                    break;
                case "zip":
                    $column_title = "User Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "User Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS group_map JOIN `joom_usergroups` AS groups ON (group_map.group_id = groups.id) WHERE group_map.user_id = user.id) AS $setting,";
                    $include_user_table = true;
                    break;

                //Transaction Info

                case "trxn_id":
                    $column_title = "Transaction ID";
                    $column_width = "200";
                    $fields .= "trans." . $setting . ",";
                    break;
                case "refund_id":
                    $column_title = "Refund ID";
                    $fields .= "trans." . $setting . ",";
                    break;
                case "date":
                    $column_title = "Transaction Date";
                    $fields .= "trans." . $setting . ",";

                    break;
                case "type":
                    $column_title = "Transaction Type";
                    $column_width = "";

                    $fields .= "CASE ";

                    foreach ($trxn_types as $key => $value) {
                        $fields .= " WHEN trans.type = '$key' THEN '$value' ";
                    }

                    $fields .= "END AS type,";

                    break;
                case "item":
                    $column_title = "Transaction Item";
                    // Get the AxsPayment::trxn_types code for this transaction type
                    $fields .= "trans.type AS type,";
                    // The type_id will be used later when we get the name of the transaction item
                    // The type_id corresponds to the table row the transaction is for
                    $fields .= "trans.type_id AS type_id,";
                    break;
                case "amount":
                    $column_title = "Transaction Amount";
                    //$fields .= "CONCAT('$', trans." . $setting . ") AS amount,";
                    $fields .= "trans." . $setting . ",";
                    $column_schema = "number";
                    $column_aggregates = array("min", "max", "average", "sum");
                    $column_footerTemplate = "<div>Total: #= sum #</div><div>Min: #= min #</div><div>Max: #= max #</div><div>Average: #= average #</div>";
                    break;
                case "status":
                    $column_title = "Transaction Status";
                    //The two tables use different types for strings, so converting them to BINARY makes them equal

                    $fields .= "CASE ";

                    foreach ($statuses as $key => $value) {
                        $fields .= " WHEN trans.status = '$key' THEN '$value' ";
                    }

                    $fields .= "END AS status,";

                    break;
                case "initiator":
                    $column_title = "Transaction Initiator";

                    $fields .= "CASE ";

                    foreach ($initiators as $key => $value) {
                        $fields .= " WHEN trans.initiator = '$key' THEN '$value' ";
                    }

                    $fields .= "END AS initiator,";

            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }

            if ($column_width == "") {
                $column_width = 125;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = "";
            $column->filter = "";
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }


        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $tables = "FROM axs_pay_transactions as trans";

        if ($include_user_table) {
            $tables .= " JOIN joom_users AS user ON trans.user_id = user.id";
        }

        if ($include_subscriptions) {
            $tables .= " JOIN axs_pay_subscriptions AS sub ON user.id = sub.user_id";
        }

        /*if ($start_date || $end_date) {
            $where .= " WHERE trans.date BETWEEN $start_date AND "
        }*/

        if ($start_date) {
            if ($end_date) {
                $where = " WHERE DATE(trans.date) >= DATE('$start_date') AND DATE(trans.date) <= DATE('$end_date')";
            } else {
                $where = " WHERE DATE(trans.date) >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE DATE(trans.date) <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }

        $limit = "LIMIT 50";
        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            $where
            ORDER BY trans.id ASC
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        // If we are getting the transaction item
        if (in_array('item', $settings)) {
            // Cycle through the transactions and get the item name depending on the transaction type
            foreach($items as $t) {
                //we need to get name, email, phone, and item desc.
                if($t->type == AxsPayment::$trxn_types['event']) {
                    $query = $db->getQuery(true);

                    $query
                        ->select('title')
                        ->from('joom_eb_events')
                        ->where('id ='.(int)$t->type_id);

                    $db->setQuery($query);
                    $cp = $db->loadObject();
                    $t->item = $cp->title;
                }

                if($t->type == AxsPayment::$trxn_types['e-commerce']) {
                    $query = $db->getQuery(true);

                    $query
                        ->select('product_name,quantity')
                        ->from('joom_eshop_orderproducts')
                        ->where('order_id ='.(int)$t->type_id);

                    $db->setQuery($query);
                    $cp = $db->loadObjectList();
                    $product = [];
                    foreach($cp as $item) {
                        array_push($product, $item->product_name.': '.$item->quantity);
                    }
                    $title = implode('</br>', $product);
                    $t->item = $title;
                }

                if($t->type == AxsPayment::$trxn_types['course']) {
                    $query = $db->getQuery(true);

                    $query
                        ->select('title')
                        ->from('axs_course_purchases AS purchase')
                        ->join('INNER', 'joom_splms_courses AS course ON purchase.course_id = course.splms_course_id')
                        ->where('purchase.id ='.(int)$t->type_id);

                    $db->setQuery($query);
                    $cp = $db->loadObject();
                    $t->item = $cp->title;
                }

                if($t->type == AxsPayment::$trxn_types['subscription']) {
                    $query = $db->getQuery(true);
                    $query
                        ->select('plan.title')
                        ->from('axs_pay_subscription_periods AS period')
                        ->join('INNER', 'axs_pay_subscriptions AS sub ON period.sub_id = sub.id')
                        ->join('INNER', 'axs_pay_subscription_plans AS plan ON sub.plan_id = plan.id')
                        ->where('period.id ='.(int)$t->type_id);
                    $db->setQuery($query);
                    $cp = $db->loadObject();
                    $t->item = $cp->title;
                }
            }
        }

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getSubscriptionsData($layout, $params, $report) {

        $db = JFactory::getDbo();

        $fields = "";
        $columns = new stdClass();

        $aggregates = [];
        $schema = [];

        $settings = $params->subscriptions_settings;

        if (count($settings) == 0) {
            return null;
        }

        $start_date = $params->subscriptions_start_date;
        $end_date = $params->subscriptions_end_date;

        $include_subscription_plans = false;
        $include_subscription_periods = false;
        $include_user_table = false;

        //If "period" is set, swap it out for two columns, "period_start" and "period_end"
        for ($i = 0; $i < count($settings); $i++) {
            if ($settings[$i] == "period") {

                $settings[$i] = "period_start";
                array_push($settings, "period_end");
                break;
            }
        }

        self::getCustomFieldSQL($fields, $columns, $params, $report);

        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";

            switch ($setting) {

                //User Info

                case "user_id":
                    $column_title = "User ID";
                    $fields .= "user.id AS user_id,";
                    $include_user_table = true;
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user.name AS username,";
                    $include_user_table = true;
                    break;
                case "email":
                    $column_title = "User Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    $include_user_table = true;
                    break;
                case "city":
                    $column_title = "User City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "User State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "address":
                    $column_title = "User Address";
                    if ($field_id == "") { $field_id = 8; }
                    break;
                case "zip":
                    $column_title = "User Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "User Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    $include_user_table = true;
                    break;

                //subscription Info

                case "sub_id":
                    $column_title = "Subscription ID";
                    $fields .= "(SELECT sub.id) AS sub_id,";
                    break;
                case "cancel":
                    $column_title = "Cancellation Date";
                    $fields .= "sub." . $setting . ",";
                    break;
                case "original_amount":
                    $column_title = "Original Amount";
                    $fields .= "sub." . $setting . ",";
                    break;
                case "status":
                    $column_title = "Status";
                    $fields .= "sub.$setting,";
                    break;
                case "type":
                    $column_title = "Subscription Type";
                    $include_subscription_periods = true;
                    $fields .= "(SELECT per.status) AS type,";
                    break;
                case "period_length":
                    $column_title = "Period Length";
                    $fields .= "sub." . $setting . ",";
                    break;
                case "points":
                    $column_title = "Points Used";
                    $fields .= "sub." . $setting . ",";
                    break;
                case "plan_id":
                    $column_title = "Subscription Plan";
                    $include_subscription_plans = true;
                    $fields .= "plans.title AS plan_id,";
                    break;
                case "cat_id":
                    $column_title = "Subscription Category";
                    $include_subscription_categories = true;
                    $include_subscription_plans = true;
                    $fields .= "cat.title AS cat_id,";
                    break;
                case "period_start":
                    $column_title = "Subscription Date Period Start";
                    $include_subscription_periods = true;
                    $fields .= "per.start AS period_start,";
                    break;
                case "period_end":
                    $column_title = "Subscription Date Period END";
                    $include_subscription_periods = true;
                    $fields .= "per.end AS period_end,";
                    break;
            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }

            if ($column_width == "") {
                $column_width = 125;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = "";
            $column->filter = "";
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }


        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        if ($start_date || $end_date) {
            $include_subscription_periods = true;
        }

        if ($start_date) {
            if ($end_date) {
                $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
            } else {
                $where = " WHERE per.start >= DATE('$start_date')";
            }
        } else {
            if ($end_date) {
                $where = " WHERE per.end <= DATE('$end_date')";
            } else {
                $where = "";
            }
        }

        $tables = "FROM axs_pay_subscriptions AS sub";

        if ($include_user_table) {
            $tables .= " JOIN joom_users AS user ON sub.user_id = user.id";
        }

        if ($include_subscription_periods) {
            $tables .= " JOIN axs_pay_subscription_periods AS per ON sub.id = per.sub_id";
        }

        if ($include_subscription_plans) {
            $tables .= " JOIN axs_pay_subscription_plans AS plans ON sub.plan_id = plans.id";
        }

        if ($include_subscription_categories) {
            $tables .= " JOIN axs_pay_subscription_categories AS cat ON sub.plan_id = plans.id AND plans.cat_id = cat.id";
        }

        $limit = "LIMIT 50";
        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            $where
            ORDER BY sub.id ASC
            $limit
        ";

        //die($query);

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }

    public function getEventsData($layout, $params, $report) {

        //joom_eb_events
        //joom_eb_categories
        //joom_eb_event_categories
        //joom_eb_locations
        //joom_eb_registrants

        $db = JFactory::getDbo();

        $report_type = $params->events_report_type;
        $settings = $params->{"events_" . $report_type . "_settings"};

        $fields = "";
        $columns = new stdClass();

        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $include_user_table = false;
        $concatenate_event_time_and_tz = false;

        if ($report_type == "registrants") {
            self::getCustomFieldSQL($fields, $columns, $params, $report);
        }

        foreach ($settings as $setting) {

            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";

            if ($report_type == "registrants") {
                switch ($setting) {

                    case "username":
                        $column_title = "User Name";
                        $column_width = 200;
                        $fields .= "user.name AS username,";
                        $include_user_table = true;
                        break;
                    case "email":
                        $column_title = "User Email";
                        $column_width = 200;
                        $fields .= "user." . $setting . ",";
                        $include_user_table = true;
                        break;
                    case "city":
                        $column_title = "User City";
                        if ($field_id == "") { $field_id = 10; }
                        break;
                    case "state":
                        $column_title = "User State";
                        if ($field_id == "") { $field_id = 9; }
                        break;
                    case "address":
                        $column_title = "User Address";
                        if ($field_id == "") { $field_id = 8; }
                        break;
                    case "zip":
                        $column_title = "User Zip";
                        if ($field_id == "") { $field_id = 23; }
                        break;
                    case "country":
                        $column_title = "User Country";
                        if ($field_id == "") { $field_id = 11; }
                        break;
                    case "groups":
                        $column_title = "Groups";
                        $column_width = 200;
                        $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                        $include_user_table = true;
                        break;

                    //Registrants Info
                    case "user_id":
                        $column_title = "User ID";
                        $fields .= "reg." . $setting . ",";
                        $include_user_table = true;
                        break;
                    case "group_id":
                        $column_title = "Group ID";
                        $fields .= "reg." . $setting . ",";
                        $include_user_table = true;
                        break;
                    case "event_id":
                        $column_title = "Event ID";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "event_name":
                        $column_title = "Event Name";
                        //$fields .= "reg." . $setting . ",";
                        $fields .= "(SELECT title FROM joom_eb_events AS event WHERE event.id = reg.event_id) AS event_name,";
                        break;
                    case "event_location":
                        $column_title = "Event Location";
                        $fields .= "(SELECT name FROM joom_eb_locations AS location INNER JOIN joom_eb_events AS events ON events.location_id = location.id WHERE events.id = reg.event_id) AS event_location,";
                        break;
                    case "event_date":
                        $column_title = "Event Date";
                        $fields .= "(SELECT DATE(event_date) FROM joom_eb_events AS event WHERE event.id = reg.event_id) AS event_date,";
                        break;
                    case "event_time":
                        $column_title = "Event Time";
                        $fields .= "(SELECT TIME(event_date) FROM joom_eb_events AS event WHERE event.id = reg.event_id) AS event_time,";
                        $fields .=  "(SELECT JSON_UNQUOTE(JSON_EXTRACT(params, " . $db->quote('$.time_zone') . ")) FROM joom_eb_events AS events WHERE events.id = reg.event_id) as event_timezone,";
                        $concatenate_event_time_and_tz = true;
                        break;
                    case "number_registrants":
                        $column_title = "Number of Registrants";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "total_amount":
                        $column_title = "Total Amount";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "discount_amount":
                        $column_title = "Discounted Amount";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "amount":
                        $column_title = "Amount";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "register_date":
                        $column_title = "Register Date";
                        $fields .= "DATE(reg." . $setting . ") as $setting,";
                        break;
                    case "payment_date":
                        $column_title = "Payment Date";
                        $fields .= "DATE(reg." . $setting . ") as $setting,";
                        break;
                    case "payment_method":
                        $column_title = "Payment Method";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "transaction_id":
                        $column_title = "Transaction ID";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "payment_processing_fee":
                        $column_title = "Payment Processing Fee";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "late_fee":
                        $column_title = "Late Fee";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "checked_in":
                        $column_title = "Checked In";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "coupon_usage_calculated":
                        $column_title = "Coupon Usage";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "checked_in_count":
                        $column_title = "Checked In Count";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "deposit_amount":
                        $column_title = "Deposit Amount";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "payment_status":
                        $column_title = "Payment Status";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "coupon_id":
                        $column_title = "Coupon ID";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "check_coupon":
                        $column_title = "Check Coupon";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "tax_amount":
                        $column_title = "Tax Amount";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "registration_code":
                        $column_title = "Registration Code";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "is_reminder_sent":
                        $column_title = "Reminder Sent";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "is_deposit_payment_reminder_sent":
                        $column_title = "Deposit Payment Reminder Sent";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "process_deposit_payment":
                        $column_title = "Process Deposit Payment";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "deposit_payment_transaction_id":
                        $column_title = "Deposit Payment Transaction ID";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "deposit_payment_method":
                        $column_title = "Deposit Payment Method";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "is_group_billing":
                        $column_title = "Group Billing";
                        $fields .= "reg." . $setting . ",";
                        break;
                    case "invoice_number":
                        $column_title = "Invoice Number";
                        $fields .= "reg." . $setting . ",";
                        break;
                }

                if ($field_id != "") {
                    $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
                }
            } else if ($report_type == "events") {
                switch ($setting) {
                    case "id":
                        $column_title = "Event ID";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "category":
                        $column_title = "Category";
                        $fields .=  "(SELECT name FROM joom_eb_categories AS cat WHERE cat.id = event.category_id) AS $setting,";
                        break;
                    case "location_name":
                        $column_title = "Location Name";
                        $fields .= "(SELECT name FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_name,";
                        break;
                    case "location_address":
                        $column_title = "Location Address";
                        $fields .= "(SELECT address FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_address,";
                        break;
                    case "location_city":
                        $column_title = "Location City";
                        $fields .= "(SELECT city FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_city,";
                        break;
                    case "location_state":
                        $column_title = "Location State";
                        $fields .= "(SELECT state FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_state,";
                        break;
                    case "location_zip":
                        $column_title = "Location Zip";
                        $fields .= "(SELECT zip FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_zip,";
                        break;
                    case "location_country":
                        $column_title = "Location Country";
                        $fields .= "(SELECT country FROM joom_eb_locations AS loc WHERE loc.id = event.location_id) AS location_country,";
                        break;
                    case "title":
                        $column_title = "Title";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "event_date":
                        $column_title = "Event Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "event_end_date":
                        $column_title = "Event End Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "event_timezone":
                        $column_title = "Event Timezone";
                        $fields .=  "JSON_UNQUOTE(JSON_EXTRACT(params, " . $db->quote('$.time_zone') . ")) as $setting,";
                        break;
                    case "individual_price":
                        $column_title = "Individual Price";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "event_capacity":
                        $column_title = "Event Capacity";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "cut_off_date":
                        $column_title = "Cut Off Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "registration_start_date":
                        $column_title = "Registration Start Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "fixed_group_price":
                        $column_title = "Fixed Group Price";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "min_group_number":
                        $column_title = "Minimum Group Number";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "max_group_number":
                        $column_title = "Max Group Number";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "enable_cancel_registration":
                        $column_title = "Cancel Registration Enabled";
                        $fields .= "CASE WHEN event." . $setting . " = '0' THEN 'No' WHEN event." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        //$fields .=  "event." . $setting . ",";
                        break;
                    case "cancel_before_date":
                        $column_title = "Cancel Before Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "hits":
                        $column_title = "Hits";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "featured":
                        $column_title = "Featured";
                        $fields .= "CASE WHEN event." . $setting . " = '0' THEN 'No' WHEN event." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        break;
                    case "recurring_frequency":
                        $column_title = "Recurring Frequency";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "recurring_end_date":
                        $column_title = "Recurring End Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "recurring_occurrencies":
                        $column_title = "Recurring Occurencies";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "late_fee_date":
                        $column_title = "Late Fee Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "late_fee_amount":
                        $column_title = "Late Fee Amount";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "deposit_amount":
                        $column_title = "Deposit Amount";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "discount_groups":
                        $column_title = "Discount Groups";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "discount_amounts":
                        $column_title = "Discount Amounts";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "discount":
                        $column_title = "Discount";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "early_bird_discount_date":
                        $column_title = "Early Bird Discount Date";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "early_bird_discount_amount":
                        $column_title = "Early Bird Discount Amount";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "enable_auto_reminder":
                        $column_title = "Auto Reminder Enabled";
                        $fields .= "CASE WHEN event." . $setting . " = '0' THEN 'No' WHEN event." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        break;
                    case "remind_before_x_days":
                        $column_title = "Days Reminded Before";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "paypal_email":
                        $column_title = "PayPal Email";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "notification_emails":
                        $column_title = "Notification Emails";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "event_password":
                        $column_title = "Event Password";
                        $fields .=  "event." . $setting . ",";
                        break;
                    case "enable_coupon":
                        $column_title = "Enable Coupon";
                        $fields .= "CASE WHEN event." . $setting . " = '0' THEN 'No' WHEN event." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        break;
                    case "tax_rate":
                        $column_title = "Tax Rate";
                        $fields .=  "event." . $setting . ",";
                        break;
                }
            }

            if ($column_width == "") {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = "";
            $column->filter = "";
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        if ($report_type == "registrants") {
            $tables = "FROM joom_eb_registrants AS reg";

            if ($include_user_table) {
                $tables .= " JOIN joom_users AS user ON reg.user_id = user.id";
            }

            $order = "ORDER BY reg.id ASC";

        } else if ($report_type == "events") {
            $tables = "FROM joom_eb_events AS event";
            $order = "ORDER BY event.id ASC";

            if ($start_date || $end_date) {
                $include_event_periods = true;
            }

            $start_date = $params->event_start_date;
            $end_date = $params->event_end_date;

            if ($start_date) {
                if ($end_date) {
                    $where = " WHERE event.event_date >= DATE('$start_date') AND event.event_end_date <= DATE('$end_date')";
                } else {
                    $where = " WHERE event.event_date >= DATE('$start_date')";
                }
            } else {
                if ($end_date) {
                    $where = " WHERE event.event_end_date <= DATE('$end_date')";
                } else {
                    $where = "";
                }
            }
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $limit = "LIMIT 50";
        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($concatenate_event_time_and_tz) {
            for ($i = 0; $i < count($items); $i++) {
                if ($items[$i]->event_timezone) {
                    $items[$i]->event_time = "{$items[$i]->event_time} ({$items[$i]->event_timezone})";
                }
            }
        }

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;

    }

    public function getStoreData($layout, $params, $report) {

        //joom_eb_events
        //joom_eb_categories
        //joom_eb_event_categories
        //joom_eb_locations
        //joom_eb_registrants

        $db = JFactory::getDbo();

        $report_type = $params->store_report_type;
        $settings = $params->{"store_" . $report_type . "_settings"};

        $fields = "";
        $columns = new stdClass();

        $aggregates = [];
        $schema = [];

        if (count($settings) == 0) {
            return null;
        }

        $include_user_table = false;

        $lang = "'en-GB'";

        foreach ($settings as $setting) {

            $column = new stdClass();
            $field_id = "";
            $column_width = "";
            $column_aggregates = "";
            $column_schema = "";
            $column_footerTemplate = "";

            if ($report_type == "orders") {
                switch ($setting) {
                    case "order_number":
                        $column_title = "Order Number";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "invoice_number":
                        $column_title = "Invoice Number";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "customer_id":
                        $column_title = "Customer ID";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "customergroup_id":
                        $column_title = "Customer Group ID";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "firstname":
                        $column_title = "Customer First Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "lastname":
                        $column_title = "Customer Last Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "email":
                        $column_title = "Customer Email";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "telephone":
                        $column_title = "Customer Telephone";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "fax":
                        $column_title = "Customer Fax";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_firstname":
                        $column_title = "Payment First Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_lastname":
                        $column_title = "Payment Last Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_email":
                        $column_title = "Payment Email";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_telephone":
                        $column_title = "Payment Telephone";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_fax":
                        $column_title = "Payment Fax";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_company":
                        $column_title = "Payment Company";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_address_1":
                        $column_title = "Payment Address 1";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_address_2":
                        $column_title = "Payment Address 2";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_city":
                        $column_title = "Payment City";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_postcode":
                        $column_title = "Payment Postcode";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_country_name":
                        $column_title = "Payment Country";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_zone_name":
                        $column_title = "Payment Zone";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_method_title":
                        $column_title = "Payment Method";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "payment_eu_vat_number":
                        $column_title = "Payment VAT Number";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "transaction_id":
                        $column_title = "Transaction ID";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_firstname":
                        $column_title = "Shipping First Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_lastname":
                        $column_title = "Shipping Last Name";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_email":
                        $column_title = "Shipping Email";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_telephone":
                        $column_title = "Shipping Telephone";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_fax":
                        $column_title = "Shipping Fax";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_company":
                        $column_title = "Shipping Company";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_address_1":
                        $column_title = "Shipping Address 1";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_address_2":
                        $column_title = "Shipping Address 2";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_city":
                        $column_title = "Shipping City";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_postcode":
                        $column_title = "Shipping Postcode";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_country_name":
                        $column_title = "Shipping Country";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_zone_name":
                        $column_title = "Shipping Zone";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_method_title":
                        $column_title = "Shipping Method";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_tracking_number":
                        $column_title = "Shipping Tracking Number";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_tracking_url":
                        $column_title = "Shipping Tracking URL";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "shipping_eu_vat_number":
                        $column_title = "Shipping VAT number";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "total":
                        $column_title = "Total";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "order_status":
                        $column_title = "Order Status";
                        //$fields .= "orders.order_status_id AS order_status,";
                        $fields .= "(SELECT orderstatus_name FROM joom_eshop_orderstatusdetails AS det WHERE orders.order_status_id = det.orderstatus_id AND orders.language = det.language) AS " . $setting . ",";
                        break;
                    case "created_date":
                        $column_title = "Created Date";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "delivery_date":
                        $column_title = "Delivery Date";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "currency":
                        $column_title = "Currency";
                        //$fields .= "orders." . $setting . ",";
                        $fields .= "(SELECT currency_name FROM joom_eshop_currencies AS cur WHERE orders.currency_id = cur.id) AS " . $setting . ",";
                        break;
                    case "currency_exchanged_value":
                        $column_title = "Currency Exchange Value";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "coupon_code":
                        $column_title = "Coupon Code";
                        $fields .= "orders." . $setting . ",";
                        break;
                    case "voucher_code":
                        $column_title = "Voucher Code";
                        $fields .= "orders." . $setting . ",";
                        break;
                }
            } else if ($report_type == "products") {
                switch ($setting) {
                    case "product_sku":
                        $column_title = "SKU";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "manufacturer":
                        $column_title = "Manufacturer";
                        $fields .= "(SELECT manufacturer_name FROM joom_eshop_manufacturerdetails AS det WHERE prod.manufacturer_id = det.manufacturer_id AND det.language = $lang) AS " . $setting . ",";
                        break;
                    case "product_sold_units":
                        $column_title = "Sold Units";
                        //$fields .= "prod." . $setting . ",";
                        $fields .= "(SELECT SUM(quantity) FROM joom_eshop_orderproducts AS ord WHERE ord.product_id = prod.id) AS $setting,";
                        break;
                    case "product_sales":
                        $column_title = "Totals";
                        //$fields .= "prod." . $setting . ",";
                        $fields .= "(SELECT SUM(quantity * price) FROM joom_eshop_orderproducts AS ord WHERE ord.product_id = prod.id) AS $setting,";
                        break;
                    case "product_weight":
                        $column_title = "Weight";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_weight_type":
                        $column_title = "Weight Units";
                        $fields .= "(SELECT weight_unit FROM joom_eshop_weightdetails AS det WHERE prod.product_weight_id = det.weight_id AND det.language = $lang) AS " . $setting . ",";
                        break;
                    case "product_length":
                        $column_title = "Length";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_width":
                        $column_title = "Width";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_height":
                        $column_title = "Height";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_length_units":
                        $column_title = "Length Units";
                        $fields .= "(SELECT length_unit FROM joom_eshop_lengthdetails AS det WHERE prod.product_length_id = det.length_id AND det.language = $lang) AS " . $setting . ",";
                        break;
                    case "product_price":
                        $column_title = "Price";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_call_for_price":
                        $column_title = "Call For Price";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_taxclass_id":
                        $column_title = "Tax Class";
                        //$fields .= "prod." . $setting . ",";
                        $fields .= "(SELECT taxclass_name FROM joom_eshop_taxclasses AS tax WHERE tax.id = prod.product_taxclass_id) AS product_taxclass_id,";
                        break;
                    case "product_quantity":
                        $column_title = "Quantity";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_threshold":
                        $column_title = "Threshold";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_threshold_notify":
                        $column_title = "Notification Threshold";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_stock_status":
                        $column_title = "Stock Status";
                        $fields .= "(SELECT stockstatus_name FROM joom_eshop_stockstatusdetails AS det WHERE prod.product_stock_status_id = det.stockstatus_id AND det.language = $lang) AS " . $setting . ",";
                        break;
                    case "product_stock_checkout":
                        $column_title = "Stock Checkout";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_minimum_quantity":
                        $column_title = "Minimum Quantity";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_maximum_quantity":
                        $column_title = "Maximum Quantity";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_shipping_cost":
                        $column_title = "Shipping Cost";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_available_date":
                        $column_title = "Availability Date";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_featured":
                        $column_title = "Featured";
                        $fields .= "CASE WHEN prod." . $setting . " = '0' THEN 'No' WHEN prod." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        break;
                    case "product_customergroups":
                        $column_title = "Customer Groups";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "product_quote_mode":
                        $column_title = "Quote Mode";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "published":
                        $column_title = "Published";
                        $fields .= "CASE WHEN prod." . $setting . " = '0' THEN 'No' WHEN prod." . $setting . " = '1' THEN 'Yes' END AS " . $setting . ",";
                        break;
                    case "hits":
                        $column_title = "Hits";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "created_date":
                        $column_title = "Created Date";
                        $fields .= "prod." . $setting . ",";
                        break;
                    case "affiliate_code":
                        $column_title = "Affiliate Code";
                        $fields .= "prod." . $setting . ",";
                        break;
                }
            }

            if ($column_width == "") {
                $column_width = 150;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = "";
            $column->filter = "";
            $column->aggregates = $column_aggregates;
            $column->width = $column_width;
            $column->footerTemplate = $column_footerTemplate;

            if ($column_aggregates) {

                foreach ($column_aggregates as $agg) {
                    $newAgg = new stdClass();
                    $newAgg->field = $setting;
                    $newAgg->aggregate = $agg;
                    array_push($aggregates, $newAgg);
                }
            }

            if ($column_schema) {
                $newSchema = new stdClass();
                $newSchema->field = $setting;
                $newSchema->schema = $column_schema;
                array_push($schema, $newSchema);
            }

            $columns->$setting = $column;
        }

        if ($report_type == "products") {
            $tables = "FROM joom_eshop_products AS prod";

            /*if ($include_user_table) {
                $tables .= " JOIN joom_users AS user ON reg.user_id = user.id";
            }*/

            $order = "ORDER BY prod.id ASC";

        } else if ($report_type == "orders") {
            $tables = "FROM joom_eshop_orders AS orders";
            $order = "ORDER BY orders.id ASC";

            /*if ($start_date || $end_date) {
                $include_event_periods = true;
            }

            $start_date = $params->event_start_date;
            $end_date = $params->event_end_date;

            if ($start_date) {
                if ($end_date) {
                    $where = " WHERE event.event_date >= DATE('$start_date') AND event.event_end_date <= DATE('$end_date')";
                } else {
                    $where = " WHERE event.event_date >= DATE('$start_date')";
                }
            } else {
                if ($end_date) {
                    $where = " WHERE event.event_end_date <= DATE('$end_date')";
                } else {
                    $where = "";
                }
            }*/

            self::getCustomFieldSQL($fields, $columns, $params, $report);
        }

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $limit = "LIMIT 50";
        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            $where
            $order
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates, $schema);
        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;

    }

    public function getEducationData($layout, $params, $report) {

        $report_type = $params->education_report_type;
        $settings_type = "education_" . $report_type . "_settings";
        $settings = $params->$settings_type;
        $functionName = "getEducation" . ucfirst($report_type) . "Data";

        //Since there are numerous reports for the LMS, they're stored in the helper file.

        return self::$functionName($layout, $params, $report, $settings);
    }

    /*
    public function getBoardsData($layout, $params, $report) {

        $db = JFactory::getDbo();

        $fields = "";
        $columns = new stdClass();

        $aggregates = null;

        $settings = $params->user_board_settings;

        if (count($settings) == 0) {
            return null;
        }

        $include_subscriptions = false;

        foreach ($settings as $setting) {
            $column = new stdClass();
            $field_id = "";
            $column_width = "";

            switch ($setting) {
                case "user_id":
                    $column_title = "ID";
                    $fields .= "user.id as user_id,";
                    break;
                case "name":
                    $column_title = "Name";
                    $fields .= "user." . $setting . ",";
                    break;
                case "username":
                    $column_title = "User Name";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    break;
                case "email":
                    $column_title = "Email";
                    $column_width = 200;
                    $fields .= "user." . $setting . ",";
                    break;
                case "city":
                    $column_title = "City";
                    if ($field_id == "") { $field_id = 10; }
                    break;
                case "state":
                    $column_title = "State";
                    if ($field_id == "") { $field_id = 9; }
                    break;
                case "address":
                    $column_title = "Address";
                    if ($field_id == "") { $field_id = 8; }
                    break;
                case "zip":
                    $column_title = "Zip";
                    if ($field_id == "") { $field_id = 23; }
                    break;
                case "country":
                    $column_title = "Country";
                    if ($field_id == "") { $field_id = 11; }
                    break;
                case "groups":
                    $column_title = "Groups";
                    $column_width = 200;
                    $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS groups ON (map.group_id = groups.id) WHERE map.user_id = user.id) AS $setting,";
                    break;

                case "board_id":
                    $column_title = "Board ID";
                    $fields .= "board." . $setting . ",";
                    break;
                case "category":
                    $column_title = "Category";
                    $fields .= "board.categories as category,";
                    break;
                case "board_name":
                    $column_title = "Board Name";
                    $fields .= "board.name as board_name,";
                    break;
                case "teams":
                    $column_title = "Public";
                    break;
                case "teams":
                    $column_title = "Team List";
                    break;
                case "image_image":
                    $column_title = "Image";
                    break;
                case "image_post":
                    $column_title = "Image Text";
                    break;
                case "goal_type":
                    $column_title = "Goal Type";
                    break;
                case "goal_text":
                    $column_title = "Goal Text";
                    break;
                case "goal_amount":
                    $column_title = "Goal Amount";
                    break;
                case "goal_accomplishable":
                    $column_title = "Goal Accomplishable";
                    break;
                case "goal_start_date":
                    $column_title = "Goal Start Date";
                    break;
                case "goal_end_date":
                    $column_title = "Goal End Date";
                    break;
                case "affirm_what":
                    $column_title = "Affirmation";
                    break;
                case "affirm_why":
                    $column_title = "Affirmation Reason";
                    break;
                case "affirm_count":
                    $column_title = "Reaffirmation Clicks";
                    break;
                case "affirm_last":
                    $column_title = "Last Affirmation";
                    break;

            }

            if ($field_id != "") {
                $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
            }

            if ($column_width == "") {
                $column_width = 100;
            }

            $column = new stdClass;
            $column->field = $setting;
            $column->title = $column_title;
            $column->format = "";
            $column->filter = "";
            $column->width = $column_width;

            $columns->$setting = $column;
        }

        self::getCustomFieldSQL($fields, $columns, $params, $report);

        //Take off the last comma on $joom_fields
        if ($fields != "") {
            $fields = rtrim($fields, ",");
        }

        $tables = "FROM joom_users AS user";
        if ($include_subscriptions) {
            $tables .= " LEFT JOIN axs_pay_subscriptions AS sub ON user.id = sub.user_id";
        }

        $limit = "";

        $query = "
            SELECT
            $fields
            $tables
            ORDER BY user.id ASC
            $limit
        ";

        $db->setQuery($query);
        $items = $db->loadObjectList();

        if ($layout == 'grid' || !$layout || $layout == 'both') {
            $result = self::buildGridData($items, $columns, $report, $aggregates);
        }

        if ($layout == 'spreadsheet') {
            $result = self::buildSpreadsheetData($items, $columns, $report);
        }

        return $result;
    }
    */

    public function getData($report) {
    	$type   = $report->report_type;
		$params = json_decode($report->params);
		$layout = $params->report_layout;
        $functionName = "self::get" . ucfirst($type) . "Data";

        $trackParams = new stdClass();
        $trackParams->eventName = 'Run Report From Admin Report Builder';
        $trackParams->description = $report->name.' [type:'.$report->report_type.' id:'.$report->id.']';
        AxsTracking::sendToPendo($trackParams);
        $params->action_type ="admin";
        AxsActions::storeAdminAction($trackParams);

        // getUsersData
        // getNotificationData
        // getTransactionsData
        // getSubscriptionsData
        // getEventsData
        // getStoreData
        // getEducationData
        // getExternalData

		$data = call_user_func($functionName, $layout, $params, $report);

        return $data;
    }

    public function getExternalData($layout, $params, $report) {
        $result = [];
        $columns = json_decode($params->external_data);
		$countColumns = count($columns);

		for ($i = 0; $i < $countColumns; $i++) {
			if($columns[$i]->type != 'label' && $columns[$i]->checked == 'checked'){
				$field = [
					"field" => $columns[$i]->field,
					"title" => ucfirst($columns[$i]->title)
				];

				if($columns[$i]->type == 'object') {
					$field['template'] = "#=checkObject(".$columns[$i]->field.")#";
					$field['filterable'] = false;
				}
			array_push($result, $field);
			}
    	}

        $data = new stdClass();
        $data->grid->columns = json_encode($result);
        $data->grid->aggregates = null;
        $data->grid->schema = null;
        return $data;
    }

    public function getColumnLetter($column) {
    	$column = $column - 1;
    	$letters = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
    	$extLetters = $letters;
    	if($column <= 26){
    		return $extLetters[$column];
    	}
    	if ($column > 26) {
    		$startingLetter = 'A';
    		foreach($letters as $letter) {
				array_push($extLetters, $startingLetter.$letter);
    		}
    	}
    	if ($column > 52) {
    		$startingLetter = 'B';
    		foreach($letters as $letter) {
				array_push($extLetters, $startingLetter.$letter);
    		}
    	}
    	if ($column > 78) {
    		$startingLetter = 'C';
    		foreach($letters as $letter) {
				array_push($extLetters, $startingLetter.$letter);
    		}
    	}
    	if ($column > 104) {
    		$startingLetter = 'D';
    		foreach($letters as $letter) {
				array_push($extLetters, $startingLetter.$letter);
    		}
    	}
    	if ($column > 130) {
    		$startingLetter = 'E';
    		foreach($letters as $letter) {
				array_push($extLetters, $startingLetter.$letter);
    		}
    	}
    	return $extLetters[$column];
    }

	public function save($data) {
        $params = new stdClass();
        $data['name'] = AxsSecurity::alphaNumericOnly(AxsSecurity::cleanInput($data['name']));
		$params->report_layout = $data['report_layout'];
		$params->users_settings = $data['users_settings'];
        $params->notifications_settings = $data['notifications_settings'];
        $params->transactions_settings = $data['transactions_settings'];
        $params->transactions_start_date = $data['transactions_start_date'];
        $params->transactions_end_date = $data['transactions_end_date'];
        $params->subscriptions_settings = $data['subscriptions_settings'];
        $params->subscriptions_start_date = $data['subscriptions_start_date'];
        $params->subscriptions_end_date = $data['subscriptions_end_date'];
        $params->education_report_type = $data['education_report_type'];
        $params->education_progress_settings = $data['education_progress_settings'];
        $params->education_assigned_settings = $data['education_assigned_settings'];
        $params->education_quiz_settings = $data['education_quiz_settings'];
        $params->education_survey_settings = $data['education_survey_settings'];
        $params->education_survey_summary_settings = $data['education_survey_summary_settings'];
        $params->education_certificates_settings = $data['education_certificates_settings'];
        $params->education_average_settings = $data['education_average_settings'];
        $params->education_enrollment_settings = $data['education_enrollment_settings'];
        $params->education_purchase_settings = $data['education_purchase_settings'];
        $params->education_structure_settings = $data['education_structure_settings'];
        $params->education_ratings_settings = $data['education_ratings_settings'];
        $params->education_start_date = $data['education_start_date'];
        $params->education_end_date = $data['education_end_date'];
        $params->events_report_type = $data['events_report_type'];
        $params->events_registrants_settings = $data['events_registrants_settings'];
        $params->events_events_settings = $data['events_events_settings'];
        $params->event_start_date = $data['event_start_date'];
        $params->event_end_date = $data['event_end_date'];
        $params->store_report_type = $data['store_report_type'];
        $params->store_orders_settings = $data['store_orders_settings'];
        $params->store_products_settings = $data['store_products_settings'];
        $params->user_board_settings = $data['user_board_settings'];
		$params->report_theme = $data['report_theme'];
		$params->external_url = $data['external_url'];
		$params->external_data = $data['external_data'];
        $params->custom_fields = $data['custom_fields'];
        $params->usergroup_filter = $data['usergroup_filter'];
        $params->use_group_filter = $data['use_group_filter'];
        $params->assignment = $data['assignment'];
        $params->quiz = $data['quiz'];
        $params->survey = $data['survey'];
        $params->survey_summary = $data['survey_summary'];
        $params->quiz_average = $data['quiz_average'];
        $params->award_type = $data['award_type'];
        $params->certificate = $data['certificate'];
        $params->badge = $data['badge'];
        $params->milestone = $data['milestone'];

        $params->teams = $data['teams'];
        $params->splms_course_id = $data['splms_course_id'];
        $params->show_team_lesson_activities = $data['show_team_lesson_activities'];
        $params->splms_lesson_id = $data['splms_lesson_id'];
        $params->team_lesson_activity = $data['team_lesson_activity'];
        $params->team_course_assignment = $data['team_course_assignment'];
        $params->team_survey = $data['team_survey'];
        $params->team_quiz = $data['team_quiz'];
        $params->team_checklist = $data['team_checklist'];
        $params->education_team_summary_settings = $data['education_team_summary_settings'];
        $params->education_point_totals_settings = $data['education_point_totals_settings'];

        $params->include_archived_activity = $data['include_archived_activity'];

        if ($data['use_custom_fields'] == "1") {
            $params->use_custom_fields = true;
        } else {
            $params->use_custom_fields = false;
        }

        $data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['report_layout'] = $params->report_layout;
			$data['report_theme'] = $params->report_theme;
			$data['users_settings'] = $params->users_settings;
            $data['notifications_settings'] = $params->notifications_settings;
            $data['transactions_settings'] = $params->transactions_settings;
            $data['transactions_start_date'] = $params->transactions_start_date;
            $data['transactions_end_date'] = $params->transactions_end_date;
            $data['subscriptions_settings'] = $params->subscriptions_settings;
            $data['subscriptions_start_date'] = $params->subscriptions_start_date;
            $data['subscriptions_end_date'] = $params->subscriptions_end_date;
            $data['education_report_type'] = $params->education_report_type;
            $data['education_progress_settings'] = $params->education_progress_settings;
            $data['education_assigned_settings'] = $params->education_assigned_settings;
            $data['education_quiz_settings'] = $params->education_quiz_settings;
            $data['education_survey_settings'] = $params->education_survey_settings;
            $data['education_survey_summary_settings'] = $params->education_survey_summary_settings;
            $data['education_certificates_settings'] = $params->education_certificates_settings;
            $data['education_enrollment_settings'] = $params->education_enrollment_settings;
            $data['education_purchase_settings'] = $params->education_purchase_settings;
            $data['education_structure_settings'] = $params->education_structure_settings;
            $data['education_ratings_settings'] = $params->education_ratings_settings;
            $data['education_start_date'] = $params->education_start_date;
            $data['education_end_date'] = $params->education_end_date;
            $data['events_report_type'] = $params->events_report_type;
            $data['events_registrants_settings'] = $params->events_registrants_settings;
            $data['events_events_settings'] = $params->events_events_settings;
            $data['event_start_date'] = $params->event_start_date;
            $data['event_end_date'] = $params->event_end_date;
            $data['user_board_settings'] = $params->user_board_settings;
            $data['store_report_type'] = $params->store_report_type;
            $data['store_orders_settings'] = $params->store_orders_settings;
            $data['store_products_settings'] = $params->store_products_settings;
            $data['custom_fields'] = $params->custom_fields;
            $data['usergroup_filter'] = $params->usergroup_filter;
            $data['use_group_filter'] = $params->use_group_filter;
            $data['assignment'] = $params->assignment;
            $data['quiz'] = $params->quiz;
            $data['survey'] = $params->survey;
            $data['survey_summary'] = $params->survey_summary;
            $data['education_average_settings'] = $params->education_average_settings;
            $data['quiz_average'] = $params->quiz_average;
            $data['award_type'] = $params->award_type;
            $data['certificate'] = $params->certificate;
            $data['badge'] = $params->badge;
            $data['milestone'] = $params->milestone;

            $data['teams'] = $params->teams;
            $data['splms_course_id'] = $params->splms_course_id;
            $data['show_team_lesson_activities'] = $params->show_team_lesson_activities;
            $data['splms_lesson_id'] = $params->splms_lesson_id;
            $data['team_lesson_activity'] = $params->team_lesson_activity;
            $data['team_course_assignment'] = $params->team_course_assignment;
            $data['team_survey'] = $params->team_survey;
            $data['team_quiz'] = $params->team_quiz;
            $data['team_checklist'] = $params->team_checklist;
            $data['education_team_summary_settings'] = $params->education_team_summary_settings;
            $data['education_point_totals_settings'] = $params->education_point_totals_settings;

            $data['include_archived_activity'] = $params->include_archived_activity;

            if ($params->use_custom_fields) {
                $data['use_custom_fields'] = "1";
            } else {
                $data['use_custom_fields'] = "0";
            }

			return $data;
		}
	}
}
