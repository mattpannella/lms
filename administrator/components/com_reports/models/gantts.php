<?php
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class ReportsModelGantts extends FOFModel {


	public function save($data) {
		$params = new stdClass();
        $params->start_date = $data['start_date'];
        $params->end_date = $data['end_date'];
        $params->height = $data['height'];
		$params->report_theme = $data['report_theme'];
        $params->chart_default = $data['chart_default'];
        $data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['report_theme'] = $params->report_theme;
            $data['chart_default'] = $params->chart_default;
            $data['height'] = $params->height;
            $data['start_date'] = $params->start_date;
            $data['end_date'] = $params->end_date;
			return $data;
		}
	}
}