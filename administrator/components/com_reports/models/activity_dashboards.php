<?php
defined('_JEXEC') or die;

class ReportsModelActivity_dashboards extends FOFModel {


	public function save($data) {
		$params = new stdClass();
		$data['name'] = AxsSecurity::alphaNumericOnly(AxsSecurity::cleanInput($data['name']));
        $params->theme = $data['theme'];
        $params->default_range = $data['default_range'];
        $params->course_selection_type = $data['course_selection_type'];
		$params->courses = !empty($data['courses']) ? implode(',',$data['courses']) : null;
		$params->user_selection_type = $data['user_selection_type'];
        $params->userlist = $data['userlist'];
        $params->usergroups_selection_type = $data['usergroups_selection_type'];
        $params->usergroup_filter = !empty($data['usergroup_filter']) ? implode(',',$data['usergroup_filter']) : null;
        $params->assignments_selection_type = $data['assignments_selection_type'];
        $params->assignments = !empty($data['assignments']) ? implode(',',$data['assignments']) : null;
        $params->certificates_selection_type = $data['certificates_selection_type'];
        $params->certificates = !empty($data['certificates']) ? implode(',',$data['certificates']) : null;
        $params->badges_selection_type = $data['badges_selection_type'];
        $params->badges = !empty($data['badges']) ? implode(',',$data['badges']) : null;
        $params->charts = !empty($data['charts']) ? implode(',',$data['charts']) : null;
        $params->data_points = !empty($data['data_points']) ? implode(',',$data['data_points']) : null;
        $data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['theme'] = $params->theme;
	        $data['default_range'] = $params->default_range;
	        $data['course_selection_type'] = $params->course_selection_type;
			$data['courses'] = $params->courses;
			$data['user_selection_type'] = $params->user_selection_type;
	        $data['userlist'] = $params->userlist;
	        $data['usergroups_selection_type'] = $params->usergroups_selection_type;
	        $data['usergroup_filter'] = explode(',',$params->usergroup_filter);
	        $data['assignments_selection_type'] = $params->assignments_selection_type;
	        $data['assignments'] = explode(',',$params->assignments);	        
	        $data['certificates_selection_type'] = $params->certificates_selection_type;
	        $data['certificates'] = explode(',',$params->certificates);
	        $data['badges_selection_type'] = $params->badges_selection_type;
	        $data['badges'] = explode(',',$params->badges);
	        $data['charts'] = explode(',',$params->charts);
	        $data['data_points'] = explode(',',$params->data_points);
			return $data;
		}
	}
}