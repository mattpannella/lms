<?php

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

function getEducationStructureData($layout, $params, $report, $settings) {
	$db = JFactory::getDbo();

    $fields = "";
    $columns = new stdClass();

    $aggregates = [];
    $schema = [];

    if (count($settings) == 0) {
        return null;
    }

    foreach ($settings as $setting) {
    	switch ($setting) {
    		case "course_purchases":
    			$showPurchases = true;
    			break;
    		case "teacher_rating":
    		case "lesson_rating":
    		case "course_rating":
    			$showRatings = true;
                break;
    		case "teacher":
    			$showTeachers = true;
    		case "quiz":
    			$showQuizzes = true;
    	}
    }

    $query = "SELECT splms_course_id AS id, splms_coursescategory_id AS cat_id, title, price FROM joom_splms_courses";
    $db->setQuery($query);
    $courses = $db->loadObjectList();

    $query = "SELECT splms_lesson_id AS id, title, teacher_materials AS materials, splms_course_id AS course_id, required_quizzes FROM joom_splms_lessons";
    $db->setQuery($query);
    $lessons = $db->loadObjectList();

    foreach ($lessons as &$lesson) {
    	$lesson->materials = json_decode($lesson->materials);
    	$lesson->required_quizzes = json_decode($lesson->required_quizzes);
    }

    $query = "SELECT splms_quizquestion_id AS id, title, splms_course_id AS course_id, splms_lesson_id AS lesson_id FROM joom_splms_quizquestions";
    $db->setQuery($query);
    $quizzes = $db->loadObjectList();

    $query = "SELECT splms_teacher_id AS id, title FROM joom_splms_teachers";
    $db->setQuery($query);
    $teachers = $db->loadObjectList();

    $query = "SELECT splms_coursescategory_id AS id, title FROM joom_splms_coursescategories";
    $db->setQuery($query);
    $categories = $db->loadObjectList();

    if ($showPurchases) {
	    $query = "SELECT course_id,COUNT(*) as count FROM axs_course_purchases WHERE status='PAID' GROUP BY course_id ORDER BY count DESC";
	    $db->setQuery($query);
	    $purchases = $db->loadObjectList();
	}

    if ($showPurchases) {
	    $query = "SELECT * FROM ratings_post";
	    $db->setQuery($query);
	    $ratings = $db->loadObjectList();
	}

    $items = array();

    $lessonCount = count($lessons);
	$teacherCount = count($teachers);
	$courseCount = count($courses);
	$categoryCount = count($categories);
	$purchasesCount = count($purchases);
	$quizCount = count($quizzes);
	$ratingsCount = count($ratings);

	if ($showRatings) {

		$tally = array();
		$courseRating = array();

		for ($r = 0; $r < $ratingsCount; $r++) {
			$rating = $ratings[$r];
			$id = $rating->rating_id;
			if (strpos($id, "course_") !== false) {

				$lesson_pos = strpos($id, "lesson_");
				$teacher_pos = strpos($id, "teacher_");
				$length = strlen($id);

				$which_course = substr($id, 7, $lesson_pos - 8);
				$which_lesson = substr($id, $lesson_pos + 7, $teacher_pos - 17);
				$which_teacher = substr($id, $teacher_pos + 8);

				if (!$which_course) {
					continue;
				}

				if (!isset($tally[$which_course])) {
					$tally[$which_course] = new stdClass();
					$tally[$which_course]->lessons = array();
					$tally[$which_course]->ratings = array();
				}

				if (!isset($tally[$which_course]->lessons[$which_lesson])) {
					$tally[$which_course]->lessons[$which_lesson] = new stdClass();
					$tally[$which_course]->lessons[$which_lesson]->teachers = array();
					$tally[$which_course]->lessons[$which_lesson]->ratings = array();
				}

				if (!isset($tally[$which_course]->lessons[$which_lesson]->teachers[$which_teacher])) {
					$tally[$which_course]->lessons[$which_lesson]->teachers[$which_teacher] = new stdClass();
					$tally[$which_course]->lessons[$which_lesson]->teachers[$which_teacher]->ratings = array();
				}

				for ($i = 1; $i <= 5; $i++) {
					$tally[$which_course]->ratings[$i] += $rating->{"stars" . $i};
					$tally[$which_course]->lessons[$which_lesson]->ratings[$i] += $rating->{"stars" . $i};
					$tally[$which_course]->lessons[$which_lesson]->teachers[$which_teacher]->ratings[$i] += $rating->{"stars" . $i};
				}
			}
		}
	}



	for ($l = 0; $l < $lessonCount; $l++) {

    	$item = new stdClass();

    	$lesson = $lessons[$l];
    	$item->lesson = $lesson->title;

    	if ($showTeachers) {
	    	$teacherList = "";
	    	for ($t = 0; $t < $teacherCount; $t++) {
	    		$teacher = $teachers[$t];

	    		if (in_array($teacher->id, $lesson->materials->splms_teacher_id)) {
	    			$teacherList .= $teacher->title . ", ";
		    	}
	    	}

	    	if ($teacherList != "") {
			    $teacherList = rtrim($teacherList, ", ");
			}

			$item->teacher = $teacherList;
		}

		if ($showQuizzes) {
			$quizList = "";
			for ($q = 0; $q < $quizCount; $q++) {
				$quiz = $quizzes[$q];
				if ($quiz->lesson_id == $lesson->id) {
					$quizList .= $quiz->title . ", ";
				}
			}

			if ($quizList != "") {
				$quizList = rtrim($quizList, ", ");
			}

			$item->quiz = $quizList;
		}

		for ($c = 0; $c < $courseCount; $c++) {
    		$course = $courses[$c];

    		if ($course->id == $lesson->course_id) {
    			break;
    		}
    	}

    	$item->course = $course->title;
    	$item->course_cost = $course->price;

    	for ($c = 0; $c < $categoryCount; $c++) {
    		$category = $categories[$c];
    		if ($course->cat_id == $category->id) {
    			break;
    		}
    	}

    	$item->category = $category->title;

    	if ($showPurchases) {
	    	for ($p = 0; $p < $purchasesCount; $p++) {
	    		$purchase = $purchases[$p];
	    		if ($purchase->course_id == $course->id) {
	    			break;
	    		}
	    	}

	    	$item->course_purchases = $purchase->count;
	    }

	    if ($showRatings) {
	    	$course_ratings = $tally[$course->id]->ratings;
	    	$lesson_ratings = $tally[$course->id]->lessons[$lesson->id]->ratings;
	    	$teacher_ratings = $tally[$course->id]->lessons[$lesson->id]->teachers[$teacher->id]->ratings;

	    	$rat = 0;
	    	$count = 0;
	    	for ($i = 1; $i <= 5; $i++) {
	    		$count += $course_ratings[$i];
	    		$rat += $course_ratings[$i] * $i;
	    	}

	    	if ($count != 0) {
	    		$final = "" . ceil(($rat * 100) / $count) / 100;
	    	} else {
	    		$final = "-";
	    	}

	    	$item->course_rating = $final;
	    	$item->course_votes = $count;

	    	$rat = 0;
	    	$count = 0;
	    	for ($i = 1; $i <= 5; $i++) {
	    		$count += $lesson_ratings[$i];
	    		$rat += $lesson_ratings[$i] * $i;
	    	}

	    	if ($count != 0) {
	    		$final = "" . ceil(($rat * 100) / $count) / 100;
	    	} else {
	    		$final = "-";
	    	}

	    	$item->lesson_rating = $final;
	    	$item->lesson_votes = $count;

	    	$rat = 0;
	    	$count = 0;
	    	for ($i = 1; $i <= 5; $i++) {
	    		$count += $teacher_ratings[$i];
	    		$rat += $teacher_ratings[$i] * $i;
	    	}

	    	if ($count != 0) {
	    		$final = "" . ceil(($rat * 100) / $count) / 100;
	    	} else {
	    		$final = "-";
	    	}

	    	$item->teacher_rating = $final;
	    	$item->teacher_votes = $count;
	    }

    	array_push($items, $item);
    }

    foreach ($settings as $setting) {
        $column = new stdClass();
        $column_width = "";
        $column_aggregates = "";
        $column_schema = "";
        $column_footerTemplate = "";

        switch ($setting) {
			case "category":
            	$column_title = "Category";
            	break;
            case "course":
            	$column_title = "Course";
            	break;
			case "lesson":
				$column_title = "Lesson";
				break;
			case "quiz":
				$column_title = "Quiz";
				break;
			case "teacher":
				$column_title = "Teacher";
				break;
			case "course_cost":
				$column_title = "Course Cost";
				break;
			case "course_purchases":
				$column_title = "Course Purchases";
				break;
			case "course_rating":
				$column_title = "Course Rating";
				break;
			case "course_votes":
				$column_title = "Course Votes";
				break;
			case "lesson_rating":
				$column_title = "Lesson Rating";
				break;
			case "lesson_votes":
				$column_title = "Lesson Votes";
				break;
			case "teacher_rating":
				$column_title = "Teacher Rating";
				break;
			case "teacher_votes":
				$column_title = "Teacher Votes";
				break;

        }

        $column = new stdClass;
        $column->field = $setting;
        $column->title = $column_title;
        $column->format = "";
        $column->filter = "";
        $column->aggregates = $column_aggregates;
        $column->width = $column_width;
        $column->footerTemplate = $column_footerTemplate;

        if ($column_aggregates) {

            foreach ($column_aggregates as $agg) {
                $newAgg = new stdClass();
                $newAgg->field = $setting;
                $newAgg->aggregate = $agg;
                array_push($aggregates, $newAgg);
            }
        }

        if ($column_schema) {
            $newSchema = new stdClass();
            $newSchema->field = $setting;
            $newSchema->schema = $column_schema;
            array_push($schema, $newSchema);
        }

        $columns->$setting = $column;
    }

    if ($layout == 'grid' || !$layout || $layout == 'both') {
        $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
    }

    if ($layout == 'spreadsheet') {
        $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
    }

    return $result;
}



/*function getEducationGradeData($layout, $params, $report, $settings) {

	$db = JFactory::getDbo();

    $fields = "";
    $columns = new stdClass();

    $aggregates = [];
    $schema = [];

    if (count($settings) == 0) {
        return null;
    }

    $start_date = $params->education_start_date;
    $end_date = $params->education_end_date;

    $include_user_table = false;
    $include_course_table = false;
    $include_lesson_table = false;
    $include_quiz_table = false;
    $include_quiz_results_table = false;

    $group_by_category = false;
    $group_by_course = false;
    $group_by_lesson = false;
    $group_by_quiz = false;

    foreach ($settings as $setting) {
        $column = new stdClass();
        $field_id = "";
        $column_width = "";
        $column_aggregates = "";
        $column_schema = "";
        $column_footerTemplate = "";



        switch ($setting) {

            //User Info

            case "user_id":
                $column_title = "User ID";
                $fields .= "user.id AS user_id,";
                $include_user_table = true;
                break;
            case "username":
                $column_title = "User Name";
                $column_width = 200;
                $fields .= "user.name AS username,";
                $include_user_table = true;
                break;
            case "email":
                $column_title = "Email";
                $column_width = 200;
                $fields .= "user." . $setting . ",";
                $include_user_table = true;
                break;
            case "city":
                $column_title = "City";
                if ($field_id == "") { $field_id = 10; }
                break;
            case "state":
                $column_title = "State";
                if ($field_id == "") { $field_id = 9; }
                break;
            case "zip":
                $column_title = "Zip";
                if ($field_id == "") { $field_id = 23; }
                break;
            case "country":
                $column_title = "Country";
                if ($field_id == "") { $field_id = 11; }
                break;
            case "groups":
                $column_title = "Groups";
                $column_width = 200;
                $fields .= "(SELECT GROUP_CONCAT(title SEPARATOR ', ') FROM joom_user_usergroup_map AS map JOIN `joom_usergroups` AS group ON (map.group_id = group.id) WHERE map.user_id = user.id) AS $setting,";
                $include_user_table = true;
                break;

            //Progress Info

			//category
			//start_date
			//end_date
			//course
			//course_score
			//course_total
			//lesson
			//lesson_score
			//lesson_total
			//quiz
			//quiz_score
			//quiz_total
			//quiz_date
			//current

            case "category":
            	$column_title = "Category";
            	//$fields .= "(SELECT cat.title FROM joom_splms_coursescategories AS cat JOIN joom_splms_courses AS course WHERE course.splms_coursescategory_id = cat.splms_coursescategory_id AND results.splms_course_id = course.splms_course_id) AS category,";
            	$fields .= "(SELECT cat.title FROM joom_splms_coursescategories AS cat JOIN joom_splms_courses AS course WHERE course.splms_coursescategory_id = cat.splms_coursescategory_id AND results.splms_course_id = course.splms_course_id) AS category,";
            	$include_quiz_results_table = true;

            	break;

            case "course":
            	$column_title = "Course";
            	$fields .= "(SELECT course.title FROM joom_splms_courses AS course WHERE course.splms_course_id = results.splms_course_id) AS course,";
            	$include_quiz_results_table = true;
            	//$include_course_table = true;

            	break;

            case "course_score":
            	$column_title = "Course Score";
            	//$fields .= "SUM(results.point) AS course_score,";
            	$fields .= "(SELECT SUM(quiz.point) FROM joom_splms_quizresults AS quiz JOIN joom_splms_courses AS course WHERE quiz.user_id = user.id AND quiz.splms_course_id = course.splms_course_id GROUP BY quiz.user_id) AS course_score,";
            	$include_quiz_results_table = true;
            	//$include_course_table = true;
            	break;

            case "course_total":
            	$column_title = "Course Total";
            	//$fields .= "SUM(results.total_marks) AS course_total,";
            	$fields .= "(SELECT SUM(quiz.total_marks) FROM joom_splms_quizresults AS quiz JOIN joom_splms_courses AS course WHERE quiz.user_id = user.id AND quiz.splms_course_id = course.splms_course_id GROUP BY quiz.user_id) AS course_total,";
            	$include_quiz_results_table = true;
            	//$include_course_table = true;
            	break;
        }

        if ($field_id != "") {
            $fields .= "(SELECT value FROM joom_community_fields_values WHERE user_id = user.id AND field_id = $field_id LIMIT 1) AS $setting,";
        }

        $column = new stdClass;
        $column->field = $setting;
        $column->title = $column_title;
        $column->format = "";
        $column->filter = "";
        $column->aggregates = $column_aggregates;
        $column->width = $column_width;
        $column->footerTemplate = $column_footerTemplate;

        if ($column_aggregates) {

            foreach ($column_aggregates as $agg) {
                $newAgg = new stdClass();
                $newAgg->field = $setting;
                $newAgg->aggregate = $agg;
                array_push($aggregates, $newAgg);
            }
        }

        if ($column_schema) {
            $newSchema = new stdClass();
            $newSchema->field = $setting;
            $newSchema->schema = $column_schema;
            array_push($schema, $newSchema);
        }

        $columns->$setting = $column;
    }

    //Take off the last comma on $joom_fields
    if ($fields != "") {
        $fields = rtrim($fields, ",");
    }

    if ($start_date || $end_date) {
        $include_subscription_periods = true;
    }

    if ($start_date) {
        if ($end_date) {
            $where = " WHERE per.start >= DATE('$start_date') AND per.end <= DATE('$end_date')";
        } else {
            $where = " WHERE per.start >= DATE('$start_date')";
        }
    } else {
        if ($end_date) {
            $where = " WHERE per.end <= DATE('$end_date')";
        } else {
            $where = "";
        }
    }

    $tables = "FROM joom_users AS user";

    if ($include_quiz_results_table) {
    	$tables .= " JOIN joom_splms_quizresults AS results ON results.user_id = user.id";
    }

    if ($include_course_table) {
    	$tables .= " JOIN joom_splms_courses AS course ON results.splms_course_id = course.splms_course_id";
    }

    $limit = "LIMIT 50";
    $limit = "";

    $order = " GROUP BY user.id, results.splms_course_id ASC";
    //$order = " GROUP BY course.splms_course_id ASC";

    $query = "
        SELECT
        $fields
        $tables
        $where
        $order
        $limit
    ";

    //die($query);

    $db->setQuery($query);
    $items = $db->loadObjectList();

    if ($layout == 'grid' || !$layout || $layout == 'both') {
        $result = ReportsModelReports::buildGridData($items, $columns, $report, $aggregates, $schema);
    }

    if ($layout == 'spreadsheet') {
        $result = ReportsModelReports::buildSpreadsheetData($items, $columns, $report);
    }

    return $result;

}*/