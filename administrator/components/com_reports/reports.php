<?php
	/*error_reporting(E_ALL);
	ini_set('display_errors', 1);*/
	defined('_JEXEC') or die();
	
	$doc = JFactory::getDocument();

	include_once JPATH_LIBRARIES.'/fof/include.php';
	require_once JPATH_COMPONENT . '/helpers/reports.php';
	
	if(!defined('FOF_INCLUDED')) {
		JError::raiseError ('500', 'FOF is not installed');
		return;
	}

	FOFDispatcher::getTmpInstance('com_reports')->dispatch();