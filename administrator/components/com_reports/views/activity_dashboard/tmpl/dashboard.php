<style>
	#toolbar, .page-title {
		display: none;
	}
    .activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    #toolbar {
        color: #777;
        font-size: 18px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
</style>
<?php
    //error_reporting(E_ALL);
	//ini_set('display_errors', 1);

    $dashboard = $this->item;
    $params = json_decode($dashboard->params);
    $dateRange  = $params->default_range;
    $courseList = $params->courses;
    $dataPoints = explode(',',$params->data_points);
    $charts = explode(',',$params->charts);
    $theme = $params->theme;
    $params->users = AxsActivityData::getUserList($params);

    switch($dateRange) {
            case 'all' :
                $rangeText = "All Time";
            break;
            case 'day' :
                $rangeText = "Today";
            break;
            case 'week' :
                $start = date('m/d/Y',strtotime('-1 week'));
                $end   = date('m/d/Y',strtotime('today'));
                $rangeText = "$start - $end";
            break;
            case 'month' :
                $start = date('m/d/Y',strtotime('-1 month'));
                $end   = date('m/d/Y',strtotime('today'));
                $rangeText = "$start - $end";
            break;
            case 'year' :
                $start = date('m/d/Y',strtotime('-1 year'));
                $end   = date('m/d/Y',strtotime('today'));
                $rangeText = "$start - $end";
            break;
        }


    $courseActivity = AxsActivityData::getCoursesStats($params);
    $certificateActivity = AxsActivityData::getCertificateStats($params);
    $seriesCertificatesArray = array();
    $seriesArray = array();
    $seriesCompletionArray = array();


    if(in_array('courses_activity',$charts)) {
        if(in_array('courses_completion',$charts)) {
            $courseChartStyle = 'col-md-6';
        } else {
            $courseChartStyle = 'col-md-12';
        }
        if(in_array('registrations',$dataPoints)) {
            $seriesCoursesRegistrations = new stdClass();
            $seriesCoursesRegistrations->name = "Courses Registrations";
            $seriesCoursesRegistrations->data = $courseActivity->coursesRegistrations;
            $seriesArray[] = $seriesCoursesRegistrations;
        }

        if(in_array('completions',$dataPoints)) {
            $seriesCoursesCompletions = new stdClass();
            $seriesCoursesCompletions->name = "Course Completion";
            $seriesCoursesCompletions->data = $courseActivity->coursesCompletions;
            $seriesArray[] = $seriesCoursesCompletions;
        }

        if(in_array('last_activity',$dataPoints)) {
            $seriesCoursesLastActivity = new stdClass();
            $seriesCoursesLastActivity->name = "Last Activity";
            $seriesCoursesLastActivity->data = $courseActivity->coursesLastActivity;
            $seriesArray[] = $seriesCoursesLastActivity;
        }
    }

    if(in_array('courses_completion',$charts)) {
        if(in_array('courses_activity',$charts)) {
            $courseCompletionChartStyle = 'col-md-6';
        } else {
            $courseCompletionChartStyle = 'col-md-12';
        }
        $seriesCoursesAverageCompletions = new stdClass();
        $seriesCoursesAverageCompletions->name = "Average Completion";
        $seriesCoursesAverageCompletions->data = $courseActivity->coursesAverageCompletions;
        $seriesCompletionArray[] = $seriesCoursesAverageCompletions;
    }

    if(in_array('assignments',$charts)) {
        if(in_array('certificates',$charts)) {
            $assignmentsChartStyle = 'col-md-6';
        } else {
            $assignmentsChartStyle = 'col-md-12';
        }
        $assignmentActivity = AxsActivityData::getAssignmentStats($params);
        $seriesAssignmentsArray = array();
        $seriesAssignmentsAverageCompletions = new stdClass();
        $seriesAssignmentsAverageCompletions->name = "Average Completion";
        $seriesAssignmentsAverageCompletions->data = $assignmentActivity->assignmentsAverageCompletions;
        $seriesAssignmentsArray[] = $seriesAssignmentsAverageCompletions;
    }


    if(in_array('certificates',$charts)) {
        if(in_array('assignments',$charts)) {
            $certificatesChartStyle = 'col-md-6';
        } else {
            $certificatesChartStyle = 'col-md-12';
        }
        if(in_array('certificates_awarded',$dataPoints)) {
            $seriesCertificatesAwarded = new stdClass();
            $seriesCertificatesAwarded->name  = "Awarded";
            $seriesCertificatesAwarded->data  = $certificateActivity->awarded;
            $seriesCertificatesArray[] = $seriesCertificatesAwarded;
        }

        if(in_array('certificates_expired',$dataPoints)) {
            $seriesCertificatesExpired = new stdClass();
            $seriesCertificatesExpired->name  = "Expired";
            $seriesCertificatesExpired->data  = $certificateActivity->expired;
            $seriesCertificatesArray[] = $seriesCertificatesExpired;
        }
    }

    if ($params->users != null) {
        $userTotal = AxsActivityData::getUserTotal($params);
        $courseCompletionTotal = array_sum($courseActivity->coursesCompletions);
        $certificateTotal  = array_sum($certificateActivity->awarded);
        $loginTotal = AxsActivityData::getLoginTotal($params);
    } else {
        // If params->users is null then a usergroup with no users is selected,
        // set these to 0, if we call the libfunctions above with null users then all users will be returned
        $userTotal = 0;
        $courseCompletionTotal = 0;
        $certificateTotal  = 0;
        $loginTotal = 0;
    }


?>
<script src="/media/kendo/js/jquery.js"></script>
<script>
    $('.page-title').html('<i class="lizicon-chart"></i> Activity Dashboard: <?php echo  AxsSecurity::alphaNumericOnly($this->item->name); ?>').show();
    $('#toolbar').html('<i class="lizicon-calendar"></i> Stats from: <?php echo $rangeText; ?>').show();
</script>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/dashboard.css?v=3" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/data_box.css" />

<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<div class="container-fluid">
    <div id="dashboard_view" style='padding: 10px;'>
        <div class="row totals">
            <div class="col-md-3 totals-lifeboards">
                <div class='stats_bar shadowed' style="background-color: rgb(67, 192, 250);">
                    <div class="totals-inner">
                        <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-users"></span></div>
                        <div class="totals-data" id="userTotal"><?php echo $userTotal; ?></div>
                        <div class="totals-footer">Total Users</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 totals-lifeboards">
                <div class='stats_bar shadowed' style="background-color: rgb(16, 196, 105);">
                    <div class="totals-inner">
                        <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-school"></span></div>
                        <div class="totals-data" id="courseCompletionTotal"><?php echo $courseCompletionTotal; ?></div>
                        <div class="totals-footer">Total Course Completions</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 totals-lifeboards">
                <div class='stats_bar shadowed' style="background-color: rgb(255, 189, 74);">
                    <div class="totals-inner">
                        <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-image2"></span></div>
                        <div class="totals-data" id="certificateTotal"><?php echo $certificateTotal; ?></div>
                        <div class="totals-footer">Total Certificates Awarded</div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 totals-lifeboards">
                <div class='stats_bar shadowed' style="background-color: rgb(240, 80, 80);">
                    <div class="totals-inner">
                        <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-verified_user"></span></div>
                        <div class="totals-data" id="loginTotal"><?php echo $loginTotal; ?></div>
                        <div class="totals-footer">Total Logins</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <?php if(in_array('courses_activity',$charts)) { ?>
        <div id="course_activity_chart" class="<?php echo $courseChartStyle; ?> activity-chart"></div>
        <?php } ?>

        <?php if(in_array('courses_completion',$charts)) { ?>
        <div id="course_average_completion" class="<?php echo $courseCompletionChartStyle; ?> activity-chart"></div>
        <?php } ?>

        <?php if(in_array('certificates',$charts)) { ?>
        <div id="certificate_activity_chart" class="<?php echo $certificatesChartStyle; ?> activity-chart"></div>
        <?php } ?>

        <?php if(in_array('assignments',$charts)) { ?>
        <div id="assignment_average_completion" class="<?php echo $assignmentsChartStyle; ?> activity-chart"></div>
        <?php } ?>
    </div>
</div>
<script>
    function truncateLabels(value) {
        if (value.length > 5) {
            value = value.substring(0, 12);
            return value + "...";
        }
    }
    <?php if(in_array('courses_activity',$charts)) { ?>
    function createCourseActivityChart() {
        $("#course_activity_chart").kendoChart({
            theme: "<?php echo $theme; ?>",
            title: {
                text: "Course Activity"
            },
            legend: {
                position: "top"
            },
            seriesDefaults: {
                type: "column"
            },
            series: <?php echo json_encode($seriesArray); ?>,
            valueAxis: {
                labels: {
                },
                line: {
                    visible: false
                },
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: <?php echo json_encode($courseActivity->coursesTitles); ?>,
                line: {
                    visible: false
                },
                labels: {
                    padding: {right: 80},
                    rotation: 45,
                    visual: function (e) {
                        var labelVisual = e.createVisual();
                        labelVisual.options.tooltip = {
                            content: e.value
                        };
                        return labelVisual;
                    },
                    template: "#= truncateLabels(value)#"
                }
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= value #"
            }
        }).data("kendoChart").resize();
    }

    createCourseActivityChart();
    <?php } ?>

    <?php if(in_array('courses_completion',$charts)) { ?>
    function createCourseAverageCompletionChart() {
        $("#course_average_completion").kendoChart({
            theme: "<?php echo $theme; ?>",
            title: {
                text: "Course Average Completion Rate"
            },
            legend: {
                position: "top"
            },
            seriesDefaults: {
                type: "column"
            },
            series: <?php echo json_encode($seriesCompletionArray); ?>,
            valueAxis: {
                labels: {
                    format: "{0}%"
                },
                line: {
                    visible: false
                },
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: <?php echo json_encode($courseActivity->coursesTitles); ?>,
                line: {
                    visible: false
                },
                labels: {
                    padding: {right: 80},
                    rotation: 45,
                    visual: function (e) {
                        var labelVisual = e.createVisual();
                        labelVisual.options.tooltip = {
                            content: e.value
                        };
                        return labelVisual;
                    },
                    template: "#= truncateLabels(value)#"
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #%"
            }
        });
    }

    createCourseAverageCompletionChart();
    <?php } ?>

    <?php if(in_array('certificates',$charts)) { ?>
    function createCertificatesActivityChart() {
        $("#certificate_activity_chart").kendoChart({
            theme: "<?php echo $theme; ?>",
            title: {
                text: "Certificates"
            },
            legend: {
                position: "top"
            },
            seriesDefaults: {
                type: "column"
            },
            series: <?php echo json_encode($seriesCertificatesArray); ?>,
            valueAxis: {
                labels: {
                },
                line: {
                    visible: false
                },
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: <?php echo json_encode($certificateActivity->certificatesTitles); ?>,
                line: {
                    visible: false
                },
                labels: {
                    padding: {right:80},
                    rotation: 45,
                    visual: function (e) {
                        var labelVisual = e.createVisual();
                        labelVisual.options.tooltip = {
                            content: e.value
                        };
                        return labelVisual;
                    },
                    template: "#= truncateLabels(value)#"
                }
            },
            tooltip: {
                visible: true,
                template: "#= series.name #: #= value #"
            }
        }).data("kendoChart").resize();
    }

    createCertificatesActivityChart();
    <?php } ?>

    <?php if(in_array('assignments',$charts)) { ?>
    function createAssignmentAverageCompletionChart() {
        $("#assignment_average_completion").kendoChart({
            theme: "<?php echo $theme; ?>",
            title: {
                text: "Assignment Average Completion Rate"
            },
            legend: {
                position: "top"
            },
            seriesDefaults: {
                type: "column"
            },
            series: <?php echo json_encode($seriesAssignmentsArray); ?>,
            valueAxis: {
                labels: {
                    format: "{0}%"
                },
                line: {
                    visible: false
                },
                axisCrossingValue: 0
            },
            categoryAxis: {
                categories: <?php echo json_encode($assignmentActivity->assignmentsTitles); ?>,
                line: {
                    visible: false
                },
                labels: {
                    padding: {right: 80},
                    rotation: 45,
                    visual: function (e) {
                        var labelVisual = e.createVisual();
                        labelVisual.options.tooltip = {
                            content: e.value
                        };
                        return labelVisual;
                    },
                    template: "#= truncateLabels(value)#"
                }
            },
            tooltip: {
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #%"
            }
        });
    }

    createAssignmentAverageCompletionChart();
    <?php } ?>
</script>