<style>
	#toolbar {
		display: none;
	}
</style>
<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	$report = $this->item;
	$type = $report->report_type;
	$model = $this->getModel();
	$data = $model->getData($report);
	$params = json_decode($report->params);
	$layout = $params->report_layout;
	$theme = $params->report_theme;
	$brand = AxsBrands::getBrand();
	$currency_code = 'USD';
	$currency_symbol = '$';
	if($brand->billing->currency_code) {
		$currency_code = $brand->billing->currency_code;
	}
	if($brand->billing->currency_symbol) {
		$currency_symbol = $brand->billing->currency_symbol;
	}
?>
<script>
	var currency_symbol = '<?php echo $currency_symbol; ?>';
</script>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />
<link rel="stylesheet" href="components/com_reports/assets/css/reports.css" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/dashboard.css?v=2" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/data_box.css" />

<script src="/media/kendo/js/jquery.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<script src="components/com_reports/assets/js/reports.js"></script>

<script>
	var report_data = <?php echo json_encode($report); ?>;
	var report_type = "<?php echo $layout; ?>";
</script>

<style>
	#spreadsheet,#grid {
		width: 100%;
		height: 700px;
	}
	.activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
</style>
<div class="row mt-2 p-2" id="report_tools">
	<div class="col-auto">
		<div class="input-group">
			<select id="load_select" class="form-control" style="width:250px;"></select>
			<button
				id="load_view"
				class="btn btn-primary disabled"
				title="Load a spreadsheet from the database."
				type="button"
			>
				Load
			</button>
			<button
				id="delete_view"
				class="btn btn-danger disabled"
				title="Delete the currently loaded spreadsheet from the database."
				type="button"
			>
				Delete
			</button>
		</div>
	</div>
	<div class="col-auto">
		<div class="input-group">
			<input
				id="save_as_name"
				class="form-control"
				placeholder="My new spreadsheet"
				style="margin-left: 20px;"
			/>
			<div
				id="save_as_view"
				class="btn btn-primary disabled"
				title="Save the current spreadsheet as a new spreadsheet."
			>
				Save As
			</div>
		</div>
	</div>
	<div class="col-auto">
		<div
			id="save_view"
			class="btn btn-primary disabled"
			style="margin-left: 20px;"
			title="Save changes to the currently loaded spreadsheet."
		>
			Update
		</div>
	</div>
	<div class="col-auto">
		<a class="btn btn-light" href="index.php?option=com_reports&view=report&task=edit&id=<?php echo $report->id ?>">
			<span class="icon-cancel"></span>
			Close Report
		</a>
	</div>

	<?php if (false) { ?>
		<div class="col-auto">
			<div id="view_selection" <?php /* class="btn-group" */ ?> data-bs-toggle="buttons" style="float: right">
				<label class="view_selection_button btn btn-primary activated">
					<input type="radio" name="grid_type" value="grid" checked>
						Grid
					</input>
				</label>

				<label class="view_selection_button btn btn-primary">
					<input type="radio" name="grid_type" value="spreadsheet">
						Spreadsheet
					</input>
				</label>
			</div>
		</div>
	<?php } ?>
</div>

<style>
	/* Used to fix bootstrap issue */
	.activated {
		background-image: 	none;
		background-color: 	#3276b1;
		border-color: 		#285e8e;
		outline-color: 		initial;
		outline-style: 		initial;
		outline-width: 		0px;
		box-shadow: 		inset 0 3px 5px rgba(0,0,0,0.125);
	}
	#activity-box {
        overflow-y: auto !important;
    }
	td {
		white-space: pre-line;
	}
</style>

<br>
<div id="activity-box"></div>
<div style="width:100%; height: 10px;"></div>

<?php
	if ($layout == 'spreadsheet') {

		if ($layout == 'spreadsheet') {
			$json = $data;
		} else {
			$json = '{"rows":2000}';
		}
?>
		<div id="spreadsheet" <?php if ($layout == 'both') { echo ' style="display:none;"'; } ?>></div>
		<script>
		    $(function() {
		        $("#spreadsheet").kendoSpreadsheet(<?php echo $json; ?>);
		    });
		</script>

<?php
	}

	if ($layout == 'grid' || !$layout || $layout == 'both') {
		//$url = "index.php?option=com_reports&task=get_data&report=".$report->id."&format=raw";
		$dataType = "json";
		$columns = $data->grid->columns;
		$aggregates = $data->grid->aggregates;
		$schema = $data->grid->schema;
		$dataItems = $data->grid->items;
		if(!$dataItems) {
			$dataItems = '[]';
		}

?>
		 <script id="btn-template" type="text/x-kendo-template">
            <div class="column-editor-wrapper">
                <button class="k-button k-button-icontext k-grid-EditColumns">
                <span class="k-icon k-i-cog"></span>
                Edit Columns
                </button>
                <div id="column-editor"></div>
            </div>
        </script>

		<div id="grid"></div>

		<script>
			function createNewLines(text) {
				return text;
			}
			var report = '<?php echo rawurlencode(json_encode($report)); ?>';
			var reportColumns = <?php echo empty($columns) ? '""' : $columns; ?>;
			<?php /*
			var aggregates = <?php echo $aggregates; ?>;
			var schema = <?php echo $schema; ?>;
			*/ ?>

			$(document).ready(
				function () {
					$('#toolbar').html($('#report_tools')).show();
				    var fileName = "export_report";

				    $("#grid").kendoGrid({
				    	toolbar: [
				    		{
	                            name: "excel"
	                        },
	                        {
	                            name: "pdf"
	                        },
	                        {
	                            name: "Select All"
	                        },
	                        {
	                        	name: "EditColumn",
	                        	template: kendo.template($("#btn-template").html())
	                        }
				    	],
				        excel: {
				            fileName: fileName+".xlsx",
				            filterable: true,
				            allPages: true
				        },
				        pdf: {
				            fileName: fileName+".pdf",
				            filterable: true,
				            allPages: true
				        },
				        dataSource: {
				            data: <?php echo $dataItems; ?>,
				            <?php
				            	if ($aggregates) {
				            		echo "aggregate: " . $aggregates . ",";
				            	}
				            	if ($schema) {
				            		echo "schema: " . $schema . ",";
				            	}
				            ?>

				        },
				        selectable: "multiple cell",
				        pageable: {
				        	refresh: true,
				            buttonCount: 10,
							pageSize: 100,
							pageSizes:[100,200,500,1000,5000,'all'],
				        },

				        sortable: true,
				        groupable: true,
				        filterable: {
	                        mode: "row"
	                    },
				        columnMenu: true,
				        reorderable: true,
				        resizable: true,
				        allowCopy: true,
				        columns: reportColumns,
				        columnMenuInit: function (e) {
	                        var menu = e.container.find(".k-menu").data("kendoMenu");
	                        var field = e.field;
	                        menu.append({
	                           text: "Edit Column",
	                           spriteCssClass: "k-icon k-i-cog"
	                        });
	                         menu.bind("select", function(e) {
	                          if ($(e.item).text() == "Edit Column") {
	                            var column = getColumnIndex(field);
	                            var html = editColumnForm(column);
	                            $('#column-editor').html(html);
	                            $('#column-editor').slideToggle();
	                          }
	                        });
	                    },
						excelExport: function(e) {
							var sheet = e.workbook.sheets[0];
							for (var rowIndex = 0; rowIndex < sheet.rows.length; rowIndex++) {
								var row = sheet.rows[rowIndex];
								for (var cellIndex = 0; cellIndex < row.cells.length; cellIndex ++) {
									var cell =row.cells[cellIndex];
									if(cell.value && cell.value.toString().indexOf("<br>") >= 0) {
										cell.value = cell.value.split("<br>").join("\n");
										cell.wrap = true;
									}
									if(cell.value && cell.value.toString().indexOf("<b>") >= 0) {
										cell.value = cell.value.split("<b>").join("");
										cell.value = cell.value.split("</b>").join("");
										cell.wrap = true;
									}
								}
							}
						}

				    });

				    $(".clearSelection").click(function () {
				        $("#grid").data("kendoGrid").clearSelection();
				    });

				    var selectRow = function (e) {
				        if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
				            var grid = $("#grid").data("kendoGrid"),
				                    rowIndex = $("#selectRow").val(),
				                    row = grid.tbody.find(">tr:not(.k-grouping-row)").eq(rowIndex);

				            grid.select(row);
				        }
				    },
				        toggleGroup = function (e) {
				            if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
				                var grid = $("#grid").data("kendoGrid"),
				                    rowIndex = $("#groupRow").val(),
				                    row = grid.tbody.find(">tr.k-grouping-row").eq(rowIndex);

				                if (row.has(".k-i-collapse").length) {
				                    grid.collapseGroup(row);
				                } else {
				                    grid.expandGroup(row);
				                }
				            }
				        };


				    $(".selectRow").click(selectRow);
				    $("#selectRow").keypress(selectRow);

				    $(".toggleGroup").click(toggleGroup);
				    $("#groupRow").keypress(toggleGroup);

				    $("#view_selection").change(
		                function() {
		                    var spreadsheet = $("#spreadsheet");
		                    var grid = $("#grid");

		                    var type = $('input[name="grid_type"]:checked').val();


		                    switch (type) {
		                        case "grid":
		                            grid.show();
		                            spreadsheet.hide();
		                            break;
		                        case "spreadsheet":
		                            grid.hide();
		                            spreadsheet.show();
		                            break;
		                    }

		                    //Without a refresh, the cells in the excel sheet will not be rendered properly.
		                    window.dispatchEvent(new Event('resize'));
		                }
		            );

				    $(document).on('click','.k-grid-SelectAll',
		                function() {
		                    var grid = document.getElementById("grid");
		                    var cells = grid.getElementsByTagName("td");

		                    $(cells).each(
		                        function() {
		                            $(this).addClass('k-state-selected');
		                        }
		                    );

		                }
					);

					<?php if($type == 'education' && ($params->education_report_type == 'survey_summary' || $params->education_report_type == 'team_summary')) { ?>
					var activity = $('#activity-box');
					activity.kendoDialog({
						width: "700px",
						height: "600px",
						title: "Student Activity",
						closable: false,
						modal: true,
						actions: [
							{
								text: 'Close',
								action: closeActivity ,
								primary: true
							}
						]
					});
					activity.data("kendoDialog").close();
					$(document).on('click', '.getSubmission', function(){
						var student_response = $(this).data('download');
						var activity_id      = $(this).data('id');
						var activity_type    = $(this).data('type');
						var user             = $(this).data('user');
						$.ajax({
							url: '/index.php?option=com_splms&task=studentactivities.getSubmission&format=raw',
							type: 'post',
							data: {
								student_response: student_response,
								activity_type: activity_type,
								user: user,
								activity_id: 'report'
							}
						}).done(function(response) {
							$('#activity-box').html(response);
							$('#activity-box').find('iframe').css('height','360');
							activity.data("kendoDialog").open();
						});
					});

					function closeActivity() {
						$('#activity-box').html('');
					}
				<?php } ?>
				}
			);
		</script>

<?php
	}
?>