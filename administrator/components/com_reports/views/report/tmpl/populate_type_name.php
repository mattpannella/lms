<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

/*
1		74023
2		20887
3 		0
4 		0
5 		69
6 		0
*/

$db = JFactory::getDBO();

$query = "SELECT * FROM axs_pay_transactions";

$db->setQuery($query);

$results = $db->loadObjectList();

$total = count($results);
$count = 0;

foreach ($results as $result) {
	echo (++$count . " / " . $total);
	$type = $result->type;
	$type_id = $result->type_id;
	$id = $result->id;
	if (isset($result->type_name)) {
		$type_name = $result->type_name;
	} else {
		$type_name = null;
	}

	if ($type_name != null && $type_name != "") {
		continue;
	}

	$query = $db->getQuery(true);

	switch ($type) {
		case 1: //courses

			$query
				->select('title')
				->from($db->quoteName('joom_splms_courses', 'course'))
				->join('INNER', $db->quoteName('axs_course_purchases', 'purchase'))
				->where(array(
						$db->quoteName('course.splms_course_id') . '=' . $db->quoteName('purchase.course_id'),
						$db->quoteName('purchase.id') . '=' . $db->quote($type_id)
					));

			//$query = "SELECT title FROM joom_splms_courses AS a JOIN axs_course_purchases AS b WHERE a.splms_course_id = b.course_id AND b.id = $type_id";
			$db->setQuery($query);
			$name = $db->loadObject()->title;
			
			break;
		case 2: //subscription
			//$query = "SELECT title FROM axs_pay_subscription_plans AS a JOIN axs_pay_subscriptions AS b WHERE a.id = b.plan_id AND b.id = $type_id";

			$query
				->select('title')
				->from($db->quoteName('axs_pay_subscription_plans', 'plan'))
				->join('INNER', $db->quoteName('axs_pay_subscriptions', 'sub'))
				->where(array(
						$db->quoteName('plan.id') . '=' . $db->quoteName('sub.plan_id'),
						$db->quoteName('sub.id') . '=' . $db->quote($type_id)
					));
			$db->setQuery($query);
			$name = $db->loadObject()->title;
			break;
		case 3: //event
			//$query = "SELECT title FROM joom_eb_events AS a JOIN joom_eb_registrants AS b WHERE a.id = b.event_id AND b.id = $type_id";
			
			$query
				->select('title')
				->from($db->quoteName('joom_eb_events', 'event'))
				->join('INNER', $db->quoteName('joom_eb_registrants', 'reg'))
				->where(array(
						$db->quoteName('event.id') . '=' . $db->quoteName('reg.event_id'),
						$db->quoteName('reg.id') . '=' . $db->quote($type_id)
					));
			$db->setQuery($query);
			$name = $db->loadObject()->title;
			break;
		case 4: //payment_plan			
			$name = "Payment Plan";
			break;
		case 5: //failed_signup
			$name = "Failed Signup";
			break;
		case 6: //e-commerce
			//$query = "SELECT product_name FROM joom_eshop_orderproducts AS a JOIN joom_eshop_orders AS b WHERE a.order_id = b.id AND b.id = $type_id";
			
			$query
				->select('product_name')
				->from($db->quoteName('joom_eshop_orderproducts', 'prod'))
				->join('INNER', $db->quoteName('joom_eshop_orders', 'order'))
				->where(array(
						$db->quoteName('prod.order_id') . '=' . $db->quoteName('order.id'),
						$db->quoteName('order.id') . '=' . $db->quote($type_id)
					));
			$db->setQuery($query);
			$name = $db->loadObject()->product_name;

			break;
	}

	$query->clear();

	$query
		->update($db->quoteName('axs_pay_transactions'))	
		->set($db->quoteName('type_name') . '=' . $db->quote($name))
		->where($db->quoteName('id') . '=' . $db->quote($id));

	//$query = "UPDATE axs_pay_transactions SET type_name = '$name' WHERE id=$id";
	$db->setQuery($query);
	$db->execute();

}

