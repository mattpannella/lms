<style>
	#toolbar, .page-title {
		display: none;
	}
    .activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    #toolbar {
        color: #777;
        font-size: 18px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
    #loading_icon {
        font-size: 30px;
        color: #007aff;
        width: 100%;
        padding-top: 150px;
        text-align: center;
    }
</style>
<?php
    /* error_reporting(E_ALL);
	ini_set('display_errors', 1); */

    $report = $this->item;
    $params = json_decode($report->params);
    $theme = $params->theme;
    $start_date  = $report->start_date;
    $end_date    = $report->end_date;
    $rangeText   = '';
    if($start_date > 0) {
        $start = date('m/d/Y',strtotime($start_date));
        $rangeText .= "Start: ".$start;
    }
    if($end_date > 0) {
        $end = date('m/d/Y',strtotime($end_date));
        $rangeText .= " End: ".$end;
    }

    $expirationObject = new AxsContentExpiration($expirationData);
    $params->start_date = $start_date;
    $params->end_date   = $end_date;
    $data = $expirationObject->getAllRows($params);

    if(!$data) {
        $date = '[]';
    } else {
        $data = json_encode($data);
    }
?>
<script src="/media/kendo/js/jquery.js"></script>
<script>
    $('.page-title').html('<i class="lizicon-chart"></i> Expiration Report: <?php echo  AxsSecurity::alphaNumericOnly($this->item->name); ?>').show();
    $('#toolbar').html('<i class="lizicon-calendar"></i> <?php echo $rangeText; ?>').show();
</script>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/dashboard.css?v=2" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/data_box.css" />

<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<div class="container-fluid">
    <div id="loading_icon">
        <span class="fa fa-spinner fa-spin"></span> Loading...
    </div>
    <div id="activity_report"></div>

<script>
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100,200,500,1000],
            refresh:        true,
            message: {
                empty:      "There are no entries to display"
            }
        },
        dataSource: {
            data:  <?php echo $data; ?>
        },
        columns: [
            {
                field: "content_type",
                title: "Content Type",
                filterable: {
                    multi: true,
                    search: true,
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "title",
                title: "Title",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "custom_id",
                title: "Custom Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "expiration_date",
                title: "Expiration Date",
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            }
        ]
    }

    var activity_report = $("#activity_report").kendoGrid(kendoOptions);
    var grid = activity_report.data("kendoGrid");
    $('#loading_icon').hide();
</script>