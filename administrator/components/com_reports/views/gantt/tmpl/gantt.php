<style>
	#toolbar {
		display: none;
	}
</style>
<?php
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	$gantt = $this->item;
	$params = json_decode($gantt->params);
	$layout = $params->chart_default;
	$theme = $params->report_theme;

	if(!$gantt->data) {
		$data = '[]';
	} else {
		$data = base64_decode($gantt->data);
	}

	if(!$gantt->data_dependencies) {
		$dataDependencies = '[]';
	} else {
		$dataDependencies = base64_decode($gantt->data_dependencies);
	}

    if($params->height) {
        $height = (int)$params->height;
    } else {
        $height = 700;
    }

?>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />

<script src="/media/kendo/js/jquery.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>

<script>
	var data = <?php echo $data; ?>;
	var dataDependencies = <?php echo $dataDependencies; ?>;
	var id =  <?php echo $gantt->id; ?>;	
</script>

<script id="btn-template" type="text/x-kendo-template">
        <button class="k-button k-button-icontext k-grid-save savebtn">
	        <span class="k-icon k-i-save"></span>
	        Save Chart
        </button>
</script>
<script id="close-btn-template" type="text/x-kendo-template">
        <a class="k-button k-button-icontext k-grid-close" href="index.php?option=com_reports&view=gantts">
	        <span class="k-icon k-i-close"></span>
	        Close Chart
        </a>
</script>

<div id="gantt"></div>

<script>          

    $(document).ready(function() {
        function onAdd(e) {
            if(e.task) {
                var gantt = e.sender;
                var selection = gantt.select();
                var dataItem = gantt.dataItem(selection);
                var data = $("#gantt").data().kendoGantt.dataSource.view();
                data.sort(function(a, b) { 
                  return a.id - b.id;
                });
                console.log(data);
                if(data.length >= 1) {
                    var index = data.length - 1;
                    e.task.id = data[index].id + 1;
                } else {
                    e.task.id = 1;
                }
            }
           
        }

         var tasksDataSource = new kendo.data.GanttDataSource({
            data: data
            
        });

        var dependenciesDataSource = new kendo.data.GanttDependencyDataSource({                      
                    data: dataDependencies
        });

        var gantt = $("#gantt").kendoGantt({
            dataSource: tasksDataSource,
           dependencies: dependenciesDataSource,
            views: [
                { type: "day", <?php if($layout == 'day') { echo 'selected: true'; } ?> },
                { type: "week", <?php if($layout == 'week') { echo 'selected: true'; } ?> },
                { type: "month", <?php if($layout == 'month') { echo 'selected: true'; } ?> },
                { type: "year", <?php if($layout == 'year') { echo 'selected: true'; } ?> }
            ],
            toolbar: [
            			"append", 
            			"pdf",
            			{
                        	name: "EditColumn",
                        	template: kendo.template($("#btn-template").html())
                        },
                        {
                        	name: "Close Chart",
                        	template: kendo.template($("#close-btn-template").html())
                        }
            		],
            pdf: {
                fileName: "<?php echo $gantt->name; ?>.pdf"
            },
            columns: [
                { field: "id", title: "ID", width: 60 },
                { field: "title", title: "Title", editable: true, sortable: true },
                { field: "start", title: "Start Time", format: "{0:MM/dd/yyyy}", width: 100, editable: true, sortable: true },
                { field: "end", title: "End Time", format: "{0:MM/dd/yyyy}", width: 100, editable: true, sortable: true }
            ],
            height: <?php echo $height; ?>,

            showWorkHours: false,
            showWorkDays: false,
            add: onAdd

        }).data("kendoGantt");

        $('.savebtn').click(function() {
            var data = $("#gantt").data().kendoGantt.dataSource.view();
            var dataAsJSON = JSON.stringify(data);            
            var dataEncoded = btoa(dataAsJSON);

            var dataDependencies = $("#gantt").data().kendoGantt.dependencies.view();
            var dataDependenciesAsJSON = JSON.stringify(dataDependencies);
            var dataDependenciesEncoded = btoa(dataDependenciesAsJSON);

            $.ajax({
            	url: '/administrator/index.php?option=com_reports&task=gantts.saveChart&format=raw',
            	type: 'post',
            	data: {
            		data_dependencies: dataDependenciesEncoded,
            		data: dataEncoded,
            		id: id

            	}
            }).done(function(response) {
            	if(response == 'success') {
            		alert('Your Chart was Saved.');
            	} else {
            		alert('There was an error saving your chart.');
            	}
            })
        });

    });
</script>