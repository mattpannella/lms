<style>
	#toolbar, .page-title {
		display: none;
	}
    .activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    #toolbar {
        color: #777;
        font-size: 18px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
    #loading_icon {
        font-size: 30px;
        color: #007aff;
        width: 100%;
        padding-top: 150px;
        text-align: center;
    }
</style>
<?php
    /* error_reporting(E_ALL);
	ini_set('display_errors', 1); */

    $report = $this->item;
    $params = json_decode($report->params);
    $params->users = AxsActivityData::getUserList($params);
    $start_date  = $report->start_date;
    $end_date  = $report->end_date;
    $rangeText = '';
    if($start_date > 0) {
        $start = date('m/d/Y',strtotime($start_date));
        $rangeText .= "Start: ".$start;
    }
    if($end_date > 0) {
        $end = date('m/d/Y',strtotime($end_date));
        $rangeText .= " End: ".$end;
    }
    $courseList = $params->courses;
    $dataPoints = explode(',',$params->data_points);
    $theme = $params->theme;
    $defaultDuration = 60;
    if($params->default_duration) {
        $defaultDuration = $params->default_duration;
    }
    $data = array();

    if(in_array('Site Visit Duration',$dataPoints)) {
        $siteVisitDuration = AxsActivityData::getActivitySiteVisitDuration($params, $start_date, $end_date, $defaultDuration);
        $data = array_merge($data,$siteVisitDuration);
    }
    if(in_array('Active Time on Site',$dataPoints)) {
        $newSiteVisitDuration = AxsActivityData::getActivityNewSiteVisitDuration($params, $start_date, $end_date);
        $data = array_merge($data,$newSiteVisitDuration);
    }
    if(in_array('Course Started',$dataPoints)) {
        $courseStarted = AxsActivityData::getActivityCourseStarted($params, $start_date, $end_date);
        $data = array_merge($data,$courseStarted);
    }
    if(in_array('Course Completed',$dataPoints)) {
        $courseCompleted = AxsActivityData::getActivityCourseCompleted($params, $start_date, $end_date);
        $data = array_merge($data,$courseCompleted);
    }
    if(in_array('Lesson Started',$dataPoints)) {
        $lessonStarted = AxsActivityData::getActivityLessonStarted($params, $start_date, $end_date);
        $data = array_merge($data,$lessonStarted);
    }
    if(in_array('Lesson Completed',$dataPoints)) {
        $lessonCompleted = AxsActivityData::getActivityLessonCompleted($params, $start_date, $end_date);
        $data = array_merge($data,$lessonCompleted);
    }
    if(in_array('Certificate Awarded',$dataPoints)) {
        $certificateAwarded = AxsActivityData::getActivityCertificateAwarded($params, $start_date, $end_date);
        $data = array_merge($data,$certificateAwarded);
    }
    if(in_array('Badge Awarded',$dataPoints)) {
        $badgeAwarded = AxsActivityData::getActivityBadgeAwarded($params, $start_date, $end_date);
        $data = array_merge($data,$badgeAwarded);
    }
    if(in_array('Milestone Achieved',$dataPoints)) {
        $milestoneAchieved = AxsActivityData::getActivityMilestoneAchieved($params, $start_date, $end_date);
        $data = array_merge($data,$milestoneAchieved);
    }
    if(in_array('Lesson Activity Submitted',$dataPoints)) {
        $activitySubmitted = AxsActivityData::getActivityLessonActivitySubmitted($params, $start_date, $end_date);
        $data = array_merge($data,$activitySubmitted);
    }
    if(in_array('Watched Video',$dataPoints)) {
        $watchedVideo = AxsActivityData::getActivityWatchedVideo($params, $start_date, $end_date);
        $data = array_merge($data,$watchedVideo);
    }
    if(in_array('Event Registration',$dataPoints)) {
        $eventRegistration = AxsActivityData::getActivityEventRegistration($params, $start_date, $end_date);
        $data = array_merge($data,$eventRegistration);
    }
    if(in_array('Quiz or Survey Taken',$dataPoints)) {
        $quizTaken = AxsActivityData::getActivityQuizTaken($params, $start_date, $end_date);
        $data = array_merge($data,$quizTaken);
    }

    usort($data, function($a, $b) {
        return strtotime($a->date) - strtotime($b->date);
    });

    if(!$data) {
        $data = '[]';
    } else {
        $data = json_encode($data);
    }
?>
<script src="/media/kendo/js/jquery.js"></script>
<script>
    $('.page-title').html('<i class="lizicon-chart"></i> Activity Report: <?php echo  AxsSecurity::alphaNumericOnly($this->item->name); ?>').show();
    $('#toolbar').html('<i class="lizicon-calendar"></i> <?php echo $rangeText; ?>').show();
</script>
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/dashboard.css" />
<link rel="stylesheet" href="components/com_dashboard/assets/css/data_box.css" />

<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<div class="container-fluid">
    <div id="loading_icon">
        <span class="fa fa-spinner fa-spin"></span> Loading...
    </div>
    <div id="activity_report"></div>

<script>
    var reportData = <?php echo $data; ?>;
    var exportingFlag = false;

    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100,200,500,1000,5000,'all'],
            refresh:        true,
            message: {
                empty:      "There are no entries to display"
            }
        },
        excelExport: function(event) {

            var sheet = event.workbook.sheets[0];
            var grid = event.sender;

            if (!exportingFlag) {
                grid.hideColumn('short_date');
                grid.showColumn('date');

                event.preventDefault();
                exportingFlag = true;

                setTimeout(function () {
                    grid.saveAsExcel();

                    grid.showColumn('short_date');
                    grid.hideColumn('date');
                });
            } else {
                exportingFlag = false;
            }

            // Cycle through each row and column and replace html tags
            sheet.rows.forEach(row => {
                row.cells.forEach(cell => {
                    if (cell && cell.value) {
                        if(cell.value.toString().indexOf("<br>") >= 0){
                            cell.value = cell.value.replaceAll("<br>", "\n");
                            cell.wrap = true;
                        }
                        if(cell.value.toString().indexOf("<br/>") >= 0){
                            cell.value = cell.value.replaceAll("<br/>", "\n");
                            cell.wrap = true;
                        }
                        if(cell.value.toString().indexOf("<b>") >= 0){
                            cell.value = cell.value.replaceAll("<b>", "");
                            cell.value = cell.value.replaceAll("</b>", "");
                            cell.wrap = true;
                        }
                    }
                });
            });

            // Get the column index of any date type columns,
            // they need to have a format stored alongside the value
            var fields = grid.dataSource.options.fields;
            var columns = grid.columns;
            var dateCells = [];

            fields
                .filter(f => f.field == "date")
                .forEach((field) => {
                    dateCells.push(columns.indexOf(columns.find(c => c.field == field.field)));
                });

            // Update any data type columns with a field type of date
            // with a format
			sheet.rows
                .filter(c => c.type == 'data')
                .forEach((row, idx) => {
                    dateCells.forEach((dateCell, idx) => {
                        var cellIndex = dateCell - 1;
                        var value = row.cells[cellIndex].value;

                        row.cells[cellIndex].value = value;
                        row.cells[cellIndex].format = "mm/d/yyyy h:mm AM/PM";
                    });
                });

        },
        dataSource: {
            transport: {
                read: function(e) {
                    e.success(reportData);
                }
            },
            schema: {

                // Parse the given data set into a short date that is
                parse: function(data) {

                    var activities = [];

                    for(var i = 0; i < data.length; i++) {

                        var activity = data[i];
                        var longDate = kendo.parseDate(activity.date);

                        activity.short_date = kendo.toString(longDate, 'MM/d/yyyy');

                        activities.push(activity);
                    }

                    return activities;
                },

                model: {
                    id: 'user_id',

                    fields: {
                        user_id: { editable: false, nullable: false },
                        date: { type: "date" },
                        short_date: { type: "date" }
                    }
                }
            }
        },
        columns: [
            {
                field: "user_id",
                title: "User ID",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "name",
                title: "Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "activity_type",
                title: "Activity Type",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "activity_data",
                title: "Activity Data",
                encoded: false,
                filterable: {
                    cell: {
                        suggestionOperator: "contains",
                        template: function(templateArgs) {

                            templateArgs.element.kendoAutoComplete({
                                dataSource: templateArgs.dataSource,
                                dataTextField: "activity_data",
                                template: "#= activity_data #",
                                valuePrimitive: true,
                                filter: "contains",
                                placeholder: "Search Activity Data",

                                // We don't need the selected value to appear in the search field - as long as the
                                // report shows the correct data when filtered we should be OK here.
                                change: function(event) {
                                    event.sender.value('');
                                }
                            });
                        }
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                field: "short_date",
                title: "Date",
                type: "date",
                template: "#= kendo.toString(date, 'MM/d/yyyy h:mm tt zzz') #",
                filterable: {
                    ui: "datepicker"
                }
            },
            {
                field: "date",
                title: "Date",
                type: "date",
                format: "{0:MM/d/yyyy h:mm tt}",
                hidden: true
            },
            {
                field: "archive_date",
                title: "Archive Date",
                type: "date",
                format: "{0:MM/d/yyyy h:mm tt}",
                hidden: <?php echo $params->include_archived_activity ? 'false' : 'true' ?>
            }
        ]
    }

    var activity_report = $("#activity_report").kendoGrid(kendoOptions);
    var grid = activity_report.data("kendoGrid");
    $('#loading_icon').hide();
</script>