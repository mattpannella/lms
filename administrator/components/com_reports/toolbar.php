<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/26/16
 * Time: 11:47 AM
 */

defined('_JEXEC') or die;

class ReportsToolbar extends FOFToolbar {
	function onReportsEdit() {
		JToolBarHelper::title('Report Builder: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::custom("apply_and_redirect", "generic.png", '', "Run Report", false);
		JToolbarHelper::cancel();
	}
	
	function onReportsAdd() {
		JToolBarHelper::title('Report Builder: New');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::custom("apply_and_redirect", "generic.png", '', "Run Report", false);
		JToolbarHelper::cancel();
	}

	function onActivity_reportsEdit() {
		$report_name = 'Edit';
		$report_id = JRequest::getVar('id');
		$report_id = (int)$report_id;

		$db = JFactory::getDBO();
		$query = "SELECT name FROM axs_activity_reports WHERE id = $report_id";
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result){
			$report_name = $result->name;
		}

		JToolBarHelper::title(JText::_('COM_REPORTS_TITLE_ACTIVITY_REPORTS') . ": " . $report_name,'reports');		
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::cancel();
	}
}