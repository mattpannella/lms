<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.<?php echo $theme; ?>.mobile.css" />

<script src="/media/kendo/js/jquery.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<style> 
    #submissions_tab .control-label { 
        display:none; 
    } 
    #submissions_tab .controls {
        margin-left: 0px;
    }
</style>
<div id="activity-box"></div>
<table id="activitiesGrid" class="table table-bordered table-striped table-responsive">
							
    <tr>
        <?php
            foreach($fields as $field) {
                if ($field->show_in_report) {
                    echo '<th>'.$field->name.'</th>';
                }
            }
        ?>
        <th>Response</th>
        <th>Date</th>
	</tr>
	

    <?php
    
    	foreach ($submissions as $row) {
            $rowParams = json_decode($row->params);        	
        ?>
    <tr>
        <?php
            foreach($fields as $field) {
                if ($field->show_in_report) {
                    if ($field->is_image) {
                        $photo = $rowParams->{$field->parameter};
                        if(!$photo) {
                            $photo = "/images/user.png";
                        }
                        echo "<td><span  class='student-image' style='padding: 5px'><span style='margin: 0px 5px;'><img src='" . $photo . "' style='width: 50px;'></span></span></td>"; 
                    } else {
                        echo '<td>'.$rowParams->{$field->parameter}.'</td>';
                    }
                }                
            }
        ?>
        <td><?php echo $rowParams->response; ?></td>
    	<td><?php echo date("m/d/Y", strtotime($row->date)); ?></td>
	</tr>
            
    <?php    }
    ?>    
</table>

<script>
    var fileName = "<?php echo $report->name; ?>";
    if(!fileName) {
        fileName = "Report";
    }
    jQuery(document).ready(function () {        
        var kendoOptions = {
            toolbar: [
                { 
                    name: "excel"
                },
                { 
                    name: "pdf"
                }
            ],
            excel: {
                fileName: fileName+".xlsx",
                filterable: true,
                allPages: true
            },
            pdf: {
                fileName: fileName+".pdf",
                filterable: true,
                allPages: true
            },            
            editable: "popup",
            sortable: true,
            columnMenu: true,
            reorderable: true,
            resizable: true,
            groupable: true,
            filterable: {
                mode: "row"
            },
            height: 600,
            pageable: {
                buttonCount: 8,
                input: true,
                pageSize: 20,
                pageSizes: true,
                refresh: true,
                message: {
                    empty: "There are no entries to display"
                }
            }
        }

        
        var activitiesGrid = $("#activitiesGrid").kendoGrid(kendoOptions);
        var grid = activitiesGrid.data("kendoGrid");            
    });
</script>