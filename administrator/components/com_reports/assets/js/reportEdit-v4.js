var hidden_text = "This feature is only available for reports that are based on users.";

var use_custom_fields = null;
var report_type = null;
var events_report_type = null;
var store_report_type = null;
var custom_fields = null;
var hidden_div_use = null;
var hidden_div_custom = null;
var board_type = null;

jQuery(document).ready(
	function() {
		//var control_labels = document.getElementsByClassName('control-label');

		use_custom_fields = document.getElementById("use_custom_fields");
		report_type = document.getElementById("report_type");
		events_report_type = document.getElementById("events_report_type");
		education_report_type = document.getElementById("education_report_type");
		store_report_type = document.getElementById("store_report_type");
		custom_fields = document.getElementById("custom_field_list");
		board_type = document.getElementById("board_type");

		hidden_div_use = document.createElement("div");
		hidden_div_use.appendChild(document.createTextNode(hidden_text));

		hidden_div_custom = document.createElement("div");
		hidden_div_custom.appendChild(document.createTextNode(hidden_text));

		use_custom_fields.parentNode.appendChild(hidden_div_use);
		custom_fields.parentNode.appendChild(hidden_div_custom);

		jQuery(report_type).change(
			function() {
				check_custom_change();
			}
		);

		jQuery(events_report_type).change(
			function() {
				check_custom_change();
			}
		);

		jQuery(education_report_type).change(
			function() {
				check_custom_change();
			}
		);

		jQuery(board_type).change(
			function () {
				check_board_options();
			}
		);

		jQuery(store_report_type).change(
			function() {
				check_custom_change();
			}
		);

		check_custom_change();
		//check_board_options();
	}
);

function check_custom_change() {
	var show = true;
	var educationShow = ["progress","assigned","quiz","certificates","survey","survey_summary","team_summary","point_totals"];
	switch (report_type.value) {
		case "events":
			if (events_report_type.value == "events") {
				show = false;
			}
			break;
		case "store":
			if (store_report_type.value == "products") {
				show = false;
			}
			break;
		case "education":
			if (educationShow.includes(education_report_type.value) === false) {
				show = false;
			}
		break;
		case "external":
			show = false;
			break;
	}

	if (show) {
		jQuery(use_custom_fields).show(250);
		jQuery(custom_fields).show(250);

		jQuery(hidden_div_use).hide(250);
		jQuery(hidden_div_custom).hide(250);
	} else {

		jQuery(use_custom_fields).hide(250);
		jQuery(custom_fields).hide(250);

		jQuery(hidden_div_custom).show(250);
		jQuery(hidden_div_use).show(250);
	}

	// Show/hide the Include Archived Activity toggle depending on the education report type
	const disallowed_report_types = ['team_summary', 'certificates', 'assigned', 'point_totals'];
	if(disallowed_report_types.indexOf(education_report_type.value) != -1) {
		jQuery('#include_archived_activity-row').hide(250);
	} else {
		jQuery('#include_archived_activity-row').show(250);
	}
}

/*
function check_board_options() {

	var user_board_settings = document.getElementById("user_board_settings");
	var checkboxes = user_board_settings.getElementsByTagName("input");
	var options = board_type.options;
	var list = [];

	var all = false;
	for (let i = 0; i < options.length; i++) {
		//console.log(options[i].value + " " + options[i].selected);
		if (options[i].selected) {
			if (options[i].value == "all") {
				all = true;
				break;
			} else {
				list.push(options[i].value);
			}
		}
	}

	for (var i = 0; i < checkboxes.length; i++) {

		var options = checkboxes[i].className;
		var include = all;

		if (!include) {
			for (var j = 0; j < list.length; j++) {
				var check_value = "board_" + list[j];
				if (options.includes(check_value) || options.includes("board_all")) {
					include = true;
				}
			}
		}

		if (include) {
			jQuery(checkboxes[i].parentNode).show(250);
		} else {
			jQuery(checkboxes[i].parentNode).hide(250);
		}
	}
}
*/