jQuery(document).ready(function() {
    
    function buildCheckbox(key,path,label,index) {
        var indent = getIndent(path);
        var labelKeys = path.split(".");
        var randomID = Math.floor((Math.random() * 1000) + 1);
        if(label) {
            var boxes = `<div 
                            class="list-label" 
                            style="margin-left:`+(indent + 10)+`px;"
                            data-index="`+index+`"
                        >
                            `+labelKeys[labelKeys.length - 1]+`
                        </div>`;
        } else {
            var  boxes = `
                        <input 
                            type="checkbox" 
                            id="check-`+randomID+`" 
                            name="external_columns[]" 
                            value="`+path+`"
                            style="margin-left:`+indent+`px;"
                            data-index="`+index+`"
                            class="external_checkbox"
                        />
                            <span 
                                id="title-`+randomID+`"
                            >
                                `+key+`
                            </span> 
                            <a 
                                href="javascript:void(0)" 
                                class="editColumn" 
                                data-id="`+randomID+`"
                            >
                                edit
                            </a>
                            <input 
                                id="input-`+randomID+`"
                                style="display:none; margin-left:15px;"
                                class="form-control input-medium"
                                placeholder="New Title"
                                data-index="`+index+`"
                            />
                            <span 
                                id="btn-`+randomID+`"
                                style="display:none;"
                                class="btn btn-primary"
                            >
                                Save
                            </span>
                        <br/>`;
        }
        return boxes;
    }

    function runLoop(obj, k){
        var parent;
        for(var key in obj) {
            if(typeof(obj[key]) === "object") {
                var current = k;
                if(k) {
                    k = k+'.'+key; 
                } else {
                    k = key;
                }
                if(obj[key].length)  {
                    columns.push({'field':k, 'title':k, 'checked':'', 'type':'object', 'indent':getIndent(k)});
                    var boxes = buildCheckbox(k,k,false,(columns.length - 1));
                    jQuery('#external_columns').append(boxes);
                } else  {
                    columns.push({'field':k, 'title':k, 'checked':'', 'type':'label', 'indent':getIndent(k)});
                    var boxes = buildCheckbox(k,k,true,(columns.length - 1));
                    jQuery('#external_columns').append(boxes);
                    runLoop(obj[key], k);
                }
                k = current;
              
            } else {
                if(k) {
                    parent = k+".";
                } else {
                    parent = '';
                }

                var path = parent+key;
                columns.push({'field':path, 'title':key, 'checked':'', 'type':'field', 'indent':getIndent(path)});
                var boxes = buildCheckbox(key,path,false,(columns.length - 1));
                jQuery('#external_columns').append(boxes);
            }
        }
    }

    function getIndent(path) {
        var labelKeys = path.split(".");
        var indent = labelKeys.length * 10;
        return indent;
    }

    jQuery(document).on('click','.editColumn', function() {
        var id = jQuery(this).data('id');
        jQuery('#input-'+id).toggle();
        jQuery('#btn-'+id).toggle();
        jQuery('#btn-'+id).click(function(){
            var index = jQuery('#input-'+id).data('index');
            var newTitle = jQuery('#input-'+id).val();
            columns[index].title = newTitle;
            jQuery('#title-'+id).text(newTitle);
            jQuery('#input-'+id).hide();
            jQuery('#btn-'+id).hide();
            jQuery('#external_data').val(JSON.stringify(columns));
        });
    });

    jQuery(document).on('click','#external_btn', function() {
        var url = encodeURIComponent(jQuery("#external_link").val());
        
        var auth = jQuery("#external_authorization_required").is(":checked");

        if (auth) {
            var type = jQuery("#external_authorization_type").val();
            switch(type) {
                case "password":
                    var username = jQuery("#external_password_username").val();
                    var password = jQuery("#external_password_password").val();

                    auth =  {
                        type: 'password',
                        username: username,
                        password: password
                    };          
                                
                    break;
                case "token":

                    var token = jQuery("#external_web_token_textarea").val();

                    auth = {
                        type: 'token',
                        token: token
                    }

                    break;
                default:
                    auth = false;
                    break;
            }

            if (auth) {
                auth = btoa(JSON.stringify(auth));                
            }
        }

        columns = [];
        jQuery('#external_columns').html('');
        jQuery('.list-label').show();
        console.log(auth);
        jQuery.ajax({
            url: "index.php?option=com_reports&task=external_data&format=raw",            
            dataType: "json",
            data: {
                url: url,
                auth: auth
            },
            type: 'post',
            success: function(data) {
                console.log("success");
                console.log(data);
                return;
                runLoop(data[0], null);
                jQuery('#external_data').val(JSON.stringify(columns)); 
            }
        }); 
    });

    jQuery(document).on('change','.external_checkbox', function(){
        var index = jQuery(this).data('index');
        if (jQuery(this).is(':checked')) {
            columns[index].checked = 'checked';
        } else {
            columns[index].checked = '';
        }
        jQuery('#external_data').val(JSON.stringify(columns));
    });

    function removeSpreadsheetOption() {
        var type = jQuery('#report_type').val();
        if(type == 'external'){
            jQuery("#report_layout option[value='spreadsheet']").hide();
        } else {
            jQuery("#report_layout option[value='spreadsheet']").show();
        }
    }

    jQuery(document).on('change','#report_type', function(){
        var type = jQuery('#report_type').val();
        var layout = jQuery('#report_layout').val();
        if(type == 'external' && layout == 'spreadsheet'){
            jQuery("#report_layout").val('grid');
        }
        removeSpreadsheetOption();
    });
    
    removeSpreadsheetOption();

});