var view_id = null;
var view_name = null;

var spreadsheet = null;
var grid = null;
var load_select = null;
var save_as_name = null;

var report_id = null;

var max_length = 10 * 1024 * 1024; //10 MB

function checkProperty(property) {
    if( property !== '_handlers' 
        && property !== '_events' 
        && typeof property !== 'function' 
        && property !== 'uid' 
        && property !== 'parent') {
        return true;
    } else {
        return false;
    }
}

function checkObject(obj) {
    if(typeof(obj[0]) !== 'object' || obj[0].length) {
        return buildObjectList(obj);
    } else {
        return buildObjectTable(obj);
    }
}

function buildObjectList(obj) {
    var list = '';
    for (var i = 0; i < obj.length; i++) {
       list += obj[i]+`,`; 
    }                    
    return list.slice(0,-1);
}

function buildObjectTable(obj) {
    var tableHead = `<table class="table table-striped table-bordered">
                        <thead class="thead-inverse">
                            <tr>`;
    var tableBody = `<tbody`;                        
    var propNames = Object.getOwnPropertyNames(obj[0]);
    var activeProps = [];
    for (var i = 0; i < propNames.length; i++) {
        if(checkProperty(propNames[i])) { 
            tableHead += `<th> `+propNames[i]+`</th>`;
            activeProps.push(propNames[i]);
        }
    }
   

    for (var i = 0; i < obj.length; i++) {
        tableBody += `<tr>`;
        for (var a = 0; a < activeProps.length; a++) {
            tableBody += `<td> `+obj[i][activeProps[a]]+`</td>`;
        }
        tableBody += `</tr>`;            
    }
    tableHead += `</tr>
                    </thead>`;
    tableBody += `<tbody>
                    </table>`;
    
    return tableHead+tableBody;
}

$(document).ready(
	function() {

		report_id = report_data.id;

		load_select = document.getElementById("load_select");
		save_as_name = document.getElementById("save_as_name");

		$("#save_view").click(
			function() {
				save_view(view_id);
			}
		);
		
		$("#load_view").click(
			function() {
				var load_id = load_select.options[load_select.selectedIndex].value;
				var load_name = load_select.options[load_select.selectedIndex].innerHTML;

				load_view(load_id, load_name, view_name);
			}
		);

		$("#save_as_view").click(
			function() {
				var data = {
					report_id: report_id,
					name: view_name
				};

				//Check to see if the name already exists.

				$.ajax({
			        type: 'POST',
			        url: 'index.php?option=com_reports&task=check_overwrite&format=raw',
			        data: data,
			        success: function(record) {

			        	/*
			        		If it exists, ask if they want to overwrite it.
			        		If they do, call the "Save" function.
			        		If they don't, exit.

			        		If it does not exists, write as normal.
			        	*/

			        	var overwrite = false;
			        	var exists = false;

			        	record = parseInt(record);

			        	if (record == -1 ) {
			        		exists = false;
			        	} else {
			        		exists = true;
			        	}
			        	
			        	if (exists) {
			        		overwrite = confirm("Spreadsheet with the name \"" + view_name + "\" already exists.\nDo you want to overwrite it?");
			        	}

			        	//If it does not exist, write as normal.
			        	if (exists == false) {
			        		save_as_view(view_name);
						} else {
							//If it does exist and they wish to overwrite, so a SAVE instead of a SAVE AS to update the existing record.
							if (overwrite == true) {								
								save_view(record);
							}
						}
			        }
			    });
			}
		);

		$("#delete_view").click(
			function() {
				var delete_id = load_select.options[load_select.selectedIndex].value;
				var delete_name = load_select.options[load_select.selectedIndex].innerHTML;

				if (confirm("This will delete the spreadsheet \"" + delete_name + "\" from the database.\nThis operation is not reversable!")) {
					delete_view(delete_id);
				}
			}
		);

		purge_old();
		refresh_select_list();

		//Checking for changes and updates
		setInterval(
			function() {
				//if (view_id == null) {						
				if (load_select.options[load_select.selectedIndex].value == -1) {
					if (!$("#save_view").hasClass("disabled")) {
						$("#save_view").addClass("disabled");							
					}
				} else {						
					if ($("#save_view").hasClass("disabled")) {
						if ($("#save_view").attr("saving") != "true") {
							$("#save_view").removeClass("disabled");
						}
					}
				}

				if (save_as_name.value == "") {
					if (!$("#save_as_view").hasClass("disabled")) { 
						$("#save_as_view").addClass("disabled");
					}
				} else {
					view_name = save_as_name.value;
					if ($("#save_as_view").hasClass("disabled")) {
						if ($("#save_as_view").attr("saving") != "true") {
							$("#save_as_view").removeClass("disabled");
						}
					}
				}

				if (report_type == 'spreadsheet' || report_type == 'both') {
					if (spreadsheet === undefined || spreadsheet === null) {
						spreadsheet = $("#spreadsheet").data("kendoSpreadsheet");
					}
				}

				if (report_type == 'grid' || report_type == 'both') {
					if (grid === undefined || grid === null) {							
						grid = $("#grid").data("kendoGrid");						
					}
				}

			}, 250
		);

		/*
			Fix bootstrap issue.
		*/
		$(".view_selection_button").click(
			function() {

				$(".view_selection_button").each(
					function() {
						$(this).removeClass("activated");
					}
				);

				$(this).addClass("activated");				
			}
		);
	
	}
);

function refresh_select_list(setting) {

	if (!$("#load_view").hasClass("disabled")) {
		$("#load_view").addClass("disabled");
	}

	if (!$("#delete_view").hasClass("disabled")) { 
		$("#delete_view").addClass("disabled");
	}

	var data = {
		report_id: report_id
	};

	var select_list = document.getElementById("load_select");
	select_list.length = 0;

	var new_option = function(id, name) {
		var new_option = document.createElement('option');
		new_option.value = id;
		new_option.innerHTML = name;
		return new_option;
	}

	select_list.appendChild(new_option(-1, "Select Spreadsheet" ));

	$.ajax({
		type: 'POST',
		url: 'index.php?option=com_reports&task=get_all_names&format=raw',
		data: data,
		success: function(names) {
			if (names) {
				names = JSON.parse(names);
				for (var i = 0; i < names.length; i++) {
					select_list.appendChild(new_option(names[i].id, names[i].name));
				}

				if (setting) {
					select_list.value = parseInt(setting);
				}

				if (names.length > 0) {
					$("#load_view").removeClass("disabled");
					$("#delete_view").removeClass("disabled");
				}
			}
		}
	});
}

function save_view(view_id) {

	if (confirm("Overwrite \"" + load_select.options[load_select.selectedIndex].innerHTML + "\" with current spreadsheet data?")) {

		var save = document.getElementById("save_view");

		$(save).addClass("disabled");
		save.setAttribute("saving", true);

		var prev_text = save.innerHTML;
		save.innerHTML = "Saving";

		var data = {
			report_id: report_id,
			view_id: view_id,
			type: report_type,
			snapshot: get_snapshot()
		};
		
		$.ajax({
	        type: 'POST',
	        url: 'index.php?option=com_reports&task=save_spreadsheet&format=raw',
	        data: data,
	        success: function(response) {
	            
	            if (response != "success") {
	            	alert("There was an error saving the file.\nPlease contact an administrator.");
	            }
	            $(save).removeClass("disabled");
	            save.innerHTML = prev_text;
	            save.setAttribute("saving", false);
	        }
	    });
	}
}

function save_as_view(view_name) {

	var save_as = document.getElementById("save_as_view");
	var prev_text = save_as.innerHTML;
	save_as.innerHTML = "Saving";
	$(save_as).addClass("disabled");
	save_as.setAttribute("saving", true);

	var data = {
		report_id: report_id,
		name: view_name,
		type: report_type,
		snapshot: get_snapshot()
	};

	$.ajax({
        type: 'POST',
        url: 'index.php?option=com_reports&task=save_as_spreadsheet&format=raw',
        data: data,
        success: function(response) {
            //Add the new record to the select list.

            refresh_select_list(response);
            $(save_as).removeClass("disabled");
            save_as.innerHTML = prev_text;
            save_as.setAttribute("saving", false);        }
    });
}

function load_view(load_id, load_name, view_name) {

	var data = {
		report_id: report_id,
		view_id: load_id
	}

	var load = document.getElementById("load_view");
	var del = document.getElementById("delete_view");

	$(load).addClass("disabled");
	$(del).addClass("disabled");

	var prev_text = load.innerHTML;
	load.innerHTML = "Loading";

	$.ajax({
		type: 'POST',
		url: 'index.php?option=com_reports&task=load_spreadsheet&format=raw',
		data: data,
		success: function(snapshot) {

			if (snapshot != 'failed') {
				load_snapshot(snapshot);

				view_id = load_id;
				view_name = load_name;

				save_as_name.value = view_name;

			} else {					
				alert("Load failed.");
			}

			$(load).removeClass("disabled");
			$(del).removeClass("disabled");
			load.innerHTML = prev_text;

			$(".k-selection-none").each(
				function() {
					if ($(this).css("height") == "0px") {
						$(this).remove();
					}
				}
			);
		}
	});
}

function delete_view(delete_id) {
	var data = {
		report_id: report_id,
		view_id: delete_id
	}

	var del = document.getElementById("delete_view");
	$(del).addClass("disabled");
	var prev_text = del.innerHTML;
	del.innerHTML = "Deleting";

	$.ajax({
		type: 'POST',
		url: 'index.php?option=com_reports&task=delete_spreadsheet&format=raw',
		data: data,
		success: function(response) {			
			del.innerHTML = prev_text;
			refresh_select_list();				
		}
	});
}

function purge_old() {
	$.ajax({
		type: 'POST',
		url: 'index.php?option=com_reports&task=purge_deleted&format=raw',
		success: function(response) {
	
		}
	});
}

function get_snapshot() {
	var snapshot = new Object();

	var grid_snapshot = null;
	var spreadsheet_snapshot = null;

	switch (report_type) {
		case "spreadsheet":
			//snapshot = JSON.stringify(spreadsheet.toJSON());
			spreadsheet_snapshot = spreadsheet.toJSON();
			
			break;
		case "grid":
			//snapshot = JSON.stringify(grid.getOptions());
			grid_snapshot = grid.getOptions();
			break;
		case "both":
			//snapshot.spreadsheet = spreadsheet.toJSON();
			spreadsheet_snapshot = spreadsheet.toJSON();
			//snapshot.grid = grid.getOptions();
			grid_snapshot = grid.getOptions();

			//snapshot = JSON.stringify(snapshot);
			
			break;
	}

	if (JSON.stringify(spreadsheet_snapshot).length >= max_length) {
		alert("The spreadsheet exceeds the maximum file size allowed by the server.  Please export it to a local xlsx file instead.");
		//spreadsheet_snapshot = null;
	}

	if (grid_snapshot != null) {
		if (spreadsheet_snapshot != null) {
			//Both are set.
			snapshot.spreadsheet = spreadsheet_snapshot;
			snapshot.grid = grid_snapshot;
		} else {
			//Only grid is set.
			snapshot.grid = grid_snapshot;

		}
	} else {
		//Only spreadsheet is set.
		snapshot.spreadsheet = spreadsheet_snapshot;
	}
	
	snapshot = JSON.stringify(snapshot);

	return snapshot;
}

function load_snapshot(snapshot) {
	snapshot = JSON.parse(snapshot);	
	
	if (spreadsheet) {
		if (snapshot.spreadsheet) {
			spreadsheet.fromJSON(snapshot.spreadsheet);
		}
	}
	
	if (grid) {
		if (snapshot.grid) {
			var toolbar = snapshot.grid.toolbar;
			for(var i = 0; i < toolbar.length; i++) {
				if(toolbar[i].name == "EditColumn") {
					toolbar[i].template = kendo.template($("#btn-template").html());
				}
			}
			grid.setOptions(snapshot.grid);
		}
	}

	//window.location.reload();
}

function getFilter(type,extra,search) {
    var filter;
    switch(type){
        case "multi":
            filter = { 
                "multi": true, 
                "search": true 
            };
        break;

        case "none":
            filter = false;
        break;

        case "default":
            filter = { 
              string: {
			      contains: "Contains",
			      startswith: "Starts with",
			      eq: "Is equal to",
			      neq: "Is not equal to",
			      doesnotcontain: "Does not contain",
			      endswith: "Ends with"
			  },
			  number: {
			      eq: "Is equal to",
			      neq: "Is not equal to",
			      gte: "Is greater than or equal to",
			      gt: "Is greater than",
			      lte: "Is less than or equal to",
			      lt: "Is less than"
			  },
			  date: {
			      eq: "Is equal to",
			      neq: "Is not equal to",
			      gte: "Is after or equal to",
			      gt: "Is after",
			      lte: "Is before or equal to",
			      lt: "Is before"
			  },
			  enums: {
			      eq: "Is equal to",
			      neq: "Is not equal to"
			  }
            };
        break;

        case "datepicker":
            filter = {
                ui: "datepicker",
                operators: {
                    string: {
                        gt: "Is After",
                        lt: "Is Before",
                        eq: "Is Equal To"
                    }
                } 
            };
        break;

        case "datetimepicker":
            filter = {
                ui: "datetimepicker",
                operators: {
                    string: {
                        gt: "Is After",
                        lt: "Is Before",
                        eq: "Is Equal To"
                    }
                } 
            };
        break;
    }
    
    filter.extra = extra;
    if(search) {
        filter.mode = search;
    }
    return filter;
}

function filterSet() {
    var filters = [
        {
            "value" : "multi",
            "title" : "Multi Select Checkbox"
        },
        {
            "value" : "datepicker",
            "title" : "Date Picker"
        },
        {
            "value" : "datetimepicker",
            "title" : "Date & Time Picker"
        },
        {
            "value" : "default",
            "title" : "Default Conditions"
        },
        {
            "value" : "none",
            "title" : "No Filter"
        }
    ];

    return filters;
}


function formatSet() {
    var formats = [
        {
            "value" : "{0:c}",
            "title" : "Currency"
        },
        {
            "value" : "{0:n}",
            "title" : "Number"
        },
        {
            "value" : "{0:dd-MMM-yyyy}",
            "title" : "Date"
        },
        {
            "value" : "{0:dd-MMM-yyyy hh:mm:ss tt}",
            "title" : "Date & Time"
        },
        {
            "value" : "none",
            "title" : "No Formating"
        }
    ];

    return formats;
}

 function typeSet() {
    var types = [
        {
            "value" : "string",
            "title" : "String"
        },
        {
            "value" : "number",
            "title" : "Number"
        },
        {
            "value" : "date",
            "title" : "Date"
        },
        {
            "value" : "boolean",
            "title" : "Boolean"
        },
        {
            "value" : "object",
            "title" : "Object"
        }
    ];

    return types;
}

 function formulaSet() {
    var types = [
        {
            "value" : "sum",
            "title" : "Sum",
        },
        {
            "value" : "count",
            "title" : "Count",
        },
        {
            "value" : "average",
            "title" : "Average",
        },
        {
            "value" : "min",
            "title" : "Min",
        },
        {
            "value" : "max",
            "title" : "Max",
        },
        {
            "value" : "none",
            "title" : "No Formula",
        }
    ];

    return types;
}

function getColumnIndex(field) {
    var grid = $("#grid").data("kendoGrid");
    for(var i = 0; i < grid.columns.length; i++) {
        if(grid.columns[i].field == field) {
            return i;
        }
    }
    return -1;
}

function editColumnForm(column) {
    var grid = $("#grid").data("kendoGrid");
    var filters = filterSet();
    var formats = formatSet();
    var types = typeSet();
    var formulas = formulaSet();
    var title = '';
    var html = '';
    if(column == -1) {
        html += '<select class="block-input" name="column" id="columnSelect">';
        html += '<option value="">--Select a Column--</option>';
        for(var i = 0; i < grid.columns.length; i++) {
            html += '<option value="'+i+'">'+grid.columns[i].title+'</option>';
        }
        html += '</select>';
    } else {
        title = grid.columns[column].title;
        html += '<h4>Edit: '+grid.columns[column].title+'</h4>';
        html += '<input type="hidden" name="column" id="columnSelect" value="'+column+'"></input>';
    }
    html += '<input type="text" placeholder="New Column Title" class="block-input new-title" value="'+title+'"></input>';

    html += '<select class="block-input" name="column-type" id="columnType">';
    html += '<option value="">--Select a Data Type--</option>';
    for(var i = 0; i < types.length; i++) {
        html += '<option value="'+types[i].value+'">'+types[i].title+'</option>';
    }
    html += '</select>';
    
    html += '<select class="block-input" name="column-filter" id="columnFilter">';
    html += '<option value="">--Select a Filter--</option>';
    for(var i = 0; i < filters.length; i++) {
        html += '<option value="'+filters[i].value+'">'+filters[i].title+'</option>';
    }
    html += '</select>';
    
    html += '<div style="float:left; font-size:11px;"><input type="checkbox" id="filter-operators" checked></input> Operators (And & Or)</div>';
    
    html += '<select class="input-inline" name="column-type" id="columnFormula">';
    html += '<option value="">--Formula--</option>';
    for(var i = 0; i < formulas.length; i++) {
        html += '<option value="'+formulas[i].value+'">'+formulas[i].title+'</option>';
    }
    html += '</select>';
    
    html += '<select class="input-inline" name="column-format" id="columnFormat">';
    html += '<option value="">--Format--</option>';
    for(var i = 0; i < formats.length; i++) {
        html += '<option value="'+formats[i].value+'">'+formats[i].title+'</option>';
    }
    html += '</select>';
    
    html += '<span class="btn btn-primary column-edit-btn input-inline">Update</span>';
    html += '<span class="btn btn-default close-column-edit-btn input-inline">Close</span>';

    return html;
}

$(document).on('click','.k-grid-EditColumns', function() {
    var html = editColumnForm(-1);
    var filterSearch = false;
    $('#column-editor').html(html);
    $('#column-editor').slideToggle(); 
});

$(document).on('click','.column-edit-btn', function() {
    var filterExtra = false;
    var grid = $("#grid").data("kendoGrid");
    var column = $('#columnSelect').val();
    var type = $('#columnType').val();
    var format = $('#columnFormat').val();
    var formula = $('#columnFormula').val();
    var title = $('.new-title').val();
    var filterType = $('#columnFilter').val();
    if($('#filter-operators').is(':checked')) {
        filterExtra = true;
    }
    if(filterType){
        var filter = getFilter(filterType, filterExtra);
        grid.columns[column].filterable = filter;
    }
    if(title){
        grid.columns[column].title = title;
    }
    if(type){
        var field = grid.columns[column].field;
        if(typeof(grid.dataSource.options.schema.model) === "undefined") {
            grid.dataSource.options.schema = { model: { fields: {}}};
            grid.dataSource.reader = { model: { fields: {}}};
        }
	    grid.dataSource.options.schema.model.fields[field] = { type : type};
        grid.dataSource.reader.model.fields[field] = { type : type};
    }
    
   
    if(format){
        grid.columns[column].format = format;
    }
    if(formula){
        if(formula == 'none') {
            grid.columns[column].aggregates = [];
            grid.columns[column].footerTemplate = "";
        } else {
            var formatting = ""
            if(grid.columns[column].format == "{0:c}"){
                formatting = "c";
            }
            if(grid.columns[column].format == "{0:n}"){
                formatting = "n";
            }
            grid.columns[column].aggregates = [formula];
            grid.columns[column].footerTemplate = formula.toUpperCase()+": #= kendo.toString("+formula+", '"+formatting+"') #";
        }
        if(!grid.dataSource.options.aggregate){
            grid.dataSource.options.aggregate = [];
        }
        var aggregates = grid.dataSource.options.aggregate;
        
        function _checkAggregate() {
            for(var i = 0; i < aggregates.length; i++) {
                if(aggregates[i].field == grid.columns[column].field) {
                    return i + 1; 
                    break;
                } 
            }
        }

        var aggregateIndex = _checkAggregate(aggregates);

        if(aggregateIndex) {
            var index = aggregateIndex - 1;
            if(formula == 'none') {
                aggregates.splice(index,1);
            } else {
                aggregates[index].aggregate = formula;
            }
        } else {
            if(formula != 'none') {
                aggregates.push({ "field": grid.columns[column].field, "aggregate": formula});
            }
        }

    }
    
    grid.setOptions({
        dataSource: {
            aggregate: grid.dataSource.options.aggregate,
            schema: grid.dataSource.options.schema
        },
        columns: grid.columns

    });
});

$(document).on('click','.close-column-edit-btn', function() {;
    $('#column-editor').slideToggle(); 
});