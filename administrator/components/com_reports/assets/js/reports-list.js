// add spinning icon when clicking view report buttons
$(document).on('click', '#itemsList tr td a.link_button', function() {
	$(this).addClass('disabled');
	$(this).get(0).innerHTML = '<i class="fa fa-spinner fa-spin"></i> Running...';
});
