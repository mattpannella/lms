jQuery(document).ready(
	function() {

		//Get the ID of the current element
		var url = window.location.search.substring();
		var elements = url.split("&");
		var id = null;
		for (var i = 0; i < elements.length; i++) {
			var element = elements[i].split("=");
			if (element[0] == "id") {
				id = element[1];
			}
		}

		//If there is no id, then it is a new element and the Run Report button needs to be disabled.
		if (!id) {
			var run_report = document.getElementById("toolbar-generic");
			if (run_report) {
				var button = run_report.getElementsByTagName("button");
				button[0].disabled = true;
				run_report.title = "Run Report is disabled for new reports.\nPlease save your work first.";
				run_report.style.cursor = "not-allowed";
			}
		}


		// if there is a run report button, add loading spinner when clicked to prevent multiple clicks
		var run_report = document.getElementById("toolbar-generic");
		if (run_report) {
			var button = run_report.getElementsByTagName("button");
			button[0].addEventListener('click', function() {
				button[0].disabled = true;
				button[0].innerHTML = '<i class="fa fa-spinner fa-spin"></i> Running...';
			})
		}

		var control_groups = document.getElementsByClassName("control-group");
		for (var i = 0; i < control_groups.length; i++) {
			var control_label = control_groups[i].getElementsByClassName("control-label")[0];
			var for_type = control_label.getAttribute("for");
			if (for_type.includes("_settings")) {

				var select_all = document.createElement("div");
				var select_none = document.createElement("div");

				select_all.innerHTML = "Select All";
				select_none.innerHTML = "Select None";

				select_all.className = "btn btn-primary btn-sm";
				select_none.className = "btn btn-primary btn-sm";

				select_all.style.marginBottom = "10px";

				var change_all = function(button, value) {
					var controls = button.parentNode.parentNode.getElementsByClassName("controls")[0];

					var inputs = controls.getElementsByTagName("input");
					for (var c = 0; c < inputs.length; c++) {
						if (inputs[c].getAttribute("type") == "checkbox") {
							inputs[c].checked = value;
						}
					}

					//Apply extra data for
					var ec = controls.getElementsByClassName("external_checkbox");
					if (ec.length > 0) {
						jQuery(ec).each(
							function() {
								var index = jQuery(this).data('index');
								if (value) {
									columns[index].checked = 'checked';
								} else {
									columns[index].checked = '';
								}

								jQuery('#external_data').val(JSON.stringify(columns));
							}
						);

					}
				}

				jQuery(select_all).click(function() {
					change_all(this, true);
				});

				jQuery(select_none).click(function() {
					change_all(this, false);
				});

				control_label.appendChild(document.createElement('br'));
				control_label.appendChild(select_all);
				control_label.appendChild(document.createElement('br'));
				control_label.appendChild(select_none);
				control_label.appendChild(document.createElement('br'));
			}
		}



		/*jQuery(".link_button").click(
			function(e) {
				if (this.getAttribute("which") == "Run Report") {
					e.preventDefault();
					var url = this.getAttribute("href");
					Joomla.submitbutton("apply_and_redirect");
				}
			}
		);*/

	}
);

