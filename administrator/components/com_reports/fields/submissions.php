<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSubmissions extends JFormField {

	protected $type = 'submissions';
	
	protected function getInput() {
        $id 		  = JRequest::getVar('id');
        $report 	  = AxsCustomReports::getCustomReport($id);
        $submissions  = AxsCustomReports::getCustomReportSubmissions($id);
        $reportParams = json_decode($report->params);
        $fields       = json_decode($reportParams->fields);
        $theme 		  = $reportParams->report_theme;
		ob_start();
		require 'components/com_reports/templates/custom_reports.php';
		return ob_get_clean();
	}
}