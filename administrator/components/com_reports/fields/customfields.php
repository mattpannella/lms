<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldCustomfields extends JFormField {
	public function getInput() {
		ob_start();

		$data = json_decode($this->form->getValue('custom_fields'));

		$lang = JFactory::getLanguage();
		$lang->load("com_community", JPATH_SITE, "en-GB", true);

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);

		$query
			->select('*')
			->from('#__community_fields')
			->where($db->quoteName('published') . '=1')
			->where($db->quoteName('type') . '!='.$db->quote('group'));

		$db->setQuery($query);

		$results = $db->loadObjectList();

		$skip_list = array(
			8, 9, 10, 11, 23
		);

		?>

			<div class="controls">
                <input id="custom_fields" type="hidden" name="custom_fields" value='<?php echo json_encode($data);?>'/>
            </div>

            <div id="custom_field_list">

				<?php
					foreach ($results as $custom) {

						if (in_array($custom->id, $skip_list)) {
							continue;
						}

						if (in_array($custom->id, $data)) {
							$checked = "checked";
						} else {
							$checked = "";
						}
				?>
						<label class="checkbox" style="display: block;">
							<input class="custom_checkbox" type="checkbox" value="<?php echo $custom->id;?>" <?php echo $checked; ?>/>
							<?php echo JText::_($custom->name);?>
						</label>
				<?php
					}
				?>
			</div>

			<script>
				var custom_checkboxes = document.getElementsByClassName("custom_checkbox");
				var storefield = document.getElementById("custom_fields");

				jQuery(".custom_checkbox").click(
					function() {
						var custom_list = [];
						jQuery(custom_checkboxes).each(
							function() {
								if (this.checked) {
									custom_list.push(this.value);
								}
							}
						);

						storefield.value = JSON.stringify(custom_list);
					}
				);
			</script>
		<?php

		return ob_get_clean();
	}
}