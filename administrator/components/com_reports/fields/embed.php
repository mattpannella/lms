<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldEmbed extends FOFFormFieldText{

	protected $type = 'embed';

	public function getInput() {		
		$html = '<pre><code>'.$this->value.'</code></pre>';
		return $html;
	}	
}