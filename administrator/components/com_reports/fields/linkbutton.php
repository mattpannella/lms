<?php

class JFormFieldLinkButton extends JFormField 
{
    public function getInput() {
    	$id = JRequest::getVar('id');
    	return self::makeButton($id);
    }

    public function getRepeatable() {    	
    	$id = $this->item->id;
    	return self::makeButton($id);
    }

    private function makeButton($id) {

    	$url = $this->getAttribute("url");
    	$text = $this->getAttribute("text");

    	$url = str_replace("[ITEM:ID]", $id, $url);
    	
    	return "<a class='link_button btn btn-primary' which='" . $text . "'href='" . $url . "'>" . $text . "</a>";
    }
}