<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldExternal extends FOFFormFieldText{

	protected $type = 'external';
	/**
	 * @return string
	 */
	public function getInput() {	

		$urlParams = JFactory::getApplication()->input;
		$id = $urlParams->get('id', null, 'INT');
		$columnsTitle = 'display:none;';
		$columnsValues = '[]';
		$url = '';
		$checkboxes = '';

		if ($id) {
			$db = JFactory::getDbo();
			$query = "SELECT params from axs_reports WHERE id = $id";
			$db->setQuery($query);
			$paramsJSON = $db->loadObject();	
			$params = json_decode($paramsJSON->params);
			$columns = json_decode($params->external_data);

			if($columns) {
				$url = $params->external_url;
				$columnsValues = json_encode($columns);
				$columnsTitle = 'display:inherit;';
				$countColumns = count($columns);

				for ($i = 0; $i < $countColumns; $i++) {
					$randomID = rand(1,10000);
					if($columns[$i]->type == 'label') {
			            ob_start();
						?>

							<div
								class="list-label"
								style="margin-left: <?php echo $columns[$i]->indent + 10; ?>px;"
								data-index="<?php echo $i;?>"
							>
								<?php echo $columns[$i]->title; ?>
							</div>

						<?php
						$checkboxes .= ob_get_clean();
			        } else {
			            
			            ob_start();
			            ?>
			            	<input 
	                            type="checkbox" 
	                            id="check-<?php echo $randomID;?>" 
	                            name="external_columns[]" 
	                            value="<?php echo $columns[$i]->field; ?>"
	                            style="margin-left:<?php echo $columns[$i]->indent; ?>px;"
	                            data-index="<?php echo $i; ?>"
	                            class="external_checkbox"
			                    <?php echo ' ' . $columns[$i]->checked; ?>
			                />
			                            
                            <span 
                                id="title-<?php echo $randomID; ?>"
                            >
                                <?php echo $columns[$i]->title; ?>
                            </span> 
			                            
                            <a 
                                href="javascript:void(0)" 
                                class="editColumn" 
                                data-id="<?php echo $randomID; ?>"
                            >
                                edit
                            </a>
                            <input 
                                id="input-<?php echo $randomID; ?>"
                                style="display:none; margin-left:15px;"
                                class="form-control input-medium"
                                placeholder="New Title"
                                data-index="<?php echo $i; ?>"
                            />
                            <span 
                                id="btn-'.$randomID.'"
                                style="display:none;"
                                class="btn btn-primary"
                            >
                                Save
                            </span>
                        	<br/>
			            <?php
			            $checkboxes .= ob_get_clean();
			        }
				}
			}
		}

		ob_start();
		?>

			<input
				type="hidden" 
				name="external_data" 
				id="external_data"
				value=""
			/>
			<input
				type="text"
				name="external_url" 
				id="external_link" 
				placeholder="External URL"
				value="<?php echo $url; ?>" 
			/>
            <span class="btn btn-primary" id="external_btn">Get Columns</span>
            </br>
            <div style="margin: 10px 0px 10px 0px">
            	<span id="auth_text" style="cursor:pointer">Authorization Required</span>
            	<input type="checkbox" id="external_authorization_required"/>
            	<br>
            	<div id="external_authorization" style="margin: 5px; display:none">
            		Type<br>
	            	<select id="external_authorization_type">
	            		<option value="password">Username and Password</option>
	            		<option value="token">JSON web token</option>
	            	</select>
	            	<br><br>

	            	<div id="external_password" class="external_option control-group">
                		<label class="control-label" for="username">User Name</label>
                		<input id="external_password_username" type="text" placeholder="username" name="username">
                		<br>
                		<label class="control-label" for="password">Password</label>
                		<input id="external_password_password" type="text" placeholder="password" name="password">
            		</div>
            		<div id="external_web_token" class="external_option" style="display:none;">
	            		Web Token: <br><textarea id="external_web_token_textarea"></textarea>
	            	</div>
	            </div>
            </div>
           	<div class="list-label" style="margin-left:10px; <?php echo $columnsTitle; ?>">Columns</div>
            <div id="external_columns"><?php echo $checkboxes; ?></div>

            <script>
        		var columns = <?php echo $columnsValues; ?>;
        		jQuery("#external_data").val(JSON.stringify(columns));

        		var required = document.getElementById("external_authorization_required");
        		jQuery(required).change(
        			function() {
        				var value = required.checked;
        				if (value) {
        					jQuery("#external_authorization").show(500);
        				} else {
        					jQuery("#external_authorization").hide(500);
        				}
        			}
        		);

        		var auth_type = document.getElementById("external_authorization_type");
        		jQuery(auth_type).change(
        			function() {
        				var value = auth_type.options[auth_type.selectedIndex].value;
        				jQuery(".external_option").hide(500);
        				switch (value) {
        					case "token":
        						jQuery("#external_web_token").show(500);
        						break;
        					case "password":
        						jQuery("#external_password").show(500);
        						break;
        				}
        			}
        		);

        		jQuery("#auth_text").click(
        			function() {
        				//var checkbox = document.getElementById("external_authorization_required");
        				jQuery("#external_authorization_required").trigger("click");
        			}
        		);
        	</script>

		<?php
		$field = ob_get_clean();

		return $field;
	}
	
}