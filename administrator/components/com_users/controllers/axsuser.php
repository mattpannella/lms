<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * User controller class.
 *
 * @since  1.6
 */
class UsersControllerAxsuser extends JControllerForm
{
	/**
	 * @var    string  The prefix to use with controller messages.
	 * @since  1.6
	 */
	protected $text_prefix = 'COM_USERS_USER';

	/**
	 * Overrides JControllerForm::allowEdit
	 *
	 * Checks that non-Super Admins are not editing Super Admins.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean  True if allowed, false otherwise.
	 *
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check if this person is a Super Admin
		if (JAccess::check($data[$key], 'core.admin'))
		{
			// If I'm not a Super Admin, then disallow the edit.
			if (!JFactory::getUser()->authorise('core.admin'))
			{
				return false;
			}
		}

		return parent::allowEdit($data, $key);
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean  True on success, false on failure
	 *
	 * @since   2.5
	 */
	public function batch($model = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('Axsuser', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_users&view=axsusers' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
		return;
	}

	public function deleteUser() {
		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$deleteUserId = $input->get('id',0,'INT');

		if(!$deleteUserId) {
			$this->setMessage("No User Found", 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
			return false;
		}
		if ($user->id == $deleteUserId) {
			$this->setMessage(JText::_('COM_USERS_USERS_ERROR_CANNOT_DELETE_SELF'), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
			return false;
		}
		$deleteUser = JFactory::getUser($deleteUserId);

		// Delete the user.
		if (!$deleteUser->delete())
		{
			$this->setMessage("Could Not Delete User", 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
			return false;
		} else {
			$this->setMessage("User Successfully Deleted", 'message');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
		}

		return true;
	}

	public function disable2FA() {
		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$encryptedId = $input->get('id',0,'STRING');
		$key = AxsKeys::getKey('lms');
		$twoFactorUserId = AxsEncryption::decrypt(base64_decode($encryptedId), $key);
		if(!$twoFactorUserId) {
			$this->setMessage('Error Disabling Two-factor Authentication', 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
			return false;
		}
		$db = JFactory::getDbo();
		$whereConditions = $db->quoteName('id')."=".$db->quote($twoFactorUserId);
		$setConditions[] = $db->quoteName('otep')."= ''";
		$setConditions[] = $db->quoteName('otpKey')."= ''";
		$query = $db->getQuery(true);
		$query->update('#__users');
		$query->set($setConditions);
		$query->where($whereConditions);
		$db->setQuery($query);
		if (!$db->execute())
		{
			$this->setMessage("Could Not Disable Two-factor Authentication", 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_users', false));
			return false;
		} else {
			$this->setMessage("Two-factor Authentication Disabled", 'message');
			$this->setRedirect(JRoute::_("index.php?option=com_users&view=axsuser&layout=edit&id=$twoFactorUserId", false));
		}

		return true;
	}
}
