<div class="container-fluid">
    <div class='card'>
        <h5 class="card-header">Course Comments</h5>
        <div>
            <?php foreach ($comments as $comment) {
                if($comment->enabled == 1) {
                    $btnType = "danger";
                    $publishBtnText = "Unpublish";
                } else {
                    $btnType = "success";
                    $publishBtnText = "Publish";
                }

            ?>
                <div class="row comment-container">
                    <div class="col-12">
                        <div class="comment-info">
                            <?php echo 'Course: '.$comment->course_title; ?>
                        </div>
                        <div class="comment-text">
                            <?php echo $comment->comment; ?>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="comment-actions pull-right">
                            <a class="btn btn-<?php echo $btnType; ?> comment-btn-publish me-1" data-status="<?php echo $comment->enabled; ?>" data-comment="<?php echo $comment->id; ?>"><i class="fas fa-pencil"></i> <span class="comment-btn-text"><?php echo $publishBtnText; ?></span></a>
                            <a class="btn btn-light comment-btn-delete" data-comment="<?php echo $comment->id; ?>"><i class="fas fa-times"></i> <span class="comment-btn-text">Delete</span></a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script src="components/com_users/assets/js/user_account_comments.js?v=6" type="text/javascript"></script>