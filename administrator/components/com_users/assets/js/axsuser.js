/**
 * Created by mar on 6/16/16.
 */

//############################ For timeline #####################################################
(function($) {
    $(function() {
        //get our handlebars templates ready by compiling them
        var subPeriodTmpl = Handlebars.compile($('#sub-period-tmpl').html());
        var trxnTmpl = Handlebars.compile($('#trxn-tmpl').html());
        var adminActionTmpl = Handlebars.compile($('#admin-action-tmpl').html());
        var eventActionTmpl = Handlebars.compile($('#event-action-tmpl').html());
        var timelineKeyTmpl = Handlebars.compile($('#timeline-key-tmpl').html());
        var subTmpl = Handlebars.compile($('#sub-tmpl').html());
        var subSettingsTmpl = Handlebars.compile($('#sub-settings-tmpl').html());
        var renewTmpl = Handlebars.compile($('#renew-tmpl').html());
        var pauseTmpl = Handlebars.compile($('#pause-tmpl').html());
        var adjustDateTmpl = Handlebars.compile($('#adjust-date-tmpl').html());
        var addSubTmpl = Handlebars.compile($('#add-sub-tmpl').html());
        var markPaidSubPerTmpl = Handlebars.compile($('#mark-paid-sub-period-tmpl').html());
        var courseActionTmpl = Handlebars.compile($('#course-actions-tmpl').html());
        var coursePurchaseTmpl = Handlebars.compile($('#course-purchases-tmpl').html());
        var eshopOrderTmpl = Handlebars.compile($('#eshop-order-tmpl').html());

        var reasonAlert = "A reason is required for this administrator action.";

        //helpers for the templates
        Handlebars.registerHelper('formatdate', function(date) {
            if (!date) return;
            var d = new Date(date);
            if (d == 'Invalid Date') {
                return;
            }
            var hour = parseInt(d.getHours());
            var half = 'am';
            if (hour > 12) {
                hour = d.getHours() - 12;
                half = 'pm';
            } else if (hour == 0) {
                hour = 12;
            }
            var minute = ("00" + d.getMinutes()).slice(-2);
            return d.getMonth() + 1 + '/' + d.getDate() + '/' + d.getFullYear() + ' ' + hour + ':' + minute + half + ' (Mountain Time)';
        });
        Handlebars.registerHelper('formattrxn', function(status) {
            switch (status) {
                case 'PLHO':
                    return 'Placeholder';
                case 'FAIL':
                    return 'Failed';
                case 'DEC':
                    return 'Declined';
                case 'SUC':
                    return 'Successful';
                case "REF":
                    return 'Refunded';
                default:
                    return status;
            }
        });
        Handlebars.registerHelper('formattrxnclass', function(status) {
            switch (status) {
                case 'PLHO':
                    return 'warning';
                case 'FAIL':
                    return 'error';
                case 'DEC':
                    return 'error';
                case 'SUC':
                    return 'success';
                case "REF":
                    return 'error';
                default:
                    return status;
            }
        });
        Handlebars.registerHelper('formatsubperiod', function(status) {
            switch (status) {
                case 'PAID':
                    return 'Fully Paid';
                case 'PART':
                    return 'Partially Paid';
                case 'NONE':
                    return 'None Paid';
                case 'PRO1':
                    return 'Prorated Unpaid';
                case 'PRO2':
                    return 'Prorated Paid';
                case 'COMP':
                    return 'Comp Extension';
                case 'PAUS':
                    return 'Paused';
                case 'FREE':
                    return 'Free';
                default:
                    return status;
            }
        });
        Handlebars.registerHelper('formatcard', function(card) {
            if (typeof card == 'object') {
                return card.cardNumber;
            } else if (card == '0') {
                return 'No card used';
            } else {
                return card;
            }
        });
        Handlebars.registerHelper('formatcardbyid', function(card_id) {
            var ret;
            cards.forEach(function(card) {
                if(card_id == card.id) {
                    ret = "Card ending in " + card.cardNumber;
                }
            });
            return ret ? ret : 'No card set';
        });
        Handlebars.registerHelper('formatinitiator', function(initiator) {
            switch (parseInt(initiator)) {
                case 1:
                    return 'System';
                case 2:
                    return 'Admin';
                case 3:
                    return 'User';
                case 4:
                    return 'System (Auto Recover)';
                default:
                    return initiator;
            }
        });
        Handlebars.registerHelper('formatcurrency', function(amount) {
            var parts = amount.split(".");
            var dollar = parts[0];
            var cents = parseInt(parts[1]);
            while (cents > 100) {
                cents /= 10;
            }

            cents = parseInt(cents);
            if (cents < 10) {
                cents = "0" + cents;
            }
            return currency_symbol + dollar + "." + cents;
        });
        Handlebars.registerHelper('formatrefund', function(status, trxn_id) {
            if (status == 'SUC') {
                return new Handlebars.SafeString('<button id="refundTrxn" class="btn btn-danger col-sm-3" data-trxn="'+trxn_id+'"><span class="icon-credit"></span> Refund Transaction</button>' +
                    '<input name="reason" type="text" class="col-sm-9" placeholder="Reason for action">');
            }
        });
        Handlebars.registerHelper('formateshop', function() {
            console.log(this);

            var returnString;

            /*
                Order statuses
                1   Canceled
                2   Canceled Reversal
                3   Chargeback
                4   Complete
                5   Denied
                6   Expired
                7   Failed
                8   Pending
                9   Processed
                10  Processing
                11  Refunded
                12  Reversed
                13  Shipped
                14  Voided
            */

            var can_cancel = false;

            switch (parseInt(this.orderstatus_id)) {
                case 8:
                case 9:
                case 10:
                    can_cancel = true;
            }

            var can_refund = false;

            switch (parseInt(this.orderstatus_id)) {
                case 1:
                case 2:
                case 4:
                case 6:
                case 8:
                case 9:
                case 10:
                case 12:
                case 13:
                case 14:
                    can_refund = true;
            }

            returnString =
                '<div id="userData" data-user-id="' + this.user_id + '" data-user="' + btoa(JSON.stringify(this)) + '"></div>' +
                '<div class="action-group">' +
                '<button ';

                if (!can_refund) {
                    returnString += " disabled ";
                }

                returnString +=
                    'id="refundOrder" ' +
                    'class="btn btn-danger ' +
                    'col-sm-3" ' +
                    'data-order="' + this.id + '">' +
                    '<span class="icon-undo"></span> Refund Order' +
                '</button>' +
                '<input style="margin-left: 8px" name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>  ';


            returnString +=
                '<div class="action-group">' +
                '<button ';

                    if (!can_cancel) {
                        returnString += " disabled ";
                    }

                returnString +=
                    'id="cancelOrder" ' +
                    'class="btn btn-warning ' +
                    'col-sm-3" ' +
                    'data-order="' + this.id + '">' +
                    '<span class="icon-cancel-circle"></span> Cancel Order' +
                '</button>' +
                '<input style="margin-left: 8px" name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>  ';

            returnString +=
                '<div class="action-group">' +
                    '<a ' +
                        'href="index.php?option=com_eshop&task=order.edit&cid[]=' + this.order_id + '"' +
                        'target="_blank"' +
                        'class="btn btn-success ' +
                        'col-sm-3">' +
                        '<span class="icon-pencil"></span> Edit Order' +
                    '</a>' +
                '</div>  ';

            returnString +=
                '<div class="action-group">' +
                    '<a ' +
                        'href="index.php?option=com_eshop&task=order.downloadInvoice&cid[]=' + this.order_id + '"' +
                        'class="btn btn-standard ' +
                        'col-sm-3">' +
                        '<span class="icon-file-2"></span> Download Invoice' +
                    '</a>' +
                '</div>';


            return new Handlebars.SafeString(returnString);
        });
        Handlebars.registerHelper('formateventaction', function() {

            var returnString =
                '<div id="userData" data-user="' + btoa(JSON.stringify(this)) + '"></div>' +
                '<div class="action-group">' +
                    '<button ';

                        if (this.payment_status == "No") {
                            returnString += ' disabled ';
                        }

                        returnString +=
                        'id="refundRegistration" ' +
                        'class="btn btn-warning">' +
                        '<span class="icon-undo"></span> Refund Registration' +
                    '</button>' +
                    '<input name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>';

            returnString +=
                '<div class="action-group">' +
                    '<button ';

                        if (this.event_id == null) {
                            returnString += ' disabled ';
                        }

                        returnString +=
                        'id="cancelRegistration" ' +
                        'class="btn btn-danger">' +
                        '<span class="icon-cancel-circle"></span> Cancel Registration' +
                    '</button>' +
                    '<input name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>';

            if (this.event_id != null) {

                var checkInButtons = function(checked_in, id) {
                    var string = "";

                    string +=
                        '<div class="action-group" ';

                        if (checked_in == "Yes") {
                            string += 'style="display:none"';
                        }

                    string +=
                        '>' +
                            '<span ' +
                                'class="checkInRegistration btn btn-success"' +
                                'which=' + id +
                                '>' +
                                '<span class="icon-checkmark"></span> Check User In' +
                            '</span>' +
                        '</div>';

                    string +=
                        '<div class="action-group" ';

                        if (checked_in == "No") {
                            string += 'style="display:none"';
                        }

                    string +=
                        '>' +
                            '<span ' +
                                'class="checkOutRegistration btn btn-warning"' +
                                'which=' + id +
                                '>' +
                                '<span class="icon-cancel-circle"></span> Check User Out' +
                            '</span>' +
                        '</div>';

                    return string;

                }

                if (!this.group_data) {
                    //Single registrant
                    returnString += checkInButtons(this.checked_in, this.registration_id);
                } else {
                    //Group registration
                    returnString += "<table class='table table-striped table-bordered table-hover'>";
                    for (var i = 0; i < this.group_data.length; i++) {
                        var attendee = this.group_data[i];
                        returnString += "<tr>";
                        returnString += "<td>" + attendee.first_name + " (" + attendee.email + ")</td>";
                        returnString += "<td>" + checkInButtons(attendee.checked_in, attendee.id) + "</td>";
                        returnString += "</tr>";
                    }
                    returnString += "</table>";

                }
            }

            return new Handlebars.SafeString(returnString);
        });
        Handlebars.registerHelper('formatsubperiodactions', function(status) {
            var disabled = "";
            var title = "";

            if (status == "PAID" || status == "PRO2") {
                disabled = "disabled";
                title = "This period is fully paid for.";
            } else if (status == "PAUS") {
                disabled = "disabled";
                title = "During paused periods they do not have access and shouldn\'t be charged.";
            } else if (status == "PRO1" && (new Date()).getTime()<(new Date(this.end)).getTime() ) {
                disabled = "disabled";
                title = "This period is set to be prorated and will be charged on it\'s end date.";
            } else {
                disabled = "";
                title = "";
            }

            return new Handlebars.SafeString(
                '<div class="action-group hasTooltip" title="'+title+'">' +
                '<button id="reprocessSubPeriod" class="btn btn-danger col-sm-3" data-sub-perid="'+this.id+'" data-userid="'+this.user_id+'" data-subid="'+this.sub_id+'" '+disabled+'><span class="icon-credit"></span> Reprocess Payment</button>' +
                '<input name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>' +
                '<div class="action-group">' +
                '<button id="markPaidSubPeriod" class="btn btn-success col-sm-3" data-bs-toggle="modal" data-bs-target="#myModal" data-sub-perid="'+this.id+'" data-userid="'+this.user_id+'" data-subid="'+this.sub_id+'" '+disabled+'><span class="icon-checkbox"></span> Mark Paid</button>' +
                '</div>'
            );
        });
        Handlebars.registerHelper('formatsub', function(status) {
            switch (status) {
                case 'ACT':
                    return 'Active';
                case 'INAC':
                    return 'Inactive';
                case 'GRC':
                    return 'Grace Period';
                case 'PAUS':
                    return 'Paused';
                default:
                    return status;
            }
        });
        Handlebars.registerHelper('formatsubaction', function(status) {
            return new Handlebars.SafeString(
                '<div class="action-group">' +
                '<button id="renewsub" data-subid="'+this.id+'" data-user="'+this.user_id+'" data-desc="'+this.title+'" '+(status == "ACT" || status == "GRC" ? "disabled" : "")+' data-bs-toggle="modal" data-bs-target="#myModal" class="btn btn-success col-sm-3" type="button"><span class="icon-play"></span> Renew Subscription</button>' +
                '</div>' +
                '<div class="action-group">' +
                '<button id="pausesub" data-subid="'+this.id+'" data-user="'+this.user_id+'" '+(status == "INAC" ? "disabled" : "")+' data-bs-toggle="modal" data-bs-target="#myModal" class="btn btn-warning col-sm-3" type="button"><span class="icon-pause"></span> Pause Subscription</button>' +
                '</div>' +
                '<div class="action-group">' +
                '<button id="cancelsub" data-subid="'+this.id+'" data-user="'+this.user_id+'" '+(status == "INAC" ? "disabled" : "")+' class="btn btn-danger col-sm-3" type="button"><span class="icon-stop"></span> Cancel Subscription</button>' +
                '<input style="margin-left: 5px" name="reason" type="text" class="col-sm-9" placeholder="Reason for action">' +
                '</div>' +
                '<div class="action-group">' +
                '<button id="adjustdate" data-subid="'+this.id+'" data-user="'+this.user_id+'" '+(status == "INAC" ? "disabled" : "")+' data-bs-toggle="modal" data-bs-target="#myModal" class="btn btn-primary col-sm-3" type="button"><span class="icon-calendar"></span> Adjust Pay Date</button>' +
                '</div>'
            );
        });
        Handlebars.registerHelper('formatlength', function(period_length) {
            var length = period_length.substr(0, period_length.length-1);
            var ret = '';
            for(var i=1; i<=28; i++) {
                ret += '<option value="'+i+'" '+(length == i ? "selected" : "")+'>'+i+'</option>';
            }
            return new Handlebars.SafeString(ret);
        });
        Handlebars.registerHelper('formatterm', function(period_length) {
            var term = period_length.substr(-1);
            return new Handlebars.SafeString(
                '<option value="D" '+(term == "D" ? "selected" : "")+'>Days</option>' +
                '<option value="M" '+(term == "M" ? "selected" : "")+'>Months</option>' +
                '<option value="Y" '+(term == "Y" ? "selected" : "")+'>Years</option>'
            );
        });
        Handlebars.registerHelper('formatselectedcard', function(card_id) {
            var ret = '';
            cards.forEach(function(card) {
                ret += '<option value="'+card.id+'" '+(card_id == card.id ? "selected" : "")+'>Ending in '+card.cardNumber+'</option>';
            });
            return new Handlebars.SafeString(ret);
        });
        Handlebars.registerHelper('formatperiodlength', function(period_length) {
            if(!period_length) {
                return 'Lifetime';
            }

            var length = period_length.substr(0, period_length.length-1);
            var term = period_length.substr(-1);
            var postfix = length > 1 ? 's' : '';

            switch(term) {
                case 'D':
                    return length + ' Day' + postfix;
                case 'M':
                    return length + ' Month' + postfix;
                case 'Y':
                    return length + ' Year' + postfix;
                default:
                    return length + ' Unit' + postfix;
            }
        });
        Handlebars.registerHelper('formatplans', function(plans) {
            var ret = '';
            plans.forEach(function(plan) {
                ret += '<option value="'+plan.id+'">'+plan.title+'</option>';
            });
            return new Handlebars.SafeString(ret);
        });
        Handlebars.registerHelper('formatcards', function(cards) {
            var ret = '';
            cards.forEach(function(card) {
                ret += '<option value="'+card.id+'">Ending in '+card.cardNumber+'</option>';
            });
            return new Handlebars.SafeString(ret);
        });
        Handlebars.registerHelper('formatcoursepayment', function(payment) {
            if (payment && typeof payment == 'object') {
                return new Handlebars.SafeString(
                    '<tr><td class="labels">Payment Type</td><td>Single Transaction</td></tr>' +
                    '<tr><td class="labels">Payment Transaction</td><td>$' + payment.amount +' - '+ Handlebars.helpers.formattrxn(payment.status) +' - '+ Handlebars.helpers.formatdate(payment.date) + '</td></tr>'
                );
            }
            //TODO: also if typeof is Array, then they used a payment plan to pay for it.
        });

        //timeline initialization
        var container = document.getElementById('visualization');

        if (!container) {
            return;
        }

        // create an observer instance
        var observer = new MutationObserver(function(mutationRecords, mutationObserverInstance) {
            mutationRecords.forEach(function(mr) {
                //check to see when the height of the timeline is set.  This means it's drawn and ready.
                if(mr.target.classList.contains('vis-timeline') && mr.target.style.height != "") {
                    //kill the observer we found the event we're looking for.
                    observer.disconnect();
                    onTimelineDrawn();
                }
            });
        });

        // pass in the target node, as well as the observer options
        observer.observe(container, {
            subtree: true,
            attributes: true
        });

        var onTimelineDrawn = function() {};

        var options = {
            stack: false,
            zoomMin: 100000,
            zoomMax: 100000000000,
            groupOrder: function(a, b) {
                return a.order - b.order;
            },
            groupOrderSwap: function(a, b, groups) {
                var v = a.order;
                a.order = b.order;
                b.order = v;
            },
            groupEditable: true
        };

        var timeline = new vis.Timeline(container, items, groups, options);
        var curr_sub;

        //check if the page was loaded with a specific trxn or sub to display, if so highlight
        if (sub_id) {
            var sub = getSubBySubId(sub_id);
            $('.displayItem').html(subTmpl(sub));
            curr_sub = sub;
            $('.hasTooltip').tooltip();
        } else if(trxn_id) {
            var trxn_data = getTrxnByTrxnId(trxn_id);
            timeline.setSelection(trxn_data.id, {
                focus: true
            });
            onTimelineDrawn = function() {
                //now give the items that aren't selected notselected
                $('.vis-item').each(function() {
                    if (!$(this).hasClass('vis-selected')) {
                        $(this).addClass('notselected');
                    }
                });
            };
            $('.displayItem').html(trxnTmpl(trxn_data.trxn));
            $('.hasTooltip').tooltip();
        }

        function getTrxnByTrxnId(trxn_id) {
            //first search through subs for trxn
            for(var i=0; i<subscriptions.length; i++) {
                var sub = subscriptions[i];
                if(sub.periods) {
                    for(var j=0; j<sub.periods.length; j++) {
                        var period = sub.periods[j];
                        if(period.trxns) {
                            for(var k=0; k<period.trxns.length; k++) {
                                var trxn = period.trxns[k];
                                if(trxn.id == trxn_id) {
                                    return {
                                        id: reverseItemMapping['subscriptions['+i+'].periods['+j+'].trxns['+k+']'],
                                        trxn: trxn
                                    };
                                }
                            }
                        }
                    }
                }
            }

            //TODO: search through other trxn types found on timeline
        }

        $('.markPaidCourse').click( function() {
            if(confirm('Are you sure that you want to mark this course paid?')) {
                var cpid = $(this).data('cpid');

                $.ajax({
                    type: 'POST',
                    url: '/index.php?option=com_splms&task=lms.markCoursePaid&format=raw',
                    data:{
                        id: cpid
                    }
                }).done(function(response) {
                    if(response == 'success') {
                        location.reload();
                    } else {
                        window.alert(response);
                    }
                });
            }
        });

        $('.deleteCoursePurchase').click( function() {
            if(confirm('Are you sure that you want to delete this course purchase?')) {
                var cpid = $(this).data('cpid');

                $.ajax({
                    type: 'POST',
                    url: '/index.php?option=com_splms&task=lms.deleteCoursePurchase&format=raw',
                    data:{
                        id: cpid
                    }
                }).done(function(response) {
                    if(response == 'success') {
                        location.reload();
                    } else {
                        window.alert(response);
                    }
                });
            }
        });

        function getSubBySubId(sub_id) {
            for(var i=0; i<subscriptions.length; i++) {
                var sub = subscriptions[i];
                if(sub.id == sub_id) {
                    return sub;
                }
            }
        }

        //add event listener for click events on an item
        timeline.on('select', function(properties) {
            //first remove all notselected
            $('.vis-item').removeClass('notselected');

            //Check for invalid data

            var id = properties.items[0];
            if (id === undefined) {
                return;
            }

            //console.log(itemMapping[id]);

            try {
                var obj = eval(itemMapping[id]);
            } catch (err) {
                return;
            }


            if (obj === undefined) {
                return;
            }

            var items = [];
            var timeline_type = obj.timeline_type;

            obj.itemMappingId = id;

            switch (timeline_type) {
                case "admin_action":
                    $('.displayItem').html(adminActionTmpl(obj));
                    items.push(id);
                    break;
                case "sub_trxn":
                    $('.displayItem').html(trxnTmpl(obj));
                    //trxn for a subscription period, grab the subscription period and all trxns that belong to it.
                    var pathArray = itemMapping[id].split(']');
                    var path = pathArray[0] + ']' + pathArray[1] + ']';
                    items.push(reverseItemMapping[path]);
                    obj = eval(path);
                    for (var i in obj.trxns) {
                        if (obj.trxns[i] && typeof(obj.trxns[i]) == "object") {
                            var item_id = reverseItemMapping[path + '.trxns[' + i + ']'];
                            items.push(item_id);
                        }
                    }
                    break;
                case "sub_period":
                    $('.displayItem').html(subPeriodTmpl(obj));
                    items.push(id);
                    //subscription period, just grab the trxns that belong to it and get their id's using the itemMapping, then we can select them
                    for (var i in obj.trxns) {
                        if (obj.trxns[i] && typeof(obj.trxns[i]) == "object") {
                            var item_id = reverseItemMapping[itemMapping[id] + '.trxns[' + i + ']'];
                            items.push(item_id);
                        }
                    }
                    break;
                case "course_activity":
                    $('.displayItem').html(courseActionTmpl(obj));
                    items.push(id);
                    break;
                case "course_purchase":
                    $('.displayItem').html(coursePurchaseTmpl(obj));
                    items.push(id);
                    break;
                case "eshop_order":
                    $('.displayItem').html(eshopOrderTmpl(obj));
                    items.push(id);
                    break;
                case "event_actions":
                    $('.displayItem').html(eventActionTmpl(obj));
                    items.push(id);
                    break;
                default:
                    items.push(id);
                    break;
            }

            timeline.setSelection(items, {
                focus: true
            });

            //now give the items that aren't selected notselected
            $('.vis-item').each(function() {
                if (!$(this).hasClass('vis-selected')) {
                    $(this).addClass('notselected');
                }
            });

            //activate tooltips
            $('.hasTooltip').tooltip();
        });

        //add event listener for click events, filter for click events on subscriptions
        timeline.on('click', function(properties) {
            if (properties.what == 'group-label') {
                var obj = eval(itemMapping[properties.group]);
                //check if the obj is a subscription, if so lets load the subscription template.
                if (obj.hasOwnProperty('plan_id')) {
                    $('.displayItem').html(subTmpl(obj));
                    curr_sub = obj;
                    $('.hasTooltip').tooltip();
                }
            }
        });

        //initialize the timeline key with the data
        $('.displayKey').html(timelineKeyTmpl(groups));
        $('input[name=master]').click(function() {
            var checked = $(this).prop('checked');
            $('.timeline-keys').prop('checked', checked);
            if (checked) {
                $.each(original_groups, function() {
                    if (!groups._data.hasOwnProperty(this.id)) {
                        groups.add(this);
                    }
                });
            } else {
                $.each(original_groups, function() {
                    groups.remove(this.id);
                });
            }
        });
        $('.timeline-keys').click(function() {
            $('input[name=master]').prop('checked', false);
            var id = $(this).val();
            if ($(this).prop('checked')) {
                $.each(original_groups, function() {
                    if (this.id == id) {
                        groups.add(this);
                        return false;
                    }
                });
            } else {
                groups.remove(id);
            }
        });

        /*
            if evalString = "array[1]"
            return
            {
                name: "array",
                index: 1
            }
        */

        var getEvalArray = function(evalString) {
            var array = evalString.split("[");
            var arrayIndex = array[1].split("]")[0];

            return {
                array: eval(array[0]),
                index: parseInt(arrayIndex)
            };
        }

        var getUserData = function() {
            //Get the div with the user's base 64 encoded data and grab the data
            var data = document.getElementById("userData").getAttribute("data-user");
            //base 64 decode it
            data = atob(data);
            //Parse it
            data = JSON.parse(data);
            return data;
        }

        $(document).on('click', '#cancelRegistration', function(e) {
            e.preventDefault();
            var _this = this;
            var button = $(this);
            var reason = button.next('input[name=reason]').val();
            var userId = $("#userInformation").attr("data-user-id");

            if (reason) {
                var userData = getUserData();
                var evalData = getEvalArray(itemMapping[userData.itemMappingId]);

                if (confirm("Are you sure you want to cancel this user's registration?")) {
                    var userData = getUserData();
                    //console.log(userData);
                    $.ajax({
                        url: 'index.php?option=com_eventbooking&task=registrant.admin_cancel_registration&format=raw',
                        data: {
                            userData: userData,
                            reason: reason,
                            userId, userId
                        },
                        type: 'post'
                    }).done(function(result) {
                        console.log("")
                        if (result) {
                            console.log(result);
                        }

                        var obj = evalData.array[evalData.index];

                        obj.action_type = "Registered (Cancelled)";
                        obj.checked_in = "No";
                        obj.original_event_id = obj.event_id;
                        obj.event_id = null;
                        obj.checked_in_count += obj.num_registrants;

                        $('.displayItem').html(eventActionTmpl(obj));

                    }).fail(function() {
                        window.alert('There was an error.');
                    });
                }
            } else {
                window.alert(reasonAlert);
            }
        });

        $(document).on('click', '#refundRegistration', function(e){
            e.preventDefault();
            var button = $(this);
            var reason = button.next('input[name=reason]').val();
            var userId = $("#userInformation").attr("data-user-id");

            if (reason) {
                if (confirm("Are you sure you want to refund this user's registration?")) {
                    var userData = getUserData();
                    $.ajax({
                        url: 'index.php?option=com_eventbooking&task=registrant.admin_refund_registration&format=raw',
                        data: {
                            userData: userData,
                            reason: reason,
                            userId: userId
                        },
                        type: 'post'
                    }).done(function(result) {

                        var trxn_id = result;
                        if (trxn_id != 'fail' && trxn_id != null) {
                            $.ajax({
                                url: 'index.php?option=com_converge&task=transactions.refundPayment&format=raw',
                                data: {
                                    trxn_id: trxn_id,
                                    reason: reason
                                },
                                type: 'post'
                            }).done(function(result) {
                                if (result) {
                                    console.log(result);
                                }

                                if (result == 'success') {
                                    window.alert('Transaction refunded successfully.');
                                    location.reload(); //refresh so they can now use that card
                                } else {
                                    window.alert('There was an error.');
                                }
                                button.prop('disabled', false);
                            }).fail(function() {
                                window.alert('There was an error.');
                            });
                        }

                    }).fail(function() {
                        window.alert('There was an error.');
                    });
                }
            } else {
                window.alert(reasonAlert);
            }
        });

        $(document).on('click', '.checkInRegistration', function() {
            //console.log(this);
            if (confirm("Are you sure you want to check this user into the event?")) {

                var userData = getUserData();
                var _this = this;
                var which = this.getAttribute("which");

                $.ajax({
                    url: 'index.php?option=com_eventbooking&task=registrant.admin_change_checkin&format=raw',
                    data: {
                        userData: userData,
                        which: which,
                        set: 1
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result) {
                        console.log(result);
                    }

                    //Update the data object
                    obj = eval(itemMapping[userData.itemMappingId]);

                    if (obj.group_data) {
                        for (var i = 0; i < obj.group_data.length; i++) {
                            if (obj.group_data[i].id == which) {
                                obj.group_data[i].checked_in = "Yes";
                                break;
                            }
                        }
                    } else {
                        obj.checked_in = "Yes";
                    }

                    obj.checked_in_count++;

                    $('.displayItem').html(eventActionTmpl(obj));

                }).fail(function() {
                    window.alert('There was an error.');
                });


            }
        });

        $(document).on('click', '.checkOutRegistration', function(){
            if (confirm("Are you sure you want to check this user out of the event?")) {

                var userData = getUserData();
                var _this = this;
                var which = this.getAttribute("which");

                $.ajax({
                    url: 'index.php?option=com_eventbooking&task=registrant.admin_change_checkin&format=raw',
                    data: {
                        userData: userData,
                        which: which,
                        set: 0
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result) {
                        console.log(result);
                    }

                    obj = eval(itemMapping[userData.itemMappingId]);

                    if (obj.group_data) {
                        for (var i = 0; i < obj.group_data.length; i++) {
                            if (obj.group_data[i].id == which) {
                                obj.group_data[i].checked_in = "No";
                                break;
                            }
                        }
                    } else {
                        obj.checked_in = "No";
                    }

                    obj.checked_in_count--;

                    $('.displayItem').html(eventActionTmpl(obj));

                }).fail(function() {
                    window.alert('There was an error.');
                });
            }
        });

        $(document).on('click', '#refundOrder', function(e) {
            e.preventDefault();
            var userData = getUserData();
            var button = $(this);
            var reason = button.next('input[name=reason]').val();
            var userId = $("#userInformation").attr("data-user-id");

            if (reason) {

                $.ajax({
                    url: 'index.php?option=com_eshop&task=admin.refund_order&format=raw',
                    data: {
                        userData: userData,
                        reason: reason,
                        userId: userId
                    },
                    type: 'post'
                }).done(function(result) {
                    var trxn_id = result;

                    if (trxn_id != 'fail' && trxn_id != null) {
                        $.ajax({
                            url: 'index.php?option=com_converge&task=transactions.refundPayment&format=raw',
                            data: {
                                trxn_id: trxn_id,
                                reason: reason
                            },
                            type: 'post'
                        }).done(function(result) {
                            if (result) {
                                console.log(result);
                            }

                            if (result == 'success') {
                                window.alert('Transaction refunded successfully.');
                                location.reload();
                            } else {
                                window.alert('There was an error.');
                            }
                            button.prop('disabled', false);
                        }).fail(function() {
                            window.alert('There was an error.');
                        });
                    } else {
                        window.alert("There was an error.");
                    }
                }).fail(function() {
                    window.alert("There was an error.");
                });
            } else {
                window.alert(reasonAlert);
            }
        });

        $(document).on('click', '#cancelOrder', function(e) {
            e.preventDefault();
            var userData = getUserData();
            var button = $(this);
            var reason = button.next('input[name=reason]').val();

            var userId = $("#userInformation").attr("data-user-id");

            if (reason) {
                $.ajax({
                    url: 'index.php?option=com_eshop&task=admin.cancel_order&format=raw',
                    data: {
                        userData: userData,
                        reason: reason,
                        userId: userId
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result) {
                        console.log(result);
                        if (result == "success") {
                            obj = eval(itemMapping[userData.itemMappingId]);
                            console.log(obj);
                            obj.orderstatus_id = 1;
                            obj.orderstatus_name = "Canceled";

                            $('.displayItem').html(eshopOrderTmpl(obj));
                        }
                    }
                }).fail(function() {
                    window.alert("There was an error.");
                });
            } else {
                window.alert(reasonAlert);
            }
        });

        //when refund is clicked
        $(document).on('click', '#refundTrxn', function(ev) {
            var button = $(this);
            ev.preventDefault();
            ev.stopPropagation();
            //grab yo info
            var trxn_id = button.data('trxn');
            var reason = button.next('input[name=reason]').val();
            if(reason) {
                button.prop('disabled', true);
                $.ajax({
                    url: 'index.php?option=com_converge&task=transactions.refundPayment&format=raw',
                    data: {
                        trxn_id: trxn_id,
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if (result == 'success') {
                        window.alert('Transaction refunded successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                    button.prop('disabled', false);
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when reprocess sub period is clicked
        $(document).on('click', '#reprocessSubPeriod', function(ev) {
            var button = $(this);
            ev.preventDefault();
            ev.stopPropagation();
            //grab yo info
            var sub_per = button.data('sub-perid');
            var sub_id = button.data('subid');
            var user_id = button.data('userid');
            var reason = button.next('input[name=reason]').val();
            if(reason) {
                button.prop('disabled', true);
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.reprocessSubPeriod&format=raw',
                    data: {
                        sub_per: sub_per,
                        sub_id: sub_id,
                        user_id: user_id,
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Subscription reprocessed successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('Subscription reprocessed unsuccessfully.');
                    }
                    button.prop('disabled', false);
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when renew subscription is clicked
        $(document).on('click', '#renewsub', function(ev) {
            var sub = {
                user_id: $(this).data('user'),
                id: $(this).data('subid'),
                desc: $(this).data('desc')
            };
            $('#myModalBody').html('');
            $('#myModalBody').html(renewTmpl(sub));
            $('#myModalLabel').text('Renew Subscription');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success renewSubSave').text('Go');
        });

        //this works for both renew and add subscription
        $(document).on('click', 'input[name=dateopt], input[name=time]', function(ev) {
            var time = $('input[name=time]:checked').val();
            var date_option = $('input[name=dateopt]:checked').val();

            if(!parseInt(date_option)) {
                $('#charge_date').show();
            } else {
                $('#charge_date').hide();
            }

            if(!parseInt(date_option) && !parseInt(time)) {
                $('#mystatus').show();
            } else {
                $('#mystatus').hide();
            }
        });

        $(document).on('click', '.renewSubSave', function() {
            var form = $('#renewForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.renewSubscription&format=raw',
                    data: {
                        user_id: form.data('user'),
                        sub_id: form.data('subid'),
                        desc: form.data('desc'),
                        time: form.find('input[name=time]:checked').val(),
                        dateopt: form.find('input[name=dateopt]:checked').val(),
                        charge_date: form.find('input[name=charge_date]').val(),
                        status: form.find('input[name=status]:checked').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if (result == 'success') {
                        window.alert('Subscription renewed successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when add subscription is clicked
        $(document).on('click', '#addSubscription', function(ev) {
            $('#myModalBody').html('');
            $('#myModalBody').html(addSubTmpl({
                plans : plans,
                cards : cards
            }));
            $('#myModalLabel').text('Add Subscription');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success addSubSave').text('Go');
        });

        //when plan is selected, update initial amount and regular amount
        $(document).on('change', 'select[name=sub_plan]', function(ev) {
            var plan_id = $(this).val();
            plans.forEach(function (plan) {
                if(plan.id == plan_id) {
                    $('input[name=initial_amount]').val(plan.default_initial_amount);
                    $('input[name=regular_amount]').val(plan.default_amount);
                }
            });
        });

        $(document).on('click', '.addSubSave', function(ev) {
            var form = $('#addSubForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.addSubscription&format=raw',
                    data: {
                        user_id: form.data('userid'),
                        plan_id: form.find('select[name=sub_plan]').val(),
                        card_id: form.find('select[name=sub_card]').val(),
                        initial_amount: form.find('input[name=initial_amount]').val(),
                        regular_amount: form.find('input[name=regular_amount]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Subscription added successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.window.alert(reasonAlert);
            }
        });

        //when pause subscription is clicked
        $(document).on('click', '#pausesub', function(ev) {
            var sub = {
                user_id: $(this).data('user'),
                id: $(this).data('subid')
            };
            $('#myModalBody').html('');
            $('#myModalBody').html(pauseTmpl(sub));
            $('#myModalLabel').text('Pause Subscription');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success pauseSubSave').text('Go');
        });

        $(document).on('click', '.pauseSubSave', function(ev) {
            var form = $('#pauseForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.pauseSubscription&format=raw',
                    data: {
                        user_id: form.data('user'),
                        sub_id: form.data('subid'),
                        pause_until: form.find('input[name=pause_until]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Subscription paused successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert(result);
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when adjust pay date is clicked
        $(document).on('click', '#adjustdate', function(ev) {
            var sub = {
                user_id: $(this).data('user'),
                id: $(this).data('subid')
            };
            $('#myModalBody').html('');
            $('#myModalBody').html(adjustDateTmpl(sub));
            $('#myModalLabel').text('Adjust Pay Date');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success adjustDateSave').text('Go');
        });

        $(document).on('click', '.adjustDateSave', function(ev) {
            var form = $('#adjustDateForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.adjustPayDate&format=raw',
                    data: {
                        user_id: form.data('user'),
                        sub_id: form.data('subid'),
                        extend_until: form.find('input[name=extend_until]').val(),
                        status: form.find('input[name=status]:checked').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if (result == 'success') {
                        window.alert('Subscription pay date changed successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert(result);
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when cancel subscription is clicked
        $(document).on('click', '#cancelsub', function(ev) {
            var button = $(this);
            ev.preventDefault();
            ev.stopPropagation();
            var reason = $(this).next('input[name=reason]').val();
            if (reason) {
                button.prop('disabled', true);
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.cancelSubscription&format=raw',
                    data: {
                        user_id: button.data('user'),
                        sub_id: button.data('subid'),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result == 'success') {
                        window.alert('Subscription cancelled successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                    button.prop('disabled', false);
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when sub settings is clicked
        $(document).on('click', '#subsettings', function(ev) {
            ev.preventDefault();
            $('#myModalBody').html('');
            $('#myModalBody').html(subSettingsTmpl(curr_sub));
            $('#myModalLabel').text('Subscription Settings');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success subSettingsSave').text('Go');
        });

        $(document).on('click', '.subSettingsSave', function(ev) {
            var form = $('#subSettingsForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.saveSubscriptionSettings&format=raw',
                    data: {
                        user_id: form.data('user'),
                        sub_id: form.data('subid'),
                        original_amount: form.find('input[name=original_amount]').val(),
                        length: form.find('select[name=length]').val(),
                        term: form.find('select[name=term]').val(),
                        card_id: form.find('select[name=card_id]').val(),
                        points: form.find('input[name=points]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if (result == 'success') {
                        window.alert('Subscription settings saved successfully.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert(result);
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        //when mark paid is clicked
        $(document).on('click', '#markPaidSubPeriod', function() {
            var sub_id = $(this).data('subid');
            var sub_perid = $(this).data('sub-perid');
            $('#myModalBody').html('');
            $('#myModalBody').html(markPaidSubPerTmpl({
                id: sub_perid,
                sub_id: sub_id
            }));
            $('#myModalLabel').text('Mark Paid');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success markPaidSubPeriodSave').text('Go');
        });

        $(document).on('click', '.markPaidSubPeriodSave', function(ev) {
            var form = $('#markPaidForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_converge&task=subscriptions.markPaidSubPeriod&format=raw',
                    data: {
                        sub_perid: form.data('sub-perid'),
                        sub_id: form.data('subid'),
                        payment_type: form.find('select[name=payment_type]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Period was marked paid.');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });
    });
})(jQuery);

//############################ For credit card stuff ############################################
(function($) {
    $(function() {

        //get our handlebars templates ready by compiling them
        //var ccTmpl = Handlebars.compile($('#credit-card-tmpl').html());
        var permissionsTmpl = Handlebars.compile($('#card-permissions-tmpl').html());
        var tokenTmpl = Handlebars.compile($('#card-token-tmpl').html());

        //helpers for the templates
        Handlebars.registerHelper('formatselectedcountry', function(country) {
            var output = '';
            countries.forEach(function(val) {
                var selected = val == country ? 'selected' : '';
                output += '<option value="'+val+'" '+selected+'>'+val+'</option>';
            });
            return new Handlebars.SafeString(output);
        });

        function setCardCountries() {
            var country = $('.card_country').data('country');
            var output = '';
                countries.forEach(function(val) {
                    var selected = val == country ? 'selected' : '';
                    output += '<option value="'+val+'" '+selected+'>'+val+'</option>';
                });
            $('.card_country').html(output);
        }

        setCardCountries();

        function setCardExpMonth() {
            var month = 0;
            /*if(expire) {
                month = expire.substr(0,2); //grab the month from converges crap
            }*/
            var output = '';
            for(var i=1; i<=12; i++) {
                var val = ('00'+i).substr(-2);
                var selected = val == month ? 'selected' : '';
                output += '<option value="'+val+'" '+selected+'>'+val+'</option>';
            }
            $('.exp_month').html(output);
        }

        setCardExpMonth();

        function setCardExpYear() {
            var year = (new Date()).getFullYear();
            /*if(expire) {
                year = '20'+expire.substr(2,2); //grab the month from converges crap and pad it with 00's
            }*/
            var output = '';
            for(var i=(new Date()).getFullYear(); i<=(new Date()).getFullYear()+10; i++) {
                var selected = i == year ? 'selected' : '';
                output += '<option value="'+i+'" '+selected+'>'+i+'</option>';
            }
            $('.exp_year').html(output);
        }

        setCardExpYear();

        Handlebars.registerHelper('formatmonth', function(expire) {
            var month = 0;
            if(expire) {
                month = expire.substr(0,2); //grab the month from converges crap
            }
            var output = '';
            for(var i=1; i<=12; i++) {
                var val = ('00'+i).substr(-2);
                var selected = val == month ? 'selected' : '';
                output += '<option value="'+val+'" '+selected+'>'+val+'</option>';
            }
            return new Handlebars.SafeString(output);
        });
        Handlebars.registerHelper('formatyear', function(expire) {
            var year = (new Date()).getFullYear();
            if(expire) {
                year = '20'+expire.substr(2,2); //grab the month from converges crap and pad it with 00's
            }
            var output = '';
            for(var i=(new Date()).getFullYear(); i<=(new Date()).getFullYear()+10; i++) {
                var selected = i == year ? 'selected' : '';
                output += '<option value="'+i+'" '+selected+'>'+i+'</option>';
            }
            return new Handlebars.SafeString(output);
        });
        Handlebars.registerHelper('formatfamilychecked', function(id) {
            if(Array.isArray(this.family_access) && this.family_access.indexOf(id) > -1) {
                return 'checked';
            }
        });

        $(document).on('click', '.addCard ', function() {
            //$('#myModalBody').html('');
            //$('#myModalBody').html(ccTmpl(user));
            $('#cardModalLabel').text('Add New Card');
            //reset class so the form submits correctly
            $('#cardModal').find('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success addCardSave').text('Save');
        });

        function addSaveCard() {
            var form_data = $('#saveCardForm').serialize();
            form_data += '&user_id='+$('#saveCardForm').data('user');

            $.ajax({
                url: 'index.php?option=com_converge&task=cards.saveCard&format=raw',
                data: form_data,
                type: 'post'
            }).done(function(result) {
                console.log(result)
                if(result == 'success') {
                    window.alert('Card Added successfully.');
                    location.reload(); //refresh so they can now use that card
                } else {
                    window.alert('There was an error, please make sure all the information is correct.');
                }
            }).fail(function() {
                window.alert('There was an error, please make sure all the information is correct.');
            });
        }

        $(document).on('click', '.addSaveAward', function() {
            addSaveAward();
        });

        function addSaveAward() {
            var form_data = $('#saveAwardForm').serialize();
            $.ajax({
                url: 'index.php?option=com_axs&task=awards.addNewAward&format=raw',
                data: form_data,
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    window.alert('Certificate or Badge Added successfully.');
                    location.reload(); //refresh so they can now use that card
                } else {
                    window.alert(result);
                }
            }).fail(function() {
                window.alert('There was an error, please make sure all the information is correct.');
            });
        }

        $(document).on('click', '.editAward', function() {
            var edit_date_earned   = $(this).data('award_earned');
            var award_title        = $(this).data('award_title');
            var edit_date_expires  = $(this).data('award_expires');
            var edit_award_user_id = $(this).data('user_id');
            var edit_award_id      = $(this).data('award_id');
            $('input[name="edit_date_earned"]').val(edit_date_earned);
            $('input[name="edit_date_expires"]').val(edit_date_expires);
            $('input[name="edit_award_user_id"]').val(edit_award_user_id);
            $('input[name="edit_award_id"]').val(edit_award_id);
            $('.editAwardTitle').text(award_title);
        });

        $(document).on('click', '.editSaveAward', function() {
            editSaveAward();
        });

        function editSaveAward() {
            var form_data = $('#editAwardForm').serialize();
            $.ajax({
                url: 'index.php?option=com_axs&task=awards.editAward&format=raw',
                data: form_data,
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    window.alert('Certificate or Badge Edited successfully.');
                    location.reload(); //refresh so they can now use that card
                } else {
                    window.alert(result);
                }
            }).fail(function() {
                window.alert('There was an error, please make sure all the information is correct.');
            });
        }

        $(document).on('click', '.deleteAward', function() {
            var award_id = $(this).data('award_id');
            var user_id  = $(this).data('user_id');
            var row      = $(this).parent().parent();
            var data = {
                award_id : award_id,
                user_id : user_id
            }

            if (confirm('Are you sure you want to delete this Certificate or Badge?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=awards.deleteAward&format=raw',
                    type: 'post',
                    data: data
                }).done(function (result) {
                    if (result == 'success') {
                        window.alert('Certificate or Badge deleted successfully.');
                        row.remove();
                    } else {
                        window.alert('There was an error deleting your card.');
                    }
                }).fail(function () {
                    window.alert("An Error occurred.");
                });
            }
        });



        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('saveCardForm');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
            var hiddenInputLastFour = document.createElement('input');
            hiddenInputLastFour.setAttribute('type', 'hidden');
            hiddenInputLastFour.setAttribute('name', 'last4');
            hiddenInputLastFour.setAttribute('value', token.card.last4);
            form.appendChild(hiddenInputLastFour);
            addSaveCard();
        }


        $(document).on('click', '.addCardSave', function() {
            var gateway = $('#saveCardForm').data('gtw');
            if(gateway == 'stripe') {
                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                      // Inform the user if there was an error.
                      var errorElement = document.getElementById('card-errors');
                      errorElement.textContent = result.error.message;
                    } else {
                        console.log(result);
                      // Send the token to your server.
                       stripeTokenHandler(result.token);
                    }
                });
            } else {
                addSaveCard();
            }
        });

        $(document).on('click', '.updateCard', function() {
            $('#myModalBody').html('');
            var card_id = this.dataset.cardid;
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.updateCard&format=raw',
                data: {
                    card_id: card_id
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'error') {
                    window.alert('There was an error trying to get the card.');
                } else {
                    result = JSON.parse(result);
                    $('#myModalBody').html(ccTmpl(result));
                }
            }).fail(function() {
                window.alert('There was an error trying to get the card.');
            });

            $('#myModalLabel').text('Update Card Info');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success updateCardSave').text('Update');
    });

        $(document).on('click', '.updateCardSave', function() {
            //grab the new settings and send to php with ajax
            var form_data = $('#saveCardForm').serialize();
            form_data += '&card_id='+$('#saveCardForm').data('cardid');

            var reason = $('#saveCardForm input[name=reason]').val();
            if(reason) {
                $.ajax({
                    url: 'index.php?option=com_converge&task=cards.updateCardSave&format=raw',
                    data: form_data,
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert("Card updated successfully.");
                    } else {
                        window.alert('An Error occurred.');
                    }
                }).fail(function() {
                    window.alert("An Error occurred.");
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        $(document).on('click', '.editCard', function() {
            $('#myModalBody').html('');

            var card_id = this.dataset.cardid;
            $.ajax({
                url: 'index.php?option=com_converge&task=cards.editCardPermissions&format=raw',
                data: {
                    card_id: card_id
                },
                type: 'post'
            }).done(function(result) {
                result = JSON.parse(result);
                $('#myModalBody').html(permissionsTmpl(result));
            }).fail(function() {
                window.alert('An Error occurred.');
            });

            $('#myModalLabel').text('Edit Card Permissions');
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success editCardSave').text('Save');
        });

        $(document).on('click', '.editCardSave', function() {
            //grab the new settings and send to php with ajax
            var card_id = $('#saveCardPermissionsForm').data('cardid');
            $(this).prop('disabled', true);

            $.ajax({
                url: 'index.php?option=com_converge&task=cards.editCardPermissionsSave&format=raw',
                data: {
                    card_id: card_id,
                    formData: $('#saveCardPermissionsForm').serialize()
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    window.alert("Card Settings Saved!");
                    location.reload();
                } else {
                    window.alert('Error trying to set permissions for this card.');
                }
            }).fail(function() {
                window.alert("Error trying to set permissions for this card.");
            });
        });

        $(document).on('click', '.updateToken', function() {
            $('#myModalBody').html('');
            $('#myModalLabel').text('Update Token');

            var card_id = parseInt(this.dataset.cardid);
            var old_card = cards.find(function(card) {
                return card.id == card_id;
            });
            $('#myModalBody').html(tokenTmpl(old_card));
            //reset class so the form submits correctly
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success updateTokenSave').text('Update');
        });

        $(document).on('click', '.updateTokenSave', function() {
            var card_id = $('#updateTokenForm').data('cardid');
            var token = $('#updateTokenForm input[name=newToken]').val();
            $(this).prop('disabled', true);

            $.ajax({
                url: 'index.php?option=com_converge&task=cards.updateToken&format=raw',
                data: {
                    card_id: card_id,
                    token: token
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    window.alert("Card Settings Saved!");
                    location.reload();
                } else {
                    window.alert("Error.");
                }
            }).fail(function() {
                window.alert("Error.");
            });
        });

        $(document).on('click', '.deleteCard', function() {
            var card_id = this.dataset.cardid;
            var row = $(this).parent().parent();

            if (confirm('Are you sure you want to delete this card?')) {
                $.ajax({
                    url: 'index.php?option=com_converge&task=cards.deleteCard&format=raw&card_id='+card_id,
                    type: 'get'
                }).done(function (result) {
                    if (result == 'success') {
                        window.alert('Card deleted successfully.');
                        row.remove();
                    } else {
                        window.alert('There was an error deleting your card.');
                    }
                }).fail(function () {
                    window.alert("An Error occurred.");
                });
            }
        });
    });
})(jQuery);

//############################ For rewards points and commissions ###############################
(function($) {
    $(function() {
        var referralCodeTmpl = Handlebars.compile($('#referral-code-tmpl').html());
        var addCommissionTmpl = Handlebars.compile($('#add-commission-tmpl').html());
        var changeCommissionTmpl = Handlebars.compile($('#change-commission-tmpl').html());

        $(document).on('click', '.adjustPoints', function() {
            var user_id = this.dataset.user;
            var points = $('input[name=adjustPoints]').val();
            var reason = $('input[name=adjustReason]').val();

            if(reason == "") {
                window.alert('You must give a reason, please think about what you\'re doing');
            } else {
                $(this).prop('disabled', true);
                //grab the new settings and send to php with ajax
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.adjustPoints&format=raw',
                    data: {
                        user_id: user_id,
                        points: points,
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result == 'success') {
                        window.alert('Points adjusted!');
                        location.reload(); //refresh page
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            }
        });

        $(document).on('click', '.deleteCode', function() {
            var code_id = this.dataset.code;
            var row = $(this).parent().parent();

            if (confirm('Are you sure you want to delete this referral code?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.deleteCode&format=raw',
                    data: {
                        code_id: code_id
                    },
                    type: 'post'
                }).done(function (result) {
                    if (result == 'success') {
                        window.alert('Code deleted successfully.');
                        row.remove();
                    } else {
                        window.alert('There was an error deleting your code.');
                    }
                }).fail(function () {
                    window.alert("An Error occurred.");
                });
            }
        });

        $(document).on('click', '.addCode', function() {
            $('#myModalBody').html('');
            $('#myModalLabel').text('Add New Code');
            $('#myModalBody').html(referralCodeTmpl());
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success saveCode').text('Save');
        });

        $(document).on('keyup', '#saveCodeForm input[name=code]', function() {
            var group = $('#saveCodeForm .code-group');
            var message = $('#saveCodeForm .code-message');
            var value =  this.value;

            if(value.length < 4) {
                group.removeClass('has-success').addClass('has-error');
                message.text('Must be four characters or more.');
            } else if(!isNaN(value)) {
                group.removeClass('has-success').addClass('has-error');
                message.text('Codes can not be numbers.');
            } else {
                group.removeClass('has-error').addClass('has-success');
                message.text('');

                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.checkCode&format=raw',
                    data: {
                        code: value
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result != 'success') {
                        group.addClass('has-error');
                        message.text('This referral code is already taken.');
                    }
                }).fail(function() {
                    window.alert('There was an error trying to check your code.');
                });
            }
        });

        $(document).on('click', '.saveCode', function() {
            var user_id = $('#saveCodeForm').data('user');
            var code = $('#saveCodeForm input[name=code]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.saveCode&format=raw',
                data: {
                    user_id: user_id,
                    code: code
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    window.alert('Code added successfully.');
                    location.reload();
                } else {
                    window.alert('There was an error trying to add your code, ' + result);
                }
            }).fail(function() {
                window.alert('There was an error trying to add your code.');
            });
        });

        $(document).on('click', '#loadMoreRewards', function() {
            $(this).prop('disabled', true);
            $(this).html('<img src="../images/ajax-loader.gif" /> Load More');
            var button = $(this);
            var uid = $(this).data('user');
            var page = $(this).data('page');
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.loadMoreRewards&format=raw',
                data: {
                    uid: uid,
                    page: page
                },
                type: 'post'
            }).done(function(result) {
                $(result).insertBefore(button.parent().parent());
                button.html('Load More');
                button.prop('disabled', false);
                button.data('page', page+1);
            }).fail(function() {
                window.alert('There was an error fetching your rewards.');
                button.html('Load More');
                button.prop('disabled', false);
            });
        });

        $(document).on('click', '#addCommission', function() {
            $('#myModalBody').html('');
            $('#myModalLabel').text('Add New Commission');
            $('#myModalBody').html(addCommissionTmpl());
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success addCommissionSave').text('Save');
        });

        $(document).on('click', '.addCommissionSave', function() {
            var form = $('#addCommissionForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.addCommission&format=raw',
                    data: {
                        user_id: form.data('user'),
                        referee_id: form.find('input[name=referee_id]').val(),
                        amount: form.find('input[name=commission_amount]').val(),
                        status: form.find('select[name=status]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Commission Added!');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });

        $(document).on('click', '.changeCommissionStatus', function() {
            $('#myModalBody').html('');
            $('#myModalLabel').text('Change Commission Status');
            $('#myModalBody').html(changeCommissionTmpl({ id: $(this).data('id') }));
            $('.modal-footer .btn-success').prop('disabled', false).removeClass().addClass('btn btn-success changeCommissionSave').text('Save');
        });

        $(document).on('click', '.changeCommissionSave', function() {
            var form = $('#changeCommissionStatusForm');
            var reason = form.find('input[name=reason]').val();
            if(reason != '') {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.changeCommissionStatus&format=raw',
                    data: {
                        commission_id: form.data('commid'),
                        status: form.find('select[name=status]').val(),
                        reason: reason
                    },
                    type: 'post'
                }).done(function(result) {
                    if (result == 'success') {
                        window.alert('Commission Status Changed!');
                        location.reload(); //refresh so they can now use that card
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            } else {
                //prevent action and pop up an alert
                window.alert(reasonAlert);
            }
        });
    });
})(jQuery);

//############################ For User Connections, will be changed eventually #################
(function($) {
    $(function() {
        $(document).on('click', '.removeReferee', function() {
            var uid = this.dataset.user;
            if(confirm('Are you sure you want to remove this user from their referees?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.removeReferee&format=raw',
                    data: {
                        uid: uid
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if(result == 'success') {
                        window.alert('User removed successfully.');
                        location.reload();
                    } else {
                        window.alert('There was an error.');
                    }
                }).fail(function() {
                    window.alert('There was an error.');
                });
            }
        });

        $(document).on('click', '.moveReferee', function() {
            var uid = this.dataset.user;

            $('#myModalLabel').text('Move user under someone else');
            var clone = $('#moveReferreeForm').clone().addClass('moveReferreeFormClone').data('user', uid);
            $('#myModalBody').html(clone);
            $('.moveReferreeFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success moveRefereeSave').text('Save').prop('disabled', true);
        });

        $(document).on('click', '.moveRefereeSave', function() {
            var uid = $('.moveReferreeFormClone').data('user');
            var pid = $('.moveReferreeFormClone input[name=parent]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.moveReferee&format=raw',
                data: {
                    uid: uid,
                    pid: pid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    window.alert('User moved successfully.');
                    location.reload();
                } else {
                    window.alert('There was an error.');
                }
            }).fail(function() {
                window.alert('There was an error.');
            });
        });

        $(document).on('click', '.addReferee', function() {
            $('#myModalLabel').text('Add a referee under this user');
            var clone = $('#addReferreeForm').clone().addClass('addReferreeFormClone');
            $('#myModalBody').html(clone);
            $('.addReferreeFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success addRefereeSave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.addRefereeSave', function() {
            var uid = $('.addReferreeFormClone').data('user');
            var cid = $('.addReferreeFormClone input[name=child]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.addReferee&format=raw',
                data: {
                    uid: uid,
                    cid: cid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    window.alert('User added successfully.');
                    location.reload();
                } else {
                    window.alert('There was an error.');
                }
            }).fail(function() {
                window.alert('There was an error.');
            });
        });

        $(document).on('click', '.changeReferrer', function() {
            $('#myModalLabel').text('Change this users referrer');
            var clone = $('#changeReferrerForm').clone().addClass('changeReferrerFormClone');
            $('#myModalBody').html(clone);
            clone.show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success changeReferrerSave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.changeReferrerSave', function() {
            var uid = $('.changeReferrerFormClone').data('user');
            var pid = $('.changeReferrerFormClone input[name=parent]').val();

            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.changeReferrer&format=raw',
                data: {
                    uid: uid,
                    pid: pid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    window.alert('User parent changed successfully.');
                    location.reload();
                } else {
                    window.alert('There was an error.');
                }
            }).fail(function() {
                window.alert('There was an error.');
            });
        });

        $(document).on('keyup', '.changeReferrerFormClone, .addReferreeFormClone, .moveReferreeFormClone, .addFamilyFormClone', function(e) {
            console.log('calling validate move on keyup');
            setTimer(this);
        });

        $(document).on('keypress', '.changeReferrerFormClone, .addReferreeFormClone, .moveReferreeFormClone, .addFamilyFormClone', function(e) {
            //console.log('Pressed a key: ' + e.which);
            if(e.which == 13){
                e.preventDefault();
                $(this).parents('.modal-content').find('.modal-footer .btn-success').click();
            }
        });

        var timer = null;

        function setTimer(sourceEl) {
            //kill last timer and start new timer
            if(timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                validateMove(sourceEl);
            }, 500);
        }

        function validateMove(sourceEl) {
            console.log('validateMove() was called');
            var modalEl = $(sourceEl).parents('.modal-content');
            var inputGroupEl = modalEl.find('.form-group');
            var inputEl = modalEl.find('input');
            var messageEl = modalEl.find('.code-message');
            var saveEl = modalEl.find('.modal-footer .btn-success');
            var inputName = inputEl.attr('name');
            var inputValue = inputEl.val();
            var canBeZero = false;
            switch(inputName) {
                case 'parent':
                    canBeZero = true;
                    break;
                case 'child':
                default:
                    canBeZero = false;
                    break;
            }

            saveEl.prop('disabled', true);

            if (inputValue === '') {
                inputGroupEl.removeClass('has-success').addClass('has-error');
                messageEl.text('User does not exist');
                return false;
            } else if (parseInt(inputValue) === 0) {
                if (canBeZero) {
                    messageEl.text('Set User to have no referrer.');
                    inputGroupEl.removeClass('has-error').addClass('has-success');
                    saveEl.prop('disabled', false);
                    return true;
                } else {
                    inputGroupEl.removeClass('has-success').addClass('has-error');
                    messageEl.text('User can not be zero.');
                    return false;
                }
            } else {
                //run the ajax to check if it's a real person and update the message text
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.checkUser&format=raw&uid='+inputValue,
                    type: 'get'
                }).done(function(result) {
                    //console.log(result);
                    if(result != 'error') {
                        messageEl.text(result);
                        inputGroupEl.removeClass('has-error').addClass('has-success');
                        saveEl.prop('disabled', false);
                        return true;
                    } else {
                        messageEl.text('User does not exist');
                        inputGroupEl.removeClass('has-success').addClass('has-error');
                        saveEl.prop('disabled', true);
                        return false;
                    }
                }).fail(function() {
                    window.alert('Something went wrong while checking for user, refresh and try again or contact tech support.');
                    return false;
                });
            }
        }

        $(document).on('click', '.addFamily', function() {
            $('#myModalLabel').text('Add a family member under this user');
            var clone = $('#addFamilyForm').clone().addClass('addFamilyFormClone');
            $('#myModalBody').html(clone);
            $('.addFamilyFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success addFamilySave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.addFamilySave', function() {
            $(this).prop('disabled', true);
            var uid = $('.addFamilyFormClone').data('user');
            var fid = $('.addFamilyFormClone input[name=family]').val();
            var rel = $('.addFamilyFormClone select[name=relationship]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.addFamily&format=raw',
                data: {
                    uid: uid,
                    fid: fid,
                    rel: rel
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    window.alert('User added successfully.');
                    location.reload();
                } else {
                    window.alert('There was an error.');
                }
            }).fail(function() {
                window.alert('There was an error.');
            });
        });
    });
})(jQuery);

//############################ For courses, will be changed eventually ##########################
(function($) {
    $(function() {
        var coursePurchaseProgress = Handlebars.compile($('#course-purchase-progress-tmpl').html());

        Handlebars.registerHelper('formatquizresults', function(quiz_results) {
            var ret = '';
            quiz_results.forEach(function(res) {
                var percentage = res.point/res.total_marks * 100;
                ret += '<tr><td>' + res.lesson_title + '</td><td>' + res.quiz_title + '</td><td>' + res.point + ' / ' + res.total_marks + '</td><td>' + percentage + '%</td></tr>';
            });
            return new Handlebars.SafeString(ret);
        });

        $(document).on('click', '.purchaseCourse', function() {
            $('#myModalLabel').text('Purchase Course');

            var clone = $('#upgradeLevelForm').clone().addClass('upgradeLevelFormClone');
            $('#myModalBody').html(clone);
            $('.upgradeLevelFormClone').show();

            //reset classes so the form submits correctly
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success purchaseCourseSave').text('Purchase Course');
        });

        $(document).on('change', '.upgradeLevelFormClone select[name=course_id]', function() {
            var course_id = $(this).val();
            $.each(courses, function() {
                if(this.splms_course_id == course_id) {
                    $('.upgradeLevelFormClone input[name=courseAdjustedAmount]').val(this.price);

                    //if there is any payment involved, we show the payment fields
                    if(parseInt(this.price)) {
                        $('.upgradeLevelFormClone .levelPaymentDetails').slideDown();
                    } else {
                        $('.upgradeLevelFormClone .levelPaymentDetails').slideUp();
                    }
                }
            });
        });

        $(document).on('change', '.addCourseProgressFormClone select[name=course_id]', function() {
            let course_id = $(this).val();
            let dateStarted = $('.addCourseProgressFormClone #dateStarted');
            let dateCompleted = $('.addCourseProgressFormClone #dateCompleted');

            if(course_id == 'Select Course') {

                dateStarted.slideUp();
                dateCompleted.slideUp();
            } else {

                dateStarted.slideDown();
                dateCompleted.slideDown();
            }
        });

        $(document).on('click', '.purchaseCourseSave', function() {
            //some validation
            var course_id = $('.upgradeLevelFormClone select[name=course_id]').val();
            var card = $('.upgradeLevelFormClone select[name=course_card]').val();
            var amount = $('.upgradeLevelFormClone input[name=courseAdjustedAmount]').val();
            var uid = $('.upgradeLevelFormClone').data('user');

            $(this).prop('disabled', true);
            $.ajax({
                url: 'index.php?option=com_converge&task=transactions.purchaseCourse&format=raw',
                data: {
                    user_id: uid,
                    course_id: course_id,
                    card: card,
                    amount: amount
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    window.alert('Course purchased successfully.');
                    location.reload(); //refresh page
                } else {
                    window.alert('There was an error.');
                    console.log(result);
                    $('.purchaseCourseSave').prop('disabled', false);
                }
            }).fail(function() {
                window.alert('There was an error.');
                $('.purchaseCourseSave').prop('disabled', false);
            });
        });

        // Set up the add progress dialog and functions
        $(document).on('click', '.addCourseProgress', function() {
            $('#myModalLabel').text('Add Course Progress');

            var clone = $('#addCourseProgressForm').clone().addClass('addCourseProgressFormClone');
            $('#myModalBody').html(clone);

            $('.addCourseProgressFormClone #dateStarted').hide();
            $('.addCourseProgressFormClone #dateCompleted').hide();

            $('.addCourseProgressFormClone').show();

            //reset classes so the form submits correctly
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success addCourseProgressSave').text('Add Course Progress');
        });

        /*
        $(document).on('change', '.addCourseProgressFormClone select[name=course_id]', function() {
            var course_id = $(this).val();
            $.each(courses, function() {
                if(this.splms_course_id == course_id) {
                    $('.addCourseProgressFormClone input[name=courseAdjustedAmount]').val(this.price);
                }
            });
        });*/

        $(document).on('click', '.addCourseProgressSave', function() {
            //some validation
            var course_id = $('.addCourseProgressFormClone select[name=course_id]').val();
            var user_id = $('.addCourseProgressFormClone').data('user-id');

            var start_date = $(".addCourseProgressFormClone input[name='date_started']").val();
            var complete_date = $(".addCourseProgressFormClone input[name='date_completed']").val();

            $(this).prop('disabled', true);
            $.ajax({
                url: 'index.php?option=com_splms&task=courseprogress.addCourseProgressEntry&format=raw',
                data: {
                    user_id: user_id,
                    course_id: course_id,
                    start_date: start_date,
                    complete_date: complete_date
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result) {
                    window.alert('Course progress added successfully.');
                    location.reload(); //refresh page
                } else {
                    window.alert('There was an error.');
                    $('.addCourseProgressSave').prop('disabled', false);
                }
            }).fail(function() {
                window.alert('There was an error. Failed to load network resource.');
                $('.addCourseProgressSave').prop('disabled', false);
            });
        });

        // User clicked the "View Progress" button in a Purchase Course row
        $(document).on('click', '.viewCourseProgress', function() {
            $('#myModalBody').html('');
            $('#myModalLabel').text('Course Progress');
            var cpid = $(this).data('cpid');
            $.ajax({
                url: 'index.php?option=com_splms&task=quizresults.courseProgress&format=raw',
                data: {
                    cpid: cpid
                },
                type: 'post'
            }).done(function(result) {
                try {
                    result = JSON.parse(result);
                    $('#myModalBody').html(coursePurchaseProgress(result));
                } catch(e) {
                    window.alert('There was an error getting data.');
                }
            }).fail(function() {
                window.alert('There was an error getting data.');
            });

            $('.modal-footer .btn-success').hide();
        });

        // User clicked on the "Complete Course" button in a Course Progress row
        $(document).on('click', '.completeCourse', function() {

            var wantsToCompleteCourse = confirm("Are you sure you want to mark this lesson as completed?");

            if(wantsToCompleteCourse) {
                var course_id = $(this).data('course-id');
                var user_id = $(this).data('user-id');
                var start_date = $(this).data('start-date');
                var complete_date = $(this).data('completed-date');

                $.ajax({
                    url: 'index.php?option=com_splms&task=courseProgress.completeCourse&format=raw',
                    data: {
                        user_id: user_id,
                        course_id: course_id,
                        start_date: start_date,
                        complete_date: complete_date
                    },
                    type: 'post'
                }).done(function(result) {
                    try {
                        result = JSON.parse(result);
                        if(result) {
                            if(!window.alert('Course successfully marked as complete!')) {
                                window.location.reload();
                            }
                        }
                    } catch(e) {
                        window.alert('There was an error getting data.');
                    }
                }).fail(function() {
                    window.alert('There was an error getting data.');
                });
            }
        });

        // User clicked on the "Archive" button in a Course Progress row
        $(document).on('click', '.archiveCourseProgress', function() {

            var wantsToArchiveCourseProgress = confirm("Are you sure you want to archive this users' course progress? All of their lesson activity responses, quizzes, and lesson progress associated with this course will be archived as well.");

            if(wantsToArchiveCourseProgress) {
                var course_id = $(this).data('course-id');
                var user_id = $(this).data('user-id');

                $.ajax({
                    url: 'index.php?option=com_splms&task=courseProgress.archiveCourseProgress&format=raw',
                    data: {
                        user_id: user_id,
                        course_id: course_id,
                    },
                    type: 'post'
                }).done(function(result) {
                    try {
                        result = JSON.parse(result);
                        if(result) {
                            if(!window.alert('Course progress archived! To view the archived data create a report with the show archived data option enabled.')) {
                                window.location.reload();
                            }
                        } else {
                            window.alert('Course progress archival failed. Please file a support request.');
                        }
                    } catch(e) {
                        console.error(e);
                        window.alert('There was an error getting data.');
                    }
                }).fail(function() {
                    window.alert('There was an error getting data.');
                });
            }
        });
    });
})(jQuery);
