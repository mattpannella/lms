const alert = `
    <div class="alert alert-warning block-alert" role="alert" style="display:none;">
        <i class="fas fa-exclamation-triangle" style="color:#ceb674; margin-right: 10px;"></i> <strong>Attention</strong>
        <p>This user does not have access to your LMS. Although you will be able to see their historical data in Tovuti.</p>
    </div>`;
$('#jform_block').parent().append(alert);
function checkBlock() {
    if($('#jform_block1').is(':checked')) {
        $('.block-alert').show();
    } else {
        $('.block-alert').hide();
    }
}
/* function checkTeacher() {
    if($('#jform_teacher1').is(':checked')) {
        $('#jform_teacher_title-lbl, #jform_teacher_title').show();
    } else {
        $('#jform_teacher_title-lbl, #jform_teacher_title').hide();
    }
} */
$('.userblock-input').change(function(){checkBlock()});
//$('.teacher-input').change(function(){checkTeacher()});
checkBlock();
//checkTeacher();
