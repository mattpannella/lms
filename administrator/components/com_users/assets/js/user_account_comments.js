$('.comment-btn-publish').click(function() {
    var comment_id     = $(this).data('comment');
    var comment_enabled = $(this).data('enabled');
    var comment_button = $(this);
    var comment_button_text = $(this).find('.comment-btn-text');
    $.ajax({
        type: 	'POST',
        url:    'index.php',
        data:   {
            'option': 'com_axs',
            'task': 'comments.commentPublish',
            'format': 'raw',
            'comment_id': comment_id,
            'comment_enabled': comment_enabled
        }
    }).done(function(response){
        var result = JSON.parse(response);
        comment_button.data('enabled',result.new_enabled);
        if(result.status == "error") {
            alert(result.message);
        }
        if(result.new_enabled == "published") {
            var addBtnType    = "btn-danger";
            var removeBtnType = "btn-success";
            var btnText       = "Unpublish";
        } else {
            var addBtnType    = "btn-success";
            var removeBtnType = "btn-danger";
            var btnText       = "Publish";
        }
        comment_button.addClass(addBtnType);
        comment_button.removeClass(removeBtnType);
        comment_button_text.text(btnText);
    });
});

$('.comment-btn-delete').click(function() {
    var comment_id     = $(this).data('comment');
    var comment_enabled = $(this).data('enabled');
    var comment_container = $(this).parent().parent();
    $.ajax({
        type: 	'POST',
        url:    'index.php',
        data:   {
            'option': 'com_axs',
            'task': 'comments.commentDelete',
            'format': 'raw',
            'comment_id': comment_id,
            'comment_enabled': comment_enabled
        }
    }).done(function(response){
        var result = JSON.parse(response);
        if(result.status == "error") {
            alert(result.message);
        }
        if(result.status == "success") {
            comment_container.remove();
        }
    });
});