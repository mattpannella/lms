<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 3/10/16
 * Time: 5:14 PM
 */

defined('_JEXEC') or die;

class UserExtrasHelper {

    public static function profile($bundle) {
        $lang = JFactory::getLanguage();

        $data = $bundle->data;
        $data->link = "index.php?option=com_community&view=users&layout=edit&id=" . $bundle->id;
        switch ($data->gender) {
            case "COM_COMMUNITY_MALE":
                $data->gender = "Male";
                break;
            case "COM_COMMUNITY_FEMALE":
                $data->gender = "Female";
                break;
        }
        //Remove dates that are time code "0"
        if ($data->birthdate == "1970-1-1 23:59:59") {
            $data->birthdate = "";
        }
        $data->affiliate_level = $bundle->affiliate_level;
        ?>
        <strong><div class='table_title'>Profile <a href="<?php echo $data->link; ?>">Edit</a></div></strong>
        <table class='table table-striped table-bordered table-hover'>
            <?php
                $profileFields = AxsUser::getUserCustomFields($bundle->id);
                foreach($profileFields as $field) {
                    if(strpos($field->name,'COM_')) {
                        $field->name = JText::_($field->name);
                    }
                    switch ($field->value) {
                        case "COM_COMMUNITY_MALE":
                            $field->value = "Male";
                            break;
                        case "COM_COMMUNITY_FEMALE":
                            $field->value = "Female";
                            break;
                    }
                    echo  '<tr><td> '.$field->name.':</td><td>'.$field->value.'</td></tr>';
                }
            ?>
        </table>
        <?php
    }

    //TODO: eventually change to fancy chart
    public static function usertree($bundle) {
        $family = $bundle->family;
        $referrer = $bundle->rewards_data->referrer_id;
        $referees = $bundle->referees;
        $total = 0;
        $subsCounts = array();

        ob_start();
        if (count($referees)) {
            foreach ($referees as $r) {
                $total++;
                $profile = AxsExtra::getUserProfileData($r->user_id);
                $userSubs = array();
                $subs = AxsPayment::getUserSubscriptions($r->user_id);
                foreach($subs as $s) {
                    $plan = $s->getPlan();
                    if($s->status == "ACT") {
                        if(!isset($subsCounts[$plan->id])) {
                            $subsCounts[$plan->id] = array(
                              'title' => $plan->title,
                              'count' => 1
                            );
                        } else {
                            $subsCounts[$plan->id]['count']++;
                        }
                    }

                    $userSubs[] = $plan->title . " [" . $s->getStatusName() . "]";
                } ?>
                <tr>
                    <td>
                        <img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;">
                        <a href="/administrator/index.php?option=com_users&task=axsuser.edit&id=<?php echo $r->user_id; ?>"><?php echo $r->name; ?></a>
                    </td>
                    <td><?php echo AxsExtra::format_date($profile->signup); ?></td>
                    <td><?php echo implode(', ', $userSubs); ?></td>
                    <td>
                        <button
                            type="button"
                            class="moveReferee btn btn-sm btn-primary"
                            style="margin-right: 5px;"
                            data-bs-toggle="modal"
                            data-bs-target="#myModal"
                            data-user="<?php echo $r->user_id; ?>"><span class="icon-pencil-2"></span> Move
                        </button>
                        <button
                            type="button"
                            class="removeReferee btn btn-sm btn-danger float-end"
                            style="margin-right: 5px;"
                            data-user="<?php echo $r->user_id; ?>"><span class="icon-trash"></span> Drop
                        </button>
                    </td>
                </tr>
            <?php } ?>
        <?php } else{ ?>
            <tr><td colspan="4" style="text-align: center;">User has not referred anyone.</td></tr>
        <?php }
        $table = ob_get_clean();
        ?>
        <div class="mysection">
            <strong><div class='table_title'>Family Members</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Name</th><th>Sign-up Date</th><th>Subscriptions</th></tr>
                <?php if (count($family)) {
                    $first = true;
                    foreach ($family as $fid) {
                        $profile = AxsExtra::getUserProfileData($fid);
                        $subs = AxsPayment::getUserSubscriptions($fid);
                        $s = array();
                        foreach($subs as $sub) {
                            $p = $sub->getPlan();
                            $s[] = $p->title . " [" . $sub->getStatusName() . "]";
                        }
                        ?>
                        <tr>
                            <td>
                                <img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;">
                                <?php if ($fid == $bundle->id) { ?>
                                    <b>This User</b>
                                <?php } else {
                                    echo self::get_link($fid);
                                }

                                if ($first) {
                                    echo ' - Primary';
                                    $first = false;
                                } else {
                                    echo ' - ' . AxsExtra::getUserExtras($fid)->primary_family_member_relationship;
                                } ?>
                            </td>
                            <td><?php echo AxsExtra::format_date($profile->signup); ?></td>
                            <td><?php echo implode(', ', $s); ?></td>
                        </tr>
                    <?php } ?>
                <?php }
                else
                { ?>
                    <tr>
                        <td colspan="3" style="text-align: center;">User has no family to display
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <button
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                class="btn btn-sm btn-success addFamily"
                style="margin-right: 5px;"
                type="button"
            >
                <span class="icon-plus"></span> Add Family Member
            </button>
        </div>

        <div class="mysection">
            <strong>
                <div class='table_title'>User's Referrer</div>
            </strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr>
                    <th>Name</th>
                    <th>Sign-up Date</th>
                    <th>Subscriptions</th>
                </tr>
                <?php if ($referrer) {
                    $profile = AxsExtra::getUserProfileData($referrer);
                    $subs = AxsPayment::getUserSubscriptions($referrer);
                    $s = array();
                    foreach($subs as $sub) {
                        $p = $sub->getPlan();
                        $s[] = $p->title . " [" . $sub->getStatusName() . "]";
                    }
                    ?>
                    <tr>
                        <td><img src="<?php if($profile->photo) echo $profile->photo; else echo '/images/user.png'; ?>" style="width: 50px;"> <?php echo self::get_link($referrer); ?></td>
                        <td><?php echo AxsExtra::format_date($profile->signup); ?></td>
                        <td><?php echo implode(', ', $s); ?></td>
                    </tr>
                <?php } else { ?>
                    <tr>
                        <td colspan="3" style="text-align: center;">User has no referrer</td>
                    </tr>
                <?php } ?>
            </table>
            <button
                data-bs-target="#myModal"
                data-bs-toggle="modal"
                type="button"
                class="changeReferrer btn btn-sm btn-primary"
                style="margin-right: 5px;"
            >
                <span class="icon-pencil-2"></span> Change Referrer
            </button>
        </div>

        <div class="mysection">
            <strong>
                <div class='table_title'>User has Referred (<?php echo $total; ?> Total,
                    <?php $a = array(); foreach($subsCounts as $id => $plan) { $a[] = $plan['title']. ": " . $plan['count'] . " active"; }  echo implode(', ', $a); ?>)
                </div>
            </strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr>
                    <th>Name</th>
                    <th>Sign-up Date</th>
                    <th>Subscriptions</th>
                    <th>Modify</th>
                </tr>
                <?php echo $table; ?>
            </table>
            <button type="button" class="addReferee btn btn-sm btn-success" style="margin-right: 5px;" data-bs-toggle="modal" data-bs-target="#myModal"><span class="icon-plus"></span> Add Referee</button>
        </div>
        <?php
    }

    public static function timeline($bundle) {

        if (!$bundle) {
            return;
        }

        $brand = AxsBrands::getBrand();
        $currency_code = 'USD';
        $currency_symbol = '$';
        if($brand->billing->currency_code) {
            $currency_code = $brand->billing->currency_code;
        }
        if($brand->billing->currency_symbol) {
            $currency_symbol = $brand->billing->currency_symbol;
        }
        //date_default_timezone_set('America/Boise');

        //build js objects for vis timeline
        $item_id = 1;
        $items = array ();
        $itemMapping = array ();
        $reverseItemMapping = array ();
        $groups = array ();

        //first set up the admin actions
        $groups[] = array (
          'id' => $item_id,
          'content' => 'Administrator<br>Actions',
          'inlineText' => 'Administrator Actions',
          'title' => 'A full history of admin actions performed on this user\'s account',
          'className' => 'groupName',
          'order' => $item_id
        );

        $itemMapping[$item_id] = "original_groups[0]";
        $reverseItemMapping["original_groups[0]"] = $item_id;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('actions.*, types.action, types.type')
          ->from($db->qn('axs_gin_admin_actions') . " AS actions")
          ->leftJoin($db->qn('axs_gin_admin_action_types') . " AS types ON actions.type_id = types.id")
          ->where('actions.user_id = ' . (int) $bundle->id);
        $db->setQuery($query);
        $adminActions = $db->loadObjectList();

        $group_id = $item_id;
        for ($i = 0; $i < count($adminActions); $i++) {
            $adminAction = $adminActions[$i];

            if (!self::checkDate($adminAction->date)) {
                break;
            }

            $admin = AxsExtra::getUserProfileData($adminAction->admin_id);
            $adminAction->photo = $admin->photo;
            $adminAction->first = $admin->first;
            $adminAction->last = $admin->last;
            $adminAction->timeline_type = "admin_action";
            $items[] = array(
              'id' => ++$item_id,
              'group' => $group_id,
              'content' => '<img src="' . $admin->photo . '"/><br>' . $admin->first . ' ' . $admin->last . '<br>' . $adminAction->action,
              'start' => $adminAction->date,
              'className' => $adminAction->type
            );
            $itemMapping[$item_id] = "adminActions[$i]";
            $reverseItemMapping["adminActions[$i]"] = $item_id;
        }

        //now grab promo code usage
        $db->setQuery("SELECT a.*, b.title, b.code FROM axs_pay_promo_codes_activity a JOIN axs_pay_promo_codes b ON a.code_id=b.id WHERE user_id=$bundle->id;");
        $promoActivities = $db->loadObjectList();
        if (count($promoActivities)) {
            $group_id = ++$item_id;
            $groups[] = array(
              'id' => $group_id,
              'content' => 'Promo Code Usage',
              'title' => 'A full history of this user\'s promo code usage',
              'className' => 'groupName',
              'order' => $item_id
            );
            $itemMapping[$item_id] = "original_groups[1]";
            $reverseItemMapping["original_groups[1]"] = $item_id;
            for ($i = 0; $i < count($promoActivities); $i++) {
                $activity = $promoActivities[$i];

                if (!self::checkDate($activity->date)) {
                    break;
                }

                $activity->timeline_type = "promo";
                $items[] = array(
                  'id' => ++$item_id,
                  'group' => $group_id,
                  'content' => ($activity->trxn_id ? "Used Promo Code" : "Applied Promo Code") ."<br>$activity->code",
                  'start' => $activity->date,
                  'className' => 'success'
                );
                $itemMapping[$item_id] = "promoUsage[$i]";
                $reverseItemMapping["promoUsage[$i]"] = $item_id;
            }
        }

        //now grab subscriptions
        $subs = $bundle->subscriptions;
        for ($i = 0; $i < count($subs); $i++) {
            $sub = $subs[$i];
            $group_id = ++$item_id;
            $plan = $sub->getPlan();
            $sub->title = $plan->title; //add a title attribute for this subscription
            $sub->timeline_type = "subscriptions";
            $groups[] = array (
              'id' => $group_id,
              'content' => "<div class='swimlaneButton'>Subscription:<br>" . $plan->title."<br>(".$sub->getStatusName().")</div>",
              'inlineText' => $plan->title,
              'title' => "A full history of this user's Subscription for " . $plan->title,
              'subgroupOrder' => 'subgroup',
              'className' => 'groupName',
              'order' => $item_id
            );
            $itemMapping[$item_id] = "subscriptions[$i]";
            $reverseItemMapping["subscriptions[$i]"] = $item_id;

            for ($j = 0; $j < count($sub->periods); $j++) {
                $period = $sub->periods[$j];

                if (!self::checkDate($period->start) || !self::checkDate($period->end)) {
                    break;
                }

                $period->timeline_type = "sub_period";

                $items[] = array(
                  'id' => ++$item_id,
                  'group' => $group_id,
                  'subgroup' => 1,
                  'content' => '<p style="margin: 0; overflow: hidden; width: 100%;">'
                                    .'<span style="display: inline-block;">' . date("D, M d", strtotime($period->start)) . '</span>'
                                    .'<span style="display: inline-block; position: absolute; right: 5px;">' . date("D, M d", strtotime($period->end)) . '</span>'
                                .'</p>',
                  'start' => $period->start,
                  'end' => $period->end,
                  'className' => $period->getCssClass() . ' sub-period'
                );
                $itemMapping[$item_id] = "subscriptions[$i].periods[$j]";
                $reverseItemMapping["subscriptions[$i].periods[$j]"] = $item_id;

                for ($k = 0; $k < count($period->trxns); $k++) {
                    $trxn = $period->trxns[$k];

                    //Avoid data with dates of "0000-00-00 00:00:00"
                    if (!self::checkDate($trxn->date)) {
                        break;
                    }

                    $trxn->timeline_type = "sub_trxn";
                    $items[] = array(
                      'id' => ++$item_id,
                      'group' => $group_id,
                      'subgroup' => 2,
                      'content' => $trxn->getStatusName() . ' Trxn<br/>'. $currency_symbol . $trxn->amount . '<br/>' . date("D, M d", strtotime($trxn->date)),
                      'start' => $trxn->date,
                      'className' => $trxn->getCssClass()
                    );
                    $itemMapping[$item_id] = "subscriptions[$i].periods[$j].trxns[$k]";
                    $reverseItemMapping["subscriptions[$i].periods[$j].trxns[$k]"] = $item_id;
                }
            }
        }

        //now add LMS activity
        $courseActivity = $bundle->course_activity;
        $coursePurchases = $bundle->course_purchases;
        $courseActivityCount = count($courseActivity);
        $coursePurchasesCount = count($coursePurchases);
        if ($courseActivityCount || $coursePurchasesCount) {
            $group_id = ++$item_id;
            $groups[] = array(
              'id' => $group_id,
              'content' => 'LMS Activity',
              'inlineText' => 'LMS Activity',
              'title' => 'A full history of this user\'s LMS activity',
              'className' => 'groupName',
              'order' => $item_id
            );

            $itemMapping[$item_id] = "original_groups[".(count($groups)-1)."]";
            $reverseItemMapping["original_groups[".(count($groups)-1)."]"] = $item_id;
            //loop through course activity (quiz results)
            for ($i = 0; $i < $courseActivityCount; $i++) {
                $activity = $courseActivity[$i];

                if (!self::checkDate($activity->date)) {
                    break;
                }

                $activity->timeline_type = "course_activity";
                $items[] = array(
                  'id' => ++$item_id,
                  'group' => $group_id,
                  'content' => "<div style=\"max-width: 100px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;\">Took Quiz<br>$activity->quiz_title<br>Score: $activity->point/$activity->total_marks</div>",
                  'start' => $activity->date,
                  'className' => 'success'
                );
                $itemMapping[$item_id] = "courseActions[$i]";
                $reverseItemMapping["courseActions[$i]"] = $item_id;
            }
            //loop through course purchases
            for ($i = 0; $i < $coursePurchasesCount; $i++) {
                $purchase = $coursePurchases[$i];

                //Avoid data with dates of "0000-00-00 00:00:00"
                if (!self::checkDate($purchase->date)) {
                    break;
                }

                // free courses don't have ->payment
                // and if a paid course has been refunded, we
                // want to update the class of the course payment in the timeline
                $className =
                    $purchase->payment
                        ? $purchase->payment->getCssClass()
                        : $purchase->getCssClass();

                $course = $purchase->getCourse();

                $purchase->timeline_type = "course_purchase";
                $items[] = array(
                  'id' => ++$item_id,
                  'group' => $group_id,
                  'content' => "<div style=\"max-width: 150px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis;\">Course Purchase<br>$course->title</div>",
                  'start' => $purchase->date,
                  'className' =>  $className
                );
                $itemMapping[$item_id] = "coursePurchases[$i]";
                $reverseItemMapping["coursePurchases[$i]"] = $item_id;
            }
        }

        //Add ECommerce Activity

        $query = "SELECT
            orders.total AS total,
            orders.order_number AS order_number,
            products.product_name AS product_name,
            orders.created_date AS created_date,
            images.image AS image,
            orders.id AS order_id,
            status.orderstatus_name AS orderstatus_name,
            orders.order_status_id AS orderstatus_id,
            orders.shipping_address_1 AS shipping_address_1,
            orders.shipping_address_2 AS shipping_address_2,
            orders.shipping_city AS shipping_city,
            orders.shipping_postcode AS shipping_postcode,
            country.country_name AS country_name,
            orders.shipping_telephone AS shipping_telephone,
            orders.shipping_method_title AS shipping_method_title
            FROM joom_eshop_orders AS orders
            LEFT JOIN joom_eshop_orderproducts AS products
            ON orders.id = products.order_id
            LEFT JOIN joom_eshop_orderstatusdetails AS status
            ON orders.order_status_id = status.orderstatus_id
            LEFT JOIN joom_eshop_countries AS country
            ON orders.shipping_country_id = country.id
            LEFT JOIN joom_eshop_productimages AS images
            ON products.product_id = images.id
            WHERE orders.customer_id = " . $bundle->id .
            " GROUP BY order_id";
        $db->setQuery($query);
        $eshopOrders = $db->loadObjectList();

        if (count($eshopOrders) > 0) {

            $group_id = ++$item_id;
            $groups[]= array(
                "id" => $group_id,
                "content" => "Ecommerce<br>Activity",
                "title" => "A full history of the user's ecommerce activity / purchases.",
                "inlineText" => "Ecommerce",
                "className" => "groupName",
                "order" => $item_id
            );

            $itemMapping[$item_id] = "original_groups[" . (count($groups) - 1) . "]";
            $reverseItemMapping["original_groups[" . (count($groups) - 1) . "]"] = $item_id;

            for ($i = 0; $i < count($eshopOrders); $i++) {
                $order = $eshopOrders[$i];

                if (!self::checkDate($order->created_date)) {
                    break;
                }

                $order->timeline_type = "eshop_order";
                $items[]= array(
                    "id" => ++ $item_id,
                    "group" => $group_id,
                    "content" => "Product: " . $order->product_name . "<br>Amount: ". $currency_symbol . money_format('%i', $order->total) . "<br>Order: " . $order->order_number,
                    "start" => $order->created_date,
                    "className" => 'eshop'

                );

                $itemMapping[$item_id] = "eshopOrders[$i]";
                $reverseItemMapping["eshopOrders[$i]"] = $item_id;
            }
        }

        $queryVariables = "SELECT
            registrants.id AS registration_id,
            registrants.event_id AS event_id,
            registrants.user_id AS user_id,
            registrants.number_registrants AS num_registrants,
            registrants.amount AS amount,
            registrants.total_amount AS total_amount,
            registrants.register_date AS register_date,
            registrants.transaction_id AS transaction_id,
            registrants.payment_processing_fee AS payment_processing_fee,
            registrants.payment_date AS payment_date,
            registrants.payment_status AS payment_status,
            registrants.checked_in AS checked_in,
            registrants.checked_in_count AS checked_in_count,
            registrants.deposit_amount AS deposit_amount,

            events.title AS title,
            events.event_date AS event_date,
            events.event_end_date AS event_end_date
            FROM joom_eb_registrants AS registrants
            JOIN joom_eb_events AS events ";

        $queryRegister = $queryVariables . "ON registrants.event_id = events.id";
        $queryCancelled = $queryVariables . "ON registrants.original_event_id = events.id";

        $conditions = " WHERE registrants.user_id = " . $bundle->id . " AND registrants.group_id = 0 GROUP BY registrants.id";

        $queryRegister .= $conditions;
        $queryCancelled .= $conditions;

        $db->setQuery($queryRegister);
        $r = $db->loadObjectList();
        $db->setQuery($queryCancelled);
        $q = $db->loadObjectList();

        $eventActions = array_merge($r, $q);

        //$eventActions = $db->loadObjectList();
        $eventDetails = array();

        if (count($eventActions) > 0) {

            $group_id = ++$item_id;
            $groups[]= array(
                "id" => $group_id,
                "content" => "Event<br>Activity",
                "title" => "A full history of the user's event activity.",
                "inlineText" => "Events",
                "className" => "groupName",
                "order" => $item_id
            );

            $itemMapping[$item_id] = "original_groups[" . (count($groups) - 1) . "]";
            $reverseItemMapping["original_groups[" . (count($groups) - 1) . "]"] = $item_id;

            $eventCount = 0;
            foreach ($eventActions as $event) {
                //Get the details on the events themselves

                if (!self::checkDate($event->register_date)) {
                    break;
                }

                $query = "SELECT * FROM joom_eb_registrants WHERE group_id = " . $event->registration_id;
                $db->setQuery($query);
                $grp = $db->loadObjectList();
                $groupData = null;
                if (count($grp) > 0) {
                    $groupData = array();
                    foreach ($grp as $attendee) {
                        $newData = new stdClass();
                        $newData->id = $attendee->id;
                        $newData->first_name = $attendee->first_name;
                        $newData->email = $attendee->email;
                        if ($attendee->checked_in == 0) {
                            $newData->checked_in = "No";
                        } else {
                            $newData->checked_in = "Yes";
                        }
                        $groupData[]= $newData;
                    }
                }

                if ($groupData) {
                    $event->group_data = $groupData;
                }

                //Get the different actions.

                //$event = json_decode(json_encode($eventData));
                $event->timeline_type = "event_actions";
                if ($event->event_id) {
                    $event->action_type = "Registered";
                } else {
                    $event->action_type = "Registered (Cancelled)";
                }

                $event->action_date = $event->register_date;
                if ($event->checked_in == 0) {
                    $event->checked_in = "No";
                } else {
                    $event->checked_in = "Yes";
                }

                if ($event->payment_status == 0) {
                    $event->payment_status = "No";
                } else {
                    $event->payment_status = "Yes";
                }

                $start_date = strtotime($event->event_date);
                $end_date = strtotime($event->event_end_date);

                if ($end_date < $start_date) {
                    $end_date = $start_date;
                }

                $start_date = date("D, M d", $start_date);
                $end_date = date("D, M d", $end_date);

                $eventDetails[]= $event;

                $items[]= array(
                    "id" => ++$item_id,
                    "group" => $group_id,
                    "content" => "Registered: " . $event->title . "<br>Cost: ". $currency_symbol . $event->total_amount,
                    "start" => $event->register_date,
                    "className" => 'event_register'
                );

                $itemMapping[$item_id] = "eventDetails[$eventCount]";
                $reverseItemMapping["eventDetails[$eventCount]"] = $item_id;
                $eventCount++;
            }
        }

        //now add analytics
        $db->setQuery("SELECT * FROM #__realtimeanalytics_serverstats WHERE user_id_person=$bundle->id;");
        $analytics = $db->loadObjectList();
        $user_sessions = array();
        foreach ($analytics as $analytic) {
            $analytic->timeline_type = "analytics";
            if(!array_key_exists($analytic->session_id_person, $user_sessions)) {
                $session = new stdClass();
                $session->pagevisits = array($analytic);
                $session->start = $analytic->visit_timestamp;
                $session->end = $analytic->visit_timestamp;
                $user_sessions[$analytic->session_id_person] = $session;
            } else {
                $session = $user_sessions[$analytic->session_id_person];
                $session->pagevisits[] = $analytic;
                if($analytic->visit_timestamp < $session->start) {
                    $session->start = $analytic->visit_timestamp;
                }
                if($analytic->visit_timestamp > $session->end) {
                    $session->end = $analytic->visit_timestamp;
                }
            }
        }

        $userSessionsCount = count($user_sessions);
        if ($userSessionsCount) {
            $group_id = ++$item_id;
            $groups[] = array(
              'id' => $group_id,
              'content' => 'Analytics<br>Website Activity',
              'inlineText' => 'Website Activity',
              'title' => 'A full history of this user\'s website activity',
              'className' => 'groupName',
              'subgroupOrder' => 'subgroup',
              'order' => $item_id
            );

            $itemMapping[$item_id] = "original_groups[".(count($groups)-1)."]";
            $reverseItemMapping["original_groups[".(count($groups)-1)."]"] = $item_id;

            //loop through users sessions
            foreach ($user_sessions as $session_id => $session) {

                if (!self::checkDate($session->start) || !self::checkDate($session->end)) {
                    break;
                }

                $items[] = array(
                  'id' => ++$item_id,
                  'group' => $group_id,
                  'subgroup' => 3,
                  'content' => "",
                  'start' => date('Y-m-d H:i:s', $session->start),
                  'end' => date('Y-m-d H:i:s', $session->end),
                  'className' => 'primary'
                );
                $itemMapping[$item_id] = "userSessions[$session_id]";
                $reverseItemMapping["userSessions[$session_id]"] = $item_id;

                $pageVisitCount = count($session->pagevisits);
                for($i = 0; $i < $pageVisitCount; $i++) {
                    $pageVisit = $session->pagevisits[$i];

                    if (!self::checkDate($pageVisit->visit_timestamp)) {
                        break;
                    }

                    $url = parse_url($pageVisit->visitedpage);

                    $items[] = array(
                      'id' => ++$item_id,
                      'group' => $group_id,
                      'subgroup' => 4,
                      'content' =>   "Domain: ".$url['host'] . "<br>Page: " . $url['path'] . "<br>Query: " . $url['query'],
                      'start' => date('Y-m-d H:i:s', $pageVisit->visit_timestamp) .' +0000',
                      'className' => 'primary'
                    );
                    $itemMapping[$item_id] = "userSessions[$session_id].pagevisits[$i]";
                    $reverseItemMapping["userSessions[$session_id].pagevisits[$i]"] = $item_id;
                }
            }
        }
        $input = JFactory::getApplication()->input;
        $uid = $input->get('id',0,'INT');

        ?>
        <div class="mysection">
            <div class="displayKey"></div>
            <div id="userInformation" data-user-id="<?php echo $bundle->id;?>"></div>
            <div id="visualization" style="background-color: white;"></div>
            <div class="displayItem"></div>


            <script>
                var adminActions = <?php echo json_encode($adminActions); ?>;
                var promoUsage = <?php echo json_encode($promoActivities); ?>;
                var subscriptions = <?php echo json_encode($subs); ?>;
                var courseActions = <?php echo json_encode($courseActivity); ?>;
                var coursePurchases = <?php echo json_encode($coursePurchases); ?>;
                var eshopOrders = <?php echo json_encode($eshopOrders); ?>;
                var eventDetails = <?php echo json_encode($eventDetails); ?>;
                var userSessions = <?php echo json_encode($user_sessions); ?>;
                var itemMapping = <?php echo json_encode($itemMapping); ?>;
                var reverseItemMapping = <?php echo json_encode($reverseItemMapping); ?>;
                var items = new vis.DataSet(<?php echo json_encode($items); ?>);
                var original_groups = <?php echo json_encode($groups); ?>;
                var groups = new vis.DataSet(original_groups);
            </script>
        </div>
        <?php
    }

    private static function checkDate($date) {
        //Avoid data with dates of "0000-00-00 00:00:00" or null dates
        if (strtotime($date) < 0 || $date == null) {
            return false;
        }

        return true;
    }

    public static function cards($bundle) {
        $cards = $bundle->cards;
        ?>
        <div class="mysection">
            <strong><div class='table_title'>Card Manager</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Card Owner</th><th>Card #</th><th>Delete Card</th></tr>
                <?php if (count($cards)) {
                    foreach ($cards as $c) {
                        $name  = ($bundle->id == $c->user_id) ? 'This User' : self::get_link($c->user_id);
                        ?>
                        <tr>
                            <td><?php echo $name; ?></td>
                            <td>
                                <?php echo sprintf('%04d', $c->cardNumber); ?>
                                <!-- <button class="updateCard btn btn-sm btn-primary float-end" type="button" data-bs-toggle="modal" data-bs-target="#myModal" data-cardid="<?php echo $c->id; ?>"><span class="icon-credit"></span> Update Card Info</button> -->
                                <!-- <button class="updateToken btn btn-sm btn-primary float-end" style="margin-right: 10px;" type="button" data-bs-toggle="modal" data-bs-target="#myModal" data-cardid="<?php echo $c->id; ?>"><span class="icon-wrench"></span> Update Token</button> -->
                            </td>
                            <td>
                                <button class="deleteCard btn btn-sm btn-danger" type="button" data-card="<?php echo $c->cardNumber; ?>" data-cardid="<?php echo $c->id; ?>"><span class="icon-trash"></span> Delete</button>
                            </td>
                        </tr>
                    <?php }
                } else { ?>
                    <tr><td colspan="4" style="text-align: center;">User has no cards on file</td></tr>
                <?php } ?>
            </table>
            <button class="btn btn-sm btn-success addCard" type="button" style="margin-right: 5px;" data-bs-toggle="modal" data-bs-target="#cardModal"><span class="icon-plus"></span> Add Card</button>
        </div>
        <?php
    }

    public static function courses($bundle) { ?>
        <div class="mysection">
            <strong><div class='table_title'>Courses Bought</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Course</th><th>Status</th><th>Date Purchased</th><th>Manage</th></tr>
                <?php if (count($bundle->course_purchases)) {
                    foreach ($bundle->course_purchases as $cp) { ?>
                        <tr>
                            <td><?php echo $cp->getCourse()->title; ?></td>
                            <td><?php echo $cp->getStatusName(); ?></td>
                            <td><?php echo AxsExtra::format_date($cp->date); ?></td>
                            <td>
                                <span class="btn btn-sm btn-success markPaidCourse" data-cpid="<?php echo $cp->id; ?>">
                                    <span class="icon-checkmark"></span>
                                    Mark Paid
                                </span>
                                <span class="btn btn-sm btn-danger deleteCoursePurchase" data-cpid="<?php echo $cp->id; ?>">
                                    <span class="icon-trash"></span>
                                    Delete
                                </span>
                            </td>

                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="3" style="text-align: center;">User has no courses to display</td></tr>
                <?php } ?>
            </table>
            <button
                class="btn btn-sm btn-success purchaseCourse"
                data-bs-toggle="modal"
                data-bs-target="#myModal"
                type="button"
            >
                <span class="icon-plus"></span> Purchase Course
            </button>
        </div>

        <div class="mysection">
            <strong><div class='table_title'>Course Progress</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Course</th><th>Completed %</th><th>Date Started</th><th>Date Ended</th><th>Settings</th></tr>
                <?php if (count($bundle->course_progress)) {
                    foreach ($bundle->course_progress as $courseProgress) { ?>
                        <tr>
                            <td><?php echo $courseProgress->title; ?></td>
                            <td><?php echo $courseProgress->progress; ?>%</td>
                            <td><?php echo AxsExtra::format_date($courseProgress->date_started); ?></td>
                            <td><?php echo AxsExtra::format_date($courseProgress->date_completed); ?></td>
                            <td>
                                <button class="completeCourse btn btn-sm btn-primary"
                                        type="button"
                                        style="margin-right: 5px;"
                                        data-course-id="<?php echo $courseProgress->course_id; ?>"
                                        data-user-id="<?php echo $courseProgress->user_id; ?>"
                                        data-start-date="<?php echo $courseProgress->date_started; ?>"
                                        data-completed-date="<?php echo $courseProgress->date_completed; ?>"
                                >
                                    <span class="icon-flag"></span> Mark As Completed
                                </button>
                                <button class="archiveCourseProgress btn btn-sm btn-danger"
                                        type="button"
                                        style="margin-right: 5px;"
                                        data-course-id="<?php echo $courseProgress->course_id; ?>"
                                        data-user-id="<?php echo $courseProgress->user_id; ?>"
                                >
                                    <span class="icon-archive"></span> Archive
                                </button>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="3" style="text-align: center;">User has no courses to display</td></tr>
                <?php } ?>
            </table>
            <button class="btn btn-small btn-success addCourseProgress" data-bs-toggle="modal" data-bs-target="#myModal" type="button" style="margin-right: 5px;"><span class="icon-plus"></span> Add Progress</button>
        </div>
        <?php
    }


    public static function certifications($bundle) { ?>
        <div class="mysection">
            <strong><div class='table_title'>Certifications and Badges</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr>
                    <th>Certificate</th>
                    <th>Award Type</th>
                    <th>Date Earned</th>
                    <th>Date Expires</th>
                    <th>Manage</th>
                </tr>
                <?php if (count($bundle->certifications)) {
                    foreach ($bundle->certifications as $award) { ?>
                        <tr>
                            <td><?php echo $award->title; ?></td>
                            <td><?php echo ucwords($award->type); ?></td>
                            <td><?php if($award->date_earned) { echo AxsExtra::format_date($award->date_earned); } ?></td>
                            <td><?php if($award->date_expires) { echo AxsExtra::format_date($award->date_expires); } ?></td>
                            <td>
                                <span
                                    data-bs-toggle="modal"
                                    data-bs-target="#editAwardModal"
                                    class="btn btn-sm btn-success editAward"
                                    data-award_earned="<?php echo date('Y-m-d',strtotime($award->date_earned)); ?>"
                                    data-award_expires="<?php echo date('Y-m-d',strtotime($award->date_expires)); ?>"
                                    data-award_title="<?php echo $award->title; ?>"
                                    data-award_id="<?php echo $award->badge_id; ?>"
                                    data-user_id="<?php echo $award->user_id; ?>"
                                >
                                    <span class="icon-pencil-2"></span>
                                    Edit
                                </span>
                                <span class="btn btn-sm btn-danger deleteAward" data-award_id="<?php echo $award->id; ?>" data-user_id="<?php echo $award->user_id; ?>">
                                    <span class="icon-trash"></span>
                                    Delete
                                </span>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="3" style="text-align: center;">User has no certificates or badges to display</td></tr>
                <?php } ?>
            </table>
            <button class="btn btn-sm btn-success addAward" data-bs-toggle="modal" data-bs-target="#addAwardModal" type="button" style="margin-right: 5px;"><span class="icon-plus"></span> Add Certificate or Badge</button>
        </div>
        <?php
    }

    public static function rewards($bundle) {
        $rewards = $bundle->rewards_trxns;
        $currPoints = $bundle->rewards_data->points;
        $codes = $bundle->affiliate_codes;
        $commissions = $bundle->commissions;
        $brand = AxsBrands::getBrand();
        $currency_code = 'USD';
        $currency_symbol = '$';
        if($brand->billing->currency_code) {
            $currency_code = $brand->billing->currency_code;
        }
        if($brand->billing->currency_symbol) {
            $currency_symbol = $brand->billing->currency_symbol;
        }

        $p = 0;
        ob_start();
        if (count($rewards)) {
            foreach ($rewards as $r) {
                if ($r->status == 'Pending') {
                    $p += $r->amount;
                } ?>
                <tr class="<?php if ($r->amount > 0) echo 'success'; else echo 'error'; ?>">
                    <td><?php echo $r->reason; ?></td>
                    <td><?php if (!$r->referee_id) echo 'System'; elseif ($r->referee_id == -1) echo 'Admin Action'; else echo self::get_link($r->referee_id); ?></td>
                    <td><?php echo AxsExtra::format_date($r->date); ?></td>
                    <td><?php echo  $currency_symbol . $r->amount; ?></td>
                    <td><?php echo $r->status; ?></td>
                </tr>
            <?php }
            if (count($rewards) == 20) { ?>
                <tr><td colspan="5" style="text-align: center;"><button id="loadMoreRewards" type="button" class="btn btn-default" data-user="<?php echo $bundle->id; ?>" data-page="1">Load More</button></td></tr>
            <?php }
        } else { ?>
            <tr><td colspan="5" style="text-align: center;">User has no rewards to display</td></tr>
        <?php }
        $table = ob_get_clean(); ?>
        <div class="mysection">
            <strong><div class='table_title'>Current Rewards Points</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr>
                    <th>Current Points Available</th>
                    <th>Adjust</th>
                </tr>
                <tr>
                    <td width="50%">
                        <?php echo $currPoints; ?>
                    </td>
                    <td width="50%">
                        <div class="row mb-3">
                            <div class="col-sm-4">
                                <input name="adjustPoints" type="number" placeholder="Examples: 50, -150"/>
                            </div>
                            <div class="col-sm-4">
                                <input name="adjustReason" type="text" placeholder="Reason for adjustment" />
                            </div>
                            <div class="col-sm-4">
                                <button type="button" class="adjustPoints btn btn-success" data-user="<?php echo $bundle->id; ?>"><span class="icon-save"></span> Save</button>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div class="mysection">
            <strong><div class='table_title'>Rewards Transactions (<?php echo $p; ?> points pending)</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Reason</th><th>Referee</th><th>Date</th><th>Amount</th><th>Status</th></tr>
                <?php echo $table; ?>
            </table>
        </div>

        <div class="mysection">
            <strong><div class='table_title'>User Referral Codes</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Code</th><th>Created On</th><th>Delete</th></tr>
                <?php if (count($codes)) {
                    foreach ($codes as $code) { ?>
                        <tr>
                            <td><?php echo $code->code; ?></td>
                            <td><?php echo AxsExtra::format_date($code->date); ?></td>
                            <td><button class="deleteCode btn btn-sm btn-danger" type="button" data-user="<?php echo $code->user_id; ?>" data-code="<?php echo $code->id; ?>"><span class="icon-trash"></span> Delete</button></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="5" style="text-align: center;">User has no codes to display</td></tr>
                <?php } ?>
            </table>
            <button class="addCode btn btn-sm btn-success" data-bs-toggle="modal" data-bs-target="#myModal" type="button" style="margin-right: 5px;"><span class="icon-plus"></span> Add Referral Code</button>
        </div>

        <div class="mysection">
            <strong><div class='table_title'>User Commissions</div></strong>
            <table class="table table-striped table-bordered table-hover table-responsive">
                <tr><th>Reason</th><th>Referee</th><th>Date</th><th>Amount</th><th>Status</th></tr>
                <?php if (count($commissions)) {
                    foreach ($commissions as $c) { ?>
                        <tr><td><?php echo $c->reason; ?></td>
                            <td><?php echo $c->referee_id; ?></td>
                            <td><?php echo AxsExtra::format_date($c->date); ?></td>
                            <td><?php echo $currency_symbol . $c->amount; ?></td>
                            <td><?php echo $c->status; ?> <button class="changeCommissionStatus btn btn-sm btn-danger float-end" data-bs-toggle="modal" data-bs-target="#myModal" data-id="<?php echo $c->id; ?>" type="button" style="margin-right: 5px;"><span class="icon-pencil-2"></span> Change Status</button></td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr><td colspan="5" style="text-align: center;">User has no commissions to display</td></tr>
                <?php } ?>
            </table>
            <button id="addCommission" class="btn btn-sm btn-danger" data-bs-toggle="modal" data-bs-target="#myModal" type="button" style="margin-right: 5px;"><span class="icon-plus"></span> Add Commission</button>
        </div>
        <?php
    }

    public static function extras($bundle) {
        $profileData = $bundle->data;
        $famIds = $bundle->family;
        $brand = AxsBrands::getBrand();
        $currency_code = 'USD';
        $currency_symbol = '$';
        if($brand->billing->currency_code) {
            $currency_code = $brand->billing->currency_code;
        }
        if($brand->billing->currency_symbol) {
            $currency_symbol = $brand->billing->currency_symbol;
        }
        $input = JFactory::getApplication()->input;
    ?>

        <!-- variables to be dumped in page -->
        <script>
            var plans = <?php echo json_encode($bundle->plans); ?>;
            var courses = <?php echo json_encode($bundle->courses); ?>;
            //var certifications = <?php //echo json_encode($bundle->certifications); ?>;
            //var badges = <?php //echo json_encode($bundle->badges); ?>;
            var cards = <?php echo json_encode($bundle->cards); ?>;
            var countries = <?php echo json_encode($bundle->countries); ?>;
            var family = <?php echo json_encode($bundle->family); ?>;
            var user = <?php echo json_encode($bundle->data); ?>;

            //check if the user came to the page to see a specific trxn or sub
            var trxn_id = <?php echo $input->get('trxn_id', 0); ?>;
            var sub_id = <?php echo $input->get('sub_id', 0); ?>;
        </script>

        <!-- These are templates for the timeline -->
        <script id="sub-period-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Subscription Period Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Start</td><td>{{formatdate start}}</td></tr>
                            <tr><td class="labels">End</td><td>{{formatdate end}}</td></tr>
                            <tr><td class="labels">Status</td><td>{{formatsubperiod status}}</td></tr>
                            <tr><td class="labels">Transactions</td><td>{{#each trxns}}<?php echo $currency_symbol; ?>{{amount}} - {{formattrxn status}} - {{formatdate date}}</br>{{/each}}</td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            {{formatsubperiodactions status}}
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="trxn-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Transaction Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Transaction ID</td><td>{{trxn_id}}</td></tr>
                            <tr><td class="labels">Initiator</td><td>{{formatinitiator initiator}}</td></tr>
                            <tr><td class="labels">Amount</td><td><?php echo $currency_symbol; ?>{{amount}}</td></tr>
                            <tr><td class="labels">Date</td><td>{{formatdate date}}</td></tr>
                            <tr><td class="labels">Card Used</td><td>{{formatcard card}}</td></tr>
                            <tr class="{{formattrxnclass status}}"><td class="labels">Status</td><td>{{formattrxn status}}</td></tr>
                            {{#if message}}
                            <tr class="{{formattrxnclass status}}"><td class="labels">Error Reason</td><td>{{message}}</td></tr>
                            {{/if}}
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            <div class="action-group">
                                {{formatrefund status id}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="admin-action-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Admin Action Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Admin</td><td><img src="{{photo}}" style="width: 70px;"></br>{{first}} {{last}}</br>Id: {{admin_id}}</td></tr>
                            <tr><td class="labels">Action</td><td>{{action}}</td></tr>
                            <tr><td class="labels">Date</td><td>{{formatdate date}}</td></tr>
                            <tr><td class="labels">Reason</td><td>{{reason}}</td></tr>
                            <tr><td class="labels">Changed Data Id</td><td>{{data_row}}</td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">

                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="timeline-key-tmpl" type="text/x-handlebars-template">
            <label class="checkbox-inline">
                <input type="checkbox" id="master" name="master" checked> View All
            </label>
            {{#each _data}}
            <label class="checkbox-inline">
                <input type="checkbox" class="timeline-keys" value="{{id}}" checked> {{inlineText}}
            </label>
            {{/each}}
        </script>

        <script id="eshop-order-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                    <h2><span class="icon-info"></span> Ecommerce Purchase Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td>Product Name</td><td>{{product_name}}</td></tr>
                            <tr><td>Order Number</td><td>{{order_id}}</td></tr>
                            <tr><td>Order ID</td><td>{{order_number}}</td></tr>
                            <tr><td>Order Amount</td><td>{{formatcurrency total}}</td></tr>
                            <tr><td>Order Status</td><td>{{orderstatus_name}}</td></tr>
                            <tr><td>Shipping Address</td><td>{{shipping_address_1}} {{shipping_address_2}}<br>{{shipping_city}}, {{shipping_postcode}}<br>{{country_name}}</td></tr>
                            <tr><td>Phone</td><td>{{shipping_telephone}}</td></tr>
                            <tr><td>Shipping Method</td><td>{{shipping_method_title}}</td></tr>
                            <tr><td>Image</td><td><img width='150' src='/<?php echo AxsImages::getImagesPath("store"); ?>/products/{{image}}'></td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            {{formateshop}}
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="event-action-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                    <h2><span class="icon-info"></span> Event Action</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td>Action Type</td><td>{{action_type}}</td></tr>
                            <tr><td>Action Date</td><td>{{formatdate action_date}}</td></tr>
                            <tr><td>Event Name</td><td>{{title}}</td></tr>
                            <tr><td>Event Start Date</td><td>{{formatdate event_date}}</td></tr>
                            <tr><td>Event End Date</td><td>{{formatdate event_end_date}}</td></tr>
                            <tr><td>Event Cost</td><td>{{formatcurrency amount}}</td></tr>
                            <tr><td>Paid</td><td>{{payment_status}}</td></tr>
                            {{#if group_data}}
                                <tr><td>Checked In Count</td><td>{{checked_in_count}}</td></tr>
                            {{else}}
                                <tr><td>Checked In</td><td>{{checked_in}}</td></tr>
                            {{/if}}
                            <tr><td>Number of Registrants</td><td>{{num_registrants}}</td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            {{formateventaction}}
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="sub-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> {{title}} Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Status</td><td>{{formatsub status}}</td></tr>
                            <tr><td class="labels">Amount Per Period</td><td>${{original_amount}}</td></tr>
                            <tr><td class="labels">Subscription Id</td><td>{{id}}</td></tr>
                            <tr><td class="labels">Cancel Date</td><td>{{formatdate cancel}}</td></tr>
                            <tr><td class="labels">Period Length</td><td>{{formatperiodlength period_length}}</td></tr>
                            <tr><td class="labels">Card to charge</td><td>{{formatcardbyid card_id}}</td></tr>
                            <tr><td class="labels">Points to use if available</td><td>{{points}}</td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            {{formatsubaction status}}
                            <div class="action-group">
                                <button id="subsettings" data-bs-toggle="modal" data-bs-target="#myModal" class="btn btn-danger col-sm-3"><span class="icon-cogs"></span> Subscription Settings</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <script id="sub-settings-tmpl" type="text/x-handlebars-template">
            <form id="subSettingsForm" class="form form-horizontal" enctype="multipart/form-data" data-subid="{{id}}" data-user="{{user_id}}">
                <h6 class="mb-3">Adjust settings for this subscription.</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Regular Period Amount<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="original_amount" value="{{original_amount}}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                         <label for="month">Term Length<span class="required">*</span></label>
                    </div>
                    <div class="col-md-8" id="month">
                        <select name="length" class="input-small" style="display: inline-block; width: 165px;">
                            {{formatlength period_length}}
                        </select>
                        <select name="term" class="input-small" style="display: inline-block; width: 165px;">
                            {{formatterm period_length}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Card to charge<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="card_id">
                            <option value="0">--No card set--</option>
                            {{formatselectedcard card_id}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Points to use if available<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="points" value="{{points}}">
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="renew-tmpl" type="text/x-handlebars-template">
            <form
                id="renewForm"
                class="form col-sm-12"
                enctype="multipart/form-data"
                data-subid="{{id}}"
                data-user="{{user_id}}"
                data-desc="{{desc}}"
            >
                <h6 class="mb-3">This will renew their subscription.</h4>
                <div class="row mb-3">
                    <label class="col-md-4">When should we charge them?<span class="required">*</span></label>
                    <fieldset class="col-md-8 btn-group btn-group-yesno radio">
                        <input type="radio" class="btn-check" id="time1" name="time" value="1" checked="checked">
                        <label for="time1" class="btn btn-outline-primary">Now</label>
                        <input type="radio" class="btn-check" id="time0" name="time" value="0"
                        ><label for="time0" class="btn btn-outline-primary">Later</label>
                    </fieldset>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Regular payment dates based on?<span class="required">*</span></label>
                    <fieldset class="col-md-8 btn-group btn-group-yesno radio">
                        <input type="radio" class="btn-check" id="dateopt1" name="dateopt" value="1" checked="checked">
                        <label for="dateopt1" class="btn btn-outline-primary">Today&#39;s Date</label>
                        <input type="radio" class="btn-check" id="dateopt0" name="dateopt" value="0">
                        <label for="dateopt0" class="btn btn-outline-primary">Set Date</label>
                    </fieldset>
                </div>
                <div class="row mb-3" id="charge_date" style="display: none;">
                    <label class="col-md-4">When should they be charged regularly?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="date" name="charge_date">
                    </div>
                </div>
                <div class="row mb-3" id="mystatus" style="display: none;">
                    <label class="col-md-4">Is the period between now and the regular payment date going to be prorated or comped?<span class="required">*</span></label>
                    <fieldset class="col-md-8 btn-group btn-group-yesno radio">
                        <input type="radio" class="btn-check" id="status1" name="status" value="1" checked="checked">
                        <label for="status1" class="btn btn-outline-primary">Proprated</label>
                        <input type="radio" class="btn-check" id="status0" name="status" value="0">
                        <label for="status0" class="btn btn-outline-primary">Comped</label>
                    </fieldset>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="pause-tmpl" type="text/x-handlebars-template">
            <form id="pauseForm" class="form form-horizontal" enctype="multipart/form-data" data-subid="{{id}}" data-user="{{user_id}}">
                <h6 class="mb-3">This will cancel their subscription until the date specified below, at which point they will begin being charged again.</h4>
                <div class="row mb-3">
                    <label class="col-md-4">When should we start charging again?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="date" name="pause_until">
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="adjust-date-tmpl" type="text/x-handlebars-template">
            <form id="adjustDateForm" class="form form-horizontal" enctype="multipart/form-data" data-subid="{{id}}" data-user="{{user_id}}">
                <h6 class="mb-3">This will change the payment date for their subscription going forward.</h4>
                <div class="row mb-3">
                    <label class="col-md-4">When do you want them to be charged?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="date" name="extend_until" placeholder="mm/dd/yyyy">
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Is this going to be prorated or comped?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <div class="btn-group">
                            <input type="radio" class="btn-check" id="status1" name="status" value="1">
                            <label for="status1" class="btn btn-outline-primary">Prorated</label>
                            <input type="radio" class="btn-check" id="status0" name="status" value="0" checked>
                            <label for="status0" class="btn btn-outline-danger">Comped</label>
                        </div>
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="add-sub-tmpl" type="text/x-handlebars-template">
            <form id="addSubForm" class="form" enctype="multipart/form-data" data-userid="<?php echo $bundle->data->user_id; ?>">
                <h6 class="mb-3">This will add a subscription to their account.</h4>
                <div class="row mb-3">
                    <label class="col-md-4">What subscription plan are you adding them to?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="sub_plan">
                            <option>--Choose Plan--</option>
                            {{formatplans plans}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Card to Use<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="sub_card">
                            <option>--No card set--</option>
                            {{formatcards cards}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Initial Amount<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="initial_amount">
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Regular Amount<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="regular_amount">
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="mark-paid-sub-period-tmpl" type="text/x-handlebars-template">
            <form id="markPaidForm" class="form form-horizontal" enctype="multipart/form-data" data-sub-perid="{{id}}" data-subid="{{sub_id}}">
                <div class="row mb-3">
                    <label class="col-md-4">How did they pay for this period?<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select class="form-control" name="payment_type">
                            <option>--Choose Payment type--</option>
                            <option value="1">Check Payment</option>
                            <option value="2">Cash Payment</option>
                            <option value="3">Money Order Payment</option>
                            <option value="4">Comp Period</option>
                        </select>
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" class="form-control" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="course-actions-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Quiz Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Lesson</td><td>{{lesson_title}}</td></tr>
                            <tr><td class="labels">Quiz</td><td>{{quiz_title}}</td></tr>
                            <tr><td class="labels">Score</td><td>{{point}}/{{total_marks}}</td></tr>
                            <tr><td class="labels">Date</td><td>{{formatdate date}}</td></tr>
                        </table>
                    </div>
                    <!--
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">

                        </div>
                    </div>
                    -->
                </div>
            </div>
        </script>

        <script id="course-purchases-tmpl" type="text/x-handlebars-template">
            <div class="container-fluid">
                <div class="row mb-3">
                    <div class="col-sm-6">
                        <h2><span class="icon-info"></span> Course Purchase Details</h2>
                        <table class="table table-striped table-bordered table-hover">
                            <tr><td class="labels">Course</td><td>{{course.title}}</td></tr>
                            <tr><td class="labels">Payment Status</td><td>{{formatsubperiod status}}</td></tr>
                            {{formatcoursepayment payment}}
                            <tr><td class="labels">Date</td><td>{{formatdate date}}</td></tr>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <h2><span class="icon-options"></span> Admin Actions</h2>
                        <div class="admin-actions">
                            <div class="action-group">
                                {{formatrefund payment.status payment.id}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </script>

        <!-- These are templates for credit cards -->
        <script id="credit-card-tmpl" type="text/x-handlebars-template">

        </script>

        <script id="card-permissions-tmpl" type="text/x-handlebars-template">
            <form id="saveCardPermissionsForm" class="form form-horizontal" enctype="multipart/form-data" data-cardid="{{id}}">
                <h6 class="mb-3">Family member access:</h4>
                <table class="table table-bordered table-striped table-responsive">
                    <tr><th>Profile</th><th>Name</th><th>User Id</th><th>Can Use Card</th></tr>
                    <?php $famIds = array_diff($famIds, array($bundle->id));
                    if (count($famIds)) { ?>
                        <?php foreach ($famIds as $fm) {
                            $db = JFactory::getDBO();
                            $query = $db->getQuery(true);
                            $query
                              ->select('*')
                              ->from('#__community_users')
                              ->where($db->quoteName('userid') . '=' . $db->quote($fm));
                            $db->setQuery($query);
                            $user = $db->loadObject();
                            if ($user->avatar != '') {
                                $photo = "../" . $user->avatar;
                            } else {
                                $photo = "../images/user.png";
                            }
                        ?>
                            <tr>
                                <td><img src="<?php echo $photo; ?>" style="width: 50px;"></td>
                                <td><?php echo self::get_link($fm); ?></td>
                                <td><?php echo $fm; ?></td>
                                <td><input type="checkbox" name="<?php echo $fm; ?>" {{formatfamilychecked <?php echo $fm; ?>}}/></td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr><td colspan="4">You don't have any family members signed up so you cannot set permissions for this card.</td></tr>
                    <?php } ?>
                </table>
            </form>
        </script>

        <script id="card-token-tmpl" type="text/x-handlebars-template">
            <form id="updateTokenForm" class="form form-horizontal" enctype="multipart/form-data" data-cardid="{{id}}">
                <h6 class="mb-3">Set token for updated card</h4>
                <table class="table table-bordered table-striped table-responsive">
                    <tr><th>Old Card (Token to remove)</th><th>New Card (Token to add)</th></tr>
                    <tr>
                        <td>{{token}}</td>
                        <td>
                            <div class="row mb-3">
                                <label class="col-md-4">New Token<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="number" name="newToken">
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </form>
        </script>

        <!-- These are forms for rewards program -->
        <script id="referral-code-tmpl" type="text/x-handlebars-template">
            <form id="saveCodeForm" class="form form-horizontal"
                  enctype="multipart/form-data" data-user="<?php echo $bundle->id; ?>">
                <h6 class="mb-3">Enter Code</h4>
                <div class="code-group row">
                    <label class="col-md-4">Code<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="code" value="" maxlength="16">
                        <span class="code-message help-block"></span>
                    </div>
                </div>
            </form>
        </script>

        <script id="add-commission-tmpl" type="text/x-handlebars-template">
            <form id="addCommissionForm" class="form form-horizontal" data-user="<?php echo $bundle->id; ?>">
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label>Who are they getting this commission for? (user id)<span class="required">*</span></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="referee_id">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label>Commission amount<span class="required">*</span></label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="commission_amount">
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Commission Status<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="status">
                            <option>Cancelled</option>
                            <option>Pending</option>
                            <option>Awarded</option>
                        </select>
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" class="form-control" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <script id="change-commission-tmpl" type="text/x-handlebars-template">
            <form id="changeCommissionStatusForm" class="form form-horizontal" data-commid="{{id}}">
                <div class="row mb-3">
                    <label class="col-md-4">Commission Status<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="status">
                            <option>Cancelled</option>
                            <option>Pending</option>
                            <option>Awarded</option>
                        </select>
                    </div>
                </div>
                <h6 class="mb-3">Reason for action</h4>
                <div class="row mb-3">
                    <label class="col-md-4">Please provide a reason<span class="required">*</span></label>
                    <div class="col-md-8"><input type="text" name="reason" class="form-control" placeholder="Reason for action"></div>
                </div>
            </form>
        </script>

        <!-- These are forms for courses -->
        <script id="course-purchase-progress-tmpl" type="text/x-handlebars-template">
            <table class="table table-bordered table-striped table-responsive">
                <tr><th>Lesson</th><th>Quiz</th><th>Score</th><th>Percentage</th></tr>
                {{formatquizresults quiz_results}}
            </table>
        </script>

        <!-- This is the modal -->
         <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="myModalLabel">Add New Card</h3>
                        <span class="btn-close" data-bs-dismiss="modal"></span>
                    </div>
                    <div class="modal-body" id="myModalBody"></div>
                    <div class="modal-footer">
                        <button type="button" class="saveCard btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="cardModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="cardModalLabel">Add New Card</h3>
                        <button data-bs-dismiss="modal" type="button" class="btn-close"></button>
                    </div>
                    <div class="modal-body" id="cardModalBody">
                        <form
                            id="saveCardForm"
                            class="form form-horizontal"
                            enctype="multipart/form-data"
                            data-user="<?php echo $profileData->user_id; ?>"
                            data-gtw="<?php echo $brand->billing->gateway_type; ?>"
                        >
                            <h6 class="mb-2">Card Info</h4>

                            <?php if($brand->billing->gateway_type == 'stripe') {
                                $stripeParams = new stdClass();
                                $stripeParams->form_id = 'saveCardForm';
                                $stripeForm = AxsPayment::buildStipePaymentForm($stripeParams);
                                echo $stripeForm;
                            } else { ?>
                            <div class="row mb-3">
                                <label class="col-md-4">
                                    Credit Card Number<span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" name="cardNumber" value="">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col-md-4">
                                    <label for="month" >Expiration Date<span class="required">*</span></label>
                                </div>
                                <div class="col-md-8" id="month">
                                    <select name="exp_month" class="exp_month input-small" style="display: inline-flex; width: 165px;">

                                    </select>
                                    / <select name="exp_year" class="exp_year input-small" style="display: inline-flex; width: 165px;">

                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">CVV Code<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" name="cvv" value="">
                                </div>
                            </div>
                            <h6 class="mb-3">Billing Address</h4>
                            <div class="row mb-3">
                                <label class="col-md-4">First Name<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="first" value="<?php echo $profileData->first; ?>"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Last Name<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="last" value="<?php echo $profileData->last; ?>"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Address<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="address" value="<?php echo $profileData->address; ?>" maxlength="30"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">City<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="city" value="<?php echo $profileData->city; ?>" maxlength="30"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">State<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="state" value="<?php echo $profileData->state; ?>" maxlength="30"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Zip Code<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="zip" value="<?php echo $profileData->zip; ?>" maxlength="9"></div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Country<span class="required">*</span></label>
                                <div class="col-md-8">
                                <select name="country" class="card_country form-control" data-country="<?php echo $profileData->country; ?>">

                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Phone<span class="required">*</span></label>
                                <div class="col-md-8"><input type="text" name="phone" value="<?php echo $profileData->phone; ?>"></div>
                            </div>
                            <?php } ?>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="saveCard btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="addAwardModal" tabindex="-1" role="dialog" aria-labelledby="addAwardLabel" aria-hidden="true">
            <div class="modal-dialog modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="addAwardLabel">Add New Certificate or Badge</h3>
                        <span class="btn-close" data-bs-dismiss="modal"></span>
                    </div>
                    <div class="modal-body" id="addAwardBody">
                        <form id="saveAwardForm" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $profileData->user_id; ?>">
                            <div class="row mb-3">
                                <label class="col-md-4">Certificate/Badge<span class="required">*</span></label>
                                <div class="col-md-8">
                                    <select name="award_id">
                                        <?php echo AxsLearnerDashboard::getAllAwards(); ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <em>*Leave dates blank if you want them to auto assign the earned and expired dates</em>
                            </div>
                            <div class="row mb-3" id="date_earned">
                                <label class="col-md-4">Date Earned</label>
                                <div class="col-md-8">
                                    <input type="date" name="date_earned">
                                </div>
                            </div>
                             <div class="row mb-3" id="date_expires">
                                <label class="col-md-4">Date Expires</label>
                                <div class="col-md-8">
                                    <input type="date" name="date_expires">
                                </div>
                            </div>
                            <input type="hidden" name="user" value="<?php echo $profileData->user_id; ?>">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="addSaveAward btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="editAwardModal" tabindex="-1" role="dialog" aria-labelledby="addAwardLabel" aria-hidden="true">
            <div class="modal-dialog modal-fullscreen-md-down modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="modal-title" id="editAwardLabel">Edit Certificate or Badge</h3>
                        <span class="btn-close" data-bs-dismiss="modal"></span>
                    </div>
                    <div class="modal-body" id="editAwardBody">
                        <form id="editAwardForm" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $profileData->user_id; ?>">
                            <div class="row mb-3">
                                <div class="col-md-12 editAwardTitle" style="font-size: 20px; font-weight: bold;"></div>
                            </div>
                            <div class="row mb-3">
                                <em>*Leave dates blank if you want them to auto assign the earned and expired dates</em>
                            </div>
                            <div class="row mb-3">
                                <label class="col-md-4">Date Earned</label>
                                <div class="col-md-8">
                                    <input type="date" name="edit_date_earned">
                                </div>
                            </div>
                             <div class="row mb-3">
                                <label class="col-md-4">Date Expires</label>
                                <div class="col-md-8">
                                    <input type="date" name="edit_date_expires">
                                </div>
                            </div>
                            <input type="hidden" name="edit_award_user_id" value="<?php echo $profileData->user_id; ?>">
                            <input type="hidden" name="edit_award_id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="editSaveAward btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <!-- @Deprecated -->
        <form id="upgradeLevelForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $bundle->id; ?>">
            <h6 class="mb-3">Options</h4>
            <div class="row mb-3">
                <label class="col-md-4">Course <span class="required">*</span></label>
                <div class="col-md-8">
                    <select name="course_id">
                        <option selected>Select Course</option>
                        <?php foreach ($bundle->courses as $c) {
                            $clickable = '';
                            foreach($bundle->course_purchases as $cp) {
                                if($cp->course_id == $c->splms_course_id) {
                                    $clickable = 'disabled';
                                    break;
                                }
                            } ?>
                            <option value="<?php echo $c->splms_course_id; ?>" <?php echo $clickable; ?>><?php echo $c->title; ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="levelPaymentDetails" style="display: none;">
                <div class="row mb-3">
                    <label class="col-md-4">Card to Use<span class="required">*</span></label>
                    <div class="col-md-8">
                        <select name="course_card">
                            <?php foreach ($bundle->cards as $c) { ?>
                                <option value="<?php echo $c->id; ?>"><?php echo sprintf("%04d", $c->cardNumber); ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-md-4">Amount to charge<span class="required">*</span></label>
                    <div class="col-md-8">
                        <input type="text" name="courseAdjustedAmount"/>
                    </div>
                </div>
            </div>
        </form>

        <form id="moveReferreeForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data">
            <div class="code-group row">
                <label class="col-md-4">New Parent<span class="required">*</span></label>
                <div class="col-md-8">
                    <input type="number" name="parent" value="" maxlength="16">
                    <span class="code-message help-block"></span>
                </div>
            </div>
        </form>

        <form id="addReferreeForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $bundle->id; ?>">
            <div class="code-group row">
                <label class="col-md-4">New Referee<span class="required">*</span></label>
                <div class="col-md-8">
                    <input type="number" name="child" value="" maxlength="16">
                    <span class="code-message help-block"></span>
                </div>
            </div>
        </form>

        <form id="changeReferrerForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $bundle->id; ?>">
            <div class="code-group row">
                <label class="col-md-4">New Referer<span class="required">*</span></label>
                <div class="col-md-8">
                    <input type="number" name="parent" value="" maxlength="16">
                    <span class="code-message help-block"></span>
                </div>
            </div>
        </form>

        <form id="addFamilyForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user="<?php echo $bundle->id; ?>">
            <div class="code-group row">
                <div class="col-md-4 mb-3">
                    <label>Family Member Id<span class="required">*</span></label>
                </div>
                <div class="col-md-8 mb-3">
                    <input type="number" name="family" value="" maxlength="16">
                    <span class="code-message help-block"></span>
                </div>
                <div class="col-md-4 mb-3">
                    <label>Relationship</label>
                </div>
                <div class="col-md-8 mb-3">
                    <select name="relationship">
                        <option value="Spouse">Spouse</option>
                        <option value="Child">Child</option>
                        <option value="Parent">Parent</option>
                        <option value="Other">Other</option>
                    </select>
                </div>
            </div>
        </form>

        <form id="addCourseProgressForm" style="display: none;" class="form form-horizontal" enctype="multipart/form-data" data-user-id="<?php echo $bundle->id; ?>">
            <h6>Select Course</h4>

            <div class="form-group">
                <label class="col-md-4 control-label">Course <span class="required">*</span></label>
                <div class="col-md-8">
                    <select name="course_id">
                        <option selected>Select Course</option>

                        <?php
                            foreach ($bundle->courses as $c) {
                                $clickable = '';
                                $display = 'block';

                                foreach($bundle->course_progress as $cp) {
                                    if($cp->course_id == $c->splms_course_id) {
                                        $clickable = 'disabled';
                                        $display = 'none';

                                        break;
                                    }
                                }
                        ?>

                            <option style="display: <?php echo $display; ?>" value="<?php echo $c->splms_course_id; ?>" <?php echo $clickable; ?>> <?php echo $c->title; ?> </option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group" id="dateStarted">
                <label class="col-md-4 control-label">Date Started</label>
                <div class="col-md-8">
                    <input type="date" class="form-control" name="date_started">
                </div>
            </div>

            <div class="form-group" id="dateCompleted">
                <label class="col-md-4 control-label">Date Completed</label>
                <div class="col-md-8">
                    <input type="date" class="form-control" name="date_completed">
                </div>
            </div>
        </form>
    <?php }

    public static function get_link($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery   (true);
        $query
          ->select('name')
          ->from($db->quoteName('#__users'))
          ->where($db->quoteName('id') . "=" . $db->quote($id));
        $db->setQuery($query);
        $user = $db->loadObject();

        return "<a href='/administrator/index.php?option=com_users&task=axsuser.edit&id=" . $id . "'>" . $user->name . "</a>";
    }
}
