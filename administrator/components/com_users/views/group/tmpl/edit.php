<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		if (task == 'group.cancel' || document.formvalidator.isValid(document.getElementById('group-form')))
		{
			Joomla.submitform(task, document.getElementById('group-form'));
		}
	};
");
?>

<form action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="group-form" class="form-validate" style="padding-left:16px;padding-top:16px">
	<fieldset>
		<div class="row mb-3">
			<div class="col-sm-3">
				<?php echo $this->form->getLabel('title'); ?>
			</div>
			<div class="col-sm-9">
				<?php echo $this->form->getInput('title'); ?>
			</div>
		</div>
		<div class="row mb-3">
			<?php $parent_id = $this->form->getField('parent_id'); ?>
			<?php if (!$parent_id->hidden) : ?>
				<div class="col-sm-3">
					<?php echo $parent_id->label; ?>
				</div>
			<?php endif; ?>
			<div class="col-sm-9">
				<?php echo $parent_id->input; ?>
			</div>
		</div>
	</fieldset>
	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>
