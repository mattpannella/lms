<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * User view class.
 *
 * @since  1.5
 */
class UsersViewAxsuser extends JViewLegacy {
	protected $form;
	protected $item;
	protected $grouplist;
	protected $groups;
	protected $state;
	protected $bundle;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 *
	 * @since   1.5
	 */
	public function display($tpl = null) {
		$this->form      = $this->get('Form');
		$this->item      = $this->get('Item');
		$this->grouplist = $this->get('Groups');
		$this->groups    = $this->get('AssignedGroups');
		$this->state     = $this->get('State');
		$this->tfaform   = $this->get('Twofactorform');
		$this->otpConfig = $this->get('otpConfig');

		if ($this->item->id) {
			$this->bundle = new stdClass();
			$this->bundle->id = $this->item->id;
			$this->bundle->cards = AxsPayment::getUserAllCards($this->item->id);
			$this->bundle->data = AxsExtra::getUserProfileData($this->item->id);
			$this->bundle->data->user_id = $this->item->id;
			//$this->bundle->affiliate_level = AxsExtra::getUserAffiliateLevel($this->item->id);
			//$this->bundle->family = AxsExtra::getFamilyMembers($this->item->id);
			//$this->bundle->rewards_data = AxsExtra::getUserRewardsData($this->item->id);
			//$this->bundle->referees = AxsExtra::getUserChildren($this->item->id);
			$this->bundle->subscriptions = AxsPayment::getUserSubscriptions($this->item->id);
			$this->bundle->course_purchases = AxsPayment::getUserCoursePurchases($this->item->id);
			$this->bundle->course_activity = AxsExtra::getUserCourseActivity($this->item->id);
			$this->bundle->course_progress = AxsExtra::getUserCourseProgress($this->item->id);
			//$this->bundle->rewards_trxns = AxsExtra::getUserRewardsTransactions($this->item->id);
			//$this->bundle->affiliate_codes = AxsExtra::getUserAffiliateCodes($this->item->id);
			//$this->bundle->commissions = AxsExtra::getUserCommissions($this->item->id);
			$this->bundle->countries = AxsExtra::getAvailableCountries();
			$this->bundle->plans = AxsPayment::getAllPlans();
			$this->bundle->courses = AxsExtra::getCourses();
			$this->bundle->certifications = AxsLearnerDashboard::getAllUserAwards($this->item->id); //all courses available
			//give the course_purchases the courses.

			if(is_iterable($this->bundle->courses_purchases)) {

				foreach ($this->bundle->courses_purchases as $cp) {

					if(is_iterable($this->bundle->courses)) {

						foreach ($this->bundle->courses as $c) {
							if ($cp->course_id == $c->splms_course_id) {
								$cp->course = $c;
								break;
							}
						}
					}
				}
			}
		}

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->form->setValue('password', null);
		$this->form->setValue('password2', null);

		parent::display($tpl);
		$this->addToolbar();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user      = JFactory::getUser();
		$canDo     = JHelperContent::getActions('com_users');
		$isNew     = ($this->item->id == 0);
		$isProfile = $this->item->id == $user->id;

		if ($isNew) {
			$profile_text = 'COM_USERS_VIEW_NEW_USER_TITLE';
			$axs_profile = 'axsuser-add';
		} else {
			if ($isProfile) {
				$profile_text = 'COM_USERS_VIEW_EDIT_PROFILE_TITLE';
				$axs_profile = 'axsuser-profile';
			} else {
				$profile_text = 'COM_USERS_VIEW_EDIT_USER_TITLE';
				$axs_profile = 'axsuser-edit';
			}
		}

		JToolbarHelper::title(JText::_($profile_text), 'axsuser ' . ($axs_profile));

		if ($canDo->get('core.edit') || $canDo->get('core.create')) {
			JToolbarHelper::apply('axsuser.apply');
			JToolbarHelper::save('axsuser.save');
		}

		if ($canDo->get('core.create') && $canDo->get('core.manage')) {
			JToolbarHelper::save2new('axsuser.save2new');
		}

		if (empty($this->item->id)) {
			JToolbarHelper::cancel('axsuser.cancel');
		} else {
			JToolbarHelper::cancel('axsuser.cancel', 'JTOOLBAR_CLOSE');
		}

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_USERS_USER_MANAGER_EDIT');
	}
}
