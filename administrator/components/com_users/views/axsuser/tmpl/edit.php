<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$app = JFactory::getApplication();
$document = JFactory::getDocument();
$id = $app->input->get('id',0,'INT');
$admin = JFactory::getUser();
if($id) {
	$user = JFactory::getUser($id);
	$userParams = CFactory::getUser($id);
	$avatar = $userParams->getAvatar();
	if($user->otep || $user->otpKey) {
		$twofacterBadge = '<span class="badge bg-success">Enabled</span>';
		$twoFactorEnabled = true;
	} else {
		$twofacterBadge = '<span class="badge bg-secondary">Disabled</span>';
		$twoFactorEnabled = false;
	}
}

if (AxsUser::isSuperUser($id) || AxsUser::isDeveloper($id)) {
	//If this is a super user or developer, the user can't view them unless they're also a super user or developer.
	$isSuperUser = AxsUser::isSuperUser($admin->id);
	if (!$isSuperUser) {
		$isDev = AxsUser::isSuperUser($admin->id);
	}

	if (!$isSuperUser && !$isDev) {
		return;
	}
}

$brand = AxsBrands::getBrand();
$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

$edit_rights = $_SESSION['admin_access_levels']->rights;

if($id) {
	$encryptedTranscriptLink       = AxsLMS::getEncryptedTranscriptLink($id);
	$encryptedLearnerDashboardLink = AxsLMS::getEncryptedLearnerDashboardLink($id);
	$key = AxsKeys::getKey('lms');
	$encrypted2FAUser = base64_encode(AxsEncryption::encrypt($id, $key));
	$fields = AxsUser::getAllUserCustomFieldsValues($id);
	$profileLink = '/my-profile/'.$userParams->getAlias();
	$durationData = new stdClass();
	$durationData->user_id = $id;
	$durationData->type    = "site";
	$durationData->start   = date('Y-m-d',strtotime('-7 days'));
	$durationData->end     = date('Y-m-d');
	$durationData->duration_total = true;
	$durationData->get_minutes    = false;
	$timeOnSite = AxsDuration::getDurationSpan($durationData);
	$comments = AxsUser::getCourseComments($id);
} else {
	$fields = AxsUser::getAllCustomFields();
}

$fullwidth_fields = ['textarea'];

include_once("components/com_users/helpers/user_extras.php");

// Include the component HTML helpers.
if($this->item->id) {
	JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
	JLoader::register('UserExtrasHelper', JPATH_COMPONENT . '/helpers/user_extras.php');
	JFactory::getDocument()->addScript('/node_modules/vis/dist/vis.min.js');
	JFactory::getDocument()->addStyleSheet('/node_modules/vis/dist/vis.min.css');
	JFactory::getDocument()->addScript('/node_modules/handlebars/dist/handlebars.min.js');
	JFactory::getDocument()->addScript('components/com_users/assets/js/axsuser.js', array('version' => 'auto'));
	JFactory::getDocument()->addScript('https://js.stripe.com/v3/');
	JFactory::getDocument()->addStyleSheet('components/com_users/assets/css/axsuser.css', array('version' => 'auto'));
	UserExtrasHelper::extras($this->bundle);
}

JHtml::_('behavior.formvalidator');

JFactory::getDocument()->addScriptDeclaration("
	Joomla.submitbutton = function(task)
	{
		var userForm = document.getElementById('axsuser-form');
		if (task == 'axsuser.cancel' || document.formvalidator.isValid(userForm))
		{
			Joomla.submitform(task, userForm);
		}
	};

	Joomla.twoFactorMethodChange = function(e)
	{
		var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

		jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
			if (el.id != selectedPane)
			{
				jQuery('#' + el.id).hide(0);
			}
			else
			{
				jQuery('#' + el.id).show(0);
			}
		});
	};
");

// Get the form fieldsets.
$fieldsets = $this->form->getFieldsets();

if (!$this->bundle->id) {
	$newUser = true;
} else {
	$newUser = false;
}

?>
<script>
	var currency_symbol = '<?php echo $currency_symbol; ?>';

	$(document).ready(function() {
		if(window.location.hash && window.location.hash == "#timeline") { 
			var hash = window.location.hash;
			$("#myTabTabs").find('li a[href="#details"]').removeClass("active");
			$("#details").removeClass("active");
			$("#myTabTabs").find('li a[href="'+hash+'"]').addClass("active");
			$("#timeline").addClass("active").show();
		}
	});
</script>
<form
	action="<?php echo JRoute::_('index.php?option=com_users&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post"
	name="adminForm"
	id="axsuser-form"
	class="form-validate form-horizontal"
	enctype="multipart/form-data"
>
	<style>
		.user-details input{
			display: block;
		}

		.user-avatar-image {
			width: 100%;
			max-width: 160px;
			height: auto;
		}
		.action-buttons {
			margin-top: 10px;
		}
		.user-account-icons {
			display: flex;
			justify-content: flex-start;
			align-items: center;
			margin-top: 12px;
			margin-bottom: 12px;
		}
		.user-account-icons-item {
			margin-right: 16px;
			display: flex;
			justify-content: flex-start;
			align-items: center;
		}
		.user-icon {
			background: #e5e7eb;
			width: 52px;
			height: 52px;
			color: #2563eb;
			border-radius: 10px;
			text-align: center;
			line-height: 52px;
			font-size: 22px;
			margin-right: 8px;
		}
		.user-account-icons-text span {
			display: block;
		}
		.user-account-icons-manintext {
			font-weight: bold;
			font-size: 16px;
		}
		.user-account-icons-subtext {
			font-size: 14px;
		}
		.user-account-name {
			font-weight: 600;
		}
		#jform_teacher_title-lbl, #jform_teacher_title {
			display: none;
		}
		.comment-container {
			background: #e5e7eb;
			border-radius: 10px;
			padding: 16px;
			margin: 16px;
		}
		.comment-info {
			font-weight: bold;
		}
		.comment-text {
			margin-top: 28px;
			margin-bottom: 20px
		}
		.profile-field-box
		textarea, .profile-field-box
		input, .profile-field-box
		select {
			max-width: 100%;
		}
	</style

	<fieldset class="adminform">
		<?php
			echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'details'));

			echo JHtml::_('bootstrap.addTab', 'myTab', 'details', JText::_('COM_USERS_USER_ACCOUNT_DETAILS', true));
		?>
		<div class="container-fluid user-details">
			<div class="row">
				<div class="col-md-8">
					<?php if($user) { ?>
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3">
									<img src="<?php echo $avatar; ?>" class="rounded user-avatar-image img-responsive"/>
								</div>
								<div class="col-md-9">
									<div class="row">
										<h4 class="user-account-name"><?php echo $user->name; ?></h4>
									</div>
									<div class="row">
										<div class="user-account-icons">
											<div class="user-account-icons-item">
												<div class="user-icon">
													<span class="fas fa-clock"></span>
												</div>
												<div class="user-account-icons-text">
													<span class="user-account-icons-manintext">
														<?php echo $timeOnSite; ?> Hours
													</span>
													<span class="user-account-icons-subtext">
														This Week
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="action-buttons-container">
											<a id="viewProfile" href="<?php echo $profileLink; ?>" target="_blank" class="action-buttons btn btn-primary me-1">View Profile</a>
										<?php if($encryptedTranscriptLink) { ?>
											<a id="viewTranscript" href="<?php echo $encryptedTranscriptLink; ?>" target="_blank" class="action-buttons btn btn-primary me-1">View Transcript</a>
										<?php } ?>

										<?php if($encryptedLearnerDashboardLink) { ?>
											<a id="viewDashboard" href="<?php echo $encryptedLearnerDashboardLink; ?>" target="_blank" class="action-buttons btn btn-primary me-1">View Learner Dashboard</a>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>

					<div class='card'>
						<h5 class="card-header">System Fields</h5>
						<div class='card-body'>
							<div class='row'>
							<?php foreach ($this->form->getSection('system_fields') as $field) { ?>
								<?php
									if (in_array($field->fieldname, array('password', 'password2', 'name', 'username'))){
										$colClass = "col-md-6";
									} else {
										$colClass = "row";
									}
								?>
								<div class="<?php echo $colClass; ?>">
									<h5 class="card-title">
										<?php echo $field->label; ?>
									</h5>
									<div class="mb-4">
										<?php if ($field->fieldname == 'password') { // Disables autocomplete ?>
											<input type="text" style="display:none;" />
										<?php }  echo $field->input; ?>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
					<div class='card profile-field-box'>
						<h5 class="card-header">Profile Fields</h5>
						<div class='card-body'>
							<div class="row">
							<?php foreach ($fields as $field) {
								$field->disableRequired = true;
								$html = AxsHTML::getField($field);
								$fieldName = '';
								$fieldParams = json_decode($field->params);
								if($fieldParams->registration_text) {
									$fieldName = $fieldParams->registration_text;
								} else {
									$fieldName = $field->name;
								}
								if (in_array($field->type,$fullwidth_fields)) {
									$colClass = "col-md-12";
								} else {
									$colClass = "col-md-6";
								}
							?>
								<div class="<?php echo $colClass; ?>">
									<h5 class="card-title">
										<label>
										<?php echo AxsLanguage::text($fieldName, $fieldName); ?>
										</label>
									</h5>
									<div class="mb-4">
										<?php echo $html; ?>
									</div>
								</div>
							<?php } ?>
							</div>
						</div>
					</div>
					<?php if($id && $admin->id != $id) { ?>
					<div class='card'>
						<h5 class="card-header">Account Deletion</h5>
						<div class='card-body'>
							<div class="row">
								<p style="color: red;">This action cannot be undone or recovered!</p>
								<p><a type="submit" href="/administrator/index.php?option=com_users&task=axsuser.deleteUser&id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to delete this user?')" id="delete-user" class="btn btn-danger" style="max-width:185px;"><i class="fas fa-trash"></i> Delete Account</a></p>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<div class="col-md-4">
					<div class='card'>
						<h5 class="card-header">User Actions</h5>
						<div class='card-body'>
							<?php if (AxsClientData::getClientSubscriptionFromDb() != 'Tovuti Core'): ?>
								<h5 class="card-title">
									<label>Add Subscription</label>
								</h5>
								<div class="mb-4">
									<?php if (empty($id)): ?>
										<span data-bs-toggle="tooltip" title="User needs to be saved before subscription can be added"><button class="btn btn-success disabled" type="button"  ><span class="fas fa-plus"></span> Add Subscription</button></span>
									<?php else: ?>
										<button id="addSubscription" class="btn btn-success" type="button" data-bs-toggle="modal" data-bs-target="#myModal"><span class="fas fa-plus"></span> Add Subscription</button>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php foreach ($this->form->getSection('user_actions') as $field) { ?>
								<h5 class="card-title">
									<?php echo $field->label; ?>
								</h5>
								<div class="mb-4">
									<?php echo $field->input; ?>
								</div>
							<?php } ?>
							<?php if ($id) : ?>
								<h5 class="card-title">
									<label>Two-Factor Authentication <?php echo $twofacterBadge; ?></label>
								</h5>
								<div class="mb-4">
									<?php if ($twoFactorEnabled): ?>
										<a id="disable2FA" href="/administrator/index.php?option=com_users&task=axsuser.disable2FA&id=<?php echo $encrypted2FAUser; ?>" class="btn btn-danger" onclick="confirm('Are you sure you want to reset this user\'s Two-factor Authentication. This action can not be undone.')">Reset</a>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<div class='card'>
						<h5 class="card-header">User Details</h5>
						<div class='card-body'>
							<?php foreach ($this->form->getSection('user_details') as $field) { ?>
								<h5 class="card-title">
									<?php echo $field->label; ?>
								</h5>
								<div class="mb-4">
									<?php echo $field->input; ?>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="components/com_users/assets/js/user_account.js?v=2" type="text/javascript"></script>
	<?php
		echo JHtml::_('bootstrap.endTab');

		//Only show the timeline if they're an existing user.
		if (!$newUser) {
			$timeline = $fieldsets['timeline'];
			echo JHtml::_('bootstrap.addTab', 'myTab', $timeline->name, JText::_($timeline->label, true));
			$func = $timeline->name;
			UserExtrasHelper::$func($this->bundle);

			echo JHtml::_('bootstrap.endTab');
		}

		unset ($fieldsets['timeline']);

		$user_id = JFactory::getUser()->id;
		if (AxsUser::isSuperUser($user_id) || AxsUser::isAdmin($user_id) || AxsUser::isDeveloper($user_id)) {
			//If they have full admin rights, then they can skip this check.
			$edit_rights['edit_usergroup'] = true;
		}

		if ($edit_rights['edit_usergroup']) {
			//See if the user has permissions to change user groups
			if ($this->grouplist) {
				echo JHtml::_('bootstrap.addTab', 'myTab', 'groups', JText::_('COM_USERS_ASSIGNED_GROUPS', true));
				echo $this->loadTemplate('groups');
				echo JHtml::_('bootstrap.endTab');
			}
		}

		foreach ($fieldsets as $fieldset) {
			if ($fieldset->name == 'user_details' || $fieldset->name == 'improved' || $fieldset->name == 'settings') {
				//Don't use these.
				continue;
			}

			echo JHtml::_('bootstrap.addTab', 'myTab', $fieldset->name, JText::_($fieldset->label, true));

			switch($fieldset->name) {
				case 'timeline':
					break;
				case 'profile':
				case 'usertree':
				case 'cards':
				case 'courses':
				case 'certifications':
				case 'rewards':
					if($this->item->id) {
						$func = $fieldset->name;
						UserExtrasHelper::$func($this->bundle);
					}
					break;
				default:
					foreach ($this->form->getFieldset($fieldset->name) as $field) { ?>
						<?php if ($field->hidden) { ?>
							<div class="control-group">
								<div class="controls">
									<?php echo $field->input; ?>
								</div>
							</div>
						<?php } else { ?>
							<div class="control-group">
								<div class="control-label">
									<?php echo $field->label; ?>
								</div>
								<div class="controls">
									<?php echo $field->input; ?>
								</div>
							</div>
						<?php }
					}
					break;
			}

			echo JHtml::_('bootstrap.endTab');

		}

		echo JHtml::_('bootstrap.addTab', 'myTab', 'Comments', "Comments");
			include_once 'components/com_users/templates/comments.php';
		echo JHtml::_('bootstrap.endTab');


		?>


	<?php

		echo JHtml::_('bootstrap.endTabSet');
	?>
	</fieldset>

	<input type="hidden" name="task" value="" />
	<?php echo JHtml::_('form.token'); ?>
</form>