<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

?>
<button class="btn border" type="button" onclick="document.getElementById('batch-group-id').value=''" data-bs-dismiss="modal">
	<?php echo JText::_('JCANCEL'); ?>
</button>
<button class="btn btn-success" type="submit" onclick="setUserGroupBadgesOnReload()">
	<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
</button>

<script>
	function setUserGroupBadgesOnReload() {
		//When the Process button for Batches is clicked, we need to mark that the badges need to be recalculated in case they added or removed usergroups.
		//This will happen on the page refresh in /plugins/axs/awards/award.php
		jQuery.ajax({
	        type:       'POST',
	        url:        'index.php',
	        data:       {
	            'option':       'com_award',
	            'task':         'awards.set_badge_refresh_on_reload',
	            'format':       'raw'
	        },
	        success: function(response) {
	      		console.log(response);
	        }
	    });
		Joomla.submitbutton('axsuser.batch');
	}
</script>