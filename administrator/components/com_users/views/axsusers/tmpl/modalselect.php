<?php

	//This is an older version of the modal.php that uses select boxes and is needed for some component fields.


	/**
	 * @package     Joomla.Administrator
	 * @subpackage  com_users
	 *
	 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
	 * @license     GNU General Public License version 2 or later; see LICENSE.txt
	 */
	defined('_JEXEC') or die;
	JHtml::_('behavior.multiselect');
	$input     = JFactory::getApplication()->input;
	$listOrder = $this->escape($this->state->get('list.ordering'));
	$listDirn  = $this->escape($this->state->get('list.direction'));
?>

<style>
	.selectbox {
		cursor: pointer;
	}
</style>

<form action="<?php echo JRoute::_("index.php?option=com_users&view=axsusers&layout=modalselect&tmpl=component"); ?>" method="post" name="adminForm" id="adminForm">
	<fieldset class="filter">
		<div id="filter-bar" class="d-flex mb-3 mx-1">
			<div class="filter-search input-group">
				<input 
					type="text" 
					name="filter_search" 
					id="filter_search" 
					placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" 
					value="<?php echo $this->escape($this->state->get('filter.search')); ?>" 
					title="<?php echo JHtml::tooltipText('COM_USERS_SEARCH_IN_NAME'); ?>"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					data-bs-placement="bottom"
				/>
				<button
					type="submit"
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					data-bs-placement="bottom"
				><span class="icon-search"></span></button>
				<button
					type="button"
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>"
					data-bs-placement="bottom"
					data-bs-html="true"
					onclick="document.getElementById('filter_search').value='';this.form.submit();"
				><span class="icon-remove"></span></button>
			</div>
			<div class="float-end hidden-phone">
				<label for="filter_group_id" class="d-none"><?php echo JText::_('COM_USERS_FILTER_USER_GROUP'); ?></label>
				<?php echo JHtml::_('access.usergroup', 'filter_group_id', $this->state->get('filter.group_id'), 'onchange="this.form.submit()"'); ?>
			</div>
		</div>
	</fieldset>

	<?php 
		if (empty($this->items)) {
	?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
	<?php 
		} else { 
	?>
			<table class="table table-striped table-condensed">
				<thead>
					<tr>
          				<th width="1%" class="nowrap center">
							<?php echo JHtml::_('grid.checkall'); ?>
          				</th>
						<th class="left">
							<?php echo JHtml::_('grid.sort', 'COM_USERS_HEADING_NAME', 'a.name', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap" width="25%">
							<?php echo JHtml::_('grid.sort', 'JGLOBAL_USERNAME', 'a.username', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap" width="25%">
							<?php echo JText::_('COM_USERS_HEADING_GROUPS'); ?>
						</th>
          				<th width="1%" class="nowrap hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
          				</th>
					</tr>
				</thead>
				<tbody>
					<?php
        				$itemCount = count($this->items);
        				for ($i = 0; $i < $itemCount; $i++) { 
        					$item = $this->items[$i]; 
        			?>
          					<tr class="selectbox row<?php echo $i % 2; ?>">
            					<td class="center">
			        				<?php echo JHtml::_('grid.id', $i, $item->id); ?>
            					</td>
								<td>
									<span class="name"><?php echo $this->escape(addslashes($item->name)); ?></span>
								</td>
								<td align="center">
									<?php echo $item->username; ?>
								</td>
								<td align="left">
									<?php echo nl2br($item->group_names); ?>
								</td>
								<td class="hidden-phone">
									<?php echo (int) $item->id; ?>
								</td>
							</tr>
        			<?php 
        				}
        			?>
				</tbody>
      			<tfoot>
        			<tr>
          				<td colspan="15">
            				<?php echo $this->pagination->getListFooter(); ?>
          				</td>
        			</tr>
      			</tfoot>
			</table>
	<?php 
		} 
	?>

	<div>
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>

<script>
	(function($) {
		$(function() {
			$(window).on('message', function(e) {
				if (e.originalEvent.data == 'getSelected') {
					//grab all the selected's name and id.
					var checked = $('#adminForm tbody input[type=checkbox]:checked');
					var usersData = [];
					checked.each(function(index) {
						var checkbox = $(checked[index]);
						var obj = {
							value: checkbox.val(),
							text: checkbox.parent().parent().find('.name').text()
						};
						usersData.push(obj);
					});
					window.parent.postMessage(JSON.stringify({eventName: "saveUsers", usersData}), '*');
				}
			});

			$(document).ready(
				function() {

					var checkboxes = document.getElementsByTagName("input");
					$(checkboxes).each(
						function() {
							if (this.getAttribute("type") == "checkbox") {
								$(this).click(
									function(e) {
										e.stopPropagation();
									}
								);	
							}
						}
					);

					$(".selectbox").click(
						function(e) {
							let checkbox = this.getElementsByTagName("input")[0];
							checkbox.click();
						}
					);
				}
			);
		});
	})(jQuery);
</script>