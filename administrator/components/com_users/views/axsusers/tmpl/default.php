<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');

$doc = JFactory::getDocument();
$doc->addScript( '../components/shared/includes/js/export_csv.js', array('version' => 'auto'));
$doc->addScriptDeclaration(trim('
	// create an export to csv button and append it to the toolbar
	jQuery( function()  {
		const exportToCSVButtonMarkup = `
		<div id="toolbar-export-to-csv" class="btn-wrapper">
			<button 
				id="export_to_csv" 
				onclick="csvExport();" 
				type="button"
				class="btn btn-primary btn-sm"
			><span class="lizicon-file-text2"></span> Export to CSV</button>
		</div>`;
		
		const exportToCSVButtonElement = jQuery.parseHTML(exportToCSVButtonMarkup);
		jQuery("#toolbar").append(exportToCSVButtonElement);
	});
'));

$listOrder  = $this->escape($this->state->get('list.ordering'));
$listDirn   = $this->escape($this->state->get('list.direction'));
$loggeduser = JFactory::getUser();

$csvData = array();
$csvData[0] = array();
$csvCount = 0;

$userId = $loggeduser->id;
?>

<form
	id="adminForm"
	name="adminForm"
	action="<?php echo JRoute::_('index.php?option=com_users&view=axsusers');?>"
	method="post"
>
	<div id="j-main-container">

		<?php
			// Search tools bar
			echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
		?>

		<div class="my-2">

			<?php
				if (empty($this->items)) {
			?>
					<div class="alert alert-no-items">
						<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
					</div>
			<?php
				} else {
			?>
				<table id="userList" class="table table-striped" >
					<thead>
						<tr>
							<th width="1%" class="nowrap center">
								<?php echo JHtml::_('grid.checkall'); ?>
							</th>
							<th class="left">
								<?php
									echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_NAME', 'a.name', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('COM_USERS_HEADING_NAME');
								?>
							</th>
							<th width="10%" class="nowrap">
								<?php
									echo JHtml::_('searchtools.sort', 'JGLOBAL_USERNAME', 'a.username', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('JGLOBAL_USERNAME');
								?>
							</th>
							<th width="5%" class="nowrap center">
								<?php
									echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_ENABLED', 'a.block', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('COM_USERS_HEADING_ENABLED');
								?>
							</th>

							<th width="10%" class="nowrap">
								<?php
									echo JText::_('COM_USERS_HEADING_GROUPS');
									//echo JHtml::_('searchtools.sort', "COM_USERS_HEADING_GROUPS", 'a.group_names', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('COM_USERS_HEADING_GROUPS');
								?>
							</th>
							<th width="15%" class="nowrap hidden-phone">
								<?php
									echo JHtml::_('searchtools.sort', 'JGLOBAL_EMAIL', 'a.email', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('JGLOBAL_EMAIL');
								?>
							</th>
							<th width="10%" class="nowrap hidden-phone">
								<?php
									echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_LAST_VISIT_DATE', 'a.lastvisitDate', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('COM_USERS_HEADING_LAST_VISIT_DATE');
								?>
							</th>
							<th width="10%" class="nowrap hidden-phone">
								<?php
									echo JHtml::_('searchtools.sort', 'COM_USERS_HEADING_REGISTRATION_DATE', 'a.registerDate', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('COM_USERS_HEADING_REGISTRATION_DATE');
								?>
							</th>
							<th width="1%" class="nowrap hidden-phone">
								<?php
									echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder);
									$csvData[0][$csvCount++] = JText::_('JGRID_HEADING_ID');
								?>
							</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td colspan="15">
								<?php echo $this->pagination->getListFooter();?>
							</td>
						</tr>
					</tfoot>
					<tbody>
					<?php
						$rowCount = 1;

						foreach ($this->items as $i => $item) {
							$csvData[$rowCount] = array();
							$csvCount = 0;
							$canEdit   = $this->canDo->get('core.edit');
							$canChange = $loggeduser->authorise('core.edit.state',	'com_users');

							// If this group is super admin and this user is not super admin, $canEdit is false
							if ((!$loggeduser->authorise('core.admin')) && JAccess::check($item->id, 'core.admin')) {
								$canEdit   = false;
								$canChange = false;
							}

					?>
							<tr class="row<?php echo $i % 2; ?>">
								<td class="center">
									<?php
										if ($canEdit) {
											echo JHtml::_('grid.id', $i, $item->id);
										}
									?>
								</td>
								<td>
									<div class="name">
										<?php
											if ($canEdit) {
										?>
												<a href="<?php echo JRoute::_('index.php?option=com_users&task=axsuser.edit&id=' . (int) $item->id); ?>" title="<?php echo JText::sprintf('COM_USERS_EDIT_USER', $this->escape($item->name)); ?>">
													<?php echo $this->escape($item->name); ?>
												</a>
										<?php

											} else {
												echo $this->escape($item->name);
											}

											$csvData[$rowCount][$csvCount++] = $this->escape($item->name);
										?>
									</div>
									<div class="btn-group mt-2">
										<?php echo JHtml::_('users.filterNotes', $item->note_count, $item->id); ?>
										<?php echo JHtml::_('users.notes', $item->note_count, $item->id); ?>
										<?php echo JHtml::_('users.addNote', $item->id); ?>
									</div>

									<?php
										echo JHtml::_('users.notesModal', $item->note_count, $item->id);

										if ($item->requireReset == '1') {
									?>
											<span class="label label-warning"><?php echo JText::_('COM_USERS_PASSWORD_RESET_REQUIRED'); ?></span>
									<?php
										}

										if (JDEBUG) {
									?>
											<div class="small"><a href="<?php echo JRoute::_('index.php?option=com_users&view=debuguser&user_id=' . (int) $item->id);?>">
											<?php echo JText::_('COM_USERS_DEBUG_USER');?></a></div>
									<?php
										}
									?>
								</td>
								<td>
									<?php
										echo $this->escape($item->username);
										$csvData[$rowCount][$csvCount++] = $this->escape($item->username);
									?>
								</td>

								<td class="center">
									<?php
										if ($canChange) {
											$text = 'can';
											$self = $loggeduser->id == $item->id;
											echo JHtml::_('jgrid.state', JHtmlUsers::blockStates($self), $item->block, $i, 'axsusers.', !$self);
										} else {
											echo JText::_($item->block ? 'JNO' : 'JYES');
										}

										if ($item->block) {
											$csvData[$rowCount][$csvCount++] = 'No';
										} else {
											$csvData[$rowCount][$csvCount++] = 'Yes';
										}
									?>
								</td>

								<td>
									<?php
										if (substr_count($item->group_names, "\n") > 1) {
									?>
											<span class="hasTooltip" title="<?php echo JHtml::tooltipText(JText::_('COM_USERS_HEADING_GROUPS'), nl2br($item->group_names), 0); ?>">
												<?php echo JText::_('COM_USERS_USERS_MULTIPLE_GROUPS'); ?>
											</span>
									<?php
										} else {
											echo nl2br($item->group_names);
										}

										$csvData[$rowCount][$csvCount++] = str_replace("\n", ", ", $item->group_names);
									?>
								</td>
								<td class="hidden-phone">
									<?php
										echo JStringPunycode::emailToUTF8($this->escape($item->email));
										$csvData[$rowCount][$csvCount++] = $this->escape($item->email);
									?>
								</td>
								<td class="hidden-phone">
                  <?php
                    if ($item->lastvisitDate != '0000-00-00 00:00:00') {

                      // Get a date object based on the correct timezone.
                      $date = JFactory::getDate($item->lastvisitDate, 'UTC');
                      $date->setTimezone($loggeduser->getTimezone());

                      // get current timezone and temporarily set to UTC
                      $tz = date_default_timezone_get();
                      date_default_timezone_set('UTC');

                      // Transform the date string.
                      $text = strftime('%Y-%m-%d %H:%M:%S %Z', strtotime($date->format('Y-m-d H:i:s', true, false)));
                      // set back to original timezone
                      date_default_timezone_set($tz);
                    } else {
                      $text = JText::_('JNEVER');
                    }

                    echo $text;
                    $csvData[$rowCount][$csvCount++] = $text;
                  ?>
								</td>
								<td class="hidden-phone">
                  <?php
                    // Get a date object based on the correct timezone.
                    $date = JFactory::getDate($item->registerDate, 'UTC');
                    $date->setTimezone($loggeduser->getTimezone());

                    // get current timezone and temporarily set to UTC
                    $tz = date_default_timezone_get();
                    date_default_timezone_set('UTC');

                    // Transform the date string.
                    $text = strftime('%Y-%m-%d %H:%M:%S %Z', strtotime($date->format('Y-m-d H:i:s', true, false)));

                    // set back to original timezone
                    date_default_timezone_set($tz);

                    echo $text;
                    $csvData[$rowCount][$csvCount++] = $text;
                  ?>
								</td>
								<td class="hidden-phone">
									<?php
										echo (int) $item->id;
										$csvData[$rowCount][$csvCount++] = (int)$item->id;
									?>
								</td>
							</tr>
					<?php
							$rowCount++;
						}
					?>
					</tbody>
				</table>
			<?php
					// Load the batch processing form if user is allowed
					if (
							$loggeduser->authorise('core.create', 'com_users') &&
							$loggeduser->authorise('core.edit', 'com_users') &&
							$loggeduser->authorise('core.edit.state', 'com_users')
						) {

						echo JHtml::_(
							'bootstrap.renderModal',
							'collapseModal',
							array(
								'title' => JText::_('COM_USERS_BATCH_OPTIONS'),
								'footer' => $this->loadTemplate('batch_footer')
							),
							$this->loadTemplate('batch_body')
						);
					}
				}

			?>
			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</div>
</form>

<script>
	<?php $csvData = str_replace("\t", ' ', json_encode($csvData));	?>
	function csvExport() {
		exportToCsv('users.csv', JSON.parse(<?php echo ("`" . $csvData . "`"); ?> ));
	}
</script>
