<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Create the copy/move options.
$options = array(
	JHtml::_('select.option', 'add', JText::_('COM_USERS_BATCH_ADD')),
	JHtml::_('select.option', 'del', JText::_('COM_USERS_BATCH_DELETE')),
	JHtml::_('select.option', 'set', JText::_('COM_USERS_BATCH_SET'))
);

// Create the reset password options.
$resetOptions = array(
	JHtml::_('select.option', '', JText::_('COM_USERS_NO_ACTION')),
	JHtml::_('select.option', 'yes', JText::_('JYES')),
	JHtml::_('select.option', 'no', JText::_('JNO'))
);
?>

<div class="row mb-3">
	<div class="col-sm-3">
		<label><?php echo JText::_('COM_USERS_BATCH_GROUP_ACTION_SELECT_LABEL'); ?></label>
	</div>
	<div class="col-sm-9">
		<div class="radio">
			<?php echo JHtml::_('select.radiolist', $options, 'batch[group_action]', '', 'value', 'text', 'add') ?>
		</div>
	</div>
</div>

<div class="row mb-3">
	<div id="batch-choose-action" class="combo col-sm-3">
		<label id="batch-choose-action-lbl" class="control-label" for="batch-choose-action">
			<?php echo JText::_('COM_USERS_BATCH_GROUP') ?>
		</label>
	</div>
	<div id="batch-choose-action" class="combo col-sm-9">
		<select name="batch[group_id]" id="batch-group-id">
			<option value=""><?php echo JText::_('JSELECT') ?></option>
			<?php echo JHtml::_('select.options', JHtml::_('user.groups')); ?>
		</select>
	</div>
</div>

<div class="row mb-3">
	<div class="col-sm-3">
		<label><?php echo JText::_('COM_USERS_REQUIRE_PASSWORD_RESET'); ?></label>
	</div>
	<div class="col-sm-9">
		<div class="radio">
			<?php echo JHtml::_('select.radiolist', $resetOptions, 'batch[reset_id]', '', 'value', 'text', '') ?>
		</div>
	</div>
</div>