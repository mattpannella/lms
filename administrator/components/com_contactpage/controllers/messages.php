<?php
defined('_JEXEC') or die();

class ContactpageControllerMessages extends FOFController {

	public function editItem(){
		$input  = JFactory::getApplication()->input;
    	$option = $input->get('option');
    	$view 	= 'message';
    	$cid 	= $input->post->get('cid', array(), 'array');
    	JArrayHelper::toInteger($cid);
    	$id = $cid[0];
    	$url = "index.php?option=".$option."&view=".$view."&id=".$id;
    	$this->setRedirect($url);    	
	}
}