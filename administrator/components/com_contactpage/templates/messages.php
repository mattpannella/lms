<?php
    echo AxsLoader::loadKendo();
?>
<table id="messagesGrid" class="table table-bordered table-striped table-responsive">
    <tr>
        <th>View</th>
        <th>From</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Message</th>
        <th>Date</th>
    </tr>
    <?php
        if (is_iterable($messages)):
            foreach ($messages as $row):
                ?>
                <tr>
                    <td class="text-center">
                        <button type="button" class="btn btn-primary" data-row="<?php echo htmlspecialchars(json_encode($row)); ?>" data-bs-toggle="modal" data-bs-target="#single-message-modal">
                            View
                        </button>
                    </td>
                    <td><?php echo $row->name; ?></td>
                    <td><?php echo $row->email; ?></td>
                    <td><?php echo $row->phone; ?></td>
                    <td><?php echo $row->message; ?></td>
                    <td><?php echo date('m/d/Y',strtotime($row->date_sent)); ?></td>
                </tr>
            <?php
            endforeach;
        endif;
    ?>
</table>
<div class="modal fade" id="single-message-modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Message Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm">From</label>
                    <div class="col-sm-10">
                        <span id="message-name"></span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm">Email</label>
                    <div class="col-sm-10">
                        <span id="message-email"></span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm">Phone</label>
                    <div class="col-sm-10">
                        <span id="message-phone"></span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm">Date</label>
                    <div class="col-sm-10">
                        <span id="message-date_sent"></span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm">Message</label>
                    <div class="col-sm-10">
                        <span id="message-message"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<script>
  const populateModalForm = function(data = null) {
    if (!data) {
      document.querySelectorAll('.modal .row [id^=message-]').forEach(function(elem) {
        elem.innerHTML = '';
      });

      return;
    }

    Object.keys(data).forEach(function(key) {
      const elem = document.querySelector('#message-' + key);
      if (elem) {
        elem.innerHTML = data[key];
      }
    });
  };

  const modal = document.querySelector('#single-message-modal');
  modal.addEventListener('show.bs.modal', function(e) {
    const data = JSON.parse(e.relatedTarget.dataset.row);
    populateModalForm(data);
  });

  modal.addEventListener('hidden.bs.modal', function() {
    populateModalForm();
  });

  var kendoOptions = {
    toolbar: ["pdf", "excel"],
    sortable:   true,
    groupable:  false,
    resizable:  true,
    columnMenu: true,
    filterable: {
      mode: "row"
    },
    /*height:   600,*/
    pageable: {
      buttonCount:    8,
      input:          true,
      pageSize:       20,
      pageSizes:      true,
      refresh:        true,
      message: {
        empty:      "There are no entries to display"
      }
    },
    columns: [
      {
        field: "View",
        filterable: false,
        sortable: false,
        width: '100px'
      },
      {
        field: "From",
        filterable: {
          operators: {
            string: {
              contains: "Contains"
            }
          }
        }
      },
      {
        field: "Email",
        filterable: {
          operators: {
            string: {
              contains: "Contains"
            }
          }
        },
      },
      {
        field: "Phone",
        filterable: {
          operators: {
            string: {
              contains: "Contains"
            }
          }
        },
      },
      {
        field: "Message",
        filterable: {
          operators: {
            string: {
              contains: "Contains"
            }
          }
        },
      },
      {
        field: "Date",
        type: "date",
        format: "{0:MM/d/yyyy}",
        filterable: {
          ui: "datepicker"
        }
      }
    ]
  }

  var messagesGrid = $("#messagesGrid").kendoGrid(kendoOptions);
  var exportFlag = true;
  messagesGrid.data("kendoGrid").bind("excelExport", function (e) {
    if (!exportFlag) {
      e.sender.hideColumn(0);
      e.preventDefault();
      exportFlag = true;
      setTimeout(function () {
        e.sender.saveAsExcel();
      });
    } else {
      e.sender.showColumn(0);
      exportFlag = false;
    }
  });
</script>
