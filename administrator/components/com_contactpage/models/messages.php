<?php

defined('_JEXEC') or die;

class ContactpageModelMessages extends FOFModel {
	protected function populateState($ordering = null, $direction = null) {
		parent::populateState('id', 'DESC');
		$this->setState('filter_order', 'id');
    	$this->setState('filter_order_Dir', 'DESC');
	}
}