<?php

defined('_JEXEC') or die;

class ContactpageModelContactpages extends FOFModel {

	public function &getItemList($overrideLimits = false, $group = '') {
		$this->blacklistFilters(array('id'));
		$query = $this->buildQuery($overrideLimits);
		if (!$overrideLimits)
		{
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
			$this->list = $this->_getList((string) $query, $limitstart, $limit, $group);
		}
		else
		{
			$this->list = $this->_getList((string) $query, 0, 0, $group);
		}	

		return $this->list;
	}

	public function save($data) {
		$this->sanitizeData($data);
		$params = new stdClass();
		$params->show_map = $data['show_map'];
		$params->page_type = $data['page_type'];
		$params->map_position = $data['map_position'];
		$params->show_form = $data['show_form'];
		$params->notes = $data['contact_notes'];
		$params->name_field = $data['name_field'];
		$params->email_field = $data['email_field'];
		$params->phone_field = $data['phone_field'];
		$params->message_field = $data['message_field'];
		$params->captcha_field = $data['captcha_field'];
		$params->thank_you_message = $data['thank_you_message'];
		$params->show_title = $data['show_title'];
		$data['params'] = json_encode($params);
		$brands = implode(',', $data['brands']);
		$data['brands'] = $brands;
		return parent::save($data);
	}

	/**
	 * Sanitize input data before inserting
	 *
	 * @param $data
	 */
	protected function sanitizeData(&$data) {
		
		$data['title'] = trim($data['title']);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {

			$data = $data;

			$params = json_decode($data['params']);
			$data['page_type'] = $params->page_type;
			$data['show_map'] = $params->show_map;
			$data['map_position'] = $params->map_position;
			$data['show_form'] = $params->show_form;
			$data['contact_notes'] = $params->notes;
			$data['name_field'] = $params->name_field;
			$data['email_field'] = $params->email_field;
			$data['phone_field'] = $params->phone_field;
			$data['message_field'] = $params->message_field;
			$data['captcha_field'] = $params->captcha_field;
			$data['thank_you_message'] = $params->thank_you_message;
			$data['show_title'] = $params->show_title;
			$brands = explode(',', $data['brands']);
			$data['brands'] = $brands;

			return $data;
			
		}
	}
}