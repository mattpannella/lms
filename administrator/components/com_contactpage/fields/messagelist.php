<?php
    defined('JPATH_PLATFORM') or die;

    class JFormFieldMessageList extends JFormField {

        protected $type = 'messagelist';

        protected function getInput() {
            $db = JFactory::getDBO();

            $query = "SELECT * FROM axs_messages WHERE form_id = " . JRequest::getVar('id');
            $db->setQuery($query);
            $messages = $db->loadObjectList();

            ob_start();
            require 'components/com_contactpage/templates/messages.php';
            return ob_get_clean();
        }
    }
