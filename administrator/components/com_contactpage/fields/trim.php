<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 6/27/16
 * Time: 4:00 PM
 */
defined('JPATH_PLATFORM') or die;

class FOFFormFieldTrim extends FOFFormFieldText {
	 protected $type = 'trim';
	
	public function trim_text($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }
  
    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }
  
    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
  
    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }
  
    return $trimmed_text;
	}

	public function getInput()
	{
		$message = $this->form->getValue('message');
		$trimmedMessage = 'asdfasd';//self::trim_text($message,200);
		return $trimmedMessage;
	}

	public function getRepeatable() {
       //return $this->item->get('type');
       $message = $this->item->get('message');
	   $trimmedMessage = self::trim_text($message,200);
	   return $trimmedMessage;
   }
	
}