<?php
/**
 * Add new H5P Content.
 *
 * @package   H5P
 * @author    Joubel <contact@joubel.com>
 * @license   MIT
 * @link      http://joubel.com
 * @copyright 2014 Joubel
 */
?>

<div class="wrap">
 
  <?php H5P_Plugin_Admin::print_messages(); ?>
  
    <form method="post" enctype="multipart/form-data" id="h5p-content-form">

      <div id="post-body-content">
        <div class="h5p-upload">
          <input type="file" name="h5p_file" id="h5p-file"/>
          <?php /*if (current_user_can('disable_h5p_security')):*/ ?>
            <div class="h5p-disable-file-check">
              <label><input type="checkbox" name="h5p_disable_file_check" id="h5p-disable-file-check"/> <?php /*_e('Disable file extension check', $this->plugin_slug);*/ ?></label>
              <div class="h5p-warning"><?php /*_e("Warning! This may have security implications as it allows for uploading php files. That in turn could make it possible for attackers to execute malicious code on your site. Please make sure you know exactly what you're uploading.", $this->plugin_slug);*/ ?></div>
            </div>
          <?php /*endif;*/ ?>
        </div>
        <div class="h5p-create"><div class="h5p-editor"><?php /*esc_html_e('Waiting for javascript...', $this->plugin_slug);*/ ?></div></div>
    
      </div>
      <div class="postbox h5p-sidebar">
        <h2><?php /* esc_html_e('Actions', $this->plugin_slug);*/ ?></h2>
        <div id="minor-publishing" <?php /*if (get_option('h5p_hub_is_enabled', TRUE)) : print 'style="display:none"'; endif;*/ ?>>
          <label><input type="radio" name="action" value="upload"<?php /*if ($upload): print ' checked="checked"'; endif;*/ ?>/><?php /*esc_html_e('Upload', $this->plugin_slug);*/ ?></label>
          <label><input type="radio" name="action" value="create"/><?php /*esc_html_e('Create', $this->plugin_slug);*/ ?></label>
          <input type="hidden" name="library" value="<?php /*print esc_attr($library);*/ ?>"/>
          <input type="hidden" name="parameters" value="<?php /*print $parameters;*/ ?>"/>
          <?php /*wp_nonce_field('h5p_content', 'yes_sir_will_do');*/ ?>
        </div>
        <div id="major-publishing-actions" class="submitbox">
          <?php if ($this->content !== NULL && !is_string($this->content)): ?>
            <a class="submitdelete deletion" href="<?php /*print wp_nonce_url(admin_url('admin.php?page=h5p_new&id=' . $this->content['id']), 'deleting_h5p_content', 'delete');*/ ?>"><?php/* esc_html_e('Delete') */ ?></a>
          <?php endif; ?>
          <input type="submit" name="submit-button" value="<?php /*$this->content === NULL ? esc_html_e('Create', $this->plugin_slug) : esc_html_e('Update') */?>" class="button button-primary button-large"/>
        </div>
      </div>
      
    </form>
  
</div>
