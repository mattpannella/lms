<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactive_content
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * interactive_content Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_interactive_content
 * @since       0.0.9
 */

class InteractiveContentControllerInteractiveContent extends JControllerForm
{

	public function add() {

		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent&view=new', false));

	}

	public function interactive_content_result() {

		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent&view=result', false));

	}

	public function cancel_interactive_content() {

		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent', false));

	}

	public function go_to_results() {

		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent&view=result', false));

	}

	public function go_to_importer_presentations() {

		$this->setRedirect(JRoute::_('index.php?option=com_axs&view=importer_presentations', false));

	}

	public function edit_interactive_content() {

		$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent&view=new&id='.$id, false));
	}

	public function delete_interactive_content_result() {

		$delete_id = $_POST['cid'];
		$db = Jfactory::getDbo();

		$query_results = "DELETE FROM `ic_h5p_results` WHERE id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_results);
		$db->execute();

		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent&view=result', false),'You have successfully deleted Content Type Result');
        $this->redirect();
		exit;

	}

	public function delete_interactive_content() {

		$delete_id = $_POST['cid'];

		$db = Jfactory::getDbo();

		$query_contents = "DELETE FROM `ic_h5p_contents` WHERE id IN (" . implode(",", $db->quote($delete_id)) . ")";

		$db->setQuery($query_contents);
		$db->execute();

		$query_libraries = "DELETE FROM `ic_h5p_contents_libraries` WHERE content_id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_libraries);
		$db->execute();

		$query_tags = "DELETE FROM `ic_h5p_contents_tags` WHERE content_id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_tags);
		$db->execute();

        $query_events = "DELETE FROM `ic_h5p_events` WHERE content_id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_events);
		$db->execute();

		$query_user_data = "DELETE FROM `ic_h5p_contents_user_data` WHERE content_id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_user_data);
		$db->execute();

		$query_results = "DELETE FROM `ic_h5p_results` WHERE content_id IN (" . implode(",", $db->quote($delete_id)) . ")";
		$db->setQuery($query_results);
		$db->execute();

		foreach($delete_id as $id) {
			$expirationData = new stdClass();
			$expirationData->content_type    = 'interactive content';
			$expirationData->content_id      = $id;
			$expirationObject = new AxsContentExpiration($expirationData);
			$expirationObject->delete();
		}


		$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent', false),'You have successfully deleted the Interactive Content');
        $this->redirect();
		exit;

	}

	public function saveSettings($data) {
		$db = JFactory::getDbo();

		$db->updateObject('ic_h5p_contents',$data,'id');
	}

	public function apply_interactive_content() {

		$plugin = H5P_Plugin::get_instance();
	    $plugin_slug = $plugin->get_plugin_slug();
	    $content_admin = new H5PContentAdmin($plugin_slug);
		$content_id = $content_admin->process_new_content('You have successfully created a new Interactive Content');
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
		$input = JFactory::getApplication()->input;

		$expiration_date = $input->get('expiration_date','','STRING');
		$custom_id 		 = $input->get('custom_id','','STRING');
		$parameters 	 = $input->get('parameters','','STRING');
		$user_id         = $input->get('user_id','','STRING');

		if(!$user_id) {
			$user_id = JFactory::getUser()->id;
		}

		$params   = json_decode($parameters);
		$settings = new stdClass();
		$settings->expiration_date = $expiration_date;
		$settings->custom_id = $custom_id;

		$data = new stdClass();
		$data->id = $content_id;
		$data->user_id = $user_id;
		$data->settings = json_encode($settings);
		$this->saveSettings($data);

		$expirationData = new stdClass();
		$info 			= new stdClass();

		$expirationData->content_type    = 'interactive content';
		$expirationData->content_id      = $content_id;
		$expirationData->custom_id       = $custom_id;
		$expirationData->title           = $params->metadata->title;
		$expirationData->expiration_date = $expiration_date;
		$expirationData->data            = "";
		$expirationData->params          = "";
		$expirationObject = new AxsContentExpiration($expirationData);
		if($custom_id || $expiration_date) {
			$expirationObject->store();
		} else {
			if($expirationObject->getRow()) {
				$expirationObject->delete();
			}
		}



		$responseType = $input->get('response','','STRING');
	    if($id=='')
	    {
	        $msg = 'You have successfully created a new Interactive Content';
	    }
	    else
	    {
	        $msg = 'You have successfully updated the Interactive Content';
		}
		/* $this->setRedirect(JRoute::_('index.php?option=com_interactivecontent', false),$msg);
		$this->redirect();
		exit; */
		if($responseType == 'json') {
			$response = new stdClass();
			$response->id = $content_id;
			$response->success = true;
			$key = AxsKeys::getKey('lms');
			$tokenParams = new stdClass();
			$tokenParams->timestamp = time();
			$tokenParams->interactive_id = $content_id;
			$tokenParams->user_id = JFactory::getUser()->id;
			$encryptedToken = base64_encode(AxsEncryption::encrypt($tokenParams, $key));
			$response->encrypted_token = $encryptedToken;
			echo json_encode($response);
			exit;
		} else {
			$this->setRedirect(JRoute::_('index.php?option=com_interactivecontent', false),$msg);
			$this->redirect();
			exit;
		}
	}

	public function ajax_libraries() {

		$plugin = H5P_Plugin::get_instance();

		$editor = new H5peditor(
			$plugin->get_h5p_instance('core'),
			new H5PEditorWordPressStorage(),
			new H5PEditorWordPressAjax()
		);


	    $name = filter_input(INPUT_GET, 'machineName', FILTER_SANITIZE_STRING);
	    $major_version = filter_input(INPUT_GET, 'majorVersion', FILTER_SANITIZE_NUMBER_INT);
	    $minor_version = filter_input(INPUT_GET, 'minorVersion', FILTER_SANITIZE_NUMBER_INT);

	   if ($name) {

	      $editor->ajax->action(H5PEditorEndpoints::SINGLE_LIBRARY, $name,
	        $major_version, $minor_version, $plugin->get_language(), '',
	        $plugin->get_h5p_path(), filter_input(INPUT_GET, 'default-language')
	      );

	      // Log library load

	      new H5P_Event('library', NULL,
	          NULL, NULL,
	          $name, $major_version . '.' . $minor_version);

	    }
	    else {

	      $editor->ajax->action(H5PEditorEndpoints::LIBRARIES);
	    }

	    exit;
  }

   public function ajax_content_type_cache() {

        $token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);

        $plugin = H5P_Plugin::get_instance();
        $editor = new H5peditor(
            $plugin->get_h5p_instance('core'),
            new H5PEditorWordPressStorage(),
            new H5PEditorWordPressAjax()
          );

        $editor->ajax->action(H5PEditorEndpoints::CONTENT_TYPE_CACHE, $token);
        exit;
  }

  public function ajax_library_install() {

        $token = filter_input(INPUT_GET, 'token');
        $name = filter_input(INPUT_GET, 'id');

        $plugin = H5P_Plugin::get_instance();
        $editor = new H5peditor(
            $plugin->get_h5p_instance('core'),
            new H5PEditorWordPressStorage(),
            new H5PEditorWordPressAjax()
          );
        $editor->ajax->action(H5PEditorEndpoints::LIBRARY_INSTALL, $token, $name);
        exit;
  }

  public function ajax_files() {

    $token = filter_input(INPUT_GET, 'token', FILTER_SANITIZE_STRING);
    $contentId = filter_input(INPUT_POST, 'contentId', FILTER_SANITIZE_NUMBER_INT);

    $plugin = H5P_Plugin::get_instance();
    $editor = new H5peditor(
        $plugin->get_h5p_instance('core'),
        new H5PEditorWordPressStorage(),
        new H5PEditorWordPressAjax()
      );
    $editor->ajax->action(H5PEditorEndpoints::FILES, $token, $contentId);
    exit;
  }



}