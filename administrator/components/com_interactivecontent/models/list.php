<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactive_content
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * H5PList Model
 *
 * @since  0.0.1
 */
class InteractiveContentModellist extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */
	

	public function getContents()
	{
		$db    = $this->getDbo();
		
		$query = "SELECT *, C.title C_TITLE, L.title L_TITLE, C.user_id Author, C.created_at C_CREATED_AT, C.updated_at C_UPDATED_AT, C.id C_ID FROM `ic_h5p_contents` C INNER JOIN `ic_h5p_libraries` L ON C.library_id = L.id";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;

	}
	

}