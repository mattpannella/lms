<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * H5Presult Model
 */
class InteractiveContentModelresult extends JModelList
{
	/**
	 * Method to build an SQL query to load the list data.
	 *
	 * @return      string  An SQL query
	 */


	public function getResults()
	{


		$db    = $this->getDbo();

		$query = trim("
			SELECT R.id R_ID, C.title C_TITLE, R.user_id R_USER_ID, U.name U_NAME, R.score R_SCORE, R.max_score R_MAX_SCORE, R.opened R_OPENED, R.finished R_FINISHED
			FROM `ic_h5p_results` R
			INNER JOIN `ic_h5p_contents` C ON R.content_id = C.id
			INNER JOIN `#__users` U ON U.id = R.user_id
			WHERE R.archive_date IS NULL
			ORDER BY R.finished DESC
		");

		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;

	}

}