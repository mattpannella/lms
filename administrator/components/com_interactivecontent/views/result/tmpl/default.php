<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

?>
<form action="" method="post" id="adminForm" name="adminForm">
	<div id="j-main-container">

	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th width="1%"><?php echo JText::_('COM_INTERACTIVECONTENT_NUM'); ?></th>
			<th width="2%">
				<?php echo JHtml::_('grid.checkall'); ?>
			</th>
			<th width="20%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_TITLE') ;?>
			</th>
			<th width="6%">
				Name
			</th>
			<th width="6%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_USER_ID');?>
			</th>
			<th width="2%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_SCORE');?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_MAXIMUM_SCORE') ;?>
			</th>
			<th width="15%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_OPENED') ;?>
			</th>
			<th width="15%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_FINISHED') ;?>
			</th>
			<th width="10%">
				<?php echo JText::_('COM_INTERACTIVECONTENT_RESULT_TIME_SPENT'); ?>
			</th>
		</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="9">
					<?php //echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php if (!empty($this->items)) : ?>
				<?php
				foreach ($this->items as $i => $row) :

				    $time_spent = $row->R_FINISHED - $row->R_OPENED;
				    $seconds = ($time_spent % 60);
                    $get_time_spent = floor($time_spent / 60) . ':' . ($seconds < 10 ? '0' : '') . $seconds;
				?>
					<tr>
						<td><?php echo $i=$i+1 ?></td>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->R_ID); ?>
						</td>
						<td>

							<?php echo $row->C_TITLE; ?>

						</td>
						<td>
							<?php echo $row->U_NAME; ?>

						</td>
						<td>
							<?php echo $row->R_USER_ID; ?>

						</td>
						<td>
							<?php echo $row->R_SCORE; ?>

						</td>
						<td>
							<?php echo $row->R_MAX_SCORE; ?>

						</td>
						<td>
							<?php echo date('F j, Y g:i a',$row->R_OPENED); ?>
						</td>
						<td>
							<?php echo date('F j, Y g:i a',$row->R_FINISHED); ?>

						</td>
						<td>
							<?php echo $get_time_spent; ?>

						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<div>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>