<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of h5p.
 *
 * @since  1.6
 */



class InteractiveContentViewResult extends JViewLegacy
{
	/**
	 * Display the H5P view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		
		$this->items       = $this->get('Results');
		// Set the toolbar
		$this->addToolBar();
		parent::display($tpl);

	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolBar()
	{
		JToolbarHelper::title(JText::_('COM_INTERACTIVECONTENT_RESULT_MANAGER'));
		JToolbarHelper::deleteList('Are you sure to delete ?', 'interactivecontent.delete_interactive_content_result');
	}

	
}
