<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View for com_interactivecontent.
 *
 * @since  1.6
 */
class InteractiveContentViewNew extends JViewLegacy
{


    public $library;
	public $parameters;
	public $settings;


	public function display($tpl = null)
	{

		$id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_NUMBER_INT);
		$plugin = H5P_Plugin::get_instance();
		$core = $plugin->get_h5p_instance('core');
		$plugin_slug = $plugin->get_plugin_slug();
		$content_admin = new H5PContentAdmin($plugin_slug);

		if(!$id)
		{

			$contentExists = FALSE;
	    	$hubIsEnabled = TRUE;


			$this->library = 0;

			$this->parameters = $this->get_input('parameters', '{"params":' . ('{}') . ',"metadata":' . ('{}') . '}');

			$this->author_id = JFactory::getUser()->id;

		    $display_options = $core->getDisplayOptionsForEdit(NULL);

			$content_admin->add_editor_assets();


		}
		else
		{


		/**************************** EDIT PART ************************************/



			$contentExists = TRUE;
			$hubIsEnabled = TRUE;
			$content = $plugin->get_content($id);
			
			$this->library = $this->get_input('library', $contentExists ? H5PCore::libraryToString($content['library']) : 0);

			$this->parameters = $this->get_input('parameters', '{"params":' . ($contentExists ? $core->filterParameters($content) : '{}') . ',"metadata":' . ($contentExists ? json_encode((object)$content['metadata']) : '{}') . '}');

			$this->author_id = $content['user_id'];

		    $safe_text = $this->wp_check_invalid_utf8($this->parameters);
		    $safe_text = $this->_wp_specialchars($safe_text, ENT_QUOTES, false, true);
		    $this->parameters =  $safe_text;


		    $display_options = $core->getDisplayOptionsForEdit($contentExists ? $content['disable'] : NULL);

		    // allows for customization of the editor's view
		    $content_admin->add_editor_assets($contentExists ? $content['id'] : NULL);
			$this->settings = $content['settings'];

			$this->id = $content['id'];

			echo "<h3><b>Note:</b><i> Editing this Interactive Content will remove any submitted user answers.</i></h3>";

		}

		H5P_Plugin_Admin::add_script('jquery', 'h5p-php-library/js/jquery.js');
		H5P_Plugin_Admin::add_script('disable', 'h5p-php-library/js/h5p-display-options.js');
		H5P_Plugin_Admin::add_script('toggle', 'admin/scripts/h5p-toggle.js');

		// Log editor opened

		if ($contentExists) {
		  new H5P_Event('content', 'edit',
		      $content['id'],
		      $content['title'],
		      $content['library']['name'],
		      $content['library']['majorVersion'] . '.' . $content['library']['minorVersion']);
		}
		else {
		  new H5P_Event('content', 'new');
		}

		if ($content['title'] == null) {
			$content['title'] = 'New';
		}
		$this->addToolbar($content['title']);

		return parent::display($tpl);

	}

	public function get_input($field, $default = NULL) {

	    $value = filter_input(INPUT_POST, $field);
	    if ($value === NULL) {
	      if ($default === NULL) {
	        // No default, set error message.
	        H5P_Plugin_Admin::set_error(sprintf('Missing %s ', $field));

	      }
	      return $default;
	    }

	    return $value;
  	}

  	public function has_libraries() {

    $db = Jfactory::getDbo();
    $query = "SELECT id FROM `ic_h5p_libraries` WHERE runnable = 1 LIMIT 1";
    $db->setQuery($query);
    $result = $db->loadResult();

    return $result!== NULL;
  }

  public function wp_check_invalid_utf8( $string, $strip = false ) {

  		$blog_charset = 'UTF-8';

	    $string = (string) $string;

	    if ( 0 === strlen( $string ) ) {
	        return '';
	    }

	    // Store the site charset as a static to avoid multiple calls to get_option()
	    static $is_utf8 = null;
	    if ( ! isset( $is_utf8 ) ) {
	        $is_utf8 = in_array( $blog_charset, array( 'utf8', 'utf-8', 'UTF8', 'UTF-8' ) );
	    }
	    if ( ! $is_utf8 ) {
	        return $string;
	    }

	    // Check for support for utf8 in the installed PCRE library once and store the result in a static
	    static $utf8_pcre = null;
	    if ( ! isset( $utf8_pcre ) ) {
	        // phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
	        $utf8_pcre = @preg_match( '/^./u', 'a' );
	    }
	    // We can't demand utf8 in the PCRE installation, so just return the string in those cases
	    if ( ! $utf8_pcre ) {
	        return $string;
	    }

	    // phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged -- preg_match fails when it encounters invalid UTF8 in $string
	    if ( 1 === @preg_match( '/^./us', $string ) ) {
	        return $string;
	    }

	    // Attempt to strip the bad chars if requested (not recommended)
	    if ( $strip && function_exists( 'iconv' ) ) {
	        return iconv( 'utf-8', 'utf-8', $string );
	    }

	    return '';
	}

	public function _wp_specialchars( $string, $quote_style = ENT_NOQUOTES, $charset = false, $double_encode = false ) {

		$alloptions['blog_charset'] = 'UTF-8';

	    $string = (string) $string;

	    if ( 0 === strlen( $string ) ) {
	        return '';
	    }

	    // Don't bother if there are no specialchars - saves some processing
	    if ( ! preg_match( '/[&<>"\']/', $string ) ) {
	        return $string;
	    }

	    // Account for the previous behaviour of the function when the $quote_style is not an accepted value
	    if ( empty( $quote_style ) ) {
	        $quote_style = ENT_NOQUOTES;
	    } elseif ( ! in_array( $quote_style, array( 0, 2, 3, 'single', 'double' ), true ) ) {
	        $quote_style = ENT_QUOTES;
	    }

	    // Store the site charset as a static to avoid multiple calls to wp_load_alloptions()
	    if ( ! $charset ) {
	        static $_charset = null;
	        if ( ! isset( $_charset ) ) {
	            /* $alloptions = wp_load_alloptions(); */
	            $_charset   = isset( $alloptions['blog_charset'] ) ? $alloptions['blog_charset'] : '';
	        }
	        $charset = $_charset;
	    }

	    if ( in_array( $charset, array( 'utf8', 'utf-8', 'UTF8' ) ) ) {
	        $charset = 'UTF-8';
	    }

	    $_quote_style = $quote_style;

	    if ( $quote_style === 'double' ) {
	        $quote_style  = ENT_COMPAT;
	        $_quote_style = ENT_COMPAT;
	    } elseif ( $quote_style === 'single' ) {
	        $quote_style = ENT_NOQUOTES;
	    }

	    if ( ! $double_encode ) {
	        // Guarantee every &entity; is valid, convert &garbage; into &amp;garbage;
	        // This is required for PHP < 5.4.0 because ENT_HTML401 flag is unavailable.
	        $string = $this->wp_kses_normalize_entities( $string );
	    }

	    $string = htmlspecialchars( $string, $quote_style, $charset, $double_encode );

	    // Back-compat.
	    if ( 'single' === $_quote_style ) {
	        $string = str_replace( "'", '&#039;', $string );
	    }

	    return $string;
	}

	public function wp_kses_normalize_entities( $string ) {

	    // Disarm all entities by converting & to &amp;
	    $string = str_replace( '&', '&amp;', $string );

	    // Change back the allowed entities in our entity whitelist

	    /*
    	    $string = preg_replace_callback( '/&amp;([A-Za-z]{2,8}[0-9]{0,2});/', 'wp_kses_named_entities', $string );
    	    $string = preg_replace_callback( '/&amp;#(0*[0-9]{1,7});/', 'wp_kses_normalize_entities2', $string );
    	    $string = preg_replace_callback( '/&amp;#[Xx](0*[0-9A-Fa-f]{1,6});/', 'wp_kses_normalize_entities3', $string );
	 	*/

	    return $string;
	}





	/**
	 * Add the page title and toolbar.
	 * 
	 * @param string $title - the title of the current interactive content
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar(string $title)
	{
			JToolbarHelper::title(JText::_('COM_INTERACTIVECONTENT_ADD_CONTENT') . (!empty($title) ? ': ' . $title : ''));
			JToolbarHelper::apply('interactivecontent.apply_interactive_content');
			JToolbarHelper::cancel('interactivecontent.cancel_interactive_content');
	}
}
