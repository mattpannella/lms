
<div class="wrap">
 <?php $settings = json_decode($this->settings); ?>
    <form action="index.php?option=com_interactivecontent&task=interactivecontent.apply_interactive_content" method="post" enctype="multipart/form-data"  id="adminForm" name="adminForm">

        <div id="post-body-content">
             <div class="h5p-create"><div class="h5p-editor">Waiting for javascript...</div></div>
        </div>

        <div class="postbox h5p-sidebar">
            <div id="minor-publishing" style="display:none">
                <label><input type="radio" name="action" value="upload"/>Upload</label>
                <label><input type="radio" name="action" value="create"/>Create</label>
                <input type="hidden" name="id" value="<?php echo $this->id ?? ''; ?>"/>
                <input type="hidden" name="library" value="<?php echo $this->library; ?>"/>
                <input type="hidden" name="parameters" value="<?php echo $this->parameters; ?>"/>

            </div>
        </div>
        <br>
        <label style="font-weight: bold;">Custom Id</label>
        <input type="text" name="custom_id" value="<?php echo $settings->custom_id; ?>"/>

        This is an optional Id for expiration reporting.
        <br><br>
        <label style="font-weight: bold;">Expiration Date</label>
        <br>
        <?php echo JHtml::_('calendar', "$settings->expiration_date", 'expiration_date', 'expiration_date', '%Y-%m-%d', null); ?>
        <br>
        This is for expiration reporting only. The content will not expire.
        <br><br>
        <input type="hidden" name="task" value="">
        <?php echo JHtml::_('form.token'); ?>
        <label style="font-weight: bold;">Author</label>
        <?php
            $db = JFactory::getDbo();
            $db->setQuery(trim("
                SELECT id, name
                FROM joom_users;
            "));
            $data = $db->loadObjectList();
        ?>
        <?php echo JHtml::_('select.genericlist', $data, 'user_id', null, 'id', 'name', $this->author_id); ?>

        Sets the author for this interactive content.
        <br><br>
    </form>

  </div>
