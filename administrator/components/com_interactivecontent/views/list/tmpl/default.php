<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_interactivecontent
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

echo AxsLoader::loadKendo();

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>
<style>
    #activity-box {
        overflow-y: auto !important;
    }
</style>
<div id="activity-box"></div>
<form action="index.php?option=com_interactivecontent&view=interactiveContent" method="post" id="adminForm" name="adminForm">
	<div id="j-main-container">
	
	<table id="activity_list" class="table table-striped table-hover">
		<tr>
			<th>
				Select
            </th>
            <th>
				Preview
			</th>
			<th>
				Title
			</th>
			<th>
				Content
			</th>
			<th>
				Author
			</th>
			<th>
			    Modified
			</th>
			<th>
				ID
			</th>
		</tr>
			<?php if (!empty($this->items)) : ?>
				<?php foreach ($this->items as $i => $row) :
					$link = JRoute::_('index.php?option=com_interactivecontent&task=interactivecontent.edit_interactive_content&id=' . $row->C_ID);
				?>
					<tr>
						<td>
							<?php echo JHtml::_('grid.id', $i, $row->C_ID); ?>
                        </td>
                        <td>
                            <?php echo '<span class="k-button k-button-icontext getPreview" data-id="'.$row->C_ID.'"><i class="fa fa-play-circle"></i> Preview</span>'; ?>
                        </td>
						<td><?php echo $row->C_TITLE; ?></td>
						<td><?php echo $row->L_TITLE; ?></td>
						<td><?php $author = JFactory::getUser($row->Author); if($author) { echo $author->name; } ?></td>
						<td>
							<?php echo time_elapsed_string(date('Y/m/d H:i:s',strtotime($row->C_UPDATED_AT))); ?>
						</td>
						<td align="center">
							<?php echo $row->C_ID; ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<div>
	<input type="hidden" name="task" value=""/>
	<input type="hidden" name="boxchecked" value="0"/>
	<?php echo JHtml::_('form.token'); ?>
</form>

<script>
jQuery(document).ready(function () {
    var kendoOptions = {
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        filterable: {
            mode: "row"
        },
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100,200,500,1000,5000,'all'],
            refresh:        true,
            message: {
                empty:      "There are no entries to display"
            }
        },
        columns: [
			{
                field: "Select",
                title: "Select",
                sortable: false,
				filterable: false,
				width: '100px'
            
            },
            {
                field: "Preview",
                title: "Preview",
                sortable: false,
				filterable: false,
				width: '120px'
            
            },                    
            {
                field: "Title",
                title: "Title",
                filterable: {
                    cell: {
						showOperators: false,
        				operator: "contains"
                    }
                },
                template: `<a href="index.php?option=com_interactivecontent&task=interactivecontent.edit_interactive_content&id=#: ID #" name="#: Title #" title="<?php echo JText::_('COM_INTERACTIVECONTENT_EDIT'); ?>">#: Title # </a>`
            },
            
            {
                field: "Content",
                title: "Content",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },                
            {
                field: "Author",
                title: "Author",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                field: "Modified",
                title: "Modified",
                sortable: false,
                filterable: false,            
			},
            {
                field: "ID",
                title: "ID",
                sortable: false,
				filterable: false,
				width: '100px'            
            }
        ]
    }

    var activity_list = jQuery("#activity_list").kendoGrid(kendoOptions);
    var grid = activity_list.data("kendoGrid");

    
    
    function closeActivity() {
        jQuery('#activity-box').html('');
    }

    var activity = jQuery('#activity-box');
    activity.kendoDialog({
        width: "700px",
        height: "600px",
        title: "Preview",
        visible: false,
        closable: false,
        modal: true,
        actions: [
            { 
                text: 'Close',
                action: closeActivity ,
                primary: true
            }
        ]
    });

    jQuery(document).on('click', '.getPreview', function(){
        var ic_id = jQuery(this).data('id');
        jQuery.ajax({
            url: '/index.php?option=com_splms&task=studentactivities.getPreview&format=raw',
            type: 'post',
            data: {
                ic_id: ic_id
            }
        }).done(function(response) {
            jQuery('#activity-box').html(response);
            jQuery('#activity-box').find('iframe').css('height','420');
            activity.data("kendoDialog").open();
        });
    });
        
});
</script>