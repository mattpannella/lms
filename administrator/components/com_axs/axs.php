<?php
	//this is the main AXS File
	
	defined('_JEXEC') or die();

	$user_id = JFactory::getUser()->id;

	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	
	$doc = JFactory::getDocument();

	//$doc->addStylesheet( JURI::root(true) . '/administrator/components/com_splms/assets/css/splms.css' );
	// Load FOF

	include_once JPATH_LIBRARIES.'/fof/include.php';
	
	if(!defined('FOF_INCLUDED')) {
		JError::raiseError ('500', 'FOF is not installed');
		return;
	}

	FOFDispatcher::getTmpInstance('com_axs')->dispatch();