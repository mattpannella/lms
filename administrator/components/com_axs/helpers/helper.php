<?php

defined('_JEXEC') or die;

class AxsSelector {

	public static function getList($type,$filter = null, $custom_conditions = array()) {
		$db = JFactory::getDBO();

        if(!$type) {
			return false;
		}

		switch($type) {
			case 'courses':
				$table = 'joom_splms_courses';
				$field = 'splms_course_id';
				$conditions[] = $db->quoteName('enabled').'=1';
			break;

			case 'media':
				$table = 'axs_gin_media';
				$field = 'id';
			break;

            case 'users':
				$table = 'joom_users';
				$field = 'id';
				$conditions[] = $db->quoteName('block').'=0';
			break;

            case 'events':
				$table = 'joom_eb_events';
				$field = 'id';

				$conditions[] = $db->quoteName('published').'=1';

				if (!empty($custom_conditions)) {
					foreach ($custom_conditions as $custom_condition) {
						$conditions[] = $custom_condition;
					}
				}

				$conditions[] = '(' . $db->quoteName('event_date') . " >= CURDATE() OR " . $db->quoteName('event_end_date') . ">= CURDATE() )";
				$conditions[]= 'access IN (' . implode(',', JAccess::getAuthorisedViewLevels(JFactory::getUser()->id)) . ')';
				$conditions[]= 'registration_access IN (' . implode(',', JAccess::getAuthorisedViewLevels(JFactory::getUser()->id)) . ')';
			break;

            case 'groups':
                $table = 'joom_usergroups';
                $field = 'id';

                if (!empty($custom_conditions)) {
                    foreach ($custom_conditions as $custom_condition) {
						$conditions[] = $custom_condition;
					}
                }

                // Make sure we avoid retrieving the Super Duper Admin group
                $conditions[] = "id != 8";
            break;

            default:
				return false;
			break;
		}

		if($filter) {
			$conditions[] = $db->quoteName($field).' IN ('.$filter.')';
		}

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($table);

		if($conditions) {
			$query->where($conditions);
		}

		if($type == 'events') {
			$query->order('event_date ASC');
		}

        $db->setQuery($query);
		$result = $db->loadObjectList();

        return $result;
	}

	public function getCategories($type,$categories) {

		if(!$type || !$categories) {
			return false;
		}
		$categoryArray = array_map('intval',explode(',',$categories));
		$categoryList  = implode(',', $categoryArray);

		switch($type) {
			case 'courses':
				$table = 'joom_splms_coursescategories';
				$conditions = "splms_coursescategory_id IN ($categoryList)";
			break;
			case 'media':
				$table = 'media_categories';
				$conditions = "id IN ($categoryList)";
			break;
			default:
				return false;
			break;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('title')
			  ->from($table)
			  ->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObjectList();

		if(!$result) {
			return false;
		}

		$titlesArray = array();
		foreach($result as $item) {
			array_push($titlesArray,$item->title);
		}
		return implode(',',$titlesArray);
	}
}
