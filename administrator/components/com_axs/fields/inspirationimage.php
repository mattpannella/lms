<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class JFormFieldinspirationimage extends JFormField 
{
    public function getInput() {
		$image = $this->form->getValue('image');

       	return "<img src='/" . AxsImages::getImagesPath('inspiration') . "/" . $image . "' style='width:300px'>";
    }

    public function getRepeatable() {    	
       	$image = $this->item->get('image');

       	return "<img src='/" . AxsImages::getImagesPath('inspiration') . "/" . $image . "'>";
    }
}
