<?php
defined('JPATH_PLATFORM') or die;

class FOFFormFieldEmailpassword extends FOFFormFieldText {

	public function getInput() {

		$field = '<input type="password" name="'.$this->name.'" id="'.$this->name.'" value="'.$this->value.'" maxlength="99" aria-invalid="false" autocomplete="off">';
		return $field;
		
	}
}
