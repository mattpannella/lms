<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldWelcomeemailfields extends JFormField {

	protected function getInput() {
		/* When this attribute is set to 'true', do not include signin mergefields.
		 * Users created via the registration form do not need to have their login info sent to them upon creation.
		 */
		$registrationWelcomeEmail = $this->getAttribute('registration');

		ob_start();

		?>
			<style>
				.callout {
					font-size: 1.5em;
					font-weight: bold;

					color: green;
				}
			</style>

			<span class="dynamic_fields">
				[name]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<?php if(empty($registrationWelcomeEmail)) : ?>
					[username]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					[password]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<?php endif; ?>
				<span class="callout">*</span>[website_name]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				[website_link]
			</span>
			<br/>
			<strong>
				<p> Use these placeholders in your email messages to pull in dynamic data.<br />
					Fields marked with an asterisk <span class="callout">(*)</span> can be used in both the body and the subject.
				</p>
			</strong>

		<?php

		return ob_get_clean();
	}
}