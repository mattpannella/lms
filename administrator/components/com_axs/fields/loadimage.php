<?php
defined('JPATH_PLATFORM') or die;

class FOFFormFieldLoadimage extends FOFFormFieldText {
	protected $type = 'loadimage';

	public function getInput($elementName = null, $values = null) {
		return '<img style="max-width: 600px;" src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/lms/listing-examples.jpg"/>';
	}
}