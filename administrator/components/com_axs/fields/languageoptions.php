<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldLanguageoptions extends JFormField
{
	public $type = 'languageoptions';
	
	public function getInput() {

        $languages = array(
            array(
                'name' => 'English', 
                'tag' => 'en-GB',
            ),
            array(
                'name' => 'Français', 
                'tag' => 'fr-FR',
            ),
            array(
                'name' => 'Español', 
                'tag' => 'es-ES',
            ),
            array(
                'name' => 'Portugues Brasiliero', 
                'tag' => 'pt-BR',
                'coming_soon' => true
            ),
            array(
                'name' => 'Dansk', 
                'tag' => 'da-DK',
                'coming_soon' => true
            ),
            array(
                'name' => 'Deutsch', 
                'tag' => 'de-DE',
                'coming_soon' => true
            ),
        );

        ob_start();
        ?>
            <div class="container">
                <div class="row">
                    <div class="alert alert-primary">
                        <div class="row">
                            <div class="col-1">
                                <i class="fas fa-exclamation-triangle fa-xl"></i>
                            </div>
                            <div class="col">
                                The languages selected here will be the only options available later as you set up the rest of your LMS. 
                                Make sure to include any and all languages you currently use or will soon be using.
                            </div>
                        </div>
                    </div>
                    <script>
                        const LANGUAGE_OPTIONS_FIELD = {
                            toggleChecked: function(el) {
                                var checkbox = jQuery(el).find('input');
                                if (checkbox.prop('disabled')) {
                                    return;
                                }
                                checkbox.prop("checked", !checkbox.prop("checked"));
                            }
                        };
                    </script>
                    <?php foreach ($languages as $language): ?>
                    <div class="col-4 p-1">
                        <div class="p-3 border rounded <?php echo $language['coming_soon'] ? "bg-light" : "hover-grey" ?>" role="button" onclick="LANGUAGE_OPTIONS_FIELD.toggleChecked(this)">
                            <input type="checkbox" value="<?php echo $language['tag'] ?>" class="me-2" <?php echo $language['coming_soon'] ? "disabled" : "" ?> />
                            <?php echo $language['name'] ?>
                            <?php if ($language['coming_soon']): ?>
                                <span class="badge bg-secondary float-end">Coming Soon!</span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php
        
        $html = ob_get_clean();
		return $html;
	}

}