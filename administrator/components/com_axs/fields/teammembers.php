<?php
defined('JPATH_PLATFORM') or die;

class FOFFormFieldTeammembers extends FOFFormFieldText {

	public function getRepeatable() {
		$id = $this->item->get('id');

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select('user_ids, team_lead, user_groups, member_type')
			  ->from($db->quoteName('axs_teams'))
			  ->where($db->quoteName('id') . '=' . $db->quote($id));

		$db->setQuery($query);

		$teamMembers = $db->loadAssoc();
		$teamMemberCount = 0;

		$individualUsers = array();

		if(strlen($teamMembers['team_lead']) > 0) {

			$teamLeads = explode(',', $teamMembers['team_lead']);

			$individualUsers = array_merge($individualUsers, $teamLeads);
		}

		// Count the individual users or groups of users depending on what the member type is
		if($teamMembers['member_type'] == 'individuals' && strlen($teamMembers['user_ids']) > 0) {

			$userIds = explode(',', $teamMembers['user_ids']);

			$individualUsers = array_merge($individualUsers, $userIds);
		} elseif($teamMembers['member_type'] == 'groups' && strlen($teamMembers['user_groups']) > 0) {

			$userGroups = explode(',', $teamMembers['user_groups']);

			$individualUsers = AxsUser::getUserIdsInGroups($userGroups);
		}

		// Count the total number of individual team members, removing any duplicates.
		$individualUsers = array_unique($individualUsers);
		$teamMemberCount = count($individualUsers);

		return $teamMemberCount;
	}
}
