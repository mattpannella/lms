<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldLinkbutton extends JFormField
{
	public $type = 'linkbutton';
	
	public function getInput() {

        $data = json_decode($this->value);

        ob_start();
        ?>
            <a href="<?php echo $data->uri ?>" class="btn btn-primary"><?php echo $data->title ?></a>
        <?php
        
        $html = ob_get_clean();
		return $html;
	}   

}