<?php



defined('JPATH_PLATFORM') or die;

class JFormFieldAttendance extends JFormField{

    protected $type = 'attendance';
    
    protected function getInput() {
        $meetingId = $this->form->getValue('meetingId');
        $virtualClassParams = new stdClass();
        $virtualClassParams->meetingId = $meetingId;
        ob_start();
        require 'components/com_axs/templates/attendance.php';
        return ob_get_clean();
    }
}
