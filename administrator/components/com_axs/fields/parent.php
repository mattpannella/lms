<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldParent extends JFormField
{
	public $type = 'parent';
	public function getStatic(){ }
	
	

	public function getInput()
	{
		
	}

	
	public function getRepeatable()
	{
		$id = $this->item->get('id');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select ($db->quoteName('parent_id'))
			->from ($db->quoteName('media_categories'))
			->where ($db->quoteName('id').'='.(int)$id);
	

		$db->setQuery($query);

		$result = $db->loadObjectList();
		
		foreach($result as $item) {
			
			if ($item->parent_id == 0) {
			return "Top Level";	
			} else {
				
		$query = $db->getQuery(true);

		$query
			->select ($db->quoteName('title'))
			->from ($db->quoteName('media_categories'))
			->where ($db->quoteName('id').'='.(int)$item->parent_id);
	

		$db->setQuery($query);

		$result = $db->loadObjectList();
		
		foreach($result as $item) {
			
		return $item->title;
			
				}
			}
			
		}
		

		
	}
}