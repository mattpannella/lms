<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldCustomfields extends JFormField {
	public function getInput() {
		ob_start();

		$data = json_decode($this->value);

		$lang = JFactory::getLanguage();
		$lang->load("com_community", JPATH_SITE, "en-GB", true);

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);

		$query
			->select('*')
			->from('#__community_fields')
			->where($db->quoteName('type'). '!=' .$db->quote('group'))
			->order('ordering ASC');
		$db->setQuery($query);
		$results = $db->loadObjectList();

		$isImporter = !empty($this->getAttribute('importer'));

?>

		<div class="controls">
			<input id="<?php echo $this->name; ?>" type="hidden" name="<?php echo $this->name; ?>" value='<?php echo json_encode($data);?>'/>
		</div>

		<div id="custom_field_list">

			<?php
				foreach ($results as $custom) {

					/* If we're using the user importer, skip the first and last name fields - these are
						* required by the importer and will be listed in its static field list
						*/
					if($isImporter && in_array($custom->fieldcode, ['FIELD_GIVENNAME', 'FIELD_FAMILYNAME'])) {

						continue;
					}

					if (!empty($data) && in_array($custom->id, $data)) {
						$checked = "checked";
					} else {
						$checked = "";
					}
			?>
					<div class="form-check">
						<input class="custom_checkbox" type="checkbox" value="<?php echo $custom->id;?>" <?php echo $checked; ?>/>
						<?php echo JText::_($custom->name);?>
					</div>
			<?php
				}
			?>
		</div>

		<script>
			var custom_checkboxes = document.getElementsByClassName("custom_checkbox");
			var storefield = document.getElementById("custom_fields");

			jQuery(".custom_checkbox").click(
				function() {
					var custom_list = [];
					jQuery(custom_checkboxes).each(
						function() {
							if (this.checked) {
								custom_list.push(this.value);
							}
						}
					);

					storefield.value = JSON.stringify(custom_list);
				}
			);
		</script>
	<?php

		return ob_get_clean();
	}
}