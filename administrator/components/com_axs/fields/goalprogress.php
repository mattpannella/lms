<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

class JFormFieldgoalprogress extends JFormField {

    protected $goalId = null;
    protected $goalTotal = null;
    protected $goalList = null;
    protected $userId = null;


    public function getProgressTotal() {

    	$db = JFactory::getDBO();

    	$query = "SELECT * FROM declaration_board WHERE id = '" . $this->goalId . "'";
    	$db->setQuery($query);

    	$results = $db->loadObjectList();

    	if ($results) {
    		$this->goalTotal = $results[0]->goal_amount;
    		$this->userId = $results[0]->user_id;
    	}       	
    }

    public function getProgressList() {
		$db = JFactory::getDBO();

    	$query = "SELECT * FROM declaration_updates WHERE goal_id = '" . $this->goalId . "'";
    	$db->setQuery($query);

    	$results = $db->loadObjectList();

    	if ($results) {    		
    		$this->goalList = $results;
    	}  
    }


    public function getInput() {

    	$jinput = JFactory::getApplication()->input;

		$this->goalId = $jinput->get('id', null, 'INT');       

		self::getProgressTotal();		
		self::getProgressList();
		self::getJavaScript($this->goalId);


		$total = 0;

		foreach ($this->goalList as $item) {
			$total += $item->update_amount;
		}

		$percent = (int)(($total * 100) / $this->goalTotal);
		$goalTotal = $this->goalTotal;

		ob_start();

		?>

			<table class="table table-striped table-bordered table-hover table-responsive">

				<tr>
					<th>Success</th>
					<th>Current Progress</th>
					<th>Goal Progress</th>
				</tr>
				<tr>
					<th id="currentPercent"><?php echo $percent;?>%</th>
					<th id="currentTotal"><?php echo $total; ?></th>
					<th id="goalTotal"><?php echo $goalTotal; ?></th>
				</tr>
			</table>

			<table class="table table-striped table-bordered table-hover table-responsive">
				<tr>
					<th>Add New</th>
					<th>Value</th>
				</tr>
		
				<tr>
					<td><input id="newValue" type="number"></td>
					<td><button class="btn btn-primary" onclick="submitNewValue()">Add</button></td>
				</tr>
			</table>

			<table id="mainList" class="table table-striped table-bordered table-hover table-responsive">

			<tr>
				<th>Delete</th>
				<th>Id</th>
				<th>Update Amount</th>
				<th>Date</th>
			</tr>

			<?php echo self::buildList(); ?>

			</table>

		<?php

		return ob_get_clean();
    }

    public function buildList() {
        
        ob_start();
        
        foreach($this->goalList as $item) {
        	$id = $item->id;
        	$amount = $item->update_amount;
        	$date = date('Y-m-d', strtotime($item->update_date));

        	?>

        	<tr id='cellData<?php echo $id; ?>'>
        		<td>
					<button 
						onclick='deleteProgress(<?php echo $id; ?>)'
						class="btn btn-danger"
					>Delete</button>
        		</td>
        	
        		<td><?php echo $id; ?></td>
        		<td>
        			<input 
        				id='inputField<?php echo $id; ?>' 
        				type='number' 
        				prev='<?php echo $amount; ?>' 
        				value='<?php echo $amount; ?>'
        			/>
        	
					<button 
						style='margin-left: 20px' 
						onclick='updateProgress(<?php echo $id; ?>)'
						class="btn btn-light"
						type="button"
					>Update</button>
        		</td>
        		<td><?php echo $date; ?></td>
        	</tr>

        	<?php
        }

        return ob_get_clean();
    }

    public function getJavaScript($goalId) {

    	?>
    	<script>
			function deleteProgress(id) {
				event.preventDefault();

				var conf = confirm('Delete this entry? This is permanent!');
				var amount = document.getElementById('inputField' + id).value;

				if (conf) {
					jQuery.ajax({
			            type:       'POST',
			            url:        'index.php',
			            data:       {
			                'option':       'com_axs',
			                'task':         'memberboards.deleteGoalProgress',
			                'which':        id
			            },
			            success: function(response) {
			                jQuery('#cellData' + id).empty();
			                updatePercentage(amount, 0);
			            }
			        });
				}
			}

			function updateProgress(id) {

				event.preventDefault();
				
				var inputField = document.getElementById('inputField' + id);
				var value = inputField.value;
				var prev = inputField.getAttribute('prev');

				if (!inputField || !value || !prev) {
					alert("One or more required values are missing");
					return;
				}
				
				jQuery.ajax({
		            type:       'POST',
		            url:        'index.php',
		            data:       {
		                'option':       'com_axs',
		                'task':         'memberboards.updateGoalProgress',
		                'format': 		'raw',
		                'which':        id,
		                'value':		value
		            },
		            success: function(response) {
		            	inputField.value = value;
            			inputField.setAttribute('prev', value);
		            	updatePercentage(prev, value);		            	
		            }
		        });
			}

			function submitNewValue() {
				event.preventDefault();
				
				var value = document.getElementById('newValue').value;
				var userId = document.getElementById('user_id').value;
				var goalId = <?php echo $goalId; ?>;

				if (!userId || !value || !goalId) {
					alert("One or more required values are missing");
					return;
				}
				
				jQuery.ajax({
		            type:       'POST',
		            url:        'index.php',
		            data:       {
		                'option':       'com_axs',
		                'task':         'memberboards.addGoalProgress',
		                'format':		'raw',
		                'value':        value,
		                'userId':		userId,
		                'goalId':		goalId
		            },
		            success: function(response) {

		            	updatePercentage(0, value);

		            	var mainList = document.getElementById('mainList');
		            	var tbody = mainList.getElementsByTagName('tbody')[0];

		            	var newRow = document.createElement('tr');
		            	var deleteCell = document.createElement('td');
		            	var idCell = document.createElement('td');
		            	var updateCell = document.createElement('td');
		            	var dateCell = document.createElement('td');

		            	tbody.appendChild(newRow);

		            	newRow.appendChild(deleteCell);
		            	newRow.appendChild(idCell);
		            	newRow.appendChild(updateCell);
		            	newRow.appendChild(dateCell);

		            	var id = response;

		            	newRow.id = 'cellData' + id;

						var deleteButton = document.createElement('button');
						deleteButton.addClass("btn btn-danger")
		            	jQuery(deleteButton).click(
		            		function() {		            			
		            			event.preventDefault();
		            			deleteProgress(id);
		            		}
		            	);

						deleteButton.appendChild(document.createTextNode('Delete'));
						deleteCell.appendChild(deleteButton);

						idCell.innerHTML = id;

						var inputField = document.createElement('input');
						inputField.id = 'inputField' + id;
						inputField.setAttribute('type', 'number');
						inputField.setAttribute('prev', value);
						inputField.value = value;

						var inputUpdate = document.createElement('button');
						inputUpdate.addClass("btn btn-light");
						jQuery(inputUpdate).css({
							'margin-left': '20px'
						});
						jQuery(inputUpdate).click(
							function() {
								event.preventDefault();								
								updateProgress(id);
							}
						);
						inputUpdate.appendChild(document.createTextNode('Update'));

						updateCell.appendChild(inputField);
						updateCell.appendChild(inputUpdate);

						var now = new Date();

						var day = now.getDate();
						if (day < 10) {
							day = '0' + day;
						}

						var month = now.getMonth() + 1;

						dateCell.innerHTML = now.getFullYear() + '-' + month + '-' + day;
		                
		            }
		        });
			}

			function updatePercentage(prev, value) {

				var total = document.getElementById('currentTotal');
            	var goal = document.getElementById('goalTotal');
            	var currentPercent = document.getElementById('currentPercent');

            	if (!total || !goal || !currentPercent) {
					alert("One or more required values are missing");
					return;
				}

            	var current = parseInt(total.innerHTML);
            	current = (current - parseInt(prev)) + parseInt(value);
            	
            	total.innerHTML = current;
            	var newPercent = parseInt((current * 100) / parseInt(goal.innerHTML));
            	currentPercent.innerHTML = newPercent + '%';
		    }
		</script>
		<?php
    }
}
