<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldZoomlicensedhosts extends FOFFormFieldList {

	protected $type = 'zoomlicensedhosts';

    /**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 */
	protected function getOptions()
	{
		$options = [
            '' => 'Select a User'
        ];

		$adminUsers = AxsZoomMeetings::getZoomUsers(true);

		if(is_iterable($adminUsers)) {

			foreach ($adminUsers as $adminUser) {

				$fullName = $adminUser->first_name . ' ' . $adminUser->last_name;
				$options[] = JHtml::_('select.option', $adminUser->id, $fullName);
			}
		}

		return $options;
    }

    protected function getInput() {
	    $input = parent::getInput();

        if (!AxsZoomApi::validApiKeys()) {
            $input .= '<div class="help-block text-danger">Zoom API Keys invalid, please validate your credentials</div>';
        }

        return $input;
    }
}
