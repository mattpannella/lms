<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

class JFormFieldactivity extends JFormField 
{

    protected $type = 'activity';


    /**
     * @return string
     */

    public function getActivityData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_affiliate_partners_activity'))
            ->where($db->qn('id').'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        
        return $result;


    }

    public function getPartnerData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_affiliate_partners'))
            ->where($db->qn('id').'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;


    }

    public function getPartnerActivityData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_affiliate_partners_activity'))
            ->where($db->qn('partner_id').'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        
        return $result;


    }

    public function getTransactionData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('cctransactions'))
            ->where($db->qn('id').'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;


    }

    public function getSessionData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('#__realtimeanalytics_serverstats'))
            ->where($db->qn('session_id_person').'='.$db->q($id));
        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;


    }

    public function getEventRegistrationData($id) 
    {
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_events_registration'))
            ->where($db->qn('id').'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;


    }

    public function getItemData($type,$id) 
    {
        switch ($type) {
            case '0':
                $table = 'joom_splms_courses';
                $column = 'splms_course_id';
                break;
            case '1':
                $table = 'joom_osmembership_plans';
                $column = 'id';
                break;
            case '2':
                $table = 'axs_events';
                $column = 'id';
                break;   
            default:
                $table = 'joom_splms_courses';
                $column = 'splms_course_id';
                break;
        }
    
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn($table))
            ->where($db->qn($column).'='.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        
        return $result;


    }

    
    public function buildList($id)
    {
        
        $activityList = self::getPartnerActivityData($id);
        $partnerData = self::getPartnerData($id);
        $rows = '';
        
        foreach($activityList as $items)
        {

            $trxnData = '';
            $purchaseData = '';
            $purchaseOutput = '<strong>No Purchase Made</strong>';
            $session = self::getSessionData($items->session_id);
            if($items->trxn_id)
            {    
                $trxnData = self::getTransactionData($items->trxn_id); 
                $purchaseData = self::getItemData($trxnData->type,$trxnData->item_id);
            }
            if ($items->user_id)
            {
                $user = $items->user_id;
            }
            else
            {
                $user = $session->user_id_person;
            }
            $userData = AxsExtra::getUserProfileData($user);
            $name = $userData->first.' '.$userData->last;
        
            if ($name == '') 
            { 
                $name = 'Unregistered Visitor';
            }
            if($userData->photo != '') 
            {
                $photo = $userData->photo;
            } 
            else
            {
                $photo = '/images/user.png';
            } 

            if ($purchaseData->title)
            {
                $purchaseOutput = '
                <strong>'.$purchaseData->title.'</strong><br/>
                Purchase Amount: $'.$trxnData->amount.'<br/>
                Affiliate Earnings: $'.$trxnData->amount * ($partnerData->percent / 100);
            }

            $rows .='
            <tr>
            <td>
            <div class="comment-img" style="margin: 0; float:left; margin-right:20px;">
            <img src="'.$photo.'"/>
            </div>
            <strong>'.$name.'</strong><br/>
            '.AXSExtra::format_date($items->date).'
            </td>
            <td>
            '.$purchaseOutput.'
            </td>
            <td>
            <a href="/administrator/index.php?option=com_jrealtimeanalytics&task=serverstats.showEntity&tmpl=component&details=flow&all=1&identifier='.$items->session_id.'" class="modal-button btn modal btn-primary" rel="{handler: \'iframe\', size: {x: 1100, y: 800}}" id="modalLink1"><span class="icon-screen"> </span> View
             </a>
            </td>
            </tr>';
        }

        return $rows;
    }


    public function getInput()
    {
        $jinput = JFactory::getApplication()->input;
        $partnerId = $jinput->get('id', null, 'INT');
        $d = '';
        foreach($this->form->getFieldset() as $field) {
           $d .= $field->name.' '.$field->value.'<br/>';
           
        }
        //jDump($this->form->getData()->get('company_name'));
       $list = 'test: '. $d.
       '<table class="table table-striped table-bordered table-hover table-responsive">
       <tr>
       <th>Visitor</th>
       <th>Transaction</th>
       <th>View Session</th>
       </tr>';
       $list .= self::buildList($partnerId);
       $list .= '</table>';
       return $list;

       /*<div class="activity-data"><a href="/administrator/index.php?option=com_jrealtimeanalytics&task=serverstats.showEntity&tmpl=component&details=flow&all=1&identifier=00jbi0oomp5de0fg1v7cnvr3q5" 
class="modal-button btn modal" rel="{handler: \'iframe\', size: {x: 1100, y: 800}}" 
id="modalLink1"><span class="icon-screen"> </span> View</a></div>*/

    }

    /**
     * Get the rendering of this field type for a repeatable (grid) display,
     * e.g. in a view listing many item (typically a "browse" task)
     *
     * @return  string  The field HTML
     *
     * @since 2.0
     */
    public function getRepeatable()
    {
       
    }
}