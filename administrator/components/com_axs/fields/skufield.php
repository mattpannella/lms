<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldSku extends JFormField
{
	public $type = 'sku';
	
	public function getInput() {
                $html = '<input type="text" id="sku" value="foobar"></input>';
                $html .= '<button class="btn btn-primary">Generate SKU</button>';
                return $html;
	}

}