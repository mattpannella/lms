<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldgetorder extends JFormField
{

	protected $type = 'getorder';
	
	protected function getInput() {

		if ($this->required) {
			$required = ' required aria-required="true"';
		} else {
			$required = '';
		}

		ob_start();
		?>

		<input 
			class="ordering"
			name="<?php echo $this->name; ?>" 
			id="<?php echo $this->id; ?>"
			table="<?php echo $this->table; ?>"
			value="<?php echo $this->value; ?>"
			<?php echo $required;?>
			type="number"
		></input>
		
		<script>
			function getOrder(parent) {

				jQuery.ajax({
	   	 			type: 	'POST',
					url:    'index.php',
					data:   {
		           		'option': 	'com_axs',
		           		'task': 	'videos.getOrder',
		           		'format':   'raw',
		           		'parent':    parent,
		           		'table':    'axs_gin_media',
		           		'column':    'category',
		           	},	
	           		success: function(response) {
                    	var data = JSON.parse(response);
                    	var order = data.order;
			            jQuery('.ordering').val(order);
			        }
			    });

			};

			var parent = jQuery('.parent_category').val();
			var currentOrder = jQuery('.ordering').val();

			if (currentOrder == 0) {
				getOrder(parent);
			}

			jQuery('.parent_category').change(function() {
				var parent = jQuery('.parent_category').val();
				getOrder(parent);
			});
				    

		</script>

		<?php

		return ob_get_clean();

	}
}