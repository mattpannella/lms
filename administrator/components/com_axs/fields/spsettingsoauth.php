<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSpsettingsoauth extends JFormField{

    protected $type = 'spsettingsoauth';

    public function getInput() {
        $input = JFactory::getApplication()->input;
        $id    = $input->get('id',0,'INT');
        if(!$id) {
            $id = AxsSSO::getNextIDPId();
        }
        $siteUrl = JURI::root();
        $acsUrl = $siteUrl . 'authorizessologin?idp='.$id;
        $spIssuer = rtrim($siteUrl,'/');

        $html  = '<input type="text" name="oauth_callback_url"  value="'.$acsUrl.'" readonly="true"/>';       
          
        $html .= '<style> .control-group  .controls .oauth_idp_field { width: 484px !important; } </style>';

        $html .= '<script>                                  
                    jQuery(document).ready(function() {
                        var preInput = `<div class="input-group-addon btn-primary">
                                        <span>HTTPS://</span>
                                    </div>`;
                        jQuery("#cognito_app_domain").before(preInput);
                        jQuery("#cognito_app_domain").parent().addClass("input-group"); 
                        jQuery("#cognito_app_domain").css("width","400px").addClass("form-control");
                        var endpoints = {
                            cognito : {
                                authorize : "https://<cognito-app-domain>/oauth2/authorize",
                                token : "https://<cognito-app-domain>/oauth2/token",
                                userInfo : "https://<cognito-app-domain>/oauth2/userInfo",
                                scopes : "openid profile"
                            },
                            google : {
                                authorize : "https://accounts.google.com/o/oauth2/auth",
                                token : "https://www.googleapis.com/oauth2/v3/token",
                                userInfo : "https://www.googleapis.com/oauth2/v3/userinfo",
                                scopes : "openid email profile"
                            },
                            facebook : {
                                authorize : "https://www.facebook.com/dialog/oauth",
                                token : "https://graph.facebook.com/v2.8/oauth/access_token",
                                userInfo : "https://graph.facebook.com/me/?fields=id,name,email,age_range,first_name,gender,last_name,link&access_token=",
                                scopes: "public_profile email"
                            },
                            windows : {
                                authorize : "https://login.live.com/oauth20_authorize.srf",
                                token : "https://login.live.com/oauth20_token.srf",
                                userInfo : "https://apis.live.net/v5.0/me",
                                scopes : "wl.emails,wl.basic,wl.photos,wl.contacts_photos"
                            },
                            linkedin : {
                                authorize : "https://www.linkedin.com/oauth/v2/authorization",
                                token : "https://www.linkedin.com/oauth/v2/accessToken",
                                userInfo : "https://api.linkedin.com/v2/me",
                                scopes: "r_basicprofile"
                            },
                            instagram : {
                                authorize : "https://api.instagram.com/oauth/authorize/",
                                token : "https://api.instagram.com/oauth/access_token",
                                userInfo : "https://api.instagram.com/v1/users/self/?access_token=",
                                scopes: ""
                            },
                            other : {
                                authorize : "",
                                token : "",
                                userInfo : "",
                                scopes: ""
                            }
                        }

                        function updateOauthConfiguration() {
                            let oauth_URL = jQuery(".oauth_URL").val();
                            let oauth_IDP = jQuery(".oauth_IDP").val();
                            if(oauth_IDP && oauth_IDP) {
                                if(oauth_URL) {
                                    endpoints.cognito.authorize = "https://"+oauth_URL+"/oauth2/authorize";
                                    endpoints.cognito.token = "https://"+oauth_URL+"/oauth2/token";
                                    endpoints.cognito.userInfo = "https://"+oauth_URL+"/oauth2/userInfo";
                                }
                                jQuery(".oauth_authorize_endpoint").val(endpoints[oauth_IDP].authorize);
                                jQuery(".oauth_token_endpoint").val(endpoints[oauth_IDP].token);
                                jQuery(".oauth_userinfo_endpoint").val(endpoints[oauth_IDP].userInfo);
                                jQuery(".oauth_scopes").val(endpoints[oauth_IDP].scopes);
                            }
                        }

                        var unique_id = jQuery("#sp_issuer").val();
                        function updateSAMLConfiguration() {
                            let site = "'.$spIssuer.'";
                            let saml_IDP = jQuery(".saml_IDP").val();
                            if(saml_IDP == "adfs" || saml_IDP == "azure_active_directory") {
                                jQuery("#sp_issuer").val(site).trigger("change");
                            } else {
                                jQuery("#sp_issuer").val(unique_id).trigger("change");
                            }
                        }

                        jQuery(".oauth_URL, .oauth_IDP").change(updateOauthConfiguration);
                        jQuery(".oauth_URL, .oauth_IDP").keyup(updateOauthConfiguration);
                        jQuery(".saml_IDP").change(updateSAMLConfiguration);
                    });
                  </script>';

        return $html;
            
    }
}
