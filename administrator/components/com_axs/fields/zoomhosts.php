<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldZoomhosts extends FOFFormFieldList {

	protected $type = 'zoomhosts';

    /**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 */
	protected function getOptions()
	{
		$options = [
            '' => 'Select a User'
        ];

        $adminUsers = AxsZoomMeetings::getZoomUsers();

		if(is_iterable($adminUsers)) {

			foreach ($adminUsers as $adminUser) {
				$fullName = $adminUser->first_name . ' ' . $adminUser->last_name;
				$options[] = JHtml::_('select.option', $adminUser->id, $fullName);
			}
		}

		return $options;
    }
}