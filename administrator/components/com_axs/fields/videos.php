<?php


defined('JPATH_PLATFORM') or die;

class JFormFieldVideos extends JFormField
{

	protected $type = 'videos';

	private function getAllVideos() {
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('axs_gin_media')
			  ->where("media_type = 'video'")
			  ->order('title ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}
	
	protected function getInput() {
		$required  = $this->required ? ' required aria-required="true"' : '';		
		$videoList = self::getAllVideos();
		$output = '<select class="video_list"  name="' . $this->name . '" id="' . $this->id . '" '. $required .'>';
		$allowedTypes = ['youtube','vimeo','mp4'];
		foreach($videoList as $option) {
			$videoParams = json_decode($option->params);
			if(in_array($videoParams->video_type, $allowedTypes)) {
				if($this->value == $option->id) {
					$selected = "selected";
				} else {
					$selected = "";
				}
				$output .= '<option value="'.$option->id.'" '.$selected.'>'.$option->title.'</option>';
			}			
		}
		$output .= '</select>';		

		return $output;

	}
}