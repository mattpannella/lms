<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldMaxparticipants extends FOFFormFieldInteger {

    protected $type = 'maxparticipants';

    public function getInput() {
        $selectorClass = $this->element['class'];
        $selectorName  = $this->element['name'];
        $value = $this->value;
        if($value == -1) {
            $value = '';
        }

        $maxParticipants = AxsClientData::getClientMaxVirtualClassroomParticipants();
        $description = "Please enter a number that is less than or equal to $maxParticipants.";

        ob_start();
        ?>
            <input type='text' value='<?php echo $value; ?>' class='<?php echo $selectorClass; ?>' name='<?php echo $selectorName; ?>'>
        <?php
        $inputField = ob_get_clean();

        ob_start();
        ?>
            <span class='input-guidance help-block'><?php echo $description; ?></span>
            <script>
                var inputGuidance = jQuery('span.input-guidance');
                inputGuidance.hide();

                let vcParticipantLimit = jQuery("input[name='<?php echo $selectorName; ?>']").val();
                let maxParticipants = <?php echo $maxParticipants; ?>;

                if(vcParticipantLimit > maxParticipants) {
                    inputGuidance.show();
                }

                jQuery("input[name='<?php echo $selectorName; ?>']").change(function(event) {
                    var target = this;
                    var maxParticipants = <?php echo $maxParticipants; ?>;

                    if(target.value > maxParticipants) {
                        event.preventDefault();
                        
                        $(this).val(maxParticipants);
                        inputGuidance.show();
                    } else {
                        inputGuidance.hide();
                    }
                });
            </script>
        <?php

        $inputRegulator = ob_get_clean();

        $formField = $inputField;

        if($maxParticipants > 0) {
            $formField .= $inputRegulator;
        }

        return $formField;
    }
}