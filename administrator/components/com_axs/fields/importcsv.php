<?php

class JFormFieldImportcsv extends JFormField 
{
    public function getInput() {
    	$id = JRequest::getVar('id');
    	return self::makeButton($id);
    }

    public function getRepeatable() {    	
    	$id = $this->item->id;
    	return self::makeButton($id);
    }

    static $modalRendered = false;

    private function makeButton($id) {

    	$url   = $this->getAttribute("url");
        $class = $this->getAttribute("class");
    	$text  = $this->getAttribute("text");

        $modal = '';
        if (!static::$modalRendered) {
            ob_start(); ?>
                <div class="mb-4">
                    <div class="text-secondary fs-6">Template Selected: <span id="selectedImportTemplate"></span></div>
                </div>
                <div id="importProgressContainer" class="mb-4" style="display:none">
                    <div id="importLog" style="width:100%;height:500px;overflow-y:scroll;font-family:monospace" class="mb-2"></div>
                    <div id="importProgress" class="progress">
                        <div class="progress-bar" role="progressbar" style="width:0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-12">
                        <button id="selectFileButton" type="button" class="btn btn-primary input-file-trigger">Select a file...</button>
                        <input class="input-file" id="csv_file" name="csv_file" type="file" style="display:none" accept=".csv"/>
                        <input id="template_id" name="template_id" type="hidden" />
                        <span class="file-return ms-1 text-secondary fs-6"></span>
                        <div id="startImportButton" class="btn btn-primary float-end uploadUsers uploadBtn mb-2" style="display:none">
                            <i class="icon-users"></i> Start Import
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-6">
                        <div id="saveImportLogButton" class="btn btn-secondary float-start mb-2" style="display:none"> <i class="fas fa-save"></i> Save Log</div>
                    </div>
                    <div class="col-6">
                        <div id="cancelImportButton" class="btn btn-danger float-end mb-2" style="display:none">
                            Cancel
                        </div>
                    </div>
                </div>
            <?
            $modalBody = ob_get_clean();
            
            $modal = JHtml::_(
                'bootstrap.renderModal',
                'importCSVmodal',
                array(
                    'title' => 'User Importer',
                    'size' => 'lg'
                ),
                $modalBody
            );
            static::$modalRendered = true;
        }
        
    	
    	return $modal . '<a class="importCSV btn btn-success '.$class.'" data-id="'.$id.'"><i class="icon-upload"></i> ' . $text . '</a>';
    }
}