<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldSelectlist extends JFormField
{
	public $type = 'selectlist';

    function isSelected($value) {
        if (is_array($this->value)) {
            return in_array($value, $this->value);
        } else {
            return $this->value == $value; 
        }
    }
	
	public function getInput() {

        $key = $this->element['key'];
        $title = $this->element['title'];
        $table = $this->element['table'];
        $where = $this->element['where'];
        $multiple = $this->element['multiple'];
        $create_new_link = $this->element['create_new_link'];

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($key . ', ' . $title);
        $query->from($table);
        if (!empty($where)) {
            $query->where($where);
        }
        $db->setQuery($query);
        $listItems = $db->loadObjectList();

        JHtml::_('script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.full.js');
        JHtml::_('stylesheet', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
	    JHtml::_('stylesheet', 'https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css');

        

        ob_start(); 
        ?>
            <style>
                .select2-container--bootstrap-5 textarea{
                    box-shadow: none;
                }
            </style>
            <select id="<?php echo md5(serialize($this->name)) ?>-select2" name=<?php echo $this->name ?> <?php echo $multiple == '1' ? 'multiple="multiple"' : '' ?>>
                <?php foreach($listItems as $listItem): ?>
                    <option value="<?php echo $listItem->{$key} ?>" <?php echo $this->isSelected($listItem->{$key}) ? 'selected' : '' ?>><?php echo $listItem->{$title} ?></option>
                <?php endforeach; ?>
            </select>
            <script>
                $(document).ready(function() {
                    $('.remove-badge').click((ev) => {
                        console.log('remove clicked');
                    });
                    $("#<?php echo md5(serialize($this->name)) ?>-select2").select2({
                        theme: "bootstrap-5",
                        closeOnSelect: <?php echo ($multiple == '1') ? 'false' : 'true' ?>,
                        // Remove button/badge might not make it into first iteration of this field....
                        // templateResult: function (data, container) {
                        //     // Add custom attributes to the <option> tag for the selected option
                        //     var removeBadge = document.createElement("span");
                        //     removeBadge.setAttribute('class', 'remove-badge badge bg-secondary float-end');
                        //     removeBadge.innerText = 'Remove';
                        //     removeBadge.onclick = function(ev) {
                        //         ev.preventDefault();
                        //         console.log('a remove badge was clicked');
                        //     };

                        //     var text = document.createElement("span");
                        //     text.innerText = data.text;
                        //     text.id = container.id;
                        //     console.log(container.dataset);
                        //     text.setAttribute('data-select2-id', container.dataset.select2Id);
                        //     container.id = '';
                        //     container.dataset.select2Id = '';
                        //     $(container).append(text);
                        //     $(container).append(removeBadge);
                        //     return container;
                        // }
                    })
                    .on('select2:open', () => {
                        <?php if (!empty($create_new_link)): ?>
                        $(".select2-results:not(:has(a))").append('<a href="<?php echo $create_new_link ?>" style="padding: 6px;height: 20px;display: inline-table;">Create New</a>');
                        <?php endif; ?>
                    });
                });
            </script>
        <?php
        $html = ob_get_clean();
		return $html;
	}

}