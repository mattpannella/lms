<?php

defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL | E_NOTICE);
//ini_set('display_errors', 1);

class JFormFieldTime extends JFormField {

	protected function getInput() {

		$name = $this->getAttribute('name');

		$loadtime = $this->form->getValue($name);

		if ($loadtime) {
			$loadtime = json_decode($loadtime);
			$starthour = $loadtime->hour;
			$startmin = $loadtime->minute;
			$startampm = $loadtime->ampm;
		}

		if ($startampm == "") {
			$startampm = "pm";
		}
		
		ob_start();

		$id_unique = strtotime('now') . rand(1000,9999);

		?>
			<style>
				.skinny {
					width: 80px;
				}
			</style>

			<input id="time_<?php echo $id_unique;?>" type="hidden" name="<?php echo $name;?>" value='<?php echo json_encode($loadtime);?>'/>

			<?php $timeselect = "timeselect_" . $id_unique;?>

			<div class="row">
				<div class="col-sm-1">
					<select class="<?php echo $timeselect;?> skinny" setting="hour">
						<?php						
							for ($i = 0; $i <= 12; $i++) {
								if ($i == $starthour) {
									$selected = "selected";
								} else {
									$selected = "";
								}
						?>
								<option value="<?php echo $i;?>" <?php echo $selected; ?>><?php echo $i; ?></option>
						<?php
							}
						?>
					</select>
				</div>
				<div class="col-sm-1">
					<select class="<?php echo $timeselect;?> skinny" setting="minute">
						<option value="00" <?php echo $selected;?>>00</option>
						<?php
							for ($i = 10; $i <= 59; $i = $i + 10) {
								if ($i == $startmin) {
									$selected = "selected";
								} else {
									$selected = "";
								}

								if ($i < 10) {
									$min = "0" . $i;
								} else {
									$min = $i;
								}
						?>
								<option value="<?php echo $i;?>" <?php echo $selected;?>><?php echo $min; ?></option>
						<?php
							}
						?>
					</select>
				</div>
				<div class="col-sm-1">
					<select class="<?php echo $timeselect;?> skinny" setting="ampm">
						<option value="am" <?php if ($startampm == "am") { echo "selected"; } ?>>AM</option>
						<option value="pm" <?php if ($startampm == "pm") { echo "selected"; } ?>>PM</option>
					</select>
				</div>
			</div>

			<script>
				jQuery(document).ready(
					function() {
						var selectors = document.getElementsByClassName("<?php echo $timeselect;?>");
						jQuery(".<?php echo $timeselect;?>").change(
							function() {

								let time = {

								};

								jQuery(selectors).each(
									function() {
										let setting = this.getAttribute("setting");
										let value = this.value;
										
										time[setting] = value;


									}
								);

								jQuery("#time_<?php echo $id_unique;?>").val(JSON.stringify(time));
							}
						);
					}
				);				
			</script>
		<?php

		return ob_get_clean();
	}

	public function getRepeatable() {

		$id = $this->item->get('id');

		ob_start();

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName("axs_notifications_emails"))
			->where($db->quoteName("id") . '=' . $db->quote($id))
			->limit(1);
			
		$db->setQuery($query);

		$result = $db->loadObject();

		echo $result->send_time;

		return ob_get_clean();
	}

}
