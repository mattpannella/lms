<?php



defined('JPATH_PLATFORM') or die;

class JFormFieldRecordings extends JFormField{

    protected $type = 'recordings';
    
    protected function getInput() {

        $meetingId = $this->form->getValue('meetingId');
        $input = JFactory::getApplication()->input;
        $mid = $input->get('id');
        if(!$meetingId || !$mid) {
            return;
        }
        ob_start();
        require 'components/com_axs/templates/recordings.php';
        return ob_get_clean();
    }
}
