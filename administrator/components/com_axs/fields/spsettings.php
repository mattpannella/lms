<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSpsettings extends JFormField{

    protected $type = 'spsettings';

    public function getInput() {
        $input = JFactory::getApplication()->input;
        $id    = $input->get('id',0,'INT');
        if(!$id) {
            $id = AxsSSO::getNextIDPId();
        }
        $siteUrl = JURI::root();
        $acsUrl = $siteUrl . 'authorizessologin?idp='.$id;

        $html = '<table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Attribute</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>';      

        $html  .=   '<tr>
                        <td class="sp_attributes">SP-EntityID / Issuer</td>
                        <td class="sp_values sp_issuer"></td>
                    </tr>';
        $html  .=   '<tr>
                        <td class="sp_attributes">ACS (AssertionConsumerService) URL / Single Sign-On URL (SSO)</td>
                        <td class="sp_values sp_acs">'.$acsUrl.'</td>
                    </tr>';
        $html  .=   '<tr>
                        <td class="sp_attributes">NameID Format</td>
                        <td class="sp_values sp_name_id">urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress</td>
                    </tr>';
        
                      
        $html .= '      </tbody>
                  </table>';       

        $html .= '<script>
                    function updateIssuer() {
                        let issuer = jQuery(".sp_issuer_field").val();
                        jQuery(".sp_issuer").text(issuer);
                    }
                    jQuery(".sp_issuer_field").change(updateIssuer);
                    jQuery(".sp_issuer_field").keyup(updateIssuer);
                    updateIssuer();
                  </script>';

        return $html;
            
    }
}
