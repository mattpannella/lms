<?php



defined('JPATH_PLATFORM') or die;

class JFormFieldModule extends JFormField{

    protected $type = 'module';

    public function getInput() {

        $app 	= JFactory::getApplication();
        $document = JFactory::getDocument();

        $option = $app->input->get('option');
        $view 	= $app->input->get('view');

        $html = "";
        if ($option == 'com_menus' && $view == 'item') {
            JHtml::_("script", "administrator/components/com_axs/assets/js/popupmodule.js", array("version" => "auto"));
       	} else {
            JHtml::_("script", "administrator/components/com_axs/assets/js/moduleshelper.js", array("version" => "auto"));

            $field_id = 'modulesButton-' . uniqid();

            $document->addScriptDeclaration('
                initializeModuleButton("'.$field_id.'");
            ');

            $html .= "
                <button
                    id='$field_id'
                    class='btn btn-primary' 
                    type='button'
                    data-modules-field-button='true'
                >
                    <span class='icon-cog'></span> Edit Module
                </button>
            ";
            
            $html .= JHtml::_(
                'bootstrap.renderModal',
                'modulesModal',
                array(
                    'title' => 'Edit Module',
                    'footer' => '<button type="button" class="btn btn-secondary closeModal" data-bs-dismiss="modal">Close</button>',
                    'size' => 'xl',
                    'sharedModalId' => 'modulesModal'
                )
            );
            
        }
       	return $html;
            
    }

    public function getRepeatable() {
      
    }
}
?>