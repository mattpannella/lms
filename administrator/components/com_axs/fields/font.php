<?php



defined('JPATH_PLATFORM') or die;

class JFormFieldFont extends JFormField{

    protected $type = 'font';
    

    public function isSelected($font){
        $selected = '';

        if($this->value == $font) { 
            $selected = 'selected'; 
        } 

        return $selected;

    }

    public function getInput()
    {
       
       
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('axs_fonts'))
            ->where($db->quoteName('enabled').'='.(int)1);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        $required  = $this->required ? ' required aria-required="true"' : '';

        $html = '<select class="fonts '.$this->element['class'].'" name="' . $this->name . '" id="' . $this->id . '" '.$required.'>';
        $options = '
            <option value="">--Select--</option>
            <option value="Arial"   '. self::isSelected('Arial') .'>Arial</option>
            <option value="Verdana" '. self::isSelected('Verdana') .'>Verdana</option>
            <option value="Georgia" '. self::isSelected('Georgia') .'>Georgia</option>
            <option value="Impact"  '. self::isSelected('Impact') .'>Impact</option>';

        foreach($result as $row) {
            
            $options .= '<option value="'.$row->title.'" '. self::isSelected($row->title) .'>'.$row->title.'</option>';
        }

        $html .= $options;
        $html .= '</select>';

        return $html;
            
    }

    public function getRepeatable()
    {
      
    }
}
