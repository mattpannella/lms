<?php

defined('FOF_INCLUDED') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class FOFFormFieldMultilingualselect extends JFormField
{

	public function getInput()
	{
		$table = $this->getAttribute('table');

		$db = JFactory::getDBO();

		$query = "SELECT * FROM joom_languages WHERE published = 1";
		$db->setQuery($query);
		$languages = $db->loadObjectList();
		//var_dump($languages);

		$query = "SELECT * FROM " . $table;
		$db->setQuery($query);
		$options = $db->loadObjectList();


		$loaded_values = json_decode($this->form->getValue($this->name));

		ob_start();
			$data = new stdClass();

			foreach ($languages as $language) {
				$code = $language->lang_code;
				$title = $language->title;
				$class = $this->name . '_multilanguage_select';

				$value = $loaded_values->$code;
				
			?>
				<div class="input-group mb-3">
					<div class="input-group-text"><?php echo $title; ?></div>

					<select class="<?php echo $class;?>" code="<?php echo $code;?>">
						<?php
							if ($value) {
								$data->$code = $value;
							} else {
								$data->$code = $options[0]->id;
							}

							foreach ($options as $option) {
								?>
									<option 
										value='<?php echo $option->id;?>'
										<?php
											if ($option->id == $value) {
												echo ' selected ';
											}
										?>
									>
										<?php echo $option->name;?>
									</option>
								<?php
							}
						?>
					</select>
				</div>
			<?php
			}

			?>			
				<input id="<?php echo $this->name;?>_input" value='<?php echo json_encode($data);?>' name="<?php echo $this->name;?>" style="display:none;">

				<script>

					var input_field = document.getElementById("<?php echo $this->name . '_input';?>");

					jQuery(".<?php echo $class;?>").change(function() {						
						var options = document.getElementsByClassName("<?php echo $class;?>");
						var list = {};

						var input_field = document.getElementById("<?php echo $this->name . '_input';?>");
						
						for (var i = 0; i < options.length; i++) {
								
								var code = options[i].getAttribute("code");
								var index = options[i].options[options[i].selectedIndex].value;

								list[code] = index;							
						}

						input_field.setAttribute("value", JSON.stringify(list));
					});
				</script>
			<?php


			
		return ob_get_clean();
	}

	
	public function getRepeatable()
	{
		return "REPEAT";
	}
}