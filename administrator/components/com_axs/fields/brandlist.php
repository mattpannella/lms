<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldBrandlist extends JFormField
{
	public $type = 'brandlist';
	
	public function getInput() {

        $key = $this->element['key'];
        $item_name = $this->element['item_name'];
        $id = JFactory::getApplication()->input->get('id', null, 'INTEGER');

        // Get all the brands associated with this item
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_brands');
        $query->where("JSON_EXTRACT(data, '$key') = " . $db->quote($id));
        $db->setQuery($query);
        $brands = $db->loadObjectList();

        ob_start(); 
        ?>
            <?php 
            foreach($brands as $brand):
            ?>
                <a href="/administrator/index.php?option=com_axs&view=brand&id=<?php echo $brand->id ?>">
                    <button type="button" class="btn btn-primary mb-3"><?php echo $brand->site_title ?></button>
                </a>
                <br />
            <?php endforeach; ?>
            <?php if (empty($brands)): ?>
                <p>This <?php echo $item_name ?> used by 0 Brands</p>
            <?php endif; ?>
        <?php
        
        $html = ob_get_clean();
		return $html;
	}

}