<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldVirtualclassroomstart  extends FOFFormFieldText {

	protected $type = 'virtualclassroomstart';

	public function getRepeatable() {

		$user = JFactory::getUser();
		$userId = $user->id;
		$usergroups = JAccess::getGroupsByUser($userId);
		$isAdmin = FALSE;

		if ( (in_array(32, $usergroups)) || (in_array(8, $usergroups)) ) {
			$isAdmin = TRUE;
		}

		$url = AxsLMS::getUrl();
		$key = AxsKeys::getKey('lms');

		$VC_Server = AxsClientData::getClientVirtualMeetingServer();
		$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
		$virtualMeeting = new AxsVirtualClassroom($setupParams);

		$isHost = $virtualMeeting->checkHostAccess($userId,$this->item->meetingId);

		$encodedLink = new stdClass();

		if($isAdmin || $isHost) {
			$encodedLink->userPassword = $this->item->moderatorPW;
		} else {
			$encodedLink->userPassword = $this->item->attendeePW;
		}

		$encodedLink->meetingId = $this->item->meetingId;
		$encodedLink->userId = $userId;
		$encodedLink->userName = JFactory::getUser($userId)->name;
		$encryptedParams = base64_encode(AxsEncryption::encrypt($encodedLink, $key));

		$link = $url . '/join-virtual-classroom?vcid='.$encryptedParams;
		$link_text = 'Start Meeting';
		$html = '<a href="'.$link.'" target="_blank" class="btn btn-success"><span class="lizicon-airplay"></span> '.$link_text.'</a>';

		return $html;
	}
}
