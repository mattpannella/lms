<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldStepsbtn extends JFormField
{
	public $type = 'stepsbtn';
	
	public function getInput() {
		// id of the tab content DOM element
		$id = $this->id;
		// button display text
		$name = $this->name;

		if (trim($name) == 'Go Back To Step 1') {
			$icon = "fa-arrow-left";
		} else {
			$icon = "fa-arrow-right";
		}
		return "
			<div id='stepsBtn mt-3'>
				<a id='step$id' class='steps_btn btn btn-light'>
					<i class='fas $icon'></i> $name
				</a>
			</div>
			<script>
				var stepButton = document.querySelector('#step$id');
				stepButton.onclick = ev => {
					var triggerEl = document.querySelector('#formNav #$id-tab');
					console.log(triggerEl);
					var tab = new bootstrap.Tab(triggerEl);
					tab.show();
				}
			</script>
		";
	}

}