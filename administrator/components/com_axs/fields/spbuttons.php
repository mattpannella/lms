<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSpbuttons extends JFormField{

    protected $type = 'spbuttons';

    public function getInput() {
        $input = JFactory::getApplication()->input;
        $id    = $input->get('id',0,'INT');
        if(!$id) {
            $id = AxsSSO::getNextIDPId();
        }

        $html  = '<input type="hidden" name="link_id" value="'.$id.'"/>';        

        $html .= '<button onclick="Joomla.submitbutton(\'apply\');" class="btn btn-sm button-apply btn-success">
                <span class="fas fa-save" aria-hidden="true"></span>
                Update</button>';
                
        if($input->get('id',0,'INT')) {
            $html .= '<button type="button" id="test-config" data-type="test_config" title="You can only test your configuration after saving your Identity Provider Settings." class="btn btn-primary btn-sm" onclick="showTestWindow(this)" style="margin-left:10px">
                <span class="icon-cog icon-white" aria-hidden="true"></span> Test Configuration</button>';
            if(JFactory::getUser()->id == 601) {
                $html .= '<button type="button" data-type="get_xml" title="You can only get the XML after saving your Identity Provider Settings." class="btn btn-primary btn-sm" onclick="showTestWindow(this)" style="margin-left:10px">
                    <span class="icon-file icon-white" aria-hidden="true"></span> Get SAML XML Response</button>';
            }
        }
        

        $html .= '<script>
                    function showTestWindow(e) {
                        var type = e.getAttribute("data-type");
                        var testconfigurl = window.location.href;
                        testconfigurl = testconfigurl.substr(0,testconfigurl.indexOf("administrator")) + "index.php?option=com_axs&view=sso_config&id='.$id.'&q="+type;
                        var myWindow = window.open(testconfigurl, "TEST SAML IDP", "scrollbars=1 width=800, height=600");
                    }
                  </script>';

        return $html;
            
    }
}
