<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSecuredfileuploader extends JFormField {

    protected $type = 'securedfileuploader';

    public function getInput() {
        $user = JFactory::getUser();
        $key = AxsKeys::getKey('lms');
        $encryptedUserId = base64_encode(AxsEncryption::encrypt($user->id, $key));
        $clientId = AxsClients::getClientId();
        ob_start();
        ?>
            <link rel="stylesheet" type="text/css" href="/administrator/components/com_axs/assets/css/secure_file_locker.css">
            <div id="file-uploader-message"></div>            
            <iframe style="width: 100%; height: 150px;" frameborder="0" scrolling="no" src="/administrator/components/com_axs/templates/securedfileuploader.php?uid=<?php echo $encryptedUserId; ?>"></iframe>
            <?php 
                echo AxsLoader::loadKendo();
                $directory = '/mnt/shared/'.$clientId.'/files/secure_file_locker/';
                $files = glob($directory.'*.*');
                $fileList = [];
                foreach($files as $file) { 
                    $fileName = str_replace($directory,'',$file);
                    array_push($fileList,$fileName);
                }
                $lockerFiles = AxsFiles::getLockerFiles();
            ?>

            <table id="fileLocker" class="table table-bordered table-striped table-responsive">
                                        
                <tr>
                    <th>File</th>
                    <th>User</th>
                    <th>Date</th>
                    <th>Delete</th>
                </tr>
                <?php 
                    foreach($lockerFiles as $item) {
                        if(in_array($item->file,$fileList)) {
                            $userName = JFactory::getUser($item->user_id)->name;
                ?>
                            <tr>
                                <td><?php echo $item->file; ?></td>
                                <td><?php echo $userName; ?></td>
                                <td><?php echo $item->date; ?></td>
                                <td><span class="fa fa-trash delete_button" data-file="<?php echo $item->file; ?>"></span></td>
                            </tr>            
                <?php 
                        }
                    } 
                ?>    
            </table>
            <script src="/administrator/components/com_axs/assets/js/secure_file_locker.js?v=2"></script>          
        <?php
        
        $html = ob_get_clean();
        return $html;            
    }
}