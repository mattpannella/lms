<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldSelector extends JFormField {

    protected $type = 'selector';

    public function getInput() {
        $selectorType         = $this->element['selection'];
        $selectorName         = $this->element['name'];
        $selectorValue        = $this->value;
        $selectorValueEncoded = '';

        if($selectorValue) {
            $selectorValueEncoded = '&val='.base64_encode($selectorValue);
        }

        if($this->element['id']) {
            $id = $this->element['id'];
        } else {
            $id = rand(0,2000);
        }

        ob_start();
            include "components/com_axs/templates/selector.php";
        return ob_get_clean();
    }
}