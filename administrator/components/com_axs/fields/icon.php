<?php

defined('JPATH_PLATFORM') or die;

 //error_reporting(E_ALL);
 //ini_set('display_errors', 1);

$document = JFactory::getDocument();
$document->addStyleSheet("components/com_axs/assets/css/icon_select.css", array("version" => "auto"));
$document->addScriptVersion("components/com_axs/assets/js/icon_select.js");


class JFormFieldicon extends JFormField {    



    public function getInput() {

        $which = $this->name;
                
        ob_start();
        ?>

          <input id='<?php echo $which; ?>' name='<?php echo $which ?>' type='hidden' class='icon_input_field' value='<?php echo $this->value;?>'>
         	<div class='icon_preview <?php if (isset($icon)) { echo $icon; } ?>'></div>
         	  <div class='btn btn-primary select_icon_button'>Change</div>
            <div class='btn btn-primary empty_icon_button'>Clear</div>
         	<br>

        <?php
        $view = ob_get_clean();

       	$all_icons = scandir("../templates/axs/icons/icons");
       	
       	$icon_count = count($all_icons);

        $id = null;

       	$view .= "<div id='icon_area_" . $id . "' class='icon_area rounded'>";
        /*
          When doing a scandir, the first two elemends are '.' and '..' for the directories.
          So... skip those.
        */
       	for ($i = 2; $i < $icon_count; $i++) {
          /*
       			Icons are listed in the folder as:
       			iconname.svg
       			Get all of the name but the last 4 characters ('.svg')
       		*/

          $all_icons[$i] = substr($all_icons[$i], 0, -4);
       	}

        sort($all_icons, SORT_STRING);

        for ($i = 2; $i < $icon_count; $i++) {
          $view .= "<div class='icon_box' icon_name='" . $all_icons[$i] . "' value=" . $id . ">";
          $view .= "<span class='icon_selection lizicon-" . $all_icons[$i] . "'></span>";
          $view .= "</div>";
        }

       	$view .= "</div>";

       	return $view;
    }

    public function getRepeatable() {

        $icon = $this->item->get($this->name);
        $color = $this->item->get('color');

        $view = "<span class='" . $icon . "' style='font-size: 32px; color:" . $color . ";'></span>";
       	
       	return $view;
    }
}