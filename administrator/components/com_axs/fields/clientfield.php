<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldClientfield extends JFormField {

    protected $type = 'clientfield';

    public function getInput() {
        $input   = JFactory::getApplication()->input;
        $client_idParam = $input->get('client_id', 0, 'INT');
        $checklist_idParam = $input->get('id', 0, 'INT');
        $field_name = $this->name;
        if ($client_idParam) {
            $client_id = $client_idParam;
        } else {
            $client_id = $this->value;
        }
        
        $client = AxsClientData::getClientById($client_id);
        
        $html  = '<h2>'.$client->client_name.'</h2>';
        $html .= '<input type="hidden" value="'.$client->client_name.'" name="client_name"/>';
        $html .= '<input type="hidden" value="'.$client_id.'" name="'.$field_name.'" id="'.$field_name.'"/>';

        return $html;
    }
}