<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldChecksso extends JFormField
{
	public $type = 'checksso';

	public function getInput() {
        // check if there is at least one SSO App - if not then hide the SSO Deep Link fields
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from('joom_sso_saml_config');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $html = "<script>jQuery('#checksso-row').hide();</script>";
        if(!$results) {
            $html .= "<script>jQuery('#enable_sso_deeplinks_button-row,#sso_app-row,#sso_deeplinks_button_permissions-row').hide();</script>";
        }
        return $html;
	}

}