<?php
defined('JPATH_PLATFORM') or die;

class FOFFormFieldTeamlead extends FOFFormFieldText {

	public function getInput() {


		$initial = $this->form->getValue($this->name);

		$id = "select_" . strtotime('now') . rand(1000,9999);

		ob_start();
		?>

			<div class="controls">
				<input id="<?php echo $id . '_input'; ?>" type="hidden" name="team_lead"/ value="<?php echo $initial;?>">
			</div>

			<select id="<?php echo $id;?>">
				<option selected="selected">-- Select User --</option>
			</select>

			<script>
				var select_id = "<?php echo $id;?>";
				var initial = "<?php echo $initial;?>";
				var oldValue = "";

				jQuery(document).ready(
					function() {
						setInterval(
							function() {
								var user_ids = document.getElementById("user_ids");
								if (user_ids.value != oldValue) {
									oldValue = user_ids.value;

									changeList(oldValue);
								}
							}, 250
						);

						jQuery("#" + select_id).on("change",
							function() {
								jQuery("#" + select_id + "_input").val(this.value);
							}
						);
					}
				);

				function changeList(ids) {
					var list = ids.split(",");
					var selectlist = document.getElementById(select_id);

					var namefield = document.getElementById("team_members").getElementsByClassName('label-info');
					var currentlySelected = null;
					
					if (initial) {
						currentlySelected = initial;
						initial = null;
					} else {
						currentlySelected = selectlist.options[selectlist.selectedIndex].value;
					}

					selectlist.options.length = 1;	//Truncate all of the options except "None"
							
					var selectedIndex = 0;			
					for (let i = 0; i < namefield.length; i++) {
						let id = list[i];
						let name = namefield[i].innerText;

						if (id == currentlySelected) {
							selectedIndex = i + 1;  //Add 1 since option 0 is the { -- Select User -- } value
						}

						let newOption = document.createElement('option');
						newOption.value = id;
						newOption.appendChild(document.createTextNode(name));

						selectlist.appendChild(newOption);
					}

					selectlist.selectedIndex = selectedIndex;
				}
			</script>



		<?php

		return ob_get_clean();
	}

	public function getRepeatable() {
		return "repeatable";
	}
}
