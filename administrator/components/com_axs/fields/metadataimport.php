<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldMetadataimport extends JFormField{

    protected $type = 'metadataimport';

    public function getInput() {
        $html = '<input style="display:inline" name="'.$this->name.'" type="text" value="'.$this->value.'" class="'.$this->class.'"/>';

        $html .= '<a id="xml_import" class="btn btn-success" style="margin-left:20px;"><i class="fa fa-download"></i> Import</a>';       

        $html .= '<script>
                    function uploadMetaData() {
                        var file = jQuery(\'input[name="'.$this->name.'"]\').val();
                        if(file) {
                            jQuery.ajax({
                                type: "POST",
                                data: {
                                    file: file
                                },
                                url: "index.php?option=com_axs&task=sso_configs.importMetadata&format=raw"
                            }).done(function(result) {
                                var fields = JSON.parse(result);
                                console.log(fields)
                                jQuery("#idp_entity_id").val(fields.idp_entity_id);
                                jQuery("#single_signon_service_url").val(fields.single_signon_service_url);
                                jQuery("#certificate").val(fields.certificate);
                            })
                           
                        }
                    }

                    jQuery("#xml_import").click(uploadMetaData);
                  </script>';

        return $html;
            
    }
}
