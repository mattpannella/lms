<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldPresentationfileuploader extends JFormField {

    protected $type = 'presentationfileuploader';

    public function getInput() {
        $user = JFactory::getUser();
        $key = AxsKeys::getKey('lms');
        $encryptedUserId = base64_encode(AxsEncryption::encrypt($user->id, $key));
        $clientId = AxsClients::getClientId();
        ob_start();
        ?>
            <link rel="stylesheet" type="text/css" href="/administrator/components/com_axs/assets/css/importer_presentations.css">
            <div class="import_instructions"><b>Instructions:</b> For best results upload PowerPoint or PDF files that have a 1920px by 1080px resolution.<em> Also note that PowerPoint animations, videos and audios will not import since all slides are saved as images.</em></div>
            <div id="file-uploader-message"></div>
            <iframe style="width: 100%; height: 200px;" frameborder="0" scrolling="no" src="/administrator/components/com_axs/templates/importer_presentations.php?uid=<?php echo $encryptedUserId; ?>"></iframe>
            <div class="presentation_box">
                <div class="import_message"><span class="fa fa-spinner fa-spin"></span> Making The Magic Happen</div>
                <div class="container presentation_wrapper" style="display: none;"></div>
                <script src="/components/com_interactivecontent/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>
            </div>
            <script src="/administrator/components/com_axs/assets/js/importer_presentations.js?v=9"></script>
        <?php
        $html = ob_get_clean();
        return $html;
    }
}