<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldVirtualclassroomlink  extends FOFFormFieldText {

	protected $type = 'virtualclassroomlink';

	public function getRepeatable() {

		$link = AxsLMS::getUrl() . '/join-virtual-classroom?guest_vcid=' . $this->item->meetingId;
		$html = $this->buildLinkHtml($link, $this->item->meetingId);

		return $html;
	}

	private function buildLinkHtml($link, $meetingId) {

		$html = '<div class="input-group" style="margin-bottom:10px; max-width: 500px;">
					<span class="input-group-text bg-primary text-white">
						<span><i class="fa fa-link"></i> Guest Link</span>
					</span>
					<input type="text" class="form-control" value="'.$link.'" readonly="true" id="link_'.$meetingId.'">
					<button type="button" class="btn btn-success copy-vc-link" data-id="link_'.$meetingId.'">
						<span><i class="fa fa-copy"></i> Copy</span>
					</button>
				</div>';

		return $html;
	}
}
