<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldEmailfields extends JFormField {

	protected function getInput() {
		ob_start();

		?>
			<style>
				.dynamic_fields {
					word-spacing: 15px;
				}	
			</style>
			<span class="dynamic_fields">
				[firstname] [lastname] [username] [password]
			</span>
			<br/>
			<b>Use these placeholders in your email messages to pull in dynamic data.</b>

		<?php

		return ob_get_clean();
	}
}