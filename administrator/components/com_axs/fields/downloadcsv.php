<?php

class JFormFieldDownloadcsv extends JFormField 
{
    public function getInput() {
    	$id = JRequest::getVar('id');
    	return self::makeButton($id);
    }

    public function getRepeatable() {    	
    	$id = $this->item->id;
    	return self::makeButton($id);
    }

    private function makeButton($id) {

    	$url   = $this->getAttribute("url");
        $class = $this->getAttribute("class");
    	$text  = $this->getAttribute("text");
    	
    	return '<a class="downloadCSV btn btn-light '.$class.'" data-id="'.$id.'" href="/index.php?option=com_axs&task=importer.downloadCSV&template_id='.$id.'"&format=raw" target="_blank"><i class="icon-download"></i> ' . $text . '</a>';
    }
}