<?php
defined('_JEXEC') or die;

class FOFFormFieldApikeys extends FOFFormFieldText {

	protected $type = 'apikeys';

	public function getInput() {
		
	}

	public function getRepeatable() {
		$key_id = $this->item->get('id');
		$public_key = $this->item->get('public_key');
		$encrypted_keys = $this->item->get('encrypted_keys');
		$key = AxsKeys::getKey('api'); 
		$secret_key = AxsEncryption::decrypt($encrypted_keys, $key);

		ob_start(); ?>
		<div class="input-group input-group-sm mb-2">
			<span class="input-group-text bg-primary text-white">
				<i class="fa fa-link me-1"></i>Public Key
			</span>
			<input
				id="<?php echo 'public_key_' . $key_id ?>"
				value="<?php echo $public_key ?>"
				aria-label="Public Key" 
				type="text" 
				readonly="true"
			>
			<button 
				id="copy-key"
				data-key="<?php echo 'public_key_' . $key_id ?>"
				class="btn btn-secondary"
				type="button"
			>
				<i class="fa fa-copy"></i> Copy
			</div>
		</div>
		<div class="input-group input-group-sm">
			<span class="input-group-text bg-primary text-white">
				<i class="fa fa-lock me-2"></i> Secret Key
			</span>
			<input 
				id="<?php echo 'secret_key' . $key_id ?>" 
				value="<?php echo $secret_key ?>" 
				type="password" 
				readonly="true"
			>
			<button 
				id="view-key" 
				data-key="<?php echo 'secret_key' . $key_id ?>"
				class="btn btn-light"
				data-bs-toggle="tooltip"
				title="Show Key"
				type="button" 
			>
				<i class="fa fa-eye"></i>
			</button>
			<button 
				id="copy-secret-key"
				data-key="<?php echo 'secret_key' . $key_id ?>"
				class="btn btn-secondary"
				type="button" 
			>
				<i class="fa fa-copy"></i> Copy
			</button>
		</div>
		<?php 
		return ob_get_clean();
	}
}
