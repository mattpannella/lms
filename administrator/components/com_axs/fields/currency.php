<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldCurrency extends JFormField {

    protected $type = 'currency';

    public function getInput() {      
        $selectorClass = $this->element['class'];
        $selectorName  = $this->element['name'];
        $selectorValue = $this->value;
        $currencyParams = new stdClass();
        $currencyParams->top = true;
        $allOptions = AxsPayment::getCurrency();
        $topOptions = AxsPayment::getCurrency($currencyParams);
        $html  = '<select name="'.$selectorName.'" class="'.$selectorClass.'" style="width: 300px;">';
        $html .= '<optgroup label="Common Currencies">';
        foreach($topOptions as $topOption) {
            $selected = '';
            if( $selectorValue == $topOption->id) {
                $selected = "selected";
            }
            $html .= '<option value="'.$topOption->id.'" '.$selected.'>'.$topOption->country.' '.$topOption->currency.' ( '.$topOption->symbol.' ) '.$topOption->code.'</option>';
        }
        $html .= '</optgroup>';

        $html .= '<optgroup label="All Currencies">';
        foreach($allOptions as $option) {
            $selected = '';
            if($selectorValue == $option->id && !$selected) {
                $selected = "selected";
            }
            $html .= '<option value="'.$option->id.'" '.$selected.'>'.$option->country.' '.$option->currency.' ( '.$option->symbol.' ) '.$option->code.'</option>';
        }
        $html .= '</optgroup>';
        $html .= '</select>';
        return $html;    
    }
}