<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldColorscheme extends JFormField
{
	public $type = 'colorscheme';
	private $brand;

	public function getColorSchemes() {
		$colors = new stdClass();

		$colors->logged_out = [
			[
				'label' 	 => 'Header Background Color',
				'field_name' => 'logged_out_header_background_color',
				'value'		 => $this->brand->homepage->colors->background
			],
			[
				'label' 	 => 'Header Navigation Text Color',
				'field_name' => 'logged_out_header_text_color',
				'value'		 => $this->brand->homepage->colors->header_text
			],
			[
				'label' 	 => 'Footer Background Color',
				'field_name' => 'logged_out_footer_background_color',
				'value'		 => $this->brand->homepage->footer->background_color
			],
			[
				'label' 	 => 'Footer Text Color',
				'field_name' => 'logged_out_footer_text_color',
				'value'		 => $this->brand->homepage->footer->text_color
			],
			[
				'label' 	 => 'Social Media Bar Color',
				'field_name' => 'logged_out_social_media_bar_color',
				'value'		 => $this->brand->homepage->header->socialmedia_color
			],
			[
				'label' 	 => 'Social Media Bar Text Color',
				'field_name' => 'logged_out_social_media_bar_text_color',
				'value'		 => $this->brand->homepage->header->socialmedia_text_color
			],
			[
				'label' 	 => 'Social Media Bar Text Hover Color',
				'field_name' => 'logged_out_social_media_bar_text_hover_color',
				'value'		 => $this->brand->homepage->header->socialmedia_hover_color
			]
		];

		$colors->logged_in = [
			[
				'label' 	 => 'Header Background Color',
				'field_name' => 'logged_in_header_background_color',
				'value'		 => $this->brand->dashboard->user_bar->color
			],
			[
				'label' 	 => 'Header Text Color',
				'field_name' => 'logged_in_header_text_color',
				'value'		 => $this->brand->dashboard->user_bar->text_color
			],
			[
				'label' 	 => 'Left Side Navigation Background Color',
				'field_name' => 'logged_in_navigation_background_color',
				'value'		 => $this->brand->dashboard->navigation_bar->color
			],
			[
				'label' 	 => 'Left Side Navigation Text Color',
				'field_name' => 'logged_in_navigation_text_color',
				'value'		 => $this->brand->dashboard->navigation_bar->text_color
			],
			[
				'label' 	 => 'Left Side Navigation Sub Text Hover Color',
				'field_name' => 'logged_in_navigation_text_hover_color',
				'value'		 => $this->brand->dashboard->navigation_bar->hover_color
			],
			[
				'label' 	 => 'Module Title Background Color',
				'field_name' => 'logged_in_module_title_background_color',
				'value'		 => $this->brand->dashboard->modules->title_bar_color
			],
			[
				'label' 	 => 'Module Title Text Color',
				'field_name' => 'logged_in_module_title_text_color',
				'value'		 => $this->brand->dashboard->modules->title_bar_text_color
			]
		];

		$colors->presets = [
			[
				'label' 	 => 'Header Background Color',
				'field_name' => 'home_header_background_color',
				'value'		 => '#ccc'
			],
			[
				'label' 	 => 'Header Navigation Color',
				'field_name' => 'home_header_background_color',
				'value'		 => '#555555'
			]
		];

		return $colors;
	}

	public function getColorFields($colors) {
		$count = count($colors);
		$html = '';

		for($i = 0; $i < $count; $i++) {
			$html .= '<span class="zen-color-label">'.$colors[$i]["label"].'</span>
						<input
							type="text"
							name="'.$colors[$i]["field_name"].'"
							id="'.$colors[$i]["field_name"].'"
							value="'.$colors[$i]["value"].'"
							placeholder="#rrggbb"
							class="minicolors hex minicolors-input zen-color-field"
							data-position="default"
							data-control="hue"
							data-format="hex"
							size="7"
							aria-invalid="false"/>';
			$id = $colors[$i]["field_name"];
			JFactory::getDocument()->addScriptDeclaration("allColorFieldIds.push('$id')");
		}

		return $html;
	}

	public function getInput() {
		$brandID = $this->form->getValue('brand');
		if($brandID) {
			$this->brand = AxsBrands::getBrandById($brandID);
		} else {
			$this->brand = AxsBrands::getBrand();
		}

		$html = '';

		$values = null;
		$colors = self::getColorSchemes();

		$html  .= '<table class="color-table">
					<th class="color-table-heading">LOGGED OUT VIEW
					</th>
					<th class="color-table-heading">LOGGED IN VIEW
					</th>
					<tr>';
		$html .= '	  <td class="color-table-first-cell">';
		$html .= self::getColorFields($colors->logged_out);
		$html .= '	  </td>';
		$html .= '	  <td class="color-table-second-cell">';
		$html .= self::getColorFields($colors->logged_in);
		$html .= '	  </td>';
		$html .= '	</tr>
				  </table>';
		$html .= JHTML::_('bootstrap.renderModal', 'designKeyModal', array('size' => 'lg'), '<img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/lms/color-scheme.jpg" />');

		return $html;
	}

}