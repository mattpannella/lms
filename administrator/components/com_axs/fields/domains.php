<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldDomains extends JFormField
{
	public $type = 'domains';
	
	public function getInput() {

        // Get all the domains associated with this brand
        $brand_id = JFactory::getApplication()->input->get('id');
        $domains = AxsBrands::getBrandDomainsById($brand_id);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_domains');
        $query->where("JSON_EXTRACT(params, '$.brand') = " . $db->quote($brand_id) . " AND JSON_EXTRACT(params, '$.brand') != ''");
        $query->orWhere("FIND_IN_SET(domain, " . $db->quote($domains) . ")");

        $db->setQuery($query);
        $domains = $db->loadObjectList();

        if (empty($domains)) {
            // Create a dummy domain so we can still add new stuff.
            $dummyDomain = new stdClass();
            $dummyDomain->id = '-1';
            $dummyDomain->is_dummy = true;
            $domains = array();
            $domains []= $dummyDomain;
        }

        $awsConfig = dbCreds::getAwsConfig();

        ob_start(); 
        ?>
            <script>
                function updateDomains(target) {
                    var idx = target.dataset.idx;
                    var key = target.dataset.key;
                    var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                    currentValue = JSON.parse(currentValue);
                    if (key.includes('params.')) {
                        var params = JSON.parse(currentValue[idx]['params']);
                        params[key.split('.')[1]] = target.value;
                        currentValue[idx]['params'] = params;
                    } else {
                        currentValue[idx][key] = target.value;
                    }
                    
                    $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));
                }
                let domainAdded = false;
                $(document).on('click', '#addDomain', (ev) => {
                    // Update the json value for this field, it will be sent to the viewmodel
                    // and if any new domains are in the value they will be added to the axs_domains table
                    var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                    currentValue = JSON.parse(currentValue) || [];
                    if (currentValue[0].is_dummy && !domainAdded) {
                        var $container = $('.domains-container');
                        var $section = $container.children().first();
                        $section.find('input[name*="domain_name"]').val('');
                        $section.find('input[name*="domain_name"]').prop('disabled', false);
                        $section.find('.transferDomainButton').remove();
                        $section.find('#defaultBadge').remove();
                        $section.show();
                        $container.append('<hr class="my-4" data-idx="'+ 0 +'" />');
                        // delete currentValue[0].is_dummy;
                        delete currentValue[0].id;
                        currentValue[0].params = {language: "", brand: "<?php echo $brand_id ?>"};
                        currentValue[0].domain = '';
                        $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));
                        domainAdded = true;
                        return;
                    }
                    currentValue.push({
                        params: {language: "", brand: "<?php echo $brand_id ?>"},
                        domain: '',
                    });
                    $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));

                    // Clone the first row in the domains container
                    var $container = $('.domains-container');
                    var $sectionClone = $container.children('.domain-section').last().clone();
                    // Clear the domain input and the domain column in the table
                    $sectionClone.find('input[name*="domain_name"]').val('');
                    $sectionClone.find('input[name*="domain_name"]').prop('disabled', false);
                    $sectionClone.find('table').find('td').first().text('');
                    // Remove the delete domain and transfer domain buttons,
                    // they won't be usable because the domain will not be created until the form is submitted
                    $sectionClone.find('.transferDomainButton').remove();
                    $sectionClone.find('#defaultBadge').remove();
                    // Update the data-idx attribute so any updated values can go to the right element in the array
                    var newIndex = null;
                    $sectionClone.find('[data-idx]').each((idx, el) => {
                        newIndex = parseInt(el.dataset.idx) + 1;
                        el.dataset.idx = parseInt(el.dataset.idx) + 1;
                    });
                    console.log($sectionClone);
                    $sectionClone.attr('data-idx', newIndex);
                    $sectionClone.find('.deleteDomainButton').attr('onclick', 'showDeleteDomainModal(' + newIndex + ')');
                    $sectionClone.find('.translateDomainButton').attr('onclick', 'showTransferDomainModal(' + newIndex + ')');
                    $sectionClone.show();
                    $container.append('<hr class="my-4" data-idx="'+ newIndex +'" />');
                    $container.append($sectionClone);
                });
                function showDeleteDomainModal(domainIdx) {
                    var deleteDomainModalEl = document.getElementById('deleteDomainModal');
                    deleteDomainModalEl.dataset.domainIdx = domainIdx;
                    var deleteDomainModal = new bootstrap.Modal(deleteDomainModalEl);
                    deleteDomainModal.show();
                }
                $(document).on('click', '.deleteDomain', (ev) => {
                    // Grab the domain id and send it to the the domains.remove controller endpoint
                    var domainIdx = document.getElementById('deleteDomainModal').dataset.domainIdx;
                    var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                    currentValue = JSON.parse(currentValue) || [];
                    var domain = currentValue[domainIdx];
                    // If there isn't a domain id
                    if (!domain.id) {
                        // The domain being deleted hasn't been saved,
                        if (currentValue.length > 1) {
                            // Remove the domain from the value field
                            currentValue = currentValue.filter((el, idx) => idx != domainIdx);
                            $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));

                            var $container = $('.domains-container');
                            $container.find('.domain-section[data-idx="' + domainIdx + '"]').remove();
                            $container.find('.my-4[data-idx="' + domainIdx + '"]').remove();
                        } else {
                            // This is the last domain, so just hide the last domain and 
                            // empty the input value
                            var $container = $('.domains-container');
                            $container.find('.domain-section[data-idx="' + domainIdx + '"]').hide();
                            $container.find('.my-4[data-idx="' + domainIdx + '"]').hide();
                            $('input[name="<?php echo $this->name ?>_data"]').val('[{"is_dummy":true, "id": "-1"}]');
                        }
                        var deleteDomainModalEl = document.getElementById('deleteDomainModal');
                        var deleteDomainModal = bootstrap.Modal.getInstance(deleteDomainModalEl);
                        deleteDomainModal.hide();
                    } else {
                        jQuery.ajax({
                            type: "POST",
                            url: "index.php",
                            data:       {
                                'option':       'com_domains',
                                'task':         'domains.remove',
                                'format': 		'raw',
                                'cid':		domain.id
                            },
                            success: (response) => {
                                response = JSON.parse(response);
                                if (response.msg == 'Domains were deleted.') {
                                    var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                                    currentValue = JSON.parse(currentValue) || [];
                                    currentValue = currentValue.filter((el) => el.id != domain.id);
                                    $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));
                                    Joomla.submitbutton('apply');
                                } else {
                                    window.alert('Something went wrong, domain not deleted');
                                }
                            }
                        });
                    }
                });
                function showTransferDomainModal(domainIdx) {
                    var transferDomainModalEl = document.getElementById('transferDomainModal');
                    transferDomainModalEl.dataset.domainIdx = domainIdx;
                    var transferDomainModal = new bootstrap.Modal(transferDomainModalEl);
                    transferDomainModal.show();
                }

                $(document).on('click', '.transferDomain', (ev) => {
                    var brandId = $('#transferDomainModal').find('select').find(':selected').val();
                    // Grab the domain id and send it to the the domains.remove controller endpoint
                    var domainIdx = document.getElementById('transferDomainModal').dataset.domainIdx;
                    var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                    currentValue = JSON.parse(currentValue) || [];
                    var domain = currentValue[domainIdx];

                    jQuery.ajax({
                        type: "POST",
                        url: "index.php",
                        data:       {
                            'option':       'com_domains',
                            'task':         'domains.transfer',
                            'format': 		'raw',
                            'domainId':		domain.id,
                            'brandId':      brandId
                        },
                        success: function(response) {
                            response = JSON.parse(response);
                            if (response.success == true) {
                                var currentValue = $('input[name="<?php echo $this->name ?>_data"]').val();
                                currentValue = JSON.parse(currentValue) || [];
                                currentValue = currentValue.filter((el) => el.id != domain.id);
                                $('input[name="<?php echo $this->name ?>_data"]').val(JSON.stringify(currentValue));
                                Joomla.submitbutton('apply');
                            } else {
                                window.alert('Something went wrong, domain not transferred');
                            }
                        }
                    });
                });
                jQuery(document).on('click',"td[name=copy_pre]", function(){
                    var jQuerythis	= jQuery(this);
                    var text = jQuerythis.text();
                    var idx = jQuerythis.data('idx');
                    console.log(idx);
                    console.log(jQuerythis);
                    console.log(document.getElementById("success-alert-" + idx));
                    jQuery(document.getElementById("success-alert-" + idx)).append(text);
                    var id = jQuerythis.attr('id');
                    var el = document.getElementById(id);
                    var range = document.createRange();
                    range.selectNodeContents(el);
                    var sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
                    document.execCommand('copy');
                    jQuery(document.getElementById("success-alert-" + idx)).show();
                    setTimeout(function() { jQuery(document.getElementById("success-alert")).hide(); }, 1000);
                    return false;
                });
            </script>
            <input type="hidden" name="<?php echo $this->name ?>_data" value='<?php echo json_encode($domains) ?>' />
            <?php 
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query->select('*');
                $query->from('axs_brands');
                $query->where('id != ' . $db->quote($brand_id));
                $db->setQuery($query);
                $brands = $db->loadObjectList();

                ob_start();
                ?>
                <span>Please select a brand to transfer to</span>
                <select name="transfer_brand">
                    <?php foreach ($brands as $brand): ?>
                        <option value="<?php echo $brand->id ?>"><?php echo $brand->site_title ?></option>
                    <?php endforeach; ?>
                </select>
                <?php 
                $modalBody = ob_get_clean();
                echo JHtml::_('bootstrap.renderModal', 
                    'transferDomainModal',
                    array(
                        'size' => 'sm',
                        'title' => 'Transfer Domain',
                        'footer' => '<btn class="btn btn-primary transferDomain">Transfer</btn>'                 
                    ),
                    $modalBody
                );

                echo JHtml::_('bootstrap.renderModal', 
                    'deleteDomainModal',
                    array(
                        'size' => 'sm',
                        'title' => 'Delete Domain',
                        'footer' => '<btn class="btn btn-danger deleteDomain">Delete</btn>'
                    ),
                    'Are you sure you want to delete this domain?'
                );
            ?>
            <div class="row">
                <div class="col">
                    <div class="alert alert-primary mt-0">
                        <div class="row">
                            <div class="col-1">
                                <i class="fas fa-exclamation-triangle fa-xl"></i>
                            </div>
                            <div class="col">
                                Add the following CNAME records to the DNS configuration for your domain. The procedure for adding
                                CNAME records depends on your DNS service Provider. <a href="https://docs.aws.amazon.com/console/acm/dns/add-record">Learn More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="domains-container">
            <?php 
            foreach($domains as $idx => $domain):
            $domainParams = json_decode($domain->params);
            ?>
                <div class="domain-section" <?php echo $domain->is_dummy ? 'style="display:none"' : '' ?> data-idx="<?php echo $idx ?>">
                    <div class="row">
                        <div class="col-6">
                            <label class="card-title" for="domain_name-<?php echo $idx ?>" aria-invalid="false">
                                <?php if ($domain->default_domain == '1'): ?>
                                    <span id="defaultBadge" class="badge bg-secondary">Default</span>
                                <?php endif; ?>
                                Domain Name
                            </label>
                            <div class="input-group">
                                <span class="input-group-text">https://</span>
                                <input disabled type="text" name="domain_name-<?php echo $idx ?>" value="<?php echo $domain->domain ?>" oninput="updateDomains(this)" data-key="domain" data-idx="<?php echo $idx ?>">
                            </div>
                        </div>
                        <div class="col-4">
                            <label class="card-title" for="domain_language-<?php echo $idx ?>" aria-invalid="false">Default Language</label>
                            <select name="domain_language-<?php echo $idx ?>" onchange="updateDomains(this)" data-key="params.language" data-idx="<?php echo $idx ?>">
                                <?php foreach(AxsLanguage::getPublishedLanguages() as $language): ?>
                                    <option value="<?php echo $language->lang_code ?>" <?php echo $domainParams->language == $language->lang_code ? 'selected="selected"' : '' ?>><?php echo $language->title ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-2" style="padding-top:30px">
                            <?php if ($domain->default_domain != '1'): ?>
                                <button
                                    onclick="showDeleteDomainModal(<?php echo $idx ?>)"
                                    type="button" 
                                    class="deleteDomainButton btn btn-light me-1" 
                                    data-bs-toggle="tooltip"
                                    data-bs-title="Delete Domain"
                                ><i class="ms-1 fas fa-trash"></i></button>
                            <?php endif; ?>
                            <button
                                onclick="showTransferDomainModal(<?php echo $idx ?>)"
                                type="button" 
                                class="transferDomainButton btn btn-light"
                                data-bs-toggle="tooltip"
                                data-bs-title="Transfer Domain"
                            ><i class="ms-1 fas fa-exchange-alt"></i></button>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col">
                            <label class="card-title"  aria-invalid="false">DNS</label>
                            <table class="table">
                                <thead>
                                    <th>NAME</th>
                                    <th>TYPE</th>
                                    <th>VALUE</th>
                                </thead>
                                <tbody>
                                    <td style="cursor:pointer" data-bs-placement="left" data-bs-toggle="tooltip" data-bs-title="Click to Copy" name="copy_pre" id="<?php echo $idx ?>-domain-col" data-idx="<?php echo $idx ?>"><?php echo $domain->domain ?></td>
                                    <td>CNAME</td>
                                    <td style="cursor:pointer" data-bs-placement="left" data-bs-toggle="tooltip" data-bs-title="Click to Copy" name="copy_pre" id="<?php echo $idx ?>-dns-col" data-idx="<?php echo $idx ?>"><?php echo $awsConfig->dnsname ?></td>
                                </tbody>
                            </table>
                            <div class="alert alert-success" id="success-alert-<?php echo $idx ?>" style="display:none">
                                <strong>Success! </strong> copied to clipboard
                                <button type="button" class="btn-close float-end" data-bs-dismiss="alert"></button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if (count($domains) > 1 && array_search($domain, $domains) != count($domains) - 1): ?>
                    <hr class="my-4" />
                <?php endif; ?>
            <?php endforeach; ?>
            </div>
            <button id="addDomain" type="button" class="btn btn-success mt-4">
                <i class="fa fa-plus"></i>
                New Domain
            </button>
        <?php
        
        $html = ob_get_clean();
		return $html;
	}

}