<?php

defined('FOF_INCLUDED') or die;

class FOFFormFieldFlagged extends JFormField
{
	public $type = 'flagged';
	public function getStatic(){
	
	}	

	public function getInput()
	{
		$jinput = JFactory::getApplication()->input;
       	$id = $jinput->get('id', null, 'INT');
       	$db = JFactory::getDBO();
       	$query = $db->getQuery(true);

       	$query
       		->select ($db->quoteName('id'))
       		->from($db->quoteName('comments_flagged'))
       		->where($db->quoteName('comment_id') . '=' . (int)$id);

       	$db->setQuery($query);

       	$result = $db->loadObjectList();

       	$count = count($result);
       	if ($count > 0) { 
			return "Flags: " . $count;
		} else {
			return "None";
		}
	}

	
	public function getRepeatable()
	{
		$id = $this->item->get('id');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select ($db->quoteName('id'))
			->from ($db->quoteName('comments_flagged'))
			->where ($db->quoteName('comment_id').'='.(int)$id);

		$db->setQuery($query);

		$result = $db->loadObjectList();
		$count = count($result);

		if ($count > 0) { 
			return 'Flags: ' . $count;
		} else {
			return ''; 
		}
	}
}