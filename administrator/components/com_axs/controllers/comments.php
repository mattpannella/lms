<?php

defined('_JEXEC') or die();

class AxsControllerComments extends FOFController {

    public function getCommentById($id) {
		if(!$id) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('id').' = '. (int)$id;
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('comments')
			  ->where($conditions)
			  ->limit(1);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function commentPublish() {
		$response = new stdClass();
		$db = JFactory::getDBO();
		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$comment_id = $input->get('comment_id',0,'INT');
		$comment_enabled = $input->get('comment_status','','STRING');
		if(!$comment_id) {
			$response->status = 'error';
			$response->message = 'No Comment Found';
			echo json_encode($response);
			return;
		}
		if($comment_enabled == '1') {
			$new_enabled = '0';
		} else {
			$new_enabled = '1';
		}
		$comment = $this->getCommentById($comment_id);
		$comment->enabled = $new_enabled;
		$result = $db->updateObject("comments",$comment,"id");
		if($result) {
			$response->status = 'success';
			$response->new_enabled = $new_enabled;
		} else {
			$response->status = 'error';
			$response->message = 'There was an error updating the comment';
		}
		echo json_encode($response);
		return;
	}

	public function commentDelete() {
		$response = new stdClass();
		$db = JFactory::getDBO();
		$user  = JFactory::getUser();
		$input = JFactory::getApplication()->input;
		$comment_id = $input->get('comment_id',0,'INT');
		if(!$comment_id) {
			$response->status = 'error';
			$response->message = 'No Comment Found';
			echo json_encode($response);
			return;
		}
		$conditions[] = $db->quoteName('id').' = '. (int)$comment_id;
		$query = $db->getQuery(true);
		$query->delete($db->quoteName('comments'));
		$query->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();
		if($result) {
			$response->status = 'success';
		} else {
			$response->status = 'error';
			$response->message = 'There was an error deleting the comment';
		}
		echo json_encode($response);
		return;
	}

}