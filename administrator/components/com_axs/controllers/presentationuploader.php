<?php


defined('_JEXEC') or die();
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
class AxsControllerPresentationuploader extends FOFController {


    public function processFile() {
        $input = JFactory::getApplication()->input;
        $userEncrypted = $input->get('uid','','STRING');
        $title = $input->get('title','','STRING');
        $title = str_replace("'",'',$title);
        $title = str_replace('"','',$title);
        $key = AxsKeys::getKey('lms');
        $userDecrypted = base64_decode($userEncrypted);
        $user_id = AxsEncryption::decrypt($userDecrypted, $key);
        $file = $_FILES["filedata"];
        $result = self::upload($file,$user_id,$title);
        echo json_encode($result);
    }

    private function upload($file,$user_id,$title) {
        $user = JFactory::getUser($user_id);
        if(!$title) {
            $title = "New Presentation";
        }
        //Build json response
        $clientId = AxsClients::getClientId();
        $tempFolder = 'tmp';
        $random = rand(1000,9000);
        $directory = '/mnt/shared/'.$clientId.'/files/'.$tempFolder.'/';
        $response = new stdClass();
        $response->message = "Your File Failed to Upload";
        $response->status = "failed";
        $fileInfo = pathinfo($file['name']);
        $response->status  = "error";
        if($file['error'] == 0) {

            //check file type
            $filetype = $file['type'];
            $allowedFileTypes = [
                'application/pdf',
                'application/vnd.ms-powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation'
            ];
            $allowedFileExtenstions = [
                'pdf',
                'ppt',
                'pptx'
            ];

            if(in_array(strtolower($fileInfo['extension']), $allowedFileExtenstions)) {
                //build directory path


                if(!file_exists($directory)) {
                    mkdir($directory);
                    chmod($directory, 0755);
                }

                $uploaddir = $directory;
                $uploadFile = basename($file['name']);
                $uploadFile = preg_replace('@[^0-9a-z\.]+@i', '-', $uploadFile);
                $newName = $uploaddir . $uploadFile;
                if(move_uploaded_file($file['tmp_name'], $newName)) {


                    $date = date("Y-m-d H:i:s");
                    $response->message = "File successfully uploaded!";
                    $response->file    = $uploadFile;
                    $response->user    = $user->name;
                    $response->date    = date("m/j/Y",strtotime($date));
                    $response->status  = "success";
                    $data = new stdClass();
                    $data->file    = $uploadFile;
                    $data->user_id = $user->id;
                    $data->date    = $date;
                    // put checks for file type
                    if($fileInfo['extension'] == 'ppt' || $fileInfo['extension'] == 'pptx') {
                        shell_exec('export HOME=/tmp && libreoffice --headless --convert-to pdf --outdir '.$uploaddir.' '.$newName);
                        $powerPointFile = $newName;
                        $newName = str_replace($fileInfo['extension'],'pdf',$newName);
                    }
                    exec('mutool draw -w 1920 -h 1080 -r 200 -c rgb -o '.$uploaddir.'slide%d.jpg '.$newName);
                    $files = glob($uploaddir.'*.jpg');
                    natsort($files);
                    $slides = array();
                    $editorImagesDirectory = '/mnt/shared/'.$clientId.'/images/h5p/editor/images/'.$random.'/';
                    mkdir($editorImagesDirectory);
                    chmod($editorImagesDirectory, 0755);
                    unlink($newName);
                    if($powerPointFile) {
                        unlink($powerPointFile);
                    }
                    foreach($files as $item) {
                        $newItemName = str_replace($uploaddir,'',$item);
                        copy($item,$editorImagesDirectory.$newItemName);
                        unlink($item);
                        $slide = '{
                            "elements":[],
                            "keywords":[],
                            "slideBackgroundSelector":{
                                    "imageSlideBackground":{
                                        "path":"images/'.$random.'/'.$newItemName.'",
                                        "mime":"image/jpeg",
                                        "copyright":{
                                            "license":"U"
                                        },
                                        "width":1920,
                                        "height":1080
                                    }
                                }
                        }';

                        array_push($slides,$slide);
                    }
                    $params = new stdClass();
                    $params->slides = $slides;
                    $params->title = $title;
                    $slideObject = self::getSlideObject($params);
                    $response->presentation = $slideObject;
                }
            } else {
                $response->status  = "error";
                $response->message = "Error: File Type Not Allowed";
            }
        }
        return $response;
    }

    public static function getSlideObject($params) {
        $slideObject =  Array(
            'action' => 'create',
            'id' => '',
            'library' => 'H5P.CoursePresentation 1.20',
            'parameters' => '{
            "params":{
            "presentation":{
            "slides":['.implode(',',$params->slides).'],
                 "keywordListEnabled":true,
                 "globalBackgroundSelector":{
            },
                 "keywordListAlwaysShow":false,
                 "keywordListAutoHide":false,
                 "keywordListOpacity":90},
                 "override":{
            "activeSurface":false,
                 "hideSummarySlide":false,
                 "enablePrintButton":false,
                 "social":{
            "showFacebookShare":false,
                 "facebookShare":{
            "url":"@currentpageurl",
                 "quote":"I scored @score out of @maxScore on a task at @currentpageurl."},
                 "showTwitterShare":false,
                 "twitterShare":{
            "statement":"I scored @score out of @maxScore on a task at @currentpageurl.",
                 "url":"@currentpageurl",
                 "hashtags":"h5p,course"},
                 "showGoogleShare":false,
                 "googleShareUrl":"@currentpageurl"}},
                 "l10n":{
            "slide":"Slide",
                 "score":"Score",
                 "yourScore":"Your Score",
                 "maxScore":"Max Score",
                 "total":"Total",
                 "totalScore":"Total Score",
                 "showSolutions":"Show solutions",
                 "retry":"Retry",
                 "exportAnswers":"Export text",
                 "hideKeywords":"Hide keywords list",
                 "showKeywords":"Show keywords list",
                 "fullscreen":"Fullscreen",
                 "exitFullscreen":"Exit fullscreen",
                 "prevSlide":"Previous slide",
                 "nextSlide":"Next slide",
                 "currentSlide":"Current slide",
                 "lastSlide":"Last slide",
                 "solutionModeTitle":"Exit solution mode",
                 "solutionModeText":"Solution Mode",
                 "summaryMultipleTaskText":"Multiple tasks",
                 "scoreMessage":"You achieved:",
                 "shareFacebook":"Share on Facebook",
                 "shareTwitter":"Share on Twitter",
                 "shareGoogle":"Share on Google+",
                 "summary":"Summary",
                 "solutionsButtonTitle":"Show comments",
                 "printTitle":"Print",
                 "printIngress":"How would you like to print this presentation?",
                 "printAllSlides":"Print all slides",
                 "printCurrentSlide":"Print current slide",
                 "noTitle":"No title",
                 "accessibilitySlideNavigationExplanation":"Use left and right arrow to change slide in that direction whenever canvas is selected.",
                 "accessibilityCanvasLabel":"Presentation canvas. Use left and right arrow to move between slides.",
                 "containsNotCompleted":"@slideName contains not completed interaction",
                 "containsCompleted":"@slideName contains completed interaction",
                 "slideCount":"Slide @index of @total",
                 "containsOnlyCorrect":"@slideName only has correct answers",
                 "containsIncorrectAnswers":"@slideName has incorrect answers",
                 "shareResult":"Share Result",
                 "accessibilityTotalScore":"You got @score of @maxScore points in total"}},
                 "metadata":{
            "license":"U",
                 "authors":[],
                 "changes":[],
                 "extraTitle":"'.$params->title.'",
                 "title":"'.$params->title.'"}}',
            'task' => 'interactivecontent.apply_interactive_content',
            'dff3e305f477d963a485830f1c35218a' => 1
        );
        return $slideObject;
    }
}