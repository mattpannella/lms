<?php
defined('_JEXEC') or die();

class AxsControllerBrand_dashboard extends FOFController {


	 function checkinModules() {

        if (!JFactory::getUser()->authorise('core.edit')) {
            return;
        }
       
	 	$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
            ->update('#__modules')
            ->set("checked_out = 0")
            ->set("checked_out_time = '0000-00-00 00:00:00'")
            ->where('checked_out != 0');
            
        $db->setQuery($query);
        $result = $db->execute();
        
    }
}