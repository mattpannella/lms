<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die();

class AxsControllerAdmin_checklists extends FOFController {

	public function completeItem() {
		$input = JFactory::getApplication()->input;		
        $checklist_item = $input->get('checklist_item','','STRING');
        $returnUrlEncoded = $input->get('returnUrl','','BASE64');
        $returnUrl = base64_decode($returnUrlEncoded);
        $app = JFactory::getApplication();        
        if(!$checklist_item) {           
            $app->redirect($returnUrl);
            exit;
        }
        $checklist_item = str_replace('_',' ',$checklist_item);
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('action'). '=' .$db->quote($checklist_item);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_actions_audit');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            $query = $db->getQuery(true);
            $query->delete('axs_actions_audit');
            $query->where($conditions);
            $query->setLimit(1);
            $db->setQuery($query);
            $db->execute();
        } else {
            $params = new stdClass();		
            $params->eventName = $checklist_item;			
            $params->description = $params->eventName;  
            $params->action_type ="admin";
            AxsActions::storeAdminAction($params);            
        }
        $app->redirect($returnUrl);
        exit;
        //$db->insertObject('axs_awards_earned',$data);
		/* $data->date_earned = date("Y-m-d H:i:s");
		$award = AxsLearnerDashboard::getAwardById($data->badge_id);
		$params = json_decode($award->params);
		$expiration = null;

		if ($params->expiration_amount && $params->expiration_type && !$data->date_expires) {
			$date_earned = strtotime($data->date_earned);
			$expiration = date("Y-m-d H:i:s",strtotime($data->date_earned .' + '. $params->expiration_amount." ".$params->expiration_type));
			$data->date_expires = $expiration;
		}
		$badge_exist = AxsActions::checkAward($data->badge_id,$data->user_id);
		if(!$badge_exist) {
			$result = $db->insertObject('axs_awards_earned',$data);	
			if($result) {
				echo 'success';
			} else {
				echo 'error';
			}
		} else {
			echo 'Certificate or Badge already exists for this user';
        } */
        }
		
	}