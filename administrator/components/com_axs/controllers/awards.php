<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die();

class AxsControllerAwards extends FOFController {

	public function addNewAward() {
		$db = JFactory::getDBO();
		$input = JFactory::getApplication()->input;
		$data = new stdClass();
		$data->user_id = $input->get('user',0,'INT');
		$data->badge_id = $input->get('award_id',0,'INT');
		$data->date_earned = $input->get('date_earned',0,'STRING');
		$data->date_expires = $input->get('date_expires',0,'STRING');
		if(!$data->date_earned) {
			$data->date_earned = date("Y-m-d H:i:s");
		}
		$award = AxsLearnerDashboard::getAwardById($data->badge_id);
		$params = json_decode($award->params);
		$expiration = null;

		if ($params->expiration_amount && $params->expiration_type && !$data->date_expires) {
			$date_earned = strtotime($data->date_earned);
			$expiration = date("Y-m-d H:i:s",strtotime($data->date_earned .' + '. $params->expiration_amount." ".$params->expiration_type));
			$data->date_expires = $expiration;
		}

		if ($params->points_actions == "1") {
			$awardData = new stdClass();
			$awardData->user_id = $data->user_id;
			$awardData->category_id = $params->points_category;
			$awardData->points = $params->points;
			$awardData->date = date("Y-m-d H:i:s");
			$awardData->params = json_encode(array('award_id' => $data->badge_id));
			AxsAwards::awardPoints($awardData);
		}

		$badge_exist = AxsActions::checkAward($data->badge_id,$data->user_id);
		if(!$badge_exist) {
			$result = $db->insertObject('axs_awards_earned',$data);
			if($result) {
				echo 'success';
			} else {
				echo 'error';
			}
		} else {
			echo 'Certificate or Badge already exists for this user';
		}

	}

	public function editAward() {
		$db = JFactory::getDBO();
		$input = JFactory::getApplication()->input;
		$data = new stdClass();
		$data->user_id = $input->get('edit_award_user_id',0,'INT');
		$data->badge_id = $input->get('edit_award_id',0,'INT');
		$data->date_earned = $input->get('edit_date_earned',0,'STRING');
		$data->date_expires = $input->get('edit_date_expires',0,'STRING');
		if(!$data->user_id || !$data->badge_id) {
			echo 'error';
		}

		if(!$data->date_earned) {
			$data->date_earned = date("Y-m-d H:i:s");
		}
		$award = AxsLearnerDashboard::getAwardById($data->badge_id);
		$params = json_decode($award->params);
		$expiration = null;

		if ($params->expiration_amount && $params->expiration_type && !$data->date_expires) {
			$date_earned = strtotime($data->date_earned);
			$expiration = date("Y-m-d H:i:s",strtotime($data->date_earned .' + '. $params->expiration_amount." ".$params->expiration_type));
			$data->date_expires = $expiration;
		}

		$conditions[] = $db->quoteName('user_id').'='.$db->quote($data->user_id);
		$conditions[] = $db->quoteName('badge_id').'='.$db->quote($data->badge_id);

		$fields[] = $db->quoteName('user_id').'='.$db->quote($data->user_id);
		$fields[] = $db->quoteName('badge_id').'='.$db->quote($data->badge_id);
		$fields[] = $db->quoteName('date_earned').'='.$db->quote($data->date_earned);
		$fields[] = $db->quoteName('date_expires').'='.$db->quote($data->date_expires);

		$query = $db->getQuery(true);
		$query->update($db->quoteName('axs_awards_earned'));
		$query->set($fields);
		$query->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();
		if($result) {
			echo 'success';
		} else {
			echo 'error';
		}
	}

	public function deleteAward() {
		$db = JFactory::getDBO();
		$input = JFactory::getApplication()->input;
		$data = new stdClass();
		$data->user_id = $input->get('user_id',0,'INT');
		$data->badge_id = $input->get('award_id',0,'INT');
		if(!$data->user_id || !$data->badge_id) {
			echo 'error';
		}
		$conditions[] = $db->quoteName('user_id').'='.$db->quote($data->user_id);
		$conditions[] = $db->quoteName('badge_id').'='.$db->quote($data->badge_id);
		$query = $db->getQuery(true);
		$query->delete($db->quoteName('axs_awards_earned'));
		$query->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();

		$query = $db->getQuery(true);
		$query->delete($db->quoteName('axs_points'));
		$query->where($db->quoteName('user_id').'='.$db->quote($data->user_id));
		$query->where('JSON_EXTRACT(params, "$.award_id") = '.(int)$data->badge_id);
		$db->setQuery($query);
		$result = $result && $db->execute();

		if($result) {
			echo 'success';
		} else {
			echo 'error';
		}
	}
}