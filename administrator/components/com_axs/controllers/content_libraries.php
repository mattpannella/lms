<?php

defined('_JEXEC') or die();

class AxsControllerContent_libraries extends FOFController {

    public function importContent() {
        $input = JFactory::getApplication()->input;
        $params = new stdClass();
        $response = new stdClass();
        $contentItems = $input->get('contentItems', '', 'ARRAY');
        $import_type = $input->get('import_type', '', 'STRING');
        $import_category = $input->get('import_category', '', 'STRING');
        $import_course = $input->get('import_course', '', 'STRING');
        $import_course_name = $input->get('import_course_name', '', 'STRING');
        $import_category_name = $input->get('import_category_name', '', 'STRING');
        $import_category_type = $input->get('category_type', '', 'STRING');
        $import_course_type = $input->get('course_type', '', 'STRING');         
        $count = count($contentItems);

        if($import_type == 'courses' && $import_category_name && $import_category_type == 'new') {
            $category = new stdClass();            
            $category->title = $import_category_name;
            $import_category = self::saveCategory($category);
        }

        if($import_type == 'lessons' && $import_course_name && $import_course_type == 'new') {
            $course = new stdClass();            
            $course->title = $import_course_name;
            $course->category = $import_category;
            $import_course = self::saveCourse($course);
        }

        for($i = 0; $i < $count; $i++) {
            $item = new stdClass();
            $item->ordering = $i;
            $item->image = $contentItems[$i]['image'];
            $item->title = $contentItems[$i]['title'];
            $item->code = $contentItems[$i]['code'];            
            switch($import_type) {
                case 'courses':
                    $item->category = $import_category;
                    self::saveCourse($item);
                break;
                case 'lessons':
                    $item->course = $import_course;
                    self::saveLesson($item);
                break;
            }
        }
       
        $response->status  = 'success';
        $response->message = 'All Content Has Been Imported';
        $response->contentItems = $contentItems;
        $response->import_type = $import_type;
        $response->import_category = $import_category;
        $response->import_course = $import_course;
        $response->import_course_name = $import_course_name;
        $response->import_category_name = $import_category_name;
        $response->import_category_type = $import_category_type;
        $response->import_course_type = $import_course_type;
        echo json_encode($response);
    }

    public function saveCourse($item) {
            $db = JFactory::getDbo(); 
			/* $data['accesslevel'] = implode(',', $data['accesslevel']);
			$data['usergroup'] = implode(',', $data['usergroup']);
			$data['access'] = implode(',', $data['access']); */
            $data = new stdClass();
            $data->image = $item->image;
            $data->title = $item->title;
            $slug = preg_replace("/[^A-Za-z0-9 ]/","",$item->title);
            $data->slug = str_replace(" ","-",$slug);
            $data->ordering = $item->ordering;
            $data->enabled = 1;
            $data->access_type = 'none';
            $data->available = 1;
            $data->access = 1;
            $data->splms_coursescategory_id = $item->category;
            
			
            $params = new stdClass();
           
			$params->access_purchase_type = "access";
			$params->usergroup_purchase_register = null;
			$params->userlist_purchase_register = "";
			$params->lesson_gating = "2";
			$params->locked = "0";
			$params->display = null;
			$params->global_settings = null;
			$params->progress_color = "#14b009";
			$params->discussion_size = "large";
			$params->discussion_auto = "0";
			$params->show_teachers = "0";
			$params->show_students = "0";
			$params->show_progress = "1";
			$params->show_progress_lessons = "1";
			$params->video_type = "mp4";
			$params->award_points = "0";
			$params->points_category = "1";
			$params->points = "";			
			$params->cover_image = "";
			$params->layout = null;
            $params->use_library_content = "1";
			$params->library_content_type = 'bizlibrary';			
			$params->scorm_id = "";
			$params->scorm_start_button = null;
			$params->bizlibrary_content_title = $item->title;
			$params->bizlibrary_content_id = $item->code;
			$params->external_link = "";
			$params->lesson_button_text = "Lessons";
			$params->students_button_text = "Students";
			$params->progress_area_text = "Progress";
			$params->student_list_text = "Students";
			$params->teacher_list_text = "Teachers";
			$params->require_minimum_students = "no";
			$params->minimum_students = "";
			$params->minimum_students_not_met_text = "";
            $params->use_due_date = false;
            $params->due_date_past_completion = false;
            $params->due_date_type = "static";
            $params->due_static_date = "";
            $params->due_dynamic_number = "";
            $params->due_dynamic_unit = "minute";
            $params->course_open_date = "";
            $params->course_close_date = "";
            $params->course_visible_before_open = false;
            $params->course_visible_after_close = false;
            $params->youtube_id = "";
            $params->vimeo_id = "";
            $params->screencast_id = "";
            $params->facebook_url = "";
			$params->show_numbering = 1;
			$data->params = json_encode($params);

            $db->insertObject("#__splms_courses",$data);
            return $db->insertid();
    }
    
    public function saveLesson($item) {
        $db = JFactory::getDbo();
        $data = new stdClass();
        $data->image = $item->image;
        $data->title = $item->title;
        $slug = preg_replace("/[^A-Za-z0-9 ]/","",$item->title);
        $data->slug = str_replace(" ","-",$slug);
        $data->ordering = $item->ordering;
        $data->enabled = 1;
        $data->splms_course_id = $item->course;
        $data->language = 'en-GB';
        
        $params = new stdClass();
       
        $params->video_type = "mp4";
        $params->student_activities = "\"\"";
        $params->files = "";
        $params->links = "";
        $params->duration = "00:00:00";
        $params->cover_image = "";
        $params->layout = null;
        $params->quiz_required_enabled = "0";
        $params->indent_level = "";
        $params->interactive_id = null;
        $params->activity_section_label = "";
        $params->award_points = "0";
        $params->points_category = "1";
        $params->points = "";
        $params->use_library_content = "1";
        $params->library_content_type = "bizlibrary";
        $params->scorm_id = "";
        $params->scorm_start_button = "mp4";
        $params->bizlibrary_content_title = $item->title;
        $params->bizlibrary_content_id = $item->code;
        $params->scorm_required = "1";			
        $params->youtube_id = "";
        $params->vimeo_id = "";
        $params->screencast_id = "";
        $params->facebook_url = "";

        $data->params = json_encode($params);

        $db->insertObject("#__splms_lessons",$data);
    }

    public function saveCategory($item) {
        $db = JFactory::getDbo(); 
        /* $data['accesslevel'] = implode(',', $data['accesslevel']);
        $data['usergroup'] = implode(',', $data['usergroup']);
        $data['access'] = implode(',', $data['access']); */
        $data = new stdClass();
        $data->title = $item->title;
        $slug = preg_replace("/[^A-Za-z0-9 ]/","",$item->title);
        $data->slug = str_replace(" ","-",$slug);
        $data->ordering = 0;
        $data->enabled = 1;
        $data->access_type = 'none';
        
        $db->insertObject("#__splms_coursescategories",$data);
        return $db->insertid();
    }
}