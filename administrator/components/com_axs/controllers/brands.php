<?php

defined('_JEXEC') or die();

class AXSControllerBrands extends FOFController
{


    public function remove()
    {
        $url = 'index.php?option=com_axs&view=brands';

        $selectedBrands = $this->input->get('cid');
        if (!$selectedBrands) {
            return;
        }
        foreach($selectedBrands as $brandId){
            $associatedDomains = AxsBrands::getBrandDomainsById($brandId);
            if($associatedDomains != "") {
                $associatedDomainsArray = explode(",", $associatedDomains);
            } else {
                $associatedDomainsArray = array();
            }

            if (in_array($_SERVER['HTTP_HOST'], $associatedDomainsArray)) {
                $getBrandsByDomain = AxsBrands::getBrandsByDomain($_SERVER['HTTP_HOST']);

                if ($brandId == 1) {
                    JFactory::getApplication()->redirect($url, 'You cannot delete the default brand that is associated with the domain you are currently using.', 'error');
                    return;
                } elseif (count($getBrandsByDomain) == 1) {
                    JFactory::getApplication()->redirect($url, 'You cannot delete the brand that is associated with the domain you are currently using.', 'error');
                    return;
                } else {
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);
                    $query = "DELETE FROM axs_brands WHERE id = $brandId";
                    $db->setQuery($query);
                    $db->execute();
                }
            } elseif ($brandId == 1) {
                JFactory::getApplication()->redirect($url, 'You cannot delete the default Brand.', 'error');
            } else {
                foreach ($associatedDomainsArray as $domainName) {
                    AxsDomains::updateDomainBrandId($domainName);
                }

                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query = "DELETE FROM axs_brands WHERE id = $brandId";
                $db->setQuery($query);
                $db->execute();
            }
        }
        $app = JFactory::getApplication();
        $msg = 'The selected Brands were deleted';
        $app->redirect($url, $msg, $type = 'message');

    }
}
