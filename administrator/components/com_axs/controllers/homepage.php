<?php

defined('_JEXEC') or die();
require_once 'components/com_axs/models/brand_homepages.php';
class AxsControllerHomepage extends FOFController {

    public function saveAjax($data) {
        AxsModelBrand_Homepages::save($data);
        $response = new stdClass();
        $response->message = 'saved';
        echo json_encode($response);
    }
}
