<?php


defined('_JEXEC') or die();
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
class AxsControllerSecuredfiles extends FOFController {

    public static function processFile() {
        $input = JFactory::getApplication()->input;
        $userEncrypted = $input->get('uid','','STRING');
        $key = AxsKeys::getKey('lms');
        $userDecrypted = base64_decode($userEncrypted);
        $user_id = AxsEncryption::decrypt($userDecrypted, $key);
        $file = $_FILES["filedata"];
        $result = self::upload($file,$user_id);
        echo json_encode($result);
    }

    public static function deleteFile() {
        $input = JFactory::getApplication()->input;
        $file = $input->get('file','','STRING');
        $data = new stdClass();
        $data->file = $file;
        $result = AxsFiles::removeFileFromLocker($data);
        echo json_encode($result);
    }

    private static function upload($file,$user_id) {
        $user = JFactory::getUser($user_id);
        //Build json response
        $clientId = AxsClients::getClientId();
        $directory = '/mnt/shared/'.$clientId.'/files/secure_file_locker/';
        $response = new stdClass();
        $response->message = "Your File Failed to Upload";
        $response->status = "failed";
        $fileExists = AxsFiles::getLockerFile(basename($file['name']));
        if($fileExists) {
            $response->message = "A file with this name already exists.";
            $response->status  = "error";
            return $response;
        }

        if($file['error'] == 0) {

            // check file type - we want the raw filetype as uploaded via the webserver daemon - this is more accurate
            $filetype = mime_content_type($file['tmp_name']);

            $allowedFileTypes = [
                'application/pdf',
                'video/mpeg',
                'video/mp4',
                'audio/mp3',
                'audio/mpeg',
                'audio/mp4',
                'application/vnd.ms-powerpoint',
                'application/vnd.openxmlformats-officedocument.presentationml.presentation'
            ];

            if(in_array($filetype, $allowedFileTypes)) {
                //build directory path

                if(!file_exists($directory)) {
                    mkdir($directory);
                    chmod($directory, 0755);
                }

                $uploaddir = $directory;
                $uploadFile = basename($file['name']);
                $uploadFile = preg_replace('@[^0-9a-z\.]+@i', '-', $uploadFile);
                $newName = $uploaddir . $uploadFile;
                if(move_uploaded_file($file['tmp_name'], $newName)) {
                    $date = date("Y-m-d H:i:s");
                    $response->message = "File successfully added to your locker!";
                    $response->file    = $uploadFile;
                    $response->user    = $user->name;
                    $response->date    = date("m/j/Y",strtotime($date));
                    $response->status  = "success";
                    $data = new stdClass();
                    $data->file    = $uploadFile;
                    $data->user_id = $user->id;
                    $data->date    = $date;
                    AxsFiles::addFileToLocker($data);

                }
            } else {
                $response->status  = "error";
                $response->message = "Error: File Type Not Allowed";
            }
        }
        return $response;
    }
}