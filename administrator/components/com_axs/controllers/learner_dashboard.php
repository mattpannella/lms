<?php
defined('_JEXEC') or die();

class AxsControllerLearner_dashboard extends FOFController {

    /*public function editItem(){
        $input  = JFactory::getApplication()->input;
        $option = $input->get('option');
        $view   = 'learner_dashboard';
        $cid    = $input->post->get('cid', array(), 'array');
        JArrayHelper::toInteger($cid);
        $id = $cid[0];
        $url = "index.php?option=".$option."&view=".$view."&id=".$id;
        $this->setRedirect($url);       
    }*/

    public function remove() {

        $cid = $this->input->get('cid');
        if (!$cid) {
            return;
        }
        if(in_array(1, $cid)) {
            $url = 'index.php?option=com_axs&view=learner_dashboards';
            $app = JFactory::getApplication();
            $msg = 'You cannot delete the default Learner Dashboard.';
            $app->redirect($url, $msg, $type = 'message');
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $dashboardIds = implode(',', $cid);
            $query = "DELETE FROM axs_learner_dashboards WHERE (FIND_IN_SET(id, '$dashboardIds') > 0) AND id != 1";
            $db->setQuery($query);
            $db->execute();
            $url = 'index.php?option=com_axs&view=learner_dashboards';
            $app = JFactory::getApplication();
            $msg = 'The selected Learner Dashboards were deleted';
            $app->redirect($url, $msg, $type = 'message');
        }
    }
}