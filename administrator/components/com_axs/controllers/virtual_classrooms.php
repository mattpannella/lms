<?php

defined('_JEXEC') or die();

class AxsControllerVirtual_classrooms extends FOFController {
	
    public function endMeeting ($meetingId = null) {		
		$input = JFactory::getApplication()->input;
		$meetingId = $input->get('meetingId');
		$db = JFactory::getDbo();
	    	$query = $db->getQuery(true);
	    	$query = "SELECT moderatorPW FROM `#__bbb_meetings` WHERE `id`=".$meetingId;
	    	$db->setQuery($query);
		$password = $db->loadResult();
		
		$VC_Server = AxsClientData::getClientVirtualMeetingServer();
		$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
		$virtualMeeting = new AxsVirtualClassroom($setupParams);
		$virtualMeeting->endMeeting($meetingId, $password);
		$app = &JFactory::getApplication();
		$app->redirect("index.php?option=com_bigbluebutton");
		jexit();
	}
	
	public function getRecordings ($meetingId = 1) {		
		$input = JFactory::getApplication()->input;
		$meetingId = $input->get('meetingId');
		$VC_Server = AxsClientData::getClientVirtualMeetingServer();
		$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
		$virtualMeeting = new AxsVirtualClassroom($setupParams);
		$virtualMeeting->getRecordings($meetingId);
		jexit();
	}
	
	public function publishRecordings($recordId = null) {		
		$input = JFactory::getApplication()->input;
		$recordId = $input->get('recordId');
		$VC_Server = AxsClientData::getClientVirtualMeetingServer();
		$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
		$virtualMeeting = new AxsVirtualClassroom($setupParams);
		$virtualMeeting->publishRecordings($recordId);
		$app = &JFactory::getApplication();
		$app->redirect("index.php?option=com_bigbluebutton&view=records&message=success");
		jexit();
	}
	
	public function deleteRecordings($recordId = null) {		
		$input = JFactory::getApplication()->input;
		$recordId = $input->get('recordId');
		$meetingId = $input->get('meetingId');
		$VC_Server = AxsClientData::getClientVirtualMeetingServer();
		$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
		$virtualMeeting = new AxsVirtualClassroom($setupParams);
		$virtualMeeting->deleteRecordings($recordId);
		$app = JFactory::getApplication();
		$msg = 'The selected recording was deleted';
		$url = "index.php?option=com_axs&view=virtual_classroom&id=".$meetingId;
        $app->redirect($url, $msg, $type = 'message');
		jexit();
	}
}
