<?php

defined('_JEXEC') or die();

class AxsControllerMemberboards extends FOFController {
    function deleteGoalProgress() {

    	$id = (int)JRequest::getVar('which');

    	$db = JFactory::getDBO();

    	$query = "DELETE FROM declaration_updates WHERE id=" . $id;
    	$db->setQuery($query);
    	$db->execute();

    	//echo ($query);
    }

    function updateGoalProgress() {

    	$id = (int)JRequest::getVar('which');
    	$value = JRequest::getVar('value');

    	$db = JFactory::getDBO();

    	$query = "UPDATE declaration_updates SET update_amount=" . $value . " WHERE id=" . $id;
    	$db->setQuery($query);
    	$db->execute();
    }

    function addGoalProgress() {

    	$value = (int)JRequest::getVar('value');
    	$userId = (int)JRequest::getVar('userId');
    	$goalId = (int)JRequest::getVar('goalId');
    	$time = strtotime("now");

    	$db = JFactory::getDBO();

    	$query = "INSERT INTO declaration_updates 
    				(goal_id, user_id, update_date, update_amount)
    				VALUES
    				(" . $goalId . ", " . $userId . ", " . $time . ", " . $value . ")";
    	
    	$db->setQuery($query);
    	$db->execute();

    	echo $db->insertid();
    }
}