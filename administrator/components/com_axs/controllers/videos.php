<?php


defined('_JEXEC') or die();

class AxsControllerVideos extends FOFController {


	 function getOrder() {
        if (!JFactory::getUser()->authorise('core.edit')) {
            return;
        }

	 	$parent = JRequest::getVar('parent');
        $table = JRequest::getVar('table');
        $column = JRequest::getVar('column');


	 	$db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query
            ->select('*')
            ->from($db->quoteName($table))
            ->where($column.'='.$parent)
            ->setLimit(1)
            ->order('ordering DESC');
            
        $db->setQuery($query);
        $result = $db->loadObject();

	 	$dat = new stdClass();
        $dat->order = $result->ordering + 1;
                    
        echo json_encode($dat);
    }
}
