<?php

defined('_JEXEC') or die();

class AxsControllerDashboard extends FOFController {

	function getMemberInfo() {

		$member_list = explode(",", JRequest::getVar('member_list'));		
		$member_list = $this->getExtraMemberInfo($member_list);
		echo json_encode($member_list);
	}

	function getExtraMemberInfo($user_list) {

		$count = count($user_list);
		$db = JFactory::getDBO();

		for ($i = 0; $i < $count; $i++) {
			/*
				Get the user's ID, get their extra info, and then pull their affiliate and member rank
				as well as their recognitions.
			*/			
			$id = $user_list[$i];			

			$query = "SELECT 						
						a.user_id AS `MemberID`, 
						c.name AS `Name`, 
						c.email AS `Email`,
						c.lastvisitDate AS `LastOn`,
						c.registerDate AS `RegisterDate`,
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=6 LIMIT 1) AS `Phone`,
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=8  LIMIT 1) AS `Address`,
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=11  LIMIT 1) AS `Country`,
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=10  LIMIT 1) AS `City`, 
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=9  LIMIT 1) AS `State`,
						(SELECT value FROM joom_community_fields_values WHERE user_id=a.user_id AND field_id=23  LIMIT 1) AS `Zip`						
					FROM axs_gin_extras a
					LEFT JOIN joom_users c ON a.user_id=c.id
					WHERE a.user_id = " . $id;

			$db->setQuery($query);

			$user_list[$i] = $db->loadObjectList()[0];

			if (count($user_list[$i]) == 0) {

				$query = "SELECT * FROM joom_users WHERE id = " . $id;
				$db->setQuery($query);
				$data = $db->loadObjectList()[0];

				$user_list[$i]->MemberID = $id;
				$user_list[$i]->Name = $data->name;
				$user_list[$i]->Email = $data->email;
				$user_list[$i]->LastOn = $data->lastvisitDate;
				$user_list[$i]->Phone = "------";
				$user_list[$i]->Address = "------";
				$user_list[$i]->Country = "------";
				$user_list[$i]->City = "------";
				$user_list[$i]->State = "------";
				$user_list[$i]->Zip = "------";
			}
		}

		return $user_list;
	}

	function getBoards() {

		$which = (int)JRequest::getVar('which');
		$start_date = JRequest::getVar('start');
		$end_date = JRequest::getVar('end');

		$queryDate = "";

		if ($start_date != "0") {			
			$queryDate .= " AND date >= " . strtotime($start_date);
		}

		if ($end_date != "0") {
			//Move the time mark ahead one day to get the whole day since strtotime grabs the beginning of the day.
			$queryDate .= " AND date < " . (strtotime($end_date) + 86400);
		}

		switch ($which) {
			case "dec": $boardName = "declaration_board"; break;
			case "grat": $boardName = "gratitude_board"; break;
			case "insp": $boardName = "inspiration_board"; break;
			case "all": $boardName = "all"; break;
			default: return;
		}

		$db = JFactory::getDBO();

		if (JRequest::getVar('allData') == 0) {
			$allData = false;
		} else {
			$allData = true;
		}

		if ($boardName != 'all') {
			$query = "SELECT * FROM " . $boardName . " WHERE deleted = 0" . $queryDate;
			$db->setQuery($query);
			$boardInfo = $db->loadObjectList();

		} else {

			$categories = "SELECT * FROM axs_pulse_categories";
			$db->setQuery($categories);
			$categories = $db->loadObjectList();

			$categoryList = array();

			foreach($categories as $cat) {
				$categoryList[$cat->id] = $cat->name;
			}

			$decQuery = "SELECT * FROM declaration_board WHERE deleted = 0" . $queryDate;
			$gratQuery = "SELECT * FROM gratitude_board WHERE deleted = 0" . $queryDate;
			$inspQuery = "SELECT * FROM inspiration_board WHERE deleted = 0" . $queryDate;

			$db->setQuery($decQuery);
			$decInfo = $db->loadObjectList();

			$db->setQuery($gratQuery);
			$gratInfo = $db->loadObjectList();

			$db->setQuery($inspQuery);
			$inspInfo = $db->loadObjectList();

			$boardInfo = array();

			foreach($decInfo as $dec) {
				$info = new stdClass();

				$info->user_id = $dec->user_id;
				$info->type = 'Declaration';
				$info->date = date("M d, Y", $dec->date);
				$info->post = $dec->declaration;
				$info->public = $dec->public;

				$info->category = $categoryList[$dec->category];
				array_push($boardInfo, $info);
			}

			foreach($gratInfo as $grat) {
				$info = new stdClass();

				$info->user_id = $grat->user_id;
				$info->type = 'Gratitude';
				$info->date = date("M d, Y", $grat->date);
				$info->post = "I am grateful for " . $grat->what . " because " . $grat->why;
				$info->public = $grat->public;
				
				$info->category = $categoryList[$grat->category];
				array_push($boardInfo, $info);
			}

			foreach($inspInfo as $insp) {
				$info = new stdClass();

				$info->user_id = $insp->user_id;
				$info->type = 'Inspiration';
				$info->date = date("M d, Y", $insp->date);
				$info->post = $insp->inspiration;
				$info->public = $insp->public;

				$info->category = $categoryList[$insp->category];
				array_push($boardInfo, $info);
			}
		}

		if ($allData) {
			echo json_encode($boardInfo);
		} else {
			$dat = new stdClass();
			$dat->which = JRequest::getVar('numWhich');
			$dat->count = count($boardInfo);
			echo json_encode($dat);
		}
	}
}

?>
