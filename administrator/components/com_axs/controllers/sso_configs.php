<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

defined('_JEXEC') or die();
include_once JPATH_SITE."/libraries/axslibs/sso/metadata_reader.php";
class AxsControllerSso_configs extends FOFController {

    public function importMetadata() {
        $input = JFactory::getApplication()->input;
        $file = $input->get('file',null,'STRING');
		self::upload_metadata($file);
	}

	public function toolsRedirect() {
		header('location: index.php?option=com_axs&view=tools');
	}

    public function upload_metadata($file) {
		$post = JFactory::getApplication()->input->post->getArray();
		$document = new DOMDocument();
        $document->load($file);

		restore_error_handler();
		$first_child = $document->firstChild;
		if(!empty($first_child)) {
			$metadata = new IDPMetadataReader($document);
			$identity_providers = $metadata->getIdentityProviders();
			
			if(empty($identity_providers)) {
				echo 'error1';
			return;
			}
			foreach($identity_providers as $key => $idp){
				$saml_login_url = $idp->getLoginURL('HTTP-Redirect');
				$saml_issuer = $idp->getEntityID();
				$saml_x509_certificate = $idp->getSigningCertificate();
				 // Fields to update.
				$fields = new stdClass();
				$fields->idp_entity_id = $saml_issuer;
				$fields->single_signon_service_url =  $saml_login_url;
				/* if($saml_x509_certificate[1]) {
					$fields->certificate = $saml_x509_certificate[1];
				} else {
					$fields->certificate = $saml_x509_certificate[0];
				} */
				$fields->certificate = implode("\n/////////Certificate/////////\n",$saml_x509_certificate);
                echo json_encode($fields);
				break;
			}
			//echo 'Identity Provider details saved successfully.';
			return;
		} else {
			//echo 'Please provide valid metadata.';
			return;
		}
	}
}
