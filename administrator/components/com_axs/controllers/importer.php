<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die();

class AxsControllerImporter extends FOFController {

	/**
	 * Groups that no users can be added to
	 * @var array 
	 */
	static $forbiddenGroups  = [8];

	/**
	 * Ids of users that cannot be modified
	 * @var array 
	 */
	static $forbiddenUserIds = [601];

	public function getTemplate($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('axs_importer')
			  ->where($db->quoteName('id').'='.(int)$id)
			  ->setLimit(1);
		$db->setQuery($query);
		$template = $db->loadObject();
		return $template;
	}

	public function getTemplateNameById() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('id', '', 'INT');
		
		echo json_encode(array('name' => $this->getTemplate($id)->title));
	}

	public static function getFieldById($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields')
			  ->where($db->quoteName('id').'='.(int)$id)
			  ->setLimit(1);
		$db->setQuery($query);
		$field = $db->loadObject();
		return $field;
	}

	public static function getFieldNameByName($name) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields')
			  ->where($db->quoteName('name') . '=' . $db->quote($name))
			  ->setLimit(1);
		$db->setQuery($query);
		$field = $db->loadObject();
		return $field;
	}

	public function checkIfExists($params) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$conditions[] = $db->quoteName('field_id').'='.(int)$params->field_id;
		$conditions[] = $db->quoteName('user_id').'='.(int)$params->user_id;
		$query->select('*')
			  ->from('#__community_fields_values')
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$field = $db->loadObject();
		return $field;
	}

	public static function getGroupNameById($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__usergroups')
			  ->where($db->quoteName('id').'='.(int)$id)
			  ->where($db->quoteName('id').'!='. 8)
			  ->setLimit(1);
		$db->setQuery($query);
		$group = $db->loadObject();
		return $group;
	}

	public function downloadCSV() {
		$input = JFactory::getApplication()->input;
		$id = $input->get('template_id',0,'INT');
		if(!$id) {
			exit();
		}

		$template = $this->getTemplate($id);
		if(!$template) {
			exit();
		}

		$templateParams = json_decode($template->params);

		$title = str_replace(' ', '_', $template->title);

		// output headers so that the file is downloaded rather than displayed
		header('Content-type: text/csv');
		header('Content-Disposition: attachment; filename="'.$title.'.csv"');

		// do not cache the file
		header('Pragma: no-cache');
		header('Expires: 0');

		// create a file pointer connected to the output stream
		$file = fopen('php://output', 'w');
		// send the column headers
		$columns = array();
		$userFields = explode(',',$template->user_fields);

		foreach($userFields as $field) {
			array_push($columns,$field);
		}

		$customFieldsArray = json_decode($template->custom_fields);

		foreach($customFieldsArray as $field_id) {
			$field = $this->getFieldById((int)$field_id);

			if(strtolower($field->name) == 'first name' || strtolower($field->name) == 'last name') {

				continue;
			}

			array_push($columns, AxsLanguage::text($field->name, $field->name));
		}

		$userGroups = explode(',',$template->usergroups);

		if(!empty($templateParams->include_groups)) {
			foreach($userGroups as $group) {
				$groupName = self::getGroupNameById((int)$group)->title;
				array_push($columns, $groupName);
			}
		}

		if($templateParams->include_courses && $templateParams->courses) {
			$courses = explode(',',$templateParams->courses);
			foreach($courses as $course_id) {
				$courseName = AxsLMS::getCourseById((int)$course_id)->title;
				array_push($columns, $courseName);
			}
		}

		if($templateParams->include_certificates && $templateParams->certificates_badges) {
			$certificates_badges = explode(',',$templateParams->certificates_badges);
			foreach($certificates_badges as $certificate_id) {
				$certificateName = AxsLMS::getAwardById((int)$certificate_id)->title;
				array_push($columns ,$certificateName);
			}
		}

		fputcsv($file, $columns);

		exit();
	}

	public function randomString($length = 6) {
		$str = "";
		$characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
		$max = count($characters) - 1;
		for ($i = 0; $i < $length; $i++) {
			$rand = mt_rand(0, $max);
			$str .= $characters[$rand];
		}
		return $str;
	}

	private function validate($headers, $template) {
		$valid = true;
		$message = array();
		$templateParams = json_decode($template->params);

		// Create a map of custom field ids -> field name
		$customFields = array();
		if (!empty($template->custom_fields)) {
			foreach(json_decode($template->custom_fields) as $field_id) {
				$fieldName = strtolower($this->getFieldById((int)$field_id)->name);
				$customFields[$field_id] = $fieldName;
			}
		}

		// Create a map of user group ids -> group title
		$userGroups = array();
		if (!empty($template->usergroups)) {
			foreach(explode(',', $template->usergroups) as $group_id) {
				$groupTitle = strtolower($this->getGroupNameById((int)$group_id)->title);
				$userGroups[$group_id] = $groupTitle;
			}
		}

		// Create a map of certificate ids -> certificate titles
		$certificates = array();
		if (!empty($templateParams->certificates_badges)) {
			foreach(explode(',', $templateParams->certificates_badges) as $certificate_id) {
				$certificate = AxsLMS::getAwardById((int)$certificate_id);
				$certificateTitle = strtolower($certificate->title);
				$certificates[$certificate_id] = $certificateTitle;
			}
		}

		// Create a map of courses ids -> courses titles
		$courses = array();
		if (!empty($templateParams->courses)) {
			foreach(explode(',', $templateParams->courses) as $course_id) {
				$course = AxsLMS::getCourseById((int)$course_id);
				$courseTitle = strtolower($course->title);
				$courses[$course_id] = $courseTitle;
			}
		}

		// Cycle through all collumn headers
		// If there is a missing required header
		// Or if a header can't be mapped to a usergroup, custom field, 
		// certificate, or course validation fails
		$requiredHeaders = array(
			"first name" => false,
			"last name" => false,
			"email" => false,
			"username" => false,
		);
		$acceptedHeaders = array(
			"id",
			"password"
		);
		foreach ($headers as $header) {
			if (in_array($header, array_keys($requiredHeaders))) {
				$requiredHeaders[$header] = true;
			} else {
				// Check if the header string can be tied to a group, custom field, certificate/badge, or course
				$isGroup = in_array($header, array_values($userGroups));
				$isCustomField = in_array($header, array_values($customFields));
				$isCertificate = in_array($header, array_values($certificates));
				$isCourse = in_array($header, array_values($courses));
				if (!$isGroup && !$isCustomField && !$isCertificate && !$isCourse && !in_array($header, $acceptedHeaders)) {
					$valid = false;
					$message[] = "Unrecognized column header '$header'";
				}
			}
		}
		foreach($requiredHeaders as $requiredHeader => $found) {
			if ($found == false) {
				$valid = false;
				$message[] = "Required column header '$requiredHeader' not found.";
			}
		}

		if ($templateParams->include_groups) {
			// All group_ids should be included in column headers
			foreach($headers as $header) {
				unset($userGroups[array_search($header, $userGroups)]);
			}
			if (!empty($userGroups)) {
				$valid = false;
				$message []= 'Usergroups were configured for this import but are missing from column headers:<br/> ' . implode(',<br/>', $userGroups);
			}
		}
		if ($templateParams->include_courses) {
			// All course_ids should be included in column headers
			foreach($headers as $header) {
				unset($courses[array_search($header, $courses)]);
			}
			if (!empty($courses)) {
				$valid = false;
				$message []= 'Courses were configured to be assigned for this import but are missing from column headers: ' . implode(',', $courses);
			}
		}
		if ($templateParams->include_certificates) {
			// All certificates should be in column headers
			foreach($headers as $header) {
				unset($certificates[array_search($header, $certificates)]);
			}
			if (!empty($certificates)) {
				$valid = false;
				$message []= 'Certificates were configured to be assigned for this import but are missing from column headers: ' . implode(',', $certificates);
			}
		}

		$messages = implode('<br/>', $message);

		if ($valid == false) {
			$messages = '<span class="text-danger">' . $messages . '</span>';
		}
		return array('valid' => $valid, 'messages' => $messages);
	}

	public function replacePlaceholders($message, $data) {
        $message = str_replace('[firstname]', $data->firstname, $message);
        $message = str_replace('[lastname]',  $data->lastname,  $message);
        $message = str_replace('[username]',  $data->username,  $message);
        $message = str_replace('[password]',  $data->password,  $message);
        return $message;
    }

	public function uploadCSVFile() {
		$input = JFactory::getApplication()->input;
		$response = new stdClass();

		$file = $input->files->get('csv_file');
		$filename = $file["name"];
		$directory = 'tmp/';
		$name = explode(".", $filename);
        $fileType = strtolower($name[1]);

		$template_id = $input->get('template_id', 0, 'INT');
		$template = $this->getTemplate($template_id);
		if (!$template) {
			$response->success = false;
			$response->message = "Error: Template not found";
			echo json_encode($response);
			return;
		}

		// Log tracking data
		$tracking = new stdClass();
		$tracking->eventName = 'Ran User Importer';
		$tracking->description = $template->title.'['.$template->id.']';
		$tracking->action_type ="admin";
		AxsActions::storeAdminAction($tracking);

        if ($fileType != 'csv') {
			$response->success = false;
			$response->message = 'Invalid filetype. Please upload a .csv file.';
		} else {
			$newFileName = $name[0] . "-" . time() . "-" . rand(1,time()) . '.' . $name[1];
            $importFile = $directory . $newFileName;

			if (move_uploaded_file($file['tmp_name'], $importFile)) {
				$fileHandle = fopen($importFile, "r");
				$firstRow = fgetcsv($fileHandle, 0, ",");
				$header = array_map('strtolower', $firstRow);
				for($i = 0;  $i < count($header); $i++) {
					$header[$i] = preg_replace('/\x{FEFF}/u', '', $header[$i]);
					$header[$i] = trim($header[$i]);
				}

				$validationData = $this->validate($header, $template);

				if ($validationData['valid'] == true) {
					$response->success = true;
					$response->message = 'Upload Success';
					$response->filepath = base64_encode(AxsEncryption::encrypt($importFile, AxsKeys::getKey('lms')));
				} else {
					$response->success = false;
					$response->message = $validationData['messages'];
					$response->filepath = base64_encode(AxsEncryption::encrypt($importFile, AxsKeys::getKey('lms')));
					unlink($fileHandle);
				}
			} else {
				$response->success = false;
				$response->message = 'Error creating temp file in system.';
			}
        };

		echo json_encode($response);
	}

	public function cancelImport() {
		$input = JFactory::getApplication()->input;
		$response = new stdClass();
		
		try {
			// Get the file from the encrypted file path
			$filepath_raw = $input->get('filepath', null, 'STRING');
			$filepath_decoded = base64_decode($filepath_raw);
			$filepath = AxsEncryption::decrypt($filepath_decoded, AxsKeys::getKey('lms'));

			// Delete fle from filesystem
			unlink($filepath);
		} catch (Exception $e) {
			$response->success = false;
			$response->message = $e->getMessage();
			$response->error = $e->getCode();
		}

		$response->success = true;
		$response->message = 'Import Cancellation Successful';

		echo json_encode($response);
	}

	public function processCSVFile() {
		$db = JFactory::getDBO();
		$input = JFactory::getApplication()->input;

		// Get the file from the encrypted file path
		$filepath_raw = $input->get('filepath', null, 'STRING');
		$filepath_decoded = base64_decode($filepath_raw);
		$filepath = AxsEncryption::decrypt($filepath_decoded, AxsKeys::getKey('lms'));
		$file = new SplFileObject($filepath);

		// Get the pagination data
		// If not already set, get the number of lines in the csv
		$pageData = $input->get('pageData', null, 'ARRAY');
		if (!isset($pageData['totalLines'])) {
			$fileForLineCount = new SplFileObject($filepath);
			$fileForLineCount->seek($fileForLineCount->getSize());
			$pageData['totalLines'] = $fileForLineCount->key();
		}
		if ($pageData['done'] == 'true') {
			$pageData['done'] == true;
		} else {
			$pageData['done'] = false;
		}

		// Initialize usersUpdated and usersAdded for this import
		if (!isset($_SESSION[$filepath_raw])) {
			$_SESSION[$filepath_raw] = array();
			$_SESSION[$filepath_raw]['usersUpdated'] = 0;
			$_SESSION[$filepath_raw]['usersAdded'] = 0;
		}

		// Get the template data for this import
		$template_id = $input->get('template_id', 0, 'INT');
		$template = $this->getTemplate($template_id);
		if (!$template) {
			exit();
		}
		$templateParams = json_decode($template->params);

		// Create an array to hold each column header
		$columns = array();

		// Append user field column headers to columns array
		// These user fields are any fields that are not custom fields
		$userFields = explode(',', $template->user_fields);
		foreach ($userFields as $field) {
			$columns []= $field;
		}

		// Get the field IDs for first name and last name for the user's profile
		$firstNameFieldID = AxsHTML::getFieldByFieldCode('FIELD_GIVENNAME')->id;
		$lastNameFieldID = AxsHTML::getFieldByFieldCode('FIELD_FAMILYNAME')->id;
		$firstNameFieldName = strtolower($this->getFieldById($firstNameFieldID)->name);
		$lastNameFieldName = strtolower($this->getFieldById($lastNameFieldID)->name);
		$customFieldsArray = json_decode($template->custom_fields);
		$customFieldsArray[] = $firstNameFieldID;
		$customFieldsArray[] = $lastNameFieldID;

		// Create a map for custom field ids -> custom field names
		$customFieldsLookup = array();
		foreach($customFieldsArray as $field_id) {
			$fieldName = strtolower($this->getFieldById((int)$field_id)->name);
			array_push($columns,AxsLanguage::text($fieldName,$fieldName));
			$customFieldsLookup[$fieldName] = $field_id;
		}

		// Create a map for user group ids -> user group names
		$userGroupLookup = array();
		foreach(explode(',', $template->usergroups) as $group) {
			if(!in_array($group, static::$forbiddenGroups)) {
				$groupName = strtolower($this->getGroupNameById((int)$group)->title);
				$userGroupLookup[$group] = $groupName;
			}
		}

		$header = array_map('strtolower', $file->fgetcsv(','));
		$headerCount = count($header);
		for($i = 0;  $i < $headerCount; $i++) {
			$header[$i] = preg_replace('/\x{FEFF}/u', '', $header[$i]);
			$header[$i] = trim($header[$i]);
		}

		$messages = [];
		$file->seek($pageData['cursor']);
		for ($i = 0; $i < $pageData['pageSize']; $i++) {

			$row = $file->fgetcsv(',');
			if (!$row) {
				$pageData['done'] = true;
				break;
			}
			if ($i == 0 && (int)$pageData['cursor'] == '0') {
				continue;
			}
			$row = array_map("trim", $row);
			$firstNameFieldName = "first name";
			$lastNameFieldName  = "last name";
			$import = array();
			$userGroupsArray = array();
			$import = array_combine($header, $row);
			
			if (!empty($templateParams->include_groups)) {
				foreach(explode(',', $template->usergroups) as $group) {

					if (!in_array($group, static::$forbiddenGroups)) {

						$groupName = $userGroupLookup[$group];

						if(
							strtolower($import[$groupName]) == 'yes' ||
							strtolower($import[$groupName]) == 'y' ||
							strtolower($import[$groupName]) == 'x' ||
							$import[$groupName] == '1'
						) {

							array_push($userGroupsArray,$group);
						}
					}
				}
			}

			
			$email 	  = $import["email"];
			$password = $import["password"];

			if ($import["id"]) {
				$user_ID  = (int)$import["id"];
			} else {
				$user_ID  = '';
			}

			// Grab existing usergroups if none were parsed from the import file
			if (empty($userGroupsArray) && $user_ID) {
				$userGroupsArray = AxsUser::getGroupIdsForUser($user_ID, $email, false);
			}

			if (!in_array(2, $userGroupsArray)) {
				array_push($userGroupsArray,2);
			}

			// Check for a username value in this row
			$username = $import["username"];
			if (!$username) {
				// If a username isn't set, generate one from the first/last name fields
				switch ($templateParams->generate_username) {
					case 'auto':
						if($templateParams->username_rule == 'both') {
							$username = $import[$firstNameFieldName] . $import[$lastNameFieldName];
						}
						if($templateParams->username_rule == 'first') {
							$username = $import[$firstNameFieldName];
						}
						if($templateParams->username_rule == 'last') {
							$username = $import[$lastNameFieldName];
						}
					break;

					case 'email':
						if($import["email"]) {
							$username = $import["email"];
						}
					break;
				}
			}

			if (!$password && (!$user_ID || $templateParams->new_password)) {
				switch ($templateParams->generate_password) {
					case 'auto':
						$password = $this->randomString();
					break;

					case 'defined':
						if($templateParams->defined_password) {
							$password = $templateParams->defined_password;
						} else {
							$password = $this->randomString();
						}
					break;
				}
			}

			$user = '';
			$name = null;

			if (!empty($import[$firstNameFieldName]) || !empty($import[$lastNameFieldName])) {

				$name = $import[$firstNameFieldName] . ' ' . $import[$lastNameFieldName];
			}

			if ($user_ID && !in_array($user_ID, static::$forbiddenUserIds) ) {
				$user = JFactory::getUser($user_ID);
				if(!empty($user)) {

					/* Just to cover all the bases, if one of the two required fields exist, we can work
						* with it and still satisfy the requirement of a user having a name.
						*/
						if(empty($name)) {

						$name = $user->name;
					}
				}
			} else {
				$user = new JUser;
			}

			$udata = array(
				"name"     	   => $name,
				"username"     => $username,
				"email"        => $email,
				"block"        => 0,
				"groups"       => $userGroupsArray,
				"requireReset" => (int)$templateParams->password_reset
			);

			if($password) {
				$udata['password']  = $password;
				$udata['password2'] = $password;
			}
			
			if(!$user->bind($udata)) {
				$messages []= "<span class='text-danger'>[Row ". ($file->key() + 1) . "] Could not bind user data ". $import[$firstNameFieldName]." ".$import[$lastNameFieldName]." Reason: ".$user->getError() . '</span>';
			}
			if (!$user->save()) {
				$messages []= "<span class='text-danger'>[Row ". ($file->key() + 1) . "] Could not save user ". $import[$firstNameFieldName].' '.$import[$lastNameFieldName]." Reason: ".$user->getError()  . '</span>';
			} else {
				if($user_ID) {
					$_SESSION[$filepath_raw]['usersUpdated'] += 1;
					$messages []= "[Row ". ($file->key() + 1) . "] Updated User ". $import[$firstNameFieldName].' '.$import[$lastNameFieldName];
				} else {
					$_SESSION[$filepath_raw]['usersAdded'] += 1;
					$messages []= "[Row ". ($file->key() + 1) . "] New User ". $import[$firstNameFieldName].' '.$import[$lastNameFieldName]." Password Created: $password";
					$CUser = new CUser($user->id);
					$CUser->init();
				}
			}

			if($user->id) {
				foreach($userGroupsArray as $addGroupId) {
					if(!in_array($addGroupId, static::$forbiddenGroups)) {
						AxsActions::trackUserGroup($user->id,$addGroupId,'add');
					}
				}
				foreach($customFieldsArray as $field_id) {
					$fieldName = strtolower(array_search($field_id, $customFieldsLookup));
					if($import[$fieldName]) {
						$customData = new stdClass();
						$customData->user_id  = $user->id;
						$customData->field_id = $field_id;

						if (preg_match("/date/i", $fieldName)) {
							$customData->value = date("Y-m-d", strtotime($import[$fieldName]));
						}else{
							$customData->value = $import[$fieldName];
						}						
						
						if (strtolower($import[$fieldName]) == 'male') {
							$customData->value = 'COM_COMMUNITY_MALE';
						} else if (strtolower($import[$fieldName]) == 'female') {
							$customData->value = 'COM_COMMUNITY_FEMALE';
						}
						$customData->access   = 40;
						if($user_ID) {
							$checkField = $this->checkIfExists($customData);
						} else {
							$checkField = false;
						}
						if($checkField) {
							$customData->id = $checkField->id;
							$db->updateObject('#__community_fields_values',$customData,'id');
						} else {
							$db->insertObject('#__community_fields_values',$customData);
						}
					}
				}
				if($templateParams->include_courses && $templateParams->courses) {

					$completion_date = null;
					$courses = explode(',',$templateParams->courses);

					foreach($courses as $course_id) {
						$course = AxsLMS::getCourseById((int)$course_id);
						$courseName = strtolower($course->title);

						if($import[$courseName]) {
							if(strtolower($import[$courseName]) == 'yes'|| $import[$courseName] == '1' ||  strtolower($import[$courseName]) == 'y' || strtolower($import[$courseName]) == 'x' || strtotime($import[$courseName])) {
								if(strtotime($import[$courseName])) {
									$completion_date = $import[$courseName];
								}
								$this->insertCourseCompletion($user->id, $course_id, $completion_date);
							}
						}
					}
				}

				if($templateParams->include_certificates && $templateParams->certificates_badges) {

					$issue_date = null;
					$certificates_badges = explode(',',$templateParams->certificates_badges);
					foreach($certificates_badges as $certificate_id) {
						$certificate = AxsLMS::getAwardById((int)$certificate_id);
						$certificateName = strtolower($certificate->title);
						if($import[$certificateName]) {
							if(strtolower($import[$certificateName]) == 'yes'|| $import[$certificateName] == '1' ||  strtolower($import[$certificateName]) == 'y' || strtolower($import[$certificateName]) == 'x' || strtotime($import[$certificateName])) {
								if(strtotime($import[$certificateName])) {
									$issue_date = strtotime($import[$certificateName]);
								}
								$this->insertCertificateAwarded($user->id, $certificate_id, $issue_date);
							}
						}
					}
				}


				if($templateParams->send_emails && $templateParams->email_message && $email) {
					$brand = AxsBrands::getBrand();
					$emailCode = AxsEmail::getCode();
					$data = new stdClass();
					$data->firstname = $import[$firstNameFieldName];
					$data->lastname  = $import[$lastNameFieldName];
					$data->username  = $username;
					$data->password  = $password;
					$subject = $this->replacePlaceholders($templateParams->email_subject,$data);
					if(!$subject) {
						$subject = "You are now registered";
					}
					$emailMessage = $this->replacePlaceholders($templateParams->email_message,$data);

					$recipientData = new stdClass();
					$recipientData->name  = $data->firstname.' '.$data->lastname;
					$recipientData->email = $email;

					$emailData = new stdClass();
					$emailData->subject = $subject;
					$emailData->content = $emailMessage;
					$emailData->recipients = json_encode(array($recipientData));
					$emailData->brand_id = $brand->id;
					$emailData->owner_code = $emailCode;
					$emailData->type = 'user import';
					AxsEmail::addToEmailQueue($emailData);
				}
			}
		}

		// Since we skip the first row on the initial batch
		$pageData['cursor'] += $pageData['pageSize'];
		

		if ($pageData['cursor'] > $pageData['totalLines']) {
			$pageData['cursor'] = $pageData['totalLines'];
		}

		if ($pageData['done']) {
			// Clear session data
			unset($_SESSION[$filepath_raw]);
			// Destroy file pointer 
			$file = null;
			// Delete fle from filesystem
			unlink($filepath);
		}

    	$response = new stdClass();
    	$response->message = implode('<br/>', $messages);
		$response->success = true;
		$response->pageData = $pageData;
		$response->filepath = $filepath_raw;
    	echo json_encode($response);
	}

	protected function insertCourseCompletion($user_id, $course_id, $completion_date = null) {
		AxsLMS::setCourseCompleted($user_id, $course_id, null, $completion_date);

		$courseLessons = AxsLMS::courseProgress_getCourseLessons($course_id);

		$languages = $courseLessons[$course_id];
		$lessons = [];

		// Get all lessons for a course by combining lessons from different language codes associated with the course
		foreach ($languages as $lang_code => $lesson_list) {

			$lessons = $lessons + $lesson_list;
		}

		// All we need to do now is tally up the points for the course and its associated lessons
		foreach($lessons as $lessonId) {

			// AxsLMS::courseProgress_completeLesson($user_id, $course_id, $lessonId, $lang_code);
			$lesson = AxsLMS::getLessonById($lessonId);

			$lessonParams = json_decode($lesson->params);

			if($lessonParams->award_points) {
				$pointsData = new stdClass();

				$pointsData->user_id = $user_id;
				$pointsData->category_id = $lessonParams->points_category;
				$pointsData->points = $lessonParams->points;
				$pointsData->date = date('Y-m-d H:i:s');

				AxsAwards::awardPoints($pointsData);
			}
		}

		// Get the course points, if any, and add it to the running total
		$course = AxsLms::getCourseById($course_id);

		$courseParams = json_decode($course->params);

		if($courseParams->award_points) {
			$pointsData = new stdClass();

			$pointsData->user_id = $user_id;
			$pointsData->category_id = $courseParams->points_category;
			$pointsData->points = $courseParams->points;
			$pointsData->date = date('Y-m-d H:i:s');

			AxsAwards::awardPoints($pointsData);
		}

		// Now assign any awards for the course
        $awardCheckParams = new stdClass();
        $awardCheckParams->requirement_type = 'course';
        $awardCheckParams->course_id = $course_id;
        $award_ids = AxsLMS::getAvailableBadges($awardCheckParams);
        if($award_ids) {
			if(!$completion_date) {
				$completion_date = null;
			} else {
				$completion_date = date('Y-m-d H:i:s',strtotime($completion_date));
			}
            AxsLMS::updateBadges($award_ids, $user_id, null, $completion_date);
        }

		return true;
	}

	protected function insertCertificateAwarded($user_id, $certificate_id, $issue_date = null) {

		if(!$user_id && !$certificate_id) {
			return false;
		}

		if(!$issue_date) {
			$issue_date = date('Y-m-d H:i:s');
		} else {
			$issue_date = date('Y-m-d H:i:s',$issue_date);
		}

		$db = JFactory::getDBO();
		$data = new stdClass();
		$data->user_id = $user_id;
		$data->badge_id = $certificate_id;
		$data->date_earned = $issue_date;
		$data->date_expires = null;
		$award = AxsLearnerDashboard::getAwardById($data->badge_id);
		$params = json_decode($award->params);
		$expiration = null;
		if ($params->expiration_amount && $params->expiration_type && !$data->date_expires) {
			$date_earned = strtotime($data->date_earned);
			$expiration = date("Y-m-d H:i:s",strtotime($data->date_earned .' + '. $params->expiration_amount." ".$params->expiration_type));
			$data->date_expires = $expiration;
		}

		if($params->usergroup_actions && ($params->add_usergroups || $params->remove_usergroups)) {
			$groupData = new stdClass();
			$groupData->user_id = $user_id;
			$groupData->usergroup_actions = $params->usergroup_actions;
			$groupData->add_usergroups = $params->add_usergroups;
			$groupData->remove_usergroups = $params->remove_usergroups;
			AxsActions::setGroups($groupData);
		}

		if($params->points_actions && $params->points_category && $params->points) {
			$awardData = new stdClass();
			$awardData->user_id = $user_id;
			$awardData->category_id = $params->points_category;
			$awardData->points = $params->points;
			$awardData->date = date("Y-m-d H:i:s");
			$awardData->params = json_encode(array('award_id' => $data->badge_id));
			AxsAwards::awardPoints($awardData);
		}

		$badge_exist = AxsActions::checkAward($data->badge_id,$data->user_id);

		if($badge_exist) {
			$query = "DELETE FROM axs_awards_earned WHERE user_id = ".$db->quote($user_id)." AND badge_id = ".$db->quote($certificate_id);
			$db->setQuery($query);
			$db->execute();
		}

		$db->insertObject('axs_awards_earned',$data);
	}

}