<?php

defined('_JEXEC') or die();

class AxsControllerStarred extends FOFController {

    function addStarredItem() {
        $input = JFactory::getApplication()->input;
        $uri = $input->get('uri', null, 'STRING');
        $text = $input->get('text', null, 'STRING');
        $category = $input->get('category', null, 'STRING');

        $id = AxsTopBar::addStarredItem($category, $text, $uri);

        echo json_encode(array("id" => $id));
    }

    function removeStarredItem() {
        $input = JFactory::getApplication()->input;
        $item_id = $input->get('item_id', null, 'INT');

        $success = AxsTopBar::removeStarredItem($item_id);

        echo json_encode(array("success" => $success));
    }

    function renderStarredItems() {
        try {
            $response = array("html" => AxsTopBar::renderStarredItems(), "success" => true);
        } catch(Exception $e) {
            $response = array("success" => false);
        }

        echo json_encode($response);
    }


}