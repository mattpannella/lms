<?php

defined('_JEXEC') or die();

class AxsControllerTrials extends FOFController {

	public function trialUpgradeRequest() {
        $brand = AxsBrands::getBrand();
        $emailCode = AxsEmail::getCode();
        $user = JFactory::getUser();
        $clientId = AxsClients::getClientId();
        $clientName = AxsClientData::getClientName();
        $siteUrl = JURI::root();
        $trialExpiration = AxsClientData::getClientExpiration();
        if(!$clientName) {
            $clientName = "Trial Client ID: $clientId";
        }
        $subject = "$clientName is Requesting Upgrade";
        $emailMessage  = "$clientName is requesting to upgrade from a trial account. <br/><br/>";
        $emailMessage .= "Admin Name: $user->name <br/>";
        $emailMessage .= "Admin Email: $user->email <br/>";
        $emailMessage .= "Admin ID: $user->id <br/>";
        $emailMessage .= "Account Name: $clientName <br/>";
        $emailMessage .= "Account ID: $clientId <br/>";
        $emailMessage .= "URL: $siteUrl <br/>";
        $emailMessage .= "Expiration Date: ".date('m-d-Y',strtotime($trialExpiration))." <br/>";

        $recipientData = new stdClass();
        $recipientData->name  = "Sales Manager";
        $recipientData->email = 'sales@tovutiteam.com';

        $emailData = new stdClass();
        $emailData->subject = $subject;
        $emailData->content = $emailMessage;
        $emailData->recipients = json_encode(array($recipientData));
        $emailData->brand_id = $brand->id;
        $emailData->owner_code = $emailCode;
        $emailData->type = 'Trial upgrade Request';
        AxsEmail::addToEmailQueue($emailData);
    }
}