<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 4/4/16
 * Time: 3:13 PM
 */
defined('_JEXEC') or die();
include_once JPATH_ROOT . '/components/shared/controllers/common.php';

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsControllerAffiliates extends FOFController {

    function deleteCode() {
        $code_id = (int)$this->input->get('code_id', '0');
        if (AxsExtra::deleteUserAffiliateCode($code_id)) {
            echo "success";
        } else {
            echo "error";
        }
    }

    function saveCode() {
        $code = $this->input->get('code', '0');
        $user_id = (int)$this->input->get('user_id', '0');
        $res = AxsExtra::setUserAffiliateCode($code, $user_id);
        if($res == 1) {
            echo "success";
        } else {
            echo $res;
        }
    }

    function checkCode() {
        $code = $this->input->get('code', 0);
        if (AxsExtra::getUserFromAffiliateCode($code)) {
            echo "error";
        } else {
            echo "success";
        }
    }

    function removeReferee() {
        $uid = (int)$this->input->get('uid', '0');
        AxsExtra::setUserParent(0, $uid);
        echo "success";
    }

    function moveReferee() {
        $pid = (int)$this->input->get('pid', '0');
        $uid = (int)$this->input->get('uid', '0');
        AxsExtra::setUserParent($pid, $uid);
        echo "success";
    }

    function addReferee() {
        $uid = (int)$this->input->get('uid', '0');
        $cid = (int)$this->input->get('cid', '0');

        AxsExtra::setUserParent($uid, $cid);
        echo "success";
    }

    function changeReferrer() {
        $pid = (int)$this->input->get('pid', '0');
        $uid = (int)$this->input->get('uid', '0');

        AxsExtra::setUserParent($pid, $uid);
        echo "success";
    }

    function checkUser() {
        $uid = (int)$this->input->get('uid', '0');

        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('name')
            ->from('#__users')
            ->where($db->qn('id').'='.$uid);
        $db->setQuery($query);
        $user = $db->loadObject();
        if(is_object($user)) echo $user->name;
        else echo 'error';
    }

    function addFamily() {
        $uid = (int)$this->input->get('uid', '0');
        $fid = (int)$this->input->get('fid', '0');
        $rel = $this->input->get('rel', '0');

        //first check if $fid is already attached to a family..
	      // if so fail. We don't allow jumping between families.
        if(count(AxsExtra::getFamilyMembers($fid)) == 1) {
            //set the users referrer
            $primary = AxsExtra::getUserRewardsData($uid);
            AxsExtra::setUserParent($primary->referrer_id, $fid);
            //set the user primary family member id
            AxsExtra::setUserPrimaryFamilyMember($uid, $fid);
            //set their relationship
            AxsExtra::setUserPrimaryFamilyMemberRelationship($rel, $uid);
            echo 'success';
        } else  {
            echo 'error';
        }
    }

    function loadMoreRewards() {

        $page = (int)$this->input->get('page', '1');
        $user_id = (int)$this->input->get('uid', '0');

        $rows = AxsExtra::getUserRewardsTransactions($user_id, $page);

        $htmlrows = '';
        foreach ($rows as $tran) {

            if ($tran->referee_id != 0) {
                $profile = ginProfile($tran->referee_id);
                $name = $profile->firstname.' '.$profile->lastname;                
            } else {
                $name = 'System';
            }

            $class = $tran->amount > 0 ? 'success' : 'error';

            $htmlrows .=
              "<tr class='$class'>".
              "<td>".$tran->reason."</td>".
              "<td><a href='index.php?option=com_users&task=axsuser.edit&id=$tran->referee_id'>".$name."</a></td>".
              "<td>".AxsExtra::format_date($tran->date)."</td>".
              "<td>".$tran->amount."</td>".
              "<td>".$tran->status."</td>".
              "</tr>";
        }
        echo $htmlrows;
    }

    function adjustPoints() {
        $user_id = (int)$this->input->get('user_id', '0');
        $points = (float)$this->input->get('points', '0');
        $reason = $this->input->get('reason', '', 'STRING');
        AxsExtra::setUserRewardsPoints($points, $user_id);

        $params = new stdClass();
        $params->referee_id = 0;
        $params->amount = $points;
        $params->reason = $reason;
        $params->status = "Awarded";
        $row = AxsExtra::setUserRewardsTransaction($params, $user_id);

        echo 'success';

        $action_type = 12;
        AxsExtra::setAdminAction($action_type, $user_id, $row, $reason);
    }

    function addCommission() {
        $user_id = (int)$this->input->get('user_id', 0);
    	$referee_id = (int)$this->input->get('referee_id', 0);
	    $reason = $this->input->get('reason', '', 'STRING');
        $amount = (float)$this->input->get('amount', 0);
        $status = $this->input->get('status', '');

	    $params = new stdClass();
	    $params->amount = $amount;
	    $params->referrer_id = $user_id;
	    $params->referee_id = $referee_id;
	    $params->status = $status;
	    $params->reason = $reason;
    	AxsExtra::setUserCommission($params);

	    $action_type = 14;
	    AxsExtra::setAdminAction($action_type, $user_id, $referee_id, $reason);

	    echo 'success';
    }

    function changeCommissionStatus() {
        $id = (int)$this->input->get('commission_id', 0);
    	$status = $this->input->get('status', '');
        $reason = $this->input->get('reason', '', 'STRING');

    	$params = new stdClass();
	    $params->id = $id;
	    $params->status = $status;
    	AxsExtra::updateUserCommissionStatus($params);

	    $commission = AxsExtra::getCommissionById($id);
	    $action_type = 0;
	    switch($status) {
		    case 'Cancelled':
		    	$action_type = 15;
			    break;
		    case 'Pending':
		    	$action_type = 27;
			    break;
		    case 'Awarded':
		    	$action_type = 16;
			    break;
	    }
	    AxsExtra::setAdminAction($action_type, $commission->user_id, $id, $reason);

	    echo 'success';
    }
}