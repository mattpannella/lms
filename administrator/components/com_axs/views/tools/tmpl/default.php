<?php

defined("_JEXEC") or die; 
?>

<div class="input-group" style="margin-bottom:10px; max-width: 800px;">
    <span class="input-group-text bg-primary prekey-text">
        <span class="text-white"><i class="icon-link"></i> URL</span>
    </span>
    <input type="text" class="form-control original_url" value="">
    <btn class="btn btn-success encode_url_btn key-btn">
        <span><i class="icon-loop"></i> Encode</span>
    </btn>
</div>

<div class="input-group encoded_url_wrapper" style="margin-bottom:10px; max-width: 800px; display:none;">
    <input type="text" id="encoded_url" value="" class="full-width">
    <btn class="btn btn-success copy_url key-btn">
        <span><i class="icon-copy"></i> Copy URL</span>
    </btn>
</div>

<script>
    jQuery(document).ready(function(){
        jQuery(document).on('click', '.encode_url_btn', function(){
            var url = jQuery(".original_url").val();
            var encoded_url = encodeURIComponent(url);
            jQuery("#encoded_url").val(encoded_url);
            jQuery(".encoded_url_wrapper").fadeIn(200);
        });
        jQuery(document).on('click', '.copy_url', function(){     
            var encoded_url = document.getElementById("encoded_url");
            encoded_url.select();
            document.execCommand('copy');
        })
    });
</script>
