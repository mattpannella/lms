<?php 
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
$clientId = AxsClients::getClientId();
$defaultDuration = 4;
?>
<?php 
    echo AxsLoader::loadKendo();
    $activityData  = AxsClientData::getClientUsers();    
?>

<table id="clients" class="table table-bordered table-striped table-responsive">                            
    <tr>
        <th>User_ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Login</th>
        <th>Logout</th>
        <th>Duration</th>
        <th>Activity</th>
    </tr>
    <?php 
        foreach($activityData as $data) {
    ?>
            <tr>
                <td><?php echo $data->user_id; ?></td>
                <td><?php echo $data->name; ?></td>
                <td><?php echo $data->email; ?></td>
                <td><?php echo $data->login; ?></td>
                <td><?php echo $data->logout; ?></td>
                <td><?php echo $data->duration; ?></td>
                <td><?php echo $data->activity; ?></td>
            </tr>            
    <?php

        } 
    ?>    
</table>
<script>
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",           
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        columns: [                    
            {
                field: "User_ID",
                title: "User Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Name",
                title: "Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Email",
                title: "Email",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },                
            {
                field: "Registered",
                title: "Registered",
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            },
            {
                field: "Client",
                title: "Client Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Account_ID",
                title: "Account Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Subscription",
                title: "Subscription",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Status",
                title: "Status",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            }
        ]
    }

    var fileLocker = $("#clients").kendoGrid(kendoOptions);
    var grid = fileLocker.data("kendoGrid");
</script>