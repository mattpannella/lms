<?php
$user = JFactory::getUser();
$isAdmin = AxsUser::isAdmin($user->id);
$isSuperUser = AxsUser::isSuperUser($user->id);

$people = true;
$content = true;
$learning = true;
$communications = true;
$analytics = true;
$configuration = true;
if (isset($_SESSION['admin_access_levels']->permissions) && (!$isAdmin && !$isSuperUser)) {
	//This is built in the axslibs permission library
	 $permissions = $_SESSION['admin_access_levels']->permissions;

	if($permissions['tovuti_nav_people']->children['tovuti_nav_user_manger']->allowed) {
		$people = true;
	} else {
		$people = false;
	}
	if($permissions['tovuti_nav_content_creation']->children['tovuti_nav_interactive_content']->allowed) {
		$content = true;
	} else {
		$content = false;
	}
	if($permissions['tovuti_nav_learning']->children['tovuti_nav_learning_settings']->allowed) {
		$learning = true;
	} else {
		$learning = false;
	}
	if($permissions['tovuti_nav_communications']->children['tovuti_nav_notifications']->allowed) {
		$communications = true;
	} else {
		$communications = false;
	}
	if($permissions['tovuti_nav_analytics']->children['tovuti_nav_analytics_report_builder']->allowed) {
		$analytics = true;
	} else {
		$analytics = false;
	}

	if($permissions['tovuti_nav_configuration']->children['tovuti_nav_configuration_profile_fields']->allowed) {
		$configuration = true;
	} else {
		$configuration = false;
	}
}

?>
<div class="tov-dashboard-container">
	<div class="tov-dashboard-wrapper">
		<div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-3">
			<?php if($people) { ?>
				<div class="col">
					<a href="index.php?option=com_users&view=axsusers" class="card">
						<div class="card-header">
							<i class="fas fa-users"></i>
						</div>
						<div class="card-body">
							<h5 class="card-title">People</h5>
							<p class="card-text">
							Manage and organize your LMS Users to give them a personalized experience geared towards success.
							</p>
						</div>
					</a>
				</div>
			<?php } ?>
			<?php if($content) { ?>
			<div class="col">
				<a href="/administrator/index.php?option=com_interactivecontent" class="card">
					<div class="card-header">
						<i class="fas fa-shapes"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title">Content Creation</h5>
						<p class="card-text">
							Build engaging content that makes the learning process for your students enjoyable and impactful.
						</p>
					</div>
				</a>
			</div>
			<?php } ?>
			<?php if($learning) { ?>
				<div class="col">
					<a href="index.php?option=com_splms&view=settings" class="card">
						<div class="card-header">
							<i class="fas fa-graduation-cap"></i>
						</div>
						<div class="card-body">
							<h5 class="card-title">Learning</h5>
							<p class="card-text">
								Organize courses and lessons to easily lay out exactly what you want your students to learn.
							</p>
						</div>
					</a>
				</div>
			<?php } ?>
			<?php if($communications) { ?>
				<div class="col">
					<a href="index.php?option=com_notifications&view=auto_alerts" class="card">
						<div class="card-header">
							<i class="fas fa-bullhorn"></i>
						</div>
						<div class="card-body">
							<h5 class="card-title">Communications</h5>
							<p class="card-text">
								Keep your students up to date with in-house communication resources.
							</p>
						</div>
					</a>
				</div>
			<?php } ?>
			<?php if($analytics) { ?>
				<div class="col">
					<a href="index.php?option=com_reports" class="card">
						<div class="card-header">
							<i class="fas fa-analytics"></i>
						</div>
						<div class="card-body">
							<h5 class="card-title">Analytics</h5>
							<p class="card-text">
								Build specialized reports to better understand your students’ results, evaluate their needs, and continually improve your LMS.
							</p>
						</div>
					</a>
				</div>
			<?php } ?>
			<?php if($configuration) { ?>
			<div class="col">
				<a href="<?php echo AxsDbAccess::checkAccess("community") ? '/administrator/index.php?option=com_community&view=profiles' : 'index.php?option=com_axs&view=email_settings' ?>" class="card">
					<div class="card-header">
						<i class="fas fa-cog"></i>
					</div>
					<div class="card-body">
						<h5 class="card-title">Configuration</h5>
						<p class="card-text">
							Begin in this tab to set up top-level features that will be applied throughout your LMS.
						</p>
					</div>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>
</div>