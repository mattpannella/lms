<?php

require_once(JPATH_ADMINISTRATOR . "/components/com_axs/helpers/helper.php");

$input = JFactory::getApplication()->input;
$selection     = $input->get('selection','','STRING');
$element       = $input->get('el','','STRING');
$selectedItems = $input->get('val','','STRING');
$filterEncoded = $input->get('filter','','BASE64');

if($filterEncoded) {
	$filter = base64_decode($filterEncoded);
}

if(!$selection) {
	echo "No Selection Group";
	die();
}

switch($selection) {
	case 'courses':
		$column1 = 'Title';
		$column2 = 'Category';
	break;
	case 'media':
		$column1 = 'Title';
		$column2 = 'Category';
	break;
	case 'users':
		$column1 = 'Name';
		$column2 = 'Email';
	break;
	case 'events':
		$column1 = 'Events';
		$column2 = 'Start Date';

        $custom_conditions = array(
            $db->quoteName('parent_id') . ' = 0',
        );
	break;
    case 'groups':
        $column1 = 'Title';
    break;
	default:
		$column1 = 'Title';
		$column2 = 'Category';
	break;
}

$db = JFactory::getDBO();

$dataList = AxsSelector::getList($selection, $filter, $custom_conditions);
echo AxsLoader::loadKendo();
?>
<style>
	.selectorCheckbox {
		width: 20px !important;
		height: 20px !important;
		margin-left: 10px !important;
	}
	.k-link {
	    font-weight: bold;
	}
	.selectBtn {
	    padding: 7px 15px 7px 15px;
	    border: 1px solid #283848;
	    color: #283848;
	    font-size: 14px;
	    text-align: center;
	    margin-top: 8px;
	    border-radius: 50px;
	    text-decoration: none;
	    background: #fff;
	    transition: ease .3s all;
	    text-shadow: none;
	    cursor: pointer;
	    display: block;
	    float: right;
	}

	.selectBtn:hover {
		color: #fff;
		background: #ea8936;
	}

	.selectTitle {
		color: #fff;
		float: left;
		font-size: 20px;
		line-height: 50px;
		text-transform: capitalize;
	}

	.selectorHeader {
		color: #fff;
		background: #283848;
		padding-left: 20px;
		padding-right: 20px;
		height: 50px;
		position: fixed;
		top: 0px;
		left: 0px;
		box-shadow: 0px 1px 2px 0px rgba(0,0,0,0.8);
		width: 100%;
		z-index: 2;
	}

	.selectorTableContainer {
		margin-top: 50px;
		z-index: 1;
	}
</style>
<div class="selectorHeader">
	<div class="selectTitle">Select <?php echo $selection ?></div>
	<div class="selectBtn"><i class="icon-plus"></i> Select</div>
	<div class="clearfix"></div>
</div>
<div id="selectorTableContainer" class="selectorTableContainer">
	<table id="selectorTable" class="table table-bordered table-striped table-responsive">
	<?php

		foreach ($dataList as $item) {
			$title = '';
			$categories = '';
			$id = '';

            switch($selection) {
                case 'courses':
                    $col1 = $item->title;
                    $col2 = AxsSelector::getCategories($selection,$item->splms_coursescategory_id);
                    $id = $item->splms_course_id;
                break;

                case 'media':
                    $col1 = $item->title;
                    $col2 = AxsSelector::getCategories($selection,$item->category);
                    $id = $item->id;
                break;

                case 'users':
                    $col1 = $item->name;
                    $col2 = $item->email;
                    $id = $item->id;
                break;

                case 'events':
                    $col1 = $item->title;
                    $col2 = date("m/d/Y",strtotime($item->event_date));
                    $id = $item->id;
                break;

                case 'groups':
                    $col1 = $item->title;
                    $id = $item->id;
                break;
            }
	?>
		<tr>
			<td><input type="checkbox" name="ids[]" value="<?php echo $id; ?>" class="selectorCheckbox selectorId"/></td>
			<td><?php echo $col1; ?></td>
			<td><?php echo $col2; ?></td>
		</tr>
	<?php } //end foreach loop ?>
	</table>
</div>

<script>
	var ids = [];
	<?php if($selectedItems) { ?>
	var selectedItemsList  = atob('<?php echo $selectedItems; ?>');
	ids = selectedItemsList.split(',');
	<?php } ?>

	function loadCheckedItems() {
		console.log('loaded items');
		console.log(ids);
		jQuery('.selectorId').each(function() {
			var value = jQuery(this).val();
			if(ids.indexOf(value) > -1) {
				jQuery(this).prop('checked',true);
			}
		});
	}


	var kendoOptions = {
		dataBound: loadCheckedItems,
		sortable: 	true,
		resizable: 	true,
		columnMenu: true,
		filterable: {
			mode: "row"
		},
		pageable: false,
		columns: [
			{
				field: 	"select",
				title: 	"Select",
				width: "100px",
				filterable: false
			},
            {
            	field: "<?php echo $column1; ?>",
            	title: "<?php echo $column1; ?>",
            	filterable: {
            		operators: {
            			string: {
            				contains: "Contains"
            			}
            		}
            	}
            },
			{
				field: 	"<?php echo $column2; ?>",
				title: 	"<?php echo $column2; ?>",
				filterable: {
            		operators: {
            			string: {
            				contains: "Contains"
            			}
            		}
            	}
			}

        ]
	}

	jQuery("#selectorTable").kendoGrid(kendoOptions);
	jQuery('.k-filtercell').each(function() {
		var field = jQuery(this).data('field');
		console.log('field: '+field);
		if(field == 'select') {
			var checkbox = '<input type="checkbox" name="selectorCheckbox_SelectAll" class="selectorCheckbox selector_SelectAll"/>';
			jQuery(this).html(checkbox);
		}
	});

	jQuery('.selector_SelectAll').change(function() {
		var checked = jQuery(this).prop('checked');
		jQuery('.selectorId').prop('checked',checked);
		jQuery('.selectorId').each(function() {
			var val = jQuery(this).val();
			if(checked) {
				if(ids.indexOf(val) == -1) {
					ids.push(val);
				}
			} else {
				var index = ids.indexOf(val);
				ids.splice(index,1);
			}
		});
		console.log(ids);
	});

	jQuery('.selectBtn').click(function() {
		var data = {
			ids: ids,
			element: '<?php echo $element; ?>',
			type: 'selectCoursesEvent'
		}
		window.parent.postMessage(data);
	});

	jQuery(document).on('change','.selectorId', function() {
		var val = jQuery(this).val();
		if(jQuery(this).prop('checked')) {
			if(ids.indexOf(val) == -1) {
				ids.push(val);
			}
		} else {
			var index = ids.indexOf(val);
			ids.splice(index,1);
		}
		console.log(ids);
	});

	loadCheckedItems();
</script>
