<?php 
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
$clientId = AxsClients::getClientId();
?>
<?php 
    echo AxsLoader::loadKendo();
    $clientData  = AxsClientData::getClientChecklists();
     
?>

<table id="clients" class="table table-bordered table-striped table-responsive">                            
    <tr>
        <th>Id</th>
        <th>Name</th>
        <!-- <th>Progress</th> -->
        <th>Action</th>
    </tr>
    <?php 
        foreach($clientData as $client) {
            $link = '';
            $percentComplete = 0;
            if($client->checklistId) {
                /* $creds = AxsClients::decryptClientParams($client->dbparams);
                $checklistRow = AxsOnboardingChecklist::getChecklistById($client->checklistId);
                $checklistData = AxsOnboardingChecklist::loadClientChecklist($creds,$checklistRow);
                $percentComplete = $checklistData->getPercentComplete(); */
                $link = 'index.php?option=com_axs&view=admin_checklist&client_id='.$client->clientId.'&id='.$client->checklistId;
                $btnClass = 'default';
                $btnText = '<i class="icon-pencil"></i> Edit Checklist';
            } else {
                $link = 'index.php?option=com_axs&view=admin_checklist&client_id='.$client->clientId;
                $btnClass = 'success';
                $btnText = '<i class="icon-new"></i> Add Checklist';
            }
        
        ?>
                <tr>
                    <td><?php echo $client->clientId; ?></td>
                    <td><?php echo str_replace('site_','',$client->client_name); ?></td>
                    <!-- <td><?php //echo $percentComplete; ?>%</td> -->
                    <td><a href="<?php echo $link; ?>" class="btn btn-sm btn-<?php echo $btnClass; ?>"><?php echo $btnText; ?></a></td>
                </tr>            
    <?php 
        } 
    ?>    
</table>
<script>
    var kendoOptions = {
        resizable:  true,
        columnMenu: true,
        filterable: {
            mode: "row"
        },
        pageable: {
                buttonCount: 8,
                input: true,
                pageSize: 20,
                pageSizes: true,
                refresh: true,
                message: {
                    empty: "There are no entries to display"
                }
            },
        columns: [                    
            {
                field: "Id",
                title: "Id",
                width: '200px',
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Name",
                title: "Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            /* {
                field: "Progress",
                title: "Progress",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            }, */
            {
                field: "Action",
                title: "Action",
                width: '200px',
                filterable: false
            
            }
        ]
    }

    var fileLocker = $("#clients").kendoGrid(kendoOptions);
    var grid = fileLocker.data("kendoGrid");
</script>