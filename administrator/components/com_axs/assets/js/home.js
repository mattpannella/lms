function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

var changeRepeatables = function () {
	var id;
	jQuery('.field-media-input').each(function(e) {
		id = jQuery(this).attr('id');
		if(id.includes('_repeatable')) {
			var tableId = jQuery(this).closest('table').attr('id');
			var newId = id.replace('_repeatable','_' + tableId);
			jQuery(this).attr('id',newId);		
		}			
		
	});
}

jQuery(document).ready(
	function() {
		changeRepeatables();		
	}
);


jQuery(document).on('click','.add', function(){
	setTimeout(function(){
		changeRepeatables();
	}, 700);			
});

jQuery(document).ready( function() {
	var id = getUrlVars()['id'];

	if(id){
		var previewButton = `<div class="btn-wrapper" id="toolbar-preview">
								<a data-url="/landingpage-preview?id=`+id+`" target="_blank" class="btn preview-button btn-primary btn-sm mx-1"><i class="fa fa-rocket"></i> Preview Design</a>
							 </div>`;
							 jQuery('.btn-toolbar').append(previewButton);
	}

	jQuery(document).on('click','.preview-button',function(){
		var url = jQuery(this).data('url');
		var data = jQuery('#adminForm').serialize();
		if(data) {
			jQuery.ajax({
				url: 'index.php?option=com_axs&task=homepage.saveAjax&format=raw',
				data: data,
				type: 'post'
				}).done(function(result) {
					window.open(url, '_blank');
				}).fail(function() {
					
				});		    		
		}
	})
});


