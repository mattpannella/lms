const hideSourceIfSecureFileLockerSelected = function() {
        var el = $('[name="media_type"]');
        var value = el.val();
    
        if (value == 'secure_file_locker') {
            $("<style id='hideSourceRowStyle'>")
                .prop("type", "text/css")
                .html(`#source-row { display: none !important; }`)
                .appendTo("head");
        } else {
            $('#source-row').remove();
        }
}

$(document).on('change', '[name="media_type"]', hideSourceIfSecureFileLockerSelected);
$(() => hideSourceIfSecureFileLockerSelected());
