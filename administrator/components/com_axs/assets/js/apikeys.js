 jQuery(() => {
    jQuery(document).on('click','#view-key', function(){
        var key_id = jQuery(this).data('key');
        if(jQuery('#'+key_id).attr('type') == 'password') {
            jQuery('#'+key_id).attr('type','text');
        } else {
            jQuery('#'+key_id).attr('type','password');
        }
    });
    jQuery(document).on('click','#copy-key', function(){
        var key_id = jQuery(this).data('key');        
        var keyText = document.getElementById(key_id);
        keyText.select();
        document.execCommand('copy');
    })

    jQuery(document).on('click','#copy-secret-key', function(){
        var key_id = jQuery(this).data('key');
        jQuery('#'+key_id).attr('type','text');      
        var keyText = document.getElementById(key_id);
        keyText.select();
        document.execCommand('copy');
        jQuery('#'+key_id).attr('type','password');
    })
});