function initializeModuleButton(buttonId) {
    // bind callback for loading module data and iframe into modal
    jQuery(document).on("click", `[id$=${buttonId}]` , function() {
        jQuery.ajax({
            type: "POST",
            url: "index.php",
            data: {
                "option": 	"com_axs",
                "task": 	"brand_dashboard.checkinModules",
                "format":   "raw"
            }
        });

        var parent = this.parentNode;
        var moduleSelect = null;
        var haveSelectList = false;

        while (!haveSelectList) {
            moduleSelect = parent.getElementsByClassName("module-select");
            if (moduleSelect.length == 0) {
                parent = parent.parentNode;
            } else {
                haveSelectList = true;
            }
        }

        var moduleId = jQuery(moduleSelect).val();

        if (moduleId == "spacer") {
            alert("You can\'t edit a spacer module.");
            return;
        }

        var src = "/administrator/index.php?option=com_modules&task=module.edit&tmpl=module&id=" + moduleId;
        var iframe = `<iframe src=${src} frameborder="0" style="width: 100%; height: 70vh; margin-bottom:10px;"></iframe>`;
        jQuery('#modulesModal').find(".modal-body").html(iframe);
        const modal = document.getElementById("modulesModal");
        const bsModal = new bootstrap.Modal(modal, {})
        bsModal.show();
    });
}
