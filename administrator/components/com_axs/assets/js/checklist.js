jQuery(document).ready(function($) {

    function disableAutoCheckOnCustomItemType(elem) {
        let row = $(elem).closest('tr');

        // first change to "user" if "auto" was selected
        if ($('[id$=__completion_type]', row).val() === 'auto') {
            $('[id$=__completion_type]', row).val('user');
        }

        // then disable "auto" option
        $('[id$=__completion_type] option[value="auto"]', row).prop('disabled', true);
    }

    function enableAutoCheckOnNonCustomItemType(elem) {
        $('[id$=__completion_type] option[value="auto"]', $(elem).closest('tr')).prop('disabled', false);
    }

    $(document).on('change', '.item_type', function() {
        if ($(this).val() === 'custom') {
            disableAutoCheckOnCustomItemType(this);
        }
        else {
            enableAutoCheckOnNonCustomItemType(this);
        }
    });

    $('.item_type').each(function(index, elem) {
        if ($(this).val() === 'custom') {
            disableAutoCheckOnCustomItemType(elem);
        }
        else {
            enableAutoCheckOnNonCustomItemType(elem);
        }
    });
});
