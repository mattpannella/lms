jQuery(document).ready(function($) {
    var rating_number = $('#rating_number').val();
    var rating_labels = 'default';
    if($('#likert_labels1').attr('checked') == 'checked') {
        rating_labels = 'custom';
    }
    const max_labels = 11;
    const default_labels = {
        five: [
            'Strongly Disagree',
            'Disagree',
            'Undecided',
            'Agree',
            'Strongly Agree'
        ],
        seven: [
            'Strongly Disagree',
            'Disagree',
            'Somewhat Disagree',
            'Undecided',
            'Somewhat Agree',
            'Agree',
            'Strongly Agree'
        ],
        eleven: [
            'Very Strongly Disagree',
            'Strongly Disagree',
            'Disagree',
            'Mostly Disagree',
            'Somewhat Disagree',
            'Undecided',
            'Somewhat Agree',
            'Mostly Agree',
            'Agree',
            'Strongly Agree',
            'Very Strongly Agree'
        ]
    }

    function autoFillLabels(overide) {
        var labels;
        switch(rating_number) {
            case '5':
                labels = default_labels.five;
            break;
            case '7':
                labels = default_labels.seven;
            break;
            case '11':
                labels = default_labels.eleven;
            break;
        }
        for(let i = 1; i <= rating_number; i++) {
            let index = i - 1;
            let value = $('#label_'+i).val();
            if(value == '' || overide) {
                $('#label_'+i).val(labels[index]);
            }

        }
    }

    function hideAllLabels() {
        for(let i = 1; i <= max_labels; i++) {
            $('#label_'+i).parent().parent().hide();
        }
    }

    function showAllLabels() {
        for(let i = 1; i <= rating_number; i++) {
            $('#label_'+i).parent().parent().show();
        }
    }

    function toggleLabels() {
        hideAllLabels();
        showAllLabels();
    }

    $('#rating_number').change(function() {
        rating_number = $(this).val();
        autoFillLabels(true);
        if(rating_labels == 'custom') {
            toggleLabels();
        }
    });

    $('input[name=likert_labels]').change(function() {
        rating_labels = $(this).val();
        if(rating_labels == 'custom') {
            toggleLabels();
        } else {
            hideAllLabels();
        }
    });

    autoFillLabels();
    hideAllLabels();
    if(rating_labels == 'custom') {
        showAllLabels();
    }

});