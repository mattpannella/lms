/*
	Justin Lloyd
	6/27/2016
*/


jQuery(document).ready(
	function() {

		

		jQuery(document).bind(
			'DOMNodeInserted',
			function(e) {
				var button_area = document.getElementsByClassName('get_current_url_button_area');
				jQuery(button_area).each(
					function(index, value) {
						var this_id = value.id;
						parent = value.parentNode;
						parent.innerHTML = "";

						var button = document.createElement("BUTTON");
						button.className = 'btn button btn-primary get_current_url_button';
						
						button.appendChild(
							document.createTextNode('Use current URL')
						);

						button.id = this_id;
						button.title = "Copy the current url into the text box.";

						parent.appendChild(button);
					}
				);
			}
		);

		jQuery(document).on(
			'click', 
			'.get_current_url_button', 
			function() {
				var which = this.id.split("-")[1];
				
				var textBox = document.getElementById("jform_params_site_url-" + which);
				var host = window.location.host;
				textBox.value = host;
			}
		);
	}
);


