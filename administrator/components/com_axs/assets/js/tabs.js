function openTab(tab) {
	jQuery('.nav-tabs').find('a').each(function() {
		var href = jQuery(this).attr('href');
		if(href == '#'+tab) {
			jQuery(this).trigger('click');
		}
	});
}

jQuery(document).ready(function() {
	jQuery(document).on('click','.steps_btn', function(){
		var tab = jQuery(this).data('tab');
		openTab(tab);
	});
});