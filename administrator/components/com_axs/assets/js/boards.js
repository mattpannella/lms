/*var hideUls = setInterval(function() {
	var uls = document.getElementsByTagName("li");

	console.log()
	for (var i = 0; i < uls.length; i++) {
		console.log(uls[i]);
		//var as = uls[i].getElementsByTagName('a');
		//console.log(as);
	}

}, 1);*/

jQuery(document).ready(
	function() {
		var uls = document.getElementsByTagName("li");

		var tablist = {
			affirmation: {},
			goal: {},
			image: {}
		};

		for (var i = 0; i < uls.length; i++) {
			//console.log(uls[i]);
			var as = uls[i].getElementsByTagName('a');
			if (as) {
				var toggle = as[0].getAttribute("data-bs-toggle");
				if (toggle == 'tab') {
					var href = as[0].getAttribute("href");
					//console.log(href);
					switch (href) {
						case "#affirmation_info":
							tablist.affirmation.main = uls[i];
							break;
						case "#my_affirmation_info":
							tablist.affirmation.my = uls[i];
							break;
						case "#goal_info":
							tablist.goal.main = uls[i];
							break;
						case "#my_goal_info":
							tablist.goal.my = uls[i];
							break;
						case "#image_sharing_info":
							tablist.image.main = uls[i];
							break;
						case "#my_image_sharing_info":
							tablist.image.my = uls[i];
							break;
					}
				}				
			}
		}

		var board_type = jQuery("#board_type").val();
		var tabkeys = Object.keys(tablist);

		var hideTabs = function(board_type) {
			for (var i = 0; i < tabkeys.length; i++) {
				
				var listkeys = Object.keys(tablist[tabkeys[i]]);
				for (var j = 0; j < listkeys.length; j++) {					
					if (tabkeys[i] != board_type) {
						jQuery(tablist[tabkeys[i]][listkeys[j]]).hide();
					} else {
						jQuery(tablist[tabkeys[i]][listkeys[j]]).show();
					}
				}
			}
		}

		hideTabs(board_type);

		//clearInterval(hideUls);
		jQuery("#board_type").change(
			function() {
				hideTabs(this.value);
			}
		);
	}
);