var chartsList = [];
var date_range;
var chartData = new Object();

/*
    Remove the standard Save, Load, etc. Joomla toolbar.
*/

var toolParent = null;

/*
    The kill* functions are for stopping setInterval functions.
*/

var killToolBarCheck = function() {
    clearInterval(deleteToolbar);
};

var killCalendarMove = function() {
    clearInterval(moveCalendar);
};
/*
    Check for the standard toolbar and remove it once it appears.
*/
var deleteToolbar = setInterval(
    function() {
        var toolbar = document.getElementById('toolbar');

        if (toolbar != null) {
            toolParent = toolbar.parentNode;            
            jQuery(toolbar).remove();
            killToolBarCheck();
        }
    }, 10
);

/*
    Wait for the calendar to appear and then move it.
*/
var moveCalendar = setInterval(
    function() {
        var datepicker = document.getElementById('date_range_calendar');
        if (toolParent != null && datepicker != null) {            
            toolParent.appendChild(datepicker);
            datepicker.show();
            killCalendarMove();
        }
    }, 10
);

/*
    Change the title bar to Mega Axs: Dashboard
*/

var killChangeTitle = function() {
    clearInterval(changeTitle);
}

var changeTitle = setInterval(
    function() {
        var title = document.getElementsByClassName("page-title")[0];
        if (title != null) {
            title.innerHTML = "<span class='icon-cog'></span>Dashboard";
            killChangeTitle();
        }
    }, 10
);

/* 
    The graphs don't properly fit on initial rendering but do after a screen resize event. c3js.glitch??? 
    A screen resize fixes the issue, but it must be done immediately or the zoom/drag won't work.
*/

var killResizePage = function() {
    clearInterval(resizePage);
}

var resizePage = setInterval(
    function() {
        //resize_viewing_area();                    
        if (document.getElementById('chartArea') != null) {
            window.dispatchEvent(new Event('resize'));
            killResizePage();
        }
    }, 10
);

(function($) {
    $(document).ready(
        function() {

            var sidebar_min_width = 50;
            var sidebar_max_width = 220;
            var animate_speed = 100;

            var expand = false;
            var shrink = false;
            
            var sidebar_div = document.getElementById('dashboard_sidebar');
            var sidebar = $(sidebar_div);
            var viewing_area_div = document.getElementById('dashboard_view_area');
            var viewing_area = $(viewing_area_div);
           

            /*sidebar.css(
                {
                    "width":    sidebar_max_width + "px",
                    "height":   height
                }
            );*/

            //Animation for expanding the sidebar.
            var expand_sidebar = function() {
                sidebar.animate(
                    { 
                        width: sidebar_max_width + "px" 
                    },
                    {
                        progress: function () {
                            resize_viewing_area();
                        },
                        done: function() {
                            $("#menu_title").show(animate_speed);
                            $(".menu_icon").show(animate_speed);
                            $(".menu_text").show(animate_speed);
                            $("#shrink_sidebar_left").show(animate_speed);
                            $("#dashboard_menu_buttons").show(animate_speed);
                            window.dispatchEvent(new Event('resize'));
                            resize_viewing_area();
                        }
                    }
                );
            }

            //Animation for shrinking the sidebar.
            var shrink_sidebar = function() {
                sidebar.animate(
                    { 
                        width: sidebar_min_width + "px" 
                    },
                    {
                        progress: function () {
                            resize_viewing_area();
                        },
                        done: function() {
                            $("#shrink_sidebar_right").show(animate_speed);
                            $(".menu_icon").show(animate_speed);
                            window.dispatchEvent(new Event('resize'));
                            resize_viewing_area();
                        }
                    }
                );
            }

            //When the sidebar shrinks or expands, resize the main view area.
            var resize_viewing_area = function() {
                var sidebar_width = sidebar.width();
                var screen_width = screen.width;
                var view_width = document.body.clientWidth;

                viewing_area.css(
                    {
                        "position":         "absolute",
                        "left":             sidebar_width,
                        "width":            view_width - sidebar_width,
                        "display":           "inherit" 
                    }
                );

                syncHeight();
            }

            //Call these functions on the first page load.
            resize_viewing_area();
            syncHeight();

            //Check for an ENTER press on the calendar
            $(".enter_calendar").keyup(
                function(e) {
                    if (e.keyCode === 13) {
                        $("#calendar_refresh").click();
                    }
                }
            );

            //Check to see if the shrink sidebar button has been clicked.
            $("#shrink_sidebar_left").click(
                function() {
                    
                    $("#shrink_sidebar_left").hide(animate_speed);
                    $(".menu_icon").hide();
                    $(".menu_text").hide()
                    $("#menu_title").hide();
                    $("#dashboard_menu_buttons").hide();
                    shrink_sidebar();
                }
            );

            //Check to see if the expand sidebar button has been clicked.
            $("#shrink_sidebar_right").click(
                function() {
                    
                    $("#shrink_sidebar_right").hide(animate_speed);
                    $(".menu_icon").hide();
                    $(".menu_text").hide();
                    $("#menu_title").hide();                    
                    expand_sidebar();
                }
            );

            init_main_area();

            //The quick data views at the top of the page are closed by default.  However, their last state is stored in local storage to
            //keep the user's preference.
            $('.totals-stats').click(
                function() {
                    var lower_area = $(this).parents('.stats_bar').find('.totals-lower');
                    var name = "dashboard_" + this.id;
                    var visible = lower_area.is(":visible");
                    
                    localStorage.setItem(name, !visible);

                    if (visible) {
                        document.getElementById(name + "_icon").className = 'lizicon-circle-down';
                        lower_area.hide(500);
                    } else {
                        document.getElementById(name + "_icon").className = 'lizicon-circle-up';
                        lower_area.show(500);
                    }
                }
            );

            //Give the appropriate expand/close icon based on its current setting.
            $(".totals-stats").each(
                function() {
                    var lower_area = $(this).parents('.stats_bar').find('.totals-lower');
                    var name = "dashboard_" + this.id;
                    var visible = localStorage.getItem(name);

                    var expand_icon = document.createElement('span');
                    this.appendChild(expand_icon);
                    expand_icon.id = name + "_icon";

                    if (visible == 'true') {
                        expand_icon.className = 'lizicon-circle-up';
                        lower_area.show(500);
                    } else {
                        expand_icon.className = 'lizicon-circle-down';
                    }
                }
            );

            /* Check if any of the graphs are donut.  If so, center them, since dragging them around is not useful */
            setInterval(
                function() {
                    for (i = 0; i < chartsList.length; i++) {
                        var list = chartsList[i].list;
                        for (key in list) {
                            var chart = list[key].element;
                            var type = chart.graphType;
                            if (type == 'donut') {

                                var parentWidth = jQuery(chart.parentElement).width();
                                var width = jQuery(chart).width();

                                var pos = (parentWidth - width) / 2;

                                jQuery(chart).css({
                                    left: pos + "px"
                                });                                
                            }
                        }
                    }
                }, 1000
            );
        }
    );
}) (jQuery);

/*
    This makes sure that the sidebar and the main area are the same height to keep from looking 'unfinished'
*/

function syncHeight(){

    var menuHeight = jQuery("#dashboard_sidebar").height();
    var viewHeight = jQuery("#dashboard_view_area").height();
    
    if ( viewHeight < menuHeight ) {
    
        jQuery("#dashboard_view_area").css('min-height',menuHeight);

    } else {

        jQuery("#dashboard_sidebar").css('min-height',viewHeight);
    }
}

/*
    The function that's called when a quick view is expanded or closed.
*/

function expand_menu(name, id) {
    var expand_button = jQuery("#menu_expand_button_" + id);
    var hide_button = jQuery("#menu_hide_button_" + id);

    if (expand_button.is(":visible")) {
        jQuery("#" + name).show(200);
        expand_button.hide(100);
        hide_button.show(100);
    } else {
        jQuery("#" + name).hide(200);
        expand_button.show(100);
        hide_button.hide(100);
    }

    syncHeight();
}

/*
    Call the four functions that create the data readouts for the quick views.
*/
function init_main_area() {
    view_current_members();
    view_life_boards();
    view_total_revenue();
    view_new_members();
}

/*
    Parses the data and creates the output for the life boards.
    Will be deprecated in the future when the Board Builder is finished, since the data will no longer be static.
*/

function view_life_boards() {

    var sdate, edate;
    if (start_date == null) {
        sdate = 0;
    } else {
        sdate = start_date;
    }

    if (end_date == null) {
        edate = 0;
    } else {
        edate = end_date;
    }

    var declarationCount    = -1;
    var gratitudeCount      = -1;
    var inspirationCount    = -1;

    var numBoards = 3;

    for (i = 0; i < numBoards; i++) {

        var whichBoard;
        var area;

        switch (i) {
            case 0: whichBoard = 'dec';break;
            case 1: whichBoard = 'grat'; break;
            case 2: whichBoard = 'insp'; break;
        }        

        jQuery.ajax({
            type:       'POST',
            url:        'index.php',
            data:       {
                'option':       'com_axs',
                'task':         'dashboard.getBoards',
                'format':       'raw',
                'which':        whichBoard,
                'numWhich':     i,
                'allData':      0,
                'start':        sdate,
                'end':          edate
            },
            success: function(response) {
                var dat = JSON.parse(response);
                var which = dat.which;
                var count = dat.count;
                var area;

                switch (which) {
                    case '0': 
                        declarationCount = count; 
                        area = "declarationArea";
                        break;
                    case '1': 
                        gratitudeCount = count; 
                        area = "gratitudeArea";
                        break;
                    case '2': 
                        inspirationCount = count; 
                        area = "inspirationArea";
                        break;
                    default:
                        area = "test";
                }
                
                document.getElementById(area).innerHTML = numberWithCommas(count);
            }
        });
    }
    

    var setLifeboardHTML = function() {
        document.getElementById("allLifeBoardsArea").innerHTML = numberWithCommas(declarationCount + gratitudeCount + inspirationCount);
        clearInterval(lifeboardInterval);
    }

    var lifeboardInterval = setInterval(
        function() {
            if (declarationCount >= 0 && gratitudeCount >= 0 && inspirationCount >= 0) {
                setLifeboardHTML();
            }
        }, 10
    );
}

/*
    Parses the data for members and displays the output.
*/
function view_current_members() {

    var today = null;
    var current = 0;
    var grace = 0;
    var affiliate = 0;

    if (daily_data.length != 0) {
        today = daily_data[daily_data.length - 1];

        if (today.membership_current != null) {
            current = today.membership_current.current.count;
            grace = today.membership_current.grace_period.count;
            affiliate = today.membership_current.affiliates.count;   
        }
    }        

    var total_members = current + grace + affiliate;

    document.getElementById("allActiveArea").innerHTML = numberWithCommas(total_members);
    document.getElementById("activeMembersArea").innerHTML = numberWithCommas(current);
    document.getElementById("activeMembersWithGraceArea").innerHTML = numberWithCommas(grace);
    document.getElementById("activeAffiliates").innerHTML = numberWithCommas(affiliate);

}

/*
    Parses the data for revenue and displays the output.
*/

function view_total_revenue() {


    var upgradesTotal = 0;
    var duesTotal = 0;
    var eventsTotal = 0;
    var total = 0;

    for (i = 0; i < daily_data.length; i++) {
        var daily_revenue = daily_data[i].revenue;

        upgradesTotal += parseInt(daily_revenue.upgrades);
        duesTotal += parseInt(daily_revenue.dues);
        eventsTotal += parseInt(daily_revenue.events);
        total += parseInt(daily_revenue.total);
    }

    document.getElementById('eventsRevenueArea').innerHTML = "$" + numberWithCommas(eventsTotal);
    document.getElementById('monthlyDuesRevenueArea').innerHTML = "$" + numberWithCommas(duesTotal);
    document.getElementById('upgradesRevenueArea').innerHTML = "$" + numberWithCommas(upgradesTotal);
    document.getElementById('totalRevenueArea').innerHTML = "$" + numberWithCommas(total);
            
}

var newMemberInfo = null;
var droppedMemberInfo = null;

/*
    Parses the data for new members and displays the output.
*/

function view_new_members() {

    var total = new Array();
    var curMonth = date_range.end.month;

    var monthNum = -1;
    
    var current_month = new Date(daily_data[0].date);
    current_month = current_month.getMonth();
    
    for (i = daily_data.length - 1; i >= 0; i--) {

        var today = daily_data[i];
        var date = today.date;

        var today_month = new Date(date + " 00:00:00");
        today_month = today_month.getMonth();

        if (today_month != current_month) {
            monthNum++;
            current_month = today_month;
        }

        if (total[monthNum] === undefined) {
            total[monthNum] = parseInt(today.members_new.count);
        } else {
            total[monthNum] += parseInt(today.members_new.count);            
        }
    }

    total.forEach(
        function(value, index) {            
            var area;

            area = 'month';
            if (index < 10) {
                area += "0";
            }

            area += index + "NewMemberArea";

            var areaDiv = document.getElementById(area);
            if (areaDiv) {
                areaDiv.innerHTML = value;
            }
        }
    );    
}

/*
    Prepares data for export to CSV.  
    caller - which button disable so that the same report can't be exported multiple times.
    type - which type of data is being received so that it can be parsed correctly.
    data - the actual data itself.
*/

function exportDataToCSV(caller, type, data) {

    if (caller.inUse == true) {
        return;
    } else {
        caller.inUse = true;
    }

    var originalClass = caller.className;
    var originalTitle = caller.title;
    var originalColor = jQuery(caller).css('color');
    var originalFontSize = jQuery(caller).css('font-size');
    var originalPointer = jQuery(caller).css('cursor');

    var restore_caller = function() {
        caller.className = originalClass;
        jQuery(caller).css({
            'color':        originalColor,
            'font-size':    originalFontSize,
            'cursor':       originalPointer
        });

        caller.title = originalTitle;
        caller.inUse = false;
    }

    var empty_list = function() {
        alert("The list contains no data.");
        restore_caller();
    }

    caller.className = originalClass.replace("lizicon-download", "lizicon-hour-glass");
    caller.title = "Downloading. Please wait...";

    jQuery(caller).css({
        "cursor":       'progress'
    });

    var task = null;
    var fileName = null;

    var today = daily_data[daily_data.length - 1];

    var postData = {
        'option':   'com_axs',
        'format':   'raw',
        'days':     '120',
        'allData':  1
    }

    var member_list = null;
    var member_data = null;
    var which_month = null;

    switch (type) {
        case "allMembers":

            if (today.membership_current == null) {
                empty_list();
                return;
            }

            member_data = [];
            member_data[0] = today.membership_current.grace_period.data;
            member_data[1] = today.membership_current.affiliates.data;
            member_data[2] = today.membership_current.current.data;

            task = 'getMemberInfo';
            fileName = "AllMembers";
            break;
        case "activeMembers":

            if (today.membership_current == null) {
                empty_list();
                return;
            }

            member_data = today.membership_current.current.data;
            task = 'getMemberInfo';
            fileName = "ActiveMembers";
            break;
        case "graceMembers":

            if (today.membership_current == null) {
                empty_list();
                return;
            }

            member_data = today.membership_current.grace_period.data;            
            task = 'getMemberInfo';
            fileName = "GracePeriodMembers";
            break;
        case "affiliateMembers":

            if (today.membership_current == null) {
                empty_list();
                return;
            }

            member_data = today.membership_current.affiliates.data; 
            task = 'getMemberInfo';
            fileName = "Affiliates";
            break;
        case "lifeboards":
            task = 'getBoards';
            fileName = 'Lifeboards';
            postData.which = 'all';            
            break;
        case "newMembers":            
            task = 'getMemberInfo';
            fileName = "NewUsers" + data.monthName;
            which_month = data.monthNum;
            member_data = 0;    //Set the member_data so that it is not null

            break;
        default:
            return;
    }

    postData.task = "dashboard." + task;
    postData.start = start_date;
    postData.end = end_date;

    /*
        If member_data is still null, it isn't needed.
    */

    if (member_data != null) {
        member_list = [];

        /*
            If member_data is an object, it is a single list of numbers.
            If member_data has a length, then it is an array of objects that each need to be taken apart.
        */

        if (member_data.length === undefined) {
            // If which_month is null, then the data is already supplied.
            if (which_month == null) {
                for (var key in member_data) {
                    var group = member_data[key];
                    for (i = 0; i < group.length; i++) {
                        member_list.push(group[i]);
                    }
                }
            } else {
                
                for (i = 0; i < daily_data.length; i++) {
                    var date = daily_data[i].date;
                    var month = date.substring(5, 7);
                    if (month == which_month) {
                        var day = daily_data[i].members_new.data;
                        for (j = 0; j < day.length; j++) {
                            member_list.push(day[j]);
                        }
                    }
                }
            }
        } else {
            for (i = 0; i < member_data.length; i++) {
                var temp = member_data[i];
                for (var key in temp) {
                    var group = temp[key];
                    for (j = 0; j < group.length; j++) {
                        member_list.push(group[j]);
                    }
                }
            }
        }

        postData.member_list = member_list.toString();

        if (member_list.length == 0) {
            empty_list();
            return;
        }
    }
    
    var date = new Date();

    fileName += "_" + (date.getMonth() + 1) + "_" + date.getDate() + "_" + date.getFullYear();

    jQuery.ajax({
        type: 'POST',
        url: 'index.php', 
        data: postData,

        success: function(response) {

            var objectData = JSON.parse(response);
            
            //Get the names of the columns

            var data = new Array();
            data.push(new Array());
            
            for (var name in objectData[0]) {
                data[0].push(name);
            }
            
            for (i = 0; i < objectData.length; i++) {
                data.push(new Array());
                var index = data.length - 1;

                for (var key in objectData[i]) {
                    data[index].push(objectData[i][key]);
                }
            }

            exportToCsv(fileName + ".csv", data);
            restore_caller();
        }
    });
}

/*
    Exports the user data to CSV
*/

function export_current_users(caller) {

    var button = document.getElementById(caller.id);

    var currentAction = button.getAttribute('onclick');
    button.setAttribute('onclick', '');
    jQuery(button).css("cursor", "wait");

    jQuery.ajax({
        type: 'POST',
        url: 'index.php',
        data: {
            'option':     'com_axs',
            'task':       'dashboard.exportCurrentUsers',
            'format':     'raw'
        },
        success: function(response) {
            var objectData = JSON.parse(response);
            
            //Get the names of the columns

            var data = new Array();
            data.push(new Array());
            
            for (var name in objectData[0]) {
                data[0].push(name);
            }
            
            for (i = 0; i < objectData.length; i++) {
                data.push(new Array());
                var index = data.length - 1;

                for (var key in objectData[i]) {
                    data[index].push(objectData[i][key]);
                }
            }

            exportToCsv("current_users.csv", data);
            button.setAttribute('onclick', currentAction);
            jQuery(button).css("cursor", "pointer");
        }
    });
}

/*
    A simple function to return the month name in a string.
*/

function getMonthName (num, full) {

    if (full) {
        switch (num) {
            case 1: return "January";
            case 2: return "February";
            case 3: return "March";
            case 4: return "April";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "August";
            case 9: return "September";
            case 10: return "October";
            case 11: return "November";
            case 12: return "December";
        }    
    } else {
        switch (num) {
            case 1: return "Jan";
            case 2: return "Feb";
            case 3: return "Mar";
            case 4: return "Apr";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "Aug";
            case 9: return "Sept";
            case 10: return "Oct";
            case 11: return "Nov";
            case 12: return "Dec";
        } 
    }

    return null;
}

/*
    Cycles through all of the data and creates an array that contains all of the possible dates that exist between
    the start and the end date.
*/

function makeDates() {    
    var today = new Date();

    var make_date = function() {
        var day = today.getDate()
        var month = today.getMonth() + 1;
        var year = today.getFullYear();
        
        var new_date = "" + year + "-";
        if (month < 10) {
            new_date += "0";
        }

        new_date += "" + month + "-";

        if (day < 10) {
            new_date += "0"
        }

        new_date += "" + day;
        return new_date;
    }

    if (start_date == null) {
        start_date = make_date();
    }

    if (end_date == null) {
        end_date = make_date();
    }

    date_range = new Object();
    date_range.start = new Object();
    date_range.end = new Object();


    date_range.start.day = parseInt(start_date.substring(8, 10));
    date_range.start.month = parseInt(start_date.substring(5, 7));
    date_range.start.year = parseInt(start_date.substring(0, 4));

    date_range.end.day = parseInt(end_date.substring(8, 10));
    date_range.end.month = parseInt(end_date.substring(5, 7));
    date_range.end.year = parseInt(end_date.substring(0, 4));
    
    date_range.num_days = 1;
    date_range.num_weeks = 1;
    date_range.num_months = 1;

    date_range.days = new Object();
    date_range.weeks = new Object();
    date_range.months = new Object();

    date_range.days = [];
    date_range.weeks = [];
    date_range.months = [];

    var day, week, month;
    var prev_day, prev_week, prev_month;

    var date_current = new Date(start_date);
    var date_end = new Date(end_date);

    var first = true;

    var getDayName = function(day, month, year) {
        var names = new Object();

        names.month = new Object();
        names.day = new Object();

        names.day.long = getMonthName(month) + " " + day + " " + year.toString().substr(2,4);
        names.day.short = getMonthName(month) + " " + day;

        names.month.long = getMonthName(month) + " " + year.toString().substr(2,4);
        names.month.short = getMonthName(month);

        var monthText = "";
        if (month < 10) {
            monthText = "0" + month;
        } else {
            monthText = month;
        }

        var dayText = "";
        if (day < 10) {
            dayText = "0" + day;
        } else {
            dayText = day;
        }

        names.day.actual = year + "-" + monthText + "-" + dayText;
        names.month.actual = year + "-" + monthText + "-" + dayText;

        return names;
    }

    while (date_current < date_end) {

        day = date_current.getDate();
        week = date_current.getWeek();
        month = date_current.getMonth() + 1;

        var yearDay = date_current.getDay();
        var year = date_current.getFullYear();

        var names = getDayName(day, month, year);

        if (first) {
            first = false;
            prev_day = day;
            prev_week = week;
            prev_month = month;

            date_range.days.push(names.day);
            date_range.weeks.push(names.day);
            date_range.months.push(names.month);
        }

        if (day != prev_day) {
            date_range.num_days++;
            date_range.days.push(names.day);
        }

        if (week != prev_week) {
            date_range.num_weeks++;
            date_range.weeks.push(names.day);
        }

        if (month != prev_month) {
            date_range.num_months++;
            date_range.months.push(names.month);
        }

        if (!first) {
            prev_day = day;
            prev_week = week;
            prev_month = month;
        }

        var newDate = date_current.setDate(date_current.getDate() + 1);
        date_current = new Date(newDate);
    }
}

/*
    Cycles through the entire range of dates selected and attaches it to an
    array index in the daily_data, if one exists.  

    This greatly reduces workload of linking data to dates.
*/

function connectData() {    

    var count = 0;
    for (i = 0; i < date_range.days.length; i++) {
        for (j = 0; j < daily_data.length; j++) {
            var today = daily_data[j].date;
            var compare = date_range.days[i].actual;
            if (today == compare) {
                date_range.days[i].daily_data_num = j;
                break;
            } else {
                date_range.days[i].daily_data_num = -1;
            }
        }
    }
}

/*
    Creates the actual graph box.
    Each graph box has a set of controls at the top for changing the graph type and the courseness (day, week, month).

*/

function makeGraph(text, name, dataArray, width) {    
    var chartArea = document.getElementById('chartArea');
    var numElements = dataArray.length;

    var newGraphBox = document.createElement('DIV');
    newGraphBox.className = 'col-md-' + width;
    newGraphBox.id = name + 'Chart';
    newGraphBox.setAttribute('chartName', text);

    var sliderUserSetting = localStorage.getItem("dashboard_" + name + "ChartSliderBar");
    if (!sliderUserSetting) {
        sliderUserSetting = 2;
    }

    var newGraphMonth = document.createElement('DIV');
    newGraphMonth.id = name + "ChartMonth";    
    if (sliderUserSetting != 2) {
        jQuery(newGraphMonth).hide();
    }

    var newGraphWeek = document.createElement('DIV');
    newGraphWeek.id = name + "ChartWeek";
    if (sliderUserSetting != 1) {
        jQuery(newGraphWeek).hide();
    }

    var newGraphDay = document.createElement('DIV');
    newGraphDay.id = name + "ChartDay";
    if (sliderUserSetting != 0) {
        jQuery(newGraphDay).hide();
    }

    var chartHolder = document.createElement('DIV');
    chartHolder.className = "chart_box";
    newGraphBox.appendChild(chartHolder);   

    jQuery(chartHolder).css({
        'overflow':     'hidden'
    });

    chartHolder.appendChild(newGraphMonth);
    chartHolder.appendChild(newGraphWeek);
    chartHolder.appendChild(newGraphDay);

    chartArea.appendChild(newGraphBox);

    /*
        Establish the data containers
    */

    var dataSet = new Object();

    var dayColumns = [];
    var weekColumns = [];
    var monthColumns = [];

    monthColumns[0] = 'x';
    weekColumns[0] = 'x';
    dayColumns[0] = 'x';

    var show_years = false;
    if (date_range.months.length > 12) {
        show_years = true;
    }

    for (i = 0; i < date_range.months.length; i++) {
        if (show_years) {
            monthColumns.push(date_range.months[i].long);
        } else {
            monthColumns.push(date_range.months[i].short);
        }
    }

    for (i = 0; i < date_range.weeks.length; i++) {
        if (show_years) {
            weekColumns.push(date_range.weeks[i].long);
        } else {
            weekColumns.push(date_range.weeks[i].short);
        }
    }

    for (i = 0; i < date_range.days.length; i++) {
        if (show_years) {
            dayColumns.push(date_range.days[i].long);
        } else {
            dayColumns.push(date_range.days[i].short);
        }
    }

    /*
        Set Base Data
    */

    chartData[name] = new Object();
    
    dataSet.month = [];
    dataSet.week = [];
    dataSet.day = [];

    for (i = 0; i <= numElements; i++) {
        dataSet.month[i] = [];
        dataSet.week[i] = [];
        dataSet.day[i] = [];
    }

    dataSet.day[0] = dayColumns;
    dataSet.week[0] = weekColumns;
    dataSet.month[0] = monthColumns;

    var dataName;
    var columnNames = new Object();

    for (i = 1; i <= numElements; i++) {

        switch (dataArray[i - 1]) {
            case "cc_declines":             dataName = "Credit Card Declines";          break;                        
            case "cc_fails":                dataName = "Credit Card Fails";             break;
            case "cc_recoveries":           dataName = "Credit Card Recoveries";        break;
            case "downgrades":              dataName = "Member Downgrades";             break;
            case "cancels":                 dataName = "Member Cancels";                break;
            case "members_new":             dataName = "New Members";                   break;
            case "members_affiliates":      dataName = "Affiliate Members";             break;
            case "members_current":         dataName = "Current Members";               break;
            case "members_grace_period":    dataName = "Grace Period Members";          break;
            case "reengages":               dataName = "Member Reengages";              break;
            case "refunds":                 dataName = "Refunds";                       break;
            case "revenue_dues":            dataName = "Monthly Dues";                  break;
            case "revenue_events":          dataName = "Events";                        break;
            case "revenue_upgrades":        dataName = "Membership Upgrades";           break;
            case "site_visits":             dataName = "Site Visits";                   break;
        }

        columnNames[dataName] = dataName;

        dataSet.day[i][0] = dataName;
        dataSet.week[i][0] = dataName;
        dataSet.month[i][0] = dataName;

        dataSet.day[i][1] = 0;
        dataSet.week[i][1] = 0;
        dataSet.month[i][1] = 0;
    }

    var monthNum = 1;
    var weekNum = 1;
    var dayNum = 1;
    
    var currentMonth;
    var currentWeek;

    for (i = 0; i < date_range.days.length; i++) {

        var date = new Date(date_range.days[i].actual + " 00:00:00");

        var month = date.getMonth() + 1;
        var week = date.getWeek();
        var day = date.getDate();
        var year = date.getFullYear();

        if (i == 0) {
            currentMonth = month;
            currentWeek = week;
        }

        if (month != currentMonth) {
            currentMonth = month;
            monthNum++;

            for (j = 1; j <= numElements; j++) {
                if (dataSet.month[j][monthNum] === undefined) {
                    dataSet.month[j][monthNum] = 0;
                }
            }
        }

        if (week != currentWeek) {
            currentWeek = week;
            weekNum++;

            for (j = 1; j <= numElements; j++) {
                if (dataSet.week[j][weekNum] === undefined) {
                    dataSet.week[j][weekNum] = 0;
                }
            }
        }

        var num = date_range.days[i].daily_data_num;

        if (num != -1) {

            for (j = 1; j <= numElements; j++) {

                var daily = daily_data[num];
                var membership_current = daily.membership_current;
                var revenue = daily.revenue;

                /* To protect against having to check for null in the switch statement, just create the data */

                if (membership_current == null) {
                    membership_current = {
                        affiliates: { count: 0 },
                        current: { count : 0 },
                        grace_period: { count: 0 }
                    }
                }

                if (revenue == null) {
                    revenue = {
                        dues: 0,
                        events: 0,
                        upgrades: 0
                    }
                }

                var data;

                var whichData = dataArray[j - 1];

                try {
                    switch (whichData) {
                        case "cc_declines":             data = daily.cc_declines.total;                 break;                    
                        case "cc_fails":                data = daily.cc_fails.total;                    break;                    
                        case "cc_recoveries":           data = daily.cc_recoveries.total;               break;
                        case "downgrades":              data = daily.downgrades.count;                  break;
                        case "cancels":                 data = daily.cancels.total;                     break;
                        case "members_new":             data = daily.members_new.count;                 break;
                        case "members_affiliates":      data = membership_current.affiliates.count;     break;
                        case "members_current":         data = membership_current.current.count;        break;
                        case "members_grace_period":    data = membership_current.grace_period.count;   break;
                        case "reengages":               data = daily.reengages.count;                   break;
                        case "refunds":                 data = refunds.total;                           break;
                        case "revenue_dues":            data = revenue.dues;                            break;
                        case "revenue_events":          data = revenue.events;                          break;
                        case "revenue_upgrades":        data = revenue.upgrades;                        break;
                        case "site_visits":             data = daily.site_visits.count;                 break;
                    }
                } catch (err) {                    
                    data = 0;
                }
                /*
                    Date when ALL members were signed up on the new system.  Inaccurate data and throws the graph way off.
                */
                /*if (whichData == 'reengages' && daily.date == '2016-02-29') {                 
                    data = 0;
                }*/
                
                if (
                    whichData == 'members_current' || 
                    whichData == 'members_grace_period' || 
                    whichData == 'members_affiliates'
                    ) 
                {
                    dataSet.month[j][monthNum] = parseInt(data);
                    dataSet.week[j][weekNum] = parseInt(data);
                    dataSet.day[j][dayNum] = parseInt(data);
                } else {
                    dataSet.month[j][monthNum] += parseInt(data);
                    dataSet.week[j][weekNum] += parseInt(data);
                    dataSet.day[j][dayNum] = parseInt(data);
                }                
            }

        } else {
            
            for (j = 1; j <= numElements; j++) {
                dataSet.month[j][monthNum] += 0;
                dataSet.week[j][weekNum] += 0;
                dataSet.day[j][dayNum] = 0;
            }
        }

        dayNum++;
    }

    var numTicks = 8;

    var monthChart = c3.generate({
        bindto: '#' + name + 'ChartMonth',
        data:   {
            x: 'x',
            columns: dataSet.month,
            type: 'area',
            names: columnNames
        },
        axis: {
            x: {
                type: 'category',
                ticks: {
                    count: numTicks
                }
            }
        }, 
        zoom: {
            enabled: true
        }
    });

    var weekChart = c3.generate({
        bindto: '#' + name + 'ChartWeek',
        data:   {
            x: 'x',
            columns: dataSet.week,
            type: 'area',
            names: columnNames
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    count: numTicks
                }
            }
        }, 
        zoom: {
            enabled: true
        }
    });

    var dayChart = c3.generate({
        bindto: '#' + name + 'ChartDay',
        data:   {
            x: 'x',
            columns: dataSet.day,
            type: 'area',
            names: columnNames
        },
        axis: {
            x: {
                type: 'category',
                tick: {
                    count: numTicks
                }
            }
        }, 
        zoom: {
            enabled: true
        }
    });

    chartData[name] = dataSet;

    var chart = new Object();
    chart.name = name;
    chart.list = new Object();
    chart.list.day = dayChart;
    chart.list.week = weekChart;
    chart.list.month = monthChart;

    makeChartControls(chart);
    chartsList.push(chart);
}

/*
    Creates the controls and functionality for each graph
*/

function makeChartControls(chart) {

    var which = chart.name;
    var chartList = chart.list;

    var dayChart = document.getElementById(which + "ChartDay");
    var weekChart = document.getElementById(which + "ChartWeek");
    var monthChart = document.getElementById(which + "ChartMonth");

    function changeGraphType(graphIcon, lineIcon, stepIcon, barIcon, donutIcon, charts) {
        return function() {

            function setChart(chart, type) {
                chart.day.transform(type);
                chart.week.transform(type);
                chart.month.transform(type);

                chart.day.element.graphType = type;
                chart.week.element.graphType = type;
                chart.month.element.graphType = type;

                localStorage.setItem("dashboard_" + which + "Chart_style", type);
            }
            
            jQuery(graphIcon).click(
                function() {
                    setChart(chartList, 'area-spline');
                }
            );

            jQuery(lineIcon).click(
                function() {
                    setChart(chartList, 'area');
                }
            );

            jQuery(stepIcon).click(
                function() {
                    setChart(chartList, 'area-step');
                }
            );

            jQuery(barIcon).click(
                function() {
                    setChart(chartList, 'bar');
                }
            );

            jQuery(donutIcon).click(
                function() {
                    setChart(chartList, 'donut');
                }
            );
        }
    }

    var setSliderAction = function(sliderBar, sliderText) {
        return function() {
            sliderBar.oninput = function() {
                var newText = "";
                var newType = parseInt(sliderBar.value);

                localStorage.setItem("dashboard_" + which + "ChartSliderBar", newType);

                switch (newType) {
                    case 0:
                        dayChart.show();
                        weekChart.hide();
                        monthChart.hide();
                        newText = "Day"; 
                        break;
                    case 1:
                        dayChart.hide();
                        weekChart.show();
                        monthChart.hide();
                        newText = "Week"; 
                        break;
                    case 2:
                        dayChart.hide();
                        weekChart.hide();
                        monthChart.show(); 
                        newText = "Month"; 
                        break;
                }
                
                sliderText.innerHTML = newText;
            }
        }
    }

    /*
        Create an object to store the charts in.
    */

    var charts = new Object();

    charts.main = document.getElementById(which + "Chart");             //The holder for the 3 charts.
    charts.month = document.getElementById(which + "ChartMonth");       //Month chart
    charts.week = document.getElementById(which + "ChartWeek");         //Week chart
    charts.day = document.getElementById(which + "ChartDay");           //Day chart

    //Grab the container that holds the main chart area.
    var chartParent = charts.main.parentElement;

    /*
        Create a new wrapper div for the chart area, as well as a div for the control bar
    */

    var collapsibleWrapper = document.createElement("DIV");
    var controlBar = document.createElement("DIV");

    /*
        Place the control bar and the chart area inside the wrapper.
        Place the wrapper in the parent container.
    */

    collapsibleWrapper.appendChild(controlBar);
    collapsibleWrapper.appendChild(charts.main);
    chartParent.appendChild(collapsibleWrapper);

    collapsibleWrapper.className = charts.main.className;
    charts.main.className = "";

    var text = document.createTextNode(charts.main.getAttribute("chartName"));
    var textBox = document.createElement('SPAN');
    textBox.appendChild(text);

    textBox.className += " dashboard_chart_textBox";
    controlBar.className += " dashboard_chart_bar";
    controlBar.appendChild(textBox);

    /*
        Create the icons for the different styles of graphs.
    */

    var graphIcon = document.createElement('SPAN');
    graphIcon.className = "control_bar_button lizicon-spline-graph";
    graphIcon.title = "Spline Graph";

    var lineIcon = document.createElement('SPAN');
    lineIcon.className = "control_bar_button lizicon-stats-dots";
    lineIcon.title = "Line Graph";

    var stepIcon = document.createElement('SPAN');
    stepIcon.className = "control_bar_button lizicon-step-graph";
    stepIcon.title = "Step Graph";

    var barIcon = document.createElement('SPAN');
    barIcon.className = "control_bar_button lizicon-stats-bars";
    barIcon.title = "Bar Graph";

    var donutIcon = document.createElement('SPAN');
    donutIcon.className = "control_bar_button lizicon-pie-chart";
    donutIcon.title = "Donut Graph";

    /*
        Make the buttons operational.
    */

    changeGraphType(graphIcon, lineIcon, stepIcon, barIcon, donutIcon, charts)();

    var buttonBlock = document.createElement('SPAN');

    var sliderBarBox = document.createElement('SPAN');
    var sliderBar = document.createElement('INPUT');
    
    sliderBar.type = "range";
    sliderBar.min = 0;
    sliderBar.max = 2;

    var sliderUserSetting = localStorage.getItem("dashboard_" + which + "ChartSliderBar");
    var sliderValue;
    if (sliderUserSetting) {        
        sliderValue = parseInt(sliderUserSetting);
    } else {
        sliderValue = 2;            
    }
    sliderBar.value = sliderValue;
    
    var settingText = "test";
    switch (sliderValue) {
        case 0: settingText = "Day"; break;
        case 1: settingText = "Week"; break;
        case 2: settingText = "Month"; break;
    }
    
    sliderBar.className = "dashboard_graph_slider";

    var sliderText = document.createElement('SPAN');
    sliderText.appendChild(document.createTextNode(settingText));
    sliderText.className = "dashboard_graph_slider_text";

    sliderBarBox.appendChild(sliderBar);
    sliderBarBox.appendChild(sliderText);

    setSliderAction(sliderBar, sliderText)();

    buttonBlock.appendChild(graphIcon);
    buttonBlock.appendChild(lineIcon);
    buttonBlock.appendChild(stepIcon);
    buttonBlock.appendChild(barIcon);
    buttonBlock.appendChild(donutIcon);
    buttonBlock.appendChild(sliderBarBox);

    buttonBlock.className = "control_bar_button_block";

    controlBar.appendChild(buttonBlock);

    for (var key in chartList) {
        var userSetting = localStorage.getItem("dashboard_" + which + "Chart_style");
        if (userSetting) {
            chartList[key].transform(userSetting);
            chartList[key].element.graphType = userSetting;
        }
    }    
}
/*
    Returns a number with commas inserted.
    12345 will be returned as 12,345
*/
function numberWithCommas(x) {
    var string;

    try {
        string = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } catch(err) {
        string = x;
    }
    
    return string;
}

/*
    Returns an object containing the day,month, and year or the date given.
*/
function parseDate(date) {
    var year = date.substring(0, 4);
    var month = date.substring(5, 7);
    var day = date.substring(8, 10);

    var newDate = { 
        year:       year, 
        month:      month, 
        day:        day 
    };    

    return newDate;
}

function redirect(location) {
    window.location = location;
}