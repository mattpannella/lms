/**
 * Created by mar on 4/4/16.
 */
(function($){
    $(function() {
        $('body').append($('#myModal')); //needed to fix the bug where modal pops up below background

        //######################################### For deleting code ########################################
        $(document).on('click', '.deleteCode', function() {
            var uid = this.dataset.user;
            var code_id = this.dataset.code;
            var row = $(this).parent().parent();

            if (confirm('Are you sure you want to delete this referral code?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.deleteCode&format=raw',
                    data: {
                        uid: uid,
                        code_id: code_id
                    },
                    type: 'post'
                }).done(function (result) {
                    if (result == 'success') {
                        alert('Code deleted successfully!');
                        row.remove();
                    } else {
                        alert('There was an error deleting your code.');
                    }
                }).fail(function () {
                    alert("An Error occurred.");
                });
            }
        });

        //######################################### For Adding a new code ########################################
        $(document).on('click', '.addCode', function() {
            alert('test1');
            $('#myModalLabel').text('Add New Code');
            var clone = $('#saveCodeForm').clone().addClass('saveCodeFormClone');
            $('#myModalBody').html(clone);
            $('.saveCodeFormClone').show();

            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success saveCode').text('Save');
        });

        $(document).on('keyup', '.saveCodeFormClone input[name=code]', function() {
            alert('test2');
            var group = $('.saveCodeFormClone .code-group');
            var message = $('.saveCodeFormClone .code-message');
            var value =  this.value;

            if(value.length < 4) {
                group.removeClass('has-success').addClass('has-error');
                message.text('Must be four characters or more.');
            } else if(!isNaN(value)) {
                group.removeClass('has-success').addClass('has-error');
                message.text('Codes can not be numbers.');
            } else {
                group.removeClass('has-error').addClass('has-success');
                message.text('');

                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.checkCode&format=raw',
                    data: {
                        code: value
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result != 'success') {
                        group.addClass('has-error');
                        message.text('This referral code is already taken.');
                    }
                }).fail(function() {
                    alert('There was an error trying to check your code.');
                });
            }
        });

        $(document).on('click', '.saveCode', function() {
            var uid = $('.saveCodeFormClone').data('user');
            var code = $('.saveCodeFormClone input[name=code]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.saveCode&format=raw',
                data: {
                    uid: uid,
                    code: code
                },
                type: 'post'
            }).done(function(result) {
                if(result == 'success') {
                    alert('Code added successfully.');
                    location.reload();
                } else {
                    alert('There was an error trying to add your code, ' + result);
                }
            }).fail(function() {
                alert('There was an error trying to add your code.');
            });
        });

        //######################################### For removing a referee ########################################
        $(document).on('click', '.removeReferee', function() {
            var uid = this.dataset.user;
            if(confirm('Are you sure you want to remove this user from their referees?')) {
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.removeReferee&format=raw',
                    data: {
                        uid: uid
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if(result == 'success') {
                        alert('User removed successfully.');
                        location.reload();
                    } else {
                        alert('There was an error.');
                    }
                }).fail(function() {
                    alert('There was an error.');
                });
            }
        });

        //######################################### For moving a referee ########################################
        $(document).on('click', '.moveReferee', function() {
            var uid = this.dataset.user;

            $('#myModalLabel').text('Move user under someone else');
            var clone = $('#moveReferreeForm').clone().addClass('moveReferreeFormClone').data('user', uid);
            $('#myModalBody').html(clone);
            $('.moveReferreeFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success moveRefereeSave').text('Save').prop('disabled', true);
        });

        $(document).on('click', '.moveRefereeSave', function() {
            var uid = $('.moveReferreeFormClone').data('user');
            var pid = $('.moveReferreeFormClone input[name=parent]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.moveReferee&format=raw',
                data: {
                    uid: uid,
                    pid: pid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert('User moved successfully.');
                    location.reload();
                } else {
                    alert('There was an error.');
                }
            }).fail(function() {
                alert('There was an error.');
            });
        });

        //######################################### For adding a referee ########################################
        $(document).on('click', '.addReferee', function() {
            $('#myModalLabel').text('Add a referee under this user');
            var clone = $('#addReferreeForm').clone().addClass('addReferreeFormClone');
            $('#myModalBody').html(clone);
            $('.addReferreeFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success addRefereeSave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.addRefereeSave', function() {
            var uid = $('.addReferreeFormClone').data('user');
            var cid = $('.addReferreeFormClone input[name=child]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.addReferee&format=raw',
                data: {
                    uid: uid,
                    cid: cid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert('User added successfully.');
                    location.reload();
                } else {
                    alert('There was an error.');
                }
            }).fail(function() {
                alert('There was an error.');
            });
        });

        //######################################### For changing a referrer ########################################
        $(document).on('click', '.changeReferrer', function() {
            $('#myModalLabel').text('Change this users referrer');
            var clone = $('#changeReferrerForm').clone().addClass('changeReferrerFormClone');
            $('#myModalBody').html(clone);
            clone.show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success changeReferrerSave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.changeReferrerSave', function() {
            var uid = $('.changeReferrerFormClone').data('user');
            var pid = $('.changeReferrerFormClone input[name=parent]').val();

            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.changeReferrer&format=raw',
                data: {
                    uid: uid,
                    pid: pid
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert('User parent changed successfully.');
                    location.reload();
                } else {
                    alert('There was an error.');
                }
            }).fail(function() {
                alert('There was an error.');
            });
        });

        //############################## Field Validation and Form Submit redirection ##############################
        $(document).on('keyup', '.changeReferrerFormClone, .addReferreeFormClone, .moveReferreeFormClone, .addFamilyFormClone', function(e) {
            console.log('calling validate move on keyup');
            setTimer(this);
        });

        $(document).on('keypress', '.changeReferrerFormClone, .addReferreeFormClone, .moveReferreeFormClone, .addFamilyFormClone', function(e) {
            //console.log('Pressed a key: ' + e.which);
            if(e.which == 13){
                e.preventDefault();
                $(this).parents('.modal-content').find('.modal-footer .btn-success').click();
            }
        });

        var timer = null;

        function setTimer(sourceEl) {
            //kill last timer and start new timer
            if(timer) {
                clearTimeout(timer);
            }
            timer = setTimeout(function() {
                validateMove(sourceEl);
            }, 500);
        }

        function validateMove(sourceEl) {
            console.log('validateMove() was called');
            var modalEl = $(sourceEl).parents('.modal-content');
            var inputGroupEl = modalEl.find('.form-group');
            var inputEl = modalEl.find('input');
            var messageEl = modalEl.find('.code-message');
            var saveEl = modalEl.find('.modal-footer .btn-success');
            var inputName = inputEl.attr('name');
            var inputValue = inputEl.val();
            var canBeZero = false;
            switch(inputName) {
                case 'parent':
                    canBeZero = true;
                    break;
                case 'child':
                default:
                    canBeZero = false;
                    break;
            }

            saveEl.prop('disabled', true);

            if (inputValue === '') {
                inputGroupEl.removeClass('has-success').addClass('has-error');
                messageEl.text('User does not exist');
                return false;
            } else if (parseInt(inputValue) === 0) {
                if (canBeZero) {
                    messageEl.text('Set User to have no referrer.');
                    inputGroupEl.removeClass('has-error').addClass('has-success');
                    saveEl.prop('disabled', false);
                    return true;
                } else {
                    inputGroupEl.removeClass('has-success').addClass('has-error');
                    messageEl.text('User can not be zero.');
                    return false;
                }
            } else {
                //run the ajax to check if it's a real person and update the message text
                $.ajax({
                    url: 'index.php?option=com_axs&task=affiliates.checkUser&format=raw&uid='+inputValue,
                    type: 'get'
                }).done(function(result) {
                    //console.log(result);
                    if(result != 'error') {
                        messageEl.text(result);
                        inputGroupEl.removeClass('has-error').addClass('has-success');
                        saveEl.prop('disabled', false);
                        return true;
                    } else {
                        messageEl.text('User does not exist');
                        inputGroupEl.removeClass('has-success').addClass('has-error');
                        saveEl.prop('disabled', true);
                        return false;
                    }
                }).fail(function() {
                    alert('Something went wrong while checking for user, refresh and try again or contact tech support.');
                    return false;
                });
            }
        }

        //######################################### For adding a family member ########################################
        $(document).on('click', '.addFamily', function() {
            $('#myModalLabel').text('Add a family member under this user');
            var clone = $('#addFamilyForm').clone().addClass('addFamilyFormClone');
            $('#myModalBody').html(clone);
            $('.addFamilyFormClone').show();
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success addFamilySave').text('Add').prop('disabled', true);
        });

        $(document).on('click', '.addFamilySave', function() {
            $(this).prop('disabled', true);
            var uid = $('.addFamilyFormClone').data('user');
            var fid = $('.addFamilyFormClone input[name=family]').val();
            var rel = $('.addFamilyFormClone select[name=relationship]').val();
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.addFamily&format=raw',
                data: {
                    uid: uid,
                    fid: fid,
                    rel: rel
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert('User added successfully.');
                    location.reload();
                } else {
                    alert('There was an error.');
                }
            }).fail(function() {
                alert('There was an error.');
            });
        });

        //######################################### For loading more rewards ########################################
        $(document).on('click', '#loadMoreRewards', function() {
            $(this).prop('disabled', true);
            $(this).html('<img src="../images/ajax-loader.gif" /> Load More');
            var button = $(this);
            var uid = $(this).data('user');
            var page = $(this).data('page');
            $.ajax({
                url: 'index.php?option=com_axs&task=affiliates.loadMoreRewards&format=raw',
                data: {
                    uid: uid,
                    page: page
                },
                type: 'post'
            }).done(function(result) {
                $(result).insertBefore(button.parent().parent());
                button.html('Load More');
                button.prop('disabled', false);
                button.data('page', page+1);
            }).fail(function() {
                alert('There was an error fetching your rewards.');
                button.html('Load More');
                button.prop('disabled', false);
            });
        });
    });
}(jQuery));