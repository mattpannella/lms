var whichGoalTypeSelected;
window.onload = function() {

	var checkWhichToShow = function(which) {
		whichGoalTypeSelected = document.querySelector('input[name=goal_type]:checked').value;

		var inputs = document.getElementById("progress_type").getElementsByTagName('label');
		jQuery(inputs).each(
			function(index, which) {
				var owner = which.getAttribute('for');
				switch(owner) {
					case "progress_type0":
					case "progress_type1":
						if (whichGoalTypeSelected == "goal") {
							jQuery(which).show();
						} else {
							jQuery(which).hide();
						}
						break;
					case "progress_type2":
					case "progress_type3":
						if (whichGoalTypeSelected == "declaration") {
							jQuery(which).show();
						} else {
							jQuery(which).hide();
						}
						break;
				}
			}
		);
	}

	checkWhichToShow();

	jQuery("#goal_type").change(
		function() {
			checkWhichToShow();
		}
	);
}