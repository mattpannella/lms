/*
	Justin Lloyd
	6/27/2016
*/

jQuery(document).ready(
	function() {
		const appendTextInputInitializeHandlers = function(self) {
			// set the width of the slider so that its width
			// plus the width of the input box are equal to
			// the width of other form inputs on the page
			self.style = "width:228px";

			// create the text input
			var inputBox = document.createElement("INPUT");
			inputBox.setAttribute("type", "text");
			inputBox.className = "float-start";
			inputBox.style = "width:66px;margin-left:10px;"

			// initialize text input value
			inputBox.value = self.value + '%';

			// when the slider is slid, update the text input value
			self.oninput = function() {
				inputBox.value = self.value + '%';
			}

			// whenever the input loses focus, or the enter
			// key is pressed while selected, update the
			// value of the slider and ensure that the
			// % sign is on the end of the input value
			const applyChangesFromInput = () => {
				inputBox.value = 
					inputBox.value.indexOf('%') != -1 
						? inputBox.value 
						: inputBox.value + '%';

				self.value = parseInt(inputBox.value);
			};
			inputBox.onfocusout = () => applyChangesFromInput();
			inputBox.onkeyup = (e) => {
				if (e.key === 'Enter' || e.keyCode === 13) {
					applyChangesFromInput();
				}
			}

			// create a container for the text input and range
			// input and append to the page
			self.after(inputBox);

			// add an empty div with clearfix in case self slider
			// has a help-block
			let clearfix = document.createElement("div");
			clearfix.className = "clearfix";
			inputBox.after(clearfix);
		}

		jQuery('.slider').each(function () { 
			appendTextInputInitializeHandlers(this);
		});

		jQuery(document).on('subform-row-add', function(ev, row) {
			jQuery(row).find('.slider').each((idx, el) => {
				appendTextInputInitializeHandlers(el);
			});
		});

	}
);