window.addEventListener('load',
	function() {

		var icon_buttons = null;

		function makeIconButtonClick(which) {
			jQuery(which).click(
				function() {
					var icon_area = which.parentNode.getElementsByClassName('icon_area')[0];
					var visible = jQuery(icon_area).is(":visible");

					if (visible) {
						jQuery(icon_area).slideToggle(300);
						which.innerHTML = "Change";
					} else {
						jQuery(icon_area).slideToggle(300);
						which.innerHTML = "Close";
					}					
				}
			);				
		}

		function makeIconBoxClick(which) {

			var icon_area = which.parentNode.getElementsByClassName('icon_area')[0];
			var icon_boxes = icon_area.getElementsByClassName('icon_box');

			var container = icon_area.parentNode;

			jQuery(icon_boxes).each(
				function() {
					jQuery(this).click(
						function() {
							
							var name = this.getAttribute('icon_name');
							var final_icon_container = container.getElementsByClassName('icon_preview')[0];
							var input_field = container.getElementsByClassName('icon_input_field')[0];							

							final_icon_container.className = "icon_preview lizicon-" + name;
							input_field.setAttribute('value', 'lizicon-' + name);
							jQuery(this).parent().parent().find('.select_icon_button').text('Change');
							jQuery(this).parent('.icon_area').slideToggle(100);
						}
					);
				}
			);
		}

		function setIcon(which) {
			
			var container = which.parentNode;			
			var icon_type = container.getElementsByClassName("icon_input_field")[0].value;
			var preview = container.getElementsByClassName("icon_preview")[0];
			preview.className += " " + icon_type;
			
		}

		setInterval(
			function() {
				icon_buttons = document.getElementsByClassName('select_icon_button');
				jQuery(icon_buttons).each(
					function() {
						if (!this.getAttribute('clickable')) {
							makeIconBoxClick(this);
							makeIconButtonClick(this);
							setIcon(this);
							this.setAttribute('clickable', true);
						}
					}
				);

			}, 250
		);

		jQuery(document).on("click",".empty_icon_button",
			function() {
				var parent = this.parentNode;
				
				var save_area = parent.getElementsByClassName("icon_input_field")[0];
				var preview_area = parent.getElementsByClassName("icon_preview")[0];
				
				save_area.setAttribute("value", null);
				preview_area.className = "icon_preview";
			}
		);
	}
);