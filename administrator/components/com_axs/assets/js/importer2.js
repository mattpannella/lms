$(document).ready(function() {
	$(document).on('click', '.importCSV', function(){
		var templateId = $(this).data('id');
		const modal = document.getElementById("importCSVmodal");
        const bsModal = new bootstrap.Modal(modal, {})
        bsModal.show();
		$('#template_id').val(templateId);
		resetImporter();
		$.ajax({
			url  : '/index.php?option=com_axs&task=importer.getTemplateNameById&format=raw',
			type : 'POST',
			data : {
				id: templateId
			},
		}).done((response) => {
			console.log(response);
			const result = JSON.parse(response);
			$('#selectedImportTemplate').text(result.name);
		});
	});

	$(document).on('click','.input-file-trigger',function(){
		$('.input-file').trigger('click');
	});

	var fileInput  = document.querySelector( ".input-file" ),
	    button     = document.querySelector( ".input-file-trigger" ),
	    the_return = document.querySelector(".file-return");

	button.addEventListener("keydown", function( event ) {
	    if ( event.keyCode == 13 || event.keyCode == 32 ) {
	        fileInput.focus();
	    }
	});
	button.addEventListener("click", function( event ) {
	   fileInput.focus();
	   return false;
	});
	fileInput.addEventListener("change", function( event ) {
	    the_return.innerHTML = this.files[0].name;
	    $('.uploadBtn').fadeIn(300);
		$('.input-file-trigger').removeClass('btn-primary').addClass('btn-secondary');
	});
 
	function setProgressBarPercentage(percentage) {
		$('#importProgress').children().first().css('width', percentage + '%');
		$('#importProgress').children().first().prop('aria-valuenow', percentage);
	}

	function updateImportLog(text) {
		var importLog = $("#importLog");
		const scrollHeightBefore = importLog[0].scrollHeight;
		importLog.append(`<br/>` + text);

		
		const scrolledToBottom = importLog[0].scrollHeight - importLog.scrollTop() - importLog.outerHeight() <= 2400;
		const neverScrolled = importLog.scrollTop() == 0 && scrollHeightBefore == 500;
		if (scrolledToBottom || neverScrolled) {
			importLog.scrollTop(importLog[0].scrollHeight);
		}
	}

	function saveImportLog() {
		const text = $('#importLog').html().replaceAll('<br/>', '\n').replaceAll('<br>', '\n');
		const filename = `Import_Log_${(new Date().toJSON().slice(0,10))}.txt`;
		download(filename, text);
	}

	function download(filename, text) {
		var element = document.createElement('a');
		element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
		element.setAttribute('download', filename);
	  
		element.style.display = 'none';
		document.body.appendChild(element);
	  
		element.click();
	  
		document.body.removeChild(element);
	}

	function resetImporter() {
		inProgress = false;
		importCancelling = false;

		// Reset cancel button
		$('#cancelImportButton').html(`Cancel`);
		$('#cancelImportButton')
			.removeClass('btn-primary').removeClass('btn-secondary')
			.addClass('btn-danger');
		$('#cancelImportButton').hide();

		// Reset start button
		$('#startImportButton').html(`Start Import`);
		$('#startImportButton').removeClass('btn-secondary').addClass('btn-primary');
		$('#startImportButton').off('click');
		$('#startImportButton').click(function(){
			uploadCSVFile();
		});
		$('#startImportButton').hide();
		
		// Reset Import Log and Progress Bar
		$('#importLog').empty();
		$('#saveImportLogButton').hide();
		setProgressBarPercentage(0);
		$('#importProgressContainer').hide();
		
		// Reset File Upload
		document.getElementById('csv_file').value = null;
		$('.file-return').empty();
		$('.input-file-trigger').removeClass('btn-secondary').addClass('btn-primary');
		$('#selectFileButton').show();
	}

	var importCancelling = false;
	var inProgress = false;
	var filepath = null;
	function cancelImport() {
		if (!importCancelling && inProgress) {
			importCancelling = true;
			$('#startImportButton').removeClass('btn-primary').addClass('btn-secondary');
			$('#startImportButton').html(`<span class="spinner-border spinner-grow-sm" role="status" aria-hidden="true"></span> Cancelling`);
		} else {
			$.ajax({
				url  : '/index.php?option=com_axs&task=importer.cancelImport&format=raw',
				type : 'POST',
				data : {
					filepath
				},
			}).done((response) => {
				var result = JSON.parse(response);
				// console.log(result);
				if (result.success) {
					resetImporter();
				} else {
					console.error('Import Cancellation Failed');
					console.error(result.error);
					console.error(result.message);
				}
			});
		}
	}

	function processCSVData(filepath, pageData) {
		inProgress = true;
		if (importCancelling) {
			cancelImport();
			return;
		}
		// Start processing the thing
		$.ajax({
			url  : '/index.php?option=com_axs&task=importer.processCSVFile&format=raw',
			type : 'POST',
			data : {
				filepath,
				pageData,
				template_id: $('#template_id').val()
			},
		}).done((response) => {
			var result = JSON.parse(response);
			// console.log(result);
			updateImportLog(result.message);
			if (result.success) {
				// Start processing the thing
				$('#startImportButton').html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Importing... <span id="userCount">${result.pageData.cursor}</span>/${result.pageData.totalLines}`);
				pageData = result.pageData;
				setProgressBarPercentage((parseInt(pageData.cursor) / parseInt(pageData.totalLines)) * 100)
				if (!pageData.done || pageData.done == 'false') {
					processCSVData(result.filepath, pageData);
				} else {
					importCancelling = true;
					$('#cancelImportButton').html(`Restart`);
					$('#cancelImportButton').removeClass('btn-danger').addClass('btn-primary');
					setProgressBarPercentage(100);
					updateImportLog('Done');
					$('#startImportButton').html(`Import Finished`);
					$('#startImportButton').removeClass('btn-primary').addClass('btn-secondary');
				}
			}
		})
	}

	function uploadCSVFile() {
		var data = new FormData();
		data.append('csv_file', document.getElementById('csv_file').files[0]);
		data.append('template_id', $('#template_id').val());

		$('#startImportButton').html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Uploading File...`);

		$('#importProgressContainer').show();
		$('#saveImportLogButton').show();
		$('#cancelImportButton').show();
		$('#selectFileButton').hide();

		updateImportLog('Template id: ' + $('#template_id').val());
		updateImportLog('Template name: ' + $('#selectedImportTemplate').text());

		$.ajax({
			url  : '/index.php?option=com_axs&task=importer.uploadCSVFile&format=raw',
			type : 'POST',
			data : data,
			enctype: 'multipart/form-data',
			processData: false,
			contentType: false,
			cache: false
		}).done(function(response) {
			var result = JSON.parse(response);
			// console.log(result);
			updateImportLog(result.message);
			let pageData = {
				cursor: 0,
				// TODO add number input to configure page size
				pageSize: 100,
				done: false
			};
			filepath = result.filepath;
			if (result.success) {
				$('#startImportButton').html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Importing...`);
				processCSVData(result.filepath, pageData);
			} else {
				$('#startImportButton').html(`Continue`);
				$('#startImportButton').off('click');
				$('#startImportButton').click(function(){
					$('#startImportButton').html(`<span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span> Importing...`);
					processCSVData(result.filepath, pageData);
				})
				updateImportLog('Validation Error. See output above. If you would like to continue anyways, click the Continue button below.');
			}
		})
	}

	$('#startImportButton').click(function(){
		uploadCSVFile();
	});

	$('#saveImportLogButton').click(function(){
		saveImportLog();
	});

	$('#cancelImportButton').click(function(){
		cancelImport();
	});

});
