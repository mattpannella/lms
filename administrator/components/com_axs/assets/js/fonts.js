jQuery(document).ready(

	function() {
		jQuery(".fonts > option").each(function() {
    		jQuery(this).attr('style','font-family:'+this.value);
		});
		
		function fontPreview(fontTag,fontName) {

			jQuery('#font-details').prepend(fontTag);
			jQuery('.font_name').css('font-family', fontName);
		}

		jQuery('.font_link').css('width',700);

		function getUrlVars(link) {

		    var vars = [], hash;
		    var hashes = link.slice(link.indexOf('?') + 1).split('&');
		    for(var i = 0; i < hashes.length; i++) {

		        hash = hashes[i].split('=');
		        vars.push(hash[0]);
		        vars[hash[0]] = hash[1];
		    }
		    return vars;
		}

		var btn = '<span class="btn btn-primary import-font" style="margin-left:15px;">Import Font</span>';
		jQuery(btn).insertAfter('.font_link');
		var fontPreviewTag = jQuery('.font_link').val();
		var fontPreviewName = jQuery('.font_name').val();
		
		if(fontPreviewTag != '') {
			fontPreview(fontPreviewTag,fontPreviewName);
			
		}

		
		jQuery(document).on(
			'click', 
			'.import-font', 
			function() {
				
				var fontTag = jQuery('.font_link').val();
				var fontLink = fontTag.match(/'([^']+)'/);
				if(!fontLink) {
					fontLink = fontTag.match(/"([^"]+)"/);
				}
				var font = getUrlVars(fontLink[1]);
				var fontName = font["family"].replace(new RegExp("\\+","g"),' ').split(':');;
				jQuery('.font_name').val(fontName[0]);
				fontPreview(fontTag,fontName[0]); 
				


				
			}
		);
	}
);