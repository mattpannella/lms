function startImport(status) {
    if(status) {
        jQuery('.presentation_wrapper').hide();
        jQuery('.import_message').fadeIn(300);
    } else {
        jQuery('.import_message').hide();
    }

}

window.addEventListener("message", receiveMessage, false);
function receiveMessage(response) {
    if(response.data.action == 'resize') {
        console.log('Presentation Loaded');
        jQuery('.import_message').hide();
        jQuery('.presentation_items').css('visibility','inherit');
    }

}

function getfileData(response) {
    var array = response.presentation;
    var url = '/administrator/index.php?option=com_interactivecontent&task=interactivecontent.apply_interactive_content&response=json';
    jQuery.ajax({
        url  : url,
        data : array,
        type : 'post',
    }).done(function(response) {
        console.log( "complete" );
        console.log('Presentation was built')
        console.log(response);
        if(response) {
            var result = JSON.parse(response);
            var interactive_id = result.id;
            var encrypted_token = result.encrypted_token;
            var editButton ='<a href="/administrator/index.php?option=com_interactivecontent&view=new&id='+interactive_id+'" data-toggle="modal" class="design-key presentation_items" style="float: right; visibility: hidden;"> <i class="fa fa-pencil"></i> Edit Presentation</a>';
            var frame ='<iframe src="/?tmpl=ic_embed&embed_id='+interactive_id+'&token='+encrypted_token+'" style="width: 100%; margin-top:10px; visibility: hidden;" class="presentation_items" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
            jQuery('.presentation_wrapper').html(editButton+frame);
            jQuery('.presentation_wrapper').show();
        }
    });
}