var kendoOptions = {
    sortable:   true,
    resizable:  true,
    columnMenu: true,
    filterable: {
        mode: "row"
    },
    columns: [                    
        {
            field: "File",
            title: "File",
            filterable: {
                cell: {
                    suggestionOperator: "contains"
                },
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
           
        },
        {
            field: "User",
            title: "User",
            filterable: {
                cell: {
                    suggestionOperator: "contains"
                },
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
           
        },                
        {
            field: "Date",
            title: "Date Added",
            type: "date",
            format: "{0:MM/d/yyyy}",
            filterable: {
                ui: "datepicker"
            }
        },
        {
            field: "Action",
            filterable: false,
            width: "100px"               
        }
    ]
}

var fileLocker = jQuery("#fileLocker").kendoGrid(kendoOptions);
var grid = fileLocker.data("kendoGrid");
function getfileData(response) {
    var trash = '<span class="fa fa-trash delete_button" data-file="'+response.file+'"></span>';
    grid.dataSource.add( { File: response.file, User: response.user, Date: response.date, Action: trash } );
}         
jQuery(document).ready(function() {
    jQuery(document).on('click','.delete_button',function(){
        var file = jQuery(this).data('file');
        var row = jQuery(this).closest('tr');
        var c = confirm("Are you sure you wan to delete the file: "+file);
        if(c) {
            var url = '/index.php?option=com_axs&task=securedfiles.deleteFile&format=raw';
            jQuery.ajax({
                url  : url,
                data : {
                    'file' : file
                },
                type : 'post'
            }).done( function(response) {
                var result = JSON.parse(response);
                if(result) {
                    var message = result.message;
                    if(result.status == 'success') {
                        var message = result.message;
                        alert(message);
                        grid.removeRow(row);
                    } else {
                        alert(message);
                    }
                }
            });
        }        
    })   
});  