var presetList = [
	{
		"name"   : "",
		"header" : "#ffffff",
		"text"   : "#555555",
		"footer" : "#143240",
		"navText": "#ffffff",
		"hover"  : "#ea8936"
	},

	{
		"name"   : "",
		"header" : "#ffffff",
		"text"   : "#777777",
		"footer" : "#06568f",
		"navText": "#cccccc",
		"hover"  : "#0a8dff"
	},

	{
		"name"   : "",
		"header" : "#143240",
		"text"   : "#ffffff",
		"footer" : "#0a1a21",
		"navText": "#ffffff",
		"hover"  : "#ea8936"
	},

	{
		"name"   : "",
		"header" : "#111111",
		"text"   : "#cccccc",
		"footer" : "#333333",
		"navText": "#ffffff",
		"hover"  : "#066094"
	},

	{
		"name"   : "",
		"header" : "#e23611",
		"text"   : "#ffffff",
		"footer" : "#424141",
		"navText": "#ffffff",
		"hover"  : "#999999"
	},

	{
		"name"   : "",
		"header" : "#333D51",
		"text"   : "#E5E5E5",
		"footer" : "#57648C",
		"navText": "#ffffff",
		"hover"  : "#0b7ed1"
	},

	{
		"name"   : "",
		"header" : "#354649",
		"text"   : "#E0E7E9",
		"footer" : "#6C7A89",
		"navText": "#ffffff",
		"hover"  : "#A3C6C4"
	}
];

function buildColorSchemes() {
	var html  = '<div class="preset-container" style="padding-top:20px;">';
	var button = `<a href="#designKeyModal" data-bs-toggle="modal" class="btn btn-light mb-3 btn-sm"> <i class="fa fa-magic"></i> View Design Key</a>`;
	html += button;
	for(var i = 0; i < presetList.length; i++) {
		html += '<div class="preset-item">';
		html += '<div class="preset-item-color-box header-color-box" data-preset="header"  data-color="'+presetList[i].header+'"  style="background: '+presetList[i].header+';"></div>';
		html += '<div class="preset-item-color-box text-color-box"   data-preset="text"    data-color="'+presetList[i].text+'"    style="background: '+presetList[i].text+';"></div>';
		html += '<div class="preset-item-color-box footer-color-box" data-preset="footer"  data-color="'+presetList[i].footer+'"  style="background: '+presetList[i].footer+';"></div>';
		html += '<div class="preset-item-color-box navText-color-box" data-preset="navText" data-color="'+presetList[i].navText+'" style="background: '+presetList[i].navText+';"></div>';
		html += '<div class="preset-item-color-box hover-color-box"  data-preset="hover"   data-color="'+presetList[i].hover+'"   style="background: '+presetList[i].hover+';"></div>';
		html += '<div class="clearfix"></div>';
		html += '</div>';
	}
	html += '</div>';
	return html;
}

jQuery(document).ready(function() {

	jQuery(".test").on('click', function(e) { 
		e.preventDefault(); 
		jQuery("#home_header_background_color").val("#ff0000").trigger("keyup"); 
	});

	jQuery("#zen_brand").on('change', function() { 
		var brand = jQuery(this).val();
		var url   = window.location.href.split('&brand')[0];
		window.location.href = url+'&brand='+brand; 
	});

	jQuery('.tov-form-label').each(function(e) {
		var field = jQuery(this).attr('for');

		if(field == 'step1' || field == 'step2' || field == 'step3') {
			jQuery(this).hide();
		}

		if(field == 'color_scheme') {
			var presetColors = buildColorSchemes();
			jQuery(this).append(presetColors);
		}
	});

	jQuery('.preset-item').click(function() {
		var header	= jQuery(this).find('.header-color-box').data('color'); 
		var text	= jQuery(this).find('.text-color-box').data('color');
		var navText	= jQuery(this).find('.navText-color-box').data('color');
		var footer	= jQuery(this).find('.footer-color-box').data('color');
		var hover	= jQuery(this).find('.hover-color-box').data('color');
		
		jQuery("#logged_out_header_background_color").val(header).trigger("keyup");
		jQuery("#logged_out_header_text_color").val(text).trigger("keyup");
		jQuery("#logged_out_footer_background_color").val(footer).trigger("keyup");
		jQuery("#logged_out_footer_text_color").val(navText).trigger("keyup");
		jQuery("#logged_out_social_media_bar_color").val(footer).trigger("keyup");
		jQuery("#logged_out_social_media_bar_text_color").val(navText).trigger("keyup");
		jQuery("#logged_out_social_media_bar_text_hover_color").val(hover).trigger("keyup");
		jQuery("#logged_in_header_background_color").val(header).trigger("keyup");
		jQuery("#logged_in_header_text_color").val(text).trigger("keyup");
		jQuery("#logged_in_navigation_background_color").val(footer).trigger("keyup");
		jQuery("#logged_in_navigation_text_color").val(navText).trigger("keyup");
		jQuery("#logged_in_navigation_text_hover_color").val(hover).trigger("keyup");
		jQuery("#logged_in_module_title_background_color").val(footer).trigger("keyup");
		jQuery("#logged_in_module_title_text_color").val(navText).trigger("keyup");

	});

	/* var button = `<a href="#designKeyModal"data-bs-toggle="modal" class="design-key" style="float: right;"> <i class="fa fa-magic"></i> View Design Key</a>`;
	jQuery('.btn-toolbar').append(button); */
});