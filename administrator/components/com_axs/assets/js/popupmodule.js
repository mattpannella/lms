jQuery(document).ready(	
	function() {

		function setLink() {
			var modID = jQuery('#jform_request_module_id').val();
			jQuery("#jform_params_menu_anchor_title").val(modID);
		}
		
		jQuery("#jform_request_module-lbl").hide();
		jQuery("#jform_params_menu_anchor_css").val('popModule');
		jQuery("#jform_params_menu_anchor_css").attr('readonly',true);
		jQuery("#jform_params_menu_anchor_title").attr('readonly',true);
		jQuery('#jform_request_module_id').change( function(){
			setLink();
		});

		setLink();
	}
);