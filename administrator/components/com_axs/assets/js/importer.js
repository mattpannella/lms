jQuery(document).ready(function() {
	var offCanvasContainer = `<div class="offCanvasContainer">
								<div class="closeBtn"><i class="icon-remove"></i></div>
								<div class="importer-header"></div>
								<form enctype="multipart/form-data" id="importForm">
								 	<h1>User Importer</h1>
									<h2>Upload Your CSV File</h2>
									  <div class="input-file-container">
									    <input class="input-file" id="csv_file" name="csv_file" type="file"/>
									    <input id="template_id" name="template_id" type="hidden"/>
									    <label tabindex="0" for="my-file" class="input-file-trigger">Select a file...</label>
									  </div>
									  <p class="file-return"></p>
									  <div class="uploadUsers uploadBtn"> <i class="icon-users"></i> Upload Users</div>
									</form>
									<div class="importingUsersProcess text-center" style="display:none; width: 100%; clear: both;">
									    <p>Importing <img src="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/loader.gif"> Users</p>
									</div>
									<div class="importMessage"></div>
							  </div>`;
	jQuery('body').append(offCanvasContainer);
	jQuery(document).on('click','.importCSV',function(){
		jQuery('.offCanvasContainer').css('right','0px');
		var templateID = jQuery(this).data('id');
		jQuery('#template_id').val(templateID);
	});

	jQuery(document).on('click','.closeBtn',function(){
		jQuery('.offCanvasContainer').css('right','-350px');
	});

	jQuery(document).on('click','.input-file-trigger',function(){
		jQuery('.input-file').trigger('click');
	});

	var fileInput  = document.querySelector( ".input-file" ),
	    button     = document.querySelector( ".input-file-trigger" ),
	    the_return = document.querySelector(".file-return");

	button.addEventListener( "keydown", function( event ) {
	    if ( event.keyCode == 13 || event.keyCode == 32 ) {
	        fileInput.focus();
	    }
	});
	button.addEventListener( "click", function( event ) {
	   fileInput.focus();
	   return false;
	});
	fileInput.addEventListener( "change", function( event ) {
	    the_return.innerHTML = this.files[0].name;
	    jQuery('.uploadBtn').fadeIn(300);
	});

	function uploadCSVFile(id) {
		var file = document.getElementById('csv_file').files[0];
		console.log(file);

		// Clear any previous status messages before processing the file
		jQuery('.importMessage').html('');

		if(file) {
			var form = jQuery('#importForm')[0];

    		var data = new FormData(form);
    		jQuery('.importingUsersProcess').fadeIn(300);
			jQuery.ajax({
				url  : '/index.php?option=com_axs&task=importer.uploadCSVFile&format=raw',
				type : 'POST',
				data : data,
				enctype: 'multipart/form-data',
        		processData: false,
        		contentType: false,
        		cache: false
			}).done(function(response) {
				jQuery('.importingUsersProcess').hide();

				var result = JSON.parse(response);
				var usersUpdatedPlural = 's';
				var usersAddedPlural  = 's';

				if(result.usersUpdated == 1) {
					usersUpdatedPlural = '';
				}

				if(result.usersAdded == 1) {
					usersAddedPlural = '';
				}

				var message = '<hr/>';

				if(result.usersAdded > 0) {
					message += result.usersAdded + ' User'+usersAddedPlural+' Added Successfully!<br/><br/>';
				}
				if(result.usersUpdated > 0) {
					message += result.usersUpdated + ' User'+usersUpdatedPlural+' Updated Successfully!<br/><br/>';
				}

				// If usersAdded and usersUpdated are both zero, display the message within the response
				if(result.usersUpdated == 0 && result.usersAdded == 0) {
					message += result.message;
				}

				jQuery('.importMessage').html(message);

				console.log(result.message);
			})
		}
	}

	jQuery('.uploadUsers').click(function(){
		uploadCSVFile();
	})
})