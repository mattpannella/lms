<?php

defined('_JEXEC') or die;

class AxsToolbar extends FOFToolbar {

	//BROWSE FORMS
	function onZenbuildersBrowse() {
		JToolBarHelper::title('Quick Start - Zenfully Easy');
		JToolbarHelper::save('save','Save','save');
		JToolbarHelper::cancel();
	}

	function onApikeysBrowse() {
		JToolBarHelper::title('API Keys');
		JToolbarHelper::addNew('add','Generate New Key');
		JToolbarHelper::unpublishList();
		JToolbarHelper::publishList();
		JToolbarHelper::deleteList();
	}

	function onToolsBrowse() {
		JToolBarHelper::title('Tools');
		JToolBarHelper::custom('componentRedirect', 'backward-2', 'backward-2', 'Back', false, false);
	}

	function onAdmin_checklistsBrowse() {
		JToolBarHelper::title('Onboarding Checklists');
	}

	function onAdmin_checklistsEdit() {
		JToolBarHelper::title('Onboarding Checklist Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::cancel();
	}

	function onSso_configsBrowse() {
		JToolBarHelper::title('Single Sign-On Settings');
		JToolbarHelper::addNew();
		JToolbarHelper::unpublishList();
		JToolbarHelper::publishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::loadView('index.php?option=com_axs&view=tools','cog','Forwarding URL Encoder');
	}

	function onLogin_pagesEdit() {
		JToolBarHelper::title('Login Page Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::cancel();
		JToolBarHelper::loadView('index.php?option=com_axs&view=tools','cog','Forwarding URL Encoder');
	}

	function onContent_librariesBrowse() {
		JToolBarHelper::title('Content Library');
	}
	function onLockedFilesBrowse() {
		JToolBarHelper::title('Secure File Locker');
	}
	function onImporter_presentationsBrowse() {
		JToolBarHelper::title('Presentation Importer');
	}

	function onUser_SettingsBrowse() {
		JToolBarHelper::title('Settings');
		JToolBarHelper::save('save', 'Save', 'save');
		JToolbarHelper::cancel();
	}

	function onSecuritiesBrowse() {
		JToolBarHelper::title('Security');
		JToolBarHelper::save('save', 'Save', 'save');
		JToolbarHelper::cancel();
	}

	// EDIT FORMS
	function onBrandsEdit() {
		JToolBarHelper::title('Brands: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}

	function onBrand_HomepagesEdit() {
		JToolBarHelper::title('Landing Pages: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}

	function onSplashPagesBrowse() {

	}

	function onBrand_DashboardsEdit() {
		JToolBarHelper::title('User Portals: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}

	function onBrand_BoardsEdit() {
		JToolBarHelper::title('Boards: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}

	function onLearner_DashboardsEdit() {
		JToolBarHelper::title('User Portals: Edit');
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy');
		JToolbarHelper::cancel();
	}

	function onCommentsBrowse() {
		JToolBarHelper::title('Course Comments');
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
	}

	function onCommentsEdit() {
		JToolBarHelper::title('Course Comments: Edit');
		JToolbarHelper::apply();
		JToolbarHelper::save();
		JToolbarHelper::cancel();
	}

    function onSurveysBrowse() {
        JToolBarHelper::title('Surveys');
		JToolbarHelper::addNew();
        JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Survey(s)');
    }

	function onSurveysEdit() {
		JToolBarHelper::title('Surveys: Edit');
		JToolbarHelper::apply();
		JToolbarHelper::save();
		JToolbarHelper::cancel();
		JToolbarHelper::save2copy('copy','Duplicate Survey');
	}

    function onChecklistsBrowse() {
        JToolBarHelper::title('Checklists');
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
        JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolBarHelper::save2copy('copy', 'Duplicate Checklist(s)');
    }

    function onChecklistsEdit() {
		JToolBarHelper::title('Checklists: Edit');
		JToolbarHelper::apply();
		JToolbarHelper::save();
		JToolbarHelper::cancel();
		JToolbarHelper::save2copy('copy','Duplicate Checklist');
	}
}