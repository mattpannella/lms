<button 
	id="selectorModalBtn_<?php echo $selectorName ?>"
	class="btn btn-light" 
	data-bs-toggle="modal" 
	data-bs-target="#selectorModal_<?php echo $selectorName; ?>" 
	style="text-transform: capitalize;"
	type="button"
>
	<span class="icon-archive" aria-hidden="true"></span>
	<?php echo AxsLanguage::text("JSELECT", "Select") ?> <?php echo $selectorType; ?>
</button>
<input type="hidden" name="<?php echo $selectorName; ?>[]" value="<?php echo $selectorValue; ?>"/>
<input type="hidden" name="full_selector_name" value="<?php echo $selectorName; ?>"/>
<?php 
	$iframeUrl = "/administrator/index.php?option=com_axs&view=selectors&tmpl=component&selection=$selectorType&el=$selectorName$selectorValueEncoded";
	$cancelText = JText::_('JCANCEL');
	echo JHtml::_(
		'bootstrap.renderModal', 
		"selectorModal_$selectorName", 
		array(
			'size' => 'lg',
			'iframeUrl' => $iframeUrl,
			'lazy' => false,
			'footer' => "<button class='btn btn-light' type='button' data-bs-dismiss='modal'>$cancelText</button>"
		)
	)
?>
<script>
	window.addEventListener("message", getSelectedItemsAndCloseModal, false);
	function getSelectedItemsAndCloseModal(e) {
		if (e.origin != window.location.origin) {
			return;
		}
		if (e.data.type != 'selectCoursesEvent') {
			return;
		}
	  	jQuery('input[name="'+e.data.element+'[]"]').val(e.data.ids);
	  	var el = document.getElementById("selectorModal_<?php echo $selectorName; ?>");
		if (el != null) {
			// this modal isn't open, don't bother continuing to close it
			var modal = bootstrap.Modal.getInstance(el);
			if (modal != null) {
				modal.hide();
			}
		}
	}
</script>