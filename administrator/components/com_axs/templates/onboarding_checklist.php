
<div <?php echo $clippingStyle; ?> class="onboarding-sidebar-clipping">
    <div <?php echo $overlayStyle; ?> class="onboarding-clip-overlay"></div>
    <div class="onboarding-sidebar-container" <?php echo $containerStyle; ?>>
        <div class="ob_sb_close-container">
            <div class="ob_sb_close_button"><img src="/administrator/templates/isis3/elements/admin_checklist/images/ob_corner-subtract.svg" alt="" class="ob_sb_subtract top">
            <div class="ob_sb_close"><img src="/administrator/templates/isis3/elements/admin_checklist/images/icon_arrow.svg" alt="" class="ob_sb_arrow"></div><img src="/administrator/templates/isis3/elements/admin_checklist/images/ob_corner-subtract.svg" alt="" class="ob_sb_subtract"></div>
        </div>
        <div class="onboarding-sidebar-wrapper">
            <h6 class="ob-sidebar-header">Onboarding Checklist</h6>
            <div class="ob_list_container">
                <div class="ob_list_wrapper">
                    <div class="ob_list_top">
                        <div class="ob_list_row"><img src="/administrator/templates/isis3/elements/admin_checklist/images/ob_icon_celebrate.svg" alt="" class="ob_list_progress-img">
                            <div class="ob_list_progress_text">Making Progress</div>
                        </div>
                        <div class="ob_list_row">
                            <div class="ob_list_progress">
                                <div class="ob_list_progress_true" style="width: <?php echo $percentCompleted; ?>%"></div>
                            </div>
                            <div class="ob_progress v2">
                                <div id="ob_progress_2" class="ob_progress_wrap"></div>
                            </div>
                        </div>
                    </div>
                    <div class="nano has-scrollbar">
	                    <div class="nano-content">
                            <div class="ob_list">
                                <?php echo $this->buildChecklistItems(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script> var amountCompleted = <?php echo $amountCompleted; ?>; </script>
<script src="/administrator/templates/isis3/elements/admin_checklist/js/onboarding-checklist.js?v=2"></script>