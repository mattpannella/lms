<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/libraries/axslibs/files/assets/css/uploader.css">

<form action="" method="post" id="fileUploadForm" enctype="multipart/form-data">
    <div id="file-uploader-message"></div>
    <div id="file-uploader">
        <div class="input-group col-md-4">
            <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
            <input type="text" id="p_title" class="form-control input-md" name="p_title"  placeholder="Presentation Title..."/>
        </div>
        <div style="width:100%; height:20px; clear:both;"></div>
        <div class="input-group col-md-4">
            <span class="input-group-addon"><i class="fa fa-file"></i></span>
            <input type="text" data-id="file_upload" readonly  class="file_upload form-control input-md file_file" name="filename"  placeholder="Upload File"/>
            <input id="file_upload" style="display: none;" type="file" name="filedata"  class="file"/>
            <span class="input-group-btn">
                <span class="browse btn btn-primary input-md" data-id="file_upload" style="border-bottom-left-radius: 0; border-top-left-radius: 0;"><i class="fa fa-search"></i> Browse</span>
            </span>
            <span class="input-group-btn">
            <span class="file-submit upload-file btn btn-success input-md" style="margin-left: 10px; border-radius: 5px; display: none;"  data-task="uploadActivity"><i class="fa fa-magic"></i> Magic Import</span>
            </span>
        </div>
    </div>
</form>
<div><b><span style="color:#ea8936">Allowed File Types:</span> PowerPoints and PDFs</b></div>
<hr>
<script>
	var sendURL = '/index.php?option=com_axs&task=presentationuploader.processFile&format=raw';
</script>
<script src="/libraries/axslibs/files/assets/js/uploader_presentation.js?v=3"></script>