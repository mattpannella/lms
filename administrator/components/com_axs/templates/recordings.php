<?php 
require 'components/com_axs/helpers/legacy_meeting_recordings.php';
$VC_Server = AxsClientData::getClientVirtualMeetingServer();
//$VC_Server = 'server1';
$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
$virtualMeeting = new AxsVirtualClassroom($setupParams);
$recordings = array();
/* $meetingParams = new stdClass();
$meetingParams->meetingId = $meetingId;
$meetingParams->moderatorPW = '1592920373c1uFMt';
echo $meetingId;
jdump($virtualMeeting->getMeetingInfo($meetingParams) );*/
$newRecordings = $virtualMeeting->getRecordings($meetingId);
if($newRecordings) {
    $recordings = array_merge($recordings,$newRecordings);
}

/* 
b5c75ac9c0edd577e9e264399d7e0c95f4f2a487-1597164291500"
fa5f1d9567c281bee20c2a56f3d14d3c8971e7ad-1597168572692"

*/
if(in_array($meetingId,$meetingList)) {
    $VC_Server_Legacy = 'server1';
    $setupParams_Legacy = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server_Legacy);
    $virtualMeeting_Legacy = new AxsVirtualClassroom($setupParams_Legacy);
    $oldRecordings = $virtualMeeting_Legacy->getRecordings($meetingId);
    $oldRecordingsList = array();
    if($oldRecordings) {
        foreach($oldRecordings as $old_rec) {
            if(in_array($old_rec['recordId'],$recordingsList)) {
                $oldRecordingsList[] = $old_rec;
            }
        }
        $recordings = array_merge($recordings,$oldRecordingsList);
    }
}

/* jdump($newRecordings,'newRecordings');

jdump($oldRecordings,'oldRecordings'); */
/* foreach($recordings as $r) {
    foreach($r as $key => $value) {
        echo $key.": ".$value."<br>";
    }
} */ 
    
?>
<style> 
    #recordings .control-label { 
        display:none; 
    } 
    #recordings .controls {
        margin-left: 0px;
    }
    .prekey-text {
        width: 145px;
    }
    .key-btn {
        cursor: pointer;
    }
    .input-group-addon:first-child {
        width: 180px;
        text-align: left;
    }
</style>
<div id="previewBox"></div>
<table class="table table-bordered table-striped table-responsive">
							
    <tr>
        <th>Date</th>
    	<th>Action</th>
	</tr>
    <?php  
        $count = 0; 
        $legacy = false;
        foreach($recordings as $recording) {
            $count++;
            if($recording['playbackFormatType'] == 'statistics') {
                $recording['playbackFormatUrl'] = str_replace('statistics','capture',$recording['playbackFormatUrl']);               
                //continue;
            } elseif($recording['playbackFormatType'] == 'presentation') {
                $legacy = true;
                $domain = $virtualMeeting->getDomain();
                $webcamsWebm = $domain.'/presentation/'.$recording['recordId'].'/video/webcams.webm';
                $webcamsMP4 = $domain.'/presentation/'.$recording['recordId'].'/video/webcams.mp4';
                $webcamsWebmExists = @fopen($webcamsWebm,'r');
                $webcamsMP4Exists = @fopen($webcamsMP4,'r');
                $sharewebcam = false;
                if($webcamsMP4Exists) {
                    $sharewebcam = true;
                    $webvideo = $webcamsMP4;
                } elseif ($webcamsWebmExists) {
                    $sharewebcam = true;
                    $webvideo = $webcamsWebm;
                }
                $recording['playbackFormatUrl'] = str_replace('meet.','meeting.',$recording['playbackFormatUrl']);
            }
            if(strpos($recording['playbackFormatUrl'],'notes')) {
                $recording['playbackFormatUrl'] = str_replace('notes','capture',$recording['playbackFormatUrl']);
            }

    ?>

    <tr>
        <td width="15%"><?php echo date('m/d/Y', $recording['startTime'] / 1000); ?></td>
    	<td width="70%">
            <?php if ($legacy) { ?>
            <a class="btn btn-success previewBox" data-title="<?php echo $recording['name']; ?>"  data-width="100%" data-height="80vh" href="<?php echo $recording['playbackFormatUrl']; ?>" style="margin-right: 15px;">
                <i class="fa fa-play"></i> View
            </a>
            <?php } else { ?>
                <a class="btn btn-success" target="_blank" href="<?php echo $recording['playbackFormatUrl']; ?>" style="margin-right: 15px;">
                <i class="fa fa-play"></i> View
            </a>
            <?php } ?>
            <a class="btn btn-success shareBox" data-title="<?php echo $recording['name']; ?>"  data-width="100%" data-height="80vh" href="<?php echo $recording['playbackFormatUrl']; ?>" <?php  if($sharewebcam) { echo 'data-webcams="'.$webvideo.'"'; } ?> data-sharebox="<?php echo $count; ?>" style="margin-right: 15px;">
                <i class="fa fa-share"></i> Share
            </a>
            <a class="btn btn-danger" href="/administrator/index.php?option=com_axs&task=virtual_classrooms.deleteRecordings&recordId=<?php echo $recording['recordId']; ?>&meetingId=<?php echo $mid; ?>">
                <i class="fa fa-remove"></i> Delete
            </a>
            <div id="share-box-<?php echo $count; ?>" style="display: none;">
                <div class="input-group input-group-sm" style="margin-top:10px; margin-bottom:10px; max-width: 550px;">
					<div class="input-group-addon btn-primary prekey-text">
						<span><i class="fa fa-link"></i> Full Recording Link</span>
					</div>
					<input type="text" class="form-control" value="" readonly="true" id="share_recording_link-<?php echo $count; ?>">
					<div class="input-group-addon btn-success copy-key key-btn" data-key="share_recording_link-<?php echo $count; ?>">
						<span><i class="fa fa-copy"></i> Copy</span>
					</div>
                </div>
                <div class="input-group input-group-sm" style="margin-bottom:10px; max-width: 550px;">
					<div class="input-group-addon btn-primary prekey-text">
						<span><i class="fa fa-code"></i> Full Recording Embed</span>
					</div>
					<input type="text" class="form-control" value="" readonly="true" id="share_embed_link-<?php echo $count; ?>"">
					<div class="input-group-addon btn-success copy-key key-btn" data-key="share_embed_link-<?php echo $count; ?>">
						<span><i class="fa fa-copy"></i> Copy</span>
					</div>
                </div>
                <?php  if($sharewebcam) { ?> 
                <div class="input-group input-group-sm" style="margin-top:10px; margin-bottom:10px; max-width: 550px;">
					<div class="input-group-addon btn-primary prekey-text">
						<span><i class="fa fa-link"></i> Webcam Recording Video</span>
					</div>
					<input type="text" class="form-control" value="" readonly="true" id="share_webcams_link-<?php echo $count; ?>">
					<div class="input-group-addon btn-success copy-key key-btn" data-key="share_webcams_link-<?php echo $count; ?>">
						<span><i class="fa fa-copy"></i> Copy</span>
                    </div>                    
                </div>
                <a class="btn btn-default" href="<?php echo $webvideo; ?>" target="_blank">
					<span><i class="fa fa-play"></i> View Webcam Video</span>
                </a>
                <?php } ?>
            </div>
        </td>
	</tr>            
    <?php } ?>    
</table>

<script>


jQuery(document).on("click", ".previewBox", function(e) {
    e.preventDefault();
    var width  = jQuery(this).data("width");
    var height = jQuery(this).data("height");
    var title  = jQuery(this).data("title");
    var src    = jQuery(this).attr("href");
    var iframe = '<center><embed frameborder="0" scrolling="no" style="width:854px; height:480px;" src="'+src+'"></embed></center>';
    jQuery('#previewBox').html(iframe);
    
});

jQuery(document).on("click", ".shareBox", function(e) {
    e.preventDefault();
    var sharebox  = jQuery(this).data("sharebox");
    if( jQuery("#share-box-"+sharebox).is(":visible")) {
        jQuery('#share-box-'+sharebox).hide();
    } else {
        jQuery('#share-box-'+sharebox).show();        
        var width  = jQuery(this).data("width");
        var height = jQuery(this).data("height");
        var title  = jQuery(this).data("title");
        var webcams= jQuery(this).data("webcams");
        var src    = jQuery(this).attr("href");
        var iframe = '<iframe frameborder="0" scrolling="no" style="width:100%; height:480px;" src="'+src+'"></iframe>';
        jQuery('#share_embed_link-'+sharebox).val(iframe);
        jQuery('#share_recording_link-'+sharebox).val(src);
        if(webcams) {
            jQuery('#share_webcams_link-'+sharebox).val(webcams);
        }
    }
    
    
});

jQuery(document).on('click','.copy-key', function(){
    var key_id = jQuery(this).data('key');        
    var keyText = document.getElementById(key_id);
    keyText.select();
    document.execCommand('copy');
})
    
</script>