<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/libraries/axslibs/files/assets/css/uploader.css">
<link rel="stylesheet" type="text/css" href="/administrator/templates/isis3/css/template.css">

<form action="" method="post" id="fileUploadForm" enctype="multipart/form-data">
    <style>
        body {
            background-color: unset;
        }
    </style>
    <div id="file-uploader-message"></div>    
    <div id="file-uploader">
        <div class="input-group">
            <span class="input-group-text"><i class="fa fa-file"></i></span>
            <input type="text" data-id="file_upload" readonly class="file_upload file_file form-control" name="filename" placeholder="Upload File" style="max-width:500px"/>
            <input id="file_upload" style="display: none;" type="file" name="filedata"  class="file"/>                       
            <button class="browse btn btn-primary" data-id="file_upload" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="fa fa-search"></i> Browse</button>
            <button class="file-submit upload-file btn btn-success input-md" style="margin-left: 10px; border-radius: 5px; display: none;"  data-task="uploadActivity"><i class="fa fa-upload"></i> Upload</button>
        </div>
    </div>
</form>
<div><b><span style="color:#ea8936">Allowed File Types:</span> PowerPoint, PDF, MP3, MP4</b></div>
<hr>
<script>
	var sendURL = '/index.php?option=com_axs&task=securedfiles.processFile&format=raw';
</script>
<script src="/libraries/axslibs/files/assets/js/uploader.js"></script>