<div class="ob_list_item <?php echo $checked; ?> <?php echo $lastItem; ?>">
    <a href="<?php echo $item['action_link']; ?>" class="ob_list_item_left_link w-inline-block">
        <div class="ob_list_item_checkbox <?php echo $checked; ?>"></div>
        <div class="ob_list-item_title <?php echo $checked; ?>"><?php echo $item['text']; ?></div>
    </a>
    <?php if($item['instruction_link']) { ?>
        <a href="<?php echo $item['instruction_link']; ?>" target="_blank" class="ob_list-instructions <?php echo $checked; ?>  w-inline-block">
            <div class="ob_instructions-text">Instructions</div>
        </a>
    <?php } ?>
</div>