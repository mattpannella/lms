<?php
echo AxsLoader::loadKendo();

$VC_Server = AxsClientData::getClientVirtualMeetingServer();
$setupParams = AxsVirtualMeetingServers::getVirtualMeetingServerParams($VC_Server);
$virtualMeeting = new AxsVirtualClassroom($setupParams);

$attendance = $virtualMeeting->getAttendance($virtualClassParams);

?>
<style>
    #attendance .control-label {
        display:none;
    }
    #attendance .controls {
        margin-left: 0px;
    }
</style>

<table id="attendanceGrid" class="table table-bordered table-striped table-responsive">

    <tr>
        <th>Profile</th>
        <th>Name</th>
        <th>Date Attended</th>
        <th>Time Spent in Class/Meeting</th>
	</tr>
    <?php
        if(is_iterable($attendance)) {
            foreach($attendance as $row) {
                $profile = AxsExtra::getUserProfileData($row->user_id);

                if ($profile->photo) {
                    $photo = $profile->photo;
                } else {
                    $photo = "/images/user.png";
                }

                $user_display  =  "<div  class='comment-img' style='border-radius:50%;'><a target='_blank'  class='student-link' href='/my-profile/" . $profile->alias . "'><img src='" . $photo . "' style='width: 50px;'></a></div>";
    ?>
                <tr>
                    <td><?php echo $user_display; ?></td>
                    <td><?php if($row->name) { echo $row->name; } else { echo $row->attendee_name; } ?></td>
                    <td><?php echo date('m/d/Y',strtotime($row->date)); ?></td>
                    <td><?php if($row->check_out) { echo gmdate('H:i:s', round( strtotime($row->check_out) - strtotime($row->check_in) ) ); } ?></td>
                </tr>
    <?php
            }
        }
    ?>
</table>

<script>

var kendoOptions = {
    toolbar: ["pdf", "excel"],
    sortable:   true,
    groupable:  true,
    resizable:  true,
    columnMenu: true,
    filterable: {
        mode: "row"
    },
    /*height:   600,*/
    pageable: {
        buttonCount:    8,
        input:          true,
        pageSize:       20,
        pageSizes:      true,
        refresh:        true,
        message: {
            empty:      "There are no entries to display"
        }
    },
    columns: [
        {
            field: "Profile",
            filterable: false,
             width: "100px"
        },
        {
            field: "Name",
            title: "Name",
            filterable: {
                operators: {
                    string: {
                        contains: "Contains"
                    }
                }
            },
        },
        {
            field: "Date",
            type: "date",
            format: "{0:MM/d/yyyy}",
            filterable: {
                ui: "datepicker"
            }
        },
        {
            field: "Duration",
            title: "Time Spent in Class/Meeting"
        }
    ]
}

var attendanceGrid = jQuery("#attendanceGrid").kendoGrid(kendoOptions);
var grid = attendanceGrid.data("kendoGrid");
var exportFlag = false;
attendanceGrid.data("kendoGrid").bind("excelExport", function (e) {
    if (!exportFlag) {
        e.sender.hideColumn(0);
        e.preventDefault();
        exportFlag = true;
        setTimeout(function () {
            e.sender.saveAsExcel();
        });
    } else {
        e.sender.showColumn(0);
        exportFlag = false;
    }
});
</script>