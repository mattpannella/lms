<?php

/*
	Justin Lloyd
	7/20/2016
*/

class AxsModelBrand_Homepages extends FOFModel {

	public function save($data) {

		if(is_array($data)) {
			$homepage = new stdClass();

			/*
				Home Page Designer Tab
			*/

			$header = new stdClass();
			$footer = new stdClass();
			$colors = new stdClass();
			$testimonials = new stdClass();
			$css = new stdClass();
			$js = new stdClass();
			$zones = new StdClass();

			$homepage->default_font = $data["home_default_font"];
			$homepage->logo = $data['logo_main'];
			$homepage->logo_link = $data['logo_link'];
			$header->menu = $data['menu'];
			$header->slideshow = json_encode($data['slideshow']);
			$header->slides = json_decode($data['slides']);
			$header->slides_overlay = $data['slides_overlay'];
			$header->slides_height_type = $data['slides_height_type'];
			$header->slides_mobile_height = $data['slides_mobile_height'];
			$header->slides_desktop_height = $data['slides_desktop_height'];
			$header->slides_mobile_height_percent = $data['slides_mobile_height_percent'];
			$header->slides_desktop_height_percent = $data['slides_desktop_height_percent'];

			$header->font = $data['home_header_font'];
			$header->custom_html = $data['header_custom_html'];
			$header->overlay = $data['header_overlay'];

			$header->height_type = $data['header_height_type'];
			$header->height_percentage = $data['header_height_percentage'];
			$header->height_pixel = $data['header_height_pixel'];

			$header->header_layout = $data['header_layout'];
			$header->header_fixed = $data['header_fixed'];
			$header->logo_height_type = $data['logo_height_type'];
			$header->logo_height_percentage = $data['logo_height_percentage'];
			$header->logo_height_pixel = $data['logo_height_pixel'];

			$header->logo_position = $data['header_logo_position'];

			if ($data['header_drop_shadow'] == "1") {
				$header->drop_shadow = true;
			} else {
				$header->drop_shadow = false;
			}

			$header->show = new stdClass();

			if ($data['header_show_logo'] == "1") {
				$header->show->logo = true;
			} else {
				$header->show->logo = false;
			}
			if ($data['header_show_menu'] == "1") {
				$header->show->menu = true;
			} else {
				$header->show->menu = false;
			}
			if ($data['header_menu_resize'] == "1") {
				$header->menu_resize = true;
			} else {
				$header->menu_resize = false;
			}
			if ($data['header_show_slides'] == "1") {
				$header->show->slides = true;
			} else {
				$header->show->slides = false;
			}
			if ($data['header_show_custom'] == "1") {
				$header->show->custom = true;
			} else {
				$header->show->custom = false;
			}

			if ($data['header_show_socialmedia'] == "1") {
				$header->show_socialmedia = true;
			} else {
				$header->show_socialmedia = false;
			}

			$header->socialmedia_orientation = $data['header_socialmedia_orientation'];
			$header->socialmedia_color = $data['header_socialmedia_color'];
			$header->socialmedia_text_color = $data['header_socialmedia_text_color'];
			$header->socialmedia_hover_color = $data['header_socialmedia_hover_color'];
			$header->socialmedia_icon_style = $data['header_socialmedia_icon_style'];

			$footer->display_type = $data['footer_display_type'];
			$footer->custom_html = $data['footer_custom_html'];
			$footer->text_color = $data['footer_text_color'];
			$footer->background_color = $data['footer_background_color'];
			$footer->font = $data['footer_font'];
			if ($data['footer_membership_link'] == '1') {
				$footer->membership_link = true;
			} else {
				$footer->membership_link = false;
			}

			if ($data['footer_benefits_link'] == '1') {
				$footer->benefits_link = true;
			} else {
				$footer->benefits_link = false;
			}

			if ($data['footer_testimonials_link'] == '1') {
				$footer->testimonials_link = true;
			} else {
				$footer->testimonials_link = false;
			}

			if ($data['footer_faq_link'] == '1') {
				$footer->faq_link = true;
			} else {
				$footer->faq_link = false;
			}

			if ($data['footer_legal_link'] == '1') {
				$footer->legal_link = true;
			} else {
				$footer->legal_link = false;
			}

			if ($data['footer_support_link'] == '1') {
				$footer->support_link = true;
			} else {
				$footer->support_link = false;
			}

			$footer->links_per_row = $data['footer_links_per_row'];
			$footer->custom_links = json_decode($data['footer_custom_links']);
			$footer->text_size = $data['footer_text_size'];
			$footer->subtext_size = $data['footer_subtext_size'];
			$footer->width = $data['footer_width'];
			$footer->link_spacing_type = $data['footer_row_spacing_type'];
			$footer->link_spacing_pixel = $data['footer_row_spacing_pixel'];
			$footer->link_spacing_percent = $data['footer_row_spacing_percent'];
			$footer->copyright = $data['footer_copyright'];

			if ($data['footer_show_socialmedia'] == "1") {
				$footer->show_socialmedia = true;
			} else {
				$footer->show_socialmedia = false;
			}

			$footer->socialmedia_orientation = $data['footer_socialmedia_orientation'];
			$footer->socialmedia_color = $data['footer_socialmedia_color'];
			$footer->socialmedia_text_color = $data['footer_socialmedia_text_color'];
			$footer->socialmedia_hover_color = $data['footer_socialmedia_hover_color'];
			$footer->socialmedia_icon_style = $data['footer_socialmedia_icon_style'];
			
			$testimonials->list = json_decode($data['testimonials']);
			$testimonials->pos = $data['testimonials_pos'];
			$testimonials->font = $data['testimonials_font'];

			$colors->header_type = $data['home_header_colors_type'];
			$colors->background = $data['home_header_background_color'];
			$colors->background_transparency = $data['home_header_background_transparency'] / 100;
			$colors->header_text = $data['home_header_text_color'];

			$colors->subpages_type = $data['home_subpages_header_colors_type'];
			$colors->subpages_background = $data['home_subpages_header_background_color'];
			$colors->subpages_background_transparency = $data['home_subpages_header_background_transparency'] / 100;
			$colors->subpages_text = $data['home_subpages_header_text_color'];

			$css->pre = $data['css_pre'];
			$css->post = $data['css_post'];
			$js->pre = $data['javascript_pre'];
			$js->post = $data['javascript_post'];

			$count = 1;
			$haveZone = true;
			while ($haveZone) {
				$key = "zone_$count";
				if (!isset($data[$key])) {
					$haveZone = false;
				} else {
					$zone = $data[$key];
					$newData = array();

					foreach ($zone as $key2 => $val) {
						$key2 = str_replace("zone_", "", $key2);
						switch ($key2) {
							//Convert booleans to strings
							case "active":
							case "title_shadowed":
								$newData[$key2] = json_decode($val);
								break;
							default:
								$newData[$key2] = $val;
						}
					}
					$zones->$key = $newData;
				}

				$count++;
			}

			$homepage->header = $header;
			$homepage->footer = $footer;
			$homepage->colors = $colors;
			$homepage->testimonials = $testimonials;
			$homepage->css = $css;
			$homepage->js = $js;
			$homepage->zones = $zones;

			$data['data'] = json_encode($homepage);
		}
		return parent::save($data);
	}

	public function loadFormData() {

		if (empty($this->_formData) && !$data) {
			return array();
		} else {

			$data = $this->_formData;

			$homepage = json_decode($data['data']);
			$header = $homepage->header;
			$footer = $homepage->footer;
			$colors = $homepage->colors;
			$testimonials = $homepage->testimonials;
			$css = $homepage->css;
			$js = $homepage->js;
			$zones = $homepage->zones;

			/*
				Home Page Designer Tab
			*/

			$data['home_default_font'] = $homepage->default_font;
			$data['logo_main'] = $homepage->logo;
			$data['logo_link'] = $homepage->logo_link;
			
			$data['slideshow'] = $header->slideshow;
			$data['menu'] = $header->menu;
			if ($header->slides) {
				$data['slides'] = json_encode($header->slides);
			}			

			$data['slides_overlay'] = $header->slides_overlay;
			$data['slides_height_type'] = $header->slides_height_type;
			$data['slides_mobile_height'] = $header->slides_mobile_height;
			$data['slides_desktop_height'] = $header->slides_desktop_height;
			$data['slides_mobile_height_percent'] = $header->slides_mobile_height_percent;
			$data['slides_desktop_height_percent'] = $header->slides_desktop_height_percent;
			$data['home_header_font'] = $header->font;
			$data['header_custom_html'] = $header->custom_html;
			$data['header_overlay'] = $header->overlay;

			$data['header_layout'] = $header->header_layout;
			$data['header_fixed'] = $header->header_fixed;
			$data['logo_height_type'] = $header->logo_height_type;
			$data['logo_height_percentage'] = $header->logo_height_percentage;
			$data['logo_height_pixel'] = $header->logo_height_pixel;

			$data['header_height_type'] = $header->height_type;
			$data['header_height_percentage'] = $header->height_percentage;
			$data['header_height_pixel'] = $header->height_pixel;

			$data['header_logo_position'] = $header->logo_position;

			if ($header->show_socialmedia) {
				$data['header_show_socialmedia'] = "1";
			} else {
				$data['header_show_socialmedia'] = "0";
			}

			$data['header_socialmedia_orientation'] = $header->socialmedia_orientation;
			$data['header_socialmedia_color'] = $header->socialmedia_color;
			$data['header_socialmedia_text_color'] = $header->socialmedia_text_color;
			$data['header_socialmedia_hover_color'] = $header->socialmedia_hover_color;
			$data['header_socialmedia_icon_style'] = $header->socialmedia_icon_style;

			if ($header->drop_shadow) {
				$data['header_drop_shadow'] = "1";
			} else {
				$data['header_drop_shadow'] = "0";
			}

			if ($header->show->logo) {
				$data['header_show_logo'] = "1";
			} else {
				$data['header_show_logo'] = "0";
			}

			if ($header->show->menu) {
				$data['header_show_menu'] = "1";
			} else {
				$data['header_show_menu'] = "0";
			}

			if ($header->menu_resize || !isset($homepage->header)) {
				$data['header_menu_resize'] = "1";
			} else {
				$data['header_menu_resize'] = "0";
			}

			if ($header->show->slides) {
				$data['header_show_slides'] = "1";
			} else {
				$data['header_show_slides'] = "0";
			}

			if ($header->show->custom) {
				$data['header_show_custom'] = "1";
			} else {
				$data['header_show_custom'] = "0";
			}

			$data['footer_display_type'] = $footer->display_type;
			$data['footer_custom_html'] = $footer->custom_html;

			$data['footer_text_color'] = $footer->text_color;
			$data['footer_background_color'] = $footer->background_color;
			$data['footer_font'] = $footer->font;

			if ($footer->show_socialmedia) {
				$data['footer_show_socialmedia'] = "1";
			} else {
				$data['footer_show_socialmedia'] = "0";
			}

			$data['footer_socialmedia_orientation'] = $footer->socialmedia_orientation;
			$data['footer_socialmedia_color'] = $footer->socialmedia_color;
			$data['footer_socialmedia_text_color'] = $footer->socialmedia_text_color;
			$data['footer_socialmedia_hover_color'] = $footer->socialmedia_hover_color;
			$data['footer_socialmedia_icon_style'] = $footer->socialmedia_icon_style;

			if ($footer->membership_link == true) {
				$data['footer_membership_link'] = '1';
			} else {
				$data['footer_membership_link'] = '0';
			}

			if ($footer->benefits_link == true) {
				$data['footer_benefits_link'] = '1';
			} else {
				$data['footer_benefits_link'] = '0';
			}

			if ($footer->testimonials_link == true) {
				$data['footer_testimonials_link'] = '1';
			} else {
				$data['footer_testimonials_link'] = '0';
			}

			if ($footer->faq_link == true) {
				$data['footer_faq_link'] = '1';
			} else {
				$data['footer_faq_link'] = '0';
			}

			if ($footer->legal_link == true) {
				$data['footer_legal_link'] = '1';
			} else {
				$data['footer_legal_link'] = '0';
			}

			if ($footer->support_link == true) {
				$data['footer_support_link'] = '1';
			} else {
				$data['footer_support_link'] = '0';
			}

			$data['footer_links_per_row'] = $footer->links_per_row;

			if ($footer->custom_links) {
				$data['footer_custom_links'] = json_encode($footer->custom_links);
			}

			$data['footer_row_spacing_type'] = $footer->link_spacing_type;
			$data['footer_row_spacing_pixel'] = $footer->link_spacing_pixel;
			$data['footer_row_spacing_percent'] = $footer->link_spacing_percent;

			$data['footer_text_size'] = $footer->text_size;
			$data['footer_subtext_size'] = $footer->subtext_size;
			$data['footer_width'] = $footer->width;
			$data['footer_copyright'] = $footer->copyright;

			if ($testimonials->list) {
				$data['testimonials'] = json_encode($testimonials->list);
			}

			$data['testimonials_pos'] = $testimonials->pos;
			$data['testimonials_font'] = $testimonials->font;

			$data['home_header_colors_type'] = $colors->header_type;
			$data['home_header_background_color'] = $colors->background;
			$data['home_header_background_transparency'] = $colors->background_transparency * 100;
			$data['home_header_text_color'] = $colors->header_text;

			$data['home_subpages_header_colors_type'] = $colors->subpages_type;
			$data['home_subpages_header_background_color'] = $colors->subpages_background;
			$data['home_subpages_header_background_transparency'] = $colors->subpages_background_transparency * 100;
			$data['home_subpages_header_text_color'] = $colors->subpages_text;

			$data['css_pre'] = $css->pre;
			$data['css_post'] = $css->post;
			$data['javascript_pre'] = $js->pre;
			$data['javascript_post'] = $js->post;


			foreach ($zones as $key => $zone) {

				//If the data decodes as an stdClass, convert it to an array.
				if (gettype($zone) == 'object') {
					$zone = json_decode(json_encode($zone), true);
				}

				$data[$key] = array();


				foreach ($zone as $key2 => $val) {

					switch ($key2) {
						//Convert booleans to strings
						case "active":
						case "title_shadowed":
							$data[$key]["zone_$key2"] = json_encode($val);
							break;
						case "columns":
							//Legacy columns will use [] if it's empty, which will break.

							//if (count(json_decode($val)) == 0) {
							if ($val == "[]") {
								$val = "";
							}

							if ($val) {
								$data[$key]["zone_$key2"] = $val;
							}
							break;
						default:
							$data[$key]["zone_$key2"] = $val;
					}
				}
			}


			return $data;
		}
	}
}
