<?php

defined('_JEXEC') or die;

class AxsModelQuizzes extends FOFModel {

    public function save($data) {
        $questions = $data['items'];

        foreach($questions as &$question) {
            if(empty($question['id'])) {
                $question['id'] = AxsLMS::createUniqueID();
            }
        }

        // Store the information given into the model
        $data['questions'] = json_encode($questions);
        $data['modified_date'] = date("Y-m-d H:i:s");

        // Set the parameters and store them
        $params = new stdClass();
        $params->custom_completed_message = $data['custom_completed_message'];
        $params->completed_message = trim($data['completed_message']);

        // Target percentage needs to be an int because we're going to do calculations on it
        $params->target_percentage = intval((trim($data['target_percentage'])));

        $data['params'] = json_encode($params);

        return parent::save($data);
    }

    public function loadFormData() {
        $data = $this->_formData;

        if(!empty($data)) {
            $data['items'] = $data['questions'];

            $params = json_decode($data['params']);
            $data['custom_completed_message'] = $params->custom_completed_message;
            $data['completed_message'] = $params->completed_message;
            $data['target_percentage'] = $params->target_percentage;
        } else {
            $data = array();
        }

        return $data;
    }
}

