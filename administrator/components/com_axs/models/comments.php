<?php

defined('_JEXEC') or die;

class AxsModelComments extends FOFModel {

    public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
            $lessonId = explode('_', $data['post_id'])[1];

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('title, splms_course_id, splms_lesson_id');
            $query->from('joom_splms_lessons');
            $query->where('splms_lesson_id = ' . $db->quote($lessonId));
            $db->setQuery($query);  
            $lesson = $db->loadObject();

            $query = $db->getQuery(true);
            $query->select('title');
            $query->from('joom_splms_courses');
            $query->where('splms_course_id = ' . $db->quote($lesson->splms_course_id));
            $db->setQuery($query);  
            $course = $db->loadObject();

            $data['course_link'] = json_encode(array(
                'uri' => '/administrator/index.php?option=com_splms&view=course&id=' . $lesson->splms_course_id,
                'title' => $course->title
            ));
            $data['lesson_link'] = json_encode(array(
                'uri' => '/administrator/index.php?option=com_splms&view=lesson&id=' . $lesson->splms_lesson_id,
                'title' => $lesson->title
            ));

            

			return $data;			
		}
	}

	public function &getItemList($overrideLimits = false, $group = '') {
		$query = $this->buildQuery($overrideLimits);
		if (!$overrideLimits)
		{
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
			$this->list = $this->_getList((string) $query, $limitstart, $limit, $group);
		}
		else
		{
			$this->list = $this->_getList((string) $query, 0, 0, $group);
		}

        $lessonIds = array();
        foreach($this->list as &$listItem) {
            $lessonId = explode('_', $listItem->post_id)[1];
            $lessonIds []= $lessonId;
        }

        $lessonIdsList = implode(',', array_unique($lessonIds));
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('title, splms_course_id, splms_lesson_id');
        $query->from('joom_splms_lessons');
        $query->where('splms_lesson_id IN (' . $lessonIdsList . ')');
        $db->setQuery($query);  
        $lessons = $db->loadAssocList('splms_lesson_id');

        $courseIds = array_map(function ($el) { return $el['splms_course_id']; }, $lessons);
        $courseIdsList = implode(',', array_unique($courseIds));

        $query = $db->getQuery(true);
        $query->select('title, splms_course_id');
        $query->from('joom_splms_courses');
        $query->where('splms_course_id IN (' . $courseIdsList . ')');
        $db->setQuery($query);
        $courses = $db->loadAssocList('splms_course_id');

        foreach($this->list as &$listItem) {
            $lessonId = explode('_', $listItem->post_id)[1];
            $listItem->lesson = $lessons[$lessonId]['title'];
            $listItem->lesson_id = $lessonId;
            $courseId = $lessons[$lessonId]['splms_course_id'];
            $listItem->course = $courses[$courseId]['title'];
            $listItem->course_id = $courseId;
        }
		return $this->list;
	}

}