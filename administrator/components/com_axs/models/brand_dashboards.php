<?php

/*
	Justin Lloyd
	7/20/2016
*/

class AxsModelBrand_Dashboards extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$dashboard = new stdClass();

			$navigation_bar = new stdClass();
			$user_bar = new stdClass();
			$logo = new stdClass();
			$modules = new stdClass();
			$menu = new stdClass();
			$module = new stdClass();
			$css = new stdClass();
			$js = new stdClass();

			$dashboard->default_font = $data['dashboard_default_font'];
			$dashboard->main_view_layout = $data['main_view_layout'];
			$dashboard->learner_dashboard_id = $data['learner_dashboard_id'];

			$navigation_bar->icons = $data['navigation_bar_icons'];
			$navigation_bar->location = $data['navigation_bar_location'];
			if ($data['navigation_bar_collapsible'] == "1") {
				$navigation_bar->collapsible = true;
			} else {
				$navigation_bar->collapsible = false;
			}
			$navigation_bar->style = $data['navigation_bar_style'];
			$navigation_bar->color = $data['navigation_bar_color'];
			$navigation_bar->icon_color = $data['navigation_bar_icon_color'];
			$navigation_bar->toplevel_hover_color = $data['navigation_bar_toplevel_hover_color'];
			$navigation_bar->hover_color = $data['navigation_bar_hover_color'];
			$navigation_bar->icon_size = (int)$data['navigation_bar_icon_size'];
			$navigation_bar->text_color = $data['navigation_bar_text_color'];
			$navigation_bar->sub_text_color = $data['navigation_bar_sub_text_color'];
			$navigation_bar->text_size = (int)$data['navigation_bar_text_size'];
			$navigation_bar->sub_text_size = $data['navigation_bar_sub_text_size'];
			$navigation_bar->text_font = $data['navigation_bar_text_font'];
			$navigation_bar->sub_text_font = $data['navigation_bar_sub_text_font'];
			$navigation_bar->width_type = $data['navigation_bar_width_type'];
			$navigation_bar->width_percentage = $data['navigation_bar_width_percentage'];
			$navigation_bar->width_pixels = (int)$data['navigation_bar_width_pixels'];

			$logo->main = $data['logo_main'];
			$logo->sub = $data['logo_sub'];
			$logo->sidebar_width_type = $data['logo_sidebar_width_type'];
			$logo->sidebar_width_percentage = $data['logo_sidebar_width_percentage'];
			$logo->sidebar_width_pixels = (int)$data['logo_sidebar_width_pixels'];
			$logo->user_bar_width_type = $data['logo_user_bar_width_type'];
			$logo->user_bar_width_percentage = $data['logo_user_bar_width_percentage'];
			$logo->user_bar_width_pixels = (int)$data['logo_user_bar_width_pixels'];
			$logo->image_width_type = $data['image_width_type'];
			$logo->image_width_percentage = $data['image_width_percentage'];
			$logo->image_width_pixels = $data['image_width_pixels'];
			$logo->image_height_type = $data['image_height_type'];
			$logo->image_height_percentage = $data['image_height_percentage'];
			$logo->image_height_pixels = $data['image_height_pixels'];
			$logo->background_color = $data['logo_background_color'];
			$logo->collapse_color = $data['collapse_color'];

			$user_bar->location = $data['user_bar_location'];
			$user_bar->color = $data['user_bar_color'];
			$user_bar->text_color = $data['user_bar_text_color'];
			$user_bar->text_size = (int)$data['user_bar_text_size'];
			$user_bar->sub_text_size = $data['user_bar_sub_text_size'];
			$user_bar->text_font = $data['user_bar_text_font'];
			$user_bar->sub_text_font = $data['user_bar_sub_text_font'];
			$user_bar->width_type = $data['user_bar_width_type'];
			$user_bar->width_percentage = $data['user_bar_width_percentage'];
			$user_bar->width_pixels = (int)$data['user_bar_width_pixels'];
			$user_bar->show_notification_icons = $data['show_notification_icons'];

			if ($data['user_bar_show_socialmedia'] == "1") {
				$user_bar->show_socialmedia = true;
			} else {
				$user_bar->show_socialmedia = false;
			}

			if ($data['user_bar_user_image'] == "1") {
				$user_bar->user_image = true;
			} else {
				$user_bar->user_image = false;
			}
			if ($data['user_bar_user_name'] == "1") {
				$user_bar->user_name = true;
			} else {
				$user_bar->user_name = false;
			}
			if ($data['user_bar_user_number'] == "1") {
				$user_bar->user_number = true;
			} else {
				$user_bar->user_number = false;
			}

			$menu->font = $data['user_menu_font'];
			if ($data['menu_view_profile'] == "1") {
				$menu->view_profile = true;
			} else {
				$menu->view_profile = false;
			}
			if ($data['menu_edit_profile'] == "1") {
				$menu->edit_profile = true;
			} else {
				$menu->edit_profile = false;
			}
			if ($data['menu_view_transcript'] == "1") {
				$menu->my_transcript = true;
			} else {
				$menu->my_transcript = false;
			}
			if ($data['menu_my_rewards'] == "1") {
				$menu->my_rewards = true;
			} else {
				$menu->my_rewards = false;
			}
			if ($data['menu_billing_manager'] == "1") {
				$menu->billing_manager = true;
			} else {
				$menu->billing_manager = false;
			}
			if ($data['menu_my_inbox'] == "1") {
				$menu->my_inbox = true;
			} else {
				$menu->my_inbox = false;
			}
			if ($data['menu_my_declarations'] == "1") {
				$menu->my_declarations = true;
			} else {
				$menu->my_declarations = false;
			}
			if ($data['menu_my_inspirations'] == "1") {
				$menu->my_inspirations = true;
			} else {
				$menu->my_inspirations = false;
			}
			if ($data['menu_my_gratitudes'] == "1") {
				$menu->my_gratitudes = true;
			} else {
				$menu->my_gratitudes = false;
			}

			$menu->view_profile_label = $data['menu_profile_view_label'];
			$menu->edit_profile_label = $data['menu_profile_edit_label'];
			$menu->my_transcript_label = $data['menu_transcript_view_label'];
			$menu->rewards_label = $data['menu_rewards_label'];
			$menu->billing_label = $data['menu_billing_label'];
			$menu->my_inbox_label = $data['menu_my_inbox_label'];
			$menu->my_declarations_label = $data['menu_my_declarations_label'];
			$menu->my_inspirations_label = $data['menu_my_inspirations_label'];
			$menu->my_gratitudes_label = $data['menu_my_gratitudes_label'];

			$temp = json_decode($data['dashboard_modules']);
			$module_list = new stdClass();

			$module_list->id = array();
			$module_list->active = array();
			$module_list->height = array();
			$module_list->height_parent = array();
			$module_list->width = array();
			$module_list->title = array();
			$module_list->title_bar = array();

			$mod_count = count($temp->module_id);

			for ($i = 0; $i < $mod_count; $i++) {
				if ($temp->module_active[$i] == "1") {
					$module_list->active[$i] = true;
				} else {
					$module_list->active[$i] = false;
				}

				$module_list->id[$i] = $temp->module_id[$i];
				$module_list->height[$i] = (int)$temp->module_height[$i];
				$module_list->height_parent[$i] = (int)$temp->module_height_parent[$i];
				$module_list->custom_id[$i] = $temp->module_custom_id[$i];
				$module_list->custom_class[$i] = $temp->module_custom_class[$i];
				$module_list->width[$i] = (int)$temp->module_width[$i];
				$module_list->title[$i] = $temp->module_title_bar_text[$i];
				$module_list->font[$i] = $temp->module_font[$i];
				$module_list->scroll_bars[$i] = $temp->module_scroll_bars[$i];

				if ($temp->module_title_bar[$i] == "1") {
					$module_list->title_bar[$i] = true;
				} else {
					$module_list->title_bar[$i] = false;
				}

				if ($temp->module_border[$i] == "1") {
					$module_list->border[$i] = true;
				} else {
					$module_list->border[$i] = false;
				}
			}
			$modules->list = $module_list;
			$modules->font = $data['module_font'];

			$modules->header_background = $data['header_background'];
			$modules->header_background_text = $data['header_background_text'];
			$modules->header_background_text_size = $data['header_background_text_size'];
			$modules->header_background_text_font = $data['header_background_text_font'];

			$modules->padding_type = $data['module_padding_type'];
			$modules->padding_pixel = $data['module_padding_pixel'];
			$modules->padding_percent = $data['module_padding_percent'];
			$modules->margin_type = $data['module_margin_type'];
			$modules->margin_pixel = $data['module_margin_pixel'];
			$modules->margin_percent = $data['module_margin_percent'];
			$modules->title_bar_color = $data['title_bar_color'];
			$modules->title_bar_text_color = $data['title_bar_text_color'];
			$modules->title_bar_font_size = $data['title_bar_font_size'];
			$modules->title_bar_padding_type = $data['title_bar_padding_type'];
			$modules->title_bar_padding_pixel = $data['title_bar_padding_pixel'];
			$modules->title_bar_padding_percent = $data['title_bar_padding_percent'];

			$css->pre = $data['css_pre'];
			$css->post = $data['css_post'];
			$js->pre = $data['javascript_pre'];
			$js->post = $data['javascript_post'];

			$dashboard->navigation_bar = $navigation_bar;
			$dashboard->user_bar = $user_bar;
			$dashboard->logo = $logo;
			$dashboard->menu = $menu;
			$dashboard->modules = $modules;
			$dashboard->scripts = $scripts;
			$dashboard->css = $css;
			$dashboard->js = $js;
			$dashboard->modules = $modules;

			$data['data'] = json_encode($dashboard);
		}
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			$dashboard = json_decode($data['data']);

			$navigation_bar = $dashboard->navigation_bar;
			$user_bar = $dashboard->user_bar;
			$logo = $dashboard->logo;
			$menu = $dashboard->menu;
			$modules = $dashboard->modules;
			$css = $dashboard->css;
			$js = $dashboard->js;

			$data['dashboard_default_font'] = $dashboard->default_font;
			$data['main_view_layout'] = $dashboard->main_view_layout;
			$data['learner_dashboard_id'] = $dashboard->learner_dashboard_id;

			$data['navigation_bar_icons'] = $navigation_bar->icons;
			$data['navigation_bar_location'] = $navigation_bar->location;

			if ($navigation_bar->collapsible == true) {
				$data['navigation_bar_collapsible'] = "1";
			} else {
				$data['navigation_bar_collapsible'] = "0";
			}

			$data['navigation_bar_style'] = $navigation_bar->style;
			$data['navigation_bar_color'] = $navigation_bar->color;
			$data['navigation_bar_icon_color'] = $navigation_bar->icon_color;
			$data['navigation_bar_toplevel_hover_color'] = $navigation_bar->toplevel_hover_color;
			$data['navigation_bar_hover_color'] = $navigation_bar->hover_color;
			$data['navigation_bar_icon_size'] = $navigation_bar->icon_size;
			$data['navigation_bar_text_color'] = $navigation_bar->text_color;
			$data['navigation_bar_sub_text_color'] = $navigation_bar->sub_text_color;
			$data['navigation_bar_text_size'] = $navigation_bar->text_size;
			$data['navigation_bar_sub_text_size'] = $navigation_bar->sub_text_size;
			$data['navigation_bar_text_font'] = $navigation_bar->text_font;
			$data['navigation_bar_sub_text_font'] = $navigation_bar->sub_text_font;
			$data['navigation_bar_width_type'] = $navigation_bar->width_type;
			$data['navigation_bar_width_percentage'] = $navigation_bar->width_percentage;
			$data['navigation_bar_width_pixels'] = $navigation_bar->width_pixels;

			$data['logo_main'] = $logo->main;
			$data['logo_sub'] = $logo->sub;
			$data['logo_sidebar_width_type'] = $logo->sidebar_width_type;
			$data['logo_sidebar_width_percentage'] = $logo->sidebar_width_percentage;
			$data['logo_sidebar_width_pixels'] = $logo->sidebar_width_pixels;
			$data['logo_user_bar_width_type'] = $logo->user_bar_width_type;
			$data['logo_user_bar_width_percentage'] = $logo->user_bar_width_percentage;
			$data['logo_user_bar_width_pixels'] = $logo->user_bar_width_pixels;
			$data['image_width_type'] = $logo->image_width_type;
			$data['image_width_percentage'] = $logo->image_width_percentage;
			$data['image_width_pixels'] = $logo->image_width_pixels;
			$data['image_height_type'] = $logo->image_height_type;
			$data['image_height_percentage'] = $logo->image_height_percentage;
			$data['image_height_pixels'] = $logo->image_height_pixels;
			$data['logo_background_color'] = $logo->background_color;
			$data['collapse_color'] = $logo->collapse_color;

			$data['user_bar_location'] = $user_bar->location;
			$data['user_bar_color'] = $user_bar->color;
			$data['user_bar_text_color'] = $user_bar->text_color;
			$data['user_bar_text_size'] = $user_bar->text_size;
			$data['user_bar_sub_text_size'] = $user_bar->sub_text_size;
			$data['user_bar_text_font'] = $user_bar->text_font;
			$data['user_bar_sub_text_font'] = $user_bar->sub_text_font;
			$data['user_bar_width_type'] = $user_bar->width_type;
			$data['user_bar_width_percentage'] = $user_bar->width_percentage;
			$data['user_bar_width_pixels'] = $user_bar->width_pixels;
			$data['show_notification_icons'] = $user_bar->show_notification_icons;

			if ($user_bar->show_socialmedia) {
				$data['user_bar_show_socialmedia'] = "1";
			} else {
				$data['user_bar_show_socialmedia'] = "0";
			}

			if ($user_bar->user_image == true) {
				$data['user_bar_user_image'] = "1";
			} else {
				$data['user_bar_user_image'] = "0";
			}
			if ($user_bar->user_name == true) {
				$data['user_bar_user_name'] = "1";
			} else {
				$data['user_bar_user_name'] = "0";
			}
			if ($user_bar->user_number == true) {
				$data['user_bar_user_number'] = "1";
			} else {
				$data['user_bar_user_number'] = "0";
			}

			$data['user_menu_font'] = $menu->font;
			if ($menu->view_profile == true) {
				$data['menu_view_profile'] = "1";
			} else {
				$data['menu_view_profile'] = "0";
			}
			if ($menu->edit_profile == true) {
				$data['menu_edit_profile'] = "1";
			} else {
				$data['menu_edit_profile'] = "0";
			}
			if ($menu->my_transcript == true) {
				$data['menu_view_transcript'] = "1";
			} else {
				$data['menu_view_transcript'] = "0";
			}
			if ($menu->my_rewards == true) {
				$data['menu_my_rewards'] = "1";
			} else {
				$data['menu_my_rewards'] = "0";
			}
			if ($menu->billing_manager == true) {
				$data['menu_billing_manager'] = "1";
			} else {
				$data['menu_billing_manager'] = "0";
			}
			if ($menu->my_inbox == true) {
				$data['menu_my_inbox'] = "1";
			} else {
				$data['menu_my_inbox'] = "0";
			}
			if ($menu->my_declarations == true) {
				$data['menu_my_declarations'] = "1";
			} else {
				$data['menu_my_declarations'] = "0";
			}
			if ($menu->my_inspirations == true) {
				$data['menu_my_inspirations'] = "1";
			} else {
				$data['menu_my_inspirations'] = "0";
			}
			if ($menu->my_gratitudes == true) {
				$data['menu_my_gratitudes'] = "1";
			} else {
				$data['menu_my_gratitudes'] = "0";
			}

			$data['menu_profile_view_label'] = $menu->view_profile_label;
			$data['menu_profile_edit_label'] = $menu->edit_profile_label;
			$data['menu_transcript_view_label'] = $menu->my_transcript_label;
			$data['menu_rewards_label'] = $menu->rewards_label;
			$data['menu_billing_label'] = $menu->billing_label;
			$data['menu_my_inbox_label'] = $menu->my_inbox_label;
			$data['menu_my_declarations_label'] = $menu->my_declarations_label;
			$data['menu_my_inspirations_label'] = $menu->my_inspirations_label;
			$data['menu_my_gratitudes_label'] = $menu->my_gratitudes_label;

			$module_list = $modules->list;

			if ($module_list != null) {

				$temp = new stdClass();
				$temp->module_id = array();
				$temp->module_width = array();
				$temp->module_active = array();
				$temp->module_height = array();
				$temp->module_height_parent = array();
				$temp->module_font = array();

				$mod_count = count($module_list->id);

				for ($i = 0; $i < $mod_count; $i++) {
					if ($module_list->active[$i] == true) {
						$temp->module_active[$i] = "1";
					} else {
						$temp->module_active[$i] = "0";
					}

					$temp->module_id[$i] = $module_list->id[$i];
					$temp->module_width[$i] = $module_list->width[$i];
					$temp->module_height[$i] = $module_list->height[$i];
					$temp->module_custom_id[$i] = $module_list->custom_id[$i];
					$temp->module_custom_class[$i] = $module_list->custom_class[$i];
					$temp->module_height_parent[$i] = $module_list->height_parent[$i];
					$temp->module_scroll_bars[$i] = $module_list->scroll_bars[$i];
					$temp->module_title_bar_text[$i] = $module_list->title[$i];
					$temp->module_font[$i] = $module_list->font[$i];

					if ($module_list->border[$i] == true) {
						$temp->module_border[$i] = "1";
					} else {
						$temp->module_border[$i] = "0";
					}

					if ($module_list->title_bar[$i] == true) {
						$temp->module_title_bar[$i] = "1";
					} else {
						$temp->module_title_bar[$i] = "0";
					}
				}

				$data['dashboard_modules'] = json_encode($temp);
			}

			$data['module_font'] = $modules->font;

			$data['module_padding_type'] = $modules->padding_type;
			$data['module_padding_pixel'] = $modules->padding_pixel;
			$data['module_padding_percent'] = $modules->padding_percent;
			$data['module_margin_type'] = $modules->margin_type;
			$data['module_margin_pixel'] = $modules->margin_pixel;
			$data['module_margin_percent'] = $modules->margin_percent;

			$data['title_bar_color'] = $modules->title_bar_color;
			$data['title_bar_text_color'] = $modules->title_bar_text_color;
			$data['title_bar_font_size'] = $modules->title_bar_font_size;
			$data['title_bar_padding_type'] = $modules->title_bar_padding_type;
			$data['title_bar_padding_pixel'] = $modules->title_bar_padding_pixel;
			$data['title_bar_padding_percent'] = $modules->title_bar_padding_percent;

			$data['header_background'] = $modules->header_background;
			$data['header_background_text'] = $modules->header_background_text;
			$data['header_background_text_size'] = $modules->header_background_text_size;
			$data['header_background_text_font'] = $modules->header_background_text_font;

			$data['css_pre'] = $css->pre;
			$data['css_post'] = $css->post;
			$data['javascript_pre'] = $js->pre;
			$data['javascript_post'] = $js->post;

			return $data;
		}
	}
}
