<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
class AxsModelZenbuilders extends FOFModel {
	public function save($data) {
		$input = JFactory::getApplication()->input;
		$brandID = (int)$input->get('brand');
		$result = AxsZenbuilder::saveZenData($data);

		$app = JFactory::getApplication();
		if($result) {
			$message     = 'Your Tovuti Design is Saved.';
			$messageType = 'success';
		} else {
			$message 	 = 'There was an issue saving your design. Please Refresh you page and try again.';
			$messageType = 'error';
		}

		$tracking = new stdClass();
		$tracking->eventName = 'Quick Start Edit';
		$tracking->description = 'Quick Start';
		$tracking->action_type ="admin";
		AxsTracking::sendToPendo($tracking);
		AxsActions::storeAdminAction($tracking);

		$app->enqueueMessage($message);

		if(empty($brandID)) {

			$brandID = AxsBrands::getDefault()->id;
		}

		$app->redirect('index.php?option=com_axs&view=zenbuilders&brand=' . $brandID);
	}

	public function loadFormData() {
		$input = JFactory::getApplication()->input;
		$brandID = (int)$input->get('brand');
		if($brandID) {
			$brand = AxsBrands::getBrandById($brandID);
		} else {
			$brand = AxsBrands::getBrand();
			$brandID = $brand->id;
		}

		$slides = json_decode($brand->homepage->header->slideshow);
		$data['brand'] 	   = $brandID;
		$data['site_name'] = $brand->email->sitename;
		$data['logo'] = $brand->homepage->logo;
		$data['favicon'] = $brand->logos->favicon;
		$data['homepage_image'] = $slides->slideshow0->file;
		$data['homepage_image_text'] = $slides->slideshow0->text;
		$data['dashboard_image'] = $brand->dashboard->modules->header_background;
		$data['dashboard_image_text'] = $brand->dashboard->modules->header_background_text;
		$data['contact_email'] = $brand->billing->admin_emails[0]->email;
		if(!$brand->homepage->header->show_socialmedia) {
			$show_socialmedia = 0;
		} else {
			$show_socialmedia = 1;
		}
		$data['show_socialmedia'] = $show_socialmedia;
		$data['pinterest_link'] = $brand->social->pinterest_link;
		$data['instagram_link'] = $brand->social->instagram_link;
		$data['facebook_link'] = $brand->social->facebook_link;
		$data['twitter_link'] = $brand->social->twitter_link;
		$data['google_plus_link'] = $brand->social->google_plus_link;
		$data['youtube_link'] = $brand->social->youtube_link;
		$data['linkedin_link'] = $brand->social->linkedin_link;
		$data['tovuti_root_styles'] = $brand->site_details->tovuti_root_styles;
		$data['root_primary_color'] = $brand->site_details->root_primary_color;
		$data['root_accent_color'] = $brand->site_details->root_accent_color;
		$data['root_base_color'] = $brand->site_details->root_base_color;

		$menuItems = ['registration', 'community', 'profile', 'inbox', 'groups', 'login', 'contact', 'courses', 'media', 'supscriptions', 'blog', 'events', 'forum'];

		$menuHomepage  = $brand->homepage->header->menu;
		$menuDashboard = $brand->dashboard->navigation_bar->icons;

		$menus = new stdClass();
		$menus->home = $menuHomepage;
		$menus->dashboard = $menuDashboard;

		foreach($menuItems as $item) {
			$checkItem = AxsZenbuilder::checkMenuItem($menus,$item);
			$data[$item] = $checkItem;
		}

		return $data;
	}
}