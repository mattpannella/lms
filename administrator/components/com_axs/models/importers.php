<?php

defined('_JEXEC') or die;

class AxsModelImporters extends FOFModel {
	public function save($data) {

        $data['user_fields']   = !empty($data['user_fields']) ? implode(',',$data['user_fields']) : null;
        $data['usergroups']    = !empty($data['usergroups']) ? implode(',',$data['usergroups']) : null;

        $params = new stdClass();
        $params->generate_username    = $data['generate_username'];
        $params->username_rule        = $data['username_rule'];
        $params->generate_password    = $data['generate_password'];
        $params->defined_password     = $data['defined_password'];
        $params->password_reset       = $data['password_reset'];
        $params->send_emails          = $data['send_emails'];  
        $params->email_message        = $data['email_message']; 
        $params->email_subject        = $data['email_subject'];
        $params->include_groups       = $data['include_groups'];
        $params->include_courses      = $data['include_courses'];
        $params->include_certificates = $data['include_certificates'];
        $params->new_password         = $data['new_password'];        
        $params->courses              = implode(',',$data['courses']);        
        $params->certificates_badges  = implode(',',$data['certificates_badges']);

        $data['params']  = json_encode($params);
        return parent::save($data);
    }

	public function loadFormData() {
       
		$data = $this->_formData;
        $brand = AxsBrands::getBrand();
        $url = AxsLMS::getUrl();

		if (empty($data)) {
			return array();
		} else {
            $params = json_decode($data['params']);

            $data['user_fields']          = explode(',',$data['user_fields']);
            $data['usergroups']           = explode(',',$data['usergroups']);
            $data['generate_username']    = $params->generate_username;
            $data['username_rule']        = $params->username_rule;
            $data['generate_password']    = $params->generate_password;
            $data['defined_password']     = $params->defined_password;
            $data['password_reset']       = $params->password_reset;
            $data['send_emails']          = $params->send_emails;  
            $data['email_message']        = $params->email_message; 
            $data['email_subject']        = $params->email_subject;
            $data['include_groups']       = $params->include_groups;
            $data['include_courses']      = $params->include_courses;      
            $data['courses']              = $params->courses;              
            $data['include_certificates'] = $params->include_certificates;
            $data['new_password']         = $params->new_password;
            $data['certificates_badges']  = explode(',',$params->certificates_badges);  

            if(!$data['email_subject']) {
                $data['email_subject'] = 'Welcome to '.$brand->site_title;
            }
            
            if(!$data['email_message']) {
                $data['email_message'] = 'Hi [firstname], <br/><br/>
                You have joined '.$brand->site_title.'. Please save your login information below:
                <br/><br/>
                <b>Username:</b> [username]<br/>
                <b>Password:</b> [password]<br/>
                <b>Website:</b> <a href="'.$url.'">'.$url.'</a> 
                <br/><br/>
                Thank you';
            }      
            return $data;
		}
	}
}