<?php

class AxsModelSecurities extends FOFModel {

	private function getData() {
        $db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("axs_user_settings");
		$query->where('id=1');
        $query->limit(1);

		$db->setQuery($query);
        $data = $db->loadObject();

		return $data;
	}


	public function save($data) {

        $db = JFactory::getDbo();

        $settings = $this->getData();
        $params = json_decode($settings->params);

        $params->user_avatar_upload = $data['user_avatar_upload'];
        $params->max_login_attempts = $data['max_login_attempts'];
        $params->max_failed_amount = $data['max_failed_amount'];
        $params->max_login_waiting_period = $data['max_login_waiting_period'];
        $params->custom_password_strength = $data['custom_password_strength'];
        $params->min_characters = $data['min_characters'];
        $params->min_uppercase = $data['min_uppercase'];
        $params->min_lowercase = $data['min_lowercase'];
        $params->min_special = $data['min_special'];
        $params->min_numbers = $data['min_numbers'];
        $params->twofactor_enabled = $data['twofactor_enabled'];
        $params->twofactor_type = $data['twofactor_type'];

        if($params->twofactor_enabled) {
            AxsSecurity::enableTwoFactor(true,$params->twofactor_type);
        } else {
            AxsSecurity::enableTwoFactor(false,$params->twofactor_type);
        }

        $updatedRow = new stdClass();
        $updatedRow->id = 1;
        $updatedRow->params = json_encode($params);

        $success = $db->updateObject('axs_user_settings', $updatedRow, 'id');

        return $success;
	}

	public function loadFormData() {
		$settings = $this->getData();
        $params = json_decode($settings->params);
        $data = $this->_formData;

        $data['user_avatar_upload'] = $params->user_avatar_upload;
        $data['max_login_attempts'] = $params->max_login_attempts;
        $data['max_failed_amount'] = $params->max_failed_amount;
        $data['max_login_waiting_period'] = $params->max_login_waiting_period;
        $data['custom_password_strength'] = $params->custom_password_strength;
        $data['min_characters'] = $params->min_characters;
        $data['min_uppercase'] = $params->min_uppercase;
        $data['min_lowercase'] = $params->min_lowercase;
        $data['min_special'] = $params->min_special;
        $data['min_numbers'] = $params->min_numbers;
        $data['twofactor_enabled'] = $params->twofactor_enabled;
        $data['twofactor_type'] = $params->twofactor_type;
        $data['link_to_user_avatars'] = json_encode(array(
            'title' => 'Edit User Avatars',
            'uri' => '/administrator/index.php?option=com_axs&view=avatars'
        ));

		return $data;
	}
}