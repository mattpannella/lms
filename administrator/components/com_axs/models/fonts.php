<?php

class AxsModelFonts extends FOFModel {	
	public function save($data) {	
		if(!empty($data['brands'])) {
			$data['brands'] = implode(",", $data['brands']);
		}

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$data['brands'] = explode(",", $data['brands']);
			
			return $data;
		}
	}
}