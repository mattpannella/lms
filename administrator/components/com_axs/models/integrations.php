<?php

class AxsModelIntegrations extends FOFModel {

	public function save($data) {
		if(is_array($data)) {

            $params = new stdClass();
            $params->zoom_api_public_key = $data['zoom_api_public_key'];
            $params->zoom_api_secret_key = AxsEncryption::encrypt($data['zoom_api_secret_key'], AxsKeys::getKey('lms'));
			$params->zoom_integration = 'yes';
            $data['params'] = json_encode($params);
		}

		return parent::save($data);
	}

   public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
            $data = $this->_formData;
            $params = json_decode($data['params']);
            $data['zoom_api_public_key'] = $params->zoom_api_public_key;
            $data['zoom_api_secret_key'] = AxsEncryption::decrypt($params->zoom_api_secret_key, AxsKeys::getKey('lms'));
		}

        return $data;
	}

	public function onBeforeDelete($id, $table) {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_brands');
        $query->where("JSON_EXTRACT(data, '$.integration_id') = " . $db->quote($id));
        $db->setQuery($query);
        $brands = $db->loadObjectList();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_integrations');
        $query->where("id = " . $db->quote($id));
        $db->setQuery($query);
        $integration = $db->loadObject();

		if (!empty($brands)) {
			$msg = "Delete failed, integration $integration->name is still being used in the following brands:";
			$msg .= "<ul>";
			foreach ($brands as $brand) {
				$msg .= "<li><a href='/administrator/index.php?option=com_axs&view=brand&id=$brand->id'>$brand->site_title</a></li>";
			}
			$msg .= "</ul>";
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return;
		}

		return true;
	}

}
