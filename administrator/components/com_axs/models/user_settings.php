<?php

class AxsModelUser_Settings extends FOFModel {

	private function getData() {
        $db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from("axs_user_settings");
		$query->where('id=1');
        $query->limit(1);

		$db->setQuery($query);
        $data = $db->loadObject();

		return $data;
	}


	public function save($data) {

        $db = JFactory::getDbo();

        $settings = $this->getData();
        $params = json_decode($settings->params);

        // Clean and store the generic new user welcome email template variables
        $params->subject = AxsSecurity::cleanInput($data['email_subject']);
        $params->body = AxsSecurity::cleanInput($data['email_body']);

        $updatedRow = new stdClass();
        $updatedRow->id = 1;
        $updatedRow->params = json_encode($params);

        $success = $db->updateObject('axs_user_settings', $updatedRow, 'id');

        return $success;
	}

	public function loadFormData() {
		$settings = $this->getData();
        $params = json_decode($settings->params);
        $data = $this->_formData;

        $data['email_subject'] = $params->subject;
        $data['email_body'] = $params->body;
        $data['registration_email_subject'] = $params->registration_email_subject;
        $data['registration_email_body'] = $params->registration_email_body;

		return $data;
	}
}