<?php

defined("_JEXEC") or die();

class AxsModelSurveys extends FOFModel {

    public function save($data) {
        if(is_array($data)) {
            $questions = $data['items'];
            foreach($questions as &$question) {
                if(!$question['id']) {
                    $question['id'] = AxsLMS::createUniqueID();
                }
            }
            $data['questions'] = json_encode($questions);
            $data['modified_date'] = date("Y-m-d H:i:s");
            $params = new stdClass();
            $params->likert_labels = $data['likert_labels'];
            $params->label_1 = trim($data['label_1']);
            $params->label_2 = trim($data['label_2']);
            $params->label_3 = trim($data['label_3']);
            $params->label_4 = trim($data['label_4']);
            $params->label_5 = trim($data['label_5']);
            $params->label_6 = trim($data['label_6']);
            $params->label_7 = trim($data['label_7']);
            $params->label_8 = trim($data['label_8']);
            $params->label_9 = trim($data['label_9']);
            $params->label_10 = trim($data['label_10']);
            $params->label_11 = trim($data['label_11']);
            $params->rating_number = trim($data['rating_number']);
            $params->rating_direction = trim($data['rating_direction']);

            $params->custom_completed_message = $data['custom_completed_message'];
            $params->completed_message = trim($data['completed_message']);

            $data['params'] = json_encode($params);
       }
       return parent::save($data);
    }

    public function loadFormData() {
        $data = $this->_formData;
        if(empty($data)) {
            return array();
        } else {
            $data['items'] = $data['questions'];
            $params = json_decode($data['params']);
            $data['likert_labels'] = $params->likert_labels;
            $data['label_1'] = $params->label_1;
            $data['label_2'] = $params->label_2;
            $data['label_3'] = $params->label_3;
            $data['label_4'] = $params->label_4;
            $data['label_5'] = $params->label_5;
            $data['label_6'] = $params->label_6;
            $data['label_7'] = $params->label_7;
            $data['label_8'] = $params->label_8;
            $data['label_9'] = $params->label_9;
            $data['label_10'] = $params->label_10;
            $data['label_11'] = $params->label_11;
            $data['rating_number'] = $params->rating_number;
            $data['rating_direction'] = $params->rating_direction;
            $data['custom_completed_message'] = $params->custom_completed_message;
            $data['completed_message'] = $params->completed_message;
            return $data;
        }
    }
}