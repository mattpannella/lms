<?php

/*
	Justin Lloyd
	8/23/2016
*/

class AxsModelBrand_Boards extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$settings = new stdClass();
			$affirmation = new stdClass();
			$my_affirmation = new stdClass();
			$goal = new stdClass();
			$my_goal = new stdClass();
			$image = new stdClass();
			$my_image = new stdClass();

			$data['categories'] = json_encode($data['categories']);

			$settings->report = new stdClass();
			$settings->admin = new stdClass();
			
			$settings->cover_image = $data['cover_image'];
			$settings->access_type = $data['access_type'];
			$settings->userlist = $data['userlist'];
			$settings->usergroup = $data['usergroup'];
			$settings->access = $data['accesslevel'];
			$settings->cover_image = $data['cover_image'];
			$settings->teamselect = $data['teamselect'];
			$settings->teamlist = $data['teamlist'];
			$settings->report->access_type = $data['report_access_type'];
			$settings->report->accesslevel = $data['report_accesslevel'];
			$settings->report->usergroup = $data['report_usergroup'];
			$settings->report->userlist = $data['report_userlist'];
			$settings->admin->access_type = $data['admin_access_type'];
			$settings->admin->accesslevel = $data['admin_accesslevel'];
			$settings->admin->usergroup = $data['admin_usergroup'];
			$settings->admin->userlist = $data['admin_userlist'];

			$affirmation->board_name = $data['affirmation_board_name'];
			$affirmation->description = $data['affirmation_description'];
			$affirmation->background_color = $data['affirmation_background_color'];
			$affirmation->text_color = $data['affirmation_text_color'];
			$affirmation->my_button = $data['affirmation_my_button'];
			$affirmation->loading = $data['affirmation_loading'];
			$affirmation->comments = $data['affirmation_comments'];
			$affirmation->comments_placeholder = $data['affirmation_comments_placeholder'];
			$affirmation->comments_submit = $data['affirmation_comments_submit'];
			$affirmation->empty_list = $data['affirmation_empty_list'];

			$my_affirmation->board_name = $data['my_affirmation_board_name'];
			$my_affirmation->background_color = $data['my_affirmation_background_color'];
			$my_affirmation->text_color = $data['my_affirmation_text_color'];
			//$my_affirmation->subtext_color = $data['my_affirmation_subtext_color'];
			$my_affirmation->new_button = $data['my_affirmation_new_button'];
			$my_affirmation->back_button = $data['my_affirmation_back_button'];
			$my_affirmation->thankful = $data['my_affirmation_thankful'];
			$my_affirmation->thankful_check = $data['my_affirmation_thankful_check'];
			$my_affirmation->already_thankful_check = $data['my_affirmation_already_thankful_check'];
			$my_affirmation->title_header = $data['my_affirmation_title_header'];
			$my_affirmation->lead_in = $data['my_affirmation_lead_in'];
			$my_affirmation->reason_lead_in = $data['my_affirmation_reason_lead_in'];
			$my_affirmation->max_length = $data['my_affirmation_max_length'];
			$my_affirmation->max_reason_length = $data['my_affirmation_max_reason_length'];
			$my_affirmation->comments = $data['my_affirmation_comments'];
			$my_affirmation->comments_placeholder = $data['my_affirmation_comments_placeholder'];
			$my_affirmation->comments_submit = $data['my_affirmation_comments_submit'];
			$my_affirmation->new_affirmation = $data['my_affirmation_new_affirmation'];
			$my_affirmation->edit_affirmation = $data['my_affirmation_edit_affirmation'];
			$my_affirmation->user_affirmation = $data['my_affirmation_user_affirmation'];
			$my_affirmation->require_reason = $data['my_affirmation_require_reason'];
			$my_affirmation->reason = $data['my_affirmation_reason'];
			$my_affirmation->new_submit = $data['my_affirmation_new_submit'];
			$my_affirmation->edit_submit = $data['my_affirmation_edit_submit'];
			$my_affirmation->warning_text = $data['my_affirmation_warning_text'];
			$my_affirmation->warning_reason = $data['my_affirmation_warning_reason'];
			$my_affirmation->warning_category = $data['my_affirmation_warning_category'];
			$my_affirmation->empty_list = $data['my_affirmation_empty_list'];

			$goal->board_name = $data['goal_board_name'];
			$goal->description = $data['goal_description'];
			$goal->background_color = $data['goal_background_color'];
			$goal->text_color = $data['goal_text_color'];
			$goal->my_button = $data['goal_my_button'];
			$goal->loading = $data['goal_loading'];
			$goal->comments = $data['goal_comments'];
			$goal->comments_placeholder = $data['goal_comments_placeholder'];
			$goal->comments_submit = $data['goal_comments_submit'];
			$goal->empty_list = $data['goal_empty_list'];
			$goal->time_left = $data['goal_time_left'];
			$goal->time_left_plural = $data['goal_time_left_plural'];
			$goal->time_passed = $data['goal_time_passed'];
			$goal->time_passed_plural = $data['goal_time_passed_plural'];
			$goal->not_finished = $data['goal_not_finished'];
			$goal->finished = $data['goal_finished'];
			$goal->user_lead_in = $data['goal_user_lead_in'];

			$my_goal->board_name = $data['my_goal_board_name'];
			$my_goal->background_color = $data['my_goal_background_color'];
			$my_goal->text_color = $data['my_goal_text_color'];
			$my_goal->new_button = $data['my_goal_new_button'];
			$my_goal->declaration_name = $data['my_goal_declaration_name'];
			$my_goal->goal_name = $data['my_goal_goal_name'];
			$my_goal->declaration_example_accomplishable = $data['my_goal_declaration_example_accomplishable'];
			$my_goal->declaration_example_not_accomplishable = $data['my_goal_declaration_example_not_accomplishable'];
			$my_goal->goal_example_measurable = $data['my_goal_goals_example_measurable'];
			$my_goal->goal_example_not_measurable = $data['my_goal_goals_example_not_measurable'];
			$my_goal->new_lead_in = $data['my_goal_new_lead_in'];
			$my_goal->declaration_accomplishable_title = $data['my_goal_declaration_accomplishable_title'];
			$my_goal->declaration_accomplishable_text = $data['my_goal_declaration_accomplishable_text'];
			$my_goal->goal_measurable_title = $data['my_goal_goal_measurable_title'];
			$my_goal->goal_measurable_text = $data['my_goal_goal_measurable_text'];
			$my_goal->goal_reach = $data['my_goal_goal_reach'];
			$my_goal->start_date = $data['my_goal_start_date'];
			$my_goal->end_date = $data['my_goal_end_date'];
			$my_goal->submit = $data['my_goal_submit'];
			$my_goal->max_length = $data['my_goal_max_length'];
			$my_goal->back_button = $data['my_goal_back_button'];
			$my_goal->lead_in = $data['my_goal_lead_in'];
			$my_goal->progress = $data['my_goal_progress'];
			$my_goal->not_finished = $data['my_goal_not_finished'];
			$my_goal->date_made = $data['my_goal_date_made'];
			$my_goal->passed_time = $data['my_goal_passed_time'];
			$my_goal->success = $data['my_goal_success'];
			$my_goal->ending = $data['my_goal_ending'];
			$my_goal->remaining_time = $data['my_goal_remaining_time'];
			$my_goal->encouragement = $data['my_goal_encouragement'];
			$my_goal->update_field = $data['my_goal_update_field'];
			$my_goal->update_field_placeholder = $data['my_goal_update_field_placeholder'];
			$my_goal->accomplish_button_instructions = $data['my_goal_accomplish_button_instructions'];
			$my_goal->accomplish_button_hover = $data['my_goal_accomplish_button_hover'];
			$my_goal->no_entries = $data['my_goal_no_entries'];
			$my_goal->warning_invalid_update_number = $data['my_goal_warning_invalid_update_number'];
			$my_goal->warning_empty_goal = $data['my_goal_warning_empty_goal'];
			$my_goal->warning_empty_goal_number = $data['my_goal_warning_empty_goal_number'];
			$my_goal->warning_invalid_number = $data['my_goal_warning_invalid_number'];
			$my_goal->warning_no_category = $data['my_goal_warning_no_category'];
			$my_goal->warning_invalid_start_date = $data['my_goal_warning_invalid_start_date'];
			$my_goal->warning_invalid_end_date = $data['my_goal_warning_invalid_end_date'];
			$my_goal->warning_invalid_date = $data['my_goal_warning_invalid_date'];

			$image->board_name = $data['image_board_name'];
			$image->description = $data['image_description'];
			$image->background_color = $data['image_background_color'];
			$image->text_color = $data['image_text_color'];
			$image->loading = $data['image_loading'];
			$image->my_button = $data['image_my_button'];
			$image->comments = $data['image_comments'];
			$image->comments_placeholder = $data['image_comments_placeholder'];
			$image->comments_submit = $data['image_comments_submit'];
			$image->empty_list = $data['image_empty_list'];
			$image->rating_1 = $data['image_rating_1'];
			$image->rating_2 = $data['image_rating_2'];
			$image->rating_3 = $data['image_rating_3'];
			$image->rating_4 = $data['image_rating_4'];
			$image->rating_5 = $data['image_rating_5'];

			$my_image->board_name = $data['my_image_board_name'];
			$my_image->background_color = $data['my_image_background_color'];
			$my_image->text_color = $data['my_image_text_color'];
			$my_image->new_button = $data['my_image_new_button'];
			$my_image->back_button = $data['my_image_back_button'];
			$my_image->loading = $data['my_image_loading'];
			$my_image->comments = $data['my_image_comments'];
			$my_image->comments_placeholder = $data['my_image_comments_placeholder'];
			$my_image->comments_submit = $data['my_image_comments_submit'];
			$my_image->max_length = $data['my_image_max_length'];
			$my_image->new_image = $data['my_image_new_image'];
			$my_image->edit_image = $data['my_image_edit_image'];
			$my_image->user_text = $data['my_image_user_text'];
			$my_image->new_submit = $data['my_image_new_submit'];
			$my_image->edit_submit = $data['my_image_edit_submit'];
			$my_image->warning_no_image = $data['my_image_warning_no_image'];
			$my_image->warning_no_category = $data['my_image_warning_no_category'];
			$my_image->empty_list = $data['my_image_empty_list'];

			$boards = new stdClass();

			$boards->affirmation = $affirmation;
			$boards->my_affirmation = $my_affirmation;
			$boards->goal = $goal;
			$boards->my_goal = $my_goal;
			$boards->image = $image;
			$boards->my_image = $my_image;
			$boards->font = $data['default_font'];
			$boards->settings = $settings;

			$data['data'] = json_encode($boards);
		}
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			if ($data['categories']) {
				$data['categories'] = json_decode($data['categories']);
			}

			$boards = json_decode($data['data']);

			$settings = $boards->settings;
			$affirmation = $boards->affirmation;
			$my_affirmation = $boards->my_affirmation;
			$goal = $boards->goal;
			$my_goal = $boards->my_goal;
			$image = $boards->image;
			$my_image = $boards->my_image;

			$data['access_type'] = $settings->access_type;
			$data['userlist'] = $settings->userlist;
			$data['usergroup'] = $settings->usergroup;
			$data['accesslevel'] = $settings->access;
			$data['cover_image'] = $settings->cover_image;
			$data['teamselect'] = $settings->teamselect;
			$data['teamlist'] = $settings->teamlist;
			$data['report_access_type'] = $settings->report->access_type;
			$data['report_accesslevel'] = $settings->report->accesslevel;
			$data['report_usergroup'] = $settings->report->usergroup;
			$data['report_userlist'] = $settings->report->userlist;
			$data['admin_access_type'] = $settings->admin->access_type;
			$data['admin_accesslevel'] = $settings->admin->accesslevel;
			$data['admin_usergroup'] = $settings->admin->usergroup;
			$data['admin_userlist'] = $settings->admin->userlist;

			$data['default_font'] = $boards->font;

			$data['affirmation_board_name'] = $affirmation->board_name;
			$data['affirmation_description'] = $affirmation->description;
			$data['affirmation_background_color'] = $affirmation->background_color;
			$data['affirmation_text_color'] = $affirmation->text_color;
			$data['affirmation_my_button'] = $affirmation->my_button;
			$data['affirmation_loading'] = $affirmation->loading;
			$data['affirmation_comments'] = $affirmation->comments;
			$data['affirmation_comments_placeholder'] = $affirmation->comments_placeholder;
			$data['affirmation_comments_submit'] = $affirmation->comments_submit;
			$data['affirmation_empty_list'] = $affirmation->empty_list;

			$data['my_affirmation_board_name'] = $my_affirmation->board_name;
			$data['my_affirmation_background_color'] = $my_affirmation->background_color;
			$data['my_affirmation_text_color'] = $my_affirmation->text_color;
			//$data['my_affirmation_subtext_color'] = $my_affirmation->subtext_color;
			$data['my_affirmation_new_button'] = $my_affirmation->new_button;
			$data['my_affirmation_back_button'] = $my_affirmation->back_button;
			$data['my_affirmation_thankful'] = $my_affirmation->thankful;
			$data['my_affirmation_thankful_check'] = $my_affirmation->thankful_check;
			$data['my_affirmation_already_thankful_check'] = $my_affirmation->already_thankful_check;
			$data['my_affirmation_title_header'] = $my_affirmation->title_header;
			$data['my_affirmation_lead_in'] = $my_affirmation->lead_in;
			$data['my_affirmation_max_length'] = $my_affirmation->max_length;
			$data['my_affirmation_max_reason_length'] = $my_affirmation->max_reason_length;
			$data['my_affirmation_reason_lead_in'] = $my_affirmation->reason_lead_in;
			$data['my_affirmation_comments'] = $my_affirmation->comments;
			$data['my_affirmation_comments_placeholder'] = $my_affirmation->comments_placeholder;
			$data['my_affirmation_comments_submit'] = $my_affirmation->comments_submit;
			$data['my_affirmation_new_affirmation'] = $my_affirmation->new_affirmation;
			$data['my_affirmation_edit_affirmation'] = $my_affirmation->edit_affirmation;
			$data['my_affirmation_user_affirmation'] = $my_affirmation->user_affirmation;
			$data['my_affirmation_require_reason'] = $my_affirmation->require_reason;
			$data['my_affirmation_reason'] = $my_affirmation->reason;
			$data['my_affirmation_new_submit'] = $my_affirmation->new_submit;
			$data['my_affirmation_edit_submit'] = $my_affirmation->edit_submit;
			$data['my_affirmation_warning_text'] = $my_affirmation->warning_text;
			$data['my_affirmation_warning_reason'] = $my_affirmation->warning_reason;
			$data['my_affirmation_warning_category'] = $my_affirmation->warning_category;
			$data['my_affirmation_empty_list'] = $my_affirmation->empty_list;

			$data['goal_board_name'] = $goal->board_name;
			$data['goal_description'] = $goal->description;
			$data['goal_background_color'] = $goal->background_color;
			$data['goal_text_color'] = $goal->text_color;
			$data['goal_my_button'] = $goal->my_button;
			$data['goal_loading'] = $goal->loading;
			$data['goal_comments'] = $goal->comments;
			$data['goal_comments_placeholder'] = $goal->comments_placeholder;
			$data['goal_comments_submit'] = $goal->comments_submit;
			$data['goal_empty_list'] = $goal->empty_list;
			$data['goal_time_left'] = $goal->time_left;
			$data['goal_time_left_plural'] = $goal->time_left_plural;
			$data['goal_time_passed'] = $goal->time_passed;
			$data['goal_time_passed_plural'] = $goal->time_passed_plural;
			$data['goal_not_finished'] = $goal->not_finished;
			$data['goal_finished'] = $goal->finished;
			$data['goal_user_lead_in'] = $goal->user_lead_in;

			$data['my_goal_board_name'] = $my_goal->board_name;
			$data['my_goal_background_color'] = $my_goal->background_color;
			$data['my_goal_text_color'] = $my_goal->text_color;			
			$data['my_goal_new_button'] = $my_goal->new_button;
			$data['my_goal_declaration_name'] = $my_goal->declaration_name;
			$data['my_goal_goal_name'] = $my_goal->goal_name;
			$data['my_goal_declaration_example_accomplishable'] = $my_goal->declaration_example_accomplishable;
			$data['my_goal_declaration_example_not_accomplishable'] = $my_goal->declaration_example_not_accomplishable;
			$data['my_goal_goals_example_measurable'] = $my_goal->goal_example_measurable;
			$data['my_goal_goals_example_not_measurable'] = $my_goal->goal_example_not_measurable;
			$data['my_goal_new_lead_in'] = $my_goal->new_lead_in;
			$data['my_goal_declaration_accomplishable_title'] = $my_goal->declaration_accomplishable_title;
			$data['my_goal_declaration_accomplishable_text'] = $my_goal->declaration_accomplishable_text;
			$data['my_goal_goal_measurable_title'] = $my_goal->goal_measurable_title;
			$data['my_goal_goal_measurable_text'] = $my_goal->goal_measurable_text;
			$data['my_goal_goal_reach'] = $my_goal->goal_reach;
			$data['my_goal_start_date'] = $my_goal->start_date;
			$data['my_goal_end_date'] = $my_goal->end_date;
			$data['my_goal_submit'] = $my_goal->submit;
			$data['my_goal_max_length'] = $my_goal->max_length;
			$data['my_goal_back_button'] = $my_goal->back_button;
			$data['my_goal_lead_in'] = $my_goal->lead_in;
			$data['my_goal_progress'] = $my_goal->progress;
			$data['my_goal_not_finished'] = $my_goal->not_finished;
			$data['my_goal_date_made'] = $my_goal->date_made;
			$data['my_goal_passed_time'] = $my_goal->passed_time;
			$data['my_goal_success'] = $my_goal->success;
			$data['my_goal_ending'] = $my_goal->ending;
			$data['my_goal_remaining_time'] = $my_goal->remaining_time;
			$data['my_goal_encouragement'] = $my_goal->encouragement;
			$data['my_goal_update_field'] = $my_goal->update_field;
			$data['my_goal_update_field_placeholder'] = $my_goal->update_field_placeholder;
			$data['my_goal_accomplish_button_instructions'] = $my_goal->accomplish_button_instructions;
			$data['my_goal_accomplish_button_hover'] = $my_goal->accomplish_button_hover;
			$data['my_goal_no_entries'] = $my_goal->no_entries;
			$data['my_goal_warning_invalid_update_number'] = $my_goal->warning_invalid_update_number;
			$data['my_goal_warning_empty_goal'] = $my_goal->warning_empty_goal;
			$data['my_goal_warning_empty_goal_number'] = $my_goal->warning_empty_goal_number;
			$data['my_goal_warning_invalid_number'] = $my_goal->warning_invalid_number;
			$data['my_goal_warning_no_category'] = $my_goal->warning_no_category;
			$data['my_goal_warning_invalid_start_date'] = $my_goal->warning_invalid_start_date;
			$data['my_goal_warning_invalid_end_date'] = $my_goal->warning_invalid_end_date;
			$data['my_goal_warning_invalid_date'] = $my_goal->warning_invalid_date;

			$data['image_board_name'] = $image->board_name;
			$data['image_description'] = $image->description;
			$data['image_background_color'] = $image->background_color;
			$data['image_text_color'] = $image->text_color;
			$data['image_loading'] = $image->loading;
			$data['image_my_button'] = $image->my_button;
			$data['image_comments'] = $image->comments;
			$data['image_comments_placeholder'] = $image->comments_placeholder;
			$data['image_comments_submit'] = $image->comments_submit;
			$data['image_empty_list'] = $image->empty_list;
			$data['image_rating_1'] = $image->rating_1;
			$data['image_rating_2'] = $image->rating_2;
			$data['image_rating_3'] = $image->rating_3;
			$data['image_rating_4'] = $image->rating_4;
			$data['image_rating_5'] = $image->rating_5;

			$data['my_image_board_name'] = $my_image->board_name;
			$data['my_image_background_color'] = $my_image->background_color;
			$data['my_image_text_color'] = $my_image->text_color;
			$data['my_image_new_button'] = $my_image->new_button;
			$data['my_image_back_button'] = $my_image->back_button;
			$data['my_image_loading'] = $my_image->loading;
			$data['my_image_comments'] = $my_image->comments;
			$data['my_image_comments_placeholder'] = $my_image->comments_placeholder;
			$data['my_image_comments_submit'] = $my_image->comments_submit;
			$data['my_image_max_length'] = $my_image->max_length;
			$data['my_image_new_image'] = $my_image->new_image;
			$data['my_image_edit_image'] = $my_image->edit_image;
			$data['my_image_user_text'] = $my_image->user_text;
			$data['my_image_new_submit'] = $my_image->new_submit;
			$data['my_image_edit_submit'] = $my_image->edit_submit;
			$data['my_image_warning_no_image'] = $my_image->warning_no_image;
			$data['my_image_warning_no_category'] = $my_image->warning_no_category;
			$data['my_image_empty_list'] = $my_image->empty_list;

			return $data;
		}
	}
}