<?php

defined('_JEXEC') or die();

class AxsModelVideos extends FOFModel {


	public function &getItemList($overrideLimits = false, $group = '') {
		$this->blacklistFilters(array('category','subcategory','language'));
		$query = $this->buildQuery($overrideLimits);
		if (!$overrideLimits)
		{
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
			$this->list = $this->_getList((string) $query, $limitstart, $limit, $group);
		}
		else
		{
			$this->list = $this->_getList((string) $query, 0, 0, $group);
		}

		return $this->list;
	}


	public function save($data) {
		$this->sanitizeData($data);
		if(is_array($data)) {
			if(!$data['created_on']) {
				$data['created_on'] = date("Y-m-d H:i:s");
			}

			$data['accesslevel'] = !empty($data['accesslevel']) ? implode(',', $data['accesslevel']) : null;
			$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : null;
			$data['access'] = !empty($data['access']) ? implode(',', $data['access']) : null;


			$params = new stdClass();
			$params->access_purchase_type = $data['access_purchase_type'];
			$params->usergroup_purchase_register = !empty($data['usergroup_purchase_register']) ? implode(',', $data['usergroup_purchase_register']) : null;
			$params->userlist_purchase_register = $data['userlist_purchase_register'];
			$params->video_type  = $data['video_type'];
			$params->cover_image = $data['cover_image'];
			$params->use_expiration = $data['use_expiration'];
			$params->custom_id = $data['custom_id'];
			$params->expiration_date = $data['expiration_date'];

			if (isset($data['youtube_id']) && $data['video_type'] == 'youtube'){
				$url = urldecode(rawurldecode($data['youtube_id']));

				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
				if ($matches[1]) {
					$params->youtube_id = $matches[1];
				} else {
					$params->youtube_id = $data['youtube_id'];
				}

			}

			if (isset($data['vimeo_id']) && $data['video_type'] == 'vimeo') {

				if(strpos($data['vimeo_id'],'/')) {
					//$vimeo_id = substr($data['vimeo_id'], strrpos($data['vimeo_id'], '/') + 1);
					$vimeo_id = AxsMedia::getVimeoVideoIdFromUrl($data['vimeo_id']);

					$params->vimeo_id = $vimeo_id;
				} else {
					$params->vimeo_id = $data['vimeo_id'];
				}

			}

			if (isset($data['screencast_id']) && $data['video_type'] == 'screencast') {
				preg_match('/src="([^"]+)"/', $data['screencast_id'], $src);
				if ($src[1]) {
	   				$screencast_id = $src[1];
	   			} else {
	   				$screencast_id = $data['screencast_id'];
	   			}

				$params->screencast_id = $screencast_id;


			}

			if (isset($data['facebook_url']) && $data['video_type'] == 'facebook') {
				$url = urlencode($data['facebook_url']);
				$params->facebook_url = $data['facebook_url'];
			}

			$data['params'] = json_encode($params);

			$data['category'] = !empty($data['category']) ? implode(',', $data['category']) : null;
			$data['subcategory'] = !empty($data['subcategory']) ? implode(',', $data['subcategory']) : null;
		}
		return parent::save($data);
	}

	/**
	 * Sanitize input data before inserting
	 *
	 * @param $data
	 */
	protected function sanitizeData(&$data) {
		
		$data['title'] = trim($data['title']);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$params = json_decode($data['params']);

			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);
			$data['access'] = explode(',', $data['access']);
			if(!$params->access_purchase_type) {
				$data['access_purchase_type'] = 'access';
			} else {
				$data['access_purchase_type'] = $params->access_purchase_type;
			}

			$data['usergroup_purchase_register'] = explode(',', $params->usergroup_purchase_register);
			$data['userlist_purchase_register'] = $params->userlist_purchase_register;

			$data['category'] = explode(',', $data['category']);
			$data['subcategory'] = explode(',', $data['subcategory']);

			$data['video_type'] 	= $params->video_type;
			$data['cover_image'] 	= $params->cover_image;
			$data['layout'] 		= $params->layout;
			$data['youtube_id'] 	= $params->youtube_id;
			$data['screencast_id'] 	= $params->screencast_id;
			$data['vimeo_id'] 		= $params->vimeo_id;
			$data['facebook_url'] 	= $params->facebook_url;
			$data['use_expiration'] = $params->use_expiration;
			$data['custom_id'] = $params->custom_id;
			$data['expiration_date'] = $params->expiration_date;
			return $data;
		}
	}

}