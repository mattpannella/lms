<?php

class AxsModelPayment_gateways extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$billing = new stdClass();

			$billing->currency_id = $data['currency_id'];
			if($billing->currency_id) {
				$currencyParams = new stdClass();
				$currencyParams->id = $billing->currency_id;
				$currencyObject = AxsPayment::getCurrency($currencyParams);
				$billing->currency_code = $currencyObject->code;
				$billing->currency_country = $currencyObject->country;
				$billing->currency_name = $currencyObject->currency;
				$billing->currency_symbol = $currencyObject->symbol;
			}

			if ($data['auto_reprocess_declines'] == "1") {
				$billing->auto_reprocess_declines = true;
			} else {
				$billing->auto_reprocess_declines = false;
			}

			$billing->auto_reprocess_days = $data['auto_reprocess_days'];

			if ($data['weekend_billing'] == "1") {
				$billing->weekend_billing = true;
			} else {
				$billing->weekend_billing = false;
			}

			if ($data['weekend_fallback'] == "1") {
				$billing->weekend_fallback = true;
			} else {
				$billing->weekend_fallback = false;
			}

			if ($data['holiday_billing'] == "1") {
				$billing->holiday_billing = true;
			} else {
				$billing->holiday_billing = false;
			}

			$billing->grace_period = (int)$data['grace_period'];
			if ($data['rewards_active'] == "1") {
				$billing->rewards_active = true;
			} else {
				$billing->rewards_active = false;
			}

			$billing->rewards_quotient = (int)$data['rewards_quotient'];

			if ($data['show_unpaid_alert'] == "1") {
				$billing->show_unpaid_alert = true;
			} else {
				$billing->show_unpaid_alert = false;
			}

			$billing->gateway_type = $data['gateway_type'];

			$billing->converge_production_mode = $data['converge_production_mode'];
			$billing->converge_dev_merchant_id = $data['converge_dev_merchant_id'];
			$billing->converge_dev_user_id = $data['converge_dev_user_id'];
			$billing->converge_dev_account_pin = AxsEncryption::encrypt($data['converge_dev_account_pin'], AxsKeys::getKey('lms'));
			$billing->converge_prod_merchant_id = $data['converge_prod_merchant_id'];
			$billing->converge_prod_user_id = $data['converge_prod_user_id'];
			$billing->converge_prod_account_pin = AxsEncryption::encrypt($data['converge_prod_account_pin'], AxsKeys::getKey('lms'));

			$billing->authnet_production_mode = $data['authnet_production_mode'];
			$billing->authnet_dev_api_login_id = $data['authnet_dev_api_login_id'];
			$billing->authnet_dev_transaction_key = AxsEncryption::encrypt($data['authnet_dev_transaction_key'], AxsKeys::getKey('lms'));
			$billing->authnet_prod_api_login_id = $data['authnet_prod_api_login_id'];
			$billing->authnet_prod_transaction_key = AxsEncryption::encrypt($data['authnet_prod_transaction_key'], AxsKeys::getKey('lms'));

			$billing->stripe_production_mode = $data['stripe_production_mode'];
			$billing->stripe_dev_api_key = AxsEncryption::encrypt($data['stripe_dev_api_key'], AxsKeys::getKey('lms'));
			$billing->stripe_prod_api_key = AxsEncryption::encrypt($data['stripe_prod_api_key'], AxsKeys::getKey('lms'));
			$billing->stripe_dev_public_api_key = $data['stripe_dev_public_api_key'];
			$billing->stripe_prod_public_api_key = $data['stripe_prod_public_api_key'];
			$billing->stripe_hide_zip = $data['stripe_hide_zip'];

			$billing->heartland_production_mode = $data['heartland_production_mode'];
			$billing->heartland_prod_secret_api_key = AxsEncryption::encrypt($data['heartland_prod_secret_api_key'], AxsKeys::getKey('lms'));
			$billing->heartland_prod_public_api_key = $data['heartland_prod_public_api_key'];
			$billing->heartland_dev_secret_api_key = AxsEncryption::encrypt($data['heartland_dev_secret_api_key'], AxsKeys::getKey('lms'));
			$billing->heartland_dev_public_api_key = $data['heartland_dev_public_api_key'];

			$emails = json_decode($data['admin_emails']);
			$admin_emails = array();
			$admin_email_count = is_countable($emails->admin_email) ? count($emails->admin_email) : 0;

			for ($i = 0; $i < $admin_email_count; $i++) {
				$new = new stdClass();
				$new->email = $emails->admin_email[$i];
				$new->name = $emails->admin_name[$i];

				array_push($admin_emails, $new);
			}

			$billing->admin_emails = $admin_emails;

			$data['params'] = json_encode($billing);
		}

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;

			$billing = json_decode($data['params']);

			if ($billing->admin_emails != null) {
				$emails = new stdClass();
				$emails->admin_email = array();
				$emails->admin_name = array();

				foreach ($billing->admin_emails as $admin_email) {
					array_push($emails->admin_email, $admin_email->email);
					array_push($emails->admin_name, $admin_email->name);
				}

				$data['admin_emails'] = json_encode($emails);
			}

			$data['currency_id'] = $billing->currency_id;

			if ($billing->auto_reprocess_declines == true) {
				$data['auto_reprocess_declines'] = "1";
			} else {
				$data['auto_reprocess_declines'] = "0";
			}

			$data['auto_reprocess_days'] = $billing->auto_reprocess_days;

			if ($billing->weekend_billing == true) {
				$data['weekend_billing'] = "1";
			} else {
				$data['weekend_billing'] = "0";
			}

			if ($billing->weekend_fallback == "Friday") {
				$data['weekend_fallback'] = "1";
			} else {
				$data['weekend_fallback'] = "0";
			}

			if ($billing->holiday_billing == true) {
				$data['holiday_billing'] = "1";
			} else {
				$data['holiday_billing'] = "0";
			}

			$data['grace_period'] = $billing->grace_period;
			if ($billing->rewards_active == true) {
				$data['rewards_active'] = "1";
			} else {
				$data['rewards_active'] = "0";
			}

			$data['rewards_quotient'] = $billing->rewards_quotient;

			if ($billing->show_unpaid_alert == true) {
				$data['show_unpaid_alert'] = "1";
			} else {
				$data['show_unpaid_alert'] = "0";
			}

			$data['gateway_type'] = $billing->gateway_type;

			$data['converge_production_mode'] = $billing->converge_production_mode;
			$data['converge_dev_merchant_id'] = $billing->converge_dev_merchant_id;
			$data['converge_dev_user_id'] = $billing->converge_dev_user_id;
			$data['converge_dev_account_pin'] = AxsEncryption::decrypt($billing->converge_dev_account_pin, AxsKeys::getKey('lms'));
			$data['converge_prod_merchant_id'] = $billing->converge_prod_merchant_id;
			$data['converge_prod_user_id'] = $billing->converge_prod_user_id;
			$data['converge_prod_account_pin'] = AxsEncryption::decrypt($billing->converge_prod_account_pin, AxsKeys::getKey('lms'));

			$data['authnet_production_mode'] = $billing->authnet_production_mode;
			$data['authnet_dev_api_login_id'] = $billing->authnet_dev_api_login_id;
			$data['authnet_dev_transaction_key'] = AxsEncryption::decrypt($billing->authnet_dev_transaction_key, AxsKeys::getKey('lms'));
			$data['authnet_prod_api_login_id'] = $billing->authnet_prod_api_login_id;
			$data['authnet_prod_transaction_key'] = AxsEncryption::decrypt($billing->authnet_prod_transaction_key, AxsKeys::getKey('lms'));

			$data['stripe_production_mode'] = $billing->stripe_production_mode;
			$data['stripe_dev_api_key'] = AxsEncryption::decrypt($billing->stripe_dev_api_key, AxsKeys::getKey('lms'));
			$data['stripe_prod_api_key'] = AxsEncryption::decrypt($billing->stripe_prod_api_key, AxsKeys::getKey('lms'));
			$data['stripe_dev_public_api_key'] = $billing->stripe_dev_public_api_key;
			$data['stripe_prod_public_api_key'] = $billing->stripe_prod_public_api_key;
			$data['stripe_hide_zip'] = $billing->stripe_hide_zip;

			$data['heartland_production_mode'] = $billing->heartland_production_mode;
			$data['heartland_prod_secret_api_key'] = AxsEncryption::decrypt($billing->heartland_prod_secret_api_key, AxsKeys::getKey('lms'));
			$data['heartland_prod_public_api_key'] = $billing->heartland_prod_public_api_key;
			$data['heartland_dev_secret_api_key'] = AxsEncryption::decrypt($billing->heartland_dev_secret_api_key, AxsKeys::getKey('lms'));
			$data['heartland_dev_public_api_key'] = $billing->heartland_dev_public_api_key;

			return $data;
		}
	}

	public function onBeforeDelete($id, $table) {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_brands');
        $query->where("JSON_EXTRACT(data, '$.payment_gateway_id') = " . $db->quote($id));
        $db->setQuery($query);
        $brands = $db->loadObjectList();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_payment_gateways');
        $query->where("id = " . $db->quote($id));
        $db->setQuery($query);
        $payment_gateway = $db->loadObject();

		if (!empty($brands)) {
			$msg = "Delete failed, payment gateway $payment_gateway->name is still being used in the following brands:";
			$msg .= "<ul>";
			foreach ($brands as $brand) {
				$msg .= "<li><a href='/administrator/index.php?option=com_axs&view=brand&id=$brand->id'>$brand->site_title</a></li>";
			}
			$msg .= "</ul>";
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return;
		}

		return true;
	}
}

