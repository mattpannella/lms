<?php

class AxsModelCategories extends FOFModel {	
	public function save($data) {
		if(is_array($data)) {
			//$data['brands'] = implode(",", $data['brands']);
			if ($data['title_text'] == "") {
				$data['title_text'] = $data['title'];
			}
			if($data['parent_id']) {
				$data['parent_id'] = implode(',', $data['parent_id']);
			} else {
				$data['parent_id'] = 0;
			}

			$data['accesslevel'] = !empty($data['accesslevel']) ? implode(',', $data['accesslevel']) : null;
			$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : null;
		}
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;	
			if($data['parent_id']) {
				$data['parent_id'] = explode(',', $data['parent_id']);
			} else {
				$data['parent_id'] = 0;
			}

			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);

			return $data;
		}
	}
}