<?php

defined('_JEXEC') or die;

class AxsModelVirtual_classrooms extends FOFModel {

	public function save($data) {
		$data['accesslevel'] = !empty($data['accesslevel']) ? implode(',', $data['accesslevel']) : null;
		$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : null;
		$data['created'] = date('Y-m-d H:i:s');
		$data['voiceBridge'] = rand(10000,99999);

		$params = new stdClass();

		$params->meetingType = $data['meetingType'];
		$params->zoomUrl = $data['zoomUrl'];
		$params->webexUrl = $data['webexUrl'];
		$params->gotomeetingUrl = $data['gotomeetingUrl'];
		$params->googleUrl = $data['googleUrl'];
		$params->joinmeUrl = $data['joinmeUrl'];
		$params->otherUrl = $data['otherUrl'];
		$params->settings_type = $data['settings_type'];
		$params->default_layout = $data['default_layout'];
		$params->muteOnStart = $data['muteOnStart'];
		$params->allowModsToUnmuteUsers = $data['allowModsToUnmuteUsers'];
		$params->webcamsOnlyForModerator = $data['webcamsOnlyForModerator'];
		$params->lockSettingsDisableCam = $data['lockSettingsDisableCam'];
		$params->lockSettingsDisableMic = $data['lockSettingsDisableMic'];
		$params->lockSettingsDisablePrivateChat = $data['lockSettingsDisablePrivateChat'];
		$params->lockSettingsDisablePublicChat = $data['lockSettingsDisablePublicChat'];
		$params->lockSettingsDisableNote = $data['lockSettingsDisableNote'];
		$params->welcome = $data['welcome'];
		$params->moderatorOnlyMessage = $data['moderatorOnlyMessage'];
		$params->useCustomLogo = $data['useCustomLogo'];
		$params->customLogo = $data['customLogo'];
		$params->time = $data['time'];
		$params->time_zone = $data['time_zone'];
		$params->zoomHost = $data['zoomHost'];
		$params->zoomIntegration = $data['zoomIntegration'];


		if(!$data['maxParticipants']) {
			$data['maxParticipants'] = -1;
		}
		if(!$data['id']) {
			$data['moderatorPW'] = AxsLMS::createUniqueID();
			$data['attendeePW'] = AxsLMS::createUniqueID();
		}
		if(!$data['record']) {
			$data['record'] = 'false';
		} else {
			$data['record'] = 'true';
		}


		if($data['meetingType'] == 'zoom' && !empty(AxsZoomApi::getInstance()) && $data['zoomIntegration'] == 'api') {

            if(!$data['zoomPassword']) {
				$data['zoomPassword'] = AxsLMS::createPassword();
			}
			$params->zoomPassword = $data['zoomPassword'];

			if($params->time && $data['date']) {
				$setDate = true;
				$scheduleType = AxsZoomApi::MEETING_SCHEDULE_DEFERRED;
			} else {
				$scheduleType = AxsZoomApi::MEETING_SCHEDULE_RECURRING_NO_TIME;
			}

			$meetingTopic = $data['meetingName'];

			$apiParams = [
				'topic' => $meetingTopic,
				'timezone' => $params->time_zone,
				'password' => $data['zoomPassword'],
				'agenda' => $data['description'],
				'type' => $scheduleType
			];

			if($setDate) {
				$virtualClassTime = json_decode($params->time);
				if($virtualClassTime->minute == '0') {
					$virtualClassTime->minute = '00';
				}
				$time = $virtualClassTime->hour.':'.$virtualClassTime->minute.' '.$virtualClassTime->ampm;
				$timeFormated = date("H:i:s", strtotime($time));
				$apiParams['start_time'] = $data['date'].'T'.$timeFormated;
			}

			$createNewZoomMeeting = false;

			if(!empty($data['zoomHost'])) {

				// Keep track of whether or not we've changed primary hosts
				$settings = json_decode($this->getTable()->get('settings'));

				if($settings->zoomHost != $data['zoomHost']) {
					$createNewZoomMeeting = true;
				}

				$meetingHost = $data['zoomHost'];
			} else {
				// We need a host for a meeting to create or update it.
				JFactory::getApplication()->enqueueMessage('You must supply a host for your Zoom meeting.', 'error');

				return false;
			}

			$alternativeHosts = null;

			if(!empty($data['zoomAltHosts'])) {
				// Create the Zoom alternative hosts string
				$alternativeHosts = implode(",", $data['zoomAltHosts']);

				$apiParams['settings'] = [
					'alternative_hosts' => $alternativeHosts
				];

				$params->zoomAltHosts = $alternativeHosts;
			}

			if(!$data['meetingId']) {
				// Meeting doesn't exist yet: create a brand new Zoom meeting
				$data['meetingId'] = AxsSSO::generateUUID();

				$params->zoomUrl = AxsZoomMeetings::createZoomMeetingWithUUID($data['meetingId'], $meetingHost, $apiParams);
			} else {
				// Meeting exists: We're changing it to a Zoom meeting

				// Grab any existing Zoom Meeting information from the database
				$zoomMeeting = AxsZoomMeetings::getZoomMeetingByUUID($data['meetingId']);

				if(!empty($zoomMeeting)) {

					// Update the existing meeting with new information
					// We'll need to delete the meeting and update it by creating a brand new one if the host changes.
					if($createNewZoomMeeting) {

						AxsZoomMeetings::deleteZoomMeetingViaApi($zoomMeeting->zoom_id);
						AxsZoomMeetings::deleteZoomMeetingByUUID($data['meetingId']);

						$params->zoomUrl = AxsZoomMeetings::createZoomMeetingWithUUID($data['meetingId'], $meetingHost, $apiParams);
					}

					AxsZoomMeetings::updateZoomMeetingViaApi($zoomMeeting->zoom_id, $apiParams);
				} else {

					// We're dealing with a new meeting, so create one via the API
					$params->zoomUrl = AxsZoomMeetings::createZoomMeetingWithUUID($data['meetingId'], $meetingHost, $apiParams);
				}
			}
		} else {
			if(!$data['meetingId']) {
				$data['meetingId'] = AxsSSO::generateUUID();
			} else {
				// Make sure to delete any existing Zoom meeting when changing meeting types
				$zoomMeeting = AxsZoomMeetings::getZoomMeetingByUUID($data['meetingId']);

				if(!empty($zoomMeeting)) {
					AxsZoomMeetings::deleteZoomMeetingViaApi($zoomMeeting->zoom_id);
					AxsZoomMeetings::deleteZoomMeetingByUUID($data['meetingId']);

					$params->zoomUrl = "";
					$params->zoomHost = "";
					$params->zoomAltHosts = "";
				}
			}
		}

		$data['settings'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data = $data;
			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);

			$params = json_decode($data['settings']);

			$data['meetingType'] = $params->meetingType;
			$data['zoomUrl'] = $params->zoomUrl;
			$data['zoomPassword'] = $params->zoomPassword;
			$data['webexUrl'] = $params->webexUrl;
			$data['gotomeetingUrl'] = $params->gotomeetingUrl;
			$data['googleUrl'] = $params->googleUrl;
			$data['joinmeUrl'] = $params->joinmeUrl;
			$data['otherUrl'] = $params->otherUrl;
			$data['time'] = $params->time;
			$data['time_zone'] = $params->time_zone;
			$data['settings_type'] = $params->settings_type;
			$data['default_layout'] = $params->default_layout;
			$data['muteOnStart'] = $params->muteOnStart;
			$data['allowModsToUnmuteUsers'] = $params->allowModsToUnmuteUsers;
			$data['webcamsOnlyForModerator'] = $params->webcamsOnlyForModerator;
			$data['lockSettingsDisableCam'] = $params->lockSettingsDisableCam;
			$data['lockSettingsDisableMic'] = $params->lockSettingsDisableMic;
			$data['lockSettingsDisablePrivateChat'] = $params->lockSettingsDisablePrivateChat;
			$data['lockSettingsDisablePublicChat'] = $params->lockSettingsDisablePublicChat;
			$data['lockSettingsDisableNote'] = $params->lockSettingsDisableNote;
			$data['welcome'] = $params->welcome;
			$data['moderatorOnlyMessage'] = $params->moderatorOnlyMessage;
			$data['useCustomLogo'] = $params->useCustomLogo;
			$data['customLogo'] = $params->customLogo;
			$data['zoomIntegration'] = $params->zoomIntegration;

			// Load the Zoom stuff if we're using the Zoom API
			if(!empty(AxsZoomApi::getInstance())) {
				$data['zoomHost'] = $params->zoomHost;
				$data['zoomAltHosts'] = explode(',', $params->zoomAltHosts);
			}

			if($data['record'] == 'false') {
				$data['record'] = 0;
			} else {
				$data['record'] = 1;
			}
			if(!$data['creator']) {
				$data['creator'] = JFactory::getUser()->id;
			}
			return $data;
		}
	}

	public function &onBeforeDelete(&$id, &$table) {
		$table->load($id);
		$meetingId = $table->get('meetingId');
		$settings = json_decode($table->get('settings'));

		if($settings->meetingType == 'zoom' && !empty(AxsZoomApi::getInstance())) {

			$zoomMeetingData = AxsZoomMeetings::getZoomMeetingByUUID($meetingId);

			// Response is just a normal object, so we have a valid row of data
			if($zoomMeetingData->zoom_id) {
				// Delete the Zoom meeting at the API level
				AxsZoomMeetings::deleteZoomMeetingViaApi($zoomMeetingData->zoom_id);

				// Now, delete the corresponding entry in axs_zoom_meetings
				AxsZoomMeetings::deleteZoomMeetingByZoomID($zoomMeetingData->zoom_id);
			}
		}

		return true;
	}
}
