<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsModelTeams extends FOFModel {
	public function save($data) {

		$this->sanitizeData($data);

		if ($data['id'] == 0) {
			//It's a new entry, record the date it was created
			$data['date_created'] = date("Y-m-d H:i:s");
		}
		$data['user_groups'] = !empty($data['user_groups']) ? implode(',', $data['user_groups']) : null;
		$params = new stdClass();
		$params->allow_adding_users = $data['allow_adding_users'];
		$params->allow_removing_users_from_team = $data['allow_removing_users_from_team'];
		$params->allow_removing_users_from_system = $data['allow_removing_users_from_system'];
		
        // Managed user groups params
        $params->allow_managing_users_groups = $data['allow_managing_users_groups'];

        // Managed user groups come in an array with one element, a string of managed user group IDs
        $managedUserGroups = $data['managed_user_groups'];

        // Now, because the rest of the software that uses this data expects it to be an array, explode the ID string
        $params->user_groups = explode(',', $managedUserGroups[0]);
		
        // Courses params
        $params->allow_assigning_courses = $data['allow_assigning_courses'];
		$params->course_ids = implode(',' , $data['course_ids']);

        // Events params
		$params->allow_assigning_events = $data['allow_assigning_events'];

		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	/**
	 * Sanitize input data before inserting
	 *
	 * @param $data
	 */
	protected function sanitizeData(&$data) {
		
		$data['title'] = trim($data['title']);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;

			$params = json_decode($data['params']);
            $managedUserGroups = implode(',', $params->user_groups);

			$data['allow_adding_users'] = $params->allow_adding_users;
			$data['allow_removing_users_from_team'] = $params->allow_removing_users_from_team;
			$data['allow_removing_users_from_system'] = $params->allow_removing_users_from_system;
			$data['allow_managing_users_groups'] = $params->allow_managing_users_groups;
			$data['managed_user_groups'] = $managedUserGroups;
			$data['allow_assigning_courses'] = $params->allow_assigning_courses;
			$data['course_ids'] = $params->course_ids;
			$data['allow_assigning_events'] = $params->allow_assigning_events;

            return $data;
		}
	}
}