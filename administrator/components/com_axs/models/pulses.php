<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1); 

class AxsModelPulses extends FOFModel {	
	public function save($data) {

		if ($data['name'] == "") {
			$lower = strtolower($data['text']);
			$data['name'] = preg_replace("/[^a-z0-9_]+/", "", $lower);
		}

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			return $data;
		}


	}
}