<?php

defined('_JEXEC') or die;

class AxsModelAvatars extends FOFModel {

    public function save($data) {

        $avatarRootPath = JPATH_ROOT . '/' . $data['path'];

        $params = new stdClass();

        $params->file_metadata = array();

        $params->file_metadata['size'] = (int)filesize($avatarRootPath);
        $params->file_metadata['type'] = mime_content_type($avatarRootPath);
        $params->file_metadata['error'] = 0;
        $params->file_metadata['name'] = basename($avatarRootPath);

        $data['params'] = json_encode($params);

        return parent::save($data);
    }
}