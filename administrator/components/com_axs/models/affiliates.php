<?php

class AxsModelAffiliates extends FOFModel {	
	public function save($data) {		
		$params = new stdClass();
		$params->subscription_commision_type = $data['subscription_commision_type'];
		$params->subscription_commision_flat = $data['sales_commision'];
		$params->subscription_commision_percentage = $data['subscription_commision_percentage'];

		$params->store_commision_type = $data['store_commision_type'];
		$params->store_commision_flat = $data['store_commision_flat'];
		$params->store_commision_percentage = $data['store_commision_percentage'];

		$params->event_commision_type = $data['event_commision_type'];
		$params->event_commision_flat = $data['event_commision_flat'];
		$params->event_commision_percentage = $data['event_commision_percentage'];

		$params->course_commision_type = $data['course_commision_type'];
		$params->course_commision_flat = $data['course_commision_flat'];
		$params->course_commision_percentage = $data['course_commision_percentage'];

		$data['params'] = json_encode($params);

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$data['subscription_commision_type'] = $params->subscription_commision_type;
			$data['subscription_commision_percentage'] = $params->subscription_commision_percentage;

			$data['store_commision_type'] = $params->store_commision_type;
			$data['store_commision_flat'] = $params->store_commision_flat;
			$data['store_commision_percentage'] = $params->store_commision_percentage;

			$data['event_commision_type'] = $params->event_commision_type;
			$data['event_commision_flat'] = $params->event_commision_flat;
			$data['event_commision_percentage'] = $params->event_commision_percentage;

			$data['course_commision_type'] = $params->course_commision_type;
			$data['course_commision_flat'] = $params->course_commision_flat;
			$data['course_commision_percentage'] = $params->course_commision_percentage;
					
			return $data;
		}
	}
}