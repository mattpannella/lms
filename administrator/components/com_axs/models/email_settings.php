<?php

class AxsModelEmail_settings extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$params = new stdClass();

			$params->email_custom_smtp = $data['email_custom_smtp'];
			$params->sitename = $data['email_sitename'];
			$params->fromname = $data['email_fromname'];
			$params->replytoname = $data['email_replytoname'];

			if( ($params->email_custom_smtp == '1' || !isset($params->email_custom_smtp) ) && $data['email_smtpuser']) {
				$params->mailfrom = $data['email_mailfrom'];
				$params->smtpauth = $data['email_smtpauth'];
				$params->smtpuser = $data['email_smtpuser'];
				$params->smtphost = $data['email_smtphost'];
				$params->replyto = $data['email_replyto'];
				$params->smtpsecure = $data['email_smtpsecure'];
				$params->smtpport = $data['email_smtpport'];
				$params->smtppass = AxsEncryption::encrypt($data['email_smtppass'], AxsKeys::getKey('lms'));
			} else {
				$params->mailfrom = 'noreply@tovutimail.com';
				$params->smtpauth = '1';
				$params->smtpuser = 'tovuti85';
				$params->smtphost = 'outbound.mailhop.org';
				$params->replyto = 'noreply@tovutimail.com';
				$params->smtpsecure = 'ssl';
				$params->smtpport = '465';
				$params->smtppass = AxsEncryption::encrypt('TOV-321!', AxsKeys::getKey('lms'));
				$params->email_custom_smtp = '0';
			}

			$data['params'] = json_encode($params);	
		}

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;
			$params = json_decode($data['params']);
			$params->smtppass = AxsEncryption::decrypt($params->smtppass, AxsKeys::getKey('lms'));

			$data['email_mailfrom'] = $params->mailfrom;
			$data['email_fromname'] = $params->fromname;
			$data['email_smtpauth'] = $params->smtpauth;
			$data['email_smtpuser'] = $params->smtpuser;
			$data['email_smtppass'] = $params->smtppass;
			$data['email_smtphost'] = $params->smtphost;
			$data['email_replyto'] = $params->replyto;
			$data['email_replytoname'] = $params->replytoname;
			$data['email_smtpsecure'] = $params->smtpsecure;
			$data['email_smtpport'] = $params->smtpport;
			$data['email_sitename'] = $params->sitename;


			$data['email_custom_smtp'] = $params->email_custom_smtp;
			$data['email_sitename'] = $params->sitename;
			$data['email_fromname'] = $params->fromname;
			$data['email_replytoname'] = $params->replytoname;

			if( ($params->email_custom_smtp == '1'|| !isset($params->email_custom_smtp)) && $params->smtpuser != 'tovuti85' && $params->smtppass != 'TOV-321!') {
				$data['email_mailfrom'] = $params->mailfrom;
				$data['email_smtpauth'] = $params->smtpauth;
				$data['email_smtpuser'] = $params->smtpuser;
				$data['email_smtphost'] = $params->smtphost;
				$data['email_replyto'] = $params->replyto;
				$data['email_smtpsecure'] = $params->smtpsecure;
				$data['email_smtpport'] = $params->smtpport;
				$data['email_smtppass'] = $params->smtppass;
				$data['email_custom_smtp'] = '1';
			} else {
				$data['email_mailfrom'] = '';
				$data['email_smtpauth'] = '';
				$data['email_smtpuser'] = '';
				$data['email_smtphost'] = '';
				$data['email_replyto'] = '';
				$data['email_smtpsecure'] = '';
				$data['email_smtpport'] = '';
				$data['email_smtppass'] = '';
				$data['email_custom_smtp'] = '0';
			}

			return $data;
		}
	}

	public function onBeforeDelete($id, $table) {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_brands');
        $query->where("JSON_EXTRACT(data, '$.email_settings_id') = " . $db->quote($id));
        $db->setQuery($query);
        $brands = $db->loadObjectList();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_email_settings');
        $query->where("id = " . $db->quote($id));
        $db->setQuery($query);
        $email_setting = $db->loadObject();

		if (!empty($brands)) {
			$msg = "Delete failed, email setting $email_setting->name is still being used in the following brands:";
			$msg .= "<ul>";
			foreach ($brands as $brand) {
				$msg .= "<li><a href='/administrator/index.php?option=com_axs&view=brand&id=$brand->id'>$brand->site_title</a></li>";
			}
			$msg .= "</ul>";
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return;
		}

		return true;
	}
}
