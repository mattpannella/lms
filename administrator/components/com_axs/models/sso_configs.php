<?php

defined('_JEXEC') or die;

class AxsModelSso_configs extends FOFModel {

	public function save($data) {
		$data['field_mapping'] = json_encode($data['field_mapping']);
		$data['group_mapping'] = json_encode($data['group_mapping']);
		$params = new stdClass();
		$params->sp_issuer = $data['sp_issuer'];
		$params->enable_field_mapping = $data['enable_field_mapping'];
		$params->enable_group_mapping = $data['enable_group_mapping'];
		$params->enable_update_group_mapping = $data['enable_update_group_mapping'];
		$params->enable_update_field_mapping = $data['enable_update_field_mapping'];
		$params->default_user_group = $data['default_user_group'];
		$params->add_link = $data['add_link'];
		$params->add_to_menu = $data['add_to_menu'];
		$params->link_name = $data['link_name'];
		$params->oauth_idp = $data['oauth_idp'];
		$params->cognito_app_domain = $data['cognito_app_domain'];
		$params->oauth_callback_url = $data['oauth_callback_url'];
		$params->oauth_authorize_endpoint = $data['oauth_authorize_endpoint'];
		$params->oauth_token_endpoint = $data['oauth_token_endpoint'];
		$params->oauth_userinfo_endpoint = $data['oauth_userinfo_endpoint'];
		$params->email_attribute = $data['email_attribute'];
		$params->oauth_app_name = $data['oauth_app_name'];
		$params->oauth_client_id = $data['oauth_client_id'];
		$params->oauth_client_secret = $data['oauth_client_secret'];
		$params->oauth_app_scopes = $data['oauth_app_scopes'];
		$params->saml_idp = $data['saml_idp'];
		$params->use_openid_connect = $data['use_openid_connect'];

		if(!$params->sp_issuer) {
			$params->sp_issuer = AxsSSO::generateUUID();
		}

		//defaults
		$data['binding']        = 'HttpRedirect';
		$data['enable_email']   = 1;
		$data['name_id_format'] = 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress';
		$id = $data['link_id'];
		$params->oauth_idp = $data['oauth_idp'];


        if($data['id']) {
        	$config = AxsSSO::getSAMLConfiguration($data['id']);
        	$configParams = json_decode($config['params']);
        	$params->last_menu = $configParams->last_menu;
        }

        if(!$configParams->last_menu && $params->add_link) {
        	$params->last_menu = $params->add_to_menu;
        }

        $menu_item = new stdClass();
        $menu_item->link_name = $params->link_name;
        $menu_item->config_id = $id;

        if($params->last_menu != $params->add_to_menu) {
        	$menu_item->menu_type = $params->last_menu;
        	$lastMenuItem = AxsSSO::checkMenuItem($menu_item);
        	if($lastMenuItem) {
				AxsSSO::deleteMenuItem($lastMenuItem->id);
			}
        }

        $menu_item->menu_type = $params->add_to_menu;

		$existingMenuItem = AxsSSO::checkMenuItem($menu_item);

		if($existingMenuItem && $existingMenuItem->menutype != $params->add_to_menu) {
			AxsSSO::deleteMenuItem($existingMenuItem->id);
			AxsSSO::setMenuItem($menu_item);
		}
		if($existingMenuItem && !$params->add_link) {
			AxsSSO::deleteMenuItem($existingMenuItem->id);
		}
		if(!$existingMenuItem && $params->add_link) {
			AxsSSO::setMenuItem($menu_item);
		}

		if($existingMenuItem && $existingMenuItem->title != $params->link_name && $params->add_link) {
			$existingMenuItem->title = $params->link_name;
			$db = JFactory::getDbo();
			$db->updateObject('#__menu',$existingMenuItem,'id');
		}

		if($params->add_link) {
			$params->last_menu = $params->add_to_menu;
		}
		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			$data['sp_issuer'] = AxsSSO::generateUUID();
			return $data;
		} else {
			$data = $data;
			$params = json_decode($data['params']);

			if(!empty($params)) {

				if(empty($params->sp_issuer)) {
					$params->sp_issuer = AxsSSO::generateUUID();
				}

				$data['sp_issuer'] = $params->sp_issuer;
				$data['enable_field_mapping'] = $params->enable_field_mapping;
				$data['enable_group_mapping'] = $params->enable_group_mapping;
				$data['enable_update_group_mapping'] = $params->enable_update_group_mapping;
				$data['enable_update_field_mapping'] = $params->enable_update_field_mapping;
				$data['default_user_group'] = $params->default_user_group;
				$data['add_link'] = $params->add_link;
				$data['add_to_menu'] = $params->add_to_menu;
				$data['link_name'] = $params->link_name;
				$data['oauth_idp'] = $params->oauth_idp;
				$data['cognito_app_domain'] = $params->cognito_app_domain;
				$data['oauth_authorize_endpoint'] = $params->oauth_authorize_endpoint;
				$data['oauth_token_endpoint'] = $params->oauth_token_endpoint;
				$data['oauth_userinfo_endpoint'] = $params->oauth_userinfo_endpoint;
				$data['email_attribute'] = $params->email_attribute;
				$data['oauth_app_name'] = $params->oauth_app_name;
				$data['oauth_client_id'] = $params->oauth_client_id;
				$data['oauth_client_secret'] = $params->oauth_client_secret;
				$data['oauth_app_scopes'] = $params->oauth_app_scopes;
				$data['oauth_callback_url'] = $params->oauth_callback_url;
				$data['saml_idp'] = $params->saml_idp;
				$data['use_openid_connect'] = $params->use_openid_connect;
			}
			return $data;
		}
	}
}