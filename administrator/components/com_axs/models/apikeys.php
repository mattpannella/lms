<?php

defined('_JEXEC') or die;

class AxsModelApikeys extends FOFModel {

	public function save($data) {
		if(is_array($data)) {
			
			$permissions = new stdClass();			
			$permissions->all_permissions = $data['all_permissions'];
			$permissions->users = $data['users'];
			$permissions->awards = $data['awards'];
			$permissions->courses = $data['courses'];
			$permissions->teams = $data['teams'];
			$permissions->events = $data['events'];
			$permissions->meetings = $data['meetings'];
			$permissions->checklists = $data['checklists'];
			$permissions->assignments = $data['assignments'];
			$permissions->quizzes = $data['quizzes'];
			$permissions->certificate_templates = $data['certificate_templates'];
			$permissions->reports = $data['reports'];
            $permissions->subscriptions = $data['subscriptions'];

			$data['permissions'] = json_encode($permissions);
			$data['date_modified'] = date('Y-m-d H:i:s');
			
			if(!$data['id']) {
				$secret_key = 'api_sk_'.str_replace('-','',AxsSSO::generateUUID());
				$data['public_key'] = 'api_pk_'.str_replace('-','',AxsSSO::generateUUID());
				$data['secret_key'] = password_hash($secret_key, PASSWORD_BCRYPT);
				$key = AxsKeys::getKey('api'); 
				$data['encrypted_keys'] = AxsEncryption::encrypt($secret_key, $key);
			}			
		}
			return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {			
			$permissions = json_decode($data['permissions']);

			$data['all_permissions'] = $permissions->all_permissions;
			$data['users'] = $permissions->users;
			$data['awards'] = $permissions->awards;
			$data['courses'] = $permissions->courses;
			$data['teams'] = $permissions->teams;
			$data['events'] = $permissions->events;
			$data['meetings'] = $permissions->meetings;
			$data['checklists'] = $permissions->checklists;
			$data['assignments'] = $permissions->assignments;
			$data['quizzes'] = $permissions->quizzes;
			$data['certificate_templates'] = $permissions->certificate_templates;
			$data['reports'] = $permissions->reports;
            $data['subscriptions'] = $permissions->subscriptions;

			return $data;			
		}
	}
}