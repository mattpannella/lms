<?php

defined('_JEXEC') or die;

class AxsModelLearner_dashboards extends FOFModel {

	public function save($data) {
		$params = new stdClass();
		$params->show_badges = $data['show_badges'];
		$params->badges_text = $data['badges_text'];
		$params->show_certificates = $data['show_certificates'];
		$params->certificates_text = $data['certificates_text'];
		$params->show_leaderboard = $data['show_leaderboard'];
		$params->leaderboard_text = $data['leaderboard_text'];
		$params->show_courses = $data['show_courses'];
		$params->courses_text = $data['courses_text'];
		$params->show_checklist = $data['show_checklist'];
		$params->checklist_text = $data['checklist_text'];
		$params->show_events = $data['show_events'];
		$params->events_text = $data['events_text'];
		$params->show_header_message = $data['show_header_message'];
		$params->header_message_text = $data['header_message_text'];
		$params->header_message_text_size = $data['header_message_text_size'];
		$params->header_message_text_font = $data['header_message_text_font'];
		$params->show_profile_cover = $data['show_profile_cover'];
		$params->cover_default = $data['cover_default'];
		$params->enable_team_lead_dashboard = $data['enable_team_lead_dashboard'];
		$params->activity_report = $data['activity_report'];
		$params->show_learners_dashboard = $data['show_learners_dashboard'];
		$params->show_team_members = $data['show_team_members'];
		$params->show_team_leaderboard = $data['show_team_leaderboard'];
		$params->show_team_reports = $data['show_team_reports'];
		$params->show_activity_report = $data['show_activity_report'];
		$params->show_attendance = $data['show_attendance'];
		$params->show_transcript = $data['show_transcript'];
		$params->show_message_module = $data['show_message_module'];
		$params->show_checklists_completed = $data['show_checklists_completed'];
		$params->show_courses_completed = $data['show_courses_completed'];
		$params->team_header_background_color = $data['team_header_background_color'];
		$params->team_header_text_color = $data['team_header_text_color'];
		$params->team_header_background_image = $data['team_header_background_image'];
		$params->show_team_leaderboard_amount = $data['show_team_leaderboard_amount'];
		$params->show_team_leaderboard_max = $data['show_team_leaderboard_max'];
		$params->show_leaderboard_user_points = $data['show_leaderboard_user_points'];
		$params->leaderboard_team_only = $data['leaderboard_team_only'];
		$params->show_team_learner_summary_report = $data['show_team_learner_summary_report'];
		$params->team_learner_summary_report = $data['team_learner_summary_report'];



		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data = $data;
			$params = json_decode($data['params']);
			$data['show_badges'] = $params->show_badges;
			$data['badges_text'] = $params->badges_text;
			$data['show_certificates'] = $params->show_certificates;
			$data['certificates_text'] = $params->certificates_text;
			$data['show_leaderboard'] = $params->show_leaderboard;
			$data['leaderboard_text'] = $params->leaderboard_text;
			$data['show_courses'] = $params->show_courses;
			$data['courses_text'] = $params->courses_text;
			$data['show_events'] = $params->show_events;
			$data['events_text'] = $params->events_text;
			$data['show_checklist'] = $params->show_checklist;
			$data['checklist_text'] = $params->checklist_text;
			$data['show_header_message'] = $params->show_header_message;
			$data['header_message_text'] = $params->header_message_text;
			$data['header_message_text_size'] = $params->header_message_text_size;
			$data['header_message_text_font'] = $params->header_message_text_font;
			$data['show_profile_cover'] = $params->show_profile_cover;
			$data['cover_default'] = $params->cover_default;
			$data['enable_team_lead_dashboard'] = $params->enable_team_lead_dashboard;
			$data['activity_report'] = $params->activity_report;
			$data['show_learners_dashboard'] = $params->show_learners_dashboard;
			$data['show_team_members'] = $params->show_team_members;
			$data['show_team_leaderboard'] = $params->show_team_leaderboard;
			$data['show_team_reports'] = $params->show_team_reports;
			$data['show_activity_report'] = $params->show_activity_report;
			$data['show_attendance'] = $params->show_attendance;
			$data['show_transcript'] = $params->show_transcript;
			$data['show_message_module'] = $params->show_message_module;
			$data['show_checklists_completed'] = $params->show_checklists_completed;
			$data['show_courses_completed'] = $params->show_courses_completed;
			$data['team_header_background_color'] = $params->team_header_background_color;
			$data['team_header_text_color'] = $params->team_header_text_color;
			$data['team_header_background_image'] = $params->team_header_background_image;
			$data['show_team_leaderboard_amount'] = $params->show_team_leaderboard_amount;
			$data['show_team_leaderboard_max'] = $params->show_team_leaderboard_max;
			$data['show_leaderboard_user_points'] = $params->show_leaderboard_user_points;
			$data['leaderboard_team_only'] = $params->leaderboard_team_only;
			$data['show_team_learner_summary_report'] = $params->show_team_learner_summary_report;
			$data['team_learner_summary_report'] = $params->team_learner_summary_report;

			return $data;
		}
	}
}