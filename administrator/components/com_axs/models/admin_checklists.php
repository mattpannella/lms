<?php

class AxsModelAdmin_checklists extends FOFModel {

	public function save($data) {
		$params = new stdClass();
		$params->Quick_Start_Edit = $data['Quick_Start_Edit'];
		$params->Interactive_Content_Creation = $data['Interactive_Content_Creation'];
		$params->Course_Creation = $data['Course_Creation'];
		$params->Teacher_Creation = $data['Teacher_Creation'];
		$params->Lesson_Creation = $data['Lesson_Creation'];
		$params->View_Your_Test_Course = $data['View_Your_Test_Course'];
		$params->User_Group_Creation = $data['User_Group_Creation'];
		$params->User_Importer_Creation = $data['User_Importer_Creation'];
		$params->Update_Course_Permissions = $data['Update_Course_Permissions'];
		$params->Assign_and_Recommend_Creation = $data['Assign_and_Recommend_Creation'];
		$params->Auto_Notification_Creation = $data['Auto_Notification_Creation'];
		$params->Validate_Test_User_End_to_End = $data['Validate_Test_User_End_to_End'];
		$params->Course_Category_Creation = $data['Course_Category_Creation'];
		$params->Quizzes_and_Survey_Creation = $data['Quizzes_and_Survey_Creation'];
		$params->Virtual_Classroom_Creation = $data['Virtual_Classroom_Creation'];
		$params->SCORM_Item_Creation = $data['SCORM_Item_Creation'];
		$params->Event_Creation = $data['Event_Creation'];
		$params->User_Creation = $data['User_Creation'];
		$params->Domain_Creation = $data['Domain_Creation'];
		$params->User_Group_Creation = $data['User_Group_Creation'];
		$params->Brand_Edit = $data['Brand_Edit'];
		$params->Brand_Creation = $data['Brand_Creation'];
		$params->Team_Edit = $data['Team_Edit'];
		$params->Team_Creation = $data['Team_Creation'];
		$params->Subscription_Plan_Creation = $data['Subscription_Plan_Creation'];
		$params->SSO_Creation = $data['SSO_Creation'];
		$params->LMS_Settings_Edit = $data['LMS_Settings_Edit'];
		$params->Checklist_Creation = $data['Checklist_Creation'];
		$params->Media_Category_Creation = $data['Media_Category_Creation'];
		$params->Media_Item_Creation = $data['Media_Item_Creation'];
		$params->Certificate_Design_Creation = $data['Certificate_Design_Creation'];
		$params->Certificate_Creation = $data['Certificate_Creation'];
		$params->Badge_Creation = $data['Badge_Creation'];
		$params->Milestone_Creation = $data['Milestone_Creation'];
		$params->Activity_Dashboard_Creation = $data['Activity_Dashboard_Creation'];
		$params->Learner_Portal_Creation = $data['Learner_Portal_Creation'];
		$params->Learner_Portal_Edit = $data['Learner_Portal_Edit'];
		$params->Landing_Page_Creation = $data['Landing_Page_Creation'];
		$params->Landing_Page_Edit = $data['Landing_Page_Edit'];
		$params->Learner_Dashboard_Creation = $data['Learner_Dashboard_Creation'];
		$params->Learner_Dashboard_Edit = $data['Learner_Dashboard_Edit'];
		$params->Email_Notification_Creation = $data['Email_Notification_Creation'];
		$params->Popup_Notification_Creation = $data['Popup_Notification_Creation'];
		$params->Report_Creation = $data['Report_Creation'];
		$params->Admin_Permissions_Creation = $data['Admin_Permissions_Creation'];
		$params->API_Key_Creation = $data['API_Key_Creation'];
		$params->Contact_Form_Creation = $data['Contact_Form_Creation'];
		$params->Course_and_Subscription_Promo_Code_Creation = $data['Course_and_Subscription_Promo_Code_Creation'];
		$params->Font_Creation = $data['Font_Creation'];
		$params->Forum_Category_Creation = $data['Forum_Category_Creation'];
		$params->Navigation_Item_Creation = $data['Navigation_Item_Creation'];
		$params->Navigation_Item_Edit = $data['Navigation_Item_Edit'];
		$params->Points_Category_Creation = $data['Points_Category_Creation'];
		$params->Points_Category_Edit = $data['Points_Category_Edit'];
		$params->Site_Page_Creation = $data['Site_Page_Creation'];
		$params->Interactive_Content_Edit = $data['Interactive_Content_Edit'];
		$data['params'] = json_encode($params);
		$data['modified_by'] = JFactory::getUser()->id;
		$data['modified_date'] = date("Y-m-d H:i:s");
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$data = $this->_formData;

			$params = json_decode($data['params']);
			$data['Quick_Start_Edit'] = $params->Quick_Start_Edit;
			$data['User_Creation'] = $params->User_Creation;
			$data['User_Group_Creation'] = $params->User_Group_Creation;
			$data['Course_Category_Creation'] = $params->Course_Category_Creation;
			$data['Teacher_Creation'] = $params->Teacher_Creation;
			$data['Course_Creation'] = $params->Course_Creation;
			$data['Interactive_Content_Edit'] = $params->Interactive_Content_Edit;
			$data['Interactive_Content_Creation'] = $params->Interactive_Content_Creation;
			$data['Update_Course_Permissions'] = $params->Update_Course_Permissions;
			$data['View_Your_Test_Course'] = $params->View_Your_Test_Course;
			$data['Validate_Test_User_End_to_End'] = $params->Validate_Test_User_End_to_End;
			$data['Lesson_Creation'] = $params->Lesson_Creation;
			$data['Quizzes_and_Survey_Creation'] = $params->Quizzes_and_Survey_Creation;
			$data['Virtual_Classroom_Creation'] = $params->Virtual_Classroom_Creation;
			$data['SCORM_Item_Creation'] = $params->SCORM_Item_Creation;
			$data['Event_Creation'] = $params->Event_Creation;
			$data['Domain_Creation'] = $params->Domain_Creation;
			$data['Brand_Edit'] = $params->Brand_Edit;
			$data['Brand_Creation'] = $params->Brand_Creation;
			$data['Team_Edit'] = $params->Team_Edit;
			$data['Team_Creation'] = $params->Team_Creation;
			$data['Subscription_Plan_Creation'] = $params->Subscription_Plan_Creation;
			$data['SSO_Creation'] = $params->SSO_Creation;
			$data['LMS_Settings_Edit'] = $params->LMS_Settings_Edit;
			$data['Checklist_Creation'] = $params->Checklist_Creation;
			$data['Media_Category_Creation'] = $params->Media_Category_Creation;
			$data['Media_Item_Creation'] = $params->Media_Item_Creation;
			$data['Certificate_Design_Creation'] = $params->Certificate_Design_Creation;
			$data['Certificate_Creation'] = $params->Certificate_Creation;
			$data['Badge_Creation'] = $params->Badge_Creation;
			$data['Milestone_Creation'] = $params->Milestone_Creation;
			$data['User_Importer_Creation'] = $params->User_Importer_Creation;
			$data['Activity_Dashboard_Creation'] = $params->Activity_Dashboard_Creation;
			$data['Auto_Notification_Creation'] = $params->Auto_Notification_Creation;
			$data['Learner_Portal_Creation'] = $params->Learner_Portal_Creation;
			$data['Learner_Portal_Edit'] = $params->Learner_Portal_Edit;
			$data['Landing_Page_Creation'] = $params->Landing_Page_Creation;
			$data['Landing_Page_Edit'] = $params->Landing_Page_Edit;
			$data['Learner_Dashboard_Creation'] = $params->Learner_Dashboard_Creation;
			$data['Learner_Dashboard_Edit'] = $params->Learner_Dashboard_Edit;
			$data['Assign_and_Recommend_Creation'] = $params->Assign_and_Recommend_Creation;
			$data['Email_Notification_Creation'] = $params->Email_Notification_Creation;
			$data['Popup_Notification_Creation'] = $params->Popup_Notification_Creation;
			$data['Report_Creation'] = $params->Report_Creation;
			$data['Admin_Permissions_Creation'] = $params->Admin_Permissions_Creation;
			$data['API_Key_Creation'] = $params->API_Key_Creation;
			$data['Contact_Form_Creation'] = $params->Contact_Form_Creation;
			$data['Course_and_Subscription_Promo_Code_Creation'] = $params->Course_and_Subscription_Promo_Code_Creation;
			$data['Font_Creation'] = $params->Font_Creation;
			$data['Forum_Category_Creation'] = $params->Forum_Category_Creation;
			$data['Navigation_Item_Creation'] = $params->Navigation_Item_Creation;
			$data['Navigation_Item_Edit'] = $params->Navigation_Item_Edit;
			$data['Points_Category_Creation'] = $params->Points_Category_Creation;
			$data['Points_Category_Edit'] = $params->Points_Category_Edit;
			$data['Site_Page_Creation'] = $params->Site_Page_Creation;

			return $data;
		}
	}
}