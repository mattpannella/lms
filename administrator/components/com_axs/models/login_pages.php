<?php

defined('_JEXEC') or die;

class AxsModelLogin_pages extends FOFModel {

	public function save($data) {
		$params = new stdClass();
		$params->welcome_text_heading = $data['welcome_text_heading'];
		$params->login_form_heading = $data['login_form_heading'];
		$params->login_form_text = $data['login_form_text'];
		$params->background_image = $data['background_image'];
		$params->welcome_text_body = $data['welcome_text_body'];
		$params->welcome_text_color = $data['welcome_text_color'];
		$params->login_form_text_color = $data['login_form_text_color'];
		$params->show_transparent_image_overlay = $data['show_transparent_image_overlay'];

		$params->logo = $data['logo'];
		$params->redirect = $data['redirect'];
		$params->include_login = $data['include_login'];
		$params->include_password_reset = $data['include_password_reset'];
		$params->include_registration = $data['include_registration'];
		$params->sso_options = !empty($data['sso_options']) ? implode(',',$data['sso_options']) : null;

		// Clean and store the new user registration email template variables
		$params->send_registration_email = $data['send_registration_email'];
        $params->registration_email_subject = AxsSecurity::cleanInput($data['registration_email_subject']);
        $params->registration_email_body = AxsSecurity::cleanInput($data['registration_email_body']);

		$data['params'] = json_encode($params);

		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data = $data;
			$params = json_decode($data['params']);
			$data['welcome_text_heading'] = $params->welcome_text_heading;
			$data['login_form_heading'] = $params->login_form_heading;
			$data['login_form_text'] = $params->login_form_text;
			$data['background_image'] = $params->background_image;
			$data['welcome_text_body'] = $params->welcome_text_body;
			$data['welcome_text_color'] = $params->welcome_text_color;
			$data['login_form_text_color'] = $params->login_form_text_color;
			$data['show_transparent_image_overlay'] = $params->show_transparent_image_overlay;

			$data['logo'] = $params->logo;
			$data['redirect'] = $params->redirect;
			$data['include_login'] = $params->include_login;
			$data['include_password_reset'] = $params->include_password_reset;
			$data['include_registration'] = $params->include_registration;
			$data['sso_options'] = explode(',',$params->sso_options);

			// Registration email template fields for new user emails
			$data['send_registration_email'] = $params->send_registration_email;

			// Build out the email template fields - if they are empty, store defaults.
			$params = $this->buildEmailTemplate($params);

			$data['registration_email_subject'] = $params->registration_email_subject;
			$data['registration_email_body'] = $params->registration_email_body;

			return $data;
		}
	}

	private function buildEmailTemplate($params) {

		if(empty($params->registration_email_subject)) {

            $params->registration_email_subject = "Welcome to [website_name]!";
		}

		if(empty($params->registration_email_body)) {

			$params->registration_email_body = <<<BODY
			Thank you for registering, [name]!<br /><br />

			You can access the site you just registered for at <a href="[website_link]" target="_blank">this link</a>.
BODY;
		}

		return $params;
    }
}