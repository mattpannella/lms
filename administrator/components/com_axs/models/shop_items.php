<?php 

defined("_JEXEC") or die();

class AxsModelShop_Items extends FOFModel {

    public function save($data) {
        // error_log(json_encode($data));
        if(is_array($data)) {
            $stock_data_in = $data["stock_items"];
            $stock_data_out = array();
            if(is_array($stock_data_in)) {
                for ($i = 1; $i <= count($stock_data_in); $i++) {
                    $sku = trim($stock_data_in["stock_items$i"]->sku);
                    if (strlen($sku) == 0) {
                        $s = new stdClass;
                        $s->sku = "none";
                        $s->timestamp = $stock_data_in["stock_items$i"]->timestamp;
                        $stock_data_out[] = $s;
                    } else {
                        $stock_data_out[] = $stock_data_in["stock_items$i"]; 
                    }
                    
                }
            }
            $data["stock"] = json_encode($stock_data_out);
        }
        return parent::save($data);
    }

    
    public function loadFormData() {
        // error_reporting(E_ALL | E_NOTICE);
        // ini_set("display_errors", 1);

        $data = $this->_formData;
        $stock_data = json_decode($data["stock"]);
        if (is_array($stock_data)) {
            for ($i = 1; $i <= count($stock_data); $i++) {
                $data["stock_items"]["items$i"] = $stock_data[$i-1];
            }
        }
        // echo "<pre>";var_export( get_defined_vars() );echo "</pre>";
        return $data;
    }
}