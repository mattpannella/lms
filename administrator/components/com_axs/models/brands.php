<?php

class AxsModelBrands extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$top = new stdClass();

			$site_details = new stdClass();
			$logos = new stdClass();
			$legal = new stdClass();
			$homepage = new stdClass();
			$dashboard = new stdClass();
			$board = new stdClass();
			$site_faq = new stdClass();
			$features = new stdClass();
			$default_settings = new stdClass();
			$social = new stdClass();

			/*
				Essentials Tab
			*/

			$top->site_title = $data['site_title'];
			$top->description = $data['description'];
			if ($data['published'] == "1") {
				$data['published'] = true;
			} else {
				$data['published'] = false;
			}

			/*
				Site Details Tab
			*/
			$domains = json_decode($data['domains_data']);

			$site_urls = array();
			foreach ($domains as $domain) {
				$site_urls []= $domain->domain;
			}
			$data['domains'] = implode(',', $site_urls);

			if ($data['site_title'] == "" || $data['site_title'] == null) {
				$data['site_title'] = "unnamed";
			}

			$logos->favicon = $data['favicon'];
			$logos->video_watermark = $data['video_watermark'];

			$site_details->plans_for_active = $data['plans_for_active'];
			$site_details->default_font = $data['brand_font'];

			$site_details->language_selector = $data['language_selector'];
			$site_details->language_selector_options = $data['language_selector_options'];
			$site_details->tovuti_root_styles	= $data['tovuti_root_styles'];
			$site_details->root_primary_color	= $data['root_primary_color'];
			$site_details->root_accent_color	= $data['root_accent_color'];
			$site_details->root_base_color		= $data['root_base_color'];
			$site_details->homepage_type = $data['homepage_type'];
			$site_details->login_page = $data['login_page'];
			$site_details->enable_sso_deeplinks_button = $data['enable_sso_deeplinks_button'];
			$site_details->sso_app = $data['sso_app'];
			$site_details->sso_deeplinks_button_permissions = $data['sso_deeplinks_button_permissions'];






			/*
				Legal Information Tab
			*/
			$legal->privacy_policy = $data['privacy_policy'];
			$legal->terms_of_use = $data['terms_of_use'];
			$legal->member_agreement = $data['member_agreement'];
			$legal->require_privacy_policy = $data['require_privacy_policy'];
			$legal->require_terms_of_use = $data['require_terms_of_use'];
			$legal->require_member_agreement = $data['require_member_agreement'];

			if (empty($data['privacy_policy']) && $data['require_privacy_policy'] == '1') {
				$validationRejectionMessage = AxsLanguage::text("PRIVACY_POLICY_NOT_FOUND", "Require Privacy policy set to ‘Yes’ but no Privacy Policy found, please fill out the Privacy Policy in the Legal Information tab");
				JFactory::getApplication()->enqueueMessage($validationRejectionMessage, 'warning');
				$legal->require_privacy_policy = '0';
			}
			if (empty($data['terms_of_use']) && $data['require_terms_of_use'] == '1') {
				$validationRejectionMessage = AxsLanguage::text("TERMS_OF_USE_NOT_FOUND", "Require Terms of Use set to ‘Yes’ but no Terms of Use found, please fill out the Terms of Use in the Legal Information tab");
				JFactory::getApplication()->enqueueMessage($validationRejectionMessage, 'warning');
				$legal->require_terms_of_use = '0';
			}
			if (empty($data['member_agreement']) && $data['require_member_agreement'] == '1') {
				$validationRejectionMessage = AxsLanguage::text("MEMBER_AGREEMENT_NOT_FOUND", "Require Member Agreement set to ‘Yes’ but no Member Agreement was found, please fill out the Member Agreement in the Legal Information tab");
				JFactory::getApplication()->enqueueMessage($validationRejectionMessage, 'warning');
				$legal->require_member_agreement = '0';
			}

			/*
				Social Media Tab
			*/

			$social->pinterest_link = $data['pinterest_link'];
			$social->instagram_link = $data['instagram_link'];
			$social->facebook_link = $data['facebook_link'];
			$social->twitter_link = $data['twitter_link'];
			$social->google_plus_link = $data['google_plus_link'];
			$social->youtube_link = $data['youtube_link'];
			$social->linkedin_link = $data['linkedin_link'];
			/*$social->socialmedia_header_color = $data['socialmedia_header_color'];
			$social->socialmedia_footer_color = $data['socialmedia_footer_color'];*/
			$social->socialmedia_text = $data['socialmedia_text'];
			/*$social->socialmedia_on_homepage_header_orientation = $data['socialmedia_on_homepage_header_orientation'];
			$social->socialmedia_on_homepage_footer_orientation = $data['socialmedia_on_homepage_footer_orientation'];

			if ($data['show_socialmedia_on_dashboard'] == "yes") {
				$social->show_socialmedia_on_dashboard = true;
			} else {
				$social->show_socialmedia_on_dashboard = false;
			}

			if ($data['show_socialmedia_on_homepage'] == "yes") {
				$social->show_socialmedia_on_homepage = true;
			} else {
				$social->show_socialmedia_on_homepage = false;
			}

			if ($data['show_socialmedia_homepage_header'] == "yes") {
				$social->show_socialmedia_homepage_header = true;
			} else {
				$social->show_socialmedia_homepage_header = false;
			}

			if ($data['show_socialmedia_homepage_footer'] == "yes") {
				$social->show_socialmedia_homepage_footer = true;
			} else {
				$social->show_socialmedia_homepage_footer = false;
			}*/

			/*
				Home Page Tab
			*/

			$homepage->template = $data['homepage_template'];
			$homepage->multilingual = $data['homepage_multilingual'];
			$homepage->language_templates = $data['homepage_language_templates'];



			/*
				Dashboard Designer Tab
			*/

			$dashboard->template = $data['dashboard_template'];
			$dashboard->multilingual = $data['dashboard_multilingual'];
			$dashboard->language_templates = $data['dashboard_language_templates'];

			/*
				Board Designer Tab
			*/

			$board->template = $data['board_template'];
			$board->multilingual = $data['board_multilingual'];
			$board->language_templates = $data['board_language_templates'];

			/*
				Site FAQ Tab
			*/

			$site_faq->faq_heading = $data['faq_heading'];
			$site_faq->type = $data['faq_type'];
			$site_faq->faq = json_decode($data['site_faq']);
			$site_faq->site_faq_free = $data['site_faq_free'];

			/*
				Active Features
			*/

			$features->lms_active = $data['lms_active'];
			$features->ecommerce_active = $data['ecommerce_active'];
			$features->media_active = $data['media_active'];
			$features->community_active = $data['community_active'];
			$features->mentor_active = $data['mentor_active'];
			$features->goals_active = $data['goals_active'];
			$features->affirmations_active = $data['affirmations_active'];
			$features->image_sharing_active = $data['image_sharing_active'];


			/*
				Default Settings
			*/

			$default_settings->lms_cover_image = $data['lms_cover_image'];
			$default_settings->event_cover_image = $data['event_cover_image'];
			$default_settings->lms_category_cover_image = $data['lms_category_cover_image'];
			$default_settings->event_category_cover_image = $data['event_category_cover_image'];
			$default_settings->default_avatar_image = $data['default_avatar_image'];

			/*
				API Settings
			*/

			/*
				Add Everything to the Top Level
			*/

			$top->site_details = $site_details;
			$top->logos = $logos;
			$top->legal = $legal;
			$top->homepage = $homepage;
			$top->dashboard = $dashboard;
			$top->social = $social;
			$top->board = $board;
			$top->site_faq = $site_faq;
			$top->features = $features;
			$top->default_settings = $default_settings;

			$top->email_settings_id = $data['email_settings_id'];
			$top->payment_gateway_id = $data['payment_gateway_id'];
			$top->integration_id = $data['integration_id'];

			$data['data'] = json_encode($top);
		}

		$adminParams = new stdClass();
		if(!$data['id']) {
			$adminParams->eventName = 'Brand Creation';
		} else {
			$adminParams->eventName = 'Brand Edit';
		}
		$adminParams->description = $data['site_title'].' ['.$data['id'].']';
		$adminParams->action_type ="admin";
		AxsActions::storeAdminAction($adminParams);

		return parent::save($data);
	}

	public function onAfterSave($context) {
		// Once the form is saved, create any new domains that
		// have been added to this brand or update any domains
		// currently assigned to this brand
		$data = $this->_formData;
		$domains = json_decode($data['domains_data']);
		$db = JFactory::getDbo();
		foreach ($domains as $domain) {
			if ($domain->is_dummy) {
				continue;
			}
			if (gettype($domain->params) !== 'string') {
				$domain->params = json_encode($domain->params);
			}
			$domain->params->brand = strval($context->id);
			if (isset($domain->id)) {
				// This domain exists already, just update it
				$db->updateObject('axs_domains', $domain, 'id');
			} else {
				$ssl_id = AxsDomains::createDomain($domain->domain, $context->id);
				if ($ssl_id !== false) {
					$db->insertObject('axs_domains', $domain);
				}
			}
		}

	}

	public function onAfterCopy($context) {
		// Once the form is saved, create any new domains that
		// have been added to this brand or update any domains
		// currently assigned to this brand
		$brandData = new stdClass();
		$brandData->id = $context->id;
		$brandData->default_brand = 0;

		$db = JFactory::getDbo();
		$db->updateObject('axs_brands', $brandData, 'id');
	}

	public function loadFormData() {
		// AxsLanguage::publishNeededLanguages();

		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			$top = json_decode($data['data']);

			if ($top == null) {
				return array();
			}

			$site_details = $top->site_details;
			$billing = $top->billing;
			$logos = $top->logos;
			$legal = $top->legal;
			$homepage = $top->homepage;
			$dashboard = $top->dashboard;
			$social = $top->social;
			$board = $top->board;
			$site_faq = $top->site_faq;
			$default_settings = $top->default_settings;

			/*
				Essentials tab
			*/

			$data['site_title'] = $top->site_title;
			$data['description'] = $top->description;
			if ($data['published'] == true) {
				$data['published'] = "1";
			} else {
				$data['published'] = "0";
			}

			$data['email_settings_id'] = $top->email_settings_id;
			$data['payment_gateway_id'] = $top->payment_gateway_id;
			$data['integration_id'] = $top->integration_id;

			/*
				Site Details Tab
			*/

			$data['plans_for_active'] = $site_details->plans_for_active;
			$data['brand_font'] = $site_details->default_font;

			$data['language_selector'] = $site_details->language_selector;
			$data['language_selector_options'] = $site_details->language_selector_options;
			$data['tovuti_root_styles']	= $site_details->tovuti_root_styles;
			$data['root_primary_color']	= $site_details->root_primary_color;
			$data['root_accent_color']	= $site_details->root_accent_color;
			$data['root_base_color']	= $site_details->root_base_color;
			$data['homepage_type'] = $site_details->homepage_type;
			$data['login_page'] = $site_details->login_page;
			$data['enable_sso_deeplinks_button'] = $site_details->enable_sso_deeplinks_button;
			$data['sso_app'] = $site_details->sso_app;
			$data['sso_deeplinks_button_permissions'] = $site_details->sso_deeplinks_button_permissions;

			/*
				Billing Tab
			*/

			if ($billing->admin_emails != null) {
				$emails = new stdClass();
				$emails->admin_email = array();
				$emails->admin_name = array();

				foreach ($billing->admin_emails as $admin_email) {
					array_push($emails->admin_email, $admin_email->email);
					array_push($emails->admin_name, $admin_email->name);
				}

				$data['admin_emails'] = json_encode($emails);
			}

			$data['currency_id'] = $billing->currency_id;

			if ($billing->auto_reprocess_declines == true) {
				$data['auto_reprocess_declines'] = "1";
			} else {
				$data['auto_reprocess_declines'] = "0";
			}

			$data['auto_reprocess_days'] = $billing->auto_reprocess_days;

			if ($billing->weekend_billing == true) {
				$data['weekend_billing'] = "1";
			} else {
				$data['weekend_billing'] = "0";
			}

			if ($billing->weekend_fallback == "Friday") {
				$data['weekend_fallback'] = "1";
			} else {
				$data['weekend_fallback'] = "0";
			}

			if ($billing->holiday_billing == true) {
				$data['holiday_billing'] = "1";
			} else {
				$data['holiday_billing'] = "0";
			}

			$data['grace_period'] = $billing->grace_period;
			if ($billing->rewards_active == true) {
				$data['rewards_active'] = "1";
			} else {
				$data['rewards_active'] = "0";
			}

			$data['rewards_quotient'] = $billing->rewards_quotient;

			if ($billing->show_unpaid_alert == true) {
				$data['show_unpaid_alert'] = "1";
			} else {
				$data['show_unpaid_alert'] = "0";
			}

			$data['gateway_type'] = $billing->gateway_type;

			$data['converge_production_mode'] = $billing->converge_production_mode;
			$data['converge_dev_merchant_id'] = $billing->converge_dev_merchant_id;
			$data['converge_dev_user_id'] = $billing->converge_dev_user_id;
			$data['converge_dev_account_pin'] = $billing->converge_dev_account_pin;
			$data['converge_prod_merchant_id'] = $billing->converge_prod_merchant_id;
			$data['converge_prod_user_id'] = $billing->converge_prod_user_id;
			$data['converge_prod_account_pin'] = $billing->converge_prod_account_pin;

			$data['authnet_production_mode'] = $billing->authnet_production_mode;
			$data['authnet_dev_api_login_id'] = $billing->authnet_dev_api_login_id;
			$data['authnet_dev_transaction_key'] = $billing->authnet_dev_transaction_key;
			$data['authnet_prod_api_login_id'] = $billing->authnet_prod_api_login_id;
			$data['authnet_prod_transaction_key'] = $billing->authnet_prod_transaction_key;

			$data['stripe_production_mode'] = $billing->stripe_production_mode;
			$data['stripe_dev_api_key'] = $billing->stripe_dev_api_key;
			$data['stripe_prod_api_key'] = $billing->stripe_prod_api_key;
			$data['stripe_dev_public_api_key'] = $billing->stripe_dev_public_api_key;
			$data['stripe_prod_public_api_key'] = $billing->stripe_prod_public_api_key;
			$data['stripe_hide_zip'] = $billing->stripe_hide_zip;

			$data['heartland_production_mode'] = $billing->heartland_production_mode;
			$data['heartland_prod_secret_api_key'] = $billing->heartland_prod_secret_api_key;
			$data['heartland_prod_public_api_key'] = $billing->heartland_prod_public_api_key;
			$data['heartland_dev_secret_api_key'] = $billing->heartland_dev_secret_api_key;
			$data['heartland_dev_public_api_key'] = $billing->heartland_dev_public_api_key;

			/*
				Site Logos Tab
			*/

			$data['favicon'] = $logos->favicon;
			$data['video_watermark'] = $logos->video_watermark;

			/*
				Legal Information Tab
			*/

			$data['privacy_policy'] = $legal->privacy_policy;
			$data['terms_of_use'] = $legal->terms_of_use;
			$data['member_agreement'] = $legal->member_agreement;

			if (!isset($legal->require_privacy_policy) && !empty($data['privacy_policy'])) {
				$data['require_privacy_policy'] = '1';
			} else {
				$data['require_privacy_policy'] = $legal->require_privacy_policy;
			}
			if (!isset($legal->require_terms_of_use) && !empty($data['terms_of_use'])) {
				$data['require_terms_of_use'] = '1';
			} else {
				$data['require_terms_of_use'] = $legal->require_terms_of_use;
			}
			if (!isset($legal->require_member_agreement) && !empty($data['member_agreement'])) {
				$data['require_member_agreement'] = '1';
			} else {
				$data['require_member_agreement'] = $legal->require_member_agreement;
			}

			/*
				Social Media Tab
			*/

			$data['pinterest_link'] = $social->pinterest_link;
			$data['instagram_link'] = $social->instagram_link;
			$data['facebook_link'] = $social->facebook_link;
			$data['twitter_link'] = $social->twitter_link;
			$data['google_plus_link'] = $social->google_plus_link;
			$data['youtube_link'] = $social->youtube_link;
			$data['linkedin_link'] = $social->linkedin_link;
			/*$data['socialmedia_on_homepage_header_orientation'] = $social->socialmedia_on_homepage_header_orientation;
			$data['socialmedia_on_homepage_footer_orientation'] = $social->socialmedia_on_homepage_footer_orientation;
			$data['socialmedia_header_color'] = $social->socialmedia_header_color;
			$data['socialmedia_footer_color'] = $social->socialmedia_footer_color;*/
			$data['socialmedia_text'] = $social->socialmedia_text;

			/*
			if ($social->show_socialmedia_on_dashboard) {
				$data['show_socialmedia_on_dashboard'] = "yes";
			} else {
				$data['show_socialmedia_on_dashboard'] = "no";
			}

			if ($social->show_socialmedia_on_homepage) {
				$data['show_socialmedia_on_homepage'] = "yes";
			} else {
				$data['show_socialmedia_on_homepage'] = "no";
			}

			if ($social->show_socialmedia_homepage_header) {
				$data['show_socialmedia_homepage_header'] = "yes";
			} else {
				$data['show_socialmedia_homepage_header'] = "no";
			}

			if ($social->show_socialmedia_homepage_footer) {
				$data['show_socialmedia_homepage_footer'] = "yes";
			} else {
				$data['show_socialmedia_homepage_footer'] = "no";
			}
			*/

			/*
				Home Page Tab
			*/

			$data['homepage_template'] = $homepage->template;
			$data['homepage_multilingual'] = $homepage->multilingual;
			$data['homepage_language_templates'] = $homepage->language_templates;

			/*
				Dashboard Designer Tab
			*/

			$data['dashboard_template'] = $dashboard->template;
			$data['dashboard_multilingual'] = $dashboard->multilingual;
			$data['dashboard_language_templates'] = $dashboard->language_templates;

			/*
				Board Designer Tab
			*/

			$data['board_template'] = $board->template;
			$data['board_multilingual'] = $board->multilingual;
			$data['board_language_templates'] = $board->language_templates;

			/*
				Site Site FAQ Tab
			*/

			$data['faq_heading'] = $site_faq->faq_heading;
			$data['faq_type'] = $site_faq->type;
			if ($site_faq->faq) {
				$data['site_faq'] = json_encode($site_faq->faq);
			}
			$data['site_faq_free'] = $site_faq->site_faq_free;

			/*
				Default Settings
			*/
			$data['lms_cover_image'] = $default_settings->lms_cover_image;
			$data['event_cover_image'] = $default_settings->event_cover_image;
			$data['lms_category_cover_image'] = $default_settings->lms_category_cover_image;
			$data['event_category_cover_image'] = $default_settings->event_category_cover_image;
			$data['default_avatar_image'] = $default_settings->default_avatar_image;



			return $data;
		}
	}
}

