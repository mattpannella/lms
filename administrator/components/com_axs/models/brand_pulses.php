<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1); 

class AxsModelBrand_Pulses extends FOFModel {	
	public function save($data) {

		$info = new stdClass();

		$info->categories = json_encode($data['category']);
		$info->description = $data['whats_this'];
		
		$data['data'] = json_encode($info);
		
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			$info = json_decode($data['data']);

			if ($info->categories) {
				$data['category'] = json_decode($info->categories);
			}

			$data['whats_this'] = $info->description;		

			return $data;
		}
	}
}