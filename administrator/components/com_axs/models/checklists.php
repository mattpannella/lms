<?php

defined('_JEXEC') or die;

class AxsModelChecklists extends FOFModel {

	public function save($data) {
		$items = json_decode(json_encode($data['items']));
		foreach ($items as $item) {
			if (!$item->id) {
				$items_updated = true;
				$item->id = AxsLMS::createUniqueID();
				
			}
			if ($item->due_date) {
				$item->due_date = date('Y-m-d 12:i:s',strtotime($item->due_date));				
			}
		}
		$data['accesslevel'] = !empty($data['accesslevel']) ? implode(',', $data['accesslevel']) : null;
		$data['usergroup'] = !empty($data['usergroup']) ? implode(',', $data['usergroup']) : null;
		$params = new stdClass();
		$params->items = json_encode($items);
		$params->show_tooltip = $data['show_tooltip'];
		$params->due_date_type = $data['due_date_type'];
		$params->checklist_due_dynamic_number = $data['checklist_due_dynamic_number'];
		$params->checklist_due_dynamic_unit = $data['checklist_due_dynamic_unit'];
		$params->notify_admin = $data['notify_admin'];
		$params->admin_id = $data['admin_id'];
		if ($items_updated) {
			$params->user_ids_emails_sent = "";	
		} else {
			$checklist = AxsChecklist::getChecklist($data['id']);
			$checklist_params = json_decode($checklist->params);
			$params_items = json_decode($checklist_params->items);
			$items_updated = json_encode($data['items']) != json_encode($params_items);
			$params->user_ids_emails_sent = $checklist_params->user_ids_emails_sent; 
		}
		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {
		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$data = $data;
			$data['accesslevel'] = explode(',', $data['accesslevel']);
			$data['usergroup'] = explode(',', $data['usergroup']);
			$params = json_decode($data['params']);
			$data['items'] = $params->items;
			$data['show_tooltip'] = $params->show_tooltip;
			$data['due_date_type'] = $params->due_date_type;
			$data['checklist_due_dynamic_number'] = $params->checklist_due_dynamic_number;
			$data['checklist_due_dynamic_unit'] = $params->checklist_due_dynamic_unit;
			$data['notify_admin'] = $params->notify_admin;
			$data['admin_id'] = $params->admin_id;
			return $data;			
		}
	}
}