<?php

class AwardControllerAwards extends FOFController {
	public function save_field() {
		$testData = JRequest::getVar('data');
		$_SESSION['com_board']['save_data'] = $testData;		
	}

	public function remove_badge() {

		$current_user = JFactory::getUser();

		if ($current_user->authorise('core.edit')) {
			$badge_id = JRequest::getVar('badge_id');
			$list = JRequest::getVar('list');
			$db = JFactory::getDBO();

			if (count($list) == 0) {
				//The list is empty.
				echo "success";
				return;
			}

			$user_list = implode(",", $list);

			$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND FIND_IN_SET(user_id, '$user_list')";
			
			$db->setQuery($query);
			$db->execute();
			echo "success";
		} else {
			if ($current_user) {
				echo "You are not authorized.";
			} else {
				echo "You must log in to use this feature.";
			}			
		}
	}

	public function set_badge_refresh_on_reload() {
		$_SESSION['badge_refresh_on_reload'] = true;
	}
}

