<?php

/*
	Justin Lloyd
	11/14/2017
*/

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("components/com_award/helpers/helper.php");

class AwardModelBadges extends FOFModel {

	public function save($data) {

		$params = new stdClass();

		$params->badge_type 		= $data['badge_image_type'];
		$params->badge_color 		= $data['badge_icon_color'];
		$params->badge_icon 		= $data['badge_icon'];
		$params->badge_image 		= $data['badge_image'];
		$params->badge_hover 		= $data['badge_hover'];
		$params->certificate 		= $data['certificate'];
		$params->expiration_amount 	= $data['expiration_amount'];
		$params->expiration_type 	= $data['expiration_type'];
		$params->requirement_type 	= $data['requirementtype'];
		$params->requirements 		= $data['requirements'];
		$params->user_list 			= $data['userlist'];
		$params->losebadge 			= $data['losebadge'];
		$params->usergroup_actions 	= $data['usergroup_actions'];
		$params->add_usergroups 	= $data['add_usergroups'];
		$params->remove_usergroups 	= $data['remove_usergroups'];
		$params->points_actions     = $data['points_actions'];
		$params->points_category    = $data['points_category'];
		$params->points             = $data['points'];
		$params->requirements_all   = $data['requirements_all'];

		//Get the language overrides
		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_languages WHERE published = 1";
		$db->setQuery($query);
		$languages = $db->loadObjectList();

		$overrides = new stdClass();

		foreach ($languages as $language) {

			$code = $language->lang_code;
			$title = $language->title;

			if (!$code) {
				continue;
			}

			if ($data["lang_use_override_$code"] === "1") {
				$overrides->$code = new stdClass();

				$overrides->$code->badge_type = $data["badge_image_type_$code"];
				$overrides->$code->badge_color = $data["badge_icon_color_$code"];
				$overrides->$code->badge_icon = $data["badge_icon_$code"];
				$overrides->$code->badge_image = $data["badge_image_$code"];
				$overrides->$code->badge_hover = $data["badge_hover_$code"];
				$overrides->$code->certificate = $data["certificate_$code"];
			} else {
				$overrides->$code = null;
			}
		}

		$params->language_overrides = $overrides;

		$data['params'] = json_encode($params);
		$save = parent::save($data);

		if ($data['id']) {
			$id = $data['id'];
		} else {
			$id = null;
		}

		return $save;
	}

	public function loadFormData() {

		if (empty($this->_formData)) {
			return array();
		} else {

			$data = $this->_formData;

			$params = json_decode($data['params']);

			$data['badge_image_type'] = $params->badge_type;
			$data['badge_icon_color'] = $params->badge_color;
			$data['badge_icon'] 	  = $params->badge_icon;
			$data['badge_image'] 	  = $params->badge_image;
			$data['badge_hover']      = $params->badge_hover;
			$data['certificate']      = $params->certificate;
			$data['expiration_amount']= $params->expiration_amount;
			$data['expiration_type']  = $params->expiration_type;

			//Cast it back to an array since it will load as a standard class
			$data['requirements'] = json_decode(json_encode($params->requirements), true);
			$data['requirementtype'] = $params->requirement_type;
			$data['userlist'] = $params->user_list;
			$data['losebadge'] = $params->losebadge;
			$data['usergroup_actions'] = $params->usergroup_actions;
			$data['add_usergroups'] = $params->add_usergroups;
			$data['remove_usergroups'] = $params->remove_usergroups;
			$data['points_actions'] = $params->points_actions;
			$data['points_category'] = $params->points_category;
			$data['points'] = $params->points;
			$data['requirements_all'] = $params->requirements_all;





			//Get the language overrides
			$db = JFactory::getDBO();
			$query = "SELECT * FROM joom_languages WHERE published = 1";
			$db->setQuery($query);
			$languages = $db->loadObjectList();

			$overrides = $params->language_overrides;

			foreach ($languages as $language) {
				$code = $language->lang_code;
				$title = $language->title;

				if ($overrides->$code) {
					$data["lang_use_override_$code"] = "1";
					$data["badge_image_type_$code"] = $overrides->$code->badge_type;
					$data["badge_icon_color_$code"] = $overrides->$code->badge_color;
					$data["badge_icon_$code"] = $overrides->$code->badge_icon;
					$data["badge_image_$code"] = $overrides->$code->badge_image;
					$data["badge_hover_$code"] = $overrides->$code->badge_hover;
					$data["certificate_$code"] = $overrides->$code->certificate;
				}
			}

			return $data;
		}
	}
}