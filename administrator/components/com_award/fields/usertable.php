<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldUsertable extends JFormField {

    public function getInput() {

        ob_start();

       
        
        $id = $this->form->getValue('id');

        if (!$id) {
            return "This list is only visible after the badge is created.";
        }
        
        echo AxsLoader::loadKendo();

        $db = JFactory::getDBO();
        $query = "
            SELECT 
                users.name, 
                badge.badge_id, 
                badge.user_id, 
                badge.date_earned,
                badge.date_expires 
            FROM axs_awards_earned AS badge 
            JOIN joom_users as users 
            WHERE badge.badge_id = $id 
            AND badge.user_id = users.id";

        $db->setQuery($query);
        $results = $db->loadObjectList();        

        ?>
            <div class="alert alert-danger">
                Note that removing badges is only useful if the credentials for the badge have changed, or if the user was assigned the badge in error.  Otherwise, if it is removed and the user still has the proper credentials to earn the badge, the user will acquire the badge again, but with a new "Date Earned".
            </div>
            <div style="width: 100%; padding: 15px 0px;">
                
                <button class="remove_badge btn btn-primary">Remove Selected Awards</button>                
            </div>

            <div style="width: 100%; padding: 15px; font-size: 18px; cursor: pointer;">
                <span>
                    <input id="select_all_checkbox" type="checkbox" class="user_checkbox" toggle="true">
                </span>
                <span id="select_all" style="padding: 4px; margin-top: 4px;">
                    Select All
                </span>
            </div>
            
            <div>
                <table id="user_list" class="table table-striped">
                    <thead>
                        <tr>
                            <th>Select</th>
                            <th>User Name</th>
                            <th>User ID </th>
                            <th>Date Earned </th>
                            <th>Date Expires </th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                            foreach ($results as $result) {
                                ?>
                                    <tr class="user_row">
                                        <td>
                                            <input type="checkbox" class="user_checkbox" user="<?php echo $result->user_id;?>">
                                        </td>
                                        <td><?php echo $result->name;?></td>
                                        <td><?php echo $result->user_id;?></td>
                                        <td><?php echo  date("M d, Y",strtotime($result->date_earned));?></td>
                                        <td><?php if($result->date_expires > 0) { echo  date("M d, Y",strtotime($result->date_expires)); } ?></td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>

            <script>
                
                var kendoOptions = {
                    sortable: true,
                    groupable: true,
                    height: 600,
                    pageable: {
                        buttonCount: 8,
                        input: true,
                        pageSize: 20,
                        pageSizes: true,
                        refresh: true,
                        message: {
                            empty: "There are no entries to display"
                        }
                    },
                    resizable: true,
                    filterable: {
                        mode: "row"
                    },
                    columnMenu: true
                }

                jQuery("#user_list").kendoGrid(kendoOptions);

                //When the tab is clicked on, refresh the grid.

                jQuery("a").click(
                    function() {
                        if (this.getAttribute("href") == "#earned_users") {
                            setTimeout(
                                function() {
                                    jQuery("#user_list").data('kendoGrid').refresh();
                                }, 200
                            );                            
                        }
                    }
                );

                jQuery("#select_all").click(
                    function() {
                        jQuery("#select_all_checkbox").click();
                    }
                );

                var badge_id = <?php echo $id;?>;

                //Toggle all of the check boxes if the top check box is selected.
                jQuery(".user_checkbox").click(
                    function() {
                        var toggle = this.getAttribute("toggle");
                        
                        if (toggle == "true") {
                            var all_checkboxes = document.getElementsByClassName("user_checkbox");
                            for (var i = 0; i < all_checkboxes.length; i++) {
                                all_checkboxes[i].checked = this.checked;
                            }
                        }
                    }
                );

                //The Remove Selected Badges button.
                jQuery(".remove_badge").click(
                    function(e) {
                        e.preventDefault();

                        var list = [];

                        //Get a list of all checked boxes.
                        var all_checkboxes = document.getElementsByClassName("user_checkbox");
                        for (var i = 0; i < all_checkboxes.length; i++) {
                            var box = all_checkboxes[i];
                            if (box.checked) {
                                var user = box.getAttribute('user');
                                if (user) {
                                    list.push(user);
                                }
                            }
                        }

                        var button = this;
                        
                        button.disabled = true;

                        if (confirm("This action will remove this badge from all of the selected users.\nAre you sure you want to continue?")) {
                            //Call the delete function and pass it the array of users.
                            jQuery.ajax({
                                type:       'POST',
                                url:        'index.php',
                                data:       {
                                    'option':       'com_award',
                                    'task':         'awards.remove_badge',
                                    'format':       'raw',
                                    'badge_id':     badge_id,
                                    'list':         list
                                },
                                success: function(response) {

                                    if (response == "success") {

                                        button.disabled = false;

                                        for (var i = 0; i < all_checkboxes.length; i++) {
                                            var box = all_checkboxes[i]
                                            if (box.checked) {
                                                var user = box.getAttribute('user');
                                                if (user) {
                                                    jQuery(box.parentNode.parentNode).hide();
                                                }
                                            }
                                        }


                                    } else {
                                        alert(response);
                                    }
                                }
                            });
                        } else {
                            button.disabled = false;
                        }
                    }
                );

                
            </script>

        <?php

        return ob_get_clean();
    }

    public function getRepeatable() {
        return "repeatable";
    }
}
