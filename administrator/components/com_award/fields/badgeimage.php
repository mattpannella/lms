<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldBadgeimage extends JFormField {

    public function getInput() {       
        
    }

    public function getRepeatable() {
        ob_start();

        $params = json_decode($this->item->params);
        switch ($params->badge_type) {
            case "image":

                $image_path = JURI::root() . $params->badge_image;
                ?>
                    <img src="<?php echo $image_path; ?>" />
                <?php
                break;
            case "icon":
                if ($params->badge_color) {
                    $color = "color:" . $params->badge_color;
                } else {
                    $color = "";
                }
                ?>
                    <span 
                        class="<?php echo $params->badge_icon?>"
                        style="font-size: 32px; <?php echo $color; ?>"
                    ></span>
                <?php
                break;
        }

        return ob_get_clean();
    }
}
