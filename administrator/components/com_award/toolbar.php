<?php

defined('_JEXEC') or die;

class AwardToolbar extends FOFToolbar {

    function onBadgesBrowse() {
		JToolBarHelper::title(JText::_('COM_AWARD_TITLE_BADGES'));
		JToolbarHelper::addNew();
		JToolbarHelper::publishList();
		JToolbarHelper::unpublishList();
		JToolbarHelper::deleteList();
		JToolbarHelper::save2copy('copy','Duplicate Award(s)');
	}

	function onBadgesEdit() {
		JToolBarHelper::title(JText::_('COM_AWARD_TITLE_BADGES'));
		JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy','Duplicate Award');
		JToolbarHelper::cancel();
	}
}