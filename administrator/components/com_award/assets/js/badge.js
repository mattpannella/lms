jQuery(document).ready(
	function() {


		function iconChangeColor(newColor) {
			var badgeControls = document.getElementById("badge_icon").parentNode;
			//console.log(badgeControls);
			var badgeIcon = badgeControls.getElementsByClassName("icon_preview");
			jQuery(badgeIcon).css({
				"color":newColor
			});
		}

		jQuery("#badge_icon_color").change(
			function() {				
				iconChangeColor(this.value);				
			}
		);

		var colorPicker = document.getElementById("badge_icon_color");
		iconChangeColor(colorPicker.value);
	}
);
