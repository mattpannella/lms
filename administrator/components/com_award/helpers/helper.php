<?php
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class BadgeHelper {
	public static function updateBadges($badge_id = null, $user_id = null, $db = null, $issue_date = null) {
		$userEarned = $user_id;
		if ($db == null) {
			$db = JFactory::getDBO();
		}

		if (!$db) {
			return;
		}

		if(!$issue_date) {
			$issue_date = date("Y-m-d H:i:s");
		}

		$query = "SELECT * FROM axs_awards WHERE enabled = 1";

		if (!empty($badge_id)) {
			if(is_array($badge_id)) {
				$badgelist = implode(',',$badge_id);
				$query .= " AND id IN ($badgelist)";
			} else {
				$query .= " AND id = $badge_id";
			}

		}

		$db->setQuery($query);
		$badges = $db->loadObjectList();

		$users = array();
		if ($user_id == null) {
			//This updates all users, such as when a badge is created or edited

			//Get a list of all the user IDs
			$query = "SELECT id FROM joom_users WHERE block = 0";
	        $db->setQuery($query);
	        $temp = $db->loadObjectList();
	        $total = count($temp);


	        //Put all the IDs into a single array
	        for ($i = 0; $i < $total; $i++) {
	        	$users[]= $temp[$i]->id;
	        }
	    } else {
	    	//This is only updating a single user, such as completing a lesson.
	    	$users[]= $user_id;
	    }

	    foreach ($badges as $badge) {

			$userEarnedList = array();
			$userRemoveList = array();

	    	if (!$badge->enabled) {
				continue;
			}

			$badge_id = $badge->id;

			$params = json_decode($badge->params);
			$expiration = null;

			if ($params->expiration_amount) {
				$expiration = date("Y-m-d H:i:s",strtotime("$issue_date + " . $params->expiration_amount." ".$params->expiration_type));
			}

			if (isset($params->losebadge) && $params->losebadge == "yes") {
				$canlose = true;
			} else {
				$canlose = false;
			}

			switch ($params->requirement_type) {
				case "assigned":

					$list = explode(",", $params->user_list);
					if($userEarned && !in_array($userEarned,$list)) {
						continue;
					}
					foreach ($list as $user_id) {
						if ($user_id) {		//An empty user_list will create a single array of "".
							$badge_exist = AxsActions::checkAward($badge_id,$user_id);
							if(!$badge_exist) {
								$query = "INSERT IGNORE INTO axs_awards_earned (user_id, badge_id, date_earned, date_expires) VALUES ($user_id, $badge_id, '$issue_date', '$expiration')";
								$db->setQuery($query);
								$insertResult = $db->execute();

                                if($insertResult) {

                                    $checklistItemType = ($badge->type == 'certificate') ? 'certificate_awarded' : 'badge_awarded';

                                    AxsLMS::markUserChecklistItemsCompletedForItemType($user_id, $badge_id, $checklistItemType);
            
                                }

								if($params->usergroup_actions && ($params->add_usergroups || $params->remove_usergroups)) {
									$groupData = new stdClass();
									$groupData->user_id = $user_id;
									$groupData->usergroup_actions = $params->usergroup_actions;
									$groupData->add_usergroups = $params->add_usergroups;
									$groupData->remove_usergroups = $params->remove_usergroups;
									AxsActions::setGroups($groupData);
								}

								if($params->points_actions && $params->points_category && $params->points) {
									$awardData = new stdClass();
					                $awardData->user_id = $user_id;
					                $awardData->category_id = $params->points_category;
					                $awardData->points = $params->points;
					                $awardData->date = $issue_date;
									$awardData->params = json_encode(array('award_id' => $badge_id));
					                AxsAwards::awardPoints($awardData);
								}
							}
						}
					}

					break;
				case "conditional":
				default:	//Also legacy for ones without a type.
					$requirements = $params->requirements;

					$badge_good = true;
					$allRequired = (bool)$params->requirements_all;

	                foreach ($requirements as $req) {

	                	$courses = null;
	                    $lessons = null;
	                    $categories = null;
	                    $category_list = null;
	                    $usergroup = null;
	                    $accesslevel = null;
	                    $events = null;
	                    $event_action = null;

	                    //Don't check it if it's been turned off.
	                    if ($req->requirement_active == "true") {

	                    	switch ($req->requirement_type) {
	                            case "category":
	                                $list = $req->required_category;
	                                if ($list) {
	                                    $query = "SELECT splms_coursescategory_id, splms_course_id FROM joom_splms_courses WHERE FIND_IN_SET(splms_coursescategory_id, '" . implode(",", $list) . "')";
	                                    $db->setQuery($query);

	                                    //If any of the categories need to be met, we need to check each category individually.
	                                    $categories = $db->loadObjectList();
	                                    $category_list = $list;

	                                } else {
	                                    //It isn't set up correctly. There should be a list.
	                                    $badge_good = false;
	                                }

	                                break;
	                            case "course":
	                                $list = $req->required_course;

	                                if ($list) {
	                                    $query = "SELECT splms_course_id FROM joom_splms_courses WHERE FIND_IN_SET(splms_course_id, '" . implode(",", $list) . "')";
	                                    $db->setQuery($query);
	                                    $courses = $db->loadObjectList();
	                                } else {
	                                    //It isn't set up correctly. There should be a list.
	                                    $badge_good = false;
	                                }
	                                break;
	                            case "event":
	                                $list = $req->required_event;
	                                $event_action = $req->required_event_action;

	                                if ($list) {
	                                    $query = "SELECT id FROM joom_eb_events WHERE FIND_IN_SET(id, '" . implode(",", $list) . "')";
	                                    $db->setQuery($query);
	                                    $events = $db->loadObjectList();
	                                } else {
	                                    //It isn't set up correctly. There should be a list.
	                                    $badge_good = false;
	                                }
	                                break;
	                            case "lesson":
	                                $list = $req->required_lesson;
	                                if ($list) {
	                                    $query = "SELECT splms_lesson_id FROM joom_splms_lessons WHERE FIND_IN_SET(splms_lesson_id, '" . implode(",", $list) . "')";
	                                    $db->setQuery($query);
	                                    $lessons = $db->loadObjectList();
	                                } else {
	                                    //It isn't set up correctly. There should be a list.
	                                    $badge_good = false;
	                                }

	                                break;

	                            case "usergroup":

	                            	$list = $req->usergroup;
	                            	if (!$list) {
	                            		$badge_good = false;
	                            	}

	                            	//This doesn't require a check here.  Just check teh user if they have the group.
	                            	$usergroup = $list;

	                            	break;

	                            case "accesslevel":
	                            	$list = $req->accesslevel;
	                            	if (!$list) {
	                            		$badge_good = false;
	                            	}

	                            	//Create a map of the access levels.

	                            	$query = "SELECT id,rules FROM joom_viewlevels";
	                            	$db->setQuery($query);
	                            	$viewlevels = $db->loadObjectList();

	                            	$accesslevel = array();

	                            	foreach ($viewlevels as $viewlevel) {
	                            		//see if the access level is one required for the badge.
	                            		if (in_array($viewlevel->id, $list)) {
	                            			//All view level rules are stored in JSON
	                            			$accesslevel[$viewlevel->id] = json_decode($viewlevel->rules);
	                            		}
	                            	}

	                            	break;

	                            default:

	                            	//Something wrong, this badge has an incorrect type.
	                            	$badge_good = false;
	                            	break;
	                        }


		                    //Get the completion type.  ANY or ALL
		                    $comp_type = $req->requirement_completion;

		                    if (!$badge_good) {
		                        //There is already an issue with the way the badge is set up.
		                        //Don't check the rest.

		                        continue;
		                    }

		                    foreach ($users as $user) {
								$allConditionsMet = true;
		                    	$earned_badge = true;

		                    	if ($categories) {
			                        $categories_list = array();

			                        //Since we need to check each category separately, divide the courses into an array indexed by the category ID.
			                        foreach ($category_list as $category) {
			                            $categories_list[$category] = array();
			                        }

			                        foreach ($categories as $category) {
			                            array_push($categories_list[$category->splms_coursescategory_id], $category->splms_course_id);
			                        }

			                        $earned_badge = self::checkCategorySuccess($user, $categories_list, $comp_type);

			                        if (!$earned_badge) {
				                    	//They already have failed at least one requirement.  Don't check the rest.
				                    	if ($canlose) {
			                    			//If they already have the badge, but are no longer earning it, take it away.

			                    			//Check to see if they already have the badge.

			                    			$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                        continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }


			                    if ($courses) {

			                        $course_list = array();
			                        foreach ($courses as $course) {
			                            array_push($course_list, $course->splms_course_id);
			                        }

			                        $earned_badge = self::checkCourseSuccess($user, $course_list, $comp_type);
			                        if (!$earned_badge) {
			                        	if ($canlose) {
											$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                    	continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }

			                    if ($events) {

			                        $event_list = array();
			                        foreach ($events as $event) {
			                            array_push($event_list, $event->id);
			                        }

			                        $earned_badge = self::checkEventRegistration($user, $event_list, $comp_type, $event_action);

			                        if (!$earned_badge) {
			                        	if ($canlose) {
											$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                    	continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }

			                    if ($lessons) {
			                        $lesson_list = array();
			                        foreach ($lessons as $lesson) {
			                            array_push($lesson_list, $lesson->splms_lesson_id);
			                        }

			                        $earned_badge = self::checkLessonSuccess($user, $lesson_list, $comp_type);

			                        if (!$earned_badge) {

			                        	if ($canlose) {
											$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                    	continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }

			                    if ($usergroup) {

			                    	$query = "SELECT group_id FROM joom_user_usergroup_map WHERE user_id = $user";
			                    	$db->setQuery($query);
			                    	$results = $db->loadObjectList();
			                    	$groups = array();

			                    	//Pull all of the group ids from the array of objects and put them into a simple numbers array.
			                    	foreach ($results as $group) {
			                    		$groups[]= $group->group_id;
			                    	}

			                    	$has_all = true;
			                    	$has_any = false;

			                    	foreach ($usergroup as $ug) {
			                    		if (in_array($ug, $groups)) {
			                    			//It at least has one.
			                    			$has_any = true;
			                    		} else {
			                    			//It doesn't have one that's required.
			                    			$has_all = false;
			                    		}
			                    	}

									$earned_badge = false;

			                    	if ($comp_type == "any") {
			                    		if ($has_any) {
			                    			$earned_badge = true;
			                    		}
			                    	} else {	//all
			                    		if ($has_all) {
			                    			$earned_badge = true;
			                    		}
									}

			                    	if (!$earned_badge) {
			                    		if ($canlose) {
			                    			$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                    	continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }

			                    if ($accesslevel) {

			                    	$query = "SELECT group_id FROM joom_user_usergroup_map WHERE user_id = $user";
			                    	$db->setQuery($query);
			                    	$results = $db->loadObjectList();
			                    	$groups = array();

			                    	//Pull all of the group ids from the array of objects and put them into a simple numbers array.
			                    	foreach ($results as $group) {
			                    		$groups[]= $group->group_id;
			                    	}

			                    	$has_any = false;
			                    	$has_all = true;

			                    	foreach ($accesslevel as $level) {
			                    		$has_access_level = false;
			                    		foreach ($level as $lv) {
			                    			if (in_array($lv, $groups)) {
			                    				$has_access_level = true;
			                    				break;
			                    			}
			                    		}

			                    		if ($has_access_level) {
			                    			$has_any = true;
			                    		} else {
			                    			$has_all = false;
			                    		}
			                    	}

			                    	$earned_badge = false;
			                    	if ($comp_type == "any") {
			                    		if ($has_any) {
			                    			$earned_badge = true;
			                    		}
			                    	} else {
			                    		if ($has_all) {
			                    			$earned_badge = true;
			                    		}
			                    	}

			                    	if (!$earned_badge) {
			                    		if ($canlose) {
											$query = "DELETE FROM axs_awards_earned WHERE badge_id = $badge_id AND user_id = $user";
			                    			$db->setQuery($query);
			                    			$db->execute();
										}
										if($allRequired) {
											$allConditionsMet = false;
											array_push($userRemoveList,$user);
										}
				                    	continue;
				                    } else {
										if(!$allRequired || $allConditionsMet) {
											array_push($userEarnedList,$user);
										}
									}
			                    }
							}
			            }
					}
			}

			$userEarnedList = array_unique($userEarnedList);
			$userRemoveList = array_unique($userRemoveList);

			foreach($userEarnedList as $userId) {
				$badge_exist = AxsActions::checkAward($badge_id,$userId);

				if(!$badge_exist && !in_array($userId,$userRemoveList)) {
					$query = "INSERT IGNORE INTO axs_awards_earned (user_id, badge_id, date_earned, date_expires) VALUES ($userId, $badge_id, '$issue_date', '$expiration')";
					$db->setQuery($query);

                    $insertResult = $db->execute();

                    if($insertResult) {
                        
                        $checklistItemType = ($badge->type == 'certificate') ? 'certificate_awarded' : 'badge_awarded';

                        AxsLMS::markUserChecklistItemsCompletedForItemType($userId, $badge_id, $checklistItemType);
                    }

					if($params->usergroup_actions && ($params->add_usergroups || $params->remove_usergroups)) {
						$groupData = new stdClass();
						$groupData->user_id = $userId;
						$groupData->usergroup_actions = $params->usergroup_actions;
						$groupData->add_usergroups = $params->add_usergroups;
						$groupData->remove_usergroups = $params->remove_usergroups;
						AxsActions::setGroups($groupData);
					}

					if($params->points_actions && $params->points_category && $params->points) {
						$awardData = new stdClass();
						$awardData->user_id = $userId;
						$awardData->category_id = $params->points_category;
						$awardData->points = $params->points;
						$awardData->date = $issue_date;
						$awardData->params = json_encode(array('award_id' => $badge_id));
						AxsAwards::awardPoints($awardData);
					}
				}
			}
		}
	}

	public static function checkCategorySuccess($profile_id, $category_list, $comp_type) {
        if (!$profile_id || !$category_list) {
            return false;
        }

        $category_pass = array();

        foreach ($category_list as $key => $category) {
            if (count($category) == 0) {
                //The category is empty.
                $category_pass[$key] = false;
            } else {
                $category_pass[$key] = self::checkCourseSuccess($profile_id, $category, "all");
            }
        }

        if ($comp_type == "any") {
            //Only one needs to be passed
            $good = false;
            foreach ($category_pass as $pass) {
                if ($pass) {
                    $good = true;
                }
            }
        } else {
            $good = true;
            foreach ($category_pass as $pass) {
                if (!$pass) {
                    $good = false;
                }
            }
        }

        return $good;
    }

    public static function checkEventRegistration($profile_id, $event_list, $comp_type, $event_action) {

        if (!$profile_id || !$event_list) {
            return false;
        }

        $checked_in = '';
        if($event_action == 'checkin') {
        	$checked_in = ' AND checked_in = 1 ';
        }

        $db = JFactory::getDBO();
        //Get all events that the user has registered for.
        $query = "SELECT event_id FROM joom_eb_registrants WHERE user_id = $profile_id $checked_in AND FIND_IN_SET(event_id, '" . implode(",", $event_list) . "')";
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $registered_list = array();

        //Create an array out of the passed list.
        foreach ($results as $result) {
            array_push($registered_list, $result->event_id);
        }
        return self::compareLists($registered_list, $event_list, $comp_type);
    }

    public static function checkCourseSuccess($profile_id, $course_list, $comp_type) {

        if (!$profile_id || !$course_list) {
            return false;
        }

        $db = JFactory::getDBO();

        //Get all of the courses the user has passed that are in the course list.
        $query = "SELECT course_id 
		FROM joom_splms_course_progress 
		WHERE archive_date IS NULL AND progress>=100 AND user_id = $profile_id AND FIND_IN_SET(course_id, '" . implode(",", $course_list) . "')";
        $db->setQuery($query);
        $results = $db->loadObjectList();

        $passed = array();

        //Create an array out of the passed list.
        foreach ($results as $result) {
            array_push($passed, $result->course_id);
        }
		$passed = array_unique($passed);
        return self::compareLists($passed, $course_list, $comp_type);
    }

    public static function checkLessonSuccess($profile_id, $lesson_list, $comp_type) {
        if (!$profile_id || !$lesson_list) {
            return false;
        }

        $db = JFactory::getDBO();

        $query = "SELECT lesson_id FROM joom_splms_lesson_status WHERE archive_date IS NULL AND status='completed' AND user_id = $profile_id AND FIND_IN_SET(lesson_id, '" . implode(",", $lesson_list) . "')";

        $db->setQuery($query);
        $results = $db->loadObjectList();

        $passed = array();

        //Create an array out of the passed list.
        foreach ($results as $result) {
            array_push($passed, $result->lesson_id);
        }

        return self::compareLists($passed, $lesson_list, $comp_type);
    }

    public static function compareLists($passed, $needed, $comp_type) {
        if ($comp_type == "all") {

            //All material in the needed list must be passed.
            if (count($passed) != count($needed)) {
                //They're not even the same length, don't check.
                return false;
            }

            $good = true;
            foreach ($needed as $need) {

                if (!in_array($need, $passed)) {

                    $good = false;
                }
            }
        } else if ($comp_type = "any") {
            $good = false;
            foreach ($needed as $need) {
                if (in_array($need, $passed)) {
                    $good = true;
                }
            }
        }
        return $good;
    }

   	public static function removeAllBadgesForUser($user_id) {
   		if (!$user_id) {
   			return;
   		}

   		$db = JFactory::getDBO();
   		$query = "DELETE FROM axs_awards_earned WHERE user_id = $user_id";
   		$db->setQuery($query);
   		$db->execute();
   	}

   	public static function removeUserGroupBadges($user_group_id) {
   		$db = JFactory::getDBO();
   		$query = "SELECT * FROM axs_awards";
   		$db->setQuery($query);
   		$results = $db->loadObjectList();
   		//Cycle through each badge and get it's params

   		foreach ($results as $result) {
   			$params = json_decode($result->params);
   			$requirements = $params->requirements;

   			//Go through each requirement and see if the type is a usergroup
   			foreach ($requirements as $req) {
   				if ($req->requirement_type == "usergroup") {
   					//If so, get it's list of required usergroups
   					$list = $req->usergroup;
   					if (in_array($user_group_id, $list)) {
   						//The usergroup IS required for the badge.
   					}
   				}
   			}
   		}
   	}

    public static function getMultiLingualFieldset() {

    	ob_start();


    	$db = JFactory::getDBO();
    	$query = "SELECT * FROM joom_languages WHERE published = 1";
    	$db->setQuery($query);
    	$languages = $db->loadObjectList();

		?>

			<fieldset
		        name="multilingual"
		        label="Multilingual Overrides"
		        class="tab-pane"
		    >

		    	<?php
		    		foreach ($languages as $language) {
		    			$code = $language->lang_code;
		    			$title = $language->title;
		    	?>

		    			<field
							name="lang_use_override_<?php echo $code;?>"
							label='Enable <?php echo $title;?> Override'
							type='radio'
							class="radio btn-group"
							default="0"
						>
							<option value="0">No</option>
							<option value="1">Yes</option>
						</field>

		    			<field
				            name="badge_hover_<?php echo $code;?>"
				            type="textarea"
				            label="Hover Text <?php echo $title;?>"
				            description="Text that is displayed when a user hovers their mouse over the badge"
				            showon="lang_use_override_<?php echo $code;?>:1"
				        />

				        <field
				            name="certificate_<?php echo $code;?>"
				            label="Certificate <?php echo $title;?>"
				            type="sql"
				            query="SELECT * FROM axs_awards_certificates"
				            key_field="id"
				            value_field="title"
				            showon="type:certificate[AND]lang_use_override_<?php echo $code;?>:1"
				        />

				        <field
				            name="badge_image_<?php echo $code;?>"
				            type="media"
            				mediatype="files"
				            label="Thumbnail <?php echo $title;?>"
				            showon="type:badge[AND]lang_use_override_<?php echo $code;?>:1"
				        />

		    	<?php
		    		}
		    	?>



		    </fieldset>
		<?php

		return ob_get_clean();
    }
}
