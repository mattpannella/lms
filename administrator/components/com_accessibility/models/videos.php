<?php
defined('_JEXEC') or die;

class AccessibilityModelVideos extends FOFModel {
	public function save($data) {

			$params = new stdClass();			
			$params->video_type 	= $data['video_type'];
			$params->video_url  	= $data['video_url'];
			$params->captions    	= json_encode($data['captions']);
			$params->video_poster   = $data['video_poster'];
			
			if (isset($data['youtube_id'])){
				$video_id = AxsMedia::getVideoID($data['youtube_id'], $data['video_type']);
				if ($video_id) {
					$params->youtube_id = $video_id;
				} else {
					$params->youtube_id = $data['youtube_id'];
				}
			
			}

			if (isset($data['vimeo_id'])) {
				$video_id = AxsMedia::getVideoID($data['vimeo_id'], $data['video_type']);
				if ($video_id) {
					$params->vimeo_id = $video_id;
				} else {
					$params->vimeo_id = $data['vimeo_id'];
				}
			}

		switch ($data['video_type']) {
			case 'mp4':
				$data['path']  = $params->video_url;
			break;
			
			case 'youtube':
				$data['path']  = $params->youtube_id;
			break;

			case 'vimeo':
				$data['path']  = $params->vimeo_id;
			break;
		}

		$data['params'] = json_encode($params);
		return parent::save($data);
	}

	public function loadFormData() {	

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {
			$params = json_decode($data['params']);
			$data['video_type'] 	= $params->video_type;
			$data['youtube_id'] 	= $params->youtube_id;
			$data['vimeo_id']   	= $params->vimeo_id;
			$data['video_url']  	= $params->video_url;
			$data['captions']   	= $params->captions;
			$data['video_poster']   = $params->video_poster;
			return $data;
		}
	}
}