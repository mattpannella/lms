<?php

	defined('_JEXEC') or die();
	
	$doc = JFactory::getDocument();

	include_once JPATH_LIBRARIES.'/fof/include.php';
	
	if (!defined('FOF_INCLUDED')) {
		JError::raiseError ('500', 'FOF is not installed');
		return;
	}

	FOFDispatcher::getTmpInstance('com_accessibility')->dispatch();