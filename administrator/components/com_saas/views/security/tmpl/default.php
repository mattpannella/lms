<script>
	//This doesn't use FOF so the toolbar.php doesn't work.  Just remove the "Save & New button since it doesn't do anything".
	var checkButtons = function() {
		var buttons = document.getElementsByTagName("button");
		var remove = null;
		if (buttons.length > 0) {
			for (var i = 0; i < buttons.length; i++) {
				var classlist = buttons[i].classList;
				for (j = 0; j < classlist.length; j++) {
					switch (classlist[j]) {
						case "button-save-new":
							remove = buttons[i];
							break;
					}
				}

			}
		}

		if (remove) {
			clearInterval(removeButtons);
			jQuery(remove.parentNode).remove();
		}
	}

	var removeButtons = setInterval(
		checkButtons, 100
	);

	var save_data = {};
	jQuery(document).ready(
		function() {

			jQuery("#loading").hide();
			jQuery("#main_area").show();

			jQuery(".table_cell_component").click(
				function() {
					var component = this.getAttribute("component");

					jQuery(".component_list").hide();
					jQuery(".controller_list").hide();
					jQuery(".view_list").hide();
					jQuery("#component_list_" + component).show();

					var selected = document.getElementsByClassName("selected_component");
					if (selected.length > 0) {
						jQuery(selected[0]).removeClass('selected_component');
					}
					jQuery(this).addClass('selected_component');
				}
			);

			jQuery(".table_cell_controller").click(
				function() {
					var controller = this.getAttribute("controller");

					jQuery(".controller_list").hide();
					jQuery(".view_list").hide();
					jQuery("#controller_list_" + controller).show();

					var selected_view = document.getElementsByClassName("selected_view");
					if (selected_view.length > 0) {
						jQuery(selected_view[0]).removeClass('selected_view');
					}

					var selected_controller = document.getElementsByClassName("selected_controller");
					if (selected_controller.length > 0) {
						jQuery(selected_controller[0]).removeClass('selected_controller');
					}

					jQuery(this).addClass('selected_controller');
				}
			);

			jQuery(".table_cell_view").click(
				function() {
					var view = this.getAttribute("view");

					jQuery(".controller_list").hide();
					jQuery(".view_list").hide();
					jQuery("#view_list_" + view).show();

					var selected_view = document.getElementsByClassName("selected_view");
					if (selected_view.length > 0) {
						jQuery(selected_view[0]).removeClass('selected_view');
					}

					var selected_controller = document.getElementsByClassName("selected_controller");
					if (selected_controller.length > 0) {
						jQuery(selected_controller[0]).removeClass('selected_controller');
					}

					jQuery(this).addClass('selected_view');
				}
			);

			jQuery(".select_all").click(
				function() {
					var controller = this.getAttribute("controller");
					var column = this.getAttribute("column");

					var checkboxes = document.getElementsByClassName("checkbox_controller_" + controller);
					var checked = this.checked;
					var group = this.getAttribute("group");

					for (var i = 0; i < checkboxes.length; i++) {
						if (checkboxes[i].getAttribute("column") == column && checkboxes[i].getAttribute("group") == group) {
							checkboxes[i].checked = checked;
						}
					}

				}
			);

			
			var save_all =function() {
				var checkboxes = document.getElementsByClassName("save_check");
				save_data = {};
				for (var i = 0; i < checkboxes.length; i++) {
					var box = checkboxes[i];
					if (box.checked) {
						var component = box.getAttribute("component");
						var controller = box.getAttribute("controller");
						var method = box.getAttribute("method");
						var view = box.getAttribute("view");
						var column = box.getAttribute("column");

						if (typeof (save_data[component]) === 'undefined') {
							save_data[component] = {};
						}

						if (typeof(save_data[component][controller]) === 'undefined') {
							save_data[component][controller] = {};
							save_data[component][controller]['methods'] = {};
							save_data[component][controller]['views'] = {};
						}

						if (method) {
							if (typeof(save_data[component][controller]['methods'][method]) === 'undefined') {
								save_data[component][controller]['methods'][method] = {};
							}

							save_data[component][controller]['methods'][method][column] = true;
						}

						if (view) {
							if (typeof(save_data[component][controller]['views'][view]) === 'undefined') {
								save_data[component][controller]['views'][view] = {};
							}

							save_data[component][controller]['views'][view][column] = true;
						}
					}
				}

				//console.log(save_data);
				//console.log(JSON.stringify(save_data));

				jQuery.ajax({
			        type: 'POST',
			        url: 'index.php?option=com_saas&task=security.save&format=raw', 
			        data: {
			        	data:save_data
			        },

			        success: function(response) {

			        	console.log(response);
			        }
			    });
			}

			jQuery(".button-save, .button-apply, .button-cancel").click(
			function (event) {
				event.preventDefault();
				var onclick = this.getAttribute("onclick");
				//Take "Joomla.submitbutton('apply')"; and turn it into "apply"
				var action = onclick.replace("Joomla.submitbutton('", "").replace("');", "");
				var save = false;
				var close = false;
				switch (action) {
					case "apply":
						save = true;
						break;
					case "save":
						save = true;
						close = true;
					case "cancel":
						close = true;
						break;
				}

				if (save) {
					save_all();
				}

				if (close) {
					window.location.href = "/administrator/index.php?option=com_saas";
				}
			}
		);
		}
	);


</script>

<?php 
	if (!JFactory::getUser()->authorise('core.edit')) {
	die();
}

$directory = JPATH_ADMINISTRATOR . "/components/";

$original_jpath = JPATH_COMPONENT;
$original_jpath_admin = JPATH_COMPONENT_ADMINISTRATOR;

define("COMMUNITY_ASSETS_URL", JPATH_ADMINISTRATOR . "/components/com_community/assets");

//These are needed for it to scan com_eventbooking.
require_once(JPATH_ADMINISTRATOR . "/components/com_eventbooking/libraries/rad/controller/controller.php");
require_once(JPATH_ADMINISTRATOR . "/components/com_eventbooking/libraries/rad/controller/admin.php");
require_once(JPATH_ADMINISTRATOR . "/components/com_eventbooking/libraries/rad/inflector/inflector.php");

$components = array();

$all_components = scandir($directory);

foreach ($all_components as $folder) {
	if ($folder != "." && $folder != ".." && $folder != "index.html") {

		$newComponent = new stdClass();

		$newComponent->name = $folder;
		$newComponent->controllers = array();
		$newComponent->views = array();
		
		$com_folders = scandir($directory . $folder);

		foreach ($com_folders as $com_folder) {
			
			if ($com_folder != "." && $com_folder != "..") {
				if ($com_folder == "controllers" || $com_folder == "controller") {

					runkit_constant_redefine('JPATH_COMPONENT', JPATH_ADMINISTRATOR . "/components/" . $folder);
					runkit_constant_redefine('JPATH_COMPONENT_ADMINISTRATOR', JPATH_ADMINISTRATOR . "/components/" . $folder);

					$controller = $com_folder;

					$controller_files = scandir($directory . $folder . "/" . $controller);

					$classes = new stdClass();

					foreach ($controller_files as $file) {

						//For base controllers in the main folder
						if ($file == "controller.php") {
							include ($directory . $folder . "/" . $controller . "/" . $file);
						}

						if ($file != "." && $file != ".." && !strpos($file, ".html")) {
							$read = fopen($directory . $folder . "/" . $com_folder . "/" . $file, "r");

							if ($read) {
								$done = false;
								while (!$done) {
									$line = fgets($read);

									if (!$line) {
										$done = true;
									}

									if (strpos($line, "*") !== false) {
										//This is a comment.  A class declaration could not contain a *
										continue;
									}

									$lower = strtolower($line);

									//declarations are in the format "class ThisControllerName"
									if (strpos($lower, "class") !== false && strpos($lower, "controller") !== false) {
										$words = explode(" ", $line);											
										$prev = null;
										$main_class = null;
										$extends_class = null;
										foreach ($words as $word) {

											if (strtolower($prev) == "class") {
												$main_class = $word;
											} else if (strtolower($prev) == "extends") {
												$extends_class = $word;
											}

											if (!isset($classes->$file)) {
												$classes->$file = new stdClass();
												$classes->$file->extend = array();
												$classes->$file->main = array();
											}

											$prev = $word;												
										}

										//Remove any white space as it will make class_exists fail.
										$extends_class = rtrim(ltrim($extends_class));	
										$main_class = rtrim(ltrim($main_class));

										if (class_exists($extends_class)) {
											if (!class_exists($main_class)) {
												include_once($directory . $folder . "/" . $controller . "/" . $file);

												$newController = new stdClass();
												$newController->name = $main_class;
												$newController->methods = array();

												$methods = get_class_methods($main_class);
												foreach ($methods as $method) {
													array_push($newController->methods, $method);
												}

												array_push($newComponent->controllers, $newController);
											}
										}
									}
								}
							}
						}
					}

				} else if ($com_folder == "views") {
					$views = scandir($directory . "/" . $folder . "/" . $com_folder);
					foreach ($views as $view) {
						if ($view != "." && $view != ".." && !strpos($view, ".html")) {
							array_push ($newComponent->views, $view);
						}
					}
				}
			}
		}		
	}
		
	array_push($components, $newComponent);
}

?>

	<style>
		.table_header {
			padding: 8px;
			font-size: 20px;
		}

		.table_cell {
			cursor: pointer;
			padding: 8px;
		}

		.table_cell:hover {
			color: rgb(255,0,0);
		}

		.component_list {
			height: 100%;
			overflow-y: hidden;
		}

		.data_column {
			width: 30%;
			height: 800px;
			float: left;
		}

		.data_column_double {
			width: 35%;
		}

		.selected_component {
			background-color: rgb(200, 200, 255);
			font-size: 24px;
		}

		.selected_controller {
			background-color: rgb(200, 200, 255);
			font-size: 24px;
		}

		.selected_view {
			background-color: rgb(200, 200, 255);
			font-size: 24px;
		}


	</style>

	<div id="loading" style="font-size:32px">
		LOADING
	</div>
	<div id="main_area" style="display:none;">
		<?php /*<div style="width:100%">
			<div id="save_all" class="btn btn-primary">Save</div>
		</div>*/?>
		
		<div class="data_column">
			<div style="overflow-y:scroll; height: 100%">
				<table class="tables table-striped">
					<tr><td class="table_header">Name</td></tr>
					<?php
						foreach ($components as $component) {
					?>
							<tr><td class="table_cell table_cell_component" component="<?php echo $component->name;?>"><?php echo $component->name; ?></td></tr>
					
					<?php
						}
					?>
				</table>
			</div>
		</div>

		<?php
			foreach ($components as $component) {
		?>

				<div id="component_list_<?php echo $component->name;?>" class="component_list data_column" style="display:none;">
					<div style="overflow-y:scroll; height: 100%">
						<table class="tables table-striped" style="margin-bottom: 20px;">
							<tr>
								<td class="table_header">Controller</td>							
							</tr>

							<?php
								foreach ($component->controllers as $controller) {
							?>

									<tr>
										<td class="table_cell table_cell_controller" controller="<?php echo $controller->name;?>">
											<?php echo $controller->name; ?>
										</td>
									</tr>
							
							<?php
								}
							?>
						</table>

						<table class="tables table-striped">
							<tr>
								<td class="table_header">Views</td>							
							</tr>
							<?php
								foreach ($component->views as $view) {
							?>
							
									<tr>
										<td class="table_cell table_cell_view" view="<?php echo $view;?>">
											<?php echo $view; ?>
										</td>
									</tr>
							
							<?php
								}
							?>
						</table>
					</div>
				</div>

		<?php
			}

			foreach ($components as $component) {
				foreach ($component->controllers as $controller) {
					?>
						<div id="controller_list_<?php echo $controller->name;?>" class="controller_list data_column data_column_double" style="display:none;">
							<div style="overflow-y:scroll; height: 100%">
								<table class="tables table-striped" style="margin-bottom: 20px;">
									<tr>
										<td class="table_header">Methods</td>
										<td class="table_header">
											Front End
											<br>
											<span style="font-size: 14px;">Select All </span>
											<input 
												type="checkbox" 
												class="select_all" 
												controller="<?php echo $controller->name;?>" 
												column="front"
												group="method"
											>
										</td>
										<td class="table_header">
											Admin
											<br>
											<span style="font-size: 14px;">Select All </span>
											<input 
												type="checkbox" 
												class="select_all" 
												controller="<?php echo $controller->name;?>" 
												column="admin"
												group="method"
											>
										</td>									
									</tr>
									<?php
										foreach ($controller->methods as $method) {
									?>

											<tr>
												<td class="table_cell table_cell_method"><?php echo $method; ?></td>
												<td>
													<input 
														type="checkbox" 
														class="save_check checkbox_controller_<?php echo $controller->name;?>"
														component="<?php echo $component->name;?>"
														controller="<?php echo $controller->name;?>"
														method="<?php echo $method;?>"
														column="front"
														group="method"
													>
												</td>
												<td>
													<input 
														type="checkbox" 
														class="save_check checkbox_controller_<?php echo $controller->name;?>"
														component="<?php echo $component->name;?>"
														controller="<?php echo $controller->name;?>"
														method="<?php echo $method;?>"
														column="admin"
														group="method"
													>
												</td>
											</tr>
									
									<?php
										}
									?>
								</table>
							</div>
						</div>
					<?php
				}

				foreach ($component->views as $view) {
					?>
						<div id="view_list_<?php echo $view;?>" class="view_list data_column data_column_double" style="display:none;">
							<div style="overflow-y:scroll; height: 100%">
								<table class="tables table-striped" style="margin-bottom: 20px;">
									<tr>
										<td class="table_header" colspan="2">Views</td>
									</tr>
									<tr>
										<td class="table_header">
											Front End
										</td>
										<td class="table_header">
											Admin
										</td>									
									</tr>
									
									<tr>
										<td>
											<input 
												type="checkbox" 
												class="save_check checkbox_controller_<?php echo $controller->name;?>" 
												view="<?php echo $view;?>"
												component="<?php echo $component->name;?>"
												controller="<?php echo $controller->name;?>" 
												column="front"
												group="view"
											>
										</td>
										<td>
											<input 
												type="checkbox" 
												class="save_check checkbox_controller_<?php echo $controller->name;?>" 
												view="<?php echo $view;?>"
												component="<?php echo $component->name;?>"
												controller="<?php echo $controller->name;?>"
												column="admin"
												group="view"
											>
										</td>
									</tr>
								</table>
							</div>
						</div>
					<?php
				}	
			}
		?>
	</div>
			
<?php

runkit_constant_redefine('JPATH_COMPONENT', $original_jpath);
runkit_constant_redefine('JPATH_COMPONENT_ADMINISTRATOR', $original_jpath_admin);
