<?php
defined('_JEXEC') or die();

class SaasControllerApps extends FOFController {


	public function saveorder() {
        $cid = JRequest::getVar( 'cid', array(), 'post', 'array' );

        $model = $this->getModel('apps');

        if($model->saveOrder($cid)){
        $msg = JText::_('Ordering save Successfully' );
        $type = 'message';

        }else{
        $msg = JText::_( 'Ordering not save' );
        $type = 'notice';
        }

        $link = 'index.php?option=com_saas&view=apps';
        $this->setRedirect( $link, $msg, $type );
    }

}