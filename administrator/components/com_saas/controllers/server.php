<?php
defined('_JEXEC') or die();

require_once("/var/www/sitebuilder/AwsBase.php");

class SaasControllerServer extends FOFController {


	public function createServer() {
        
        $data = JRequest::getVar('specs');

        switch ($data['instance_type']) {
            case "shared":
                $dedicated_instance = 0;
                $zone = $data['location_shared'];
                $instance_type = null;
                break;
            case "dedicated":
                $zone = $data['location_dedicated'];
                $dedicated_instance = 1;
                $instance_type = $data['server_specs'];
                $data['volume_type'] = "dedicated";
                break;
            default:
                echo "Instance Type Error";
                return;
        }

        switch ($data['volume_type']) {
            case "shared":
                $dedicated_volume = 0;
                $volume_type = null;
                break;
            case "dedicated":
                $dedicated_volume = 1;
                $volume_type = $data['volume_performance'];
                break;
            default:
                echo "Volume Type Error";
                return;
        }

        
        if ($data['volume_size']) {
            $volume_size = (int)$data['volume_size'];
        } else {
            $volume_size = 0;
        }

        if (!$data['title']) {
            echo "A title is required";
            return;
        }

        $domain = $data['domain'];
        $title = $data['title'];
        $database_user = $data['database_user'];

        if (!$domain) {
            echo "A domain is required";
            return;
        } else {
            if (!$this->checkDomain($domain)) {
                echo "This domain is already taken";
                return;
            }
        }        

        $params = array(
            "dedicated_instance" => $dedicated_instance,
            "dedicated_volume" => $dedicated_volume,
            "account_zone" => $zone,
            "instance_type" => $instance_type,
            "volume_size" => $volume_size,
            "volume_type" => $volume_type,
            "site_name" => $title,
            "site_domain" => $domain,
            "database_user" => $database_user
        );

        $aws = new AwsControl();
        $new_ip = $aws::newAccount($params);
        print_r($new_ip);
    }

    function checkDomainName() {
        $domain = JRequest::getVar("domain");

        $available = $this->checkDomain($domain);

        if (!$available) {
            echo "false";
        } else {
            echo "true";
        }
    }

    private function checkDomain($domain) {
        $site_list = array(
            "tovuti.io",
            "youraxs.com"
        );

        $available = true;

        foreach ($site_list as $site) {
            $which = $domain . "." . $site;
            $free = AxsDomains::checkDomainAvailable($which);
            if (!$free) {
                $available = false;
            }
        }

        if ($available) {
            return true;
        } else {
            return false;
        }
    }
}