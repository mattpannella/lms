<?php

defined('JPATH_PLATFORM') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class JFormFieldservers extends JFormField 
{    

    public function getInput() {

        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_saas_server_options";
        $db->setQuery($query);
        $result = $db->loadObjectList();

        $servers = json_decode($result[0]->server);

        $specs = array_unique($servers->specs);
        $alias = array_unique($servers->server_alias);
      
        ob_start();

        ?>

        	<select id="<?php echo $this->id;?>" name="<?php echo $this->id;?>">
        		<?php foreach ($specs as $key => $spec) { ?>
        			<option value="<?php echo $alias[$key]; ?>"><?php echo $spec;?></option>
        		<?php } ?>
        	</select>

        <?php

        $view = ob_get_clean();
        

       	return $view;
    }

    public function getRepeatable() {
        $view = "REPEAT";
       	
       	return $view;
    }
}