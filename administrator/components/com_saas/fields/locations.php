<?php

defined('JPATH_PLATFORM') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class JFormFieldlocations extends JFormField 
{    

	public function getInput() {

		$db = JFactory::getDBO();
		$query = "SELECT * FROM axs_saas_server_options";
		$db->setQuery($query);
		$result = $db->loadObjectList();

		$location = json_decode($result[0]->location);

		$names = $location->region_name;
		$aws_names = $location->region_aws;
		$available = $location->shared_available;
	
		ob_start();

		?>

			<select id="location_dedicated" style="display:none">
				<?php foreach ($names as $key => $name) { ?>
					<option value="<?php echo $aws_names[$key]; ?>"><?php echo $name;?></option>
				<?php } ?>
			</select>

			<select id="location_shared">
				<?php foreach ($names as $key => $name) { ?>
					<?php if ($available[$key] == "1") { ?>
						<option value="<?php echo $aws_names[$key]; ?>"><?php echo $name;?></option>
					<?php } ?>
				<?php } ?>
			</select>

			<script>
				jQuery("#instance_type").change(
					function() {
						switch (jQuery("#instance_type").val()) {
							case "dedicated":
								jQuery("#location_dedicated").show(250);
								jQuery("#location_shared").hide(250);
								break;
							case "shared":
								jQuery("#location_dedicated").hide(250);
								jQuery("#location_shared").show(250);
								break;
						}
					}
				);
			</script>

		<?php

		$view = ob_get_clean();

		return $view;
	}

	public function getRepeatable() {
		$view = "REPEAT";
		
		return $view;
	}
}