<?php

defined('JPATH_PLATFORM') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class JFormFieldcreateserver extends JFormField {    

    public function getInput() {

        ob_start();

        ?>
        <style>
            .create_server {
                border: 1px solid green;
                background-color: rgb(200, 255, 200);
                border-radius: 5px;
                padding: 10px;
                width: 300px;
                font-size: 20px;
                display:none;
            }
        </style>

        <button id="create_server" class="btn btn-primary">GO</button>
        <div id="creating_server_message" class="create_server">
            <span id="create_server_text">
                Creating server, please wait
            </span>
            <span id="create_server_dots">
            </span>
            <div style="font-size: 14px">
                This may take a few minutes
            </div>
        </div>

        <script>

            var domain_text = jQuery("#domain");

            var domain_error_message = document.createElement("div");
            domain_error_message.className = "error_message";
            document.getElementById("domain").parentNode.appendChild(domain_error_message);
            domain_error_message.appendChild(document.createTextNode("Subdomain Name already taken."));

            var checkDomainName = null;

            var button_good = false;
            var domain_good = false;

            domain_text.on(
                "change keyup paste",
                function() {
                    domain_good = false;
                    clearTimeout(checkDomainName);
                    checkDomainName = setTimeout(
                        function() {
                            jQuery.ajax({
                                url: 'index.php?option=com_saas&task=server.checkDomainName&format=raw',
                                data: {
                                    domain: domain_text.val()
                                },
                                type: 'post'
                            }).done(
                                function(result) {                                    
                                    if (result == 'true') {                                        
                                        domain_good = true;
                                        jQuery(domain_error_message).hide(250);
                                    } else {                                        
                                        domain_good = false;
                                        jQuery(domain_error_message).show(250);
                                    }
                                }
                            );
                        }, 100
                    );
                }
            );

            var update_wait_dots;
            var dot_count = 0;

            var check_server_message = function() {
                if (jQuery("#creating_server_message").is(":visible")) {
                    update_wait_dots = setInterval(
                        function() {
                            dot_count++;
                            if (dot_count > 3) {
                                dot_count = 0;
                            }
                            var text = "";
                            for (var i = 0; i < dot_count; i++) {
                                text += ".";
                            }
                            document.getElementById("create_server_dots").innerHTML = text;
                        }, 250
                    );
                } else {
                    if (update_wait_dots) {
                        clearInterval(update_wait_dots);
                        update_wait_dots = null;
                    }
                }
            }

            setInterval(
                function() {

                    var volume_size = parseInt(document.getElementById("volume_size").value);
                    var min = parseInt(document.getElementById("volume_size").getAttribute("min"));
                    var max = parseInt(document.getElementById("volume_size").getAttribute("max"));

                    var volume_type = document.getElementById("volume_type").value;
                    var instance_type = document.getElementById("instance_type").value;
                    var server_button = document.getElementById("create_server");

                    var error = document.getElementById("volume_size_error");

                    var title = document.getElementById("title").value;
                    var domain = document.getElementById("domain").value;

                    button_good = false;
                    var show_error = false;

                    if (volume_type == "shared" && instance_type == "shared") {
                        button_good = true;
                    } else {                        
                        if (volume_size) {
                            
                            if (volume_size < min) {
                                button_good = false;
                                show_error = true;
                            } else if (volume_size > max) {
                                button_good = false;
                                show_error = true;
                            } else {
                                button_good = true;
                                show_error = false;
                            }
                        } else {
                            show_error = true;
                            button_good = false;
                        }
                    }

                    if (title == "" || domain == "") {
                        button_good = false;
                    }

                    if (show_error) {
                        jQuery(error).show(250);
                    } else {
                        jQuery(error).hide(250);
                    }

                    if (button_good && domain_good) {
                    //if (button_good) {
                        server_button.disabled = false;
                    } else {                        
                        server_button.disabled = true;
                    }
                },
                250
            );

            jQuery(create_server).click(
                function(e) {
                    e.preventDefault();

                    var specs_array = {
                        title : document.getElementById("title").value,
                        domain : document.getElementById("domain").value,
                        instance_type : document.getElementById("instance_type").value,
                        server_specs : document.getElementById("specs").value,
                        database_user : document.getElementById("database_user").value,
                        volume_performance : document.getElementById("volume_performance").value,
                        volume_type : document.getElementById("volume_type").value,
                        volume_size : document.getElementById("volume_size").value,
                        location_dedicated : document.getElementById("location_dedicated").value,
                        location_shared : document.getElementById("location_shared").value
                    };

                    data = {
                        specs: specs_array
                    };

                    jQuery("#create_server").hide(250);
                    jQuery("#creating_server_message").show(250);

                    check_server_message();

                    jQuery.ajax({
                        url: 'index.php?option=com_saas&task=server.createServer&format=raw',
                        data: data,
                        type: 'post'
                    }).done(function (result) {
                        console.log(result);
                        jQuery("#create_server").show(250);
                        jQuery("#creating_server_message").hide(250);
                    }).fail(function () {
                        alert("An Error occurred.");
                    });
                }
            );

        </script>

        <?php

        $view = ob_get_clean();        

       	return $view;
    }

    public function getRepeatable() {
        $view = "REPEAT";
       	
       	return $view;
    }
}