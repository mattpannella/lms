<?php

defined('JPATH_PLATFORM') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class JFormFieldvolumes extends JFormField 
{    

    public function getInput() {

        $db = JFactory::getDBO();
        $query = "SELECT * FROM axs_saas_server_options";
        $db->setQuery($query);
        $result = $db->loadObjectList();

        $storage = json_decode($result[0]->storage);

        $types = $storage->type;
        $alias = $storage->volume_alias;
        $min = $storage->min;
        $max = $storage->max;

        $data = array();

        ob_start();

        ?>


            Type<br>
            <select id="volume_performance" name="<?php echo $this->id;?>">
        		<?php foreach ($types as $key => $type) { ?>
        			<option value="<?php echo $alias[$key]; ?>"><?php echo $type;?></option>
                    <?php
                        $new = new stdClass();                        
                        $new->min = $min[$key];
                        $new->max = $max[$key];
                        $data[$alias[$key]]= $new;
                    ?>
        		<?php } ?>
        	</select>
            <br>Size (MB)<br>
            <input id="volume_size" type="number" min="<?php echo $min[0];?>" max="<?php echo $max[0];?>">

            <style>
                .error_message {
                     border: 1px solid red; 
                     border-radius: 4px;
                     padding: 8px;
                     font-size: 16px;
                     margin-top: 4px;
                     width: 200px;
                     background-color: rgb(255, 200, 200);
                     display: none;
                }
            </style>

            <div id="volume_size_error" class="error_message">
                Invalid Volume Size
            </div>

            <div id="volume_data" data="<?php echo base64_encode(json_encode($data));?>"></div>

            <script>
                var vol_performance = document.getElementById("volume_performance");
                var vol_size = document.getElementById("volume_size");

                var volume_data = document.getElementById("volume_data").getAttribute("data");
                volume_data = JSON.parse(atob(volume_data));

                jQuery(vol_performance).change(
                    function() {
                        var val = jQuery(vol_performance).val();

                        var min = volume_data[val].min;
                        var max = volume_data[val].max;
                        
                        vol_size.setAttribute("min", min);
                        vol_size.setAttribute("max", max);
                    }
                );
            </script>

        <?php

        $view = ob_get_clean();
        

       	return $view;
    }

    public function getRepeatable() {
        $view = "REPEAT";
       	
       	return $view;
    }
}