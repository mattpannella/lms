<?php

defined('_JEXEC') or die;

class SaasModelApps extends FOFModel {
	
	public function save($data) {
		$option = JFactory::getApplication()->input->get('option');
        $view = JFactory::getApplication()->input->get('view');
        if ($option == 'com_saas' && $view == 'app') {
			$params = new stdClass();
			$params->icon_type = $data['icon_type'];
			$params->icon_background_color = $data['icon_background_color'];
			$params->icon_color = $data['icon_color'];
			$params->app_icon = $data['app_icon'];
			$params->app_image = $data['app_image'];
			$data['params'] = json_encode($params);
			$data['created_on'] = date("Y/m/d H:i:s");
			return parent::save($data);
		}
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);
			$this->_formData['icon_type'] = $params->icon_type;
			$this->_formData['icon_background_color'] = $params->icon_background_color;
			$this->_formData['icon_color'] = $params->icon_color;
			$this->_formData['app_image'] = $params->app_image;
			$this->_formData['app_icon'] = $params->app_icon;
			return $this->_formData;
		}
	}

	public function saveOrder($cid) {
		JArrayHelper::toInteger($cid);
		$total = count( $cid );
		$order = JRequest::getVar( 'order', array(0), 'post', 'array' );
		JArrayHelper::toInteger($order, array(0));
		$table = $this->getTable($this->table);
		
		// update ordering values
		for( $i=0; $i < $total; $i++ ) {
			
			$table->load( (int) $cid[$i] );
			if ($table->ordering != $order[$i]) {
				$table->ordering = $order[$i];
				if (!$table->store()) {
					$this->setError($this->_db->getErrorMsg());
					return false;
				}
			}
		}
		return true;
	}

}