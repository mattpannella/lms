jQuery(document).ready(function() {
	var db_user = document.getElementById("database_user");	

	var accepted = "abcdefghijklmnopqrstuvwxyz123456789_";

	jQuery(db_user).on("change keyup paste",
		function() {
			var text = db_user.value;
			var new_text = "";
			for (var i = 0; i < text.length; i++) {
				for (var j = 0; j < accepted.length; j++) {
					if (text[i] == accepted[j]) {
						new_text += text[i];
						break;
					}
				}
			}

			db_user.value = new_text;
		}
	);
});
