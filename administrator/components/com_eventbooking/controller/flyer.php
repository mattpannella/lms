<?php
class EventbookingControllerFlyer extends EventbookingController {	
	public function save() {
		$name = base64_decode(JRequest::getVar('name'));
		$html = base64_decode(JRequest::getVar('html'));
		$template = base64_decode(JRequest::getVar('template'));
		$which = JRequest::getVar('which');

		$db = JFactory::getDBO();

		$fields = array(
			$db->quoteName('name') . '=' . $db->quote($name),
			$db->quoteName('html') . '=' . $db->quote($html),
			$db->quoteName('template') . '=' . $db->quote($template)			
		);

		$conditions = array(
			$db->quoteName('id') . '=' . (int)$which
		);

		$query = $db->getQuery(true);
		$query
			->update($db->quotename('joom_eb_flyers'))
			->set($fields)
			->where($conditions);

		$db->setQuery($query);
		$db->execute();

		$query = "SELECT id, name FROM joom_eb_flyers";
		$db->setQuery($query);
		echo json_encode($db->loadObjectList());
	}

	public function savenew() {
		$name = base64_decode(JRequest::getVar('name'));
		$html = base64_decode(JRequest::getVar('html'));
		$template = base64_decode(JRequest::getVar('template'));

		$db = JFactory::getDBO();

		$columns = array(
			'name',
			'html',
			'template',
			'creator'
		);

		$values = array(
			$db->quote($name),			
			$db->quote($html),
			$db->quote($template),
			(int)JFactory::getUser()->id
		);

		$query = $db->getQuery(true);
		$query
			->insert($db->quotename('joom_eb_flyers'))
			->columns($db->quotename($columns))
			->values(implode(',', $values));

		$db->setQuery($query);
		$db->execute();

		$query = "SELECT id, name FROM joom_eb_flyers";
		$db->setQuery($query);
		echo json_encode($db->loadObjectList());
	}	

	public function delete() {
		$which = JRequest::getVar('which');
		$db = JFactory::getDBO();

		$query = "DELETE FROM joom_eb_flyers WHERE id=" . $which;
		$db->setQuery($query);
		$db->execute();

		$query = "SELECT id, name FROM joom_eb_flyers";
		$db->setQuery($query);
		echo json_encode($db->loadObjectList());
	}

	public function load() {
		$which = JRequest::getVar('which');
		
		$db = JFactory::getDBO();
		$query = "SELECT html, template FROM joom_eb_flyers WHERE id=" . $which;
		$db->setQuery($query);
		echo json_encode($db->loadObject());
	}
}