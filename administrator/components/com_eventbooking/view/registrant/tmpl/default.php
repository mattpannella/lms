<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

$selectedState = '';
$document = JFactory::getDocument();
$input = JFactory::getApplication()->input;

$eventParams = json_decode($this->event->params);
$ticketSettings = json_decode($eventParams->ticket_settings);
$previousEventId = intval($input->get('old_event_id', 0));

$selectedEvent = EventbookingHelperDatabase::getEvent($previousEventId);
$registrants = EventBookingHelper::getTotalRegistrants($previousEventId);
$capacity = $selectedEvent->event_capacity;

$success = true;

if(isset($capacity)) {
    // If the capacity of the event is unlimited (null or 0), automatically mark a successful registration.
    // If the number of registrants is equal to or greater than the capacity, the registration should fail.
    $success = empty($capacity) ? true : ($capacity > $registrants);
}
?>

<script>
    let success = <?php echo $success ? 'true' : 'false'; ?>;
    let registrants = <?php echo $registrants; ?>;
    let capacity = <?php echo $capacity; ?>;
    let message = '';

    if(success) {
        message = 'The event registration was a success!\n';
    } else {
        message = 'The event registration failed; the event is at capacity.\n';
    }

    if(capacity == 0) {
        message += 'Event capacity is unlimited. There are currently ' + registrants + ' registrants for this event.';
    } else {
        message += 'Remaining registrations available for event: (' + (capacity - registrants) + ' / ' + capacity + ')';
    }

    alert(message);
</script>

<form action="index.php?option=com_eventbooking&view=registrant" method="post" name="adminForm" id="adminForm" class="form mx-3 my-4" enctype="multipart/form-data">
<div class="container-fluid">
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_EVENT'); ?>
		</label>
		<div class="col-sm-9">
			<?php echo $this->lists['event_id']; ?>
		</div>
	</div>
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_NB_REGISTRANTS'); ?>
		</label>
		<div class="col-sm-9">
			<?php
				if ($this->item->number_registrants > 0)
				{
				?>
					<input type="text" name="number_registrants" value="<?php echo $this->item->number_registrants ?>" readonly="readonly" />
				<?php
				}
				else
				{
				?>
					<input class="text_area" type="text" name="number_registrants" id="number_registrants" size="40" maxlength="250" value="1" />
					<small><?php echo JText::_('EB_NUMBER_REGISTRANTS_EXPLAIN'); ?></small>
				<?php
				}
			?>
		</div>
	</div>
	<?php
		if (!empty($this->ticketTypes))
		{
		?>
			<h3>
				<?php 
					if ($ticketSettings->ticket_main_heading) {
						echo $ticketSettings->ticket_main_heading;
					} else {
						echo JText::_('EB_TICKET_INFORMATION');	
					}
				?>
			</h3>
		<?php
			foreach($this->ticketTypes AS $ticketType)
			{
				$available = $ticketType->capacity - $ticketType->registered;
				$quantity = 0;
				if (!empty($this->registrantTickets[$ticketType->id]))
				{
					$quantity = $this->registrantTickets[$ticketType->id]->quantity;
				}
				?>
				<div class="row mb-3">
					<label class="col-sm-3">
						<?php echo  $ticketType->title; ?><br/>
						<?php echo  $ticketType->description; ?>
					</label>
					<div class="col-sm-9">
						<?php
						if ($available > 0 || $quantity > 0)
						{
							$fieldName = 'ticket_type_'.$ticketType->id;
							if ($available < $quantity)
							{
								$available = $quantity;
							}
							echo JHtml::_('select.integerlist', 0, $available, 1, $fieldName, 'class="ticket_type_quantity input-small"', $quantity);
						}
						else
						{
							echo JText::_('EB_NA');
						}
						?>
					</div>
				</div>
				<?php
			}
		}
		?>
		<div class="row mb-3">
			<label class="col-sm-3">
				<?php echo  JText::_('EB_USER'); ?>
			</label>
			<div class="col-sm-9">
				<?php echo EventbookingHelper::getUserInput($this->item->user_id,'user_id',(int) $this->item->id) ; ?>
			</div>
		</div>
		<?php
		$fields = $this->form->getFields();

		if (isset($fields['state']))
		{
			$selectedState = $fields['state']->value;
		}

		foreach ($fields as $field)
		{
			$fieldType = strtolower($field->type);
			switch ($fieldType)
			{
				case 'heading':
				case 'message':
					break;
				default:
					$controlGroupAttributes = 'id="field_' . $field->name . '" ';
					if ($field->hideOnDisplay)
					{
						$controlGroupAttributes .= ' style="display:none;" ';
					}
					$class = "";
					if ($field->isMasterField)
					{
						if ($field->suffix)
						{
							$class = ' master-field-' . $field->suffix;
						}
						else
						{
							$class = ' master-field';
						}
					}
			?>
			<div class="row mb-3<?php echo $class; ?>" <?php echo $controlGroupAttributes; ?>>
				<div class="col-sm-3">
					<?php echo $field->getLabel(); ?>
				</div>
				<div class="col-sm-9">
					<?php echo $field->input; ?>
				</div>
			</div>
			<?php
				break;
			}
		}
	?>
	<div class="row mb-4">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_REGISTRATION_DATE'); ?>
		</label>
		<div class="col-sm-9">
			<?php echo JHtml::_('date', $this->item->register_date, $this->config->date_format, null);?>
		</div>
	</div>
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_TOTAL_AMOUNT'); ?>
		</label>
		<div class="col-sm-9">
			<div class="input-group">
				<span class="input-group-text">
					<?php echo $this->config->currency_symbol?>
				</span>
				<input
					name="total_amount"
					value="<?php echo $this->item->total_amount > 0 ? round($this->item->total_amount , 2) : null;?>"
					type="text"
				/>
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_DISCOUNT_AMOUNT'); ?>
		</label>
		<div class="col-sm-9">
			<div class="input-group">
				<div class="input-group-text">
					<?php echo $this->config->currency_symbol?>
				</div>
				<input
					name="discount_amount"
					value="<?php echo $this->item->discount_amount > 0 ? round($this->item->discount_amount , 2) : null;?>"
					type="text"
				/>
			</div>
		</div>
	</div>
	<?php
	if ($this->item->late_fee > 0)
	{
	?>
		<div class="row mb-3">
			<label class="col-sm-3">
				<?php echo  JText::_('EB_LATE_FEE'); ?>
			</label>
			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-text">
						<?php echo $this->config->currency_symbol?>
					</span>
					<input
						name="late_fee"
						value="<?php echo $this->item->late_fee > 0 ? round($this->item->late_fee , 2) : null;?>"
						type="text"
					/>
				</div>
			</div>
		</div>
	<?php
	}

	if ($this->event->tax_rate > 0 || $this->item->tax_amount > 0)
	{
	?>
		<div class="row mb-3">
			<label class="col-sm-3">
				<?php echo  JText::_('EB_TAX'); ?>
			</label>
			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-text"><?php echo $this->config->currency_symbol?></span>
					<input
						name="tax_amount"
						value="<?php echo $this->item->tax_amount > 0 ? round($this->item->tax_amount , 2) : null;?>" 
						type="text"
					/>
				</div>
			</div>
		</div>
	<?php
	}

	if ($this->showPaymentFee || $this->item->payment_processing_fee > 0)
	{
	?>
		<div class="row mb-3">
			<label class="col-sm-3">
				<?php echo JText::_('EB_PAYMENT_FEE'); ?>
			</label>
			<div class="col-sm-9">
				<div class="input-group">
					<span class="input-group-text"><?php echo $this->config->currency_symbol?></span>
					<input
						name="payment_processing_fee"
						value="<?php echo $this->item->payment_processing_fee > 0 ? round($this->item->payment_processing_fee , 2) : null;?>"
						type="text"
					/>
				</div>
			</div>
		</div>
	<?php
	}
	?>
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo JText::_('EB_GROSS_AMOUNT'); ?>
		</label>
		<div class="col-sm-9">
			<div class="input-group">
				<span class="input-group-text"><?php echo $this->config->currency_symbol?></span>
				<input
					name="amount"
					value="<?php echo $this->item->amount > 0 ? round($this->item->amount , 2) : null;?>"
					type="text"
				/>
			</div>
		</div>
	</div>
	<?php
		if ($this->config->activate_deposit_feature)
		{
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_DEPOSIT_AMOUNT'); ?>
				</label>
				<div class="col-sm-9">
					<div class="input-group">
						<span class="input-group-text"><?php echo $this->config->currency_symbol?></span>
						<input 
							name="deposit_amount" 
							value="<?php echo $this->item->deposit_amount > 0 ? round($this->item->deposit_amount , 2) : null;?>" 
							type="text"
						/>
					</div>
				</div>
			</div>
			<?php
				if ($this->item->payment_status == 0 && $this->item->id)
				{
				?>
					<div class="row mb-3">
						<label class="col-sm-3">
							<?php echo JText::_('EB_DUE_AMOUNT'); ?>
						</label>
						<div class="col-sm-9">
							<?php
							if ($this->item->payment_status == 1)
							{
								$dueAmount = 0;
							}
							else
							{
								$dueAmount = $this->item->amount - $this->item->deposit_amount;
							}
							?>
							<div class="input-group">
								<span class="input-group-text"><?php echo $this->config->currency_symbol?></span>
								<input 
									type="text" 
									name="due_amount" 
									value="<?php echo $dueAmount > 0 ? round($dueAmount , 2) : null;?>" 
								/>
							</div>
						</div>
					</div>
				<?php
				}
			?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_PAYMENT_STATUS'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $this->lists['payment_status'];?>
				</div>
			</div>
		<?php
		}

		if ($this->item->id && $this->item->total_amount > 0)
		{
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo EventbookingHelperHtml::getFieldLabel('re_calculate_fee', JText::_('EB_RE_CALCULATE_FEE'), JText::_('EB_RE_CALCULATE_FEE_EXPLAIN')); ?>
				</label>
				<div class="col-sm-9">
					<input type="checkbox" value="1" id="re_calculate_fee" name="re_calculate_fee" />
				</div>
			</div>
		<?php
		}

		if (!$this->item->id || $this->item->amount > 0)
		{
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_PAYMENT_METHOD'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $this->lists['payment_method']; ?>
				</div>
			</div>
		<?php
		}

		if ($this->item->amount > 0)
		{
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_TRANSACTION_ID'); ?>
				</label>
				<div class="col-sm-9">
					<input type="text" name="transaction_id" value="<?php echo $this->item->transaction_id;?>" />
				</div>
			</div>
		<?php
		}

		if ($this->item->payment_method == "os_offline_creditcard")
		{
			$params = new JRegistry($this->item->params);
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_FIRST_12_DIGITS_CREDITCARD_NUMBER'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $params->get('card_number'); ?>
				</div>
			</div>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('AUTH_CARD_EXPIRY_DATE'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $params->get('exp_date'); ?>
				</div>
			</div>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('AUTH_CVV_CODE'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $params->get('cvv'); ?>
				</div>
			</div>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo JText::_('EB_CARD_HOLDER_NAME'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo $params->get('card_holder_name'); ?>
				</div>
			</div>
		<?php
		}
		if ($this->config->activate_checkin_registrants)
		{
		?>
			<div class="row mb-3">
				<label class="col-sm-3">
					<?php echo  JText::_('EB_CHECKED_IN'); ?>
				</label>
				<div class="col-sm-9">
					<?php echo EventbookingHelperHtml::getBooleanInput('checked_in', $this->item->checked_in); ?>
				</div>
			</div>
		<?php
		}
	?>
	<div class="row mb-3">
		<label class="col-sm-3">
			<?php echo  JText::_('EB_REGISTRATION_STATUS'); ?>
		</label>
		<div class="col-sm-9">
			<?php echo $this->lists['published'] ; ?>
		</div>
	</div>
	<?php
	if ($this->item->user_ip)
	{
	?>
		<div class="row mb-3">
			<label class="col-sm-3">
				<?php echo  JText::_('EB_USER_IP'); ?>
			</label>
			<div class="col-sm-9">
				<?php echo $this->item->user_ip; ?>
			</div>
		</div>
	<?php
	}
	if ($this->config->collect_member_information && count($this->rowMembers)) 
	{
	?>
		<h3 class="eb-heading"><?php echo JText::_('EB_MEMBERS_INFORMATION') ; ?> <button type="button" class="btn btn-success" onclick="addGroupMember();"><span class="icon-new icon-white"></span><?php echo JText::_('EB_ADD_MEMBER'); ?></button></h3>
	<?php
		$n = count($this->rowMembers) + 4;
		for ($i = 0 ; $i < $n ; $i++)
		{
			if (isset($this->rowMembers[$i]))
			{
				$rowMember = $this->rowMembers[$i] ;
				$memberId = $rowMember->id ;
				$memberData = EventbookingHelper::getRegistrantData($rowMember, $this->memberFormFields);
				$style = '';
			}
			else
			{
				$memberId = 0;
				$memberData = array();
				$style = ' style="display:none;"';
			}

			$form = new RADForm($this->memberFormFields);
			$form->setEventId($this->item->event_id);
			$form->bind($memberData);
			$form->setFieldSuffix($i+1);
			$form->prepareFormFields('setRecalculateFee();');
			$form->buildFieldsDependency();
			if ($i%2 == 0)
			{
				echo "<div class=\"row-fluid\">\n" ;
			}					
			?>
				<div class="span6" id="group_member_<?php echo $i + 1; ?>"<?php echo $style; ?>>
					<h4><?php echo JText::sprintf('EB_MEMBER_INFORMATION', $i + 1); ;?><button type="button" class="btn btn-danger" onclick="removeGroupMember(<?php echo $memberId; ?>);"><span class="icon-remove icon-white"></span><?php echo JText::_('EB_REMOVE'); ?></button></h4>
					<?php
						$fields = $form->getFields();
						foreach ($fields as $field)
						{
							if ($i > 0 && $field->row->only_show_for_first_member)
							{
								continue;
							}
							$fieldType = strtolower($field->type);
							switch ($fieldType)
							{
								case 'heading':
								case 'message':
									break;
								default:
									$controlGroupAttributes = 'id="field_' . $field->name . '" ';
									if ($field->hideOnDisplay)
									{
										$controlGroupAttributes .= ' style="display:none;" ';
									}
									$class = '';
									if ($field->isMasterField)
									{
										if ($field->suffix)
										{
											$class = ' master-field-' . $field->suffix;
										}
										else
										{
											$class = ' master-field';
										}
									}
								?>
								<div class="row mb-3<?php echo $class; ?>" <?php echo $controlGroupAttributes; ?>>
									<label class="col-sm-3">
										<?php echo $field->title; ?>
									</label>
									<div class="col-sm-9">
										<?php echo $field->input; ?>
									</div>
								</div>
							<?php
							}
						}
					?>
					<input type="hidden" name="ids[]" value="<?php echo $memberId; ?>" />
				</div>
			<?php	
			if (($i + 1) %2 == 0)
			{
				echo "</div>\n" ;
			}
		}
		if ($i %2 != 0)
		{
			echo "</div>" ;
		}	
	?>				
	</table>	
	<?php	
	}
	?>				
</div>		
<div class="clearfix"></div>	
	<input type="hidden" name="id" value="<?php echo $this->item->id; ?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="group_member_id" value="0" />
	<?php echo JHtml::_( 'form.token' ); ?>
	<script type="text/javascript">
		var newMemberAdded = 0;
		var numberMembers = <?php echo (int) count($this->rowMembers); ?>;
		(function($){
			setRecalculateFee = (function() {
				$('#re_calculate_fee').prop('checked', true);
			});

			addGroupMember = (function() {
				if (newMemberAdded < 4)
				{
					newMemberAdded++;
					$('input[name=number_registrants]').val(newMemberAdded + numberMembers);
					var newMemberContainerId = 'group_member_' + (newMemberAdded + numberMembers);
					$('#' + newMemberContainerId).show();

					$('#re_calculate_fee').prop('checked', true);
				}
				else
				{
					alert('<?php echo JText::_('EB_ADD_MEMBER_MAXIMUM_WARNING'); ?>');
				}
			});

			removeGroupMember = (function(memberId) {
				if (memberId == 0)
				{
					var newMemberContainerId = 'group_member_' + (newMemberAdded + numberMembers);
					$('#' + newMemberContainerId).hide();
					newMemberAdded--;
					$('input[name=number_registrants]').val(newMemberAdded + numberMembers);
					$('#re_calculate_fee').prop('checked', true);
				}
				else
				{
					if (confirm('<?php echo JText::_('EB_REMOVE_EXISTING_MEMBER_CONFIRM'); ?>'))
					{
						var form = document.adminForm;
						form.group_member_id.value = memberId;
						form.task.value = 'registrant.remove_group_member';
						form.submit();
					}
				}
			});

			showHideDependFields = (function(fieldId, fieldName, fieldType, fieldSuffix) {
				$('#ajax-loading-animation').show();
				var masterFieldsSelector;
				if (fieldSuffix)
				{
					masterFieldsSelector = '.master-field-' + fieldSuffix + ' input[type=\'checkbox\']:checked,' + ' .master-field-' + fieldSuffix + ' input[type=\'radio\']:checked,' + ' .master-field-' + fieldSuffix + ' select';
				}
				else
				{
					masterFieldsSelector = '.master-field input[type=\'checkbox\']:checked, .master-field input[type=\'radio\']:checked, .master-field select';
				}
				$.ajax({
					type: 'POST',
					url: siteUrl + 'index.php?option=com_eventbooking&task=get_depend_fields_status&field_id=' + fieldId + '&field_suffix=' + fieldSuffix + langLinkForAjax,
					data: $(masterFieldsSelector),
					dataType: 'json',
					success: function(msg, textStatus, xhr) {
						$('#ajax-loading-animation').hide();
						var hideFields = msg.hide_fields.split(',');
						var showFields = msg.show_fields.split(',');
						for (var i = 0; i < hideFields.length ; i++)
						{
							$('#' + hideFields[i]).hide();
						}
						for (var i = 0; i < showFields.length ; i++)
						{
							$('#' + showFields[i]).show();
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						alert(textStatus);
					}
				});
			});
			buildStateField = (function(stateFieldId, countryFieldId, defaultState){
				if($('#' + stateFieldId).length && $('#' + stateFieldId).is('select'))
				{
					//set state
					if ($('#' + countryFieldId).length)
					{
						var countryName = $('#' + countryFieldId).val();
					}
					else 
					{
						var countryName = '';
					}			
					$.ajax({
						type: 'POST',
						url: siteUrl + 'index.php?option=com_eventbooking&task=get_states&country_name='+ countryName+'&field_name='+stateFieldId + '&state_name=' + defaultState,
						success: function(data) {
							$('#field_' + stateFieldId + ' .controls').html(data);
						},
						error: function(jqXHR, textStatus, errorThrown) {						
							alert(textStatus);
						}
					});			
					//Bind onchange event to the country 
					if ($('#' + countryFieldId).length)
					{
						$('#' + countryFieldId).change(function(){
							$.ajax({
								type: 'POST',
								url: siteUrl + 'index.php?option=com_eventbooking&task=get_states&country_name='+ $(this).val()+'&field_name=' + stateFieldId + '&state_name=' + defaultState,
								success: function(data) {
									$('#field_' + stateFieldId + ' .controls').html(data);
								},
								error: function(jqXHR, textStatus, errorThrown) {						
									alert(textStatus);
								}
							});
							
						});
					}						
				}//end check exits state
							
			});
			$(document).ready(function(){
				buildStateField('state', 'country', '<?php echo $selectedState; ?>');
			})
			populateRegistrantData = (function(){
				var userId = $('#user_id_id').val();
				var eventId = $('#event_id').val();
				$.ajax({
					type : 'POST',
					url : 'index.php?option=com_eventbooking&task=get_profile_data&user_id=' + userId + '&event_id=' +eventId,
					dataType: 'json',
					success : function(json){
						var selecteds = [];
						for (var field in json)
						{
							value = json[field];
							if ($("input[name='" + field + "[]']").length)
							{
								//This is a checkbox or multiple select
								if ($.isArray(value))
								{
									selecteds = value;
								}
								else
								{
									selecteds.push(value);
								}
								$("input[name='" + field + "[]']").val(selecteds);
							}
							else if ($("input[type='radio'][name='" + field + "']").length)
							{
								$("input[name="+field+"][value=" + value + "]").attr('checked', 'checked');
							}
							else
							{
								$('#' + field).val(value);
							}
						}						
					}
				})
			});
		})(jQuery);
	</script>
</form>