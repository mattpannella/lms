<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;
?>
<form action="index.php?option=com_eventbooking&view=states" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar" class="d-flex my-4">
			<div class="filter-search input-group float-start">
				<input
					id="filter_search" 
					name="filter_search"
					placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" 
					value="<?php echo $this->escape($this->state->filter_search); ?>" 
					title="<?php echo JHtml::tooltipText('EB_SEARCH_STATES_DESC'); ?>"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					type="text"
				/>
				<label
					for="filter_search"
					class="d-none"
				><?php echo JText::_('EB_FILTER_SEARCH_STATES_DESC');?></label>
				<button
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					type="submit"
				><span class="icon-search"></span></button>
				<button
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>"
					onclick="document.getElementById('filter_search').value='';this.form.submit();"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					type="button"
				><span class="icon-remove"></span></button>
			</div>
			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->lists['filter_country_id'];
				?>
			</div>
			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->lists['filter_state'];
				?>
			</div>
			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->pagination->getLimitBox();
				?>
			</div>
		</div>
		<table class="adminlist table table-striped">
			<thead>
			<tr>
				<th width="20">
					<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="title" style="text-align: left;">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_STATE_NAME'), 'tbl.state_name', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th class="title" style="text-align: left;">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_COUNTRY_NAME'), 'cou.name', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th class="center title" width="15%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_STATE_CODE_3'), 'tbl.state_3_code', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th class="center title" width="15%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_STATE_CODE_2'), 'tbl.state_2_code', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th class="center" width="10%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_PUBLISHED'), 'tbl.published', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th class="center" width="5%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_ID'), 'tbl.id', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="7">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row = &$this->items[$i];
				$link 	= JRoute::_( 'index.php?option=com_eventbooking&view=state&id='. $row->id );
				$checked 	= JHtml::_('grid.id',   $i, $row->id );
				$published 	= JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png');
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td>
						<?php echo $checked; ?>
					</td>
					<td>
						<a href="<?php echo $link; ?>">
							<?php echo $row->state_name; ?>
						</a>
					</td>
					<td>
						<?php echo $row->country_name; ?>
					</td>
					<td class="center">
						<?php echo $row->state_3_code; ?>
					</td>
					<td class="center">
						<?php echo $row->state_2_code; ?>
					</td>
					<td class="center">
						<?php echo $published; ?>
					</td>
					<td class="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
		</table>
	<div>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->state->filter_order; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->state->filter_order_Dir; ?>" />	
	<?php echo JHtml::_( 'form.token' ); ?>
</form>