<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die;
?>
<table class="table table-striped">
	<thead>
		<tr>
			<th><?php echo JText::_('EB_EVENT')?></th>
			<th><?php echo JText::_('EB_EVENT_DATE')?></th>
			<th><?php echo JText::_('EB_NUMBER_REGISTRANTS')?></th>
		</tr>
	</thead>
	<tbody>	
		<?php
			if (count($this->upcomingEvents))
			{
				foreach ($this->upcomingEvents as $row)
				{
				?>
					<tr>
						<td><a href="<?php echo JRoute::_('index.php?option=com_eventbooking&view=event&id='.$row->id); ?>"><?php echo $row->title; ?></a></td>
						<td>
						<?php
						if ($row->event_date != '0000-00-00 00:00:00') { 
                            $params = json_decode($row->params, true);
                            if (!empty($params['time_zone'])) {
                                date_default_timezone_set($params['time_zone']);
                            } else {
                                date_default_timezone_set('UTC');
                            }
                            $date = date_create($row->event_date);
                            echo date_format($date,"m-d-Y T");
                        } ?>
						</td>
						<td class="center"><a href="<?php echo JRoute::_('index.php?option=com_eventbooking&view=registrants&filter_event_id='.$row->id);?>"> <?php echo $row->total_registrants; ?></a></td>
					</tr>
				<?php
				}
			}
		?>
	</tbody>
</table>