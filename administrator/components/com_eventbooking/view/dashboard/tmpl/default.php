<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die;
JToolBarHelper::title(JText::_('EB_DASHBOARD'), 'generic.png');
?>
<style>
	.tov-content-actionbar {
		padding: 0px !important;
	}
</style>
<div id="cpanel" class="container-flex">
	<div class="row">
		<div class="col-lg-2 col-md-3">
			<?php $this->quickIconCard('index.php?option=com_eventbooking&view=configuration', 'fas fa-cog', JText::_('EB_CONFIGURATION')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
			<?php $this->quickIconCard('index.php?option=com_eventbooking&view=categories', 'fas fa-layer-group', JText::_('EB_CATEGORIES')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
			<?php $this->quickIconCard('index.php?option=com_eventbooking&view=events', 'fas fa-calendar-alt', JText::_('EB_EVENTS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
			<?php $this->quickIconCard('index.php?option=com_eventbooking&view=registrants', 'fas fa-user', JText::_('EB_REGISTRANTS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
			<?php $this->quickIconCard('index.php?option=com_eventbooking&view=fields', 'fas fa-file-image', JText::_('EB_CUSTOM_FIELDS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=locations', 'fas fa-map-pin', JText::_('EB_LOCATIONS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=coupons', 'fas fa-tag', JText::_('EB_COUPONS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=language', 'fas fa-language', JText::_('EB_TRANSLATION')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=message', 'fas fa-envelope', JText::_('EB_EMAIL_MESSAGES')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&task=registrant.export', 'fas fa-file-csv', JText::_('EB_EXPORT_REGISTRANTS')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=massmail', 'fas fa-mail-bulk', JText::_('EB_MASS_MAIL')); ?>
		</div>
		<!-- <div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=countries', 'fas fa-globe', JText::_('EB_COUNTRIES')); ?>
		</div>
		<div class="col-lg-2 col-md-3">
		<?php $this->quickIconCard('index.php?option=com_eventbooking&view=states', 'fas fa-flag-usa', JText::_('EB_STATES')); ?>
		</div> -->
		<div class="col-lg-2 col-md-3">
			<?php
				if (JFactory::getUser()->authorise('core.admin')) {
					//Normal admin should NOT have access to this.
				}
			?>
		</div>
	</div>
	<div class="row">
		<?php
			echo JHtml::_('sliders.start', 'statistics_pane');
			echo JHtml::_('sliders.panel', JText::_('EB_LASTEST_REGISTRANGS'), 'registrants');
			echo $this->loadTemplate('registrants');
			echo JHtml::_('sliders.panel', JText::_('EB_UPCOMING_EVENTS'), 'upcoming_events');
			echo $this->loadTemplate('upcoming_events');
			echo JHtml::_('sliders.panel', JText::_('EB_STATISTICS'), 'statistic');
			echo $this->loadTemplate('statistics');
			echo JHtml::_('sliders.end');
		?>
	</div>
</div>
<style>
	#statistics_pane
    {
		margin:0px !important
	}
</style>
<script type="text/javascript">
    var upToDateImg = '<?php echo JUri::base(true).'/components/com_eventbooking/assets/icons/icon-48-jupdate-uptodate.png' ?>';
    var updateFoundImg = '<?php echo JUri::base(true).'/components/com_eventbooking/assets/icons/icon-48-jupdate-updatefound.png';?>';
    var errorFoundImg = '<?php echo JUri::base(true).'/components/com_eventbooking/assets/icons/icon-48-deny.png';?>';
    jQuery(document).ready(function() {
        jQuery.ajax({
            type: 'POST',
            url: 'index.php?option=com_eventbooking&task=check_update',
            dataType: 'json',
            success: function(msg, textStatus, xhr)
            {
                if (msg.status == 1)
                {
                    jQuery('#update-check').find('img').attr('src', upToDateImg).attr('title', msg.message);
                    jQuery('#update-check').find('span').text(msg.message);
                }
                else if (msg.status == 2)
                {
                    jQuery('#update-check').find('img').attr('src', updateFoundImg).attr('title', msg.message);
                    jQuery('#update-check').find('a').attr('href', 'index.php?option=com_installer&view=update');
                    jQuery('#update-check').find('span').text(msg.message);
                }
                else
                {
                    jQuery('#update-check').find('img').attr('src', errorFoundImg);
                    jQuery('#update-check').find('span').text("<?php echo JText::_('EB_UPDATE_CHECKING_ERROR'); ?>");
                }
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                jQuery('#update-check').find('img').attr('src', errorFoundImg);
                jQuery('#update-check').find('span').text("<?php echo JText::_('EB_UPDATE_CHECKING_ERROR'); ?>");
            }
        });
    });
</script>