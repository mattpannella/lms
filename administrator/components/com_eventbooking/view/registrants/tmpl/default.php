<?php
/**
 * @package            Joomla
 * @subpackage         Event Booking
 * @author             Tuan Pham Ngoc
 * @copyright          Copyright (C) 2010 - 2016 Ossolution Team
 * @license            GNU/GPL, see LICENSE.php
 */
// no direct access
defined('_JEXEC') or die;

$input = JFactory::getApplication()->input;

$colSpan = 10;
if (in_array('last_name', $this->coreFields))
{
	$colSpan++;
	$showLastName = true;
}
else
{
	$showLastName = false;
}

$previousEventId = intval($input->get('old_event_id', 0));

$selectedEvent = EventbookingHelperDatabase::getEvent($previousEventId);
$registrants = EventBookingHelper::getTotalRegistrants($previousEventId);
$capacity = $selectedEvent->event_capacity;

$success = true;

if(isset($capacity)) {
    // If the capacity of the event is unlimited (null or 0), automatically mark a successful registration.
    // If the number of registrants is equal to or greater than the capacity, the registration should fail.
    $success = empty($capacity) ? true : ($capacity > $registrants);
}
?>

<script>
    let success = <?php echo $success ? 'true' : 'false'; ?>;
    let registrants = <?php echo $registrants; ?>;
    let capacity = <?php echo $capacity; ?>;
    let message = '';
    

    if(success) {
        message = 'The event registration was a success!\n';
    } else {
        message = 'The event registration failed; the event is at capacity.\n';
    }

    if(capacity == 0) {
        message += 'Event capacity is unlimited. There are currently ' + registrants + ' registrants for this event.';
    } else {
        message += 'Remaining registrations available for event: (' + (capacity - registrants) + ' / ' + capacity + ')';
    }

    alert(message);
</script>
<style>
	.btn-toolbar #toolbar-edit {
		display: inherit;
	}
</style>
<form action="index.php?option=com_eventbooking&view=registrants" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar" class="d-flex my-3">
			<div class="filter-search input-group float-start">
				<input
					name="filter_search"
					id="filter_search"
					placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>"
					value="<?php echo $this->escape($this->state->filter_search); ?>"
					title="<?php echo JHtml::tooltipText('EB_SEARCH_REGISTRANTS_DESC'); ?>"
					type="text"
				/>
				<label
					for="filter_search"
					class="d-none"
				><?php echo JText::_('EB_FILTER_SEARCH_REGISTRANTS_DESC');?></label>
				<button
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_SUBMIT'); ?>"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					type="submit"
				><span class="icon-search"></span></button>
				<button
					class="btn btn-light"
					title="<?php echo JHtml::tooltipText('JSEARCH_FILTER_CLEAR'); ?>"
					onclick="document.getElementById('filter_search').value='';this.form.submit();"
					data-bs-toggle="tooltip"
					data-bs-html="true"
					type="button"
				><span class="icon-remove"></span></button>
			</div>
			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->lists['filter_event_id'];
				?>
			</div>
			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->lists['filter_published'];
				?>
			</div>
			<?php
				if ($this->config->activate_checkin_registrants)
				{
					echo '<div class="float-end hidden-phone ms-2">';
					echo $this->lists['filter_checked_in'];
					echo '</div>';
				}
			?>

			<div class="float-end hidden-phone ms-2">
				<?php
					echo $this->pagination->getLimitBox();
				?>
			</div>
		</div>
		<div class="clearfix"></div>
		<table class="adminlist table table-striped">
			<thead>
			<tr>
				<th width="2%" class="text_center">
					<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this)" />
				</th>
				<th class="title" style="text-align: left;" width="10%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_FIRST_NAME'), 'tbl.first_name', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<?php
					if ($showLastName)
					{
					?>
						<th class="title" style="text-align: left;" width="10%">
							<?php echo JHtml::_('grid.sort',  JText::_('EB_LAST_NAME'), 'tbl.last_name', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
						</th>
					<?php
					}
				?>
				<th class="title" style="text-align: left;" width="15%">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_EVENT'), 'ev.title', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<?php
				if ($this->config->show_event_date)
				{
					$colSpan++;
				?>
					<th width="7%" class="title" nowrap="nowrap">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_EVENT_DATE'), 'ev.event_date', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
					</th>
				<?php
				}
				?>
				<th width="10%" class="title" nowrap="nowrap">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_EMAIL'), 'tbl.email', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th width="8%" class="title" nowrap="nowrap">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_NUMBER_REGISTRANTS'), 'tbl.number_registrants', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th width="10%" class="title" nowrap="nowrap">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_REGISTRATION_DATE'), 'tbl.register_date', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<th width="5%" class="title" nowrap="nowrap">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_AMOUNT'), 'tbl.amount', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<?php
				if ($this->config->activate_deposit_feature)
				{
					$colSpan++;
				?>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_PAYMENT_STATUS'), 'tbl.payment_status', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
					</th>
				<?php
				}
				if ($this->config->show_coupon_code_in_registrant_list)
				{
					$colSpan++;
				?>
					<th width="7%" class="title" nowrap="nowrap">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_COUPON'), 'cp.code', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
					</th>
				<?php
				}
				if ($this->totalPlugins > 1)
				{
					$colSpan++;
				?>
					<th width="5%" class="title" nowrap="nowrap">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_PAYMENT_METHOD'), 'tbl.payment_method', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
					</th>
				<?php
				}
				?>
				<th width="5%" class="title">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_REGISTRATION_STATUS'), 'tbl.published', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
				<?php
				if ($this->config->activate_checkin_registrants)
				{
					$colSpan++;
				?>
					<th width="8%" class="title">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_CHECKED_IN'), 'tbl.checked_in', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
					</th>
				<?php
				}
				if ($this->config->activate_invoice_feature)
				{
					$colSpan++;
				?>
					<th width="8%" class="center">
						<?php echo JHtml::_('grid.sort',  JText::_('EB_INVOICE_NUMBER'), 'tbl.invoice_number', $this->state->filter_order_Dir, $this->state->filter_order); ?>
					</th>
				<?php
				}
				?>
				<th width="3%" class="title" nowrap="nowrap">
					<?php echo JHtml::_('grid.sort',  JText::_('EB_ID'), 'tbl.id', $this->state->filter_order_Dir, $this->state->filter_order ); ?>
				</th>
			</tr>
			</thead>
			<tfoot>
			<tr>
				<td colspan="<?php echo $colSpan ; ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php
			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row = $this->items[$i];
				$link 	= JRoute::_( 'index.php?option=com_eventbooking&view=registrant&id='. $row->id );
				$checked 	= JHtml::_('grid.id',   $i, $row->id );
				if ($row->published == 0 || $row->published == 1)
				{
					$published 	= JHtml::_('grid.published', $row, $i, 'tick.png', 'publish_x.png');
				}
				elseif($row->published == 3)
				{
					$published = JText::_('EB_WAITING_LIST');
				}
				else
				{
					$imageSrc = 'components/com_eventbooking/assets/icons/cancelled.jpg' ;
					$title = JText::_('EB_CANCELLED') ;
					$published = '<img src="'.$imageSrc.'" title="'.$title.'" />';
				}
				$isMember = $row->group_id > 0 ? true : false ;
				if ($isMember)
				{
					$groupLink = JRoute::_( 'index.php?option=com_eventbooking&view=registrant&id='. $row->group_id );
				}

				$img 	= $row->checked_in ? 'tick.png' : 'publish_x.png';
				$alt 	= $row->checked_in ? JText::_( 'EB_CHECKED_IN' ) : JText::_( 'EB_NOT_CHECKED_IN');
				$img = JHtml::_('image','admin/'.$img, $alt, array('border' => 0), true);
				$action = $row->checked_in ? JText::_( 'EB_UN_CHECKIN' ) : JText::_( 'EB_CHECKIN' );
				$task 	= $row->checked_in ? 'reset_check_in' : 'check_in';
				$href = '
					<a href="javascript:void(0);" onclick="return listItemTask(\'cb'. $i .'\',\''. $task .'\')" title="'. $action .'">'.
						$img .'</a>'
				;
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td class="text_center">
						<?php echo $checked; ?>
					</td>
					<td>
						<a href="<?php echo $link; ?>">
							<?php echo $row->first_name ?>
						</a>
						<?php
						if ($row->is_group_billing)
						{
							echo '<br />' ;
							echo JText::_('EB_GROUP_BILLING');
						}
						if ($isMember)
						{
							?>
							<br />
							<?php echo JText::_('EB_GROUP'); ?><a href="<?php echo $groupLink; ?>"><?php echo $row->group_name ;  ?></a>
						<?php
						}
						?>
					</td>
					<?php
						if ($showLastName)
						{
						?>
							<td>
								<a href="<?php echo $link; ?>">
									<?php echo $row->last_name ; ?>
								</a>
							</td>
						<?php
						}
					?>
					<td>
						<a href="index.php?option=com_eventbooking&view=event&id=<?php echo $row->event_id; ?>"><?php echo $row->title ; ?></a>
					</td>
					<?php
					if ($this->config->show_event_date)
					{
						?>
						<td class="text_center">
							<?php echo JHtml::_('date', $row->event_date, $this->config->date_format, null) ; ?>
						</td>
					<?php
					}
					?>
					<td>
						<a href="mailto:<?php echo $row->email;?>"><?php echo $row->email;?></a>
					</td>
					<td class="center" style="font-weight: bold;">
						<?php echo $row->number_registrants; ?>
					</td>
					<td class="center">
						<?php 
						if (!date_default_timezone_get()) {
							date_default_timezone_set('UTC');
						}
						echo JHtml::_('date', $row->register_date, "m-d-Y T"); ?>
					</td>
					<td>
						<?php echo EventbookingHelper::formatAmount($row->amount, $this->config) ; ?>
					</td>
					<?php
					if ($this->config->activate_deposit_feature)
					{
						?>
						<td>
							<?php
							if($row->payment_status == 1)
							{
								echo JText::_('EB_FULL_PAYMENT');
							}
							else
							{
								echo JText::_('EB_PARTIAL_PAYMENT');
							}
							?>
						</td>
					<?php
					}
					if ($this->config->show_coupon_code_in_registrant_list)
					{
					?>
						<td>
							<a href="index.php?option=com_eventbooking&view=coupon&id=<?php echo $row->coupon_id; ?>" target="_blank"><?php echo $row->coupon_code ; ?></a>
						</td>
					<?php
					}
					if ($this->totalPlugins > 1)
					{
						$method = os_payments::getPaymentMethod($row->payment_method) ;
						?>
						<td>
							<?php if ($method) echo JText::_($method->getTitle()); ?>
						</td>
					<?php
					}
					?>
					<td class="center">
						<?php
							echo $published ;
						?>
					</td>
					<?php
					if ($this->config->activate_checkin_registrants)
					{
					?>
						<td class="center"><?php echo $href; ?></td>
					<?php
					}
					if ($this->config->activate_invoice_feature)
					{
						?>
						<td class="center">
							<?php
							if ($row->invoice_number)
							{
							?>
								<a href="<?php echo JRoute::_('index.php?option=com_eventbooking&task=registrant.download_invoice&id='.($row->cart_id ? $row->cart_id : ($row->group_id ? $row->group_id : $row->id))); ?>" title="<?php echo JText::_('EB_DOWNLOAD'); ?>"><?php echo EventbookingHelper::formatInvoiceNumber($row->invoice_number, $this->config) ; ?></a>
							<?php
							}
							?>
						</td>
					<?php
					}
					?>
					<td class="center">
						<?php echo $row->id; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
		</table>

		<?php echo JHtml::_(
				'bootstrap.renderModal',
				'collapseModal',
				array(
						'title' => JText::_('EB_MASS_MAIL'),
						'footer' => $this->loadTemplate('batch_footer')
				),
				$this->loadTemplate('batch_body')
		); ?>

	</div>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $this->state->filter_order; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $this->state->filter_order_Dir; ?>" />
	<?php echo JHtml::_( 'form.token' ); ?>
	<script type="text/javascript">
		Joomla.submitbutton = function(pressbutton)
		{
			var form = document.adminForm;
			if (pressbutton == 'add')
			{
				if (form.filter_event_id.value == 0)
				{
					alert("<?php echo JText::_("EB_SELECT_EVENT_TO_ADD_REGISTRANT"); ?>");
					form.filter_event_id.focus();
					return;
				}
			}
			Joomla.submitform( pressbutton );
		}
	</script>
</form>