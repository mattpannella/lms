(function($) {
  $('#send_emails').on('change', function() {
    if ($(this).val() == 3) {
      $('.mail-settings-option').hide();
    }
    else {
      $('.mail-settings-option').show();
    }
  });

  $('#send_emails').trigger('change');
})(jQuery)
