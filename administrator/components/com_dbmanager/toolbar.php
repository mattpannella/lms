<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/26/16
 * Time: 11:47 AM
 */

defined('_JEXEC') or die;

class DbmanagerToolbar extends FOFToolbar {
	function onDashboardsBrowse() {
		JToolBarHelper::title(JText::_('COM_DBMANAGER').": ".JText::_('COM_DBMANAGER_TITLE_DASHBOARD'), 'DBMANAGER');
	}
	function onClientsBrowse() {
		JToolBarHelper::title('Tovuti Clients');
	}
	function onClientsEdit() {
		JToolBarHelper::title('Tovuti Clients');
	}
	function onDatabasesBrowse() {
		JToolBarHelper::title('Tovuti Clients');
	}
}