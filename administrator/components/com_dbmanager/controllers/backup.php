<?php

defined('_JEXEC') or die;

require_once("/var/www/files/creds.php");

class DbmanagerControllerBackup extends FOFController
{
    public function delete()
    {

        if (!JFactory::getUser()->authorise('core.edit')) {
            return;
        }

        $database = JRequest::getVar('db');
        $file = JRequest::getVar('file');
        $file .= ".gz";

        $loc = "/mnt/axs/backup/$database/$file";

        if (!file_exists($loc)) {
            echo "The backup file doesn't exist.";
            return;
        }

        if (unlink($loc)) {
            echo "success";
        } else {
            echo "There was an error deleting the backup\n$loc";
        }
    }

    public function restore()
    {

        if (!JFactory::getUser()->authorise('core.edit')) {
            return;
        }

        $siteId = JRequest::getVar('id');
        $database = JRequest::getVar('db');
        $file = JRequest::getVar('file');
        $file .= ".gz";


        $loc = "/mnt/axs/backup/$database/$file";

        if (!file_exists($loc)) {
            echo "The backup file doesn't exist.";
            return;
        }


        $credentials = dbCreds::getCreds();
        $creds = "-h $credentials->dbhost -u $credentials->dbuser -p" . $credentials->dbpass;

        $maindir = "/mnt/axs/backup/$database/";

        $date = date("Y_m_d_H_i_s");

        //The database is on the local server, so we can just run commands on the command line and restore it directly.

        //Back up the current database first, in case of a mistake.
        $dest = $maindir . $database . "_restore_" . $date;
        shell_exec("mysqldump --single-transaction  --set-gtid-purged=OFF $creds $database > $dest");

        //Compress the file
        shell_exec("gzip -9 $dest");
        $shell = "sudo chown ubuntu:www-data $dest";
        shell_exec($shell);

        //Copy the current compressed database backup to a new file so the original remains
        $temp = $maindir . "temp.gz";
        $dest = $maindir . $file;
        shell_exec("cp $dest $temp");

        //Unzip the temp file
        shell_exec("gzip -d $temp");

        $temp = $maindir . "temp";

        //Recreate the database with the dump file.
        shell_exec("mysql $creds $database < $temp");
        //Delete the temp file.
        shell_exec("rm $temp");

        echo "success";

    }

}