<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/26/16
 * Time: 12:22 PM
 */

defined('_JEXEC') or die();

class DbmanagerControllerDashboards extends FOFController {
	public function runsql() {

		if (!JFactory::getUser()->authorise('core.edit')) {
			return;
		}

		$sql = $this->input->get('sql', '', 'STRING');

		//look up what databases to run it on
		$db = JFactory::getDbo();
		$db->setQuery("SELECT * FROM `axs_dbmanager`;");
		$dbses = $db->loadObjectList();
		$dbsCount = count($dbses);
		$stepSize = 100.0 / $echo;

		for ($i = 0; $i < $dbsCount; $i++) {
			$dbs = $dbses[$i];

			$m = new stdClass();
			$m->progress = $i*$stepSize;
			$m->message = "Connecting to Database: $dbs->dbname...";
			echo json_encode($m) . str_repeat(" ", 4096);
			ob_flush();
			flush();

			$params = AxsClients::decryptClientParams($dbs->dbparams);


			$options = array();
			$options['driver']   = 'mysqli';           // Database driver name
			$options['host']     = 'localhost';        // Database host name

			//$options['user']     = $dbs->dbuser;       // User for database authentication
			//$options['password'] = $dbs->dbpass;       // Password for database authentication
			//$options['database'] = $dbs->dbname;       // Database name
			$options['user']     = $params->dbuser;       // User for database authentication
			$options['password'] = $params->dbpass;       // Password for database authentication
			$options['database'] = $params->dbname;       // Database name
			$options['prefix']   = $dbs->dbprefix;     // Database prefix (may be empty)
			$db2 = JDatabaseDriver::getInstance($options);

			$m = new stdClass();
			$m->progress = $i*$stepSize;
			$m->message = " Connected.<br>";
			echo json_encode($m) . str_repeat(" ", 4096);
			ob_flush();
			flush();

			$commands = array_filter(explode(';', $sql));
			$queryCount = count($commands);
			for ($j = 0; $j < $queryCount; $j++) {
				$query = $commands[$j];

				$m = new stdClass();
				$m->progress = $i*$stepSize + $j/$queryCount*$stepSize;
				$m->message = "Running: $query...";
				echo json_encode($m) . str_repeat(" ", 4096);
				ob_flush();
				flush();

				try {
					$db2->setQuery($query);
					$db2->execute();

					$m = new stdClass();
					$m->progress = $i*$stepSize + ($j+1)/$queryCount*$stepSize;
					$m->message = " Done.<br>";
					echo json_encode($m) . str_repeat(" ", 4096);
					ob_flush();
					flush();
				} catch(Exception $e) {
					$m = new stdClass();
					$m->progress = $i*$stepSize + ($j+1)/$queryCount*$stepSize;
					$m->message = "<span style=\"color: red;\">" . $e->getMessage() . "</span><br>";
					echo json_encode($m) . str_repeat(" ", 4096);
					ob_flush();
					flush();
				}
			}

			$m = new stdClass();
			$m->progress = ($i+1)*$stepSize;
			$m->message = " SQL on $dbs->dbname Done.<br><br>";
			echo json_encode($m) . str_repeat(" ", 4096);
			ob_flush();
			flush();
		}
	}
}