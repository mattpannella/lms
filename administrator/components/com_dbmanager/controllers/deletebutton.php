<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/26/16
 * Time: 12:22 PM
 */

defined('_JEXEC') or die();

require_once("/var/www/sitebuilder/AwsBase.php");

class DbmanagerControllerDeletebutton extends FOFController {
	public function delete() {

		$user = JFactory::getUser();

		$isAdmin = $user->authorise('core.admin');

		if (!$isAdmin) {
			echo "Not Admin!";
			return;
		}

		$id = JRequest::getVar('id');
		$func = JRequest::getVar('func');

		AwsControl::deleteAccount($id, $func);
	}
}