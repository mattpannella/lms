<?php

echo AxsLoader::loadKendo();
function getResultsData() {
    $db = JFactory::getDbo();
    $conditions = array();
    $query = $db->getQuery(true);
    $query->select('id as edit,id as ID, client_name as Client, status as Status, subscription_type as Subscription, domains as Domains, enabled as Enabled, template as Template, created as Created, expires as Expires');
    $query->from('axs_dbmanager');
    $query->order('id DESC');
    $db->setQuery($query);
    $results = $db->loadObjectList();
    return json_encode($results);
}
$data = getResultsData();

if(!$data) {
    $date = '[]';
}
?>
<style>
    #loading_icon {
        font-size: 30px;
        color: #007aff;
        width: 100%;
        padding-top: 150px;
        text-align: center;
    }
</style>

<div id="j-main-container">
	<div id="loading_icon">
        <span class="fa fa-spinner fa-spin"></span> Loading...
    </div>
	<div id="grid"></div>
<div>

<script>
    var exportingFlag = false;
    const fileName = 'clients';
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        excel: {
            fileName: fileName+".xlsx",
            filterable: true,
            allPages: true
        },
        pdf: {
            fileName: fileName+".pdf",
            filterable: true,
            allPages: true
        },
        sortable:   true,
        reorderable: true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100,200,500,1000,5000,'all'],
            refresh:        false,
            message: {
                empty:      "There are no entries to display"
            }
        },
        excelExport: function(event) {

            var sheet = event.workbook.sheets[0];
            var grid = event.sender;

            if (!exportingFlag) {
                grid.hideColumn('edit');
                event.preventDefault();
                exportingFlag = true;

                setTimeout(function () {
                    grid.saveAsExcel();
                    setTimeout(function () {
                        grid.showColumn('edit');
                    },3000);
                });
            } else {
                exportingFlag = false;
            }
        },
        dataSource: {
            data:  <?php echo $data; ?>
        },
        columns: [
            {
                field: "edit",
                title: "edit",
                template: '<a class="btn btn-primary" href="index.php?option=com_dbmanager&view=database&id=#: edit #">Edit</a>',
                filterable: false,
                sortable: false,
                width: 100

            },
			{
                field: "ID",
                title: "ID",
				width: 120,
                type: "number",

            },
            {
                field: "Client",
                title: "Client",
                width: 300,
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                }
            },
            {
                field: "Status",
                title: "Status",
                width: 300,
                filterable: {
                    multi: true,
                    search: true
                },

            },
            {
                field: "Subscription",
                title: "Subscription",
                width: 300,
                filterable: {
                    multi: true,
                    search: true
                },

            },
            {
                field: "Domains",
                title: "Domains",
                width: 300,
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Created",
                title: "Created",
                width: 200,
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            },
            {
                field: "Expires",
                title: "Expires",
                width: 200,
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            }
        ]
    }

    var clients = jQuery("#grid").kendoGrid(kendoOptions);
    var grid = clients.data("kendoGrid");
    jQuery('#loading_icon').hide();
</script>