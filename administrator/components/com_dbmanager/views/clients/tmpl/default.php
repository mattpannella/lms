<?php 
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
$clientId = AxsClients::getClientId();
?>
<?php 
    echo AxsLoader::loadKendo();
    $clientData  = AxsClientData::getClientUsers();    
?>

<table id="clients" class="table table-bordered table-striped table-responsive">                            
    <tr>
        <th>User_ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Send_Product_Email</th>
        <th>Registered</th>
        <th>Client</th>
        <th>Account_ID</th>
        <th>Subscription</th>
        <th>Status</th>
    </tr>
    <?php 
        foreach($clientData as $client) {
            foreach($client->users as $user) {    ?>
                <tr>
                    <td><?php echo $user->id; ?></td>
                    <td><?php echo $user->name; ?></td>
                    <td><?php echo $user->email; ?></td>
                    <td><?php echo $user->productEmails; ?></td>
                    <td><?php echo $user->registerDate; ?></td>
                    <td><?php echo $client->client_name; ?></td>
                    <td><?php echo $client->id; ?></td>
                    <td><?php echo $client->subscription_type; ?></td>
                    <td><?php echo $client->status; ?></td>
                </tr>            
    <?php 
            }
        } 
    ?>    
</table>
<script>
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",           
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        columns: [                    
            {
                field: "User_ID",
                title: "User Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Name",
                title: "Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Email",
                title: "Email",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Send_Product_Email",
                title: "Send Product Email",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },                
            {
                field: "Registered",
                title: "Registered",
                type: "date",
                format: "{0:MM/d/yyyy}",
                filterable: {
                    ui: "datepicker"
                }
            },
            {
                field: "Client",
                title: "Client Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Account_ID",
                title: "Account Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Subscription",
                title: "Subscription",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            },
            {
                field: "Status",
                title: "Status",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },
            
            }
        ]
    }

    var fileLocker = $("#clients").kendoGrid(kendoOptions);
    var grid = fileLocker.data("kendoGrid");
</script>