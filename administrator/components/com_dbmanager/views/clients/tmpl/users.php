<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
$clientId = AxsClients::getClientId();
?>
<?php
    echo AxsLoader::loadKendo();
    $input = JFactory::getApplication()->input;
    $year =  $input->get('year',date('Y'),'INT');
    $clientData  = AxsClientData::getClientActiveUsers($year);
    if(!$clientData) {
        $data = '[]';
    } else {
        $data = json_encode($clientData);
    }
?>
<style>
	#toolbar, .page-title {
		display: none;
	}
    .activity-chart svg {
        box-shadow: 0px 1px 3px 0px rgba(0,0,0,.5);
        border: 2px solid #fff;
    }
    .activity-chart {
        margin-bottom: 40px;
    }
    #toolbar {
        color: #777;
        font-size: 18px;
    }
    .stats_bar {
        padding: 0px;
        border-radius: 5px;
    }
    #loading_icon {
        font-size: 30px;
        color: #007aff;
        width: 100%;
        padding-top: 150px;
        text-align: center;
    }
</style>
<script>
    <?php
        $current_year = date("Y");
        $start_year = date("Y") - 10;
    ?>
    const yearSelect = `
        <select id="yearSelector" style="padding:2px; margin-top: 8px;">
            <?php
                for($i=$current_year; $i >= $start_year; $i--) {
            ?>
                    <option
                        value="<?php echo $i; ?>"
                        <?php if ($i == $year) { echo " selected "; } ?>
                    >
                        <?php echo $i; ?>
                    </option>
            <?php
                }
            ?>
        </select>`;
    $('.page-title').html('<i class="lizicon-chart"></i> Client Active Users').show();
    $('#toolbar').html('<i class="lizicon-calendar"></i> Year: '+yearSelect).show();
</script>

<div class="container-fluid">
    <div id="loading_icon">
        <span class="fa fa-spinner fa-spin"></span> Loading...
    </div>
    <div id="active_user_report"></div>

<script>
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        pageable: {
            buttonCount:    8,
            input:          true,
            pageSize:       20,
            pageSizes:      [10,20,50,100,200,500,1000],
            refresh:        true,
            message: {
                empty:      "There are no entries to display"
            }
        },
        dataSource: {
            data:  <?php echo $data; ?>
        },
        columns: [
            {
                field: "client_id",
                title: "ID",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "client_name",
                title: "Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "January",
                title: "January",
                type: "number"

            },
            {
                field: "February",
                title: "February",
                type: "number"

            },
            {
                field: "March",
                title: "March",
                type: "number"

            },
            {
                field: "April",
                title: "April",
                type: "number"

            },
            {
                field: "May",
                title: "May",
                type: "number"

            },
            {
                field: "June",
                title: "June",
                type: "number"

            },
            {
                field: "July",
                title: "July",
                type: "number"

            },
            {
                field: "August",
                title: "August",
                type: "number"

            },
            {
                field: "September",
                title: "September",
                type: "number"

            },
            {
                field: "October",
                title: "October",
                type: "number"

            },
            {
                field: "November",
                title: "November",
                type: "number"

            },
            {
                field: "December",
                title: "December",
                type: "number"

            }
        ]
    }

    var active_user_report = $("#active_user_report").kendoGrid(kendoOptions);
    var grid = active_user_report.data("kendoGrid");
    $('#loading_icon').hide();

    var operator = '?';
    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts = url.split('?');
        if (urlparts.length >= 2) {

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i = pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            return urlparts[0] + (pars.length > 0 ? '?' + pars.join('&') : '');
        }
        return url;
    }
    var currentUrl = removeURLParameter(window.location.href, 'year');
    if(currentUrl.indexOf('?') != -1) {
        var operator = '&';
    }
    $('#yearSelector').change( function(){
            const year = $(this).val();
			window.location.href = currentUrl+operator+"year="+year;
		});
</script>