<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
$clientId = AxsClients::getClientId();
?>
<?php
    echo AxsLoader::loadKendo();
    $clientData  = AxsClientData::getClientUsage();
?>

<table id="clients" class="table table-bordered table-striped table-responsive">
    <tr>
        <th>Client</th>
        <th>Account_ID</th>
        <th>Subscription</th>
        <th>Status</th>
        <th>business_directory</th>
        <th>contracts</th>
        <th>affiliate_dashboards</th>
        <th>rewards</th>
        <th>ecommerce_products</th>
        <th>gant_charts</th>
        <th>campaigns</th>
        <th>community_activity</th>
        <th>blog_posts</th>
        <th>blog_comments</th>
        <th>blogFeeds</th>
        <th>blogSubscriptions</th>
        <th>blogCategorySubscriptions</th>
        <th>blogBloggerSubscriptions</th>
        <th>blogTeams</th>
        <th>forum_topics</th>
    </tr>
    <?php
        foreach($clientData as $client) { ?>
            <tr>
                <td><?php echo $client->client_name; ?></td>
                <td><?php echo $client->id; ?></td>
                <td><?php echo $client->subscription_type; ?></td>
                <td><?php echo $client->status; ?></td>
                <td><?php echo $client->business_directory; ?></td>
                <td><?php echo $client->contracts; ?></td>
                <td><?php echo $client->affiliate_dashboards; ?></td>
                <td><?php echo $client->rewards; ?></td>
                <td><?php echo $client->ecommerce_products; ?></td>
                <td><?php echo $client->gant_charts; ?></td>
                <td><?php echo $client->campaigns; ?></td>
                <td><?php echo $client->community_activity; ?></td>
                <td><?php echo $client->blog_posts; ?></td>
                <td><?php echo $client->blog_comments; ?></td>
                <td><?php echo $client->blogFeeds; ?></td>
                <td><?php echo $client->blogSubscriptions; ?></td>
                <td><?php echo $client->blogCategorySubscriptions; ?></td>
                <td><?php echo $client->blogBloggerSubscriptions; ?></td>
                <td><?php echo $client->blogTeams; ?></td>
                <td><?php echo $client->forum_topics; ?></td>
            </tr>
    <?php } ?>
</table>
<script>
    var kendoOptions = {
        toolbar: [
            "pdf",
            "excel",
        ],
        sortable:   true,
        resizable:  true,
        columnMenu: true,
        groupable: true,
        filterable: {
            mode: "row"
        },
        columns: [
            {
                field: "Client",
                title: "Client Name",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Account_ID",
                title: "Account Id",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Subscription",
                title: "Subscription",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "Status",
                title: "Status",
                filterable: {
                    cell: {
                        suggestionOperator: "contains"
                    },
                    operators: {
                        string: {
                            contains: "Contains"
                        }
                    }
                },

            },
            {
                field: "business_directory",
                title: "Business Directory",
                type: "number"

            },
            {
                field: "contracts",
                title: "Contracts",
                type: "number"

            },
            {
                field: "affiliate_dashboards",
                title: "Affiliate Dashboards",
                type: "number"

            },
            {
                field: "rewards",
                title: "Rewards",
                type: "number"

            },
            {
                field: "ecommerce_products",
                title: "Ecommerce Products",
                type: "number"

            },
            {
                field: "gant_charts",
                title: "Gantt Charts",
                type: "number"

            },
            {
                field: "campaigns",
                title: "Campaigns",
                type: "number"

            },
            {
                field: "community_activity",
                title: "Community Activity",
                type: "number"

            },
            {
                field: "blog_posts",
                title: "Blog Posts",
                type: "number"

            },
            {
                field: "blog_comments",
                title: "Blog Comments",
                type: "number"

            },
            {
                field: "blogFeeds",
                title: "Blog Feeds",
                type: "number"

            },
            {
                field: "blogSubscriptions",
                title: "Blog Subscriptions",
                type: "number"

            },
            {
                field: "blogCategorySubscriptions",
                title: "Blog Category Subscriptions",
                type: "number"

            },
            {
                field: "blogBloggerSubscriptions",
                title: "Blog Blogger Subscriptions",
                type: "number"

            },
            {
                field: "blogTeams",
                title: "Blog Teams",
                type: "number"

            },
            {
                field: "forum_topics",
                title: "Forum Topics",
                type: "number"

            }
        ]
    }

    var fileLocker = $("#clients").kendoGrid(kendoOptions);
    var grid = fileLocker.data("kendoGrid");
</script>