<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldUsername extends FOFFormFieldText {

	protected $type = 'username';


	/**
	 * @return string
	 */
	public function getInput()
	{
		$ptext = parent::getInput();
		$user_id = $this->form->getValue('user_id');

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName('name'))
			->from($db->quoteName('#__users'))
			->where($db->quoteName('id').'='.(int)$user_id);
		$db->setQuery($query);
		$result = $db->loadObject();

		$name = 'value="'.$result->name.'"';
		$pattern= "/value=\".*\"/";
		return preg_replace($pattern, $name, $ptext);
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable()
	{
		$user_id = $this->item->get('user_id');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName('name'))
			->from($db->quoteName('#__users'))
			->where($db->quoteName('id').'='.(int)$user_id);
		$db->setQuery($query);
		$result = $db->loadObject();
		$name = $result->name;
		return "<a href=\"index.php?option=com_users&view=axsusers&task=axsuser.edit&id=".$user_id."\">".$name."</a>";
	}
}
