<?php


defined('JPATH_PLATFORM') or die;

class FOFFormFieldDeletebutton extends FOFFormFieldText {
	
	public function getInput() {

		$user_groups = JFactory::getUser()->getAuthorisedGroups();

		if (!in_array(8, $user_groups)) {
			//Nobody but super users
            return;
        }
		
		ob_start();

		$id = $this->form->getValue('id');

		$db = JFactory::getDBO();
		$query = "SELECT * FROM axs_dbmanager WHERE id=$id";
		$db->setQuery($query);
		$data = $db->loadObject();

		if ($data->enabled === "0") {
			?>
				<div style="font-size: 18px; padding: 8px; border: 1px solid red;">This site has already been deleted</div>
			<?php
		} else {

			?>

				<button class="btn btn-primary delete_button" which="<?php echo $id; ?>">Delete</button>
				<script>
					var id = "<?php echo $id;?>";
					var step = 0;
					var func = null;

					function deleteStage() {
						switch (step) {
							case 0: 		
								//Acquire all the data and check if the instances are shared or dedicated
								func = 'start';
								break;

							case 1:							
								//The instance is dedicated. Wait for it to terminate
								func = 'check_instance_terminate';
								break;

							case 2:
								//The instance is dealt with. Now to check the volume.
								func = 'check_volume';
								break;

							case 3:
								//The volume is being unmounted.  Check it's success
								func = 'check_volume_unmount';
								break;

							case 4:
								//Remove the drive from the OS
								func = 'remove_volume';
								break;

							case 5:
								//Check that it was removed
								func = 'check_volume_remove';
								break;

							case 6:
								//Delete the volume from AWS
								func = 'delete_volume';
								break;

							case 7:
								//Check that it was deleted.
								func = 'check_volume_delete';
								break;

							case 8:
								//Delete their info and files from the server
								func = 'delete_user';
								break;

							default:
								return;
						}

						console.log("Calling function : " + func);

						jQuery.ajax({
		                    url: 'index.php?option=com_dbmanager&task=deletebutton.delete&format=raw',
		                    data: {
		                        id: id,
		                        func: func
		                    },
		                    type: 'post'
		                }).success(
			                function(response) {
			                	/*console.log(response);
			                	if (response == 'success') {
			                		alert("The database was deleted successfully");
			                		window.location.href = "administrator/index.php?option=com_dbmanager&view=database";
			                	} else {
			                		alert("There was an error.\nPlease check the console log.");
			                		console.log(response);
			                	}*/

			                	try {
			                		var result = JSON.parse(response);
			                		if (result.success == true) {
			                			step = result.step;
			                			if (step !== "done") {
			                				deleteStage ();
			                			} else {
			                				if (result.message) {
			                					alert("The process completed but there was an error: \n" + result.message);
			                					document.getElementsByClassName("delete_button")[0].enabled 
			                				} else {
			                					alert("The site has been deleted.");
			                					window.location = "index.php?option=com_dbmanager&view=databases";
			                				}
			                			}
			                		}

			                		console.log(result.message);
			                	} catch (e) {
			                		console.log(e.message);
			                		console.log(response);
			                	}
			                }
		               	);
					}




					jQuery(".delete_button").click(
						function(e) {
							e.preventDefault();
							if (confirm("Are you sure? You cannot undo this action.")) {
								//this.disabled = true;
								
								console.log('Starting');

								deleteStage();
				            }
						}
					);
				</script>

			<?php					
		}

		return ob_get_clean();
	}


	public function getRepeatable() {
		
	}
}
