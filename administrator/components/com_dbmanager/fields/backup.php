<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldbackup extends FOFFormFieldText {
	/**
	 * @return string
	 */
	public function getInput() {
		$database = $this->form->getValue('dbname');
		$site_id = $this->form->getValue('id');

		$dir = "/mnt/axs/backup/$database";
		$files = scandir($dir);
		ob_start();

		echo AxsLoader::loadKendo();
		?>		

		<style>
			td {
				padding: 5px 10px;
			}
		</style>

		<table id="backup_list">
			<thead>
				<tr>
					<th><b>Year</b></th>
					<th><b>Month</b></th>
					<th><b>Day</b></th>
					<th><b>Time (server)</b></th>
					<th><b>Operations</b></th>
					<th><b>Notes</b></th>
				</tr>

			</thead>

			<tbody>
				<?php

				foreach (array_reverse($files) as $file) {
					if ($file != "." && $file != "..") {

						if (substr($file, -3) == ".gz") {
							$file = substr($file, 0, -3);
						}

						//echo $file . "<br>";

						$parts = explode("_", $file);
						$length = count($parts);
						$year = $parts[$length - 6];
						$month = $parts[$length - 5];
						$day = $parts[$length - 4];
						$hour = $parts[$length - 3];
						$min = $parts[$length - 2];
						$sec = $parts[$length - 1];


						$restore = $parts[$length - 7];
						if ($restore == "restore") {
							$restore = true;
						} else {
							$restore = false;
						}

						$date = strtotime("$year-$month-$day $hour:$min:$sec");

						?>
						
						<tr>
							<td><?php echo date("Y", $date);?></td>	<!-- Year -->
							<td><?php echo date("F", $date);?></td>	<!-- Month -->
							<td><?php echo date("d (D)", $date);?></td>	<!-- Day -->							
							<td><?php echo date("g:i:s A", $date); ?></td> <!-- Time -->
							<td>
								<div class="restore_db btn btn-primary" file="<?php echo $file; ?>">
									Restore
								</div>
								<div class="delete_db btn btn-primary" file="<?php echo $file; ?>">
									Delete
								</div>
							</td>
							<td>
								<?php
									if ($restore) {
								?>
										<b>This is a copy of the database right before it was restored</b>
								<?php
									} else {
										echo "---";
									}
								?>
							</td>
						</tr>
						<?php


						//echo date("F d, Y - H:i:s", strtotime($date)) . "<br>";
					}
				}
				?>
			</tbody>
		</table>

		<script>

			var kendoOptions = {
    			sortable: true,
    			groupable: true,
    			height: 600,
    			pageable: {
    				buttonCount: 8,
    				input: true,
    				pageSize: 20,
    				pageSizes: true,
    				refresh: true,
    				message: {
    					empty: "There are no entries to display"
    				}
    			},
    			resizable: true,
    			filterable: {
    				mode: "row"
    			},
    			columnMenu: true
    		}

			jQuery("#backup_list").kendoGrid(kendoOptions);

			jQuery("a").click(
				function() {
					var href = this.getAttribute("href");
					if (href == "#backups") {
						setTimeout(
							function() {
								jQuery("#backup_list").data('kendoGrid').refresh();		
							}, 100
						);						
					}
				}
			);

			

			var database_name = "<?php echo $database;?>";
			var site_id = "<?php echo $site_id;?>";
			var restore_message = "Are you sure you want to restore this backup? \n This operation should be used for emergencies only. \n A copy of the current database will be made.";
			var delete_message = "Are you sure you want to delete this backup? \n This operation is not reversable and will delete the file from the server.";			

			jQuery(document).on("click", ".restore_db",
				function() {

					var file = this.getAttribute("file");
					var restore_button = this;
					if (confirm(restore_message)) {

						var restore_buttons = document.getElementsByClassName("restore_db");
						for (var i = 0; i < restore_buttons.length; i++) {
							restore_buttons[i].setAttribute("disabled", true);
						}
						
						jQuery.ajax({
					        type: 'POST',
					        url: 'index.php?option=com_dbmanager&task=backup.restore&format=raw', 
					        data: {
					        	id: site_id,
					        	file: file, 
					        	db: database_name
					        },

					        success: function(response) {

					        	var restore_buttons = document.getElementsByClassName("restore_db");
								for (var i = 0; i < restore_buttons.length; i++) {
									restore_buttons[i].removeAttribute("disabled");
								}
					        	
					            if (response == "success") {
					            	alert("The database has been restored");
					            } else {
					            	alert("There was an error, please check the console.");
					            	console.log(response);
					            }
					        }
					    });
					}
				}
			);

			jQuery(document).on("click", ".delete_db",
				function() {
					var file = this.getAttribute("file");
					var delete_button = this;
					if (confirm(delete_message)) {
						delete_button.disabled = true;
						jQuery.ajax({
					        type: 'POST',
					        url: 'index.php?option=com_dbmanager&task=backup.delete&format=raw', 
					        data: {
					        	file: file,
					        	db: database_name
					        },

					        success: function(response) {

					            if (response == "success") {
					            	jQuery(delete_button.parentNode.parentNode).remove();
					            } else {
					            	alert("There was an error, please check the console.");
					            	console.log(response);
					            }
					        }
					    });
					}
				}
			);
		</script>
		<?php

		return ob_get_clean();
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable()
	{
		return "repeatable";
	}
}
