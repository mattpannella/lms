<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFielddbuser extends FOFFormFieldText {

	


	/**
	 * @return string
	 */
	public function getInput()
	{
		return $this->form->getValue('dbparams');
		return "input";
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable()
	{
		$dbparams = $this->item->get('dbparams');
		$params = AxsClients::decryptClientParams($dbparams);		
		ob_start();
		?>

			<a href="index.php?option=com_dbmanager&view=database&id=<?php echo $this->item->id;?>">
				<?php echo $params->dbuser;?>
			</a>

		<?php
		return ob_get_clean();		
	}
}
