<?php

defined('_JEXEC') or die;

class DbmanagerModelDatabases extends FOFModel {
	public function save($data) {
		$domains = json_decode($data['domains'])->site_url;
		$domains = implode(',', $domains);
		$data['domains'] = $domains;

		/* $dbparams = AxsClients::encryptClientParams($data['dbname'], $data['dbuser'], $data['dbpass']);
		$data['dbparams'] = $dbparams; */

		$access = new stdClass();
		$params = new stdClass();

		if ($data['eshop_access'] == "1") {
			$access->eshop = true;
		} else {
			$access->eshop = false;
		}
		if ($data['lms_access'] == "1") {
			$access->lms = true;
		} else {
			$access->lms = false;
		}
		if ($data['community_access'] == "1") {
			$access->community = true;
		} else {
			$access->community = false;
		}
		if ($data['affiliate_access'] == "1") {
			$access->affiliates = true;
		} else {
			$access->affiliates = false;
		}
		if ($data['forum_access'] == "1") {
			$access->forum = true;
		} else {
			$access->forum = false;
		}
		if ($data['events_access'] == "1") {
			$access->events = true;
		} else {
			$access->events = false;
		}
		if ($data['business_directory_access'] == "1") {
			$access->business_directory = true;
		} else {
			$access->business_directory = false;
		}

		if ($data['boards_access'] == "1") {
			$access->boards = true;
		} else {
			$access->boards = false;
		}
		if ($data['blog_access'] == "1") {
			$access->blog = true;
		} else {
			$access->blog = false;
		}
		if ($data['media_access'] == "1") {
			$access->media = true;
		} else {
			$access->media = false;
		}
		if ($data['reporting_access'] == "1") {
			$access->reporting = true;
		} else {
			$access->reporting = false;
		}
		if ($data['polls_access'] == "1") {
			$access->polls = true;
		} else {
			$access->polls = false;
		}
		if ($data['contract_signing_access'] == "1") {
			$access->contract_signing = true;
		} else {
			$access->contract_signing = false;
		}
		if ($data['subscriptions_access'] == "1") {
			$access->subscriptions = true;
		} else {
			$access->subscriptions = false;
		}
		if ($data['virtual_classroom_access'] == "1") {
			$access->virtual_classroom = true;
		} else {
			$access->virtual_classroom = false;
		}

		// Set the upper limit for the virtual classroom participants / attendees
		if (!empty($data['virtual_classroom_participants_limit'])) {
			$params->vc_participants_limit = $data['virtual_classroom_participants_limit'];
		}

		if ($data['notifications'] == "1") {
			$access->notifications = true;
		} else {
			$access->notifications = false;
		}
		if ($data['scorm_access'] == "1") {
			$access->scorm = true;
		} else {
			$access->scorm = false;
		}
		if ($data['bizlibrary_access'] == "1") {
			$access->bizlibrary = true;
		} else {
			$access->bizlibrary = false;
		}

		$access->bizlibrary_content_selection = $data['bizlibrary_content_selection'];

		if ($data['sso_access'] == "1") {
			$access->sso = true;
		} else {
			$access->sso = false;
		}

		if ($data['api_access'] == "1") {
			$access->api = true;
		} else {
			$access->api = false;
		}




		$params->virtual_meeting_server = $data['virtual_meeting_server'];
		$params->access = $access;
		$data['params'] = json_encode($params);

		$appParams = new stdClass();
		$appParams->clientDB = $data['dbname'];
		$appParams->clientStatus = $data['status'];
		$app = new AxsOkta($appParams);
		$app->updateClientAppGroups();

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$domains = new stdClass();
			$domains->site_url = array();

			$data = $this->_formData;

			$dbparams = AxsClients::decryptClientParams($data['dbparams']);
			$data['dbname'] = $dbparams->dbname;

			$domain_list = explode(',', $data['domains']);
			foreach($domain_list as $domain) {
				$domains->site_url[] = $domain;
			}

			$data['domains'] = json_encode($domains);

			$params = json_decode($data['params']);
			$access = $params->access;
			$data['virtual_meeting_server'] = $params->virtual_meeting_server;

			// Load the virtual classroom participant limit
			$data['virtual_classroom_participants_limit'] = $params->vc_participants_limit;

			$data['bizlibrary_content_selection'] = $access->bizlibrary_content_selection;

			if ($access->eshop) {
				$data['eshop_access'] = "1";
			} else {
				$data['eshop_access'] = "0";
			}
			if ($access->lms) {
				$data['lms_access'] = "1";
			} else {
				$data['lms_access'] = "0";
			}
			if ($access->community) {
				$data['community_access'] = "1";
			} else {
				$data['community_access'] = "0";
			}
			if ($access->affiliates) {
				$data['affiliate_access'] = "1";
			} else {
				$data['affiliate_access'] = "0";
			}
			if ($access->forum) {
				$data['forum_access'] = "1";
			} else {
				$data['forum_access'] = "0";
			}
			if ($access->events) {
				$data['events_access'] = "1";
			} else {
				$data['events_access'] = "0";
			}
			if ($access->business_directory) {
				$data['business_directory_access'] = "1";
			} else {
				$data['business_directory_access'] = "0";
			}

			if ($access->boards) {
				$data['boards_access'] = "1";
			} else {
				$data['boards_access'] = "0";
			}
			if ($access->blog) {
				$data['blog_access'] = "1";
			} else {
				$data['blog_access'] = "0";
			}
			if ($access->media) {
				$data['media_access'] = "1";
			} else {
				$data['media_access'] = "0";
			}
			if ($access->reporting) {
				$data['reporting_access'] = "1";
			} else {
				$data['reporting_access'] = "0";
			}
			if ($access->polls) {
				$data['polls_access'] = "1";
			} else {
				$data['polls_access'] = "0";
			}
			if ($access->contract_signing) {
				$data['contract_signing_access'] = "1";
			} else {
				$data['contract_signing_access'] = "0";
			}
			if ($access->subscriptions) {
				$data['subscriptions_access'] = "1";
			} else {
				$data['subscriptions_access'] = "0";
			}
			if ($access->virtual_classroom) {
				$data['virtual_classroom_access'] = "1";
			} else {
				$data['virtual_classroom_access'] = "0";
			}
			if ($access->notifications) {
				$data['notifications'] = "1";
			} else {
				$data['notifications'] = "0";
			}
			if ($access->scorm) {
				$data['scorm_access'] = "1";
			} else {
				$data['scorm_access'] = "0";
			}
			if ($access->bizlibrary) {
				$data['bizlibrary_access'] = "1";
			} else {
				$data['bizlibrary_access'] = "0";
			}
			if ($access->sso) {
				$data['sso_access'] = "1";
			} else {
				$data['sso_access'] = "0";
			}
			if ($access->api) {
				$data['api_access'] = "1";
			} else {
				$data['api_access'] = "0";
			}

			return $data;
		}
	}
}