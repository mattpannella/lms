<?php
/**
 * Kunena Component
 * @package     Kunena.Administrator.Template
 * @subpackage  Prune
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die();

// @var KunenaAdminViewTools $this

?>

<div id="kunena" class="admin override">
	<div id="j-main-container" >

		<form id="adminForm" name="adminForm" action="<?php echo KunenaRoute::_('administrator/index.php?option=com_kunena&view=tools') ?>" method="post">
			<input type="hidden" name="task" value="cleanupip" />
			<?php echo JHtml::_('form.token'); ?>

			<fieldset>
				<legend><?php echo JText::_('COM_KUNENA_LEGEND_CLEANUP_IP'); ?></legend>
				<table class="table table-bordered table-striped">
					<tr>
						<td width="20%"><?php echo JText::_('COM_KUNENA_CLEANUP_IP_LEGEND_FROMDAYS') ?></td>
						<td>
							<div class="input-append">
								<input class="span3" type="text" name="cleanup_ip_days" value="30" />
								<span class="add-on"><?php echo JText::_('COM_KUNENA_CLEANUP_IP_LEGEND_DAYS') ?></span>
							</div>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
	<div class="float-end small">
		<?php echo KunenaVersion::getLongVersionHTML(); ?>
	</div>
</div>
