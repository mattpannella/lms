<?php
/**
 * Kunena Component
 * @package     Kunena.Administrator.Users
 * @subpackage  Users
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/

defined('_JEXEC') or die;

ob_start();
?>
	<button class="btn btn-danger" type="button" onclick="document.getElementById('catid').value='';" data-bs-dismiss="modal">
		<?php echo JText::_('JCANCEL'); ?>
	</button>
	<button class="btn btn-primary" type="submit" onclick="Joomla.submitbutton('batch_moderators');">
		<?php echo JText::_('JSUBMIT'); ?>
	</button>
<?php
$modalFooter = ob_get_clean();

ob_start();
?>
	<p><?php echo JText::_('COM_KUNENA_BATCH_USERS_TIP'); ?></p>
	<?php echo $this->modcatlist; ?>
<?php
$modalBody = ob_get_clean();

echo JHtml::_(
	'bootstrap.renderModal',
	'moderateModal',
	array(
		'title' => JText::_('COM_KUNENA_BATCH_USERS_OPTIONS'),
		'footer' => $modalFooter
	),
	$modalBody
);

?>