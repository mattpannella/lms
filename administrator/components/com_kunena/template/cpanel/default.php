<?php
/**
 * Kunena Component
 * @package     Kunena.Administrator.Template
 * @subpackage  CPanel
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die();
?>
<div id="kunena" class="admin override">
	<div id="j-sidebar-container" class="d-none">
		<div id="sidebar">
			<div class="sidebar-nav"><?php include KPATH_ADMIN . '/template/common/menu.php'; ?></div>
		</div>
	</div>
	<div id="j-main-container" class="col-sm-12 mt-3">
		<section class="content-block container-fluid" role="main">
			<?php echo JToolbarHelper::title('Discussion Boards'); ?>
			<div class="row">
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=categories">
						<div class="card-body">
							<i class="icon-big icon-list-view" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_CATEGORIES') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_CATEGORIES') ?></div>
					</a>
				</div>
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=users">
						<div class="card-body">
							<i class="icon-big icon-users" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_USERS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_USERS') ?></div>
					</a>
				</div>
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=attachments">
						<div class="card-body">
							<i class="icon-big icon-flag-2" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_FILES') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_FILES') ?></div>
					</a>
				</div>
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=smilies">
						<div class="card-body">
							<i class="icon-big icon-thumbs-up" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_EMOTICONS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_EMOTICONS') ?></div>
					</a>
				</div>
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=ranks">
						<div class="card-body">
							<i class="icon-big icon-star-2" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_RANKS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_RANKS') ?></div>
					</a>
				</div>
				<!-- <div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=templates">
						<div class="card-body">
							<i class="icon-big icon-color-palette" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TEMPLATES') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TEMPLATES') ?></div>
					</a>
				</div> -->
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=config">
						<div class="card-body">
							<i class="icon-big icon-cogs" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_CONFIG') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_CONFIG') ?></div>
					</a>
				</div>
				<!-- <div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=plugins">
						<div class="card-body">
							<i class="icon-big icon-puzzle" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_PLUGINS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_PLUGINS') ?></div>
					</a>
				</div> -->
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=logs">
						<div class="card-body">
							<i class="icon-big icon-search" alt="<?php echo JText::_('COM_KUNENA_LOG_MANAGER') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_LOG_MANAGER') ?></div>
					</a>
				</div>
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=statistics">
						<div class="card-body">
							<i class="icon-big icon-chart" alt="<?php echo JText::_('COM_KUNENA_MENU_STATISTICS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_MENU_STATISTICS') ?></div>
					</a>
				</div>
				<!-- <div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=tools">
						<div class="card-body">
							<i class="icon-big icon-tools" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TOOLS') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TOOLS') ?></div>
					</a>
				</div> -->
				<div class="col-md-3 col-lg-2">
					<a class="card text-center hover-grey" href="index.php?option=com_kunena&view=trash">
						<div class="card-body">
							<i class="icon-big icon-trash" alt="<?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TRASH') ?>"></i>
						</div>
						<div class="card-footer"><?php echo JText::_('COM_KUNENA_CPANEL_LABEL_TRASH') ?></div>
					</a>
				</div>
			</div>
		</section>
	</div>
</div>
