<?php
/**
 * Kunena Component
 *
 * @package    Kunena.Template
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license    https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link       https://www.kunena.org
 **/
defined('_JEXEC') or die();

class KunenaAdminTemplate
{
	/**
	 *
	 */
	public function initialize()
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::base(true) . '/components/com_kunena/media/css/layout.css', array('version' => 'auto'));
		$document->addStyleSheet(JUri::base(true) . '/components/com_kunena/media/css/styles.css');
	}

	/**
	 * Get template paths.
	 *
	 * @param   string     $path
	 *
	 * @param   bool|false $fullpath
	 *
	 * @return array
	 */
	public function getTemplatePaths($path = '', $fullpath = false)
	{
		if ($path)
		{
			$path = KunenaPath::clean("/$path");
		}

		$array   = array();
		$array[] = ($fullpath ? KPATH_ADMIN : KPATH_COMPONENT_RELATIVE) . '/template/' . $path;

		return $array;
	}

	/**
	 * Renders an item in the pagination block
	 *
	 * @param   JPaginationObject  $item  The current pagination object
	 *
	 * @return  string  HTML markup for active item
	 *
	 * @since   3.0
	 */
	public function paginationItem(JPaginationObject $item)
	{
		// Special cases for "Start", "Prev", "Next", "End".
		switch ($item->text)
		{
			case JText::_('JLIB_HTML_START') :
				$display = '<i class="lizicon-first"></i>';
				break;
			case JText::_('JPREV') :
				$display = '<i class="lizicon-previous2"></i>';
				break;
			case JText::_('JNEXT') :
				$display = '<i class="lizicon-next2"></i>';
				break;
			case JText::_('JLIB_HTML_END') :
				$display = '<i class="lizicon-last"></i>';
				break;
			default:
				$display = $item->text;
		}

		// Check if the item can be clicked.
		if (!is_null($item->base))
		{
			$limit = 'limitstart.value=' . (int) $item->base;

			return '<li class="page-item"><a class="page-link" href="#" title="' . $item->text . '" onclick="document.adminForm.' . $item->prefix . $limit . ';
			 Joomla.submitform();return false;">' . $display . '</a></li>';
		}

		// Check if the item is the active (or current) page.
		if (!empty($item->active))
		{
			return '<li class="page-item active"><a class="page-link">' . $display . '</a></li>';
		}

		// Doesn't match any other condition, render disabled item.
		return '<li class="page-item disabled"><a class="page-link">' . $display . '</a></li>';
	}
}
