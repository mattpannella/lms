<?php
/**
 * Kunena Component
 * @package     Kunena.Administrator.Template
 * @subpackage  Logs
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die();

// @var KunenaAdminViewTools $this

?>

<div id="kunena" class="admin override">
	<div id="j-main-container" >

		<form action="<?php echo KunenaRoute::_('administrator/index.php?option=com_kunena&view=logs') ?>" method="post" id="adminForm" name="adminForm">
			<input type="hidden" name="task" value="clean" />
			<?php echo JHtml::_('form.token'); ?>

			<fieldset>
				<legend><?php echo JText::_('COM_KUNENA_LOG_MANAGER'); ?></legend>
				<table class="table table-bordered table-striped">
					<tr>
						<td colspan="2"><?php echo JText::_('COM_KUNENA_LOG_CLEAN_DESC') ?></td>
					</tr>
					<tr>
						<td width="20%"><?php echo JText::_('COM_KUNENA_LOG_CLEAN_FROM') ?></td>
						<td>
							<div class="input-group">
								<input type="text" name="clean_days" value="30" />
								<span class="input-group-text"><?php echo JText::_('COM_KUNENA_LOG_CLEAN_FROM_DAYS') ?></span>
							</div>
						</td>
					</tr>
				</table>
			</fieldset>
		</form>
	</div>
	<div class="float-end small">
		<?php echo KunenaVersion::getLongVersionHTML(); ?>
	</div>
</div>
