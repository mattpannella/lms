<?php
/**
 * Kunena Component
 * @package     Kunena.Administrator.Users
 * @subpackage  Categories
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/

defined('_JEXEC') or die;

ob_start();
?>
	<button class="btn" type="button" data-bs-dismiss="modal">
		<?php echo JText::_('JCANCEL'); ?>
	</button>
	<button class="btn btn-primary" type="submit" onclick="document.getElementById('settingFormModal').submit();">
		<?php echo JText::_('JSUBMIT'); ?>
	</button>
<?php
$modalFooter = ob_get_clean();

ob_start();
?>
	<input type="hidden" name="task" value="setdefault" />
	<?php echo JHTML::_('form.token') ?>
	<p><?php echo JText::_('COM_KUNENA_CONFIG_MODAL_DESCRIPTION'); ?></p>
<?php
$modalBody = ob_get_clean();

?>
<form 
	id="settingFormModal" 
	name="settingFormModal" 
	action="<?php echo KunenaRoute::_('administrator/index.php?option=com_kunena&view=config') ?>" 
	method="post"
>
	<?php 
		echo JHtml::_(
			'bootstrap.renderModal',
			'settingModal',
			array(
				'title' => JText::_('COM_KUNENA_BATCH_USERS_OPTIONS'),
				'footer' => $modalFooter
			),
			$modalBody
		);
	?>
</form>
