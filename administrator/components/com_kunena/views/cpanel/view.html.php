<?php
/**
 * Kunena Component
 *
 * @package     Kunena.Administrator
 * @subpackage  Views
 *
 * @copyright   (C) 2008 - 2017 Kunena Team. All rights reserved.
 * @license     https://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link        https://www.kunena.org
 **/
defined('_JEXEC') or die();

/**
 * About view for Kunena cpanel
 *
 * @since  K1.X
 */
class KunenaAdminViewCpanel extends KunenaView
{
	/**
	 *
	 */
	function displayDefault()
	{
		$this->display();
	}
}
