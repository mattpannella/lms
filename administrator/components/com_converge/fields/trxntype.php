<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 1/19/16
 * Time: 12:27 PM
 */
defined('JPATH_PLATFORM') or die;

class FOFFormFieldTrxntype extends FOFFormFieldText {
    protected $type = 'trxntype';

    /**
     * @return string
     */
    public function getInput() {
        $ptext = parent::getInput();
        $type = $this->form->getValue('type');
	      $name = '';
		    foreach(AxsPayment::$trxn_types as $key => $value) {
			    if($value == $type) {
				    $name = $key;
				    break;
			    }
		    }
        $name = 'value="'.$name.'"';
        $pattern= '/value=".*"/';
        return preg_replace($pattern, $name, $ptext);
    }

    /**
     * Get the rendering of this field type for a repeatable (grid) display,
     * e.g. in a view listing many item (typically a "browse" task)
     *
     * @return  string  The field HTML
     *
     * @since 2.0
     */
    public function getRepeatable() {
        $type = $this->item->get('type');
	      foreach(AxsPayment::$trxn_types as $key => $value) {
	      	if($value == $type) {
	      		return $key;
		      }
	      }
    }
}