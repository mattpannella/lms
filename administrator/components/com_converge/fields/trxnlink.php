<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/5/16
 * Time: 1:31 PM
 */
defined('JPATH_PLATFORM') or die;

class FOFFormFieldTrxnlink extends FOFFormFieldText {
	protected $type = 'trxnlink';

	public function getRepeatable() {
		$id = $this->item->get('trxn_id');
		$user_id = $this->item->get('user_id');
		if($id) {
			return "<a href=\"index.php?option=com_users&view=axsusers&task=axsuser.edit&id=$user_id&trxn_id=$id#timeline\">Used Promo</a>";
		}
		return 'Applied Promo';
	}
}