<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldGenerateCode extends FOFFormFieldText {

	public function getInput() {
		
		ob_start();
		?>

			<div>
				Length: <input id="generateCodeLength" min="4" max="64" value="4" type="number" style="width: 100px; margin-bottom: 8px;">
			</div>

			<br>

			<div class="btn btn-primary" id="generateCode">Generate</div>
			<input id="generateCodeField" type="text" style="width: 640px"/>
			<br>
			Set a length and use this button to generate random passcodes

			<script>

				var codeField = document.getElementById("generateCodeField");
				jQuery("#generateCode").click(
					function() {

						var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
						var lengthField = document.getElementById("generateCodeLength");
						var length = lengthField.value;
						
						var code = "";

						var min = parseInt(lengthField.getAttribute('min'));
						var max = parseInt(lengthField.getAttribute('max'));

						if (length < min) {
							length = min;
						} else if (length > max) {
							length = max;
						}
						
						for (var i = 0; i < length; i++) {
							var rand = Math.floor(Math.random() * possible.length);
							code += possible.charAt(rand);
						}


						//var code = btoa(parseInt(Date.now() * Math.random(1000,9999)));
						//code = code.replace("=","");
						//console.log(now);
						codeField.value = code;
					}
				);

				jQuery(document).ready(
					function() {
						//Neither preventDefault() or stopPropagation() is stopping the onclick.  So, plan B.
						jQuery(".button-save-new, .button-save, .button-apply").each(
							function() {
								var onclick = this.getAttribute("onclick");
								this.setAttribute('onclick', '');
								this.setAttribute('saveFunction', onclick);
							}
						);

						jQuery(".button-save-new, .button-save, .button-apply").click(
							function() {
								var saveFunction = this.getAttribute("saveFunction");



								eval(saveFunction);
							}
						);
					}
				);
			</script>

		<?php
		return ob_get_clean();
	}	

	public function getRepeatable() {
		return "repeat";
	}
}