<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 7/20/16
 * Time: 4:33 PM
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldTriallength extends FOFFormFieldText {

	protected $type = 'triallength';


	/**
	 * @return string
	 */
	public function getInput()
	{
		$sublength = $this->form->getValue('default_trial_length');
		$out = '<select id="default_trial_length" name="default_trial_length">';
		$out .= '<option value="0" '.($sublength == 0 ? "selected" : "").'>Lifetime</option>';
		for($i=1; $i<=15; $i++) {
			$out .= '<option value="'.$i.'D" '.($sublength == $i."D" ? "selected" : "").'>'.$i.' Day'.($i>1 ? 's' : '').'</option>';
		}
		for($i=1; $i<=12; $i++) {
			$out .= '<option value="'.$i.'M" '.($sublength == $i."M" ? "selected" : "").'>'.$i.' Month'.($i>1 ? 's' : '').'</option>';
		}
		for($i=1; $i<=5; $i++) {
			$out .= '<option value="'.$i.'Y" '.($sublength == $i."Y" ? "selected" : "").'>'.$i.' Year'.($i>1 ? 's' : '').'</option>';
		}
		$out .= '</select>';
		return $out;
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable()
	{
		$sublength = $this->item->get('default_trial_length');
		if(!$sublength) {
			return "Lifetime";
		} else {
			$length = substr($sublength, 0, -1);
			$term = substr($sublength, -1);
			switch($term) {
				case 'M':
					$term = 'Month';
					break;
				case 'D':
					$term = 'Day';
					break;
				case 'Y':
					$term = 'Year';
					break;
			}
			return $length." ".$term.($length > 1 ? "s" : "");
		}
	}
}