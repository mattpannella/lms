<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/5/16
 * Time: 1:44 PM
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldPromocode extends FOFFormFieldText {
	protected $type = 'promocode';

	public function getRepeatable() {
		$id = $this->item->get('code_id');
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->qn('title'))
			->from($db->qn('axs_pay_promo_codes'))
			->where($db->qn('id').'='.(int)$id);
		$db->setQuery($query);
		$result = $db->loadObject();
		return "<a href=\"index.php?option=com_converge&view=promo&id=$id\">$result->title</a>";
	}
}