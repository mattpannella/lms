<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 6/28/16
 * Time: 4:58 PM
 */
defined('JPATH_PLATFORM') or die;

class FOFFormFieldFieldtype extends FOFFormFieldText {

	protected $type = 'fieldtype';

	/**
	 * @return string
	 */
	public function getInput() {
		$type = $this->form->getValue('type');
		$out = '<select id="type" name="type">';
		$out .= '<option value="text">text</option>';
		$out .= '<option value="checkbox">checkbox</option>';
		$out .= '<option value="color">color</option>';
		$out .= '<option value="date">date</option>';
		$out .= '<option value="email">email</option>';
		$out .= '<option value="hidden">hidden</option>';
		$out .= '<option value="image">image</option>';
		$out .= '<option value="number">number</option>';
		$out .= '<option value="password">password</option>';
		$out .= '<option value="countries">select countries</option>';
		$out .= '</select>';
		return $out;
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable() {
		return $this->item->get('type');
	}
}