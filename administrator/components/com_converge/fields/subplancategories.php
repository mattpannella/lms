<?php

defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class JFormFieldSubPlanCategories extends JFormField {

	public function getInput() {

		ob_start();

		?>
			<div id="hidden_cat_id"></div>
			<script>

				var hidden = document.getElementById("hidden_cat_id");
				
				jQuery(hidden.parentNode.parentNode).hide();

				jQuery(document).ready(
					function() {
						jQuery(document).on("click", ".search-choice", function() {
							redoUrl();
						});

						jQuery(document).on("click", ".result-selected", function() {
							redoUrl();
						});

						jQuery(document).on("mouseup", ".search-choice-close",
							function() {
								redoUrl();
							}
						);						
					}
				);

				function redoUrl() {
					//This needs to be delayed or it will run BEFORE an item is deleted/add and register incorrectly.
					setTimeout(
						function() {
							var url = document.getElementById("jform_link");
							var parse = url.value.split("&");
							var newUrl = "";

							for (let i = 0; i < parse.length; i++) {
								if (parse[i].contains("option") || parse[i].contains("view")) {
									if (newUrl) {
										newUrl += "&";
									}

									newUrl += parse[i];
								}
							}

							var list = document.getElementById("jform_request_cat_id").options;
							let count = 0;
							for (let i = 0; i < list.length; i++) {
								if (list[i].selected) {
									newUrl += "&cat_id[" + count + "]=" + list[i].value;
									count++;
								}
							}

							url.value = newUrl;
						}, 100
					);
				}
			</script>

		<?php

		return ob_get_clean();
	}
}