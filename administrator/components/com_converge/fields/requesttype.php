<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/13/16
 * Time: 5:34 PM
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldRequesttype extends FOFFormFieldText {

	protected $type = 'requesttype';

	/**
	 * @return string
	 */
	public function getInput() {
		return 'not yet implemented';
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable() {
		$type = $this->item->get('type');
		switch($type) {
			case 0:
				return 'Cancellation';
			case 1:
				return 'Change Plan';
		}
	}
}