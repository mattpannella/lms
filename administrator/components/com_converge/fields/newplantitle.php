<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 10/13/16
 * Time: 5:37 PM
 */

defined('JPATH_PLATFORM') or die;

class FOFFormFieldNewplantitle extends FOFFormFieldText {

	protected $type = 'newplantitle';

	/**
	 * @return string
	 */
	public function getInput() {
		return 'not yet implemented';
	}

	/**
	 * Get the rendering of this field type for a repeatable (grid) display,
	 * e.g. in a view listing many item (typically a "browse" task)
	 *
	 * @return  string  The field HTML
	 *
	 * @since 2.0
	 */
	public function getRepeatable() {
		$plan_id = $this->item->get('new_plan_id');
		if($plan_id) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select($db->qn('title'))
				->from($db->qn('axs_pay_subscription_plans'))
				->where($db->qn('id').'='.(int)$plan_id);
			$db->setQuery($query);
			return $db->loadObject()->title;
		} else {
			return '';
		}
	}
}