<?php

defined('JPATH_PLATFORM') or die;

class FOFFormFieldError extends FOFFormFieldText {

    protected $type = 'error';


    /**
     * @return string
     */
    public function getInput()
    {
        $trxn_row_id = $this->form->getValue('id');

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('cctransactions_errors'))
            ->where($db->quoteName('trxn_row_id').'='.(int)$trxn_row_id);
        $db->setQuery($query);
        $row = $db->loadObject();
        $error = isset($row) ? $row->error_message : 'No errors.';
        $eArray = explode("\n", $error);

        $out = '';
        foreach($eArray as $e) {
            $out .= $e . "<br>";
        }
        return "<p>".$out."</p>";
    }
}