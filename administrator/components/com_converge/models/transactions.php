<?php

defined('_JEXEC') or die;

class ConvergeModelTransactions extends FOFModel {

	public function getSettings() {
		$settings = new stdClass();
		$settings->master = 0;
		$settings->initiator = 0;
		$settings->start = 0;
		$settings->end = 0;
		$settings->transaction_type = 0;
		$settings->transaction_status = 0;
		$settings->user_id = 0;
		if($this->input->get('filter', false)) {
			$settings->master = $this->input->get('filter-master', 0);
			$settings->initiator = $this->input->get('filter-initiator', 0);
			$settings->start = $this->input->get('start', 0);
			$settings->end = $this->input->get('end', 0);
			$settings->transaction_type = $this->input->get('filter-transaction-type', 0);
			$settings->transaction_status = $this->input->get('filter-transaction-status', 0);
			$settings->user_id = $this->input->get('filter-user-id', 0);
			//also set the cookie
			$cookie = $this->input->cookie;
			$cookie->set('filter-master', $settings->master, time()+3600);
			$cookie->set('filter-initiator', $settings->initiator, time()+3600);
			$cookie->set('filter-start', $settings->start, time()+3600);
			$cookie->set('filter-end', $settings->end, time()+3600);
			$cookie->set('filter-transaction-type', $settings->transaction_type, time()+3600);
			$cookie->set('filter-transaction-status', $settings->transaction_status, time()+3600);
			$cookie->set('filter-user-id', $settings->user_id, time()+3600);
		} else {
			$cookie = $this->input->cookie;
			$settings->master = $cookie->get('filter-master', 0);
			$settings->initiator = $cookie->get('filter-initiator', 0);
			$settings->start = $cookie->get('filter-start', 0);
			$settings->end = $cookie->get('filter-end', 0);
			$settings->transaction_type = $cookie->get('filter-transaction-type', 0);
			$settings->transaction_status = $cookie->get('filter-transaction-status', 0);
			$settings->user_id = $cookie->get('filter-user-id', 0);
		}
		return $settings;
	}

	public function getData($limit, $page, $settings) {
		$db = JFactory::getDBO();
		$conditions = $settings->initiator ?  $db->qn('initiator').'='.(int)$settings->initiator : '';
		$conditions .= $settings->start ? ($conditions != '' ? ' AND ' : '') . 'DATE('.$db->qn('date').')>='. $db->q($settings->start) : '';
		$conditions .= $settings->end ? ($conditions != '' ? ' AND ' : '') . 'DATE('.$db->qn('date').')<='. $db->q($settings->end) : '';
		$conditions .= $settings->transaction_type ? ($conditions != '' ? ' AND ' : '') . $db->qn('type').'='.(int)$settings->transaction_type : '';
		
		$status = $this->getStatusName($settings->transaction_status);
		
		if (is_countable($status) && count($status) > 1) {			
			$conditions .= $conditions != '' ? ' AND (' : '(';
			for ($i = 0; $i < count($status); $i++) {
				$conditions .= $db->qn('status') . '=' . $db->q($status[$i]);
				$conditions .= $i != (count($status) - 1) ? ' OR ' : '';
			}
			$conditions .= ")";
		} else {
			$conditions .= $settings->transaction_status ? ($conditions != '' ? ' AND ' : '') . $db->qn('status').'='.$db->q($status) : '';
		}

		$conditions .= $settings->user_id ? ($conditions != '' ? ' AND ' : '') . $db->qn('user_id').'='.(int)$settings->user_id : '';

		//First grab the totals for the totals boxes
		$query = $db->getQuery(true);
		$query->select("
			COUNT(*) as `count`,
			SUM(CASE WHEN status='DEC' THEN 1 ELSE 0 END) as `dec`, 
			SUM(CASE WHEN status='FAIL' THEN 1 ELSE 0 END) as `fail`, 
			SUM(CASE WHEN status='SUC' THEN 1 ELSE 0 END) as `suc`, 
			SUM(CASE WHEN status='PLHO' THEN 1 ELSE 0 END) as `plho`,
			SUM(CASE WHEN status='REF' THEN 1 ELSE 0 END) as `ref`,
			
			SUM(CASE WHEN status='DEC' THEN amount ELSE 0 END) as `amountdec`,
			SUM(CASE WHEN status='FAIL' THEN amount ELSE 0 END) as `amountfail`,
			SUM(CASE WHEN status='SUC' THEN amount ELSE 0 END) as `amountsuc`,
			SUM(CASE WHEN status='PLHO' THEN amount ELSE 0 END) as `amountplho`,
			SUM(CASE WHEN status='REF' THEN amount ELSE 0 END) as `amountref`
		")
			->from('axs_pay_transactions');
		if ($conditions != '') {
			$query->where($conditions);
		}
		$db->setQuery($query);
		$data = $db->loadObject();

		//now lets run the real query to return the rows to display paginated
		$query->clear();
		$query->select('*')
			->from('axs_pay_transactions');
		if ($conditions != '') {
			$query->where($conditions);
		}
		$query->order('date DESC');
		$offset = ($limit * $page);
		$db->setQuery($query, $offset, $limit);
		$data->transactions = $db->loadObjectList();
		foreach($data->transactions as $t) {
			//we need to get name, email, phone, and item desc.
			$pd = AxsExtra::getUserProfileData($t->user_id);
			$t->email = $pd->email;
			$t->name = $pd->first.' '.$pd->last;
			$t->phone = $pd->phone;

			if($t->type == AxsPayment::$trxn_types['event']) {
				$query = $db->getQuery(true);

				$query
					->select('title')
					->from('joom_eb_events')
					->where('id ='.(int)$t->type_id);

				$db->setQuery($query);
				$cp = $db->loadObject();
				$t->item = $cp->title;
			}

			if($t->type == AxsPayment::$trxn_types['e-commerce']) {
				$query = $db->getQuery(true);

				$query
					->select('product_name,quantity')
					->from('joom_eshop_orderproducts')
					->where('order_id ='.(int)$t->type_id);

				$db->setQuery($query);
				$cp = $db->loadObjectList();
				$product = [];
				foreach($cp as $item) {
					array_push($product, $item->product_name.': '.$item->quantity);
				}
				$title = implode('</br>', $product);
				$t->item = $title;
			}

			if($t->type == AxsPayment::$trxn_types['course']) {
				$query = $db->getQuery(true);

				$query
					->select('title')
					->from('axs_course_purchases AS purchase')
					->join('INNER', 'joom_splms_courses AS course ON purchase.course_id = course.splms_course_id')
					->where('purchase.id ='.(int)$t->type_id);

				$db->setQuery($query);
				$cp = $db->loadObject();
				$t->item = $cp->title;
			}

			if($t->type == AxsPayment::$trxn_types['subscription']) {
				$query = $db->getQuery(true);
				$query
					->select('plan.title')
					->from('axs_pay_subscription_periods AS period')
					->join('INNER', 'axs_pay_subscriptions AS sub ON period.sub_id = sub.id')
					->join('INNER', 'axs_pay_subscription_plans AS plan ON sub.plan_id = plan.id')
					->where('period.id ='.(int)$t->type_id);
				$db->setQuery($query);
				$cp = $db->loadObject();
				$t->item = $cp->title;
			} 
		}
		return $data;
	}

	public function getStatusName($status_id) {
		$status = '';
		switch($status_id) {
			case 1:
				$status = 'SUC';
				break;
			case 2:
				$status = 'PLHO';
				break;
			case 3:
				$status = 'FAIL';
				break;
			case 4:
				$status = 'DEC';
				break;
			case 5:
				$status = 'REF';
				break;
			case 6:
				$status = ['DEC', 'FAIL'];
				break;
		}
		return $status;
	}
}