<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class ConvergeModelCategories extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$params = new stdClass();
			$params->multi_purchase = $data['multi_purchase'];
			$params->background_color = $data['background_color'];
			$params->show_title = $data['show_title'];
			$data['params'] = json_encode($params);
		}	

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);

			$data = $this->_formData;

			$data['multi_purchase'] = $params->multi_purchase;
			$data['background_color'] = $params->background_color;
			$data['show_title'] = $params->show_title;
			return $data;
		}
	}
}