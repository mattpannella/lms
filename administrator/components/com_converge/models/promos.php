<?php

defined('_JEXEC') or die;

class ConvergeModelPromos extends FOFModel {
	public function save($data) {
		if(is_array($data)) {
			$valid_products = new stdClass();
			$valid_products->courses = json_encode($data['valid_courses']);
			$valid_products->plans = json_encode($data['valid_plans']);
			$data['valid_products'] = json_encode($valid_products);
		}
		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$valid_products = json_decode($this->_formData['valid_products']);
			$this->_formData['valid_courses'] = json_decode($valid_products->courses);
			$this->_formData['valid_plans'] = json_decode($valid_products->plans);
			return $this->_formData;
			
		}
	}
}