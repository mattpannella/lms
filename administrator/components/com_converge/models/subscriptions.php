<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 8/10/16
 * Time: 2:04 PM
 */

defined('_JEXEC') or die;

//include_business_directory
//business_directory_plan

class ConvergeModelSubscriptions extends FOFModel {
	public function saveFailedSubscription($user_id, $first, $last, $email, $phone, $address, $city, $state, $country, $zip, $plan_id, $reason) {
		
		$session_id = JFactory::getSession()->getId();

		$db = JFactory::getDbo();
		$cols = array (
			'user_id',
			'first',
			'last',
			'email',
			'phone',
			'address',
			'city',
			'state',
			'country',
			'zip',
			'plan_id',
			'date',
			'session_id',
			'reason'
		);

		$vals = array (
			(int)$user_id,
			$db->quote($first),
			$db->quote($last),
			$db->quote($email),
			$db->quote($phone),
			$db->quote($address),
			$db->quote($city),
			$db->quote($state),
			$db->quote($country),
			$db->quote($zip),
			(int)$plan_id,
			$db->quote(date("Y-m-d H:i:s")),
			$db->quote($session_id),
			$db->quote($reason)
		);

		$query = $db->getQuery(true);

		$query
			->insert('axs_pay_failed_subscriptions')
			->columns($db->quoteName($cols))
			->values(implode(',', $vals));

		$db->setQuery($query);
		$db->execute();
		return $db->insertid();
	}

	public function getSettings() {

		$settings = new stdClass();
		$settings->master = 0;
		$settings->plan = 0;
		$settings->status = 0;
		$settings->user_id = 0;

		if ($this->input->get('filter', false)) {
			$settings->master = $this->input->get('filter-subscription-master', 0);
			$settings->plan = $this->input->get('filter-subscription-plan', 0);
			$settings->status = $this->input->get('filter-subscription-status', 0);
			$settings->user_id = $this->input->get('filter-subscription-userid', 0);
			//also set the cookie
			$cookie = $this->input->cookie;
			$cookie->set('filter-subscription-master', $settings->master, time()+3600);
			$cookie->set('filter-subscription-plan', $settings->plan, time()+3600);
			$cookie->set('filter-subscription-status', $settings->status, time()+3600);
			$cookie->set('filter-subscription-userid', $settings->user_id, time()+3600);
		} else {
			$cookie = $this->input->cookie;
			$settings->master = $cookie->get('filter-subscription-master', 0);
			$settings->plan = $cookie->get('filter-subscription-plan', 0);
			$settings->status = $cookie->get('filter-subscription-status', 0);
			$settings->user_id = $cookie->get('filter-subscription-userid', 0);
		}

		return $settings;
	}

	public function getData($limit, $page, $settings) {

		$db = JFactory::getDBO();

		$conditions = array();
		
		if ($settings->plan) {
			$conditions[]= $db->quoteName('plan_id') . '=' . (int)$settings->plan; 
		}

		if ($settings->status) {
			$conditions[]= $db->quoteName('status') . '=' . $db->quote($this->getStatusName($settings->status));
		}

		if ($settings->user_id) {
			$conditions[]= $db->quoteName('user_id') . '=' . (int)$settings->user_id;
		}

		//First grab the totals for the totals boxes
		$query = $db->getQuery(true);
		$query
			->select("
				COUNT(*) as `count`,
				SUM(CASE WHEN status='ACT' THEN 1 ELSE 0 END) as `act`, 
				SUM(CASE WHEN status='INAC' THEN 1 ELSE 0 END) as `inac`, 
				SUM(CASE WHEN status='GRC' THEN 1 ELSE 0 END) as `grc`
			")
			->from('axs_pay_subscriptions');

		if ($conditions) {
			$query->where($conditions);
		}

		$db->setQuery($query);		
		$data = $db->loadObject();


		//now lets run the real query to return the rows to display paginated
		$query->clear();
		$query
			->select('*')
			->from('axs_pay_subscriptions');

		if ($conditions) {
			$query->where($conditions);
		}
		
		$offset = ($limit * $page);
		$db->setQuery($query, $offset, $limit);
		$data->subscriptions = $db->loadObjectList();

		foreach ($data->subscriptions as $sub) {
			//we need to get name, email, phone, and plan desc.
			$profile = AxsExtra::getUserProfileData($sub->user_id);
			$sub->email = $profile->email;
			$sub->name = $profile->first.' '.$profile->last;
			$sub->phone = $profile->phone;

			$query = $db->getQuery(true);
			$query
				->select('title')
				->from('axs_pay_subscription_plans')
				->where('id='.(int)$sub->plan_id);

			$db->setQuery($query);

			$plan = $db->loadObject();
			$sub->plan = $plan->title;
		}

		return $data;
	}

	public function getStatusName($status_id) {
		$status = '';
		switch($status_id) {
			case 1:
				$status = 'ACT';
				break;
			case 2:
				$status = 'INAC';
				break;
			case 3:
				$status = 'GRC';
				break;
		}
		return $status;
	}
}