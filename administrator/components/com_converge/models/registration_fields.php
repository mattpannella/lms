<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 1/27/16
 * Time: 10:16 AM
 */

defined('_JEXEC') or die();

class ConvergeModelRegistration_fields extends FOFModel {

	public function save($data) {
		if(is_array($data)) {
			$data['name'] = preg_replace('/[^a-zA-Z0-9]/', '', $data['name']);
		}
		return parent::save($data);
	}
 
}