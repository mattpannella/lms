<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class ConvergeModelPlans extends FOFModel {

	public function getItemList($overrideLimits = false, $group = '') {
		$this->blacklistFilters(array('cat_id'));

		$query = $this->buildQuery($overrideLimits);
		if (!$overrideLimits)
		{
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
			$this->list = $this->_getList((string) $query, $limitstart, $limit, $group);
		}
		else
		{
			$this->list = $this->_getList((string) $query, 0, 0, $group);
		}

		return $this->list;
	}

	public function save($data) {
		if(is_array($data)) {
			$params = new stdClass();

			$params->tovuti_builder = $data['tovuti_builder'];
			$params->tovuti_builder_price_text = $data['tovuti_builder_price_text'];
			$params->include_address = $data['include_address'];
			$params->require_address = $data['require_address'];
			$params->button_text = $data['button_text'];
			$params->button_link = $data['button_link'];

			$params->plan_image_type = $data['plan_image_type'];
			$params->plan_image = $data['plan_image'];
			$params->plan_image_style = $data['plan_image_style'];
			$params->plan_image_border_color = $data['plan_image_border_color'];
			$params->plan_image_border_size = $data['plan_image_border_size'];
			$params->plan_icon = $data['plan_icon'];
			$params->plan_icon_color = $data['plan_icon_color'];
			$params->plan_icon_background_color = $data['plan_icon_background_color'];
			$params->plan_title_background_color = $data['plan_title_background_color'];
			$params->plan_title_text_color = $data['plan_title_text_color'];
			$params->plan_pricing_background_color = $data['plan_pricing_background_color'];
			$params->plan_pricing_text_color = $data['plan_pricing_text_color'];
			$params->use_pricing_text = $data['use_pricing_text'];
			$params->amount_pricing_text = $data['amount_pricing_text'];

			$params->custom_cancel_text = $data['custom_cancel_text'];
			$params->redirect_on_signup = $data['redirect_on_signup'];
			$params->redirect_on_signup_link = $data['redirect_on_signup_link'];
			$params->captcha = $data['captcha'];
			$params->passcodes = json_encode($data['plan_passcodes']);
			$params->usepasscodes = $data['usepasscodes'];
			$params->requirement_text = $data['requirement_text'];

			if ($data['include_business_directory'] == "1") {
				$params->include_business_directory = true;
			} else {
				$params->include_business_directory = false;
			}

			$params->business_directory_plan = $data['business_directory_plan'];

			$error = new stdClass();
			$error->invalid = $data['error_invalid_text'];
			$error->full = $data['error_full_text'];
			$error->expired = $data['error_expired_text'];

			$params->error = $error;

			$data['params'] = json_encode($params);
			$data['usergroups'] = json_encode($data['usergroups']);
			$data['courses'] = json_encode($data['courses']);
			$data['community_groups'] = json_encode($data['community_groups']);
			$data['reg_fields'] = json_encode($data['reg_fields']);

			//Save the codes to the passcoes table
			$id = (int)$data['id'];

			$db = JFactory::getDBO();

			$passcodes = $data['plan_passcodes'];

			// we're using this so that we can delete all passcodes that were not in the save request
			$valid_codes = array();

			if(is_iterable($passcodes)) {

				foreach ($passcodes as $passcode) {

					if (!$passcode['code']) {
						continue;
					}

					$code = $db->quote($passcode['code']);

					$maximum = (int)$passcode['maximum'];
					if ($maximum < 0) {
						$maximum = 0;
					}

					if ($passcode['unlimited'] == "yes") {
						$unlimited = 1;
					} else {
						$unlimited = 0;
					}

					if ($passcode['active'] == "yes") {
						$active = 1;
					} else {
						$active = 0;
					}

					if ($passcode['expiredate']) {
						$expire = date('Y-m-d', strtotime($passcode['expiredate']));
					} else {
						$expire = null;
					}

					$expire = $db->quote($expire);

					$id = (int)$data['id'];

					$query = "
						INSERT INTO axs_subscription_plan_codes (
							plan_id,
							code,
							maximum,
							userlimit,
							expire,
							active
						) VALUES (
							$id,
							$code,
							$maximum,
							$unlimited,
							$expire,
							$active
						) ON DUPLICATE KEY UPDATE 
							maximum = $maximum,
							userlimit=$unlimited,
							active=$active,
							expire=$expire
						";

					$valid_codes[] = $code;

					$db->setQuery($query);
					$db->execute();
				}
			}

			// delete all that were not received in the request
			$clean_query = $db->getQuery(true);
			$clean_query->delete($db->quoteName('axs_subscription_plan_codes'));
			$clean_query->where($db->quoteName('plan_id') . ' = ' . $id);

			if (!empty($valid_codes)) {
				$clean_query->where($db->quoteName('code') . ' NOT IN (' . implode(', ', $valid_codes) . ')');
			}

			$db->setQuery($clean_query);
			$db->execute();
        }

		return parent::save($data);
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);

			$data = $this->_formData;

			$data['captcha'] = $params->captcha;
			$data['tovuti_builder'] = $params->tovuti_builder;
			$data['tovuti_builder_price_text'] = $params->tovuti_builder_price_text;
			$data['include_address'] = $params->include_address;
			$data['require_address'] = $params->require_address;
			$data['button_text'] = $params->button_text;
			$data['button_link'] = $params->button_link;
			$data['plan_image_type'] = $params->plan_image_type;
			$data['plan_image'] = $params->plan_image;
			$data['plan_image_style'] = $params->plan_image_style;
			$data['plan_image_border_color'] = $params->plan_image_border_color;
			$data['plan_image_border_size'] = $params->plan_image_border_size;
			$data['plan_icon'] = $params->plan_icon;
			$data['plan_icon_color'] = $params->plan_icon_color;
			$data['plan_icon_background_color'] = $params->plan_icon_background_color;
			$data['plan_title_background_color'] = $params->plan_title_background_color;
			$data['plan_title_text_color'] = $params->plan_title_text_color;
			$data['plan_pricing_background_color'] = $params->plan_pricing_background_color;
			$data['plan_pricing_text_color'] = $params->plan_pricing_text_color;
			$data['use_pricing_text'] = $params->use_pricing_text;
			$data['amount_pricing_text'] = $params->amount_pricing_text;
			$data['custom_cancel_text'] = $params->custom_cancel_text;
			$data['redirect_on_signup'] = $params->redirect_on_signup;
			$data['redirect_on_signup_link'] = $params->redirect_on_signup_link;
			$data['plan_passcodes'] = json_decode($params->passcodes, true);
			$data['requirement_text'] = $params->requirement_text;

			if ($params->include_business_directory) {
				$data['include_business_directory'] = "1";
			} else {
				$data['include_business_directory'] = "0";
			}

			$data['business_directory_plan'] = $params->business_directory_plan;

			$error = $params->error;
			$data['error_invalid_text'] = $error->invalid;
			$data['error_full_text'] = $error->full;
			$data['error_expired_text'] = $error->expired;

			$db = JFactory::getDBO();
			$query = $db->getQuery(true);

			if(is_iterable($data['plan_passcodes'])) {

				foreach ($data['plan_passcodes'] as &$passcode) {
					//$query = "SELECT inuse FROM axs_subscription_plan_codes WHERE plan_id = " . $data['id'] . " AND code='"'

					$conditions = array(
						$db->quoteName("plan_id") . '=' . (int)$data['id'],
						$db->quoteName("code") . '=' . $db->quote($passcode['code'])
					);

					$query->clear();
					$query
						->select('inuse')
						->from($db->quoteName("axs_subscription_plan_codes"))
						->where($conditions);

					$db->setQuery($query);
					$result = $db->loadObject();

					if (!$result->inuse) {
						$passcode['codesinuse'] = 0;
					} else {
						$passcode['codesinuse'] = (int)$result->inuse;
					}
				}
			}

			$data['usepasscodes'] = $params->usepasscodes;

			$data['usergroups'] = json_decode($data['usergroups']);
			$data['courses'] = json_decode($data['courses']);
			$data['community_groups'] = json_decode($data['community_groups']);
			$data['reg_fields'] = json_decode($data['reg_fields']);

			return $data;
		}
	}
}
