var $ = jQuery;

$(function() {
    $('.toolbar button').click(function(ev) {
        var id =  this.id;
        //do something based on id
        switch(id) {
            case 'unsettled':
                fetchUnsettled(id);
                break;
        }
    });
});

function fetchUnsettled(id) {
    //start the loader
    $('.loading-image').show();
    $.ajax({
        url: '/administrator?option=com_converge&task=helper.refreshUnsettled&format=raw',
        type: 'get'
    }).done(function(result) {
        $('#table-unsettled').remove();
        $('#view-unsettled').append(result);
        //hide the loader
        $('.loading-image').hide();
    });
}