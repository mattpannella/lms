/**
 * Created by mar on 1/19/16.
 * THIS FILE IS INCLUDED IN BOTH THE SINGLE TRANSACTION VIEW AND THE USERS TAB VIEW.
 */

//iffe to make my code safe from outside interference
(function($) {
    $(function() {
        $('body').append($('#myModal')); //needed to fix the bug where modal pops up below background

        //######################################### For cancelling their plan ########################################
        $(document).on('click', '.cancelPlan', function() {
            var uid = this.dataset.user;
            var row_id = this.dataset.id;
            if(confirm('Confirm that you want to cancel their subscription.')) {
                $(this).prop('disabled', true);
                //grab the new settings and send to php with ajax
                $.ajax({
                    url: 'index.php?option=com_converge&task=transactions.cancelPlanUserView&format=raw',
                    data: {
                        user_id: uid,
                        id: row_id
                    },
                    type: 'post'
                }).done(function(result) {
                    console.log(result);
                    if(result == 'success') {
                        alert('Plan was successfully cancelled!');
                        location.reload(); //refresh page
                    } else {
                        alert('There was an error trying to cancel plan!');
                    }
                }).fail(function() {
                    alert('There was an error trying to cancel plan!');
                });
            }
        });

        //######################################### For changing their plan ########################################
        $(document).on('click', '.upgradePlan', function() {
            var uid = this.dataset.user;

            $('#myModalLabel').text('Change Plan');

            var clone = $('#upgradePlanForm').clone().addClass('upgradePlanFormClone');
            $('#myModalBody').html(clone);
            $('.upgradePlanFormClone').show();

            //reset class so the form submits correctly
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success upgradePlanSave').text('Change Plan');
        });

        $(document).on('change', '.upgradePlanFormClone select[name=plan_id]', function() {
            var plan_id = $(this).val();
            $.each(plans, function() {
                if(this.id == plan_id) {
                    var fee = 0.0;
                    if(parseInt(this.trial_amount)) {
                        fee = parseInt(this.trial_amount) - parseInt(this.price);
                    }
                    $('.upgradePlanFormClone .planTitle').html(this.title);
                    $('.upgradePlanFormClone input[name=planAdjustedSignup]').val(fee);
                    $('.upgradePlanFormClone input[name=planAdjustedAmount]').val(this.price);

                    //if there is any payment involved, we show the payment fields
                    if(fee || parseInt(this.price)) {
                        $('.upgradePlanFormClone .planPaymentDetails').slideDown();
                    } else {
                        $('.upgradePlanFormClone .planPaymentDetails').slideUp();
                    }
                }
            });
        });

        $(document).on('click', '.upgradePlanSave', function() {
            //some validation
            var plan_id = $('.upgradePlanFormClone select[name=plan_id]').val();
            var card = 0;
            var date = '';
            var signup = 0;
            var amount = 0;
            var uid = $('.upgradePlanFormClone').data('user');
            var stop = false;

            $.each(plans, function() {
                if(this.id == plan_id) {
                    //if there is payment involved, we grab the payment data
                    if(parseInt(this.trial_amount) || parseInt(this.price)) {
                        card = $('.upgradePlanFormClone select[name=upgrade_card]').val();
                        if(!card) {
                            stop = true; //stop the save if they didn't enter a card.
                        }
                        date = $('.upgradePlanFormClone input[name=upgrade_date]').val();
                        signup = $('.upgradePlanFormClone input[name=planAdjustedSignup]').val();
                        amount = $('.upgradePlanFormClone input[name=planAdjustedAmount]').val();
                    }
                }
            });

            if(!stop) {
                $(this).prop('disabled', true);
                //grab the new settings and send to php with ajax
                $.ajax({
                    url: 'index.php?option=com_converge&task=transactions.changePlan&format=raw',
                    data: {
                        user_id: uid,
                        plan_id: plan_id,
                        card: card,
                        date: date,
                        signup: signup,
                        amount: amount
                    },
                    type: 'post'
                }).done(function(result) {
                    if(result == 'success') {
                        alert('Plan was changed successfully!');
                        location.reload(); //refresh page
                    } else {
                        alert('There was an error trying to change plan!');
                        $('.upgradePlanSave').prop('disabled', false);
                    }
                }).fail(function() {
                    alert('There was an error trying to change plan!');
                    $('.upgradePlanSave').prop('disabled', false);
                });
            } else {
                alert('Can\'t save, you need to set a card to charge, if no card on file, add one.');
            }
        });

        //######################################### For changing their plan ########################################
        $(document).on('click', '.upgradeLevel', function() {
            $('#myModalLabel').text('Upgrade Level');

            var clone = $('#upgradeLevelForm').clone().addClass('upgradeLevelFormClone');
            $('#myModalBody').html(clone);
            $('.upgradeLevelFormClone').show();

            //reset classes so the form submits correctly
            $('.modal-footer .btn-success').removeClass().addClass('btn btn-success upgradeLevelSave').text('Upgrade Level');
        });

        $(document).on('change', '.upgradeLevelFormClone select[name=level_id]', function() {
            var level_id = $(this).val();
            $.each(courses, function() {
                if(this.splms_course_id == level_id) {
                    $('.upgradeLevelFormClone .levelTitle').html(this.title);
                    $('.upgradeLevelFormClone input[name=levelAdjustedAmount]').val(this.price);

                    //if there is any payment involved, we show the payment fields
                    if(parseInt(this.price)) {
                        $('.upgradeLevelFormClone .levelPaymentDetails').slideDown();
                    } else {
                        $('.upgradeLevelFormClone .levelPaymentDetails').slideUp();
                    }
                }
            });
        });

        $(document).on('click', '.upgradeLevelSave', function() {
            //some validation
            var level_id = $('.upgradeLevelFormClone select[name=level_id]').val();
            var card = $('.upgradeLevelFormClone select[name=upgrade_card]').val();
            var amount = $('.upgradeLevelFormClone input[name=levelAdjustedAmount]').val();
            var uid = $('.upgradeLevelFormClone').data('user');

            $(this).prop('disabled', true);
            $.ajax({
                url: 'index.php?option=com_converge&task=transactions.upgradeLevel&format=raw',
                data: {
                    user_id: uid,
                    level_id: level_id,
                    card: card,
                    amount: amount
                },
                type: 'post'
            }).done(function(result) {
                console.log(result);
                if(result == 'success') {
                    alert('Level upgraded successfully!');
                    location.reload(); //refresh page
                } else {
                    alert('There was an error!');
                    $('.upgradePlanSave').prop('disabled', false);
                }
            }).fail(function() {
                alert('There was an error!');
                $('.upgradePlanSave').prop('disabled', false);
            });
        });



        //######################################### For Changing the user's settings ########################################
        $(document).on('change', '#usePoints', function() {
            var uid = this.dataset.user;
            var plan = this.dataset.plan;
            savePlanSettings(uid, plan);
        });

        $(document).on('change', '#useCard', function() {
            var uid = this.dataset.user;
            var plan = this.dataset.plan;
            savePlanSettings(uid, plan);
        });

        function savePlanSettings(uid, plan) {
            var card = $('#useCard').val();
            var points = $('#usePoints').val();

            $.ajax({
                url: 'index.php?option=com_converge&task=transactions.savePlan&format=raw&uid='+uid+'&plan='+plan+'&points='+points+'&card='+card,
                type: 'get'
            }).done(function(result) {
                //console.log(result);
                if(result != 'success') {
                    alert('There was an error trying to save their plan settings.');
                }
            }).fail(function() {
                alert('There was an error trying to save their plan settings.');
            });
        }
    });
})(jQuery);