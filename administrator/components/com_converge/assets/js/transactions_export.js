function exportCSV (count, titles) {

	var message = false;
	if (count > 50000) {
		message = "You are trying to export " + count + " entries.\nThis can take a very long time.\n(Upwards of 5 - 10 minutes)\nWe recommend refining your search filters before continuing.\nAre you sure you want to continue?";
	} else if (count > 10000) {
		message = "You are trying to export " + count + "entries.\nThis may take a long time.\nAre you sure you want to continue?";
	} else if (count > 1000) {
		message = "You are trying to export " + count + " entries.\nAre you sure you want to continue?";
	}

	if (message != false) {
		var result = confirm(message);
		if (result == false) {
			return;
		}
	}

	var exportButton = document.getElementById('exportCSVButton');
	var originalText = exportButton.innerHTML;
	exportButton.innerHTML = "Processing...";
	exportButton.disabled = true;

	jQuery(exportButton).css("cursor", "wait");
	jQuery.ajax({
        type: 'GET',
        url: '/administrator/?option=com_converge&view=transactions&task=export_to_csv&format=raw',
        data: {
        	titles: 	JSON.stringify(titles),
        	csv: 		1
        },
        success: function(response) {
        	exportButton.innerHTML = originalText;
        	exportButton.disabled = false;

        	jQuery(exportButton).css("cursor", "pointer");

        	var data = JSON.parse(response);

        	exportToCsv("transactions.csv", data);
        }
    });
}