var current_page = 0;
var num_per_page = 0;


window.onload = function() {
	var button_area = document.getElementById("pagination_buttons");
	var total_count = jQuery(button_area).attr("rowCount");

	var button_list = document.createElement("ul");
    button_list.className += "pagination";
	button_area.appendChild(button_list);

	var select_list = jQuery("#pagination_select");
	var num_per_page = select_list.val();

	var num_pages = Math.ceil(total_count / num_per_page);

	select_list.change(
		function() {
			num_per_page = select_list.val();
		}
	);

	jQuery("#pagination_refresh").click(
		function() {
			refresh_page(select_list.val(), 0);
		}
	);

	var make_button = function(className, text) {
			var button = document.createElement("li");
			button.className += "page-item";
			var buttonLink = document.createElement("a");

			if (typeof(className) != 'undefined') {
					buttonLink.className = className;
					if (typeof(text) != 'undefined') {
							buttonLink.innerHTML = text;
					}
			}

			button.appendChild(buttonLink);
			return button;
	};

	var change_page = function(new_page) {
			if (new_page < 0 || new_page > num_pages) {
					return;
			}
			refresh_page(num_per_page, new_page);
	};

	var setClick = function(which, i) {
			return function() {
					jQuery(which).click(
							function() {
									change_page(i);
							}
					);
			}
	};

	if (current_page > 0) {
		var start_button = make_button("page-link", "<span class='icon-first'></span>");
		start_button.title = "Page: 0";
	    jQuery(start_button).click(
	        function () {
	            change_page(0);
	        }
	    );

	    var back_button = make_button("page-link", "<span class='icon-previous'></span>");
	    back_button.title = "Page: " + (current_page - 1);
	    jQuery(back_button).click(
	        function() {
	            change_page(current_page - 1);
	        }
	    );

    	button_list.appendChild(start_button);
    	button_list.appendChild(back_button);
    }

    for (i = 10; i >= 1; i--) {

    	var dest = Math.pow(10, i);

    	if (current_page >= dest) {
    		var button = make_button("page-link", "Back " + dest);
	    	button.title = "Page: " + (current_page - dest);

    		button_list.appendChild(button);
    		setClick(button, current_page - dest)();
    	}
    }

    var available_pages = 20;

    var start_page = current_page - Math.ceil(available_pages / 2);
    var end_page = current_page + Math.ceil(available_pages / 2);

    //Don't allow page numbers less than 0 or greater than the number of available pages.
    if (start_page < 0) {
    	start_page = 0;
    	end_page = start_page + available_pages;
    }

    if (end_page > num_pages) {
    	end_page = num_pages;
    	start_page = end_page - available_pages;
    }

    if (start_page < 0) {
    	start_page = 0;
    }

    //Don't display pages if there is only 1.
    if (end_page - start_page > 1) {
	    for (var i = start_page; i < end_page; i++) {
	    	var new_button = make_button('page-link', i);
	    	new_button.title = "Page: " + i;
	    	if (i == current_page) {
	    		new_button.className += " active";
	    	}

	        button_list.appendChild(new_button);
	        setClick(new_button, i)();
	    }
	}

	for (i = 1; i <= 10; i++) {
		var dest = Math.pow(10, i);

		if (current_page < (num_pages - dest)) {
			var button = make_button("page-link", "Forward " + dest);
			button.title = "Page: " + (current_page + dest);

			button_list.appendChild(button);
			setClick(button, current_page + dest)();
		}
	}

	if (current_page < (num_pages - 1)) {
		var next_button = make_button("page-link", "<span class='icon-next'></span>");
		next_button.title = "Page: " + (current_page + 1);
		jQuery(next_button).click(
				function() {
						change_page(current_page + 1);
				}
		);

		var last_button = make_button("page-link", "<span class='icon-last'></span>");
		next_button.title = "Page: " + (num_pages - 1);
		jQuery(last_button).click(
				function () {
						change_page(num_pages - 1);
				}
		);

		button_list.appendChild(next_button);
		button_list.appendChild(last_button);
	}
};

function refresh_page(show, page) {
		window.location = "index.php?option=com_converge&view=" + document.querySelector('#pagination_buttons').dataset.view + "&count=" + show + "&page=" + page;
}

function set_page(show, page) {
		num_per_page = show;
		current_page = page;
		jQuery("select#pagination_select option").each(
				function() {
						this.selected = (this.value == show);
				}
		);
}
