/**
 * Created by mar on 1/20/16.
 */

function deleteCard(ev) {
    var $ = jQuery; //grab me jquery as $
    ev.preventDefault();

    if(confirm('Are you sure you want to delete the card?')) {
        //snag their transaction id from the page
        var token = $('input[name=token]').val();
        $.ajax({
            url: '/administrator?option=com_converge&task=helper.deleteCard&format=raw&token='+token,
            type: 'get'
        }).done(function(result) {
            console.log(result);
            if(result == 'success') {
                alert('Card was deleted.');
                $('#cancel').prop('disabled', true);
            } else {
                alert('Something went wrong. Try again, then contact support.');
            }
        });
    }
}