/**
 * Created by mar on 5/3/16.
 */

//iffe to make my code safe from outside interference
(function($) {
  $(function() {
    $('.totals-stats').click(function() {
      $(this).parents('.span3').find('.totals-lower').slideToggle();
    });

    $('select[name=filter-master]').change(function() {
      var today = new Date().toJSON().slice(0,10);
      switch($(this).val()) {
        case '0':
          resetForm();
          $(this).val('0');
          break;
        case '1':
          //today's automated
          resetForm();
          $(this).val('1');
          $('select[name=filter-initiator]').val(1);
          $('input[name=start]').val(today);
          $('input[name=end]').val(today);
          break;
        case '2':
          //today's manual
          resetForm();
          $(this).val('2');
          $('select[name=filter-initiator]').val(2);
          $('input[name=start]').val(today);
          $('input[name=end]').val(today);
          break;
      }
    });

    function resetForm() {
      var myForm = $('#filters')[0];
      selectTags = myForm.getElementsByTagName("select");
      for(var i = 0; i < selectTags.length; i++) {
        selectTags[i].selectedIndex = 0;
      }

      inputTags = myForm.getElementsByTagName("input:not(type=submit)");
      for( i = 0; i < inputTags.length; i++) {
        inputTags[i].value = '';
      }
    }
  });
})(jQuery);