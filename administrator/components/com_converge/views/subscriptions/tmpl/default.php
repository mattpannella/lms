<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 8/10/16
 * Time: 4:36 PM
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet('components/com_axs/assets/css/cpanels.css', array('version' => 'auto'));
$doc->addScript('components/com_converge/assets/js/subscriptions_export.js', array('version' => 'auto'));
$doc->addScript('../components/shared/includes/js/export_csv.js', array('version' => 'auto'));
$doc->addScript('components/com_converge/assets/js/pagination.js', array('version' => 'auto'));
$doc->addScript('components/com_converge/assets/js/transactions.js', array('version' => 'auto'));

$model = $this->getModel();
$limit = $this->input->get('count', 50);
$page = $this->input->get('page', 0);
$settings = $model->getSettings();
$data = $model->getData($limit, $page, $settings);

$title = new stdClass;
$title->subscription = "Subscription";
$title->user_id = "User Id";
$title->name = "Name";
$title->contact = "Contact";
$title->plan = "Plan";
$title->reccurring_amount = "Recurring Amount";
$title->status = "Status";
$title->length = "Period Length";
$title->card = "Card";
$title->points = "Points to Use";

function formatPhone($number) {
	if (preg_match('~.*(\d{1})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', $number)) {
		$ret = preg_replace('~.*(\d{1})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '+$1 ($2) $3-$4', $number);
	} else if (preg_match('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', $number)) {
		$ret = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '+1 ($1) $2-$3', $number);
	} else {
		$ret = $number;
	}
	return $ret;
}

function formatStatus($status) {
	switch ($status) {
		case 'ACT':
			return 'Active';
		case 'INAC':
			return 'Inactive';
		case 'GRC':
			return 'Grace Period';
		default:
			return $status;
	}
}

function formatLength($period_length) {
	$length = substr($period_length, 0, 1);
	$term = substr($period_length, -1);
	$postfix = $length > 1 ? 's' : '';
	switch($term) {
		case 'D':
			return $length . ' Day' . $postfix;
		case 'M':
			return $length . ' Month' . $postfix;
		case 'Y':
			return $length . ' Year' . $postfix;
		default:
			return $length . ' Unit' . $postfix;
	}
}


?>

<style>
	.icon-checkmark:before {
		color: #fff;
	}
</style>

<form
	id="filters"
	action="<?php echo $_SERVER['REQUEST_URI']; ?>"
	method="post"
	class="col-md-6 p-3 d-flex"
>
	<select name="filter-subscription-plan" form="filters" class="input-medium mx-1">
		<option value="0">--select plan--</option>
		<?php $plans = AxsPayment::getAllPlans(); if(count($plans)) {
			foreach($plans as $p) { ?>
				<option value="<?php echo $p->id; ?>"><?php echo $p->title; ?></option>
			<?php } ?>
		<?php } ?>
	</select>
	<select name="filter-subscription-status" form="filters" class="input-large mx-1">
		<option value="0">--select status--</option>
		<option value="1" <?php if($settings->status == 1) echo "selected"; ?>>Active</option>
		<option value="2" <?php if($settings->status == 2) echo "selected"; ?>>Inactive</option>
		<option value="3" <?php if($settings->status == 3) echo "selected"; ?>>Grace Period</option>
	</select>
	<div class="input-group">
		<input
			name="filter-subscription-userid"
			form="filters"
			class="form-input d-inline"
			type="text"
			value="<?php echo $settings->user_id != 0 ? $settings->user_id : ''; ?>"
			placeholder="User Id"
		/>
		<input
			type="submit"
			form="filters"
			class="btn btn-primary form-input d-inline"
			name="filter"
			value="Filter"
		/>
	</div>
</form>

<div id="masterControls" class="col-md-6 p-3 d-flex">
	<div class="input-group">
		<input
			id="pagination_refresh"
			class="btn btn-sm btn-primary float-end"
			type="button"
			value="Set Limit"
		/>
		<select id="pagination_select">
			<option value="" disabled>--how many to show--</option>
			<option value="20">20</option>
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="250">250</option>
		</select>
	</div>

	<div class="float-end">
		<button
			id="exportCSVButton"
			class="btn btn-primary ms-2"
			onclick='exportCSV(<?php echo $data->count . ',' . json_encode($title); ?>);'
		>Export CSV</button>
	</div>
</div>

<script>
	jQuery('#toolbar').append(jQuery('#filters'));
	jQuery('#toolbar').append(jQuery('#masterControls'));
</script>

<div class="row totals">
	<div class="col-md-4"></div>
	<div class="col-md-4 totals-status rounded mb-3" style="background-color: #20ae24">
		<div class="totals-inner">
			<div class="totals-icon" style="margin-top: 5px;"><i class="icon-checkmark"></i></div>
			<div class="totals-data"><?php echo $data->act; ?></div>
			<div class="totals-footer">
				<i class="icon-bars pull-left totals-stats"></i>
				Active Users
			</div>
		</div>
		<div class="totals-lower">
			<hr>
			<p><i class="icon-checkmark"></i> <?php echo $data->act; ?> Active</p>
			<p><i class="icon-warning"></i> <?php echo $data->grc; ?> Grace Period</p>
			<p><i class="icon-remove"></i> <?php echo $data->inac; ?> Inactive</p>
		</div>
	</div>
	<div class="col-md-4"></div>
</div>

<form name="adminForm" id="adminForm" action="index.php" method="post">
	<table class="table table-striped">
		<tr>
			<th><?php echo $title->subscription; ?></th>
			<th><?php echo $title->user_id; ?></th>
			<th><?php echo $title->name; ?></th>
			<th><?php echo $title->contact; ?></th>
			<th><?php echo $title->plan; ?></th>
			<th><?php echo $title->reccurring_amount; ?></th>
			<th><?php echo $title->status; ?></th>
			<th><?php echo $title->length; ?></th>
			<th><?php echo $title->card; ?></th>
			<th><?php echo $title->points; ?></th>
		</tr>

		<?php foreach($data->subscriptions as $s) { ?>
			<tr class="<?php if($s->status == 'GRC') echo 'warning'; elseif($s->status == 'INAC') echo 'error'; ?>">
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $s->user_id; ?>&sub_id=<?php echo $s->id; ?>#timeline">View</a></td>
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $s->user_id;?>#timeline"><?php echo $s->user_id; ?></a></td>
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $s->user_id;?>#timeline"><?php echo $s->name; ?></a></td>
				<td><a href="mailto:<?php echo $s->email; ?>"><?php echo $s->email;?></a><br><?php echo "Phone: " . formatPhone($s->phone); ?></td>
				<td><?php echo $s->plan; ?></td>
				<td><?php echo sprintf("$%.2f", $s->original_amount); ?></td>
				<td><?php echo formatStatus($s->status); ?></td>
				<td><?php echo formatLength($s->period_length); ?></td>
				<td><?php echo $s->card ? $s->card : ''; ?></td>
				<td><?php echo $s->points; ?></td>
			</tr>
		<?php } ?>
	</table>

	<input type="hidden" name="option" value="com_converge" />
	<input type="hidden" name="view" value="subscriptions" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="" />
	<input type="hidden" name="hidemainmenu" value="" />
</form>
<script>
	set_page(<?php echo $limit . "," . $page ?>);
</script>

<div id="pagination_buttons" class="pagination pagination-toolbar" data-view="subscriptions" style="padding-left:16px;padding-top:16px;" rowCount="<?php echo $data->count ?>"></div>
