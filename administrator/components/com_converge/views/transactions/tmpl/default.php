<?php

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addScript('components/com_converge/assets/js/transactions.js', array('version' => 'auto'));
$doc->addStyleSheet('components/com_axs/assets/css/cpanels.css', array('version' => 'auto'));
$doc->addScript('components/com_converge/assets/js/transactions_export.js', array('version' => 'auto'));
$doc->addScript('../components/shared/includes/js/export_csv.js', array('version' => 'auto'));
$doc->addScript('components/com_converge/assets/js/pagination.js', array('version' => 'auto'));

$model = $this->getModel();
$limit = $this->input->get('count', 50);
$page = $this->input->get('page', 0);
$settings = $model->getSettings();
$data = $model->getData($limit, $page, $settings);

$title = new stdClass;
$title->transaction = "Transaction";
$title->user_id = "User Id";
$title->name = "Name";
$title->contact = "Contact";
$title->item = "Item";
$title->type = "Billing Type";
$title->amount = "Amount";
$title->card = "Card";
$title->status = "Status";
$title->date = "Date Paid";
$title->initiator = "Initiator";

$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}

function formatPhone($number) {
	if (preg_match('~.*(\d{1})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', $number)) {
		$ret = preg_replace('~.*(\d{1})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '+$1 ($2) $3-$4', $number);
	} else if (preg_match('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', $number)) {
		$ret = preg_replace('~.*(\d{3})[^\d]{0,7}(\d{3})[^\d]{0,7}(\d{4}).*~', '+1 ($1) $2-$3', $number);
	} else {
		$ret = $number;
	}
	return $ret;
}

function formatStatus($status) {
	switch ($status) {
		case 'PLHO':
			return 'Placeholder';
		case 'FAIL':
			return 'Failed';
		case 'DEC':
			return 'Declined';
		case 'SUC':
			return 'Successful';
		case "REF":
			return 'Refunded';
		default:
			return $status;
	}
}

function formatInitiator($i) {
	return array_search($i, AxsPayment::$initiators);
}

function formatType($t) {
	return array_search($t, AxsPayment::$trxn_types);
}

?>

<div id="masterMaster" class="container-fluid">
	<div class="row my-2 ">
		<div id="masterFilter" class="col-sm-12">
			<div class="input-group float-start">
				<span class="input-group-text">Filter Presets</span>
				<select name="filter-master" form="filters">
					<option value="0" <?php echo $settings->master == 0 ? 'selected' : ''; ?>>Unset</option>
					<option value="1" <?php echo $settings->master == 1 ? 'selected' : ''; ?>>Today's Automated</option>
					<option value="2" <?php echo $settings->master == 2 ? 'selected' : ''; ?>>Today's Manual</option>
				</select>
			</div>

			<button
				id="exportCSVButton"
				class="btn btn-success float-end"
				onclick='exportCSV(<?php echo $data->count . "," . json_encode($title); ?>);'
			>Export CSV</button>

			<div class="input-group float-end me-2">
				<span class="input-group-text">Show</span>
				<select id="pagination_select">
					<option value="20">20</option>
					<option value="50">50</option>
					<option value="100">100</option>
					<option value="250">250</option>
				</select>
				<input
					id="pagination_refresh"
					class="btn btn-primary"
					type="button"
					value="Apply"
				/>
			</div>
		</div>
	</div>

	<hr style="margin: 5px 0;">

	<form
		id="filters"
		action="<?php echo $_SERVER['REQUEST_URI']; ?>"
		method="post"
		class="row"
	>
		<div class="col-auto my-2">
			<div class="input-group">
				<span class="input-group-text">User</span>
				<select name="filter-initiator" form="filters">
					<option value="0">Any</option>
					<?php foreach(AxsPayment::$initiators as $k => $v) { ?>
						<option value="<?php echo $v; ?>" <?php echo $settings->initiator == $v ? 'selected' : ''; ?>><?php echo $k; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-auto my-2">
			<input
				name="filter-user-id"
				form="filters"
				value="<?php echo $settings->user_id != 0 ? $settings->user_id : ''; ?>"
				placeholder="User Id"
				type="text"
			/>
		</div>
		<div class="col-auto my-2">
			<?php
				echo JHtml::calendar(
					$settings->start,
					'start',
					'start',
					'%Y-%m-%d',
					$attribs = array(
						'class' => 'float-center',
						'label' => 'Start'
					)
				);
			?>
		</div>
		<div class="col-auto my-2">
			<?php
				echo JHtml::calendar(
					$settings->end,
					'end',
					'end',
					'%Y-%m-%d',
					$attribs = array(
						'class' => 'float-center',
						'label' => 'End'
					)
				);
			?>
		</div>
		<div class="col-auto my-2">
			<div class="input-group float-end">
				<span class="input-group-text">Billing Type</span>
				<select name="filter-transaction-type" form="filters">
					<option value="0">Any</option>
					<?php foreach(AxsPayment::$trxn_types as $k => $v) { ?>
						<option value="<?php echo $v; ?>" <?php echo $settings->transaction_type == $v ? 'selected' : ''; ?>><?php echo $k; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		<div class="col-auto my-2">
			<div class="input-group">
				<span class="input-group-text">Status</span>
				<select name="filter-transaction-status" form="filters">
					<option value="0" <?php echo $settings->transaction_status == 0 ? 'selected' : ''; ?>>Any</option>
					<option value="1" <?php echo $settings->transaction_status == 1 ? 'selected' : ''; ?>>Successful</option>
					<option value="2" <?php echo $settings->transaction_status == 2 ? 'selected' : ''; ?>>Placeholder</option>
					<option value="3" <?php echo $settings->transaction_status == 3 ? 'selected' : ''; ?>>Failed</option>
					<option value="4" <?php echo $settings->transaction_status == 4 ? 'selected' : ''; ?>>Declined</option>
					<option value="6" <?php echo $settings->transaction_status == 6 ? 'selected' : ''; ?>>Declined & Failed</option>
					<option value="5" <?php echo $settings->transaction_status == 5 ? 'selected' : ''; ?>>Refunded</option>
				</select>
			</div>
		</div>
		<div class="col-auto my-2">
			<input
				type="submit"
				name="filter"
				form="filters"
				value="Apply Filters"
				class="btn btn-primary"
			/>
		</div>
	</form>

</div>

<script>
	jQuery('#toolbar').append(jQuery('#masterMaster'));
</script>

<div class="container-fluid row totals my-4 align-center" style="max-width:100%">
	<div class="col-sm-3">
		<!-- empty column here to center inner columns on this row -->
	</div>
	<div class="col-sm-2 totals-count mx-2 rounded" style="background-color: #00a8e6">
		<div class="totals-inner">
			<div class="totals-icon" style="margin-top: 5px;"><?php echo number_format($data->count); ?></div>
			<div class="totals-footer"><i class="icon-chart"></i> Transactions</div>
		</div>
	</div>

	<div class="col-sm-2 totals-money mx-2 rounded" style="background-color: #20ae24">
		<div class="totals-inner">
			<div class="totals-icon" style="margin-top: 5px; margin-bottom:20px"><?php echo $currency_symbol.number_format($data->amountsuc); ?></div>
			<div class="totals-footer">
				<i class="icon-bars float-center totals-stats"></i>
				US Dollars
			</div>
		</div>
		<div class="totals-lower">
			<hr>
			<p><i class="icon-checkmark"></i> <?php echo $currency_symbol.number_format($data->amountsuc); ?> Successful</p>
			<p><i class="icon-warning"></i> <?php echo $currency_symbol.number_format($data->amountdec); ?> Declined</p>
			<p><i class="icon-remove"></i> <?php echo $currency_symbol.number_format($data->amountfail); ?> Failed</p>
			<p><i class="icon-pencil-2"></i> <?php echo $currency_symbol.number_format($data->amountplho); ?> Placeholder</p>
			<p><i class="icon-pencil-2"></i> <?php echo $currency_symbol.number_format($data->amountref); ?> Refunded</p>
		</div>
	</div>

	<div class="col-sm-2 totals-status mx-2 rounded" style="background-color: #e66834">
		<div class="totals-inner">
			<div class="totals-icon" style="margin-top: 5px;"><?php echo number_format($data->suc); ?></div>
			<div class="totals-footer">
				<i class="icon-pie"></i>
				Successful
			</div>
		</div>
		<div class="totals-lower">
			<hr>
			<p><i class="icon-checkmark"></i> <?php echo number_format($data->suc); ?> Successful</p>
			<p><i class="icon-warning"></i> <?php echo number_format($data->dec); ?> Declined</p>
			<p><i class="icon-remove"></i> <?php echo number_format($data->fail); ?> Failed</p>
			<p><i class="icon-pencil-2"></i> <?php echo number_format($data->plho); ?> Placeholder</p>
			<p><i class="icon-pencil-2"></i> <?php echo number_format($data->ref); ?> Refunded</p>
		</div>
	</div>

	<div class="col-sm-3">
		<!-- empty column here to center inner columns on this row -->
	</div>
</div>

<form name="adminForm" id="adminForm" action="index.php" method="post" style="padding-left:16px;padding-right:16px">
	<table class="table table-striped">
		<tr>
			<th><?php echo $title->transaction; ?></th>
			<th><?php echo $title->user_id; ?></th>
			<th><?php echo $title->name; ?></th>
			<th><?php echo $title->contact; ?></th>
			<th><?php echo $title->item; ?></th>
			<th><?php echo $title->type; ?></th>
			<th><?php echo $title->amount; ?></th>
			<th><?php echo $title->card; ?></th>
			<th><?php echo $title->status; ?></th>
			<th><?php echo $title->date; ?></th>
			<th><?php echo $title->initiator; ?></th>
		</tr>

		<?php foreach($data->transactions as $t) { ?>
			<tr class="<?php if($t->status == 'DEC') echo 'warning'; elseif($t->status == 'FAIL') echo 'error'; ?>">
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $t->user_id; ?>&trxn_id=<?php echo $t->id; ?>#timeline">View</a></td>
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $t->user_id;?>#timeline"><?php echo $t->user_id; ?></a></td>
				<td><a href="/administrator/index.php?option=com_users&view=axsuser&layout=edit&id=<?php echo $t->user_id;?>#timeline"><?php echo $t->name; ?></a></td>
				<td><a href="mailto:<?php echo $t->email; ?>"><?php echo $t->email;?></a><br><?php echo "Phone: " . formatPhone($t->phone); ?></td>
				<td><?php echo $t->item; ?></td>
				<td><?php echo formatType($t->type); ?></td>
				<td><?php echo $currency_symbol.$t->amount; ?></td>
				<td><?php echo $t->card ? $t->card : ''; ?></td>
				<td><?php echo formatStatus($t->status); ?></td>
				<td><?php echo AxsExtra::format_date($t->date, true); //true shows timestamp ?></td>
				<td><?php echo formatInitiator($t->initiator); ?></td>
			</tr>
		<?php } ?>
	</table>

	<input type="hidden" name="option" value="com_converge" />
	<input type="hidden" name="view" value="transactions" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="" />
	<input type="hidden" name="hidemainmenu" value="" />
</form>
<script>
	set_page(<?php echo $limit . "," . $page ?>);
</script>

<div id="pagination_buttons" class="pagination pagination-toolbar" data-view="transactions" style="padding-left:16px;padding-top:16px;" rowCount="<?php echo $data->count ?>"></div>
