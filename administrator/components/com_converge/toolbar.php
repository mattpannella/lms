<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class ConvergeToolbar extends FOFToolbar {
    function onTransactionsBrowse() {
        JToolBarHelper::title(JText::_('COM_CONVERGE_TITLE_TRANSACTIONS'), 'CONVERGE');
    }

    function onSubscriptionsBrowse() {
        JToolbarHelper::title(JText::_('COM_CONVERGE_TITLE_SUBSCRIPTIONS'), 'CONVERGE');
    }

    function onPlansBrowse() {
        JToolBarHelper::title(JText::_('COM_CONVERGE_TITLE_PLANS'), 'CONVERGE');
        JToolbarHelper::addNew();
        JToolbarHelper::publishList();
        JToolbarHelper::unpublishList();
        JToolbarHelper::deleteList();
        JToolBarHelper::save2copy('copy', 'Duplicate Subscription Plan(s)');
    }

    function onPlansEdit() {
        JToolbarHelper::apply('apply');
		JToolbarHelper::save('save');
		JToolbarHelper::save2new('savenew');
		JToolbarHelper::save2copy('copy', 'Duplicate Subscription Plan');
		JToolbarHelper::cancel();
    }

    function onCancellation_requestsBrowse() {
        JToolbarHelper::title(JText::_('COM_CONVERGE_TITLE_CANCELLATION_REQUESTS'), 'CONVERGE');
    }

    function onFailed_signupsBrowse() {
        JToolbarHelper::title(JText::_('COM_CONVERGE_TITLE_FAILED_SIGNUPS'), 'CONVERGE');
        JToolbarHelper::deleteList();
    }
}