<?php

defined('_JEXEC') or die();

class ConvergeControllerSubscriptions extends FOFController {

	//TODO: integrate with rewards system.
	function renewSubscription() {
		$user_id = $this->input->get('user_id', 0);
		$sub_id = $this->input->get('sub_id', 0);
		$desc = $this->input->get('desc', 0);
		$time = $this->input->get('time', 0);
		$dateopt = $this->input->get('dateopt', 0);

		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;

		$subscription = AxsPayment::newSubscription($subParams);
		$subscription->getSubscriptionWithId(false);
		$length = substr($subscription->period_length, 0, -1);
		$term = substr($subscription->period_length, -1);

		$start = date("Y-m-d H:i:s");

		if ($time && $dateopt) {
			//make regular subscription period and charge now
			switch($term) {
				case 'M':
					$end = new DateTime();
					for($i = 0; $i < $length; $i++) {
						$end = AxsExtra::getDayNextMonth($end);
					}
					$end = $end->format("Y-m-d H:i:s");
					break;
				case 'D':
					$end = (new DateTime())->modify("+$length day")->format("Y-m-d H:i:s");
					break;
				case 'Y':
					$end = (new DateTime())->modify("+$length year")->format("Y-m-d H:i:s");
			}
			$status = 'NONE';

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = $status;
			$subPeriodParams->user_id = $user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();

			//make charge and get status
			$amount = $subscription->original_amount;

			$cardParams = AxsPayment::getCardParams();
			$cardParams->id = $subscription->card_id;
			$card = AxsPayment::newCard($cardParams);
			$card->getCardWithId();

			$trxnParams = AxsPayment::getTransactionParams();

			$trxnParams->user_id = $user_id;
	        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
	        $trxnParams->type_id = $new_period->id;
	        $trxnParams->amount = $amount;
	        $trxnParams->date = date("Y-m-d H:i:s");
	        $trxnParams->card = $card;
	        $trxnParams->initiator = AxsPayment::$initiators['admin'];
	        $trxnParams->description = $desc;

			$trxn = AxsPayment::newTransaction($trxnParams);
			$trxn->charge();
			if($trxn->status == "SUC") {
				$new_period->status = "PAID";
				$new_period->save();

				$subscription->status = "ACT";
				$subscription->cancel = null;
				$subscription->save();
			}
		} else if ($time && !$dateopt) {
			//figure out how much to charge them and make charge.
			$end = new DateTime($this->input->get('charge_date', '0'));

			$per_day_amount = 0;
			switch($term) {
				case 'M':
					$days_per_month = 30.44;  //average based on 365.2425 days per year.
					$per_day_amount = $subscription->original_amount / ($days_per_month * $length);
					break;
				case 'D':
					$per_day_amount = $subscription->original_amount / $length;
					break;
				case 'Y':
					$days_per_year = 365.2425;
					$per_day_amount = $subscription->original_amount / ($days_per_year * $length);
			}
			$days_to_charge = (new DateTime())->diff(new DateTime($end))->format("%a");
			$amount = $days_to_charge*$per_day_amount;

			$end = $end->format("Y-m-d H:i:s");
			$status = 'NONE';

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = $status;
			$subPeriodParams->user_id = $user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();

			//make charge and get status
			$cardParams = AxsPayment::getCardParams();
			$cardParams->id = $subscription->card_id;
			$card = AxsPayment::newCard($cardParams);
			$card->getCardWithId();

			$trxnParams = AxsPayment::getTransactionParams();

			$trxnParams->user_id = $user_id;
	        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
	        $trxnParams->type_id = $new_period->id;
	        $trxnParams->amount = $amount;
	        $trxnParams->date = date("Y-m-d H:i:s");
	        $trxnParams->card = $card;
	        $trxnParams->initiator = AxsPayment::$initiators['admin'];
	        $trxnParams->description = $desc;

			$trxn = AxsPayment::newTransaction($trxnParams);
			$trxn->charge();
			if($trxn->status == "SUC") {
				$new_period->status = "PAID";
				$new_period->save();

				$subscription->status = "ACT";
				$subscription->cancel = null;
				$subscription->save();
			}
		} else if (!$time && $dateopt) {
			//make a period with regular term_length but set status to PRO1
			switch($term) {
				case 'M':
					$end = new DateTime();
					for($i=0; $i<$length; $i++) {
						$end = AxsExtra::getDayNextMonth($end);
					}
					$end = $end->format("Y-m-d H:i:s");
					break;
				case 'D':
					$end = (new DateTime())->modify("+$length day")->format("Y-m-d H:i:s");
					break;
				case 'Y':
					$end = (new DateTime())->modify("+$length year")->format("Y-m-d H:i:s");
			}
			$status = 'PRO1';

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = $status;
			$subPeriodParams->user_id = $user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();

			$subscription->status = "ACT";
			$subscription->cancel = null;
			$subscription->save();
		} else if (!$time && !$dateopt) {
			if($this->input->get('status', 0)) {
				$status = "PRO1";
			} else {
				$status = "COMP";
			}

			$end = new DateTime($this->input->get('charge_date', 0));
			$end = $end->format("Y-m-d H:i:s");

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = $status;
			$subPeriodParams->user_id = $user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();

			$subscription->status = "ACT";
			$subscription->cancel = null;
			$subscription->save();
		}

		$action_type = 23;
		AxsExtra::setAdminAction($action_type, $user_id, $sub_id, $this->input->get('reason', '', 'STRING'));

		echo 'success';
	}

	function pauseSubscription() {
		$sub_id = $this->input->get('sub_id', 0);

		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$subscription = AxsPayment::newSubscription($subParams);

		$subscription->getSubscriptionWithId(false); //don't need to load the cards for this one.

		//we need to grab the last sub period and start a new one until the pause_until date.
		$period = $subscription->periods[count($subscription->periods)-1];

		$end = (new DateTime($this->input->get('pause_until', '0')));

		$period_end = new DateTime($period->end);
		if($period_end > $end) {
			echo "Cannot select a start up date that is before the end of the current Subscription period.\nEnd date must be after " . $period_end->format('M d Y') . ".";
		} else {
			$start = $period->end;
			$end = $end->format("Y-m-d H:i:s");

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = 'PAUS';
			$subPeriodParams->user_id = $subscription->user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();
			//if they are already in grace period, we need to remove their usergroups right away
			if ($subscription->status == "GRC") {
				$plan = $subscription->getPlan();
				$user = JFactory::getUser($subscription->user_id);
				if(!is_array($plan->active_usergroup)) {
					$activeUserGroup = array($plan->active_usergroup);
				} else {
					$activeUserGroup = $plan->active_usergroup;
				}
				$user->groups = array_diff($user->groups, $activeUserGroup);
				$user->save();
			}
			$subscription->pause();

			$action_type = 22;
			AxsExtra::setAdminAction($action_type, $user_id = $this->input->get('user_id', 0), $sub_id, $this->input->get('reason', '', 'STRING'));

			echo 'success';
		}
	}

	function cancelSubscription() {
		$sub_id = $this->input->get('sub_id', 0);
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$subscription = AxsPayment::newSubscription($subParams);
		$subscription->getSubscriptionWithId(false);
		//if they are already in grace period, we need to remove their usergroups right away
		if($subscription->status == "GRC") {
			$plan = $subscription->getPlan();
			$user = JFactory::getUser($subscription->user_id);
			if(!is_array($plan->active_usergroup)) {
				$activeUserGroup = array($plan->active_usergroup);
			} else {
				$activeUserGroup = $plan->active_usergroup;
			}
			$user->groups = array_diff($user->groups, $activeUserGroup);
			$user->save();
		} else if($subscription->status == "PAUS") {
			//if they are paused and they want to cancel, we need to remove the paus period first.
			$period = $subscription->periods[count($subscription->periods)-1];
			$period->delete();
		}
		$subscription->cancel();

		$action_type = 11;
		AxsExtra::setAdminAction($action_type, $this->input->get('user_id', '0'), $sub_id, $this->input->get('reason', '', 'STRING'));

		echo 'success';
	}

	function adjustPayDate() {
		$sub_id = $this->input->get('sub_id', 0);
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$subscription = AxsPayment::newSubscription($subParams);
		$subscription->getSubscriptionWithId(false); //don't need to load the cards for this one.
		//(new DateTime($this->input->get('cancel_till', '0')))->format("Y-m-d H:i:s");
		//we need to grab the last sub period and start a new one till the pause_until date.
		$period = $subscription->periods[count($subscription->periods)-1];

		$end = (new DateTime($this->input->get('extend_until', '0')));
		if((new DateTime($period->end))>$end) {
			echo 'Invalid charge date.';
		} else {
			$start = $period->end;
			$end = $end->format("Y-m-d H:i:s");
			$user_id = $this->input->get('user_id', '0');
			$status = $this->input->get('status', '0') ? 'PRO1' : 'COMP';

			$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
			$subPeriodParams->sub_id = $sub_id;
			$subPeriodParams->start = $start;
			$subPeriodParams->end = $end;
			$subPeriodParams->status = $status;
			$subPeriodParams->user_id = $user_id;

			$new_period = AxsPayment::newSubscriptionPeriod($subPeriodParams);
			$new_period->save();

			$action_type = 24;
			AxsExtra::setAdminAction($action_type, $user_id, $sub_id, $this->input->get('reason', '', 'STRING'));

			echo 'success';
		}
	}

	function saveSubscriptionSettings() {
		$length = $this->input->get('length', '0');
		$term = $this->input->get('term', '0');
		$period_length = $length . $term;

		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $this->input->get('sub_id', '0');
		$subParams->user_id = $this->input->get('user_id', '0');
		$subParams->original_amount = $this->input->get('original_amount', '0');
		$subParams->period_length = $period_length;
		$subParams->card_id = $this->input->get('card_id', '0');
		$subParams->points = $this->input->get('points', '0');

		$subscription = AxsPayment::newSubscription($subParams);
		$subscription->save();

		$action_type = 13;
		AxsExtra::setAdminAction($action_type, $user_id, $card_id, $this->input->get('reason', '', 'STRING'));

		echo 'success';
	}

	function reprocessSubPeriod() {
		$sub_per_id = $this->input->get('sub_per', '0');
		$sub_id = $this->input->get('sub_id', '0');
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$sub = AxsPayment::newSubscription($subParams);
		$sub->getSubscriptionWithId(false);
		$plan = $sub->getPlan();

		$dispatcher = JEventDispatcher::getInstance();

		$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
		$subPeriodParams->id = $sub_per_id;

		$sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);
		$sp->getSubPeriodWithId();

		if ($sp->status == "NONE") {
			$amount = $sub->original_amount;

			//lets see if we can subtract from this with the rewards points
			$rewards = $dispatcher->trigger("onReprocessCalcPoints", array(&$sub->user_id, &$amount, &$sub->points));
			$rewards = $rewards[0];
			if($rewards) {
				$amount = $rewards->total;
			}

			if($amount > 0) {
				$card = 0;
				if ($sub->card_id) {
					$cardParams = AxsPayment::getCardParams();
					$cardParams->id = $sub->card_id;
					$card = AxsPayment::newCard($cardParams);
					$card->getCardWithId();
					//if they don't own the card and they aren't part of the family, try to just grab the first card available to them.
					if($card->user_id != $sub->user_id && !in_array($sub->user_id, $card->family_access)) {
						$cards = AxsPayment::getUserAllCards($sub->user_id);
						$card = $cards[0];
					}
				} else {
					//if they have no card set to use for this sub, try to just grab the first card available to them.
					$cards = AxsPayment::getUserAllCards($sub->user_id);
					$card = $cards[0];
				}

				//if there is still no charge-able card
				if(!$card) {
					echo 'No card on file that can be used.';
				}

				$trxnParams = AxsPayment::getTransactionParams();

				$trxnParams->user_id = $sub->user_id;
		        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
		        $trxnParams->type_id = $sp->id;
		        $trxnParams->amount = $amount;
		        $trxnParams->date = date("Y-m-d H:i:s");
		        $trxnParams->card = $card;
		        $trxnParams->initiator = AxsPayment::$initiators['admin'];
		        $trxnParams->description = $plan->title;

				//make charge and get status
				$trxn = AxsPayment::newTransaction($trxnParams);
				$trxn->charge();
				if($trxn->status == "SUC") {
					$sp->status = "PAID";
					$sp->save();
					//if the sub period is for the current time, set the status to active for the sub.
					$now = new DateTime();
					if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
						$sub->status = "ACT";
						$sub->save();
					}
					echo 'success';
					$dispatcher->trigger("onReprocessSuccess", array(&$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
				} else {
					echo 'error';
				}
			} else {
				$sp->status = "PAID";
				$sp->save();
				//if the sub period is for the current time, set the status to active for the sub.
				$now = new DateTime();
				if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
					$sub->status = "ACT";
					$sub->save();
				}
				echo 'success';
				$dispatcher->trigger("onReprocessSuccess", array(&$sub->user_id, &$amount, &$rewards->pointsToUse, &$plan->title));
			}
		} else {
			echo 'we don\'t support reprocessing prorated yet.';
		}

		$action_type = 8;
		AxsExtra::setAdminAction($action_type, $sub->user_id, $sub_per_id, $this->input->get('reason', '', 'STRING'));
	}

	function addSubscription() {
		$user_id = $this->input->get('user_id', 0);
		$plan_id = $this->input->get('plan_id', 0);
		$card_id = $this->input->get('card_id', 0);
		$initial_amount = $this->input->get('initial_amount', 0);
		$regular_amount = $this->input->get('regular_amount', 0);
		$now = date("Y-m-d H:i:s");

		//create the subscription

		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->user_id = $user_id;
		$subParams->plan_id = $plan_id;
		$subParams->original_amount = $regular_amount;
		$subParams->card_id = $card_id;

		$sub = AxsPayment::newSubscription($subParams);

		//jDump($sub);
		//grab plan and use it to fill in missing info.
		$plan = $sub->getPlan();
		$sub->period_length = $plan->default_sub_length;

		//create the subscription period
		if ($plan->default_sub_length) {
			$length = substr($sub->period_length, 0, -1);
			$term = substr($sub->period_length, -1);
			switch($term) {
				case 'M':
					$end = new DateTime();
					for($i=0; $i<$length; $i++) {
						$end = AxsExtra::getDayNextMonth($end);
					}
					$end = $end->format("Y-m-d H:i:s");
					break;
				case 'D':
					$end = (new DateTime())->modify("+$length day")->format("Y-m-d H:i:s");
					break;
				case 'Y':
					$end = (new DateTime())->modify("+$length year")->format("Y-m-d H:i:s");
			}
		} else {
			$end = date("Y-m-d H:i:s"); //lifetime membership
		}

		$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

		$subPeriodParams->start = $now;
		$subPeriodParams->end = $end;
		$subPeriodParams->user_id = $user_id;
		$sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);

		//create the card
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $card_id;
		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId();

		if($initial_amount > 0) {
			//create the trxn
			$desc = $plan->title;
			$trxnParams = AxsPayment::getTransactionParams();

			$trxnParams->user_id = $user_id;
	        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
	        //$trxnParams->type_id = $new_period->id;
	        $trxnParams->amount = $initial_amount;
	        $trxnParams->date = $now;
	        $trxnParams->card = $card;
	        $trxnParams->initiator = AxsPayment::$initiators['admin'];
	        $trxnParams->description = $desc;

			$trxn = AxsPayment::newTransaction($trxnParams);
			$trxn->charge(false); //so we dont save trxn automatically
			if($trxn->status == "SUC") {
				$sub->status = "ACT";
				$sub->save();

				$sp->sub_id = $sub->id;
				$sp->status = "PAID";
				$sp->save();

				$trxn->type_id = $sp->id;
				$trxn->save();

				//give them the new usergroups that come from this plan.
				$user = JFactory::getUser($user_id);

				if (!is_array($plan->active_usergroup)) {
					$activeUserGroup = array($plan->active_usergroup);
				} else {
					$activeUserGroup = $plan->active_usergroup;
				}

				$groups = array();
				if (is_array($user->groups)) {
					$groups = array_merge($groups,$user->groups);
				}

				if (is_array(json_decode($plan->usergroups))) {
					$groups = array_merge($groups,json_decode($plan->usergroups));
				}

				if (is_array($activeUserGroup) && $activeUserGroup ) {
					$groups = array_merge($groups,$activeUserGroup);
				}

				$user->groups = array_unique($groups);
				$user->save();

				//give them the lms course and community groups
				$dispatcher = JEventDispatcher::getInstance();
				$dispatcher->trigger('onSubscriptionPlay', array(&$sub));

				$action_type = 25;
				AxsExtra::setAdminAction($action_type, $user_id, $sub->id, $this->input->get('reason', '', 'STRING'));

				echo 'success';
			} else {
				//instead save failed signup
				$model = $this->getThisModel();

				$message = 'Failed to charge your card. Error: '.$trxn->message;
				$trxn->type_id = $model->saveFailedSubscription($user_id, 0, 0, 0, 0, 0, 0, 0, 0, 0, $plan_id, $message);
				$trxn->save();

				$action_type = 25;
				AxsExtra::setAdminAction($action_type, $user_id, $trxn->type_id, $this->input->get('reason', '', 'STRING'));

				echo 'error';
			}
		} else {
			$sub->status = "ACT";
			$sub->save();

			$sp->sub_id = $sub->id;
			$sp->status = "PAID";
			$sp->save();

			//give them the new usergroups that come from this plan.
			$user = JFactory::getUser($user_id);
			if(!is_array($plan->active_usergroup)) {
				$activeUserGroup = array($plan->active_usergroup);
			} else {
				$activeUserGroup = $plan->active_usergroup;
			}
			$user->groups = array_unique(($user->groups + json_decode($plan->usergroups)) + $activeUserGroup);
			$user->save();

			//give them the lms course and community groups
			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onSubscriptionPlay', array(&$sub));

			$action_type = 25;
			AxsExtra::setAdminAction($action_type, $user_id, $sub->id, $this->input->get('reason', '', 'STRING'));

			echo 'success';
		}
	}

	function markPaidSubPeriod() {

		$subPeriodParams = AxsPayment::getSubscriptionPeriodParams();
		$subPeriodParams->id = $this->input->get('sub_perid', 0);
		$sp = AxsPayment::newSubscriptionPeriod($subPeriodParams);
		$sp->getSubPeriodWithId();

		$sub_id = $this->input->get('sub_id', 0);
		$subParams = AxsPayment::getSubscriptionParams();
		$subParams->id = $sub_id;
		$sub = AxsPayment::newSubscription($subParams);
		$sub->getSubscriptionWithId(false);

		$type = $this->input->get('payment_type', 0);

		$trxnParams = AxsPayment::getTransactionParams();

		$trxnParams->user_id = $sub->user_id;
        $trxnParams->type = AxsPayment::$trxn_types['subscription'];
        $trxnParams->type_id = $sub_per_id;
        $trxnParams->amount = $sub->original_amount;
        $trxnParams->date = date("Y-m-d H:i:s");
        $trxnParams->status = "SUC";
        $trxnParams->initiator = AxsPayment::$initiators['admin'];

        $trxn = AxsPayment::newTransaction($trxnParams);

		switch($type) {
			case 1:
				$trxnParams->trxn_id = "Check Payment";
				$trxn->save();
				$sp->status = "PAID";
				break;
			case 2:
				$trxnParams->trxn_id = "Cash Payment";
				$trxn->save();
				$sp->status = "PAID";
				break;
			case 3:
				$trxnParams->trxn_id = "Money Order Payment";
				$trxn->save();
				$sp->status = "PAID";
				break;
			case 4:
				$sp->status = "COMP";
				break;
		}
		$sp->save();
		//if the sub period is for the current time, set the status to active for the sub.
		$now = new DateTime();
		if(new DateTime($sp->start) < $now && $now < new DateTime($sp->end)) {
			$sub->status = "ACT";
			$sub->save();
		}

		echo 'success';

		//store admin action
		$action_type = 26;
		AxsExtra::setAdminAction($action_type, $sub->user_id, $sub_per_id, $this->input->get('reason', '', 'STRING'));
	}

	function export_to_csv() {
		$csv = $this->input->get('csv', 0);
		$titles = $this->input->get('titles', '', 'STRING');

		//make sure needed data was sent correctly
		if (!$csv || $csv != 1 || !$titles) {
			return;
		}

		$titles = json_decode($titles);

		$model = $this->getThisModel();
		$settings = $model->getSettings();
		$dataObject = $model->getData(null, null, $settings);

		$databaseCount = $dataObject->count;
		$rowData = $dataObject->subscriptions;

		// Create all the titles
		$titleArray = array();
		$count = 0;
		foreach($titles as $title) {
			$titleArray[$count++] = $title;
		}

		$data = array();
		$data[0] = array();
		for ($j = 0; $j < count($titleArray); $j++) {
			$data[0][$j] = $titleArray[$j];
		}

		$count = 1;
		foreach($rowData as $row) {
			$contact = "None";

			if ($row->phone) {
				$contact = "Phone: " . $row->phone;
				if ($row->email) {
					$contact .= " - Email: " . $row->email;
				}
			} else if ($row->email) {
				$contact = "Email: " . $row->email;
			}

			$status = "";
			switch ($row->status) {
				case 'ACT':
					$status = 'Active';
					break;
				case 'INAC':
					$status = 'Inactive';
					break;
				case 'GRC':
					$status = 'Grace Period';
					break;
			}

			$data[$count] = array();
			$data[$count][0] = $row->id;
			$data[$count][1] = $row->user_id;
			$data[$count][2] = $row->name;
			$data[$count][3] = $contact;
			$data[$count][4] = $row->plan;
			$data[$count][5] = $row->original_amount;
			$data[$count][6] = $status;
			$data[$count][7] = $row->period_length;
			$data[$count][8] = $row->card_id;
			$data[$count][9] = $row->points;

			$count++;
		}

		echo json_encode($data);
	}
}