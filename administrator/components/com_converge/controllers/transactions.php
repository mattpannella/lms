<?php

defined('_JEXEC') or die();

class ConvergeControllerTransactions extends FOFController {

    function getCards () {
        $user_id = $this->input->get('user_id', 0);
        $cards = AxsPayment::getUserAllCards($user_id);
        echo json_encode($cards);
    }

    function export_to_csv() {
        $csv = JRequest::getVar('csv');
        $titles = JRequest::getVar('titles');

        if (!isset($csv)) {
            return;
        } else {
            if ($csv != 1) {
                return;
            }
        }

        if (!isset($titles)) {
            return;
        } else {
            $titles = json_decode($titles);
        }

        $model = $this->getModel('Transactions');

        $settings = $model->getSettings();

        $dataObject = $model->getData(null, null, $settings);


        $databaseCount = $dataObject->databaseCount;
        $rowData = $dataObject->transactions;

        $titleArray = array();

        $count = 0;
        foreach($titles as $title) {
            $titleArray[$count++] = $title;
        }

        /*
            Create all the titles
        */

        $data = array();
        $data[0] = array();
        for ($j = 0; $j < count($titleArray); $j++) {
            $data[0][$j] = $titleArray[$j];
        }

        $count = 1;
        foreach($rowData as $row) {

            /*
                0   -   Transaction
                1   -   User Id
                2   -   Name
                3   -   Contact
                4   -   Item
                5   -   Transaction Type
                6   -   Amount
                7   -   Card
                8   -   Status
                9   -   Date Paid
                10  -   Initiator
            */

            $contact = "None";

            if ($row->phone) {
                $contact = "Phone: " . $row->phone;
                if ($row->email) {
                    $contact .= " - Email: " . $row->email;
                }
            } else if ($row->email) {
                $contact = "Email: " . $row->email;
            }

            $transaction_type = array_search($row->type, AxsPayment::$trxn_types);

            switch ($row->status) {
                case 'PLHO':
                    $transaction_status = 'Placeholder';
                case 'FAIL':
                    $transaction_status = 'Failed';
                case 'DEC':
                    $transaction_status = 'Declined';
                case 'SUC':
                    $transaction_status = 'Successful';
                case "REF":
                    $transaction_status = 'Refunded';
                default:
                    $transaction_status = $row->status;
            }

            $transaction_initiator = array_search($row->initiator, AxsPayment::$initiators);

            $data[$count] = array();
            $data[$count][0] = $row->id;
            $data[$count][1] = $row->user_id;
            $data[$count][2] = $row->name;
            $data[$count][3] = $contact;
            $data[$count][4] = $row->item;
            $data[$count][5] = $transaction_type;
            $data[$count][6] = $row->amount;
            $data[$count][7] = $row->card;
            $data[$count][8] = $transaction_status;
            $data[$count][9] = $row->date;
            $data[$count][10] = $transaction_initiator;

            $count++;

        }

        echo json_encode($data);
    }


    function purchaseCourse() {
	    $user_id = $this->input->get('user_id', 0);
	    $reason = $this->input->get('reason', 0, "STRING");
	    $course_id = $this->input->get('course_id', 0);
	    $amount = $this->input->get('amount', 0);
	    $card_id = $this->input->get('card', 0);

        $courseExists = AxsLMS::getCourseById($course_id);
        if (!$courseExists) {
            echo "failed to purchase course - course with id $course_id doesn't exist";
            return;
        }

        $cardParams = AxsPayment::getCardParams();
        $cardParams->id = $card_id;
        $card = AxsPayment::newCard($cardParams);
	    $card->getCardWithId();

        $courseParams = AxsPayment::getCoursePurchaseParams();
        $courseParams->user_id = $user_id;
        $courseParams->course_id = $course_id;
        $courseParams->date = date('Y-m-d H:i:s');

        $cp = AxsPayment::newCoursePurchase($courseParams);
	    $course = $cp->getCourse();

        if ($amount > 0) {

            $trxnParams = AxsPayment::getTransactionParams();

            $trxnParams->user_id = $user_id;
            $trxnParams->type = AxsPayment::$trxn_types['course'];
            $trxnParams->amount = $amount;
            $trxnParams->date = date("Y-m-d H:i:s");
            $trxnParams->card = $card;
            $trxnParams->initiator = AxsPayment::$initiators['admin'];
            $trxnParams->description = $course->title;

            $trxn = AxsPayment::newTransaction($trxnParams);

            $trxn->charge(false);
            if($trxn->status == "SUC") {
                $cp->status = "PAID";
                //send to auto alerts
                $alertParams = new stdClass();
                $alertParams->user_id = $user_id;
                $alertParams->course_id = $course_id;
                $alertParams->amount = $amount;
                $alertParams->date = date("m-d-Y");
                $alerts = new AxsAlerts($alertParams);
                $alerts->runAutoAlert('course purchase',$alertParams);
                echo "success";
            } else {
                $cp->status = "NONE";
                if($trxn->reasonCode) {
                    echo $trxn->reasonCode;
                } else {
                    echo $trxn->message;
                }

            }

            $cp->save();
            $trxn->type_id = $cp->id;
            $trxn->save();
        } else {
            $cp->status = "PAID";
            $cp->save();
            echo "success";
        }

        $action_type = 6;
        AxsExtra::setAdminAction($action_type, $user_id, $cp->id, $reason);
    }

    function refundPayment() {
        $trxn_id = $this->input->get('trxn_id', 0);

        $trxnParams = AxsPayment::getTransactionParams();
        $trxnParams->id = $trxn_id;

	    $trxn = AxsPayment::newTransaction($trxnParams);
	    $trxn->getTrxnWithId();

        if($trxn->refund()) {
	        echo 'success';

		    if ($trxn->type == AxsPayment::$trxn_types['subscription']) {

                $subPeriodParams = AxsPayment::getSubscriptionPeriodParams();

                $subPeriodParams->id = $trxn->type_id;
                $subPeriodParams->status = "NONE";

		      	$sp = AxsPayment::newSubscriptionPeriod($subPeriodParams); //set the status of the sub period we refunded
			    $sp->save();
            }
        } else {
	      	echo 'error';
        }

	    $action_type = 7;
	    AxsExtra::setAdminAction($action_type, $trxn->user_id, $trxn->id, $this->input->get('reason', 0, 'STRING'));
    }
}