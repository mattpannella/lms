<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 8/22/16
 * Time: 11:47 AM
 */

defined('_JEXEC') or die();

class ConvergeControllerPlans extends FOFController {
	public function remove() {
		$model = $this->getThisModel();
		if (!$model->getId()) {
			$model->setIDsFromRequest();
		}
		$plan_ids = $model->getIds();
		$leave_plan_ids = array();

		//make sure that no users are subscribed to these plans.
		$plans = implode(', ', $plan_ids);
		$db = JFactory::getDbo();
		$db->setQuery("SELECT plan_id, COUNT(*) as count FROM `axs_pay_subscriptions` WHERE plan_id IN ($plans) AND status != 'INAC' GROUP BY plan_id;");
		$planSubscriptions = $db->loadObjectList();
		foreach($planSubscriptions as $ps) {
			if($ps->count > 0) {
				//let them know why the plan can't be deleted
				JFactory::getApplication()->enqueueMessage("Unable to delete plan with id: $ps->plan_id. There are $ps->count users with this plan.", "error");
				$leave_plan_ids[] = $ps->plan_id;
			}
		}

		//delete these mofos
		$to_delete = array_diff($plan_ids, $leave_plan_ids);

		foreach($to_delete as $planId) {
			$item = AxsPayment::getPlanById($planId);
			$params = new stdClass();
			$params->eventName = 'Subscription Plan Delete';
			$params->description = $item->title.' ['.$item->id.']';
			$params->action_type ="admin";
			AxsActions::storeAdminAction($params);
		}

		if(count($to_delete)) {
			$plans = implode(', ', $to_delete);
			$db = JFactory::getDbo();
			$db->setQuery("DELETE FROM axs_pay_subscription_plans WHERE id IN ($plans);");
			$db->execute();
		}

		//now redirect them back to list view
		$url = JRoute::_('index.php?option=com_converge&view=plans', false);
		JFactory::getApplication()->redirect($url);
	}

	public function publish() {
		$model = $this->getThisModel();
		$model->setIDsFromRequest();
		$ids = implode(',', $model->getIds());

		$db = $model->getDbo();

		$query = $db->getQuery(true);

		$query->update($model->getTable()->getTableName())
			  ->set('published = 1')
			  ->where("id IN ($ids)");

		$db->setQuery($query);
		$db->execute();

		//now redirect them back to list view
		$url = JRoute::_('index.php?option=com_converge&view=plans', false);
		JFactory::getApplication()->redirect($url);
	}

	public function unpublish() {
		$model = $this->getThisModel();
		$model->setIDsFromRequest();
		$ids = implode(',', $model->getIds());

		$db = $model->getDbo();

		$query = $db->getQuery(true);

		$query->update($model->getTable()->getTableName())
			  ->set('published = 0')
			  ->where("id IN ($ids)");

		$db->setQuery($query);
		$db->execute();

		//now redirect them back to list view
		$url = JRoute::_('index.php?option=com_converge&view=plans', false);
		JFactory::getApplication()->redirect($url);
	}
}