<?php
/**
* Created by PhpStorm.
* User: mar
* Date: 12/29/15
* Time: 4:25 PM
*/
defined('_JEXEC') or die;

class ConvergeControllerHelper extends FOFController {
    
    public function deleteCard() {
    	$cardParams = AxsPayment::getCardParams();
    	$cardParams->token = $this->input->get('token', '0');
        $card = AxsPayment::newCard($cardParams);

        if($card->deTokenize()) {
            echo 'success';
        } else {
            echo 'fail';
        }
    }
}