<?php

defined('_JEXEC') or die();

class ConvergeControllerCards extends FOFController {

	//pull the card data from converge and return it.
	public function updateCard() {
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', '0');

		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId(); //fetch the other card data
		if($card->fetchTokenizedData()) {
			echo json_encode($card);
		} else {
			echo 'error';
		}
	}

	//send the card data back to converge with updated info.
	public function updateCardSave() {
		
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', 0);

		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId(); //fetch the other card data

		$card->cardNumber = $this->input->get('cardNumber', 0);
		$card->expire = str_pad($this->input->get('exp_month', 0), 2, '0', STR_PAD_LEFT) . substr($this->input->get('exp_year', 0), 2, 2);
		$card->cvv = $this->input->get('cvv', 0);
		$card->first = $this->input->get('first', 0, 'STRING');
		$card->last = $this->input->get('last', 0, 'STRING');
		$card->address = $this->input->get('address', 0, 'STRING');
		$card->zip = $this->input->get('zip', 0, 'STRING');
		$card->city = $this->input->get('city', 0, 'STRING');
		$card->state = $this->input->get('state', 0, 'STRING');
		$card->country = $this->input->get('country', 0, 'STRING');
		$card->phone = $this->input->get('phone', 0, 'STRING');

		$result = $card->updateTokenizedData();
		if($result['result']) {
			echo 'success';
		} else {
			echo 'error';
		}

		$action_type = 2;
		AxsExtra::setAdminAction($action_type, $card->user_id, $card->id, $this->input->get('reason', '', 'STRING'));
	}

	public function saveCard() {
		$brand = AxsBrands::getBrand();
		$cardParams = AxsPayment::getCardParams();

		if($brand->billing->gateway_type == 'stripe') {
			$cardParams->user_id = $this->input->get('user_id', '0');
			$cardParams->stripeToken = $this->input->get('stripeToken', 0, 'STRING');
			$cardParams->cardNumber = $this->input->get('last4', 0);
			$user = JFactory::getUser($cardParams->user_id);
			$cardParams->email = $user->email;
			$cardParams->description = $user->name;
		} else {
			$cardParams->firstName = $this->input->get('first', 0, 'STRING');
	        $cardParams->lastName = $this->input->get('last', 0, 'STRING');
	        $cardParams->cardNumber = $this->input->get('cardNumber', 0);
	        $cardParams->expire = str_pad($this->input->get('exp_month', 0), 2, '0', STR_PAD_LEFT) . substr($this->input->get('exp_year', 0), 2, 2);
	        $cardParams->cvv = $this->input->get('cvv', 0);
	        $cardParams->address = $this->input->get('address', 0, 'STRING');
	        $cardParams->zip = $this->input->get('zip', 0, 'STRING');
	        $cardParams->city = $this->input->get('city', 0, 'STRING');
	        $cardParams->state = $this->input->get('state', 0, 'STRING');
	        $cardParams->country = $this->input->get('country', 0, 'STRING');
	        $cardParams->phone = $this->input->get('phone', 0, 'STRING');
	        $cardParams->user_id = $this->input->get('user_id', '0');
		}		

		$card = AxsPayment::newCard($cardParams);
		if($card->tokenize()) {
			$card->save();
			echo 'success';
		} else {
			echo 'error';
		}

		$action_type = 4;
		AxsExtra::setAdminAction($action_type, $cardParams->user_id, $card->token, $this->input->get('reason', '', 'STRING'));
	}

	public function deleteCard() {
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', '0');
		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId(); //fetch the other card data
		if($card->deTokenize()) {
			echo 'success';
		} else {
			echo 'error';
		}

		$action_type = 3;
		AxsExtra::setAdminAction($action_type, $card->user_id, $card->cardNumber, '');
	}

	public function editCardPermissions() {
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', '0');
		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId();
		echo json_encode($card);
	}

	public function editCardPermissionsSave() {
		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', '0');
		$card = AxsPayment::newCard($cardParams);
		$card->getCardWithId();
		//parse form data
		$permissions = array();
		parse_str($_POST['formData'], $permissions);
		$perms = array();
		foreach($permissions as $key => $val) {
			//key is the family member's id
			$perms[] = $key;
		}
		$card->family_access = $perms;
		$card->save();
		echo 'success';

		$action_type = 9;
		AxsExtra::setAdminAction($action_type, $card->user_id, $card->id, '');
	}

	public function updateToken() {

		$cardParams = AxsPayment::getCardParams();
		$cardParams->id = $this->input->get('card_id', '0');
		$cardParams->token = $this->input->get('token', '0');
		$cardParams->cardNumber = substr($cardParams->token, -4);

		$card = AxsPayment::newCard($cardParams);
		$card->save();
		echo 'success';

		$action_type = 21;
		AxsExtra::setAdminAction($action_type, $card->user_id, $card->cardNumber, '');
	}
}