<?php
	defined('_JEXEC') or die;

	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);

	if (!JFactory::getUser()->authorise('core.admin')) {
		return;
	}

	class CreatePermissionsSetup {

		private static $permission_list = null;

		public static function dispatch() {

			$db = JFactory::getDBO();
			$query = "SELECT * FROM axs_permissions_admin";			
			$db->setQuery($query);
			$results = $db->loadObjectList();

			self::$permission_list = array();

			foreach ($results as $result) {
				self::$permission_list[$result->id] = $result;
			}

			$menu = AxsSideNav::getItems(true);
			/*$menu_items = array();
			foreach ($menu as $item) {
				$menu_items[$item->id] = $item;
			}
			jDump($menu_items);
			*/

			self::nextLevel($menu);

			//$permissions = $_SESSION['admin_access_levels']->permissions;
			//self::nextLevel($permissions);
		}

		private static function nextLevel($menu, $parent = null) {
			?>

			<div>

				<?php
					if ($parent) {
						$parent_array = explode("-", $parent);
					}

					foreach ($menu as $menu_item) {
						$id = $menu_item->id;
						?>
						<div>
							<?php

								if (is_array($menu_item->function)) {
									$last = false;
								} else {
									$last = true;
								}

								if ($parent) {
									$tag = "{$parent}-{$id}";
								} else {
									$tag = $id;
								}

								if (!$last) {
									self::nextLevel($menu_item->function, $tag);
								} else {
									foreach ($parent_array as $p) {
										if ($p) {
										?>
												<span class="title_box"><?php echo $p; ?></span>
											<?php
										}
									}

									?> 
										<span class="title_box"> 
											<?php 
												echo $id;
											?> 
										</span>

									<?php
								}
							?>
						</div>
						<?php
							if ($last) {
								//jDump(self::$permission_list["axsusers-groups"], $tag);

								$option = ltrim(rtrim(self::$permission_list[$tag]->option));
								$view = ltrim(rtrim(self::$permission_list[$tag]->view));
								$extension = ltrim(rtrim(self::$permission_list[$tag]->extension));

								

						?>
								<div class="input_area">
									<table>
										<tr>
											<td>Options</td>
											<td>Views</td>
											<td>Extensions</td>
										</tr>
										<tr>
											<td>
												<textarea class="input_data" type="option" owner="<?php echo $tag; ?>"><?php echo $option; ?></textarea>
											</td>
											<td>
												<textarea class="input_data" type="view" owner="<?php echo $tag; ?>"><?php echo $view; ?></textarea>
											</td>
											<td>
												<textarea class="input_data" type="extension" owner="<?php echo $tag; ?>"><?php echo $extension; ?></textarea>
											</td>
										</tr>
									</table>
								</div>
						<?php
							}
					}
				?>
			</div>
			<?php
		}
	}

	CreatePermissionsSetup::dispatch();

?>

<style>
	.title_box {
		border: 1px solid #ccc;
		padding: 5px;
		background-color: #eee;
		border-radius: 3px;
		font-size: 18px;
	}

	.input_area {
		margin: 8px 0px;		
	}
</style>

<script>	
	jQuery(document).ready(
		function() {
			jQuery(".button-apply").each(
				function() {
					//console.log(this);
					this.setAttribute("onclick","saveSetup()");
				}
			);
		}
	);

	function saveSetup() {
		var data = document.getElementsByClassName("input_data");
		//console.log(data.length);
		var save = {};
		for (var i = 0; i < data.length; i++) {
			var type = data[i].getAttribute("type");
			var owner = data[i].getAttribute("owner");
			var field = data[i].value;

			if (save[owner] === undefined) {
				save[owner] = [];
			}

			save[owner].push({
				"type": type,
				"data": field
			});
		}

		//console.log(save);

		jQuery.ajax({
            url: 'index.php?option=com_permissioning&task=permissioning.setupSave&format=raw',
            data: {
                save: save
            },
            type: 'post',
            success: function(result) {
            	if (result == 'complete') {
            		alert("Permission Settings Saved");
            	} else if (result == 'denied') {
            		alert("You don't have permission to save this.");
            	} else {
            		alert("There was an error. Please see the console log.");
            		console.log(result);
            	}
            }
        });
	}
</script>