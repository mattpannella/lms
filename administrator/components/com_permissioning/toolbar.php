<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class PermissioningToolbar extends FOFToolbar {
 	function onSetupsAdd() {
		JToolBarHelper::title('Set Up Permissioning');
		JToolbarHelper::apply('apply');
	}
}