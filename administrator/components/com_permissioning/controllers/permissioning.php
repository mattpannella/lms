<?php

defined('_JEXEC') or die();

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class PermissioningControllerPermissioning extends FOFController {

    public static function setupSave() {

        if (!JFactory::getUser()->authorise('core.admin')) {
            echo "denied";
            return;
        }

        $saveData = JRequest::getVar('save');

        $db = JFactory::getDBO();
        
        foreach ($saveData as $key => $data) {

            $option = "";
            $view = "";
            $extension = "";

            foreach ($data as $datum) {
                switch ($datum["type"]) {
                    case "option":
                        $option = $db->quote($datum["data"]);
                        break;
                    case "view":
                        $view = $db->quote($datum["data"]);
                        break;
                    case "extension":
                        $extension = $db->quote($datum["data"]);
                        break;
                }
            }

            $id = $db->quote($key);            

            $query = "
                INSERT INTO `axs_permissions_admin`
                    (`id`, `option`, `view`, `extension`)
                VALUES
                    ($id, $option, $view, $extension)
                ON DUPLICATE KEY UPDATE
                    `id` = $id,
                    `option` = $option,
                    `view` = $view,
                    `extension` = $extension
            ";

            //echo $query;

            $db->setQuery($query);
            $db->execute();            
        }

        echo "complete";
    }
}