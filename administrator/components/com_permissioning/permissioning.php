<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

defined('_JEXEC') or die();

/*
$user = JFactory::getUser();
if (!$user->authorise('core.admin')) {
	JError::raiseError('401', "You're not authorized to view this resource");
	return;
}
*/

$doc = JFactory::getDocument();
//$doc->addStylesheet( JURI::root(true) . '/administrator/components/com_splms/assets/css/splms.css' );

// Load FOF

include_once JPATH_LIBRARIES.'/fof/include.php';
if(!defined('FOF_INCLUDED')) {
	JError::raiseError ('500', 'FOF is not installed');
	
	return;
}

FOFDispatcher::getTmpInstance('com_permissioning')->dispatch();