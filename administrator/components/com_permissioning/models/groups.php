<?php

defined('_JEXEC') or die;

class PermissioningModelGroups extends FOFModel {
	public function save($data) {

		$data['access_level'] = json_encode($data['access_level']);
		$data['user_group'] = json_encode($data['user_group']);
		$data['updated'] = date("Y-m-d H:i:s");
		
		return parent::save($data);
	}

	public function loadFormData() {

		$data = $this->_formData;

		if (empty($data)) {
			return array();
		} else {

			$data['access_level'] = json_decode($data['access_level']);
			$data['user_group'] = json_decode($data['user_group']);

			return $data;
			
		}
	}
}