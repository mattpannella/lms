<?php
defined('JPATH_PLATFORM') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class FoFFormFieldAccess extends JFormField {

    //This is for the Permissioning Access view.

    private $permissions = null;
    private $rights = null;
    private $hasAdmin = false;

    public function getInput() {

        $id = JRequest::getVar('id');
        $db = JFactory::getDBO();

        $permissions = array();     //The access they are allowed to have on the back end of the site.
        $rights = array();          //The rights and priviledges they have to edit users.

        if ($id) {
            //Get the existing permissions for this, if it exists.
            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from('axs_permissions')
                ->where($db->quoteName('id') . '=' . $db->quote($id));

            $db->setQuery($query);
            $result = $db->loadObject();

            //If it exists, decode it and build the permissions and rights objects.
            if ($result) {
                $menu_settings = json_decode($result->permissions);
                foreach ($menu_settings as $setting) {
                    if (isset($setting->system)) {
                        $permissions[$setting->system] = $setting->allowed;
                    } else if (isset($setting->access)) {
                        foreach ($setting->access as $access) {
                            $rights[$access->action] = $access->allowed;
                        }
                    }
                }
            }
        }

        $this->permissions = $permissions;
        $this->rights = $rights;

        //Get a copy of the full dashboard menu.  If this is a sub-admin, they will only get the menu they have access to.
        //As such, they cannot give permission to anything that is above their permissioning level.
        $menu_items = AxsSideNav::getItems();

        $list = self::buildList($menu_items);

        $affects_user = false;          //Whether or not the user is editing a permissioning level that affects them.  Doesn't affect admin

        $user_id = JFactory::getUser()->id;
        if (AxsUser::isAdmin($user_id) || AxsUser::isSuperUser($user_id) || AxsUser::isDeveloper($user_id)) {
            //This user has full administrator priviledges.
            $this->hasAdmin = true;

        } else {

            switch ($result->type) {
                case "accesslevel":

                    $user_access_levels = $access_levels = JFactory::getUser()->getAuthorisedViewLevels();
                    $access_levels = json_decode($result->access_level);
                    foreach ($access_levels as $access) {
                        if (in_array($access, $user_access_levels)) {
                            $affects_user = true;
                        }
                    }

                    break;

                case "userid":

                    $user_list = explode(",", $result->user_list);
                    if (in_array(JFactory::getUser()->id, $user_list)) {
                        $affects_user = true;
                    }

                    break;

                case "usergroup":

                    $users_user_groups = JUserHelper::getUserGroups(JFactory::getUser()->id);
                    $user_groups = json_decode($result->user_group);

                    foreach ($user_groups as $group) {
                        if (in_array($group, $users_user_groups)) {
                            $affects_user = true;
                        }
                    }

                    break;
            }
        }

        ob_start();

        ?>
            <style>

                .entry_section {
                    width: 35%;
                    float: left;
                    min-width: 300px;
                    margin-left: 2%;
                    margin-top: 10px;
                }

                .edits_section {
                    padding: 8px;
                }

                .menu_entry {
                    width: 100%;
                    min-width: 275px;
                    padding: 8px;
                    cursor: pointer;
                }

                .edit_entry {
                    /*width: 100%;
                    min-width: 300px;
                    padding: 8px;*/
                    cursor: pointer;

                }

                .main_category_name {
                    font-weight: bold;
                }

                .header_name {
                    font-weight: bold;
                    color: #0d6efd;
                }

                /*.secondary_category_name {
                    margin-left: 30px;
                    cursor: pointer;
                }*/

                /* .selected {
                    background-color: rgb(200,255,200);
                }

                .unselected {
                    background-color: rgb(255,200,200);
                } */

                .user_alert {
                    padding: 30px;
                    background-color: rgb(255,200,200);
                    font-size: 18px;
                    border: red;
                    border-radius: 8px;
                    margin: 10px 0px;
                }

                .warning_header {
                    font-weight: bold;
                }

                .entry_title {
                    font-size: 18px;
                    padding: 5px;
                    font-weight: bold;
                    border-bottom: 1px solid #dddddd;
                    padding-bottom: 10px;
                    margin-bottom: 20px;
                }

            </style>

            <?php
                if ($affects_user) {
                    //If the permission setting affects the current user, give them the warning.
            ?>
                    <div class="user_alert">
                        <div class="warning_header">
                            <center>
                                <?php echo JText::_('COM_PERMISSIONING_AFFECTS_USER_WARNING');?>
                            </center>
                        </div>
                        <div class="warning_text">
                            <?php echo JText::_('COM_PERMISSIONING_AFFECTS_USER_WARNING_TEXT');?>
                        </div>
                    </div>
            <?php
                }
            ?>

            <div class="controls">
                <input id="permissions" type="hidden" name="permissions" value='<?php echo json_encode($menu_settings);?>'/>
            </div>

            <div style="width: 100%">
                <div class="entry_section">
                    <div class="entry_title">
                        Access Permissions
                        <div menu='primary' class='select_all btn btn-primary btn-sm float-end' action="all" style="margin-bottom: 8px;">
                            Select All
                        </div>
                    </div>

                    <?php
                        //Creates the HTML for the left side column (the access to the menus)
                        self::buildPrimaryPermissions($list);
                    ?>
                </div>

                <?php
                    if ($this->hasAdmin) {
                ?>
                        <div class="entry_section">
                            <div class="entry_title">
                                Editing Permissions
                                <div menu='secondary' class='select_all btn btn-primary btn-sm float-end' action="all" style="margin-bottom: 8px;">
                                    Select All
                                </div>
                            </div>
                            <?php
                                //Creates the HTML for the right side column (user editing permissions)
                                self::buildSecondaryPermissions();
                            ?>
                        </div>
                <?php
                    }
                ?>
            </div>

            <script>
                //Create a javascript variable from PHP
                var affects_user = <?php
                    if ($affects_user) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                ?>;
            </script>

            <script>

                //saveInterval = null;
                var menu_entries = document.getElementsByClassName("menu_entry");
                var edit_entries = document.getElementsByClassName("edit_entry");

                var propagate_click = true;

                //Clicking on a user permissions setting button.
                //This allows the user to either click on the name area, or the checkbox
                jQuery(".edit_entry").click(
                    function(e) {
                        var checkbox = this.getElementsByTagName("input")[0];
                        var selected;
                        //These classes highlight the selected area
                        if (jQuery(this).hasClass("edits_selected")) {
                            jQuery(this).removeClass("edits_selected");
                            jQuery(this).addClass("edits_unselected");
                            selected = false;
                        } else {
                            jQuery(this).removeClass("edits_unselected");
                            jQuery(this).addClass("edits_selected");
                            selected = true;
                        }

                        checkbox.checked = selected;
                    }
                );

                //Clicking on a menu permissions setting button.
                //This allows the user to either click on the name area, or the checkbox

                jQuery(".menu_entry").click(
                    function(e) {

                        var item_id = this.id;
                        var parent_id = this.getAttribute("parent");
                        var depth = parseInt(this.getAttribute("depth"));

                        var checkbox = this.getElementsByTagName("input")[0];
                        var selected;

                        if (jQuery(this).hasClass("selected")) {
                            jQuery(this).removeClass("selected");
                            jQuery(this).addClass("unselected");
                            selected = false;
                        } else {
                            jQuery(this).removeClass("unselected");
                            jQuery(this).addClass("selected");
                            selected = true;
                        }

                        checkbox.checked = selected;

                        if (propagate_click) {
                            //Find all entries that have a deeper depth, and have this entry as a parent.  Check or uncheck them accordingly.

                            for (var i = 0; i < menu_entries.length; i++) {
                                var entry = menu_entries[i];
                                var entry_depth = entry.getAttribute("depth");
                                if (entry_depth > depth) {
                                    var entry_parent = entry.getAttribute("parent");
                                    if (entry_parent == item_id) {

                                        //Only click it if it does not match the state of the current one.
                                        var entry_select = entry.getElementsByTagName("input")[0].checked;

                                        if (entry_select == !selected) {
                                            entry.click();
                                        }
                                    }
                                }
                            }
                        }
                    }
                );

                //The Select All button at the top.
                jQuery('.select_all').click(
                    function() {
                        var action = this.getAttribute("action");
                        var menu = this.getAttribute("menu");
                        var check = false;

                        if (action == "all") {
                            this.setAttribute("action", "none");
                            this.innerHTML = "Select None";
                            check = true;
                        } else {
                            this.setAttribute("action", "all");
                            this.innerHTML = "Select All";
                            check = false;
                        }

                        if (menu == "primary") {
                            //turn off click propagation so that each one can be clicked on individually, or else clicking on a parent
                            //will click on all the children, and then this will click them again.
                            propagate_click = false;
                            jQuery(menu_entries).each(
                                function() {
                                    if (this.getElementsByTagName("input")[0].checked != check) {
                                        this.click();
                                    }
                                }
                            );
                            propagate_click = true;
                        } else if (menu == "secondary") {
                            jQuery(edit_entries).each(
                                function() {
                                    if (this.getElementsByTagName("input")[0].checked != check) {
                                        this.click();
                                    }
                                }
                            );
                        }
                    }
                );

                function savePermissions() {
                    //When the save button is clicked, save all of the data to a field that wil be passed to the database.
                    var selected = [];
                    var access = [];
                    jQuery(menu_entries).each(
                        function() {

                            var id = this.id;
                            var checked = this.getElementsByTagName("input")[0].checked;

                            selected.push({
                                "system": id,
                                "allowed": checked
                            });
                        }
                    );

                    jQuery(edit_entries).each(
                        function() {
                            var id = this.id;
                            var checked = this.getElementsByTagName("input")[0].checked;

                            access.push({
                                "action": id,
                                "allowed": checked
                            })
                        }
                    );

                    selected.push({
                        "access": access
                    });

                    var permissions = document.getElementById("permissions");
                    permissions.value = JSON.stringify(selected);
                }

                jQuery(".checkbox").click(
                    function(e) {
                        jQuery(this.parentNode).click();
                        e.stopPropagation();
                    }
                );

                jQuery(document).ready(
                    function() {
                        jQuery('button').each(
                            function() {
                                var onclick = this.getAttribute("onclick");

                                if (onclick) {

                                    //format is Joomla.submitButton('apply');
                                    //Splitting on the ' puts the action into index 1
                                    var action = onclick.split("'")[1];


                                    this.removeAttribute("onclick");
                                    this.setAttribute("action", action);

                                    jQuery(this).click(
                                        function() {
                                            switch (action) {
                                                case "cancel":
                                                    Joomla.submitbutton(action);
                                                    break;

                                                default:
                                                    if (affects_user) {
                                                        if (confirm("<?php echo JText::_('COM_PERMISSIONING_AFFECTS_USER_WARNING_TEXT_SHORT'); ?>")) {
                                                            savePermissions();
                                                            Joomla.submitbutton(action);
                                                        }
                                                    } else {
                                                        savePermissions();
                                                        Joomla.submitbutton(action);
                                                    }
                                            }
                                        }
                                    );
                                }
                            }
                        );
                    }
                );

            </script>


        <?php
        return ob_get_clean();

    }

    private function buildList($items, $parent_id = null) {
        $list = array();
        //The menu items list is a little much for what we need.  This is a simple, stripped down version.

        foreach ($items as $item) {
            if (!$item) {
                continue;
            }

            if ($parent_id) {
                $id = $parent_id . "-" . $item->id;
            } else {
                $id = $item->id;
            }

            $new = new stdClass();
            $new->name = $item->name;
            $new->id = $id;
            $new->type = $item->type;

            if (is_array($item->function)) {
                //Recursively go through all the children functions.
                $new->data = self::buildList($item->function, $id);
            }

            $list []= $new;
        }

        return $list;
    }

    private function buildPrimaryPermissions($items, $depth = 0, $parent_id = null) {

        //Build the HTML for the left side menu permissions.  The $items will be the administrator menu.
        $permissions = $this->permissions;

        foreach ($items as $item) {

            $active = $permissions[$item->id];

            if ($active) {
                $selected = "selected";
                $checked = "checked";
            } else {
                $selected = "unselected";
                $checked = "";
            }

            ?>
                <div
                    id="<?php echo $item->id; ?>"
                    parent="<?php echo $parent_id;?>"
                    class="menu_entry <?php echo $selected; if (!$parent_id || $item->type == "header") { echo " main_category_name"; } if ($item->type == "header") { echo " header_name"; } ?>"
                    depth="<?php echo $depth;?>"
                >

                    <input class="checkbox" type="checkbox" <?php echo $checked; ?>/>
                    <span style="margin-left: <?php echo $depth * 16 + 7; ?>px;">
                        <?php echo $item->name; ?>
                    </span>
                </div>
            <?php

            if (isset($item->data)) {
                self::buildPrimaryPermissions($item->data, $depth + 1, $item->id);
            }
        }
    }

    private function buildSecondaryPermissions() {

        //Create the HTML for the right side user permissions settings.

        if (!$this->hasAdmin) {
            //Only admins get this.
            return;
        }

        $rights_list = array(
            "edit_users" => true,
            "create_users" => true,
            "delete_users" => true,
            "edit_usergroup" => true,
            "edit_self" => true,
            "edit_admin" => true,
            "create_admin" => true,
            "delete_admin" => true,
            "edit_subadmin" => true,
            "create_subadmin" => true,
            "delete_subadmin" => true,
        );

        if (!$this->rights) {
            //None have been set yet.
            $this->rights = $rights_list;
        } else {
            //This refreshes the list in case new options have been added since the last time it was saved.
            foreach ($rights_list as $key => $right) {
                if (isset($this->rights[$key])) {
                    $rights_list[$key] = $this->rights[$key];
                }
            }

            $this->rights = $rights_list;
        }

        ?>
            <div class="edits_section">

                <?php
                    foreach ($this->rights as $key => $allowed) {

                        if ($allowed) {
                            $selected = "edits_selected";
                            $checked = "checked";
                        } else {
                            $selected = "edits_unselected";
                            $checked = "";
                        }
                ?>
                        <div id="<?php echo $key; ?>" class="edit_entry mb-4 <?php echo $selected;?>">
                            <input class="checkbox" type="checkbox" <?php echo $checked;?>>
                            <span style="margin-left:8px">
                                <?php
                                    switch ($key) {
                                        case "edit_users":
                                            $title = "Can Edit Users";
                                            break;
                                        case "create_users":
                                            $title = "Can Create New Users";
                                            break;
                                        case "delete_users":
                                            $title = "Can Delete Users";
                                            break;
                                        case "edit_usergroup":
                                            $title = "Can Edit User Groups";
                                            break;
                                        case "edit_self":
                                            $title = "Can Edit Own Account";
                                            break;
                                        case "edit_admin":
                                            $title = "Can Edit Site Administrators";
                                            break;
                                        case "create_admin":
                                            $title = "Can Grant/Revoke Site Administrator priviledges";
                                            break;
                                        case "delete_admin":
                                            $title = "Can Delete Site Administrators";
                                            break;
                                        case "edit_subadmin":
                                            $title = "Can Edit Sub-administrators";
                                            break;
                                        case "create_subadmin":
                                            $title = "Can Grant/Revoke Sub-administrator priviledges";
                                            break;
                                        case "delete_subadmin":
                                            $title = "Can Delete Sub-administrators";
                                            break;
                                    }

                                    echo $title;
                                ?>
                            </span>
                        </div>
                <?php
                    }
                ?>
            </div>

        <?php
    }

    public function getRepeatable() {

    }
}