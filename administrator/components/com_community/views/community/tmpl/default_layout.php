<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<script>
(function( root ) {
    root.joms || (root.joms = {});
    root.joms.jQuery = jQuery;
})( window );
</script>

<!-- pickadate.js -->
<link rel="stylesheet" href="<?php echo COMMUNITY_BASE_ASSETS_URL; ?>/pickadate/themes/classic.combined.css" />
<style>.picker__select--month{width:35% !important}.picker__select--year{width:22.5% !important}</style>
<script src="<?php echo COMMUNITY_BASE_ASSETS_URL; ?>/pickadate/picker.combined.js"></script>

<script>
    jQuery(() => {
        var notificationButton = $('#notificationButton');
        $('#toolbar').append(notificationButton);
        notificationButton.show();
    });
    
</script>

<div id="js-cpanel">
    <div id="notificationButton" class="btn-wrapper" style="display:none">
        <button class="btn btn-sm btn-secondary dropdown-toggle" href="#" data-bs-toggle="dropdown">
            <?php echo JText::_('COM_COMMUNITY_CONFIGURATION_NOTIFICATIONS')?>
            <span class="badge bg-primary ms-1"><?php echo $this->total; ?></span> <!-- show the total reports -->
        </button>
    <?php if($this->total) { ?>
        <ul class="dropdown-menu" style="top: inherit !important;left: inherit !important;margin-top: inherit !important;">
            <!-- Don't show the empty report -->
            <?php if($this->pendingGroup){?>
            <li>
                <a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_community&view=groups&status=0')?>">
                    <div class="clearfix">
                        <span class="float-start">
                            <?php echo JText::_('COM_COMMUNITY_GROUPS_PENDING')?>
                        </span>
                        <div class="float-end ps-4"><?php echo $this->pendingGroup; ?></div>
                    </div>
                </a>
            </li>
            <?php }?>
            <?php if($this->pendingEvent){?>
            <li>
                <a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_community&view=events&status=0')?>">
                    <div class="clearfix">
                        <span class="float-start">
                            <?php echo JText::_('COM_COMMUNITY_EVENTS_PENDING')?>
                        </span>
                        <span class="float-end"><?php echo $this->pendingEvent; ?></span>
                    </div>
                </a>
            </li>
            <?php }?>
            <?php if($this->pendingUser){?>
            <li>
                <a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_community&view=users&usertype=jomsocial&status=1&usesearch=0'); ?>">
                    <div class="clearfix">
                        <span class="float-start">
                            <?php echo JText::_('COM_COMMUNITY_MEMBERS_PENDING')?>
                        </span>
                        <span class="float-end"><?php echo $this->pendingUser;?></span>
                    </div>
                </a>
            </li>
            <?php }?>
            <?php if($this->reportCount){ ?>
            <li>
                <a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_community&view=reports&status=0'); ?>">
                    <div class="clearfix">
                        <span class="float-start">
                            <?php echo JText::_('COM_COMMUNITY_REPORTS')?>
                        </span>
                        <span class="float-end"><?php echo $this->reportCount; ?></span>
                    </div>
                </a>
            </li>
            <?php }?>
            <?php if($this->unsendCount){?>
            <li>
                <a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_community&view=mailqueue&status=0')?>">
                    <div class="clearfix">
                        <span class="float-start text-danger">
                            <?php echo JText::_('COM_COMMUNITY_MAIL_UNSENT')?>
                        </span>
                        <span class="float-end"><?php echo $this->unsendCount; ?></span>
                    </div>
                </a>
            </li>
            <?php }?>
            <?php if($this->version){?>
            <li>
                <a class="dropdown-item" href="<?php echo (empty($this->versionUrl)) ? '#' : $this->versionUrl; ?>">
                    <div class="clearfix text-success">
                        <span class="float-start">
                            <?php echo JText::_('COM_COMMUNITY_NEW_VERSION')?>
                        </span>
                        <span class="float-end"><?php echo $this->version ?></span>
                    </div>
                </a>
            </li>
            <?php }?>
        </ul>
    <?php }?>
    </div>

    <div class="main-container container-fluid">
      <a class="menu-toggler" id="menu-toggler" href="#">
        <span class="menu-text"></span>
      </a>

      <div class="sidebar mb-4" id="sidebar" style="border:none;height:100%">

        <?php echo $this->getSideMenuHTML(); ?>

      </div>

      <div class="main-content">

        <div class="page-content">
            <div class="page-header clearfix no-padding">
                
            </div>
            <?php echo $this->pageContent; ?>
        </div><!--/.page-content-->

      </div><!--/.main-content-->
</div> <!-- #js-cpanel end -->

<script>

// Apply class to doc buttons @TODO: probably can be achieved with JButton
jQuery('span.icon-help').parent().addClass('btn-doc');

// joms.jQuery('ul.nav.ace-nav.float-end > li > a.dropdown-toggle').dropdown();

// joms.jQuery('div.pagination div.limit:first').remove();

<?php
// $jinput = JFactory::getApplication()->input;
// if($jinput->get('view')) {?>
// if (window.MooTools) (function($) {
// // fix for Bootstrap Tooltips - conflicting with mootools-more
//   $$('.js-tooltip, .hasTooltip').each(function (e) {
//     e.hide = null;
//   });
// })(MooTools);
</script>
