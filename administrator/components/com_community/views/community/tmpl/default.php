<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
// Disallow direct access to this file
defined('_JEXEC') or die('Restricted access');
?>

<style>
.demo-container, .demo-container-2 {
box-sizing: border-box;
width: 100%;
height: 150px;
}
.demo-container {margin-bottom:20px;}
.demo-placeholder, .demo-placeholder-2 {
width: 80%;
height: 150px;
float:left;
}

#choices, #choices-2 {
float:left;
width:20%;
}

.avg-avatar {
    width:56px;
    margin-right:6px;
}

/* User engagement */
.eng-loading {
    margin-left: 10px;
    margin-top: 10px;
}

	.tov-content-actionbar {
		padding: 0px !important;
	}
</style>

<!-- ijoomla logo -->


<!-- Infobox -->
<div class="row mb-4">
  <div class="col-xs-12 col-sm-6 col-lg-3">

    <div class="card text-white bg-primary text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-group me-2"></i><?php echo $this->userCount; ?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_USERS'); ?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-primary text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-envelope me-2"></i><?php echo $this->messageCount; ?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_MESSAGES')?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-primary text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-comment me-2"></i><?php echo $this->postCount?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_POSTS')?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-success text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-camera me-2"></i><?php echo $this->photoCount; ?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_PHOTOS')?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-success text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-film me-2"></i><?php echo $this->videosCount?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo Jtext::_('COM_COMMUNITY_VIDEOS');?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-warning text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-group me-2"></i><?php echo $this->groupsCount; ?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_GROUPS')?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-warning text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-calendar me-2"></i><?php echo $this->eventsCount?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_EVENTS')?></div>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-sm-6 col-lg-3">
    <div class="card text-white bg-danger text-center m-1">
      <div class="card-body">
        <h2 class="infobox-content count"><i class="js-icon-comments me-2"></i><?php echo $this->discussionCount; ?></h2>
      </div>
      <div class="card-footer">
        <div class="infobox-content"><?php echo JText::_('COM_COMMUNITY_DISCUSSIONS')?></div>
      </div>
    </div>
  </div>

</div><!--/.row-->

<!-- Engagement & statistic , most recent stream -->
<div class="row">
<div class="col-7">

<div class="card" id="statistic-box">
    <div class="card-header">
      <h5>
        <i class="js-icon-signal"></i>
        <?php echo JText::_('COM_COMMUNITY_USER_ENGAGEMENT_TITLE')?>
        <button class="btn float-end btn-primary" id="dropdown-time" data-time="week">
          <span><?php echo JText::_('COM_COMMUNITY_THIS_WEEK')?></span>
          <i class="js-icon-angle-down js-icon-on-right"></i>
        </button>
        <ul class="dropdown-menu dropdown-info float-end dropdown-caret" id="dropdown-time-menu" aria-labelledby="dropdown-time">
          <li class="active">
            <a class="dropdown-item" href="#time-week"><?php echo JText::_('COM_COMMUNITY_THIS_WEEK')?></a>
          </li>
          <li>
            <a class="dropdown-item" href="#time-lastweek"><?php echo JText::_('COM_COMMUNITY_LAST_WEEK')?></a>
          </li>
          <li>
            <a class="dropdown-item" href="#time-month"><?php echo JText::_('COM_COMMUNITY_THIS_MONTH')?></a>
          </li>
          <li>
            <a class="dropdown-item" href="#time-lastmonth"><?php echo JText::_('COM_COMMUNITY_LAST_MONTH')?></a>
          </li>
        </ul>
      </h5>
    </div>

    <div class="card-body">
      <div class="widget-main">

        <div class="row">
          <div class="col-12">

          <!-- Engagement chart -->

          <h5 class="lighter">
            <?php echo JText::_('COM_COMMUNITY_DASHBOARD_USER_ENGAGEMENT')?>
          </h5>

          <div class="widget-toolbar float-end the-chev">
            <a href="#" data-action="collapse">
              <i class="js-icon-chevron-up"></i>
            </a>
          </div>

            <ul class="nav nav-tabs" id="engagement-tabs" data-type="message">
              <li class="nav-item">
                <a class="nav-link active" data-bs-toggle="tab" data-bs-target="#eng-message" href="#eng-message"><?php echo JText::_('COM_COMMUNITY_STREAM')?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#eng-photo" href="#eng-photo"><?php echo JText::_('COM_COMMUNITY_PHOTOS')?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#eng-video" href="#eng-video"><?php echo JText::_('COM_COMMUNITY_VIDEOS')?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#eng-event" href="#eng-event"><?php echo JText::_('COM_COMMUNITY_EVENTS')?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-bs-toggle="tab" data-bs-target="#eng-group" href="#eng-group"><?php echo JText::_('COM_COMMUNITY_GROUPS')?></a>
              </li>
            </ul>

            <div class="demo-container p-4">
                <div class="tab-content">
                    <div class="tab-pane active" id="eng-message">
                        <div id="eng-message-plot" class="demo-placeholder"></div>
                        <div id="eng-message-choices" class="demo-choices"></div>
                    </div>
                    <div class="tab-pane" id="eng-photo">
                        <div id="eng-photo-plot" class="demo-placeholder"></div>
                        <div id="eng-photo-choices" class="demo-choices"></div>
                    </div>
                    <div class="tab-pane" id="eng-video">
                        <div id="eng-video-plot" class="demo-placeholder"></div>
                        <div id="eng-video-choices" class="demo-choices"></div>
                    </div>
                    <div class="tab-pane" id="eng-event">
                        <div id="eng-event-plot" class="demo-placeholder"></div>
                        <div id="eng-event-choices" class="demo-choices"></div>
                    </div>
                    <div class="tab-pane" id="eng-group">
                        <div id="eng-group-plot" class="demo-placeholder"></div>
                        <div id="eng-group-choices" class="demo-choices"></div>
                    </div>
                </div>
            </div>
          </div>

          <!-- Statistic chart -->
          <div class="row mt-4">
          <div class="col-12">
            <h5>
                <?php echo JText::_('COM_COMMUNITY_DASHBOARD_DATA_STATISTIC')?>
            </h5>

            <div class="demo-container-2 p-4">
              <div id="placeholder-2" class="demo-placeholder-2"></div>
              <div id="choices-2" ></div>
            </div>
          </div>

          </div>
        </div>

      </div><!--/widget-main-->
    </div><!--/card-body-->
    </div>
</div>

<!-- Most Recent Stream -->
<div class="col-5">

  <div class="card" id="recent-box">
    <div class="card-header">
      <h5>
        <i class="js-icon-th-list"></i>
        <?php echo JText::_('COM_COMMUNITY_MOST_RECENT')?>
      </h5>

      <div>
        <ul class="nav nav-tabs" id="recent-tab">
          <li class="nav-item">
            <a class="nav-link active" data-bs-toggle="tab" data-bs-target="#posts-tab" href="#posts-tab"><?php echo JText::_('COM_COMMUNITY_POSTS')?></a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="tab" data-bs-target="#comments-tab" href="#comments-tab"><?php echo JText::_('COM_COMMUNITY_COMMENTS')?></a>
          </li>

          <li class="nav-item">
            <a class="nav-link" data-bs-toggle="tab" data-bs-target="#members-tab" href="#members-tab"><?php echo JText::_('COM_COMMUNITY_MEMBERS')?></a>
          </li>
        </ul>
      </div>
    </div>

        <div class="card-body clearfix p-4">
            <div class="tab-content">
              <!-- Posts tab -->
              <div id="posts-tab" class="tab-pane active">
                <div class="dialogs">
                  <!-- Contain start here-->
                  <?php if(!empty($this->statusData)) { ?>
                      <?php foreach($this->statusData as $data) { ?>
                        <div class="itemdiv dialogdiv">
                          <div class="user">
                            <img alt="<?php echo $data->user->getDisplayName(); ?>" src="<?php echo $data->user->getThumbAvatar()?>"/>
                          </div>
                          <div class="body">
                            <div class="time">
                              <i class="icon-time"></i>
                              <span class="green"><?php echo  CTimeHelper::timeLapse(JDate::getInstance($data->created)); ?></span>
                            </div>
                            <div class="name">
                              <?php if(!empty($data->user->id)){?>
                                    <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=users&layout=edit&id=' . $data->user->id; ?>"><?php echo $data->user->getDisplayName(); ?></a>
                                <?php }?>
                            </div>
                            <div class="text"><?php
                              $title = ucfirst(trim($data->title));
                              if ($data->app !== 'events.attend') {
                                $title = JHtml::_('string.truncate', $title, 750);
                              }
                              echo $title;
                            ?></div>
                          </div>
                        </div>
                      <?php }?>
                    <?php } else {?>
                            <?php echo JText::_('COM_COMMUNITY_DASHBOARD_NO_POST')?>
                    <?php }?>
                   <!-- Contain End here-->
                  <div class="center cta-full">
                    <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=activities'; ?>">
                      <?echo JText::_('COM_COMMUNITY_DASHBOARD_SEE_ACTIVITIES')?> &nbsp;
                      <i class="js-icon-arrow-right"></i>
                    </a>
                  </div>
                </div>
              </div>
              <!-- Comments tab -->
              <div id="comments-tab" class="tab-pane">

                <div class="dialogs">
                  <!-- Contain start here-->
                  <?php if(!empty($this->comments)){?>
                      <?php foreach($this->comments as $comment) {?>
                        <div class="itemdiv dialogdiv">
                          <div class="user">
                            <img alt="<?php echo $comment->user->getDisplayName() ?>" src="<?php echo $comment->user->getThumbAvatar() ?>" />
                          </div>
                          <div class="body">
                            <div class="time">
                              <i class="icon-time"></i>
                              <span class="green"><?php echo  CTimeHelper::timeLapse(JDate::getInstance($comment->date)); ?></span>
                            </div>
                            <div class="name">
                              <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=users&layout=edit&id=' . $comment->user->id; ?>"><?php echo $comment->user->getDisplayName() ?></a>
                            </div>

                            <div class="text">
                              <i class="js-icon-quote-left"></i>
                                <?php echo JHtml::_('string.truncate', $comment->comment, 220); ?>
                            </div>
                          </div>
                        </div>
                      <?php }?>
                 <?php } else {?>
                        <?php echo JText::_('COM_COMMUNITY_DASHBOARD_NO_COMMENT')?>
                 <?php }?>
                  <!-- Contain End here-->

                  <!--div class="center cta-full">
                    <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=activities'; ?>">
                      See all Activities &nbsp;
                      <i class="js-icon-arrow-right"></i>
                    </a>
                  </div-->
                 </div>
              </div>
              <!-- Members tab -->
              <div id="members-tab" class="tab-pane clearfix">
                <?php foreach($this->latestMembers as $member ) { ?>
                  <div id="member-<?php echo $member->id?>"class="itemdiv memberdiv clearfix">
                    <div class="user">
                      <img alt="<?php echo $member->getDisplayName()?>" src="<?php echo $member->getThumbAvatar()?>" />
                    </div>

                    <div class="body">
                      <div class="name">
                        <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=users&layout=edit&id=' . $member->id; ?>"><?php echo $member->getDisplayName()?></a>
                      </div>

                      <div class="time">
                        <i class="js-icon-time"></i>
                        <span class="green"><?php echo CTimeHelper::timeLapse(JDate::getInstance($member->registerDate)); ?></span>
                      </div>

                      <div id="member-label-<?php echo $member->id?>">
                        <span class="label <?php echo $this->getLabelCss($member->memberstatus)?>"><?php echo $member->memberstatus?></span>
                        <?php if($member->memberstatus !=='approved') {?>
                          <div class="inline position-relative">
                            <button class="btn btn-minier bigger btn-yellow dropdown-toggle">
                              <i class="js-icon-angle-down js-icon-only bigger-120"></i>
                            </button>

                            <ul class="dropdown-menu dropdown-icon-only dropdown-yellow float-end dropdown-caret dropdown-close">
                              <li>
                                <a href="javascript:void(0);" class="tooltip-success" onClick= "azcommunity.toggleStatus('<?php echo $member->id?>',1)" data-rel="tooltip" title="Approve">
                                  <span class="green">
                                    <i class="js-icon-ok bigger-110"></i>
                                  </span>
                                </a>
                              </li>

                              <li>
                                <a href="javascript:void(0);" class="tooltip-warning" onClick= "azcommunity.toggleStatus('<?php echo $member->id?>',0)" data-rel="tooltip" title="Reject">
                                  <span class="orange">
                                    <i class="js-icon-remove bigger-110"></i>
                                  </span>
                                </a>
                              </li>

                              <li>
                                <a href="javascript:void(0);" class="tooltip-error" onClick= "azcommunity.toggleStatus('<?php echo $member->id?>',2)" data-rel="tooltip" title="Delete">
                                  <span class="red">
                                    <i class="js-icon-trash bigger-110"></i>
                                  </span>
                                </a>
                              </li>
                            </ul>
                          </div>
                        <?php }?>

                      </div>

                    </div>
                  </div>
                  <?php }?>

                  <div class="clearfix"></div>

                  <div class="center cta-full">
                    <a href="<?php echo JURI::root() . '/administrator/index.php?option=com_community&view=users'; ?>">
                      <?php echo JText::_('COM_COMMUNITY_DASHBOARD_ALL_MEMBERS')?> &nbsp;
                      <i class="js-icon-arrow-right"></i>
                    </a>
                  </div>

            </div> <!-- members tab end -->
          </div><!--/widget-main-->
        </div><!--/card-body-->
      </div>
</div>
</div>

<?php if(false) { //hide ?>
<!-- Demographic & Member Locations -->
<div class="row">
<!-- Demographic -->
<div class="col-4">
      <div class="card" id="demographic-box">
        <div class="card-header">

          <h5><i class="js-icon-globe"></i> <?php echo JText::_('COM_COMMUNITY_DEMOGRAPHIC')?></h5>
        </div>

        <div class="card-body">
          <div class="widget-main">
            <div id="pie-placeholder"></div>

            <div>
            <div class="hr hr-8 hr-double"></div>
            <h4 class="text-center"><?php echo JText::sprintf('COM_COMMUNITY_DASHBOARD_AVARAGE_AGE', $this->age->average)?></h4>
            <div class="hr hr-8 hr-double"></div>
                <div class="row">
                  <div class="col-6 text-center">
                    <img src="<?php echo COMMUNITY_BASE_ASSETS_URL . "/user-Male.png" ?>" alt="" class="inline avg-avatar img-circle">
                    <h3 class="inline"><?php echo $this->age->MaleAverage?></h3>
                    <span class="block"><?php echo JText::_('COM_COMMUNITY_DASHBOARD_AVARAGE_AGE_MALE')?></span>
                  </div>
                  <div class="col-6 text-center">
                    <img src="<?php echo COMMUNITY_BASE_ASSETS_URL . "/user-Female.png" ?>" alt="" class="inline avg-avatar img-circle">
                    <h3 class="inline"><?php echo $this->age->FemaleAverage?></h3>
                    <span class="block"><?php echo Jtext::_('COM_COMMUNITY_DASHBOARD_AVARAGE_AGE_FEMALE')?></span>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>
<!-- Member Location -->
<div class="span16">
    <div class="card" id="location-box">
      <div class="card-header">
        <h5><i class="js-icon-map-marker"></i><?php echo JText::_('COM_COMMUNITY_DASHBOARD_MEMBER_LOCATION')?></h5>
      </div>
      <div class="card-body">
        <div class="widget-main">
          <div class="row reset-gap">
            <div class="col-12">
              <!-- Map -->
              <div id="map_canvas"></div>
            </div>
          </div>
          <div class="row reset-gap">
            <div class="col-6">
              <table class="table table-bordered reset-gap">
                <thead>
                  <tr>
                    <th><?php echo JText::_('COM_COMMUNITY_DASHBOARD_COUNTRY') ?></th>
                    <th><?php echo JText::_('COM_COMMUNITY_DASHBOARD_FAN') ?></th>
                  </tr>
                </thead>
                <tbody>
                    <?php if(!empty($this->userCountry)) {?>
                      <?php foreach($this->userCountry as $userCountry){?>
                      <tr>
                        <td><?php echo JText::_($userCountry->country) ?></td>
                        <td><?php echo $userCountry->count ?></td>
                      </tr>
                      <?php }?>
                    <?php } else {?>
                        <tr>
                            <td colspan=2> <?php echo JText::_('COM_COMMUNITY_DASHBOARD_NO_COUNTRY')?> </td>
                        </tr>
                    <?php }?>
                </tbody>
              </table>
            </div>
            <div class="col-6">
              <table class="table table-bordered reset-gap">
                <thead>
                  <tr>
                    <th><?php echo JText::_('COM_COMMUNITY_DASHBOARD_CITY')?></th>
                    <th><?php echo Jtext::_('COM_COMMUNITY_DASHBOARD_FAN')?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(!empty($this->userCity)) {?>
                      <?php foreach($this->userCity as $city){?>
                          <?php if ( trim($city->city) != '' ) { ?>
                              <tr>
                                <td><?php echo $this->escape($city->city)?></td>
                                <td><?php echo $city->count?></td>
                              </tr>
                          <?php }?>
                      <?php }?>
                 <?php } else {?>
                        <tr>
                            <td colspan=2><?php echo JText::_('COM_COMMUNITY_DASHBOARD_NO_CITY')?></td>
                        </tr>
                 <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
</div>

<div class="row">
<div class="col-12">
  <!-- Calendar -->
  <div class="card" id="event-box">
    <div class="card-header">

      <h5>
        <i class="js-icon-calendar"></i>
        <?php echo JText::_('COM_COMMUNITY_DASHBOARD_EVENT_SCHEDULE')?>
      </h5>
    </div>
    <div class="card-body">
      <div class="widget-main">
        <div id="calendar"></div>
      </div>
    </div>
  </div>
</div>
<!-- <div class="col-6">

       <div class="card" id="news-feed-box">
        <div class="card-header">
          <h5>
            <i class="js-icon-rss orange"></i>
            <?php JText::_('COM_COMMUNITY_DASHBOARD_NEWS_FEED')?>
          </h5>

          <div class="widget-toolbar no-border">
            <ul class="nav nav-tabs" id="recent-tab">
              <li class="active">
                <a data-bs-toggle="tab" href="#news-tab"><?php echo JText::_('COM_COMMUNITY_DASHBOARD_NEWS'); ?></a>
              </li>

              <li class="">
                <a data-bs-toggle="tab" href="#addon-tab"><?php echo JText::_('COM_COMMUNITY_DASHBOARD_FEED_ADDONS'); ?></a>
              </li>

              <li class="">
                <a data-bs-toggle="tab" href="#ijoomla-tab"><?php echo JText::_('COM_COMMUNITY_DASHBOARD_FEED_IJOOMLA'); ?></a>
              </li>

            </ul>
          </div>
        </div>

        <div class="card-body">
          <div class="widget-main padding-4">
            <div class="tab-content padding-8 overflow-visible">
              <div id="news-tab" class="tab-pane active"> </div>

              <div id="addon-tab" class="tab-pane"> </div>

              <div id="ijoomla-tab" class="tab-pane"> </div>
            </div>
          </div>
        </div>
      </div>

</div> -->
</div>
<?php } ?>
<script src="<?php echo COMMUNITY_ASSETS_URL; ?>/js/jquery.flot.min.js"></script>
<script src="<?php echo COMMUNITY_ASSETS_URL; ?>/js/jquery.flot.categories.min.js"></script>
<script src="<?php echo COMMUNITY_ASSETS_URL; ?>/js/jquery.flot.pie.min.js"></script>
<script src="<?php echo COMMUNITY_ASSETS_URL; ?>/js/fullcalendar.min.js"></script>
<script src="<?php echo COMMUNITY_ASSETS_URL; ?>/js/jquery.slimscroll.min.js"></script>

<!-- User engagement and statistics -->
<script type="text/javascript">
var dashboard = {
    init: function()
    {
        var $engTabs = $('#engagement-tabs');

        // Graph tabs
        $engTabs.find('a').click(function (e) {
            e.preventDefault();

            var $self = $(this);
            var type = $self.attr('href').replace('#eng-', '');

            // Set type
            $engTabs.data('type', type);

            dashboard.getEngagementGraph();

            $self.tab('show');
        });

        // Initialize the default graphs
        dashboard.getEngagementGraph();
        dashboard.getStatisticGraph();
    },
    getEngagementGraph: function()
    {
        // Load the graph
        var time = $('#dropdown-time').data('time');
        var type = $('#engagement-tabs').data('type');

        $('.demo-placeholder, .demo-choices').html('');
        dashboard.showEngagementLoading();

        jax.call('community','admin,community,getEngagementGraph', type, time);
    },
    getStatisticGraph: function()
    {
        var time = $('#dropdown-time').data('time');
        var type = $('#engagement-tabs').data('type');

        $('#placeholder-2, #choices-2').html('');
        dashboard.showStatisticLoading();

        jax.call('community','admin,community,getStatisticGraph', time);
    },
    showEngagementLoading: function()
    {
        $('<img src="<?php echo JURI::root() . 'components/com_community/assets/window/wait.gif'; ?>" class="eng-loading"/>').prependTo('.demo-placeholder');
    },
    showStatisticLoading: function()
    {
        $('<img src="<?php echo JURI::root() . 'components/com_community/assets/window/wait.gif'; ?>" class="eng-loading"/>').prependTo('#placeholder-2');
    }
}

$(function() {
    dashboard.init();

    // move the ijoomla logo
    jQuery("#ijoomla-logo").css('display','block');
    jQuery("#ijoomla-logo").addClass("float-end no-margin").prependTo(".page-header");
    jQuery("#toolbar").remove();

    // Time dropdown
    $('#dropdown-time').unbind().dropdown();
    $('#dropdown-time-menu a').click(function (e) {
        e.preventDefault();

        var $self = $(this);

        // Active selection
        $self.closest('ul').find('li').removeClass('active');
        $self.parent('li').addClass('active');
        $('#dropdown-time').find('span:first').text($self.text());

        // Set time
        var time = $self.attr('href').replace('#time-', '');
        $('#dropdown-time').data('time', time);

        // Reload graph
        dashboard.getEngagementGraph();
        dashboard.getStatisticGraph();
    });
});
</script>

<!-- Add google maps -->
<style>
  #map_canvas {
    width: 100%;
    height: 200px;
    margin-bottom:10px !important;
  }
</style>
<script src="//maps.googleapis.com/maps/api/js?sensor=false<?php echo (CFactory::getConfig()->get('googleapikey', '')) ? '&key='.CFactory::getConfig()->get('googleapikey', '') : ''  ?> "></script>
<script>
    var geocoder;
    var map;
    function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(44.5403, -78.5463);
    var map_canvas = document.getElementById('map_canvas');
    var map_options = {
      center: latlng,
      zoom: 2,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    //map = new google.maps.Map(map_canvas, map_options);
    map = new google.maps.Map(document.getElementById('map_canvas'), map_options);

    <?php
    if(! empty($this->userCity)) {
        foreach($this->userCity as $userCity) {
        ?>
        codeAddress("<?php echo JText::_($userCity->city) ?>");
        <?php
        }
    }
    ?>

    }

    function codeAddress(address) {
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            title:address,
            position: results[0].geometry.location
        });
      } else {
        console.log('Geocode was not successful for the following reason: ' + status);
      }
    });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>


<script type="text/javascript">

jQuery(function() {

  /* initialize the calendar
  -----------------------------------------------------------------*/

  var date = new Date();
  var d = date.getDate();
  var m = date.getMonth();
  var y = date.getFullYear();

    var months = [  "<?php echo JText::_('COM_COMMUNITY_MONTH_JANUARY');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_FEBRUARY');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_MARCH');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_APRIL');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_MAY');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_JUNE');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_JULY');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_AUGUST');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_SEPTEMBER');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_OCTOBER');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_NOVEMBER');?>",
                    "<?php echo JText::_('COM_COMMUNITY_MONTH_DECEMBER');?>"
                ];
    var days = ["<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_1');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_2');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_3');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_4');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_5');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_6');?>",
                "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY_7');?>"];


  var calendar = jQuery('#calendar').fullCalendar({
     buttonText: {
      prev: '<i class="js-icon-chevron-left"></i>',
      next: '<i class="js-icon-chevron-right"></i>'
    },
      monthNames: months,
      monthNamesShort: months,
      dayNames: days,
      dayNamesShort: days,
      buttonText: {
          prev: "&nbsp;&#9668;&nbsp;",
          next: "&nbsp;&#9658;&nbsp;",
          prevYear: "&nbsp;&lt;&lt;&nbsp;",
          nextYear: "&nbsp;&gt;&gt;&nbsp;",
          today: "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_CURRENT');?>",
          month: "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_MONTH');?>",
          week: "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_WEEK');?>",
          day: "<?php echo JText::_('COM_COMMUNITY_DATEPICKER_DAY');?>"},

    header: {
      left: 'prev,next today',
      center: 'title',
      right: ''
    },
    events: <?php echo $this->eventcal;?>
    ,
    editable: false,
    droppable: false, // this allows things to be dropped onto the calendar !!!
    drop: function(date, allDay) { // this function is called when something is dropped

      // retrieve the dropped element's stored Event Object
      var originalEventObject = jQuery(this).data('eventObject');
      var $extraEventClass = jQuery(this).attr('data-class');


      // we need to copy it, so that multiple events don't have a reference to the same object
      var copiedEventObject = $.extend({}, originalEventObject);

      // assign it the date that was reported
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;
      if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];

      // render the event on the calendar
      // the last true argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      jQuery('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

      // is the "remove after drop" checkbox checked?
      if (jQuery('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        jQuery(this).remove();
      }

    }
    ,
    selectable: true,
    selectHelper: true,
    select: function(start, end, allDay) {
    }
    ,
    eventClick: function(calEvent, jsEvent, view) {

    }

  });

});

jax.call( 'community' , 'admin,community,getRssFeed','http://feeds.feedburner.com/JomsocialBlog','news-tab');
jax.call( 'community' , 'admin,community,getRssFeed','http://feeds.feedburner.com/new-addons','addon-tab');
jax.call( 'community' , 'admin,community,getRssFeed','http://www.ijoomla.com/blog/feed/','ijoomla-tab');

</script>

<script>
    // jquery slimscroll
    jQuery("#news-feed-box .card-body").slimScroll({
        height: '460px'
    });
    jQuery('#recent-box > .card-body').slimScroll({
        height: '500px'
    });

    jQuery('#statistic-box > .card-body').height('500px');
    jQuery('#event-box > .card-body').css({ minHeight: 460 });
    jQuery('#demographic-box > .card-body,#location-box > .card-body').height('464px');

    // joms.jQuery('div.inline.position-relative > button').dropdown();
</script>
