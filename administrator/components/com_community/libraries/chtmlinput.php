<?php
/**
* @copyright (C) 2013 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined('JPATH_PLATFORM') or die;

class CHTMLInput
{
	public static function checkbox($name, $class, $attribs = array(), $selected = null, $id = false)
	{
		$html = '';
		$selectedHtml = " checked=\"checked\"";

		if (is_array($attribs)) {
			$attribs = Joomla\Utilities\ArrayHelper::toString($attribs);
		}

		ob_start(); ?>
		<div role="group" id="<?php echo $name ?>" class="btn-group rounded <?php echo $class ?>" <?php echo $attribs ?>>
		 	<input type="radio" value="1" class="btn-check" id="<?php echo $name . '1' ?>" name="<?php echo $name ?>" <?php echo $selected == "1" ? $selectedHtml : '' ?>>			
			<label for="<?php echo $name . '1' ?>" class="btn btn-outline-primary">Yes</label>
		 	<input type="radio" value="0" class="btn-check" id="<?php echo $name . '0' ?>" name="<?php echo $name ?>" <?php echo $selected == "0" ? $selectedHtml : '' ?>>
			<label for="<?php echo $name . '0' ?>" class="btn btn-outline-secondary">No</label>
		 	</div>
		<?php 
		$html = ob_get_clean();

		return $html;
	}
}