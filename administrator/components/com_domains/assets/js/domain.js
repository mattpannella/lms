jQuery(document).ready(function(){
    jQuery(window).on('load', function () {
        var domain = document.getElementById("domain_name").value;
        var current = domain.toLowerCase();

        if (current.match(/^.*tovuti\.iojQuery/)) {

            jQuery("dns").parentNode.parentNode.hide()
            jQuery(document.getElementById("dns").value).parentNode.parentNode.hide()
        }

    })
    jQuery(document).on('focusout',"#domain_name", function(){
        var domain = document.getElementById("domain_name").value;
        var current = domain.toLowerCase();


        if (current.match(/^.*tovuti\.iojQuery/)) {

            jQuery("dns").parentNode.parentNode.hide();
            jQuery(document.getElementById("dns").value).parentNode.parentNode.hide();


        } else {
            jQuery("dns").parentNode.parentNode.show();
            jQuery(document.getElementById("dns").value).parentNode.parentNode.show();
        }

    })

    jQuery(document).on('click',"span[name=copy_pre]", function(){
        var jQuerythis	= jQuery(this);
        var text = jQuerythis.text();
        jQuery(document.getElementById("success-alert")).append(text);
        var id = jQuerythis.attr('id');
        var el = document.getElementById(id);
        var range = document.createRange();
        range.selectNodeContents(el);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        document.execCommand('copy');
        jQuery(document.getElementById("success-alert")).show();
        setTimeout(function() { jQuery(document.getElementById("success-alert")).hide(); }, 1000);
        return false;
    });


});