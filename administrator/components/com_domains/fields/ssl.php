<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldSsl extends JFormField {

    public function getInput() {
        return "Input";
    }

    public function getRepeatable() {
        $id = $this->item->get('id');
        $details = AxsDomains::getDomainDetailsById($id);
        if($details->ssl_id){
            $ssl_details = AxsDomains::getSSLDetailsById($details->ssl_id);
            if (!$ssl_details->active) {
                $return_status = '<span>Pending <i class="fa fa-clock-o" style="color:dodgerblue"></i></span>';

            } else {
                $return_status = '<span>Active <i class="fa fa-check-circle" style="color:green"></i></span>';
            }
        } else {
            if(AxsDomains::isTovutiManagedDomain($details->domain)) {
                $return_status = '<span>Managed <i class="fa fa-check-circle" style="color:green"></i></span>';
            } else {
                $return_status = '<span>Not Implemented <i class="fa fa-exclamation-triangle" style="color:goldenrod"></i></span>';
            }
        }

        return $return_status;
    }
}