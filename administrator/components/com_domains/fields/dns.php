<?php

defined('JPATH_PLATFORM') or die;

require_once("/var/www/files/creds.php");

class JFormFieldDns extends JFormField
{

    public function getInput()
    {
        $jinput = JFactory::getApplication()->input->getArray();
        $task = $jinput['task'];
        $id = $jinput['id'];
        $add_html = '<div id="' . $this->id . '" class="alert alert-info col-md-3" align="center"><span>Once domain is submitted this field will update with record required for associating your custom domain</span></div>';


        if ($task == "add") {
            return $add_html;
        } else {


            $details = AxsDomains::getDomainDetailsById($id);

            $config = dbCreds::getAwsConfig();
            $domain = $details->domain;

            $edit_html = '<div>
  <span>Add the following CNAME records to the DNS configuration for your domain. The procedure for adding CNAME records depends on your DNS service Provider. <a href="https://docs.aws.amazon.com/console/acm/dns/add-record" target="_blank">Learn more.</a></span>
<p>
  <div >
    <table class="table table-bordered table-sm">
      <thead>
        <tr>
          <th>Name</th>
          <th>Type</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <div>
              <span id="dns_name" title="click to copy" name="copy_pre">' . $domain . '</span> </div>
          </td>
          <td>CNAME</td>
          <td>
            <div>
              <span id="dns_value" title="click to copy" name="copy_pre">' . $config->dnsname . '</span> </div>
          </td>
        </tr>
      </tbody>
    </table>
    <span>Click fields to copy</span>
  </div>
<div class="alert alert-success" id="success-alert" style="display:none">
  <strong>Success! </strong> copied to clipboard
  <button type="button" class="btn-close float-end" data-dismiss="alert"></button>
</div>
</div>';
            return $edit_html;


        }

    }

}