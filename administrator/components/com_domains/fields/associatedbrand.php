<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldAssociatedbrand extends JFormField {

	public function getInput() {
		return "Input";
	}

	public function getRepeatable() {
      $params = json_decode($this->item->get('params'));
      $brand = AxsBrands::getBrandById($params->brand);
      return $brand->site_title;

   }
}
