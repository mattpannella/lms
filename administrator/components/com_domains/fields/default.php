<?php

defined('JPATH_PLATFORM') or die;

class JFormFielddefault extends JFormField {

	protected $type = 'default';
	
	public function getDefault() {
        $default = $this->item->get('default_domain');
        if ($default) { 
            return 'Yes';
        } else {
            return 'No'; 
        }
    }
	
	public function getInput() {
		return self::getDefault();
	}

	public function getRepeatable() {
       return self::getDefault();
   }
}