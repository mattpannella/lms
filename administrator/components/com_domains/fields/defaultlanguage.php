<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldDefaultlanguage extends JFormField {

	public function getInput() {
		return "Input";
	}

	public function getRepeatable() {
      $params = json_decode($this->item->get('params'));
      $language = JLanguage::getInstance($params->language);
      return $language->get('name');

   }
}