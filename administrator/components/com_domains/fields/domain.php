<?php

defined('JPATH_PLATFORM') or die;

class JFormFielddomain extends JFormField {

    protected $type = 'domain';

    protected function getInput()
    {
        if(!$this->value) {
            $required = $this->required ? ' required aria-required="true"' : '';
            $output   = '<input id="domain_name" type="text" name="' . $this->name . '" value="' . $this->value . '" '. $required .'></input>';
        } else {
            $output   = '<span style="font-size: 15px; font-weight:bold;">'.$this->value.'</span>';
            $output  .= '<input id="domain_name" type="hidden" name="' . $this->name . '" value="' . $this->value . '"></input>';
        }
        return $output;
    }
}