<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die;

class DomainsModelDomains extends FOFModel {

	public function save($data) {
        $params = new stdClass();
        $domain = trim($data['domain']);
		$data['domain'] = $domain;
        $id = $data['id'];

        $available = AxsDomains::checkDomainAvailable($domain);
		if(!$available && !$id){
			JFactory::getApplication()->enqueueMessage('That domain is not available.', 'error');
		} else {
			if(!$id) {
				AxsDomains::saveDomain($domain,NULL);
				$appParams = new stdClass();
				$appParams->clientDB = AxsClients::getClientDbName();
				$app = new AxsOkta($appParams);
				$app->addClientAppDomains($domain);
				$date = date('Y-m-d');
			} else {
				$date = $data['created_date'];
			}
			#get current details
            $result = AxsDomains::getDomainDetailsById($id);

            if ($result) {
                $json = json_decode($result->params);
                $currentBrandId = $json->brand;
                $ssl_id = $result->ssl_id;
            } else {
                $currentBrandId = NULL;
                $ssl_id = NULL;
            }

			# Setup SSl
            if (!AxsDomains::isTovutiManagedDomain($domain)) {
                if($ssl_id == NULL){
                    $ssl_id = AxsDomains::addSSLRecord($domain);
                }
            }

            # Associate Branding
			if($currentBrandId != $data['brand'] && $id != 0){
			    # If this is not the initial add, and the brandId has changed, remove domain from current brand association
			    $currentBrandDomains = AxsBrands::getUpdatedDomainsList($domain, "remove", $currentBrandId);
			    AxsBrands::updateBrand($currentBrandDomains, $currentBrandId);

            }
			$updatedDomains = AxsBrands::getUpdatedDomainsList($domain, "add", $data['brand']);
            AxsBrands::updateBrand($updatedDomains, $data['brand']);

			$params->language = $data['language'];
			$params->brand = $data['brand'];
			AxsLanguage::publishNeededLanguages();
			$data['created_date'] = $date;
			$data['ssl_id'] = $ssl_id;
			$data['params'] = json_encode($params);
			return parent::save($data);
		}
	}

	public function loadFormData() {
		if (empty($this->_formData)) {
			return array();
		} else {
			$params = json_decode($this->_formData['params']);
			$this->_formData['language'] = $params->language;
            $this->_formData['brand'] = $params->brand;
            return $this->_formData;
		}
	}
}