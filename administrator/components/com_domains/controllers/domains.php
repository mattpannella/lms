<?php

defined('_JEXEC') or die();

class DomainsControllerDomains extends FOFController {

	public function editItem(){
		$input  = JFactory::getApplication()->input;
    	$option = $input->get('option');
    	$view 	= 'domain';
    	$cid 	= $input->post->get('cid', array(), 'array');
    	JArrayHelper::toInteger($cid);
    	$id = $cid[0];
    	$url = "index.php?option=".$option."&view=".$view."&id=".$id;
    	$this->setRedirect($url);    	
	}

	public function transfer() {
		$brandId = $this->input->get('brandId');
		$domainId = $this->input->get('domainId');

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_domains');
		$query->where('id = ' . $db->quote($domainId));
		$db->setQuery($query);
		$domain = $db->loadObject();

		// Remove domain from the domain list in the old brand
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_brands');
		$query->where('id = ' . $db->quote(json_decode($domain->params)->brand));
		$query->orWhere('FIND_IN_SET(domains, ' . $db->quote($domain->domain) . ')');
		$db->setQuery($query);
		$oldBrand = $db->loadObject();

		$updatedDomains = array();
		if (!empty($oldBrand->domains)) {
			$updatedDomains = explode(',', $oldBrand->domains);
		}
		$updatedDomains = array_filter($updatedDomains, function ($el) use (&$domain) { return $el != $domain->domain; });
		$oldBrand->domains = implode(',', $updatedDomains);
		$success = $db->updateObject('axs_brands', $oldBrand, 'id');
		
		// Update axs_domains.params.brandId to brandId
		$query = $db->getQuery(true);
		$query->update('axs_domains');
		$query->set('params = JSON_SET(params, "$.brand", '. $db->quote($brandId) . ')');
		$query->where('id = ' . $db->quote($domainId));
		$db->setQuery($query);
		$success = $success && $db->execute();

		// Update the new domain
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_brands');
		$query->where('id = ' . $db->quote($brandId));
		$db->setQuery($query);
		$newBrand = $db->loadObject();
		
		$updatedDomains = array();
		if (!empty($newBrand->domains)) {
			$updatedDomains = explode(',', $newBrand->domains);
		}
		$updatedDomains []= $domain->domain;
		$newBrand->domains = implode(',', $updatedDomains);
		$success = $success && $db->updateObject('axs_brands', $newBrand, 'id');
		
		echo json_encode(array('success' => $success));
	}

	public function remove() {
        $url = 'index.php?option=com_domains&view=domains';

		$cid = $this->input->get('cid');
		if (!$cid) {
			return;
		}

		if (is_array($cid)) {
			$domainIds = implode(',', $cid);
		} else {
			$domainIds = $cid;
		}
		
		$currentDomains = AxsDomains::getClientDomains();
		$domainArray = explode(',', $currentDomains);

		$db = JFactory::getDbo();
        $query = "select GROUP_CONCAT(domain) as domains from (SELECT domain FROM axs_domains WHERE (FIND_IN_SET(id, '$domainIds') > 0)) as s";
        $db->setQuery($query);
        $selectedDomains = $db->loadObjectList();

		$query = "SELECT domain FROM axs_domains WHERE (FIND_IN_SET(id, '$domainIds') > 0) AND default_domain = 1";
		$db->setQuery($query);
		$defaultDomains = $db->loadObjectList();

		if(in_array($_SERVER['HTTP_HOST'], explode(",", $selectedDomains[0]->domains))) {
            JFactory::getApplication()->redirect($url, 'You cannot delete the domain you are currently using.', 'error');
            return;
        } elseif ($defaultDomains){
            JFactory::getApplication()->redirect($url, 'You cannot delete the default domain.', 'error');
            return;
		} else {
			$msg = 'Domains were deleted.';
		}

        $query = "SELECT * FROM axs_domains WHERE (FIND_IN_SET(id, '$domainIds') > 0) AND default_domain != 1";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		foreach($result as $item){
			$key = array_search($item->domain, $domainArray);
			if($key > -1){
				unset($domainArray[$key]);
			}
			$params = new stdClass();
			$params->eventName = 'Domain Delete';
			$params->description = $item->domain.' ['.$item->id.']';  
			$params->action_type ="admin";
			AxsTracking::sendToPendo($params);
			AxsActions::storeAdminAction($params);
			$iParams = json_decode($item->params);
            $brandId = $iParams->brand;
            $ssl_id = $item->ssl_id;

            if($brandId) {
                $updatedDomains = AxsBrands::getUpdatedDomainsList($item->domain, "remove", $brandId);
                AxsBrands::updateBrand($updatedDomains, $brandId);
            }

            if($ssl_id) {
                AxsDomains::removeSSLById($ssl_id);
            }
		}
		$domainList = implode(',',$domainArray);
		AxsDomains::updateDomainList($domainList,NULL);
		$query = "DELETE FROM axs_domains WHERE (FIND_IN_SET(id, '$domainIds') > 0) AND default_domain != 1";
		$db->setQuery($query);
		$db->execute();

		$format = $this->input->get('format', null, 'STRING');
		if ($format == 'raw') {
			echo json_encode(array('msg' => $msg));
		} else {
			$app = JFactory::getApplication();
			$app->redirect($url, $msg, $type = 'message');
		}
	}

	public function setDefault() {
		$domainIds = $this->input->get('cid');
		if (!$domainIds) {
			return;
		}

		$id = $domainIds[0];
		if (!$id) {
			return;
		}

		$db = JFactory::getDbo();
		$query = "UPDATE axs_domains SET default_domain = 1 WHERE id = $id";
		$db->setQuery($query);
		$db->execute();

		$query = "UPDATE axs_domains SET default_domain = 0 WHERE id != $id";
		$db->setQuery($query);
		$db->execute();
		header("Location: index.php?option=com_domains&view=domains");
	}
}