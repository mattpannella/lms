<?php
/**
 * @package     SP LMS
 *
 * @copyright   Copyright (C) 2010 - 2015 JoomShaper. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

class DomainsToolbar extends FOFToolbar {
  	function onDomainsEdit() {
		JToolBarHelper::title('Edit Domain');
		JToolbarHelper::save('save', 'Save & Close');
        JToolbarHelper::apply('apply', 'Submit');
        JToolbarHelper::cancel();
	}
	function onDomainsAdd() {
		JToolBarHelper::title('Add Domain');
		JToolbarHelper::save('save', 'Save & Close');
        JToolbarHelper::apply('apply', 'Submit');
        JToolbarHelper::cancel();
	}
	function onDomainsBrowse() {
		JToolBarHelper::title('Domain Manager');
		JToolbarHelper::addNew();		
		JToolBarHelper::editList('editItem');
		JToolBarHelper::custom('setDefault', 'star', 'star', 'Set as Default', false, false);
		JToolbarHelper::deleteList();

	}
}