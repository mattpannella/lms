<?php

defined('_JEXEC') or die;

class DashboardToolbar extends FOFToolbar {

	function onDefaultsBrowse() {
		JToolBarHelper::title('Dashboard');
	}

	function onDefaults() {
		JToolBarHelper::title('Dashboard');
	}
}