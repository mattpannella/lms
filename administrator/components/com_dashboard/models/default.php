<?php

class DashboardModelDefault extends FOFModel {

	function getDashboard($id) {
		$db  = JFactory::getDbo();
		$query = "SELECT * FROM axs_dashboards WHERE id = $id";
		$db->setQuery($query);
		$dashboard = $db->loadObject();
		$data = new stdClass();
		$data->dashboard = $dashboard;
		$data->charts = self::getCharts($dashboard);
		return $data;
	}

	function getDefault() {
		$db  = JFactory::getDbo();
		$user = JFactory::getUser()->id;
		$query = "SELECT * FROM axs_dashboards_defaults WHERE user_id = $user";
		$db->setQuery($query);
		$user_default = $db->loadObject();
		if($user_default) {
			$condition = "WHERE id = ". (int)$user_default->dashboard_id;
		}
		$query = "SELECT * FROM axs_dashboards $condition LIMIT 1";
		$db->setQuery($query);
		$dashboard = $db->loadObject();
		return $dashboard;
	}

	function getCharts($dashboard) {
		$db  = JFactory::getDbo();
		$charts = json_decode($dashboard->params);
		$chartsList = [];
		foreach($charts as $chart) { 
			$id = str_replace("chart_", "", $chart->id);
			array_push($chartsList, $id);
		}
		if(!count($chartsList)) {
			return "{}";
		}
		$chartsString = implode(',', $chartsList); 
		$query = "SELECT * FROM axs_dashboards_charts WHERE id IN ($chartsString)";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$chartData = [];
		foreach ($results as $result) {
			$chart = [
				"chart_".$result->id => 
					[
						"title" => $result->title,
	        			"data" => $result->params
					]
			];
	       array_push($chartData, $chart);
	       
	    }
		return $chartData;
	}
}