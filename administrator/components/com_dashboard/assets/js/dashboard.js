var chartsList = [];
var date_range;
var chartData = new Object();

/*
    Remove the standard Save, Load, etc. Joomla toolbar.
*/

var toolParent = null;

/*
    The kill* functions are for stopping setInterval functions.
*/

// var killToolBarCheck = function() {
//     clearInterval(deleteToolbar);
// };

// var killCalendarMove = function() {
//     clearInterval(moveCalendar);
// };
/*
    Check for the standard toolbar and remove it once it appears.
*/
// var deleteToolbar = setInterval(
//     function() {
//         var toolbar = document.getElementById('toolbar');

//         if (toolbar != null) {
//             toolParent = toolbar.parentNode;
//             jQuery(toolbar).remove();
//             killToolBarCheck();
//         }
//     }, 10
// );

/*
    Wait for the calendar to appear and then move it.
*/
// var moveCalendar = setInterval(
//     function() {
//         var datepicker = document.getElementById('date_range_calendar');
//         if (toolParent != null && datepicker != null) {
//             toolParent.append(datepicker);
//         }
//     }, 10
// );

(function($) {
    $(document).ready(
        function() {

            //Check for an ENTER press on the calendar
            $(".enter_calendar").keyup(
                function(e) {
                    if (e.keyCode === 13) {
                        $("#calendar_refresh").click();
                    }
                }
            );

            init_main_area();

            //The quick data views at the top of the page are closed by default.  However, their last state is stored in local storage to
            //keep the user's preference.
            $('.totals-stats').click(
                function() {
                    var lower_area = $(this).parents('.stats_bar').find('.totals-lower');
                    var name = "dashboard_" + this.id;
                    var visible = lower_area.is(":visible");

                    localStorage.setItem(name, !visible);

                    if (visible) {
                        document.getElementById(name + "_icon").className = 'lizicon-circle-down';
                        lower_area.hide(500);
                    } else {
                        document.getElementById(name + "_icon").className = 'lizicon-circle-up';
                        lower_area.show(500);
                    }
                }
            );

            //Give the appropriate expand/close icon based on its current setting.
            $(".totals-stats").each(
                function() {
                    var lower_area = $(this).parents('.stats_bar').find('.totals-lower');
                    var name = "dashboard_" + this.id;
                    var visible = localStorage.getItem(name);

                    var expand_icon = document.createElement('span');
                    this.appendChild(expand_icon);
                    expand_icon.id = name + "_icon";

                    if (visible == 'true') {
                        expand_icon.className = 'lizicon-circle-up';
                        lower_area.show(500);
                    } else {
                        expand_icon.className = 'lizicon-circle-down';
                    }
                }
            );
        }
    );
}) (jQuery);

/*
    This makes sure that the sidebar and the main area are the same height to keep from looking 'unfinished'
*/



/*
    Call the four functions that create the data readouts for the quick views.
*/
function init_main_area() {
    view_current_members();
    //view_life_boards();
    view_total_revenue();
    view_new_members();
}

/*
    Parses the data and creates the output for the life boards.
    Will be deprecated in the future when the Board Builder is finished, since the data will no longer be static.
*/

function view_life_boards() {

    var sdate, edate;
    if (start_date == null) {
        sdate = 0;
    } else {
        sdate = start_date;
    }

    if (end_date == null) {
        edate = 0;
    } else {
        edate = end_date;
    }

    var declarationCount    = -1;
    var gratitudeCount      = -1;
    var inspirationCount    = -1;

    var numBoards = 3;

    for (i = 0; i < numBoards; i++) {

        var whichBoard;
        var area;

        switch (i) {
            case 0: whichBoard = 'dec';break;
            case 1: whichBoard = 'grat'; break;
            case 2: whichBoard = 'insp'; break;
        }

        jQuery.ajax({
            type:       'POST',
            url:        'index.php',
            data:       {
                'option':       'com_axs',
                'task':         'dashboard.getBoards',
                'format':       'raw',
                'which':        whichBoard,
                'numWhich':     i,
                'allData':      0,
                'start':        sdate,
                'end':          edate
            },
            success: function(response) {
                var dat = JSON.parse(response);
                var which = dat.which;
                var count = dat.count;
                var area;

                switch (which) {
                    case '0':
                        declarationCount = count;
                        area = "declarationArea";
                        break;
                    case '1':
                        gratitudeCount = count;
                        area = "gratitudeArea";
                        break;
                    case '2':
                        inspirationCount = count;
                        area = "inspirationArea";
                        break;
                    default:
                        area = "test";
                }

                document.getElementById(area).innerHTML = numberWithCommas(count);
            }
        });
    }


    var setLifeboardHTML = function() {
        document.getElementById("allLifeBoardsArea").innerHTML = numberWithCommas(declarationCount + gratitudeCount + inspirationCount);
        clearInterval(lifeboardInterval);
    }

    var lifeboardInterval = setInterval(
        function() {
            if (declarationCount >= 0 && gratitudeCount >= 0 && inspirationCount >= 0) {
                setLifeboardHTML();
            }
        }, 10
    );
}

/*
    Parses the data for members and displays the output.
*/
function view_current_members() {

    var today = null;
    var current = 0;
    var grace = 0;
    var affiliate = 0;

    if (daily_data.length != 0) {
        today = daily_data[daily_data.length - 1];

        if (today.membership_current != null) {
            current = today.membership_current.current.count;
            grace = today.membership_current.grace_period.count;
            affiliate = today.membership_current.affiliates.count;
        }
    }

    var total_members = current + grace + affiliate;

    document.getElementById("allActiveArea").innerHTML = numberWithCommas(total_members);
    document.getElementById("activeMembersArea").innerHTML = numberWithCommas(current);
    document.getElementById("activeMembersWithGraceArea").innerHTML = numberWithCommas(grace);
    document.getElementById("activeAffiliates").innerHTML = numberWithCommas(affiliate);

}

/*
    Parses the data for revenue and displays the output.
*/

function view_total_revenue() {


    var upgradesTotal = 0;
    var duesTotal = 0;
    var eventsTotal = 0;
    var total = 0;

    for (i = 0; i < daily_data.length; i++) {
        var daily_revenue = daily_data[i].revenue;

        upgradesTotal += parseInt(daily_revenue.upgrades);
        duesTotal += parseInt(daily_revenue.dues);
        eventsTotal += parseInt(daily_revenue.events);
        total += parseInt(daily_revenue.total);
    }

    document.getElementById('eventsRevenueArea').innerHTML = currency_symbol + numberWithCommas(eventsTotal);
    document.getElementById('monthlyDuesRevenueArea').innerHTML = currency_symbol + numberWithCommas(duesTotal);
    document.getElementById('upgradesRevenueArea').innerHTML = currency_symbol + numberWithCommas(upgradesTotal);
    document.getElementById('totalRevenueArea').innerHTML = currency_symbol + numberWithCommas(total);

}

var newMemberInfo = null;
var droppedMemberInfo = null;

/*
    Parses the data for new members and displays the output.
*/

function view_new_members() {

    var total = new Array();
    var curMonth = date_range.end.month;

    var monthNum = -1;

    if (daily_data.length == 0) {
        return;
    }

    var current_month = new Date(daily_data[0].date);
    current_month = current_month.getMonth();

    for (i = daily_data.length - 1; i >= 0; i--) {

        var today = daily_data[i];
        var date = today.date;

        var today_month = new Date(date + " 00:00:00");
        today_month = today_month.getMonth();

        if (today_month != current_month) {
            monthNum++;
            current_month = today_month;
        }

        if (total[monthNum] === undefined) {
            total[monthNum] = parseInt(today.members_new.count);
        } else {
            total[monthNum] += parseInt(today.members_new.count);
        }
    }

    total.forEach(
        function(value, index) {
            var area;

            area = 'month';
            if (index < 10) {
                area += "0";
            }

            area += index + "NewMemberArea";

            var areaDiv = document.getElementById(area);
            if (areaDiv) {
                areaDiv.innerHTML = value;
            }
        }
    );
}

/*
    A simple function to return the month name in a string.
*/

function getMonthName (num, full) {

    if (full) {
        switch (num) {
            case 1: return "January";
            case 2: return "February";
            case 3: return "March";
            case 4: return "April";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "August";
            case 9: return "September";
            case 10: return "October";
            case 11: return "November";
            case 12: return "December";
        }
    } else {
        switch (num) {
            case 1: return "Jan";
            case 2: return "Feb";
            case 3: return "Mar";
            case 4: return "Apr";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "Aug";
            case 9: return "Sept";
            case 10: return "Oct";
            case 11: return "Nov";
            case 12: return "Dec";
        }
    }

    return null;
}

/*
    Cycles through all of the data and creates an array that contains all of the possible dates that exist between
    the start and the end date.
*/

var today = new Date();

function makeDates() {

    var make_date = function() {

        var day = today.getDate()
        var month = today.getMonth() + 1;
        var year = today.getFullYear();

        var new_date = "" + year + "-";
        if (month < 10) {
            new_date += "0";
        }

        new_date += "" + month + "-";

        if (day < 10) {
            new_date += "0"
        }

        new_date += "" + day;

        return new_date;
    };

    if (start_date == null) {
        start_date = make_date();
    }

    if (end_date == null) {
        end_date = make_date();
    }

    date_range = new Object();
    date_range.start = new Object();
    date_range.end = new Object();


    date_range.start.day = parseInt(start_date.substring(8, 10));
    date_range.start.month = parseInt(start_date.substring(5, 7));
    date_range.start.year = parseInt(start_date.substring(0, 4));

    date_range.end.day = parseInt(end_date.substring(8, 10));
    date_range.end.month = parseInt(end_date.substring(5, 7));
    date_range.end.year = parseInt(end_date.substring(0, 4));

    date_range.num_days = 1;
    date_range.num_weeks = 1;
    date_range.num_months = 1;

    date_range.days = new Object();
    date_range.weeks = new Object();
    date_range.months = new Object();

    date_range.days = [];
    date_range.weeks = [];
    date_range.months = [];

    var day, week, month;
    var prev_day, prev_week, prev_month;

    var date_current = new Date(start_date);
    var date_end = new Date(end_date);

    var first = true;

    var getDayName = function(day, month, year) {
        var names = new Object();

        names.month = new Object();
        names.day = new Object();

        names.day.long = getMonthName(month) + " " + day + " " + year.toString().substr(2,4);
        names.day.short = getMonthName(month) + " " + day;

        names.month.long = getMonthName(month) + " " + year.toString().substr(2,4);
        names.month.short = getMonthName(month);

        var monthText = "";
        if (month < 10) {
            monthText = "0" + month;
        } else {
            monthText = month;
        }

        var dayText = "";
        if (day < 10) {
            dayText = "0" + day;
        } else {
            dayText = day;
        }

        names.day.actual = year + "-" + monthText + "-" + dayText;
        names.month.actual = year + "-" + monthText + "-" + dayText;

        return names;
    }

    /**
     * Date.prototype.getWeek
     * Calculates the Julian day of the current day of the given Date object and returns the current week number (1-52)
     * 
     * @returns int Week corresponding to the current day of the year
     */
    Date.prototype.getWeek = function() {
        let year = this.getUTCFullYear();
        let month = this.getUTCMonth();
        let date = this.getUTCDate();

        // Get the UNIX UTC epochs for the first day of the current year and the current date
        let yearStartEpoch = Date.UTC(year, 0, 1);
        let currentEpoch = Date.UTC(year, month, date);

        /* The Julian date is the number of days that have elapsed since Jan 1 of the current year, inclusive.
         * For example, March 15 would have a Julian date of 31+(28 or 29)+15 = 74 or 75.
         * The extra day would be in case of a leap year. Calculating the days elapsed via milliseconds since the UNIX 
         * standard date of Jan 1. 1970 (epoch time), seems to be the best way to approach this without requiring leap * year calculations.
         */
        let julianDate = (currentEpoch - yearStartEpoch) / (1000 * 3600 * 24);

        // The current week of the year is found by calculating the Julian date for a given date and dividing by 7.
        // The integral part of the result is the current week in the year (1-based value)
        let weekNumber = Math.floor(julianDate / 7.0);
        
        return weekNumber;
    }

    while (date_current < date_end) {

        day = date_current.getDate();
        week = date_current.getWeek();
        month = date_current.getMonth() + 1;

        var year = date_current.getFullYear();

        var names = getDayName(day, month, year);

        if (first) {
            first = false;
            prev_day = day;
            prev_week = week;
            prev_month = month;

            date_range.days.push(names.day);
            date_range.weeks.push(names.day);
            date_range.months.push(names.month);
        }

        if (day != prev_day) {
            date_range.num_days++;
            date_range.days.push(names.day);
        }

        if (week != prev_week) {
            date_range.num_weeks++;
            date_range.weeks.push(names.day);
        }

        if (month != prev_month) {
            date_range.num_months++;
            date_range.months.push(names.month);
        }

        if (!first) {
            prev_day = day;
            prev_week = week;
            prev_month = month;
        }

        var newDate = date_current.setDate(date_current.getDate() + 1);
        date_current = new Date(newDate);
    }
}

/*
    Cycles through the entire range of dates selected and attaches it to an
    array index in the daily_data, if one exists.

    This greatly reduces workload of linking data to dates.
*/

function connectData() {

    var count = 0;
    for (i = 0; i < date_range.days.length; i++) {
        for (j = 0; j < daily_data.length; j++) {
            var today = daily_data[j].date;
            var compare = date_range.days[i].actual;
            if (today == compare) {
                date_range.days[i].daily_data_num = j;
                break;
            } else {
                date_range.days[i].daily_data_num = -1;
            }
        }
    }
}

/*
    Creates the actual chart box.
    Each chart box has a set of controls at the top for changing the chart type and the courseness (day, week, month).

*/

function makeChart(params) {

    if (!params) {
        return;
    }

    var chartData = params.data;

    if (!chartData) {
        return;
    }

    //var chartData = params.data;
    var name = params.name;
    id = params.id;
    theme = params.theme;

    text = chartData.title;
    dataArray = chartData.params;

    var chartArea = document.getElementById(name);
    chartArea.innerHTML = "";

    var numElements = dataArray.length;

    var newChartBox = document.createElement('DIV');
    newChartBox.className = 'myChart';
    newChartBox.id = name + 'ChartBox';
    newChartBox.setAttribute('chartName', text);

    var sliderUserSetting = parseInt(localStorage.getItem("dashboard_" + name + "ChartSliderBar"));
    if (!sliderUserSetting) {
        sliderUserSetting = 2;
    }

    var chartHolder = document.createElement('DIV');
    chartHolder.className = "chart_box";
    chartHolder.id = name + '-ChartHolder';
    newChartBox.appendChild(chartHolder);

    jQuery(chartHolder).css({
        'overflow':     'hidden'
    });

    chartArea.appendChild(newChartBox);

    /*
        Set Base Data
    */

    chartData[name] = new Object();

    var dataName;
    var columnNames = [];

    var dataSet = [];

    dataSet.date = [];
    var series = [];

    for (i = 0; i < numElements; i++) {

        dataSet[dataArray[i].field] = [];

        switch (dataArray[i].field) {
            case "cc_declines":             dataName = "Credit Card Declines";          break;
            case "cc_fails":                dataName = "Credit Card Fails";             break;
            case "cc_recoveries":           dataName = "Credit Card Recoveries";        break;
            case "downgrades":              dataName = "Member Downgrades";             break;
            case "cancels":                 dataName = "Member Cancels";                break;
            case "members_new":             dataName = "New Members";                   break;
            case "members_affiliates":      dataName = "Affiliate Members";             break;
            case "members_current":         dataName = "Current Members";               break;
            case "members_grace_period":    dataName = "Grace Period Members";          break;
            case "reengages":               dataName = "Member Reengages";              break;
            case "refunds":                 dataName = "Refunds";                       break;
            case "revenue_dues":            dataName = "Subscriptions";                 break;
            case "revenue_ecommerce":       dataName = "Ecommerce Store";               break;
            case "revenue_total":           dataName = "Total Revenue";                 break;
            case "revenue_events":          dataName = "Event Registrations";           break;
            case "revenue_upgrades":        dataName = "Course Purchases";              break;
            case "site_visits":             dataName = "Site Visits";                   break;
        }

        columnNames.push(dataName);

        dataArray[i].categoryField = "date";
        dataArray[i].aggregate = "sum";

        series.push(dataArray[i]);
    }

    for (i = 0; i < date_range.days.length; i++) {

        var date = new Date(date_range.days[i].actual + " 00:00:00");

        var newDataSet = new Object();
        newDataSet.date = date;

        var num = date_range.days[i].daily_data_num;
        var daily = daily_data[num];

        if (daily === undefined || num == -1) {

            for (j = 0; j < numElements; j++) {
                newDataSet[dataArray[j].field] = 0;
            }

        } else {

            var membership_current = daily.membership_current;
            var revenue = daily.revenue;
            var site_visits = daily.site_visits;

            /* To protect against having to check for null in the switch statement, just create the data */

            if (membership_current == null) {
                membership_current = {
                    affiliates: { count: 0 },
                    current: { count : 0 },
                    grace_period: { count: 0 }
                }
            }

            if (site_visits == null) {
                site_visits = {
                    count: 0
                }
            }

            if (revenue == null) {
                revenue = {
                    dues: 0,
                    events: 0,
                    upgrades: 0
                }
            }

            for (j = 0; j < numElements; j++) {

                var data;
                var whichData = dataArray[j].field;
                switch (whichData) {
                    case "cc_declines":             data = daily.cc_declines.total;                 break;
                    case "cc_fails":                data = daily.cc_fails.total;                    break;
                    case "cc_recoveries":           data = daily.cc_recoveries.total;               break;
                    //case "downgrades":              data = daily.downgrades.count;                  break;
                    case "cancels":                 data = daily.cancels.total;                     break;
                    case "members_new":             data = daily.members_new.count;                 break;
                    case "members_affiliates":      data = membership_current.affiliates.count;     break;
                    case "members_current":         data = membership_current.current.count;        break;
                    case "members_grace_period":    data = membership_current.grace_period.count;   break;
                    //case "reengages":               data = daily.reengages.count;                   break;
                    case "refunds":                 data = refunds.total;                           break;
                    case "revenue_dues":            data = revenue.dues;                            break;
                    case "revenue_total":           data = revenue.total;                           break;
                    case "revenue_ecommerce":       data = revenue.ecommerce;                       break;
                    case "revenue_events":          data = revenue.events;                          break;
                    case "revenue_upgrades":        data = revenue.upgrades;                        break;
                    case "site_visits":             data = site_visits.count;                       break;
                    default:                        data = 0;
                }

                newDataSet[whichData] = parseInt(data);

            }
        }
        dataSet.push(newDataSet);
    }

    /*
        The DONUT style of chart does not work correctly with the data compiled by day (over time).  It shows a slice for every day.
        Each TYPE of data needs to be added up and reconfigured for the donut to work correctly.
        Also, donut charts cannot be mixed with any other charts.  So, if one is a donut, they all need to be a donut.
    */

    var convertDonut = false;
    for (var i = 0; i < series.length; i++) {
        if (series[i].type == "donut") {
            convertDonut = true;
            break;
        }
    }

    if (convertDonut) {
        var newSeries = [];
        var seriesObject = new Object();

        seriesObject.type = "donut";
        var data = [];

        for (var i = 0; i < series.length; i++) {
            var dataObject = new Object();

            var total = 0;
            for (j = 0; j < dataSet.length; j++) {
                total += dataSet[j][series[i].field];
            }

            dataObject.category = series[i].name;
            dataObject.color = series[i].color;
            dataObject.value = total;


            data.push(dataObject);
        }
        seriesObject.data = data;

        newSeries.push(seriesObject);
        dataSet = [];

        series = newSeries;
    }

    var baseUnit = null;
    switch (parseInt(sliderUserSetting)) {
        case 0: baseUnit = "days"; break;
        case 1: baseUnit = "weeks"; break;
        case 2: baseUnit = "months"; break;
        case 3: baseUnit = "years"; break;
    }

    var stored_date = localStorage.getItem("chart_" + id + "_date_range");
    var range = {};

    if (stored_date) {
        range = getDates(stored_date);
    } else {
        range.start = new Date(start_date);
        range.end = new Date(end_date);
    }

    date_range.chart_range = range;

    var chartSettings = {
        dataSource: {
            data: dataSet
        },
        legend: {
            position: "top"
        },
        theme: theme,
        series: series,
        tooltip: {
            visible: true
        },
        categoryAxis: {
            baseUnit: baseUnit,
            labels: {
                rotation: -45
                    },
            crosshair: {
                visible: true
            },
            min: range.start,
            max: range.end
        },
        valueAxis: {
            labels: {

            }
        },
        pannable: {
            lock: "y"
        },
        zoomable: {
            mousewheel: {
                lock: "y"
            },
            selection: {
                lock: "y"
            }
        }
    };

    var chartObject = new Object();

    chartObject.settings = chartSettings;
    chartObject.container = chartHolder;
    chartObject.name = name;
    chartObject.id = id;
    $(chartObject.container).kendoChart(chartObject.settings);

    makeChartControls(chartObject);

    var chart = $(chartHolder).data("kendoChart");
    chartsList.push(chart);
    setInterval(
        function() {
            chart.resize();
        }, 100
    );
}

/*
    Creates the controls and functionality for each chart
*/

function makeChartControls(chart) {

    var chartSettings = chart.settings;
    var chartContainer = chart.container;
    var which = chart.name;
    var id = chart.id;

    function changeChartType(chartIcon, lineIcon, stepIcon, barIcon, donutIcon, charts) {
        return function() {

            var chart = $(chartContainer).data("kendoChart");

            function setChart(settings) {
                for (var i = 0; i < chart.options.series.length; i++) {
                    var keys = Object.keys(settings);
                    keys.forEach(
                        function(key) {
                            chart.options.series[i][key] = settings[key];
                        }
                    );
                }

                chart.refresh();

                localStorage.setItem("dashboard_" + which + "Chart_style", settings);
            }

            jQuery(chartIcon).click(
                function() {
                    setChart({
                        type: "line",
                        style: "smooth"
                    });
                }
            );

            jQuery(lineIcon).click(
                function() {
                    setChart({
                        type: "area",
                        line: {
                            style: "smooth"
                        }
                    });
                }
            );

            jQuery(stepIcon).click(
                function() {
                    setChart({
                        type: "line",
                        style: "smooth"
                    });
                }
            );

            jQuery(barIcon).click(
                function() {
                    setChart({
                        type: "line",
                        style: "smooth"
                    });
                }
            );

            jQuery(donutIcon).click(
                function() {
                    setChart({
                        type: "line",
                        style: "smooth"
                    });
                }
            );


        }
    }

    var setSliderAction = function(sliderBar, sliderText) {
        return function() {
            sliderBar.oninput = function() {

                var newText = "";
                var newType = parseInt(sliderBar.value);

                localStorage.setItem("dashboard_" + which + "ChartSliderBar", newType);

                var chart = $(chartContainer).data("kendoChart");
                var baseUnit;

                switch (newType) {
                    case 0:
                        newText = "Day";
                        baseUnit = "days";
                        break;
                    case 1:
                        newText = "Week";
                        baseUnit = "weeks";
                        break;
                    default:
                    case 2:
                        newText = "Month";
                        baseUnit = "months";
                        break;
                    case 3:
                        newText = "Year";
                        baseUnit = "years";
                        break;
                }

                chart.options.categoryAxis.baseUnit = baseUnit;
                chart.refresh();

                sliderText.innerHTML = newText;

            }
        }
    }

    /*
        Create an object to store the charts in.
    */

    var chartParent = chartContainer.parentElement;

    /*
        Create a new wrapper div for the chart area, as well as a div for the control bar
    */

    var collapsibleWrapper = document.createElement("DIV");
    var controlBar = document.createElement("DIV");

    /*
        Place the control bar and the chart area inside the wrapper.
        Place the wrapper in the parent container.
    */

    collapsibleWrapper.appendChild(controlBar);
    collapsibleWrapper.appendChild(chartContainer);
    chartParent.appendChild(collapsibleWrapper);

    var text = document.createTextNode(chartParent.getAttribute("chartName"));
    var textBox = document.createElement('SPAN');
    textBox.appendChild(text);

    textBox.className += " dashboard_chart_textBox";
    controlBar.className += " dashboard_chart_bar";
    controlBar.appendChild(textBox);

    /*
        Make the buttons operational.
    */
    //var optionsIcon = document.createElement("a");
    var optionsIcon = document.createElement("span");
    //optionsIcon.className = "axsmodal control_bar_button lizicon-cogs";
    optionsIcon.className = "control_bar_button lizicon-cogs";
    optionsIcon.title = "Chart Settings";
    //optionsIcon.href = "index.php?option=com_dashboard&view=dashboards&task=loadChartForm&format=raw&id="+id;

    var optionsMenu = document.createElement('div');
    optionsMenu.className = "dashboard-dropdown-menu";

    var optionsMenuEdit = document.createElement('a');
    optionsMenuEdit.className = "axsmodal lizicon-pencil settings-btns";
    optionsMenuEdit.href = "index.php?option=com_dashboard&view=dashboards&task=loadChartForm&format=raw&id="+id;
    optionsMenuEdit.setAttribute("data-width", "800");
    optionsMenuEdit.setAttribute("data-height", "600");
    optionsMenuEdit.setAttribute("data-title", "Edit Chart");
    optionsMenuEdit.appendChild(document.createTextNode(" Edit"));

    var optionsMenuDelete = document.createElement('div');
    optionsMenuDelete.className = "lizicon-cancel-circle settings-btns";
    optionsMenuDelete.appendChild(document.createTextNode(" Delete"));
    optionsMenuDelete.setAttribute("chart_id", id);

    optionsMenu.appendChild(optionsMenuEdit);
    optionsMenu.appendChild(optionsMenuDelete);
    optionsIcon.appendChild(optionsMenu);

    jQuery(optionsIcon).click(
        function() {
            var visible = jQuery(optionsMenu).is(":visible");
            if (visible) {
                jQuery(optionsMenu).hide(500);
            } else {
                jQuery(optionsMenu).show(500);
            }
        }
    );

    jQuery(optionsMenu).css({
        "left" : "35px",
        "top" : "5px"
    });

    jQuery(optionsMenuDelete).click(
        function() {
            //if (confirm("Are you sure you want to remove the chartbox " + )
            var chart_id = this.getAttribute("chart_id");
            var chart_to_delete = document.getElementById("chart_" + chart_id).parentNode;
            var chart_title = document.getElementById("chart_" + chart_id + "ChartBox").getAttribute("chartname");

            if (confirm("Are you sure you want to remove the chart \"" + chart_title + "\" from the dashboard?")) {
                jQuery(chart_to_delete).remove();
            }
        }
    );

    var calendarIcon = document.createElement('SPAN');
    calendarIcon.className = "control_bar_button set_chart_range lizicon-calendar";
    calendarIcon.title = "Set Chart Date Range";
    /*jQuery(calendarIcon).css({
        "font-size": "14px",
        "margin-left": "4px",
        "color":"#777"
    });*/

    var buttonBlock = document.createElement('SPAN');

    var sliderBarBox = document.createElement('SPAN');
    var sliderBar = document.createElement('INPUT');

    sliderBar.type = "range";
    sliderBar.min = 0;
    sliderBar.max = 3;

    var sliderUserSetting = localStorage.getItem("dashboard_" + which + "ChartSliderBar");
    if (sliderUserSetting) {
        sliderBar.value = parseInt(sliderUserSetting);
    } else {
        sliderBar.value = 2;
    }

    var settingText = null;

    switch (parseInt(sliderBar.value)) {
        case 0: settingText = "Day"; break;
        case 1: settingText = "Week"; break;
        default:
        case 2: settingText = "Month"; break;
        case 3: settingText = "Year"; break;
    }

    sliderBar.className = "dashboard_chart_slider";

    var sliderText = document.createElement('SPAN');
    sliderText.appendChild(document.createTextNode(settingText));
    sliderText.className = "dashboard_chart_slider_text";

    sliderBarBox.appendChild(sliderBar);
    sliderBarBox.appendChild(sliderText);

    setSliderAction(sliderBar, sliderText)();

    buttonBlock.appendChild(optionsIcon);
    buttonBlock.appendChild(calendarIcon);
    buttonBlock.appendChild(sliderBarBox);

    var calendar = document.createElement('INPUT');
    calendar.setAttribute("chart_id", id);
    calendar.setAttribute("title", "Set date range");
    var cal = jQuery(calendar);

    /*
    var calendar_start = null;
    var calendar_end = null;

    console.log(date_range);

    var dateRange = localStorage.getItem("chart_" + id + "_date_range");
    if (dateRange) {
        var range = getDates(dateRange);

        calendar_start = range.start;
        calendar_end = range.end;
    } else {
        calendar_start = new Date(start_date);
        calendar_end = new Date(end_date);
    }*/

    cal.daterangepicker({
        startDate: date_range.chart_range.start,
        endDate: date_range.chart_range.end
    });


    cal.change(
        function() {
            var dateRange = cal.val();
            var range = getDates(dateRange);
            var which = this.getAttribute("chart_id");
            var chart_box = document.getElementById("chart_" + which + "-ChartHolder");
            var chartData = jQuery(chart_box).data("kendoChart");
            chartData.setOptions({
                categoryAxis: {
                    min: range.start,
                    max: range.end
                }
            });

            date_range.chart_range = range;

            localStorage.setItem("chart_" + which + "_date_range", dateRange);
        }
    );

    cal.css({
        "display":"none"
    });

    jQuery(calendarIcon).click(
        function() {
            if (cal.is(":visible")) {
                cal.hide(150);
            } else {
                cal.show(150);
            }
        }
    );

    buttonBlock.appendChild(calendar);
    buttonBlock.className = "control_bar_button_block";

    controlBar.appendChild(buttonBlock);
}

/*
    Takes a string date range and returns an object with Date objects.
*/
function getDates(range) {
    var dates = range.split("-");
    var s = new Date(dates[0]);
    var e = new Date(dates[1]);
    range = {
        start: s,
        end: e
    };

    return range;
}

/*
    Returns a number with commas inserted.
    12345 will be returned as 12,345
*/
function numberWithCommas(x) {
    var string;

    try {
        string = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } catch(err) {
        string = x;
    }

    return string;
}

/*
    Returns an object containing the day,month, and year or the date given.
*/
function parseDate(date) {
    var year = date.substring(0, 4);
    var month = date.substring(5, 7);
    var day = date.substring(8, 10);

    var newDate = {
        year:       year,
        month:      month,
        day:        day
    };

    return newDate;
}

/*
    This function is specifically for the dropdown lists that show/hide the privacy settings.
*/
function hideShowText(select_list) {

    var texts = select_list.parentNode.getElementsByClassName("text");

    jQuery(texts).hide().each(
        function() {
            if (this.getAttribute("which") == select_list.value) {
                jQuery(this).show();
            }
        }
    );
}
