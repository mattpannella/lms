<?php

defined('_JEXEC') or die();

class DashboardControllerDashboards extends FOFController {

	function loadDashboardForm() {
		helperDashboardForm();
	}

	function saveDashboardForm() {
		$form = $this->input;
		helperSaveDashboard($form);
	}

	function loadThemeSelector() {
		helperThemeSelector();
	}

	function loadDashboardSelector() {
		buildSelector("dashboard");
	}

	function setDashboardDefault() {
		helperSetDashboardDefault();
	}	

	function loadChartForm() {
		helperChartForm();
	}

	function saveChartForm() {
		helperSaveChartForm();
	}

	function loadChartData() {
		helperLoadChartData();
	}

	function deleteChartData() {
		helperDeleteChartData();
	}
}

?>