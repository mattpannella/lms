<?php

defined('_JEXEC') or die();

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

function helperSaveChartForm() {

	$title = JRequest::getVar('title');
	$params = JRequest::getVar('params');
	$description = JRequest::getVar('desc');
	$access = JRequest::getVar('access');
	$chart_id = JRequest::getVar('chart_id');
	$save_type = JRequest::getVar('type');

	//Do not allow for empty data.
	if ((count(json_decode($params)) == 0) || ($title == null || $title == "")) {
		return "denied";
	}

	if ($save_type != 'new' && $save_type != 'update') {
		return "denied";
	}

	if ($save_type == 'new') {
		//If the user is saving an existing chart as new, remove the id.
		$chart_id = null;
	}

	$db = JFactory::getDBO();
	$user_id = JFactory::getUser()->id;

	//Get the current date
	$query = "SELECT NOW()";
	$db->setQuery($query);
	$now = $db->loadObject()->{'NOW()'};

	if ($chart_id) {
		
		$query = "SELECT mod_by, access, user_id FROM axs_dashboards_charts WHERE ID = " . $chart_id;

		$db->setQuery($query);
		$result = $db->loadObject();

		if (!empty($result)) {
			$mod_by = json_decode($result->mod_by);

			$access = $result->access;
			$user_id = $result->user_id;
			$current_user = $user_id;

			if ($user_id == $current_user) {
				$can_change_access = true;
			} else {
				$can_change_access = false;
			}

			//Check that the user has permission to save/edit this file
			//	1	-	Private.  	Only the original user can make changes.
			//	2	-	Shared. 	Only the original user can make changes.
			//	3 	-	Public. 	Everyone can make it.
			if ($access != 3 && ($user_id != $current_user)) {
				echo "denied";
				return;
			} else {
				$new_mod_by = new stdClass();
				$new_mod_by->date = $now;
				$new_mod_by->id = $user_id;

				array_push($mod_by, $new_mod_by);
				
				$columns = array(
				  'title',
				  'description',
				  'mod_by',
				  'params'
				);

				$fields = array(
				  $db->quoteName('title') . '=' . $db->quote($title),
				  $db->quoteName('description') . '=' . $db->quote($description),
				  $db->quoteName('mod_by') . '=' . $db->quote(json_encode($mod_by)),
				  $db->quoteName('params') . '=' . $db->quote(json_encode($params))
				);

				if ($can_change_access) {
					array_push($fields, $db->quoteName("access") . '=' . $db->quote((int)$access));
				}

				$conditions = array(
					$db->quoteName('id') . '=' . (int)$chart_id
				);

				$query = $db->getQuery(true);
				$query
				  ->update($db->quoteName('axs_dashboards_charts'))
				  ->set($fields)
				  ->where($conditions);

				$db->setQuery($query);
				$db->execute();

				$response_type = "update";
				
			}

		} else {
			//It does not exist.  Make a new one.
			$chart_id = null;
		}
	}

	if (!$chart_id) {
		//Create New
		if ($save_type == 'update') {
			//Cannot update a non-existing chart.
			echo "denied";
			return;
		}

		$mod_by = array();
		$new_mod_by = new stdClass();
		$new_mod_by->date = $now;
		$new_mod_by->id = $user_id;
		array_push($mod_by, $new_mod_by);

		$columns = array(
		  'user_id',
		  'title',
		  'description',
		  'access',
		  'mod_by',
		  'params'
		);

		$values = array(
		  (int)$user_id,
		  $db->quote($title),
		  $db->quote($description),
		  (int)$access,
		  $db->quote(json_encode($mod_by)),
		  $db->quote(json_encode($params))
		);

		$query = $db->getQuery(true);
		$query
		  ->insert($db->quoteName('axs_dashboards_charts'))
		  ->columns($db->quoteName($columns))
		  ->values(implode(',', $values));

		$db->setQuery($query);
		$db->execute();

		$chart_id = $db->insertId();
		$response_type = "new";
	}

	$response = new stdClass();
	$response->id = $chart_id;
	$response->type = $response_type;
	$response->params = $params;

	echo json_encode($response);

}

function helperLoadChartData() {
	$user_id = JFactory::getUser()->id;
	$which = JRequest::getVar('which');


	$db = JFactory::getDBO();
	$query = "SELECT * FROM axs_dashboards_charts WHERE ID = $which";
	$db->setQuery($query);
	$result = $db->loadObject();

	if ($result->access == 1 && $result->user_id != $user_id) {
		echo "denied";
	} else {
		$dat = new stdClass();
		$dat->title = $result->title;
		$dat->id = $result->id;
		$dat->params = json_decode($result->params);
		
		if ($result->user_id == $user_id) {
			//They own it.  Full access
			$dat->access = "Y";
		} else {
			if ($result->access == 3) {
				//Public access
				$dat->access = "Y";
			} else {
				$dat->access = "N";
			}
		}
		
		echo json_encode($dat);
	}
}

function helperDeleteChartData() {
	$user_id = JFactory::getUser()->id;
	$chart_id = JRequest::getVar('chart_id');

	$db = JFactory::getDBO();

	$query = "SELECT * FROM axs_dashboards_charts WHERE id=$chart_id";
	$db->setQuery($query);

	$result = $db->loadObject();
	if (!empty($result)) {

		if ($result->user_id == $user_id) {

			$query = "DELETE FROM axs_dashboards_charts WHERE id = $chart_id";
			$db->setQuery($query);
			$db->execute();

			buildSelector('charts');

		} else {
			echo "denied";
			return;
		}

	} else {
		echo "denied";
		return;
	}
}

function helperChartForm() {

	$user_id = JFactory::getUser()->id;

	$db = JFactory::getDBO();
	$query = "SELECT * FROM axs_dashboards_charts WHERE access = 3 OR access = 2 OR user_id = $user_id";
	$all_charts = $db->loadObject();

	//$types = array("a","b","c","d");

	$chart_id = JRequest::getVar("id");

	if ($chart_id) {
		$query = "SELECT * FROM axs_dashboards_charts WHERE id = $chart_id";
		$db->setQuery($query);
		$result = $db->loadObject();
		$params = json_decode($result->params);
	} else {
		$chart_id = "null";
		$params = "null";
	}

	if ($result->user_id == $user_id) {
			//They own it.  Full access
			$access = "Y";
		} else {
			if ($result->access == 3) {
				//Public access
				$access = "Y";
			} else {
				$access = "N";
			}
		}

	?>
		<style>
			.edit_element {
				margin: 			10px;
			}

			.chart_color {
				position: 			relative; 
				border-radius: 		5px; 
				width: 				50px; 
				height: 			25px; 
				background-color: 	rgb(0,0,0);					
			}
		</style>
		
		

		<div id="setup_area" style="width: 100%; height: 20%;">			
			<div style="width: 100%; top: 20%;">
				<div id="chart_setup" style="width: 46%; margin-left: 2%; display: inline-block;">
					<div class="edit_element">
						Chart Title: <input id="chart_title" type="text" placeholder="My Chart" 
							value="<?php
										if ($result) {
											echo $result->title;
										}
									?>"></input>
					</div>
					
					<div class="edit_element">
						Add New Chart: <button id="add_new_chart_button" title="Add New Chart" class="btn btn-primary" style="font-size: 18px;">Add</button>
					</div>
				</div>

				<div id="load_area" style="width: 46%; margin-left: 2%; display: inline-block;">
					<div class="edit_element">
						Load Existing Chart
						<select id="existing_chart_select">
							<?php
								echo buildSelector("charts");
							?>
						</select>
						<br>
						<button id="load_button" class="btn btn-primary">Load</button>
						<button id="delete_button" class="btn btn-danger">Delete</button>
						
						<button id="save_chart" style="float: right;" class="btn btn-success">Update</button>
						<button id="save_chart_new" style="float: right; margin-right: 4px;" class="btn btn-success">Save New</button>
					</div>
				</div>
			</div>
		</div>
		
		<div id="edit_area" style="width: 100%; height: 75%; margin-top: 30px;">
			<div id="left_side" style="width: 48%; margin-left: 1%; height: 95%; float: left;">
				<div id="existing_charts_area" style="width: 100%; height: 100%; overflow-y: scroll; border: 1px solid rgb(160, 160, 160); padding: 2%;">
				</div>
			</div>
			<div id="right_side" style="width: 48%; margin-right: 1%; height: 95%; float: right;">
				<div id="preview_area" style="width: 100%; height: 70%; border: 1px solid rgb(160, 160, 160);">
					<div id="chart_preview" style="width: 100%; height: 100%;"></div>
				</div>
				<div class="edit_element" style="width: 100%; height: 20%">
					Description:
					<br>
					<textarea id="description_field" style="resize: none; width: 100%; height: 80%;"><?php 
						if ($result) {
							echo $result->description;
						}
					?></textarea>
				</div>
				<div class="edit_element" style="width: 100%; height: 10%;">
					Access Level:
					<select id="privacy_settings" onchange="hideShowText(this)">
						<?php
							if ($result) {
								$which = $result->access;
							} else {
								$which = -1;
							}
						?>
						<option value="1" <?php if ($which == 1) { echo "selected";}?>>Private</option>
						<option value="2" <?php if ($which == 2) { echo "selected";}?>>Shared</option>
						<option value="3" <?php if ($which == 3) { echo "selected";}?>>Public</option>
					</select>
					<div style="font-size: 10px; color: rgb(128,128,128);">
						<div style="display: none;" class="text" which="1">
							Only viewable and editable by you.
						</div>
						<div style="display: none;" class="text" which="2">
							Viewable by everyone but only editable by you.
						</div>
						<div style="display: none;" class="text" which="3">
							Viewable and editable by everyone.
						</div>
					</div>
				</div>
			</div>
		</div>

		<script>

			hideShowText(document.getElementById("privacy_settings"));

			var user_id = <?php echo $user_id; ?>;
			var params = <?php echo $params; ?>;
			var chart_id = <?php echo $chart_id; ?>;

			var access_to_save = false;
			if ('<?php echo $access; ?>' == 'Y') {
        		access_to_save = true;
        	}

			var existing_charts_area = document.getElementById("existing_charts_area");
			if (params) {
				for (var i = 0; i < params.length; i++) {
					existing_charts_area.appendChild(createEditCell(params[i]));
				}
			}

			var old_title = document.getElementById("chart_title").value;

			//Check if chart data has updated.
			var cur = new Date();
			var checkTitleStart = cur.getTime();
			var checkTitleNow = cur.getTime();
			var updatedViewForTitle = true;

			var saveButton = document.getElementById("save_chart");
			var saveButtonNew = document.getElementById("save_chart_new");
			saveButton.disabled = true;
			saveButtonNew.disabled = true;
			var currently_loading = false;

			var update_data = setInterval(function() {
				cur = new Date();				
				var title = document.getElementById("chart_title");
				if (title) {					
					var new_chart_title = title.value;					
					if (new_chart_title != old_title) {
						old_title = new_chart_title;
						checkTitleStart = cur.getTime();
						updatedViewForTitle = false;
					} else {
						checkTitleNow = cur.getTime();
						if ((checkTitleNow - checkTitleStart) > 1000) {
							if (!updatedViewForTitle) {
								updatePreview();
								updatedViewForTitle = true;
							}
						}
					}

					var enable_save = true;
					if (new_chart_title == "") {
						enable_save = false;
					}

					if (document.getElementsByClassName("chart_details").length == 0) {
						enable_save = false;
					}

					if (enable_save && !currently_loading) {
						saveButtonNew.disabled = false;
						if (access_to_save) {
							saveButton.disabled = false;
						}
					} else {
						saveButtonNew.disabled = true;
						saveButton.disabled = true;
					}
				} else {
					//Discontinue running the code if there is no title (meaning the modal has been closed.)
					clearInterval(update_data);
				}

				var select_list = document.getElementById("existing_chart_select");

				if (select_list) {
					var selected_option = select_list.options[select_list.selectedIndex];

                    // Make sure we're working with a defined element
                    if(selected_option !== undefined) {
                        var access = selected_option.getAttribute("access");
                        var area = selected_option.parentNode;
                        var delete_button = document.getElementById("delete_button");

                        if (area.getAttribute("label") == "My Charts") {
                            delete_button.disabled = false;
                            delete_button.title = "";
                        } else {
                            delete_button.disabled = true;
                            delete_button.title = "You do not have permission to delete this chart";
                        }
                    }
				} else {
					clearInterval(update_data);
				}
			}, 100);

			var save_chart_data = function(saveType) {

                var chartData = getChartData();

				var title = document.getElementById("chart_title").value;
				var params = JSON.stringify(getChartData());
				var access_select = document.getElementById("privacy_settings");
				var access = access_select.options[access_select.selectedIndex].value;
				var description = document.getElementById("description_field").value;

                // Grab the currently selected chart via the chart selector's current value
                var chart_selector = $('#existing_chart_select');
                var chartId = chart_selector.val();

				jQuery.ajax({
		            type:       'POST',
		            url:        'index.php',
		            data:       {
		                'option':       'com_dashboard',
		                'view':         'dashboard',
		                'task':         'saveChartForm',
		                'format':       'raw',
		                'title': 		title,
		                'access': 		access,
		                'desc': 		description,
		                'params': 		params,
		                'chart_id': 	chartId,
		                'type': 		saveType
		            },
		            success: function(response) {
		            	if (data == 'denied') {
		            		console.log(data);
		            	} else {

			                var data = JSON.parse(response);
			                data.params = JSON.parse(data.params);
			                data.title = title;
			                if (data.type == 'update') {
			                	//update the current chart box
			                	updateChart(data);
			                } else if (data.type == 'new') {
			                	//Create a new box
			                	addChart(data);
			                }
			            }
			            jQuery("#axsModal").modal("hide");		                
		            }
		        });
			}

			jQuery("#save_chart").click(
				function() {					
					save_chart_data('update');
				}
			);

			jQuery("#save_chart_new").click(
				function() {
					save_chart_data('new');
				}
			);

			jQuery("#load_button").click(
				function() {

					var select_list = document.getElementById("existing_chart_select");
					var which = select_list.value;
					var data = null;

					saveButton.disabled = true;
					saveButtonNew.disabled= true;
					currently_loading = true;

					jQuery.ajax({
			            type:       'POST',
			            url:        'index.php',
			            data:       {
			                'option':       'com_dashboard',
			                'view':         'dashboard',
			                'task':         'loadChartData',
			                'format':       'raw',			                
			                'which': 		which
			            },
			            success: function(response) {

			            	data = JSON.parse(response);
			            	if (data.access == 'Y') {
			            		access_to_save = true;
			            	} else {
			            		access_to_save = false;			            		
			            	}

			            	document.getElementById("chart_title").value = data.title;

			            	//updatePreview(JSON.parse(data.params));

			            	var existing_charts_area = document.getElementById("existing_charts_area");
			            	existing_charts_area.innerHTML = "";
			            	var params = JSON.parse(data.params);

							if (params) {
								for (var i = 0; i < params.length; i++) {
									existing_charts_area.appendChild(createEditCell(params[i]));
								}
							}

							currently_loading = false;

							var title = document.getElementById("chart_title").value;
							data.params = JSON.parse(data.params);
			                data.title = title;
			                
			                if (chart_id) {
			                	//update the current chart box
			                	var current = document.getElementById("chart_" + chart_id).parentElement;
			                	data.x = current.getAttribute("data-gs-x");
								data.y = current.getAttribute("data-gs-y");
								data.width = current.getAttribute("data-gs-width");
								data.height = current.getAttribute("data-gs-height");
			                	var grid = jQuery('.grid-stack').data('gridstack');
			                	grid.removeWidget(current);
			                	jQuery(current).remove();
			                	addChart(data);			                	
			                } else {
			                	//Create a new box
			                	addChart(data);
			                }
			            }
			        });
					
				}
			);

			jQuery("#delete_button").click(
				function() {
					var select_list = document.getElementById("existing_chart_select");
					var selected_option = select_list.options[select_list.selectedIndex];
					//if (confirm("Are you sure you want to delete"))
					
					if (confirm("Are you sure you want to permanently delete the chart \"" + selected_option.innerHTML + "\" from the database?\nThis action is not reversable!")) {
						jQuery.ajax({
				            type:       'POST',
				            url:        'index.php',
				            data:       {
				                'option':       'com_dashboard',
				                'view':         'dashboard',
				                'task':         'deleteChartData',
				                'format':       'raw',				                
				                'chart_id': 	selected_option.value
				            },
				            success: function(response) {
				            	if (response == 'denied') {
				            		console.log(response);
				            	} else {
				            		select_list.innerHTML = response;
				            	}
				            }
				        });
					}
				}
			);

			function createEditCell(param) {

				var new_cell = document.createElement("div");
				new_cell.className = "chart_details edit_element";
				jQuery(new_cell).css({
					"border":"1px solid gray",
					"padding":"10px"
				});

				var createOption = function(value, title, selected) {
					var new_option = document.createElement("option");
					new_option.setAttribute("value", value);
					new_option.innerHTML = title;
					if (selected === value) {
						new_option.setAttribute("selected", "selected");
					}

					return new_option;
				}

				var createDataTypeSelect = function() {

					var data_type = document.createElement("div");
					data_type.className = "edit_element";

					var select_list = document.createElement("select");
					select_list.className = "chart_type";
					select_list.setAttribute("onChange", "updatePreview()");

					var current;
					if (param) {
						current = param.field;
					} else {
						current = null;
					}
					
					select_list.appendChild(createOption("revenue_total", "Total Revenue", current));
					select_list.appendChild(createOption("cc_declines", "Credit Card Declines", current));
		            select_list.appendChild(createOption("cc_fails", "Credit Card Fails", current));
		            select_list.appendChild(createOption("cc_recoveries", "Credit Card Recoveries", current));
		            //select_list.appendChild(createOption("downgrades", "Member Downgrades", current));
		            select_list.appendChild(createOption("cancels", "Member Cancels", current));
		            select_list.appendChild(createOption("members_new", "New Members", current));
		            select_list.appendChild(createOption("members_affiliates", "Affiliate Members", current));
		            select_list.appendChild(createOption("members_current", "Current Members", current));
		            select_list.appendChild(createOption("members_grace_period", "Grace Period Members", current));
		            //select_list.appendChild(createOption("reengages", "Member Reengages", current));
		            select_list.appendChild(createOption("refunds", "Refunds", current));
		            select_list.appendChild(createOption("revenue_dues", "Subscriptions", current));
		            select_list.appendChild(createOption("revenue_events", "Event Registrations", current));
		            select_list.appendChild(createOption("revenue_ecommerce", "Ecommerce Store", current));
		            select_list.appendChild(createOption("revenue_upgrades", "Course Purchases", current));
		            select_list.appendChild(createOption("site_visits", "Site Visits", current));

		            data_type.appendChild(document.createTextNode("Chart Data Type"));
		            data_type.appendChild(document.createElement('br'));
		            data_type.appendChild(select_list);

		            return data_type;
				}

				var createColorPicker = function() {
					var color_picker = document.createElement('div');
		            var color_picker_text = document.createElement('div');
		            var color_picker_select = document.createElement('div');

		            color_picker.className = "edit_element";
		            color_picker.appendChild(color_picker_text);
		            color_picker.appendChild(color_picker_select);

		            color_picker_text.appendChild(document.createTextNode("Color:"));

		            var color_bar = document.createElement('input');
		            color_bar.className = "line_color";
		            color_bar.setAttribute("type", "color");
		            color_bar.setAttribute("onchange", "updatePreview()");
		            if (param) {
		            	color_bar.setAttribute("value", param.color);
		            }

		            color_picker_select.appendChild(color_bar);

		            return color_picker;
				}

				var createStylingOptions = function() {
		            var chart_options = document.createElement('div');
		            chart_options.className = "edit_element";

		            var chart_type = document.createElement('div');
		            var chart_styling = document.createElement('div');
		            chart_options.appendChild(chart_type);
		            chart_options.appendChild(chart_styling);

		            var type_select = document.createElement('select');
		            type_select.className = "type_select";
		            type_select.setAttribute("onChange", "updatePreview()");

		            var current_type = null;
		            var current_styling = null;
		            if (param) {

		            	current_type = param.type;
		            	switch (param.type) {
		            		case "area":
		            			current_styling = param.line.style;
		            			break;
		            		default:
		            			current_styling = param.style;
		            			break;
		            	}
		            	
		            }

		            type_select.appendChild(createOption("line", "Line", current_type));
		            type_select.appendChild(createOption("area", "Area", current_type));
		            type_select.appendChild(createOption("column", "Bar", current_type));
		            type_select.appendChild(createOption("donut", "Donut", current_type));

		            chart_type.appendChild(document.createTextNode("Chart Type"));
		            chart_type.appendChild(document.createElement('br'));
		            chart_type.appendChild(type_select);

		            styling_select = document.createElement('select');
		            styling_select.className = "styling_select";
		            styling_select.setAttribute("onChange", "updatePreview()");
		            styling_select.appendChild(createOption("straight", "Straight", current_styling));
		            styling_select.appendChild(createOption("smooth", "Smooth", current_styling));

		            chart_styling.appendChild(document.createTextNode("Styling"));
		            chart_styling.appendChild(document.createElement('br'));
		            chart_styling.appendChild(styling_select);

		            return chart_options;
		        }

	            var createDeleteButton = function() {
	            	var delete_button = document.createElement('button');		            
		            delete_button.className = "remove_button btn btn-danger";
		            delete_button.appendChild(document.createTextNode("Remove"));
		            jQuery(delete_button).css({
		            	"float":"right",
		            	"width":"50px",
		            	"height": "20px",
		            	"font-size": "11px",
		            	"padding":"1px"
		            });

		            jQuery(delete_button).click(
		            	function() {
		            		jQuery(new_cell).remove();
		            		updatePreview();
		            	}
		            );

		            return delete_button;
	            }

	            var createOptionsCell = function() {
		            var options_cell_area = document.createElement('div');
		            jQuery(options_cell_area).css({
		            	"display":"none"
		            });

		            var view_button = document.createElement('div');
		            view_button.className = "btn btn-primary";
		            view_button.appendChild(document.createTextNode("Edit"));
		            jQuery(view_button).css({
		            	"font-size":"11px",
		            	"width":"30",
		            	"height":"21px",
		            	"padding": "2px"
		            });

		            jQuery(view_button).click(
		            	function() {
		            		var options = jQuery(options_cell_area);
		            		var visible = options.is(":visible");
		            		if (visible) {
		            			options.hide(500);
		            		} else {
		            			options.show(500);
		            		}
		            	}
		            );

		            options_cell_area.appendChild(createColorPicker());
		            options_cell_area.appendChild(createStylingOptions());

		            var options_cell = document.createElement('div');
		            options_cell.appendChild(options_cell_area);
		            options_cell.appendChild(view_button);

		            return options_cell;

		        }
	            
	            new_cell.appendChild(createDeleteButton());
	            new_cell.appendChild(createDataTypeSelect());
	            new_cell.appendChild(createOptionsCell());

				return new_cell;
			}

			function getChartData() {
				var data = document.getElementsByClassName("chart_details");

				var options = [];
				for (var i = 0; i < data.length; i++) {

					var select_list = null;

					select_list = data[i].getElementsByClassName("chart_type")[0];
					var chart_type = select_list.value;
					var chart_name = select_list.options[select_list.selectedIndex].innerHTML;

					var color = data[i].getElementsByClassName("line_color")[0].value;					
					var line_type = data[i].getElementsByClassName("type_select")[0].value;
					var line_styling = data[i].getElementsByClassName("styling_select")[0].value;
					
					var newOption = new Object();

					newOption.field = chart_type;
					newOption.name = chart_name;
					newOption.color = color;
					newOption.type = line_type;
					if (line_type == "area") {
						newOption.line = { style: line_styling };
					} else {
						newOption.style = line_styling;
					}					

					options.push(newOption);
					
				}

				return options;
			}

			function hideShowText(select_list) {	            
	            
	            var texts = select_list.parentNode.getElementsByClassName("text");

	            jQuery(texts).hide().each(
	            	function() {
	            		if (this.getAttribute("which") == select_list.value) {
	            			jQuery(this).show();
	            		}
	            	}
	            );	            
	        }
			
			function updatePreview() {

				var series = getChartData();
				
				for (var i = 0; i < series.length; i++) {
					series[i].data = [];
					for (var j = 0; j < 7; j++) {
						series[i].data.push(Math.random() * 20);
					}
				}

				var num = 0;
				var pos = 0;

				var chart_title = document.getElementById("chart_title").value;

	            $("#chart_preview").kendoChart({
	                /*dataSource: {
			            data: dataSet
			        },*/
			        theme: "bootstrap",
			        series: series,
			        categoryAxis: {
			            title: {
			            	text: chart_title
			            }
			        },
			        valueAxis: {
			            labels: {

			            }
			        },
			        pannable: {
			            lock: "y"
			        },
			        zoomable: {
			            mousewheel: {
			                lock: "y"
			            },
			            selection: {
			                lock: "y"
			            }
			        }
	            });
	        }

	        updatePreview();

            jQuery("#add_new_chart_button").click(
            	function() {
            		var chart_details = document.getElementsByClassName("chart_details");
            		var newCell = createEditCell();
            		existing_charts_area.appendChild(newCell);
            		updatePreview();
            	}
            );

			var chart = $("#chart_preview").data("kendoChart");
			setInterval(
				function() {
					chart.resize();
				}, 250
			);

					

		</script>

    <?php
}