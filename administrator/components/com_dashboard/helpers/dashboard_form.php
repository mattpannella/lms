<?php

defined('_JEXEC') or die();

function helperDashboardForm() {
	$new = JRequest::getVar('new');
	$current_user = JFactory::getUser()->id;
	$title = '';
	$description = '';
	$dashboard_id = '';
	$access = '';
	$default = '';
	if(!$new) {
		$id  = JRequest::getVar('id');
		$db  = JFactory::getDbo();
		$query = "SELECT * FROM axs_dashboards WHERE id = $id";
		$db->setQuery($query);
		$dashboard = $db->loadObject();
		$title = $dashboard->title;
		$description = $dashboard->description;
		$dashboard_id = $dashboard->id;
		$access = $dashboard->access;
		$default = $dashboard->default_dashboard;

		if ($access != 3 && ($dashboard->user_id != $current_user)) {
			$error = "You don't have permission to modify this dashboard.";
			echo $error;
			return;
		}
	}

	?>

	<form id="dashboard-form">
		<div class="row mb-3">
			<div class="col-md-3">
				Access Level
			</div>
			<div class="col-md-9">
				<input 
					name="dashboard_title" 
					class="block-input mb-3" 
					type="text" 
					value="<?php echo $title; ?>" 
					placeholder="Dashboard Title..." />
			</div>
		</div>	

		<div class="row mb-3">
			<div class="col-md-3">
				Description
			</div>
			<div class="col-md-9">
				<textarea 
					name="dashboard_description" 
					class="block-input mb-3" 
					placeholder="Dashboard Description..."
				><?php echo $description; ?></textarea>
			</div>
		</div>

		<div class="row mb-3">
			<input 
				name="dashboard_id" 
				class="block-input mb-3" 
				type="hidden" 
				value="<?php echo $dashboard_id; ?>">
			</input>
		</div>

		<div class="row mb-3">
			<div class="col-md-3">
				Access Level
			</div>
			<div class="col-md-9">
				<select id="dashboard_access" name="dashboard_access" onchange="hideShowText(this)">
					<option value="1" <?php if ($access == 1) { echo "selected";}?>>Private</option>
					<option value="2" <?php if ($access == 2) { echo "selected";}?>>Shared</option>
					<option value="3" <?php if ($access == 3) { echo "selected";}?>>Public</option>
				</select>
				<div style="font-size: 10px; color: rgb(128,128,128);">
					<div style="display: none;" class="text" which="1">
						Only viewable and editable by you.
					</div>
					<div style="display: none;" class="text" which="2">
						Viewable by everyone but only editable by you.
					</div>
					<div style="display: none;" class="text" which="3">
						Viewable and editable by everyone.
					</div>
				</div>
			</div>
		</div>

		<div class="row mb-3">
			<div class="col-12">
				Set as main default dashboard for everyone:<br/>
				<fieldset id="default_dashboard" class="btn-group float-end" role="group" aria-label="Set dashboard as default">
					<input
						id="default_dashboard1"
						name="default_dashboard"
						value="1"
						type="radio"
						class="btn-check"
						<?php if($default) { echo 'checked="checked"'; } ?>
					/>		
					<label for="default_dashboard1" class="btn btn-outline-primary">Yes</label>
					<input 
						id="default_dashboard0"
						name="default_dashboard"
						value="0" 
						type="radio"
						class="btn-check"
						<?php if(!$default) { echo 'checked="checked"'; } ?>
					/>			
					<label 
						for="default_dashboard0" 
						class="btn btn-outline-secondary"
					>No</label>
					
				</fieldset>
			</div>
			
		</div>
	</form>

	<br/>

	<span class="btn btn-primary edit-dashboard float-end">Save Settings</span>

	<script>
		hideShowText(document.getElementById("dashboard_access"));
	</script>

	<?php
}

function helperSaveDashboard($form) {
	$db = JFactory::getDbo();
	$data = new stdClass();
	$current_user = JFactory::getUser()->id;
	$mod_by = array();
	if($form->get('dashboard_id') || JRequest::getVar('charts')) {
		$charts = JRequest::getVar('charts');
		if($form->get('dashboard_id')) {
			$data->id = $form->get('dashboard_id', 0);
		} elseif(JRequest::getVar('charts')) {
			$data->id = JRequest::getVar('id');
		}
		$default = (int)$form->get('default_dashboard');
		if($default) {
			$query = "UPDATE axs_dashboards SET default_dashboard = 0 WHERE id != " . $data->id;
			$db->setQuery($query);
			$db->execute();
		}
		$method = 'updateObject';
		$query = "SELECT mod_by, access, user_id FROM axs_dashboards WHERE id = " . $data->id;
		$db->setQuery($query);
		$result = $db->loadObject();

		if ($result) {			
			$mod_by = json_decode($result->mod_by);
			$access = $result->access;
			$user_id = $result->user_id;

			//Check that the user has permission to save/edit this file 1620190
			//	1	-	Private.  	Only the original user can make changes.
			//	2	-	Shared. 	Only the original user can make changes.
			//	3 	-	Public. 	Everyone can make it.
			if ($access != 3 && ($user_id != $current_user)) {
				$error = "You don't have permission to modify this dashboard.";
				echo $error;
				return;
			}

		} 

	} else {
		$method = 'insertObject';
		$data->user_id = $current_user;
		$data->params = '{}';
	}
	//Get the current date
	$query = "SELECT NOW()";
	$db->setQuery($query);
	$now = $db->loadObject()->{'NOW()'};		

	$new_mod_by = new stdClass();
	$new_mod_by->date = $now;
	$new_mod_by->id = $current_user;

	array_push($mod_by, $new_mod_by);

	$data->mod_by = json_encode($mod_by);
	if($charts) {
		$data->params = $charts;
	} else {
		$data->title = $form->get('dashboard_title', 0, 'STRING');
		$data->description = $form->get('dashboard_description', 0, 'STRING');
		$data->access = (int)$form->get('dashboard_access');
		$data->default_dashboard = $default;		
	}
	$update = $db->$method('axs_dashboards', $data, 'id');
	if($update && $method == 'insertObject') {
		$message = $db->insertid();
	}
	elseif($update && $method == 'updateObject') {
		$message = 'Dashboard Was Updated';
	} else {
		$message = 'Something Went Wrong';
	}
	echo $message;
}

function helperThemeSelector() {
	?>
	<div class="container-fluid">
		<div class="row mb-3">
			<div class="col-md-4 text-center">
				Material
				<img src="/media/kendo/thumbs/material.png" class="thumbnail themeChange img-responsive" data-theme="material"/>
			</div>
			<div class="col-md-4 text-center">
				Material Black
				<img src="/media/kendo/thumbs/materialBlack.png" class="thumbnail themeChange img-responsive" data-theme="materialBlack"/>
			</div>
			<div class="col-md-4 text-center">
				Metro
				<img src="/media/kendo/thumbs/metro.png" class="thumbnail themeChange img-responsive" data-theme="metro"/>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-4 text-center">
				Metro Black
				<img src="/media/kendo/thumbs/metroBlack.png" class="thumbnail themeChange img-responsive" data-theme="metroBlack"/>
			</div>
			<div class="col-md-4 text-center">
				Black
				<img src="/media/kendo/thumbs/black.png" class="thumbnail themeChange img-responsive" data-theme="black"/>
			</div>
			<div class="col-md-4 text-center">
				Blue Opal
				<img src="/media/kendo/thumbs/blueOpal.png" class="thumbnail themeChange img-responsive" data-theme="blueOpal"/>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-4 text-center">
				Bootstrap
				<img src="/media/kendo/thumbs/bootstrap.png" class="thumbnail themeChange img-responsive" data-theme="bootstrap"/>
			</div>
			<div class="col-md-4 text-center">
				Flat
				<img src="/media/kendo/thumbs/flat.png" class="thumbnail themeChange img-responsive" data-theme="flat"/>
			</div>
			<div class="col-md-4 text-center">
				High Contrast
				<img src="/media/kendo/thumbs/highContrast.png" class="thumbnail themeChange img-responsive" data-theme="highContrast"/>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-4 text-center">
				Moonlight
				<img src="/media/kendo/thumbs/moonlight.png" class="thumbnail themeChange img-responsive" data-theme="moonlight"/>
			</div>
			<div class="col-md-4 text-center">
				Silver
				<img src="/media/kendo/thumbs/silver.png" class="thumbnail themeChange img-responsive" data-theme="silver"/>
			</div>
			<div class="col-md-4 text-center">
				Uniform
				<img src="/media/kendo/thumbs/uniform.png" class="thumbnail themeChange img-responsive" data-theme="uniform"/>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-4 text-center">
				Fiori
				<img src="/media/kendo/thumbs/fiori.png" class="thumbnail themeChange img-responsive" data-theme="fiori"/>
			</div>
			<div class="col-md-4 text-center">
				Nova
				<img src="/media/kendo/thumbs/nova.png" class="thumbnail themeChange img-responsive" data-theme="nova"/>
			</div>
			<div class="col-md-4 text-center">
				Default
				<img src="/media/kendo/thumbs/default.png" class="thumbnail themeChange img-responsive" data-theme="default"/>
			</div>
		</div>
	</div>
	<?php
}

function buildOptions($results, $title, $id) {
	$html = '';
	if($results) {
		$html .= '<optgroup label="'.$title.'">';
		foreach ($results as $item) {
			$selected = '';
			if($item->id == $id) {
				$selected = 'selected';
			}
			$html .= '<option value="'.$item->id.'" access="'.$item->access.'"'.$selected.'>'.$item->title.'</option>';
		}
		$html .= '</optgroup>';
	}
	return $html;
}

function buildSelector($type) {
	switch ($type) {
		case 'dashboard':
			$table = "axs_dashboards";
			$title = "Dashboards";
			break;

		case 'charts':
			$table = "axs_dashboards_charts";
			$title = "Charts";
			break;
	}
	$db = JFactory::getDbo();
	$id = JRequest::getVar('id');
	$user = JFactory::getUser()->id;
	$html = '';
	$query = "SELECT * FROM $table WHERE user_id = $user ORDER BY title ASC";
	$db->setQuery($query);
	$results = $db->loadObjectList();
	$html .= buildOptions($results, "My $title", $id);

	$query = "SELECT * FROM $table WHERE access = 3 AND user_id != $user ORDER BY title ASC";
	$db->setQuery($query);
	$results = $db->loadObjectList();
	$html .= buildOptions($results, "Public $title", $id);

	$query = "SELECT * FROM $table WHERE access = 2 AND user_id != $user ORDER BY title ASC";
	$db->setQuery($query);
	$results = $db->loadObjectList();
	$html .= buildOptions($results, "Shared $title", $id);

	echo $html;
}

function helperSetDashboardDefault() {
	$db = JFactory::getDbo();
	$user = JFactory::getUser()->id;
	$query = "SELECT * FROM axs_dashboards_defaults WHERE user_id = $user";
	$db->setQuery($query);
	$check = $db->loadObject();

	$method = !empty($check) ? 'updateObject' : 'insertObject';

	$data = new stdClass();
	$data->user_id = $user;
	$data->dashboard_id = JRequest::getVar("id");
	$result = $db->$method("axs_dashboards_defaults", $data, "user_id");
	if($result) {
		echo "This Dashboard is now set as your default.";
	} else {
		echo "Could not set your default dashboard.";
	}
}