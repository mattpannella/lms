<script>jQuery.noConflict();</script>
<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die;

JFactory::getApplication()->input->set('hidemainmenu', false);
$doc = JFactory::getDocument();
$doc->addScript('components/com_dashboard/assets/js/dashboard.js', array('version' => 'auto'));
$doc->addStyleSheet('components/com_dashboard/assets/css/dashboard.css', array('version' => 'auto'));
$doc->addStyleSheet('components/com_dashboard/assets/css/data_box.css', array('version' => 'auto'));
$doc->addStyleSheet('/templates/axs/icons/icons2.css', array('version' => 'auto'));

//require_once("components/com_dashboard/helpers/menu_slot.php");
//require_once("../libraries/axslibs/brands.php");
$theme = "bootstrap";
if(isset($_COOKIE['dashboard_theme'])) {
    $theme = $_COOKIE['dashboard_theme'];
}
$current_user = JFactory::getUser()->id;
$model = $this->getModel();
if(JRequest::getVar("id")) {
	$dashboard = $this->item;
} else {
	$dashboard = $model->getDefault();
}
$dashboard_id = $dashboard->id;

if ($dashboard->access != 3 && ($dashboard->user_id != $current_user)) {
	$canEdit = FALSE;
} else {
	$canEdit = TRUE;
}

$courses = AxsAnalytics::getAllCourses();
if($courses) {
    $coursesTotal = count($courses);
} else {
    $coursesTotal = 0;
}

/*$date = '2019-06-25';
$test = AxsLMS::getCoursesStats($date);
jdump($test);*/
$brand = AxsBrands::getBrand();

$currency_code = 'USD';
$currency_symbol = '$';
if($brand->billing->currency_code) {
	$currency_code = $brand->billing->currency_code;
}
if($brand->billing->currency_symbol) {
	$currency_symbol = $brand->billing->currency_symbol;
}
?>



<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.bootstrap.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.bootstrap.mobile.css" />
<link rel="stylesheet" href="components/com_reports/assets/css/reports.css" />
<link rel="stylesheet" href="/media/gridstack/dist/gridstack.css?v=3"/>

<script src="/media/kendo/js/jquery.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3/css/bootstrap.css" />-->


<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script>
// This fixes Bootstrap 3 from hiding top navigation dropdowns
jQuery(document).on('click', '.dropdown-toggle', function () {
    jQuery(this).parent().show();
    jQuery(this).parent().addClass("open");
});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
<script src="/media/gridstack/dist/gridstack.js?v=3"></script>
<script src="/media/gridstack/dist/gridstack.jQueryUI.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>

<?php

    /*
        By converting dates to strtotime(), it removes any garbage data.
        Also, erroneous dates such as 2016-02-31 will be converted to 2016-03-02
    */

    /*

    $start_date = strtotime(JRequest::getVar('start'));
    $end_date = strtotime(JRequest::getVar('end'));

    $start_date = null;
    $end_date = null;

    //$year_ago = strtotime("-1 year", time());

    // Start date cannot be after end date
    if ($end_date != null && $start_date != null) {
        if ($end_date < $start_date) {
            $end_date = null;
            $start_date = null;
        }
    }

    if ($start_date == false) {
        $start_date = null;
    } else {
        $start_date = date('Y-m-d', $start_date) . " 00:00:00";
    }

    if ($end_date == false || $end_date > strtotime('now')) {
        $end_date = strtotime('now');
    }

    $end_date = date('Y-m-d', $end_date) . " 23:59:59";

    if ($start_date != null && $start_date != 0) {
        //There is a start date
        if ($end_date != null && $end_date != 0) {
            //There is an end date
            $daily_data = AxsAnalytics::getDashboardData($start_date, $end_date);
        } else {
            //There is no end date
            $daily_data = AxsAnalytics::getDashboardData($start_date);
        }
    } else {
        //There is no start date
        if ($end_date != null && $end_date != 0) {
            //There is an end date
            $daily_data = AxsAnalytics::getDashboardData(null, $end_date);
        } else {
            //There is no end date
            $daily_data = AxsAnalytics::getDashboardData();
        }
    }
    */


    $start_date = null;
    $end_date = date('Y-m-d', strtotime('now') . " 23:59:59");

    $daily_data = AxsAnalytics::getDashboardData($start_date, $end_date);

    if(!is_null($daily_data) && (is_array($daily_data) && count($daily_data) !== 0)) {

        $daily_data_encoded = json_encode($daily_data);

        $first_day = $daily_data[0];
        $today = $daily_data[count($daily_data) - 1];

        $start_date = $daily_data[0]->date . " 00:00:00";
    }
?>

<script>
    <?php if($daily_data_encoded === null) : ?>
        var daily_data = [];
    <?php else: ?>
        var daily_data = <?php echo $daily_data_encoded; ?>;
    <?php endif; ?>

    var currency_symbol = '<?php echo $currency_symbol; ?>';

    <?php if(is_null($start_date)) : ?>
        var start_date = null;
    <?php else : ?>
        var start_date = '<?php echo $start_date; ?>';
    <?php endif; ?>

    var end_date = '<?php echo $end_date ?>';

    makeDates();
    connectData();
</script>

<div id="date_range_calendar" class="row">
    <?php /*
    From:
    <span id="from_calendar" class="enter_calendar" style="display: inline-block;">
        <?php
            echo JHTML::calendar($start_date,'calendar_start','calendar_start','%Y-%m-%d', $attribs='style="width: 200px;" form="filters" placeholder="Start"');
        ?>
    </span>
    To:
    <span id="to_calendar" class="enter_calendar" style="display: inline-block;">
        <?php
            echo JHTML::calendar($end_date,'calendar_end','calendar_end','%Y-%m-%d', $attribs='style="width: 200px;" form="filters" placeholder="End"');
        ?>
    </span>
    <button id="calendar_refresh" class="btn btn-primary">
        Refresh
    </button>
    */?>
    <div class="ms-5 dashboard-settings">

        <div class="input-group">
            <span class="input-group-text">Dashboard</span>
            <select
                id="dashboard_selector"
                name="dashboard"
                class="form-select"
            ></select>
        </div>

        <?php
            if($canEdit) {
        ?>
            <button class="btn btn-primary dashboard-settings-btn mt-2 mb-2" type="button">
                <span style="color:white" class="lizicon-cogs big-icon"></span> Settings
            </button>
            <button
                class="btn btn-success"
                id="save-grid"
                href="#"
                ><span class="lizicon-floppy-disk"></span> Save Changes
            </button>
        <?php } ?>

        <div id="dashboard-dropdown" class="dashboard-dropdown-menu">
            <a
                href="index.php?option=com_dashboard&view=dashboards&task=loadDashboardForm&id=<?php echo $dashboard_id; ?>&format=raw&new=1"
                class="settings-btns editDashboard axsmodal"
                data-title="Add New Dashboard"
                data-height="500px"
                data-bs-toggle="modal"
                data-bs-target="#axsModal"
            >
                <span class="lizicon-plus settings-icon"></span> Add New Dashboard
            </a>
        	<?php if($canEdit) { ?>
            <a
                href="index.php?option=com_dashboard&view=dashboards&task=loadDashboardForm&id=<?php echo $dashboard_id; ?>&format=raw&new=0"
                class="settings-btns axsmodal"
                data-title="Edit Dashboard"
                data-height="500px"
                data-bs-toggle="modal"
                data-bs-target="#axsModal"
            >
                <span class="lizicon-cog settings-icon"></span> Dashboard Settings
            </a>
            <a
            href="index.php?option=com_dashboard&view=dashboards&task=loadChartForm&format=raw"
                class="settings-btns editDashboard axsmodal"
                data-title="Add New Chart"
                data-size="xl"
                data-height="800px"
                data-bs-toggle="modal"
                data-bs-target="#axsModal"
            >
                <span class="lizicon-stats-bars settings-icon"></span> Add New Chart
            </a>
            <?php } ?>
            <a
                href="index.php?option=com_dashboard&view=dashboards&task=loadThemeSelector&id=<?php echo $dashboard_id; ?>&format=raw"
                class="settings-btns axsmodal"
                data-title="Dashboard Theme"
                data-height="700px"
                data-bs-toggle="modal"
                data-bs-target="#axsModal"
            >
                <span class="lizicon-insert-template settings-icon"></span> Dashboard Theme
            </a>
            <span class="settings-btns set-default">
                <span class="lizicon-star-full settings-icon"></span> Make My Default
            </span>
        </div>

    </div>
</div>

<div id="com_axs_dashboard">
    <div id="dashboard_view" style='padding: 10px;'>
        <div class='container-fluid'>
            <div class="row totals">

                <!-- COURSES TOTAL -->
                <div class="col-md-3 totals-lifeboards">
                    <div class='stats_bar shadowed' style="background-color: rgb(67, 192, 250);">
                        <div class="totals-inner">
                            <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-ondemand_video"></span></div>
                            <div class="totals-data" id="allCoursesArea"><?php echo $coursesTotal; ?></div>
                            <div class="totals-footer">
                                Courses
                            </div>
                        </div>
                    </div>
                </div>

                <!-- TOTAL REVENUE -->
                <div class="col-md-3 totals-revenue">
                    <div class='stats_bar shadowed' style="background-color: rgb(16, 196, 105);">
                        <div title = '<?php echo "Total revenue made from " . $first_day->date . " to " . $today->date; ?>' class="totals-inner">
                            <div class="totals-icon"><span class="lizicon-credit-card"></span></div>
                            <div class="totals-data" id='totalRevenueArea'>Calculating...</div>
                            <div class="totals-footer">
                                <span id='revenue_expand' class="expand_button pull-left totals-stats" title="Expand for More Info"></span>
                                Total Revenue
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <table class='dashboard_table'>
                                <tr title = '<?php echo "Total revenue from monthly dues made from " . $first_day->date . " to " . $today->date; ?>' >
                                    <td><span class="lizicon-calendar"></span></td>
                                    <td><span id='monthlyDuesRevenueArea'>...</span></td>
                                    <td><span>Monthly Dues</span></td>
                                </tr>
                                <tr title = '<?php echo "Total revenue from membership upgrades made from " . $first_day->date . " to " . $today->date; ?>' >
                                    <td><span class="lizicon-rocket"></span></td>
                                    <td><span id='upgradesRevenueArea'>...</span></td>
                                    <td><span>Upgrades</span></td>
                                </tr>
                                <tr title = '<?php echo "Total revenue from events made from " . $first_day->date . " to " . $today->date; ?>' >
                                    <td><span class="lizicon-mic"></span></td>
                                    <td><span id='eventsRevenueArea'>...</span></td>
                                    <td><span>Events</span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- ACTIVE MEMBERS -->
                <div class="col-md-3 totals-members">
                    <div class='stats_bar shadowed' style="background-color: rgb(255, 189, 74);">
                        <div title='<?php echo "Number of members on " . $today->date; ?>' class="totals-inner">
                            <div class="totals-icon"><span class="lizicon-users"></span></div>
                            <div class="totals-data" id="allActiveArea">Calculating...</div>
                            <div class="totals-footer">
                                Users
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <table class='dashboard_table'>
                                <tr title='<?php echo "Number of current paid members on " . $today->date; ?>'>
                                    <td><span class="lizicon-accessibility"></span></td>
                                    <td><span id="activeMembersArea">...</span></td>
                                    <td><span>Current</span></td>
                                    <?php /*
                                    <td><span class="clickable lizicon-download" title="Download" onclick='exportDataToCSV(this, "activeMembers")'></span></td>
                                    */?>
                                </tr>
                                <tr title='<?php echo "Number of grace period members on " . $today->date; ?>'>
                                    <td><span class="lizicon-mug"></span></td>
                                    <td><span id="activeMembersWithGraceArea">...</span></td>
                                    <td><span>Grace Period</span></td>
                                    <?php /*
                                    <td><span class="clickable lizicon-download" title="Download" onclick='exportDataToCSV(this, "graceMembers")'></span></td>
                                    */?>
                                </tr>
                                <tr title='<?php echo "Number of affiliate members on " . $today->date; ?>'>
                                    <td><span class="lizicon-user-tie"></span></td>
                                    <td><span id="activeAffiliates">...</span></td>
                                    <td><span>Affiliates</span></td>
                                    <?php /*
                                    <td><span class="clickable lizicon-download" title="Download" onclick='exportDataToCSV(this, "affiliateMembers")'></span></td>
                                    */?>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <!-- NEW MEMBERS  -->
                <?php

                    $monthsPrev = 4;
                    $curMonth = date("m", strtotime($today->date));

                    ?>
                        <script>
                            var monthsPrev = <?php echo $monthsPrev ?>
                        </script>
                    <?php

                        $datePrev = array();

                        for ($i = 0; $i <= $monthsPrev; $i++) {
                            $monthNum = "" . ($curMonth - $i);
                            if ($monthNum < 1) {
                                $monthNum += 12;
                            }

                            if ($monthNum < 10) {
                                $monthNum = "0" . $monthNum;
                            }
                            $datePrev[$i] = new stdClass();
                            $datePrev[$i]->monthNum = $monthNum;
                            $datePrev[$i]->monthName = date("M", mktime(0, 0, 0, $monthNum, 1, 0));
                        }

                ?>

                <div class="col-md-3 totals-new-members">
                    <div class='stats_bar shadowed' style="background-color: rgb(240, 80, 80);">
                        <div class="totals-inner">
                            <div class="totals-icon" style="margin-top: 10px;"><span class="lizicon-user-plus"></span></div>
                            <div class="totals-data"><?php echo $datePrev[0]->monthName ?>: <span id="month00NewMemberArea">Calculating...</span></div>
                            <div class="totals-footer">
                                <span id='new_members_expand' class="expand_button pull-left totals-stats" title="Expand for More Info"></span>
                                New Users
                                <?php /*
                                <span class="lizicon-download clickable pull-right" title="Download" onclick='exportDataToCSV(this, "newMembers", <?php echo htmlspecialchars(json_encode($datePrev[0]), ENT_QUOTES) ?>)'></span>
                                */ ?>
                            </div>
                        </div>
                        <div class="totals-lower">
                            <hr>
                            <table class='dashboard_table'>

                                <?php
                                    for ($i = 1; $i <= $monthsPrev; $i++) {
                                        if ($i < 10) {
                                            $monthNum = "0" . $i;
                                        } else {
                                            $monthNum = $i;
                                        }
                                ?>

                                        <tr>
                                            <td><span class="lizicon-calendar"></span></td>
                                            <td><span><?php echo $datePrev[$i]->monthName ?>: </span></td>
                                            <td><span id="month<?php echo $monthNum ?>NewMemberArea">...</span></td>
                                            <?php /*
                                            <td><span class="clickable lizicon-download" title="Download" onclick='exportDataToCSV(this, "newMembers", <?php echo json_encode($datePrev[$i]) ?>)'></span></td>
                                            */ ?>
                                        </tr>

                                <?php } ?>

                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="margin-top:50px;">
                <div class="grid-stack"></div>
            </div>
        </div>
    </div>
</div>

<?php
    JHtml::_('behavior.calendar');

    $db = JFactory::getDBO();

    if (isset($dashboard->params)) {
        $params = json_decode($dashboard->params);
    } else {
        $params = null;
    }

    $chart_id_list = [];
    if ($params) {
        foreach ($params as $param) {
            $chart_id = str_replace("chart_", "", $param->id);
            array_push($chart_id_list, $chart_id);
        }
    }

    if (count($chart_id_list) > 0) {
        $ids = implode(",", $chart_id_list);

        $query = "SELECT * FROM axs_dashboards_charts WHERE id IN (" . $ids . ")";
        $db->setQuery($query);
        $results = $db->loadObjectList();

        $chart_data = new stdClass();
        foreach ($chart_id_list as $chart_id) {
            foreach ($results as $result) {
                if ($result->id == $chart_id) {
                    $data = new stdClass();
                    $which = "chart_" . $chart_id;

                    $data->title = $result->title;
                    $params = json_decode(json_decode($result->params));
                    $data->params = $params;

                    $chart_data->$which = $data;
                }
            }
        }

        $chart_data = array($chart_data);
    } else {
        $chart_data = null;
    }
?>

<script>

    // Trims whitespace from cookie value and returns the value associated with the cookie given
    function readCookie(name) {
        var cookiePrefix = name + "=";
        var cookieArray = document.cookie.split(';');

        cookieArray.forEach(cookie => {
            var cookieTrimmed = cookie.trim();

            if (cookieTrimmed.indexOf(cookiePrefix) == 0) {
                return cookieTrimmed.substring(cookiePrefix.length, cookieTrimmed.length);
            }
        });
        return null;
    }

    function getCurrentTheme() {
        if (readCookie("dashboard_theme")) {
            var theme = readCookie("dashboard_theme");
        } else {
            var theme = '<?php echo $theme; ?>';
        }
        return theme;
    }


    var chartData = <?php echo json_encode($chart_data); ?>;

    var options = {
        cellHeight: 80,
        verticalMargin: 20,
        resizable: {
            handles: 'e, se, s, sw, w'
        },
        draggable: {
            handle: '.dashboard_chart_bar'
        }
    };

    $('.grid-stack').gridstack(options);

    new function () {
        this.serializedData = <?php
            if (isset($dashboard->params)) {
                echo $dashboard->params;
            } else {
                echo "null";
            }
        ?>;

        var theme = "<?php echo $theme; ?>";

        this.grid = $('.grid-stack').data('gridstack');

        this.loadGrid = function () {
            this.grid.removeAll();
            var items = GridStackUI.Utils.sort(this.serializedData);
            _.each(items, function (node) {
                var id = node.id.replace('chart_','');

                this.grid.addWidget(
                    $(`<div><div class="grid-stack-item-content" id="`+node.id+`" /></div></div>`),
                    node.x, node.y, node.width, node.height
                );
                if(chartData){
                    var data = chartData[0][node.id];
                } else {
                    var data = null;
                }
                var params = {
                    data: data,
                    name: node.id,
                    id: id,
                    theme: theme
                }

                makeChart(params);

                setChartHeight(null, node.id, node.height);
            }, this);
            return false;
        }.bind(this);

        this.saveGrid = function () {
            this.serializedData = _.map($('.grid-stack > .grid-stack-item:visible'), function (el) {
                el = $(el);
                var node = el.data('_gridstack_node');
                return {
                    id: el.find('.grid-stack-item-content').attr('id'),

                    x: node.x,
                    y: node.y,
                    width: node.width,
                    height: node.height
                };
            }, this);
            var data = JSON.stringify(this.serializedData);
            $.ajax({
                type: 'POST',
                url: 'index.php?option=com_dashboard&view=dashboards&task=saveDashboardForm&charts='+data+'&id=<?php echo $dashboard_id; ?>&format=raw',
                success: function(response) {
                    if (response) {
                        alert(response);
                    } else {
                        alert(response);
                    }
                }
            });
            return false;
        }.bind(this);

        this.clearGrid = function () {
            this.grid.removeAll();
            return false;
        }.bind(this);

        $('#save-grid').click(this.saveGrid);
        $('#load-grid').click(this.loadGrid);
        $('#clear-grid').click(this.clearGrid);

        this.loadGrid();
    };

    function setChartHeight(el, id, heightUnit) {
        if (el) {
            var id = $(el).find('.grid-stack-item-content').attr('id');
            var heightUnit = $(el).attr('data-gs-height');
        }
        var height = (options.cellHeight - 45) + ((heightUnit - 1) * (options.cellHeight + options.verticalMargin));
        $('#'+id+'-ChartHolder').height(height);
    }

    $(document).on('resizestop', '.grid-stack-item', function() {
        var el = this;
        setTimeout(
            function() {
                setChartHeight(el);
            }, 100
        );
    });

    /*
    var calendar_set = document.getElementById('calendar_refresh');
    $(calendar_set).click(
        function() {
            var calendar_start = document.getElementById('calendar_start').value;
            var calendar_end = document.getElementById('calendar_end').value;

            if (!calendar_start) {
                calendar_start = "0";
            }

            if (!calendar_end) {
                calendar_end = "0";
            }

            window.location = 'index.php?option=com_dashboard&view=default&id=<?php echo $dashboard_id; ?>&start=' + calendar_start + "&end=" + calendar_end;
        }
    );
    */

    $(document).on('click', '.dashboard-settings-btn',
        function() {
            $('#dashboard-dropdown').slideToggle( 300 );
        }
    );

    $(document).on('click', '.edit-dashboard', function() {
        var data = jQuery('#dashboard-form').serialize();
        $.ajax({
            type: 'POST',
            url: 'index.php?option=com_dashboard&view=dashboards&task=saveDashboardForm&data=data&format=raw',
            data: data,
            success: function(response) {

                if (response) {
                    $('#dashboard-dropdown').hide();
                    if (response.length < 6) {
                        window.location ='index.php?option=com_dashboard&view=default&id='+response;
                    }
                    $("#axsModal").modal("hide");
                    loadDashboardSelector();
                } else {
                    alert("Something Went Wrong.");
                }
            }
        });
    });

    function changeTheme(theme) {
        document.cookie = "dashboard_theme=" + theme + "; expires=Wed, 1 Jan 2050 13:47:11 UTC; path=/";
        for(var i = 0; i < chartsList.length; i++) {
            chartsList[i].setOptions({theme : theme});
        }
    }

    $(document).on('click', '.themeChange', function(){
        var theme = $(this).data('theme');
        changeTheme(theme);
    });

    function loadDashboardSelector() {
        $.ajax({
            type: 'GET',
            url: 'index.php?option=com_dashboard&view=dashboards&task=loadDashboardSelector&id=<?php echo $dashboard_id; ?>&format=raw',
            success: function(response) {
                if (response) {
                    $('#dashboard_selector').html(response);
                }
            }
        });
    }

    $(document).on('change', '#dashboard_selector', function() {
        var id = $("#dashboard_selector").val();
        window.location ='index.php?option=com_dashboard&view=default&id='+id;
    });

    loadDashboardSelector();

    function addChart(data) {
        var id = data.id;
        var grid = $('.grid-stack').data('gridstack');
        var chart_id = "chart_"+id;
        var x = 0;
        var y = 0;
        var width = 4;
        var height = 4;
        if(data.x) {
            var x = data.x;
            var y = data.y;
            var width = data.width;
            var height = data.height;
        }
        var theme = getCurrentTheme();
        grid.addWidget(
            $(`<div><div class="grid-stack-item-content" id="`+chart_id+`" /></div></div>`),
            x, y, width, height
        );
        var params = {
            data: data,
            name: chart_id,
            id: id,
            theme: theme
        }
        makeChart(params);
        setChartHeight(null, chart_id, height);
    }

    function updateChart(data) {
        var id = data.id;
        var grid = $('.grid-stack').data('gridstack');
        var chart_id = "chart_"+id;
        var height = $('#'+chart_id+'-ChartHolder').height();
        var theme = getCurrentTheme();

        $("#"+chart_id).html("");
        var params = {
            data: data,
            name: chart_id,
            id: id,
            theme: theme
        }
        makeChart(params);
        $('#'+chart_id+'-ChartHolder').height(height);

    }

    function hideShowText(select_list) {
        var texts = select_list.parentNode.getElementsByClassName("text");
        $(".text").hide().each(
            function() {
                if (this.getAttribute("which") == select_list.value) {
                    $(this).show();
                }
            }
        );
    }

    $(document).on('click', '.set-default', function () {
        $.ajax({
            type: 'POST',
            url: 'index.php?option=com_dashboard&view=dashboards&task=setDashboardDefault&id=<?php echo $dashboard_id; ?>&format=raw',
            success: function(response) {
                if (response) {
                    alert(response);
                }
            }
        });
    });

    $(document).on("hidden.bs.modal", function(e) {
        $('#axsModalLabel').html("");
        $("#axsModal").find(".modal-body").html("");
    });

    $(document).on("click", ".axsmodal", function(e) {
        e.preventDefault();

        const modal = $("#axsModal");

        // reset modal contents
        const modalBody = modal.find(".modal-body");
        modalBody.html("");

        // update modal title
        const modalTitle = $("#axsModal_modal_title");
        modalTitle.html("");
        modalTitle.html($(this).data("title"));

        // apply data-size from .axsmodal elements
        var size  = $(this).data("size");
        modal.find('.modal-dialog').removeClass(function (index, className) {
            return (className.match (/modal-((sm)|(lg)|(xl))/g) || []).join(' ');
        });
        if (size) {
            // expected values for size: sm, lg, xl (md is default)
            $("#axsModal > .modal-dialog").addClass('modal-' + size);
        }

        // load template html into the modal
        $("#axsModal").find(".modal-body").load($(this).attr("href"));
    });

</script>

<?php
    echo JHtml::_(
        'bootstrap.renderModal',
        'axsModal',
        array(
            'title' => 'THIS TITLE IS A PLACEHOLDER!, it will be replaced by .axsmodal onclick callback',
            'footer' => '<button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>'
        )
    );
?>