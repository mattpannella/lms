<?php

	defined('_JEXEC') or die();
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	
	$doc = JFactory::getDocument();
	require_once JPATH_LIBRARIES . '/fof/include.php';
	require_once "components/com_dashboard/helpers/chart_form.php";
	require_once("components/com_dashboard/helpers/dashboard_form.php");
	
	if(!defined('FOF_INCLUDED')) {
		JError::raiseError ('500', 'FOF is not installed');
		return;
	}

	FOFDispatcher::getTmpInstance('com_dashboard')->dispatch();