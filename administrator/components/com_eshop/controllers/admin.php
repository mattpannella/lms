<?php

defined('_JEXEC') or die();

class EShopControllerAdmin extends JControllerLegacy {
	
	public function refund_order() {
		$userData = JRequest::getVar('userData');
		$reason = JRequest::getVar('reason');
		$user_id = JRequest::getVar('userId');

		if (!$userData || !$reason || !$user_id) {
			echo "fail";
			return;
		}

		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_eshop_orders WHERE id = " . $userData['order_id'];
		$db->setQuery($query);

		$results = $db->loadObject();		

		//Some validation
		if ($results->order_number == $userData['order_number']) {
			$query = "UPDATE joom_eshop_orders SET order_status_id = 11 WHERE id = " . $results->id;
			$db->setQuery($query);
			$db->execute();
			echo $results->axs_trxn_id;

			$type_id = 27;
			$data_row = $results->transaction_id;

			AxsExtra::setAdminAction($type_id, $user_id, $data_row, $reason);

		} else {
			echo "fail";
		}
	}

	public function cancel_order() {
		$userData = JRequest::getVar('userData');
		$reason = JRequest::getVar('reason');
		$user_id = JRequest::getVar('userId');

		if (!$userData || !$reason || !$user_id) {
			echo "fail";
			return;
		}

		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_eshop_orders WHERE id = " . $userData['order_id'];
		$db->setQuery($query);

		$results = $db->loadObject();

		//Some validation
		if ($results->order_number == $userData['order_number']) {
			$query = "UPDATE joom_eshop_orders SET order_status_id = 1 WHERE id = " . $results->id;
			$db->setQuery($query);
			$db->execute();
			echo "success";

			$type_id = 28;
			$data_row = $results->id;
			AxsExtra::setAdminAction($type_id, $user_id, $data_row, $reason);

		} else {
			echo "fail";
		}
	}
}