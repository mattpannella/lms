<?php
/**
 * @version		2.4.0
 * @package		Joomla
 * @subpackage	EShop
 * @author  	Giang Dinh Truong
 * @copyright	Copyright (C) 2012 Ossolution Team
 * @license		GNU/GPL, see LICENSE.php
 */
// no direct access
defined( '_JEXEC' ) or die();
?>
<button class="btn btn-light" type="button" onclick="Joomla.submitbutton('products.cancel'); ">
	<?php echo JText::_('ESHOP_CANCEL'); ?>
</button>
<button class="btn btn-primary" type="submit" onclick="Joomla.submitbutton('products.batch');">
	<?php echo JText::_('ESHOP_PROCESS'); ?>
</button>