<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

require_once(JPATH_ADMINISTRATOR.'/components/com_jbusinessdirectory/library/category_lib.php');

class modJBusinessCategoriesOffersHelper {
	
	function getCategories($categoriesIds=array(), $showListingsCount=false) {
		$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
		$categoryService = new JBusinessDirectorCategoryLib();
	
		if(!empty($categoriesIds))
			$categoriesIds = implode(",",$categoriesIds);
		else
			$categoriesIds = null;

		$categories = $categoryService->getCategories(CATEGORY_TYPE_OFFER, $categoriesIds);

		if(!empty($categories)){
			if($appSettings->show_total_business_count && $showListingsCount){
				$details = array();
	
				$details["enablePackages"] = $appSettings->enable_packages;
				$details["showPendingApproval"] =  $appSettings->show_pending_approval==1;
	
				JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_jbusinessdirectory/tables');
				$categoryTable = JTable::getInstance('Category', 'JBusinessTable');
				$listingsCount = $categoryTable->getCountPerCategory($details, CATEGORY_TYPE_OFFER);
	
				foreach($categories as &$category){
					if(!is_array($category)){
						$category = array($category);
						$category["subCategories"] = array();
					}
					if(isset($category[0]->id)) {
						$category[0]->nr_listings = isset($listingsCount[$category[0]->id]->nr_listings) ? $listingsCount[$category[0]->id]->nr_listings : '0';
	
					}
				}
			}
			 
			foreach($categories as &$category){
				if(isset($category[0]->id)){
					$category[0]->link = JBusinessUtil::getOfferCategoryLink($category[0]->id,  $category[0]->alias);
				}
			}
		}
	
		return $categories;
	}

	function getCategoriesByIdsOnMenu($categoriesIds) {
		$categoryService = new JBusinessDirectorCategoryLib();
		$categoriesIds = implode(",",$categoriesIds);
		$categories = $categoryService->getCategories(CATEGORY_TYPE_OFFER, $categoriesIds);
		return $categories;
	}

	function getCategoriesByIdsOnSlider($categoriesIds) {
		$categoryService = new JBusinessDirectorCategoryLib();
		$categoriesIds = implode(",",$categoriesIds);
		$categories = $categoryService->getAllCategories(CATEGORY_TYPE_OFFER, $categoriesIds);
		$newCategories = array();
		foreach ($categories as $category) {
			$newCategories[$category->id] = array($category,"subCategories"=>array());
		}
		return $newCategories;
	}
}
?>
