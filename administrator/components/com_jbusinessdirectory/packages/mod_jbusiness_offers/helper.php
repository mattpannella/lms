<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );


abstract class modJBusinessOffersHelper
{
    public static function getList($params){

        $appSettings = JBusinessUtil::getInstance()->getApplicationSettings();

        $searchDetails = array();

        $categoriesIds = $params->get('categoryIds');
        if(isset($categoriesIds) && count($categoriesIds)>0 && $categoriesIds[0]!= 0 && $categoriesIds[0]!= "") {
            $searchDetails["categoriesIds"] = $categoriesIds;
        }

        $featured  = $params->get('only_featured');
        if(isset($featured))
            $searchDetails["featured"] = $featured;

        $ordering = $params->get('order');
        if($ordering == 1) {
            $orderBy ="co.created desc";
        } elseif ($ordering == 2) {
            $orderBy ="co.id desc";
        } elseif ($ordering == 3) {
            $orderBy ="co.subject asc";
        } else {
            $orderBy = " rand() ";
        }

        $nrResults = $params->get('count');

        $searchDetails["enablePackages"] = $appSettings->enable_packages;
        $searchDetails["showPendingApproval"] = $appSettings->show_pending_approval;
        $searchDetails["orderBy"] = $orderBy;
        $searchDetails["citySearch"] = $params->get('city');
        $searchDetails["regionSearch"] = $params->get('region');
        $searchDetails["featured"] = $params->get('only_featured');
        
        $radius = $params->get('radius');
        $latitude = JRequest::getVar("latitude");
        $longitude = JRequest::getVar("longitude");

        $searchDetails["radius"] = $radius;
        $searchDetails["latitude"] = $latitude;
        $searchDetails["longitude"] = $longitude;

        JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_jbusinessdirectory/tables');
        $offersTable = JTable::getInstance("Offer", "JTable");
        $offers =  $offersTable->getOffersByCategories($searchDetails, 0, $nrResults);

        foreach($offers as $offer){
        	$offer->picture_path = str_replace(" ", "%20", $offer->picture_path);
            switch($offer->view_type){
                case 1:
                    $offer->link = JBusinessUtil::getOfferLink($offer->id, $offer->alias);
                    break;
                case 2:
                    $itemId = JRequest::getVar('Itemid');
                    $offer->link = JRoute::_("index.php?option=com_content&view=article&Itemid=$itemId&id=".$offer->article_id);
                    break;
                case 3:
                    $offer->link = $offer->url;
                    break;
                default:
                    $offer->link = JBusinessUtil::getOfferLink($offer->id, $offer->alias);
            }
            $offer->name = $offer->subject;
            $offer->logoLocation = $offer->picture_path;
        }
        
        if($appSettings->enable_multilingual){
        	JBusinessDirectoryTranslations::updateOffersTranslation($offers);
        }
        
        return $offers;
    }
}
?>
