<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

abstract class modJBusinessEventsHelper
{
    public static function getList($params){

        $appSettings = JBusinessUtil::getInstance()->getApplicationSettings();

        $searchDetails = array();

        $categoriesIds = $params->get('categoryIds');
        if(isset($categoriesIds) && count($categoriesIds)>0 && $categoriesIds[0]!= 0 && $categoriesIds[0]!= "") {
            $searchDetails["categoriesIds"] = $categoriesIds;
        }

        $ordering = $params->get('order');
        if($ordering == 1) {
            $orderBy ="co.created desc";
        } elseif ($ordering == 2) {
            $orderBy ="co.id desc";
        } elseif ($ordering == 3) {
            $orderBy ="co.name asc";
        } else {
            $orderBy = " rand() ";
        }

        $nrResults = $params->get('count');

        $searchDetails["enablePackages"] = $appSettings->enable_packages;
        $searchDetails["showPendingApproval"] = $appSettings->show_pending_approval;
        $searchDetails["orderBy"] = $orderBy;
        $searchDetails["featured"] = $params->get('only_featured');
        $searchDetails["citySearch"] = $params->get('city');
        $searchDetails["regionSearch"] = $params->get('region');

        $radius = $params->get('radius');
        $latitude = JRequest::getVar("latitude");
        $longitude = JRequest::getVar("longitude");

        $searchDetails["radius"] = $radius;
        $searchDetails["latitude"] = $latitude;
        $searchDetails["longitude"] = $longitude;

        JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_jbusinessdirectory/tables');
        $eventsTable = JTable::getInstance("Event", "JTable");
        $events =  $eventsTable->getEventsByCategories($searchDetails, 0, $nrResults);

        foreach($events as $event){
            $event->link = JBusinessUtil::getEventLink($event->id, $event->alias, true);
            $event->picture_path = str_replace(" ", "%20", $event->picture_path);
            $event->logoLocation = $event->picture_path;
//          $event->mainCategoryLink = JBusinessUtil::getEventCategoryLink($event->mainCategoryId, $event->mainCategoryAlias);
        }
        
        if($appSettings->enable_multilingual){
        	JBusinessDirectoryTranslations::updateEventsTranslation($events);
        	JBusinessDirectoryTranslations::updateEventTypesTranslation($events);
        }
        
        return $events;
    }
}
?>
