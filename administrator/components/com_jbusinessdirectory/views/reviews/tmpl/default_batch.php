<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2007 - 2015 CMS Junkie. All rights reserved.
 * @license     GNU General Public License version 2 or later;
 */

defined('_JEXEC') or die;

$published = $this->state->get('filter.published');

$batchStatusLabelTooltip = "<strong>Set Approval Status</strong><br />Not making a selection will keep the original approval status when processing.";
$batchStateLabelTooltip = "<strong>Set State</strong><br />Not making a selection will keep the original state when processing.";

ob_start(); 
?>
    <div class="row mb-3">
        <div class="col-sm-12">
            <p><?php echo JText::_('COM_JBUSINESSDIRECTORY_REVIEW_BATCH_TIP'); ?></p>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-sm-6">
            <label
                id="batch-status-lbl" 
                for="batch-approval_status_id"
                title="<?php echo $batchStatusLabelTooltip ?>"
                data-bs-toggle="tooltip"
                data-bs-html="true"
            >
                <?php echo JText::_("LNG_SET_APPROVAL_STATUS")?>
            </label>
        </div>
        <div class="col-sm-6">
            <select
                id="batch-approval_status_id"
                class="inputbox"
                name="batch[approval_status_id]"
            >
                <option value="">
                    <?php echo JText::_("LNG_KEEP_ORIGINAL_APPROVAL_STATUS")?>
                </option>
                <?php 
                    echo JHtml::_(
                        'select.options', 
                        $this->statuses, 
                        'value', 
                        'text', 
                        $this->state->get('filter.status_id')
                    );
                ?>
            </select>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col-sm-6">
            <label
                id="batch-state-lbl" 
                for="batch-state_id" 
                title="<?php echo $batchStateLabelTooltip ?>"
                data-bs-toggle="tooltip"
                data-bs-html="true"
            >
                <?php echo JText::_("LNG_SET_STATE")?>
            </label>
        </div>
        <div class="col-sm-6">
            <select id="batch-state-id" class="inputbox" name="batch[state_id]">
                <option value=""><?php echo JText::_("LNG_KEEP_ORIGINAL_STATE")?></option>
                <?php 
                    echo JHtml::_(
                        'select.options', 
                        $this->states,
                        'value',
                        'text',
                        $this->state->get('filter.state_id')
                    );
                ?>
            </select>
        </div>
    </div>
<?php
$modalBody = ob_get_clean();

ob_start();
?>
    <button 
        onclick="
            document.getElementById('batch-approval_status_id').value='';
            document.getElementById('batch-featured-status-id').value='';
            document.getElementById('batch-state-id').value='';
        " 
        data-bs-dismiss="modal"
        class="btn btn-danger" 
        type="button" 
    >
        <?php echo JText::_('JCANCEL'); ?>
    </button>
    <button
        onclick="Joomla.submitbutton('review.batch');"
        class="btn btn-primary" 
        type="submit" 
    >
        <?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
    </button>
<?php
$modalFooter = ob_get_clean();

echo JHtml::_(
    'bootstrap.renderModal',
    'collapseModal',
    array(
        'title' => JText::_('COM_JBUSINESSDIRECTORY_REVIEW_BATCH_OPTIONS'),
        'footer' => $modalFooter
    ),
    $modalBody
)

?>
