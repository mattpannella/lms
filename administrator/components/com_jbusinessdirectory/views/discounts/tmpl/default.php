<?php 
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.multiselect');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task != 'discounts.delete' || confirm("<?php echo JText::_('ARE_YOU_SURE_YOU_WANT_TO_DELETE', true);?>")) {
			Joomla.submitform(task);
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=discounts');?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar">
			<div class="filter-search btn-group float-start">
				<label class="filter-search-lbl d-none" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
				<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />
				<button class="btn btn-light" data-bs-toggle="tooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn btn-light" data-bs-toggle="tooltip" type="button" onclick="document.getElementById('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
			</div>
			<div class="hidden-phone float-end me-2">
				<label for="limit" class="d-none"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="filter-select float-end me-2">
				<select name="filter_state_id" class="inputbox input-medium" onchange="this.form.submit()">
					<option value=""><?php echo JText::_('LNG_JOPTION_SELECT_STATE');?></option>
					<?php echo JHtml::_('select.options', $this->states, 'value', 'text', $this->state->get('filter.state_id'));?>
				</select>
			</div>
			<div class="filter-select float-end me-2">
				<select name="filter_package_id" class="inputbox input-medium" onchange="this.form.submit()">
					<option value=""><?php echo JText::_('LNG_JOPTION_SELECT_PACKAGE');?></option>
					<?php echo JHtml::_('select.options', $this->packages, 'id', 'name', $this->state->get('filter.package_id'));?>
				</select>
			</div>
		</div>
	</div>
	<div class="clr clearfix"></div>
	<table class="table table-striped adminlist" id="itemList">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">#</th>
				<th width="1%" class="hidden-phone">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th width='5%' ><?php echo JHtml::_('grid.sort', 'LNG_STATE', 'co.state', $listDirn, $listOrder); ?></th>
				<th width='23%' ><?php echo JHtml::_('grid.sort', 'LNG_NAME', 'co.name', $listDirn, $listOrder); ?></th>
				<th width='13%' ><?php echo JHtml::_('grid.sort', 'LNG_CODE', 'co.code', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone" width='10%' ><?php echo JHtml::_('grid.sort', 'LNG_START_DATE', 'co.start_date', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone" width='10%' ><?php echo JHtml::_('grid.sort', 'LNG_END_DATE', 'co.end_date', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone" width='23%' ><?php echo JText::_("LNG_PACKAGES")?></th>
				<th class="hidden-phone" width='10%' ><?php echo JHtml::_('grid.sort', 'LNG_VALUE', 'co.value', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone" nowrap width='1%' ><?php echo JHtml::_('grid.sort', 'LNG_ID', 'co.id', $listDirn, $listOrder); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php $nrcrt = 1; $i=0;
			foreach( $this->items as $discount) { ?>
				<TR class="row<?php echo $i % 2; ?>">
					<TD class="center hidden-phone"><?php echo $nrcrt++?></TD>
					<TD align=center class="hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $discount->id); ?>
					</TD>
					<td align=center>
						<img class="cursor-pointer"
							src ="<?php echo JURI::base() ."components/".JBusinessUtil::getComponentName()."/assets/img/".($discount->state==0? "no_joomla.png" : "yes_joomla.png")?>"
							onclick="document.location.href = '<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=discount.chageState&id='. $discount->id )?> '"
						/>
					</td>
					<TD align=left><a
						href='<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=discount.edit&id='. $discount->id )?>'
						title="<?php echo JText::_('LNG_CLICK_TO_EDIT'); ?>"> <B><?php echo $discount->name?>
						</B>
					</a>
					</TD>
					<td >
						<?php echo $discount->code ?>
					</td>
					<td class="hidden-phone">
						<?php echo JBusinessUtil::getDateGeneralFormat($discount->start_date) ?>
					</td>
					<td class="hidden-phone">
						<?php echo JBusinessUtil::getDateGeneralFormat($discount->end_date) ?>
					</td>
					<td class="hidden-phone" align="center">
						<?php echo $discount->packageNames?>
					</td>
					<td class="hidden-phone">
						<?php echo $discount->value ?> <?php if($discount->price_type==2) echo "%"?>
					</td>
					<td class="hidden-phone">
						<?php echo $discount->id?>
					</td>
				</TR>
			<?php
				$i++;
			} ?>
		</tbody>
	</table>

	<input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
	<input type="hidden" name="task" value="" /> 
	<input type="hidden" name="companyId" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHTML::_('form.token'); ?> 
</form>