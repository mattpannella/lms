<fieldset class="container-fluid">
	<legend><?php echo JText::_('LNG_EVENTS'); ?></legend>
	<div class="row mb-3">
		<div class="col-md-3"><label id="enable_events-lbl" for="enable_events" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_EVENTS');?></strong><br/><?php echo JText::_('LNG_ENABLE_EVENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_EVENTS'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="enable_events_fld" >
				<input type="radio" class="validate[required] btn-check" name="enable_events" id="enable_events1" value="1" <?php echo $this->item->enable_events==true? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="enable_events1"><?php echo JText::_('LNG_YES')?></label> 
				<input type="radio" class="validate[required] btn-check" name="enable_events" id="enable_events0" value="0" <?php echo $this->item->enable_events==false? 'checked="checked"' :""?> />
				<label class="btn btn-outline-secondary" for="enable_events0"><?php echo JText::_('LNG_NO')?></label> 
			</fieldset>
		</div>
	</div>
	
	<div class="row mb-3">
		<div class="col-md-3"><label id="max_events-lbl" for="max_events" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_EVENTS');?></strong><br/><?php echo JText::_('LNG_MAX_EVENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_EVENTS'); ?></label></div>
		<div class="col-md-9">
			<input type="text" size="40" maxlength="20"  id="max_events" name="max_events" value="<?php echo $this->item->max_events ?>">
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-3"><label id="enable_search_filter_events-lbl" for="enable_search_filter_events" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_SEARCH_FILTER_EVENTS');?></strong><br/><?php echo JText::_('LNG_ENABLE_SEARCH_FILTER_EVENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_SEARCH_FILTER_EVENTS'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="enable_search_filter_events_fld" >
				<input type="radio" class="validate[required] btn-check" name="enable_search_filter_events" id="enable_search_filter_events1" value="1" <?php echo $this->item->enable_search_filter_events==true? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="enable_search_filter_events1"><?php echo JText::_('LNG_YES')?></label> 
				<input type="radio" class="validate[required] btn-check" name="enable_search_filter_events" id="enable_search_filter_events0" value="0" <?php echo $this->item->enable_search_filter_events==false? 'checked="checked"' :""?> />
				<label class="btn btn-outline-secondary" for="enable_search_filter_events0"><?php echo JText::_('LNG_NO')?></label> 
			</fieldset>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-3"><label id="events_search_view-lbl" for="events_search_view" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_DEFAULT_EVENTS_VIEW');?></strong><br/><?php echo JText::_('LNG_DEFAULT_EVENTS_VIEW_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_DEFAULT_EVENTS_VIEW"); ?></label></div>
		<div class="col-md-9">
			<fieldset id="enable_packages_fld" >
				<input type="radio" class="validate[required] btn-check" name="events_search_view" id="events_search_view1" value="1" <?php echo $this->item->events_search_view==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="events_search_view1"><?php echo JText::_('LNG_GRID')?></label> 
				<input type="radio" class="validate[required] btn-check" name="events_search_view" id="events_search_view0" value="2" <?php echo $this->item->events_search_view==2? 'checked="checked"' :""?> />
				<label class="btn btn-outline-secondary" for="events_search_view0"><?php echo JText::_('LNG_LIST')?></label> 
			</fieldset>
		</div>
	</div>
	<div class="row mb-3" style="display:none">
		<div class="col-md-3"><label id="enable_event_reservation-lbl" for="enable_event_reservation" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_EVENT_RESERVATION');?></strong><br/><?php echo JText::_('LNG_ENABLE_EVENT_RESERVATION_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_ENABLE_EVENT_RESERVATION"); ?></label></div>
		<div class="col-md-9">
			<fieldset id="enable_event_reservation_fld" >
				<input type="radio" class="validate[required] btn-check" name="enable_event_reservation" id="enable_event_reservation1" value="1" <?php echo $this->item->enable_event_reservation==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="enable_event_reservation1"><?php echo JText::_('LNG_YES')?></label>
				<input type="radio" class="validate[required] btn-check" name="enable_event_reservation" id="enable_event_reservation0" value="0" <?php echo $this->item->enable_event_reservation==0? 'checked="checked"' :""?> />
				<label class="btn btn-outline-secondary" for="enable_event_reservation0"><?php echo JText::_('LNG_NO')?></label>
			</fieldset>
		</div>
	</div>
</fieldset>

<fieldset class="container-fluid">
	<legend><?php echo JText::_('LNG_SEARCH'); ?></legend>
	<div class="row mb-3">
		<div class="col-md-3"><label id="order_search_events-lbl" for="order_search_events" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ORDER_SEARCH_EVENTS');?></strong><br/><?php echo JText::_('LNG_ORDER_SEARCH_EVENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ORDER_SEARCH_EVENTS'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="order_search_events_fld" >
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events1" value="" <?php echo $this->item->order_search_events==""? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events1"><?php echo JText::_('LNG_RELEVANCE')?></label> 
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events2" value="name" <?php echo $this->item->order_search_events=="name"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events2"><?php echo JText::_('LNG_NAME')?></label> 
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events3" value="city" <?php echo $this->item->order_search_events=="city"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events3"><?php echo JText::_('LNG_CITY')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events4" value="rand()" <?php echo $this->item->order_search_events=="rand()"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events4"><?php echo JText::_('LNG_RANDOM')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events5" value="id desc" <?php echo $this->item->order_search_events=="id desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events5"><?php echo JText::_('LNG_LAST_ADDED')?></label><br/>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events6" value="id asc" <?php echo $this->item->order_search_events=="id asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events6"><?php echo JText::_('LNG_FIRST_ADDED')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events7" value="start_date asc" <?php echo $this->item->order_search_events=="start_date asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events7"><?php echo JText::_('LNG_EARLIEST_DATE')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events8" value="start_date desc" <?php echo $this->item->order_search_events=="start_date desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events8"><?php echo JText::_('LNG_LATEST_DATE')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_events" id="order_search_events9" value="distance asc" <?php echo $this->item->order_search_events=="distance asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-primary" for="order_search_events9"><?php echo JText::_('LNG_DISTANCE')?></label>
			</fieldset>
		</div>
	</div>
</fieldset>
	