		<fieldset class="container-fluid">
			<legend><?php echo JText::_('LNG_SEO_SETTINGS'); ?></legend>
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_seo-lbl" for="enable_seo" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_SEO');?></strong><br/><?php echo JText::_('LNG_ENABLE_SEO_SEO');?>" title=""><?php echo JText::_('LNG_ENABLE_SEO'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_seo_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="enable_seo" id="enable_seo1" value="1" <?php echo $this->item->enable_seo==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_seo1"><?php echo JText::_('LNG_YES')?></label> 
						<input class="btn-check" type="radio" class="validate[required]" name="enable_seo" id="enable_seo0" value="0" <?php echo $this->item->enable_seo==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_seo0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="menu_item_id-lbl" for="menu_item_id" class="hasTooltip required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MENU_ITEM_ID');?></strong><br/><?php echo JText::_('LNG_MENU_ITEM_ID_SEO');?>" title=""><?php echo JText::_('LNG_MENU_ITEM_ID'); ?></label></div>
				<div class="col-md-9"><input name="menu_item_id" id="menu_item_id" value="<?php echo $this->item->menu_item_id?>" size="50" maxlength="10" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="listing_url_type-lbl" for="listing_url_type_fld" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_URL_TYPE');?></strong><br/><?php echo JText::_('LNG_URL_TYPE_SEO');?>" title=""><?php echo JText::_('LNG_URL_TYPE'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="listing_url_type_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="listing_url_type" id="listing_url_type1" value="1" <?php echo $this->item->listing_url_type==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="listing_url_type1"><?php echo JText::_('LNG_SIMPLE')?></label> 
						<input class="btn-check" type="radio" class="validate[required]" name="listing_url_type" id="listing_url_type2" value="2" <?php echo $this->item->listing_url_type==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="listing_url_type2"><?php echo JText::_('LNG_CATEGORY')?></label> 
						<input class="btn-check" type="radio" class="validate[required]" name="listing_url_type" id="listing_url_type3" value="3" <?php echo $this->item->listing_url_type==3? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="listing_url_type3"><?php echo JText::_('LNG_REGION')?></label> 
					</fieldset>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="category_url_type-lbl" for="enable_packages" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_CATEGORY_URL_TYPE');?></strong><br/><?php echo JText::_('LNG_CATEGORY_URL_TYPE_SEO');?>" title=""><?php echo JText::_('LNG_CATEGORY_URL_TYPE'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="category_url_type_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="category_url_type" id="category_url_type1" value="1" <?php echo $this->item->category_url_type==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="category_url_type1"><?php echo JText::_('LNG_KEYWORD')?></label>
						<input class="btn-check" type="radio" class="validate[required]" name="category_url_type" id="category_url_type2" value="2" <?php echo $this->item->category_url_type==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="category_url_type2"><?php echo JText::_('LNG_SIMPLE')?></label>
					</fieldset>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_menu_alias_url-lbl" for="enable_packages" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADD_MENU_ALIAS_URL');?></strong><br/><?php echo JText::_('LNG_ADD_MENU_ALIAS_URL_SEO');?>" title=""><?php echo JText::_('LNG_ADD_MENU_ALIAS_URL'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_menu_alias_url_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="enable_menu_alias_url" id="enable_menu_alias_url1" value="1" <?php echo $this->item->enable_menu_alias_url==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_menu_alias_url1"><?php echo JText::_('LNG_YES')?></label>
						<input class="btn-check" type="radio" class="validate[required]" name="enable_menu_alias_url" id="enable_menu_alias_url0" value="0" <?php echo $this->item->enable_menu_alias_url==0? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_menu_alias_url0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="url_menu_alias-lbl" for="url_menu_alias" class="hasTooltip required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_URL_MENU_ALIAS_SEO');?></strong><br/><?php echo JText::_('LNG_URL_MENU_ALIAS_SEO');?>" title=""><?php echo JText::_('LNG_URL_MENU_ALIAS_SEO'); ?></label></div>
				<div class="col-md-9"><input name="url_menu_alias" id="url_menu_alias" value="<?php echo $this->item->url_menu_alias?>" size="50" maxlength="10" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="add_url_id-lbl" for="add_url_id" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADD_URL_ID');?></strong><br/><?php echo JText::_('LNG_ADD_URL_ID_SEO');?>" title=""><?php echo JText::_('LNG_ADD_URL_ID'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="add_url_id_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="add_url_id" id="add_url_id1" value="1" <?php echo $this->item->add_url_id==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="add_url_id1"><?php echo JText::_('LNG_YES')?></label> 
						<input class="btn-check" type="radio" class="validate[required]" name="add_url_id" id="add_url_id0" value="0" <?php echo $this->item->add_url_id==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="add_url_id0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="add_url_language-lbl" for="add_url_language" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADD_URL_LANGUAGE');?></strong><br/><?php echo JText::_('LNG_ADD_URL_LANGUAGE_SEO');?>" title=""><?php echo JText::_('LNG_ADD_URL_LANGUAGE'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="add_url_language_fld" >
						<input class="btn-check" type="radio" class="validate[required]" name="add_url_language" id="add_url_language1" value="1" <?php echo $this->item->add_url_language==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="add_url_language1"><?php echo JText::_('LNG_YES')?></label> 
						<input class="btn-check" type="radio" class="validate[required]" name="add_url_language" id="add_url_language0" value="0" <?php echo $this->item->add_url_language==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="add_url_language0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
		</fieldset>
