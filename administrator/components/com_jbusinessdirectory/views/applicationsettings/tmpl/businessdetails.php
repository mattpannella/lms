<div class="row">
	<div class="col-lg-6 general-settings">
		<fieldset>
			<legend><?php echo JText::_('LNG_COMPANY_DETAILS'); ?></legend>
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_name-lbl" for="company_name" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_NAME');?></strong><br/><?php echo JText::_('LNG_NAME_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_NAME'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="company_name" id="company_name" value="<?php echo $this->item->company_name?>" size="50" maxlength="255" type="text"></div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_EMAIL');?></strong><br/><?php echo JText::_('LNG_EMAIL_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_EMAIL'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="company_email" id="company_email" value="<?php echo $this->item->company_email?>" size="50" maxlength="255" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="facebook-lbl" for="facebook" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_FACEBOOK');?></strong><br/><?php echo JText::_('LNG_FACEBOOK_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_FACEBOOK'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="facebook" id="facebook" value="<?php echo $this->item->facebook?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_TWITTER');?></strong><br/><?php echo JText::_('LNG_TWITTER_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_TWITTER'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="twitter" id="twitter" value="<?php echo $this->item->twitter?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_GOOGLE_PLUS');?></strong><br/><?php echo JText::_('LNG_GOOGLE_PLUS_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_GOOGLE_PLUS'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="googlep" id="googlep" value="<?php echo $this->item->googlep?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_LINKEDIN');?></strong><br/><?php echo JText::_('LNG_LINKEDIN_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_LINKEDIN'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="linkedin" id="linkedin" value="<?php echo $this->item->linkedin?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_YOUTUBE');?></strong><br/><?php echo JText::_('LNG_YOUTUBE_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_YOUTUBE'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="youtube" id="youtube" value="<?php echo $this->item->youtube?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_LOGO');?></strong><br/><?php echo JText::_('LNG_LOGO_BUSINESS_DETAILS');?>" title=""><?php echo JText::_('LNG_LOGO'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9">
					<div class="form-upload-elem">
						<div class="form-upload">
							<input type="hidden" name="logo" id="imageLocation" value="<?php echo $this->item->logo?>">
							<input type="file" id="imageUploader" name="uploadfile" size="50">
							<div class="clear"></div>
							<a href="javascript:removeLogo();"><?php echo JText::_("LNG_REMOVE")?></a>
						</div>

					</div>
					<div class="picture-preview-settings" id="picture-preview">
							<?php
								if(!empty($this->item->logo)) {
									echo "<img  id='logoImg' src='".JURI::root().PICTURES_PATH.$this->item->logo."'/>";
								}
							?>
						</div>
				</div>
			</div>
		</fieldset>
	</div>

    <div class="col-lg-6 general-settings">
        <fieldset>
            <legend><?php echo JText::_('LNG_INVOICE_INFORMATION'); ?></legend>

            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_company_name-lbl" for="invoice_company_name" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_NAME');?></strong><br/><?php echo JText::_('LNG_NAME_INVOICE');?>" title=""><?php echo JText::_('LNG_NAME'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><input name="invoice_company_name" id="invoice_company_name" value="<?php echo $this->item->invoice_company_name?>" size="50" maxlength="100" type="text"></div>
            </div>

            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_company_address-lbl" for="invoice_company_address" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADDRESS');?></strong><br/><?php echo JText::_('LNG_ADDRESS_INVOICE');?>" title=""><?php echo JText::_('LNG_ADDRESS'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><input name="invoice_company_address" id="invoice_company_address" value="<?php echo $this->item->invoice_company_address?>" size="50" maxlength="75" type="text"></div>
            </div>

            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_company_phone-lbl" for="invoice_company_phone" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_TELEPHONE_NUMBER');?></strong><br/><?php echo JText::_('LNG_TELEPHONE_NUMBER_INVOICE');?>" title=""><?php echo JText::_('LNG_TELEPHONE_NUMBER'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><input name="invoice_company_phone" id="invoice_company_phone" value="<?php echo $this->item->invoice_company_phone?>" size="50" maxlength="75" type="text"></div>
            </div>

            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_company_email-lbl" for="invoice_company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_EMAIL');?></strong><br/><?php echo JText::_('LNG_EMAIL_NUMBER_INVOICE');?>" title=""><?php echo JText::_('LNG_EMAIL'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><input name="invoice_company_email" id="invoice_company_email" value="<?php echo $this->item->invoice_company_email?>" size="50" maxlength="75" type="text"></div>
            </div>

            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_vat-lbl" for="invoice_vat" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_VAT_NUMBER');?></strong><br/><?php echo JText::_('LNG_VAT_NUMBER_NUMBER_INVOICE');?>" title=""><?php echo JText::_('LNG_VAT_NUMBER'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><input name="invoice_vat" id="invoice_vat" value="<?php echo $this->item->invoice_vat?>" size="50" maxlength="75" type="text"></div>
            </div>
		
			<div class="row mb-3">
				<div class="col-md-3"><label id="vat-lbl" for="vat" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_VAT');?></strong><br/><?php echo JText::_('LNG_VAT_NUMBER_INVOICE');?>" title=""><?php echo JText::_('LNG_VAT'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size=40 maxlength=20  id="vat" name = "vat" value="<?php echo $this->item->vat?>">
				</div>
			</div>
            <div class="row mb-3">
                <div class="col-md-3"><label id="invoice_details-lbl" for="invoice_details" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_INVOICE_DETAILS');?></strong><br/><?php echo JText::_('LNG_INVOICE_DETAILS_NUMBER_INVOICE');?>" title=""><?php echo JText::_('LNG_INVOICE_DETAILS'); ?><span class="star">&nbsp;</span></label></div>
                <div class="col-md-9"><textarea rows="5" cols="300" name="invoice_details" id="invoice_details" name="meta_keywords"><?php echo $this->item->invoice_details ?></textarea></div>
            </div>
        </fieldset>
    </div>
</div>

<?php include JPATH_COMPONENT_SITE.'/assets/uploader.php'; ?>
<script>
	var appImgFolder = '<?php echo APP_PICTURES_PATH ?>';
	var appImgFolderPath = '<?php echo JURI::root()?>components/<?php echo JBusinessUtil::getComponentName()?>/assets/upload.php?t=<?php echo strtotime("now")?>&picture_type=<?php echo PICTURE_TYPE_LOGO?>&_root_app=<?php echo urlencode(JPATH_ROOT."/".PICTURES_PATH) ?>&_target=<?php echo urlencode(APP_PICTURES_PATH)?>';
	var removePath = '<?php echo JURI::root()?>/components/<?php echo JBusinessUtil::getComponentName()?>/assets/remove.php?_root_app=<?php echo urlencode(JPATH_COMPONENT_SITE)?>&_filename=';

	imageUploader(appImgFolder, appImgFolderPath);
</script>