<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
jimport( 'joomla.html.html.tabs' );

JHtml::_('formbehavior.chosen');
JHtml::_('behavior.formvalidation');
?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">

<?php 
	echo JHtml::_('bootstrap.startTabSet', 'tab_general_id', array('active' => 'panel_0_id'));

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_0_id', JText::_('LNG_GENERAL_SETTINGS'));
	require_once 'general.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_1_id', JText::_('LNG_COMPANY_DETAILS'));
	require_once 'businessdetails.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_2_id', JText::_('LNG_SEO'));
	require_once 'seo.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_3_id', JText::_('LNG_METADATA_SETTINGS'));
	require_once 'metadata.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_4_id', JText::_('LNG_FRONT_END'));
	require_once 'frontend.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_5_id', JText::_('LNG_BUSINESS_LISTINGS'));
	require_once 'businesslistings.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_6_id', JText::_('LNG_COMPANY_ATTRIBUTES'));
	require_once 'defaultattributes.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_7_id', JText::_('LNG_OFFERS'));
	require_once 'businessoffers.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_8_id', JText::_('LNG_EVENTS'));
	require_once 'businessevents.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.addTab', 'tab_general_id', 'panel_9_id', JText::_('LNG_LANGUAGES'));
	require_once 'languages.php';
	echo JHtml::_('bootstrap.endTab');

	echo JHtml::_('bootstrap.endTabSet');
?>

</div>
	<input type="hidden" name="sendmail_from" value="<?php echo $this->item->sendmail_from?>" />
	<input type="hidden" name="option" value="<?php echo JBusinessUtil::getComponentName()?>" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="applicationsettings_id" value="<?php echo $this->item->applicationsettings_id?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>

<script>
	jQuery(document).ready(function(){
		jQuery("#enable_packages1").click(function(){
			jQuery("#assign-packages").show();
		});

		jQuery("#enable_packages2").click(function(){
			jQuery("#assign-packages").hide();
		});

		jQuery('.hasTooltip').tooltip({"html": true,"container": "body"});

	});
</script>

