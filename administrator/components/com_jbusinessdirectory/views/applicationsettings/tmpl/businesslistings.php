<fieldset class="container-fluid">
	<legend><?php echo JText::_('LNG_GENERAL_SETTINGS'); ?></legend>
	<div class="row">
		<div class="col-lg-6 general-settings">

			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_ratings-lbl" for="enable_ratings" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_RATINGS');?></strong><br/><?php echo JText::_('LNG_ENABLE_RATINGS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_RATINGS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_ratings_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="enable_ratings" id="enable_ratings1" value="1" <?php echo $this->item->enable_ratings==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_ratings1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_ratings" id="enable_ratings0" value="0" <?php echo $this->item->enable_ratings==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_ratings0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_reviews-lbl" for="enable_reviews" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_REVIEWS');?></strong><br/><?php echo JText::_('LNG_ENABLE_REVIEWS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_REVIEWS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_reviews_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="enable_reviews" id="enable_reviews1" value="1" <?php echo $this->item->enable_reviews==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_reviews1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_reviews" id="enable_reviews0" value="0" <?php echo $this->item->enable_reviews==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_reviews0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_reviews_users-lbl" for="enable_reviews_users" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_REVIEWS_USERS_ONLY');?></strong><br/><?php echo JText::_('LNG_ENABLE_REVIEWS_USERS_ONLY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_REVIEWS_USERS_ONLY'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_reviews_users_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="enable_reviews_users" id="enable_reviews_users1" value="1" <?php echo $this->item->enable_reviews_users==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_reviews_users1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_reviews_users" id="enable_reviews_users0" value="0" <?php echo $this->item->enable_reviews_users==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_reviews_users0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="claim_business-lbl" for="claim_business" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_CLAIM_BUSINESS');?></strong><br/><?php echo JText::_('LNG_ENABLE_CLAIM_BUSINESS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_CLAIM_BUSINESS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="claim_business_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="claim_business" id="claim_business1" value="1" <?php echo $this->item->claim_business==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="claim_business1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="claim_business" id="claim_business0" value="0" <?php echo $this->item->claim_business==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="claim_business0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_pending_approval-lbl" for="show_pending_approval" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_PENDING_APPROVAL');?></strong><br/><?php echo JText::_('LNG_SHOW_PENDING_APPROVAL_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_PENDING_APPROVAL'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_pending_approval_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="show_pending_approval" id="show_pending_approval1" value="1" <?php echo $this->item->show_pending_approval==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_pending_approval1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="show_pending_approval" id="show_pending_approval0" value="0" <?php echo $this->item->show_pending_approval==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_pending_approval0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_pending_review-lbl" for="show_pending_review" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_PENDING_REVIEW');?></strong><br/><?php echo JText::_('LNG_SHOW_PENDING_REVIEW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_PENDING_REVIEW'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="show_pending_review" id="show_pending_review1" value="1" <?php echo $this->item->show_pending_review==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_pending_review1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="show_pending_review" id="show_pending_review0" value="0" <?php echo $this->item->show_pending_review==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_pending_review0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
		</div>
		<div class="col-lg-6 general-settings">
			<div class="row mb-3">
				<div class="col-md-3"><label id="limit_cities-lbl" for="limit_cities" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_LIMIT_CITIES');?></strong><br/><?php echo JText::_('LNG_LIMIT_CITIES_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_LIMIT_CITIES'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="limit_cities" id="limit_cities1" value="1" <?php echo $this->item->limit_cities==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="limit_cities1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="limit_cities" id="limit_cities0" value="0" <?php echo $this->item->limit_cities==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="limit_cities0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_details_user-lbl" for="show_details_user" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_DETAILS_ONLY_FOR_USERS_INFO');?></strong><br/><?php echo JText::_('LNG_SHOW_DETAILS_ONLY_FOR_USERS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_DETAILS_ONLY_FOR_USERS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_details_user_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="show_details_user" id="show_details_user1" value="1" <?php echo $this->item->show_details_user==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_details_user1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="show_details_user" id="show_details_user0" value="0" <?php echo $this->item->show_details_user==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_details_user0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_email-lbl" for="show_email" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_EMAIL');?></strong><br/><?php echo JText::_('LNG_SHOW_EMAIL_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_EMAIL'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_email_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="show_email" id="show_email1" value="1" <?php echo $this->item->show_email==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_email1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="show_email" id="show_email0" value="0" <?php echo $this->item->show_email==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_email0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3" style="display:none">
				<div class="col-md-3"><label data-original-title="<strong><?php echo JText::_('LNG_NR_IMAGES_SLIDE'); ?></strong><br />Enter the number of images per slide for business detail view slider" id="nr_images_slide-lbl" for="nr_images_slide" class="required" title=""><?php echo JText::_('LNG_NR_IMAGES_SLIDE'); ?></label></div>
				<div class="col-md-9"><input name="nr_images_slide" id="nr_images_slide" value="<?php echo $this->item->nr_images_slide?>" size="50" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="max_pictures-lbl" for="max_pictures" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_PICTURES');?></strong><br/><?php echo JText::_('LNG_MAX_PICTURES_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_PICTURES'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size=40 maxlength=20  id="max_pictures" name = "max_pictures" value="<?php echo $this->item->max_pictures?>">
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="max_video-lbl" for="max_video" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_VIDEOS');?></strong><br/><?php echo JText::_('LNG_MAX_VIDEOS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_VIDEOS'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size=40 maxlength=20  id="max_video" name = "max_video" value="<?php echo $this->item->max_video?>">
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="max_business-lbl" for="max_business" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_BUSINESS_LISTINGS_INFO');?></strong><br/><?php echo JText::_('LNG_MAX_BUSINESS_LISTINGS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_BUSINESS_LISTINGS'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size=40 maxlength=20  id="max_business" name = "max_business" value="<?php echo $this->item->max_business?>">
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_secondary_locations-lbl" for="show_secondary_locations" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_SECONDARY_LOCATIONS');?></strong><br/><?php echo JText::_('LNG_SHOW_SECONDARY_LOCATIONS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_SECONDARY_LOCATIONS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_secondary_locations_fld" >
						<input type="radio" class="validate[required] btn-check" name="show_secondary_locations" id="show_secondary_locations1" value="1" <?php echo $this->item->show_secondary_locations==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_secondary_locations1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="show_secondary_locations" id="show_secondary_locations0" value="0" <?php echo $this->item->show_secondary_locations==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_secondary_locations0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
		</div>
	</div>
</fieldset>

<fieldset class="container-fluid">
	<legend><?php echo JText::_('LNG_SEARCH'); ?></legend>
	<div class="row mb-3">
		<div class="col-lg-6 general-settings">
			<div class="row mb-3">
				<div class="col-md-3"><label id="submit_method-lbl" for="submit_method" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SUBMIT_METHOD');?></strong><br/><?php echo JText::_('LNG_SUBMIT_METHOD_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SUBMIT_METHOD'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="submit_method_fld" >
						<input type="radio" class="validate[required] btn-check" name="submit_method" id="submit_method1" value="post" <?php echo $this->item->submit_method=="post"? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="submit_method1"><?php echo JText::_('LNG_POST')?></label> 
						<input type="radio" class="validate[required] btn-check" name="submit_method" id="submit_method2" value="get" <?php echo $this->item->submit_method=="get"? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="submit_method2"><?php echo JText::_('LNG_GET')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_geolocation-lbl" for="enable_packages" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_GEOLOCATION');?></strong><br/><?php echo JText::_('LNG_ENABLE_GEOLOCATION_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_GEOLOCATION'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_geolocation_fld" >
						<input type="radio" class="validate[required] btn-check" name="enable_geolocation" id="enable_geolocation1" value="1" <?php echo $this->item->enable_geolocation==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_geolocation1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_geolocation" id="enable_geolocation0" value="0" <?php echo $this->item->enable_geolocation==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_geolocation0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="country_ids-lbl" for="country_ids[]" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SELECT_ZIPCODE_COUNTRY');?></strong><br/><?php echo JText::_('LNG_SELECT_ZIPCODE_COUNTRY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SELECT_ZIPCODE_COUNTRY'); ?></label></div>
				<div class="col-md-9">
					<select	id="country_ids" name="country_ids[]" data-placeholder="<?php echo JText::_("LNG_SELECT_COUNTRY") ?>" class="advancedSelect" multiple>
						<?php 
						foreach($this->item->countries as $country) {
							$selected = "";
							if (!empty($this->item->country_ids)) {
								if (in_array($country->id, $this->item->country_ids)) 
									$selected = "selected";
							} ?>
							<option value='<?php echo $country->id ?>' <?php echo $selected ?>> <?php echo $country->country_name ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="search_view_mode-lbl" for="search_view_mode" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_DEFAULT_SEARCH_VIEW');?></strong><br/><?php echo JText::_('LNG_DEFAULT_SEARCH_VIEW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_DEFAULT_SEARCH_VIEW'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="search_view_mode_fld" >
						<input type="radio" class="validate[required] btn-check" name="search_view_mode" id="search_view_mode1" value="1" <?php echo $this->item->search_view_mode==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="search_view_mode1"><?php echo JText::_('LNG_GRID_MODE')?></label> 
						<input type="radio" class="validate[required] btn-check" name="search_view_mode" id="search_view_mode0" value="0" <?php echo $this->item->search_view_mode==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="search_view_mode0"><?php echo JText::_('LNG_LIST_MODE')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_numbering-lbl" for="enable_numbering" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_NUMBERING');?></strong><br/><?php echo JText::_('LNG_ENABLE_NUMBERING_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_NUMBERING'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_numbering_fld" >
						<input type="radio" class="validate[required] btn-check" name="enable_numbering" id="enable_numbering1" value="1" <?php echo $this->item->enable_numbering==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_numbering1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_numbering" id="enable_numbering0" value="0" <?php echo $this->item->enable_numbering==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_numbering0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
		</div>

		<div class="col-lg-6 general-settings">
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_search_map-lbl" for="show_search_map" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_SEARCH_MAP');?></strong><br/><?php echo JText::_('LNG_SHOW_SEARCH_MAP_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_SEARCH_MAP'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_search_map_fld" >
						<input type="radio" class="validate[required] btn-check" name="show_search_map" id="show_search_map1" value="1" <?php echo $this->item->show_search_map==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_search_map1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="show_search_map" id="show_search_map0" value="0" <?php echo $this->item->show_search_map==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_search_map0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="show_secondary_map_locations-lbl" for="show_secondary_map_locations" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_SECONDARY_MAP_LOCATIONS');?></strong><br/><?php echo JText::_('LNG_SHOW_SECONDARY_MAP_LOCATIONS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_SECONDARY_MAP_LOCATIONS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="show_secondary_map_locations_fld" >
						<input type="radio" class="validate[required] btn-check" name="show_secondary_map_locations" id="show_secondary_map_locations1" value="1" <?php echo $this->item->show_secondary_map_locations==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="show_secondary_map_locations1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="show_secondary_map_locations" id="show_secondary_map_locations0" value="0" <?php echo $this->item->show_secondary_map_locations==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="show_secondary_map_locations0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_search_filter-lbl" for="enable_search_filter" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_SEARCH_FILTER');?></strong><br/><?php echo JText::_('LNG_ENABLE_SEARCH_FILTER_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_ENABLE_SEARCH_FILTER"); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" >
						<input type="radio" class="validate[required] btn-check" name="enable_search_filter" id="enable_search_filter1" value="1" <?php echo $this->item->enable_search_filter==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_search_filter1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_search_filter" id="enable_search_filter0" value="0" <?php echo $this->item->enable_search_filter==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_search_filter0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3" style="display:none">
				<div class="col-md-3"><label id="enable_search_letters-lbl" for="enable_search_letters" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_SEARCH_LETTERS');?></strong><br/><?php echo JText::_('LNG_ENABLE_SEARCH_LETTERS_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_ENABLE_SEARCH_LETTERS"); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" >
						<input type="radio" class="validate[required] btn-check" name="enable_search_letters" id="enable_search_letters1" value="1" <?php echo $this->item->enable_search_letters==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_search_letters1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="enable_search_letters" id="enable_search_letters0" value="0" <?php echo $this->item->enable_search_letters==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_search_letters0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="search_type-lbl" for="search_type" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SEARCH_FILTER');?></strong><br/><?php echo JText::_('LNG_SEARCH_FILTER_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_SEARCH_FILTER"); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" >
						<input type="radio" class="validate[required] btn-check" name="search_type" id="search_type1" value="1" <?php echo $this->item->search_type==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="search_type1"><?php echo JText::_('LNG_FACETED')?></label> 
						<input type="radio" class="validate[required] btn-check" name="search_type" id="search_type0" value="0" <?php echo $this->item->search_type==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="search_type0"><?php echo JText::_('LNG_REGULAR')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="zipcode_search_type-lbl" for="zipcode_search_type" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ZIPCODE_SEARCH_TYPE');?></strong><br/><?php echo JText::_('LNG_ZIPCODE_SEARCH_TYPE_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ZIPCODE_SEARCH_TYPE'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" >
						<input type="radio" class="validate[required] btn-check" name="zipcode_search_type" id="zipcode_search_type1" value="1" <?php echo $this->item->zipcode_search_type==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="zipcode_search_type1"><?php echo JText::_('LNG_BY_BUSINESS_ACTIVITY_RADIUS')?></label> 
						<input type="radio" class="validate[required] btn-check" name="zipcode_search_type" id="zipcode_search_type0" value="0" <?php echo $this->item->zipcode_search_type==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="zipcode_search_type0"><?php echo JText::_('LNG_BY_DISTANCE')?></label> 
					</fieldset>
				</div>
			</div>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-3">
			<label 
				id="search_result_view-lbl" 
				for="search_result_view" 
				data-bs-toggle="tooltip" 
				data-original-title="<strong><?php echo JText::_('LNG_SEARCH_RESULT_VIEW');?></strong><br/><?php echo JText::_('LNG_SEARCH_RESULT_VIEW_DESCRIPTION');?>" 
				title=""
			><?php echo JText::_('LNG_SEARCH_RESULT_VIEW'); ?></label>
		</div>
		<div class="col-md-9">
			<fieldset id="search_result_view_fld" >
				<input type="radio" class="validate[required] btn-check" name="search_result_view" id="search_result_view1" value="1" <?php echo $this->item->search_result_view==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_view1"><?php echo JText::_('LNG_STYLE_1')?></label> 
				<input type="radio" class="validate[required] btn-check" name="search_result_view" id="search_result_view2" value="2" <?php echo $this->item->search_result_view==2? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_view2"><?php echo JText::_('LNG_STYLE_2')?></label> 
				<input type="radio" class="validate[required] btn-check" name="search_result_view" id="search_result_view3" value="3" <?php echo $this->item->search_result_view==3? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_view3"><?php echo JText::_('LNG_STYLE_3')?></label>
				<input type="radio" class="validate[required] btn-check" name="search_result_view" id="search_result_view4" value="4" <?php echo $this->item->search_result_view==4? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_view4"><?php echo JText::_('LNG_STYLE_4')?></label>
				<input type="radio" class="validate[required] btn-check" name="search_result_view" id="search_result_view5" value="5" <?php echo $this->item->search_result_view==5? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_view5"><?php echo JText::_('LNG_STYLE_5')?></label>
			</fieldset>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-3"><label id="search_result_grid_view-lbl" for="search_result_grid_view" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SEARCH_RESULTS_GRID_VIEW');?></strong><br/><?php echo JText::_('LNG_SEARCH_RESULTS_GRID_VIEW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SEARCH_RESULTS_GRID_VIEW'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="search_result_grid_view_fld" >
				<input type="radio" class="validate[required] btn-check" name="search_result_grid_view" id="search_result_grid_view1" value="1" <?php echo $this->item->search_result_grid_view==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_grid_view1"><?php echo JText::_('LNG_STYLE_1')?></label> 
				<input type="radio" class="validate[required] btn-check" name="search_result_grid_view" id="search_result_grid_view2" value="2" <?php echo $this->item->search_result_grid_view==2? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="search_result_grid_view2"><?php echo JText::_('LNG_STYLE_2')?></label> 
			</fieldset>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-md-3"><label id="order_search_listings-lbl" for="order_search_listings" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ORDER_SEARCH_LISTINGS');?></strong><br/><?php echo JText::_('LNG_ORDER_SEARCH_LISTINGS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ORDER_SEARCH_LISTINGS'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="order_search_listings_fld" >
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings1" value="packageOrder desc" <?php echo $this->item->order_search_listings=="packageOrder desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings1"><?php echo JText::_('LNG_RELEVANCE')?></label> 
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings6" value="id desc" <?php echo $this->item->order_search_listings=="id desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings6"><?php echo JText::_('LNG_LAST_ADDED')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings8" value="id asc" <?php echo $this->item->order_search_listings=="id asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings8"><?php echo JText::_('LNG_FIRST_ADDED')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings2" value="companyName" <?php echo $this->item->order_search_listings=="companyName"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings2"><?php echo JText::_('LNG_NAME')?></label> 
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings3" value="city asc" <?php echo $this->item->order_search_listings=="city asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings3"><?php echo JText::_('LNG_CITY')?></label> <br/>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings4" value="averageRating desc" <?php echo $this->item->order_search_listings=="averageRating desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings4"><?php echo JText::_('LNG_RATING')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings7" value="review_score desc" <?php echo $this->item->order_search_listings=="review_score desc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings7"><?php echo JText::_('LNG_REVIEW')?></label> 
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings5" value="rand()" <?php echo $this->item->order_search_listings=="rand()"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings5"><?php echo JText::_('LNG_RANDOM')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings9" value="typeName" <?php echo $this->item->order_search_listings=="typeName"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings9"><?php echo JText::_('LNG_TYPE')?></label>
				<input type="radio" class="validate[required] btn-check" name="order_search_listings" id="order_search_listings10" value="distance asc" <?php echo $this->item->order_search_listings=="distance asc"? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="order_search_listings10"><?php echo JText::_('LNG_DISTANCE')?></label>
			</fieldset>
		</div>
	</div>

	<div class="row mb-3">
		<div class="col-md-3"><label id="search_fields-lbl" for="search_data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SELECT_SEARCH_FIELDS');?></strong><br/><?php echo JText::_('LNG_SELECT_SEARCH_FIELDS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SELECT_SEARCH_FIELDS'); ?></label></div>
		<div class="col-md-9">
			<select	id="search_fields[]" name="search_fields[]" data-placeholder="<?php echo JText::_("LNG_SELECT_FIELDS") ?>" class="advancedSelect" multiple>
				<?php
				foreach($this->searchFields as $field) {
					$selected = "";
					if (!empty($this->item->search_fields)) {
						if (in_array($field->value, $this->item->search_fields))
							$selected = "selected";
					} ?>
					<option value='<?php echo $field->value ?>' <?php echo $selected ?>> <?php echo $field->name ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

</fieldset>

<fieldset class="container-fluid">
	<legend><?php echo JText::_('LNG_BUSINESS_LISTING_DETAILS'); ?></legend>
	<div class="row mb-3">
		<div class="col-md-3"><label id="company_view-lbl" for="compdata-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_COMPANY_VIEW');?></strong><br/><?php echo JText::_('LNG_COMPANY_VIEW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_COMPANY_VIEW'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="company_view_fld" >
				<input type="radio" class="validate[required] btn-check" name="company_view" id="company_view1" value="1" <?php echo $this->item->company_view==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="company_view1"><?php echo JText::_('LNG_TABS_STYLE_1')?></label> 
				<input type="radio" class="validate[required] btn-check" name="company_view" id="company_view2" value="2" <?php echo $this->item->company_view==2? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="company_view2"><?php echo JText::_('LNG_TABS_STYLE_2')?></label> 
				<input type="radio" class="validate[required] btn-check" name="company_view" id="company_view3" value="3" <?php echo $this->item->company_view==3? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="company_view3"><?php echo JText::_('LNG_ONE_PAGE')?></label>
				<input type="radio" class="validate[required] btn-check" name="company_view" id="company_view4" value="4" <?php echo $this->item->company_view==4? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="company_view4"><?php echo JText::_('LNG_STYLE_4')?></label>
				<input type="radio" class="validate[required] btn-check" name="company_view" id="company_view5" value="5" <?php echo $this->item->company_view==5? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="company_view5"><?php echo JText::_('LNG_STYLE_5')?></label>
			</fieldset>
		</div>
	</div>
	<div class="row mb-3">
		<div class="col-md-3"><label id="view_count-lbl" for="show_vidata-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_COMPANY_VIEW_COUNT');?></strong><br/><?php echo JText::_('LNG_COMPANY_VIEW_COUNT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_COMPANY_VIEW_COUNT'); ?></label></div>
		<div class="col-md-9">
			<fieldset id="enable_packages_fld" >
				<input type="radio" class="validate[required] btn-check" name="show_view_count" id="enable_view_count1" value="1" <?php echo $this->item->show_view_count==1? 'checked="checked"' :""?> />
				<label class="btn btn-outline-success" for="enable_view_count1"><?php echo JText::_('LNG_YES')?></label>
				<input type="radio" class="validate[required] btn-check" name="show_view_count" id="enable_view_count0" value="0" <?php echo $this->item->show_view_count==0? 'checked="checked"' :""?> />
				<label class="btn btn-outline-secondary" for="enable_view_count0"><?php echo JText::_('LNG_NO')?></label>
			</fieldset>
		</div>
	</div>
</fieldset>