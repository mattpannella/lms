<div class="row">
	<div class="col-lg-6 general-settings">
		<fieldset class="form-horizontal">
			<legend><?php echo JText::_('LNG_GENERAL'); ?></legend>

			<div class="row mb-3">
				<div class="col-md-3"><label id="add_country_address-lbl" for="add_country_address" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADD_COUNTRY_ADDRESS');?></strong><br/><?php echo JText::_('LNG_ADD_COUNTRY_ADDRESS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ADD_COUNTRY_ADDRESS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="add_country_address_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="add_country_address" id="add_country_address1" value="1" <?php echo $this->item->add_country_address==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="add_country_address1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="add_country_address" id="add_country_address0" value="0" <?php echo $this->item->add_country_address==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="add_country_address0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="address_format-lbl" for="address_format" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ADDRESS_FORMAT');?></strong><br/><?php echo JText::_('LNG_ADDRESS_FORMAT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ADDRESS_FORMAT'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="address_format_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="address_format" id="address_format4" value="4" <?php echo $this->item->address_format==4? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="address_format4"><?php echo JText::_('LNG_EUROPEAN')." 2"?></label>
						<input type="radio" class="validate[required] btn-check" name="address_format" id="address_format3" value="3" <?php echo $this->item->address_format==3? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="address_format3"><?php echo JText::_('LNG_AMERICAN')." 2"?></label><br/>
						<input type="radio" class="validate[required] btn-check" name="address_format" id="address_format2" value="2" <?php echo $this->item->address_format==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="address_format2"><?php echo JText::_('LNG_EUROPEAN')?></label>
						<input type="radio" class="validate[required] btn-check" name="address_format" id="address_format1" value="1" <?php echo $this->item->address_format==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="address_format1"><?php echo JText::_('LNG_AMERICAN')?></label>
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="adaptive_height_gallery-lbl" for="adaptive_height_gallery" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_GALLERY_ADAPTIVE_HEIGHT');?></strong><br/><?php echo JText::_('LNG_GALLERY_ADAPTIVE_HEIGHT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_GALLERY_ADAPTIVE_HEIGHT'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="adaptive_height_gallery_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="adaptive_height_gallery" id="adaptive_height_gallery1" value="1" <?php echo $this->item->adaptive_height_gallery==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="adaptive_height_gallery1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="adaptive_height_gallery" id="adaptive_height_gallery0" value="0" <?php echo $this->item->adaptive_height_gallery==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="adaptive_height_gallery0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="autoplay_gallery-lbl" for="autoplay_gallery" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_GALLERY_AUTOPLAY');?></strong><br/><?php echo JText::_('LNG_GALLERY_AUTOPLAY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_GALLERY_AUTOPLAY'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="autoplay_gallery_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="autoplay_gallery" id="autoplay_gallery1" value="1" <?php echo $this->item->autoplay_gallery==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="autoplay_gallery1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="validate[required] btn-check" name="autoplay_gallery" id="autoplay_gallery0" value="0" <?php echo $this->item->autoplay_gallery==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="autoplay_gallery0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="col-lg-6 general-settings">
		<fieldset class="form-horizontal">
			<legend><?php echo JText::_('LNG_MAP'); ?></legend>
			<div class="row mb-3">
				<div class="col-md-3"><label id="google_map_key-lbl" for="google_map_key" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_GOOGLE_MAP_KEY');?></strong><br/><?php echo JText::_('LNG_GOOGLE_MAP_KEY_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_GOOGLE_MAP_KEY"); ?></label></div>
				<div class="col-md-9">
					<input type="text" id="google_map_key" name="google_map_key" maxlength="45" value="<?php echo $this->item->google_map_key ?>">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_auto_show-lbl" for="map_auto_show" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAP_AUTO_SHOW');?></strong><br/><?php echo JText::_('LNG_MAP_AUTO_SHOW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAP_AUTO_SHOW'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="map_auto_show_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="map_auto_show" id="map_auto_show1" value="1" <?php echo $this->item->map_auto_show==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="map_auto_show1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="map_auto_show" id="map_auto_show0" value="0" <?php echo $this->item->map_auto_show==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="map_auto_show0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_google_map_clustering-lbl" for="enable_google_map_clustering" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_GOOGLE_MAP_CLUSTERING');?></strong><br/><?php echo JText::_('LNG_ENABLE_GOOGLE_MAP_CLUSTERING_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_GOOGLE_MAP_CLUSTERING'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_google_map_clustering_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="validate[required] btn-check" name="enable_google_map_clustering" id="enable_google_map_clustering1" value="1" <?php echo $this->item->enable_google_map_clustering==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_google_map_clustering1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="enable_google_map_clustering" id="enable_google_map_clustering0" value="0" <?php echo $this->item->enable_google_map_clustering==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_google_map_clustering0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
					
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_latitude-lbl" for="map_latitude" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_LATITUDE');?></strong><br/><?php echo JText::_('LNG_LATITUDE_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_LATITUDE"); ?></label></div>
				<div class="col-md-9">
					<input type="text" id="map_latitude" name="map_latitude" value="<?php echo $this->item->map_latitude ?>">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_longitude-lbl" for="map_longitude" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_LONGITUDE');?></strong><br/><?php echo JText::_('LNG_LONGITUDE_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_LONGITUDE"); ?></label></div>
				<div class="col-md-9">
					<input type="text" id="map_longitude" name="map_longitude" value="<?php echo $this->item->map_longitude ?>">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_zoom-lbl" for="map_zoom" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ZOOM');?></strong><br/><?php echo JText::_('LNG_ZOOM_DESCRIPTION');?>" title=""><?php echo JText::_("LNG_ZOOM"); ?></label></div>
				<div class="col-md-9">
					<input type="text" id="map_zoom" name="map_zoom" value="<?php echo $this->item->map_zoom ?>">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_enable_auto_locate-lbl" for="map_enable_auto_locate" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_AUTO_LOCATE');?></strong><br/><?php echo JText::_('LNG_ENABLE_AUTO_LOCATE_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_AUTO_LOCATE'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="map_enable_auto_locate_fld" >
						<input type="radio" class="validate[required] btn-check" name="map_enable_auto_locate" id="map_enable_auto_locate1" value="1" <?php echo $this->item->map_enable_auto_locate==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="map_enable_auto_locate1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="map_enable_auto_locate" id="map_enable_auto_locate0" value="0" <?php echo $this->item->map_enable_auto_locate==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="map_enable_auto_locate0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="map_apply_search-lbl" for="map_apply_search" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_APPLY_SEARCH');?></strong><br/><?php echo JText::_('LNG_APPLY_SEARCH_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_APPLY_SEARCH'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="map_apply_search_fld" >
						<input type="radio" class="validate[required] btn-check" name="map_apply_search" id="map_apply_search1" value="1" <?php echo $this->item->map_apply_search==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="map_apply_search1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="validate[required] btn-check" name="map_apply_search" id="map_apply_search0" value="0" <?php echo $this->item->map_apply_search==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="map_apply_search0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
		</fieldset>
	</div>
</div>

<div class="row">
	<fieldset>
		<legend><?php echo JText::_('LNG_CATEGORIES'); ?></legend>
		<div class="row mb-3">
			<div class="col-md-3"><label id="category_view-lbl" for="category_view" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_CATEGORIES_VIEW');?></strong><br/><?php echo JText::_('LNG_CATEGORIES_VIEW_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_CATEGORIES_VIEW'); ?></label></div>
			<div class="col-md-9">
				<fieldset id="category_view_fld" >
					<input type="radio" class="validate[required] btn-check" name="category_view" id="category_view1" value="1" <?php echo $this->item->category_view==1? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="category_view1"><?php echo JText::_('LNG_ACCORDION')?></label> 
					<input type="radio" class="validate[required] btn-check" name="category_view" id="category_view2" value="2" <?php echo $this->item->category_view==2? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="category_view2"><?php echo JText::_('LNG_BOXES')?></label> 
					<input type="radio" class="validate[required] btn-check" name="category_view" id="category_view3" value="3" <?php echo $this->item->category_view==3? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="category_view3"><?php echo JText::_('LNG_SIMPLE')?></label>
					<input type="radio" class="validate[required] btn-check" name="category_view" id="category_view4" value="4" <?php echo $this->item->category_view==4? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="category_view4"><?php echo JText::_('LNG_ICONS')?></label>
				</fieldset>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-3"><label id="show_cat_description-lbl" for="show_cat_description" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_CAT_DESCRIPTION');?></strong><br/><?php echo JText::_('LNG_SHOW_CAT_DESCRIPTION_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_CAT_DESCRIPTION'); ?></label></div>
			<div class="col-md-9">
				<fieldset id="show_cat_description_fld" >
					<input type="radio" class="validate[required] btn-check" name="show_cat_description" id="show_cat_description1" value="1" <?php echo $this->item->show_cat_description==true? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="show_cat_description1"><?php echo JText::_('LNG_YES')?></label> 
					<input type="radio" class="validate[required] btn-check" name="show_cat_description" id="show_cat_description0" value="0" <?php echo $this->item->show_cat_description==false? 'checked="checked"' :""?> />
					<label class="btn btn-outline-secondary" for="show_cat_description0"><?php echo JText::_('LNG_NO')?></label> 
				</fieldset>
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-3"><label id="max_categories-lbl" for="max_categories" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_CATEGORIES');?></strong><br/><?php echo JText::_('LNG_MAX_CATEGORIES_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_CATEGORIES'); ?></label></div>
			<div class="col-md-9">
				<input type="text" size="40" maxlength="20"  id="max_categories" name="max_categories" value="<?php echo $this->item->max_categories?>">
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-md-3"><label id="show_total_business_count-lbl" for="show_total_business_count" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_TOTAL_BUSINESS_COUNT_INFO');?></strong><br/><?php echo JText::_('LNG_SHOW_TOTAL_BUSINESS_COUNT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_TOTAL_BUSINESS_COUNT'); ?></label></div>
			<div class="col-md-9">
				<fieldset id="show_total_business_count" >
					<input type="radio" class="validate[required] btn-check" name="show_total_business_count" id="show_total_business_count1" value="1" <?php echo $this->item->show_total_business_count==true? 'checked="checked"' :""?> />
					<label class="btn btn-outline-success" for="show_total_business_count1"><?php echo JText::_('LNG_YES')?></label>
					<input type="radio" class="validate[required] btn-check" name="show_total_business_count" id="show_total_business_count0" value="0" <?php echo $this->item->show_total_business_count==false? 'checked="checked"' :""?> />
					<label class="btn btn-outline-secondary" for="show_total_business_count0"><?php echo JText::_('LNG_NO')?></label>
				</fieldset>
			</div>
		</div>
	</fieldset>
</div>


