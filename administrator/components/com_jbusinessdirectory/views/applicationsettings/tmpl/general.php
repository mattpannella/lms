<div class="row">
	<div class="col-lg-6 general-settings">
		<fieldset>
			<legend><?php echo JText::_('LNG_GENERAL_SETTINGS'); ?></legend>
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_name-lbl" for="company_name" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_DATE_FORMAT');?></strong><br/><?php echo JText::_('LNG_DATE_FORMAT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_DATE_FORMAT'); ?></label></div>
				<div class="col-md-9">
					<select id='date_format_id' name='date_format_id'>
						<?php foreach ($this->item->dateFormats as $dateFormat){?>
							<option value = '<?php echo $dateFormat->id?>' <?php echo $dateFormat->id==$this->item->date_format_id? "selected" : ""?>> <?php echo $dateFormat->name?></option>
						<?php }	?>
					</select>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="time_format-lbl" for="time_format" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_TIME_FORMAT');?></strong><br/><?php echo JText::_('LNG_TIME_FORMAT_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_TIME_FORMAT'); ?></label></div>
				<div class="col-md-9">
					<select id='time_format' name='time_format'>
						<option value = "h:i A" <?php echo $this->item->time_format=="h:i A"? "selected" : ""?>><?php echo "12"." ".JText::_("LNG_HOURS")?></option>
						<option value = "H:i" <?php echo $this->item->time_format=="H:i"? "selected" : ""?>><?php echo "24"." ".JText::_("LNG_HOURS")?></option>
					</select>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="list_limit" for="list_limit">Number of Listings to Show on Search</label></div>
				<div class="col-md-9">
					<fieldset id="list_limit_field">						 
						<input type="number"  name="list_limit" value="<?php if($this->item->list_limit) { echo $this->item->list_limit; } else { echo 20; } ?>"/>
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_rss-lbl" for="enable_rss" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_RSS');?></strong><br/><?php echo JText::_('LNG_ENABLE_RSS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_RSS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_rss_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_rss" id="enable_rss1" value="1" <?php echo $this->item->enable_rss==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_rss1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_rss" id="enable_rss0" value="0" <?php echo $this->item->enable_rss==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_rss0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_socials-lbl" for="enable_socials" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_SOCIALS');?></strong><br/><?php echo JText::_('LNG_ENABLE_SOCIALS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_SOCIALS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_socials" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_socials" id="enable_socials1" value="1" <?php echo $this->item->enable_socials==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_socials1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_socials" id="enable_socials0" value="0" <?php echo $this->item->enable_socials==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_socials0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_multilingual-lbl" for="enable_multilingual" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_MULTILINGUAL');?></strong><br/><?php echo JText::_('LNG_ENABLE_MULTILINGUAL_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_MULTILINGUAL'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_multilingual_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_multilingual" id="enable_multilingual1" value="1" <?php echo $this->item->enable_multilingual==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_multilingual1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_multilingual" id="enable_multilingual0" value="0" <?php echo $this->item->enable_multilingual==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_multilingual0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="captcha-lbl" for="captcha" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_CAPTCHA');?></strong><br/><?php echo JText::_('LNG_ENABLE_CAPTCHA_DESCRIPTION');?><br><em><?php echo JText::_('LNG_ENABLE_CAPTCHA_DESCRIPTION_NOTE');?></em>" title=""><?php echo JText::_('LNG_ENABLE_CAPTCHA'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="captcha_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="captcha" id="captcha1" value="1" <?php echo $this->item->captcha==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="captcha1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="captcha" id="captcha0" value="0" <?php echo $this->item->captcha==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="captcha0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3" style="display:none">
				<div class="col-md-3"><label id="allow_multiple_companies-lbl" for="allow_multiple_companies" title=""><?php echo JText::_('LNG_ALLOW_MULTIPLE_COMPANIES_PER_USER'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="allow_multiple_companies" id="allow_multiple_companies1" value="1" <?php echo $this->item->allow_multiple_companies==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="allow_multiple_companies1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="allow_multiple_companies" id="allow_multiple_companies0" value="0" <?php echo $this->item->allow_multiple_companies==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="allow_multiple_companies0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_bookmarks-lbl" for="enable_bookmarks" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_BOOKMARKS');?></strong><br/><?php echo JText::_('LNG_ENABLE_BOOKMARKS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_BOOKMARKS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_bookmarks_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_bookmarks" id="enable_bookmarks1" value="1" <?php echo $this->item->enable_bookmarks==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_bookmarks1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_bookmarks" id="enable_bookmarks0" value="0" <?php echo $this->item->enable_bookmarks==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_bookmarks0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_attachments-lbl" for="enable_packages" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_ATTACHMENTS');?></strong><br/><?php echo JText::_('LNG_ENABLE_ATTACHMENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_ATTACHMENTS'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_attachments_fld" class="btn-group btn-group-yesno">
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_attachments" id="enable_attachments1" value="1" <?php echo $this->item->enable_attachments==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_attachments1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_attachments" id="enable_attachments0" value="0" <?php echo $this->item->enable_attachments==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_attachments0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="max_attachments-lbl" for="max_attachments" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_MAX_ATTACHMENTS_INFO');?></strong><br/><?php echo JText::_('LNG_MAX_ATTACHMENTS_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_MAX_ATTACHMENTS'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size="40" maxlength="20"  id="max_attachments" name="max_attachments" value="<?php echo $this->item->max_attachments?>">
				</div>
			</div>
		
			<div class="row mb-3">
				<div class="col-md-3"><label id="metric-lbl" for="metric" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_METRIC');?></strong><br/><?php echo JText::_('LNG_METRIC_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_METRIC'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_packages_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="metric" id="metric1" value="1" <?php echo $this->item->metric==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="metric1"><?php echo JText::_('LNG_MILES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="metric" id="metric0" value="0" <?php echo $this->item->metric==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="metric0"><?php echo JText::_('LNG_KM')?></label> 
					</fieldset>
				</div>
			</div>

			<div class="row mb-3">
				<div class="col-md-3"><label id="expiration_day_notice-lbl" for="expiration_day_notice"  data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_EXPIRATION_DAYS_NOTICE');?></strong><br/><?php echo JText::_('LNG_EXPIRATION_DAYS_NOTICE_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_EXPIRATION_DAYS_NOTICE'); ?></label></div>
				<div class="col-md-9">
					<input type="text" size=40 maxlength=20  id="expiration_day_notice" name = "expiration_day_notice" value="<?php echo $this->item->expiration_day_notice?>">
				</div>
			</div>
			
			<div class="row mb-3" style="display:none">
				<div class="col-md-3"><label id="direct_processing-lbl" for="direct_processing" title=""><?php echo JText::_('LNG_DIRECT_PROCESSING'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="direct_processing_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="direct_processing" id="direct_processing1" value="1" <?php echo $this->item->direct_processing==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="direct_processing1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="direct_processing" id="direct_processing0" value="0" <?php echo $this->item->direct_processing==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="direct_processing0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			
			
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="front_end_acl-lbl" for="front_end_acl" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_FRONT_END_ACL');?></strong><br/><?php echo JText::_('LNG_ENABLE_FRONT_END_ACL_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_ENABLE_FRONT_END_ACL'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="front_end_acl_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="front_end_acl" id="front_end_acl1" value="1" <?php echo $this->item->front_end_acl==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="front_end_acl1"><?php echo JText::_('LNG_YES')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="front_end_acl" id="front_end_acl0" value="0" <?php echo $this->item->front_end_acl==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="front_end_acl0"><?php echo JText::_('LNG_NO')?></label> 
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="usergroup-lbl" for="usergroup" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_CHOOSE_USERGROUP');?></strong><br/><?php echo JText::_('LNG_CHOOSE_USERGROUP_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_CHOOSE_USERGROUP'); ?></label></div>
				<div class="col-md-9">
					<select	id="usergroup" name="usergroup" class="chzn-color">
						<?php echo JHtml::_('select.options',$this->userGroups, 'value', 'name', $this->item->usergroup);?>
					</select>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="enable_https_payment-lbl" for="enable_https_payment" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_ENABLE_HTTPS_ON_PAYMENT_INFO');?></strong><br/><?php echo JText::_('LNG_ENABLE_HTTPS_ON_PAYMENT_DESCRIPTION');?><br/><em><?php echo JText::_('LNG_ENABLE_HTTPS_ON_PAYMENT_DESCRIPTION_2');?></em>" title=""><?php echo JText::_('LNG_ENABLE_HTTPS_ON_PAYMENT'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="enable_https_payment_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_https_payment" id="enable_https_payment1" value="1" <?php echo $this->item->enable_https_payment==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="enable_https_payment1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="btn-check"  class="validate[required]" name="enable_https_payment" id="enable_https_payment0" value="0" <?php echo $this->item->enable_https_payment==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="enable_https_payment0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-md-3"><label id="business_update_notification-lbl" for="business_update_notification" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_BUSINESS_UPDATE_NOTIFICATION');?></strong><br/><?php echo JText::_('LNG_BUSINESS_UPDATE_NOTIFICATION_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_BUSINESS_UPDATE_NOTIFICATION'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="business_update_notification_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="business_update_notification" id="business_update_notification1" value="1" <?php echo $this->item->business_update_notification==true? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="business_update_notification1"><?php echo JText::_('LNG_YES')?></label>
						<input type="radio" class="btn-check"  class="validate[required]" name="business_update_notification" id="business_update_notification0" value="0" <?php echo $this->item->business_update_notification==false? 'checked="checked"' :""?> />
						<label class="btn btn-outline-secondary" for="business_update_notification0"><?php echo JText::_('LNG_NO')?></label>
					</fieldset>
				</div>
			</div>
		</fieldset>
	</div>
	<div class="col-lg-6 general-settings">
		<fieldset class="form-horizontal">
			<legend><?php echo JText::_('LNG_CURRENCY'); ?></legend>
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_name-lbl" for="company_name" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_NAME');?></strong><br/><?php echo JText::_('LNG_NAME_CURRENCY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_NAME'); ?></label></div>
				<div class="col-md-9">
					<select	id="currency_id" name="currency_id" class="chzn-color">
						<?php
							for($i = 0; $i <  count( $this->item->currencies ); $i++){
								$currency = $this->item->currencies[$i]; 
						?>
							<option value = '<?php echo $currency->currency_id?>' <?php echo $currency->currency_id==$this->item->currency_id? "selected" : ""?>> <?php echo $currency->currency_name." - ". $currency->currency_description ?></option>
						<?php }	?>
					</select>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="company_email-lbl" for="company_email" class="required" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_CURRENCY_SYMBOL');?></strong><br/><?php echo JText::_('LNG_CURRENCY_SYMBOL_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_CURRENCY_SYMBOL'); ?><span class="star">&nbsp;</span></label></div>
				<div class="col-md-9"><input name="currency_symbol" id="currency_symbol" value="<?php echo $this->item->currency_symbol?>" size="50" maxlength="45" type="text"></div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="currency_display-lbl" for="enable_packages" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_CURRENCY_DISPLAY');?></strong><br/><?php echo JText::_('LNG_CURRENCY_DISPLAY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_CURRENCY_DISPLAY'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="currency_display_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="currency_display" id="currency_display1" value="1" <?php echo $this->item->currency_display==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="currency_display1"><?php echo JText::_('LNG_NAME')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="currency_display" id="currency_display2" value="2" <?php echo $this->item->currency_display==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="currency_display2"><?php echo JText::_('LNG_SYMBOL')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="currency_location-lbl" for="enable_packages" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_SHOW_CURRENCY');?></strong><br/><?php echo JText::_('LNG_SHOW_CURRENCY_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_SHOW_CURRENCY'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="currency_location_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="currency_location" id="currency_location1" value="1" <?php echo $this->item->currency_location==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="currency_location1"><?php echo JText::_('LNG_BEFORE_PRICE')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="currency_location" id="currency_location2" value="2" <?php echo $this->item->currency_location==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="currency_location2"><?php echo JText::_('LNG_AFTER_PRICE')?></label> 
					</fieldset>
				</div>
			</div>
			
			<div class="row mb-3">
				<div class="col-md-3"><label id="amount_separator-lbl" for="enable_packages" class="hasTooltip" data-bs-toggle="tooltip" data-original-title="<strong><?php echo JText::_('LNG_AMOUNT_SEPARATOR');?></strong><br/><?php echo JText::_('LNG_AMOUNT_SEPARATOR_DESCRIPTION');?>" title=""><?php echo JText::_('LNG_AMOUNT_SEPARATOR'); ?></label></div>
				<div class="col-md-9">
					<fieldset id="amount_separator_fld" >
						<input type="radio" class="btn-check"  class="validate[required]" name="amount_separator" id="amount_separator1" value="1" <?php echo $this->item->amount_separator==1? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="amount_separator1"><?php echo JText::_('LNG_DOT_SEPARATOR')?></label> 
						<input type="radio" class="btn-check"  class="validate[required]" name="amount_separator" id="amount_separator2" value="2" <?php echo $this->item->amount_separator==2? 'checked="checked"' :""?> />
						<label class="btn btn-outline-success" for="amount_separator2"><?php echo JText::_('LNG_COMMA_SEPARATOR')?></label> 
					</fieldset>
				</div>
			</div>
		</fieldset>
	</div>
</div>

	<div class="container-fluid">
		<fieldset>
			<legend><?php echo JText::_('LNG_TERMS_AND_CONDITIONS'); ?></legend>
			<div class="row mb-3">
				<?php
				$app = JFactory::getApplication();
				$options = array(
					'onActive' => 'function(title, description) {
					description.setStyle("display", "block");
					title.addClass("open").removeClass("closed");
				}',
					'onBackground' => 'function(title, description) {
					description.setStyle("display", "none");
					title.addClass("closed").removeClass("open");
				}',
					'startOffset' => 0,  // 0 starts on the first tab, 1 starts the second, etc...
					'useCookie' => true, // this must not be a string. Don't use quotes.
				);

				if($this->appSettings->enable_multilingual) {
					echo JHtml::_('tabs.start', 'tab_groupsd_id', $options);
					foreach ($this->languagesTranslations as $k => $lng) {
						echo JHtml::_('tabs.panel', $lng, 'tab-'.$lng);

						$langContent = isset($this->translations[$lng]) ? $this->translations[$lng] : "";

						if ($lng == JFactory::getLanguage()->getTag() && empty($langContent)) {
							$langContent = $this->item->terms_conditions;
						}

						$editor = JFactory::getEditor();
						echo $editor->display('terms_conditions_'.$lng, $langContent, '550', '200', '80', '10', false);
					}
					echo JHtml::_('tabs.end');
				} else {
					$editor = JFactory::getEditor();
					echo $editor->display('terms_conditions', $this->item->terms_conditions, '550', '200', '80', '10', false);
				}
				?>
			</div>
		</fieldset>
	</div>

<?php include JPATH_COMPONENT_SITE.'/assets/uploader.php'; ?>			
<script>
	var appImgFolder = '<?php echo APP_PICTURES_PATH ?>';
	var appImgFolderPath = '<?php echo JURI::root()?>components/<?php echo JBusinessUtil::getComponentName()?>/assets/upload.php?t=<?php echo strtotime("now")?>&picture_type=<?php echo PICTURE_TYPE_LOGO?>&_root_app=<?php echo urlencode(JPATH_ROOT."/".PICTURES_PATH) ?>&_target=<?php echo urlencode(APP_PICTURES_PATH)?>';
	var removePath = '<?php echo JURI::root()?>/components/<?php echo JBusinessUtil::getComponentName()?>/assets/remove.php?_root_app=<?php echo urlencode(JPATH_COMPONENT_SITE)?>&_filename=';
	
	imageUploader(appImgFolder, appImgFolderPath);
</script>