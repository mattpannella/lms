<?php 
/*------------------------------------------------------------------------
 # JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
?>
<style>
#content-wrapper{
	margin: 20px;
	padding: 0px;
}
</style>

<div id="jbd-dashbord">
	<div class="row-fluid">
		<div class="span3">
			<div class="ibox">
				<div class="ibox-title">
					<span class="dir-label dir-label-info  pull-right"><?php echo JText::_("LNG_TOTAL");?></span>
					<h5><?php echo JText::_("LNG_BUSINESS_LISTINGS");?></h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins"><?php echo $this->statistics->totalListings ?></h1>
					<div class="stat-percent font-bold text-success">
						<?php echo $this->statistics->month ?>
					</div>
					<small><?php echo JText::_("LNG_THIS_MONTH");?></small>
				</div>
			</div>
		</div>
		<div class="span3">
			<div class="ibox">
				<div class="ibox-title">
					<span class="dir-label dir-label-success pull-right"><?php echo JText::_("LNG_TOTAL");?></span>
					<h5><?php echo JText::_("LNG_OFFERS");?></h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins"><?php echo $this->statistics->totalOffers ?></h1>
					<div class="stat-percent font-bold text-success">
						<?php echo $this->statistics->totalOffers>0 ? round($this->statistics->activeOffers*100/$this->statistics->totalOffers,2):0 ?>%
					</div>
					<small><?php echo JText::_("LNG_ACTIVE");?></small>
				</div>
			</div>
		</div>
		<div class="span3">
			<div class="ibox">
				<div class="ibox-title">
					<span class="dir-label dir-label-primary pull-right"><?php echo JText::_("LNG_TOTAL");?></span>
					<h5><?php echo JText::_("LNG_EVENTS");?></h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins"><?php echo $this->statistics->totalEvents ?></h1>
					<div class="stat-percent font-bold text-success">
						<?php echo $this->statistics->totalEvents>0 ? round($this->statistics->activeEvents*100/$this->statistics->totalEvents,2):0 ?>%
					</div>
					<small><?php echo JText::_("LNG_ACTIVE");?></small>
				</div>
			</div>
		</div>
		<div class="span3">
			<div class="ibox">
				<div class="ibox-title">
					<span class="dir-label dir-label-warning  pull-right"><?php echo JText::_("LNG_TOTAL");?></span>
					<h5><?php echo JText::_("LNG_INCOME");?></h5>
				</div>
				<div class="ibox-content">
					<h1 class="no-margins"><?php echo intval($this->income->total) ?></h1>
					<div class="stat-percent font-bold text-success">
						<?php echo intval($this->income->month) ?>
					</div>
					<small><?php echo JText::_("LNG_THIS_MONTH");?></small>
				</div>
			</div>
		</div>

	</div>

	<div class="row-fluid">
		<div class="ibox">
				<div class="ibox-content">
					<div class="row-fluid">
						<div class="span9">
							<?php $days_ago = 70; ?>
							<?php $time = strftime('%Y-%m-%d',(strtotime($days_ago.' days ago'))); ?>
							<div id="dir-dashboard-calendar-form">
								<div class="row-fluid">
									<div class="span12">
										<div id="tabs">
											<div id="dir-dashboard-tabs" class="row-fluid">
												<div class="span7" id="dir-dashboard-tabs-col">
													<ul>
														<li><a href="#newCompanies"><?php echo JText::_("LNG_BUSINESS_LISTINGS");?></a></li>
														<li><a href="#newOffers"><?php echo JText::_("LNG_OFFERS");?></a></li>
														<li><a href="#newEvents"><?php echo JText::_("LNG_EVENTS");?></a></li>
														<li><a href="#income"><?php echo JText::_("LNG_INCOME");?></a></li>
													</ul>
												</div>
												<div class="span5 item-calendar remove-margin" id="dir-dashboard-tabs-col">
													<div class="remove-margin item-calendar">
														<div class="detail_box remove-margin">
															<?php echo JHTML::_('calendar', $time, 'start_date', 'start_date', $this->appSettings->calendarFormat, array('id'=>'start_date', 'class'=>'inputbox calendar-date', 'size'=>'10',  'maxlength'=>'10', 'onchange'=>'calendarChange()')); ?>
															<span><?php echo JText::_("LNG_TO")?></span>															
															<?php echo JHTML::_('calendar', date("Y-m-d"), 'end_date', 'end_date', $this->appSettings->calendarFormat, array('id'=>'end_date', 'class'=>'inputbox calendar-date', 'size'=>'10',  'maxlength'=>'10', 'onchange'=>'calendarChange()')); ?>
															<div class="clear"></div>
														</div>
													</div>
													<div class="clear"></div>
												</div>
											</div>
											<div id="newCompanies">
												<div id="graph"></div>
											</div>
											<div id="newOffers">
											</div>
											<div id="newEvents">
											</div>
											<div id="income">
											</div>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="span3">
							<div>
								<h2><?php echo JText::_("LNG_TOTAL_VIEWS")?></h2> <h3><?php echo $this->statistics->totalViews?></h3>
							</div>
							<br/><br/><br/>
							<ul class="stat-list">
								<li>
									<h2 class="no-margins"><?php echo $this->statistics->listingsTotalViews ?></h2> <?php echo JText::_("LNG_BUSINESS_LISTING_VIEWS")?>

									<div class="stat-percent">
										<?php
											echo $this->statistics->totalViews>0 ? round($this->statistics->listingsTotalViews * 100/$this->statistics->totalViews): 0
										?>%
									</div>
									<div class="dir-progress progress-mini">
										<div class="dir-progress-bar" style="width: <?php echo $this->statistics->totalViews > 0 ? round($this->statistics->listingsTotalViews * 100/$this->statistics->totalViews) : 0 ?>%;"></div>
									</div>
								</li>
								<li>
									<h2 class="no-margins "><?php echo $this->statistics->offersTotalViews ?></h2><?php echo JText::_("LNG_OFFER_VIEWS")?>
									<div class="stat-percent">
										<?php
											echo $this->statistics->totalViews>0 ? round($this->statistics->offersTotalViews * 100/$this->statistics->totalViews): 0
										?>%
									</div>
									<div class="dir-progress progress-mini">
										<div class="dir-progress-bar" style="width: <?php echo $this->statistics->totalViews > 0 ? round($this->statistics->offersTotalViews * 100/$this->statistics->totalViews) : 0 ?>%;"></div>
									</div>
								</li>
								<li>
									<h2 class="no-margins "><?php echo $this->statistics->eventsTotalViews ?></h2><?php echo JText::_("LNG_EVENT_VIEWS")?>
									<div class="stat-percent">
										<?php echo $this->statistics->totalViews > 0 ?round($this->statistics->eventsTotalViews * 100/$this->statistics->totalViews): 0?>%
									</div>
									<div class="dir-progress progress-mini">
										<div class="dir-progress-bar" style="width: <?php echo $this->statistics->totalViews > 0 ? round($this->statistics->eventsTotalViews * 100/$this->statistics->totalViews): 0?>%;"></div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
		</div>
	</div>
	
</div>

<script type="text/javascript">
	jQuery("#tabs").tabs();

	var curTab = jQuery("#tabs").tabs('option', 'active');
	var siteRoot = '<?php echo JURI::root(); ?>';
	var compName = '<?php echo JBusinessUtil::getComponentName(); ?>';   
	var url = siteRoot+'administrator/index.php?option='+compName;
	var urlNews = siteRoot+'administrator/index.php?option='+compName+'&task=jbusinessdirectory.getLatestServerNewsAjax';
	var urlReport = siteRoot+'administrator/index.php?option='+compName+'&task=jbusinessdirectory.newCompanies';
	
	var start_date = jQuery("#start_date").val();
	var end_date = jQuery("#end_date").val();

	//retrieve the latest news
	jQuery.ajax({
		url: urlNews,
		type: 'GET'
	})
	
	function requestData(urlReport, start_date, end_date, chart) {
		jQuery.ajax({
			url: urlReport,
			dataType: 'json',
			type: 'GET',
			data: { start_date: start_date, end_date: end_date },
		})
		.done(function(data) {
			console.log(JSON.stringify(data));
			chart.setData(data);
		})
		.fail(function(data) {
			console.log("Error");
			console.log(JSON.stringify(data));
		});
	}

	var chart = Morris.Area({
		element: 'graph',
		data: [{date: '<?php echo date("d-m-Y"); ?>', value: 0}],
		fillOpacity: 0.6,
		hideHover: 'auto',
		behaveLikeLine: true,
		resize: true,
		lineColors: ['#54cdb4'],
		xkey: 'date',
		ykeys: ['value'],
		labels: ['Total'],
	});

	requestData(urlReport, start_date, end_date, chart);

	jQuery("#tabs").click(function(e) {
		e.preventDefault();
		calendarChange();
	});

	jQuery("#start_date, #end_date").bind("paste keyup", function(e) {
		e.preventDefault();
		calendarChange();
	});

	function calendarChange() {
		var curTab = jQuery("#tabs .ui-tabs-panel:visible").attr("id");
		var start_date = jQuery("#start_date").val();
		var end_date = jQuery("#end_date").val();
		var urlReport = siteRoot+'administrator/index.php?option='+compName+'&task=jbusinessdirectory.'+curTab;
		jQuery("#graph").appendTo("#"+curTab);
		requestData(urlReport, start_date, end_date, chart);
	}

	
	//retrieve current version status; 
	var versionCheckTask = '&task=updates.getVersionStatus';
	jQuery.ajax({
		url: url+versionCheckTask,
		dataType: 'json',
		type: 'POST',
		success: function(data){
				
                if(compareVersions(data.currentVersion,data.updateVersion)){
             	  	jQuery("#update-status").html("<span class='text-success'><?php echo JText::_("LNG_UP_TO_DATE")?></span>");	
                }else{
                	jQuery("#update-status").html("<span class='text-danger'><?php echo JText::_("LNG_OUT_OF_DATE")?></span>");	
                	jQuery("#update-version").html(data.updateVersion);
             	  	jQuery("#update-version-holder").show();
             	  	jQuery("#current-version").removeClass("dir-label-info");
             	  	jQuery("#current-version").addClass("dir-label-warning");
                }

                if(data.message.indexOf("Please enter your order details")>0){
                	jQuery("#update-status").html(data.message);
                }  	
        }
	})
</script>