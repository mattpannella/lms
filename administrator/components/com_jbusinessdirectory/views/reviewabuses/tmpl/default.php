<?php 
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.multiselect');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task != 'reviewabuses.delete' || confirm("<?php echo JText::_("COM_JBUSINESSDIRECTORY_REVIEW_ABUSE_CONFIRM_DELETE", true);?>")) {
			Joomla.submitform(task);
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=reviewabuses');?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar">
			<div class="filter-search btn-group float-start">
				<label class="filter-search-lbl element-invisible" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
				<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />
				<button class="btn btn-light" data-bs-toggle="tooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn btn-light" data-bs-toggle="tooltip" type="button" onclick="document.getElementById('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
			</div>
			<div class="float-end hidden-phone me-2">
				<label for="limit" class="d-none"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>	
			<div class="filter-select float-end me-2">
				<select name="filter_state_id" class="input-medium" onchange="this.form.submit()">
					<option value=""><?php echo JText::_('LNG_JOPTION_SELECT_STATE');?></option>
					<?php echo JHtml::_('select.options', $this->states, 'value', 'text', $this->state->get('filter.state_id'));?>
				</select>
			</div>
		</div>
	</div>
	<div class="clr clearfix"> </div>
	<table class="table table-striped adminlist" id="itemList">
		<thead>
			<tr>
				<th class="hidden-phone" width="1%">#</th>
				<th class="hidden-phone" width="1%"><input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" /></th>
				<th width="5%"><?php echo JHtml::_('grid.sort', 'LNG_STATE', 'ra.state', $listDirn, $listOrder); ?></th>
				<th><?php echo JHtml::_('grid.sort', 'LNG_REVIEW_NAME', 'subject', $listDirn, $listOrder); ?></th>
				<th><?php echo JHtml::_('grid.sort', 'LNG_EMAIL', 'ra.email', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_DESCRIPTION', 'ra.description', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_ID', 'ra.id', $listDirn, $listOrder); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
		<tbody>
			<?php $nrcrt = 1;
			foreach($this->items as $reviewabuse) { ?>
				<tr class="row<?php echo $nrcrt%2?>"  >
					<td align='center' class="hidden-phone"><?php echo $nrcrt++?></td>
					<td align='center' class="hidden-phone"><?php echo JHtml::_('grid.id', $nrcrt, $reviewabuse->id); ?></td>
					<td valign='top' align='center'>
						<img class="cursor-pointer" src="<?php echo JURI::base() ."components/".JBusinessUtil::getComponentName()."/assets/img/".($reviewabuse->state==0? "no_joomla.png" : "yes_joomla.png")?>"
							 onclick="document.location.href = '<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=reviewabuses.chageState&id='. $reviewabuse->id )?> '"/>
					</td>
					<td align='left'>
						<a href='<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&task=reviewabuse.edit&id='.$reviewabuse->id) ?>' title="<?php echo JText::_('LNG_CLICK_TO_EDIT'); ?>">
							<b><?php echo $reviewabuse->subject ?></b>
						</a>	
					</td>
					<td><?php echo $reviewabuse->email ?></td>
					<td class="hidden-phone"><?php echo substr($reviewabuse->description, 0, 50) ?></td>
					<td class="hidden-phone"><?php echo $reviewabuse->id ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
	<input type="hidden" name="task" value="" /> 
	<input type="hidden" name="id" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHTML::_('form.token'); ?> 
</form>