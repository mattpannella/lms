<?php
/**
 * @package    JBusinessDirectory
 * @subpackage com_jbusinessdirectory
 *
 * @copyright  Copyright (C) 2007 - 2015 CMS Junkie. All rights reserved.
 * @license    GNU General Public License version 2 or later; 
 */

defined('_JEXEC') or die('Restricted access');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('behavior.formvalidation');
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'report.cancel' || !validateCmpForm()) {
			Joomla.submitform(task, document.getElementById('item-form'));
		}
	}
</script>

<?php 
	$appSetings = JBusinessUtil::getInstance()->getApplicationSettings();
	$user = JFactory::getUser(); 
?>

<div class="category-form-container">	
	<form action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form">
		<div class="clr mandatory oh">
			<p><?php echo JText::_("LNG_REQUIRED_INFO")?></p>
		</div>

		<fieldset class="boxed">
			<h2> <?php echo JText::_('LNG_REPORT_DETAILS');?></h2>
			<p><?php echo JText::_('LNG_REPORT_INFO_TXT');?></p>
			<div class="form-box">
				<div class="detail_box">
					<div  class="form-detail req"></div>
					<label for="subject"><?php echo JText::_('LNG_NAME')?> </label> 
					<input type="text"
						name="name" id="name" class="input_txt validate[required]" value="<?php echo $this->item->name ?>" maxlength="145">
					<div class="clear"></div>
					<span class="error_msg" id="frmCompanyName_error_msg" style="display: none;"><?php echo JText::_('LNG_REQUIRED_FIELD')?></span>
				</div>
				<div class="detail_box">
					<div class="form-detail req"></div>
					<label for="description_id"><?php echo JText::_('LNG_DESCRIPTION')?>  &nbsp;&nbsp;&nbsp;</label>
					<textarea name="description" id="description" class="input_txt validate[required]"  cols="75" rows="5"><?php echo $this->item->description ?></textarea>
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>

		<fieldset class="boxed">
			<h2> <?php echo JText::_('LNG_REPORT_TYPE');?></h2>
			<p> <?php echo JText::_('LNG_REPORT_TYPE_INFORMATION_TEXT');?>.</p>
			<div class="form-box">
				<div class="detail_box">
					<div  class="form-detail req"></div>
					<label for="subject"><?php echo JText::_('LNG_TYPE')?> </label> 
					<select id="type" name="type" class="validate[required]">
						<?php
						if($this->item->type == 1) { ?>
							<option value='0'><?php echo JText::_('LNG_COMPANY')?></option>
							<option value='1' selected><?php echo JText::_('LNG_CONFERENCE')?></option>
						<?php } else { ?>
							<option value='0' selected><?php echo JText::_('LNG_COMPANY')?></option>
							<option value='1'><?php echo JText::_('LNG_CONFERENCE')?></option>
						<?php } ?>
					</select>
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>

		<fieldset id="business_params" class="boxed">
			<h2> <?php echo JText::_('LNG_REPORT_PARAMS');?></h2>
			<p> <?php echo JText::_('LNG_REPORT_PARAMS_INFORMATION_TEXT');?>.</p>
			<div class="form-box">
				<div class="detail_box">
					<div  class="form-detail req"></div>
					<select id="features" class="multiselect" multiple="multiple" name="selected_params[]" size="10">
						<?php
						foreach($this->params as $key=>$param){
							if(in_array($key, $this->item->selected_params)>0)
								$selected = "selected='selected'";
							else
								$selected = "";
							echo "<option value='$key' $selected> ".JText::_($param)."</option>";
						} ?>
					</select>
					<div class="clear"></div>
				</div>
			</div>
			<div class="form-box">
				<div class="detail_box">
					<div  class="form-detail req"></div>
					<select id="customFeatures" class="multiselect2" multiple="multiple" name="custom_params[]" size="10">
						<?php
						foreach($this->customFeatures as $feature){
							if(in_array($feature->code,$this->item->custom_params)>0)
								$selected = "selected='selected'";
							else
								$selected = "";
							echo "<option value='$feature->code' $selected>$feature->name</option>";
						} ?>
					</select>
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>
		
		<fieldset id="conference_params" class="boxed">
			<h2> <?php echo JText::_('LNG_REPORT_PARAMS');?></h2>
			<p> <?php echo JText::_('LNG_REPORT_PARAMS_INFORMATION_TEXT');?>.</p>
			<div class="form-box">
				<div class="detail_box">
					<div class="form-detail req"></div>
					<select id="customFeatures" class="multiselect3" multiple="multiple" name="selected_conference_params[]" size="10">
						<?php 
						foreach($this->conferenceParams as $key=>$conferenceParam){
							if(in_array($key, $this->item->selected_params)>0)
								$selected = "selected='selected'";
							else
								$selected = "";
							echo "<option value='$key' $selected> ".JText::_($conferenceParam)."</option>";
						} ?>
					</select>
					<div class="clear"></div>
				</div>
			</div>
		</fieldset>

		<script  type="text/javascript">
			function save() {
				if(validateCmpForm())
					return false;
				jQuery("#task").val('report.save');
				var form = document.adminForm;
				form.submit();
			}
			function cancel() {
				jQuery("#task").val('report.cancel');
				var form = document.adminForm;
				form.submit();
			}
			function validateCmpForm() {
				var isError = jQuery("#item-form").validationEngine('validate');
				return !isError;
			}

			jQuery(document).ready(function() {
				const multiSelectOptions = {
					selectableHeader: "<div style='margin-bottom:20px'><?php echo JText::_('LNG_PARAMS')?></div>",
					selectableFooter: `<button id="muliselectSelectAll" class="btn btn-secondary btn-sm mt-2" type="button">Select All</button>`,
					selectionHeader: "Selected <?php echo JText::_('LNG_PARAMS')?>",
					selectionFooter: `<button id="muliselectDeselectAll" class="btn btn-light btn-sm mt-2" type="button">Clear All</button>`
				};
				jQuery(".multiselect").multiSelect(multiSelectOptions);
				jQuery("#muliselectSelectAll").click(() => jQuery(".multiselect").multiSelect("select_all"));
				jQuery("#muliselectDeselectAll").click(() => jQuery(".multiselect").multiSelect("deselect_all"));

				const multiSelectOptions2 = {
					selectableHeader: "<div style='margin-bottom:20px'><?php echo JText::_('LNG_PARAMS')?></div>",
					selectableFooter: `<button id="muliselectSelectAll2" class="btn btn-secondary btn-sm mt-2" type="button">Select All</button>`,
					selectionHeader: "Selected <?php echo JText::_('LNG_PARAMS')?>",
					selectionFooter: `<button id="muliselectDeselectAll2" class="btn btn-light btn-sm mt-2" type="button">Clear All</button>`
				};
				jQuery(".multiselect2").multiSelect(multiSelectOptions2);
				jQuery("#muliselectSelectAll2").click(() => jQuery(".multiselect2").multiSelect("select_all"));
				jQuery("#muliselectDeselectAll2").click(() => jQuery(".multiselect2").multiSelect("deselect_all"));

				const multiSelectOptions3 = {
					selectableHeader: "<div style='margin-bottom:20px'><?php echo JText::_('LNG_PARAMS')?></div>",
					selectableFooter: `<button id="muliselectSelectAll3" class="btn btn-secondary btn-sm mt-2" type="button">Select All</button>`,
					selectionHeader: "Selected <?php echo JText::_('LNG_PARAMS')?>",
					selectionFooter: `<button id="muliselectDeselectAll3" class="btn btn-light btn-sm mt-2" type="button">Clear All</button>`
				};
				jQuery(".multiselect3").multiSelect(multiSelectOptions3           );
				jQuery("#muliselectSelectAll3").click(() => jQuery(".multiselect3").multiSelect("select_all"));
				jQuery("#muliselectDeselectAll3").click(() => jQuery(".multiselect3").multiSelect("deselect_all"));

				jQuery('#type').on('change', function() {
					if ( this.value == '1') {
						jQuery("select[name='selected_params']").empty();
						jQuery("select[name='custom_params']").empty();
						jQuery("#business_params").hide();
						jQuery("#conference_params").show();
					} else {
						jQuery("select[name='selected_conference_params']").empty();
						jQuery("#conference_params").hide();
						jQuery("#business_params").show();
					}
				}).trigger('change');
			});
		</script>
		<input type="hidden" name="option" value="<?php echo JBusinessUtil::getComponentName()?>" /> 
		<input type="hidden" name="task" id="task" value="" /> 
		<input type="hidden" name="id" value="<?php echo $this->item->id ?>" /> 
		<?php echo JHTML::_( 'form.token' ); ?>
	</form>
</div>
