<?php 
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.multiselect');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if(task == 'events.delete') {
			//showDeleteDialog();
		}
		if (task != 'events.delete' || confirm("<?php echo JText::_('ARE_YOU_SURE_YOU_WANT_TO_DELETE', true);?>")) {
			Joomla.submitform(task);
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=events');?>" method="post" name="adminForm" id="adminForm">
	<div id="j-main-container">
		<div id="filter-bar">
			<div class="filter-search btn-group float-start">
				<label class="filter-search-lbl d-none" for="filter_search"><?php echo JText::_('JSEARCH_FILTER_LABEL'); ?></label>
				<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" />
				<button class="btn btn-light" data-bs-toggle="tooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn btn-light" data-bs-toggle="tooltip" type="button" onclick="document.getElementById('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
			</div>
			<div class="float-end hidden-phone ms-2">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="filter-select float-end ms-2">
				<select name="filter_state_id" class="input-medium" onchange="this.form.submit()">
					<option value=""><?php echo JText::_('LNG_JOPTION_SELECT_STATE');?></option>
					<?php echo JHtml::_('select.options', $this->states, 'value', 'text', $this->state->get('filter.state_id'));?>
				</select>
			</div>
			<div class="filter-select float-end ms-2">
				<select name="filter_status_id" class="input-medium" onchange="this.form.submit()">
					<option value=""><?php echo JText::_('LNG_JOPTION_SELECT_STATUS');?></option>
					<?php echo JHtml::_('select.options', $this->statuses, 'value', 'text', $this->state->get('filter.status_id'));?>
				</select>
			</div>
		</div>
	</div>
	<div class="clr clearfix"> </div>
	<table class="table table-striped adminlist" id="itemList">
		<thead>
			<tr>
				<th width="1%" class="hidden-phone">#</th>
				<th width="1%" class="hidden-phone">
					<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
				<th width='5%' ><?php echo JHtml::_('grid.sort', 'LNG_STATE', 'co.state', $listDirn, $listOrder); ?></th>
				<th width="6%" align="center" class="hidden-phone"><?php echo JText::_("LNG_IMAGE"); ?></th>
				<th width='18%' ><?php echo JHtml::_('grid.sort', 'LNG_NAME', 'co.name', $listDirn, $listOrder); ?></th>
				<th width='18%' ><?php echo JHtml::_('grid.sort', 'LNG_COMPANY', 'bc.name', $listDirn, $listOrder); ?></th>
				<th width='9%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_TYPE', 'co.type', $listDirn, $listOrder); ?></th>
				<th width='7%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_LOCATION', 'co.location', $listDirn, $listOrder); ?></th>
				<th width='10%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_START_DATE', 'co.start_date', $listDirn, $listOrder); ?></th>
				<th width='10%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_END_DATE', 'co.end_date', $listDirn, $listOrder); ?></th>
				<th class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_VIEW_NUMBER', 'co.view_count', $listDirn, $listOrder); ?></th>
				<th width='6%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_FEATURED', 'co.featured', $listDirn, $listOrder); ?></th>
				<th width='9%' ><?php echo JHtml::_('grid.sort', 'LNG_APROVED', 'co.approved', $listDirn, $listOrder); ?></th>
				<th width='10%'><?php echo JHtml::_('grid.sort', 'LNG_STATUS', 'co.status', $listDirn, $listOrder); ?></th>
				<th nowrap width='1%' class="hidden-phone"><?php echo JHtml::_('grid.sort', 'LNG_ID', 'co.id', $listDirn, $listOrder); ?></th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="15">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
		</tfoot>
		<tbody>
			<?php $nrcrt = 1; $i=0;
			foreach( $this->items as $event) { ?>
				<TR class="row<?php echo $i % 2; ?>">
					<TD class="center hidden-phone"><?php echo $nrcrt++?></TD>
					<TD class="center hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $event->id); ?>
					</TD>
					<td align="center">
						<img class="cursor-pointer"
							src="<?php echo JURI::base() ."components/".JBusinessUtil::getComponentName()."/assets/img/".($event->state==0? "no_joomla.png" : "yes_joomla.png")?>"
							onclick="document.location.href = '<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=event.chageState&id='. $event->id )?> '"
						/>
					</td>
					<td class="hidden-phone" align="center">
						<div class="item-image text-center">
							<a href='<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=event.edit&id='. $event->id )?>'>
								<?php if (!empty($event->picture_path)) { ?>
									<img src="<?php echo JURI::root().PICTURES_PATH.$event->picture_path ?>" class="img-circle"/>
								<?php } else { ?>
									<img src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>" class="img-circle"/>
								<?php } ?>
							</a>
						</div>
					</td>
					<td align="left">
						<div class="row-fluid">
							<a href='<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=event.edit&id='. $event->id )?>'
								title="<?php echo JText::_('LNG_CLICK_TO_EDIT'); ?>">
								<b><?php echo $event->name?></b>
							</a><br/>
							<div class="small"> (<?php echo JText::_("LNG_ALIAS")?>: <?php echo $event->alias?>) </div>
						</div>
						<div class="row-fluid jbd-buttons-row">
							<a href='<?php echo JBusinessUtil::getEventLink($event->id, $event->alias) ?>' target='_blank'
								title="<?php echo JText::_('LNG_CLICK_TO_VIEW'); ?>" class="btn-xs btn-primar btn-panel">
								<?php echo JText::_('LNG_VIEW'); ?>
							</a>
						</div>
					</td>
					<td>
						<?php echo $event->companyName ?>
					</td>
					<td class="hidden-phone" align="center">
						<?php echo $event->eventType?>
					</td>
					<td class="hidden-phone">
						<?php echo $event->address.", ".$event->city ?>
					</td>
					<td class="hidden-phone">
						<?php echo JBusinessUtil::getDateGeneralFormat($event->start_date) ?>
					</td>
					<td class="hidden-phone">
						<?php echo JBusinessUtil::getDateGeneralFormat($event->end_date) ?>
					</td>
					<td class="hidden-phone">
						<?php echo $event->view_count ?>
					</td>
					<td valign="top" align="center" class="hidden-phone">
						<img 
							src="<?php echo JURI::base() ."components/".JBusinessUtil::getComponentName()."/assets/img/".($event->featured==0? "unchecked.gif" : "checked.gif")?>" 
							onclick="document.location.href = '<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=event.changeStateFeatured&id='. $event->id )?> '"
						/>
					</td>
					<td nowrap="nowrap">
						<?php  
						switch($event->approved){
							case 0:
								echo JTEXT::_("LNG_NEEDS_APPROVAL");
								break;
							case -1:
								echo JTEXT::_("LNG_DISAPPROVED");
								break;
							case 1:
								echo JTEXT::_("LNG_APPROVED");
								break;
						} ?>
						&nbsp;
						<img style="vertical-align: bottom"
							src="<?php echo JURI::base() ."components/".JBusinessUtil::getComponentName()."/assets/img/".($event->approved==1? "checked.gif" : "unchecked.gif")?>" 
							onclick="document.location.href = '<?php echo JRoute::_( 'index.php?option=com_jbusinessdirectory&task=event.'.($event->approved==1?"disaprove":"aprove").'&id='. $event->id )?>'"
						/>
					</td>
					<td align="center">
					<?php if(($event->state == 1) && ($event->approved == 1)) {
						if($event->expired)
							echo '<span class="status-btn status-btn-warning">'.JText::_("LNG_EXPIRED").'</span>';
						elseif(!$event->allow_events)
							echo '<span class="status-btn status-btn-warning warn">'.JText::_("LNG_NOT_INCLUDED").'</span>';
						else
							echo '<span class="status-btn status-btn-success">'.JText::_("LNG_ACTIVE").'</span>';
					} else {
						echo '<span class="status-btn status-btn-danger">'.JText::_("LNG_NOT_ACTIVE").'</span>';
					} ?>
					</td>
					<td class="hidden-phone">
						<?php echo $event->id?>
					</td>
				</TR>
			<?php
				$i++;
			} ?>
		</tbody>
		<tfoot>
		<tr>
			<td colspan="16">
				<a href="#" id="open_legend_events">
					<h5 class="right"><?php echo JText::_('LNG_STATUS_MESSAGES_LEGEND'); ?></h5>
				</a>
			</td>
		</tr>
		</tfoot>
	</table>

	<!-- Modal -->
	<div id="legend_events" style="display:none;">
		<div class="dialog-container">
			<div class="titleBar">
				<span class="dialogTitle" id="dialogTitle"></span>
			<span title="Cancel" class="dialogCloseButton" onclick="jQuery.unblockUI();">
				<span title="Cancel" class="closeText">x</span>
			</span>
			</div>
			<div class="dialogContent">
				<div class="row-fluid">
					<div class="row-fluid">
						<div class="span10 offset1">
							<dl class="dl-horizontal">
								<dt><span class="status-btn status-btn-success"><?php echo JText::_('LNG_ACTIVE'); ?></span></dt>
								<dd><?php echo JText::_('LNG_ACTIVE_LEGEND'); ?></dd>
								<dt><span class="status-btn status-btn-warning"><?php echo JText::_('LNG_EXPIRED'); ?></span></dt>
								<dd><?php echo JText::_('LNG_EXPIRED_LEGEND'); ?></dd>
								<dt><span class="status-btn status-btn-warning warn"><?php echo JText::_('LNG_NOT_INCLUDED'); ?></span></dt>
								<dd><?php echo JText::_('LNG_NOT_INCLUDED_LEGEND'); ?></dd>
								<dt><span class="status-btn status-btn-danger"><?php echo JText::_('LNG_NOT_ACTIVE'); ?></span></dt>
								<dd><?php echo JText::_('LNG_NOT_ACTIVE_LEGEND'); ?></dd>
							</dl>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
	<input type="hidden" name="task" value="" /> 
	<input type="hidden" name="companyId" value="" />
	<input type="hidden" name="delete_mode" id="delete_mode" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHTML::_('form.token'); ?>

	<?php // Load the batch processing form. ?>
	<?php echo $this->loadTemplate('batch'); ?>
</form>

<div id="delete-event-dialog" style="display:none">
	<div id="dialog-container">
		<div class="titleBar">
			<span class="dialogTitle" id="dialogTitle"></span>
			<span title="Cancel" class="dialogCloseButton" onClick="jQuery.unblockUI();">
				<span title="Cancel" class="closeText">x</span>
			</span>
		</div>
		<div class="dialogContent">
			<h3 class="title"><?php echo JText::_('LNG_DELETE_RECURRING_EVENT') ?></h3>
	  		<div class="dialogContentBody" id="dialogContentBody">
				<p>
					<?php echo JText::_('LNG_DELETE_RECURRING_EVENT_INFO') ?>
				</p>
				<fieldset>
					<div>
						<button type="button" class="ui-dir-button" onclick="deleteEvent()">
							<span class="ui-button-text"> <?php echo JText::_("LNG_DELETE_ONLY_THIS_EVENT")?></span>
						</button>
						<?php echo JText::_('LNG_DELETE_ONLY_THIS_EVENT_INFO') ?>
					</div>
					<div>
						<button type="button" class="ui-dir-button" onclick="deleteAllFollowignEvents()">
							<span class="ui-button-text"> <?php echo JText::_("LNG_DELETE_ALL_FOLLOWINGS_EVENT")?></span>
						</button>
						<?php echo JText::_('LNG_DELETE_ALL_FOLLOWINGS_EVENT_INFO') ?>
					</div>
					<div>
						<button type="button" class="ui-dir-button" onclick="deleteAllSeriesEvents()">
							<span class="ui-button-text"> <?php echo JText::_("LNG_DELETE_ALL_SERIES_EVENTS")?></span>
						</button>
						<?php echo JText::_('LNG_DELETE_ALL_SERIES_EVENTS_INFO') ?>
					</div>
					<div class="clearfix clear-left">
					<div class="button-row">
						<button type="button" class="ui-dir-button ui-dir-button-grey" onclick="jQuery.unblockUI()">
							<span class="ui-button-text"><i class="dir-icon-remove-sign red"></i> <?php echo JText::_("LNG_CANCEL")?></span>
						</button>
					</div>
				</div>
				</fieldset>			
			</div>
		</div>
	</div>
</div>

<script>
	function showDeleteDialog() {
		jQuery.blockUI({ message: jQuery('#delete-event-dialog'), css: {width: 'auto',top: '10%', left:"0", position:"absolute"} });
		jQuery('.blockUI.blockMsg').center();
		jQuery('.blockOverlay').attr('title','Click to unblock').click(jQuery.unblockUI); 
	} 

	function deleteEvent() {
		jQuery("#delete_mode").val(1);
		Joomla.submitform('events.delete');
		jQuery.unblockUI();
	}

	function deleteAllFollowignEvents() {
		jQuery("#delete_mode").val(2);
		Joomla.submitform('events.delete');
		jQuery.unblockUI();
	}
	
	function deleteAllSeriesEvents() {
		jQuery("#delete_mode").val(3);
		Joomla.submitform('events.delete');
		jQuery.unblockUI();
	}

	jQuery(document).ready(function() {
		jQuery('#open_legend_events').click(function() {
			jQuery.blockUI({ message: jQuery('#legend_events'), css: {width: 'auto', top: '30%', left:"27%", position:"absolute", cursor:'default'} });
			jQuery('.blockUI.blockMsg').center();
			jQuery('.blockOverlay').attr('title','Click to unblock').click(jQuery.unblockUI);
			jQuery(document).scrollTop( jQuery("#legend_events").offset().top );
			jQuery("html, body").animate({ scrollTop: 0}, "slow");

			!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
		});
	});
</script>