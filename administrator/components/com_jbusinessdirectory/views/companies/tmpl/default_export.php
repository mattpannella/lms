<?php 
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.multiselect');
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task != 'companies.delete' || confirm("<?php echo JText::_('COM_JBUSINESS_DIRECTORY_COMPANIES_CONFIRM_DELETE', true);?>"))
		{
			Joomla.submitform(task);
		}
	}
</script>

<div class="modal fade" id="export-model">
	<div class="modal-dialog modal-fullscreen-md-down modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-header">
				<h3><?php echo JText::_('LNG_EXPORT_CSV'); ?></h3>
				<span class="btn-close" data-bs-dismiss="modal">&#215;</span>
			</div>
			<div class="modal-body">
				<p><?php echo JText::_('LNG_EXPORT_CSV_TEXT'); ?></p>
				<br/>
				<form action="<?php echo JRoute::_('index.php?option=com_jbusinessdirectory&view=companies');?>" method="post" name="exportForm" id="exportForm" enctype="multipart/form-data">
					<div class="row">
						<label for="delimiter"><?php echo JText::_('LNG_DELIMITER')?> </label> 
						<select name="delimiter">
							<option value=";"><?php echo JText::_('LNG_SEMICOLON')?></option>
							<option value=","><?php echo JText::_('LNG_COMMA')?></option>
						</select>
						
						<div class="clear"></div>
						<label for="category"><?php echo JText::_('LNG_CATEGORY')?> </label> 
						<select name="category" id="category">
							<option value="0"><?php echo JText::_("LNG_ALL_CATEGORIES") ?></option>
							<?php echo JHtml::_('select.options', $this->categoryOptions, 'value', 'text', null);?>
						</select>
						<br/>
						<div class="clear"></div>
						<input type="submit" name="submit" value="<?php echo JText::_("LNG_EXPORT");?>">		
					</div>

					<input type="hidden" name="option"	value="<?php echo JBusinessUtil::getComponentName()?>" />
					<input type="hidden" name="task" id="task" value="companies.exportCompaniesCsv" /> 
					<?php echo JHTML::_( 'form.token' ); ?> 
				</form>
			</div>
		</div>
	</div>
</div>