<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class JTableEventBookings extends JTable
{

    /**
     * Constructor
     *
     * @param object Database connector object
     */
    function __construct(&$db){

        parent::__construct('#__jbusinessdirectory_company_event_bookings', 'id', $db);
    }

    function setKey($k)
    {
        $this->_tbl_key = $k;
    }

    function getBookingDetails($bookingId){
        $db =JFactory::getDBO();
        $query = "select eb.*, SUM(ticket_quantity) as nr_tickets
                  from #__jbusinessdirectory_company_event_bookings eb
                  left join #__jbusinessdirectory_company_event_booking_tickets ebt on ebt.booking_id = eb.id
                  where eb.id = $bookingId";
        $db->setQuery($query);

        return $db->loadObject();
    }
    
    function getBookingTickets($bookingId){
    	$db =JFactory::getDBO();
    	
    	$query = "select et.*, ebt.ticket_quantity
    			from #__jbusinessdirectory_company_event_bookings eb
    			inner join #__jbusinessdirectory_company_event_booking_tickets ebt on ebt.booking_id = eb.id
    			inner join #__jbusinessdirectory_company_event_tickets et on et.id = ebt.ticket_id
    			where eb.id = $bookingId
    			group by et.id";
   
    	$db->setQuery($query);
		$result = $db->loadObjectList();
        
        
        return $result;
    }

    
    function getNumberOfBookings($eventId){
    	$db =JFactory::getDBO();
        $query = "select et.id, SUM(ebt.ticket_quantity) as nr_tickets
                  from #__jbusinessdirectory_company_event_tickets et
                  inner join #__jbusinessdirectory_company_event_booking_tickets ebt on ebt.ticket_id = et.id
                  inner join #__jbusinessdirectory_company_event_bookings eb on eb.id =  ebt.booking_id
                  where et.event_id = $eventId and eb.status!=".EVENT_BOOKING_CANCELED."
        		  group by et.id
        		";
        $db->setQuery($query);
		$result = $db->loadObjectList();

        return $result;
    }
    
    function updateBookingsStatus($bookingId, $status){
    	$db =JFactory::getDBO();
    	$query = " UPDATE #__jbusinessdirectory_company_event_bookings SET status=$status WHERE id = ".$bookingId ;
    	$db->setQuery( $query );
    	
    	if (!$db->query()){
    		return false;
    	}
    	return true;
    }
}