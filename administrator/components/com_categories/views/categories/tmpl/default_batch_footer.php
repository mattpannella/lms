<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_categories
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

?>
<a 
	onclick="document.getElementById('batch-category-id').value='';document.getElementById('batch-access').value='';document.getElementById('batch-language-id').value=''" 
	class="btn btn-secondary" 
	type="button"
	data-bs-dismiss="modal"
>
	<?php echo JText::_('JCANCEL'); ?>
</a>
<button onclick="Joomla.submitbutton('category.batch');" class="btn btn-success" type="submit" >
	<?php echo JText::_('JGLOBAL_BATCH_PROCESS'); ?>
</button>