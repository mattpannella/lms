<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_menus
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.multiselect');

$uri       = JUri::getInstance();
$return    = base64_encode($uri);
$user      = JFactory::getUser();
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$modMenuId = (int) $this->get('ModMenuId');

$script = array();
$script[] = 'jQuery(document).ready(function() {';

foreach ($this->items as $item) :
	if ($user->authorise('core.edit', 'com_menus')) :
		$script[] = '	function jSelectPosition_' . $item->id . '(name) {';
		$script[] = '		document.getElementById("' . $item->id . '").value = name;';
		$script[] = '		jQuery(".modal").modal("hide");';
		$script[] = '	};';
	endif;
endforeach;

$script[] = '	jQuery(".modal").on("hidden", function () {';
$script[] = '		setTimeout(function(){';
$script[] = '			window.parent.location.reload();';
$script[] = '		},1000);';
$script[] = '	});';
$script[] = '});';

JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));
?>
<form action="<?php echo JRoute::_('index.php?option=com_menus&view=menus'); ?>" method="post" name="adminForm" id="adminForm">
<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif; ?>
		<?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this, 'options' => array('filterButton' => false))); ?>
		<div class="clearfix"> </div>
		<?php if (empty($this->items)) : ?>
			<div class="alert alert-no-items">
				<?php echo JText::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
			</div>
		<?php else : ?>
			<table class="table table-striped" id="menuList" style="color:black">
				<thead>
					<tr>
						<th width="1%">
							<?php echo JHtml::_('grid.checkall'); ?>
						</th>
						<th>
							<?php echo JHtml::_('searchtools.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder); ?>
						</th>
						<th width="10%" class="nowrap center">
							<span class="icon-publish" aria-hidden="true"></span>
							<span class="hidden-phone"><?php echo JText::_('COM_MENUS_HEADING_PUBLISHED_ITEMS'); ?></span>
						</th>
						<th width="10%" class="nowrap center">
							<span class="icon-unpublish" aria-hidden="true"></span>
							<span class="hidden-phone"><?php echo JText::_('COM_MENUS_HEADING_UNPUBLISHED_ITEMS'); ?></span>
						</th>
						<th width="10%" class="nowrap center">
							<span class="icon-trash" aria-hidden="true"></span>
							<span class="hidden-phone"><?php echo JText::_('COM_MENUS_HEADING_TRASHED_ITEMS'); ?></span>
						</th>
						
						<th width="1%" class="nowrap hidden-phone">
							<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="15">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) :
					$canEdit        = $user->authorise('core.edit',   'com_menus.menu.' . (int) $item->id);
					$canManageItems = $user->authorise('core.manage', 'com_menus.menu.' . (int) $item->id);
				?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="center">
							<?php echo JHtml::_('grid.id', $i, $item->id); ?>
						</td>
						<td>
							<?php if ($canManageItems) : ?>
							<a href="<?php echo JRoute::_('index.php?option=com_menus&view=items&menutype=' . $item->menutype); ?>">
								<?php echo $this->escape($item->title); ?></a>
							<?php else : ?>
								<?php echo $this->escape($item->title); ?>
							<?php endif; ?>
							<div class="small">
								<?php echo JText::_('COM_MENUS_MENU_MENUTYPE_LABEL'); ?>:
								<?php if ($canEdit) : ?>
									<a href="<?php echo JRoute::_('index.php?option=com_menus&task=menu.edit&id=' . $item->id); ?>" title="<?php echo $this->escape($item->description); ?>">
									<?php echo $this->escape($item->menutype); ?></a>
								<?php else : ?>
									<?php echo $this->escape($item->menutype); ?>
								<?php endif; ?>
							</div>
						</td>
						<td class="center btns">
							<?php if ($canManageItems) : ?>
								<a class="badge<?php echo $item->count_published > 0 ? ' bg-success' : ' bg-secondary' ?>" href="<?php echo JRoute::_('index.php?option=com_menus&view=items&menutype=' . $item->menutype . '&filter[published]=1'); ?>">
									<?php echo $item->count_published; ?></a>
							<?php else : ?>
								<span class="badge<?php echo $item->count_published > 0 ? ' bg-success' : ' bg-secondary' ?>">
									<?php echo $item->count_published; ?></span>
							<?php endif; ?>
						</td>
						<td class="center btns">
							<?php if ($canManageItems) : ?>
								<a class="badge<?php echo $item->count_unpublished > 0 ? ' bg-danger' : ' bg-secondary' ?>" href="<?php echo JRoute::_('index.php?option=com_menus&view=items&menutype=' . $item->menutype . '&filter[published]=0'); ?>">
									<?php echo $item->count_unpublished; ?></a>
							<?php else : ?>
								<span class="badge<?php echo $item->count_unpublished > 0 ? ' bg-danger' : ' bg-secondary' ?>">
									<?php echo $item->count_unpublished; ?></span>
							<?php endif; ?>
						</td>
						<td class="center btns">
							<?php if ($canManageItems) : ?>
								<a class="badge<?php echo $item->count_trashed > 0 ? ' bg-light text-dark' : ' bg-secondary' ?>" href="<?php echo JRoute::_('index.php?option=com_menus&view=items&menutype=' . $item->menutype . '&filter[published]=-2'); ?>">
									<?php echo $item->count_trashed; ?></a>
							<?php else : ?>
								<span class="badge<?php echo $item->count_trashed > 0 ? ' bg-light text-dark' : ' bg-secondary' ?>">
									<?php echo $item->count_trashed; ?></span>
							<?php endif; ?>
						</td>
						
						<td class="hidden-phone">
							<?php echo $item->id; ?>
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		<?php endif; ?>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
