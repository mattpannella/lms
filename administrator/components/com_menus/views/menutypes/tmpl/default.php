<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_menus
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$input = JFactory::getApplication()->input;

// Checking if loaded via index.php or component.php
$tmpl = ($input->getCmd('tmpl') != '') ? '1' : '';

JHtml::_('behavior.core');
JFactory::getDocument()->addScriptDeclaration('
		setmenutype = function(type) {
			var tmpl = ' . json_encode($tmpl) . ';
			if (tmpl)
			{
				window.parent.Joomla.submitbutton("item.setType", type);
				window.parent.jQuery("#menuTypeModal").modal("hide");
			}
			else
			{
				window.location="index.php?option=com_menus&view=item&task=item.setType&layout=edit&type=" + type;
			}
		};
');

$user = JFactory::getUser();

//Get their admin permissioning level.
$isSuperUser = AxsUser::isSuperUser($user->id);

//if not a SuperUser set menu item types to just those allowed in the menus librabry

	$this->types = AxsMenuTypes::filterMenuTypes($this->types);
	ksort($this->types);


echo JLayoutHelper::render('joomla.html.accordion', array('types' => $this->types));
?>


