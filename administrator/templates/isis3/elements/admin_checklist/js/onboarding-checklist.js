function toggleChecklist(task) {
    if(task == 'open') {
    jQuery('.onboarding-clip-overlay').css('opacity',1);
    jQuery('.onboarding-sidebar-clipping').show();
    jQuery('.onboarding-sidebar-container').animate({right : '0px'},500);
    } else {
        jQuery('.onboarding-clip-overlay').css('opacity',0);
        jQuery('.onboarding-sidebar-container').animate({right : '-650px'},500);
        setTimeout(function(){
            jQuery('.onboarding-sidebar-clipping').hide();
        },700)

    }
}
jQuery('.onboarding-wrapper').click( function() { toggleChecklist('open') });
jQuery('.ob_sb_close-container').click( function() { toggleChecklist() });
jQuery(document).on('click', '.onboarding-sidebar-clipping', function(el) {
    if (jQuery('.onboarding-sidebar-container:hover').length == 0) {
        toggleChecklist()
    }
});


function runProgress(element,percentage,style) {
    if(style == 'circle') {
        var strokeWidth = 15;
        var trailWidth = 15;
        var width = 16;
        var color = '#fff';
        var symbol = '';
    } else {
        var strokeWidth = 0;
        var trailWidth = 0;
        var width = 0;
        var color = '#333';
        var symbol = '%';
    }
    var bar = new ProgressBar.Circle(element, {
        color: color,
        // This has to be the same size as the maximum width to
        // prevent clipping

        strokeWidth: strokeWidth,
        trailWidth: trailWidth,
        easing: 'easeInOut',
        duration: 1400,
        text: {
            autoStyleContainer: false
        },
        from: { color: '#1E2935', width: width },
        to: { color: '#1E2935', width: width },
        // Set default step function for all animate calls
        step: function(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);
            var value = Math.round(circle.value() * 100);
            if (value === 0) {
            circle.setText('');
            } else {
            circle.setText(value + symbol);
            }
        }
        });
    bar.text.style.fontFamily = 'sans-serif';
    bar.text.style.fontSize = '14px';
    bar.animate(percentage);  // Number from 0.0 to 1.0

}
runProgress(ob_progress,amountCompleted,'circle')
runProgress(ob_progress_2,amountCompleted,'line')