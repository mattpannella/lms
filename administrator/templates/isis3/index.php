<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @since       3.0
 */
defined('_JEXEC') or die;
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$doc->addStyleSheet('components/com_dashboard/assets/css/dashboard.css?v=2');

$lang            = JFactory::getLanguage();
$this->language  = $doc->language;
$this->direction = $doc->direction;
$input           = $app->input;
$user            = JFactory::getUser();
$userId 		 = $user->id;

$clientId = AxsClients::getClientId();
$clientStatus = AxsClientData::getClientStatus();
$clientName = AxsClientData::getClientName();
$siteUrl = JURI::root();
$onboardingChecklist = AxsClientData::getClientOnboardingChecklist();
$checklistObj = new AxsOnboardingChecklist($onboardingChecklist);
$brand = AxsBrands::getBrand();
$logo = $brand->homepage->logo;
$defaultFont = $brand->data->site_details->default_font;
//Get their admin permissioning level.
$isSuperUser = AxsUser::isSuperUser($user->id);
$isDeveloper = AxsUser::isDeveloper($user->id);
$isSiteAdmin = AxsUser::isAdmin($user->id);
$isSubadmin = AxsUser::isSubadmin($user->id);

//This stifles any messages that we don't want users to see, such as PHP warnings.
function killMessage($error) {
    $app = JFactory::getApplication();
    $appReflection = new ReflectionClass(get_class($app));
    $_messageQueue = $appReflection->getProperty('_messageQueue');
    $_messageQueue->setAccessible(true);
	$allMessages = $app->getMessageQueue();

	foreach ($allMessages as $key => $message) {
    	//See if the error message contains all or part of the error message.
        if (strpos($message['message'], $error) !== false) {
        	unset($allMessages[$key]);
        }
	}
	$_messageQueue->setValue($app, $allMessages);
}

killMessage("We have detected that your server is using PHP");
killMessage("Error loading component: com_fields, Component not found.");
killMessage("SMTP");
killMessage("Unable to load user");

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
JHtml::_('jquery.framework');

// Add layout stylesheet and scripts (includes bootstrap)
$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js', array('version' => 'auto'));
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', array('version' => 'auto'));
JHtml::_('script', 'administrator/templates/isis3/js/starred.js', array('version' => 'auto'));
JHtml::_('stylesheet', 'administrator/templates/isis3/scss/node_modules/@fortawesome/fontawesome-pro/css/all.min.css');

// Load specific language related CSS
$file = 'language/' . $lang->getTag() . '/' . $lang->getTag() . '.css';

if (is_file($file)) {
	$doc->addStyleSheetVersion($file);
}

// Detecting Active Variables
$option   = $input->get('option', '');
$view     = $input->get('view', '');
$layout   = $input->get('layout', '');
$task     = $input->get('task', '');
$itemid   = $input->get('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename', ''), ENT_QUOTES, 'UTF-8');
$cpanel   = ($option === 'com_cpanel');

$noView = false;

//If they're being directed to the CPanel and they're not a super user or developer, we want to route them to our new dashboard. /// old && !$isSuperUser && !$isDeveloper
if ($cpanel) {

	/*$redirect = null;
	$access = $_SESSION['admin_access_levels'];
	$allowed = false;

	if (isset($access->permissions) && isset($access->permissions["dashboard"])) {
		//See if they're allowed to go to the dashboard.
		$allowed = $access->permissions['dashboard']->allowed;
	}

	if ($isSiteAdmin) {
		$allowed = true;
	}*/

	$redirect = "/administrator/index.php?option=com_axs&view=splashpages";

	/*if ($allowed) {

    } else {
    	//They're not allowed, redirect them to their assigned homepage.
    	if (isset($access->homepage)) {
    		$homepage = $access->homepage;
    	} else {
    		$homepage = null;
    	}

        if ($homepage) {
		    $redirect = "/administrator/index.php?option=" . $homepage->option;
		    if ($homepage->view) {
		    	$views = explode(',', $homepage->view);
		        $redirect .= "&view=" . $views[0];
		    }
		}
    }*/

	if ($redirect) {
		//They have a redirect.  Send them there.
		$app->redirect($redirect);
	} else {
		//They don't have any redirect, so they probably have no permissions set.
		$noView = true;
	}
}

$hidden = JFactory::getApplication()->input->get('hidemainmenu');

$showSubmenu          = false;
$this->submenumodules = false; //JModuleHelper::getModules('submenu');

if(is_iterable($this->submenumodules)) {

	foreach ($this->submenumodules as $submenumodule) {
		$output = JModuleHelper::renderModule($submenumodule);

		if (strlen($output)) {
			$showSubmenu = true;
			break;
		}
	}
}

// Template Parameters
$displayHeader = $this->params->get('displayHeader', '1');
$statusFixed   = $this->params->get('statusFixed', '1');
$stickyToolbar = $this->params->get('stickyToolbar', '1');
?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />

	<jdoc:include type="head" />

	<!-- Template color -->
	<?php
	$fonts = AxsFonts::getFonts($brand->id);
	foreach ($fonts as $font) {
		if ($font == $defaultFont) {
			if ($font->link) {
				echo $font->link . "\n";
			} else {
				echo "<link href='//fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>" . "\n";
			}
		}
	}
	?>

  <link rel="apple-touch-icon" sizes="180x180" href="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/tovuti-icon.png" />
  <link rel="icon" href="https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/tovuti-icon.png" type ="image/x-icon" />

  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--[if lt IE 9]>
		<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="/assets/scroll/jquery.nanoscroller.js"></script>

	<script src="https://unpkg.com/@popperjs/core@2"></script>
	<script src="https://unpkg.com/tippy.js@6"></script>

	<style type="text/css">
		<?php
		if ($this->params->get('templateColor')) {
		?>
		@media only screen and (min-width: 768px) {
			.admin_main {
				padding-left: 328px;
			}
		}
		@media only screen and (max-width: 768px) {
			.admin_main {
				padding-top: 48px !important;
			}
			.tov-sidenav-radius {
				display: none;
			}
		}
		.admin_main {
			width: 100%;
			padding-top: 56px;
			min-height: 100%;
			position: absolute;
		}

		.beta_tag {
			background: #007aff !important;
			height: 20px;
			color: white;
			border-radius: 20px;
			text-align: center;
			line-height: 16px;
		}
		.beta_tag::before {
			font-size: 11px;
			content: 'beta';
		}

		#system-message-container {
			position: fixed;
			right: 15px;
			top: 75px;
			z-index: 1000;
		}

		#tovuti-admin-search {
			margin-top: 15px;
		}
		<?php
		}

		if ($this->params->get('headerColor')) {
		?>

		/* Template header color */
		.header {
			background: #283848;
		}

		<?php
		}

		if ($this->params->get('sidebarColor')) {
		?>

		/* Sidebar background color */
		.nav-list>.active>a,
		.nav-list>.active>a:hover {
			background: <?php echo $this->params->get('sidebarColor'); ?>;
		}

		<?php
		}

		if ($option == 'com_interactivecontent') {
			?>

			.container-main {
				padding: 16px;
			}
			<?php
			}

		if ($this->params->get('linkColor')) {
		?>

		/* Link color */
		a,
		.j-toggle-sidebar-button {
			color: <?php echo $this->params->get('linkColor'); ?>;
		}

		<?php
		}
		?>

		.admin-avatar {
			height: 60px;
			width: 60px;
			border-radius: 50%;
			cursor: pointer;
			padding: 7px;
		}

		#jsd-widget,._pendo-badge {
			z-index: 1000 !important;
		}
	</style>

	<link rel="stylesheet" href="/media/player/plyr.css" />

	<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=es6,Array.prototype.includes,CustomEvent,Object.entries,Object.values,URL,Math.trunc" crossorigin="anonymous"></script>
	<script src="/media/player/plyr.js"></script>
	<link href="/administrator/templates/isis3/elements/admin_checklist/css/onboarding-checklist.css?v=4" rel="stylesheet" type="text/css">

	<?php if($onboardingChecklist) { ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/progressbar.js/1.1.0/progressbar.min.js" integrity="sha256-c83qPqBpH5rEFQvgyTfcLufqoQIFFoqE5B71yeBXhLc=" crossorigin="anonymous"></script>
	<?php } ?>

</head>

<body class="admin <?php echo $option . ' view-' . $view . ' layout-' . $layout . ' task-' . $task . ' itemid-' . $itemid; ?>">
	<div id="trialToast" class="toast hide position-absolute top-0 start-50 translate-middle-x
 text-white bg-primary border-0" role="alert" aria-live="assertive" aria-atomic="true" style="z-index: 2000; margin-top: 74px;">
		<div class="d-flex">
			<div class="toast-body">
			Request received.
			A Tovuti Team Member will contact you!
			</div>
			<button type="button" class="btn-close btn-close-white me-2 mt-2" data-bs-dismiss="toast" aria-label="Close"></button>
		</div>
	</div>
	<!-- Top Navigation -->
	<header>
		<?php echo AxsTopBar::renderTopBar(); ?>
	</header>
	<?php
	if ($onboardingChecklist) {
		echo $checklistObj->buildChecklistHTML();
	}
	?>

		<div class="admin_sidebar">
			<div id="admin_menu_buttons" >
				<?php
					echo AxsSideNav::renderSideMenu();
				?>
			</div>
		</div>
	</div>

	<div class="admin_main">
		<?php
		if ($displayHeader && !$noView) {
		?>
			<header class="tov-content-header">
				<jdoc:include type="modules" name="title" />
			</header>
		<?php
		}

		if ((!$statusFixed) && ($this->countModules('status'))) {
		?>
			<!-- Begin Status Module -->
			<div id="status" class="navbar status-top hidden-phone">
				<div class="btn-toolbar">
					<jdoc:include type="modules" name="status" style="no" />
				</div>
				<div class="clearfix"></div>
			</div>
			<!-- End Status Module -->
		<?php
		}

		if (!$cpanel) {
		?>
			<!-- Subheader -->
			<div class="tov-content-actionbar">
				<div class="tov-actionbar-radius-top">
					<svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M8 0H0V8C0 3.6 3.6 0 8 0Z" fill="#374151"></path>
					</svg>
				</div>
				<jdoc:include type="modules" name="toolbar" style="no" />
			</div>
		<?php
		} else {
		?>
			<div style="margin-bottom: 20px"></div>
		<?php
		}
		?>

		<?php
		if ($noView) {
		?>
			<style>
				.no_access {
					font-weight: bold;
					font-size: 18px;
					padding: 10px;

				}
			</style>

			<div class="no_access">
				You do not have access to any administrative views.<br>
				Please contact an administrator to check your access levels.
			</div>
		<?php
			return;
		}
		?>

		<!-- container-fluid -->

		<div class="container-main">
			<section id="content">
				<jdoc:include type="modules" name="bottom" style="xhtml" />
				<!-- Begin Content -->
				<jdoc:include type="modules" name="top" style="xhtml" />
				<div>
					<?php
						// Show the page title here if the header is hidden
						if (!$displayHeader) {
						?>
							<h1 class="content-title">
								<?php echo JHtml::_('string.truncate', $app->JComponentTitle, 0, false, false); ?>
							</h1>
						<?php
						}
					?>
					<jdoc:include type="component" />
					<jdoc:include type="message" />
				</div>
				<?php
				if ($this->countModules('bottom')) {
				?>
				<?php
				}
				?>
				<!-- End Content -->
			</section>

			<?php
			if (!$this->countModules('status') || (!$statusFixed && $this->countModules('status'))) {
			?>
				<div class="footer clearfix" style="position:relative; width:100%; clear:both;">
					<p align="center">
						&copy; <?php echo $sitename . ' ' . date('Y'); ?>
					</p>
				</div>
			<?php } ?>
		</div>


	</div>

	<jdoc:include type="modules" name="debug" style="none" />

	<?php
	if ($stickyToolbar) {
	?>

		<script>
			jQuery(function($)
			{

				var navTop;
				var isFixed = false;

				processScrollInit();
				processScroll();

				$(window).on('resize', processScrollInit);
				$(window).on('scroll', processScroll);

				function processScrollInit()
				{
					if ($('.subhead').length) {
						navTop = $('.subhead').length && $('.subhead').offset().top - <?php echo ($displayHeader || !$statusFixed) ? 30 : 20;?>;

						// Only apply the scrollspy when the toolbar is not collapsed
						if (document.body.clientWidth > 480)
						{
							$('.subhead-collapse').height($('.subhead').height());
							$('.subhead').scrollspy({offset: {top: $('.subhead').offset().top - $('nav.navbar').height()}});
						}
					}
				}

				function processScroll()
				{
					if ($('.subhead').length) {
						var scrollTop = $(window).scrollTop();
						if (scrollTop >= navTop && !isFixed) {
							isFixed = true;
							$('.subhead').addClass('subhead-fixed');
						} else if (scrollTop <= navTop && isFixed) {
							isFixed = false;
							$('.subhead').removeClass('subhead-fixed');
						}
					}
				}
			});
		</script>
	<?php
	}
	?>
	<style>
		#jsd-widget {
			right: 66px !important;
    		bottom: -7px !important;
		}
	</style>
	<?php if($clientStatus == 'active' || $clientStatus == 'active (onboarding)') { ?>
		<script data-jsd-embedded data-key="9b6bfeef-24be-4d47-b3b9-fbc3e2d50f26" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>
	<?php } else { ?>
		<script data-jsd-embedded data-key="8f279172-31ed-44f3-9a32-ee81f03d4875" data-base-url="https://jsd-widget.atlassian.com" src="https://jsd-widget.atlassian.com/assets/embed.js"></script>
	<?php } ?>

	<?php if($clientStatus == 'trial') { ?>
		<style>
			#jsd-widget {
				right: 136px !important;
			}
			#hubspot-messages-iframe-container.widget-align-right {
				z-index: 200 !important;
				right: 60px !important;
				bottom: -8px !important;
			}
		</style>
		<script type="text/javascript" id="hs-script-loader" async defer src="//js-na1.hs-scripts.com/4789974.js"></script>
	<?php } ?>

	<script>
		jQuery.ajax({
			type: 'POST',
			url: '/index.php?option=com_axs&task=update.session&format=raw'
		});
	</script>

	<?php if($clientStatus != "dev") { ?>
		<script>
			(function(apiKey){
				(function(p,e,n,d,o){var v,w,x,y,z;o=p[d]=p[d]||{};o._q=o._q||[];
				v=['initialize','identify','updateOptions','pageLoad','track'];for(w=0,x=v.length;w<x;++w)(function(m){
					o[m]=o[m]||function(){o._q[m===v[0]?'unshift':'push']([m].concat([].slice.call(arguments,0)));};})(v[w]);
					y=e.createElement(n);y.async=!0;y.src='https://cdn.pendo.io/agent/static/'+apiKey+'/pendo.js';
					z=e.getElementsByTagName(n)[0];z.parentNode.insertBefore(y,z);})(window,document,'script','pendo');

					pendo.initialize({
						visitor: {
							id:             '<?php echo $user->email; ?>'
						},

						account: {
							id:             '<?php echo $clientId; ?>',
							AccountName:    '<?php echo $clientName; ?>',
							AccountStatus:  '<?php echo $clientStatus; ?>',
							SiteURL:        '<?php echo $siteUrl; ?>'
						}
					});

			})('60a6ad13-46f0-46bf-57f6-c8e1b6b76839');
		</script>
	<?php } ?>


</body>
</html>
