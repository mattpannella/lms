(function($) {
	$(document).ready(function() {
		// enable all bootstrap tooltips on the page
		var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
		tooltipTriggerList.map(function (tooltipTriggerEl) {
			return new bootstrap.Tooltip(tooltipTriggerEl)
		});

		// enable all bootstrap popovers on the page
		var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
		popoverTriggerList.map(function (popoverTriggerEl) {
			popoverTriggerEl.setAttribute("data-bs-trigger", "hover");
			return new bootstrap.Popover(popoverTriggerEl)
		})

		var toastElList = [].slice.call(document.querySelectorAll('.toast'));
		toastElList.map(function (toastEl) {
			return new bootstrap.Toast(toastEl);
		});

		jQuery(document).on('click', '#hideToast', () => {
			var systemMessageToastEl = document.getElementById('systemMessageToast');
			var systemMessageToast = bootstrap.Toast.getInstance(systemMessageToastEl);
			if (systemMessageToast == null) {
				systemMessageToast = new bootstrap.Toast(systemMessageToastEl);
			}
			systemMessageToast.hide();
		});
		
	});
})(jQuery);