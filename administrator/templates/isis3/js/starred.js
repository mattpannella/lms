$(() => {
	$('#starredIcon').click(function (ev) {
        // Need to use .dataset here because jQuery wont update 
        // data for some reason
		if (ev.target.dataset.starredId) {
			TovutiStarred.removeStarredItem(ev.target.dataset.starredId);
		} else {
			TovutiStarred.addStarredItem();
		}
	});
});

const TovutiStarred = {
	renderStarredItems: function() {
		jQuery.ajax({
			type: "GET",
			url: "index.php?option=com_axs&task=starred.renderStarredItems&format=raw",
		}).done((result) => {
			result = JSON.parse(result);
			if (result.success) {
				jQuery('.tov-starred').replaceWith(result.html);
			}
		}).fail((err) => {
			console.error(err);
		});
	},
	removeStarredItem: function(ev, item_id) {
		// The remove button is within the starred menu item,
		// so prevent it from changing the page with stopPropagation
		ev.stopPropagation();
		// This item is starred, unstar it
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_axs&task=starred.removeStarredItem&format=raw",
			data: {
				"item_id": item_id
			}
		}).done((result) => {
			result = JSON.parse(result);
			if (result.success === true) {
				// Turn starIcon to outlined version if we just removed
				// the starred item for the current page
				var starredIcon = $('#starredIcon');
				if (starredIcon.data('starred-id') == item_id) {
					starredIcon.removeClass('fas fa-star');
					starredIcon.addClass('far fa-star');
					starredIcon.removeAttr('data-starred-id');
				}
				// Find the category name and remove
				// the items from the dropdown
				var category = null;
				jQuery('.tov-starred')
					.find('[data-item-id="' + item_id + '"]')
					.each((idx, el) =>  {
						if (el.dataset.itemCategory) {
							category = el.dataset.itemCategory;
						}
						jQuery(el).remove();
					});
				
				// Check if the category is empty, remove 
				// the tab and show the first tab
				var categoryTab = jQuery('.tov-starred').find('#' + category);
				if (categoryTab.find('[data-item-id]').length == 0) {
					var categoryButton = jQuery('.tov-starred').find('#' + category + '-tab');
					categoryButton.remove();
					categoryTab.remove();
					var tab = new bootstrap.Tab(jQuery('#starred-filters').find('.nav-link').first());
					tab.show();
					window.localStorage.setItem('lastActiveStarredCategory', 'all')
				}
			}
		}).fail((err) => {
			console.error(err);
		});
	},
	addStarredItem: function() {
		// This item is not starred, star it
		// Get the title from the page title
		const data = {
			category: jQuery('#adminNavContainer').data('active-item'),
			text: jQuery('#pageTitle').text(),
			uri: '/administrator/index.php' + window.location.search
		};
		jQuery.ajax({
			type: "POST",
			url: "index.php?option=com_axs&task=starred.addStarredItem&format=raw",
			data
		}).done((result) => {
			result = JSON.parse(result);
			if (result.id !== null) {
				var starredIcon = $('#starredIcon');
				starredIcon.removeClass('far fa-star');
				starredIcon.addClass('fas fa-star');
				starredIcon.attr('data-starred-id', result.id);
				TovutiStarred.renderStarredItems();
			}
		}).fail((err) => {
			console.error(err);
		});
	}
};