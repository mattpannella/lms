<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.isis
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$lang            = JFactory::getLanguage();
$this->language  = $doc->language;
$this->direction = $doc->direction;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

$doc->addScript($this->baseurl . '/templates/' . $this->template . '/js/template.js', array('version' => 'auto'));

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', array('version' => 'auto'));

// Load optional RTL Bootstrap CSS
// JHtml::_('bootstrap.loadCss', false, $this->direction);

// Load specific language related CSS
$file = 'language/' . $lang->getTag() . '/' . $lang->getTag() . '.css';

if (is_file($file))
{
	$doc->addStyleSheet($file);
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		<jdoc:include type="head" />
		<!--[if lt IE 9]>
			<script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
		<![endif]-->

		<!-- Link color -->
		<style>
			#page {
				display: none;
			}
			#loading {
				display: block;
				position: absolute;
				top: 0;
				left: 0;
				z-index: 100;
				width: 100vw;
				height: 100vh;
			
				background-image: url("/administrator/components/com_axs/assets/img/gears.gif");
				background-repeat: no-repeat;
				background-position: center;
			}
		</style>

		<script>
			function onReady(callback) {
				var intervalID = window.setInterval(checkReady, 1000);
				function checkReady() {
					if (document.getElementsByTagName('body')[0] !== undefined) {
						window.clearInterval(intervalID);
						callback.call(this);
					}
				}
			}

			function show(id, value) {
				document.getElementById(id).style.display = value ? 'block' : 'none';
			}

			onReady(function () {
				show('page', true);
				show('loading', false);
			});

		</script>	
	</head>

	<body class="contentpane component">
		<div id="loading"></div>
		<div id="page">
			<div class="d-flex">
				<div class="btn-toolbar" id="toolbar">
					<div class="btn-wrapper" id="toolbar-apply">
						<button onclick="Joomla.submitbutton('module.apply')" class="btn btn-success">
						<span class="fas fa-save"></span>
						Save</button>
					</div>
					<div class="btn-wrapper ms-2" id="toolbar-save-copy">
						<button onclick="Joomla.submitbutton('module.save2copy')" class="btn border">
						<span class="icon-save-copy"></span>
						Save as Copy</button>
					</div>
				</div>
			</div>
			<jdoc:include type="message" />
			
			<jdoc:include type="component" />
		</div>
		
		<script>
			var action = jQuery('#module-form').attr('action');
			jQuery('#module-form').attr('action', action+'&tmpl=module');
		</script>
	</body>
</html>
