<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Template.Isis
 *
 * @copyright   Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

function renderMessage($msgList)
{
	$buffer  = null;
	$buffer .= "\n<div id=\"system-message-container\">";
	$alert = array('error' => 'text-danger', 'warning' => 'text-warning', 'notice' => '', 'message' => 'text-success');
	$bg = array('error' => 'bg-danger', 'warning' => 'bg-warning', 'notice' => '', 'message' => 'bg-success');
	$title = array('error' => 'Error', 'warning' => 'Warning', 'notice' => 'Notice', 'message' => 'Success');

	if (!empty($msgList) && in_array('message', array_keys($msgList)) && count(array_unique(array_keys($msgList))) == 1) {
		$buffer .= "<script>setTimeout(() => jQuery('#system-message-container').fadeOut(), 4000)</script>";
	}

	// Only render the message list and the close button if $msgList has items
	if (is_array($msgList) && (count($msgList) >= 1))
	{
		
		foreach ($msgList as $type => $msgs)
		{
			$buffer .= '<div id="systemMessageToast" class="toast bg-white show mb-3">';
			$buffer .= '
			<div class="toast-header ' . $alert[$type] . ' ">
				<span class="'. $bg[$type] .' rounded p-2 me-2"></span>
				<span class="me-auto">' . JText::_($title[$type]) . '</span>
				<button id="hideToast" type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
			</div>';
			$buffer .= '
				<div class="toast-body">
			';
			if (count($msgs))
			{
				foreach ($msgs as $msg)
				{
					if ($msg == '<li></li>') {
						continue;
					}
					$buffer .= "\n\t\t" . $msg . "<br />";
				}
			}
			$buffer .= "\n</div>";
			$buffer .= "\n</div>";
		}
	}

	$buffer .= "\n</div>";

	return $buffer;
}
