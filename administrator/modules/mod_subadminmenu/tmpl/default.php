<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_subadminmenu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
//$showhelp = $params->get('showhelp', 1);
	$user = JFactory::getUser();
	$isSuper = $user->authorise('core.admin');

	function make_horizontal_menu($menu, $submenu) {
		
			if($submenu < 1) {
				$ul_attributes = 'id="menu" class="nav"';
			} else {
				$ul_attributes = 'class="dropdown-menu"';
			}
	        $output ='<ul '.$ul_attributes.'>';
	    for ($i = 0; $i < count($menu); $i++) {

	        $name = $menu[$i]->name;
	        $id = $menu[$i]->id;
	        $icon = $menu[$i]->icon;
	        $function = $menu[$i]->function;

	        if (is_array($function)){
	        	if($submenu >= 1) { 

	        		$li_attributes = 'class="dropdown-submenu"';
	        		$span_attributes = '';

	        	} else {

					$li_attributes = 'class="dropdown"';
					$span_attributes = 'class="caret"';

				}

				$a_attributes = 'class="dropdown-toggle" data-bs-toggle="dropdown" href="#"';
				
			} else {
				
				$li_attributes = '';
				$a_attributes = "onclick='".$function."' href='#'";
				$span_attributes = '';
			}

	        $output .= '<li '.$li_attributes.'><a '.$a_attributes.'>'.$name.'<span '.$span_attributes.'></span></a>';

	         if (is_array($function)){
	        	
	        	$submenu = make_horizontal_menu($function,$submenu + 1);
	        	$output .= $submenu;
	        }
	        $output .= '</li>';

	    }
	    	$output .='</ul>';
	        return $output;
	}

if(!$isSuper) {

	
	$menu = AxsSideNav::getItems();		
	$topNavBar = make_horizontal_menu($menu,0);
    echo $topNavBar;
?>
<script>
function redirect(location) {
    window.location = location;
}
</script>
<!--<ul id="menu" class="nav">
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">System <span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a class="menu-cpanel" href="index.php">Control Panel</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-config" href="index.php?option=com_config">Global Configuration</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-checkin" href="index.php?option=com_checkin">Global Check-in</a></li>
<li><a class="menu-clear" href="index.php?option=com_cache">Clear Cache</a></li>
<li><a class="menu-purge" href="index.php?option=com_cache&amp;view=purge">Clear Expired Cache</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-info" href="index.php?option=com_admin&amp;view=sysinfo">System Information</a></li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Users <span class="caret"></span></a>
<ul class="dropdown-menu">
<li class="dropdown-submenu">
<a class="dropdown-toggle" data-bs-toggle="dropdown" href="index.php?option=com_users&amp;view=axsusers">Manage</a>
<ul  class="dropdown-menu">
<li>
<a  href="https://sandbox.allaxs.com/administrator/index.php?option=com_users&amp;task=axsuser.add">Add New User</a>
</li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-groups" data-bs-toggle="dropdown" href="index.php?option=com_users&amp;view=groups">Groups</a><ul id="menu-com-users-groups" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_users&amp;task=group.add">Add New Group</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-levels" data-bs-toggle="dropdown" href="index.php?option=com_users&amp;view=levels">Access Levels</a><ul id="menu-com-users-levels" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_users&amp;task=level.add">Add New Access Level</a></li>
</ul>
</li>
<li class="divider"><span></span></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-user-note" data-bs-toggle="dropdown" href="index.php?option=com_users&amp;view=notes">User Notes</a><ul id="menu-com-users-notes" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_users&amp;task=note.add">Add User Note</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-category" data-bs-toggle="dropdown" href="index.php?option=com_categories&amp;view=categories&amp;extension=com_users">User Note Categories</a><ul id="menu-com-categories-categories-com-users" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_categories&amp;task=category.add&amp;extension=com_users">Add New Category</a></li>
</ul>
</li>
<li class="divider"><span></span></li>
<li><a class="menu-massmail" href="index.php?option=com_users&amp;view=mail">Mass Mail Users</a></li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Menus <span class="caret"></span></a><ul class="dropdown-menu">
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menumgr" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=menus">Manage</a><ul id="menu-com-menus-menus" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=menu&amp;layout=edit">Add New Menu</a></li>
</ul>
</li>
<li class="divider"><span></span></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menu" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=items&amp;menutype=control-central">Control Central</a><ul id="menu-com-menus-items-control-central" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=item&amp;layout=edit&amp;menutype=control-central">Add New Menu Item</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menu" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=items&amp;menutype=hidden2">Hidden2 <span class="icon-home"></span></a><ul id="menu-com-menus-items-hidden2" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=item&amp;layout=edit&amp;menutype=hidden2">Add New Menu Item</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menu" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=items&amp;menutype=adagency">LIZ Ads</a><ul id="menu-com-menus-items-adagency" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=item&amp;layout=edit&amp;menutype=adagency">Add New Menu Item</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menu" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=items&amp;menutype=jomsocial">LIZ Member NavBar</a><ul id="menu-com-menus-items-jomsocial" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=item&amp;layout=edit&amp;menutype=jomsocial">Add New Menu Item</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-menu" data-bs-toggle="dropdown" href="index.php?option=com_menus&amp;view=items&amp;menutype=mainmenu">Main Menu</a><ul id="menu-com-menus-items-mainmenu" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_menus&amp;view=item&amp;layout=edit&amp;menutype=mainmenu">Add New Menu Item</a></li>
</ul>
</li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Content <span class="caret"></span></a><ul class="dropdown-menu">
<li class="dropdown-submenu"><a class="dropdown-toggle menu-article" data-bs-toggle="dropdown" href="index.php?option=com_content">Articles</a><ul id="menu-com-content" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_content&amp;task=article.add">Add New Article</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-category" data-bs-toggle="dropdown" href="index.php?option=com_categories&amp;extension=com_content">Categories</a><ul id="menu-com-categories-com-content" class="dropdown-menu menu-component">
<li><a class="menu-newarticle" href="index.php?option=com_categories&amp;task=category.add&amp;extension=com_content">Add New Category</a></li>
</ul>
</li>
<li><a class="menu-featured" href="index.php?option=com_content&amp;view=featured">Featured Articles</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-media" href="index.php?option=com_media">Media</a></li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Components <span class="caret"></span></a><ul class="dropdown-menu">
<li><a class="menu-community-favicon" href="index.php?option=com_community">★ JomSocial</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-s_icon" data-bs-toggle="dropdown" href="index.php?option=com_improved_ajax_login"><i class="icon-key"></i>&nbsp;Improved AJAX Login Register</a><ul id="menu-com-improved-ajax-login" class="dropdown-menu menu-component">
<li><a class="menu-s_modules" href="index.php?option=com_improved_ajax_login&amp;view=modules"><i class="icon-cube"></i>&nbsp;Module Manager</a></li>
<li><a class="menu-s_forms" href="index.php?option=com_improved_ajax_login&amp;view=forms"><i class="icon-wrench"></i>&nbsp;Form Manager</a></li>
<li><a class="menu-s_oauths" href="index.php?option=com_improved_ajax_login&amp;view=oauths"><i class="icon-users"></i>&nbsp;Social Settings</a></li>
</ul>
</li>
<li><a class="menu-jrealtime-16x16" href="index.php?option=com_jrealtimeanalytics"><img src="components/com_jrealtimeanalytics/images/jrealtime-16x16.png"> JRealtime Analytics</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-banners" data-bs-toggle="dropdown" href="index.php?option=com_banners">Banners</a><ul id="menu-com-banners" class="dropdown-menu menu-component">
<li><a class="menu-banners" href="index.php?option=com_banners">Banners</a></li>
<li><a class="menu-banners-cat" href="index.php?option=com_categories&amp;extension=com_banners">Categories</a></li>
<li><a class="menu-banners-clients" href="index.php?option=com_banners&amp;view=clients">Clients</a></li>
<li><a class="menu-banners-tracks" href="index.php?option=com_banners&amp;view=tracks">Tracks</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-user_add" data-bs-toggle="dropdown" href="index.php?option=com_invitex">com-invitex</a><ul id="menu-com-invitex" class="dropdown-menu menu-component">
<li><a class="menu-invites" href="index.php?option=com_invitex&amp;view=invites">com-invite-invites</a></li>
<li><a class="menu-users" href="index.php?option=com_invitex&amp;view=topinviters">com-invite-top-inviters</a></li>
<li><a class="menu-process" href="index.php?option=com_invitex&amp;view=config&amp;layout=templates">com-invite-template</a></li>
<li><a class="menu-types" href="index.php?option=com_invitex&amp;view=types">com-types</a></li>
<li><a class="menu-limit" href="index.php?option=com_invitex&amp;view=invitation_limit">com-invite-limit</a></li>
<li><a class="menu-unsubscribe" href="index.php?option=com_invitex&amp;view=unsubscribe_list">com-unsub-users</a></li>
<li><a class="menu-reminder" href="index.php?option=com_invitex&amp;view=reminder">com-reminder</a></li>
</ul>
</li>
<li><a class="menu-component" href="index.php?option=com_lifeboards">com-lifeboard</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-contact" data-bs-toggle="dropdown" href="index.php?option=com_contact">Contacts</a><ul id="menu-com-contact" class="dropdown-menu menu-component">
<li><a class="menu-contact" href="index.php?option=com_contact">Contacts</a></li>
<li><a class="menu-contact-cat" href="index.php?option=com_categories&amp;extension=com_contact">Categories</a></li>
</ul>
</li>
<li><a class="menu-component" href="index.php?option=com_converge">Converge Payment Gateway</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-easyblog-16x16" data-bs-toggle="dropdown" href="index.php?option=com_easyblog">EasyBlog</a><ul id="menu-com-easyblog" class="dropdown-menu menu-component">
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=settings">Settings</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=autoposting">Auto Posting</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=blogs">Posts</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=blocks">Blocks</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=fields">Custom Fields</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=categories">Categories</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=comments">Comments</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=acls">ACL</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=bloggers">Authors</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=feeds">Feed Importer</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=spools">Mail Activities</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=languages">Languages</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=metas">SEO</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=migrators">Migrators</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=reports">Reports</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=subscriptions">Subscriptions</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=tags">Tags</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=teamblogs">Team Blogs</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=themes">Themes</a></li>
<li><a class="menu-component" href="index.php?option=com_easyblog&amp;view=maintenance">Maintenance</a></li>
</ul>
</li>
<li><a class="menu-component" href="index.php?option=com_eshop">EShop</a></li>
<li><a class="menu-component" href="index.php?option=com_dump">j-dump</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-logo" data-bs-toggle="dropdown" href="index.php?option=com_jce">JCE Editor</a><ul id="menu-com-jce" class="dropdown-menu menu-component">
<li><a class="menu-jce-cpanel" href="index.php?option=com_jce">Control Panel</a></li>
<li><a class="menu-jce-config" href="index.php?option=com_jce&amp;view=config">Global Configuration</a></li>
<li><a class="menu-jce-profiles" href="index.php?option=com_jce&amp;view=profiles">Profiles</a></li>
<li><a class="menu-jce-install" href="index.php?option=com_jce&amp;view=installer">Install Add-ons</a></li>
</ul>
</li>
<li><a class="menu-joomlaupdate" href="index.php?option=com_joomlaupdate">Joomla! Update</a></li>
<li><a class="menu-jumi" href="index.php?option=com_jumi">Jumi</a></li>
<li><a class="menu-component" href="index.php?option=com_axs">Mega Axs</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-component" data-bs-toggle="dropdown" href="index.php?option=com_osmembership">Membership Pro</a><ul id="menu-com-osmembership" class="dropdown-menu menu-component">
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=dashboard">Dashboard</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=configuration">Configuration</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=categories">Categories</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=plans">Subscription Plans</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=subscriptions">Subscriptions</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=subscriptions">Group Members</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=fields">Custom Fields</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=taxes">Tax Rules</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=coupons">Coupons</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=import">Import Subscribers</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=plugins">Payment Plugins</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=message">Emails &amp; Messages</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=language">Translation</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=countries">Countries</a></li>
<li><a class="menu-component" href="index.php?option=com_osmembership&amp;view=states">States</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-messages" data-bs-toggle="dropdown" href="index.php?option=com_messages">Messaging</a><ul id="menu-com-messages" class="dropdown-menu menu-component">
<li><a class="menu-messages-add" href="index.php?option=com_messages&amp;task=message.add">New Private Message</a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-newsfeeds" data-bs-toggle="dropdown" href="index.php?option=com_newsfeeds">News Feeds</a><ul id="menu-com-newsfeeds" class="dropdown-menu menu-component">
<li><a class="menu-newsfeeds" href="index.php?option=com_newsfeeds">Feeds</a></li>
<li><a class="menu-newsfeeds-cat" href="index.php?option=com_categories&amp;extension=com_newsfeeds">Categories</a></li>
</ul>
</li>
<li><a class="menu-postinstall" href="index.php?option=com_postinstall">Post-installation Messages</a></li>
<li><a class="menu-redirect" href="index.php?option=com_redirect">Redirect</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-rsticketspro" data-bs-toggle="dropdown" href="index.php?option=com_rsticketspro">RSTickets! Pro</a><ul id="menu-com-rsticketspro" class="dropdown-menu menu-component">
<li><a class="menu-component" href="index.php?option=com_rsticketspro">Overview</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=tickets">Manage Tickets</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=departments">Departments</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=groups">Staff Groups</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=staffs">Staff Members</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=priorities">Priorities</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=statuses">Statuses</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=knowledgebase">Knowledgebase</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=emails">Email Messages</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=configuration">Configuration</a></li>
<li><a class="menu-component" href="index.php?option=com_rsticketspro&amp;view=updates">Updates</a></li>
</ul>
</li>
<li><a class="menu-search" href="index.php?option=com_search">Search</a></li>
<li><a class="menu-finder" href="index.php?option=com_finder">Smart Search</a></li>
<li><a class="menu-component" href="index.php?option=com_splms">SP LMS</a></li>
<li><a class="menu-tags" href="index.php?option=com_tags">Tags</a></li>
<li><a class="menu-icon-16-revolution" href="index.php?option=com_uniterevolution">Unite Revolution Slider</a></li>
<li><a class="menu-icon-16-revolution" href="index.php?option=com_uniterevolution2">Unite Revolution Slider 2</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-component" data-bs-toggle="dropdown" href="index.php?option=com_uniteshowbiz">Unite Showbiz</a><ul id="menu-com-uniteshowbiz" class="dropdown-menu menu-component">
<li><a class="menu-component" href="index.php?option=com_uniteshowbiz&amp;view=sliders">Sliders</a></li>
<li><a class="menu-component" href="index.php?option=com_uniteshowbiz&amp;view=templates">Skin Editor</a></li>
<li><a class="menu-component" href="index.php?option=com_uniteshowbiz&amp;view=templates&amp;navigation=1">Navigation Skin Editor </a></li>
</ul>
</li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-weblinks" data-bs-toggle="dropdown" href="index.php?option=com_weblinks">Weblinks</a><ul id="menu-com-weblinks" class="dropdown-menu menu-component">
<li><a class="menu-weblinks" href="index.php?option=com_weblinks">Links</a></li>
<li><a class="menu-weblinks-cat" href="index.php?option=com_categories&amp;extension=com_weblinks">Categories</a></li>
</ul>
</li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Extensions <span class="caret"></span></a><ul class="dropdown-menu">
<li class="dropdown-submenu"><a class="dropdown-toggle menu-install" data-bs-toggle="dropdown" href="index.php?option=com_installer">Manage</a><ul id="menu-com-installer" class="dropdown-menu menu-component">
<li><a class="menu-install" href="index.php?option=com_installer">Install</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=update">Update</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=manage">Manage</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=discover">Discover</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=database">Database</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=warnings">Warnings</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=languages">Install Languages</a></li>
<li><a class="menu-install" href="index.php?option=com_installer&amp;view=updatesites">Update Sites</a></li>
</ul>
</li>
<li class="divider"><span></span></li>
<li><a class="menu-module" href="index.php?option=com_modules">Modules</a></li>
<li><a class="menu-plugin" href="index.php?option=com_plugins">Plugins</a></li>
<li><a class="menu-themes" href="index.php?option=com_templates">Templates</a></li>
<li class="dropdown-submenu"><a class="dropdown-toggle menu-language" data-bs-toggle="dropdown" href="index.php?option=com_languages">Language(s)</a><ul id="menu-com-languages" class="dropdown-menu menu-component">
<li><a class="menu-language" href="index.php?option=com_languages&amp;view=installed">Installed</a></li>
<li><a class="menu-language" href="index.php?option=com_languages&amp;view=languages">Content Languages</a></li>
<li><a class="menu-language" href="index.php?option=com_languages&amp;view=overrides">Overrides</a></li>
</ul>
</li>
</ul>
</li>
<li class="dropdown"><a class="dropdown-toggle" data-bs-toggle="dropdown" href="#">Help <span class="caret"></span></a><ul class="dropdown-menu">
<li><a class="menu-help" href="index.php?option=com_admin&amp;view=help">Joomla! Help</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-help-forum" href="http://forum.joomla.org" target="_blank">Official Support Forum</a></li>
<li><a class="menu-help-forum" href="http://forum.joomla.org/viewforum.php?f=511" target="_blank">Official Language Forums</a></li>
<li><a class="menu-help-docs" href="https://docs.joomla.org" target="_blank">Documentation Wiki</a></li>
<li class="divider"><span></span></li>
<li><a class="menu-help-jed" href="http://extensions.joomla.org" target="_blank">Joomla! Extensions</a></li>
<li><a class="menu-help-trans" href="http://community.joomla.org/translations.html" target="_blank">Joomla! Translations</a></li>
<li><a class="menu-help-jrd" href="http://resources.joomla.org" target="_blank">Joomla! Resources</a></li>
<li><a class="menu-help-community" href="http://community.joomla.org" target="_blank">Community Portal</a></li>
<li><a class="menu-help-security" href="https://developer.joomla.org/security-centre.html" target="_blank">Security Centre</a></li>
<li><a class="menu-help-dev" href="https://developer.joomla.org" target="_blank">Developer Resources</a></li>
<li><a class="menu-help-dev" href="https://joomla.stackexchange.com" target="_blank">Stack Exchange</a></li>
<li><a class="menu-help-shop" href="https://shop.joomla.org" target="_blank">Joomla! Shop</a></li>
</ul>
</li>
</ul>-->
<?php } ?>
