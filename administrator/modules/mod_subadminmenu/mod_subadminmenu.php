<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_subadminmenu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
/*

$lang    = JFactory::getLanguage();
$user    = JFactory::getUser();
$input   = JFactory::getApplication()->input;
$menu    = new JAdminCSSMenu;
$enabled = $input->getBool('hidemainmenu') ? false : true;
*/
// Render the module layout
require JModuleHelper::getLayoutPath('mod_subadminmenu', $params->get('layout', 'default'));
