<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  mod_multilangstatus
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include jQuery
JHtml::_('jquery.framework');

// Use javascript to remove the modal added below from the current div and add it to the end of html body tag.
JFactory::getDocument()->addScriptDeclaration("
	jQuery(document).ready(function($) {
		var multilangueModal = $('#multiLangModal').clone();
		$('#multiLangModal').remove();
		$('body').append(multilangueModal);
	});
");
?>

<div class="btn-group multilanguage">
	<a data-bs-toggle="modal"
		href="#multiLangModal"
		title="<?php echo JText::_('MOD_MULTILANGSTATUS'); ?>"
		role="button">
		<span class="icon-comment" aria-hidden="true"></span><?php echo JText::_('MOD_MULTILANGSTATUS'); ?>
	</a>
	<span class="btn-group separator"></span>
</div>

<?php echo JHtml::_(
	'bootstrap.renderModal',
	'multiLangModal',
	array(
		'title'       => JText::_('MOD_MULTILANGSTATUS'),
		'iframeUrl'   => JRoute::_('index.php?option=com_languages&view=multilangstatus&tmpl=component'),
		'size'        => 'lg',
		'footer'      => '<a class="btn" data-bs-dismiss="modal" type="button" aria-hidden="true">'
				. JText::_('JTOOLBAR_CLOSE') . '</a>',
	)
);
