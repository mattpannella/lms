<?php
/**
 * @version			$Id: pkg_jsappointments.script.php 179 2014-07-25 05:55:43Z sanawin $
 * @package		Joomla
 * @subpackage	JomSocial
 * @copyright		Marvelic Engine Co.,Ltd. All Rights Reserved.http://www.cmsplugin.com
 * @license			GNU/GPL v3 http://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Appoinment system for JomSocial : This project is a joomla component used with com_community(JomSocial) component.
 * - The system Can make an appointment within the people group.
 * - The system Can make personal appointments.
 * - The system Calendar Have Monthly View, Year View and Weekly View
 * - Add People to list of appointments none limited.
 * - The appointment can change the status to Accept, Not Accept and Not Sure
 * - Can specify the reason If change the status to Accept, Not Accept and Not Sure
 */
 
//No direct access
defined('_JEXEC') or die('Restricted access');

class pkg_jsappointmentsInstallerScript
{
	/**
     * Called before any type of action
     *
     * @param     string              $route      Which action is happening (install|uninstall|discover_install)
     * @param     jadapterinstance    $adapter    The object responsible for running this script
     *
     * @return    boolean                         True on success
     */
    public function preflight($route, JAdapterInstance $adapter)
    {
		// first check if PHP5 is running
		if (version_compare(PHP_VERSION, '5.0.0', '<')) {
			// we use the static loadLanguage() method in the JPlugin class - a little tricky
			jimport('joomla.plugin.plugin');
			JFactory::getLanguage()->load('com_community', JPATH_ADMINISTRATOR);

			echo '<div class="fc-error">';
			echo JText::_( 'Please upgrade your php version to be at lease 5.0.0.' ) . '<br />';
			echo '</div>';
			return false;
		}

		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');

		//check have plugin system jsappointments
		if(JFolder::exists(JPATH_SITE."/plugins/system/jsappointments"))
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->delete('#__extensions');
			$query->where('folder ='.$db->Quote('system'));
			$query->where('element ='.$db->Quote('jsappointments'));
			$query->where('type='.$db->Quote('plugin'));

			//Delete old folder
			JFolder::delete(JPATH_SITE."/plugins/system/jsappointments");
		}
		return true;
	}

	/**
     * Called after any type of action
     *
     * @param     string              $route      Which action is happening (install|uninstall|discover_install)
     * @param     jadapterinstance    $adapter    The object responsible for running this script
     *
     * @return    boolean                         True on success
     */
    public function postflight($route, JAdapterInstance $adapter)
    {
		 //clear a cache
        $cache =JFactory::getCache();
        $cache->clean( null, 'com_community' );

		$db = JFactory::getDbo();

		//set vars
		$extensions = array();
		$extensions[] = array(
									'type'=>'plugin',
									'name'=>'plg_community_appointments',
									'element'=>'appointments',
									'folder'=>'community',
									'status'=>false,
									'published'=>false
								);

		foreach($extensions as $key=>$extension)
		{	
			$query = $db->getQuery(true);
			$query = 'SELECT * ' .
						' FROM `#__extensions`' .
						' WHERE folder = '.$db->Quote($extension['folder']) .
						' AND element = '.$db->Quote($extension['element']).
						' AND `type` = '.$db->Quote($extension['type']);

			$db->setQuery($query);
			if($row = $db->loadObject())
			{
				$extensions[$key]['status']	=  true;
				
				//Update the plugin enabled
				$query = $db->getQuery(true);
				$query =' UPDATE `#__extensions` SET enabled = 1 '
							.' WHERE folder = '.$db->Quote($row->folder)
							.' AND element = '.$db->Quote($row->element)
							.' AND `type` = '.$db->Quote($row->type);

				$db->setQuery($query);
                if($db->query()) {
					$extensions[$key]['published'] =  true;
				}
			}
		}

		// enable module
		$params = new JRegistry;
		$params->set('moduleclass_sfx',' app-box');
		$params->set('default', '5');

		$module = 'mod_community_appointments';

		$query	= 'SELECT * FROM ' . $db->quoteName( '#__modules' ) . ' WHERE ';
		$query	.= $db->quoteName( 'module' ) . '=' . $db->Quote( $module );

		$db->setQuery( $query );
		$result = $db->loadObject();

		if(empty($result->position))
		{
			$query	= 'UPDATE '.$db->quoteName('#__modules').' SET '.$db->quoteName('published').' = '.$db->quote(1)
						.' , '.$db->quoteName('position').' = '. $db->quote('js_side_frontpage')
						.' , '.$db->quoteName('ordering'). ' = '.$db->quote(1)
						.' , '.$db->quoteName('access'). ' = '.$db->quote(2)
						.' , '.$db->quoteName('params'). ' = '.$db->quote($params->toString())
						.' WHERE '.$db->quoteName('module').' = '.$db->quote($module);

			$db->setQuery($query);

			if ( ! $db->query())
			{
				return $db->getErrorNum().':'.$db->getErrorMsg();
			}
		}

		$query = 'SELECT COUNT(*) FROM ' .$db->quoteName( '#__modules_menu' )
					.' WHERE ' .$db->quoteName( 'moduleid' ) .' = ' . $db->quote( $result->id );

		$db->setQuery($query);

		$count = $db->loadResult();

		if($count < 1)
		{

			$query	= 'INSERT INTO ' . $db->quoteName( '#__modules_menu' )
					. '(' . $db->quoteName( 'moduleid' ) . ', '. $db->quoteName( 'menuid' ). ')'
					. 'VALUES('. $db->quote( $result->id ) . ', '. $db->quote( '' ). ')';

			$db->setQuery( $query );
			$db->Query();
		}

		$extensions[] = array('type'=>'module', 'name'=>'mod_community_appointments', 'status'=>true, 'published'=>true);
		?>
		<h3><?php echo JText::_('Additional Extensions'); ?></h3>
		<table class="adminlist table table-striped">
			<thead>
				<tr>
					<th class="title"><?php echo JText::_('Extension'); ?></th>
					<th width="60%"><?php echo JText::_('Status'); ?></th>
					<th width="10%"><?php echo JText::_('Published'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach ($extensions as $i => $ext) : ?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="nowrap key"><?php echo JText::_($ext['name']); ?> (<?php echo JText::_($ext['type']); ?>)</td>
						<td>
							<?php $style = $ext['status'] ? 'font-weight: bold; color: green;' : 'font-weight: bold; color: red;'; ?>
							<span style="<?php echo $style; ?>"><?php echo $ext['status'] ? JText::_('Installed successfully') : JText::_('NOT Installed'); ?></span>
						</td>
						<td class="center"><?php echo $ext['published']?JText::_('Yes'):JText::_('No');?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php
		return true;
	}
}