<?php
	/* error_reporting(E_ALL);
    ini_set('display_errors', 1); */
	defined( '_JEXEC' ) or die( 'Restricted access' );

	$this->setGenerator(null);

	include_once "components/shared/controllers/common.php";
	$language = AxsLanguage::getCurrentLanguage()->get('tag');
	$_SESSION['current_language'] = $language;

	$userId = JFactory::getUser()->id;

	$siteLanguage = AxsExtra::getLanguage();

	$input = JFactory::getApplication()->input;

	$view = $input->get('view');

	$commit_number = shell_exec('cat /var/www/html/.git/ORIG_HEAD | cut -c1-6') ?? '--';

	// If we are logged in and on a landing page view that isn't the root landing page
	if ($userId != 0 && $view == 'landingpage' && $_SERVER['REQUEST_URI'] != '/' && !strpos($_SERVER['REQUEST_URI'],'landingpage-preview')) {
		// Redirect the user to / so the learner dashboard loads
		JFactory::getApplication()->redirect('/');
	}

	$brand = AxsBrands::getBrand();

	$clientId = AxsClients::getClientId();

	if ( ($userId != 0) && ($view != 'landingpage')) {
		include ('dashboard.php');
	} else {
		include ('home.php');
	}

?>
<script>
	$.ajax({
        type: 'POST',
        url: '/index.php?option=com_axs&task=update.session&format=raw'
    });
</script>
