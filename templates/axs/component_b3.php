<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$this->language  = $doc->language;
$this->direction = $doc->direction;
$brand = AxsBrands::getBrand();

?>
<meta charset="UTF-8">
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1">
    <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <jdoc:include type="head" />
    <?php
        $fonts = AxsFonts::getFonts($brand->id);
        foreach($fonts as $font) {
            if($font->link) {
                echo $font->link."\n";
            } else {
                echo "<link href='https://fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>"."\n";
            }

        }
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css?v=2" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/icons/style.css?v=38" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/styles.css?v=53" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/dashboard.css?v=5">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <?php
        $useRootStyles = false;
        if(!$brand->site_details->root_base_color) {
            $brand->site_details->root_base_color = "#fff";
        }
        if(
            $brand->site_details->tovuti_root_styles &&
            $brand->site_details->root_primary_color &&
            $brand->site_details->root_accent_color  &&
            $brand->site_details->root_base_color
        ) {
        $useRootStyles = true;
    ?>
    <style>
        :root {
            /* Tovuti Platform Default Theme */
            --primary-color: <?php echo $brand->site_details->root_primary_color; ?>; /* Tovuti Blue */
            --accent-color: <?php echo $brand->site_details->root_accent_color; ?>; /* Tovuti Orange */
            --base-color: <?php echo $brand->site_details->root_base_color; ?>; /* Background Base */

        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/axs/css/tov-style/tovuti-master-v1.css?v=<?php echo $randomVersion; ?>">
    <?php } ?>

    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/icons/style.css" type="text/css" />
</head>
<body class="contentpane">
	<jdoc:include type="component" />
</body>
</html>
