<?php

defined('_JEXEC') or die('Restricted access');

$this->setGenerator(null);
$page = urlencode($_SERVER['REQUEST_URI']);
$user = JFactory::getUser();
$userId = $user->id;
$usergroups = JAccess::getGroupsByUser($userId);
$accessLevels = JAccess::getAuthorisedViewLevels($userId);
$canReset = in_array(4500, $usergroups);
$app = JFactory::getApplication();
$randomVersion = rand(100, 100000);
$hasCommunityAccess = AxsDbAccess::checkAccess('community');
$isAdmin = FALSE;
if ((in_array(20, $usergroups)) || (in_array(8, $usergroups))) {
    $isAdmin = TRUE;
}

    $groups = '';
    $security = AxsSecurity::getSettings();
    $date = strtotime('now');
    $profile = ginProfile($userId);
    $myphoto = $profile->photo;
    $myalias = $profile->alias;
    $myfirstName = $profile->firstname;
    $mylastName = $profile->lastname;

if ($myphoto == "") {
    $myphoto = "/images/user.png";
}

$settings = json_decode(AxsLMS::getSettings()->params);
$userPoints = AxsLearnerDashboard::getLeaderBoardPointsForUser($userId);

AxsExtra::removeMessage($app, 'Login');
AxsExtra::removeMessage($app, 'login');
AxsExtra::removeMessage($app, 'Login successful');
AxsExtra::removeMessage($app, 'You have logged out.');
AxsExtra::removeMessage($app, 'The security token did not match. The request was aborted to prevent any security breach. Please try again.');
AxsExtra::removeMessage($app, 'El token de seguridad no coincidía. La solicitud se abortó para evitar cualquier violación de seguridad. Vuelve a intentarlo.');
AxsExtra::removeMessage($app, 'SMTP');


$menu = $app->getMenu();
$activeMenuItem = $menu->getActive();
$pageclass = '';

if (is_object($activeMenuItem)) {
    $pageclass = $activeMenuItem->params->get('pageclass_sfx');
}

$dashboard = $brand->dashboard;

$nav_bar = $dashboard->navigation_bar;
$user_bar = $dashboard->user_bar;
$logo_area = $dashboard->logo;
$user_menu = $dashboard->menu;
$modules = $dashboard->modules;

$input    = $app->input;
$option   = $input->get('option', '', 'STRING');
$view     = $input->get('view', '', 'STRING');
$item_id  = $input->get('id', 0, 'INT');
$duration_object = new stdClass();
$duration_object->option  = $option;
$duration_object->view    = $view;
$duration_object->item_id = $item_id;
if ($view == "lesson") {
    $lesson = AxsLMS::getLessonById($item_id);
    $duration_object->course_id = $lesson->splms_course_id;
}
$duration_data = base64_encode(json_encode($duration_object));
jimport('joomla.application.module.helper');
?>

<!DOCTYPE html>
<html xmlns:jdoc="http://www.w3.org/2001/XInclude" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">

<head>
    <meta charset="UTF-8">
    <!-- REMOVE forces inaccessibility - should never have been here -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <meta name="apple-mobile-web-app-title" content="AXS">
    <link rel="apple-touch-icon" href="/graphics/icon.png" />
    <link href="/graphics/icon.png" media="(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
    <link href="/graphics/icon.png" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)" rel="apple-touch-startup-image">
    <!-- REMOVE no need to load Open Sans as we host our own font shown by default and over-ride with customer font -->
    <!-- <link href='//fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' /> -->
    <?php
    $defaultFont = $brand->site_details->default_font;
    $fonts = AxsFonts::getFonts($brand->id);
    foreach ($fonts as $font) {
        if ($font->title == $defaultFont) {
            echo "<link href='https://fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>" . "\n";
        }
    }
    ?>

    <meta name="apple-mobile-web-app-title" content="<?php echo $brand->site_title; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="/<?php echo $brand->logos->favicon ?>">
    <link rel="icon" href="/<?php echo $brand->logos->favicon ?>" type="image/x-icon">

    <jdoc:include type="head" lang="en-GB" />

        <?php if (($option == 'com_splms') or ($option == 'com_axs')) { ?>
        <!-- Bootstrap Utilities -->
            <script src="<?php echo $this->baseurl; ?>/templates/axs/bootstrap-utilities/bootstrap-utilities.min.css" crossorigin="anonymous"></script>
        <!-- END - Bootstrap Utilities -->
        <?php } ?>

        <?php if (($option == 'com_splms') or ($option == 'com_axs')) { ?>
        <!-- Bootstrap Utilities -->
            <script src="<?php echo $this->baseurl; ?>/templates/axs/bootstrap-utilities/bootstrap-utilities.min.css" crossorigin="anonymous"></script>
        <!-- END - Bootstrap Utilities -->
        <?php } ?>

        <?php if($option == 'com_eshop') { ?>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
            <script type="text/javascript" src="/assets/scroll/jquery.nanoscroller.js"></script>
        <?php } ?>

    <?php if ($option != 'com_eshop') { ?>
        <?php
        JHtml::_('jquery.framework');
        JHtml::_('bootstrap.framework');
        ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/axs/css/bootstrap.css?v=7">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <?php if ($option != 'com_splms') { ?>
            <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <?php } ?>
        <script type="text/javascript" src="/assets/scroll/jquery.nanoscroller.js"></script>
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/eshop-patch.css">
    <?php } ?>
    <script type="text/javascript" src="/js/wcag-dropdown.js"></script>

    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css?v=2" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/icons/style.css?v=39" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/styles.css?v=72" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/dashboard.css?v=6">
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/axs/js/dashboard.js?v=11"></script>

    <style>
        :root {
            /* Tovuti Platform Default Theme */
            --primary-color: #1e2935;
            /* Tovuti Blue */

        }
    </style>
    <?php
    $useRootStyles = false;
    if (!$brand->site_details->root_base_color) {
        $brand->site_details->root_base_color = "#fff";
    }
    if (
        $brand->site_details->tovuti_root_styles &&
        $brand->site_details->root_primary_color &&
        $brand->site_details->root_accent_color  &&
        $brand->site_details->root_base_color
    ) {
        $useRootStyles = true;
    ?>
        <style>
            :root {
                /* Tovuti Platform Default Theme */
                --primary-color: <?php echo $brand->site_details->root_primary_color; ?>;
                /* Tovuti Blue */
                --accent-color: <?php echo $brand->site_details->root_accent_color; ?>;
                /* Tovuti Orange */
                --base-color: <?php echo $brand->site_details->root_base_color; ?>;
                /* Background Base */
            }

            #system-debug {
                margin-left: 200px !important;
            }
        </style>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl ?>/templates/axs/css/tov-style/tovuti-master-v1.css?v=<?php echo $randomVersion; ?>">
    <?php } ?>

    <?php
    $javaScript = $brand->dashboard->js;
    $css = $brand->dashboard->css;

    if ($javaScript->pre) {
        echo "<script>";
        echo $javaScript->pre;
        echo "</script>";
    }

    if ($css->pre) {
        echo "<style>";
        echo $css->pre;
        echo "</style>";
    }
    ?>




    <?php
    switch ($nav_bar->width_type) {
        case "pixel":
            $sidebarWidth = $nav_bar->width_pixels . "px";
            break;
        case "percent":
            $sidebarWidth = $nav_bar->width_percentage . "vw";
            break;
    }
    ?>

    <script>
        var sidebarWidth = "<?php echo $sidebarWidth; ?>";
        var sidebarStatus;
        var mobile;

        if ($(window).width() > 768) {
            mobile = false;
            sidebarStatus = "open";
        } else {
            mobile = true;
            sidebarStatus = "closed";
        }
    </script>

    <?php


    switch ($logo_area->user_bar_width_type) {
        case "pixel":
            $top_padding = "padding-top: " . $logo_area->user_bar_width_pixels . "px;";
            break;
        case "percent":
            $top_padding = "padding-top: " . $logo_area->user_bar_width_percentage . "vh;";
        case "auto":
            switch ($user_bar->width_type) {
                case "pixel":
                    $top_padding = "padding-top: " . $user_bar->width_pixels . "px;";
                    break;
                case "percent":
                    $top_padding = "padding-top: " . $user_bar->width_percentage . "vh;";
                    break;
            }
    }

    $top_padding = "padding-top: " . $user_bar->width_pixels . "px;";

    echo "<style>";
    echo "body {";
    if ($dashboard->default_font != "") {
        echo    "font-family: '" . $dashboard->default_font . "';";
    } else if ($brand->default_font != "") {
        echo    "font-family: '" . $brand->site_details->default_font . "';";
    }
    echo "}";
    echo "#header-wrap {";

    switch ($user_bar->width_type) {
        case "pixel":
            echo "height: " . $user_bar->width_pixels . "px;";
            break;
        case "percent":
            echo "height: " . $user_bar->width_percentage . "vh;";
            break;
    }

    if ($user_bar->color != "") {
        echo "background-color: " . $user_bar->color . ";";
    }
    echo "}";

    echo "#card_info {";
    if ($user_bar->text_size != "") {
        echo "font-size: " . $user_bar->text_size . "px;";
    }
    if ($user_bar->text_color != "") {
        echo "color: " . $user_bar->text_color . ";";
    }
    if ($user_bar->text_font != "") {
        echo "font-family: " . $user_bar->text_font . ";";
    }
    echo "}";

    echo "#header-wrap a {";

    if ($user_bar->text_color != "") {
        echo "color: " . $user_bar->text_color . ";";
    }

    echo "}";

    echo "#card_info_sub {";
    if ($user_bar->sub_text_size != "") {
        echo "font-size: " . $user_bar->sub_text_size . "px;";
    }
    if ($user_bar->sub_text_font != "") {
        echo "font-family: " . $user_bar->sub_text_font . ";";
    }
    echo "}";

    echo "#user_language_selector {";
    if ($user_bar->text_size != "") {
        echo "font-size: " . $user_bar->text_size . "px;";
    }
    if ($user_bar->text_color != "") {
        echo "color: " . $user_bar->text_color . ";";
    }
    if ($useRootStyles) {
        //echo "margin-top: -35px !important;";
    }
    echo "}";

    echo "#menu-toggle {";
    if ($logo_area->collapse_color != "") {
        echo "color: " . $logo_area->collapse_color . ";";
    }
    echo "}";

    echo "#page-content-wrapper {";
    switch ($nav_bar->width_type) {
        case "pixel":
            $sidebarWidth = $nav_bar->width_pixels . "px";
            echo "margin-left: " . $sidebarWidth . ";";
            break;
        case "percent":
            $sidebarWidth = $nav_bar->width_percentage . "vw";
            echo "margin-left: " . $sidebarWidth . ";";
            break;
    }
    echo $top_padding;
    echo "}";

    echo "#sidebar-wrapper {";
    echo "position: fixed;";
    switch ($nav_bar->width_type) {
        case "pixel":
            $sidebarWidth = $nav_bar->width_pixels . "px";
            echo "width: " . $sidebarWidth . ";";
            break;
        case "percent":
            $sidebarWidth = $nav_bar->width_percentage . "vw";
            echo "width: " . $sidebarWidth . ";";
            break;
    }

    if ($nav_bar->color != "") {
        echo "background-color: " . $nav_bar->color . ";";
    }

    if ($nav_bar->text_font != "") {
        echo "font-family: " . $nav_bar->text_font . ";";
    }

    echo "margin-top: 0px;";
    //Check if 3 color System is on
    if (!$useRootStyles) {
        echo $top_padding;
    }
    echo "}";

    echo ".navLabel {";
    if ($nav_bar->text_color != "") {
        echo "color: " . $nav_bar->text_color . ";";
    }
    if ($nav_bar->text_size != "") {
        echo "font-size: " . $nav_bar->text_size . "px;";
    }
    echo "}";

    echo ".submenu {";
    if ($nav_bar->sub_text_font != "") {
        echo "font-family: " . $nav_bar->sub_text_font . ";";
    }
    echo "}";

    echo ".submenu li a span, .submenu li a {";
    if ($nav_bar->sub_text_color != "") {
        echo "color: " . $nav_bar->sub_text_color . ";";
    }
    echo "}";

    echo ".axsNav > a:hover {";
    if ($nav_bar->toplevel_hover_color != "") {
        echo "background-color: " . $nav_bar->toplevel_hover_color . ";";
    }
    echo "}";

    echo ".submenu li a:hover {";
    if ($nav_bar->hover_color != "") {
        echo "background-color: " . $nav_bar->hover_color . ";";
    }
    if ($nav_bar->sub_text_color != "") {
        echo "color: " . $nav_bar->sub_text_color . ";";
    }
    echo "}";

    echo ".iconStyle {";
    if ($nav_bar->icon_color != "") {
        echo "color: " . $nav_bar->icon_color . ";";
    }
    if ($nav_bar->icon_size != "") {
        echo "font-size: " . $nav_bar->icon_size . "px !important;";
    }
    echo "}";


    echo ".dropdown-menu {";
    if ($user_menu->font != "") {
        echo "font-family: " . $user_menu->font . ";";
    }
    echo "}";

    echo ".dashboard_module {";
    if ($modules->font != "") {
        echo "font-family: " . $modules->font . ";";
    }
    echo "}";
    echo "</style>";

    ?>

    <style>
        @media(max-width:768px) {
            /* #sidebar-wrapper {
                width: 0px;
            } */
        }

        @media(max-width:500px) {
            .joms-notifications {
                display: none;
            }
        }

        @media(max-width:768px) {
            /* #page-content-wrapper {
                margin-left: 0px;
            } */
        }


        #menu-toggle {
            top: 12px;
            left: 5px;
            margin-right: 15px;
        }

        #my-account-menu,
        #my-account-menu a {
            color: #555;

        }

        #my-account-menu a:hover {
            color: #0980f9;

        }


        <?php if ($nav_bar->style == 'icons_only') { ?>.axsNav .navLabel,
        .axsNav .submenu {
            display: none;
        }

        <?php } ?>
    </style>

    <link rel="stylesheet" href="/media/player/plyr.css" />

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=es6,Array.prototype.includes,CustomEvent,Object.entries,Object.values,URL,Math.trunc" crossorigin="anonymous"></script>
    <script src="/media/player/plyr.js?v=2"></script>
    <script src="/media/player/tracking.js?v=3"></script>

    <?php if ($userId) { ?>
        <script>
            const duration_data = "<?php echo $duration_data; ?>";
        </script>
        <script src="/media/tovuti/js/duration.js?v=8"></script>
    <?php } ?>

    <script src="https://js.stripe.com/v3/"></script>
    <?php if (!$_COOKIE['timezone']) { ?>
        <script>
            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            document.cookie = "timezone = " + timezone;
        </script>
    <?php } ?>


    <script src="https://kit.fontawesome.com/8c1112f2d0.js" crossorigin="anonymous"></script>

</head>

<body id="top" class="<?php echo $pageclass; ?>">
    <header id="header-wrap">
        <div class="logo_box">
            <a href="#main-content" class="sr-only" tabindex="-1"><?php echo AxsLanguage::text("AXS_SKIP_TO_MAIN_CONTENT", "Skip to main content") ?></a>
            <button type="button" class="sideToggle" id="menu-toggle" aria-label="Menu" aria-haspopup="true" aria-controls="primary-menu">
                <i class="fa-solid fa-bars"></i>
            </button>
            <span>
                <img id="dash_logo" src="<?php echo $logo_area->main ?>" <?php echo AxsAccessibility::image($logo_area->main); ?>>
            </span>
        </div>
        <?php if ($this->countModules('notifications') && $hasCommunityAccess) { ?>
            <?php
            if (!$dashboard->user_bar->show_notification_icons) {
                echo "<style>.notifications .joms-notifications {display:none!important;}</style>";
            }
            ?>
            <jdoc:include type="modules" name="notifications" style="xhtml" />
        <?php } ?>

        <?php
        if (isset($brand->social)) {
            $social_settings = $brand->social;
        } else {
            $social_settings = null;
        }
        ?>

        <?php if ($social_settings && $dashboard->user_bar->show_socialmedia) { ?>

            <style>
                #social_links {
                    position: absolute;
                    top: 15px;
                    left: 33px;
                    background-color: white;
                    border: 0px solid #777;
                    border-radius: 4px;
                    color: #555;
                    min-width: 200px;
                    display: inline-block;
                    clear: both;
                    font-size: 14px;
                    padding: 4px 0px;
                    box-shadow: 0px 6px 12px rgba(0, 0, 0, 1.175);
                }

                #social_links a {
                    color: #555;
                    cursor: pointer;
                }

                .social_link_icon {
                    padding-right: 6px;
                    justify-content: center;
                    flex-direction: column;
                }

                .social_link_item {
                    width: auto;
                    padding: 3px 15px;
                    display: flex;
                    align-items: center;
                }

                .social_link_item:hover {
                    color: #0099ff;
                    background-color: #eee;
                    cursor: pointer;
                    transition: all 0.4s ease;
                }

                #social_links_menu {
                    margin-top: 18px;
                    float: left;
                    position: relative;
                }

                #share_social_links:hover {
                    color: red;
                    transition: all 0.4s ease;
                    cursor: pointer;
                }
            </style>


            <div id="social_links_menu">
                <i id="share_social_links" class="fa-solid fa-share-nodes" aria-label="Social Media" role="button" tabindex="0"></i>
                <!-- REMOVE lizicon -->
                <!-- <span id="share_social_links" title="Share" class="lizicon-share2" style="margin-left: 10px;"></span> -->
                <div class='clearfix'></div>
                <div id="social_links" style="display:none;">
                    <?php if ($social_settings->facebook_link) { ?>
                        <a href="<?php echo $social_settings->facebook_link; ?>" target="_blank" rel="noopener" aria-label="Follow us on Facebook">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-facebook-square"></div>
                                <div class="social_link_name">Facebook</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($social_settings->twitter_link) { ?>
                        <a href="<?php echo $social_settings->twitter_link; ?>" target="_blank" rel="noopener" aria-label="Follow us on Twitter">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-twitter-square"></div>
                                <div class="social_link_name">Twitter</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($social_settings->google_plus_link) { ?>
                        <a href="<?php echo $social_settings->google_plus_link; ?>" target="_blank" rel="noopener" aria-label="Follow us on Google">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-google-plus-square"></div>
                                <div class="social_link_name">Google Plus</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($social_settings->youtube_link) { ?>
                        <a href="<?php echo $social_settings->youtube_link; ?>" target="_blank" rel="noopener" aria-label="Follow us on Youtube">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-youtube-square"></div>
                                <div class="social_link_name">YouTube</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($social_settings->linkedin_link) { ?>
                        <a href="<?php echo $social_settings->linkedin_link; ?>" target="_blank" rel="noopener" aria-label="Follow us on Linkedin">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-linkedin-square"></div>
                                <div class="social_link_name">LinkedIn</div>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if ($social_settings->instagram_link) : ?>
                        <a href="<?php echo $social_settings->instagram_link; ?>" target="_blank" rel="noopener">
                            <div class="social_link_item">
                                <div class="social_link_icon fa fa-instagram"></div>
                                <div class="social_link_name">Instagram</div>
                            </div>
                        </a>
                    <?php endif; ?>
                </div>
            </div>


            <script>
                $("#social_links_menu").click(
                    function() {
                        var menu = document.getElementById("social_links");
                        $(menu).slideToggle();
                    }
                );
            </script>
        <?php } ?>

        <?php
            if(
                $brand->site_details->enable_sso_deeplinks_button &&
                $brand->site_details->sso_app &&
                ($brand->site_details->sso_deeplinks_button_permissions == 'admins' && $isAdmin || $brand->site_details->sso_deeplinks_button_permissions == 'users')
            ) {

        ?>

            <?php if($useRootStyles){
//<span class="my-account-icon fa-solid fa-envelope" title="<?php echo AxsLanguage::text("AXS_MAIL", "Mail")

            ?>
            <a class="linkButton" style="margin-top: 0px; margin-left: 16px; cursor: pointer;"><span class="my-account-icon fa-solid fa fa-link"></span><div class="copied"><?php echo AxsLanguage::text("AXS_URL_COPIED", "URL Copied"); ?></div></a>

            <?php }else if(!$useRootStyles){

            ?>
            <a class="linkButton" style="margin-top: 14px; margin-left: 16px; cursor: pointer; background:black; "><span class="fa fa-link"></span><div class="copied"><?php echo AxsLanguage::text("AXS_URL_COPIED", "URL Copied"); ?></div></a>
            <?php } ?>

            <script>
                const baseUrl = window.location.origin+"/component/axs/?view=sso_config&id=<?php echo $brand->site_details->sso_app; ?>&forward=";
                const ssoDeepLinkUrl = baseUrl+encodeURIComponent(window.location.href);
            </script>
            <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/axs/js/sso-link.js"></script>
        <?php } ?>

        <div class="set-right right-profile">
            <div id="card_info" class="hidden-xs" style="margin-right:10px;float:left;text-align:right;margin-top:18px">
                <div style="">
                    <?php
                    if ($user_bar->user_name) {
                        echo $myfirstName . " " . $mylastName;
                    }

                    if ($user_bar->user_number) {
                    ?>
                        <div id="card_info_sub"><?php echo $userId; ?></div>
                    <?php
                    }
                    ?>
                </div>
                <?php if ($settings->show_user_points) { ?>
                    <div>
                        <span class="tovuti-user-points"><?php echo AxsLanguage::text("AXS_YOUR_POINTS", "Your Points") ?>: <?php echo $userPoints; ?></span>
                    </div>
                <?php } ?>
            </div>
                    <div id="my-account" class="dropdowns" style="float: left; margin-top: 5px;" data-step="14" data-intro="<?php echo AxsLanguage::text("AXS_THIS_IS_YOUR_ACCOUNT_MENU", "This is your Account Menu") ?>" data-position="left">
                        <a class="dropdown-toggles" <?php /*data-toggle="dropdown"*/?> aria-haspopup="true" aria-expanded="false" alt="<?php echo AxsLanguage::text("AXS_THIS_IS_YOUR_ACCOUNT_MENU", "This is your Account Menu") ?>" tabindex="0">
                        <?php
                            if ($user_bar->user_image) {
                        ?>
                                <div class="comment-img" tabindex="-1">
                                    <img
                                        src="<?php echo $myphoto; ?>"
                                        class="bw"
                                        alt="profile picture"
                                    />
                                </div>
                        <?php
                            }
                        ?>
                        <span class="top-dropdown lizicon-1-TEP-Connect-Icon-dropdown"></span>
                    </a>
                    <ul id="my-account-menu" class="dropdown-menu dropdown-menu-right">
                        <?php if($isAdmin) { ?>
                            <li>
                                <a href="<?php echo JRoute::_('administrator'); ?>" target="_blank">
                                    <span class="my-account-icon lizicon-1-TEP-Connect-Icon-administrator"></span>
                                    <span class="my-account-label"><?php echo AxsLanguage::text("TPL_MWM_GIN_ADMIN", "Administrator Options"); ?></span>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if($security->twofactor_enabled) {?>
                            <li data-step="15" data-intro="<?php echo AxsLanguage::text("AXS_TWOFACTOR", "Manage your Two Factor Authentication.") ?>" data-position="left">
                                <a href="<?php echo JRoute::_('index.php?option=com_axs&view=2fa&id=1') ?>">
                                    <span class="my-account-icon lizicon-security" title="<?php echo AxsLanguage::text("AXS_2FA", "Two-Factor Authentication") ?>"></span>
                                    <span class="my-account-label"><?php echo AxsLanguage::text("AXS_2FA", "Two-Factor Authentication") ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    <?php if ($user_menu->view_profile) { ?>
                        <li data-step="15" data-intro="<?php echo AxsLanguage::text("AXS_GO_TO_YOUR_PERSONAL_PROFILE", "Go to your personal profile.") ?>" data-position="left" role="none">
                            <a href="<?php echo JRoute::_('index.php?option=com_community&view=profile') ?>" role="menuitem">
                                <span class="my-account-icon fa-solid fa-address-card" title="<?php echo AxsLanguage::text("AXS_PROFILE", "Profile") ?>"></span>
                                <span class="my-account-label"><?php
                                                                echo $user_menu->view_profile_label != "" ? $user_menu->view_profile_label : AxsLanguage::text("TPL_MWM_GIN_PROFILE", "View Profile");
                                                                ?></span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if($security->twofactor_enabled) {?>
                            <li data-step="15" data-intro="<?php echo AxsLanguage::text("AXS_SECURITY_MANAGE", "Manage your security settings") ?>" data-position="left">
                                <a href="<?php echo JRoute::_('index.php?option=com_axs&view=2fa&id=1') ?>">
                                    <span class="my-account-icon fa-solid fa-shield-check" title="<?php echo AxsLanguage::text("AXS_SECURITY", "Security") ?>"></span>
                                    <span class="my-account-label"><?php echo AxsLanguage::text("AXS_SECURITY", "Security") ?></span>
                                </a>
                            </li>
                    <?php } ?>
                    <?php if ($user_menu->edit_profile) { ?>
                        <li data-step="16" data-intro="<?php echo AxsLanguage::text("AXS_EDIT_YOUR_PERSONAL_PROFILE", "Edit your personal profile.") ?>" data-position="left" role="none">
                            <a href="<?php echo JRoute::_('index.php?option=com_community&view=profile&task=edit') ?>" role="menuitem">
                                <span class="my-account-icon fa-solid fa-address-card" title="<?php echo AxsLanguage::text("AXS_PROFILE", "Profile") ?>"></span>
                                <span class="my-account-label"><?php
                                                                echo $user_menu->edit_profile_label != "" ? $user_menu->edit_profile_label : AxsLanguage::text("TPL_MWM_GIN_EDIT_PROFILE", "Edit Profile");
                                                                ?></span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($user_menu->my_transcript) { ?>
                        <li data-step="16" data-intro="<?php echo AxsLanguage::text("AXS_VIEW_YOUR_TRANSCRIPT", "View your transcript") ?>" data-position="left" role="none">
                            <a href="<?php echo JRoute::_('index.php?option=com_splms&view=transcripts'); ?>" role="menuitem">
                                <span class="my-account-icon fa-solid fa-scroll" title="<?php echo AxsLanguage::text("AXS_TRANSCRIPT", "Transcript") ?>"></span>
                                <span class="my-account-label"><?php
                                                                echo $user_menu->my_transcript_label != "" ? $user_menu->my_transcript_label : AxsLanguage::text('AXS_MY_TRANSCRIPT', 'My Transcript');
                                                                ?></span>
                            </a>
                        </li>
                    <?php } ?>

                    <?php if ($user_menu->billing_manager) { ?>
                        <li data-step="18" role="none" data-intro="<?php echo AxsLanguage::text("AXS_DASHBOARD_MANAGE_CREDIT_CARDS_HERE", "Here you can Manage your credit cards and view charges as well as use your Rewards Points!") ?>" data-position="left">
                            <a href="<?php echo JRoute::_('index.php?Itemid=' . $menu->getItems('route', 'billing', true)->id); ?>" role="menuitem">
                                <span class="my-account-icon fa-solid fa-credit-card" title="<?php echo AxsLanguage::text("AXS_BILLING", "Billing") ?>"></span>
                                <span class="my-account-label"><?php echo AxsLanguage::text("TPL_MWM_GIN_BILLING", "Billing Manager"); ?></span>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($user_menu->my_inbox && $hasCommunityAccess) { ?>
                        <li data-step="19" data-intro="<?php echo AxsLanguage::text("AXS_CHECK_YOUR_INBOX", "Check your inbox for new messages.") ?>" data-position="left">
                            <a href="<?php echo JRoute::_('index.php?option=com_community&view=inbox'); ?>" role="menuitem">
                               <span class="my-account-icon fa-solid fa-envelope" title="<?php echo AxsLanguage::text("AXS_MAIL", "Mail") ?>"></span>
                                <span class="my-account-label"><?php
                                                                echo $user_menu->my_inbox_label != "" ? $user_menu->my_inbox_label : AxsLanguage::text("TPL_MWM_GIN_MAIL", "My Inbox");
                                                                ?> </span>
                            </a>
                        </li>
                    <?php } ?>

                    <li data-step="23" data-intro="<?php echo AxsLanguage::text("AXS_CLICK_HERE_TO_LOGOUT", "Click here to logout.") ?>" data-position="left" role="none">
                        <a href="<?php echo JRoute::_('index.php?option=com_user&task=logout'); ?>" title="<?php echo AxsLanguage::text("AXS_LOG_OUT", "Log Out") ?>" role="menuitem">
                            <span class="my-account-icon fa-solid fa-right-from-bracket"></span>
                            <span class="my-account-label"><?php echo AxsLanguage::text("TPL_MWM_GIN_LOGOUT", "Logout"); ?></span>
                        </a>
                    </li>

                    <?php
                        $filename = "/var/www/html/version.txt";
                        $handle = fopen($filename, "r");
                        $version_number = fread($handle, filesize($filename));
                        fclose($handle);
                    ?>
                    <li class="commit-version">
                        Version <?php echo $version_number; ?>
                    </li>
                </ul>
            </button>
        </div>

        <div class="set-left right-language" style="padding:15px;">
            <?php if ($this->countModules('language') && $brand->site_details->language_selector) { ?>
                <jdoc:include type="modules" name="language" style="xhtml" />
            <?php } ?>
        </div>

    </header>

    <div id="wrapper">
        <div class="set-right">
            <div class="set-left" style="padding:15px; float: left; margin-right: 20px;">
                <?php if ($this->countModules('language') && $brand->site_details->language_selector) { ?>
                    <jdoc:include type="modules" name="language" style="xhtml" />
                <?php } ?>
            </div>
        </div>
        <div id="sidebar-wrapper">
            <div class="nano">
                <div class="nano-content">

                    <?php if ($logo_area->sub != "") { ?>
                        <?php
                        $sub_height = getimagesize($logo_area->sub)[1];
                        echo "<style>.sideIcons{margin-top:" . $sub_height . "px;}</style>";


                        ?>
                        <div class="logo4" style="margin-top:-10px; clear:both; margin-bottom:10px;">
                            <img src="<?php echo $logo_area->sub; ?>" <?php echo AxsAccessibility::image($logo_area->sub); ?>>
                        </div>
                    <?php } ?>

                    <nav id="primary-menu" class="sideIcons" <?php if (!empty($brand->logos->dashboard_sub) && $brand->logos->dashboard_sub != "") {
                                                                    echo 'style="padding-top:15px;"';
                                                                } ?>>
                        <?php
                        if ($this->countModules('sidemenu')) {
                        ?>
                            <jdoc:include type="modules" name="sidemenu" style="xhtml" />
                        <?php } ?>

                        <div id="clr" style="height:150px;"></div>
                    </nav>
                </div>
            </div>
        </div>

        <div id="page-content-wrapper">
            <?php


            if ($activeMenuItem) {
                $fullWidth = $activeMenuItem->params->get('full_width');
                $header_background = $activeMenuItem->params->get('header_background');
                $header_background_text = $activeMenuItem->params->get('header_background_text');
                $header_background_text_size = $activeMenuItem->params->get('header_background_text_size');
                $header_background_text_font = $activeMenuItem->params->get('header_background_text_font');
            }

            if ($modules->header_background && $activeMenuItem == $menu->getDefault()) {
                $header_background = $modules->header_background;
                if ($modules->header_background_text) {
                    $header_background_text = $modules->header_background_text;
                }
                if ($modules->header_background_text_size) {
                    $header_background_text_size = $modules->header_background_text_size;
                }
                if ($modules->header_background_text_font) {
                    $header_background_text_font = $modules->header_background_text_font;
                }
            }

            if ($header_background_text_size) {
                $titleSize = (int)$header_background_text_size;
            } else {
                $titleSize = 80;
            }

            if ($header_background_text_font) {
                $titleFont = ' font-family: ' . $header_background_text_font . '; ';
            } else {
                $titleFont = '';
            }

            if ($fullWidth) {
                $fluid = 'fullWidth';
            } else {
                $fluid = 'container';
            }
            ?>

            <?php

            ?>

            <?php if ($header_background) {
                if (($activeMenuItem == $menu->getDefault() && $dashboard->main_view_layout != 'learner_dashboard') || ($activeMenuItem != $menu->getDefault())) { ?>
                    <div class="header-fullwidth" style="
                                    background-image: url(/<?php echo $header_background; ?>);
                                    background-size:cover;
                                    background-position:center;
                                    height: 300px;
                                    position: relative;">
                        <div class="header-fullwidth-overlay" style="background: rgba(0,0,0,.6); width:100%; height:300px; position:relative;">
                            <?php if ($header_background_text) { ?>
                                <div class="slide-overlay">
                                    <h2 style="color:#ffffff;font-size:<?php echo $titleSize; ?>px;text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.8);<?php echo $titleFont; ?>">
                                        <?php echo $header_background_text; ?>
                                    </h2>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
            <?php
                }
            }
            ?>
            <main id="main-content" class="container-fluid">

            <?php
             $lang = $_REQUEST['language'];
             if ($lang != "en-GB") {
                ?>
                    <!-- style="right:103px;" -->
                    <style>
                    .joms-icon.joms-icon--add {
                        right: 103px;
                    }
                    </style>
                <?php
             }
            ?>

                <?php if ($this->countModules('dash_message')) { ?>
                    <jdoc:include type="modules" name="dash_message" style="xhtml" />
                <?php } ?>

                <?php
                if ($activeMenuItem == $menu->getDefault()) {
                    if ($dashboard->main_view_layout == 'learner_dashboard') {
                        $learnerDashboard_id = $dashboard->learner_dashboard_id;
                        include_once "components/com_axs/views/learner_dashboard/tmpl/default.php";
                    } else {

                        echo "<style>";
                        echo ".dashboard_title_bar {";
                        if ($modules->title_bar_padding_type == "pixel") {
                            echo "padding: " . $modules->title_bar_padding_pixel . "px;";
                        } else {
                            echo "padding: " . $modules->title_bar_padding_percent . "%;";
                        }

                        echo "font-size: " . $modules->title_bar_font_size . "px;";
                        echo "color: " . $modules->title_bar_text_color . ";";
                        echo "background-color: " . $modules->title_bar_color . ";";


                        echo "}";
                        echo ".dashboard_module {";
                        if ($modules->margin_type == "pixel") {
                            echo "margin: " . $modules->margin_pixel . "px;";
                        } else {
                            echo "margin: " . $modules->margin_percent . "%;";
                        }

                        if ($modules->padding_type == "pixel") {
                            echo "padding: " . $modules->padding_pixel . "px;";
                        } else {
                            echo "padding: " . $modules->padding_percent . "%;";
                        }
                        echo "}";
                        echo "</style>";

                        for ($i = 0; $i < count($modules->list->active); $i++) {

                            if ($modules->list->active[$i] && $modules->list->id[$i] != 'spacer') {

                                $db = JFactory::getDBO();
                                $query = "SELECT * FROM joom_modules WHERE id = " .  $modules->list->id[$i] . " LIMIT 1";

                                $db->setQuery($query);
                                $result = $db->loadObject();
                                if (!in_array((int)$result->access, $accessLevels)) {
                                    continue;
                                }
                                /* Publish up is the time that a module should be published at or after, and
                                         * publish down is the time that a module should be unpublished.
                                         */
                                $timePublish = DateTime::createFromFormat('Y-m-d H:i:s', $result->publish_up);
                                $timeUnpublish = DateTime::createFromFormat('Y-m-d H:i:s', $result->publish_down);

                                /// Get the current time in the UTC timezone
                                $utcNow = new DateTime('now', new DateTimeZone('UTC'));

                                /* Timestamps are funny in that they are created by the base date in a DateTime object,
                                         * not the timezone-adjusted date one would expect
                                         * by creating a DateTime object with a specified DateTimeZone.
                                         */
                                $currentTime = strtotime($utcNow->format('Y-m-d H:i:s'));

                                $publishTimestamp = $timePublish->getTimestamp();
                                $unpublishTimestamp = $timeUnpublish->getTimestamp();

                                $published = true;

                                // We need to show the module if it's published
                                if ($result->published == 1) {

                                    /* If there are time limits imposed on the publishing of the module
                                             * then make sure that the current time falls within the specified interval.
                                             */
                                    if ($publishTimestamp > 0 || $unpublishTimestamp > 0) {

                                        if ($publishTimestamp > 0) {
                                            $published = $published && ($currentTime >= $publishTimestamp);
                                        }

                                        if ($unpublishTimestamp > 0) {
                                            $published = $published && ($currentTime < $unpublishTimestamp);
                                        }
                                    } else {

                                        // No publish or unpublish times are set, so it's published by default.
                                        $published = true;
                                    }
                                } else {

                                    $published = false;
                                }

                                if ($result && $published) {

                                    $title = $result->title;
                                    $which = $result->module;

                                    $module = JModuleHelper::getModule($which, $title);

                                    $attribs['style'] = 'xhtml';

                                    $style = "";

                                    if ($result->module == "mod_eventviewer" || $result->module == "mod_easyblogshowcase") {
                                        $module_config->border = false;
                                    }

                                    if ($result->module == "mod_eventviewer") {
                                        $module_config->title_bar = false;
                                    }

                                    if ($modules->list->height[$i] > 0 || $modules->list->height_parent[$i]) {
                                        $style .= "overflow: hidden;";
                                    }

                                    if ($modules->list->border[$i] == true) {
                                        $style .= "box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.28);";
                                    } else {
                                        $style .= "box-shadow: none !important;";
                                    }



                                    if ($modules->list->height_parent[$i]) {
                                        $style .= "display: none;";
                                    }

                                    if ($modules->list->font[$i] != "") {
                                        $style .= "font-family: " . $modules->list->font[$i] . ";";
                                    }

                                    if ($modules->list != "") {
                                        $style = "style='" . $style . "'";
                                    }

                                    if ($modules->list->height[$i] == 0) {
                                        $height = "auto";
                                    } else {
                                        $height = $modules->list->height[$i] . "px";
                                        $modHeight = ($modules->list->height[$i] - 50) . "px";
                                    }


                                    echo "
                                            <style>
                                                @media(min-width:768px) {
                                                    #dash_module_" . $i . "{
                                                        height:" . $height  . ";
                                                    }
                                                    #dash_module_" . $i . " .zone_module_container {
                                                        height: " . $modHeight  . ";
                                                    }

                                                }
                                                @media(max-width:768px) {
                                                    #dash_module_" . $i . ",
                                                    #dash_module_" . $i . " .zone_module_container {
                                                        max-height: 700px;
                                                    }
                                                }
                                            </style>";

                                    if ($modules->list->custom_id[$i]) {
                                        $module_custom_id = 'id="' . $modules->list->custom_id[$i] . '"';
                                    } else {
                                        $module_custom_id = '';
                                    }

                                    echo "<div tabindex='0' " . $module_custom_id . " class='col-md-" . $modules->list->width[$i] . " " . $modules->list->custom_class[$i] . "'>";
                                    echo "<div id='dash_module_" . $i . "' class='dashboard_module'" . $style . ">";
                                    if ($modules->list->title_bar[$i]) {
                                        echo "<div id='dash_module_title_" . $i . "' class='dashboard_title_bar'>" . $modules->list->title[$i] . "</div>";
                                    }

                                    switch ($modules->list->scroll_bars[$i]) {
                                        case "none":
                                            $overflow = "overflow: hidden;";
                                            break;
                                        case "x":
                                            $overflow = "overflow-x: scroll; overflow-y: hidden;";
                                            break;
                                        case "y":
                                            $overflow = "overflow-y: scroll; overflow-x: hidden;";
                                            break;
                                        case "both":
                                            $overflow = "overflow: scroll;";
                                            break;
                                        case "auto":
                                            $overflow = "overflow: auto;";
                                            break;
                                        default:
                                            $overflow = "overflow: auto;";
                                            break;
                                    }
                                    echo "<div style='$overflow' class='zone_module_container'>";
                                    echo JModuleHelper::renderModule($module, $attribs);
                                    echo "</div>";
                                    echo "</div>";
                                    echo "</div>";
                                    if ($modules->list->height_parent[$i]) {
                                        $parent = $modules->list->height_parent[$i] - 1;
                                        echo "<script>
                                                        modResize('dash_module_{$parent}','dash_module_{$i}');
                                                        modules.push({'parent' : 'dash_module_{$parent}', 'child' : 'dash_module_{$i}'});
                                                      </script>";
                                    }
                                }
                            } elseif ($modules->list->id[$i] == 'spacer') {
                                echo "<div class='col-md-" . $modules->list->width[$i] . "'></div>";
                            }
                        }
                    }
                }

                ?>

                <script>
                    //Add a height to the dashboard modules so that the scrollbars work properly.
                    $(".zone_module_container").each(
                        function() {
                            var parentContainer = this.parentNode;
                            var titleBar = parentContainer.getElementsByClassName('zone_module_title_bar');
                            var parentHeight = $(parentContainer).css("height");
                            var titleHeight = $(titleBar).css("height");

                            var divHeight = parseInt(parentHeight) - parseInt(titleHeight);
                            if (divHeight < 0) {
                                divHeight = 0;
                            }

                            divHeight = "" + divHeight + "px";

                            $(this).css({
                                "height": divHeight
                            });
                        }
                    );
                </script>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 tovuti-content" style="margin-top:40px;">

                            <?php if ($option == 'com_jbusinessdirectory' && $input->get('view') == 'search') { ?>
                                <jdoc:include type="modules" name="directory_top" style="xhtml" />
                            <?php } ?>
                            <?php if ($this->countModules('top2')) { ?>
                                <jdoc:include type="modules" name="top2" style="xhtml" />
                            <?php } ?>
                            <?php if ($option == 'com_eshop') { ?>
                                <div class="container">
                                    <?php if ($this->countModules('top')) { ?>
                                        <jdoc:include type="modules" name="top" style="xhtml" />
                                        <div id="clr" style="margin-bottom:20px;"></div>
                                    <?php } ?>
                                    <?php if ($this->countModules('left')) {
                                        $mainStyle = ''; ?>
                                        <div class="left-mod-col">
                                            <jdoc:include type="modules" name="left" style="xhtml" />
                                        </div>
                                    <?php } else {
                                        $mainStyle = "width:100%;";
                                    } ?>
                                    <div class="main-col" <?php echo $mainStyle; ?>>
                                        <?php if (($option == 'com_community' && $view == 'profile') || ($option == 'com_users' && $view == 'profile') || ($option == 'com_converge' && $view == 'plans')) { ?>
                                            <style>
                                                .alert {
                                                    display: block;
                                                }
                                            </style>
                                            <jdoc:include type="message" />
                                        <?php } ?>
                                        <jdoc:include type="component" />
                                    </div>
                                </div>
                            <?php } else { ?>
                                <?php if (($option == 'com_community' && ($view == 'profile' || $view = 'groups')) || ($option == 'com_users' && $view == 'profile') || ($option == 'com_converge' && $view == 'plans')) { ?>
                                    <style>
                                        .alert {
                                            display: block;
                                        }
                                    </style>
                                    <jdoc:include type="message" />
                                <?php } ?>
                                <jdoc:include type="component" />
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class='clearfix'></div>

            </main>

            <footer id="footer" aria-label="Footer">
                <?php echo $brand->site_title; ?>
                &copy;
                <?php echo date('Y') . ' ' . AxsLanguage::text("TPL_MWM_GIN_COPYRIGHT", "All Rights Reserved."); ?>
                <br />
                <br />
            </footer>
            <script>
                $(".nano").nanoScroller();
            </script>
        </div>
    </div>

    <?php

    if ($javaScript->post) {
        echo "<script>";
        echo $javaScript->post;
        echo "</script>";
    }

    if ($css->post) {
        echo "<style>";
        echo $css->post;
        echo "</style>";
    }
    ?>

    <?php if ($this->countModules('mediamodal')) { ?>
        <jdoc:include type="modules" name="mediamodal" style="xhtml" />
    <?php } ?>
    <?php
    if ($option == 'com_users' && $view == 'profile') {
        $checkNotifications = false;
    } else {
        $checkNotifications = true;
    }
    if ($checkNotifications) {
        $notifications = AxsNotifications::loadNotifications();
        if ($notifications) {
            echo $notifications;
        }
    }
    ?>
</body>

</html>