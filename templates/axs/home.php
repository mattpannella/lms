<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

defined('_JEXEC') or die('Restricted access');

$homepage = $brand->homepage;
$app = JFactory::getApplication();
$client = $app->client;
$input = $app->input;
//check if is mobile = $client->mobile returns boolean

$menu = $app->getMenu();
$activeMenuItem = $menu->getActive();
$pageclass = '';

if (is_object($activeMenuItem)) {
    $pageclass = $activeMenuItem->params->get('pageclass_sfx');
    $meta_description = $activeMenuItem->params->get('menu-meta_description');
    $meta_keywords = $activeMenuItem->params->get('menu-meta_keywords');
    $meta_robots = $activeMenuItem->params->get('robots');
}

if ($activeMenuItem == $menu->getDefault() || ($view == 'landingpage') || ($view == 'login_page')) {
    $isHomePage = true;
} else {
    $isHomePage = false;
}

if ((($view == 'login_page') && $input->get('id')) ||  ($isHomePage && $brand->site_details->homepage_type == 'login_page') && $view != 'landingpage') {
    if ($view == 'login_page' && $input->get('id')) {
        $loginPageId = $input->get('id');
    } else {
        $loginPageId = $brand->site_details->login_page;
    }
    $loginPage = AxsLandingPages::getLoginPage($loginPageId);
    include('components/com_axs/views/login_page/tmpl/default.php');
    exit;
}

if (($view == 'landingpage') && $input->get('id')) {
    $landpageId = $input->get('id');
    $homepage = AxsLandingPages::getLandingPage($landpageId);
}

AxsExtra::removeMessage($app, 'Login successful');
AxsExtra::removeMessage($app, 'You have logged out.');
AxsExtra::removeMessage($app, 'The security token did not match. The request was aborted to prevent any security breach. Please try again.');
AxsExtra::removeMessage($app, 'El token de seguridad no coincidía. La solicitud se abortó para evitar cualquier violación de seguridad. Vuelve a intentarlo.');
AxsExtra::removeMessage($app, 'SMTP Error');


$option   = $input->get('option', '');
$view     = $input->get('view', '');

?>

<!doctype html>
<html xmlns="//www.w3.org/1999/xhtml" xmlns:jdoc="http://www.w3.org/2001/XInclude" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">

<head lang="en-gb">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php if (!empty($meta_description)) : ?>
        <meta name="description" content="<?php echo $meta_description ?>">
    <?php endif; ?>

    <?php if (!empty($meta_keywords)) : ?>
        <meta name="keywords" content="<?php echo $meta_keywords ?>">
    <?php endif; ?>

    <?php if (!empty($meta_robots)) : ?>
        <meta name="robots" content="<?php echo $meta_robots ?>">
    <?php endif; ?>

    <?php
    $fonts = AxsFonts::getFonts($brand->id);
    foreach ($fonts as $font) {
        if ($font->link) {
            echo $font->link . "\n";
        } else {
            echo "<link href='//fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>" . "\n";
        }
    }
    ?>
    <style>
        :root {
            /* Tovuti Platform Default Theme */
            --primary-color: #1e2935;
            /* Tovuti Blue */

        }
    </style>
    <?php
    $useRootStyles = false;
    if (!$brand->site_details->root_base_color) {
        $brand->site_details->root_base_color = "#fff";
    }
    if (
        $brand->site_details->tovuti_root_styles &&
        $brand->site_details->root_primary_color &&
        $brand->site_details->root_accent_color  &&
        $brand->site_details->root_base_color
    ) {
        $useRootStyles = true;
    ?>
        <style>
            :root {
                /* Tovuti Platform Default Theme */
                --primary-color: <?php echo $brand->site_details->root_primary_color; ?>;
                /* Tovuti Blue */
                --accent-color: <?php echo $brand->site_details->root_accent_color; ?>;
                /* Tovuti Orange */
                --base-color: <?php echo $brand->site_details->root_base_color; ?>;
                /* Background Base */

            }
        </style>

    <?php } ?>

    <meta name="apple-mobile-web-app-title" content="<?php echo $brand->site_title; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="/<?php echo $brand->logos->favicon ?>">
    <link rel="icon" href="/<?php echo $brand->logos->favicon ?>" type="image/x-icon">

    <?php if ($option == 'com_eshop') { ?>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <?php } ?>

    <jdoc:include type="head" />


    <?php if ($option != 'com_eshop') { ?>
        <?php
        JHtml::_('jquery.framework');
        JHtml::_('bootstrap.framework');
        ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <?php if ($option != 'com_splms') { ?>
            <script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <?php } ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/axs/css/bootstrap.css?v=6">
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/eshop-patch.css">
    <?php } ?>


    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/icons/style.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/axs/js/dashboard.js?v=3"></script>
    <!-- Deprecate old and multiple versions of FontAwesome and update to FontAwesome 6 -->
    <!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> -->
    <!-- <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.4.1/css/all.css"> -->
    <!-- ADD fontawesome 6 -->
    <script src="https://kit.fontawesome.com/8c1112f2d0.js" crossorigin="anonymous"></script>
    <!-- ADD fontawesome 6 -->
    <?php if ($isHomePage) { ?>
        <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/reset.css">
    <?php } ?>
    <!-- CSS reset -->
    <script>
        //Some bootstrap function makes the language selector disappear when you click off of it.  This puts it back immediately.
        $(document).on('click', function() {
            $('#user_language_selector').show();
        });
        /*$('.nav-tab').click(function() {
            console.log(this);
        });*/
    </script>
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/styles.css?v=68">
    <link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/css/slidefader.css?v=29">
    <!-- Resource style -->
    <script src="<?php echo $this->baseurl ?>/js/modernizr.js"></script>
    <!-- Modernizr -->
    <!--  <script src="//code.jquery.com/jquery-1.10.2.min.js"></script> -->

    <?php
    if (isset($brand->social)) {
        $social_links = $brand->social;
    } else {
        $social_links = null;
    }
    ?>


    <?php

    if ($social_links && $homepage->header->show_socialmedia) {
        $social_bar_padding = 40;
    } else {
        $social_bar_padding = 0;
    }

    $headerHeight =  $homepage->header->height_pixel + $social_bar_padding . "px";

    if ($homepage->header->overlay == "overlay") {
        $slidesHeaderOffset = $social_bar_padding . "px";
    } else {
        $slidesHeaderOffset = $headerHeight;
    }

    if ($social_links) {

        if ($homepage->header->socialmedia_color) {
            $social_header_color = $homepage->header->socialmedia_color;
        } else {
            $social_header_color = "#aaa";
        }

        if ($homepage->header->socialmedia_text_color) {
            $social_header_text_color = $homepage->header->socialmedia_text_color;
        } else {
            $social_header_text_color = "#fff";
        }

        if ($homepage->header->socialmedia_hover_color) {
            $social_header_hover_color = $homepage->header->socialmedia_hover_color;
        } else {
            $social_header_hover_color = "#ec3d35";
        }

        if ($homepage->footer->socialmedia_color) {
            $social_footer_color = $homepage->footer->socialmedia_color;
        } else {
            $social_footer_color = "#aaa";
        }

        if ($homepage->footer->socialmedia_text_color) {
            $social_footer_text_color = $homepage->footer->socialmedia_text_color;
        } else {
            $social_footer_text_color = "#fff";
        }

        if ($homepage->footer->socialmedia_hover_color) {
            $social_footer_hover_color = $homepage->footer->socialmedia_hover_color;
        } else {
            $social_footer_hover_color = "#ec3d35";
        }

    ?>
        <style>
            .social-bar {
                width: 100%;
                height: 40px;
                color: #fff;
                padding: 5px;
            }

            .social-bar-top {
                background: <?php echo $social_header_color; ?>;
            }

            .social-bar-bottom {
                background: <?php echo $social_footer_color; ?>;
            }

            .social-links a {
                margin-left: 10px;
                margin-right: 10px;
                text-decoration: none;
                font-size: 30px;
            }

            .social-links a:hover {
                text-decoration: none;
            }

            .social-links-top a {
                color: <?php echo $social_header_text_color; ?>;
            }

            .social-links-top a:hover {
                color: <?php echo $social_header_hover_color; ?>;
            }

            .social-links-bottom a {
                color: <?php echo $social_footer_text_color; ?>;
            }

            .social-links-bottom a:hover {
                color: <?php echo $social_footer_hover_color; ?>;
            }

            .social-links-text,
            .social-links-text a {
                margin-left: 10px;
                text-decoration: none;
                font-size: 20px;
            }

            .social-links-text-top,
            .social-links-text-top a {
                color: <?php echo $social_header_text_color; ?>;
            }

            .social-links-text-bottom,
            .social-links-text-bottom a {
                color: <?php echo $social_footer_text_color; ?>;
            }
        </style>
    <?php
    }
    ?>



    <?php

    $show = $homepage->header->show;
    switch ($homepage->header->header_layout) {
        case "fullwidth":
            $header_layout = "container-fluid";
            break;
        case "centered":
            $header_layout = "container";
            break;
        default:
            $header_layout = "container-fluid";
            break;
    }

    if ($show) {
        if ($show->logo) {
            switch ($homepage->header->logo_position) {
                case "above":
                    $logo_above = true;
                    $logo_id = "logo_above";
                    break;
                case "left":
                    $logo_left = true;
                    $logo_id = "cd-logo";
                    break;
            }

            switch ($homepage->header->logo_height_type) {
                case "auto":
                    $logo_height = "auto";
                    break;
                case "percent":
                    $logo_height = $homepage->header->logo_height_percentage . "%";
                    break;
                case "pixel":
                    $logo_height = $homepage->header->logo_height_pixel . "px";
                    break;
            }
        }
    } else {
        //For legacy sites that don't have these settings.
        $show = new stdClass();
        $show->logo = true;
        $show->menu = true;
        $show->slides = true;
        $show->custom = false;
        $logo_left = true;
    }

    ?>

    <style>
        .cd-primary-nav>ul>li>a>img {
            width: 25px;
            position: absolute;
            left: -15px;
            bottom: 10px;
        }

        @media only screen and (min-width: 768px) {
            .header-logo {
                height: <?php echo $logo_height; ?>;
            }


            #cd-logo img {
                display: inherit;
                max-height: inherit;
            }

            <?php if ($logo_above) { ?>.cd-primary-nav {
                float: none;
                margin-right: auto;
                margin-left: auto;
                width: 98%;
            }

            <?php } ?><?php if ($header_layout == "container") { ?>#cd-logo {
                margin: 5px 0 0 0;
            }

            .cd-primary-nav {
                margin-right: 0;
            }

            <?php } ?>
        }

        body {

            <?php
            if ($homepage->default_font != "") {
                echo    "font-family: '" . $homepage->default_font . "';";
            } else if ($brand->default_font != "") {
                echo    "font-family: '" . $brand->site_details->default_font . "';";
            }
            ?>
        }

        .language_text {
            color: <?php echo $homepage->colors->header_text; ?>
        }

        <?php

        if ($homepage->header->font != "") {
        ?>.cd-hero {
            font-family: '<?php echo $homepage->header->font; ?>'
        }

        <?php
        }

        $r = 255;
        $g = 255;
        $b = 255;

        $subHeaderBackgroundColor = "";
        $headerBackgroundColor = "";
        if (!$isHomePage) {

            if ($homepage->colors->subpages_background != "") {
                list($r, $g, $b) = sscanf($homepage->colors->subpages_background, "#%02x%02x%02x");
                $subHeaderBackgroundOpacity = $homepage->colors->subpages_background_transparency;
                $subHeaderBackgroundColor = "rgba( $r, $g, $b, $subHeaderBackgroundOpacity );";
            }
        ?>.cd-primary-nav a {
            color: <?php echo $homepage->colors->subpages_text; ?>;
        }

        .cd-header {
            height: <?php echo $headerHeight; ?>;
            background: <?php echo $subHeaderBackgroundColor; ?>;
            color: <?php echo $homepage->colors->subpages_text ?>;
            <?php
            if ($homepage->header->header_fixed) {
                echo "position: fixed;";
            } else {
                echo "position: relative;";
            }
            ?>box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.75);
            /* border-bottom:      <?php echo $homepage->colors->subpages_text ?> 1px solid;*/
            z-index: 100;
        }


        .language_text {
            color: <?php echo $homepage->colors->subpages_text; ?>
        }


        <?php

        } else {
            if ($homepage->colors->background != "") {
                list($r, $g, $b) = sscanf($homepage->colors->background, "#%02x%02x%02x");
                $headerBackgroundOpacity =  $homepage->colors->background_transparency;
                $headerBackgroundColor = "background-color:   rgba( $r, $g, $b, $headerBackgroundOpacity );";
            }
        ?>.cd-primary-nav a {
            color: <?php echo $homepage->colors->header_text ?>;
        }

        .cd-header {
            width: 100%;
            color: <?php echo $homepage->colors->header_text ?>;
            <?php echo $headerBackgroundColor; ?>;

            <?php
            if ($homepage->header->drop_shadow) {
            ?>box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.75);
            <?php
            }
            if ($homepage->header->overlay == "overlay" && !$homepage->header->header_fixed) {
                echo "position: absolute;";
            } elseif ($homepage->header->header_fixed) {
                echo "position: fixed;";
            } else {
                echo "position: relative;";
            }
            ?>
        }

        <?php
        }
        ?><?php
            if ($homepage->header->overlay == "overlay") {
                if ($social_bar_padding) {
                    $contentPaddingTop = $social_bar_padding . 'px';
                }
            }

            if (($homepage->header->header_fixed && $homepage->header->overlay == "above") || ($homepage->header->header_fixed && !$isHomePage)) {
                $contentPaddingTop = $headerHeight;
            }

            ?>.content-pad {
            padding-top: <?php echo $contentPaddingTop; ?>;
            <?php echo $contentMarginTop; ?>position: relative;

        }

        <?php
        //Set Slider Height
        $slideDesktopHeight = '650px';
        $slideMobileHeight  = '350px';

        if ($homepage->header->slides_height_type == 'pixel' || !$homepage->header->slides_height_type) {

            if ($homepage->header->slides_desktop_height) {
                $slideDesktopHeight = $homepage->header->slides_desktop_height . 'px';
            }

            if ($homepage->header->slides_mobile_height) {
                $slideMobileHeight = $homepage->header->slides_mobile_height . 'px';
            }
        }

        if ($homepage->header->slides_height_type == 'percent') {

            if ($homepage->header->slides_desktop_height_percent) {
                $slideDesktopHeight = 'calc(' . $homepage->header->slides_desktop_height_percent . 'vh - ' . $slidesHeaderOffset . ')';
            }

            if ($homepage->header->slides_mobile_height_percent) {
                $slideMobileHeight = 'calc(' . $homepage->header->slides_mobile_height_percent . 'vh - ' . $slidesHeaderOffset . ')';
            }
        }


        ?>@media only screen and (min-width: 768px) {

            .cd-hero-slider,
            .cd-hero {
                height: <?php echo $slideDesktopHeight; ?>;
            }
        }

        @media only screen and (max-width: 767px) {

            .cd-hero-slider,
            .cd-hero {
                height: <?php echo $slideMobileHeight; ?>;
            }
        }

        .cd-header {
            height: <?php echo $headerHeight; ?>;
        }

        .header-logo-container {
            height: 100%;
        }





        #lang_box {
            margin-left: 10px;
            margin-top: 20px;
            float: left;
        }

        @media only screen and (max-width: 767px) {
            #lang_box {
                margin-left: 0px;
                margin-top: 0px;
                top: 5px;
                right: 100px;
                float: left;
                position: absolute;
                z-index: 200;
            }

            /*.cd-primary-nav {
                    padding-top: 0px;
                    background: url(../../../assets/cd-icon-menu.svg) no-repeat right center;
                    position: absolute;
                    margin-right: 0;
                    top: 0px;
                    right: 10px;
                    width: 100%;
                    cursor: pointer;
                }
                .cd-primary-nav ul.is-visible > #more_container {
                    display: none;
                }*/
            .content-pad {
                margin-top: 80px;
                padding-top: 0px;
            }

            .logo-clear {
                display: none;
            }

            .cd-header {
                width: 100%;
                height: 80px;
                position: fixed;
                box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.75);
                z-index: 100;
            }

            .header-logo {
                max-height: 100%;
                max-width: 100%;

            }

            .header-logo-container {
                max-height: 70%;
                max-width: 75%;
                position: absolute;
                z-index: 400;
            }

            .cd-primary-nav li ul.nav-child {
                top: 50px;
            }
        }

        /*@media only screen and (min-width: 768px) {*/
        .cd-primary-nav {
            <?php
            if (!$logo_above) {
            ?>margin-top: <?php echo ($homepage->header->height_pixel - 40) / 2; ?>px;
            <?php
            }
            ?>z-index: 100;
        }

        #menu-open-button {
            <?php
            if (!$logo_above) {
            ?>margin-top: <?php echo (($homepage->header->height_pixel - 40) / 2) - 10; ?>px;
            <?php
            }
            ?>z-index: 100;
        }

        .cd-primary-nav #more_dropdown {
            top: 20px;
            width: 190px;
            left: -25px;
        }

        #more_container {
            height: 26px;
            font-size: 20px;
            cursor: pointer;
        }

        /*}*/

        <?php if ($option == 'com_users' && $view == 'login') { ?>body {
            background: #eee;
        }

        .nav {
            display: none;
        }

        <?php } ?>
    </style>

    <?php
    $javaScript = $homepage->js;
    $css = $homepage->css;

    if ($javaScript->pre) {
        echo "<script>";
        echo $javaScript->pre;
        echo "</script>";
    }

    if ($css->pre) {
        echo "<style>";
        echo $css->pre;
        echo "</style>";
    }
    ?>
    <link rel="stylesheet" href="/media/player/plyr.css" />

    <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=es6,Array.prototype.includes,CustomEvent,Object.entries,Object.values,URL,Math.trunc" crossorigin="anonymous"></script>
    <script src="/media/player/plyr.js"></script>
    <script src="/media/player/tracking.js?v=3"></script>


    <script src="https://js.stripe.com/v3/"></script>
    <?php if (!$_COOKIE['timezone']) { ?>
        <script>
            var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
            document.cookie = "timezone = " + timezone;
        </script>
    <?php } ?>
</head>

<body class="<?php echo $pageclass; ?>">
    <div id="header_bar" class="cd-header">

        <?php
        if ($social_links && $homepage->header->show_socialmedia) {

            switch ($homepage->header->socialmedia_orientation) {
                case "left":
                    $social_link_float = "pull-left";
                    $social_text_float = "pull-right";
                    break;
                default:
                    $social_link_float = "pull-right";
                    $social_text_float = "pull-left";
                    break;
            }
        ?>

            <div class="social-bar social-bar-top hidden-xs">
                <?php
                if ($social_links->socialmedia_text) {
                ?>
                    <div class="<?php echo $social_text_float; ?> social-links-text social-links-text-top">
                        <?php echo $social_links->socialmedia_text; ?>
                    </div>
                <?php
                }

                switch ($homepage->header->socialmedia_icon_style) {
                    case "square":
                        $icon_style = "-square";
                        break;
                    case "standard":
                    default:
                        $icon_style = "";
                }
                ?>





                <div class="<?php echo $social_link_float; ?> social-links social-links-top">
                    <?php if ($social_links->facebook_link) { ?>
                        <a href="<?php echo $social_links->facebook_link; ?>" target="_blank" class="fa-brands fa-facebook<?php echo $icon_style; ?>" title="facebook" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->twitter_link) { ?>
                        <a href="<?php echo $social_links->twitter_link; ?>" target="_blank" class="fa-brands fa-twitter<?php echo $icon_style; ?>" title="twitter" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->youtube_link) { ?>
                        <a href="<?php echo $social_links->youtube_link; ?>" target="_blank" class="fa-brands fa-youtube<?php echo $icon_style; ?>" title="youtube" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->linkedin_link) { ?>
                        <a href="<?php echo $social_links->linkedin_link; ?>" target="_blank" class="fa-brands fa-linkedin<?php echo $icon_style; ?>" title="linkedin" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->instagram_link) { ?>
                        <a href="<?php echo $social_links->instagram_link; ?>" target="_blank" class="fa-brands fa-instagram" alt="instagram" title="instagram" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->pinterest_link) { ?>
                        <a href="<?php echo $social_links->pinterest_link; ?>" target="_blank" class="fa-brands fa-pinterest<?php echo $icon_style; ?>" title="pinterest" rel="noopener"></a>
                    <?php } ?>

                    <?php if ($social_links->google_plus_link) { ?>
                        <a href="<?php echo $social_links->google_plus_link; ?>" target="_blank" class="fa-brands fa-google-plus<?php echo $icon_style; ?>" title="google" rel="noopener"></a>
                    <?php } ?>


                </div>
            </div>
        <?php
        }
        ?>

        <?php
        if ($show->custom) {
            echo $homepage->header->custom_html;
        }
        ?>
        <div class="<?php echo $header_layout; ?>">
            <div class="row header-content">
                <div id="<?php echo $logo_id; ?>" class="header-logo-container">
                    <?php if ($homepage->logo_link || !isset($homepage->logo_link)) { ?>
                        <a href="/">
                        <?php } ?>
                        <img class="header-logo" id="homepage_logo" src="<?php echo $homepage->logo ?>" <?php echo AxsAccessibility::image($homepage->logo); ?>>
                        <?php if ($homepage->logo_link || !isset($homepage->logo_link)) { ?>
                        </a>
                    <?php } ?>
                </div>
                <?php
                if ($logo_above) {
                ?>
                    <div class="clearfix logo-clear"></div>
                <?php
                }

                if ($this->countModules('language') && $brand->site_details->language_selector) {
                ?>
                    <div id="lang_box">
                        <jdoc:include type="modules" name="language" style="xhtml" />
                    </div>
                <?php
                }

                if ($show->menu) {

                    $new_div_width = 'auto';
                    if ($header_layout == 'container-fluid') {
                        $more_width_offset = 175;
                    } else {
                        $more_width_offset = 40;
                    }

                    if ($logo_above) {
                        $above = "true";
                    } else {
                        $above = "false";
                    }
                ?>
                    <jdoc:include type="modules" name="default_public_menu" style="raw" />



                    <?php
                    if ($homepage->header->menu_resize || !isset($homepage->header->menu_resize)) {
                    ?>
                        <script>
                            $('.cd-primary-nav').hide();

                            var new_div_width = "<?php echo $new_div_width; ?>";
                            var more_width_offset = <?php echo $more_width_offset; ?>;
                            var logoAbove = <?php echo $above; ?>;
                        </script>
                        <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/axs/js/home.js?v=8"></script>
                    <?php
                    }
                    ?>
                    <script type="text/javascript" src="<?php echo $this->baseurl ?>/templates/axs/js/off-canvas-menu.js?v=1"></script>
                <?php
                }
                ?>

            </div>
        </div>
    </div>
    <div class="clearfix content-pad"></div>

    <?php
    if (!$isHomePage) {

        if ($activeMenuItem) {
            $fullWidth = $activeMenuItem->params->get('full_width');
            $header_background = $activeMenuItem->params->get('header_background');
            $header_background_text = $activeMenuItem->params->get('header_background_text');
            $header_background_text_size = $activeMenuItem->params->get('header_background_text_size');
            $header_background_text_font = $activeMenuItem->params->get('header_background_text_font');
        }


        if ($header_background_text_size) {
            $titleSize = (int)$header_background_text_size;
        } else {
            $titleSize = 80;
        }

        if ($header_background_text_font) {
            $titleFont = ' font-family: ' . $header_background_text_font . '; ';
        } else {
            $titleFont = '';
        }

        if ($fullWidth) {
            $fluid = 'fullWidth';
        } else {
            $fluid = 'container';
        }

    ?>
        <?php if ($header_background) { ?>
            <div class="header-fullwidth" style="
                        background-image: url(/<?php echo $header_background; ?>);
                        background-size:cover;
                        background-position:center;
                        height: 300px;
                        position: relative;" <?php echo AxsAccessibility::image($header_background); ?>>
                <div class="header-fullwidth-overlay" style=" width:100%; height:300px; position:relative;">
                    <?php if ($header_background_text) { ?>
                        <div style="background: rgba(0,0,0,.6);">
                            <h1 class="slide-overlay" style="color:#ffffff;font-size:<?php echo $titleSize; ?>px;text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.8);<?php echo $titleFont; ?>">
                                <?php echo $header_background_text; ?>
                            </h1>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>

        <div class="<?php echo $fluid; ?>">
            <?php
            if ($option == 'com_jbusinessdirectory' && $view == 'search') {
            ?>
                <jdoc:include type="modules" name="directory_top" style="xhtml" />
            <?php
            }

            if ($this->countModules('top2')) {
            ?>
                <jdoc:include type="modules" name="top2" style="xhtml" />
            <?php
            }
            ?>

            <div id="clr"></div>

            <?php
            if ($option == 'com_eshop') {
            ?>
                <div class="<?php echo $fluid; ?>">
                    <?php
                    if ($this->countModules('top')) {
                    ?>
                        <jdoc:include type="modules" name="top" style="xhtml" />
                        <div id="clr" style="margin-bottom:20px;"></div>
                    <?php
                    }

                    if ($this->countModules('left')) {
                        $mainStyle = '';
                    ?>
                        <div class="left-mod-col">
                            <jdoc:include type="modules" name="left" style="xhtml" />
                        </div>
                    <?php
                    } else {
                        $mainStyle = "width:100%;";
                    }
                    ?>

                    <div class="main-col" <?php echo $mainStyle; ?>>
                        <jdoc:include type="message" />
                        <jdoc:include type="component" />
                    </div>
                </div>
            <?php
            } else {
            ?>
                <jdoc:include type="message" />
                <jdoc:include type="component" />
            <?php
            }
            ?>
        </div>


        <?php
    } else {
        if ($show->slides) {
        ?>
            <div class="cd-hero">
                <ul class="cd-hero-slider autoplay">
                    <?php
                    $slideshow = json_decode($homepage->header->slideshow);
                    if ($slideshow) {

                        $slides = $slideshow;
                        $slideCount = 0;
                        foreach ($slides as $slide) {
                            if (($slide->view == 'desktop' && $client->mobile) || ($slide->view == 'mobile' && !$client->mobile)) {
                                continue;
                            }
                            $slideCount++;
                            $button = '';
                            if ($slide->button_text || $slide->link) {
                                $button .= '
                                                <style>
                                                    a.slide-btn' . $slideCount . '  {
                                                        background-color: ' . $slide->button_color . ';
                                                        color: ' . $slide->button_text_color . ';
                                                    }

                                                    a.slide-btn' . $slideCount . ':hover {
                                                        background-color: ' . $slide->button_color_hover . ';
                                                        color: ' . $slide->button_text_color_hover . ';
                                                    }
                                                </style>
                                            ';
                                $button .= '<a href="' . $slide->link . '" class="slide-btn' . $slideCount . ' btn' . $slide->button_size . '">';
                                if ($slide->button_icon) {
                                    $button .= '<i class="lizicon ' . $slide->button_icon . '"></i> ';
                                }

                                $button .= $slide->button_text;
                                $button .= '</a>';
                            }



                            //CD Hero does not accept extensions, so create a file name without them.
                            $file = pathinfo($slide->file);
                            $fileName = $file['dirname'] . '/' . $file['filename'];

                            //$mime will be false if the file is a URL
                            $mime = mime_content_type($slide->file);
                            $type = null;

                            //$headers will be false if the file is a local file.
                            $headers = @get_headers($slide->file);
                            if ($headers && !$mime) {
                                foreach ($headers as $header) {
                                    $pos = strpos($header, "Content-Type:");
                                    if ($pos !== false) {
                                        $mime = substr($header, strpos($header, ":") + 2);
                                    }
                                }
                            }

                            $isVideo = strpos($mime, "video/");
                            $isImage = strpos($mime, "image/");

                            if ($isVideo !== false) {
                                $type = 0;
                            } else if ($isImage !== false) {
                                $type = 1;
                            }

                            if ($type == 0) {             //VIDEO
                                $main_class = "cd-bg-video";
                                $wrapper_div = "<div class='cd-bg-video-wrapper' data-video='/$fileName'></div>";
                                $slider_style = "";
                            } else if ($type == 1) {      //IMAGE
                                $main_class = "";
                                $wrapper_div = "";

                                if ($headers) {
                                    $fileName = $slide->file;
                                } else {
                                    $fileName = "/" . $slide->file;
                                }
                                $slider_style = "
                                                filter:
                                                grayscale(" . ($slide->slide_grayscale / 100) . ")
                                                blur(" . ($slide->slide_blur) . "px)
                                                contrast(" . ($slide->slide_fade_gray / 100) . ")
                                                opacity(" . ($slide->slide_fade_black / 100) . ")
                                                sepia(" . ($slide->slide_sepia / 100) . ");
                                                background-image: url($fileName);
                                                background-size:cover;
                                                background-position:center;";
                            }

                            $slide_text_style = '';
                            if ($slide->text_color) {
                                $slide_text_style .= 'color:' . $slide->text_color . ';';
                            }
                            if ($slide->custom_text_size) {
                                $slide->text_size = $slide->custom_text_size . 'px';
                            }
                            if ($slide->text_size) {
                                $slide_text_style .= 'font-size:' . $slide->text_size . ';';
                            }
                            if ($slide->text_shadow) {
                                $slide_text_style .= 'text-shadow: 1px 1px 2px rgba(0, 0, 0, 0.8);';
                            }
                            if ($slide->text_font) {
                                $slide_text_style .= 'font-family:' . $slide->text_font . ';';
                            }
                    ?>

                            <li class="<?php echo $main_class;
                                        if ($slideCount == 1) {
                                            echo ' selected ';
                                        } ?>" <?php echo AxsAccessibility::image($fileName); ?> tabindex="0">

                                <div class="cd-full-width" style="<?php echo $slider_style; ?>">

                                </div>
                                <?php if ($slide->text || $button) { ?>
                                    <div class="slide-overlay">
                                        <h2 style="<?php echo $slide_text_style; ?>"><?php echo $slide->text; ?></h2>
                                        <?php echo $button; ?>
                                    </div>
                                <?php } ?>
                                <?php echo $wrapper_div; ?>

                            </li>
                        <?php
                        }
                    } else {
                        $slides = $homepage->header->slides;

                        for ($i = 0; $i < count($slides->file); $i++) {
                            //CD Hero does not accept extensions, so create a file name without them.
                            $file = pathinfo($slides->file[$i]);
                            $fileName = $file['dirname'] . '/' . $file['filename'];

                            //$mime will be false if the file is a URL
                            $mime = mime_content_type($slides->file[$i]);
                            $type = null;

                            //$headers will be false if the file is a local file.
                            $headers = get_headers($slides->file[$i]);
                            if ($headers && !$mime) {
                                foreach ($headers as $header) {
                                    $pos = strpos($header, "Content-Type:");
                                    if ($pos !== false) {
                                        $mime = substr($header, strpos($header, ":") + 2);
                                    }
                                }
                            }

                            $isVideo = strpos($mime, "video/");
                            $isImage = strpos($mime, "image/");

                            if ($isVideo !== false) {
                                $type = 0;
                            } else if ($isImage !== false) {
                                $type = 1;
                            }

                            if ($type == 0) {             //VIDEO
                                $main_class = "cd-bg-video";
                                $wrapper_div = "<div class='cd-bg-video-wrapper' data-video='/$fileName'></div>";
                                $slider_style = "";
                            } else if ($type == 1) {      //IMAGE
                                $main_class = "";
                                $wrapper_div = "";

                                if ($headers) {
                                    $fileName = $slides->file[$i];
                                } else {
                                    $fileName = "/" . $slides->file[$i];
                                }
                                $slider_style = "background-image: url($fileName); background-size:cover; background-position:center;";
                            }
                        ?>

                            <li class="<?php echo $main_class;
                                        if ($i == 0) {
                                            echo ' selected';
                                        } ?>">
                                <div class="cd-full-width" style="<?php echo $slider_style ?>">
                                    <h2><?php echo $slides->text[$i]; ?></h2>
                                    <?php echo $slides->link[$i]; ?>
                                </div>
                                <?php echo $wrapper_div; ?>
                            </li>
                    <?php
                        }
                    }

                    ?>
                </ul>
                <?php if ($homepage->header->slides_overlay) { ?>
                    <div class="absolute-bottom">
                        <div class="row opacity-50">
                            <div class="container-fluid">
                                <div class="col-sm-12 pad-top extra-pad-bottom white">
                                    <?php echo $homepage->header->slides_overlay; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        <?php
        }

        $testimonials = $homepage->testimonials;
        $testimonial_count = count($testimonials->list->author);

        if ($testimonial_count > 0 && $testimonials->pos != 'none') {
        ?>
            <div id="testimonials_area" class="row" style="display:none;">
                <div class="container-fluid">
                    <div class="row">
                        <a id="testimonials"></a>
                        <link rel="stylesheet" href="/templates/axs/testimonials/css/reset.css"> <!-- CSS reset -->
                        <link rel="stylesheet" href="/templates/axs/testimonials/css/style.css?v=2">
                        <script src="/templates/axs/testimonials/js/modernizr.js"></script>

                        <div class="cd-testimonials-wrapper cd-container">

                            <?php
                            if ($testimonials->font != "") {
                                $testimonial_style = "style=\"font-family: '" . $testimonials->font . "';\"";
                            }
                            ?>

                            <ul class="cd-testimonials" <?php echo $testimonial_style; ?>>
                                <?php
                                $list = $testimonials->list;
                                for ($i = 0; $i < count($list->author); $i++) { ?>
                                    <li>
                                        <p>
                                            <?php echo $list->quote[$i]; ?>
                                        </p>

                                        <div class="cd-author">
                                            <?php
                                            $url = $list->author_image[$i];
                                            ?>

                                            <?php if ($url != "") { ?>
                                                <img src="<?php echo $url ?>" <?php echo AxsAccessibility::image($url); ?>>
                                            <?php } else { ?>
                                                <img src="/images/user.png" alt="Testimonal Profile Place Holder Image">
                                            <?php } ?>

                                            <ul class="cd-author-info">
                                                <li><?php echo $list->author[$i]; ?></li>
                                            </ul>
                                        </div>
                                    </li>

                                <?php } ?>
                            </ul>

                            <!-- cd-testimonials -->

                            <!--<a href="#0" class="cd-see-all">See all</a>-->
                        </div> <!-- cd-testimonials-wrapper -->

                        <div id="testimonials-all" class="cd-testimonials-all" style="display:none;">
                            <div class="cd-testimonials-all-wrapper">
                                <ul>
                                    <?php for ($i = 0; $i < count($list->author); $i++) { ?>
                                        <li class="cd-testimonials-item">
                                            <p>
                                                <?php echo $list->quote[$i]; ?>
                                            </p>

                                            <?php
                                            $url = $list->author_image[$i];
                                            ?>

                                            <div class="cd-author">
                                                <?php if ($url != "") { ?>
                                                    <img src="<?php echo $url ?>" <?php echo AxsAccessibility::image($url); ?>>
                                                <?php } else { ?>
                                                    <img src="/images/user.png" alt="Testimonal Profile Place Holder Image">
                                                <?php } ?>
                                                <ul class="cd-author-info">
                                                    <li><?php echo $list->author[$i]; ?></li>
                                                </ul>
                                            </div>
                                        </li>
                                    <?php } ?>

                                </ul>
                            </div> <!-- cd-testimonials-all-wrapper -->
                            <a href="#0" class="close-btn">Close</a>
                        </div> <!-- cd-testimonials-all -->

                        <script src="/templates/axs/testimonials/js/masonry.pkgd.min.js"></script>
                        <script src="/templates/axs/testimonials/js/jquery.flexslider-min.js"></script>
                        <script src="/templates/axs/testimonials/js/main.js?v=9"></script>
                    </div>
                </div>
            </div>

        <?php } ?>

        <!-- .cd-hero -->

        <?php
        /*
                    Check for Zone existance.
                */

        $zones = $homepage->zones;

        $zone_count = 0;
        foreach ($zones as $zone) {

            $zone_count++;

            $zoneTransparency = (float)((int)$zone->background_transparency / 100.00);
            $zoneAlpha = (1.00 - $zoneTransparency);

            $type = $zone->type;

            if ($zone->active == false) {
                continue;
            }

            /*
                        Zone CSS
                    */
            echo "<style>";
            /*
                            Main CSS
                        */
            echo ".zone_$zone_count {";
            if ($zone->background_color != "") {
                list($r, $g, $b) = sscanf($zone->background_color, "#%02x%02x%02x");
                echo "background-color: rgba( $r, $g, $b, $zoneAlpha ) ;";
            }

            if ($zone->background_image != "") {
                echo "background: url(\"/$zone->background_image\") 0 0 fixed no-repeat;";
                echo "
                                    background-size:        cover;
                                    background-position:    center;
                                    height:                 auto;
                                    margin:                 0 auto;
                                    padding:                100px 0;
                                    overflow:               hidden;
                                ";
            }

            echo '}';

            if ($zone->background_image != "" && $zone->background_color != "") {
                list($r, $g, $b) = sscanf($zone->background_color, "#%02x%02x%02x");
                echo ".zone_" . $zone_count . "_overlay {";
                echo "height: 100%;";
                echo "width: 100%;";
                echo "position: absolute;";
                echo "left: 0;";
                echo "top: 0;";
                echo "background-color: rgba( $r, $g, $b, $zoneAlpha );";
                echo "}";
            }
            /*
                            Title CSS
                        */
            echo ".zone_" . $zone_count . "_title {";
            echo "color: ";
            if ($zone->title_color != "") {
                echo "$zone->title_color;";
            } else {
                echo "#ffffff";
            }

            if ($zone->title_font != "") {
                echo "font-family: '" . $zone->title_font . "';";
            }
            echo "}";
            echo "</style>";

            $zone_width = $zone->screen_width;
            $zone_margin = (100 - $zone_width) / 2;

            $columns = null;
            $content = null;
            $module_html = null;

            switch ($type) {
                case "columns":

                    /*
                                If marked columns and the array of data is null, the zone isn't used.
                            */
                    $columns = json_decode($zone->columns);
                    if (!$columns) {
                        continue 2;
                    }

                    break;
                case "module":

                    ob_start();

                    echo "<style>";
                    echo ".zone_module_title_bar {";
                    if ($zone->module_title_bar_padding_type == "pixel") {
                        echo "padding: " . $zone->module_title_bar_padding_pixel . "px;";
                    } else {
                        echo "padding: " . $zone->module_title_bar_padding_percent . "%;";
                    }

                    echo "font-size: " . $zone->module_title_bar_font_size . "px;";
                    if ($zone->module_title_bar_text_color) {
                        echo "color: " . $zone->module_title_bar_text_color . ";";
                    }

                    if ($zone->module_title_bar_color) {
                        echo "background-color: " . $zone->module_title_bar_color . ";";
                    }


                    echo "}";
                    echo ".zone_module {";
                    if ($zone->modules_margin_type == "pixel") {
                        echo "margin: " . $zone->module_margin_pixel . "px;";
                    } else {
                        echo "margin: " . $zone->module_margin_percent . "%;";
                    }

                    if ($modules->padding_type == "pixel") {
                        echo "padding: " . $zone->module_padding_pixel . "px;";
                    } else {
                        echo "padding: " . $zone->module_padding_percent . "%;";
                    }
                    echo "}";
                    echo "</style>";

                    $zone_modules = json_decode($zone->modules);

                    $loaded = array();
                    $getModuleById = function ($id) use (&$loaded) {
                        if (isset($loaded[$id])) {
                            return $loaded[$id];
                        }
                        $db = JFactory::getDBO();
                        $query = "SELECT * FROM joom_modules WHERE id = " . $db->quote($id) . " AND published = 1 LIMIT 1";

                        $db->setQuery($query);
                        $result = $db->loadObject();

                        $loaded[$id] = $result;

                        return $result;
                    }

        ?>
                    <div class="container-fluid">
                        <div class="row">
                            <?php

                            for ($i = 0; $i < count($zone_modules->module_id); $i++) {
                                if ($zone_modules->module_active[$i]) {

                                    if ($zone_modules->module_id[$i] == 'spacer') {
                                        //There is no ID, it's an empty spacer
                                        echo "<div class='col-lg-" . $zone_modules->module_width[$i] . "'></div>";
                                        continue;
                                    }

                                    $result  = $getModuleById($zone_modules->module_id[$i]);
                                    if ($result) {
                                        $module = JModuleHelper::getModule($result->module, $result->title);
                                        $attribs['style'] = 'xhtml';

                                        $style = "";

                                        if ($zone_modules->module_height[$i] > 0) {
                                            $style .= "overflow: hidden;";
                                        }

                                        if ($zone_modules->module_border[$i] == true) {
                                            $style .= "box-shadow: 0px 0px 3px 1px rgba(0,0,0,0.28);";
                                        }

                                        if ($zone_modules->module_font[$i] != "") {
                                            $style .= "font-family: " . $zone_modules->module_font[$i] . ";";
                                        }

                                        if ($style != "") {
                                            $style = "style='" . $style . "'";
                                        }

                                        if ($zone_modules->module_height[$i] == 0 || $zone_modules->module_height[$i] == '') {
                                            $height = "auto";
                                        } else {
                                            $height = $zone_modules->module_height[$i] . "px";
                                        }

                                        echo "
                                                    <style>

                                                        #zone_module_$i {
                                                            height: $height;
                                                        }

                                                        @media(min-width:768px) {
                                                            #zone_module_$i {
                                                                height: $height;
                                                            }111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
                                                        }

                                                    </style>";


                                        echo "<div class='col-lg-" . $zone_modules->module_width[$i] . "'>";
                                        echo "<div id='zone_module_" . $i . "' class='homepage_zone_module'" . $style . ">";
                                        if ($zone_modules->module_title_bar[$i] == true) {
                                            echo "<div id='zone_module_title_" . $i . "' class='zone_module_title_bar'>" . $zone_modules->module_title_bar_text[$i] . "</div>";
                                            //echo "HERE";
                                        }
                                        switch ($zone_modules->module_scroll_bars[$i]) {
                                            case "none":
                                                $overflow = "overflow: hidden;";
                                                break;
                                            case "x":
                                                $overflow = "overflow-x: scroll; overflow-y: hidden;";
                                                break;
                                            case "y":
                                                $overflow = "overflow-y: scroll; overflow-x: hidden;";
                                                break;
                                            case "both":
                                                $overflow = "overflow: scroll;";
                                                break;
                                            case "auto":
                                            default:
                                                $overflow = "overflow: auto;";
                                                break;
                                        }
                                        echo "<div style='$overflow height: $height;' class='zone_module_container'>";
                                        echo JModuleHelper::renderModule($module, $attribs);
                                        echo "</div>";
                                        echo "</div>";
                                        echo "</div>";
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>

                    <script>
                        /*$(".zone_module_container").each(
                                        function() {
                                            var parentContainer = this.parentNode;
                                            var titleBar = parentContainer.getElementsByClassName('zone_module_title_bar');
                                            var parentHeight = $(parentContainer).css("height");
                                            var titleHeight = $(titleBar).css("height");

                                            if (!titleHeight) {
                                                titleHeight = 0;
                                            }

                                            var divHeight = parseInt(parentHeight) - parseInt(titleHeight);
                                            if (divHeight < 0) {
                                                divHeight = 0;
                                            }

                                            divHeight = "" + divHeight + "px";


                                            $(this).css({
                                                "height":divHeight
                                            });
                                        }
                                    );*/
                    </script>

            <?php

                    $module_html = ob_get_clean();



                    break;
                case "free":
                    /*
                                If marked content and the content is empty, the zone isn't used.
                            */
                    $content = $zone->content;
                    if (empty($zone->content)) {
                        continue 2;
                    }

                    break;
                default:
                    break;
            }
            ?>
            <div id="zone_<?php echo $zone_count ?>" class="row zone_<?php echo $zone_count ?>">
                <div class="zone_<?php echo $zone_count ?>_overlay"></div>
                <?php if (strtolower($zone->label) != 'testimonials') { ?>
                    <div id="<?php echo strtolower($zone->label) ?>">
                    <?php } else {
                    echo "<div>";
                } ?>
                    <?php if ($zone->label != "") { ?>
                        <div class="title-section pad-top pad-bottom zone_<?php echo $zone_count ?>_title <?php if ($zone->title_shadowed == true) {
                                                                                                                echo 'text-shadow';
                                                                                                            } ?>">
                            <?php echo $zone->label ?>
                        </div>
                    <?php } ?>
                    <div style="width: <?php echo $zone_width ?>%; margin-left: <?php echo $zone_margin ?>%;">
                        <?php
                        if ($columns != null) {
                            echo '<div class="row">';
                            $grid_count = 0;
                            for ($i = 0; $i < count($columns->zone_image); $i++) {
                                if (($grid_count + $columns->zone_width[$i]) > 12) {
                                    echo '</div><div class="row">';
                                    $grid_count = 0;
                                }
                        ?>
                                <div class="zone_<?php echo $zone_count; ?>_column pad-top pad-bottom text-center col-sm-<?php echo $columns->zone_width[$i]; ?>">
                                    <?php
                                    if ($columns->zone_image[$i] != "") {

                                        switch ($columns->zone_style[$i]) {
                                            case 'rounded':
                                                $columns->zone_image_style[$i] = 'style="border-radius: 20px;"';
                                                break;
                                            case 'circle':
                                                $columns->zone_image_style[$i] = 'style="border-radius: 50%;"';
                                                break;
                                            default:
                                                $columns->zone_image_style[$i] = '';
                                                break;
                                        }

                                        if ($columns->zone_title_color[$i] == "") {
                                            $columns->zone_title_color[$i] = "#ffffff";
                                        }
                                        if ($columns->zone_text_color[$i] == "") {
                                            $columns->zone_text_color[$i] = "#ffffff";
                                        }

                                        $text_style = 'style="color: ' . $columns->zone_text_color[$i] . ';';
                                        if ($text_font != "") {
                                            $text_style .= 'font-family: ' . $columns->zone_text_font[$i] . ';';
                                        }
                                        $text_style .= '"';

                                        $title_style = 'style="color: ' . $columns->zone_title_color[$i] . ';';
                                        if ($title_font != "") {
                                            $title_style .= 'font-family: ' . $columns->zone_title_font[$i] . ';';
                                        }
                                        $title_style .= '"';
                                    ?>
                                        <?php if ($columns->zone_link[$i]) {
                                        ?>

                                            <a href="<?php echo $columns->zone_link[$i]; ?>">
                                                <img src="<?php echo $columns->zone_image[$i]; ?>" <?php echo AxsAccessibility::image($columns->zone_image[$i]); ?> <?php echo $image_style; ?> />
                                            </a>
                                        <?php } else { ?>
                                            <img src="<?php echo $columns->zone_image[$i]; ?>" <?php echo AxsAccessibility::image($columns->zone_image[$i]); ?> <?php echo $image_style; ?> />
                                        <?php } ?>
                                        <br />
                                        <div class="text-large text-center pad-bottom" <?php echo $title_style . '>' . $columns->zone_title[$i] ?></div>
                                            <span class="text-justify text-small" <?php echo $text_style; ?>>
                                                <?php echo $columns->zone_text[$i] ?>
                                            </span>
                                        <?php
                                    }
                                        ?>
                                        </div>
                                <?php
                                $grid_count += $columns->zone_width[$i];
                            }
                            echo '</div>';
                        }

                        if ($content != null) {
                            echo $content;
                        }

                        if ($module_html != null) {
                            echo $module_html;
                        }

                                ?>
                                </div>
                    </div>
                    </div>
                <?php
            }

            $move_testimonials = 'true';
            switch ($homepage->testimonials->pos) {
                case 0:
                    $move_testimonials = 'false';
                    break;
                default:
                    echo '<script>var zoneId="zone_' . $homepage->testimonials->pos . '";</script>';
            }

                ?>
                <script>
                    var testimonials = document.getElementById('testimonials_area');
                    if (<?php echo $move_testimonials ?>) {
                        var area = document.getElementById(zoneId);
                        jQuery(area).after(testimonials);
                    }

                    jQuery(testimonials).show();
                    jQuery('#testimonials-all').show();
                </script>
                <?php
                ?>



                <!-- Resource jQuery -->
                <script type="text/javascript">
                    jQuery(document).ready(
                        function($) {
                            $(".scroll").click(
                                function(event) {
                                    event.preventDefault();
                                    $('html,body').animate({
                                        scrollTop: $(this.hash).offset().top
                                    }, 1000);
                                }
                            );
                        }
                    );
                </script>

            <? } ?>

            <script src="<?php echo $this->baseurl ?>/js/slidefader.js"></script>

            <?php

            if ($javaScript->post) {
                echo "<script>";
                echo $javaScript->post;
                echo "</script>";
            }

            if ($css->post) {
                echo "<style>";
                echo $css->post;
                echo "</style>";
            }
            ?>
            <?php
            if ($this->countModules('pop-login')) {
            ?>
                <div style="display:none;">
                    <jdoc:include type="modules" name="pop-login" style="xhtml" />
                </div>
            <?php
            }

            if ($this->countModules('mediamodal')) {
            ?>
                <jdoc:include type="modules" name="mediamodal" style="xhtml" />
            <?php
            }
            ?>
            <?php
            $notifications = AxsNotifications::loadNotifications();
            if ($notifications) {
                echo $notifications;
            }
            ?>

            <?php
            $footer = $homepage->footer;
            ?>

            <section>
                <?php

                $style = "";
                $bg = "";
                $tc = "";
                $f = "";

                if ($footer->background_color != "") {
                    $bg = "background-color: " . $footer->background_color . ";";
                }

                if ($footer->text_color != "") {
                    $tc = "color: " . $footer->text_color . ";";

                ?>
                    <style>
                        .footer-links a {
                            color: <?php echo $footer->text_color ?>;
                        }
                    </style>
                <?php
                }

                if ($footer->font != "") {
                    $f = "font-family: " . $footer->font . ";";
                }


                if ($bg != "" || $tc != "" || $f != "") {
                    $style = "style='" . $bg . $tc . $f . "'";
                }
                ?>

                <?php
                if (isset($footer->display_type) && $footer->display_type == "custom") {
                    echo $footer->custom_html;
                } else {

                    echo "<style>";
                    echo ".footer-links a {";

                    if ($footer->text_size) {
                        echo "font-size: " . $footer->text_size . "px;";
                    }

                    if ($footer->text_color != "") {
                        echo "color: " . $footer->text_color . ";";
                    }
                    echo "}";


                    if ($footer->link_spacing_type == "pixel") {
                        $spacing = ($footer->link_spacing_pixel / 2) . "px;";
                    } else {
                        $spacing = ($footer->link_spacing_percent / 2) . "vw;";
                    }

                    echo ".extra-pad-top {";
                    echo "padding-top: " . $spacing;
                    echo "}";

                    echo ".extra-pad-bottom {";
                    echo "padding-bottom: " . $spacing;
                    echo "}";


                    echo "</style>";

                ?>
                    <div class="row grey-bg" <?php echo $style ?>>

                        <?php
                        $width = $footer->width;
                        $margin = (100 - $footer->width) / 2;
                        $style = "style='width: " . $width . "%; margin-left: " . $margin . "%;'";

                        $text = array();
                        $url = array();

                        if ($footer->membership_link) {
                            array_push($text, "Membership");
                            array_push($url, "/#membership");
                        }
                        if ($footer->benefits_link) {
                            array_push($text, "Benefits");
                            array_push($url, "/#benefits");
                        }
                        if ($footer->testimonials_link) {
                            array_push($text, "Testimonials");
                            array_push($url, "#testimonials");
                        }
                        if ($footer->faq_link) {
                            array_push($text, "FAQ");
                            array_push($url, "/index.php?option=com_axs&view=policy&type=faq");
                        }
                        if ($footer->legal_link) {
                            $legalText = array();
                            $legalUrl = array();

                            array_push($legalText, "Legal");
                            array_push($legalText, "Member Agreement");
                            array_push($legalText, "Terms of Use");
                            array_push($legalText, "Privacy Policy");

                            array_push($legalUrl, '#');
                            array_push($legalUrl, '/index.php?option=com_axs&view=policy&type=agreement');
                            array_push($legalUrl, '/index.php?option=com_axs&view=policy&type=terms');
                            array_push($legalUrl, '/index.php?option=com_axs&view=policy&type=privacy');

                            array_push($text, $legalText);
                            array_push($url, $legalUrl);
                        }
                        if ($footer->support_link) {
                            array_push($text, "Support");
                            array_push($url, "/contact");
                        }

                        $custom = $footer->custom_links;
                        $customLinkCount = is_countable($custom->link_active) ? count($custom->link_active) : 0;

                        for ($i = 0; $i < $customLinkCount; $i++) {
                            if ($custom->link_active[$i] == "1") {
                                $link_text = $custom->link_text[$i];
                                $link_url = $custom->link_url[$i];

                                if ($link_text != "" && $link_url != "") {
                                    array_push($text, $link_text);
                                    array_push($url, $link_url);
                                }
                            }
                        }

                        $numLinks = count($url);

                        if ($footer->links_per_row > 0) {
                            $maxPerRow = $footer->links_per_row;
                        } else {
                            $maxPerRow = $numLinks;
                        }
                        $numRows = $maxPerRow > 0 ? ceil($numLinks / $maxPerRow) : 0;
                        $containersPerRow = ceil($maxPerRow / 12);
                        $columnWidth = $maxPerRow > 0 ? floor(12 / $maxPerRow) : 0;
                        if ($columnWidth < 1) {
                            $columnWidth = 1;
                        }

                        $count = 0;

                        echo "<div class='container-fluid' " . $style . ">";
                        for ($i = 0; $i < $numRows; $i++) {
                            for ($j = 0; $j < $containersPerRow; $j++) {
                                echo "<div class='col-md-12 footer-links extra-pad-top extra-pad-bottom'>";

                                for ($k = 0; $k < $maxPerRow; $k++) {
                                    echo "<div class='col-md-" . $columnWidth . "'>";

                                    $addLinks = false;

                                    if (!is_array($url[$count])) {
                                        $linkUrl = $url[$count];
                                        $linkText = $text[$count];
                                    } else {
                                        $addLinks = true;
                                        $linkUrl = $url[$count][0];
                                        $linkText = $text[$count][0];
                                    }

                                    $href = "<a href='" . $linkUrl . "'";
                                    if ($linkUrl[1] == "#") {
                                        $href .= " class='scroll'";
                                    }
                                    $href .= ">" . $linkText . "</a>";
                                    echo $href;

                                    if ($addLinks) {

                                        $subUrl = $url[$count];
                                        $subText = $text[$count];

                                        $style = "style='font-size:";
                                        if ($footer->subtext_size) {
                                            $style .= $footer->subtext_size . "px;'";
                                        } else {
                                            $style .= "12px;'";
                                        }

                                        echo "<ul>";
                                        for ($s = 1; $s < count($subUrl); $s++) {
                                            echo "<li>";
                                            echo "<a href = '" . $subUrl[$s] . "' " . $style . ">" . $subText[$s] . "</a>";
                                            echo "</li>";
                                        }
                                        echo "</ul>";
                                    }

                                    echo "</div>";
                                    $count++;
                                }
                                echo "</div>";
                            }
                        }

                        echo "<div class='col-sm-12 extra-pad-top footer-sublinks'>";
                        echo "<div class='col-sm-12 text-center'>";
                        if ($footer->copyright != "") {
                            $foot = $footer->copyright;
                            $foot = str_replace('[COPY]', '&copy;', $foot);
                            $foot = str_replace('[REG]', '&reg;', $foot);

                            echo $foot;
                        } else {
                            echo $brand->site_title;
                            echo " &copy; ";
                            echo date('Y') . ' ' . Jtext::_('TPL_MWM_GIN_COPYRIGHT');
                        }

                        echo "</div>";
                        echo "</div>";
                        echo "</div>";

                        ?>

                    </div>
                <?php
                }
                ?>

                <?php
                if ($social_links && $homepage->footer->show_socialmedia) {

                    switch ($homepage->footer->socialmedia_orientation) {
                        case "left":
                            $social_link_float = "pull-left";
                            $social_text_float = "pull-right";
                            break;
                        default:
                            $social_link_float = "pull-right";
                            $social_text_float = "pull-left";
                            break;
                    }
                ?>

                    <div class="social-bar social-bar-bottom hidden-xs">
                        <?php
                        if ($social_links->socialmedia_text) {
                        ?>
                            <div class="<?php echo $social_text_float; ?> social-links-text social-links-text-bottom">
                                <?php echo $social_links->socialmedia_text; ?>
                            </div>
                        <?php
                        }

                        switch ($homepage->footer->socialmedia_icon_style) {
                            case "square":
                                $icon_style = "-square";
                                break;
                            case "standard":
                            default:
                                $icon_style = "";
                        }
                        ?>

                        <div class="<?php echo $social_link_float; ?> social-links social-links-bottom">
                            <?php if ($social_links->facebook_link) { ?>
                                <a href="<?php echo $social_links->facebook_link; ?>" target="_blank" class="fa-brands fa-facebook<?php echo $icon_style; ?>" title="facebook" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->twitter_link) { ?>
                                <a href="<?php echo $social_links->twitter_link; ?>" target="_blank" class="fa-brands fa-twitter<?php echo $icon_style; ?>" title="twitter" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->youtube_link) { ?>
                                <a href="<?php echo $social_links->youtube_link; ?>" target="_blank" class="fa-brands fa-youtube<?php echo $icon_style; ?>" title="youtube" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->linkedin_link) { ?>
                                <a href="<?php echo $social_links->linkedin_link; ?>" target="_blank" class="fa-brands fa-linkedin<?php echo $icon_style; ?>" title="linkedin" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->instagram_link) { ?>
                                <a href="<?php echo $social_links->instagram_link; ?>" target="_blank" class="fa-brands fa-instagram" title="instagram" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->pinterest_link) { ?>
                                <a href="<?php echo $social_links->pinterest_link; ?>" target="_blank" class="fa-brands fa-pinterest<?php echo $icon_style; ?>" title="pinterest" rel="noopener"></a>
                            <?php } ?>

                            <?php if ($social_links->google_plus_link) { ?>
                                <a href="<?php echo $social_links->google_plus_link; ?>" target="_blank" class="fa-brands fa-google-plus<?php echo $icon_style; ?>" title="google" rel="noopener"></a>
                            <?php } ?>
                        </div>
                    </div>
                <?php
                }
                ?>


            </section>
</body>

</html>