<?php

if(JDEBUG) {


defined('_JEXEC') or die;

/** @var JDocumentError $this */

if (!isset($this->error))
{
	$this->error = JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
	$this->debug = false;
}

$siteUrl = JURI::root();

$status = $this->_error->getCode();

if ($status == '404') {
    include_once '404.php';
    exit;
} elseif ($status >= '500' AND $status <= '599') {
    include_once '500.php';
    exit;
}