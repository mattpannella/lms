function toggleSideMenu() {
    var vis = $("#side-menu-nav").attr("show");
    if (vis == "true") {
        $("#side-menu-nav").attr("show", "false");
        $("#side-menu-nav").css({                   
            "left": "-90vw"
        });
    } else {
        $("#side-menu-nav").attr("show", "true");
        $("#side-menu-nav").css({
            "left": "0"
        });
    }
        
}

$("#menu-open-button").click(function() {
   toggleSideMenu();
});

 $("#menu-open-button").keypress(function(e){
    if(e.which == 13 || e.which == 32){
        toggleSideMenu();
    }
});