$('.linkButton').on('click', function(){
    var textarea = document.createElement("textarea");
    textarea.classList.add("linkText");
    textarea.textContent = ssoDeepLinkUrl;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand('copy');
    document.body.removeChild(textarea);
    $('.copied').fadeIn(700);
    setTimeout(()=>{
        $('.copied').fadeOut(700);
    },3000)
})
