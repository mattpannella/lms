var items = null;
var nav_bar = $('.cd-primary-nav');
var logo = $('.header-logo-container');
var menu_list = null;

$(document).ready(function() {

    var nav_bar_resize = function() {
        $(".menu-item").hide();
        $('.cd-primary-nav').show();
        var nav_bar_icon = $('.cd-primary-nav').css("background-image");

        //The nav bar is a menu list.  Otherwise, it would be a dropdown icon
        if (nav_bar_icon == "none") {
                            

            var logo_left = $('.header-logo-container').offset().left;
            var logo_right = $('.header-logo-container').outerWidth() + logo_left;

            var nav_left = $('.cd-primary-nav').offset().left;
            var nav_right = $('.cd-primary-nav').outerWidth() + nav_left;

            var more_dropdown = document.getElementById("more_dropdown");
            var show_more_dropdown = false;

            var more_width = $("#more_container").width();
            more_width += parseInt($("#more_container").css("margin-left"));
            more_width += parseInt($("#more_container").css("margin-right"));

           // var available_width = nav_right - logo_right - more_width + more_width_offset;
            //console.log('Header: '+ $('.header-content').width());
            //console.log('nav: '+ $('.cd-primary-nav').width());
            //console.log('logo: '+ $('.header-logo-container').width());

            if (logoAbove) {
                var available_width = $('.header-content').width() - more_width_offset;
            } else {
                var available_width = $('.header-content').width() - $('.header-logo-container').width() - more_width_offset;
            }

            var cur_width = 0;

            menu_list.className = "";

            var more_menu_items = document.getElementsByClassName("menu-item");
            var widest = 0;

            $(items).each(
                function() {

                    var left = $(this).css("margin-left");
                    var right = $(this).css("margin-right");
                    var node_width = $(this).width() + parseInt(left) + parseInt(right);
                    
                    cur_width += node_width;

                    
                    if (node_width > widest) {
                        widest = node_width;
                    }
                    //console.log(cur_width +' '+ available_width);
                    if (cur_width >= available_width) {
                        //console.log(this);
                        //console.log(cur_width +' '+ available_width);
                        if (this.id != "more_container") {
                            var menu_id = this.getAttribute("menu-id");
                            
                            this.setAttribute("style","display:none");
                            
                            show_more_dropdown = true;
                            
                            $(more_menu_items).each(
                                function() {
                                    var this_id = this.getAttribute("menu-id");
                                    if (this_id == menu_id) {
                                        $(this).show();
                                    }
                                }
                            );
                        }
                    } else {
                        this.setAttribute("style","");
                    }
                }
            );

            //Reposition the dropdown menu
            more_container = document.getElementById("more_container");
            var more_container_left = $(more_container).offset().left;
            var checkLeftInterval = null;

            var setOffset = function() {

                var more_menu_width = $(more_dropdown).width();
                var more_container_left = $(more_container).offset().left;
                var screen_width = $(window).width();

                var offset = screen_width - more_container_left - more_menu_width;
                if (offset < 0) {
                    $(more_dropdown).css("left", offset + "px");
                }
            }

            if (more_container_left == 0) {
                checkLeftInterval = setInterval(
                    function() {
                        more_container_left = $(more_container).offset().left;
                        if (more_container_left != 0) {
                            clearInterval(checkLeftInterval);
                            setOffset();
                        }
                    }, 200
                );
            } else {
                setOffset();
            }
                
            if (show_more_dropdown) {
                $(more_container).show();
            } else {
                $(more_container).hide();
            }
        } else {
            $(items).each(
                function() {
                    $(this).show();
                }
            );
        }
    }



    //nav_bar = document.getElementsByClassName("cd-primary-nav")[0];     //There's only one Nav Bar
    //logo = document.getElementById("cd-logo");

    if (logo == null) {
        //The left side is not the logo, but the language selector.
        logo = document.getElementById("lang_box");
    }

    var menu_container = $(nav_bar).children();
    menu_list = menu_container[0];                                  //The list containing the menu items

    var new_div = document.createElement("li");

    //Do not include the 'fa' class with font awesome as it will add unwanted styling.  Instead, just add the font-family to the css.
    new_div.className = "deeper parent fa fa-angle-double-down";
    //new_div.className = "deeper parent";
    //new_div.innerHTML = "More";

    new_div.id = "more_container";
    $(new_div).css({
        "width": new_div_width,          //Give the user some space to click on.
        "cursor":"pointer",
        "display":"none",
        "font-family":"FontAwesome"     
    });

    var more_container = document.createElement('ul');
    new_div.appendChild(more_container);
    more_container.className = "nav-child unstyled small";
    more_container.id = "more_dropdown";

    menu_list.appendChild(new_div);

    items = $(menu_list).children();
    var more_dropdown = document.getElementById("more_dropdown"); 

    for (var i = 0; i < items.length; i++) {
        var item = items[i];

        if (item.id == "more_container") {
            continue;
        }

        var new_div = document.createElement('div');
        new_div.innerHTML = item.innerHTML;

        item.setAttribute("menu-id", i);
        new_div.setAttribute("menu-id", i);
        new_div.className = "menu-item";
        $(new_div).hide();

        more_dropdown.appendChild(new_div);
    }

    nav_bar_resize();

    $(window).resize(nav_bar_resize);
});

