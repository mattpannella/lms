//var sidebarStatus;

window.addEventListener("load", function () {

    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    if($(window).width() < 575 && !$("#wrapper").hasClass("toggled")){
        $("#wrapper").addClass("toggled");

        $("#sidebar-wrapper").css({
            "width": "0px"
        });

        $("#page-content-wrapper").css({
            "margin-left": "0px"
        });
    }

    $("#uplift_slide_out_menu").click(
        function () {
            $("#slide_menu_items").toggle();
        }
    );

    var sidebarWrapper = document.getElementById("sidebar-wrapper");
    var pageContentWrapper = document.getElementById("page-content-wrapper");

    $('.sideToggle').click(function () {

        switch (sidebarStatus) {
            case "open":
                sidebarStatus = "closed";
                $("#sidebar-wrapper").css({
                    "width": "0px"
                });

                $("#page-content-wrapper").css({
                    "margin-left": "0px"
                });
                break;
            case "closed":
                sidebarStatus = "open";
                $("#sidebar-wrapper").css({
                    "width": sidebarWidth
                });

                $("#page-content-wrapper").css({
                    "margin-left": sidebarWidth
                });
                break;
        }
    });

    $(".sideToggle").keypress(function (e) {
        if (e.which == 13 || e.which == 32) {
            $('.sideToggle').trigger('click');
        }
    });

    $("#my-account").click(
        function () {
            var menu = $("#my-account-menu");
            menu.slideToggle(100);
        }
    );

    $('.submenu-parent a').click(function () {
        toggleSubmenu($(this).parent());
    });

    //onload persist last
    for (var key in localStorage) {
        var menuarray = key.split('_');
        if (menuarray[0] == 'menu') {
            if (localStorage[key] == '1') {
                //find the menu and show it's submenu
                $('.submenu-parent a .navLabel').each(function () {
                    if ($(this).text() == menuarray[1]) {
                        $(this).parent().parent().find('.submenu').show();
                    }
                });
            }
        }
    }
});

window.addEventListener('resize', function(){
    if($(window).width() < 575){
        $("#wrapper").addClass("toggled");
        $("#sidebar-wrapper").css({
            "width": "0px"
        });
        $("#page-content-wrapper").css({
            "margin-left": "0px"
        });
    }else{
        $("#wrapper").removeClass("toggled");
        $("#sidebar-wrapper").css({
            "width": "150px"
        });
        $("#page-content-wrapper").css({
            "margin-left": "150px"
        });
    }
});

function toggleSubmenu(selector) {
    selector.find('.submenu').slideToggle();
    var menu = selector.find('.navLabel').text();
    localStorage['menu_' + menu] = localStorage['menu_' + menu] == 1 ? 0 : 1;
}

function ChangeUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = {
            Title: title,
            Url: url
        };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}

var modules = [];

function modResize(parent, child) {
    var parentHeight = $('#' + parent).height();
    if (child) {
        $('#' + child).css('height', parentHeight, 'maxHeight', parentHeight);
        $('#' + child + " > .zone_module_container").css('height', (parentHeight - 55), 'maxHeight', (parentHeight - 55));
        $('#' + child).show();

        $('#' + parent).css('height', parentHeight, 'maxHeight', parentHeight);
        $('#' + parent + " > .zone_module_container").css('height', (parentHeight - 55), 'maxHeight', (parentHeight - 55));
        $('#' + parent).show();
    }

}

function resizeAllModules() {
    for (let i = 0; i < modules.length; i++) {
        modResize(modules[i].parent, modules[i].child);
    }
}


$(window).resize(function () {
    resizeAllModules();
});

$(document).on('click', '#menu-toggle', function () {
    setTimeout(function () {
        resizeAllModules();
    }, 700);
});

$(document).ready(function () {
    if (modules.length > 0) {
        resizeAllModules();
    }
});

/// custom javascript - bryan funk 3/13/22

document.addEventListener('DOMContentLoaded', function (event) {
    let hamburger = document.getElementById('my-account');
    // If JS is enabled, it will un-expand the hamburger
    hamburger.setAttribute('aria-expanded', 'false');
    hamburger.onclick = function () {
        if (this.getAttribute('aria-expanded') == 'false') {
            this.setAttribute('aria-expanded', 'true');
        } else {
            this.setAttribute('aria-expanded', 'false');
        }
    }
});

