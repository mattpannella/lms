<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;


$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$this->language  = $doc->language;
$this->direction = $doc->direction;
$brand = AxsBrands::getBrand();
// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css');

// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);
$fonts = AxsFonts::getFonts($brand->id);


?>
<meta charset="UTF-8">
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<?php
    foreach($fonts as $font) {
        if($font->link) {
            echo $font->link."\n";
        } else {
            echo "<link href='//fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>"."\n";
        }

    }
?>
<jdoc:include type="head" />
<link href="/administrator/templates/isis3/css/template.css?69223b1a3b3ee3e2d50524fefb27828f" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/axs/icons/style.css" type="text/css" />
</head>
<body class="contentpane">
	<jdoc:include type="message" />
	<jdoc:include type="component" />
</body>
</html>
