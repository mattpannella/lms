
function search(searchId,container) {
	var keyword = $('#'+searchId).val();
	if(keyword.length > 1 || keyword.length == 0) {
		$('.'+container).hide();
		$('.'+container).each(function(){
			var title = $(this).find('.tld-team-username').text();
			if( title.toLowerCase().search(keyword.toLowerCase()) > -1 ) {
				$(this).show();
			}
		});
	}
}

function filterDashboardDropdown() {
	var input, filter, a, i;
	input = document.getElementById("searchQuery");
	filter = input.value.toUpperCase();
	div = document.getElementById("learner-dropdown");
	a = div.getElementsByClassName("tld-dropdown-container");
	
	for (i = 0; i < a.length; i++) {
	  txtValue = a[i].textContent || a[i].innerText;
	  if (txtValue.toUpperCase().indexOf(filter) > -1) {
		a[i].style.display = "";
	  } else {
		a[i].style.display = "none";
	  }
	}
  }

function filterTeam(filter) {
	var members = $('.tld-team-member-container');

	members.sort(function(a,b) {
		switch(filter) {
			case "alphabeticalASC":
				return (a.dataset.name.toUpperCase()) > (b.dataset.name.toUpperCase()) ? 1 : -1;
			break;
			case "alphabeticalDESC":
				return (b.dataset.name.toUpperCase()) > (a.dataset.name.toUpperCase()) ? 1 : -1;
			break;
			case "coursesMost":
				return b.dataset.courses - a.dataset.courses;
			break;
			case "coursesLeast":
				return a.dataset.courses - b.dataset.courses;
			break;
			case "checklistMost":
				return b.dataset.checklists - a.dataset.checklists;
			break;
			case "checklistLeast":
				return a.dataset.checklists - b.dataset.checklists;
			break;
		}
	});

	$('.team-list').html(members);
}

function setDefaultTeam(id) {
	$.ajax({
		type: "POST",
		url : "/index.php?option=com_axs&task=teams.setDefault&format=raw",
		data: {
			team_id : id
		}
	}).done((response) => {
		var result = JSON.parse(response);
		if(result.status == 'success') {
			$('[id^="team_selector"].default_team').hide();
			$('[id^="team_selector"].set').show();

			$('#team_selector_' + id).hide();
			$('#team_selector_default_' + id).show();
			
		}
	})
}

$('.assignment-btn').click(function() {
	$('.tld-members-container').hide();
	$('.tld-assignment-container').show();
	$('.tov-ld-tab-button').removeClass('active');
	$(this).addClass('active');
});

$('.members-btn').click(function() {
	$('.tld-assignment-container').hide();
	$('.tld-members-container').show();
	$('.tov-ld-tab-button').removeClass('active');
	$(this).addClass('active');
})

/* $('#tld_search').keyup(function(){
	search('tld_search','member-list');
});

$('.tld_search').click(function(){
	search('tld_search','member-list');
}); */

$('#tld_search_assignment').keyup(function(){
	search('tld_search_assignment','assignment-list-item');
});

$('.tld_search_assignment').click(function(){
	search('tld_search_assignment','assignment-list-item');
});

$('.team_dropdown').click(function() {
	$('.tld-dropdown').toggle();
});

/* $('#team-filter').change(function() {
	var filter = $(this).val();
	filterTeam(filter);
}); */

$(document).on('click', '.tld-user-dd-toggle', function() {
	$(this).next('.tld-mobile-options').toggle();
});

$('.tld-set-default-button').click(function() {
	var id = $(this).data('id');
	setDefaultTeam(id);
});
