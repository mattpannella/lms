<?php

defined('_JEXEC') or die;

$user = JFactory::getUser();
$input = JFactory::getApplication()->input;
$params = $input->get('params','','BASE64');
$key = AxsKeys::getKey('lms');
$scormParams = AxsEncryption::decrypt(base64_decode($params),$key);

$clientId = AxsClients::getClientId();
$contentLibraryAccess = AxsDbAccess::getAccessLevels('bizlibrary_content_selection');

if(isset($_SESSION['current_language'])) {
    $language = $_SESSION['current_language'];
} else {
    $language = AxsLanguage::getCurrentLanguage()->get('tag');
}

if(!$language) {
    $language = 'en-GB';
}
$billingID = 'tovuti01';
if($contentLibraryAccess == 'production') {
    $billingID = 'tovuti02';
}

$scormParams->clientId     = $billingID;
$scormParams->student_id   = $clientId.'_'.$user->id;
$scormParams->student_name = $user->name;

$scormItem = AxsScormFactory::getScormItemById($scormParams->scorm_course_id);
$scormItemParams = json_decode($scormItem->params);

if($scormParams->lesson_id) {
    $lesson_id = $scormParams->lesson_id;
} else {
    $lesson_id = 0;
};

$isAICCContent = !empty(strstr($scormItemParams->version, "AICC"));

if($isAICCContent) {

    $hacpSessionPayload = new stdClass();

    $hacpSessionPayload->scormId = $scormParams->scorm_course_id;
    $hacpSessionPayload->userId = $user->id;
    $hacpSessionPayload->studentId = $scormParams->student_id;
    $hacpSessionPayload->courseId = $scormParams->course_id;
    $hacpSessionPayload->lessonId = $lesson_id;

    // Check to see if we have an existing session that didn't properly terminate, and use it if one exists
    $existingHACPSession = AxsLMS::retrieveAICCSession(null, $user->id, $scormParams->course_id, $scormParams->scorm_course_id);

    if(empty($existingHACPSession)) {

        // No existing HACP session exists, so create a new one
        $hacpSessionPayload->sessionId = strtolower('aicc_' . AxsLMS::createUniqueID());
        $sessionCreatedFlag = AxsLms::createAICCSession($hacpSessionPayload);
    } else {

        $hacpSessionPayload->sessionId = $existingHACPSession->session_id;
        $sessionCreatedFlag = AxsLms::updateAICCSession($hacpSessionPayload);
    }

    if(!$sessionCreatedFlag) {

        echo "There was an error loading your AICC content. Please contact support for assistance.";
        exit;
    }

    // We want to use the actual root URI of the current instance - automatically respects current protocol
    $aiccURL = urlencode(JUri::root() . '/media/scorm/php/aicc.php');

    $aiccParams = 'AICC_URL=' . $aiccURL . '&AICC_SID=' . $hacpSessionPayload->sessionId;
}

$scormData = base64_encode(json_encode($scormParams));

switch ($scormParams->scorm_type) {
    case 'bizlibrary':
        $launchURL = '/media/scorm/html/bizlibrary.html?lb=i&params='.$scormData;
        if(isset($_SERVER['HTTP_USER_AGENT'])) {
            if(strpos($_SERVER['HTTP_USER_AGENT'],'iPad') || strpos($_SERVER['HTTP_USER_AGENT'],'iPhone')) {
                $launchURL = '/media/scorm/html/bizlibrary.html?params='.$scormData;
            }
        }
        $version = '2004';
    break;

    case 'scormlibrary':
        /* Build the URL that will be accessed to render the AICC course content
         * This needs to be directed towards an external URL when one exists, or an internal one when it doesn't.
         */
        if($isAICCContent) {

            if($scormItemParams->external) {

                $url = $scormItemParams->launch_file . (!empty($aiccParams) ? '&' . $aiccParams : '');

                // Rewrite the destination URL to utilize a matching protocol schema
                $launchURL = preg_replace("/https?(:\/\/.*)/", "https$1", $url);

                if(empty($launchURL)) {

                    $launchURL = $url;
                }
            } else {

                $launchURL = $scormItemParams->fullPath . '?' . (!empty($aiccParams) ? '&' . $aiccParams : '');
            }
        } else {
            $launchURL = '/' . $scormItemParams->fullPath;
            //a locally installed scorm course, so we have access to the manifest to pull objective definitions from
            $data = AxsScormFactory::readManifest($scormParams->scorm_course_id);
            $item = AxsScormFactory::getFirstItem($data);
        }

        $version = $scormItemParams->version;
    break;

    default:
        $scormItem = AxsScormFactory::getScormItemById($scormParams->scorm_course_id);
        $scormItemParams = json_decode($scormItem->params);
        $launchURL = '/'.$scormItemParams->fullPath;
        $version = $scormItemParams->version;
    break;
}

$params                     = new stdClass();
$params->user_id            = $user->id;
$params->course_id          = $scormParams->course_id;
$params->lesson_id          = $lesson_id;
$params->scorm_content_id   = $scormParams->scorm_course_id;

$row = AxsScormFactory::getLearnerScormData($params);
if($row) {
	$rowData = json_decode($row->data);
}

$row = AxsScormFactory::getLearnerScormData($params);
if($row) {
	$rowData = json_decode($row->data);
}

$scorm2004 = true;
if($version == '1.2') {
    $api = 'API';
    $jsFile = 'scormAPI_1_2';
    $prefix = "LMS";
    $scorm2004 = false;
} elseif( strpos($version, '2004') || strpos($version, 'CAM 1.3') || strpos($version, 'cam 1.3')) {
    $api = 'API_1484_11';
    $jsFile = 'scormAPI_2004';
    $prefix = "";
} else {
    $api = 'API_1484_11';
    $jsFile = 'scormAPI_2004';
    $prefix = "";
}

$firstName = AxsUser::getFirstName($user->id);
$lastName = AxsUser::getLastName($user->id);
if($firstName && $lastName) {
    $name = $firstName.' '.$lastName;
} else {
    $name = $user->name;
}

if($scormItemParams->name_format == 'last_first') {
    $nameArray = explode(' ',$name);
    if(count($nameArray) < 2) {
        array_push($nameArray,'lastName');
    }
    $name = implode(',',array_reverse($nameArray));
}

?>
<html>
<head>
    <title>SCORM Player</title>
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script>
        /* var userAgent = window.navigator.userAgent;
        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i)) {
        } */
    <?php if($row->data) { ?>
        var data = <?php echo $row->data; ?>;
    <?php } elseif($scorm2004) { ?>
        var data = {
            "learner_name"     : "<?php echo $name; ?>",
            "learner_id"       : "<?php echo $scormParams->student_id; ?>",
            "entry"            : "ab_initio",
            <?php
            if(isset($item->objectives) && count($item->objectives) > 0) {
                print("\"objectives\": {");
                print("\"childArray\": [");
                foreach($item->objectives as $objective) {
                    print("{");
                    print("\"id\" : \"{$objective->objectiveid}\"");
                    print("},");
                }
                print("]}");
            }
            ?>
        };

    <?php } else { ?>
        var data = {
            "core" : {
                "student_id"       : "<?php echo $scormParams->student_id; ?>",
                "student_name"     : "<?php echo $name; ?>",
                "entry"            : "ab-initio",
                <?php
                if(isset($item->objectives) && count($item->objectives) > 0) {
                    print("\"objectives\": {");
                    print("\"childArray\": [");
                    foreach($item->objectives as $objective) {
                        print("{");
                        print("\"id\" : \"{$objective->objectiveid}\"");
                        print("},");
                    }
                    print("]}");
                }
                ?>
            }
        };
    <?php } ?>
        var learner_name     = "<?php echo $name; ?>";
        var learner_id       = "<?php echo $scormParams->student_id; ?>";
        var course_id        = <?php echo ($scormParams->course_id ? $scormParams->course_id : 0); ?>;
        var lesson_id        = <?php echo ($lesson_id ? $lesson_id : 0); ?>;
        var scorm_course_id  = "<?php echo $scormParams->scorm_course_id; ?>";
        var student_id       = "<?php echo $scormParams->student_id; ?>";
        var student_name     = "<?php echo $name; ?>";
        var language         = "<?php echo $language; ?>";
    </script>

    <script type="text/javascript" src="/media/scorm/js/tovuti_scorm/<?php echo $jsFile; ?>.js?v=11"></script>
    <script type="text/javascript" src="/media/scorm/js/tovuti_scorm/scormTracking.js?v=28"></script>
    
    <script>

        // If we're using AICC, we're going to need to poll for completion so we can make a completion check for UI update reasons
        var isAICC = "<?php echo $isAICCContent; ?>";

        // Check to see if we're using AICC
        if(isAICC) {

            initializeAICCWatchdog(course_id, lesson_id);
        }

        const scormAPI = window.<?php echo $api; ?>;

        scormAPI.loadFromJSON(data);
        scormAPI.on("<?php echo $prefix; ?>Initialize", function() {
            scormAPI.apiLogLevel = 1;
            var learnerData = scormAPI.cmi.toJSON();
            console.log('SCORM Initialized ' + JSON.stringify(learnerData));
        });
        let lastCommitTimestamp;
        let dequeueTimer;
        let commitQueue = [];
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }
        const dumpCommitQueue = () => {
            console.log('Dumping Commit Queue');
            console.log('Commit Queue Unfiltered: ');
            console.log(commitQueue);
            const hasIncompletes = commitQueue.find(el => el.completion_status == 'incomplete');
            const hasCompleteds = commitQueue.find(el => el.completion_status == 'completed');
            if (hasIncompletes && hasCompleteds) {
                commitQueue = commitQueue.filter(el => el.completion_status == 'completed');
                console.log('Commit Queue Was Filtered: ');
                console.log(commitQueue);
            }
            
            commitQueue.forEach(commit => {
                console.log('Dequeueing Commit');
                console.log(commit);
                saveData(commit);
                sleep(300);
            });
            commitQueue = [];
        }
        scormAPI.on("<?php echo $prefix; ?>Commit", function() {
            console.log('SCORM Commit');
            let currentCommitTimestamp = new Date();
            var learnerData = scormAPI.cmi.toJSON();
            if (!lastCommitTimestamp) {
                lastCommitTimestamp = currentCommitTimestamp;
            }
            console.log(currentCommitTimestamp);
            console.log(lastCommitTimestamp);
            console.log('currentCommitTimestamp - lastCommitTimestamp');
            console.log(currentCommitTimestamp.getTime() - lastCommitTimestamp.getTime());
            console.log('currentCommitTimestamp - lastCommitTimestamp < 1000');
            console.log(currentCommitTimestamp.getTime() - lastCommitTimestamp.getTime() < 1000);
            if (currentCommitTimestamp - lastCommitTimestamp < 1000) {
                clearTimeout(dequeueTimer);
                commitQueue.push(learnerData);
                dequeueTimer = setTimeout(dumpCommitQueue, 1000);
            } else {
                console.log(scormAPI);
                console.log(learnerData);
                saveData(learnerData);
                lastCommitTimestamp = currentCommitTimestamp;                
            }
            
        });
        scormAPI.on("<?php echo $prefix; ?>SetValue.cmi.suspend_data", function() {
            var learnerData = scormAPI.cmi.toJSON();
            console.log('SCORM SetValue');
            console.log(scormAPI);
            console.log(learnerData);
            saveData(learnerData);
        });
        scormAPI.on("LMSFinish", function() {
            var learnerData = scormAPI.cmi.toJSON();
            saveData(learnerData); //force a last commit before finishing
        });
        scormAPI.on("Terminate", function() {
            var learnerData = scormAPI.cmi.toJSON();
            saveData(learnerData); //force a last commit before finishing
        });

    </script>
    <?php if($scormParams->scorm_type == 'bizlibrary') { ?>
        <iframe
            style="width: 100%; overflow-y: auto; overflow-x:hidden; height: <?php echo $scormParams->height; ?>"
            frameborder="0"
            src="<?php echo $launchURL; ?>"
            name="scorm_course"
            id="scorm_course"
            allow="autoplay; fullscreen"
            allowfullscreen="allowfullscreen">
        </iframe>
    <?php } else { ?>
        <frameset frameborder="0" framespacing="0" border="0" rows="0,*" cols="*"  id="player">
            <frame src="/media/scorm/js/tovuti_scorm/scormAPI2004.js" name="API" noresize>
            <frame src="<?php echo $launchURL; ?>" name="scorm_course" id="scorm_course">
        </frameset>
    <?php } ?>
</html>