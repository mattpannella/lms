<?php

defined('_JEXEC') or die;

require_once JPATH_ROOT.'/components/com_interactivecontent/autoloader.php';

$input = JFactory::getApplication()->input;

$embed_id = $input->get('embed_id','','INT');
$tokenEncoded = $input->get('token','','BASE64');
$version_number = $input->get('version_number',null,'INT');
$content_user_id = $input->get('user',null,'INT');

$plugin = H5P_Plugin::get_instance();
$plugin->enqueue_styles_and_scripts();
$output = $plugin->shortcode($embed_id, $version_number, $content_user_id);
$user_id = JFactory::getUser()->id;

unset(
    $this->_scripts['/plugins/system/axsusers/axsusers.js'],
    //$this->_scripts['/media/jui/js/jquery.min.js'],
    $this->_scripts['/media/system/js/caption.js'],
    $this->_scripts['/media/jui/js/jquery-noconflict.js'],
    $this->_scripts['/media/jui/js/jquery-migrate.min.js']
);

if(empty($tokenEncoded)) {
    echo '<h3 style="text-align:center; mFargin-top:40px;"> You do not have access to this content. Please login to access.</h3>';
    exit;
} else {

    $key = AxsKeys::getKey('lms');
    $encryptedParams = base64_decode($tokenEncoded);
    $tokenParams = AxsEncryption::decrypt($encryptedParams, $key);
    $allowedTime = 3600;
    $timeDifference = time() - $tokenParams->timestamp;

    if(!$tokenParams->isAdmin) {
        if(
            $timeDifference > $allowedTime ||
            !$tokenParams->timestamp ||
            (
                empty($content_user_id) &&
                ($tokenParams->user_id != $user_id)
            ) ||
            (
                $tokenParams->interactive_id != $embed_id &&
                $content_user_id != $tokenParams->content_user_id
            ) ||
            (
                !is_null($version_number) && ($version_number != $tokenParams->version_number)
            )
        ) {
            echo '<h3 style="text-align:center; margin-top:40px;"> You do not have access to this content. Please login to access.</h3>';
            exit;
        }
    } else {
        $settings = $plugin->get_settings();
        $contentSettings = $settings['contents']['cid-'.$embed_id];

        // We want to make sure that if we are loading an archived content version, to allow backwards navigation.
        if(!is_null($version_number) || strpos($contentSettings['library'], 'QuestionSet')) {
            $filters = json_decode($contentSettings['jsonContent']);
            $filters->disableBackwardsNavigation = FALSE;

            $contentSettings['jsonContent'] = json_encode($filters);
        }

        $settings['contents']['cid-'.$embed_id] = $contentSettings;
        $plugin->set_settings($settings);
    }
}

?>

<!doctype html>
<html lang="en" class="h5p-iframe">
<head>
    <meta charset="utf-8">
    <title>Interactive Content</title>
    <jdoc:include type="head" />
    <style>
        .h5p-multichoice .h5p-answer.h5p-wrong .h5p-alternative-container, .h5p-multichoice .h5p-answer.h5p-wrong:hover .h5p-alternative-container, ul.h5p-sc-alternatives.h5p-sc-selected li.h5p-sc-alternative.h5p-sc-reveal-wrong.h5p-sc-selected, ul.h5p-sc-alternatives.h5p-sc-selected li.h5p-sc-alternative.h5p-sc-reveal-wrong.h5p-sc-selected:hover, ul.h5p-sc-alternatives.h5p-sc-selected li.h5p-sc-alternative.h5p-sc-reveal-wrong.h5p-sc-selected:active, ul.h5p-sc-alternatives.h5p-sc-selected li.h5p-sc-alternative.h5p-sc-reveal-wrong.h5p-sc-selected:focus, ul.h5p-sc-alternatives.h5p-sc-selected li.h5p-sc-alternative.h5p-sc-is-wrong.h5p-sc-selected.h5p-sc-reveal-wrong .h5p-sc-progressbar {
            background: red !important;
            color: white;

        }
        ul.h5p-sc-alternatives li.h5p-sc-alternative:hover, ul.h5p-sc-alternatives li.h5p-sc-alternative:focus {
            background: #ddd !important;
        }
        .h5p-advanced-text ul li ul li {
            list-style-type: square;
        }
        .h5p-essay .h5p-essay-solution-sample-text {
            display: block;
        }
    </style>
</head>
<body>
    <?php
        echo $output;
        $plugin->add_settings();
    ?>
</body>
</html>
