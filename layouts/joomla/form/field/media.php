<?php

/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

/**
 * Layout variables
 * -----------------
 * @var   string   $autocomplete    Autocomplete attribute for the field.
 * @var   boolean  $autofocus       Is autofocus enabled?
 * @var   string   $class           Classes for the input.
 * @var   string   $description     Description of the field.
 * @var   boolean  $disabled        Is this field disabled?
 * @var   string   $group           Group the field belongs to. <fields> section in form XML.
 * @var   boolean  $hidden          Is this field hidden in the form?
 * @var   string   $hint            Placeholder for the field.
 * @var   string   $id              DOM id of the field.
 * @var   string   $label           Label of the field.
 * @var   string   $labelclass      Classes to apply to the label.
 * @var   boolean  $multiple        Does this field support multiple values?
 * @var   string   $name            Name of the input field.
 * @var   string   $onchange        Onchange attribute for the field.
 * @var   string   $onclick         Onclick attribute for the field.
 * @var   string   $pattern         Pattern (Reg Ex) of value of the form field.
 * @var   boolean  $readonly        Is this field read only?
 * @var   boolean  $repeat          Allows extensions to duplicate elements.
 * @var   boolean  $required        Is this field required?
 * @var   integer  $size            Size attribute of the input.
 * @var   boolean  $spellcheck      Spellcheck state for the form field.
 * @var   string   $validate        Validation rules to apply.
 * @var   string   $value           Value attribute of the field.
 * @var   array    $checkedOptions  Options that will be set as checked.
 * @var   boolean  $hasValue        Has this field a value assigned?
 * @var   array    $options         Options available for this field.
 *
 * @var   string   $preview         The preview image relative path
 * @var   integer  $previewHeight   The image preview height
 * @var   integer  $previewWidth    The image preview width
 * @var   string   $asset           The asset text
 * @var   string   $authorField     The label text
 * @var   string   $folder          The folder text
 * @var   string   $link            The link text
 */

extract($displayData);

$document = JFactory::getDocument();

// The root dom element for our field
echo "<div class='input-group'>";

$src = '';
$imgElement = '';
$style = '';
$inputId = $id . '-' . uniqid();;

if ($value) {
	// render an <img> tag for it using the preview params
	$src = '';
	if (file_exists(JPATH_ROOT . '/' . $value)) {
		// $value is a uri of an image on our server
		$src = JUri::root() . $value;
	} else {
		// value is a url to an image from the internet
		$src = $value;
	}
	
	if ($previewWidth > 0) {
		$style .=  'max-width:' . $previewWidth . 'px;';
	}
	if ($previewHeight > 0) {
		$style .= 'max-height:' . $previewHeight . 'px;';
	}
	$imgElement = JHtml::_(
		'image', 
		$src, 
		JText::_('JLIB_FORM_MEDIA_PREVIEW_ALT'), 
		array(
			'id' => $inputId . '_preview',
			'class' => 'media-preview',
			'style' => $style,
		)
	);
} else {
	// render localized 'None Selected' message
	$imgElement = JText::_('JLIB_FORM_MEDIA_PREVIEW_EMPTY');
}

// eye icon preview button and tooltip
echo "
	<span 
		id='$inputId-img-tooltip'
		class='input-group-text bg-light'
		type='button' 
		class='btn btn-outline' 
		data-bs-html='true' 
		data-bs-toggle='popover'
		data-bs-placement='top' 
		data-bs-content='$imgElement'
	>
		<span class='icon-eye' aria-hidden='true'></span>
	</span>
";

// render text input with relative image path as
$inputAttrs = '';
if (!empty($class)) {
	$inputAttrs .= "class='form-control $class' "; 
} else {
	$inputAttrs .= "class='form-control' ";
}

if (!empty($size)) { 
	$inputAttrs .= "size='$size' "; 
}

if (!empty($onchange)) {
	$inputAttrs .= "onchange='$onchange'";
}

$sanitizedValue = htmlspecialchars($value, ENT_COMPAT, 'UTF-8');

$valueUserFriendly = "";
if (gettype(strpos($sanitizedValue, "http")) == 'boolean') {
	$valueUserFriendly = implode('/', array_slice(explode('/', $sanitizedValue), 2));
}
if (empty($valueUserFriendly)) {
	$valueUserFriendly = $sanitizedValue;
}

$modalId = 'updateImageModal';

$basePath = JUri::root();

$params =& JComponentHelper::getParams('com_media');
$imagesPath = $params->get('image_path') . '/';

$readonly = !empty($readonly) ? "readonly" : "";

// CAREFUL, The order of these input elements matters!
// When rendering in a FOF subform, the last input field is given the field value.
// We want the value of the hidden input to be the value of the FOF subform field,
// so it must be the last <input> element!
echo "
	<input 
		type='text' 
		name='$name-forDisplay' 
		id='$inputId-forDisplay' 
		value='$valueUserFriendly'
		data-bs-toggle='tooltip'
		title='$valueUserFriendly'
		style='margin-right:-2px;max-width:264px'
		data-basepath='$basePath'
		data-imagespath='$imagesPath'
		$inputAttrs
		$readonly
		/>	
	<input 
		type='hidden' 
		name='$name' 
		id='$inputId' 
		value='$sanitizedValue' 
		data-basepath='$basePath'
		data-imagespath='$imagesPath'
		shared-modal-id='#$modalId'
		data-media-field-input='true'
		$inputAttrs
	/>
	
";

// initializeMediaInput comes from /jui/js/media-modal-helper.js
$document->addScriptDeclaration("
	jQuery(() => initializeMediaInput('$inputId'));
");
$selectButtonTextLocalized = JText::_('JLIB_FORM_BUTTON_SELECT');
$deleteButtonTextLocalized = JText::_('JLIB_FORM_BUTTON_CLEAR');
$href = '';
if (!$readonly) {
	if ($link) {
		$href .= $link;
	} else {
		require_once JPATH_ADMINISTRATOR . '/components/com_jce/helpers/browser.php';
		$href .= WFBrowserHelper::getMediaFieldLink('', $this->mediatype);
	}
	$href .= "&amp;fieldid=$inputId&ampfolder=$folder";
}

$label = empty($label) ? 'Upload Image' : $label;

// Select and Delete Buttons
echo "
	<button
		id='$inputId-selectMediaFieldButton'
		name='$inputId-selectMediaFieldButton'
		title='$selectButtonTextLocalized' 
		shared-modal-id='#$modalId'
		class='btn btn-light btn-sm'
		type='button'
	>
		$selectButtonTextLocalized
	</button>
	<button
		id='$inputId-clearMediaFieldButton'
		title='$deleteButtonTextLocalized' 
		onclick='$deleteButtonOnclick'
		class='btn btn-light hover-red'
		type='button'
	>
		<span class='icon-remove'></span>
	</button>
</div>
";

echo JHtml::_(
	'bootstrap.renderModal', 
	$modalId, 
	array(
		'iframeUrl' => $href,
		'size' => 'lg',
		'sharedModalId' => true,
		'inputId' => $inputId,
	)
);

echo JHtml::_(
	'bootstrap.mediaModalHelper', 
	$modalId, 
	array(
		'iframeUrl' => $href,
		'size' => 'lg',
		'sharedModalId' => true,
		'inputId' => $inputId,
	)
);