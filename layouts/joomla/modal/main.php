<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

use Joomla\Utilities\ArrayHelper;

extract($displayData);

/**
 * Layout variables
 * ------------------
 * @param   string  $selector  Unique DOM identifier for the modal. CSS id without #
 * @param   array   $params    Modal parameters. Default supported parameters:
*                             - title        string   The modal title
*                             - backdrop     mixed    A boolean select if a modal-backdrop element should be included (default = true)
*                                            The string 'static' includes a backdrop which doesn't close the modal on click.
*                             - keyboard     boolean  Closes the modal when escape key is pressed (default = true)
*                             - closeButton  boolean  Display modal close button (default = true)
*                             - animation    boolean  Fade in from the top of the page (default = true)
* 							  - lazy         boolean  Lazy render modal iframe specified in url param (default = true)
*                             - iframeUrl    string   URL of a resource to be inserted as an <iframe> inside the modal body
*                             - iframeHeight string   height value to be passed to inline height style (default = ???)
* 							  - breakpoint   string   Defines modal dialog class .modal-fullscreen-[breakpoint]-down (default = sm)
* 							  - fullscreen   boolean  Defines whether the modal is fullscreen (default = false)
* 							  - centered     boolean  Vertically center the modal (default = true)
* 							  - scrollable   boolean  Defines modal overflow type (default = false)
*                             - size         string   Defines modal size, options are: sm, lg, xl (default = md)
*                             - footer       string   Optional markup for the modal footer
 * @param   string  $body      Markup for the modal body. Appended after the <iframe> if the URL option is set
 *
 */

$modalClasses = array('modal');
if (!(isset($params['animation']) && $params['animation'] == false)) {
	$modalClasses[] = 'fade';
}

$modalAttributes = array(
	'id' => $selector,
	'tabindex' => '-1',
	'class'    => implode(' ', $modalClasses)
);
if (isset($params['backdrop'])) {
	if (is_bool($params['backdrop'])) {
		$modalAttributes['data-bs-backdrop'] = $params['backdrop'] ? 'true' : 'false';
	} else {
		$modalAttributes['data-bs-backdrop'] = $params['backdrop'];
	}
}

if (isset($params['keyboard'])) {
	if (is_bool($params['keyboard'])) {
		$modalAttributes['data-bs-keyboard'] = $params['keyboard'] ? 'true' : 'false';
	} else {
		$modalAttributes['data-bs-keyboard'] = $params['keyboard'];
	}
}

$modalDialogClasses = array();
if (isset($params['fullscreen'])) {
	$modalDialogClasses[] = "modal-fullscreen";
} else {
	if (isset($params['breakpoint'])) {
		$modalDialogClasses[] = "modal-fullscreen-" . $params['breakpoint'] . "-down";
	} else {
		if (isset($params['size'])) {
			switch ($params['size']) {
				case 'xl':
					$modalDialogClasses[] = "modal-fullscreen-lg-down";
				break;
				case 'lg':
					$modalDialogClasses[] = "modal-fullscreen-md-down";
				break;
				case 'sm':
					$modalDialogClasses[] = "modal-fullscreen-sm-down";
				break;
			} 
		} else {
			$modalDialogClasses[] = "modal-fullscreen-sm-down";
		}
	}
}

if (isset($params['size'])) {
	$modalDialogClasses[] = "modal-" . $params['size'];
}
if (isset($params['scrollable']) && $params['scrollable'] == true) {
	$modalDialogClasses[] = "modal-dialog-srollable";
}
if (!(isset($params['centered']) && $params['centered'] == false)) {
	$modalDialogClasses[] = "modal-dialog-centered";
}

if (isset($params['iframeUrl'])) {
	$iframeHtml = JLayoutHelper::render('joomla.modal.iframe', $displayData);

	$disableLazyRender = isset($params['lazy']) && $params['lazy'] == false;
	if ($disableLazyRender) {
		$displayData['body'] = $iframeHtml;
	} else {
		$script = "
			jQuery(() => {
				jQuery(document).on('show.bs.modal', '#" . $selector . "', function() {
					var modalBody = jQuery(this).find('.modal-body');
					modalBody.find('iframe').remove();
					modalBody.prepend('" . trim($iframeHtml) . "');
				})
			});
		";
		JFactory::getDocument()->addScriptDeclaration($script);
	}
}

?>
<div <?php echo ArrayHelper::toString($modalAttributes); ?> aria-hidden="true">
	<div class="modal-dialog <?php echo implode(" ", $modalDialogClasses); ?>"  >
		<div class="modal-content">
			<?php
				// Header
				if (!isset($params['closeButton']) || isset($params['title']) || $params['closeButton'])
				{
					echo JLayoutHelper::render('joomla.modal.header', $displayData);
				}

				// Body
				echo JLayoutHelper::render('joomla.modal.body', $displayData);

				// Footer
				if (isset($params['footer']))
				{
					echo JLayoutHelper::render('joomla.modal.footer', $displayData);
				}
			?>
		</div>
	</div>
</div>
