<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

use Joomla\Utilities\ArrayHelper;

extract($displayData);

/**
 * Layout variables
 * ------------------
 * @param   string  $selector  Unique DOM identifier for the modal. CSS id without #
 * @param   array   $params    Modal parameters. Default supported parameters:
 *                             - title        string   The modal title
 *                             - backdrop     mixed    A boolean select if a modal-backdrop element should be included (default = true)
 *                                                     The string 'static' includes a backdrop which doesn't close the modal on click.
 *                             - keyboard     boolean  Closes the modal when escape key is pressed (default = true)
 *                             - closeButton  boolean  Display modal close button (default = true)
 *                             - animation    boolean  Fade in from the top of the page (default = true)
 * 							   - lazy         boolean  Lazy render modal iframe specified in url param (default = true)
 *                             - iframeUrl    string   URL of a resource to be inserted as an <iframe> inside the modal body
 *                             - iframeHeight string   height value to be passed to inline height style (default = ???)
 * 							   - breakpoint   string   Defines modal dialog class .modal-fullscreen-[breakpoint]-down (default = sm)
 * 							   - fullscreen   boolean  Defines whether the modal is fullscreen (default = false)
 * 							   - centered     boolean  Vertically center the modal (default = true)
 * 							   - scrollable   boolean  Defines modal overflow type (default = false)
 *                             - size         string   Defines modal size, options are: sm, lg, xl (default = md)
 *                             - footer       string   Optional markup for the modal footer
 * @param   string  $body      Markup for the modal body. Appended after the <iframe> if the URL option is set
 *
 */

$iframeAttributes = array(
	'src'   => $params['iframeUrl']
);

if (isset($params['title'])) {
	$iframeAttributes['name'] = addslashes($params['title']);
} else {
	$iframeAttributes['name'] = $selector . '-iframe';
}

if (isset($params['iframeHeight'])) {
	$iframeAttributes['style'] = 'height:' . $params['iframeHeight'] . ';';
} else {
	if (isset($params['size'])) {
		switch ($params['size']) {
			case 'xl':
				$iframeAttributes['style'] = 'height:800px;';
			break;
			case 'lg':
				$iframeAttributes['style'] = 'height:700px;';
			break;
			case 'sm':
				$iframeAttributes['style'] = 'height:400px;';
			break;
			default:
				$iframeAttributes['style'] = 'height:200px;';
			break;
			
		} 
	}
}
$iframeAttributes['style'] .= "width:100%";

?>
<iframe <?php echo ArrayHelper::toString($iframeAttributes); ?>></iframe>
