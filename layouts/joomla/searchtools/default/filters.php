<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$data = $displayData;

// Load the form filters
$filters = $data['view']->filterForm->getGroup('filter');
?>
<?php if ($filters) : ?>
	<?php foreach ($filters as $fieldName => $field) : ?>
		<?php if ($fieldName !== 'filter_search') : ?>
			<?php $dataShowOn = ''; ?>
			<?php if ($field->showon) : ?>
				<?php 
					JHtml::_('jquery.framework');
					JHtml::_('script', 'jui/cms.js', array('version' => 'auto', 'relative' => true));
					$dataShowOn = " data-showon='" . json_encode(JFormHelper::parseShowOnConditions($field->showon, $field->formControl, $field->group)) . "'";
					$fieldId = 'id="' . $field->name . '-id"';
					JFactory::getDocument()->addScriptDeclaration("allShowOnFieldsIds.push($fieldId)");
				?>
			<?php endif; ?>
			<div <?php echo $fieldId; ?> class="js-stools-field-filter float-start "<?php echo $dataShowOn; ?>>
				<?php echo $field->input; ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>
