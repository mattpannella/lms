<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

$icon = empty($displayData['icon']) ? 'generic' : preg_replace('#\.[^ .]*$#', '', $displayData['icon']);
$starredId = $displayData['starredId'];
$iconClass = 'far fa-star';
if ($starredId) {
	$iconClass = 'fas fa-star';
	$dataAttr = 'data-starred-id="' . $starredId . '"';
}
?>
<i class="<?php echo $icon; ?> text-white" aria-hidden="true"></i>
<h1 id="pageTitle"><?php echo $displayData['title']; ?></h1>
<i id="starredIcon" style="cursor:pointer" class="<?php echo $iconClass ?>" <?php echo $dataAttr ?>></i>