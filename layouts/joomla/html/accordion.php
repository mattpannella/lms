<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

extract($displayData);

// Get some system objects.
$document = JFactory::getDocument();

/**
 * Layout variables
 * -----------------
 * @var   string   $types           An array() of menu item types, each of which has 1 or more menu items to choose from
 */

?>

<div class="accordion" id="menuTypesAccordion">
	<?php $i = 0 ?>
	<?php foreach ($types as $name => $list) : ?>
		<div class="accordion-item">
			<div id="heading-<?php echo $i ?>" class="accordion-header">
				<h2>
					<button 
						class="accordion-button collapsed" 
						type="button" 
						data-bs-toggle="collapse" 
						data-bs-target="#collapse-<?php echo $i ?>" 
						aria-expanded="true" 
						aria-controls="collapse-<?php echo $i ?>"
					>
						<?php echo $name ?>
					</button>
				</h2>
			</div>
			<div 
				id="collapse-<?php echo $i ?>" 
				class="accordion-collapse collapse" 
				aria-labelledby="heading-<?php echo $i ?>" 
				data-bs-parent="#menuTypesAccordion"
			>
				<div class="accordion-body">
					<ul class="nav flex-column">
						<?php foreach ($list as $title => $item) : ?>
							<li class="nav-item">
								<?php 
									$menutype = array(
										'id' => $this->recordId, 
										'title' => isset($item->type) ? $item->type : $item->title, 
										'request' => $item->request); 
									$menutype = base64_encode(json_encode($menutype));
								?>
								<a 
									class="choose_type nav-link"
									href="#" 
									title="<?php echo JText::_($item->description); ?>"
									onclick="setmenutype('<?php echo $menutype; ?>')"
								>
									<?php echo $title;?>
									<small class="muted">
										<?php echo JText::_($item->description); ?>
									</small>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>	
				</div>
			</div>
		</div>
		<?php $i += 1 ?>
	<?php endforeach; ?>
</div>

