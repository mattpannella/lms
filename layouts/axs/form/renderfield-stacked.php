<?php

defined('JPATH_BASE') or die;

extract($displayData);

/**
 * Layout variables
 * ---------------------
 * 	$options         : (array)  Optional parameters
 * 	$label           : (string) The html code for the label (not required if $options['hiddenLabel'] is true)
 * 	$input           : (string) The input field html code
 */

$class = empty($options['class']) ? '' : ' ' . $options['class'];
$rel   = empty($options['rel']) ? '' : ' ' . $options['rel'];
$show  = isset($options['show']) && $options['show'] == false ? 'display:none' : '';
$rowId = isset($options['rowId']) ? 'id="' . $options['rowId'] . '"' : '';

?>
<div <?php echo $rowId ?> class="row p-1 mb-4<?php echo $class; ?>" <?php echo $rel; ?> style="<?php echo $show; ?>;max-width:100%">
	<?php if (empty($options['hiddenLabel'])) : ?>
		<div class="col-lg-12 col-xl-12 mb-1"><?php echo $label; ?></div>
		<div class="col-lg-12 col-xl-12 mb-3"><?php echo $input; ?></div>
        <div class="col-lg-12 col-xl-12">
			<span class="text-muted"><?php echo $description?></span>
		</div>
	<?php else: ?>
		<div class="col-xl-12"><?php echo $input; ?></div>
	<?php endif; ?>
	
</div>
