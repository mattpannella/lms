#  tovuti-lms LAMP stack built with Docker Compose

A basic LAMP stack environment built using Docker Compose. It consists of the following:

* PHP 7.4.x
* Apache
* MySQL
* phpMyAdmin
* Memcached

## Before setup

* [Install docker](https://docs.docker.com/get-docker/)

##  Setup

1. Download hosted files (images, scripts, etc.) from https://drive.google.com/file/d/1EWg9OD9-3mw5n2knOprQeroZhWGTHlyX/view?usp=sharing then unzip and copy to `./data/shared` using the command:

```shell
tar -C data/shared -zxvf shared.tar.gz
```

2. Download database contents and grants from https://drive.google.com/drive/folders/16vzkCJ7YTV7GKkOmQSiMvg9qNbKLMXJV?usp=sharing and place all files in `./config/initdb`

3. Download fonts from https://drive.google.com/file/d/1EWg9OD9-3mw5n2knOprQeroZhWGTHlyX/view?usp=sharing and  extract to `./data/fonts` using the command:

```shell
tar -C data/fonts -zxvf fonts.tar.gz
```

4. Run `docker-compose up` from the directory that contains `docker-compose.yml`

```shell
cd tovuti-lms/docker/
docker-compose up
```

5. Open a browser and go to `localhost`
6. **Log in with credentials:** 
    - `username: admin` 
    - `password: admin`

## Xdebug Setup for VS Code

1. Copy `./config/php/xdebug.example.ini` to `./config/php/xdebug.ini`
2. Uncomment and update the line `xdebug.client_host=[your ip here]` with your ip
    - WSL/Linux -  `hostname -I | cut -d ' ' -f1` 
    - macOS -  `ipconfig getifaddr en0`
3. Copy `./config/vscode/launch.example.json` to `../.vscode/launch.json`. 
    - `/.vscode` should be in the root directory of the tovuti-lms repository, or whichever directory vs code has open currently

##  Configuration and Usage

### General Information 
This Docker Stack is build for local development and not for production usage.

### Configuration
This package comes with default configuration options. You can modify them by creating `.env` file in your root directory.
To make it easy, just copy the content from `sample.env` file and update the environment variable values as per your need.

### Configuration Variables
There are following configuration variables available and you can customize them by overwritting in your own `.env` file.

---
#### PHP
---
_**PHPVERSION**_
Is used to specify which PHP Version you want to use. Defaults always to latest PHP Version. 

_**PHP_INI**_
Define your custom `php.ini` modification to meet your requirments. 

---
#### Apache 
---

_**DOCUMENT_ROOT**_

It is a document root for Apache server. The default value for this is `./www`. All your sites will go here and will be synced automatically.

_**VHOSTS_DIR**_

This is for virtual hosts. The default value for this is `./config/vhosts`. You can place your virtual hosts conf files here.

> Make sure you add an entry to your system's `hosts` file for each virtual host.

_**APACHE_LOG_DIR**_

This will be used to store Apache logs. The default value for this is `./logs/apache2`.

---
#### Database
---

_**DATABASE**_
Define which MySQL or MariaDB Version you would like to use. 

_**MYSQL_DATA_DIR**_

This is MySQL data directory. The default value for this is `./data/mysql`. All your MySQL data files will be stored here.

_**MYSQL_LOG_DIR**_

This will be used to store Apache logs. The default value for this is `./logs/mysql`.


##### Loading Database ON startup

Any files placed under `.data/mysql-db` will be loaded in on startup. This works for mysqldumps, or user grants. 


## Web Server

Apache is configured to run on port 80. So, you can access it via `http://localhost`.

#### Apache Modules

By default following modules are enabled.

* rewrite
* headers

> If you want to enable more modules, just update `./bin/webserver/Dockerfile`. You can also generate a PR and we will merge if seems good for general purpose.
> You have to rebuild the docker image by running `docker-compose build` and restart the docker containers.

#### Connect via SSH

You can connect to web server using `docker-compose exec` command to perform various operation on it. Use below command to login to container via ssh.

```shell
docker-compose exec webserver bash
```

## PHP

The installed version of depends on your `.env`file. 

#### Extensions

By default following extensions are installed. 
May differ for PHP Verions <7.x.x

* mysqli
* pdo_sqlite
* pdo_mysql
* mbstring
* zip
* intl
* mcrypt
* curl
* json
* iconv
* xml
* xmlrpc
* gd

> If you want to install more extension, just update `./bin/webserver/Dockerfile`. You can also generate a PR and we will merge if it seems good for general purpose.
> You have to rebuild the docker image by running `docker-compose build` and restart the docker containers.

## phpMyAdmin

phpMyAdmin is configured to run on port 8080. Use following default credentials.

http://localhost:8080/  
username: root  
password: tiger

## Memcached

It comes with Memcached. It runs on default port `11211`.

# Getting Setup



## Links

### Webserver
http://localhost:80 

Login
Username: `admin`
Password: `admin`

### PHP My Admin
http://localhost:8080

### MySQL

username: `docker`
host: `127.0.0.1`
password: `docker`

