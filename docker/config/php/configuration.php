<?php

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("/var/www/files/creds.php");
require_once("libraries/axslibs/keys.php");
require_once("libraries/axslibs/clients.php");
require_once("libraries/axslibs/encryption.php");

class JConfig
{

    public function __construct()
    {
        $dbinfo = $this->getDB();
        $this->setDB($dbinfo);
    }

    private function getDB()
    {

        $res = null;
        $url = null;

        $creds = dbCreds::getCreds();
        if (isset($_SERVER['SERVER_NAME'])) {
            $url = $_SERVER['SERVER_NAME'];
        }
//        $url = "ha.tovuti.io";


        //check if the url is available
        if ($url) {

            $cookie_data = AxsClients::getCookieParams();

            if (isset($_COOKIE[$cookie_data->name])) {
                $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_data->name], $cookie_data->key);
                $this->checkDbManager($db_manager);
            } else {
                $db_manager = null;
            }


            if (!isset($db_manager->config)) {
                $conn = new mysqli(
                    $creds->dbhost,
                    $creds->dbuser,
                    $creds->dbpass,
                    $creds->dbname
                );
                $query = "SELECT * FROM `axs_dbmanager` WHERE (FIND_IN_SET('$url', domains) > 0) AND enabled=1;";
                $result = $conn->query($query);
                $res = $result->fetch_object();
            } else {
                $res = $db_manager->config;
            }
        }


        /*if (!is_object($res)) {
                $res = new stdClass();

                $res->dbname    = $creds->dbname;
                $res->dbuser    = $creds->dbuser;
                $res->dbpass    = $creds->dbpass;
                $res->dbprefix  = $creds->dbpref;
                $res->id                = null;
                $res->encode    = null;
        }*/
        if (!is_object($res)) {

            //include_once '/mnt/axs/developers/development/html/templates/axs/coming_soon.html';
            if (PHP_SAPI !== 'cli') {
                include_once '/var/www/html/templates/axs/404.php';
                exit;
            } else {
                $res = new stdClass();
                $res->dbname = $creds->dbname;
                $res->dbuser = $creds->dbuser;
                $res->dbpass = $creds->dbpass;
                $res->dbprefix = $creds->dbpref;
                $res->id = null;
                $res->encode = null;
            }

        }

        return $res;
    }

    public function setDB($dbinfo)
    {


        if (isset($dbinfo->dbparams)) {
            $params = AxsClients::decryptClientParams($dbinfo->dbparams);

               //     phpinfo();
               //     var_dump($params);
               //     die();

            $dbinfo->dbname = $params->dbname;
            //$dbinfo->dbuser = $params->dbuser;
            //$dbinfo->dbpass = $params->dbpass;
            $dbinfo->dbuser = "docker";
            $dbinfo->dbpass = "docker";
        }

        $this->db = $dbinfo->dbname;
        $this->user = $dbinfo->dbuser;
        $this->password = $dbinfo->dbpass;
        $this->dbprefix = $dbinfo->dbprefix;
        $this->id = $dbinfo->id;
        $this->encode = $dbinfo->encode;
    }

    public function checkDbManager(&$db_manager)
    {

        $mustHave = array();

        //These settings in the cookie must be set or the site will never load under the cookie is deleted.
        $mustHave = ["dbname", "dbuser", "dbpass", "dbprefix", "id", "encode"];

        $bad = false;
        //If there is no config, it's bad.
        if (!isset($db_manager->config)) {
            $bad = true;
        } else {
            foreach ($mustHave as $have) {
                //Check to see if the settings exist.
                if (!isset($db_manager->config->$have)) {
                    //If not, it's bad.
                    $bad = true;
                } else {
                    //Check to see that the setting has a value
                    if (!$db_manager->config->$have) {
                        //If not, it's bad.
                        $bad = true;
                    }
                }
            }
        }

        if ($bad) {
            $db_manager = null;
        }

    }

    //DB
    public $db;
    public $user;
    public $password;
    public $dbprefix;
    public $id;
    public $encode;
    public $dbtype = 'mysqli';
    public $host = 'database';

    //FTP
    public $ftp_host = '127.0.0.1';
    public $ftp_port = '21';
    public $ftp_user = '';
    public $ftp_pass = '';
    public $ftp_root = '';
    public $ftp_enable = '0';
    public $offset = 'America/Boise';

    //EMAIL
    public $mailer = 'smtp';
    public $mailfrom = 'noreply@tovutimail.com';
    public $fromname = 'Support';
    public $sendmail = '/usr/sbin/sendmail';
    public $smtpauth = '1';
    public $smtpuser = 'noreply@tovutimail.com';
    public $smtppass = 'axs123456';
    public $smtphost = 'mail.tovutimail.com';
    public $replyto = '';
    public $replytoname = '';
    public $smtpsecure = 'none';
    public $smtpport = '587';

    //SITE INFO
    public $sitename = '';
    public $offline = '0';
    public $offline_message = 'This site is down for maintenance.<br> Please check back again soon.';
    public $display_offline_message = '1';
    public $offline_image = '';
    public $editor = 'jce';
    public $captcha = 'recaptcha';
    public $list_limit = '10';
    public $access = '1';
    public $debug = '0';
    public $debug_lang = '0';
    public $live_site = '';
    public $secret = 'LRFcztuIxeOnCwvf';
    public $gzip = '0';
    public $error_reporting = 'none';
    public $helpurl = 'https://help.joomla.org/proxy/index.php?option=com_help&keyref=Help{major}{minor}:{keyref}';
    public $caching = '2';
    public $cache_handler = '';
    public $cachetime = '15';
    public $MetaDesc = '';
    public $MetaKeys = '';
    public $MetaTitle = '1';
    public $MetaAuthor = '0';
    public $MetaVersion = '0';
    public $robots = '';
    public $sef = '1';
    public $sef_rewrite = '1';
    public $sef_suffix = '0';
    public $unicodeslugs = '0';
    public $feed_limit = '10';
    public $log_path = '/mnt/axs/html/logs';
    public $tmp_path = '/mnt/axs/html/tmp';
    public $lifetime = '1500';
    public $session_handler = '';
    public $MetaRights = '';
    public $sitename_pagetitles = '0';
    public $force_ssl = '0';
    public $feed_email = 'author';
    public $cookie_domain = '';
    public $cookie_path = '';
    public $proxy_enable = '0';
    public $proxy_host = '';
    public $proxy_port = '';
    public $proxy_user = '';
    public $proxy_pass = '';
    public $mailonline = '1';
    public $frontediting = '0';
    public $asset_id = '1';
    public $memcache_persist = '1';
    public $memcache_compress = '0';
    public $memcache_server_host = '127.0.0.1';
    public $memcache_server_port = '11211';
    public $memcached_persist = '1';
    public $memcached_compress = '0';
    public $memcached_server_host = 'memcached';
    public $memcached_server_port = '11211';
    public $session_memcache_server_host = '127.0.0.1';
    public $session_memcache_server_port = '11211';
    public $session_memcached_server_host = 'memcached';
    public $session_memcached_server_port = '11211';
    public $redis_persist = '1';
    public $redis_server_host = 'redis';
    public $redis_server_port = '6379';
    public $redis_server_auth = '';
    public $redis_server_db = '0';
    public $massmailoff = '0';
    public $cache_platformprefix = '0';
    public $shared_session = '1';
    public $session_name = 'YGHA95qfghiy6Qef';
}