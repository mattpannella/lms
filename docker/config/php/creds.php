<?php

class dbCreds {
    public static $dbname = "root_db";
    public static $dbuser = "docker";
    public static $dbpass = 'docker';
    public static $dbhost = "database";
    public static $dbpref = "joom_";

    public static $dnsname = "us-test.tovuti.io";
    public static $log_dir = "var/log/tovuti";
    public static $email   = "letsencrypt-aaaadj7wspzwn6btvjf3xs5som@tovuti.slack.com";

    public static function getCreds() {
        $creds = new stdClass();
        $creds->dbname = self::$dbname;
        $creds->dbuser = self::$dbuser;
        $creds->dbpass = self::$dbpass;
        $creds->dbhost = self::$dbhost;
        $creds->dbpref = self::$dbpref;

        return $creds;
    }

    public static function getAwsConfig() {
        $config = new stdClass();
        $config->dnsname = self::$dnsname;
        $config->email = self::$email;
        $config->log_dir = self::$log_dir;

        return $config;
    }

}