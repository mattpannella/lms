<?php
class appConfig {
    public static $dbname = "site_security";
    public static $dbuser = "docker";
    public static $dbpass = "docker";
    public static $dbhost = "database";
    public static $dbpref = "joom_";

    public static $region = "us-west-2";
    public static $dnsname = "us-test.tovuti.io";
    public static $acm_alb_function = "arn:aws:lambda:us-west-2:656936593328:function:ALBCertAssoc-test";

    public static function getCreds() {
        $creds = new stdClass();
        $creds->dbname = self::$dbname;
        $creds->dbuser = self::$dbuser;
        $creds->dbpass = self::$dbpass;
        $creds->dbhost = self::$dbhost;
        $creds->dbpref = self::$dbpref;

        return $creds;
    }

    public static function getAwsConfig() {
        $config = new stdClass();
        $config->region = self::$region;
        $config->dnsname = self::$dnsname;
        $config->acm_alb_function = self::$acm_alb_function;


        return $config;
    }

}