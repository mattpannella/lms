<?php

class ModVideoplayerHelper {
    public static function getVideoData($view_type, $videoID) {

        switch ($view_type) {            
            case "featured":
                $column = 'featured_home';
                $int = 1;    
                break;

            case "single":
                $column = 'id';
                $int = $videoID;
                break;
            
            default:
                $column = "featured_home";
                $int = 1;
                return;
        }

        $db = getDatabase();
        $query = $db->getQuery(true);

        $conditions = $db->quoteName($column) . '=' . (int)$int;        
        
        $query
            ->select('*')
            ->from($db->quoteName('axs_gin_media'))
            ->where($conditions)
            ->order('id DESC')
            ->setLimit(1);

        $db->setQuery($query);
        $data = $db->loadObjectList();

        foreach($data as $row) {
            $brand = $row->brand;
            $videofile = $row->source;
            $external_source = $row->external_source;
            $cover_image = $row->cover_image;
        }

        return array($videofile, $external_source, $cover_image, $brand);
    }
}