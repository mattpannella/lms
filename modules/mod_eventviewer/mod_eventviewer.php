<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$module_settings = new stdClass();
$module_settings->event_category = $params->get("event_category");
$module_settings->num_to_show = $params->get("num_to_show");
$module_settings->blocksit_columns = $params->get("blocksit_columns");

if ($params->get("featured_events_only") == "yes") {
    $module_settings->featured_events_only = true;
} else {
    $module_settings->featured_events_only = false;
}

if ($params->get("show_previous") == "yes") {
    $module_settings->show_previous = true;
} else {
    $module_settings->show_previous = false;
}
if ($attribs['title_bar'] == true) {
    $module_settings->show_title_bar = true;
} else {
    $module_settings->show_title_bar = false;
}
if ($module->showtitle == "1") {
    $module_settings->showtitle = true;
} else {
    $module_settings->showtitle = false;
}
if ($params->get("load_javascript") == "yes") {
    $module_settings->load_javascript = true;
} else {
    $module_settings->load_javascript = false;
}



include "components/com_eventbooking/view/eventlist/tmpl/default.php";

/*
$db = JFactory::getDBO();
$query = "SELECT name FROM joom_eb_categories WHERE id = $event_category";
$db->setQuery($query);
$cat = $db->loadObject();
*/


?>






