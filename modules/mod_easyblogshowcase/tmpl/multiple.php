<?php
/**
* @package		EasyBlog
* @copyright	Copyright (C) 2010 - 2015 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasyBlog is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/


defined('_JEXEC') or die('Unauthorized Access');
//JLoader::register('JHtmlString', JPATH_LIBRARIES.'/joomla/html/html/string.php');
//defaults
$mod_unique = rand(1,10000);
$module_rows = 1;
$posts_per_row = 3;
$titleLimit = (int)$params->get('titlelimit');

if((int)$params->get('number_of_rows')) {
	$module_rows = (int)$params->get('number_of_rows');
}

if((int)$params->get('posts_per_row')) {
	$posts_per_row = (int)$params->get('posts_per_row');
}

switch ($posts_per_row) {
	case '1':
		$columns = 12;
	break;

	case '2':
		$columns = 6;
	break;

	case '3':
		$columns = 4;
	break;

	case '4':
		$columns = 3;
	break;

	case '6':
		$columns = 2;
	break;
	
	default:
		$columns = 4;
	break;
}

$postCount = count($posts);
$maxPostPerSection = $posts_per_row * $module_rows; 
$sectionsCount = ceil($postCount / $maxPostPerSection);
$sections = array();
$postsRemaining = $postCount;
$start = 0;
$end   = 0;

for($p = 0; $p < $sectionsCount ; $p++) {
	$section = new stdClass();
	$section->start = $start;	

	if($postsRemaining >= $maxPostPerSection) {
		$section->end = $end + $maxPostPerSection;
		$end = $section->end;
		$postsRemaining = $postsRemaining - $maxPostPerSection;	
	} else {
		$section->end = $end + $postsRemaining;
	}
	$start = $end;
	array_push($sections, $section);
}

?>

<style>

	div#fd.eb .eb-mod-thumb .eb-mod-image-cover:before {
	    content: '';
	    display: block;
	    position: absolute;
	    left: 0;
	    right: 0;
	    top: 0;
	    bottom: 0;
	    border: 0px;
    }

	div#fd.eb.eb-mod .eb-gallery-title2 {
	    margin: 0 !important;
	    font-size: 20px;
	    position: absolute;
	    left: 0px;
	    bottom: 0px;
	    color: #fff;
	    width: 100%;
	    text-align: left;
	    font-weight: bold;
	    padding: 15px;
	    line-height: 25px !important;
	    z-index: 10;
	    text-transform: uppercase;
	}

	.thumb-overlay {
		width: 100%;
		height: 100%;
		background: rgba(0,0,0,0.3);
		position: absolute;
		z-index: 5;
	}

	div#fd.eb .eb-mod-thumb .eb-mod-image-cover {
	    background-size: cover;
	    background-position: center;
	    background-repeat: no-repeat;
	    border: 0;
	    border-radius: 0;
	    display: inline-block;
	    height: 220px;
	    vertical-align: top;
	    position: relative;
	    opacity: 1;
	    filter: alpha(opacity=100);
	    -webkit-transform: scale(1);
	    -moz-transform: scale(1);
	    -o-transform: scale(1);
	    transform: scale(1);
	    position: static;
	    margin: 0 !important;
	}

	div#fd.eb .mod-avatar>img {
	    display: inline-block;
	    width: 40px;
	    height: 40px;
	    max-width: none;
	    max-height: none;
	    border: 2px solid #fff;
	    border-radius: 50%;
	    position: absolute;
	    left: 15px;
	    top: 25px;
	    z-index: 10;
	}

	.featured_item {
		position: relative;
		padding-left: 0px !important;
		padding-right: 20px !important;
	}

	.eb-gallery-thumb {
		position: relative;
	}

	div#fd.eb.eb-mod .eb-gallery-box {
    	padding: 10px 0px 0px 0px !important;
	}

	div#fd.eb .eb-mod-thumb {
	    width: 100%;
	}

	div#fd.eb.eb-mod .eb-gallery-viewport {
	    position: relative;
	    white-space: normal !important;
	    -webkit-transition: ease .5s;
	    transition: ease .5s;
	    left: 0;
	    direction: inherit !important;
	}

	@media (max-width: 992px) {
		.featured_item {
			padding-right: 0px !important;
		}
	}

	div#fd.eb.eb-mod .eb-gallery-body {
	    overflow: visible;
	}
	.eb-body .eb-gallery-content {
		font-size: 18px;
    	line-height: 22px !important;
	}
	.eb-body {
		color: #7B869B;
		padding: 15px 20px 20px 20px;		
		display: block;
		position: relative;
		background: #fff;
		margin-bottom: 20px;
	}

	.sectionHide {
		display: none;
	}

	.mod_pagination {
		text-align: center;
	}

	div#fd.eb.st-2 .eb-gallery-menu-item {
	    float: left;
	    margin: 0 10px 0 0;
	    width: 36px;
	    height: 36px;
	    line-height: 36px;
	    text-align: center;
	    border: 0px;
	    border-radius: 0;
	    color: #555;
	    cursor: pointer;
	}

	div#fd.eb.st-2 .eb-gallery-menu-item.active {
	    background: #00a8e6;
	    border: #00a8e6;
	    color: #fff;
	}

	div#fd.eb.st-2 .eb-gallery-foot {
	    padding: 10px;
	    border-top: 0px;
	    background: #fff;
	}

	div#fd.eb.eb-mod .eb-gallery-stage {	    
	    border: 10px;
	}

</style>

<?php if ($config->get('main_ratings')) { ?>

<script type="text/javascript">
EasyBlog.require()
.script('ratings')
.done(function($) {

    $('#fd.mod-easyblogshowcase [data-rating-form]').implement(EasyBlog.Controller.Ratings);
});

</script>
<?php }  ?>


<div id="fd" class="eb eb-mod st-2 eb-responsive mod-easyblogshowcase<?php echo $params->get('moduleclass_sfx'); ?> <?php echo 'mod_'.$mod_unique; ?>">
	<div class="eb-gallery" data-autoplay="<?php echo $autoplay;?>" data-interval="<?php echo $autoplayInterval;?>">
		<div class="eb-gallery-stage">
			<?php
				$s = 0;				
				foreach($sections as $section) {
					$s++;
					if($section->start > 0) {
						$sectionClass = 'style="display:none;"';
					} else {
						$sectionClass = '';						
					}
			?>
			<div class="eb-gallery-container" <?php echo $sectionClass; ?> section="<?php echo $s; ?>">
				<?php 
					for($i = $section->start; $i < $section->end; $i++) {
						$post = $posts[$i];

						if($titleLimit) {
							$blog_title = JHTML::_('string.truncate', ($post->title), $titleLimit);
						} else {
							$blog_title = $post->title;
						}
						
						$lastPostPadding = '';
						$clear = '';
						if ( ($i + 1) % $posts_per_row == 0 ) {
							$lastPostPadding = 'style="padding-right: 0px !important;"';
							$clear = '<div class="clearfix"></div>';
						}
				?>
				<div class="featured_item col-md-<?php echo $columns; ?>" <?php echo $lastPostPadding; ?>> 
					<div class="eb-gallery-box">
						<?php if ($params->get('authoravatar', true)) { ?>
								<a href="<?php echo $post->getAuthor()->getProfileLink(); ?>" class="eb-gallery-avatar mod-avatar">
									<img src="<?php echo $post->getAuthor()->getAvatar(); ?>" width="50" height="50" alt="author profile picture" />
								</a>
						<?php } ?>
						<?php if ($params->get('photo_show', true)) { ?>
							<?php if ($post->postCover) { ?>
							<div class="eb-gallery-thumb eb-mod-thumb">
								<?php if (isset($post->postCoverLayout->layout->crop) && $post->postCoverLayout->layout->crop) { ?>
						            <a href="<?php echo $post->getPermalink();?>" 
						            	class="eb-mod-image-cover"
						            	alt="<?php echo $post->title; ?>"  
						            	style="
						                	display: block;
						                    background-image: url('<?php echo $post->postCover;?>') !important;
						                    width: 100%;
						                    height: <?php echo $post->postCoverLayout->layout->height;?>px;"
						            >
						            <div class="thumb-overlay"></div>
								<?php } else { ?>
						            <a href="<?php echo $post->getPermalink();?>" class="eb-mod-image"
						                style="width:<?php echo $post->postCoverLayout->layout->width;?>px;">
						                <img src="<?php echo $post->postCover;?>" alt="<?php echo $post->title;?>" />
						            </a>
								<?php } ?>
								<div class="eb-gallery-title2"> 
									<?php echo $blog_title;?>
								</div>
								</a>
							</div>
							<?php } ?>
						<?php } ?>

						<div class="eb-body">					
							<div class="eb-gallery-meta">
								<?php if ($params->get('contentauthor', true)) { ?>
									<span>
										<a href="<?php echo $post->getAuthor()->getProfileLink(); ?>" class="eb-mod-media-title"><?php echo $post->getAuthor()->getName(); ?></a>
									</span>
								<?php } ?>

								

								<?php if ($params->get('contentdate' , true)) { ?>
									<span>
										<?php echo $post->getCreationDate()->format($params->get('dateformat', JText::_('DATE_FORMAT_LC3'))); ?>
									</span>
								<?php } ?>
							</div>

							<div class="eb-gallery-content">
								<?php echo $post->content; ?>
							</div>

							<?php if ($params->get('showratings', true)) { ?>
							    <div class="eb-rating">
							        <?php echo EB::ratings()->html($post, 'ebmostshowcase-' . $post->id . '-ratings', JText::_('MOD_SHOWCASE_RATE_BLOG_ENTRY'), $disabled); ?>
							    </div>
							<?php } ?>

							<?php if ($params->get('showreadmore', true)) { ?>
								<div class="eb-gallery-more">
									<a href="<?php echo $post->getPermalink();?>"><?php echo JText::_('MOD_SHOWCASE_READ_MORE');?></a>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
				<?php echo $clear; ?>
				<?php } ?> <!--PLEASE KEEP THIS DOM THIS WAY TO REMOVE WHITESPACING-->
			</div>
			<?php } ?>

			
		</div>
		<div class="eb-gallery-foot">
		<?php if ($sectionsCount > 1): ?>
			<div class="mod-table">
				<div class="mod_pagination">
					<div class="eb-gallery-menu">
						<span class="eb-gallery-button eb-gallery-nav-button" task="previous">
							<i class="fa fa-angle-left"></i>
						</span>
						<?php for($i = 0; $i < $sectionsCount; $i++) { ?>
							<span class="eb-gallery-menu-item <?php echo $i == 0 ? 'active' : '';?>" section="<?php echo $i+1; ?>">
								<span class="eb-gallery-menu-item-num">
									<?php echo $i+1 ?>
								</span>
							</span>
						<?php } ?>
						<span class="eb-gallery-button eb-gallery-nav-button" task="next">
							<i class="fa fa-angle-right"></i>
						</span>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	</div>

</div>

<script>
	var mod_unique = '<?php echo 'mod_'.$mod_unique; ?>';
	var maxPage = <?php echo $sectionsCount; ?>;

	function changeSection(section) {
		$('.'+mod_unique).find('.eb-gallery-container').hide();
	 	$('.'+mod_unique).find('.eb-gallery-container').each(function() {
	 		if(section == $(this).attr('section')) {
	 			$(this).fadeIn(700);
	 		}
	 	});
	}

	$('.eb-gallery-menu-item').click(function() {
	 	var section = $(this).attr('section');
	 	changeSection(section);
	});

	$('.eb-gallery-nav-button').click(function() {
		var page;
		var task = $(this).attr('task');		
	 	var section = $('.'+mod_unique).find('.active').attr('section');	 	

	 	if(task == 'next' && section < maxPage) {
	 		page = parseInt(section) + 1;
	 	}

	 	if(task == 'previous' && section > 1) {
	 		page = parseInt(section) - 1;
	 	}

	 	if(page) {
	 		$('.'+mod_unique).find('.eb-gallery-menu-item').removeClass('active');
		 	$('.'+mod_unique).find('.eb-gallery-menu-item').each(function() {
		 		if(page == $(this).attr('section')) {
		 			$(this).addClass('active');
		 		}
		 	});
	 		changeSection(page);
	 	}	 	

	});
</script>