<?php defined('_JEXEC') or die;

    //error_reporting(E_ALL);
    $brand = AxsBrands::getBrand();

    $cover = '/'.$brand->logos->video_watermark;
    $languageTag = AxsLanguage::getCurrentLanguage()->get('tag');
    $languageSEF = AxsLanguage::getSefByLangCode($languageTag);

?>

<style>
    .modal-dialog {
        color:      #333;
    }
</style>

<script>
    $('.ginAudioPopup').attr('href','javascript:void(0)');
    $('.popModule').attr('href','javascript:void(0)');

    $(document).on("click", ".ginAudioPopup" ,
        function() {
            var src = $(this).attr('alias');
            if ((src.substr(0,1) != '/') && (src.substr(0,4) != 'http')) {
                src = "/" + src;
            }

            var extension = src.substr( (src.lastIndexOf('.') +1) );
            var title = $(this).attr('title');
            extension = extension.toLowerCase();
            switch(extension) {
                case 'mp3':
                    var media = '<audio controls  autoplay style="width:100%;"><source src="'+src+'" type="audio/mpeg"></audio>';
                    break;

                case 'mp4':
                    var media = '<video controls  style="width:100%;" poster="<?php echo $cover; ?>"><source src="'+src+'" type="video/mp4"></video>';
                    break;

                default :
                    var media = '<div class="embed-responsive embed-responsive-16by9"><iframe width="100%"  class="embed-responsive-item" src="https://www.youtube.com/embed/'+src+'" frameborder="0" allowfullscreen></iframe></div>';
                    break;
            }

            var downloadLink = encodeURI(src);
            $('#mediaModalLabel').html(title);
            $('.modal-body').html(media);
            $('.downloadBTN').attr('href','/download.php?file=' + downloadLink);
            $("#mediaModal").modal();

        }
    );

    $(document).on("click", ".quizButton" ,
        function() {
            var src = $(this).attr('alias');
            var title = $(this).text();
            var height = $(this).attr('data-mediabox-height');
            if(!height) {
                height = 700;
            }
            var iframe = '<iframe src="'+src+'" frameborder="0" scrolling="auto" width="100%" height="'+height+'"><iframe>';
            $('#mediaModal').addClass('quizModal');
            $('#mediaModalLabel').html(title);
            $('.modal-body').html(iframe);
            $("#mediaModal").modal();
            $('.downloadBTN').hide();

        }
    );

    $(document).on("click", ".tovuti_popup" ,
            function(el) {
                el.preventDefault();
                if(!$(this).attr('id')) {
                    var title = $(this).attr('title');
                    var src = $(this).attr('href');
                    if(!src) {
                        var src = $(this).attr('link');
                    }
                    var iframe = '<iframe src="'+src+'" frameborder="0" scrolling="auto" style="height: 725px; width: 100%;"><iframe>';
                    $('#mediaModalLabel').html(title);
                    $('.modal-body').html(iframe);
                    $("#mediaModal").modal();
                    $('.downloadBTN').hide();
                }
            }
    );

    $(document).on("click", ".popModule" ,
        function() {
            var src = $(this).attr('title');
            $.ajax({
                url  : '/component/axs?task=popupmodule.loadModule&format=raw&lang=<?php echo $languageSEF; ?>',
                data : {
                    moduleID : src
                },
                type : 'post'
            }).done(function(result) {
                $('#mediaModalLabel').html('');
                $('#loginModal > .modal-dialog').html('');
                $('#loginModal > .modal-dialog').html(result);
                $("#loginModal").modal();
            });
        }
    );

    $(document).on("click", ".popLogin" ,
        function() {
            $('#mediaModalLabel').html('');
            $('.modal-dialog').html('');
            $("#mediaModal").modal();
        }
    );

    $(document).on('hidden.bs.modal','.quizModal',
        function() {
            $('#mediaModal').removeClass('quizModal');
            $('.modal-body').html('');
            parent.location.reload();
        }
    );

    $(document).on('hidden.bs.modal','#mediaModal',
        function() {
            $('.axsAudio').stop();
            $('.modal-body').html('');
        }
    );
</script>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    </div>
</div>

<div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closeModal" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="mediaModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">Loading...</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>