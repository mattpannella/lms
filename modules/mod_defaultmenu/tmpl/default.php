<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
// The menu class is deprecated. Use nav instead.

?>

<nav class="cd-primary-nav hidden-xs hidden-sm">
	<ul class="<?php echo $class_sfx;?>"
		<?php
			$tag = $params->get('tag_id');

			if ($tag) {
				echo " id='$tag'";
			}
		?>
	>

		<?php
			foreach ($list as $i => &$item) {
				$class = 'item-' . $item->id;

				if (($item->id == $active_id) || ($item->type == 'alias' && $item->params->get('aliasoptions') == $active_id)) {
					$class .= ' current';
				}

				if (in_array($item->id, $path)) {
					$class .= ' active';
				} elseif ($item->type == 'alias') {
					$aliasToId = $item->params->get('aliasoptions');

					if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
						$class .= ' active';
					} elseif (in_array($aliasToId, $path)) {
						$class .= ' alias-parent-active';
					}
				}

				if ($item->type == 'separator') {
					$class .= ' divider';
				}

				if ($item->deeper) {
					$class .= ' deeper';
				}

				if ($item->parent) {
					$class .= ' parent';
				}

				if (!empty($class)) {
					$class = ' class="' . trim($class) . '"';
				}

				echo "<li $class >";

				// Render the menu item.
				switch ($item->type) {
					case 'separator':
					case 'url':
					case 'component':
					case 'heading':
						require JModuleHelper::getLayoutPath('mod_defaultmenu', 'default_' . $item->type);
						break;
					default:
						require JModuleHelper::getLayoutPath('mod_defaultmenu', 'default_url');
						break;
				}

				// The next item is deeper.
				if ($item->deeper) {
					echo '<ul class="nav-child unstyled small">';
				} elseif ($item->shallower) {
					// The next item is shallower.
					echo '</li>';
					echo str_repeat('</ul></li>', $item->level_diff);
				} else {
					// The next item is on the same level.
					echo '</li>';
				}
			}
		?>
	</ul>
</nav>

<div id="menu-open-button" class="visible-xs visible-sm" role="button" aria-label="navigation collapisble menu button" aria-controls="navigation" tabindex="0">
	<span class="lizicon-menu" ></span>
</div>

<div id="side-menu-nav" class="visible-xs visible-sm" show="false" style="overflow-y: scroll;">

	<?php

		$depth = 0;
		foreach ($list as $i => &$item) {

			echo "<li style='padding-left: " . (($depth * 25) + 5) . "px;'>";

			// Render the menu item.
			switch ($item->type) {
				
				case 'url':
				case 'component':
					echo "<div class='side-menu-item side-menu-click-item'>";
						require JModuleHelper::getLayoutPath('mod_defaultmenu', 'default_' . $item->type);
					echo "</div>";
					break;

				case 'separator':
				case 'heading':
					echo "<div class='side-menu-item side-menu-heading'>";
						require JModuleHelper::getLayoutPath('mod_defaultmenu', 'default_' . $item->type);
					echo "</div>";
					break;
				default:
					echo "<div class='side-menu-item'>";
						require JModuleHelper::getLayoutPath('mod_defaultmenu', 'default_url');
					echo "</div>";
					break;
			}

			//echo $item->level_diff;


			// The next item is deeper.
			if ($item->deeper) {

				//echo '<ul class="nav-child unstyled small">';
				echo "<ul>";
				$depth++;
			} elseif ($item->shallower) {
				// The next item is shallower.
				echo '</li>';			
				echo str_repeat('</ul></li>', $item->level_diff);
				$depth -= $item->level_diff;
			} else {				
				// The next item is on the same level.
				echo '</li>';
			}
		}

	?>

</div>

