<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 3/23/16
 * Time: 10:40 AM
 */

defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

// The class name must always be the same as the filename (in camel case)
class JFormFieldMydatepicker extends JFormField {

    //The field class must know its own type through the variable $type.
    protected $type = 'mydatepicker';

    public function getLabel() {
        // code that returns HTML that will be shown as the label
        return parent::getLabel();
    }

    public function getInput() {
        // code that returns HTML that will be shown as the form field
        return '<input name="'. $this->name .'" id="'. $this->id .'" type="date" value="'. date('Y-m-d') .'" />';
    }
}