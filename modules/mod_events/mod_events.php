<?php
defined('_JEXEC') or die;

require_once(dirname(__FILE__) . '/helper.php');
// $module and $params contain stuff about this instance of the module.
$name = $module->title;
$events = json_decode($params->get('page_events', '{}'));

//because the joomla people decided to store this in an insane format, we have to turn it into a format we can use.
$properFormat = array();
//first grab how many rows of values were actually stored.
$loopCount = count(current($events));
//now loop through again and grab the values
for($i=0; $i<$loopCount; $i++) {
    $event = new stdClass();
    foreach($events as $key => $value) {
        $event->{$key} = $value[$i];
    }
    $properFormat[] = $event;
}

$events = $properFormat;

require JModuleHelper::getLayoutPath('mod_events'); //this must be placed at the bottom