<?php
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$document->addStyleSheet('modules/mod_events/libraries/slick/slick.css');
$document->addStyleSheet('modules/mod_events/libraries/slick/slick-theme.css');
$document->addStyleSheet('modules/mod_events/assets/css/event.css');

?>
<div class="<?php echo $name; ?> eventsDiv">
    <?php foreach($events as $event) {
        if((new DateTime())->createFromFormat('Y-m-d', $event->cutoff_date) > (new DateTime())) { ?>
            <div class="eventDiv">
                <div class="eventTopRow">
                    <p class="eventTitle"><?php echo $event->title; ?></p>
                </div>
                <div class="eventBottomRow">
                    <div class="eventLeftColumn">
                        <img data-lazy="<?php echo $event->media; ?>" height="120px" width="auto">
                        <div class="eventDateContainer">
                            <p class="eventDate"><?php echo $event->event_date; ?></p>
                        </div>
                    </div>
                    <div class="eventRightColumn">
                        <p class="eventLocation"><span class="lizicon lizicon-location"></span> <?php echo $event->location; ?></p>
                        <a href="https://axs.events/event_details.cfm?event_id=<?php echo $event->event_id; ?>" class="btn btn-primary" style="margin-bottom: 10px;" target="_blank"><?php echo Jtext::_('MOD_EVENTS_DETAILS'); ?></a>
                        <a href="https://axs.events/contestant-Reg.cfm?event_id=<?php echo $event->event_id; ?>" class="btn btn-primary" target="_blank"><?php echo Jtext::_('MOD_EVENTS_REGISTER'); ?></a>
                    </div>
                </div>
            </div>
        <?php }
    } ?>
</div>

<script src="modules/mod_events/libraries/slick/slick.min.js"></script>
<script>
    (function($) {
        $(function(){
            $('.<?php echo $name; ?>').slick({
                infinite: true,
                autoplay: true,
                lazyLoad: 'ondemand', //lazy load the images
                autoplaySpeed: 4000, //milliseconds
                dots: true,
                arrows: true,
                centerMode: true,
                centerPadding: '60px',
                slidesToScroll: 1,
                variableWidth: true,
                focusOnSelect: false,
                pauseOnHover: true,
                respondTo: 'slider'
            });
        });
    })(jQuery);
</script>