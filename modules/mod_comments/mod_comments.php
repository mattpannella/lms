<?php
defined('_JEXEC') or die;

require_once ('modules/mod_comments/helper.php');
require_once ('components/shared/controllers/common.php');

?>

	<link rel="stylesheet" type="text/css" href="modules/mod_comments/assets/css/comments.css">
	<script type="text/javascript" src="modules/mod_comments/assets/js/comments.js?v=5"></script>