<?php

class ModCommentsHelper {

	public static function getInitialCommentsAjax() {

        $list = JRequest::getVar('list');
        $return = new stdClass();
        $return->time = strtotime('now');
        $return->data = array();

        foreach ($list as $list_data) {

            $post_id = $list_data['post_id'];
            $current = $list_data['current'];
            $init_load = $list_data['init_load'];
            $load_more = $list_data['load_more'];

            $comments = self::getComments($post_id, null, $current, $init_load, $load_more);
            $comment_data = new stdClass();
            $comment_data->total = $comments->total;
            $comment_data->comments = array();

            foreach($comments->results as $comment) {
                $data = self::getCommentData($comment);
                array_push($comment_data->comments, $data);
            }

            array_push($return->data, $comment_data);
        }

        echo json_encode($return);
	}

    public static function loadMoreCommentsAjax() {
        $load_more = JRequest::getVar('load_more');
        $current_load = JRequest::getVar('current_load');
        $post_id = JRequest::getVar('post_id');



        $comments = self::getComments($post_id, null, $current_load, null, $load_more);

        $return = array();

        foreach ($comments->results as $comment) {
            $data = self::getCommentData($comment);
            array_push($return, $data);
        }

        echo json_encode($return);
    }

    public static function checkForNewCommentsAjax() {
        //$update_list = JRequest::getVar('list');
        $input = JFactory::getApplication()->input;
        $update_list = $input->get('list', '', 'ARRAY');

        $db = JFactory::getDBO();
        //$query = $db->getQuery(true);

        $query = "SELECT * FROM comments WHERE status = 'published' AND ";

        $current_id_list = [];
        foreach ($update_list as $post) {

            $post_id = preg_replace("/[^A-Za-z0-9_]/", '', $post['post_id']);
            
            $time = date("Y-m-d H:i:s", $post['last_load']);

            $id_array = implode(",", $db->quote($post['id_list']));
            if($id_array) {
                if (count($post['id_list']) > 0) {
                    //The post already has comments, check to see if they're updated.
                    foreach ($post['id_list'] as $id) {
                        //Push all of the ids into a single list to later check whether a post is new or updated.
                        array_push($current_id_list, $id);
                    }

                    $query .= "(".$db->quoteName('post_id')." = ".$db->quote($post_id)." AND ".$db->quoteName('id')." IN ($id_array) AND (".$db->quoteName('modified_date')." >= ".$db->quote($time)." OR ".$db->quoteName('deleted')." IS NOT NULL)) OR ";
                }
            }

            $query .= "(".$db->quoteName('post_id')." = ".$db->quote($post_id)." AND ".$db->quoteName('comment_date')." >= ".$db->quote($time).") OR";
        }

        $query = substr($query, 0, -3); //Remove the last " OR"
        //echo $query; die();
        $db->setQuery($query);
        $results = $db->loadObjectList();

        $deleted = array();     //All of the posts that have been deleted since last check.
        $updated = array();     //All of the posts that have been added or updated.
        $added = array();

        foreach ($results as $result) {
            $newData = new stdClass();
            $newData->comment_id = $result->id;
            $newData->post_id = $result->post_id;
            $newData->comment = $result->comment;

            if ($result->deleted != 0) {
                //Deleted posts would not have originally been pulled, so if it was pulled and now has a deleted date, it has been deleted.
                array_push($deleted, $newData);
            } else {
                //Check whether its ID is in the list of ids currently on the page.
                if (!in_array($result->id, $current_id_list)) {
                    //No = New Posts                    
                    $data = new stdClass();
                    $data->post_id = $result->post_id;
                    $data->post = self::getCommentData($result);
                    array_push($added, $data);
                } else {
                    //Yes = Existing post
                    array_push($updated, $newData);
                }
                
            }
        }

        $return = new stdClass();
        $return->time = strtotime('now');
        $return->deleted = $deleted;        
        $return->updated = $updated;
        $return->added = $added;
        
        /*  //For error checking
        $return->old_time = $time;
        $return->list = $current_id_list;
        $return->query = $query;
        $return->result = $result;
        */

        echo json_encode($return);
    }

    public static function postNewCommentAjax() {
        $user_id = JFactory::getUser()->id;
        if($user_id) {
            $db = JFactory::getDBO();

            $text = htmlentities(JRequest::getVar('text'), ENT_QUOTES);
            $post_id = JRequest::getVar('pid');

            $date = $date = date("Y-m-d H:i:s", strtotime('now'));
            
            
            $query = $db->getQuery(true);
            
            $columns = array(
                'post_id',
                'user_id', 
                'comment', 
                'comment_date',
                'enabled'    
            );
            
            $values = array(
                $db->quote($post_id),
                (int)$user_id, 
                $db->quote($text),
                $db->quote($date),
                $db->quote('1')
            );
            
            $query
                ->insert($db->quoteName('comments'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));
                
            $db->setQuery($query);
            $db->execute();

            $last_id = $db->insertid();

            $comment = self::getComments($post_id, $last_id);
            echo json_encode(self::getCommentData($comment->results));
        }
                
    }

    public static function editCommentAjax() {
        $user_id = JFactory::getUser()->id;
        if($user_id) { 
            $db = JFactory::getDBO();

            $text = JRequest::getVar('text');
            $comment_id = JRequest::getVar('cid');
            

            $query = $db->getQuery(true);

            $date = date("Y-m-d H:i:s", strtotime('now'));
            
            $fields = array(
                $db->quoteName('comment') . '=' . $db->quote($text),
                $db->quoteName('modified_date') . '=' . $db->quote($date)
            );
            
            $conditions = array(
                $db->quoteName('id') . '=' . $db->quote($comment_id),
                $db->quoteName('user_id') . '=' . $db->quote($user_id)
            );
            
            $query
                ->update($db->quoteName('comments'))
                ->set($fields)
                ->where($conditions);
            
            $db->setQuery($query);
            $result = $db->execute();
        }
        
        
    }

    public static function flagCommentAjax() {
        $user_id = JFactory::getUser()->id;
        if($user_id) { 
            $db = JFactory::getDBO();
        
            $comment_id = JRequest::getVar('cid');
            $user_id = JFactory::getUser()->id;
            
            $query = $db->getQuery(true);

            $date = strtotime('now');
            
            $columns = array(
                'comment_id', 
                'flagging_user_id', 
                'date'
            );
            
            $values = array(
                $db->quote($comment_id), 
                (int)$user_id,         
                (int)$date
            );
            
            $query
                ->insert($db->quoteName('comments_flagged'))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values)
            );
            
            $db->setQuery($query);
            $result = $db->execute();
        }

    }

    public static function deleteCommentAjax() {

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $comment_id = JRequest::getVar('cid');
        $user_id = JFactory::getUser()->id;

        $date = date("Y-m-d H:i:s", strtotime('now'));
        
        $fields = array(
            $db->quoteName('deleted') . '=' . $db->quote($date)
        );
        
        $conditions = array(
            $db->quoteName('id') . '=' . $db->quote($comment_id),
            $db->quoteName('user_id') . '=' . $db->quote($user_id)
        );
        
        $query
            ->update($db->quoteName('comments'))
            ->set($fields)
            ->where($conditions);

        $db->setQuery($query);

        $db->execute();
    }

    private static function getUserProfile($user_id) {
        if (!isset($_SESSION['comment_profiles'])) {
            $_SESSION['comment_profiles'] = array();
        }

        if (!isset($_SESSION['comment_profiles'][$user_id])) {
            //Store the user's profile in SESSION so that it doesn't have to be pulled every time.
            $_SESSION['comment_profiles'][$user_id] = AxsExtra::getUserProfileData($user_id);
        }

        return $_SESSION['comment_profiles'][$user_id];
    }

    private static function getCommentData($comment) {
        $data = new stdClass();

        $user_id = JFactory::getUser()->id;

        $profile = self::getUserProfile($comment->user_id);

        $data->profile = new stdClass();

        $data->profile->first = $profile->first;
        $data->profile->last = $profile->last;
        $data->profile->id = $comment->user_id;
        $data->profile->photo = $profile->photo;
        $data->profile->alias = $profile->alias;

        //Check that the data is not null.  If it's all null, the user has been deleted.
        $notNull = false;
        foreach($data->profile as $key => $info) {
            //Even if null, id will be set.
            if ($key == 'id') {
                continue;
            }
            if ($data->profile->$key != null) {            
                $notNull = true;
            }
        }

        if ($notNull == false) {
            $data->profile = null;
        }
        
        $data->comment = new stdClass();
        $data->comment->id = $comment->id;
        $data->comment->comment = $comment->comment;
        $data->comment->date = date('M jS, Y', strtotime($comment->comment_date));

        $data->controls = new stdClass();

        if ($user_id == $comment->user_id) {
            $data->controls->edit = true;
        } else {
            $data->controls->edit = false;
        }

        //TODO
        //get the post creator's ID
        $post_creator = null;

        if (
            $user_id == $comment->user_id ||
            $user_id == $post_creator
            ) 
        {
            $data->controls->delete = true;
        } else {
            $data->controls->delete = false;
        }

        if ($user_id != $comment->user_id) {
            $data->controls->flag = true;
        } else {
            $data->controls->flag = false;
        }

        return $data;
    }

    private static function getComments($post_id, $comment_id = null, $current = null, $init_load = null, $load_more = null) {

		$db = JFactory::getDbo();        

        $conditions = array(
            $db->quoteName('post_id') . '=' . $db->quote($post_id),
            $db->quoteName('enabled') . '=' . $db->quote('1'),        
            $db->quoteName('deleted') . ' IS NULL'
        );

        if ($comment_id) {
            array_push(
                $conditions, 
                $db->quoteName('id') . '=' . $db->quote($comment_id)
            );
        }

        //Get the total amount of comments for the post.
        $query = $db->getQuery(true);
        $query
            ->select('COUNT(id)')
            ->from($db->quoteName('comments'))
            ->where($conditions);

        $db->setQuery($query);

        $total = $db->loadObject();
        $total = $total->{'COUNT(id)'};

        //Get the actual posts.
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('comments'))
            ->where($conditions)
            ->order($db->quoteName('id') . ' DESC');

        if ($current == 0 && $init_load) {
            //Get the initial load of comments.  
            $query->setLimit($init_load, 0);
        }

        if ($current > 0 && $load_more) {
            //Get the next set of comments.
            $query->setLimit($load_more, $current);
        }
            
        $db->setQuery( $query );

        if ($comment_id != null) {
            $results = $db->loadObject();
        } else {
            $results = $db->loadObjectList();
        }

        $data = new stdClass();
        $data->total = $total;
        $data->results = $results;

        return $data;
	}
}
