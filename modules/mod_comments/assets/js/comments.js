//post = The original post.  Inspirational, gratitude, declaration, etc.            pid
//comments = comments made on the above posts                                       cid

var deleted_user_text = "<i>This user profile no longer exists.</i>";
var module_data = null;

$(document).ready(
    function() {        

        //The comment modules might not be on the page yet.  Wait until they are.
        var checkModules = setInterval(
            function() {
                module_data = getAllComments();
                if (module_data) {
                    clearInterval(checkModules);
                    loadInitialComments(module_data);
                }
            }, 100
        );
    }
);

function refreshComments() {
    module_data = getAllComments();
    loadInitialComments(module_data);
}

function getAllComments() {

    var comment_modules = document.getElementsByClassName("comment_module");

    var list = [];
    var settings = [];

    var ajax_call = false;        //The ajax call will be made if any of them are set to true, but only the ones set to true will be updated.

    if (comment_modules.length == 0) {
        //There are no comments loaded on the page yet.
        return null;
    }

    jQuery(comment_modules).each(
        function() {

            let comment_module = this;

            var post_id = comment_module.getAttribute("post-id");
            var button_title = comment_module.getAttribute("button-title");
            var placeholder = comment_module.getAttribute("placeholder");
            var auto_update = comment_module.getAttribute("auto-update");
            var title = comment_module.getAttribute("comment-title");
            var refresh_time = comment_module.getAttribute("refresh-time");

            var init_load = comment_module.getAttribute("init-load");
            var load_more = comment_module.getAttribute("load-more");

            var current_load = comment_module.getAttribute("current-load");

            var current_comments = comment_module.getElementsByClassName("comment");          //Get all of the comments that are currently in the module.
            var comment_ids = [];
            for (var i = 0; i < current_comments.length; i++) {
                comment_ids.push(current_comments[i].getAttribute("comment-id"));
            }

            if (auto_update == "true") {
                ajax_call = true;
            }

            var post_dat = {
                'post_id': post_id,
                'current': current_load,
                'init_load': init_load,
                'load_more': load_more,
                'comment_ids': comment_ids
            }

            list.push(post_dat);
            
            var params = {
                'post_id': post_id,
                'button_title': button_title,
                'placeholder': placeholder,
                'auto': auto_update,
                'title': title,
                'refresh': refresh_time
            }

            var dat = {
                params: params,
                module: comment_module
            };

            settings.push(dat);


            var initialized = comment_module.getAttribute("initialized");

            //Don't initialize a comments box twice.
            if (!initialized) {
                //Create the comment areas
                createCommentsArea(comment_module, params);
                //Create an empty list of comments so that it's displayed immediately.
                makeComments(comment_module, [], post_id);
                comment_module.setAttribute("initialized", true);
            }
        }
    );            

    return {
        settings: settings,
        list: list
    };
}

function loadInitialComments(data) {

    if (!data) {
        return;
    }

    var settings = data.settings;
    var list = data.list;
    if (settings.length > 0) {

        var data = {
                    method: 'getInitialComments',
                    format: 'raw',
                    module: 'comments',
                    option: 'com_ajax',
                    list: list
                }

        jQuery.ajax({
            type: 'GET',
            data: data,
            url: "index.php?",
            success:
                function(response) {
                    response = JSON.parse(response);
                    var comments_data = response.data;
                    var time = response.time;

                    for (var i = 0; i < comments_data.length; i++) {
                        var data = comments_data[i];
                        makeComments(settings[i].module, data, settings[i].params.post_id, time);
                    }

                    if (settings[0].params.auto == "true") {
                        startCommentAutorefresh();
                    }
                }
        });
    }
}

function startCommentAutorefresh() {
            
    var module_data = getAllComments();

    var settings = module_data.settings;

    /*
        This is a list of all of the refresh times.  
        This is so that if 10 different comments modules all have
        a refresh time of 2000 milliseconds, there are not 10 different
        AJAX calls, but only 1 for all of them.
    */
    var refresh_list = {};

    for (var i = 0; i < settings.length; i++) {
        var params = settings[i].params;

        if (!params.auto) {
            //This is not set to auto update.
            continue;
        }

        var index = parseInt(params.refresh);
        if (refresh_list[index] === undefined) {
            refresh_list[index] = [];
        }



        refresh_list[index].push(i);
    }

    var times = Object.keys(refresh_list);

    for (let i = 0; i < times.length; i++) {
        let checkComments = function() {
            setTimeout(
                function() {

                    //Start the update process

                    //Get a list of which comment boxes are in this refresh time.
                    let list = refresh_list[times[i]];      
                    let update_list = [];
                    for (let j = 0; j < list.length; j++) {
                        let module = settings[j].module;

                        //Get a list of all of the comments currently displayed so non-displayed ones aren't updated.
                        let comments = module.getElementsByClassName("comment");    
                        let id_list = [];
                        
                        for (let c = 0; c < comments.length; c++) {
                            id_list.push(comments[c].getAttribute('comment-id'));
                        }

                        update_list.push({                                
                            post_id: module.getAttribute("post-id"),
                            last_load: module.getAttribute("last-load"),
                            id_list: id_list
                        });                            
                    }
                    
                    let data = {
                        method: 'checkForNewComments',
                        format: 'raw',
                        module: 'comments',
                        option: 'com_ajax',

                        list : update_list
                    }

                    jQuery.ajax({
                        type: 'POST',
                        url: '/index.php?',
                        data: data,
                        success: function(response) {

                            response = JSON.parse(response);

                            var time = response.time;
                            var deleted = response.deleted;
                            var updated = response.updated;
                            var added = response.added;

                            deleted.forEach(
                                function(element) {

                                    var comment_area = document.getElementById("comments_" + element.post_id);

                                    var comments = comment_area.getElementsByClassName("comment");
                                    var original_comment = null;
                                    for (let i = 0; i < comments.length; i++) {
                                        let comment = comments[i];
                                        var comment_id = comment.getAttribute("comment-id");
                                        if (comment_id == element.comment_id) {
                                            original_comment = comment;
                                            break;
                                        }
                                    }

                                    //Remove the comment that was deleted.
                                    jQuery(original_comment).remove();

                                    changeCommentCount(element.post_id, -1);
                                    
                                    comment_area.setAttribute("last-load", time);
                                }
                            );

                            updated.forEach(
                                function(element) {
                                    var comment_area = document.getElementById("comments_" + element.post_id);
                                    var comments = comment_area.getElementsByClassName("comment");
                                    var original_comment = null;
                                    for (let i = 0; i < comments.length; i++) {
                                        let comment = comments[i];
                                        var comment_id = comment.getAttribute("comment-id");
                                        if (comment_id == element.comment_id) {
                                            original_comment = comment;
                                            break;
                                        }
                                    }

                                    if (original_comment != null) {
                                        var user_comment = original_comment.getElementsByClassName("user_comment")[0];
                                        user_comment.innerHTML = element.comment;
                                    }

                                    //Update the comments area with the newly updated time.
                                    comment_area.setAttribute("last-load", time);
                                }
                            );

                            added.forEach(
                                function(element) {
                                    var comment_area = document.getElementById("comments_" + element.post_id);
                                    var container = comment_area.getElementsByClassName("comments_area")[0];

                                    //Create the new comment HTML
                                    var new_comment = makeNewPost(element.post, element.post_id);

                                    container.insertBefore(new_comment, container.childNodes[0]);

                                    changeCommentCount(element.post_id, 1);
                                    comment_area.setAttribute("last-load", time);
                                }
                            );

                            /*
                                Do not put this in a setInterval, or comments can stack up.  
                                Comments should only be rechecked after the previous check has finished.
                            */
                            checkComments();
                        }
                    });
                }, times[i]
            );
        }

        checkComments();
    }
}

function createCommentsArea(div, params) {

    //Checks to see if the elements exist and only creates them if they don't.

    //Comment Button

    var commentButton = div.getElementsByClassName("comments_button");
    if (commentButton.length == 0) {

        commentButton = document.createElement('div');
        commentButton.className = "comments_button";
        var icon = document.createElement('span');
        icon.className = "comment_icon lizicon lizicon-bubbles2";        
        var commentsText = document.createTextNode("0 Comments");
        var commentsTextDiv = document.createElement('span');                
        commentsTextDiv.appendChild(commentsText);
        commentsTextDiv.className = "comments_button_text";
        commentButton.appendChild(icon);
        commentButton.appendChild(commentsTextDiv);

        commentButton.setAttribute("onclick","toggle_comment_display(this)");

        div.appendChild(commentButton);
    }

    //Comment Display

    var commentDisplay = div.getElementsByClassName("comments_display");
    if (commentDisplay.length == 0) {

        var commentDisplay = document.createElement('div');        
        commentDisplay.className = "comments_display";
        var inputArea = document.createElement('input');
        inputArea.className = "comment_input_field";
        inputArea.setAttribute("type", "text");
        inputArea.setAttribute("post-id", params.post_id);
        inputArea.setAttribute("placeholder", params.placeholder);

        inputArea.setAttribute("onkeyup", "submit_with_enter(event, this)");
        commentDisplay.appendChild(inputArea);

        var submitButton = document.createElement('button');
        submitButton.className = "comment_submit_button btn btn-primary";
        submitButton.setAttribute("post-id", params.post_id);
        submitButton.setAttribute("onclick", "submit_new_comment(this)");

        submitButton.innerHTML = params.button_title;
        commentDisplay.appendChild(submitButton);

        var commentsArea = document.createElement('div');
        commentsArea.className = "comments_area";
        commentsArea.setAttribute("post-id", params.post_id);

        commentDisplay.appendChild(commentsArea);

        var loadMoreButton = document.createElement('button');
        loadMoreButton.innerHTML = "Load More";
        loadMoreButton.className = "load_more btn btn-primary";
        loadMoreButton.setAttribute("post-id", params.post_id);
        loadMoreButton.setAttribute("onclick", "load_more_comments(this)");

        jQuery(loadMoreButton).css({
            "padding":"1px 1px",
            "margin":"0px 0px 12px 8px",
            "font-size":"11px"
        });

        commentDisplay.appendChild(loadMoreButton);

        div.appendChild(commentDisplay);        
    }    
}

function makeComments(area, data, post_id, time_update) {

    if (data.length == 0) {
        //Comments haven't been loaded yet.  Create an empty comments box.
        data.comments = [];
        data.total = 0;
    }

    var comments = data.comments;
    var total_comments = data.total;
    var new_current_load = parseInt(area.getAttribute("current-load")) + comments.length;

    var button = area.getElementsByClassName("load_more")[0];
    if (total_comments <= new_current_load) {
        //There are no more comments to load.  Hide the button.
        jQuery(button).hide();
    } else {
        //Else, be sure the button is visible.
        jQuery(button).show();
    }

    var comments_area = area.getElementsByClassName("comments_area")[0];    

    //Get all existing comments.
    var currentComments = area.getElementsByClassName("comment");

    area.setAttribute("last-load", time_update);    
    area.setAttribute("current-load", new_current_load);
    area.setAttribute("total-comments", total_comments);
    
    //Loop through them backwards since the newest is always placed at the beginning.
    //Otherwise, they'll be reverse chronological order
    
    for (var i = comments.length - 1; i >= 0; i--) {
        var id = comments[i].comment.id;
        
        comments_area.insertBefore(makeNewPost(comments[i], post_id), currentComments[0]);                    
    }

    var comments_button_text = area.getElementsByClassName("comments_button_text")[0];
    var text = total_comments + " Comment";

    if (total_comments < 0) {
        total_comments = 0;
    }

    if (total_comments != 1) {
        text += "s";
    }

    comments_button_text.innerHTML = text;

}

function makeNewPost(comment, post_id) {

    var text = comment.comment.comment;
    var date = comment.comment.date;
    var comment_id = comment.comment.id;

    if (comment.profile) {
        var name = comment.profile.first + " " + comment.profile.last;
        var user_id = comment.profile.id;
        var photo = comment.profile.photo;
        var alias = comment.profile.alias;

        var hrefImage = document.createElement('a');
        hrefImage.setAttribute("href", "community/my-profile/" + alias);

        var imageDiv = document.createElement('div');
        imageDiv.className = "comment-img";    
        var image = document.createElement('img');
        image.setAttribute("src", photo);
        imageDiv.appendChild(image);
        hrefImage.appendChild(imageDiv);
    
        var hrefName = document.createElement('a');
        hrefName.setAttribute("href", "community/my-profile/" + alias);
        var hrefNameText = document.createElement('span');
        hrefNameText.className = "comment-name";
        hrefNameText.innerHTML = name;
        hrefName.appendChild(hrefNameText);
    } else {
        text = deleted_user_text;
    }    

    var dateSpan = document.createElement('span');
    dateSpan.className = "comment-date";
    dateSpan.innerHTML = " - " + date;

    var brk = document.createElement('div');
        brk.className = "clearfix";

    if (comment.profile) {
        if (comment.controls.edit) {
            var editSpan = document.createElement('span');
            editSpan.className = "comment_edit_button icon_text icon_text_blue lizicon lizicon-pencil";
            editSpan.setAttribute("title", "Edit");
            editSpan.setAttribute("post-id", post_id);
            editSpan.setAttribute("comment-id", comment_id);
            editSpan.setAttribute("onclick", "toggle_comment_edit(this, 'edit')");

            var editBox = document.createElement('div');
            editBox.className = "user_comment_edit";
            editBox.setAttribute("comment-id", comment_id);
            jQuery(editBox).css({"display":"none"});

            var editInput = document.createElement('input');
            editInput.className = "comment_edit_text";
            editInput.setAttribute("type", "text");
            editInput.setAttribute("comment-id", comment_id);
            editInput.setAttribute("value", text);

            editInput.setAttribute("onkeyup", "update_with_enter(event, this)");

            var editButton = document.createElement('button');
            editButton.className = "comment_update btn btn-info btn-sm";
            editButton.setAttribute("comment-id", comment_id);
            editButton.setAttribute("onclick", "submit_edit_comment(this)");
            editButton.innerHTML = "Update";

            editBox.appendChild(editInput);
            editBox.appendChild(editButton);

        }

        if (comment.controls.delete) {
            var deleteSpan = document.createElement('span');
            deleteSpan.className = "comment_delete_button icon_text icon_text_blue lizicon lizicon-cross";
            deleteSpan.setAttribute("title", "Delete");
            deleteSpan.setAttribute("post-id", post_id);
            deleteSpan.setAttribute("comment-id", comment_id);
            deleteSpan.setAttribute("onclick", "toggle_comment_edit(this, 'delete')");

            var deleteBox = document.createElement('div');
            deleteBox.className = "user_comment_delete";
            deleteBox.setAttribute("post-id", post_id);
            deleteBox.setAttribute("comment-id", comment_id);
            jQuery(deleteBox).css({"display":"none"});

            var deleteText = document.createTextNode("Delete this comment?");

            var deleteButton = document.createElement("button");
            deleteButton.className = "delete_comment_button btn btn-danger btn-sm";
            deleteButton.setAttribute("comment-id", comment_id);
            deleteButton.setAttribute("post-id", post_id);
            deleteButton.setAttribute("onclick", "submit_delete_comment(this)");
            deleteButton.innerHTML = "Delete";

            deleteBox.appendChild(brk);
            deleteBox.appendChild(deleteText);        
            deleteBox.appendChild(deleteButton);
        }

        if (comment.controls.flag) {
            var flagSpan = document.createElement('span');
            flagSpan.className = "comment_flag_button icon_text icon_text_blue lizicon lizicon-flag";
            flagSpan.setAttribute("title", "Flag");
            flagSpan.setAttribute("post-id", post_id);
            flagSpan.setAttribute("comment-id", comment_id);
            flagSpan.setAttribute("onclick", "toggle_comment_edit(this, 'flag')");

            var flagBox = document.createElement('div');
            flagBox.className = "user_comment_flag";
            flagBox.setAttribute("comment-id", comment_id);
            jQuery(flagBox).css({"display":"none"});

            var flagText = document.createTextNode("Flag this comment as inappropriate?");

            var flagButton = document.createElement('div');
            flagButton.className = "flag_comment btn btn-danger btn-sm";
            flagButton.setAttribute("comment-id", comment_id);
            flagButton.setAttribute("onclick", "submit_flag_comment(this)");
            flagButton.innerHTML = "Flag";

            flagBox.appendChild(brk);
            flagBox.appendChild(flagText);
            flagBox.appendChild(flagButton);
        }
    }

    var commentContainer = document.createElement('div');
    commentContainer.className = "user_comment";
    commentContainer.setAttribute("comment-id", comment_id);
    commentContainer.innerHTML = text;

    var container = document.createElement('div');
    container.className = "comment";
    container.setAttribute("comment-id", comment_id);

    if (hrefImage) {
        container.appendChild(hrefImage);
    }

    if (hrefName) {
        container.appendChild(hrefName);    
    }

    if (dateSpan) {
        container.appendChild(dateSpan);
    }

    if (editSpan) {
        container.appendChild(editSpan);
    }

    if (deleteSpan) {
        container.appendChild(deleteSpan);
    }

    if (flagSpan) {
        container.appendChild(flagSpan);
    }

    if (editBox) {
        container.appendChild(editBox);
    }

    if (deleteBox) {
        container.appendChild(deleteBox);
    }

    if (flagBox) {
        container.appendChild(flagBox);
    }

    container.appendChild(brk);
    container.appendChild(commentContainer);
    container.appendChild(brk);

    return container;
}

function changeCommentCount(post_id, amount) {

    var comment_area = document.getElementById("comments_" + post_id);

    let current_total = parseInt(comment_area.getAttribute("total-comments")) + amount;
    if (current_total < 0) {
        current_total = 0;
    }

    let text = current_total + " Comment";
    if (current_total != 1) {
        text += "s";
    }

    comment_area.setAttribute("total-comments", current_total);
    comment_area.getElementsByClassName("comments_button_text")[0].innerHTML = text;
}

function toggle_comment_display(caller) {
    var comments_display = caller.parentNode.getElementsByClassName("comments_display")[0];
    var visible = jQuery(comments_display).is(":visible");

    if (visible) {        
        jQuery(comments_display).hide(250);
    } else {        
        jQuery(comments_display).show(250);
    }    
}

function update_with_enter(event, caller) {
    if (event.keyCode == 13) {        
        var update_button = caller.parentNode.getElementsByClassName("comment_update")[0];
        submit_edit_comment (update_button);        
    }
}

function submit_with_enter (event, caller) {
    if (event.keyCode == 13) {        
        var submit_button = caller.parentNode.getElementsByClassName("comment_submit_button")[0];
        submit_new_comment (submit_button);
    }
}

function submit_new_comment(caller) {

    if (caller.disabled) {
        return;
    }

    caller.disabled = true;
    var post_id = caller.getAttribute("post-id");

    var new_comment = caller.parentNode.getElementsByClassName("comment_input_field")[0];
    var comment_text = new_comment.value;

    if (comment_text == "" || comment_text == null) {
        return;
    }

    new_comment.value = "";

    var data = {
        method: 'postNewComment',
        format: 'raw',
        module: 'comments',
        option: 'com_ajax',

        text : comment_text,
        pid : post_id
    }

    jQuery.ajax({
        type: 'POST',
        url: '/index.php?',
        data: data,
        success: function(response) {

            var new_comment = makeNewPost(JSON.parse(response), post_id);
            var post_block = document.getElementById("comments_" + post_id);
            var comment_area = post_block.getElementsByClassName("comments_area")[0];
            var all_comments = comment_area.getElementsByClassName("comment");

            comment_area.insertBefore(new_comment, all_comments[0]);

            if (typeof(refreshBlocksit) !== "undefined") {
                setTimeout(
                    function() {
                        refreshBlocksit(get_num_columns(), 'div', '#block-container');
                    },
                    410
                );
            }

            changeCommentCount(post_id, 1);
            caller.disabled = false;
        }
    });
}

function submit_flag_comment (caller) {

    if (caller.disabled) {
        return;
    }

    var comment_id = caller.getAttribute("comment-id");
    caller.disabled = true;
    
    var data = {
        method: 'flagComment',
        format: 'raw',
        module: 'comments',
        option: 'com_ajax',

        cid : comment_id
    }

    jQuery.ajax({
        type: 'POST',
        url: '/index.php?',
        data: data,
        success: function(response) {
            alert("Thank you.\n This comment has been flagged.");
            toggle_comment_edit(caller.parentNode, 'reset');
            caller.disabled = false;
        }
    });
};

function submit_edit_comment (caller) {

    if (caller.disabled) {
        return;
    }

    caller.disabled = true;

    var comment_id = caller.getAttribute("comment-id");
    var comment_edit = caller.parentNode;

    var original_text = comment_edit.parentNode.getElementsByClassName("user_comment")[0];
    var user_comment_text = comment_edit.getElementsByClassName("comment_edit_text")[0].value;

    if (user_comment_text == "" || user_comment_text == null) {
        return;
    }

    var data = {
        method: 'editComment',
        format: 'raw',
        module: 'comments',
        option: 'com_ajax',
        text : user_comment_text,
        cid : comment_id
    }
    
    jQuery.ajax({
        type: 'POST',
        url: '/index.php?',
        data: data,
        success: function(response) {            
            original_text.innerHTML = user_comment_text;
            toggle_comment_edit(caller.parentNode, 'reset');
            caller.disabled = false;
        }
    });
};

function submit_delete_comment (caller) {

    if (caller.disabled) {
        return;
    }

    var comment_id = caller.getAttribute("comment-id");
    var post_id = caller.getAttribute("post-id");

    caller.disabled = true;
    
    var data = {
        method: 'deleteComment',
        format: 'raw',
        module: 'comments',
        option: 'com_ajax',

        cid : comment_id
    }

    jQuery.ajax({
        type: 'POST',
        url: '/index.php?',
        data: data,
        success: function(response) {
            var edit_area = caller.parentNode;
            var original_comment = edit_area.parentNode;

            jQuery(original_comment).hide(500,
                function() {
                    jQuery(original_comment).remove();
                }
            );

            if (typeof(refreshBlocksit) !== "undefined") {
                setTimeout(
                    function() {
                        refreshBlocksit(get_num_columns(), 'div', '#block-container');
                    },
                    410
                );
            }

            changeCommentCount(post_id, -1);
        }
    });
};

function toggle_comment_edit(caller, action) {

    var comment = caller.parentNode;

    var comment_box = jQuery(comment).find(".user_comment").first();
    var edit_box = jQuery(comment).find(".user_comment_edit").first();
    var delete_box = jQuery(comment).find(".user_comment_delete").first();
    var flag_box = jQuery(comment).find(".user_comment_flag").first();

    var comment_visible = comment_box.is(":visible");
    var edit_visible = edit_box.is(":visible");
    var delete_visible = delete_box.is(":visible");
    var flag_visible = flag_box.is(":visible");
    
    switch (action) {
        case "edit":
            delete_visible = false;
            flag_visible = false;
            edit_visible = !edit_visible;
            break;
        case "delete": 
            edit_visible = false;
            flag_visible = false;
            delete_visible = !delete_visible;
            break;
        case "flag":
            edit_visible = false;
            delete_visible = false;
            flag_visible = !flag_visible;
            break;
        case "reset":
        default:
            edit_visible = false;
            delete_visible = false;
            flag_visible = false;            
            break;
    }

    if (!delete_visible && !flag_visible && !edit_visible) {
        comment_visible = true;
    } else {
        comment_visible = false;
    }
    
    var toggle_speed = 400;
    
    if (comment_visible) {
        comment_box.show(toggle_speed);
    } else {
        comment_box.hide(toggle_speed);
    }
    
    if (edit_visible) {
        edit_box.show(toggle_speed);
    } else {
        edit_box.hide(toggle_speed);
    }
    
    if (delete_visible) {
        delete_box.show(toggle_speed);
    } else {
        delete_box.hide(toggle_speed);
    }
    
    if (flag_visible) {
        flag_box.show(toggle_speed);
    } else {
        flag_box.hide(toggle_speed);
    } 
}

function load_more_comments(button) {

    var comment_box = button.parentNode;
    var comments_module = comment_box.parentNode;

    var load_more = comments_module.getAttribute("load-more");
    var current_load = comments_module.getAttribute("current-load");
    var post_id = comments_module.getAttribute("post-id");

    var data = {
        method: 'loadMoreComments',
        format: 'raw',
        module: 'comments',
        option: 'com_ajax',
        load_more: load_more,
        current_load: current_load,
        post_id: post_id
    }

    jQuery.ajax({
        type: 'GET',
        data: data,
        url: "index.php?",
        success:
            function(response) {

                response = JSON.parse(response);
                var count = response.length;
                var new_current_load = 0 + parseInt(comments_module.getAttribute("current-load")) + count;
                comments_module.setAttribute("current-load", new_current_load);

                var comments_area = comment_box.getElementsByClassName("comments_area")[0];

                for (var i = 0; i < count; i++) {
                    var new_comment = makeNewPost(response[i], post_id);
                    comments_area.appendChild(new_comment);
                }

                if (new_current_load == parseInt(comments_module.getAttribute("total-comments"))) {
                    jQuery(button).hide(200);
                }
            }
    });

}