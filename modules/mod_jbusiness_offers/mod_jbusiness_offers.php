<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');

JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/common.css');
JHTML::_('stylesheet', 	'components/com_jbusinessdirectory/assets/css/responsive.css');
JHtml::_('stylesheet', 'media/com_jbusinessdirectory/css/latest-module.css');
JHtml::_('stylesheet', 'modules/mod_jbusiness_offers/assets/style.css');

JHtml::_('jquery.framework', true, true);

if($params->get('viewtype') == 'slider') {
    JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/slick.css');
    JHtml::_('script', 'components/com_jbusinessdirectory/assets/js/slick.js');
}

JHTML::_('script', 	'components/com_jbusinessdirectory/assets/js/jquery.raty.min.js');

require_once JPATH_SITE.'/administrator/components/com_jbusinessdirectory/helpers/translations.php';

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

JBusinessUtil::loadSiteLanguage();

//load items through cache mechanism
$cache = JFactory::getCache();
$items = $cache->call( array( 'modJBusinessOffersHelper', 'getList' ), $params );

$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
if($appSettings->enable_multilingual){
    JBusinessDirectoryTranslations::updateOffersTranslation($items);
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

$backgroundCss="";
if($params->get('backgroundColor')) {
    $backgroundCss = "background-color:".$params->get('backgroundColor').";";
}

$borderCss="";
if($params->get('borderColor')) {
    $borderCss="border-color:".$params->get('borderColor').";";
}
require JModuleHelper::getLayoutPath('mod_jbusiness_offers', "default_".$params->get('viewtype'));

?>