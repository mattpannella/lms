<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_login
 *
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @since       1.5
 */
class ModLoginAxsHelper
{
	/**
	 * Retrieve the URL where the user should be returned after logging in
	 *
	 * @param   \Joomla\Registry\Registry  $params  module parameters
	 * @param   string                     $type    return type
	 *
	 * @return string
	 */
	public static function getReturnUrl($params, $type)
	{
		$app  = JFactory::getApplication();
		$getType = $params->get($type);
		$item = $app->getMenu()->getItem($getType);

		// Stay on the same page
		$url = JUri::getInstance()->toString();

		if ($item)
		{
			$lang = '';

			if ($item->language !== '*' && JLanguageMultilang::isEnabled())
			{
				$lang = '&lang=' . $item->language;
			}

			$url = 'index.php?Itemid=' . $item->id . $lang;
		}

		return base64_encode($url);
	}

	/**
	 * Returns the current users type
	 *
	 * @return string
	 */
	public static function getType()
	{
		$user = JFactory::getUser();

		return (!$user->get('guest')) ? 'logout' : 'login';
	}

	/**
	 * Get list of available two factor methods
	 *
	 * @return array
	 *
	 * @deprecated  4.0  Use JAuthenticationHelper::getTwoFactorMethods() instead.
	 */
	public static function getTwoFactorMethods()
	{
		return JAuthenticationHelper::getTwoFactorMethods();
	}

	public static function checkTwoFactorRequiredAjax() {

		$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
		if (count($twofactormethods) > 1) {
			$un = JRequest::getVar('un');
			$db = JFactory::getDBO();
			//$query = "SELECT otpKey FROM joom_users WHERE username = '$un' LIMIT 1";

			$query = $db->getQuery(true);
			$query
				->select($db->quoteName('otpKey'))
				->from($db->quoteName('joom_users'))
				->where($db->quoteName('username') . '=' . $db->quote($un))
				->limit(1);

			$db->setQuery($query);

			$results = $db->loadResult();

			if ($results) {
				return "true";
			} else {
				return "false";
			}
		} else {
			return "false";
		}
	}
}
