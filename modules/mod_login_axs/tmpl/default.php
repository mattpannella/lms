<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('UsersHelperRoute', JPATH_SITE . '/components/com_users/helpers/route.php');

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
$uri = & JFactory::getURI();
$pageURL = $uri->toString();
?>
<link href="/modules/mod_login_axs/css/login.css" rel="stylesheet" type="text/css" />
<div class="tovuti-login-page">
	<div class="tovuti-login-form">
		<div id="login-error-message" style="display: none;"></div>
		<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-inline" aria-label="Login Form">
			<?php if ($params->get('pretext')) { ?>
				<div class="pretext">
					<p><?php echo $params->get('pretext'); ?></p>
				</div>
			<?php } ?>
			<div class="userdata">
				<div id="form-login-username" class="control-group">
					<div class="controls">
						<input aria-label="Enter Your Username or Email" id="modlgn-username" type="text" name="username" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME_EMAIL'); ?>" />
					</div>
				</div>
				<div id="form-login-password" class="control-group">
					<div class="controls">
						<input aria-label="Enter Your Password" id="modlgn-passwd" type="password" name="password" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" />
					</div>
				</div>

				<?php
					if (count($twofactormethods) > 1) {
				?>
					<div id="form-login-secretkey" class="control-group" style="display:none;">
						<div class="controls">
							<div class="input-prepend input-append">
								<input id="modlgn-secretkey" autocomplete="off" type="<?php echo $sercretKeyInputType; ?>" name="secretkey" class="input-small" tabindex="0" size="18" placeholder="<?php echo AxsLanguage::text("AXS_SECURITY_KEY", "Security Key") ?>" />
							</div>
						</div>
					</div>
				<?php
					}
				?>
				<div id="form-login-submit" class="control-group">
					<div class="controls">
						<button type="submit" aria-label="Click to Login" tabindex="0" name="Submit" class="btn btn-primary login-button"><?php echo JText::_('JLOGIN'); ?></button>
					</div>
				</div>
				<p class="message"><a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=reset"); ?>">
							<?php echo JText::_("MOD_LOGIN_FORGOT_YOUR_PASSWORD"); ?></a> |
							<a tabindex="0" href="<?php echo JRoute::_("index.php?option=com_users&view=remind"); ?>">
							<?php echo JText::_("MOD_LOGIN_FORGOT_YOUR_USERNAME"); ?></a>
						</p>
				<input type="hidden" name="return" id="return" value="<?php echo $return; ?>" />

				<?php echo JHtml::_('form.token'); ?>
			</div>
			<?php if ($params->get('posttext')) { ?>
				<div class="posttext">
					<p><?php echo $params->get('posttext'); ?></p>
				</div>
			<?php } ?>
		</form>
		<a class="btn btn-default closeModal" aria-label="Close Login Form" tabindex="0" data-dismiss="modal">Close</a>
	</div>
</div>

<script>
	var pageURL = '<?php echo base64_encode($pageURL); ?>';
	var returnURL = '<?php echo $return; ?>';

	if (returnURL == pageURL) {
		$('#return').val(btoa(parent.window.location));
	}

	$("#login-form").submit(function(e) {
		e.preventDefault();
		$('.tovuti-login-form button').css('background', '#777').attr('disabled', 'true');
		//var data = $(this).serialize();
		var data = {
			username: $("#modlgn-username").val(),
			password: $("#modlgn-passwd").val(),
			secretkey: $("#modlgn-secretkey").val(),
			return: $("#return").val()
		}

		if ($("#login-error-message:visible")) {
			$("#login-error-message").hide();
		}

		$.ajax({
			url: '/index.php?option=com_axs&task=popupmodule.loginAuthenticate&format=raw',
			type: 'post',
			data: data
		}).done(
			function(result) {
				//console.log(result);
				var response = JSON.parse(result);
				if(response.secretKeyVerification) {
					$('.tovuti-login-form button').css('background','#4CAF50').removeAttr('disabled');
					$("#login-error-message").html(response.message);
					if($("#login-error-message:hidden")) {
						$("#login-error-message").slideToggle();
					}
					if($("#form-login-secretkey").is(':hidden')) {
						$('#form-login-secretkey').slideToggle();
					}
					return;
				}
				if (response.status == "success") {
					$.ajax({
						url: '/index.php?option=com_users&task=user.login',
						type: 'post',
						data: data
					}).done(function() {
						window.location = response.url;
					});

				} else {
					$('.tovuti-login-form button').css('background', '#4CAF50').removeAttr('disabled');
					$("#login-error-message").html(response.message);
					if ($("#login-error-message:hidden")) {
						$("#login-error-message").slideToggle();
					}
				}
			});
	});
</script>