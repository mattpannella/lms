<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the login functions only once
require_once dirname(__FILE__).'/helper.php';

$params->def('greeting', 1);

$type             = ModLoginAxsHelper::getType();
$return           = ModLoginAxsHelper::getReturnUrl($params, $type);
$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
$security = AxsSecurity::getSettings();
if($security->twofactor_type == 'yubikey') {
	$sercretKeyInputType = 'password';
} else {
	$sercretKeyInputType = 'text';
}
$user             = JFactory::getUser();
$layout           = $params->get('layout', 'default');

// Logged users must load the logout sublayout
if (!$user->guest)
{
	$layout .= '_logout';
}

require JModuleHelper::getLayoutPath('mod_login_axs', $layout);

?>

<script>
	/* $(document).ready(
		function() {
			var username = document.getElementById("modlgn-username");

			var timer = null;
			var startTime = function() {
				timer = setTimeout(
					function() {
						var currentUser = username.value;

						$.ajax({
							type: 'GET',
							data: {
								method: 'checkTwoFactorRequired',
								format: 'raw',
								un: currentUser,
								module: 'login_axs',
								option: 'com_ajax'
							},
							url: "index.php?",
							success:
								function(response) {
									if (response == "true") {
										$("#form-login-secretkey").show(250);
									} else {
										$("#form-login-secretkey").hide(250);
									}
								}
						});
					}, 200
				);
			}

			$(username).on(
				"keyup change paste",
				function() {

					if (timer) {
						clearTimeout(timer);
					}

					startTime();
				}
			);
		}
	); */
</script>
