

jQuery(document).ready(
	function() {

		var member_badge_container = document.getElementById("member_badge_container");
		if(member_badge_container) {
			var member_badges = member_badge_container.getElementsByClassName("member_badge");
			var member_badges_count = member_badges.length;
			var member_badge_icon = document.getElementById("member_badge_icon");
		}		
		
		jQuery("#member_badge_show_all").click(
			function() {

				var state = this.getAttribute("state");
				var minimum = this.getAttribute("show");
				
				var alternate = member_badge_icon.getAttribute("alternate");
				var current = member_badge_icon.className;
				member_badge_icon.className = alternate;
				member_badge_icon.setAttribute("alternate", current);


				switch (state) {
					case "hide":
						this.setAttribute("state", "show");
						jQuery(".member_badge").show();
						
						break;
					case "show":
						this.setAttribute("state", "hide");

						for (var i = 0; i < member_badges_count; i++) {
							if (i > minimum) {
								jQuery(member_badges[i]).css({
									"display":"none"
								});
							}
						}						
						
						break;
				}
			}
		);

	}
);