<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
class ModAwardHelper {

	static function makeAward($type) {

        if (class_exists('CFactory')) {
            $mainframe = JFactory::getApplication();
            $jinput    = $mainframe->input;
            $my        = CFactory::getUser();
            $profile_id = $jinput->get('userid', $my->id, 'INT');
        } else {
            return;
        }

        switch ($type) {
            case "badge":
                $awards = self::makeBadges($profile_id);
                break;
            case "certificate":
                $awards = self::makeCertificates($profile_id);
                break;
            case "both":
                $awards  = self::makeBadges($profile_id);
                $awards .= self::makeCertificates($profile_id);
                break;
        }

        return $awards;
    }

    static function makeBadges($profile_id) {
        $db = JFactory::getDBO();
        $user = (int)$profile_id;
        $query = "SELECT
                    earned.date_earned,
                    badge.description,
                    badge.params,
                    badge.title
                FROM axs_awards_earned AS earned
                    JOIN axs_awards AS badge
                    WHERE earned.user_id = $user
                    AND earned.badge_id = badge.id
                    AND(badge.type = 'badge' OR badge.type = '')
                    AND badge.enabled = 1";
        $db->setQuery($query);

        $badges = $db->loadObjectList();
        $badge_count = 0;
        $badge_maximum = 100;

        $code = AxsLanguage::getCurrentLanguage()->getTag();    //Get the lang_code, such as en-GB
        if(!$badges) {
            return false;
        }

        ob_start();
        ?>
        <div id='member_badge_container'>
            <?php
            foreach ($badges as $badge) {

                $params = json_decode($badge->params);
                $overrides = $params->language_overrides;

                if (isset($overrides->$code) && $overrides->$code) {
                    //Some older badges do not have overrides

                    $override = $overrides->$code;

                    $badge_hover = $override->badge_hover;
                    $badge_icon = $override->badge_icon;
                    $badge_color = $override->badge_color;
                    $badge_image = $override->badge_image;
                    $badge_type = $override->badge_type;

                } else {
                    $badge_hover = $params->badge_hover;
                    $badge_icon = $params->badge_icon;
                    $badge_color = $params->badge_color;
                    $badge_image = $params->badge_image;
                    $badge_type = $params->badge_type;
                }

                $badge_count++;
                if ($badge_count > $badge_maximum) {
                    $hidden = "display:none;";
                } else {
                    $hidden = "";
                }

                switch ($badge_type) {
                    case "icon":
                        ?>
                            <div
                                class="member_badge_wrap"
                                style="display: inline-block;"
                            >
                                <span
                                    title="<?php echo $badge_hover;?>"
                                    class="member_badge <?php echo $badge_icon;?>"
                                    style="font-size: 32px;
                                            <?php
                                                if ($badge_color != "") {
                                                    echo "color:" . $badge_color . ";";
                                                }

                                                echo $hidden;
                                            ?>"

                                >

                                </span>
                                <div class="cert-title"><?php echo AxsLMS::truncate($badge->title,25); ?></div>
                            </div>
                        <?php
                        break;

                    case "image":
                        $brand = AxsBrands::getBrand();
                        $thisuser = JFactory::getUser()->id;
                        $thisprofile = CFactory::getActiveProfile()->id;
                        $input = JFactory::getApplication()->input;
                        $view = $input->get('view');

                        if(!isset($_SESSION['facebookShareAccess'])) {
                            $domain = $_SERVER['SERVER_NAME'];
                            $checkDomainAccess = AxsFacebook::checkDomain($domain);
                            if(!$checkDomainAccess) {
                                $addDomain = AxsFacebook::addDomain($domain);
                                if($addDomain) {
                                    $checkDomainAccess = AxsFacebook::checkDomain($domain);
                                } else {
                                    $checkDomainAccess = false;
                                }
                            }
                            $_SESSION['facebookShareAccess'] = $checkDomainAccess;
                        }

                        $date = date("M d, Y", strtotime($badge->date_earned));

                        $facebookShareAccess = $_SESSION['facebookShareAccess'];
                        ?>
                            <div
                                class="member_badge_wrap"
                                style="display: inline-block;"
                            >
                                <img
                                    src="<?php echo $badge_image;?>"
                                    class="member_badge"
                                    title="<?php echo $badge_hover;?>"
                                    style="width: 175px; height: auto; margin: 10px; <?php echo $hidden;?>"
                                />
                                <?php if ($thisuser == $thisprofile && $view != 'transcripts') { ?>
                                    <div class="member_badge_overlink">
                                    <?php if($facebookShareAccess) { ?>
                                        <div class="member_badge_sharelink_bg facebook-bg">
                                            <div
                                                class="member_badge_sharelink fa fa-facebook clearfix"
                                                path="<?php echo JUri::base().$badge_image;?>"
                                                brand="<?php echo $brand->site_title;?>"
                                                badge="<?php echo $badge_hover;?>"
                                                title="<?php echo AxsLanguage::text("AXS_SHARE_ON_FACEBOOK", "Share on Facebook") ?>"
                                            >
                                            </div>
                                        </div>
                                    <?php } ?>
                                        <div class="member_badge_sharelink_bg download-bg">
                                            <a
                                                href="<?php echo $badge_image;?>"
                                                class="member_badge_sharelink fa fa-download clearfix"
                                                title="<?php echo AxsLanguage::text("AXS_DOWNLOAD", "Download") ?>"
                                                download="<?php echo $badge_hover;?>"
                                            >
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="cert-title"><?php echo AxsLMS::truncate($badge->title,25); ?></div>
                            </div>
                        <?php
                        break;
                }

            }
            ?>
        </div>

        <?php
            if ($badge_count > $badge_maximum) {
        ?>
                <div
                    id="member_badge_show_all"
                    title="show more"
                    class="btn btn-primary"
                    state="hide"
                    show="<?php echo $badge_maximum;?>"
                >
                    <span id="member_badge_icon" class="lizicon-circle-down" alternate="lizicon-circle-up"></span>
                </div>
        <?php
            }
        return ob_get_clean();
    }

    public function makeTranscriptBadges($profile_id) {
        $db = JFactory::getDBO();
        $user = (int)$profile_id;
        $query = "SELECT
                    earned.date_earned,
                    badge.description,
                    badge.params,
                    badge.title
                FROM axs_awards_earned AS earned
                    JOIN axs_awards AS badge
                    WHERE earned.user_id = $user
                    AND earned.badge_id = badge.id
                    AND(badge.type = 'badge' OR badge.type = '')
                    AND badge.enabled = 1";
        $db->setQuery($query);

        $badges = $db->loadObjectList();

        $code = AxsLanguage::getCurrentLanguage()->getTag();    //Get the lang_code, such as en-GB
        if(!$badges) {
            return false;
        }

        ob_start();
        ?>

            <?php
            foreach ($badges as $badge) {

                $params = json_decode($badge->params);
                $overrides = $params->language_overrides;

                if (isset($overrides->$code) && $overrides->$code) {
                    //Some older badges do not have overrides

                    $override = $overrides->$code;

                    $badge_hover = $override->badge_hover;
                    $badge_icon = $override->badge_icon;
                    $badge_color = $override->badge_color;
                    $badge_image = $override->badge_image;
                    $badge_type = $override->badge_type;

                } else {
                    $badge_hover = $params->badge_hover;
                    $badge_icon = $params->badge_icon;
                    $badge_color = $params->badge_color;
                    $badge_image = $params->badge_image;
                    $badge_type = $params->badge_type;
                }
            ?>

                <img
                    src="<?php echo $badge_image;?>"
                    class="member_badge"
                />


        <?php } ?>


        <?php
        return ob_get_clean();
    }

    public function getCertificate($certificate_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards_certificates'))
              ->where($db->qn('id').'='.(int)$certificate_id)
              ->limit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();

        return $certificate;
    }

    static function makeCertificates($profile_id) {
        $db = JFactory::getDBO();
        $user = (int)$profile_id;
        $query = "SELECT *
                FROM axs_awards_earned AS earned
                    JOIN axs_awards AS badge
                    WHERE earned.user_id = $user
                    AND earned.badge_id = badge.id
                    AND badge.type = 'certificate'
                    AND badge.enabled = 1";
        $db->setQuery($query);

        $certificates = $db->loadObjectList();
        $badge_count = 0;
        $badge_maximum = 100;
        $code = AxsLanguage::getCurrentLanguage()->getTag();    //Get the lang_code, such as en-GB

        if(!$certificates) {
            return false;
        }
        ob_start();
        ?>
        <div id='member_certificate_container'>
            <?php
            foreach ($certificates as $certificate) {


                $params = json_decode($certificate->params);
                $overrides = $params->language_overrides;
                if (isset($overrides->$code) && $overrides->$code) {
                    $override = $overrides->$code;
                    $certificate_hover = $override->badge_hover;
                    $certificate_id = $override->certificate;
                } else {
                    $certificate_hover = $params->badge_hover;
                    $certificate_id = $params->certificate;
                }

                $certParams = new stdClass();
                $certParams->certificate_id = $certificate_id;
                $certParams->award_id = $certificate->id;
                $certParams->user_id = $user;
                $certParams->language = $code;
                $key = AxsKeys::getKey('lms');
                $encryptedCertificate = base64_encode(AxsEncryption::encrypt($certParams, $key));

                $cert = self::getCertificate($certificate_id);
                $certParams = json_decode($cert->params);
                $badge_count++;
                if ($badge_count > $badge_maximum) {
                    $hidden = "display:none;";
                } else {
                    $hidden = "";
                }

                $defaultCertThumbnail = "/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/learning-management-system/defaultCertThumbnail.png";

                if(!$certParams->certificate_thumbnail) {
                    $certParams->certificate_thumbnail = $defaultCertThumbnail;
                }
                ?>
                <div
                    class="member_badge_wrap"
                    style="display: inline-block;"
                    data-toggle="tooltip"
                    title="<?php echo $cert->title ?>"
                >
                <a
                    href="/certificates?cert=<?php echo $encryptedCertificate; ?>"
                    class="jcepopup noicon"
                    data-mediabox="1"
                    type="iframe"
                    data-mediabox-width="850"
                    data-mediabox-height="725">
                        <img
                            src="<?php echo $certParams->certificate_thumbnail;?>"
                            class="member_badge certificate_image_thumb"
                            title="<?php echo $badge_hover;?>"
                            style="width: 175px; height: auto; margin: 10px; <?php echo $hidden;?>"
                        />
                </a>
                    <div class="cert-title"><?php echo AxsLMS::truncate($cert->title,25); ?></div>
                </div>
     <?php  } ?>
        </div>

        <?php
            if ($badge_count > $badge_maximum) {
        ?>
                <div
                    id="member_badge_show_all"
                    title="show more"
                    class="btn btn-primary"
                    state="hide"
                    show="<?php echo $badge_maximum;?>"
                >
                    <span id="member_badge_icon" class="lizicon-circle-down" alternate="lizicon-circle-up"></span>
                </div>
        <?php
            }
        return ob_get_clean();
    }

}
