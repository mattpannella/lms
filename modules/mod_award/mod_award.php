<?php
defined('_JEXEC') or die;

require_once ('modules/mod_award/helper.php');

$thisuser = JFactory::getUser()->id;
$thisprofile = CFactory::getActiveProfile()->id;

?>
	<link rel="stylesheet" type="text/css" href="modules/mod_award/assets/css/badges.css?v=4">

	<script type="text/javascript" src="modules/mod_award/assets/js/badges.js?v=2"></script>
	

<?php	
	$award_type = $params->get('award_type');
	$memberAwards = ModAwardHelper::makeAward($award_type);
	require JModuleHelper::getLayoutPath('mod_award');
?>
<?php if ($thisuser == $thisprofile) { ?>
	<script type="text/javascript" src="/media/tovuti/js/social-share.js"></script>
<?php } ?>