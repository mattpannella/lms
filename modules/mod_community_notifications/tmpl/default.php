<?php

/**
 * @copyright (C) 2015 iJoomla, Inc. - All rights reserved.
 * @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
 * @author iJoomla.com <webmaster@ijoomla.com>
 * @url https://www.jomsocial.com/license-agreement
 * The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
 * More info at https://www.jomsocial.com/license-agreement
 */
defined('_JEXEC') or die('Restricted access');
?>





<?php
if ($my->id != 0) {
    $jinput = JFactory::getApplication()->input;
    $inboxModel         = CFactory::getModel('inbox');
    $notifModel = CFactory::getModel('notification');
    $filter             = array();
    $filter['user_id']  = $my->id;
    $friendModel        = CFactory::getModel('friends');
    $profileid          = $jinput->get('userid', 0);

    //CFactory::load( 'libraries' , 'toolbar' );
    $toolbar = CToolbarLibrary::getInstance();
    $newMessageCount        = $toolbar->getTotalNotifications('inbox');
    $newEventInviteCount    = $toolbar->getTotalNotifications('events');
    $newFriendInviteCount   = $toolbar->getTotalNotifications('friends');
    $newGroupInviteCount    = $toolbar->getTotalNotifications('groups');

    $myParams           =   $my->getParams();
    $newNotificationCount   = $notifModel->getNotificationCount($my->id, '0', $myParams->get('lastnotificationlist', ''));
    $newEventInviteCount    = $newEventInviteCount + $newGroupInviteCount + $newNotificationCount;

    //CFactory::load( 'helpers' , 'string');

    $config = CFactory::getConfig();
    $uri    = CRoute::_('index.php?option=com_community', false);
    $uri    = base64_encode($uri);

    //CFactory::load('helpers' , 'string' );

    $show_global_notification   = $params->get('show_notification', 1);
    $show_friend_request    = $params->get('show_request', 1);
    $enable_private_message     = $params->get('show_privatemsg', 1);

    // IDs.
    $mainframe = JFactory::getApplication();
    $jinput = $mainframe->input;
    $my = CFactory::getUser();
    $userid = $jinput->get('userid', '', 'INT');
    $user = CFactory::getUser($userid);

    $document->addScriptDeclaration('joms_my_id = ' . $my->id . ';');
    $document->addScriptDeclaration('joms_user_id = ' . $user->id . ';');

?>

    <div class="joms-notifications">
        <?php if ($show_global_notification) { ?>
            <a title="<?php echo JText::_('COM_COMMUNITY_NOTIFICATIONS_GLOBAL'); ?>" href="javascript:" aria-label="<?php echo AxsLanguage::text("AXS_NOTIFICATIONS", "Notifications") ?>" onclick="joms.popup.notification.global();">
                <!-- REMOVE remove instances of lizicons used by tovuti - these are unapproved and should be deprecated throughout     -->
                <!-- <span class="notification-icon lizicon-1-TEP-Connect-Icon-notification"></span> -->
                <i class="fa-solid fa-bell"></i>
                <?php if ($newEventInviteCount) { ?>
                    <span class="notification-alert joms-js--notiflabel-general"><?php echo ($newEventInviteCount) ? $newEventInviteCount : 0; ?></span>
                <?php } ?>
            </a>
        <?php } ?>

        <?php if ($enable_private_message) { ?>
            <a title="<?php echo JText::_('COM_COMMUNITY_NOTIFICATIONS_INBOX'); ?>" aria-label="<?php echo AxsLanguage::text("AXS_INBOX", "Inbox") ?>" href="<?php echo CRoute::_('index.php?option=com_community&view=inbox'); ?>" onclick="joms.popup.notification.pm(); return false;">
                <!-- REMOVE remove instances of lizicons used by tovuti - these are unapproved and should be deprecated throughout     -->
                <!-- <span class="notification-icon lizicon-1-TEP-Connect-Icon-myinbox"></span> -->
                <i class="fa-solid fa-envelope"></i>
                <?php if ($newMessageCount) { ?>
                    <span class="notification-alert joms-js--notiflabel-inbox"><?php echo ($newMessageCount) ? $newMessageCount : 0; ?></span>
                <?php } ?>
            </a>
        <?php } ?>

        <?php if ($show_friend_request) { ?>
            <a title="<?php echo JText::_('COM_COMMUNITY_NOTIFICATIONS_INVITE_FRIENDS'); ?>" aria-label="<?php echo AxsLanguage::text("AXS_FRIEND_REQUESTS", "Friend Requests") ?>" href="<?php echo CRoute::_('index.php?option=com_community&view=friends&task=pending'); ?>" onclick="joms.popup.notification.friend(); return false;">
                <!-- REMOVE remove instances of lizicons used by tovuti - these are unapproved and should be deprecated throughout     -->
                <!-- <span class="notification-icon lizicon-1-TEP-Connect-Icon-friendrequest"></span> -->
                <i class="fa-solid fa-users"></i>
                <?php if ($newFriendInviteCount) { ?>
                    <span class="notification-alert joms-js--notiflabel-frequest"><?php echo ($newFriendInviteCount) ? $newFriendInviteCount : 0; ?></span>
                <?php } ?>
            </a>
        <?php } ?>

    </div>

<?php
}
?>