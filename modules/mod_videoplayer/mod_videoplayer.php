<?php

defined('_JEXEC') or die;
require_once 'components/shared/controllers/common.php';
require_once 'modules/mod_videoplayer/helper.php';

$video_type = $params->get("video_type");
$video_title = $params->get("video_title");
if(!$video_title) {
    $video_title = $module->title;
}
$brand = AxsBrands::getBrand();
$videoParams = new stdClass();
$videoParams->video_title = $video_title;
switch ($video_type) {
    case 'youtube':
        $videoParams->video      = $params->get("youtube_id");
        $videoParams->video_type = $video_type;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
    case 'screencast':
        $videoParams->video      = $params->get("screencast_id");
        $videoParams->video_type = $video_type;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
    case 'vimeo':
        $videoParams->video      = $params->get("vimeo_id");
        $videoParams->video_type = $video_type;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
    case 'facebook':
        $videoParams->video      = $params->get("facebook_url");
        $videoParams->video_type = $video_type;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
    case 'mp4':
        $videoParams->video      = $params->get("video_url");
        $videoParams->video_type = $video_type;        
        if($params->get("cover_image")) {
            $poster = $params->get("cover_image");
        }
        $videoParams->poster     = $poster;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
    
    default:
        if($params->get("cover_image")) {
            $poster = $params->get("cover_image");
        }        
        $videoParams->video       = $video_url;
        $videoParams->video_type  = $video_type;
        $videoParams->poster      = $poster;
        $video_html = AxsMedia::getVideo($videoParams);
        break;
}

require JModuleHelper::getLayoutPath('mod_videoplayer');