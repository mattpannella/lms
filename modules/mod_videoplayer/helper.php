<?php

class ModVideoplayerHelper {
    public static function getVideoData($view_type, $videoID) {
        $column = 'id';

        $db = getDatabase();
        $query = $db->getQuery(true);

        $conditions = $db->quoteName($column) . '=' . (int)$videoID;        
        
        $query
            ->select('*')
            ->from($db->quoteName('axs_gin_media'))
            ->where($conditions)
            ->order('id DESC')
            ->setLimit(1);

        $db->setQuery($query);
        $data = $db->loadObjectList();

        foreach($data as $row) {
            $brand = $row->brand;
            $videofile = $row->source;
            $external_source = $row->external_source;
            $cover_image = $row->cover_image;
        }

        return array($videofile, $external_source, $cover_image, $brand);
    }
}