<?php
defined('_JEXEC') or die;

// $module and $params contain stuff about this instance of the module.
$main_message = $params->get('main_message', '');
$when_to_show = $params->get('when_to_show', '');
$background = $params->get('background', '');
$main_message = $params->get('main_message', '');
$border = $params->get('border', '');
$user_id = JFactory::getUser()->id;
$input = JFactory::getApplication()->input;
$idYo = $module->id;

//check if there is a cookie set to hide this. if not then see if it should be shown.
if(!$input->cookie->get("dashTop".$idYo, 0)) {
	$shouldBeShown = false;
  switch($when_to_show) {
    case 0:
      $shouldBeShown = true;
      $cookieExpire = (new DateTime('tomorrow'))->format(DateTime::COOKIE);
      break;
    case 1:
	    $subs = AxsPayment::getUserSubscriptions($user_id);
	    foreach($subs as $s) {
		    $latestPeriod = $s->periods[count($s->periods) - 1];
		    if ($latestPeriod->status == 'NONE' || $latestPeriod->status == 'PART') {
		    	foreach($latestPeriod->trxns as $t) {
		    		if($t->status == 'REF') {
		    			$shouldBeShown = false;
		    			break 2;
				    }
			    }
			    $shouldBeShown = true;
			    break;
		    }
	    }
      $cookieExpire = (new DateTime('tomorrow'))->format(DateTime::COOKIE);
      break;
    case 2:
      $shouldBeShown = true;
      $cookieExpire = (new DateTime('2050-12-12'))->format(DateTime::COOKIE);
      break;
  }
} else {
	$shouldBeShown = false;
}

require JModuleHelper::getLayoutPath('mod_top_message'); //this must be placed at the bottom