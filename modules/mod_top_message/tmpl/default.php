<?php
defined('_JEXEC') or die;

if($shouldBeShown) { ?>
    <div id="dashTop<?php echo $idYo; ?>" style="background-color: <?php echo $background; ?>; border: 2px solid <?php echo $border; ?>; margin: 5px 10px; font-size: 20px; padding: 7px 15px; color: black; text-align: center;">
        <p style="display: inline-block; margin: 0;">
            <?php echo $main_message;
            if($when_to_show == 1) { ?>
                <a href="index.php?option=com_converge&view=cards" class="btn btn-primary">Update Card</a> or <a href="index.php?option=com_converge&view=cards" class="btn btn-primary">Retry Payment</a>
            <?php } else if($when_to_show == 2) {
                $doc = JFactory::getDocument();
                $doc->addScript('modules/mod_top_message/assets/js/intro.min.js');
                $doc->addStyleSheet('modules/mod_top_message/assets/css/introjs.min.css');
                ?>
                <button class="btn btn-primary" type="button" onclick="startTour()">Take the Tour</button>
            <?php } ?>
        </p>
        <p class="pull-right" style="display: inline-block; margin: 0; font-size: 14px; line-height: 34px; cursor: pointer;" onclick="close<?php echo $idYo; ?>(event)"><span class="lizicon lizicon-cross"></span></p>
    </div>
    <style>
        .tourSidebar {
            background-color: #444;
        }
        .tourMain {
            background-color: inherit;
            opacity: .5;
        }
        .dropdown-menu {
            z-index: 9999997!important;
        }
    </style>
    <script>
        function close<?php echo $idYo; ?>(e) {
            jQuery('#dashTop<?php echo $idYo; ?>').fadeOut();
            document.cookie = 'dashTop<?php echo $idYo; ?>=hide; expires=<?php echo $cookieExpire; ?>; path=/';
        }

        var toggled = false;
        var element = 0;

        var ddtoggled = false;
        var ddelement = 0;

        function showMenu(selector) {
            selector.find('.submenu').show();
            var menu = selector.find('.navLabel').text();
            localStorage['menu_'+menu] = 1;
        }

        function hideMenu(selector) {
            selector.find('.submenu').hide();
            var menu = selector.find('.navLabel').text();
            localStorage['menu_'+menu] = 0;
        }

        var stopper = function () {
            return false;
        };

        function startTour() {
            var tour = introJs();

            tour.setOptions({
                showBullets: true,
                showStepNumbers: false,
                highlightClass: 'tourMain'
            });

            tour.onbeforechange(function(el) {
                if($(el).parents('.sideIcons').length > 0) {
                    tour.setOption('highlightClass', 'tourSidebar');
                } else {
                    tour.setOption('highlightClass', 'tourMain');
                }

                if(toggled) {
                    hideMenu($(element));
                    element = 0;
                    toggled = false;
                }

                if($(el).hasClass('submenu-parent')) {
                    showMenu($(el));
                    element = el;
                    toggled = true;
                }

                if(ddtoggled) {
                    if($(el).parents('.dropdown-menu').length == 0 && !$(el).hasClass('dropdown')) {
                        $(ddelement).unbind('hide.bs.dropdown', stopper);
                        $(ddelement).find('.dropdown-toggle').dropdown("toggle");
                        ddelement = 0;
                        ddtoggled = false;
                    }
                } else if($(el).parents('.dropdown-menu').length > 0) {
                    $(el).parent().parent().on('hide.bs.dropdown', stopper);
                    ddelement = $(el).parent().parent();
                    ddelement.find('.dropdown-toggle').dropdown("toggle");
                    ddtoggled = true;
                }

                if($(el).hasClass('dropdown')) {
                    $(el).on('hide.bs.dropdown', stopper);
                    $(el).find('.dropdown-toggle').dropdown("toggle");
                    ddelement = el;
                    ddtoggled = true;
                }
            });

            tour.onexit(function() {
                if(ddtoggled) {
                    $(ddelement).unbind('hide.bs.dropdown', stopper);
                    $(ddelement).find('.dropdown-toggle').dropdown("toggle");
                    ddelement = 0;
                    ddtoggled = false;
                }
                if($('.Events').length > 0) {
                    $('.slick-track').css({
                        'width': '20000px',
                        'height': '',
                        'overflow': ''
                    });
                    $('.Events').slick('slickPlay');
                }
            });

            tour.oncomplete(function() {
                if(ddtoggled) {
                    $(ddelement).unbind('hide.bs.dropdown', stopper);
                    $(ddelement).find('.dropdown-toggle').dropdown("toggle");
                    ddelement = 0;
                    ddtoggled = false;
                }
                if($('.Events').length > 0) {
                    $('.slick-track').css({
                        'width': '20000px',
                        'height': '',
                        'overflow': ''
                    });
                    $('.Events').slick('slickPlay');
                }
            });

            if($('.Events').length > 0) {
                $('.Events').slick('slickPause');
                $('.Events').slick('slickSetOption', 'autoplay', false, false);
                $('.slick-track').css({
                    'width': 'inherit',
                    'height': '250px',
                    'overflow': 'hidden',
                    'left': 0
                });
            }
            tour.start();
        }
    </script>
<?php } ?>