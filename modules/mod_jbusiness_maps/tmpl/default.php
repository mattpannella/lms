<?php

/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/font-awesome.css');
$preserve = $params->get('preserve');
?>

<div class="module-search-map">
    <?php
        require JPATH_SITE.'/components/com_jbusinessdirectory/include/search-map.php';
    ?>
</div>

<script>
    var siteRoot = '<?php echo JURI::root(); ?>';
    var compName = '<?php echo "com_jbusinessdirectory"; ?>';
    var url = siteRoot+'index.php?option='+compName;

    jQuery(document).ready(function(){
        <?php
        $load = JRequest::getVar("latitude");
        if($params->get('geo_location') && empty($load)){ ?>
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(addCoordinatesToUrl);
        }
        <?php } ?>
        
    });

    

    jQuery(document).ready(function(){
    });
    

    loadMapScript();

   
</script>