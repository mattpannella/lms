<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

class modJBusinessMapsHelper{

    static function getList($params) {
        $appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
  	
		JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_jbusinessdirectory/tables');
		$companiesTable = JTable::getInstance('Company','JTable');
		$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
		$searchDetails = array();
		$searchDetails["enablePackages"] = $appSettings->enable_packages;
		$searchDetails["showPendingApproval"] = $appSettings->show_pending_approval==1;;
		$searchDetails["showSecondayLocationsMap"] = $appSettings->show_secondary_map_locations;
		
		$companies =  $companiesTable->getCompaniesByNameAndCategories($searchDetails, 0, $maxListings);

		foreach($companies as $company) {
			$company->packageFeatures = explode(",", $company->features);
		}
		
		foreach($companies as $company) {
			$company->packageFeatures = explode(",", $company->features);
			$attributesTable =  JTable::getInstance('CompanyAttributes','JTable');
			$company->customAttributes = $attributesTable->getCompanyAttributes($company->id);
	        $company->link = JBusinessUtil::getCompanyLink($company, true);
            $company->mainCategoryLink = JBusinessUtil::getCategoryLink($company->mainCategoryId, $company->mainCategoryAlias);
            $company->packageFeatures = explode(",", $company->features);
        }

        return $companies;
    }

}
?>
