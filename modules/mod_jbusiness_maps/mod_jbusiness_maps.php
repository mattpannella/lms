<?php

/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once JPATH_SITE.'/components/com_jbusinessdirectory/assets/defines.php';
require_once JPATH_SITE.'/components/com_jbusinessdirectory/assets/utils.php';
require_once JPATH_SITE.'/administrator/components/com_jbusinessdirectory/helpers/translations.php';

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

JHtml::_('jquery.framework', true, true);
JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/common.css');
JHtml::_('stylesheet', 'components/com_jbusinessdirectory/assets/css/forms.css', array('version' => 'auto'));
JHtml::_('stylesheet', 'media/com_jbusinessdirectory/css/search-module.css');

$session = JFactory::getSession();

$geoLocation = $session->get("geolocation");
$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
JBusinessUtil::loadSiteLanguage();

$appSettings = JBusinessUtil::getInstance()->getApplicationSettings();
if($appSettings->enable_multilingual){
    JBusinessDirectoryTranslations::updateBusinessListingsTranslation($items);
    JBusinessDirectoryTranslations::updateBusinessListingsSloganTranslation($items);
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));


//load items through cache mechanism
$cache = JFactory::getCache();
$companies = $cache->call( array( 'modJBusinessMapsHelper', 'getList' ), $params );

if($appSettings->enable_multilingual) {
    JBusinessDirectoryTranslations::updateCategoriesTranslation($categories);
    JBusinessDirectoryTranslations::updateTypesTranslation($types);
}

$mapHeight = $params->get('mapHeight');
$mapWidth = $params->get('mapWidth');

$menuItemId ="";
if($params->get('mItemId')){
    $menuItemId="&Itemid=".$params->get('mItemId');
}


$layoutType = $params->get('layout-type', 'horizontal');
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

$radius = $session->get("radius");
if(!isset($radius))
    $radius = $params->get('radius'); 

require (JModuleHelper::getLayoutPath( 'mod_jbusiness_maps',$params->get('layout', 'default')));

?>