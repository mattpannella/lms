<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//jDumpMessage('default_heading');

$title = $item->anchor_title ? ' title="' . $item->anchor_title . '"' : '';
$anchor_css = $item->anchor_css ? $item->anchor_css : '';

$icon = $item->params->get('sidemenu-icon');
$icon_position = $item->params->get('sidemenu-icon-position');

$navLabelText = $item->language == '*' ? AxsLanguage::translate($item->title) : $item->title;

if ($icon)
{
	if (strpos($icon, '#') !== FALSE){
		$text_icon = str_replace('#', '', $icon);
		$icon = '';
	} else {
		$text_icon = '';
	}

	$iconClass = "iconStyle " . $icon;

	switch ($icon_position) {
		case 'top':
			$linktype = '<span class="lizicon '.$icon.'">'.$text_icon.'</span><span class="navLabel">'.$navLabelText.'</span>';
			break;

		case 'left':
			$linktype = '<span class="lizicon '.$icon.'"></span>'.$navLabelText;
			break;

		case 'right':
			$linktype = $navLabelText.'<span class="lizicon '.$icon.'"></span>';
			break;
		default:
			$linktype = '<span class="lizicon '.$icon.'"></span><span class="navLabel">'.$navLabelText.'</span>';
			break;
	}


}
else
{
	$linktype = $navLabelText;
}

echo '<a tabindex="0">'.$linktype.'</a>';
?>
