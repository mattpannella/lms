<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//jDumpMessage('default');

// Note. It is important to remove spaces between elements.

?>
<?php // The menu class is deprecated. Use nav instead. 
?>
<ul role="menu" class="sidemenu <?php echo $class_sfx; ?>" <?php
															$tag = '';

															if ($params->get('tag_id') != null) {
																$tag = $params->get('tag_id') . '';
																echo ' id="' . $tag . '"';
															}
															?>>
	<?php
	$tabindex = 0;
	foreach ($list as $i => &$item) {
		$aria = '';
		$class = 'item-' . $item->id;
		$class .= ' axsNav';

		$tour_order = $item->params->get('tour-order');
		$tour_text = $item->params->get('tour-text');
		$tour_position = $item->params->get('tour-position');

		if ($tour_text) {
			$tour = 'data-step="' . $tour_order . '" data-intro="' . $tour_text . '" data-position="' . $tour_position . '"';
		} else {
			$tour = '';
		}

		if (($item->id == $active_id) or ($item->type == 'alias' and $item->params->get('aliasoptions') == $active_id)) {
			$class .= ' current';
		}

		if (in_array($item->id, $path)) {
			$class .= ' active';
		} elseif ($item->type == 'alias') {
			$aliasToId = $item->params->get('aliasoptions');

			if (count($path) > 0 && $aliasToId == $path[count($path) - 1]) {
				$class .= ' active';
			} elseif (in_array($aliasToId, $path)) {
				$class .= ' alias-parent-active';
			}
		}

		if ($item->type == 'separator') {
			$class .= ' divider';
		}

		if ($item->deeper) {
			$class .= ' deeper';
		}

		if ($item->parent) {
			$class .= ' submenu-parent';
			$aria = ' aria-haspopup="true" aria-expanded="false" ';
		}

		if (!empty($class)) {
			$class = ' class="' . trim($class) . '"';
		}

		echo '<li' . $class . ' ' .  $tour . ' ' . $aria . ' role="menuitem">';

		// Render the menu item.
		switch ($item->type):
			case 'separator':
			case 'url':
			case 'component':
			case 'heading':
				require JModuleHelper::getLayoutPath('mod_sidemenu', 'default_' . $item->type);
				break;

			default:
				require JModuleHelper::getLayoutPath('mod_sidemenu', 'default_url');
				break;
		endswitch;

		// The next item is deeper.
		if ($item->deeper) {
			echo '<ul class="submenu" role="menu">';
		} elseif ($item->shallower) {
			// The next item is shallower.
			echo '</li>';
			echo str_repeat('</ul></li>', $item->level_diff);
		} else {
			// The next item is on the same level.
			echo '</li>';
		}
	}
	?></ul>