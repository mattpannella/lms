<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

//jDumpMessage('default_url');

// Note. It is important to remove spaces between elements.
$class = $item->anchor_css ? 'class="' . $item->anchor_css . '" ' : '';
$title = $item->anchor_title ? 'title="' . $item->anchor_title . '" ' : '';
$rel   = $item->anchor_rel ? 'rel="' . $item->anchor_rel . '" ' : '';
$icon = $item->params->get('sidemenu-icon');
$icon_position = $item->params->get('sidemenu-icon-position');

$navLabelText = $item->language == '*' ? AxsLanguage::translate($item->title) : $item->title;

if ($icon)
{
	if (strpos($icon, '#') !== FALSE)
	{
		$text_icon = str_replace('#', '', $icon);
		$icon = '';
	}
	else
	{
		$text_icon = '';
	}

	$iconClass = "iconStyle " . $icon;

	switch ($icon_position) {
		case 'top':
			$linktype = '<span class="lizicon '.$icon.'">'.$text_icon.'</span><span class="navLabel">'.$navLabelText.'</span>';
			break;

		case 'left':
			$linktype = '<span class="lizicon '.$icon.'"></span>'.$navLabelText;
			break;

		case 'right':
			$linktype = $navLabelText.'<span class="lizicon '.$icon.'"></span>';
			break;
		default:
			$linktype = '<span class="lizicon '.$icon.'"></span><span class="navLabel">'.$navLabelText.'</span>';
			break;
	}
}
else
{
	$linktype = '<span class="navLabel">' . $navLabelText . '</span>';
}

$flink = $item->flink;
$flink = JFilterOutput::ampReplace(htmlspecialchars($flink));

switch ($item->browserNav) {
	default:
	case 0:
		?>
			<a tabindex="0" <?php echo $class; ?>href="<?php echo $flink; ?>" <?php echo $title . $rel; ?>>
				<?php echo $linktype; ?>
			</a>
		<?php
		break;
	case 1:
		// _blank
		?>
			<a tabindex="0" <?php echo $class; ?>href="<?php echo $flink; ?>" target="_blank" <?php echo $title . $rel; ?>>
				<?php echo $linktype; ?>
			</a>
		<?php
		break;
	case 2:
		// Use JavaScript "window.open"
		$options = 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,' . $params->get('window_open');

		?>
			<a tabindex="0" <?php echo $class; ?>href="<?php echo $flink; ?>" onclick="window.open(this.href,'targetWindow','<?php echo $options;?>');return false;" <?php echo $title; ?>>
				<?php echo $linktype; ?>
			</a>
		<?php
		break;
}
