<?php

defined('_JEXEC') or die;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

$contact_module_settings = new stdClass();
$contact_module_settings->contact_form_id = $params->get("contact_form_id");
$contact_module_settings->contact_form_background_color = $params->get("contact_form_background_color");
$contact_module_settings->contact_form_background_transparency = $params->get("contact_form_background_transparency");
$contact_module_settings->contact_form_text_color = $params->get("contact_form_text_color");
$contact_module_settings->contact_form_layout = $params->get("contact_form_layout");
$contact_module_settings->contact_form_padding = $params->get("contact_form_padding");
$contact_module_settings->contact_form_show_title = $params->get("contact_form_show_title");
$contact_module_settings->contact_form_show_contact_us = $params->get("contact_form_show_contact_us");
$contact_module_settings->contact_form_show_location_title = $params->get("contact_form_show_location_title");
$contact_module_settings->contact_form_submit_button_position = $params->get("contact_form_submit_button_position");
$contact_module_settings->contact_form_submit_button_icon = $params->get("contact_form_submit_button_icon");
$contact_module_settings->contact_form_submit_button_text = $params->get("contact_form_submit_button_text");
$contact_module_settings->contact_form_submit_button_background_color = $params->get("contact_form_submit_button_background_color");
$contact_module_settings->contact_form_submit_button_text_color = $params->get("contact_form_submit_button_text_color");
$contact_module_settings->contact_form_submit_button_hover_background_color = $params->get("contact_form_submit_button_hover_background_color");
$contact_module_settings->contact_form_submit_button_hover_text_color = $params->get("contact_form_submit_button_hover_text_color");
$contact_module_settings->contact_form_button_style_custom = $params->get("contact_form_button_style_custom");
include "components/com_contactpage/views/contactpages/tmpl/default.php";

?>