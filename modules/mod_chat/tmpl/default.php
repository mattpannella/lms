<?php defined('_JEXEC') or die; 
require_once('components/shared/controllers/comments.php');
require_once('components/shared/controllers/rating.php');

$doc = JFactory::getDocument();
$doc->addStyleSheet('modules/mod_chat/assets/css/chat.css');
?>

<div class="discussion">
	<div class="clearfix"></div>
	<?php 
		$comment_id = "chat_" . $chat_id;

		$params = new stdClass();

		$params->title = "Discussion";
		$params->button_title = "Send";
		$params->placeholder = "Write Something...";
		$params->auto_update = true;
		$params->post_id = $comment_id;
		
		display_comments($params);
	?>
	<div class="clearfix"></div>
</div>
<script>
$(document).ready(function(){
	function setChatHeight() {
		var container = $('.discussion').closest('.dashboard_module');
		var maxHeight = container.height() - 110;
		$('.comments_area').css('maxHeight',maxHeight);
	}

	setChatHeight();
	$(window).resize(function(){
		setChatHeight();
	});
});
</script>

