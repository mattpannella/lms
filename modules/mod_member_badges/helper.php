<?php

class ModMemberBadgesHelper {
	static function makeBadges() {		

        if (class_exists('CFactory')) {
            $profileId = CFactory::getActiveProfile()->id;
        } else {
            $profileId = null;
        }

        $dataUser = AxsExtra::getUserLevels();
        
        ?>
            <script>
                displayBadges(<?php echo "'" . json_encode($dataUser) . "', 'user'"?>);
            </script>
        <?php

        if ($profileId != null) { 
                        
            $dataProfile = AxsExtra::getUserLevels($profileId);

            ?>
                <script>
                    displayBadges(<?php echo "'" . json_encode($dataProfile) . "', 'profile'"?>);
                </script>
            <?php
        }        
	}
}
