<?php
defined('_JEXEC') or die;

require_once ('modules/mod_member_badges/helper.php');

?>

	<link rel="stylesheet" type="text/css" href="modules/mod_member_badges/assets/css/member_badges.css">
	<script type="text/javascript" src="modules/mod_member_badges/assets/js/member_badges.js"></script>

<?php
	$app = JFactory::getApplication();
	$Itemid = $app->getMenu()->getActive()->id;
?>

<script> 
	setItemid(<?php echo $Itemid ?>);
</script>

<?php    
	$memberBadges = ModMemberBadgesHelper::makeBadges();
	require JModuleHelper::getLayoutPath('mod_member_badges');