var Itemid;
function setItemid(newId) {
	Itemid = newId;
}

function displayBadges(info, type) {

	var userData = JSON.parse(info);

	/*
		userData

		affiliate.level
		affiliate.name

		member.level
		member.name

		recognitions (array)
			[].id
			[].name
	*/

	var runCreate = false;

	var badgesParent = null;

	if (type == 'profile') {		

		var checkForImage = setInterval(
			function() {
				var badges = document.getElementsByClassName('joms-focus__badges');

				var imageSpace;

				if (badges.length > 0) {
					for (i = 0; i < badges.length; i++) {
						imageSpace = badges[i];
					}

					badgesParent = imageSpace.parentNode;
					badgesParent.removeChild(imageSpace);

					stopChecking();
					createBadges();
				}

				//If the page has fully loaded, and the profile image hasn't been found, it isn't there.  So... stop.
				$(document).ready(
					function() {						
						stopChecking();
					}
				);
			}, 10
		);

		var stopChecking = function() {
			clearInterval(checkForImage);
		}
	} else {
		runCreate = true;
		badgesParent = document.getElementById('member_badges_wrapper');
	}

	var createBadges = function() {
		
		var numBadges = 0;
		var badges = document.createElement('div');

		badges.id = "member_badges_" + type;

		var image;

		var imageContainer = document.createElement('div');
		imageContainer.className = "member_badge_image_" + type;

		var createImage = function(source, title) {

			if (source == "" || source == null) {
				return;
			}

			if (title == "" || title == null || title == 'undefined' || title === undefined) {
				return;
			}

			badges.appendChild(imageContainer);

			image = document.createElement('img');
			image.title = title;
			image.src = source;
			if (type == 'user') {
				$(image).css('width', '0px');
			}
			imageContainer.appendChild(image);

			numBadges++;
		}
		
		if (userData.affiliate !== undefined) {
			createImage(userData.affiliate.image, userData.affiliate.name);
		}

		if (userData.member !== undefined) {			
			createImage(userData.member.image, userData.member.name);
		}

		/*
			13  -   Inner Circle
            20  -   Sanctioned Speaker
            25  -   Visionary
            26  -   Founder            
            28  -   Translators
            29  -   PSP Sanctioned Speaker
            30  -   PSP Certified
        */

		if (userData.recognitions !== undefined) {
			for (i = 0; i < userData.recognitions.length; i++) {
				var rec = userData.recognitions[i];
				if (rec.image != null && rec.image != "") {									
					createImage(rec.image, rec.name);
				}
			}
		}

		/*
			If the type is "user" then it is on the front page and the CSS needs to be made dynamically.
		*/

		if (type == "user") {
			//$("#user_language_selector");
			$(window).ready(
				function() {

					var sizeBadges = function() {

						var max_width = 50;
						var min_width = 18;

						var flag_selector = document.getElementById("user_language_selector");		//Don't let the badges extend beyond the language selector
						if(flag_selector) {
							var rect = flag_selector.getBoundingClientRect();							//Get the bounding box for the selector element
							var width = rect.left - 145;												//Find the available width.  The title is 145px
						} else {
							var width = 150;
						}
						var margin_left = 0;
						var margin_top = 0;
						var image_width = 0;

						if (width > 200) {

							margin_left = Math.floor(width / 150);									//Make a margin-left spacing for each badge.							

							image_width = Math.floor(width / numBadges) - margin_left;				//Calculate the image width.

							if (image_width > max_width) {														//Limit the size to 50 (the top border is 60)
								image_width = max_width;
							} else if (image_width < min_width) {
								image_width = min_width;
							}
							
							margin_top = Math.floor((60 - image_width) / 2);							//Center the badges

							if (margin_top > 10) {
								margin_top = 10;
							}
						} else {

							if (screen.width >= 360) {
								image_width = min_width;
								margin_top = 2;
								margin_left = 2;
							} else {
								image_width = 0;
							}

						}

						$(".member_badge_image_user img").css({
							"width": image_width + "px",
							"margin-top": margin_top + "px",
							"margin-left": margin_left + "px"
						});



					}

					sizeBadges();
					$(window).resize(
						function() {
							sizeBadges();
						}
					);
				}
			);
		}

		badgesParent.appendChild(badges);
		badges.appendChild(document.createElement('br'));

	}

	if (runCreate == true) {
		createBadges();
	}

}
