<?php defined('_JEXEC') or die; ?>

<style>
	.lang-select {
		color: inherit;
	}
	.lang-select:hover {
		color: inherit;
	}

	@media only screen and (max-width: 500px) {
		.language_text {
			display: none;
		}
	}
	.open {
		font-size: inherit;
		color: inherit;
	}
	.open:hover {
		color: inherit;
		font-size: inherit;
	}

	.language-text {
		color: #404040;
		font-size: 12px;
	}

	.language-text:hover {
		color: #FF4040;
	}
</style>

<?php
	$languages = AxsLanguage::getPublishedLanguages();

	$curTag = JFactory::getLanguage()->get('tag');
	foreach ($languages as $language) {
		if ($language->lang_code == $curTag) {
			$current = $language;
			break;
		}
	}

?>

<div id="user_language_selector" class="dropdown lang-select">
	<a href="javascript:void(0)" class="dropdown-toggle lang-select" data-toggle="dropdown">
		<span class="flag_image">
			<img src="media/mod_languages/images/<?php echo $current->image;?>.gif" style="display:inline;">
		</span>
		<span class="language_text"><?php echo " " . $current->title_native;?></span>		
	</a>
	<ul class="dropdown-menu" style="min-width: inherit">
		<?php

			// Get uri components, https://www.php.net/manual/en/function.parse-url.php
			$url_components = parse_url($_SERVER['REQUEST_URI']);

			// Remove the language sef from the path if it exists
			$uri_path_array = explode('/', $url_components['path']);
			foreach ($languages as $langauge) {
				if ($uri_path_array[1] === $language->sef) {
					$url_components['path'] = implode('/', array_slice($uri_path_array, 2));
				} else {
					$url_components['path'] = implode('/', array_slice($uri_path_array, 1));
				}
			}

			// Remove any XSS vulnerabilities that can't be caught by htmlspecialchars
			// SEC-165
			$query_params = array();
			foreach (explode('&', $url_components['query']) as $query_param) {
				$param_value = explode('=', $query_param)[1];
				if (strpos(strtolower($param_value), 'javascript:') === false) {
					$query_params []= $query_param;
				}
			}
			$url_components['query'] = implode('&', $query_params);
			if (strlen($url_components['query'] ) > 0) {
				$url_components['query'] = '?' . $url_components['query'];
			}
			
			// Get an array of associated menu items
			$associations_enabled = JLanguageAssociations::isEnabled();
			$active_menu_item = JFactory::getApplication()->getMenu()->getActive();
			$associations = array();
			if ($associations_enabled && $active_menu_item)	{
				$associations = MenusHelper::getAssociations($active_menu_item->id);
			}


			// Render the dropdown, prepending the language->sef to the modified
			// uri path and query for each dropdown item href
			foreach ($languages as $language) { 
				if (in_array($language->lang_code, AxsBrands::getBrand()->site_details->language_selector_options)):

					$itemid = $associations[$language->lang_code];
					if ($itemid) {
						$href = JRoute::_('index.php?lang=' . $language->sef . '&Itemid=' . $itemid);
					} else {
						$href = $language->sef . '/' . $url_components['path'] . $url_components['query'];
					}
					$selected_style = "";
					
					if ($current->title_native == $language->title_native) {
						$selected_style = 'background-color: #EFFBFF;"';
					}

					?>
						<li>
							<a class='lang-selector' lang='<?php echo $language->sef;?>'>
								<a href="<?php echo $href ?>" style="padding-left: 3px !important; <?php echo $selected_style;?>">
									<span class='language-text'>
										<img src="media/mod_languages/images/<?php echo $language->image;?>.gif"><?php echo " " . $language->title_native;?>
									</span>
								</a>
							</a>
						</li>
					<?php
				endif;				
			}
		?>
	</ul>
</div>
