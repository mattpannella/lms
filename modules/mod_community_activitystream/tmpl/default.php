<?php
/**
* @copyright (C) 2015 iJoomla, Inc. - All rights reserved.
* @license GNU General Public License, version 2 (http://www.gnu.org/licenses/gpl-2.0.html)
* @author iJoomla.com <webmaster@ijoomla.com>
* @url https://www.jomsocial.com/license-agreement
* The PHP code portions are distributed under the GPL license. If not otherwise stated, all images, manuals, cascading style sheets, and included JavaScript *are NOT GPL, and are released under the IJOOMLA Proprietary Use License v1.0
* More info at https://www.jomsocial.com/license-agreement
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' ); ?>


<?php if($user->isOnline()):?>

<?php else:?>

<?php endif;?>

<div id="activity-stream-container" class="joms-module scrolling">
    <?php echo $stream; ?>
</div>
<script>
	/* $(document).ready(function (){
		var button = '<span class="btn btn-default feed-control" style="position:absolute; top:15px; right:40px;" data-control="play"><span class="lizicon-pause"></span> Play</span>';
		$('#activity-stream-container').closest('.dashboard_module').append(button);
		var module_height = $('.scrolling').closest('.dashboard_module').height();
		
		$('.scrolling').css('height',module_height+'px');
		$('.scrolling').css('overflow','auto');

		var autoFeed;
		function runFeed() {
			autoFeed = setInterval(function (){ playFeed() }, 10000);
		}

	 	function playFeed() {
	 		$.ajax({
		    url: 'index.php?option=com_axs&task=feed.community&format=raw',
		    
		    type: 'get'
		    }).done(function(result) {

		    	document.getElementById('activity-stream-container').innerHTML = result;
		        
		    });
		}

		function pauseFeed() {
			clearInterval(autoFeed);
		}

		//runFeed();

		$('.feed-control').click(function(){
			var control = $('.feed-control').data('control');
			if(control == 'play') {
				runFeed();
				$('.feed-control').data('control', 'pause');
				$('.feed-control').html('<span class="lizicon-pause"></span> Pause');
			} else if(control == 'pause'){
				pauseFeed();
				$('.feed-control').data('control', 'play');
				$('.feed-control').html('<span class="lizicon-play2"></span> Play');
		
			}
		});

		$('#activity-stream-container').click(function(){
			pauseFeed();
			$('.feed-control').data('control', 'play');
			$('.feed-control').html('<span class="lizicon-play2"></span> Play');
		});
		

	});	 */
		
</script>
