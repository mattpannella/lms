<?php
/*------------------------------------------------------------------------
# JBusinessDirectory
# author CMSJunkie
# copyright Copyright (C) 2012 cmsjunkie.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.cmsjunkie.com
# Technical Support:  Forum - http://www.cmsjunkie.com/forum/j-businessdirectory/?p=1
-------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');
?>

<div class="latestbusiness<?php echo $moduleclass_sfx; ?>" itemscope itemtype="http://schema.org/ItemList">
	<ul>
		<?php foreach ($items as $company) : ?>
			<li itemscope itemprop="itemListElement" itemtype="http://schema.org/Organization">
				<div class="business-logo" itemprop="logo" itemscope itemtype="http://schema.org/ImageObject">
					<a href="<?php echo JBusinessUtil::getCompanyLink($company)?>">
						<?php if(isset($company->logoLocation) && $company->logoLocation!='') { ?>
							<img title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.$company->logoLocation ?>" itemprop="contentUrl">
						<?php } else { ?>
							<img title="<?php echo $company->name?>" alt="<?php echo $company->name?>" src="<?php echo JURI::root().PICTURES_PATH.'/no_image.jpg' ?>" itemprop="contentUrl">
						<?php } ?>
					</a>
				</div>
				<div class="company-info">				
					<a class="company-name" href="<?php echo JBusinessUtil::getCompanyLink($company); ?>">
						<span itemprop="name"><?php echo $company->name; ?></span>
					</a>
					<span class="company-address" itemprop="address">
						<i class="dir-icon-map-marker"></i> <?php echo JBusinessUtil::getAddressText($company)?>
					</span>				
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
	<div class="clear"></div>
</div>

<script>
jQuery(document).ready(function(){
	<?php 
	$load = JRequest::getVar("latitude");
	if($params->get('geo_location') && empty($load)){ ?>
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(addCoordinatesToUrl);
		}
	<?php } ?>
});
</script>