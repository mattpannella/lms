<?php

/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
// Initialize framework
const _JEXEC = 1;

//load system defines
define('JPATH_BASE', realpath(__DIR__.'/..'));

require_once JPATH_BASE . '/includes/defines.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';
require_once ( JPATH_BASE. '/includes/framework.php' );
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();
JLoader::discover('Axs', JPATH_LIBRARIES . '/axslibs');


$clientId = AxsClients::getClientId();
$key = AxsKeys::getKey('lms');
$fileParam = $_GET['file'];
$encryptedParams = base64_decode($fileParam);
$decryptedParams = AxsEncryption::decrypt($encryptedParams, $key);

$allowedTime = 7200;
$timeDifference = time() - $decryptedParams->timestamp;

$session = JFactory::getSession();
$access_user_id = $session->get('access_user_id');

if($timeDifference > $allowedTime || !$decryptedParams->timestamp || $access_user_id != $decryptedParams->user_id) {
    echo 'You do not have access to this content. Please login to access.';
    exit;
}
$files = glob('/mnt/shared/'.$clientId.'/files/secure_file_locker/*');
$file = '/mnt/shared/'.$clientId.'/files/secure_file_locker/'.$decryptedParams->file;
$fileInfo = pathinfo($file);


if(!$file || $fileInfo['extension'] != 'mp4') {
    include_once '/var/www/html/templates/axs/404.php';
    exit;
}

$fp = @fopen($file, 'rb');

$size   = filesize($file); // File size
$length = $size;           // Content length
$start  = 0;               // Start byte
$end    = $size - 1;       // End byte

header('Content-type: video/mp4');
header("Accept-Ranges: 0-$length");
if (isset($_SERVER['HTTP_RANGE'])) {

    $c_start = $start;
    $c_end   = $end;

    list(, $range) = explode('=', $_SERVER['HTTP_RANGE'], 2);
    if (strpos($range, ',') !== false) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header("Content-Range: bytes $start-$end/$size");
        exit;
    }
    if ($range == '-') {
        $c_start = $size - substr($range, 1);
    }else{
        $range  = explode('-', $range);
        $c_start = $range[0];
        $c_end   = (isset($range[1]) && is_numeric($range[1])) ? $range[1] : $size;
    }
    $c_end = ($c_end > $end) ? $end : $c_end;
    if ($c_start > $c_end || $c_start > $size - 1 || $c_end >= $size) {
        header('HTTP/1.1 416 Requested Range Not Satisfiable');
        header("Content-Range: bytes $start-$end/$size");
        exit;
    }
    $start  = $c_start;
    $end    = $c_end;
    $length = $end - $start + 1;
    fseek($fp, $start);
    header('HTTP/1.1 206 Partial Content');
}

header("Content-Range: bytes $start-$end/$size");
header("Content-Length: ".$length);


$buffer = 1024 * 8;
while(!feof($fp) && ($p = ftell($fp)) <= $end) {

    if ($p + $buffer > $end) {
        $buffer = $end - $p + 1;
    }
    set_time_limit(0);
    echo fread($fp, $buffer);
    flush();
}

fclose($fp);
exit();