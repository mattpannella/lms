<?php

/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
// Initialize framework
const _JEXEC = 1;

//load system defines
define('JPATH_BASE', realpath(__DIR__.'/..'));

require_once JPATH_BASE . '/includes/defines.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';
require_once ( JPATH_BASE. '/includes/framework.php' );
$mainframe = JFactory::getApplication('site');
$mainframe->initialise();
JLoader::discover('Axs', JPATH_LIBRARIES . '/axslibs');


$user = JFactory::getUser();
$clientId = AxsClients::getClientId();
$key = AxsKeys::getKey('lms');
$fileParam = $_GET['file'];
if(strpos($fileParam,'?v=')) {
    $fileParts = explode('?v=',$fileParam)[0];
}

$encryptedParams = base64_decode($fileParam);
$decryptedParams = AxsEncryption::decrypt($encryptedParams, $key);

$allowedTime = 120;
$timeDifference = time() - $decryptedParams->timestamp;

$session = JFactory::getSession();
$access_user_id = $session->get('access_user_id');
if($timeDifference > $allowedTime || !$decryptedParams->timestamp) {
    echo 'You do not have access to this content. Please login to access.';
    exit;
}
$files = glob('/mnt/shared/'.$clientId.'/files/secure_file_locker/*');
$file = '/mnt/shared/'.$clientId.'/files/secure_file_locker/'.$decryptedParams->file;
$fileInfo = pathinfo($file);

if( ($fileInfo['extension'] != 'ppt' && $fileInfo['extension'] != 'pptx') && ($access_user_id != $decryptedParams->user_id) ) {
    echo 'You do not have access to this content. Please login to access.';
    exit;
}

if(!$file) {
    include_once '/var/www/html/templates/axs/404.php';
    exit;
}
$valid = false;
if(in_array($file,$files) && AxsFiles::getLockerFile($decryptedParams->file)) {

    switch($fileInfo['extension']) {
        case "ppt":
            $valid = true;
            header('Content-Type: application/vnd.ms-powerpoint');
        break;
        case "pptx":
            $valid = true;
            header('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
        break;
        case "pdf":
            $valid = true;
            header('Content-Type: application/pdf');
        break;
        case "mp3":
            $valid = true;
            header('Content-Type: audio/mp3');
        break;
    }
    if($valid) {
        readfile($file);
    } else {
        include_once '/var/www/html/templates/axs/404.php';
        exit;
    }

} else {
    include_once '/var/www/html/templates/axs/404.php';
    exit;
}