<?php

abstract class API {
	/**
	 * Property: method
	 * The HTTP method this request was made in, either GET, POST, PUT or DELETE
	 */
	public $method = '';
	/**
	 * Property: endpoint
	 * The Model requested in the URI. eg: /files
	 */
	public $endpoint = '';
	

	/**
	 * Property: args
	 * Any additional URI components after the endpoint and verb have been removed, in our
	 * case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1>
	 * or /<endpoint>/<arg0>
	 */
	public $args = Array();
	/**
	 * Property: items
	 * an array of ids for specific items
	 * example: user id or course id
	 */
	public $items = null;
	/**
	 * Property: tasks
	 * An optional additional descriptor about the endpoint, used for things that can
	 * not be handled by the basic methods. eg: /files/process
	 */
	public $tasks = null;
	/**
	 * Property: body
	 * Stores the input of the PUT request
	 */
	public $filter = null;
	public $body = null;

	/**
	 * Property: authorization
	 * This is the value that was set in the authorization header. Should be a JWT.
	 */
	protected $public_key = null;
	protected $secret_key = null;

	/**
	 * Property: permissions
	 * These are the permissions for API Access
	 */
	public $permissions = null;

	/**
	 * Constructor: __construct
	 * Allow for CORS, assemble and pre-process the data
	 */
  	public function __construct($request) {    
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
		header ("Access-Control-Allow-Headers: *");
		$headers = apache_request_headers();
		if($headers['X-API-KEY']) {
			$this->secret_key = $headers['X-API-KEY'];
		} else {
			$this->secret_key = $headers['x-api-key'];
		}
		if($headers['X-PUBLIC-KEY']) {
			$this->public_key = $headers['X-PUBLIC-KEY'];
		} else {
			$this->public_key = $headers['x-public-key'];
		}

		if(!$this->secret_key) {
			if($_GET['x-api-key']) {
				$this->secret_key = preg_replace("/[^A-Za-z0-9_ ]/","", $_GET['x-api-key']);
			}
		}

		if(!$this->public_key) {
			if($_GET['x-public-key']) {
				$this->public_key = preg_replace("/[^A-Za-z0-9_ ]/","", $_GET['x-public-key']);
			}
		}		
		
		$this->authenticate();	
		
		$this->args = explode('/', rtrim($request, '/'));
		if(isset($this->args[0])) {
			$this->endpoint = $this->args[0];
		} else {
			throw new Exception($this->_requestStatus(405));
		}
		$task = null;
		foreach($this->args as $param) {
			if(isset($param)) {
				if(is_numeric($param)) {
					if($task) {
						$this->items[$task] = $param;
					}
				} else {
					$this->tasks[] = $param;
					$task = $param;
					
				}
			}
		}
		$this->method = $_SERVER['REQUEST_METHOD'];
		if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
			if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
				$this->method = 'DELETE';
			} else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
				$this->method = 'PUT';
			} else {
				throw new Exception("Unexpected Header");
			}
		}

		switch($this->method) {
			case 'DELETE':
				$this->filter = $this->_cleanInputs($_REQUEST);
				$this->body = $this->_cleanBody(file_get_contents("php://input"));
				break;
			case 'POST':
				$this->filter = $this->_cleanInputs($_REQUEST);
				$this->body = $this->_cleanBody(file_get_contents("php://input"));
				break;
			case 'GET':
				$this->filter = $this->_cleanInputs($_REQUEST);
				$this->body = $this->_cleanBody(file_get_contents("php://input"));
				break;
			case 'PUT':
				$this->filter = $this->_cleanInputs($_REQUEST);
				$this->body = $this->_cleanBody(file_get_contents("php://input"));
				break;
			case 'PATCH':
				$this->filter = $this->_cleanInputs($_REQUEST);
				$this->body = $this->_cleanBody(file_get_contents("php://input"));
				break;
			default:
				$this->_response('Invalid Method', 405);
				break;
		}
  	}

  	public function processAPI() {
		if(method_exists($this, $this->endpoint)) {
			return $this->_response($this->{$this->endpoint}($this->args));
		}
		return $this->_response("Endpoint Does Not Exist", 404);
	}

  	public function authenticate() {     
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select($db->quoteName('secret_key').','.$db->quoteName('permissions'));
		$query->from($db->quoteName('axs_api_keys'));
		$query->where($db->quoteName('public_key').' = '.$db->quote($this->public_key));
		$db->setQuery($query);
		$key = $db->loadObject();
		if(password_verify($this->secret_key,  $key->secret_key)) {
			$this->permissions = json_decode($key->permissions);
		} else {			
			throw new Exception('Access Forbidden');
		}
		/* foreach($keys as $key) {		
			if(password_verify($this->secret_key,  $key->secret_key)) {
				$this->permissions = json_decode($key->permissions);
				$valid = true;
			}
			if(!$valid) {
				
				throw new Exception('Access Forbidden');
			}              
		} */
	}

	protected function hasAllInputs($neededData) {
		foreach($neededData as $data) {
			if(!property_exists($this->body, $data)) throw new Exception("Necessary data missing from request.");
		}
	}

	private function _response($data, $status = 200) {
		header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
		return json_encode($data, JSON_PRETTY_PRINT);
	}

	private function _cleanInputs($data) {
		$clean_input = Array();
		if (is_array($data)) {
			foreach ($data as $k => $v) {
				$clean_input[$k] = $this->_cleanInputs($v);
			}
		} else {
			$clean_input = trim(strip_tags($data));
		}
		return $clean_input;
	}

	private function _cleanBody($data) {
		if($data != '') {
			$result = json_decode($data);
			if($result == null) {
				//an error occurred, throw exception
				throw new Exception("The body of your request is not valid JSON.");
			} else {
				return $result;
			}
		} else {
			return new stdClass();
		}
	}

	public function _requestStatus($code) {
		$status = array(
			200 => 'OK',
			404 => 'Not Found',
			403 => 'Forbidden',
			405 => 'Method Not Allowed',
			500 => 'Internal Server Error',
		);
		return ($status[$code])?$status[$code]:$status[500];
	}

	public function setType($item, $type) {
		switch ($type) {
			case 'int':
				return (int) $item;
			break;

			case 'bool':
				return (bool) $item;
			break;

			case 'float':
				return (float) $item;
			break;

			case 'string':
				return (string) $item;
			break;

			case 'array':
				return (array) $item;
			break;

			case 'object':
				return (object) $item;
			break;
			
			default:
				(string) $item;
			break;
		}
	}

    /**
     * Build a string of quoted filter params of a specified type
     *
     * @param Array $items Items of type $type as described in the $type param
     * @param string $type Describes the PHP type that should be used for parameter filtering ('string' is the default)
     * @return string Filtered and quoted list of API parameters
     */
	public function quoteFilterList($items, $type = 'string') {
		$db = JFactory::getDbo();

		if(!is_array($items)) {
			$items = explode(',',$items);
		}

        foreach($items as $item) {

			if($item) {

				$itemSet = $this->setType($item, $type);

				if($itemSet) {
					$outputArray[] = $db->quote($itemSet);
				}				
			}			
		}

        $output = implode(',',$outputArray);

		return $output;
	}
}