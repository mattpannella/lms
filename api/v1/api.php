<?php
header('Content-Type: application/json');
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
// Initialize framework
const _JEXEC = 1;

//load system defines
define('JPATH_BASE', realpath(__DIR__.'/../..'));

require_once JPATH_BASE . '/includes/defines.php';

// Get the framework.
require_once JPATH_LIBRARIES . '/import.legacy.php';

// Bootstrap the CMS libraries.
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_CONFIGURATION . '/configuration.php';
JLoader::discover('Axs', JPATH_LIBRARIES . '/axslibs');
//Begin API code.
require_once 'TovutiAPI.class.php';

// Requests from the same server don't have a HTTP_ORIGIN header
if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
  $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
}

try {
  $API = new TovutiAPI($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
  echo $API->processAPI();
} catch (Exception $e) {
  echo json_encode(Array('error' => $e->getMessage()));
}