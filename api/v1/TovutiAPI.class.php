<?php
require_once 'API.class.php';
require_once 'endpoints/users.php';
require_once 'endpoints/awards.php';
require_once 'endpoints/courses.php';
require_once 'endpoints/events.php';
require_once 'endpoints/teams.php';
require_once 'endpoints/meetings.php';
require_once 'endpoints/checklists.php';
require_once 'endpoints/assignments.php';
require_once 'endpoints/quizzes.php';
require_once 'endpoints/certificatetemplates.php';
require_once 'endpoints/reports.php';
require_once 'endpoints/subscriptions.php';

class TovutiAPI extends API {

	public $forbiddenGroups = [8];
	public $forbiddenAccessLevels = [3];
	public $forbiddenUsers = [601];

	public function __construct($request) {
		parent::__construct($request);
	}

	public function checkPermissions($action,$section) {
		if(
			(
				empty($this->permissions->$section) ||
				!in_array(strtolower($action), $this->permissions->$section)
			) && !$this->permissions->all_permissions
		) {
			throw new Exception('Access Forbidden');
		}
	}

	public function domainRoot() {
		return JUri::getInstance()->toString(['scheme', 'host']) . '/';
	}

	protected function Shop() {
		$allowedTasks = [];

		$shop = new Shop($this);

		switch($this->method) {
			case 'GET':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'items':
							return $shop->getItems();
						break;
						case 'orders':
							return $shop->getOrders();
						break;
					}
				}
			break;

			case 'POST':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'items':
							//$this->checkPermissions('create_checklist', 'checklists');
							return $shop->createItem();
						break;
						case 'orders':
							//$this->checkPermissions('create_checklist', 'checklists');
							return $shop->createOrder();
						break;
					}
				}
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Users() {
		$allowedTasks = [
			'certificates',
			'courses',
			'course_progress',
			'badges',
			'milestones',
			'lessons',
			'purchases',
			'checklists',
			'checklist_progress',
			'assignments',
			'events',
			'virtual_classes',
			'awards',
			'groups',
			'create',
			'quizresults'
		];

		if(!empty($this->tasks[1]) && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		$users = new Users($this);

		switch($this->method) {
			case 'GET':
				if(isset($this->tasks[1])) {
					switch ($this->tasks[1]) {
						case 'certificates':
							$this->checkPermissions('get_users_certificates','users');
							return $users->getUserAwards('certificate');
						break;

						case 'badges':
							$this->checkPermissions('get_users_badges','users');
							return $users->getUserAwards('badge');
						break;

						case 'milestones':
							$this->checkPermissions('get_users_milestones','users');
							return $users->getUserAwards('milestone');
						break;

						case 'awards':
							$this->checkPermissions('get_users_awards','users');
							return $users->getUserAwards();
						break;

						case 'courses':
							$this->checkPermissions('get_users_courses','users');
							return $users->getUserCourses();
						break;

						case 'course_progress':
							$this->checkPermissions('get_users_course_progress','users');
							return $users->getUserCourseProgress();
						break;

						case 'events':
							$this->checkPermissions('get_users_events','users');
							return $users->getUserEvents();
						break;

						case 'groups':
							$this->checkPermissions('get_users_groups', 'users');
							return $users->getUserGroups();
						break;

						case 'checklist_progress':
							$this->checkPermissions('get_users_checklist_progress', 'users');
							return $users->getUserChecklistProgress();
						break;

						case 'quizresults':
							$this->checkPermissions('get_users_quiz_results', 'users');
							return $users->getUserQuizResults();
						break;

						default:
							$this->checkPermissions('get_users','users');
							return $users->getUsers();
						break;
					}
				} else {
					$this->checkPermissions('get_users','users');
					return $users->getUsers();
				}

			break;

			case 'POST':
				if(isset($this->tasks[1])) {
					switch ($this->tasks[1]) {
						case 'create':
							$this->checkPermissions('create_user','users');
							return $users->createUser();
						break;

						case 'groups':
							$this->checkPermissions('create_user_group','users');
							return $users->createUserGroup();
						break;

						case 'courses':
							$this->checkPermissions('create_user_course_enrollment','users');
							return $users->createUserCourseEnrollment();
						break;
					}
				}
			break;

			case 'PATCH':
				if(isset($this->tasks[1])) {
					switch ($this->tasks[1]) {

						case 'groups':
							$this->checkPermissions('edit_user_group','users');
							return $users->editUserGroup();
						break;

						default:
							$this->checkPermissions('edit_user','users');
							return $users->editUser();
						break;
					}
				} else {
					$this->checkPermissions('edit_user','users');
					return $users->editUser();
				}
			break;

			case 'DELETE':
				if(isset($this->tasks[1])) {
					switch($this->tasks[1]) {
						case 'groups':
							$this->checkPermissions('delete_user_group', 'users');
							return $users->deleteUserGroup();
						break;
					}
				} else {
					$this->checkPermissions('delete_user', 'users');
					return $users->deleteUser();
				}
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Awards() {
		$allowedTasks = [
			'certificates',
			'badges',
			'milestones',
			'all'
		];

		if(!empty($this->tasks[1]) && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		$awards = new Awards($this);

		switch($this->method) {
			case 'GET':
				if(!empty($this->tasks[1])) {
					switch ($this->tasks[1]) {
						case 'certificates':
							$this->checkPermissions('get_certificates','awards');
							return $awards->getAwards('certificate');
						break;

						case 'badges':
							$this->checkPermissions('get_badges','awards');
							return $awards->getAwards('badge');
						break;

						case 'milestones':
							$this->checkPermissions('get_milestones','awards');
							return $awards->getAwards('milestone');
						break;

						case 'all':
						default:
							$this->checkPermissions('get_awards','awards');
							return $awards->getAwards();
						break;
					}
				} else {
					$this->checkPermissions('get_awards','awards');
					return $awards->getAwards();
				}
			break;

			case 'PATCH':

				if(!empty($this->tasks[1])) {
					switch($this->tasks[1]) {
						case 'certificates':
							$this->checkPermissions('edit_certificate','awards');
							return $awards->editAward('certificate');
						break;

						case 'badges':
							$this->checkPermissions('edit_badge','awards');
							return $awards->editAward('badge');
						break;

						case 'milestones':
							$this->checkPermissions('edit_milestone','awards');
							return $awards->editAward('milestone');
						break;

						default:
							$this->checkPermissions('edit_award','awards');
							return $awards->editAward();
						break;
					}
				} else {
					$this->checkPermissions('edit_award','awards');
					return $awards->editAward();
				}

			case 'POST':

				if(!empty($this->tasks[1])) {
					switch($this->tasks[1]) {

						case 'certificates':
							$this->checkPermissions('create_certificate','awards');
							return $awards->createAward('certificate');
						break;

						case 'badges':
							$this->checkPermissions('create_badge','awards');
							return $awards->createAward('badge');
						break;

						case 'milestones':
							$this->checkPermissions('create_milestone','awards');
							return $awards->createAward('milestone');
						break;
					}
				}
			break;

			case 'DELETE':
				if(!empty($this->tasks[1])) {
					switch($this->tasks[1]) {
						case 'certificates':
							$this->checkPermissions('delete_certificate', 'awards');
							return $awards->deleteAward('certificate');
						break;

						case 'badges':
							$this->checkPermissions('delete_badge', 'awards');
							return $awards->deleteAward('badge');
						break;

						case 'milestones':
							$this->checkPermissions('delete_milestone', 'awards');
							return $awards->deleteAward('milestone');
						break;

						default:
							$this->checkPermissions('delete_award', 'awards');
							return $awards->deleteAward();
						break;
					}
				} else {
					$this->checkPermissions('delete_award', 'awards');
					return $awards->deleteAward();
				}
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Courses() {
		$allowedTasks = [
			'categories',
			'registrants'
		];

		if($this->tasks[1] && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		$courses = new Courses($this);

		switch($this->method) {

			case 'GET':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'categories':
							$this->checkPermissions('get_categories', 'courses');
							return $courses->getCoursesCategories();
						break;

						case 'registrants':
							$this->checkPermissions('get_registrants', 'registrants');
							return $courses->getCourseRegistrants();
						break;

						default: {break;}
					}
				} else {
					$this->checkPermissions('get_courses','courses');
					return $courses->getCourses();
				}
			break;

			case 'POST':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'categories':
							$this->checkPermissions('create_category', 'courses');
							return $courses->createCourseCategory();
						break;

						default: {break;}
					}
				}
			break;

			case 'DELETE':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'categories':
							$this->checkPermissions('delete_category', 'courses');
							return $courses->deleteCourseCategory();
						break;

						default: {break;}
					}
				}
			break;

			case 'PATCH':
				if($this->tasks[1]) {
					switch($this->tasks[1]) {
						case 'categories':
							$this->checkPermissions('edit_category', 'categories');
							return $courses->editCourseCategory();
						break;

						default: {break;}
					}
				}
			break;

			default: {break;}
		}
	}

	protected function Events() {

		$events = new Events($this);

		switch($this->method) {

			case 'GET':
				$this->checkPermissions('get_events', 'events');
				return $events->getEvents();
			break;

            case 'PATCH':
                $this->checkPermissions('edit_events', 'events');
                return $events->updateEvents();
            break;

            case 'DELETE':
                $this->checkPermissions('delete_events', 'events');
                return $events->deleteEvents();
            break;

            case 'POST':
                $this->checkPermissions('create_event', 'events');
                return $events->createEvent();
            break;

            default: break;
		}
	}

	protected function Teams() {
		$allowedTasks = [
            'add_users',
            'remove_users',
            'add_user_groups',
            'remove_user_groups',
            'edit_team_lead_permissions'
        ];

		$teams = new Teams($this);

        if(!empty($this->tasks[1]) && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		switch($this->method) {

			case 'GET':
				$this->checkPermissions('get_teams', 'teams');
				return $teams->getTeams();
			break;

			case 'PATCH':
                if(!empty($this->tasks[1])) {

					switch($this->tasks[1]) {

						case 'add_users':

							$this->checkPermissions('add_users', 'teams');
							return $teams->addUsersToTeam();
						break;

                        case 'remove_users':

                            $this->checkPermissions('remove_users', 'teams');
                            return $teams->removeUsersFromTeam();
                        break;

                        case 'add_user_groups':

                            $this->checkPermissions('add_user_groups', 'teams');
                            return $teams->addUserGroupsToTeam();
                        break;

                        case 'remove_user_groups':

                            $this->checkPermissions('remove_user_groups', 'teams');
                            return $teams->removeUserGroupsFromTeam();
                        break;

                        case 'edit_team_lead_permissions':

                            $this->checkPermissions('edit_team_lead_permissions', 'teams');
                            return $teams->editTeamLeadPermissions();
                        break;
					}
				} else {
                    $this->checkPermissions('edit_team', 'teams');
				    return $teams->editTeam();
                }

			break;

			case 'POST':
				$this->checkPermissions('create_team', 'teams');
				return $teams->createTeam();
			break;

			case 'DELETE':
				$this->checkPermissions('delete_team', 'teams');
				return $teams->deleteTeam();
			break;

			default: {break;}
		}
	}

	protected function Meetings() {
		$allowedTasks = ['attendance'];
		$task = @$this->tasks[1];

		if(!empty($task) && !in_array($task,$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		$meetings = new Meetings($this);

		switch($this->method) {
			case 'POST':
				$this->checkPermissions('create_meeting', 'meetings');
				return $meetings->createMeeting();
			break;

			case 'DELETE':
				$this->checkPermissions('delete_meeting', 'meetings');
				return $meetings->deleteMeeting();
			break;

			case 'PATCH':
				$this->checkPermissions('edit_meeting', 'meetings');
				return $meetings->editMeeting();
			break;

			case 'GET':
				if(!empty($task)) {
					switch($task) {
						case 'attendance':
							$this->checkPermissions('get_meeting_attendance', 'meetings');
							return $meetings->getMeetingAttendance();
						break;

						default: {break;}
					}
				}
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Checklists() {
		$allowedTasks = [];

		$checklists = new Checklists($this);

		switch($this->method) {
            case 'GET':
                $this->checkPermissions('get_checklists', 'checklists');
                return $checklists->getChecklists();
            break;

            case 'POST':
				$this->checkPermissions('create_checklist', 'checklists');
				return $checklists->createChecklist();
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Assignments() {
		$allowedTasks = [];

		$assignments = new Assignments($this);

		switch($this->method) {
            case 'GET':
                $this->checkPermissions('get_assignments', 'assignments');
                return $assignments->getAssignments();
            break;

			case 'POST':
				$this->checkPermissions('create_assignment', 'assignments');
				return $assignments->createAssignment();
			break;

			case 'PATCH':
				$this->checkPermissions('edit_assignment', 'assignments');
				return $assignments->editAssignment();
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

	protected function Quizzes() {

		$allowedTasks = [];

		$quizzes = new Quizzes($this);

		switch($this->method) {

			case 'GET':
				$this->checkPermissions('get_quizzes', 'quizzes');
				return $quizzes->getQuizzes();
			break;
		}
	}

	protected function CertificateTemplates() {

		$allowedTasks = [];

		$certificateTemplates = new CertificateTemplates($this);

		switch($this->method) {

			case 'POST':
				$this->checkPermissions('create_certificate_templates', 'certificate_templates');
				return $certificateTemplates->createCertificateTemplate();
			break;

			case 'DELETE':
				$this->checkPermissions('delete_certificate_templates', 'certificate_templates');
				return $certificateTemplates->deleteCertificateTemplate();
			break;

			case 'GET':
				$this->checkPermissions('get_certificate_templates', 'certificate_templates');
				return $certificateTemplates->getCertificateTemplates();
			break;

			case 'PATCH':
				$this->checkPermissions('edit_certificate_templates', 'certificate_templates');
				return $certificateTemplates->editCertificateTemplate();
			break;

			default: break;
		}
	}

	protected function Reports() {
		$allowedTasks = [
			'run',
            'list'
		];

		$reports = new Reports($this);

		if($this->tasks[1] && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		switch($this->method) {

			case 'GET':
				if(!empty($this->tasks[1])) {

					switch($this->tasks[1]) {

						case 'run':

							$this->checkPermissions('run_reports', 'reports');
							return $reports->runReports();
						break;
                        case 'list':
						default:

							$this->checkPermissions('list_reports', 'reports');
							return $reports->listReports();
						break;
					}
				}  else {

					$this->checkPermissions('list_reports', 'reports');
					return $reports->listReports();
				}
			break;

			default:
				return array('error' => $this->endpoint . ' does not support ' . $this->method . ' requests.');
			break;
		}
	}

    protected function Subscriptions() {

        $allowedTasks = [
            'add_user_groups',
            'remove_user_groups'
        ];

        $subscriptions = new Subscriptions($this);

        if(!empty($this->tasks[1]) && !in_array($this->tasks[1],$allowedTasks)) {
			throw new Exception($this->_requestStatus(405));
		}

		switch($this->method) {

			case 'GET':
				$this->checkPermissions('get_subscriptions', 'subscriptions');
				return $subscriptions->getSubscriptions();
			break;

            case 'PATCH':

                if(!empty($this->tasks[1])) {

					switch($this->tasks[1]) {

						case 'add_user_groups':

							$this->checkPermissions('add_user_groups', 'subscriptions');
							return $subscriptions->addUserGroupToSubscription();
						break;

                        case 'remove_user_groups':

                            $this->checkPermissions('remove_user_groups', 'subscriptions');
                            return $subscriptions->removeUserGroupFromSubscription();
                        break;
					}
				} else {

                    $this->checkPermissions('edit_subscriptions', 'subscriptions');
                    return $subscriptions->editSubscription();
                }
            break;

            case 'PUT':
            case 'POST':
                $this->checkPermissions('create_subscription', 'subscriptions');
                return $subscriptions->createSubscription();
            break;

            case 'DELETE':
                $this->checkPermissions('delete_subscription', 'subscriptions');
                return $subscriptions->deleteSubscription();
            break;

            default:
                $this->checkPermissions('get_subscriptions', 'subscriptions');
                return $subscriptions->getSubscriptions();
            break;
		}
    }

	/**
	 * Parses an array API parameter as a comma-separated string list containing the values in $parameter without the values
	 * in $forbiddenValues. If $forbiddenValues is empty, no subtractions will be made.
	 *
	 * @param Array $parameter API array-type parameter containing data to be stored as a comma-separated list (CSL).
	 * @param Array|null $forbiddenValues Optional array of values that will be subtracted from the $parameter array, if given.
	 *
	 * @return string CSL containing the resultant values from $parameter that are not found in $forbiddenValues.
	 */
	public static function parseArrayParameterAsString($parameter, $forbiddenValues = null) {

		$resultCSL = "";

		// If the usergroup list is an array, implode it to a string of comma-separated group IDs, excluding forbidden ones
		if(!empty($parameter) && is_array($parameter)) {

			if(!is_null($forbiddenValues)) {
				$filteredArray = array_diff($parameter, $forbiddenValues);
			} else {
				$filteredArray = $parameter;
			}

			$resultCSL = implode(',', $filteredArray);
		}

		return $resultCSL;
	}

	/**
	 * Sets the access control type and access control lists for a given parameter object.
	 * This is handy when processing an API endpoint that has to work with access control for a given item, such as virtual meetings.
	 *
	 * @param stdClass $obj Object that contains API parameters that need access control processing
	 * @param array $userList List of users that need access to a specific resource
	 * @param array $userGroupList List of user groups that need access to a specific resource
	 * @param array $accessLevelList List of user access levels that need access to a specific resource
	 *
	 * @return void
	 */
	public function processAccessControlParameters(&$obj, $userList = null, $userGroupList = null, $accessLevelList = null) {

		if(!empty($userList) || !empty($userGroupList) || !empty($accessLevelList)) {

			// Process the access type and users, groups or access levels, depending on which type
			switch($obj->access_type) {
				case 'users':
					$obj->userlist = $this->parseArrayParameterAsString($userList);
					$obj->access_type = 'list';
				break;

				case 'groups':
					$obj->usergroup = $this->parseArrayParameterAsString($userGroupList, $this->forbiddenGroups);
					$obj->access_type = 'user';
				break;

				case 'accessLevels':
					$obj->accesslevel = $this->parseArrayParameterAsString($accessLevelList, $this->forbiddenAccessLevels);
					$obj->access_type = 'access';
				break;

				case 'none':
				case 'default':
				break;
			}
		}
	}

    /**
     * Update Array with new values
     *
     * @param Array $sourceArray Source data to modify
     * @param Array $addArray Data that will be added to the array
     * @param Array $subtractArray Data that will be removed from the array
     * @return Array Updated array of data
     */
	public function updateArray($sourceArray, $addArray = null, $subtractArray = null) {

		if(!empty($addArray) && is_array($addArray)) {

            if(is_null($sourceArray)) {

                $updatedArray = $addArray;
            } else {

                $updatedArray = array_unique(array_merge($sourceArray, $addArray));
            }
		}

		if(!empty($subtractArray) && is_array($subtractArray)) {

            if(is_null($sourceArray)) {

                $updatedArray = [];
            } else {

                $updatedArray = array_diff($sourceArray, $subtractArray);
            }
		}

        // Re-index the updated array so that the values are in sequential index order (0,1,...)
		// Clean up any array gaps by re-indexing the resulting array
		$updatedArray = array_values($updatedArray);

		return $updatedArray;
	}

	public function parseRestrictedInput($input, $definitionArray) {
        $parsedInput = AxsSecurity::alphaNumericOnly($input);

        if(empty($parsedInput) || !in_array($parsedInput, $definitionArray)) {

            $parsedInput = '';
        }

        return $parsedInput;
	}

	/**
	 * Converts input of the type "Yes" or "No" to "1" or "0", respectively.
	 *
	 * @param string $input
	 * @return string Parsed input ("0" is the default regardless)
	 */
	public function parseYesNoInput($input) {
		$parsedInput = AxsSecurity::alphaNumericOnly($input);

		if(!empty($input) && in_array(strtolower($parsedInput), ['yes', 'no'])) {

			$parsedInput = $parsedInput == 'yes' ? '1' : '0';
		} else {

			$parsedInput = '0';
		}

		return $parsedInput;
	}

    public function cleanHTMLInput($input) {

        $badTagPatterns = [
            '(<script>.*<\/script>)',
            '(<\?=?.*\?>)',
            '(<\?php.*\?>)'
        ];

        $cleanedInput = preg_replace($badTagPatterns, '', $input);

        return $cleanedInput;
    }
}
