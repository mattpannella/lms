<?php

class CertificateTemplates {
    protected $api = null;
    protected $certificateTemplateIds = null;
    protected $items = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['certificatetemplates'])) {
            $this->certificateTemplateIds = $this->api->quoteFilterList($this->items['certificatetemplates'],'int');
        } elseif(!empty($this->api->filter['certificate_template_ids'])) {
            $this->certificateTemplateIds = $this->api->quoteFilterList($this->api->filter['certificate_template_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->certificateTemplateIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif(!empty($this->api->body->certificate_template_ids)) {
            $this->certificateTemplateIds = $this->api->quoteFilterList($this->api->body->certificate_template_ids);
        }
    }

    public function createCertificateTemplate() {
        $result = new stdClass();

        $titleText = AxsSecurity::cleanInput($this->api->body->TitleText);
        $descriptionText = AxsSecurity::cleanInput($this->api->body->DescriptionText);
        $headerTopText = AxsSecurity::cleanInput($this->api->body->HeaderTopText);
        $headerBottomText = AxsSecurity::cleanInput($this->api->body->HeaderBottomText);
        $introductionText = AxsSecurity::cleanInput($this->api->body->IntroText);
        $subtextAreaText = AxsSecurity::cleanInput($this->api->body->SubtextAreaText);
        $footerAreaLeftType = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftType);
        $footerAreaRightType = AxsSecurity::cleanInput($this->api->body->FooterAreaRightType);
        $footerAreaLeftTextTop = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftTopText);
        $footerAreaLeftTextBottom = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftBottomText);
        $footerAreaRightTextTop = AxsSecurity::cleanInput($this->api->body->FooterAreaRightTopText);
        $footerAreaRightTextBottom = AxsSecurity::cleanInput($this->api->body->FooterAreaRightBottomText);

        if(!empty($titleText)) {

            try {
                $db = JFactory::getDbo();
                $data = new stdClass();
                $templateParams = new stdClass();

                $footerAreaTypes = [
                    'signature',
                    'date_issued',
                    'date_expires'
                ];

                $data->title = htmlspecialchars($titleText);
                $data->description = $descriptionText;
                $data->enabled = true;

                if(!empty($headerTopText)) {

                    $templateParams->header_top = 'custom';
                    $templateParams->header_top_text = $headerTopText;
                }

                if(!empty($headerBottomText)) {

                    $templateParams->header_bottom = 'custom';
                    $templateParams->header_bottom_text = $headerBottomText;
                }

                if(!empty($introductionText)) {

                    $templateParams->introduction = 'custom';
                    $templateParams->introduction_text = $introductionText;
                }

                if(!empty($subtextAreaText)) {

                    $templateParams->sub_area = 'custom';
                    $templateParams->sub_text = $subtextAreaText;
                }

                if(!empty($footerAreaLeftType)) {

                    $templateParams->footer_area_left = 'custom';
                    $templateParams->footer_area_left_type = !in_array($footerAreaLeftType, $footerAreaTypes) ? 'signature' : $footerAreaLeftType;
                }

                if(!empty($footerAreaRightType)) {

                    $templateParams->footer_area_right = 'custom';
                    $templateParams->footer_area_right_type = !in_array($footerAreaRightType, $footerAreaTypes) ? 'signature' : $footerAreaRightType;
                }

                if(!empty($footerAreaLeftTextTop)) {

                    $templateParams->footer_area_left = 'custom';
                    $templateParams->footer_area_left_text_top = $footerAreaLeftTextTop;
                }

                if(!empty($footerAreaLeftTextBottom)) {

                    $templateParams->footer_area_left = 'custom';
                    $templateParams->footer_area_left_text_bottom = $footerAreaLeftTextBottom;
                }

                if(!empty($footerAreaRightTextTop)) {

                    $templateParams->footer_area_right = 'custom';
                    $templateParams->footer_area_right_text_top = $footerAreaRightTextTop;
                }

                if(!empty($footerAreaRightTextBottom)) {

                    $templateParams->footer_area_right = 'custom';
                    $templateParams->footer_area_right_text_bottom = $footerAreaRightTextBottom;
                }

                $data->params = json_encode($templateParams);

                $created = $db->insertObject('axs_awards_certificates', $data, 'id');

                if($created) {
                    $result->Status = 'success';
                    $result->id = $db->insertId();
                } else {
                    $result->Status = 'fail';
                    $result->Message = 'Could not create a certificate template with the parameters given.';
                }
            } catch(Exception $ex) {

                $result->Status = 'fail';
                $result->Message = 'Could not create a certificate template with the parameters given.';
            }
        } else {

            $result->Status = 'fail';
            $result->Message = 'One or more required fields missing.';
        }

        return $result;
    }

    public function getCertificateTemplates() {

        $certificateTemplates = array();

        if(!empty($this->certificateTemplateIds)) {

            $certificateTemplateIds = explode(',', $this->certificateTemplateIds);

            foreach($certificateTemplateIds as $templateId) {

                $certificate = AxsLMS::getCertificate(trim($templateId, "'"));

                if(!empty($certificate)) {

                    $certificateTemplates[] = $this->buildCertificateResponseObject($certificate);
                }
            }
        } else {
            // Make the ordering filterable by the user
            $newestFirst = false;

            if(!empty($this->api->filter['order'])) {

                $newestFirst = (strtolower($this->api->filter['order']) == 'desc');
            }

            // Get all of the certificate records - get the newest first if the flag is set
            $certificates = AxsLMS::getAllCertificates($newestFirst);

            foreach($certificates as $certificate) {

                $certificateTemplates[] = $this->buildCertificateResponseObject($certificate);
            }
        }

        return $certificateTemplates;
    }

    private function buildCertificateResponseObject($certificate) {

        $data = new stdClass();

        $data->id = $certificate->id;
        $data->TitleText = $certificate->title;
        $data->DescriptionText = $certificate->description;

        $certificateParams = json_decode($certificate->params);

        $data->HeaderTopText = $certificateParams->header_top_text;
        $data->HeaderBottomText = $certificateParams->header_bottom_text;
        $data->IntroText = $certificateParams->introduction_text;
        $data->SubtextAreaText = $certificateParams->sub_text;
        $data->FooterAreaLeftType = $certificateParams->footer_area_left_type;
        $data->FooterAreaRightType = $certificateParams->footer_area_right_type;
        $data->FooterAreaLeftTopText = $certificateParams->footer_area_left_text_top;
        $data->FooterAreaLeftBottomText = $certificateParams->footer_area_left_text_bottom;
        $data->FooterAreaRightTopText = $certificateParams->footer_area_right_text_top;
        $data->FooterAreaRightBottomText = $certificateParams->footer_area_right_text_bottom;

        return $data;
    }

    public function editCertificateTemplate() {
        $result = new stdClass();

        $titleText = AxsSecurity::cleanInput($this->api->body->TitleText);
        $descriptionText = AxsSecurity::cleanInput($this->api->body->DescriptionText);
        $headerTopText = AxsSecurity::cleanInput($this->api->body->HeaderTopText);
        $headerBottomText = AxsSecurity::cleanInput($this->api->body->HeaderBottomText);
        $introductionText = AxsSecurity::cleanInput($this->api->body->IntroText);
        $subtextAreaText = AxsSecurity::cleanInput($this->api->body->SubtextAreaText);
        $footerAreaLeftType = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftType);
        $footerAreaRightType = AxsSecurity::cleanInput($this->api->body->FooterAreaRightType);
        $footerAreaLeftTextTop = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftTopText);
        $footerAreaLeftTextBottom = AxsSecurity::cleanInput($this->api->body->FooterAreaLeftBottomText);
        $footerAreaRightTextTop = AxsSecurity::cleanInput($this->api->body->FooterAreaRightTopText);
        $footerAreaRightTextBottom = AxsSecurity::cleanInput($this->api->body->FooterAreaRightBottomText);

        $db = JFactory::getDbo();
        $id = (int)trim($this->certificateTemplateIds, "'");
        
        // Build out the updated object - if a field doesn't exist, don't include it
        $data = AxsLMS::getUserCertificate(null, null, $id);
        
        $response = new stdClass();
        
        if(empty($data)) {
            $response->Status = 'fail';
            $response->Message = 'Certificate could not be found';

            return $response;
        }
        
        if(!empty($titleText)) {

            $data->title = $titleText;
        }

        if(!empty($descriptionText)) {

            $data->description = $descriptionText;
        }
        
        $footerAreaTypes = [
            'signature',
            'date_issued',
            'date_expires'
        ];
        
        $templateParams = json_decode($data->params);

        if(!empty($headerTopText)) {

            $templateParams->header_top = 'custom';
            $templateParams->header_top_text = $headerTopText;
        }

        if(!empty($headerBottomText)) {

            $templateParams->header_bottom = 'custom';
            $templateParams->header_bottom_text = $headerBottomText;
        }

        if(!empty($introductionText)) {

            $templateParams->introduction = 'custom';
            $templateParams->introduction_text = $introductionText;
        }

        if(!empty($subtextAreaText)) {

            $templateParams->sub_area = 'custom';
            $templateParams->sub_text = $subtextAreaText;
        }

        if(!empty($footerAreaLeftType)) {

            $templateParams->footer_area_left = 'custom';
            $templateParams->footer_area_left_type = !in_array($footerAreaLeftType, $footerAreaTypes) ? 'signature' : $footerAreaLeftType;
        }

        if(!empty($footerAreaRightType)) {

            $templateParams->footer_area_right = 'custom';
            $templateParams->footer_area_right_type = !in_array($footerAreaRightType, $footerAreaTypes) ? 'signature' : $footerAreaRightType;
        }

        if(!empty($footerAreaLeftTextTop)) {

            $templateParams->footer_area_left = 'custom';
            $templateParams->footer_area_left_text_top = $footerAreaLeftTextTop;
        }

        if(!empty($footerAreaLeftTextBottom)) {

            $templateParams->footer_area_left = 'custom';
            $templateParams->footer_area_left_text_bottom = $footerAreaLeftTextBottom;
        }

        if(!empty($footerAreaRightTextTop)) {

            $templateParams->footer_area_right = 'custom';
            $templateParams->footer_area_right_text_top = $footerAreaRightTextTop;
        }

        if(!empty($footerAreaRightTextBottom)) {

            $templateParams->footer_area_right = 'custom';
            $templateParams->footer_area_right_text_bottom = $footerAreaRightTextBottom;
        }

        $data->params = json_encode($templateParams);

        try {
            $result = $db->updateObject('axs_awards_certificates', $data, 'id');

            if($result) {
                $response->Status = 'success';
                $response->id = $id;
            } else {
                $response->Status = 'fail';
                $response->Message = 'Could not update certificate template with the data given.';
            }
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Could not update certificate template with the data given.';
        }

        return $response;
    }

    public function deleteCertificateTemplate() {
        $response = new stdClass();

        if(!empty($this->certificateTemplateIds)) {
            $certificateTemplateId = trim($this->certificateTemplateIds, "'");

            $certificateTemplate = AxsLMS::getUserCertificate(null,null,$certificateTemplateId);

            if(empty($certificateTemplate)) {
                $response->Status = 'fail';
                $response->Message = 'Could not find a record with the given ID.';

                return $response;
            }

            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            $query->delete('axs_awards_certificates')
                  ->where($db->qn('id') . ' = ' . $certificateTemplateId);

            $db->setQuery($query);

            try {
                $result = $db->execute();

                if(!$result) {
                    $response->Status = 'fail';
                    $response->Message = 'Could not delete record with given ID.';
                } else {
                    $response->Status = 'success';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Could not delete record with given ID.';
            }            
        } else {
            $response->Status = 'fail';
            $response->Message = 'Please provide the ID of the record to delete.';
        }

        return $response;
    }
}