<?php

class Shop {
    protected $api = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;         
    }

    // -------- PRIVATE FUNCTIONS ----------

    private function parse_request_body($required_body_fields, $supported_body_fields) {
        // TODO validation and prevent SQL injection vector
        $data = new stdClass();
        foreach($this->api->body as $field_name => $value) {
            if (in_array($field_name, $required_body_fields)) {
                $field_idx = array_search($field_name, $required_body_fields);
                array_splice($required_body_fields, $field_idx, 1);
            }
            if (in_array($field_name, $supported_body_fields)) {
                $data->$field_name = $value;
            }            
        }
        if (count($required_body_fields) != 0) {
            $missing_fields = implode(", ", $required_body_fields);
            throw new Exception("Missing required field(s): $missing_fields");
        }
        return $data;
    }

    // ---------- PUBLIC FUNCTIONS ----------

    public function getItems($type = null) {
        // TODO add search by column value functionality
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from('axs_shop_items')        
            ->order('id ASC');

        $id_list = $this->api->filter['id'];
        if (!empty($id_list)) {
            $query->where('id IN (' . $id_list . ')');
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public function getOrders($type = null) {
        // TODO add search by column value functionality
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from('axs_shop_orders')       
            ->order('timestamp ASC');

        $id_list = $this->api->filter['id'];
        if (!empty($id_list)) {
            $query->where('id IN (' . $id_list . ')');
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;    
    }

    public function createItem($type = null) {
        $supported_body_fields = array(
            'name',
            'description',
            'sku',
            'order_id',
            'user_id'
        );
        $required_body_fields = array(
            'name',
            'sku'
        );

        $itemData = $this->parse_request_body($required_body_fields, $supported_body_fields);
        
        $db = JFactory::getDbo();
        try {
            // TODO this try/catch seems to not catch anything
            // so we need to manually check our db constraints
            $db->insertObject('axs_shop_orders', $itemData);
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        
        return $db->insertid() != 0 
            ? [ "message" => "item creation successful", "id" => $db->insertid()] 
            : [ "message" => "item creation failed"];
    }

    public function createOrder($type = null) {   
        $supported_body_fields = array(
            'order_date',
            'sent_date',
            'completion_date',
            'status',
            'user_id'
        );
        $required_body_fields = array(
            'user_id'
        );

        $orderData = $this->parse_request_body($required_body_fields, $supported_body_fields);

        $db = JFactory::getDbo();        
        try {
            // TODO this try/catch seems to not catch anything
            // so we need to manually check our db constraints
            $db->insertObject('axs_shop_orders', $orderData);
        } catch (Exception $e) {
            error_log($e->getMessage());
        }

        return $db->insertid() != 0 
            ? [ "message" => "order creation successful", "id" => $db->insertid()] 
            : [ "message" => "order creation failed"];    
    }   
}
