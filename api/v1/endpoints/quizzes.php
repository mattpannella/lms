<?php

class Quizzes {

    protected $api;
    protected $quizIds;
    protected $items;
    protected $mode;

    public function __construct($api) {

        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['quizzes'])) {

            $this->quizIds = $this->api->quoteFilterList($this->items['quizzes'],'int');
        } elseif(!empty($this->api->filter['quiz_ids'])) {

            $this->quizIds = $this->api->quoteFilterList($this->api->filter['quiz_ids'], 'int');
        } elseif(!empty($this->api->filter['id'])) {

            $this->quizIds = $this->api->quoteFilterList($this->api->filter['id'], 'int');
        } elseif(!empty($this->api->body->quiz_ids)) {

            $this->quizIds = $this->api->body->quoteFilterList($this->api->body->quiz_ids, 'int');
        } elseif(!empty($this->api->body->id)) {

            $this->quizIds = $this->api->body->quoteFilterList($this->api->body->id, 'int');
        }

        if(!empty($this->api->filter['mode'])) {

            $quizMode = $this->api->filter['mode'];
        } elseif(!empty($this->api->body->mode)) {

            $quizMode = $this->api->body->mode;
        }

        // If $quizMode is set and it's in the set of quiz types, set the mode accordingly
        $this->mode = !empty($quizMode) ? $quizMode : null;
    }

    public function getQuizzes() {

        $response = new stdClass();

        $db = JFactory::getDbo();

        try {

            $query = $db->getQuery(true);

            $query->select('*')
                  ->from('#__splms_quizquestions');

            if(!empty($this->quizIds)) {

                $query->where($db->qn('splms_quizquestion_id') . ' IN (' . $this->quizIds . ')');
            }

            $db->setQuery($query);

            $results = $db->loadObjectList('splms_quizquestion_id');
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Could not retrieve Quiz / Survey records.';

            return $response;
        }

        $quizzes = array();

        // If a mode is specified, filter by that as well
        if(!empty($this->mode)) {

            $results = array_filter($results, function($result) {

                $params = json_decode($result->params);

                return $params->mode == $this->mode;
            });
        }

        foreach($results as &$quiz) {

            // Map the base data
            $parsedQuizData = new stdClass();
            $parsedQuizData->Id = (int)$quiz->splms_quizquestion_id;
            $parsedQuizData->Title = $quiz->title;
            $parsedQuizData->Slug = $quiz->slug;
            $parsedQuizData->Image = $quiz->image;
            $parsedQuizData->Attempts = $quiz->tries;
            $parsedQuizData->Description = $quiz->description;
            $parsedQuizData->CourseId = (int)$quiz->splms_course_id;
            $parsedQuizData->LessonId = (int)$quiz->splms_lesson_id;
            $parsedQuizData->Ordering = (int)$quiz->ordering;
            $parsedQuizData->Creator = (int)$quiz->created_by;
            $parsedQuizData->CreatedDate = $quiz->created_on;

            // Parse the params object
            $params = json_decode($quiz->params);

            $parsedQuizData->RequiredScore = (int)$params->required_score;
            $parsedQuizData->Mode = $params->mode;

            if($params->passed_message) {
                $parsedQuizData->PassedMessage = $params->passed_message;
            } elseif($params->failed_message) {
                $parsedQuizData->FailedMessage = $params->failed_message;
            } else {
                $parsedQuizData->CompletionMessage = $params->completion_message;
            }

            $quizzes[] = $parsedQuizData;

            // Handle the question / answer arrays
            $listAnswers = json_decode($quiz->list_answers);

            $parsedQuizData->ListAnswers = array();

            $totalQuestions = count($listAnswers->qes_title);

            for($questionIndex = 0; $questionIndex < $totalQuestions; $questionIndex++) {

                $answerObj = new stdClass();

                $answerObj->Title = $listAnswers->qes_title[$questionIndex];
                $answerObj->Media = $listAnswers->media[$questionIndex];
                $answerObj->AnswerOne = $listAnswers->ans_one[$questionIndex];
                $answerObj->AnswerTwo = $listAnswers->ans_two[$questionIndex];
                $answerObj->AnswerThree = $listAnswers->ans_three[$questionIndex];
                $answerObj->AnswerFour = $listAnswers->ans_four[$questionIndex];
                $answerObj->CorrectAnswer = $listAnswers->right_ans[$questionIndex];

                $parsedQuizData->ListAnswers[] = $answerObj;
            }
        }

        return $quizzes;
    }
}