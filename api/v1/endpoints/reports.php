<?php

class Reports {
    protected $api = null;
    protected $reportIds = null;
    protected $items = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['reports'])) {
            $this->reportIds = $this->api->quoteFilterList($this->items['reports'],'int');
        } elseif(!empty($this->api->filter['report_ids'])) {
            $this->reportIds = $this->api->quoteFilterList($this->api->filter['report_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->reportIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif(!empty($this->api->body->reportIds)) {

            // This is going to be an array, so we want to convert it to a string
            $this->reportIds = $this->api->quoteFilterList($this->api->body->reportIds,'int');
        }
    }

    public function listReports() {

        $reports = [];

        $conditions = [];

        if(!empty($this->reportIds)) {

            $conditions[] = "id IN ($this->reportIds)";
        }

        // Report name, report type, report sub-type, report IDs
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_reports');

        if(!empty($conditions)) {

            $query->where($conditions);
        }

        $db->setQuery($query);

        $results = $db->loadObjectList();

        foreach($results as $result) {

            $report = new stdClass();

            $report->id = $result->id;
            $report->Name = $result->name;
            $report->Type = $result->report_type;

            $reportParams = json_decode($result->params);

            if(!empty($reportParams)) {

                $type = $result->report_type;
                $subtype = $reportParams->{$type . '_report_type'};

                $report->Subtype = !empty($subtype) ? $subtype : null;
            }

            $reports[] = $report;
        }

        return $reports;
    }
    
    public function runReports() {

        $reportsList = [];        
        $response = new stdClass();

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_reports');

        if(!empty($this->reportIds)) {

            $query->where("id IN ($this->reportIds)");
        }

        $db->setQuery($query);

        try {

            $reports = $db->loadObjectList();

            // Parse the reports
            foreach($reports as $report) {

                $reportData = new stdClass();

                // Note: the API does not support external reports at this time.
                if($report->report_type != 'external') {

                    $model = FOFModel::getAnInstance('Reports', 'ReportsModel');
                    $data = $model->getData($report);
                    
                    $reportData->id = $report->id;
                    $reportData->Type = $report->report_type;

                    $reportParams = json_decode($report->params);

                    if(!empty($reportParams)) {

                        $type = $report->report_type;
                        $subtype = $reportParams->{$type . '_report_type'};

                        $reportData->Subtype = !empty($subtype) ? $subtype : null;
                    }
                    
                    $reportData->Results = $data->grid->items;

                    $reportsList[] = $reportData;
                }
            }

            return $reportsList;

        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Requested reports could not be executed.';
        }
    }
}