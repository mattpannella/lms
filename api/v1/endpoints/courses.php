<?php

class Courses {
    protected $api = null;
    protected $courseIds = null;
    protected $courseTitles = null;
    protected $categoryIds = null;
    protected $categoryNames = null;
    protected $items = null;
    protected $baseUrl = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if($this->items['courses']) {
            $this->courseIds = $this->api->quoteFilterList($this->items['courses'],'int');
        } elseif($this->api->filter['course_ids']) {
            $this->courseIds = $this->api->quoteFilterList($this->api->filter['course_ids'],'int');
        } elseif($this->api->filter['id']) {
            $this->courseIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif($this->api->body->course_ids) {
            $this->courseIds = $this->api->quoteFilterList($this->api->body->course_ids,'int');
        } elseif($this->api->filter['titles']) {
            $this->courseTitles = $this->api->quoteFilterList($this->api->filter['titles'],'string');
        }

        $urlArray = explode('api/v1',JUri::base());
        $this->baseUrl = $urlArray[0];
    }

    public function getCourses() {
        $db = JFactory::getDBO();

        if($this->api->body->titles) {
            $courseTitles = $this->api->quoteFilterList($this->api->body->titles, 'string');
        } else if($this->courseTitles) {
            $courseTitles = $this->courseTitles;
        }

        if($this->courseIds) {
            $conditions[] = $db->qn('splms_course_id').' IN ('.$this->courseIds.')';
        } else if($courseTitles) {
            $conditions[] = $db->qn('title') . ' IN ( ' . $courseTitles . ')';
        }

        $query = $db->getQuery(true);
        $query->select('splms_course_id, title, description, image, slug');
        $query->from('#__splms_courses');

        if($conditions) {
            $query->where($conditions);
        }

        $query->order('splms_course_id DESC');
        $db->setQuery($query);
        $courses = $db->loadObjectList();

        $dataArray = array();

        foreach($courses as $course) {
            $apiData = new stdClass();

            $apiData->id = (int)$course->splms_course_id;
            $apiData->Title = $course->title;
            $apiData->Description = $course->description;
            $apiData->CardImage = empty($course->image) ? '' : ($this->baseUrl . $course->image);
            $apiData->Url = $this->baseUrl . 'courses/course/' . $course->slug;

            $dataArray[] = $apiData;
        }

        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function getCoursesCategories() {
        $coursesCategoriesIds = null;

        if($this->items['categories']) {
            $coursesCategoriesIds = $this->api->quoteFilterList($this->items['categories'],'int');
        } else if($this->api->filter['id']) {
            $coursesCategoriesIds = $this->api->quoteFilterList($this->api->filter['id'], 'int');
        } elseif($this->api->filter['category_ids']) {
            $coursesCategoriesIds = $this->api->quoteFilterList($this->api->filter['category_ids'], 'int');
        }

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $conditions = array();

        if($coursesCategoriesIds) {
            $conditions[] = "{$db->qn('splms_coursescategory_id')} IN ({$coursesCategoriesIds})";
        }

        $query->select('*')
              ->from('#__splms_coursescategories');

        if($conditions) {
            $query->where($conditions);
        }

        $query->order('splms_coursescategory_id DESC');
        $db->setQuery($query);

        $results = $db->loadObjectList();
        $dataArray = array();

        foreach($results as $result) {

            $data = new stdClass();
            $data->id = $result->splms_coursescategory_id;
            $data->Title = $result->title;
            $data->Slug = $result->slug;
            $data->Description = $result->description;
            $data->Enabled = (bool)$result->enabled;
            $data->Image = empty($result->image) ? $result->image : $this->baseUrl . $result->image;
            $data->Order = $result->ordering;

            $dataArray[] = $data;
        }

        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function getCourseRegistrants() {

        $db = JFactory::getDbo();

        $currentLanguage = JFactory::getLanguage()->getTag();
        
        $conditions = array('progress.archive_date IS NULL');

        if(!empty($this->courseIds)) {

            $conditions[] = $db->qn('course_id') . ' IN (' . $this->courseIds . ')';
        }

        $conditions[] = $db->qn('language') . ' = ' . $db->quote($currentLanguage);

        $query = $db->getQuery(true);

        $query->select('progress.user_id as user_id, progress.progress as progress, progress.date_started as date_started, progress.date_last_activity as date_last_activity, course.params as params')
              ->from('#__splms_course_progress AS progress')
              ->leftJoin('#__splms_courses AS course ON course.splms_course_id = progress.course_id')
              ->where($conditions);

        $db->setQuery($query);

        try {

            $courseProgressList = $db->loadObjectList();

            $registrationData = array();

            foreach($courseProgressList as $courseProgressDatum) {

                $registrationInfo = new stdClass();

                $courseParameters = json_decode($courseProgressDatum->params);

                $registrationInfo->UserId = (int)$courseProgressDatum->user_id;
                $registrationInfo->CourseCompletion = $courseProgressDatum->progress . '%';
                $registrationInfo->CourseStartDate = $courseProgressDatum->date_started;
                $registrationInfo->LatestCourseActivityDate = $courseProgressDatum->date_last_activity;
                $registrationInfo->CourseExpiryDate = !empty($courseParameters) ? $courseParameters->course_close_date : null;

                $registrationData[] = $registrationInfo;
            }

            $response = $registrationData;
        } catch(Exception $ex) {

            $response = new stdClass();

            $response->Status = 'fail';
            $response->Message = 'Could not retrieve registrant information for the given course.';
        } finally {

            return $response;
        }
    }

    public function createCourseCategory() {

        $db = JFactory::getDbo();
        $courseCategoryData = new stdClass();
        $response = new stdClass();

        try {
            $courseCategoryData->title = $this->api->body->Title;

            $slug = preg_replace("/[^A-Za-z0-9 ]/", "", $this->api->body->Title);
            $courseCategoryData->slug = strtolower(strtr($slug, ' ', '-'));

            $courseCategoryData->featured = false;
            $courseCategoryData->show = true;
            $courseCategoryData->enabled = $this->api->body->Enabled;
            $courseCategoryData->ordering = $this->api->body->Ordering;

            $courseCategoryData->access_type = $this->api->body->AccessType;

            $courseCategoryData->accesslevel = implode(',', $this->api->body->AccessLevels);
            $courseCategoryData->usergroup = implode(',', $this->api->body->AccessUserGroups);
            $courseCategoryData->userlist = implode(',', $this->api->body->AccessByUserList);

            $courseCategoryData->created_on = date('y-m-d H:i:s');
            $courseCategoryData->modified_on = date('y-m-d H:i:s');

            $db->insertObject('#__splms_coursescategories', $courseCategoryData);

            $response->Status = 'success';
            $response->Id = $db->insertid();
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Error = 'Could not create a new course category.';
        }

        return $response;
    }

    public function deleteCourseCategory() {
        $courseCategoryId = empty($this->items['categories']) ? $this->api->body->id : $this->items['categories'];
        $response = new stdClass();

        if(!empty($courseCategoryId)) {
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            $query->delete('#__splms_coursescategories')
                  ->where("splms_coursescategory_id = $courseCategoryId");

            $db->setQuery($query);

            try {
                $result = $db->execute();

                if($result) {
                    $response->Status = 'success';
                }
            } catch(Exception $ex) {
                $response->Status = 'fail';
                $response->Error = 'Could not delete given course category record.';
            }
        } else {
            $response->Status = 'fail';
            $response->Error = 'Please provide a ID for the course category that you wish to delete.';
        }

        return $response;
    }

    public function editCourseCategory() {
        $newData = new stdClass();
        $response = new stdClass();

        if(!empty($this->items['categories']) || !empty($this->api->body->id)) {

            $newData->splms_coursescategory_id = empty($this->api->body->id) ? $this->items['categories'] : $this->api->body->id;
            $newData->title = $this->api->body->Title;

            if(!empty($newData->title)) {

                // Slugify the title and create the slug
                $slug = preg_replace("/[^A-Za-z0-9 ]/", "", $this->api->body->Title);
                $newData->slug = strtolower(strtr($slug, ' ', '-'));
            }

            $newData->description = $this->api->body->Description;
            $newData->enabled = $this->api->body->Enabled;
            $newData->order = $this->api->body->Order;

            // Prepend the base URL component (domain) in front of the image url if it doesn't have one
            if(!stristr($this->api->body->Image, $this->baseUrl)) {
                $newData->image = $this->baseUrl . $this->api->body->Image;
            } else {
                $newData->image = $this->api->body->Image;
            }

            // Update access information (if any)
            $newData->access_type = $this->api->body->AccessType;
            $newData->accesslevel = !empty($this->api->body->AccessLevels) ? implode(',', $this->api->body->AccessLevels) : null;
            $newData->usergroup = !empty($this->api->body->AccessUserGroups) ? implode(',', $this->api->body->AccessUserGroups) : null;
            $newData->userlist = !empty($this->api->body->AccessUsers) ? implode(',', $this->api->body->AccessUsers) : null;

            $newData->modified_on = date('y-m-d H:i:s');

            $db = JFactory::getDbo();

            try {
                $result = $db->updateObject('#__splms_coursescategories', $newData, 'splms_coursescategory_id');

                if($result) {
                    $response->Status = 'success';
                    $response->Id = $newData->splms_coursescategory_id;
                }
            } catch (Exception $ex) {
                $response->Status = 'fail';
                $response->Error = 'Could not update course category record.';
            }
        } else {
            $response->Status = 'fail';
            $response->Error = 'Please provide a course category ID to edit.';
        }

        return $response;
    }
}