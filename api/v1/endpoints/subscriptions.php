<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class Subscriptions {
    protected $api = null;
    protected $subscriptionIds = null;
    protected $items = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['subscriptions'])) {
            $this->subscriptionIds = $this->api->quoteFilterList($this->items['subscriptions'],'int');
        } elseif(!empty($this->api->filter['subscription_ids'])) {
            $this->subscriptionIds = $this->api->quoteFilterList($this->api->filter['subscription_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->subscriptionIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }

        // If the ids are given in the body, process those instead.
        if ($this->api->body->id) {
            $this->subscriptionIds = $this->api->body->id;
        } elseif ($this->api->body->subscription_ids){
            $this->subscriptionIds = $this->api->quoteFilterList($this->api->body->subscription_ids);
        }
    }

    public function getSubscriptions() {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_pay_subscription_plans');

        if(!empty($this->subscriptionIds)) {
            $query->where("id in ($this->subscriptionIds)");
        }

        $db->setQuery($query);
        $results = $db->loadObjectList();

        $subscriptions = array();

        foreach($results as $result) {

            $subscription = new stdClass();

            $subscription->id = intval($result->id);
            $subscription->Title = $result->title;
            $subscription->Published = $result->published == '1';
            $subscription->DefaultLength = $result->default_sub_length;
            $subscription->DefaultSignupAmount = $result->default_initial_amount;
            $subscription->DefaultRecurringAmount = $result->default_amount;
            $subscription->UserGroups = array_map('intval', json_decode($result->usergroups, true));

            if($result->description_type == 'html') {

                $decodedHTML = html_entity_decode($result->description_html);
                $subscription->HTMLDescription = $this->api->cleanHTMLInput($decodedHTML);
            }

            $subscriptions[] = $subscription;
        }

        if(!empty($subscriptions)) {
            return $subscriptions;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function createSubscription() {

        $db = JFactory::getDbo();

        $data = $this->api->body;
        $values = new stdClass();
        $response = new stdClass();

        $hasRequiredFields = !empty($data->Title);

        if($hasRequiredFields) {

            $values->title = $data->Title;
            $values->published = $data->Published ?? true;

            if(!empty($data->HTMLDescription)) {
                $values->description_html = $this->api->cleanHTMLInput($data->HTMLDescription);
                $values->description_type = 'html';
            }

            $values->default_sub_length = $data->DefaultLength ?? '';
            $values->default_initial_amount = $data->DefaultSignupAmount ?? null;
            $values->default_amount = $data->DefaultRecurringAmount ?? null;

            if(!empty($data->UserGroups)) {

                $values->usergroups = json_encode($data->UserGroups);
            } else {

                $values->usergroups = null;
            }

            try {

                $result = $db->insertObject('axs_pay_subscription_plans', $values, 'id');

                if(!$result) {

                    $response->Status = 'fail';
                    $response->Error = 'Could not create a new subscription.';
                } else {

                    $response->id = intval($values->id);
                    $response->Status = 'success';
                }
            } catch (Exception $ex) {

                $response->Status = 'fail';
                $response->Error = 'Could not create a new subscription.';
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';
        }

        return $response;
    }

    public function editSubscription() {

        $db = JFactory::getDbo();

        $data = $this->api->body;
        $values = new stdClass();
        $response = new stdClass();

        $id = intval(trim($this->subscriptionIds, "'"));

        $query = $db->getQuery(true);

        // Get existing record to update
        $query->select('*')
              ->from('axs_pay_subscription_plans')
              ->where("id = $id");

        $db->setQuery($query);

        $hasRequiredFields = !empty($id);
        $existingSubPlan = $db->loadObject();

        if($hasRequiredFields) {

            if(!is_null($existingSubPlan)) {

                $values->id = $id;
                $values->title = @$data->Title;
                $values->published = @$data->Published;

                if(!empty($data->HTMLDescription)) {
                    $values->description_html = $this->api->cleanHTMLInput($data->HTMLDescription);
                    $values->description_type = 'html';
                }

                $values->default_sub_length = @$data->DefaultLength;
                $values->default_initial_amount = @$data->DefaultSignupAmount;
                $values->default_amount = @$data->DefaultRecurringAmount;

                $query = $db->getQuery(true);

                $query->select('*')
                      ->from('axs_pay_subscription_plans')
                      ->where("id = $id");

                $db->setQuery($query);

                if(!empty($data->UserGroups)) {

                    $values->usergroups = json_encode($data->UserGroups);
                }

                try {
                    $result = $db->updateObject('axs_pay_subscription_plans', $values, 'id');

                    if(!$result) {

                        $response->Status = 'fail';
                        $response->Error = 'Could not edit the subscription.';
                    } else {

                        $response->id = intval($values->id);
                        $response->Status = 'success';
                    }
                } catch (Exception $ex) {

                    $response->Status = 'fail';
                    $response->Error = 'Could not edit the subscription.';
                }
            } else {

                $response->Status = 'fail';
                $response->Error  = "Subscription plan with id $id doesn't exist.";
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';
        }

        return $response;
    }

    public function deleteSubscription() {

        $response = new stdClass();

        if(!empty($this->subscriptionIds)) {

            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->delete('axs_pay_subscription_plans')
                  ->where("id in ($this->subscriptionIds)");

            $db->setQuery($query);
            $result = $db->execute();

            if(!$result) {

                throw new Exception('There was an error deleting the specified subscription(s).');
            } else {

                $response->Status = 'success';
            }
        } else {

            $response = new stdClass();

            $response->Status = "fail";
            $response->Error  = "Please provide an ID or IDs for the subscription(s) you wish to delete.";
        }

        return $response;
    }

    public function addUserGroupToSubscription() {

        $db = JFactory::getDbo();

        $data = $this->api->body;
        $response = new stdClass();

        $inputId = !empty($data->id) ? $data->id : @$this->items['add_user_groups'];
        $subscriptionId = intval(trim($inputId, "'"));

        $query = $db->getQuery(true);

        // Get existing record to update
        $query->select('*')
              ->from('axs_pay_subscription_plans')
              ->where("id = $subscriptionId");

        $db->setQuery($query);

        $existingSubPlan = $db->loadObject();

        $hasRequiredFields = !empty($subscriptionId) && !empty($data->UserGroups);

        if($hasRequiredFields) {

            if(!is_null($existingSubPlan)) {

                $existingUserGroups = json_decode($existingSubPlan->usergroups) ?? [];

                $newUserGroups = array_unique(array_merge($existingUserGroups, $data->UserGroups));
                $newUserGroups = array_values($newUserGroups);

                $existingSubPlan->usergroups = json_encode($newUserGroups);

                try {
                    $result = $db->updateObject('axs_pay_subscription_plans', $existingSubPlan, 'id');

                    if(!$result) {

                        $response->Status = 'fail';
                        $response->Error = 'Could not update the subscription.';
                    } else {

                        $response->id = intval($existingSubPlan->id);
                        $response->Status = 'success';
                    }
                } catch (Exception $ex) {

                    $response->Status = 'fail';
                    $response->Error = 'Could not update the subscription.';
                }
            } else {

                $response->Status = 'fail';
                $response->Error  = "Subscription plan with id $subscriptionId doesn't exist.";
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';
        }

        return $response;
    }

    public function removeUserGroupFromSubscription() {

        $db = JFactory::getDbo();

        $data = $this->api->body;
        $response = new stdClass();

        $inputId = !empty($data->id) ? $data->id : @$this->items['remove_user_groups'];
        $subscriptionId = intval(trim($inputId, "'"));

        $query = $db->getQuery(true);

        // Get existing record to update
        $query->select('*')
              ->from('axs_pay_subscription_plans')
              ->where("id = $subscriptionId");

        $db->setQuery($query);

        $existingSubPlan = $db->loadObject();

        $hasRequiredFields = !empty($subscriptionId) && !empty($data->UserGroups);

        if($hasRequiredFields) {

            if(!is_null($existingSubPlan)) {

                $existingUserGroups = json_decode($existingSubPlan->usergroups) ?? [];

                $newUserGroupSet = $this->api->updateArray($existingUserGroups, null, $data->UserGroups);

                $existingSubPlan->usergroups = json_encode($newUserGroupSet);

                try {
                    $result = $db->updateObject('axs_pay_subscription_plans', $existingSubPlan, 'id');

                    if(!$result) {

                        $response->Status = 'fail';
                        $response->Error = 'Could not update the subscription.';
                    } else {

                        $response->id = intval($existingSubPlan->id);
                        $response->Status = 'success';
                    }
                } catch (Exception $ex) {

                    $response->Status = 'fail';
                    $response->Error = 'Could not update the subscription.';
                }
            } else {

                $response->Status = 'fail';
                $response->Error  = "Subscription plan with id $subscriptionId doesn't exist.";
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';
        }

        return $response;
    }
}