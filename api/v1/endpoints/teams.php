<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class Teams {
    protected $api = null;
    protected $teamIds = null;
    protected $items = null;
    private   $userLib;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['teams'])) {
            $this->teamIds = $this->api->quoteFilterList($this->items['teams'],'int');
        } elseif(!empty($this->api->filter['team_ids'])) {
            $this->teamIds = $this->api->quoteFilterList($this->api->filter['team_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->teamIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }

        // If the ids are given in the body, process those instead.
        if (!empty($this->api->body->id)) {
            $this->teamIds = $this->api->body->id;
        } elseif (!empty($this->api->body->team_ids)) {
            $this->teamIds = $this->api->quoteFilterList($this->api->body->team_ids);
        }

        $this->userLib = new AxsUser();
    }

    public function getTeams() {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_teams');

        if(!empty($this->teamIds)) {
            $query->where("id in ($this->teamIds)");
        }

        $db->setQuery($query);
        $results = $db->loadObjectList();

        $teams = array();

        foreach($results as $result) {
            $team = new stdClass();

            $team->id = intval($result->id);
            $team->Title = $result->title;
            $team->Description = $result->description;
            $team->Enabled = boolval($result->enabled);
            $team->Type = $result->member_type ?? 'individuals';

            $teamDash = new AxsTeamLeadDashboard(null, $result, null);

            if($team->Type == 'individuals') {

                $team->Members = $this->getUserInformationForUserIds($teamDash->getUserList());
            }

            if($team->Type == 'groups') {

                $groups = explode(',', $result->user_groups);

                $teamMemberIds = AxsUser::getUserIdsInGroups($groups);
                $team->Members = $this->getUserInformationForUserIds($teamMemberIds);
            }

            $team->Leaders = $this->getUserInformationForUserIds($result->team_lead);

            $teams[] = $team;
        }

        if(!empty($teams)) {
            return $teams;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function editTeam() {

        $response = new stdClass();

        // If the team ID isn't in the API route, try getting it from the body
        $teamId = !empty($this->items['teams']) ? $this->items['teams'] : $this->api->body->id;

        if(is_null($teamId)) {

            $response->Status = 'fail';
            $response->Error = 'No user team ID provided. Please pass a single team ID and try again.';

            return $response;
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Get existing record to update
        $query->select('*')
              ->from('axs_teams')
              ->where("id = $teamId");

        $db->setQuery($query);

        $existingTeam = $db->loadObject();

        if(!empty($existingTeam)) {

            if(!empty($this->api->body->Title)) {

                $existingTeam->title = $this->api->body->Title;
            }

            if(!empty($this->api->body->Type)) {

                $existingTeam->member_type = $this->api->body->Type;
            }

            if(!empty($this->api->body->Description)) {

                $existingTeam->description = $this->api->body->Description;
            }

            if(isset($this->api->body->Enabled)) {

                $existingTeam->enabled = $this->api->body->Enabled;
            }

            if(!empty($this->api->body->LeadUsers)) {

                $existingTeam->team_lead = implode(',', $this->api->body->LeadUsers);
            }

            if(!empty($this->api->body->LeadGroups)) {

                $leadGroups = $this->api->body->LeadGroups;

                $usersInLeadGroups = AxsUser::getUserIdsInGroups($leadGroups);

                $existingTeam->team_lead = implode(',', $usersInLeadGroups);
            }

            if(!empty($this->api->body->Members)) {

                $existingTeam->user_ids = implode(',', $this->api->body->Members);
            }

            if(!empty($this->api->body->Groups)) {

                $existingTeam->user_groups = implode(',', $this->api->body->Groups);                
            }

            try {

                $result = $db->updateObject('axs_teams', $existingTeam, 'id');

                if($result) {
                    $response->Status = 'success';
                    $response->id = $existingTeam->id;
                } else {
                    $response->Status = 'fail';
                    $response->Error = 'Could not update user team record.';
                }
            } catch(Exception $ex) {
                $response->Status = 'fail';
                $response->Error = 'Could not update user team record.';
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = "Team with id $teamId doesn't exist.";
        }

        return $response;
    }

    public function deleteTeam() {
        $response = new stdClass();

        if(!empty($this->teamIds)) {

            $db = JFactory::getDbo();

            $query = $db->getQuery(true);
            $query->delete('axs_teams')
                  ->where("id in ($this->teamIds)");

            $db->setQuery($query);
            $result = $db->execute();

            if(!$result) {
                throw new Exception('There was an error deleting the specified user team.');
            } else {
                $response->Status = 'success';
                return $response;
            }
        } else {
            $response = new stdClass();

            $response->Status = "fail";
            $response->Error  = "Please provide a ID for the team you wish to delete.";

            return $response;
        }
    }

    public function createTeam() {

        $db = JFactory::getDbo();
        $data = $this->api->body;
        $values = new stdClass();
        $response = new stdClass();

        $hasValidFields = !empty($data->Title);

        if($hasValidFields) {

            $values->title = $data->Title;
            $values->description = !empty($data->Description) ? $data->Description : '';
            $values->enabled = !empty($data->Enabled) ? $data->Enabled : false;
            $values->date_created = date('y-m-d H:i:s');
            $values->member_type = !empty($data->Type) ? $data->Type : 'individuals';

            if(!empty($data->Members)) {

                $values->user_ids = implode(',', $data->Members);
            }
            
            if(!empty($data->LeadUsers)) {

                $values->team_lead = implode(',', $data->LeadUsers);
            }

            if(!empty($data->Groups)) {

                $values->user_groups = implode(',', $data->Groups);
            }

            if(!empty($data->LeadGroups)) {

                // Build out the team leads via the groups passed in
                $teamLeads = $this->userLib->getUserIdsInGroups($data->LeadGroups);

                $values->team_lead = implode(',', $teamLeads);
            }

            try {

                $result = $db->insertObject('axs_teams', $values, 'id');

                if(!$result) {

                    $response->Status = 'fail';
                    $response->Error = 'Could not create a new user team.';
                } else {

                    $response->id = intval($values->id);
                    $response->Status = 'success';
                }
            } catch (Exception $ex) {

                $response->Status = 'fail';
                $response->Error = 'Could not create a new user team.';
            } finally {
                return $response;
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';

            return $response;
        }
    }

    public function addUsersToTeam() {

        $data = $this->api->body;
        $response = new stdClass();

        $inputId = !empty($data->id) ? $data->id : @$this->items['add_users'];

        $response = $this->updateTeamUsers($inputId, $data, 'add');

        return $response;
    }

    public function removeUsersFromTeam() {

        $data = $this->api->body;
        $inputId = !empty($data->id) ? $data->id : @$this->items['remove_users'];

        $response = $this->updateTeamUsers($inputId, $data, 'remove');

        return $response;
    }

    public function addUserGroupsToTeam() {

        $data = $this->api->body;
        $inputId = !empty($data->id) ? $data->id : @$this->items['add_user_groups'];

        $response = $this->updateTeamGroups($inputId, $data, 'add');

        return $response;
    }

    public function removeUserGroupsFromTeam() {

        $data = $this->api->body;
        $inputId = !empty($data->id) ? $data->id : @$this->items['remove_user_groups'];

        $response = $this->updateTeamGroups($inputId, $data, 'remove');

        return $response;
    }

    private function updateTeamUsers($teamId, $teamData, $mode = 'add') {

        $response = new stdClass();

        if(!empty($teamId)) {

            $teamId = intval(trim($teamId, "'"));

            // Get team from database
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select('*')
                  ->from('axs_teams')
                  ->where("id = $teamId AND member_type = 'individuals'");

            $db->setQuery($query);

            $existingTeam = $db->loadObject();

            $hasRequiredFields = !empty($teamId) && (!empty($teamData->TeamLeads) || !empty($teamData->Members));

            if($hasRequiredFields) {

                if(!is_null($existingTeam)) {

                    if(!empty($teamData->Members)) {

                        $existingUsers = [];

                        if(!empty($existingTeam->user_ids)) {

                            $existingUsers = explode(',', $existingTeam->user_ids);
                        }

                        if($mode == 'add') {

                            // Add the given users to the existing array
                            $updatedUsers = $this->api->updateArray($existingUsers, $teamData->Members);
                        }

                        if($mode == 'remove') {

                            // Remove the given users from the existing array
                            $updatedUsers = $this->api->updateArray($existingUsers, null, $teamData->Members);
                        }

                        $existingTeam->user_ids = implode(',', $updatedUsers);
                    }

                    if(!empty($teamData->TeamLeads)) {

                        $existingTeamLeads = [];

                        if(!empty($existingTeam->team_lead)) {

                            $existingTeamLeads = explode(',', $existingTeam->team_lead);
                        }

                        if($mode == 'add') {

                            // Add the given users to the team leads array
                            $updatedTeamLeads = $this->api->updateArray($existingTeamLeads, $teamData->TeamLeads);
                        }

                        if($mode == 'remove') {

                            // Remove the given users from the team leads array
                            $updatedTeamLeads = $this->api->updateArray($existingTeamLeads, null, $teamData->TeamLeads);
                        }

                        $existingTeam->team_lead = implode(',', $updatedTeamLeads);
                    }

                    try {

                        $result = $db->updateObject('axs_teams', $existingTeam, 'id');

                        if(!$result) {

                            $response->Status = 'fail';
                            $response->Error = 'Could not update the team.';
                        } else {

                            $response->id = intval($existingTeam->id);
                            $response->Status = 'success';
                        }
                    } catch (Exception $ex) {

                        $response->Status = 'fail';
                        $response->Error = 'Could not update the team.';
                    }
                } else {

                    $response->Status = 'fail';
                    $response->Error  = "Team with id $teamId and member type of 'individuals' doesn't exist.";
                }
            } else {

                $response->Status = 'fail';
                $response->Error  = 'There are one or more required fields missing.';
            }
        } else {
            $response->Status = 'fail';
            $response->Error  = 'Missing team ID parameter';
        }

        return $response;
    }

    private function updateTeamGroups($teamId, $teamData, $mode = 'add') {

        $response = new stdClass();

        if(!empty($teamId)) {

            $teamId = intval(trim($teamId, "'"));

            // Get team from database
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select('*')
                ->from('axs_teams')
                ->where("id = $teamId AND member_type = 'groups'");

            $db->setQuery($query);

            $existingTeam = $db->loadObject();

            $hasRequiredFields = !empty($teamId) && (!empty($teamData->TeamLeadGroups) || !empty($teamData->MemberGroups));

            if($hasRequiredFields) {

                if(!is_null($existingTeam)) {

                    if(!empty($teamData->MemberGroups)) {

                        $existingMemberGroups = [];

                        if(!empty($existingTeam->user_groups)) {

                            $existingMemberGroups = explode(',', $existingTeam->user_groups);
                        }

                        if($mode == 'add') {
                            $updatedMemberGroups = $this->api->updateArray($existingMemberGroups, $teamData->MemberGroups);
                        }

                        if($mode == 'remove') {

                            // Remove the given user groups from the existing array
                            $updatedMemberGroups = $this->api->updateArray($existingMemberGroups, null, $teamData->MemberGroups);
                        }

                        $existingTeam->user_groups = implode(',', $updatedMemberGroups);
                    }

                    // This is going to be handled differently, as team leads are always represented in the axs_teams
                    // table as a string of user IDs.
                    if(!empty($teamData->TeamLeadGroups)) {

                        $teamLeads = [];

                        if(!empty($existingTeam->team_lead)) {

                            $teamLeads = explode(',', $existingTeam->team_lead);
                        }

                        /* Take the user IDs from the team lead groups passed in and merge them into the existing team
                         * lead array. If one or more groups don't exist, no user IDs will be returned from them and
                         * the net effect will be that the non-existant groups won't have any information in the team
                         * record.
                         */
                        $teamLeadUsersFromGroups = AxsUser::getUserIdsInGroups($teamData->TeamLeadGroups);

                        if($mode == 'add') {

                            $updatedTeamLeads = $this->api->updateArray($teamLeads, $teamLeadUsersFromGroups);
                        }

                        if($mode == 'remove') {

                            $updatedTeamLeads = $this->api->updateArray($teamLeads, null, $teamLeadUsersFromGroups);
                        }

                        $existingTeam->team_lead = implode(',', $updatedTeamLeads);
                    }

                    try {

                        $result = $db->updateObject('axs_teams', $existingTeam, 'id');

                        if(!$result) {

                            $response->Status = 'fail';
                            $response->Error = 'Could not update the team.';
                        } else {

                            $response->id = intval($existingTeam->id);
                            $response->Status = 'success';
                        }
                    } catch (Exception $ex) {

                        $response->Status = 'fail';
                        $response->Error = 'Could not update the team.';
                    }
                } else {

                    $response->Status = 'fail';
                    $response->Error  = "Team with id $teamId and member type of 'groups' doesn't exist.";
                }
            } else {

                $response->Status = 'fail';
                $response->Error  = 'There are one or more required fields missing.';
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = 'Missing team ID parameter';
        }

        return $response;
    }

    public function editTeamLeadPermissions() {

        $db = JFactory::getDbo();

        $data = $this->api->body;
        $response = new stdClass();

        $inputId = !empty($data->id) ? $data->id : @$this->items['edit_team_lead_permissions'];
        $teamId = intval(trim($inputId, "'"));

        if(is_null($teamId)) {

            $response->Status = 'fail';
            $response->Error = 'No user team ID provided. Please pass a single team ID and try again.';

            return $response;
        }

        $query = $db->getQuery(true);

        // Get the team record associated with the given team ID
        $query->select('*')
              ->from('axs_teams')
              ->where('id = ' . $teamId);

        $db->setQuery($query);

        $team = $db->loadObject();

        if(!empty($team)) {

            $options = json_decode($team->params);

            if(is_null($options)) {
                
                $options = new stdClass();
            }

            $allowedParams = [
                'allow_adding_users',
                'allow_removing_users_from_team',
                'allow_removing_users_from_system',
                'allow_managing_users_groups',
                'allow_assigning_courses',
                'allow_assigning_events'
            ];

            // Build out the options object based on API request params
            foreach($data as $field => $value) {

                if(!isset($value)) {
                    continue;
                }

                // Convert the field name from StudlyCase to snake_case
                $fieldSlug = preg_replace('/(?<!^)[A-Z]/', '_$0', $field);
                $fieldSlug = strtolower($fieldSlug);

                switch($fieldSlug) {

                    case 'add_managed_user_groups':

                        // Add the AddManagedUserGroups to the usergroups array
                        $options->user_groups = $this->api->updateArray($options->user_groups, $value);
                    break;

                    case 'remove_managed_user_groups':

                        // Remove the groups in RemoveManagedUserGroups from the usergroups array
                        $options->user_groups = $this->api->updateArray($options->user_groups, null, $value);
                    break;

                    case 'add_allowed_courses':
                        
                        // Add the AddAllowedCourses course ids to the allowed courses array
                        $courseIds = !empty($options->course_ids) ? explode(',', $options->course_ids) : [];
                        $allowedCourses = $this->api->updateArray($courseIds, $value);

                        $options->course_ids = implode(',', $allowedCourses);
                    break;

                    case 'remove_allowed_courses':

                        // Remove the RemoveAllowedCourses course ids from the allowed courses array
                        $courseIds = !empty($options->course_ids) ? explode(',', $options->course_ids) : [];
                        $allowedCourses = $this->api->updateArray($courseIds, null, $value);

                        $options->course_ids = implode(',', $allowedCourses);
                    break;

                    default:

                        if(in_array($fieldSlug, $allowedParams)) {

                            $options->{$fieldSlug} = $value ? '1' : '0';
                        }
                    break;
                }
            }

            $team->params = json_encode($options);

            try {

                $result = $db->updateObject('axs_teams', $team, 'id');

                if($result) {

                    $response->Status = 'success';
                    $response->id = $team->id;
                } else {

                    $response->Status = 'fail';
                    $response->Error = 'Could not update user team record.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Error = 'Could not update user team record.';
            }
        } else {

            $response->Status = 'fail';
            $response->Error  = "Team with id $teamId doesn't exist.";
        }

        return $response;
    }

    private function getUserInformationForUserIds($userIds) {
        $users = array();

        // Regular strings of user IDs should be exploded into an array
        if(!is_array($userIds)) {

            $userIdsArray = explode(',', $userIds);
        } else {

            // If the user ids are already in an array, no preprocessing is needed.
            $userIdsArray = $userIds;
        }

        foreach($userIdsArray as $userId) {
            $user = $this->userLib->getUser($userId);

            if(empty($user)) {
                continue;
            }

            $userData = new stdClass();

            $userData->id = $user->id;
            $userData->Name = $user->name;
            $userData->Email = $user->email;

            $users[] = $userData;
        }

        return $users;
    }
}