<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
class Events {
    protected $api = null;
    protected $eventIds = null;
    protected $items = null;
    protected $bodyData = null;
    protected $globalConfig = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;
        $this->bodyData = $this->api->body;

        // Get the event ID(s) from the request
        if(!empty($this->items['events'])) {

            $this->eventIds = $this->api->quoteFilterList($this->items['events'],'int');
        } elseif(!empty($this->api->filter['event_ids'])) {

            $this->eventIds = $this->api->quoteFilterList($this->api->filter['event_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {

            $this->eventIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif(!empty($this->bodyData) && isset($this->bodyData->event_ids))  {

            $this->eventIds = $this->api->quoteFilterList($this->bodyData->event_ids, 'int');
        }

        // We need to bootstrap the EventBooking helper and other support mechanisms to grab the global event config
        JLoader::registerPrefix('RAD', dirname(__FILE__));
        JLoader::registerPrefix('Eventbooking', JPATH_BASE . '/components/com_eventbooking');
        JLoader::register('EventbookingHelper', JPATH_ROOT . '/components/com_eventbooking/helper/helper.php');

        $this->globalConfig = EventbookingHelper::getConfig();
    }

    public function getEvents() {
        $events = [];

        // First, determine whether or not we have at least one ID available
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $columns = [
            'event.id as id',
            'event.title as Title',

            // Location Info
            'loc.id as LocationId',
            'loc.name as LocationName',
            'loc.address as LocationAddress',
            'loc.city as LocationCity',
            'loc.state as LocationState',
            'loc.zip as LocationZip',
            'loc.country as LocationCountry',
            'loc.lat as LocationLatitude',
            'loc.long as LocationLongitude',

            // Various event metadata
            'event.event_date as StartDate',
            'event.event_end_date as EndDate',
            'event.short_description as ShortDescription',
            'event.description as FullDescription',
            'event.individual_price as IndividualPrice',
            'event.event_capacity as Capacity',
            'event.registration_start_date as RegistrationStartDate',
            'event.cut_off_date as CutOffDate',
            'event.event_password as Password',

            // Event image and page links
            'event.image as Image',
            'event.alias as Alias',
            'event.params as Params',

            // Event flags (published, enable_auto_reminder, featured)
            'event.published as Published',
            'event.enable_auto_reminder as AutoReminder',
            'event.featured as Featured',

            // Access levels
            'event.access',
            'event.registration_access',

            // Language of event
            'event.language as Language',
        ];

        $query->select($columns)
              ->from('#__eb_events as event')
              ->leftJoin('#__eb_locations as loc on loc.id = event.location_id');

        if(!empty($this->eventIds)) {
            $query->where("event.id in ({$this->eventIds})");
        }

        $db->setQuery($query);

        try {

            $events = $db->loadObjectList();

            $response = new stdClass();

            if(is_null($events)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_NOT_FOUND: No data could be found. ' . $db->getErrorMsg();
                $response->ErrorCode = 'API_NOT_FOUND';

                return $response;
            }

            foreach($events as &$event) {

                $params = json_decode($event->Params);

                $timezone = (!empty($params) && isset($params->time_zone)) ? $params->time_zone : null;

                if(!is_null($timezone)) {

                    $timeZoneObject = new DateTimeZone($timezone);

                    $outputFormat = DateTime::ATOM;
                    $sourceFormat = 'Y-m-d H:i:s';
                    
                    if(!is_null($event->StartDate) && $event->StartDate != '0000-00-00 00:00:00') {

                        $eventDate = DateTime::createFromFormat($sourceFormat, $event->StartDate, $timeZoneObject);
                        $event->StartDate = !$eventDate ? '0000-00-00 00:00:00' : $eventDate->format($outputFormat);
                    }
                    
                    if(!is_null($event->EndDate) && $event->EndDate != '0000-00-00 00:00:00') {

                        $eventEndDate = DateTime::createFromFormat($sourceFormat, $event->EndDate, $timeZoneObject);
                        $event->EndDate = !$eventEndDate ? '0000-00-00 00:00:00' : $eventEndDate->format($outputFormat);
                    }
                    
                    if(!is_null($event->RegistrationStartDate) && $event->RegistrationStartDate != '0000-00-00 00:00:00') {
                    
                        $registrationStartDate = DateTime::createFromFormat($sourceFormat, $event->RegistrationStartDate, $timeZoneObject);
                    
                        $event->RegistrationStartDate = !$registrationStartDate ? '0000-00-00 00:00:00' : $registrationStartDate->format($outputFormat);
                    }
                    
                    if(!is_null($event->CutOffDate) && $event->CutOffDate != '0000-00-00 00:00:00') {
                    
                        $registrationCutoffDate = DateTime::createFromFormat($sourceFormat, $event->CutOffDate, $timeZoneObject);
                    
                        $event->CutOffDate = !$registrationCutoffDate ? '0000-00-00 00:00:00' : $registrationCutoffDate->format($outputFormat);
                    }
                }

                // Set the timezone on the return object
                $event->Timezone = $timezone;

                // Create the Location object
                $location = new stdClass();

                $location->id = intval($event->LocationId);
                $location->Name = $event->LocationName;
                $location->Address = $event->LocationAddress;
                $location->City = $event->LocationCity;
                $location->State = $event->LocationState;
                $location->Zip = $event->LocationZip;
                $location->Country = $event->LocationCountry;
                $location->Latitude = $event->LocationLatitude;
                $location->Longitude = $event->LocationLongitude;

                unset($event->LocationId);
                unset($event->LocationName);
                unset($event->LocationAddress);
                unset($event->LocationCity);
                unset($event->LocationState);
                unset($event->LocationZip);
                unset($event->LocationCountry);
                unset($event->LocationLatitude);
                unset($event->LocationLongitude);

                $event->Location = $location;

                // Set up the absolute URLs
                $event->Image = $this->api->domainRoot() . $event->Image;

                // We want to call the event URL "Url" instead of "Alias"
                $event->Url = $this->buildEventUri($event);
                unset($event->Alias);

                $event->CategoryNames = $this->getEventCategoryNamesForEventId($event->id);

                // View and registration access levels
                $event->ViewAccess = AxsExtra::accessLevelName($event->access);
                $event->RegistrationAccess = AxsExtra::accessLevelName($event->registration_access);

                // Remove the old fields from the output data
                unset($event->access);
                unset($event->registration_access);

                // Don't need Params
                unset($event->Params);

                // Convert the integer params to integers, boolean params to bools, etc.
                $event->id = intval($event->id);
                $event->Capacity = intval($event->Capacity);
                $event->Published = boolval($event->Published);
                $event->Featured = boolval($event->Featured);
                $event->AutoReminder = boolval($event->AutoReminder);
            }
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Error API_DATABASE_EXCEPTION: ' . $db->getErrorMsg();
            $response->ErrorCode = 'API_DATABASE_EXCEPTION';

            return $response;
        }

        return $events;
    }

    public function updateEvents() {

        $response = new stdClass();

        if(is_null($this->eventIds)) {

            $response->Status = 'fail';
            $response->Message = 'Error API_MISSING_IDS: No eventIds were provided.';
            $response->ErrorCode = 'API_MISSING_IDS';
            
            return $response;
        }

        if(!isset($this->bodyData)) {

            $response->Status = 'fail';
            $response->Message = 'Error API_MISSING_BODY: No data is present; cannot update events.';
            $response->ErrorCode = 'API_MISSING_BODY';
        }

        // Get the events in the event_ids list, keeping track of how many results we get back
        $db = JFactory::getDbo();

        $eventUpdateFields = [
            'id',
            'enable_auto_reminder',
            'title',
            'event_date',
            'event_end_date',
            'params',
            'short_description',
            'description',
            'individual_price',
            'event_capacity',
            'registration_start_date',
            'cut_off_date',
            'image',
            'thumb',
            'location_id',
            'category_id',
            'event_password',
            'access',
            'registration_access',
            'featured',
            'enable_auto_reminder',
            'published',
            'language'
        ];

        $query = $db->getQuery(true);

        $query->select($eventUpdateFields)
              ->from('#__eb_events')
              ->where("id IN ({$this->eventIds})");

        $db->setQuery($query);

        $events = $db->loadObjectList();
        $eventsMatched = 0;
        
        if(!is_null($events) && is_countable($events)) {

            $eventsMatched = count($events);
        }

        if($eventsMatched == 0) {

            $response->Status = 'fail';
            $response->Message = 'Error API_NOT_FOUND: No events could be found matching the IDs provided.';
            $response->ErrorCode = 'API_NOT_FOUND';

            return $response;
        }

        $eventsUpdated = [];

        // Update the events given the common fields that should be updated for all existing events requested
        foreach($events as $event) {

            // Parse the given fields and ensure that they are in the correct format for their types
            if(!empty($this->bodyData->TimeZone)) {

                $timeZone = $this->bodyData->TimeZone;

                try {

                    $timeZoneObject = new DateTimeZone($timeZone);
                } catch(Exception $ex) {
    
                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_TIMEZONE: ' . $ex->getMessage();
                    $response->ErrorCode = 'API_INVALID_TIMEZONE';
    
                    return $response;
                }

                $eventParams = new stdClass();

                // Grab the existing event params, if any
                if(isset($event->params)) {

                    $eventParams = json_decode($event->params);
                }
                
                $eventParams->time_zone = $timeZone;

                $event->params = json_encode($eventParams);
            }

            // Event Title
            if(isset($this->bodyData->Title)) {
                
                $event->title = strip_tags($this->bodyData->Title);
            }

            // Event dates

            // Start Date
            if(isset($this->bodyData->StartDate)) {

                $startDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->StartDate, $timeZoneObject);

                // If $startDate = false, the timezone conversion failed - let the user know
                if(!$startDate) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                    $response->ErrorCode = 'API_INVALID_DATE';

                    return $response;
                }

                $event->event_date = $startDate->format('Y-m-d H:i:s');
            }
            
            // End Date
            if(!empty($this->bodyData->EndDate)) {

                $endDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->EndDate, $timeZoneObject);

                // If $startDate = false, the timezone conversion failed - let the user know
                if(!$endDate) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                    $response->ErrorCode = 'API_INVALID_DATE';

                    return $response;
                }

                $event->event_end_date = $endDate->format('Y-m-d H:i:s');
            }

            // Registration Start Date
            if(!empty($this->bodyData->RegistrationStartDate)) {

                $registrationStartDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->RegistrationStartDate, $timeZoneObject);

                // If $startDate = false, the timezone conversion failed - let the user know
                if(!$registrationStartDate) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                    $response->ErrorCode = 'API_INVALID_DATE';

                    return $response;
                }
                
                $event->registration_start_date = $registrationStartDate->format('Y-m-d H:i:s'); 
            }

            // Registration Cutoff (end) Date
            if(!empty($this->bodyData->CutOffDate)) {

                $registrationCutoffDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->CutOffDate, $timeZoneObject);

                // If $startDate = false, the timezone conversion failed - let the user know
                if(!$registrationCutoffDate) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                    $response->ErrorCode = 'API_INVALID_DATE';

                    return $response;
                }

                $event->cut_off_date = $registrationCutoffDate->format('Y-m-d H:i:s');
            }

            // Short Event Description (HTML-enabled)
            if(!empty($this->bodyData->ShortDescription)) {

                $event->short_description = $this->api->cleanHTMLInput($this->bodyData->ShortDescription);
            }

            // Long Event Description (HTML-enabled)
            if(!empty($this->bodyData->FullDescription)) {

                $event->description = $this->api->cleanHTMLInput($this->bodyData->FullDescription);
            }

            /* Parse the IndividualPrice parameter - must be of the format d{n}.cc, where n can be between 1 and n
             * digits which represents the dollar amount, and cc represents the 0-padded cent amount (00 thru 99).
             * 
             * The decimal point is optional for whole numbers, but must be present when the cents are given.
             *
             * Examples of valid strings: 1.50, 100.44, 0.99, 0
             * Examples of invalid strings: 45.533, a.bcd, -1.10
             */
            if(!empty($this->bodyData->IndividualPrice)) {

                $validFormatPattern = "/^[^-]?\d+(\.?\d{1,2})?$/";
                $hasValidFormat = preg_match($validFormatPattern, $this->bodyData->IndividualPrice);

                if($hasValidFormat) {

                    // Price matches the valid format, so convert it to a number and store it
                    $event->individual_price = floatval($this->bodyData->IndividualPrice);
                } else {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_INVALID_CURRENCY_FORMAT: The currency format provided is invalid. Please refer to the API documentation for details.';
                    $response->ErrorCode = 'API_INVALID_CURRENCY_FORMAT';

                    return $response;
                }
            }

            // Parse the Capacity parameter - must be a positive integer
            if(!empty($this->bodyData->Capacity)) {

                $capacity = intval($this->bodyData->Capacity);

                if($capacity < 0) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_NEGATIVE_COUNTING_NUMBER: Counting numbers should not be negative.';
                    $response->ErrorCode = 'API_COUNTING_NUMBER';

                    return $response;
                }

                $event->event_capacity = $capacity;
            }            

            // Image link
            if(!empty($this->bodyData->Image)) {

                // Not only do we need a correct image path, we need to build the thumbnail for it.
                $givenImage = $this->bodyData->Image;
                $absoluteImagePath = JPATH_ROOT . '/' . $givenImage;
    
                $exists = file_exists($absoluteImagePath);
    
                if(!$exists) {
    
                    $response->Status = 'fail';
                    $response->Message = 'Error API_FILE_NOT_FOUND: The file provided cannot be found.';
                    $response->ErrorCode = 'API_FILE_NOT_FOUND';
    
                    return $response;
                } else {
    
                    // Build the thumbnail for the image path and store the filename in the DB as the thumbnail
                    $baseFileName = basename($givenImage);
                
                    $fileName = time() . "_" . preg_replace('/[^a-zA-Z0-9.]/', '', $baseFileName);
                    
                    $imagePath = JPATH_ROOT . '/'.AxsImages::getImagesPath('events').'/images/' . $fileName;
                    $thumbPath = JPATH_ROOT . '/'.AxsImages::getImagesPath('events').'/images/thumbs/' . $fileName;
    
                    copy($absoluteImagePath, $imagePath);
    
                    if (!$this->globalConfig->thumb_width)
                    {
                        $this->globalConfig->thumb_width = 500;
                    }
    
                    if (!$this->globalConfig->thumb_height)
                    {
                        $this->globalConfig->thumb_height = 313;
                    }
    
                    $image = new JImage($imagePath);
                    
                    $image->cropResize($this->globalConfig->thumb_width, $this->globalConfig->thumb_height, false)
                            ->toFile($thumbPath);
    
                    $event->thumb = $fileName;
                    $event->image = AxsImages::getImagesPath('events').'/images/' . $fileName;
                }
            }

            // Attach the location ID to the new event record
            if(!empty($this->bodyData->Location)) {

                $locationId = $this->bodyData->Location;

                // Search for the location to ensure it exists
                $locationName = AxsEvents::getEventLocationNameById($locationId);

                if(is_null($locationName)) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_RECORD_NOT_FOUND: The location corresponding to the given ID could not be found.';
                    $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                    return $response;
                }

                $event->location_id = $locationId;
            }

            if(!empty($this->bodyData->Category)) {

                $category = htmlentities(strip_tags($this->bodyData->Category));

                $categoryData = AxsEvents::getEventCategoryByName($category);

                if(is_null($categoryData)) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_RECORD_NOT_FOUND: The category corresponding to the given category name could not be found.';
                    $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                    return $response;
                }

                $categoryId = intval($categoryData->id);

                $mainCategoryRecord = $this->getMainEventCategoryXRefRecord($event->id);

                $mainCategoryRecord->category_id = $categoryId;
                $event->category_id = $categoryId;

                $db->updateObject('joom_eb_event_categories', $mainCategoryRecord, 'id');
            }

            if(!empty($this->bodyData->Password)) {

                $event->event_password = strip_tags($this->bodyData->Password);
            }

            // Add view and registration access levels
            if(!empty($this->bodyData->ViewAccess)) {

                $viewAccess = AxsExtra::getAccessLevelByName($this->bodyData->ViewAccess);

                if(is_null($viewAccess)) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_RECORD_NOT_FOUND: The access level corresponding to the given level name could not be found.';
                    $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                    return $response;
                }

                $event->access = intval($viewAccess->id);
            }

            if(!empty($this->bodyData->RegistrationAccess)) {

                $registrationAccess = AxsExtra::getAccessLevelByName($this->bodyData->RegistrationAccess);

                if(is_null($registrationAccess)) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_RECORD_NOT_FOUND: The access level corresponding to the given level name could not be found.';
                    $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                    return $response;
                }

                $event->registration_access = intval($registrationAccess->id);
            }

            if(isset($this->bodyData->Featured) && is_bool($this->bodyData->Featured)) {

                $event->featured = $this->bodyData->Featured;
            }

            if(isset($this->bodyData->AutoReminder) && is_bool($this->bodyData->AutoReminder)) {

                $event->enable_auto_reminder = $this->bodyData->AutoReminder;
            }

            if(isset($this->bodyData->Published) && is_bool($this->bodyData->Published)) {

                $event->published = $this->bodyData->Published;
            }

            // By default, the languages string will be set to * to indicate support for all languages that Tovuti
            // supports.
            $language = '*';

            // Process the Language parameter
            if(!empty($this->bodyData->Language)) {

                $language = $this->bodyData->Language;

                // Get the supported language codes (languages that are published within a Tovuti instance)
                $supportedLanguages = AxsLanguage::getPublishedLanguages();
                $supportedLanguageCodes = array_map(function($languageObject) {

                    return $languageObject->lang_code;
                }, $supportedLanguages);

                // If the languages in the parameter are not a subset of the supported languages, one or more languages in
                // the given language array are not supported within this Tovuti instance.
                $languageExists = in_array($language, $supportedLanguageCodes);

                if(!$languageExists) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_UNSUPPORTED_LANGUAGE: The language provided is not supported.';
                    $response->ErrorCode = 'API_UNSUPPORTED_LANGUAGE';

                    return $response;
                }
            }

            $event->language = $language;

            try {

                $result = $db->updateObject('#__eb_events', $event, 'id');

                if($result) {

                    // Add the ID to the succesfully processed events list
                    $eventsUpdated[] = $event->id;
                } else {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_DB_EXCEPTION: ' . $db->getErrorMsg();
                    $response->ErrorCode = 'API_DB_EXCEPTION';

                    return $response;
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Error API_DB_EXCEPTION: ' . $db->getErrorMsg();
                $response->ErrorCode = 'API_DB_EXCEPTION';

                return $response;
            }
        }

        $updatedEventCount = is_countable($eventsUpdated) ? count($eventsUpdated) : 0;
        $givenEventIds = explode(',', $this->eventIds);
        $eventsInBody = 0;

        if(!empty($givenEventIds) && is_countable($givenEventIds)) {

            $eventsInBody = count($givenEventIds);
        }

        if($updatedEventCount > 0) {

            $response->Status = 'success';
            $response->UpdatedIds = $eventsUpdated;
            $response->Message = "Successfully updated $updatedEventCount events out of $eventsInBody requested events.";
        } else {

            $response->Status = 'fail';
            $response->Message = 'Error API_NONE_UPDATED: No events were updated.';
            $response->ErrorCode = 'API_NONE_UPDATED';
        }

        return $response;
    }

    public function deleteEvents() {

        $response = new stdClass();

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->delete('#__eb_events');

        if(!empty($this->eventIds)) {

            $query->where("id IN ($this->eventIds)");
        } else {

            $response->Status = 'fail';
            $response->Message = 'Error API_NO_IDS: No event IDs were given. You must provide at least one event ID to delete.';
            $response->ErrorCode = 'API_NO_IDS';

            return $response;
        }

        $db->setQuery($query);

        try {

            $result = $db->execute();

            if(!$result) {

                $response->Status = 'fail';
                $response->Message = 'Error API_DB_EXCEPTION: ' . $db->getErrorMsg();
                $response->ErrorCode = 'API_DB_EXCEPTION';

                return $response;
            }
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Error API_DB_EXCEPTION: ' . $db->getErrorMsg();
            $response->ErrorCode = 'API_DB_EXCEPTION';

            return $response;
        }

        $itemsDeleted = $db->getAffectedRows();

        $response->Status = 'success';
        $response->Message = "Deleted $itemsDeleted event(s).";

        return $response;
    }

    public function createEvent() {

        $response = new stdClass();
        $newEvent = new stdClass();

        // Check to see if the body has any data
        if(empty($this->bodyData)) {

            $response->Status = 'fail';
            $response->Message = 'Error API_MISSING_PARAMETERS: The request has no body parameters and cannot continue.';
            $response->ErrorCode = 'API_MISSING_PARAMETERS';

            return $response;
        }

        // Required fields: Title, StartDate
        $requiredFieldsExist = !empty($this->bodyData->Title) && !empty($this->bodyData->StartDate);

        if(!$requiredFieldsExist) {

            $response->Status =  'fail';
            $response->Message = 'Error API_MISSING_REQUIRED_FIELDS: One or more body parameters are missing';
            $response->ErrorCode = 'API_MISSING_REQUIRED_FIELDS';

            return $response;
        }

        // Parse the given fields and ensure that they are in the correct format for their types
        $timeZone = !empty($this->bodyData->TimeZone) ? $this->bodyData->TimeZone : 'America/Boise';

        try {

            $timeZoneObject = new DateTimeZone($timeZone);
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Error API_INVALID_TIMEZONE: ' . $ex->getMessage();
            $response->ErrorCode = 'API_INVALID_TIMEZONE';

            return $response;
        }
        // Event Title - Required
        $title = strip_tags($this->bodyData->Title);

        // Start Date - Required
        $startDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->StartDate, $timeZoneObject);

        // If $startDate = false, the timezone conversion failed - let the user know
        if(!$startDate) {

            $response->Status = 'fail';
            $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
            $response->ErrorCode = 'API_INVALID_DATE';

            return $response;
        }

        // Event dates
        $newEvent->event_date = $startDate->format('Y-m-d H:i:s');
        $newEvent->event_end_date = "0000-00-00 00:00:00";
        $newEvent->registration_start_date = "0000-00-00 00:00:00";
        $newEvent->cut_off_date = "0000-00-00 00:00:00";

        // End Date
        if(!empty($this->bodyData->EndDate)) {

            $endDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->EndDate, $timeZoneObject);

            // If $startDate = false, the timezone conversion failed - let the user know
            if(!$endDate) {

                $response->Status = 'fail';
                $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                $response->ErrorCode = 'API_INVALID_DATE';

                return $response;
            }

            $newEvent->event_end_date = $endDate->format('Y-m-d H:i:s');
        }

        // Registration Start Date
        if(!empty($this->bodyData->RegistrationStartDate)) {

            $registrationStartDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->RegistrationStartDate, $timeZoneObject);

            // If $startDate = false, the timezone conversion failed - let the user know
            if(!$registrationStartDate) {

                $response->Status = 'fail';
                $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                $response->ErrorCode = 'API_INVALID_DATE';

                return $response;
            }
            
            $newEvent->registration_start_date = $registrationStartDate->format('Y-m-d H:i:s'); 
        }

        // Registration Cutoff Date
        if(!empty($this->bodyData->CutOffDate)) {

            $registrationCutoffDate = DateTime::createFromFormat('Y-m-d|', $this->bodyData->CutOffDate, $timeZoneObject);

            // If $startDate = false, the timezone conversion failed - let the user know
            if(!$registrationCutoffDate) {

                $response->Status = 'fail';
                $response->Message = 'Error API_INVALID_DATE: The date format provided is invalid.';
                $response->ErrorCode = 'API_INVALID_DATE';

                return $response;
            }

            $newEvent->cut_off_date = $registrationCutoffDate->format('Y-m-d H:i:s');
        }

        if(!empty($this->bodyData->ShortDescription)) {

            $shortDescription = $this->api->cleanHTMLInput($this->bodyData->ShortDescription);
        }

        if(!empty($this->bodyData->FullDescription)) {

            $fullDescription = $this->api->cleanHTMLInput($this->bodyData->FullDescription);
        }

        /* Parse the IndividualPrice parameter - must be of the format d{n}.cc, where n can be between 1 and n digits
         * which represents the dollar amount, and cc represents the 0-padded cent amount (00 thru 99).
         * The decimal point is optional for whole numbers, but must be present when the cents are given.
         *
         * Examples of valid strings: 1.50, 100.44, 0.99, 0
         * Examples of invalid strings: 45.533, a.bcd, -1.10
         */
        $individualEventPrice = 0.00;
        if(!empty($this->bodyData->IndividualPrice)) {

            $validFormatPattern = "/^[^-]?\d+(\.?\d{1,2})?$/";
            $hasValidFormat = preg_match($validFormatPattern, $this->bodyData->IndividualPrice);

            if($hasValidFormat) {

                // Price matches the valid format, so convert it to a number and store it
                $individualEventPrice = floatval($this->bodyData->IndividualPrice);
            } else {

                $response->Status = 'fail';
                $response->Message = 'Error API_INVALID_CURRENCY_FORMAT: The currency format provided is invalid. Please refer to the API documentation for details.';
                $response->ErrorCode = 'API_INVALID_CURRENCY_FORMAT';

                return $response;
            }
        }

        $capacity = 0;

        // Parse the Capacity parameter - must be a positive integer
        if(!empty($this->bodyData->Capacity)) {

            $capacity = intval($this->bodyData->Capacity);

            if($capacity < 0) {

                $response->Status = 'fail';
                $response->Message = 'Error API_NEGATIVE_COUNTING_NUMBER: Counting numbers should not be negative.';
                $response->ErrorCode = 'API_COUNTING_NUMBER';

                return $response;
            }
        }

        // Image link
        if(!empty($this->bodyData->Image)) {

            // Not only do we need a correct image path, we need to build the thumbnail for it.
            $givenImage = $this->bodyData->Image;
            $absoluteImagePath = JPATH_ROOT . '/' . $givenImage;

            $exists = file_exists($absoluteImagePath);

            if(!$exists) {

                $response->Status = 'fail';
                $response->Message = 'Error API_FILE_NOT_FOUND: The file provided cannot be found.';
                $response->ErrorCode = 'API_FILE_NOT_FOUND';

                return $response;
            } else {

                // Build the thumbnail for the image path and store the filename in the DB as the thumbnail
                $baseFileName = basename($givenImage);
			
                $fileName = time() . "_" . preg_replace('/[^a-zA-Z0-9.]/', '', $baseFileName);
                
                $imagePath = JPATH_ROOT . '/'.AxsImages::getImagesPath('events').'/images/' . $fileName;
                $thumbPath = JPATH_ROOT . '/'.AxsImages::getImagesPath('events').'/images/thumbs/' . $fileName;

                copy($absoluteImagePath, $imagePath);

                if (!$this->globalConfig->thumb_width)
                {
                    $this->globalConfig->thumb_width = 500;
                }

                if (!$this->globalConfig->thumb_height)
                {
                    $this->globalConfig->thumb_height = 313;
                }

                $image = new JImage($imagePath);
                
                $image->cropResize($this->globalConfig->thumb_width, $this->globalConfig->thumb_height, false)
                        ->toFile($thumbPath);

                $thumbnail = $fileName;
                $imageFile = AxsImages::getImagesPath('events').'/images/' . $fileName;
            }
        }

        // Attach the location ID to the new event record
        if(!empty($this->bodyData->Location)) {

            $locationId = $this->bodyData->Location;

            // Search for the location to ensure it exists
            $locationName = AxsEvents::getEventLocationNameById($locationId);

            if(is_null($locationName)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_RECORD_NOT_FOUND: The location corresponding to the given ID could not be found.';
                $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                return $response;
            }
        }

        if(!empty($this->bodyData->Category)) {

            $category = htmlentities(strip_tags($this->bodyData->Category));

            $categoryData = AxsEvents::getEventCategoryByName($category);

            if(is_null($categoryData)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_RECORD_NOT_FOUND: The category corresponding to the given category name could not be found.';
                $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                return $response;
            }

            $categoryId = intval($categoryData->id);
        }

        if(!empty($this->bodyData->Password)) {

            $password = strip_tags($this->bodyData->Password);
        }

        // Add view and registration access levels
        if(!empty($this->bodyData->ViewAccess)) {

            $viewAccess = AxsExtra::getAccessLevelByName($this->bodyData->ViewAccess);

            if(is_null($viewAccess)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_RECORD_NOT_FOUND: The access level corresponding to the given level name could not be found.';
                $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                return $response;
            }

            $viewAccessId = intval($viewAccess->id);
        }

        if(!empty($this->bodyData->RegistrationAccess)) {

            $registrationAccess = AxsExtra::getAccessLevelByName($this->bodyData->RegistrationAccess);

            if(is_null($registrationAccess)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_RECORD_NOT_FOUND: The access level corresponding to the given level name could not be found.';
                $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                return $response;
            }

            $registrationAccessId = intval($registrationAccess->id);
        }

        // These should default to false
        $featured = false;
        $autoReminder = false;

        // Published is set to true by default to mirror how our admin section works when creating an event
        $published = true;

        if(isset($this->bodyData->Featured) && is_bool($this->bodyData->Featured)) {

            $featured = $this->bodyData->Featured;
        }

        if(isset($this->bodyData->AutoReminder) && is_bool($this->bodyData->AutoReminder)) {

            $autoReminder = $this->bodyData->AutoReminder;
        }

        if(isset($this->bodyData->Published) && is_bool($this->bodyData->Published)) {

            $published = $this->bodyData->Published;
        }

        // By default, the languages string will be set to * to indicate support for all languages that Tovuti supports.
        $language = '*';

        // Process the Language parameter
        if(!empty($this->bodyData->Language)) {

            $language = $this->bodyData->Language;

            // Get the supported language codes (languages that are published within a Tovuti instance)
            $supportedLanguages = AxsLanguage::getPublishedLanguages();
            $supportedLanguageCodes = array_map(function($languageObject) {

                return $languageObject->lang_code;
            }, $supportedLanguages);

            // If the languages in the parameter are not a subset of the supported languages, one or more languages in
            // the given language array are not supported within this Tovuti instance.
            $languageExists = in_array($language, $supportedLanguageCodes);

            if(!$languageExists) {

                $response->Status = 'fail';
                $response->Message = 'Error API_UNSUPPORTED_LANGUAGE: The language provided is not supported.';
                $response->ErrorCode = 'API_UNSUPPORTED_LANGUAGE';

                return $response;
            }
        }

        if(!empty($this->bodyData->Category)) {

            $category = htmlentities(strip_tags($this->bodyData->Category));

            $categoryData = AxsEvents::getEventCategoryByName($category);

            if(is_null($categoryData)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_RECORD_NOT_FOUND: The category corresponding to the given category name could not be found.';
                $response->ErrorCode = 'API_RECORD_NOT_FOUND';

                return $response;
            }

            $categoryId = intval($categoryData->id);
        }

        // Event title / name
        $newEvent->title = $title;

        // Event descriptions
        $newEvent->short_description = $shortDescription;
        $newEvent->description = $fullDescription;

        // Event details
        $newEvent->individual_price = $individualEventPrice;
        $newEvent->event_capacity = $capacity;
        $newEvent->event_password = $password;

        // Event details
        $newEvent->individual_price = $individualEventPrice;
        $newEvent->event_capacity = $capacity;
        $newEvent->event_password = $password;

        // Event image fields
        $newEvent->thumb = $thumbnail;
        $newEvent->image = $imageFile;

        // Event location ID
        $newEvent->location_id = $locationId;

        // Event category ID
        $newEvent->category_id = $categoryId;

        // Access levels (view access and registration access)
        $newEvent->access = $viewAccessId;
        $newEvent->registration_access = $registrationAccessId;

        // Featured, AutoReminder, Published booleans
        $newEvent->featured = $featured;
        $newEvent->enable_auto_reminder = $autoReminder;
        $newEvent->published = $published;

        $newEvent->language = $language;

        $newEventParams = new stdClass();
        $newEventParams->time_zone = $timeZone;

        $newEvent->params = json_encode($newEventParams);

        // Create the new event record
        $db = JFactory::getDbo();

        try {

            $result = $db->insertObject('#__eb_events', $newEvent, 'id');

            if(!$result) {

                $response->Status = 'fail';
                $response->Message = 'Error API_DATABASE_ERROR: ' . $db->getErrorMsg();
                $response->ErrorCode = 'API_DATABASE_ERROR';

                return $response;
            } else {

                $eventId = intval($newEvent->id);

                // Create the category crossref record
                $categoryXRef = new stdClass();

                $categoryXRef->category_id = $categoryId;
                $categoryXRef->event_id = $eventId;
                $categoryXRef->main_category = 1;

                $eventCategoryResult = $db->insertObject('#__eb_event_categories', $categoryXRef);

                if(!$eventCategoryResult) {

                    $response->Status = 'fail';
                    $response->Message = 'Error API_DATABASE_ERROR: ' . $db->getErrorMsg();
                    $response->ErrorCode = 'API_DATABASE_ERROR';
    
                    return $response;
                } else {

                    $response->Status = 'success';
                    $response->id = $eventId;

                    return $response;
                }
            }
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Error API_DATABASE_ERROR: ' . $db->getErrorMsg();
            $response->ErrorCode = 'API_DATABASE_ERROR';

            return $response;
        }
    }

    private function getEventCategoryNamesForEventId($eventId) {
        $categories = [];

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $categoryColumns = [
            'category.name'
        ];

        $query->select($categoryColumns)
              ->from('#__eb_categories as category')
              ->leftJoin('#__eb_event_categories as eventCategory on eventCategory.category_id = category.id')
              ->where("eventCategory.event_id = $eventId");

        $db->setQuery($query);

        $categories = $db->loadObjectList();

        foreach($categories as $index => $category) {
            $categories[$index] = $category->name;
        }

        return $categories;
    }

    private function buildEventUri($event) {
        $eventUri = $this->api->domainRoot() . 'events-cal';
        $categoryParentAlias = null;
        $categoryAlias = null;
        $parent = null;

        // Get the topmost category attached to this event
        $categories = AxsEvents::getEventCategoriesForEventId($event->id);

        foreach($categories as $category) {

                // Get the parent URI alias
                if($category->parent > 0) {
                    $parentId = $category->parent;
                    $parent = AxsEvents::getEventCategoryParentByCategoryId($category->id);
                    if(!empty($parent)) {
                        $categoryParentAlias = $parent->alias;
                    }

                    if($parent->parent > 0) {
                        $parentArray = array();
                        $parentId = $parent->parent;
                        $i = 0;
                        while($parentId > 0 && $i < 20) {
                            $parent = AxsEvents::getEventCategoryById($parent->parent);
                            if($parent->id > 0) {
                                array_push($parentArray, $parent->alias);
                            }
                            $parentId = $parent->parent;
                            $i++;
                        }
                        if($parentArray) {
                            $parentArray = array_reverse($parentArray);
                            foreach($parentArray as $cat) {
                                $eventUri .= '/' . $cat;
                            }
                        }
                    }
                }

                $categoryAlias = $category->Alias;

                break;
        }



        if(!empty($categoryParentAlias)) {
            $eventUri .= '/' . $categoryParentAlias;
        }

        if(!empty($categoryAlias)) {
            $eventUri .= '/' . $categoryAlias;
        }

        $eventUri .= '/' . $event->Alias;

        return $eventUri;
    }

    public static function getMainEventCategoryXRefRecord($eventId) {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('#__eb_event_categories')
              ->where('event_id = ' . $db->quote($eventId))
              ->andWhere('main_category = 1');

        $db->setQuery($query);

        $record = $db->loadObject();

        return $record;
    }
}