<?php

class Awards {
    protected $api = null;
    protected $awardIds = null;
    protected $items = null;
    protected $baseUrl = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;
        if($this->items['awards']) {
            $this->userIds = $this->api->quoteFilterList($this->items['awards'],'int');
        } elseif($this->api->filter['award_ids']) {
            $this->userIds = $this->api->quoteFilterList($this->api->filter['award_ids'],'int');
        } elseif($this->api->filter['id']) {
            $this->userIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }
        $urlArray = explode('api/v1',JUri::base());
        $this->baseUrl = $urlArray[0];
    }

    public function getAwards($type = null) {
        $awardIds = null;
        if($this->items[$type.'s']) {
            $awardIds = $this->api->quoteFilterList($this->items[$type.'s'],'int');
        }
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_awards');
        if($awardIds) {
            $conditions[] = $db->qn('id').' IN ('.$awardIds.')';
        }
        if($type) {
            $conditions[] = $db->qn('type')." = ".$db->quote($type);
        }
        if($conditions) {
            $query->where($conditions);
        }
        $query->order('title ASC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $dataArray = array();
        foreach($results as $result) {
            $params = json_decode($result->params);
            $data = new stdClass();
            $data->id            = $result->id;
            $data->Title         = $result->title;
            $data->AwardType     = $result->type;
            if($params->expiration_amount && $params->expiration_type) {
                if($params->expiration_amount > 1) {
                    $plural = 's';
                } else {
                    $plural = '';
                }
                $data->ExpirationPeriod = $params->expiration_amount . ' ' . $params->expiration_type.$plural;
            }
            if($result->type == 'badge' && $params->badge_image) {
                $data->Image = $this->baseUrl.$params->badge_image;
            }

            $dataArray[] = $data;
        }
        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function deleteAward($type = null) {

        $response = new stdClass();

        if(!empty($this->api->body->id)) {

            $awardId = $this->api->body->id;
        } else {

            if(!empty($type) && $this->items[$type.'s']) {
                $awardIdString = $this->api->quoteFilterList($this->items[$type.'s'],'int');
            } else {
                $awardIdString = $this->api->quoteFilterList($this->items['awards'],'int');
            }

            $awardIds = explode(',', $awardIdString);
        }

        if(!empty($awardId) || !empty($awardIds)) {

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            if(!empty($awardIds)) {
                $awardId = trim($awardIds[0], '\'');
            }

            // Check to see if the award actually exists before we attempt to delete it
            $conditions = [
                "id = {$db->quote($awardId)}"
            ];

            if(!empty($type)) {
                $conditions[] = "{$db->qn('type')} = {$db->quote($type)}";
            }

            $query->select('*')
                  ->from('axs_awards')
                  ->where($conditions);

            $db->setQuery($query);

            $result = $db->loadObjectList();

            if(!empty($result) && is_array($result) && count($result) > 0) {

                $query = $db->getQuery(true);

                $query->delete('axs_awards')
                      ->where($conditions);

                $db->setQuery($query);

                try {
                    $result = $db->execute();

                    if(!empty($result)) {

                        $response->Status = 'success';
                    } else {

                        $response->Status = 'fail';
                        $response->Message = 'Could not delete the requested award.';
                    }
                } catch(Exception $ex) {

                    $response->Status = 'fail';
                    $response->Message = 'Could not delete the requested award.';
                }
            } else {

                $response->Status = 'fail';
                $response->Message = 'Could not find the requested award to delete.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'Please provide the ID of the award you wish to delete.';
        }

        return $response;
    }

    public function createAward($type) {

        $response = new stdClass();
        $title = !empty($this->api->body->Title) ? AxsSecurity::cleanInput($this->api->body->Title) : null;

        if(!empty($title) && !empty($type)) {
            
            $awardObject = new stdClass();

            $awardObject->title = $title;
            $awardObject->type = $type;
            $awardObject->description = AxsSecurity::cleanInput($this->api->body->Description);
            $awardObject->enabled = true;   // Default newly created awards to enabled by default

            $params = new stdClass();

            // If this is a certificate, include the certificate template ID to use
            if($type == 'certificate') {

                $params->certificate = AxsSecurity::alphaNumericOnly($this->api->body->CertificateTemplateId);
            }
  
            // Process Expiration Types
            $expirationTypes = ['year','month','week','day'];

            $params->expiration_type = strtolower($this->api->parseRestrictedInput($this->api->body->ExpirationType, $expirationTypes));

            // Process Expiration Amount
            $params->expiration_amount = AxsSecurity::alphaNumericOnly($this->api->body->ExpirationAmount);
            
            // Process Requirement Types
            $requirementTypes = ['conditional', 'assigned'];

            $params->requirement_type = strtolower($this->api->parseRestrictedInput($this->api->body->RequirementType, $requirementTypes));

            if(!empty($this->api->body->UserCanLoseBadge) && $this->api->body->UserCanLoseBadge == 'yes') {

                $params->losebadge = 'yes';
            } else {

                $params->losebadge = 'no';
            }

            // If the requirement type is conditional, process AllRequirements and parse the set of requirements
            if($params->requirement_type == 'conditional') {
                
                $params->requirements_all = $this->api->parseYesNoInput($this->api->body->AllRequirements);

                $requirementsInput = @$this->api->body->Requirements;

                if(!empty($requirementsInput)) {
                    
                    // Process the set of requirements passed in
                    $requirements = new stdClass();

                    $requirementTypes = ['category','course','lesson','usergroup','accesslevel','event'];

                    foreach($requirementsInput as $index => $requirement) {
                        
                        $parsedRequirement = new stdClass();
                        $parsedRequirement->requirement_active = ($requirement->RequirementActive ? 'true' : 'false');
                        $parsedRequirement->requirement_type = strtolower($this->api->parseRestrictedInput($requirement->RequirementType, $requirementTypes));

                        $parsedRequirement->requirement_completion = 'any';
                        
                        switch($parsedRequirement->requirement_type) {
                            case 'category':
                                $parsedRequirement->required_category = $requirement->Categories;
                            break;

                            case 'course':
                                $parsedRequirement->required_course = $requirement->Courses;
                            break;

                            case 'lesson':
                                $parsedRequirement->required_lesson = $requirement->Lessons;
                            break;

                            case 'usergroup':
                                $parsedRequirement->usergroup = explode(',', $this->api->parseArrayParameterAsString($requirement->UserGroups, $this->api->forbiddenGroups));
                            break;

                            case 'accesslevel':
                                $parsedRequirement->accesslevel = explode(',', $this->api->parseArrayParameterAsString($requirement->AccessLevels, $this->api->forbiddenAccessLevels));
                            break;

                            case 'event':
                                $parsedRequirement->required_event_action = $requirement->EventAction;
                                $parsedRequirement->required_event = $requirement->Events;
                            break;
                        }

                        $requirements->{'requirement' . ($index + 1)} = $parsedRequirement;
                    }

                    $params->requirements = $requirements;
                }
            }

            // If the requirement type is "assigned", parse the UserList property
            if($params->requirement_type == 'assigned' && !empty($this->api->body->UserList)) {

                if(is_array($this->api->body->UserList)) {

                    $params->user_list = $this->api->parseArrayParameterAsString($this->api->body->UserList, $this->api->forbiddenUsers);
                } else {

                    $userList = explode(',', AxsSecurity::cleanInput($this->api->body->UserList));

                    $params->user_list = $this->api->parseArrayParameterAsString($userList, $this->api->forbiddenUsers);
                }
            }

            // Add or Remove User Groups
            if(!empty($this->api->body->AddRemoveUserGroups) && $this->api->body->AddRemoveUserGroups == 'yes') {

                $params->usergroup_actions = '1';

                $filteredAddUserGroups = $this->api->parseArrayParameterAsString($this->api->body->UserGroupsToAdd, $this->api->forbiddenGroups);

                $params->add_usergroups = explode(',', $filteredAddUserGroups);

                $filteredRemoveUserGroups = $this->api->parseArrayParameterAsString($this->api->body->UserGroupsToRemove, $this->api->forbiddenGroups);

                $params->remove_usergroups = explode(',', $filteredRemoveUserGroups);
            } else {

                $params->usergroup_actions = '0';
            }

            // Process points
            if(!empty($this->api->body->AwardPoints) && $this->api->body->AwardPoints == 'yes') {

                $params->points_actions = '1';

                $params->points_category = AxsSecurity::alphaNumericOnly($this->api->body->PointsCategoryId);
                $params->points = AxsSecurity::alphaNumericOnly($this->api->body->PointsForCompletion);
            } else {

                $params->points_actions = '0';
            }

            $awardObject->params = json_encode($params);

            try {

                $db = JFactory::getDbo();

                $result = $db->insertObject('axs_awards', $awardObject);

                if($result) {
                    $response->Status = 'success';
                    $response->id = $db->insertid();
                } else {
                    $response->Status = 'fail';
                    $response->Message = 'Could not create the award with the information given.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Could not create the award with the information given.';
            }
        } else {
            $response->Status = 'fail';
            $response->Message = 'One or more required parameters are missing.';
        }

        return $response;
    }

    public function editAward($type = null) {

        $response = new stdClass();

        $params = new stdClass();

        $db = JFactory::getDbo();

        if(!empty($this->api->body->id)) {

            $awardId = $this->api->body->id;
        } else {

            if(!empty($type) && $this->items[$type.'s']) {
                $awardIdString = $this->api->quoteFilterList($this->items[$type.'s'],'int');
            } else {
                $awardIdString = $this->api->quoteFilterList($this->items['awards'],'int');
            }

            $awardIds = explode(',', $awardIdString);
        }

        if(!empty($awardId) || !empty($awardIds)) {

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            if(!empty($awardIds)) {
                $awardId = trim($awardIds[0], '\'');
            }

            $title = !empty($this->api->body->Title) ? AxsSecurity::cleanInput($this->api->body->Title) : null;
            $awardType = !empty($this->api->body->Type) ? AxsSecurity::cleanInput($this->api->body->Type) : null;
                
            // Check to see if the award actually exists before we attempt to delete it
            $conditions = [
                "id = {$db->quote($awardId)}"
            ];

            if(!empty($type)) {
                $conditions[] = "{$db->qn('type')} = {$db->quote($type)}";
            }

            $query->select('*')
                ->from('axs_awards')
                ->where($conditions);

            $db->setQuery($query);

            $result = $db->loadObject();

            if(!empty($result)) {
                
                $awardObject = $result;

                if(!empty($title)) {

                    $awardObject->title = $title;
                }

                if(!empty($awardType)) {

                    $awardObject->type = $awardType;
                }

                if(!empty($this->api->body->Description)) {

                    $awardObject->description = AxsSecurity::cleanInput($this->api->body->Description);
                }

                // If we're changing this award to a certificate, change the certificate template ID, if provided.
                if($type == 'certificate' && !empty($this->api->body->CertificateTemplateId)) {

                    $params->certificate = AxsSecurity::alphaNumericOnly($this->api->body->CertificateTemplateId);
                }

                // Process Expiration Types
                $expirationTypes = ['year','month','week','day'];

                if(!empty($this->api->body->ExpirationType)) {

                    $params->expiration_type = strtolower($this->api->parseRestrictedInput($this->api->body->ExpirationType, $expirationTypes));
                }
                
                // Process Expiration Amount

                if(!empty($this->api->body->ExpirationAmount)) {

                    $params->expiration_amount = AxsSecurity::alphaNumericOnly($this->api->body->ExpirationAmount);
                }
                
                // Process Requirement Types
                $requirementTypes = ['conditional', 'assigned'];

                if(!empty($this->api->body->RequirementType)) {

                    $params->requirement_type = strtolower($this->api->parseRestrictedInput($this->api->body->RequirementType, $requirementTypes));
                }
                
                if(!empty($this->api->body->UserCanLoseBadge)) {

                    $params->losebadge = strtolower($this->api->body->UserCanLoseBadge);
                }

                // If the requirement type is conditional, process AllRequirements and parse the set of requirements
                if($params->requirement_type == 'conditional') {
                    
                    $params->requirements_all = $this->api->parseYesNoInput($this->api->body->AllRequirements);

                    $requirementsInput = @$this->api->body->Requirements;

                    if(!empty($requirementsInput)) {
                        
                        // Process the set of requirements passed in
                        $requirements = new stdClass();

                        $requirementTypes = ['category','course','lesson','usergroup','accesslevel','event'];

                        foreach($requirementsInput as $index => $requirement) {
                            
                            $parsedRequirement = new stdClass();
                            $parsedRequirement->requirement_active = ($requirement->RequirementActive ? 'true' : 'false');
                            $parsedRequirement->requirement_type = strtolower($this->api->parseRestrictedInput($requirement->RequirementType, $requirementTypes));

                            $parsedRequirement->requirement_completion = 'any';
                            
                            switch($parsedRequirement->requirement_type) {
                                case 'category':
                                    $parsedRequirement->required_category = $requirement->Categories;
                                break;

                                case 'course':
                                    $parsedRequirement->required_course = $requirement->Courses;
                                break;

                                case 'lesson':
                                    $parsedRequirement->required_lesson = $requirement->Lessons;
                                break;

                                case 'usergroup':
                                    $parsedRequirement->usergroup = explode(',', $this->api->parseArrayParameterAsString($requirement->UserGroups, $this->api->forbiddenGroups));
                                break;

                                case 'accesslevel':
                                    $parsedRequirement->accesslevel = explode(',', $this->api->parseArrayParameterAsString($requirement->AccessLevels, $this->api->forbiddenAccessLevels));
                                break;

                                case 'event':
                                    $parsedRequirement->required_event_action = $requirement->EventAction;
                                    $parsedRequirement->required_event = $requirement->Events;
                                break;
                            }

                            $requirements->{'requirement' . ($index + 1)} = $parsedRequirement;
                        }

                        $params->requirements = $requirements;
                    }
                }

                // If the requirement type is "assigned", parse the UserList property
                if($params->requirement_type == 'assigned' && !empty($this->api->body->UserList)) {

                    if(is_array($this->api->body->UserList)) {

                        $params->user_list = $this->api->parseArrayParameterAsString($this->api->body->UserList, $this->api->forbiddenUsers);
                    } else {

                        $userList = explode(',', AxsSecurity::cleanInput($this->api->body->UserList));

                        $params->user_list = $this->api->parseArrayParameterAsString($userList, $this->api->forbiddenUsers);
                    }
                }

                // Add or Remove User Groups
                if(!empty($this->api->body->AddRemoveUserGroups) && $this->api->body->AddRemoveUserGroups == 'yes') {

                    $params->usergroup_actions = '1';

                    $filteredAddUserGroups = $this->api->parseArrayParameterAsString($this->api->body->UserGroupsToAdd, $this->api->forbiddenGroups);

                    $params->add_usergroups = explode(',', $filteredAddUserGroups);

                    $filteredRemoveUserGroups = $this->api->parseArrayParameterAsString($this->api->body->UserGroupsToRemove, $this->api->forbiddenGroups);

                    $params->remove_usergroups = explode(',', $filteredRemoveUserGroups);
                } else {

                    $params->usergroup_actions = '0';
                }

                // Process points
                if(!empty($this->api->body->AwardPoints) && $this->api->body->AwardPoints == 'yes') {

                    $params->points_actions = '1';

                    $params->points_category = AxsSecurity::alphaNumericOnly($this->api->body->PointsCategoryId);
                    $params->points = AxsSecurity::alphaNumericOnly($this->api->body->PointsForCompletion);
                } else {

                    $params->points_actions = '0';
                }

                $awardObject->params = json_encode($params);

                try {

                    $result = $db->updateObject('axs_awards', $awardObject, 'id');

                    if($result) {

                        $response->Status = 'success';
                        $response->id = $awardObject->id;
                    } else {

                        $response->Status = 'fail';
                        $response->Message = 'Could not edit the award with the information given.';
                    }
                } catch(Exception $ex) {

                    $response->Status = 'fail';
                    $response->Message = 'Could not edit the award with the information given.';
                }
            } else {

                $response->Status = 'fail';
                $response->Message = 'Could not find the award with the information given.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'Please provide an ID for the award you wish to edit.';
        }
        return $response;
    }
}
