<?php

class Checklists {
    protected $api = null;
    protected $checklistIds = null;
    protected $items = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['checklists'])) {
            $this->checklistIds = $this->api->quoteFilterList($this->items['checklists'],'int');
        } elseif(!empty($this->api->filter['checklist_ids'])) {
            $this->checklistIds = $this->api->quoteFilterList($this->api->filter['checklist_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->checklistIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }

        // If the ids are given in the body, process those instead.
        if (!empty($this->api->body->id)) {
            $this->checklistIds = $this->api->body->id;
        } elseif (!empty($this->api->body->checklist_ids)) {
            $this->checklistIds = $this->api->quoteFilterList($this->api->body->checklist_ids);
        }
    }

    public function createChecklist() {
        $db = JFactory::getDbo();

        $checklistData = new stdClass();

        $checklistData->title = @$this->api->body->Title;
        $checklistData->description = @$this->api->body->Description;
        $checklistData->enabled = !empty($this->api->body->Enabled) ? $this->api->body->Enabled : false;
        $checklistData->access_type = @$this->api->body->AccessType;

        $userList = @$this->api->body->Users;
        $userGroupList = @$this->api->body->UserGroups;
        $accessLevelList = @$this->api->body->AccessLevels;

        $items = @$this->api->body->Items;

        $response = new stdClass();

        if(!empty($checklistData->title) && !empty($items)) {

            // Create the params object with the given item objects from the API consumer
            $checklistItems = array();
            $parameters = new stdClass();

            // Items is an array of checklist item objects
            foreach($items as $index => $item) {

                $checklistItem = new stdClass();

                $checklistItem->id = AxsLMS::createUniqueID();
                $checklistItem->title = @$item->Title;
                $checklistItem->type = @$item->Type;
                $checklistItem->description = @$item->Description;
                $checklistItem->instructions = @$item->Instructions;
                $checklistItem->course_id = @$item->CourseId;
                $checklistItem->event_id = @$item->EventId;
                $checklistItem->lesson_id = @$item->LessonId;

                $checklistItems['items' . ($index + 1)] = $checklistItem;
            }

            $parameters->items = json_encode($checklistItems);
            $parameters->show_tooltip = '1';
            $parameters->due_date_type = 'none';
            $parameters->checklist_due_dynamic_number = '';
            $parameters->checklist_due_dynamic_unit = 'day';

            $checklistData->params = json_encode($parameters);

            $this->api->processAccessControlParameters($checklistData, $userList, $userGroupList, $accessLevelList);

            if(!empty($this->api->body->DueDate)) {

                $checklistDueDate = DateTime::createFromFormat('Y-m-d', $this->api->body->DueDate);
                $checklistData->due_date = $checklistDueDate->format('Y-m-d');
            } else {

                $checklistData->due_date = 0;
            }

            try {

                $result = $db->insertObject('axs_checklists', $checklistData);

                if($result) {
                    $response->Status = 'success';
                    $response->Id = $db->insertId();
                } else {
                    $response->Status = 'fail';
                    $response->Message = 'Could not create a checklist given the data provided.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Could not create a checklist given the data provided.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'One or more required parameters are missing.';
        }

        return $response;
    }

    public function getChecklists() {
        $checklists = [];

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_checklists');

        if(!empty($this->checklistIds)) {
            $query->where("id in ($this->checklistIds)");
        }

        $query->order('id DESC');
        $db->setQuery($query);
        $results = $db->loadObjectList();

        foreach($results as $result) {
            $item = new stdClass();

            $item->id = intval($result->id);
            $item->Title = $result->title;
            $item->Description = $result->description;
            $item->Enabled = $result->enabled;
            $item->AccessType = $result->access_type;
            $item->UserGroup = $result->usergroup;
            $item->UserList = $result->userlist;
            $item->AccessLevel = $result->accesslevel;
            $item->DueDate = $result->due_date;

            $params = json_decode($result->params);

            $item->Items = json_decode($params->items);
            $item->DueDateType = $params->due_date_type;
            $item->ShowTooltip = $params->show_tooltip;
            $item->DueDynamicUnit = $params->checklist_due_dynamic_unit;
            $item->DueDynamicNumber = $params->checklist_due_dynamic_number;
            $item->NotifyAdmin = $params->notify_admin;
            $item->AdminId = $params->admin_id;
            $item->UserIdsEmailsSent = $params->user_ids_emails_sent;

            $checklists[] = $item;
        }

        if(!empty($checklists)) {
            return $checklists;
        } else {
            throw new Exception('No Data Found');
        }
    }

}
