<?php

class Assignments {
    protected $api = null;
    protected $items = null;
    protected $assignmentIds = null;

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(@$this->items['assignments']) {
            $this->assignmentIds = $this->api->quoteFilterList($this->items['assignments'],'int');
        } elseif(@$this->api->filter['assignment_ids']) {
            $this->assignmentIds = $this->api->quoteFilterList($this->api->filter['assignment_ids'],'int');
        } elseif(@$this->api->filter['id']) {
            $this->assignmentIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }

        // If the ids are given in the body, process those instead.
        if (!empty($this->api->body->id)) {
            $this->assignmentIds = $this->api->body->id;
        } elseif (!empty($this->api->body->assignment_ids)) {
            $this->assignmentIds = $this->api->quoteFilterList($this->api->body->assignment_ids);
        }
    }

    public function getAssignments() {

        $result = [];

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')
              ->from('#__splms_courses_groups');

        if(!empty($this->assignmentIds)) {
            $query->where("id in ({$this->assignmentIds})");
        }

        $db->setQuery($query);

        try {
            $items = $db->loadObjectList();

            foreach($items as $item) {
                $object = new stdClass();

                $object->id = intval($item->id);
                $object->Title = $item->title;
                $object->Type = $item->type == 0 ? 'Assigned' : 'Recommended';
                $object->Free = ($item->free == 1);

                $object->CourseIds = array_map('intval', explode(',', $item->course_ids));
                $object->UserGroups = array_map('intval', explode(',', $item->usergroups));

                $object->UserIds = array_map('intval', explode(',', $item->user_ids));

                $startDate = DateTime::createFromFormat("Y-m-d H:i:s", $item->start_date);
                $object->StartDate = !empty($startDate) ? $startDate->format("Y-m-d") : 0;

                $endDate = DateTime::createFromFormat("Y-m-d H:i:s", $item->end_date);
                $object->EndDate = !empty($endDate) ? $endDate->format("Y-m-d") : 0;

                $dueDate = DateTime::createFromFormat("Y-m-d H:i:s", $item->due_date);
                $object->DueDate = !empty($dueDate) ? $dueDate->format("Y-m-d") : 0;

                $object->DaysToComplete = intval($item->days_to_complete);

                $params = json_decode($item->params);
                $object->SendNotifications = ($params->send_notifications == 1);
                $object->FilterType = $params->assignment_filter_type;

                $result[] = $object;
            }
        } catch(Exception $ex) {
            $response = new stdClass();

            $response->Status = 'fail';
            $response->Message = 'Failed to retrieve any course assignments. Please check your input.';

            return json_encode($response);
        }

        if(!empty($result)) {
            return $result;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function createAssignment() {

        $db = JFactory::getDbo();

        $newAssignment = new stdClass();

        $newAssignment->course_ids = !empty($this->api->body->CourseIds) ? implode(',', $this->api->body->CourseIds) : null;
        $newAssignment->usergroups = TovutiApi::parseArrayParameterAsString(@$this->api->body->UserGroups, $this->api->forbiddenGroups);

        $newAssignment->user_ids = !empty($this->api->body->UserIds) ? implode(',', @$this->api->body->UserIds) : null;
        $newAssignment->title = @$this->api->body->Title;

        // This is to differentiate this record from a Recommendation (both types are stored in the same table)
        $newAssignment->type = 0;

        // If the IsFree parameter is a boolean value and exists, store that value. Otherwise, assume the assignment is
        // free.
        $newAssignment->free = isset($this->api->body->IsFree) ? boolval($this->api->body->IsFree) : true;

        $startDate = DateTime::createFromFormat("Y-m-d", @$this->api->body->StartDate);
        $newAssignment->start_date = !empty($startDate) ? $startDate->format("Y-m-d") : 0;

        $endDate = DateTime::createFromFormat("Y-m-d", @$this->api->body->EndDate);
        $newAssignment->end_date = !empty($endDate) ? $endDate->format("Y-m-d") : 0;

        $dueDate = DateTime::createFromFormat("Y-m-d", @$this->api->body->DueDate);
        $newAssignment->due_date = !empty($dueDate) ? $dueDate->format("Y-m-d") : 0;

        $newAssignment->days_to_complete = @$this->api->body->DaysToComplete;

        // Default email notifications to be turned off
        $params = new stdClass();
        $params->send_notifications = "0";

        $newAssignment->params = json_encode($params);

        $response = new stdClass();

        // Required parameters: CourseIds, UserGroups or UserIds, Title
        if(!empty($newAssignment->course_ids) && (!empty($newAssignment->usergroups) || !empty($newAssignment->user_ids)) && $newAssignment->title) {

            try {
                $result = $db->insertObject('#__splms_courses_groups', $newAssignment, 'id');

                if($result) {

                    $response->Status = 'success';
                    $response->id = $newAssignment->id;
                } else {

                    $response->Status = 'fail';
                    $response->Message = 'Failed to insert new assignment record into database.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Failed to insert new assignment record into database.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'One or more required parameters are missing.';
        }

        return $response;
    }

    public function editAssignment() {

        $db = JFactory::getDbo();

        $assignmentId = (int)trim($this->assignmentIds, '\'');

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('#__splms_courses_groups')
              ->where("{$db->quoteName('id')} = $assignmentId");

        $db->setQuery($query);

        $assignment = $db->loadObject();

        $response = new stdClass();

        if(!is_null($assignment)) {

            $userIdsToAdd = @$this->api->body->AddUsers;
            $userGroupsToAdd = @$this->api->body->AddUserGroups;

            $userIdsToRemove = @$this->api->body->RemoveUsers;
            $userGroupsToRemove = @$this->api->body->RemoveUserGroups;

            $newUserIds = @$this->api->body->Users;
            $newUserGroups = @$this->api->body->UserGroups;

            // If either Users or UserGroups exist, those will be clobbering the entire set of users or usergroups
            if(!empty($newUserIds)) {

                $userIdsToUpdate = array_filter($newUserIds, function($userId) {

                    return is_int($userId);
                });

                $userIdsToUpdate = implode(',', $userIdsToUpdate);
            } else {

                $userIdsToUpdate = array_map(function($userId) {

                    return (int)$userId;
                }, explode(',', $assignment->user_ids));

                $updatedUserIds = $this->api->updateArray($userIdsToUpdate, $userIdsToAdd, $userIdsToRemove);

                if(!empty($updatedUserIds)) {

                    $userIdsToUpdate = $this->api->parseArrayParameterAsString($updatedUserIds);
                }
            }

            if(!empty($newUserGroups)) {

                $userGroupsToUpdate = array_filter($newUserGroups, function($groupId) {

                    return is_int($groupId);
                });

                $userGroupsToUpdate = implode(',', $userGroupsToUpdate);
            } else {

                $userGroupsToUpdate = array_map(function($groupId) {

                    return (int)$groupId;
                }, explode(',', $assignment->usergroups));

                $updatedUserGroups = $this->api->updateArray($userGroupsToUpdate, $userGroupsToAdd, $userGroupsToRemove);

                if(!empty($updatedUserGroups)) {

                    $userGroupsToUpdate = $this->api->parseArrayParameterAsString($updatedUserGroups);
                }
            }

            $assignment->user_ids = $userIdsToUpdate;
            $assignment->usergroups = $userGroupsToUpdate;

            try {

                $result = $db->updateObject('#__splms_courses_groups', $assignment, 'id');

                if($result) {

                    $response->Status = 'success';
                } else {

                    $response->Status = 'fail';
                    $response->Message = 'Failed to update the given course assignment.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Failed to update the given course assignment.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'Given assignment ID does not exist';
        }

        return $response;
    }
}
