<?php

use Joomla\Registry\Registry;

class Users {
    protected $api = null;
    protected $userIds = null;
    protected $items = null;
    protected $forbiddenGroups = [8];

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['users'])) {
            $this->userIds = $this->api->quoteFilterList($this->items['users'],'int');
        } elseif(!empty($this->api->filter['user_ids'])) {
            $this->userIds = $this->api->quoteFilterList($this->api->filter['user_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->userIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif(!empty($this->api->body->userIds)) {

            // Parse the filter data from the body, if any
            $this->userIds = $this->api->quoteFilterList($this->api->body->userIds, 'int');
        } elseif(!empty($this->api->body->id)) {

            $this->userIds = trim($this->api->quoteFilterList($this->api->body->id, 'int'), '\'');
        }

        $urlArray = explode('api/v1',JUri::base());
        $this->baseUrl = $urlArray[0];
    }

    public function checkUserGroupExist($id = null) {
        if(!$id) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions[] = $db->quoteName('id')."=".$db->quote($id);
        $query->select('id');
        $query->from("joom_usergroups");
        $query->where($conditions);
        $query->setLimit(1);
        $result = $db->loadObject();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    private function getGroupForId($userGroupId) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('#__usergroups')
              ->where("id = $userGroupId");

        $db->setQuery($query);

        $result = $db->loadObject();

        return $result;
    }

    private function getGroupTitleForGroupId($userGroupId) {

        $title = null;

        $columns = [
            'id',
            'title'
        ];

        if(!empty($userGroupId)) {

            $db = JFactory::getDbo();
            $parentGroupQuery = $db->getQuery(true);
            $parentGroupQuery->select($columns);

            $parentGroupQuery->where("id = $userGroupId");
            $parentGroupQuery->from('#__usergroups');

            $db->setQuery($parentGroupQuery);
            $result = $db->loadObject();

            $title = $result->title;
        }

        return $title;
    }

    public function createUser() {
        $email              = $this->api->body->Email;
		$lastname           = $this->api->body->Lastname;
		$firstname          = $this->api->body->Firstname;
		$username           = $this->api->body->Username;
        $password           = $this->api->body->Password;
        $groups             = $this->api->body->UserGroups;
        $customFields       = $this->api->body->CustomFields;
        $requireReset       = $this->api->body->RequireReset;
        $sendWelcomeEmail   = $this->api->body->SendWelcomeEmail;

        $response  = new stdClass();

        $lang = JFactory::getLanguage();
        $lang->load('lib_joomla', JPATH_SITE, 'en-GB', true);

        $hasName = !empty($firstname) || !empty($lastname);

		if($email && $hasName && $password) {

            $firstName = $firstname ?? '';
            $lastName = $lastname ?? '';

            $data['name'] = trim($firstName.' '.$lastName);

            // Use the email address as the username for the new user if no username is provided.
			$data['username'] = !empty($username) ? trim($username) : trim($email);
			$data['email']    = trim($email);
			$data['password'] = trim($password);
			$data['activation'] = '0';
            $data['block'] = '0';

            if (isset($requireReset)) {
                if (gettype($requireReset) != 'integer') {
                    $requireReset = intval($requireReset);
                }
                if (in_array($requireReset, array(0, 1))) {
                    $data['requireReset'] = $requireReset;
                }
            }

            if($groups && is_array($groups)) {
                foreach($groups as $group) {
                    if(!in_array($group, $this->forbiddenGroups) && $this->checkUserGroupExist($group)) {
                        $data['groups'][] = (int)$group;
                        error_log("Adding user to group:");
                        error_log((int)$group);
                    }
                }
            } else {
                $data['groups'][] = 2;
            }

            if(!$data['groups']) {
                $data['groups'][] = 2;
            }

            $user = new JUser;
			//Write to database
			if(!$user->bind($data)) {
				$response->Status = "fail";
                $response->Error = JText::_($user->getError());
                return $response;
            }

            if (!$user->saveFromAPI()) {
				$response->Status = "fail";
                $response->Error = JText::_($user->getError());
                return $response;
            }

            if($user->id) {
				$userData = new stdClass();
				$userData->userId = $user->id;
				$userData->firstName = $firstname;
                $userData->lastName = $lastname;

                AxsUser::setProfileNameFields($userData);

                if($customFields) {
                    $this->updateCustomFields($user->id,$customFields);
                }

                $response->Status = 'success';
                $response->Id = $user->id;

                // Send new user a welcome email
                if($sendWelcomeEmail || !isset($sendWelcomeEmail)) {
                    $this->sendNewUserEmail($user->getProperties());
                }
			}
        } else {
            $response->Status = 'fail';
            $response->Error  = 'One or more required fields missing';
        }

        return $response;
    }

    public function updateCustomFields($user_id,$customFields) {
        $db = JFactory::getDBO();
        $table = 'joom_community_fields_values';
        foreach($customFields as $key => $value) {
            $field = AxsUser::getCustomFieldByName($key);
            if($field->id) {
                $fieldRow = AxsUser::getUserProfileFieldRowById($user_id,$field->id);
                
                $fieldName = $field->name;
                if (preg_match("/date/i", $fieldName)) {
                    $value = date("Y-m-d", strtotime($value));
                }
                if($fieldRow && $value) {
                    $fieldRow->value = $value;
                    $db->updateObject($table,$fieldRow,'id');
                } else {
                    $fieldValues = new stdClass();
                    $fieldValues->user_id  = $user_id;
                    $fieldValues->field_id = $field->id;
                    $fieldValues->value    = $value;
                    $fieldValues->access   = 40;
                    $db->insertObject($table,$fieldValues);
                }
            }
        }
    }

    public function getUsers() {
        $db = JFactory::getDBO();
        $normalizeCustomFields = isset($this->api->body->normalizeCustomFields) ? $this->api->body->normalizeCustomFields : null;
        $email = isset($this->api->filter['email']) ? $this->api->filter['email'] : null;
        $conditions = [];

        if($this->userIds) {

            $conditions[] = $db->qn('id').' IN ('.$this->userIds.')';
        }

        if(isset($email)) {

            $conditions[] = $db->qn('email') . ' = ' . $db->quote($email);
        }

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__users');

        if(count($conditions) > 0) {

            $query->where($conditions);
        }

        $query->order('id DESC');

        $db->setQuery($query);
        $results = $db->loadObjectList();

        $dataArray = array();

        foreach($results as $result) {

            $customFields = AxsUser::getUserCustomFields($result->id);
            $customFieldsObject = new stdClass();

            foreach($customFields as $field) {
                if($field->value) {
                    if($normalizeCustomFields) {
                        $customFieldsObject->{$field->name} = $field->value;
                    } else {
                        $title = preg_replace('/[\W]/', '',ucfirst($field->name));
                        $customFieldsObject->$title = $field->value;
                    }
                }
            }

            $data = new stdClass();
            $data->id    = $result->id;
            $data->Name  = $result->name;
            $data->Email = $result->email;
            $data->RequireReset = $result->requireReset;
            $data->RegisterDate = date('Y-m-d',strtotime($result->registerDate));
            $data->UserGroups = AxsUser::getUserGroupNames($result->id);
            $data->CustomFields = $customFieldsObject;

            $dataArray[] = $data;
        }

        if(isset($dataArray)) {

            return $dataArray;
        } else {

            $response = new stdClass();

            $response->Status = 'fail';
            $response->Message = 'Error API_NOT_FOUND: No user data was found given the input provided.';

            return $response;
        }
    }

    public function getUserEvents() {
        $eventIds = null;
        if($this->items['events']) {
            $eventIds = $this->api->quoteFilterList($this->items['events'],'int');
        }  elseif($this->api->filter['event_ids']) {
            $eventIds = $this->api->quoteFilterList($this->api->filter['event_ids'],'int');
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('registration.*,registration.id as registration_id,event.*,event.id as event_id');
        $query->from('joom_eb_registrants as registration');
        $query->join('LEFT','joom_eb_events as event ON registration.event_id = event.id');
        if($this->userIds) {
            $conditions[] = $db->qn('registration.user_id').' IN ('.$this->userIds.')';
        }
        if($eventIds) {
            $conditions[] = $db->qn('event.id').' IN ('.$eventIds.')';
        }
        if($conditions) {
            $query->where($conditions);
        }

        $db->setQuery($query);
        $events = $db->loadObjectList();

        foreach($events as $row) {
            $user = AxsUser::getUser($row->user_id);
            $data = new stdClass();
            $data->id        = $row->registration_id;
            $data->UserId    = $row->user_id;
            $data->UserFullName = $user->name;
            $data->UserEmail = $user->email;
            $data->EventId    = $row->event_id;
            $data->EventName = $row->title;
            if($row->event_date) {
                $data->EventDate = date('Y-m-d',strtotime($row->event_date));
            }
            if($row->event_end_date) {
                $data->EventEndDate = date('Y-m-d',strtotime($row->event_end_date));
            }
            $data->Amount = $row->amount;
            $data->DiscountAmount = $row->discount_amount;
            $data->TotalAmount = $row->total_amount;
            $data->RegisterDate = date('Y-m-d',strtotime($row->register_date));
            $data->CheckedIn = (bool)$row->checked_in;
            $dataArray[] = $data;
        }

        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function getUserCourses() {
        $courseIds = null;
        $userCourses = array();

        if($this->items['courses']) {
            $courseIds = $this->api->quoteFilterList($this->items['courses'],'int');
        }  elseif($this->api->filter['course_ids']) {
            $courseIds = $this->api->quoteFilterList($this->api->filter['course_ids'],'int');
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('progress.*,progress.id as progress_id,MAX(progress.progress) as max_progress,MAX(progress.date_started) as max_date_started,MAX(progress.date_completed) as max_date_completed,MAX(progress.date_last_activity) as max_date_last_activity,course.*,course.splms_course_id as course_id, course.slug as slug, bookmark.id as bookmarked');

        $query->from('joom_splms_course_progress as progress');
        $query->join('LEFT','joom_splms_courses as course ON progress.course_id = course.splms_course_id');
        $query->leftJoin("#__splms_course_wishlist bookmark ON (bookmark.course_id = course.splms_course_id AND bookmark.user_id = progress.user_id)");

        if($this->userIds) {
            $conditions[] = $db->qn('progress.user_id').' IN ('.$this->userIds.')';
        }

        if($courseIds) {
            $conditions[] = $db->qn('progress.course_id').' IN ('.$courseIds.')';
        }

        $conditions[] = 'progress.archive_date IS NULL';

        if($conditions) {
            $query->where($conditions);
        }

        $query->group('progress.user_id, progress.course_id');
        $db->setQuery($query);
        $courseList = $db->loadObjectList();

        foreach($courseList as $row) {
            $row->percentComplete = round($row->max_progress);

            if($row->percentComplete == 100) {
                $row->status = 'Completed';
            } else {
                $row->status = 'In Progress';
            }

            $row->score  = AxsLMS::getCourseTotalScore((int)$row->course_id,$row->user_id);

            $user = AxsUser::getUser($row->user_id);
            $data = new stdClass();

            $data->id      = $row->progress_id;
            $data->UserId  = $row->user_id;
            $data->UserFullName = $user->name;
            $data->UserEmail = $user->email;
            $data->CourseId  = $row->course_id;
            $data->CourseName  = $row->title;
            $data->PercentComplete = $row->percentComplete;
            $data->ProgressStatus = $row->status;
            $data->CourseUrl = $this->baseUrl . 'courses/course/' . $row->slug;
            $data->Bookmarked = !empty($row->bookmarked);

            if($row->max_date_started) {
                $data->StartDate = date('Y-m-d',strtotime($row->max_date_started));
            }
            if($row->max_date_completed) {
                $data->CompletedDate = date('Y-m-d',strtotime($row->max_date_completed));
            }
            if($row->max_date_last_activity) {
                $data->LastActivityDate = date('Y-m-d',strtotime($row->max_date_last_activity));
            }

            $userCourses[] = $data;
        }

        if(count($userCourses) > 0) {
            return $userCourses;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function getUserCourseProgress() {

        $db = JFactory::getDbo();

        // Use the Email body parameter to look up course progress by user email
        $email = @$this->api->body->Email;

        if(!empty($this->userIds) || !empty($email)) {

            $query = $db->getQuery(true);

            // Prefer the passed in email parameter over any given user ID in the API route itself
            $userId = !empty($email) ? AxsUser::getUserForEmail($email) : $this->userIds;

            $query->select('course.splms_course_id as course_id, course.title AS course_name, progress.progress AS course_progress, progress.score AS course_score')
                  ->from('#__splms_course_progress as progress')
                  ->innerJoin('#__splms_courses AS course ON course.splms_course_id = progress.course_id')
                  ->where("progress.user_id = $userId AND progress.archive_date IS NULL");

            $db->setQuery($query);

            $courseProgressData = $db->loadObjectList();
            $courseData = array();
            $courseIds = array();

            if(!empty($courseProgressData)) {

                // Get the lesson progress and lesson information for each course in the course list
                foreach($courseProgressData as $courseProgressDatum) {

                    $courseProgress = new stdClass();

                    $courseProgress->CourseName = $courseProgressDatum->course_name;
                    $courseProgress->CourseProgress = $courseProgressDatum->course_progress . '%';
                    $courseProgress->CourseScore = $courseProgressDatum->course_score;

                    $courseProgress->Lessons = array();

                    $courseData['CourseID_' . $courseProgressDatum->course_id] = $courseProgress;

                    // For each course ID, get the corresponding lesson and lesson progress information
                    $courseIds[] = $courseProgressDatum->course_id;
                }
            }

            $courseIdList = implode(',', $courseIds);

            $query = $db->getQuery(true);

            /* Add ORDER BY clause to order by lesson_id, course_id and lesson_progress_status to get "completed"
               status instead of "started" because completed lessons are marked with both records. We just want to make sure "completed" is returned if a lesson has been completed. This will match the lesson status view in the Course details view under the Students tab.
            */
            $query->select('lesson.splms_lesson_id, lesson_progress.course_id, lesson.title AS lesson_title, lesson.description AS lesson_description, lesson_progress.status AS lesson_progress_status, lesson_progress.date AS lesson_progress_date')
                 ->from('#__splms_lessons AS lesson')
                 ->innerJoin('#__splms_lesson_status AS lesson_progress ON lesson.splms_lesson_id = lesson_progress.lesson_id')
                 ->where("lesson_progress.archive_date IS NULL AND lesson_progress.user_id = $userId AND lesson_progress.course_id IN ($courseIdList)");

            $db->setQuery($query);

            $lessonProgressData = $db->loadObjectList();

            foreach($lessonProgressData as $lessonProgressDatum) {

                $lessonProgress = new stdClass();

                $courseId = $lessonProgressDatum->course_id;
                $lessonId = $lessonProgressDatum->splms_lesson_id;

                $lessonProgress->LessonName = $lessonProgressDatum->lesson_title;
                $lessonProgress->LessonDescription = $lessonProgressDatum->lesson_description;
                $lessonProgress->Status = $lessonProgressDatum->lesson_progress_status;
                $lessonProgress->Date = $lessonProgressDatum->lesson_progress_date;

                $previousLessonProgress = $courseData['CourseID_' . $courseId]->Lessons['LessonID_' . $lessonId];

                if(!empty($previousLessonProgress) && $previousLessonProgress->Status === 'completed') {
                    // There is a prior progress entry for the given course ID and lesson ID, check if it's completed
                    // If the lesson is completed, skip assigning any new lesson progress
                    continue;
                } else {
                    $courseData['CourseID_' . $courseId]->Lessons['LessonID_' . $lessonId] = $lessonProgress;
                }

                $courseData['CourseID_' . $courseId]->Lessons['LessonID_' . $lessonId]->Activities = array();

                $lessonIds[] = $lessonProgressDatum->splms_lesson_id;
            }

            $activityData = array();

            // Get all activity data for all given course IDs, lesson IDs and the user ID passed in.
            $query = $db->getQuery(true);

            $query->select('activity.course_id, activity.lesson_id, activity.activity_id, activity.activity_type, activity.activity_request, activity.student_response, activity.grade, activity.completed')
                  ->from('#__splms_student_activities AS activity')
                  ->where("activity.archive_date IS NULL activity.user_id = $userId");

            $db->setQuery($query);

            $activityData = $db->loadObjectList();

            foreach($activityData as $activityDatum) {

                $activityProgress = new stdClass();

                $courseId = $activityDatum->course_id;
                $lessonId = $activityDatum->lesson_id;

                $activityProgress->ActivityId = $activityDatum->activity_id;
                $activityProgress->ActivityType = $activityDatum->activity_type;
                $activityProgress->ActivityName = $activityDatum->activity_request;

                if((int)$activityDatum->completed == 1) {

                    $activityProgress->Status = "Complete";

                    $activityProgress->Answer = $activityDatum->student_response;
                    $activityProgress->Grade = $activityDatum->grade;
                } else {

                    $activityProgress->Status = "Incomplete";
                }

                $courseData['CourseID_' . $courseId]->Lessons['LessonID_' . $lessonId]->Activities[] = $activityProgress;
            }

            $results = $courseData;
        } else {
            $results = new stdClass();

            $results->Status = 'fail';
            $results->Message = 'One or more required parameters are missing.';
        }

        return $results;
    }

    public function createUserCourseEnrollment() {

        $courseIds = null;

        if($this->items['courses']) {
            $courseIds = $this->api->quoteFilterList($this->items['courses'],'int');
        }

        $response = new stdClass();

        try {

            $course_id = (int)trim($courseIds, '\'');
            $user_id = (int)trim($this->userIds, '\'');

            $result = AxsLMS::courseProgress_addNewCourse($user_id, $course_id);

            if($result) {

                $response->Status = 'success';
            } else {

                $response->Status = 'fail';
                $response->Message = 'Could not enroll the selected user in the given course.';
            }
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = 'Could not enroll the selected user in the given course.';
        }

        return $response;
    }

    public function getUserAwards($type = null) {
        $awardIds = null;
        if($this->items[$type.'s']) {
            $awardIds = $this->api->quoteFilterList($this->items[$type.'s'],'int');
        } elseif($this->api->filter['certificate_ids']) {
            $awardIds = $this->api->quoteFilterList($this->api->filter['certificate_ids'],'int');
        }
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('award_id,award.*,badge.*');
        $query->from('axs_awards_earned as award');
        $query->join('INNER','axs_awards as badge ON award.badge_id = badge.id');
        if($this->userIds) {
            $conditions[] = $db->qn('award.user_id').' IN ('.$this->userIds.')';
        }
        if($awardIds) {
            $conditions[] = $db->qn('award.badge_id').' IN ('.$awardIds.')';
        }
        if($type) {
            $conditions[] = $db->qn('badge.type')." = ".$db->quote($type);
        }
        if($conditions) {
            $query->where($conditions);
        }
        $query->order('award_id DESC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        if(!$type) {
            $type = 'Award';
        }
        $idName = ucfirst($type).'Id';
        $dataArray = array();
        foreach($results as $result) {
            $user = AxsUser::getUser($result->user_id);
            $data = new stdClass();
            $data->id            = $result->award_id;
            $data->UserId        = $result->user_id;
            $data->UserFullName  = $user->name;
            $data->UserEmail     = $user->email;
            $data->{$idName}     = $result->badge_id;
            $data->Title         = $result->title;
            if(!$type) {
                $data->AwardType = $result->type;
            }
            if($result->date_earned > 0) {
                $data->DateEarned    = date('Y-m-d',strtotime($result->date_earned));
            }
            if($result->date_expires > 0) {
                $data->DateExpires   = date('Y-m-d',strtotime($result->date_expires));
            }
            if($result->description) {
                $data->Description   = $result->description;
            }
            if($type =='certificate'){
                $awardParams = json_decode($result->params);
                $certificate_id = $awardParams->certificate;
                $certificate = AxsLMS::getUserCertificate($result->award_id,$result->user_id,$certificate_id);
                $key = AxsKeys::getKey('lms');
                $certificateParams = json_decode($certificate->params);
                $cert = AxsLearnerDashboard::getCertificateById($certificate_id);
                $baseURL = AxsLMS::getUrl();
                $certObj = new stdClass();
                $certObj->certificate_id = $certificate_id;
                $certObj->award_id = $result->award_id;
                $certObj->user_id = $result->user_id;
                $encryptedCertificate = base64_encode(AxsEncryption::encrypt($certObj, $key));

                $certURL= "$baseURL/certificates?tmpl=component&cert=$encryptedCertificate";
                $NoDownloadCertificateURL= "$baseURL/certificates?tmpl=component&HideDownloadButton=1&cert=$encryptedCertificate";
                $data->CertificateURLs->URL  = $certURL;
                $data->CertificateURLs->NoDownloadButton  = $NoDownloadCertificateURL;
                $data->CertificateURLs->Thumbnail = $baseURL.'/'.$certificateParams->certificate_thumbnail;
            }
            $dataArray[] = $data;
        }
        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function getUserGroups() {
        $groupIds = null;
        $conditions = array();

        if(!empty($this->items['groups'])) {
            $groupIds = $this->api->quoteFilterList($this->items['groups'],'int');
        }  elseif(!empty($this->api->filter['group_ids'])) {
            $groupIds = $this->api->quoteFilterList($this->api->filter['group_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $groupIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        } elseif(!empty($this->api->body->group_ids)) {
            $groupIds = $this->api->quoteFilterList($this->api->body->group_ids,'int');
        } elseif(!empty($this->api->body->id)) {
            $groupIds = $this->api->quoteFilterList($this->api->body->id,'int');
        }

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $columns = [
            'id',
            'title',
            'parent_id'
        ];

        $forbiddenGroups = implode(',', $this->forbiddenGroups);

        // Get usergroup by id
        $query->select($columns);
        $query->from('#__usergroups');
        $conditions[] = "id NOT IN ($forbiddenGroups)";

        if($groupIds) {

            $conditions[] = "id IN ($groupIds)";
        }

        $query->where($conditions);

        $query->order('id DESC');

        $db->setQuery($query);
        $groups = $db->loadObjectList();

        $dataArray = array();

        foreach($groups as $group) {

            $data = new stdClass();

            $data->id = $group->id;
            $data->Title = $group->title;
            $data->ParentGroupId = $group->parent_id;
            $data->ParentGroupTitle = $this->getGroupTitleForGroupId($group->parent_id);

            $dataArray[] = $data;
        }

        if($dataArray) {
            return $dataArray;
        } else {
            throw new Exception('No Data Found');
        }
    }

    public function editUserGroup() {
        $groupId    = $this->items['groups'] ?? null;

        // Grab the id from the body if it is present
        if(!empty($this->api->body->id)) {

            $groupId = $this->api->body->id;
        }

        $hasTitle = !empty($this->api->body->Title);
        $hasParentId = !empty($this->api->body->ParentId);

        $title = $hasTitle ? $this->api->body->Title : null;
        $parentGroupId = $hasParentId ? $this->api->body->ParentId : null;

        $response  = new stdClass();

        if(!$title && !$parentGroupId) {

            $response->Status = 'fail';
            $response->Error = 'Please provide one or more parameters to update.';

            return $response;
        }

        if(!empty($groupId)) {
            if(in_array($groupId, $this->forbiddenGroups)) {
                $response->Status = 'fail';
                $response->Error = 'Group not available to edit';

                return $response;
            }
            if(!empty($title)) {

                try {
                    $db = JFactory::getDbo();
                    $updateObject = new stdClass();

                    $updateObject->id = $groupId;
                    $updateObject->title = $title;

                    if(!empty($parentGroupId) && ($groupId != $parentGroupId)) {
                        if(in_array($parentGroupId, $this->forbiddenGroups)) {
                            $parentGroupId = 1;
                        }
                        $query = $db->getQuery(true);

                        $query->select('*')
                              ->from('#__usergroups')
                              ->where("id = $parentGroupId");

                        $db->setQuery($query);

                        $parentGroup = $db->loadObject();

                        if(!empty($parentGroup)) {
                            // Group exists, so proceed to edit the user group
                            $updateObject->parent_id = $parentGroupId;
                            $groupObject = new JTableUsergroup($db);

                            // We want to rebuild the entire group tree just in case
                            $groupObject->rebuild(0, 1);
                        } else {
                            $response->Status = 'fail';
                            $response->Error = 'Given parent group ID does not exist.';

                            return $response;
                        }
                    } else if($groupId == $parentGroupId) {
                        $response->Status = 'fail';
                        $response->Error = 'A group cannot be its own parent.';

                        return $response;
                    }

                    $result = $db->updateObject('#__usergroups', $updateObject, 'id');

                    /*
                     * If both the current group and the parent group's right boundary (if parentId is given) has been updated
                     * successfully, return success.
                     */
                    if($result) {
                        $response->Status = 'success';
                        $response->Id = $groupId;
                    }
                } catch(Exception $ex) {
                    $response->Status = 'fail';
                    $response->Error = 'Could not update user group with the given information.';
                }
            } else {
                $response->Status = 'fail';
                $response->Error = 'One or more required fields are missing.';
            }
        } else {
            $response->Status = 'fail';
            $response->Error = 'Please provide a valid user group ID to edit.';
        }

        return $response;
    }

    public function deleteUser() {
        if($this->userIds) {
            $userId = $this->userIds;
        } elseif ($this->api->body->id) {
            $userId = $this->api->body->id;
        } elseif ($this->api->body->user_ids){
            $userId = $this->api->quoteFilterList($this->api->body->user_ids);
        } else {
            $userId = null;
        }

        if(!empty($userId) && $userId != 601) {
            $db = JFactory::getDbo();

            $conditions = [
                $db->qn('id') . " IN (" . $userId . ")"
            ];

            $query = $db->getQuery(true);

            $query->delete('#__users');
            $query->where($conditions);

            $db->setQuery($query);
            $result = $db->execute();
        }

        $response = new stdClass();

        if($result) {
            $response->Status = 'success';
        } else {
            $response->Status = 'fail';
            $response->Error = "Please provide a ID for the user you wish to delete.";
        }

        return $response;
    }

    public function editUser() {

        $userId     = trim($this->userIds, '\'');
        $bodyParams = $this->api->body;
        $data       = [];
        $userGroups = null;

        // Process the basic user information
		$firstname    = !empty($bodyParams->Firstname) ? $bodyParams->Firstname : null;
		$lastname     = !empty($bodyParams->Lastname) ? $bodyParams->Lastname : null;
		$username     = !empty($bodyParams->Username) ? $bodyParams->Username : null;
        $email        = !empty($bodyParams->Email) ? $bodyParams->Email : null;
        $password     = !empty($bodyParams->Password) ? $bodyParams->Password : null;
        $customFields = !empty($bodyParams->CustomFields) ? $bodyParams->CustomFields : null;

        // Process the user group parameters
        $newGroups     = isset($bodyParams->UserGroups) ? $bodyParams->UserGroups : null;
        $addGroups     = isset($bodyParams->AddUserGroups) ? $bodyParams->AddUserGroups : null;
        $removeGroups  = isset($bodyParams->RemoveUserGroups) ? $bodyParams->RemoveUserGroups : null;

        // Process the flags / toggles for a user
        $isUserEnabled = isset($bodyParams->Enabled) ? $bodyParams->Enabled : null;
        $requireReset  = isset($bodyParams->RequireReset) ? $bodyParams->RequireReset : null;

        $updateOnly = true;
        $response   = new stdClass();

        $lang = JFactory::getLanguage();
        $lang->load('lib_joomla', JPATH_SITE, 'en-GB', true);

		if($userId) {

            $user = JFactory::getUser($userId);

            if(empty($user)) {

                $response->Status = 'fail';
                $response->Message = 'Error API_NOTFOUND: User corresponding to user ID ' . $userId . ' could not be found.';
                $response->ErrorCode = 'API_NOTFOUND';

                return $response;
            }

            if(!is_null($firstname) && !is_null($lastname)) {
                $data['name'] = trim($firstname.' '.$lastname);
            } else {

                $data['name'] = $user->name;
            }

            if(!is_null($username)) {
                $data['username'] = trim($username);
            } else {

                $data['username'] = $user->username;
            }

            if(!is_null($email)) {
                $data['email'] = trim($email);
            } else {

                $data['email'] = $user->email;
            }

            if(!is_null($password)) {
                $data['password'] = trim($password);

                // We're updating the password too, so make sure they match
                $data['password2'] = trim($password);
            }

            // Do a wholesale user groups update
            if(!is_null($newGroups) && is_array($newGroups)) {

                // Remove the forbidden user groups from the provided user groups array
                $userGroups = $this->api->updateArray($newGroups, null, $this->api->forbiddenGroups);
            } else {

                // Get the current user groups for the given user ID
                $userGroups = AxsUser::getGroupIdsForUser($user->id);

                // It's possible to add AND remove a user from the usergroups they are assigned to.
                if(!is_null($addGroups) && is_array($addGroups)) {

                    // Don't want to add forbidden groups
                    $addGroups = $this->api->updateArray($addGroups, null, $this->api->forbiddenGroups);
                    $userGroups = $this->api->updateArray($userGroups, $addGroups);
                }

                if(!is_null($removeGroups) && is_array($removeGroups)) {
                    $userGroups = $this->api->updateArray($userGroups, null, $removeGroups);
                }
            }

            // Assign the groups to the user
            if(!is_null($userGroups)) {

                $data['groups'] = array_map('intval', $userGroups);
            } else {

                $data['groups'] = $user->groups;
            }

            if (!is_null($requireReset)) {

                $data['requireReset'] = boolval($requireReset);
            } else {

                $data['requireReset'] = $user->requireReset;
            }

            if(!is_null($isUserEnabled)) {

                $data['block'] = !boolval($isUserEnabled);
            } else {

                $data['block'] = $user->block;
            }

            //Write to database
			if(!$user->bind($data)) {

                $response->Status = "fail";
                $response->Error = JText::_($user->getError());

                return $response;
            }

            if (!$user->saveFromAPI($updateOnly)) {

                $response->Status = "fail";
                $response->Error = JText::_($user->getError());

                return $response;
            }

            if($user->id) {

                $userData = new stdClass();
                $userData->userId = $user->id;

                if($firstname || $lastname) {

                    if($firstname) {
                        $userData->firstName = $firstname;
                    }

                    if($lastname) {
                        $userData->lastName = $lastname;
                    }

                    AxsUser::updateProfileNameFields($userData);

                }

                if($customFields) {
                    $this->updateCustomFields($user->id,$customFields);
                }

                $response->Status = 'success';
                $response->id = $user->id;
			}
        }

        return $response;
    }

    public function createUserGroup() {
        $userGroupData = $this->api->body;

        $newUserGroup = new stdClass();
        $response = new stdClass();

        // Map the inputs to the database table fields
        if(in_array($userGroupData->ParentId, $this->forbiddenGroups)) {
            $newUserGroup->parent_id = 1;
        } else {
            $newUserGroup->parent_id = $userGroupData->ParentId;
        }
        $newUserGroup->title = $userGroupData->Title;

        if(!empty($userGroupData) && !empty($userGroupData->Title)) {
            try {
                $db = JFactory::getDbo();

                // We want to put the group under its parent (if any) and directly following the rightmost group position
                if(empty($newUserGroup->parent_id)) {

                    // Put the new group as a child of the root category 'Public' if all else fails
                    $newUserGroup->parent_id = 1;
                }

                $parent = $this->getGroupForId($newUserGroup->parent_id);

                if(empty($parent)) {
                    $response->Status = 'fail';
                    $response->Message = 'Parent group doesn\'t exist.';

                    return $response;
                }

                $left = $parent->rgt;
                $right = $left + 1;

                $newUserGroup->lft = $left;
                $newUserGroup->rgt = $right;

                $newUserInserted = $db->insertObject('#__usergroups', $newUserGroup, 'id');

                $parent->rgt = $newUserGroup->rgt + 1;
                $parentRightBoundaryUpdated = $db->updateObject('#__usergroups', $parent, 'id');

                if($newUserInserted && $parentRightBoundaryUpdated) {
                    $response->Status = 'success';
                    $response->Id = $newUserGroup->id;
                } else {
                    $response->Status = 'fail';
                    $response->Message = 'Failed to create new User Group with the given data.';
                }
            } catch(Exception $ex) {
                $response->Status = 'fail';
                $response->Message = 'Failed to create a new User Group with the given data.';
            }
        } else {
            $response->Status = 'fail';
            $response->Message = 'One or more required fields are missing.';
        }

        return $response;
    }

    public function deleteUserGroup() {
        $userGroupId = null;
        $response = new stdClass();

        if(!empty($this->items['groups'])) {
            $userGroupId = (int)$this->items['groups'];
        } elseif($this->api->body->id) {
            $userGroupId = $this->api->body->id;
        } else {
            $response->Status = 'fail';
            $response->Error = "Please provide a ID for the user group you wish to delete.";

            return $response;
        }

        if(in_array($userGroupId, $this->forbiddenGroups)) {
            $response->Status = 'fail';
            $response->Error = "Group ID $userGroupId cannot be deleted via the API.";

            return $response;
        } else {
            // Proceed to delete the user group given by the supplied group ID
            $db = JFactory::getDbo();

            $query = $db->getQuery(true);

            $query->delete('#__usergroups')
                  ->where("id = $userGroupId");

            $db->setQuery($query);

            try {
                $result = $db->execute();

                if(!$result) {
                    $response->Status = 'fail';
                    $response->Message = 'Failed to delete user group.';
                } else {
                    $response->Status = 'success';
                }
            } catch(Exception $ex) {
                $response->Status = 'fail';
                $response->Message = $ex->getMessage() . ": Failed to delete user group.";
            } finally {
                return $response;
            }
        }
    }

    public function getUserChecklistProgress() {

        // This will get one user's checklist along with the progress on each item
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        // The goal is to create an array of checklist items that a user has, keyed off by checklist ID
        $checklistTaskSets = array();

        $userId = trim($this->userIds, '\'');

        // This will retrieve all checklists that a given user is associated with, via user lists, groups or access levels.
        $checklists = AxsChecklist::getUserChecklists($userId);

        foreach($checklists as $checklist) {

            $checklistParams = json_decode($checklist->params);
            $checklistItems = json_decode($checklistParams->items);

            // Parse out each checklist item into a brief object containing the most important fields
            foreach($checklistItems as $checklistItem) {
                $item = new stdClass();

                $item->ChecklistId = $checklist->id;
                $item->ActivityId = $checklistItem->id;
                $item->Name = $checklistItem->title;

                $checklistTaskSets[$checklist->id][] = $item;
            }
        }

        $query = $db->getQuery(true);
        $query->select('checklist_id, item_id')
              ->from('axs_checklist_activity')
              ->where($db->qn('user_id') . ' = ' . $userId)
              ->andWhere('archive_date IS NULL');

        $db->setQuery($query);

        $checklistActivityData = $db->loadObjectList();

        $checklistProgress = array();
        $checklistActivities = array();

        // Pool the results into an array keyed by checklistId
        foreach($checklistActivityData as $checklistActivity) {

            $checklistActivities[$checklistActivity->checklist_id][] = $checklistActivity->item_id;
        }

        // Grab the checklist progress items and filter them by the user's checklist progress items
        foreach($checklistTaskSets as $checklistId => $checklistTaskSet) {

            foreach($checklistTaskSet as $checklistTask) {

                $checklistActivityItemIds = $checklistActivities[$checklistId];

                $completedActivity = in_array($checklistTask->TaskId, $checklistActivityItemIds);
                $checklistTask->Status = !empty($completedActivity) ? "Complete" : "Incomplete";

                $checklistProgress[] = $checklistTask;
            }
        }

        return $checklistProgress;
    }

    public function getUserQuizResults() {

        $userQuizResults = array();

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $columns = [
            'title',
            'user_id',
            'quizresult.splms_course_id',
            'quizresult.splms_lesson_id',
            "`point`",
            'total_marks',
            'questions.params',
            "`date`",
            'answers'
        ];

        $query->select($columns)
              ->from('#__splms_quizresults AS quizresult')
              ->rightJoin('#__splms_quizquestions AS questions ON quizresult.splms_quizquestion_id = questions.splms_quizquestion_id');

        if(!empty($this->userIds)) {

            $conditions = [
                "{$db->qn('user_id')} IN ($this->userIds)",
                'quizresult.archive_date IS NULL'
            ];

            $query->where($conditions);
        }

        $db->setQuery($query);

        $quizResponses = $db->loadObjectList();

        foreach($quizResponses as $quizResponse) {

            $data = new stdClass();

            $user = AxsUser::getUser($quizResponse->user_id);
            $course = AxsLMS::getCourseById($quizResponse->splms_course_id);
            $lesson = AxsLMS::getLessonById($quizResponse->splms_lesson_id);
            $questionParams = json_decode($quizResponse->params);

            $data->id = (int)$quizResponse->user_id;
            $data->UserName = $user->name;
            $data->QuizTitle = $quizResponse->title;
            $data->CourseID = (int)$quizResponse->splms_course_id;
            $data->CourseTitle = $course->title;
            $data->LessonID = (int)$quizResponse->splms_lesson_id;
            $data->LessonTitle = $lesson->title;
            $data->NumberCorrect = (int)$quizResponse->point;
            $data->TotalQuestions = (int)$quizResponse->total_marks;
            $data->PassingThreshold = $questionParams->required_score . '%';
            $data->Score = (round(((int)$quizResponse->point / (int)$quizResponse->total_marks), 2) * 100) . '%';
            $data->DateSubmitted = $quizResponse->date;

            $answers = json_decode($quizResponse->answers);

            $data->Answers = array();

            foreach($answers as $answer) {

                $answerObj = new stdClass();
                $tmpAnswer = json_decode($answer);

                $answerObj->Question = $tmpAnswer->question;
                $answerObj->Answer = $tmpAnswer->answer;
                $answerObj->Correct = $tmpAnswer->correct;

                $data->Answers[] = $answerObj;
            }

            $userQuizResults[] = $data;
        }

        return $userQuizResults;
    }

    /**
	 * Return an array of userIds that belong to the groups given in the $userGroups parameter.
	 *
	 * @param Array $userGroups Set of user groups to retrieve user IDs from
	 * @return Array | null Set of user IDs that belong to the given groups or null if an error occurred.
	 */
	public static function getUserIdsInGroups($userGroups) {
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$groupList = implode(',', $userGroups);

		$query->select('user_id')
			  ->from('#__user_usergroup_map')
			  ->where("group_id IN ($groupList)");

		$db->setQuery($query);
		$results = [];

		try {
			$userIdRows = $db->loadAssocList();

			foreach($userIdRows as $row) {
				$results[] = $row['user_id'];
			}
		} catch (Exception $ex) {
			$results = null;
		} finally {
			return $results;
		}
    }

    private function sendNewUserEmail($user) {

		$lang = JFactory::getLanguage();
		$defaultLocale = $lang->getTag();

		/**
		 * Look for user language. Priority:
		 * 	1. User frontend language
		 * 	2. User backend language
		 */
		$userParams = new Registry($user['params']);
		$userLocale = $userParams->get('language', $userParams->get('admin_language', $defaultLocale));

        // Create config object
		$config = JFactory::getConfig();

		if ($userLocale !== $defaultLocale)
		{
			$lang->setLanguage($userLocale);
		}

		$lang->load('plg_user_joomla', JPATH_ADMINISTRATOR);

        $currentBrand = AxsBrands::getBrand();

        // Set the email from information to defaults if none are configured within the current brand.
        if(empty($currentBrand->email)) {
            $mailFrom = $config['mailfrom'] ?? 'noreply@tovutimail.com';
            $fromName = $config['fromname'] ?? 'Support';
        } else {
            $mailFrom = $currentBrand->email->mailfrom;
            $fromName = $currentBrand->email->fromname;
        }

        $siteName = $currentBrand->site_title ?? $config['sitename'];

        AxsUser::sendNewUserEmailToUser($user, $mailFrom, $fromName, $siteName, $this->baseUrl);

		// Set application language back to default if we changed it
		if ($userLocale !== $defaultLocale)
		{
			$lang->setLanguage($defaultLocale);
		}
    }
}
