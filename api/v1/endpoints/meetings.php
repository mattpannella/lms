<?php

class Meetings {
    protected $api = null;
    protected $meetingIds = null;
    protected $items = null;

    protected $forbiddenGroups = [8];
    protected $forbiddenAccessLevels = [3];

    public function __construct($api) {
        $this->api = $api;
        $this->items = $this->api->items;

        if(!empty($this->items['meetings'])) {
            $this->meetingIds = $this->api->quoteFilterList($this->items['meetings'],'int');
        } elseif(!empty($this->api->filter['meeting_ids'])) {
            $this->meetingIds = $this->api->quoteFilterList($this->api->filter['meeting_ids'],'int');
        } elseif(!empty($this->api->filter['id'])) {
            $this->meetingIds = $this->api->quoteFilterList($this->api->filter['id'],'int');
        }
    }

    /**
     * Optional body parameters: ModeratorPassword, AttendeePassword, MaxParticipants, Description, Enabled, Duration, RecordMeeting,
     * ScheduledDate, AccessType, UserGroups, Users, AccessLevels, Hosts, Type, ExternalUrl
     *
     * Required body parameters: Name, Creator
     */
    public function createMeeting() {

        $name = $this->api->body->Name;
        $moderatorPassword = $this->api->body->ModeratorPassword;
        $attendeePassword = $this->api->body->AttendeePassword;
        $maxParticipants = $this->api->body->MaxParticipants;
        $description = $this->api->body->Description;
        $enabled = $this->api->body->Enabled;
        $recordMeeting = $this->api->body->RecordMeeting;
        $duration = $this->api->body->Duration;
        $creatorUserId = $this->api->body->Creator;
        $scheduledDate = empty($this->api->body->ScheduledDate) ? null : $this->api->body->ScheduledDate;

        // Meeting access permissioning
        $accessType = empty($this->api->body->AccessType) ? "users" : $this->api->body->AccessType;
        $userGroupList = $this->api->body->UserGroups;
        $userList = $this->api->body->Users;
        $accessLevelList = $this->api->body->AccessLevels;

        $meetingHosts = $this->api->body->Hosts;

        // Parameters that control what type of meeting this is
        $meetingType = $this->api->body->Type;
        $meetingUrl = $this->api->body->ExternalUrl;

        $response = new stdClass();

        if(!empty($name) && !empty($creatorUserId)) {
            $db = JFactory::getDbo();

            // Generate a moderator password if left blank
            if(empty($moderatorPassword)) {
                $moderatorPassword = AxsLMS::createUniqueID();
            }

            // Generate an attendee password if left blank
            if(empty($attendeePassword)) {
                $attendeePassword = AxsLms::createUniqueID();
            }

            $meetingData = new stdClass();
            $meetingData->meetingName = $name;
            $meetingData->meetingId = AxsSSO::generateUUID();
            $meetingData->moderatorPW = $moderatorPassword;
            $meetingData->attendeePW = $attendeePassword;
            $meetingData->description = $description;

            $meetingData->enabled = empty($enabled) ? true : $enabled;

            if(!empty($recordMeeting)) {
                $meetingData->record = $recordMeeting;
            }

            $meetingData->maxParticipants = $maxParticipants;
            $meetingData->voiceBridge = rand(10000,99999);

            // If $accessType is "users", store the access_type as 'list', etc.
            switch($accessType) {
                case 'users':
                    $meetingData->userlist = TovutiAPI::parseArrayParameterAsString($userList);
                    $meetingData->access_type = 'list';
                break;

                case 'groups':
                    $meetingData->usergroup = TovutiAPI::parseArrayParameterAsString($userGroupList, $this->forbiddenGroups);
                    $meetingData->access_type = 'user';
                break;

                case 'accessLevels':
                    $meetingData->accesslevel = TovutiAPI::parseArrayParameterAsString($accessLevelList, $this->forbiddenAccessLevels);
                    $meetingData->access_type = 'access';
                break;

                case 'none':
                case 'default':
                break;
            }

            $meetingData->hosts = TovutiAPI::parseArrayParameterAsString($meetingHosts);
            $meetingData->creator = $creatorUserId;

            /* Now we want to create the default settings object because users are going to want to specify a particular meeting type
             * and Zoom parameters, if this is a Zoom meeting.
             */
            $meetingSettings = new stdClass();

            $meetingSettings->meetingType = empty($meetingType) ? 'tovuti' : $meetingType;

            if($meetingSettings->meetingType != 'tovuti') {

                switch($meetingSettings->meetingType)
                {
                    case 'zoom':
                        // Right now, we're just going to use the manual Zoom URL.
                        // @TODO: Implement creating Zoom meetings via our Zoom API within the Create Meeting endpoint of our API.

                        $meetingSettings->zoomIntegration = 'manual';
                        $meetingSettings->zoomUrl = $meetingUrl;
                    break;

                    case 'webex':
                        $meetingSettings->webexUrl = $meetingUrl;
                    break;

                    case 'gotomeeting':
                        $meetingSettings->gotomeetingUrl = $meetingUrl;
                    break;

                    case 'google':
                        $meetingSettings->googleUrl = $meetingUrl;
                    break;

                    case 'joinme':
                        $meetingSettings->joinmeUrl = $meetingUrl;
                    break;

                    default: {
                        $meetingSettings->otherUrl = $meetingUrl;
                    }
                }
            }

            // Set the optional parameters
            $meetingSettings->settings_type = 'global';

            if(!empty($this->api->body->ScheduledTime)) {

                // Ensure the given time string fits the specified format, and if not, do not update the time.
                $formattedTime = DateTime::createFromFormat('H:i:s', $this->api->body->ScheduledTime);

                if(!empty($formattedTime)) {
                    $meetingSettings->time = $formattedTime->format('H:i:s');
                }
            }

            $meetingSettings->time_zone = @$this->api->body->ScheduledTimezone;

            $meetingData->settings = json_encode($meetingSettings);

            // Timestamps:
            $meetingData->created = date(AxsLMS::ISO_TIMESTAMP_FORMAT);
            $meetingData->date = empty($scheduledDate) ? 0 : DateTime::createFromFormat(AxsLMS::ISO_TIMESTAMP_FORMAT, $scheduledDate);

            $result = null;

            try {

                $result = $db->insertObject('#__bbb_meetings', $meetingData);
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Failed to create the meeting. Please check your parameters and try again.';
            } finally {

                if(!empty($result)) {
                    $response->Status = 'success';
                    $response->Id = $db->insertid();
                } else {
                    $response->Status = 'fail';
                    $response->Message = 'Failed to create the meeting. Please check your parameters and try again.';
                }
            }
        } else {
            $response->Status = 'fail';
            $response->Message = 'One or more required parameters are missing.';
        }

        return $response;
    }

    public function getMeetingAttendance() {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $getAttendanceOnly = false;

        // If we have filter params coming in, process them
        if(count($this->api->filter) > 1) {

            $beginDate = @$this->api->filter['BeginDate'];
            $endDate = @$this->api->filter['EndDate'];
            $userId = (int)trim(@$this->api->filter['UserId'], '\'');
            $virtualMeetingId = @$this->api->filter['VirtualMeetingId'];
            $virtualMeetingName = @$this->api->filter['VirtualMeetingName'];
            $getAttendanceOnly = @$this->api->filter['Zapier'];
        } else {
            // Otherwise, process the body filters (if any);

            // Process the date span filter (all dates within given range)
            $beginDate = @$this->api->body->BeginDate;
            $endDate = @$this->api->body->EndDate;

            // Filter by user ID
            $userId = @$this->api->body->UserId;

            // Filter by virtual meeting (name or id)
            $virtualMeetingId = @$this->api->body->VirtualMeetingId;
            $virtualMeetingName = @$this->api->body->VirtualMeetingName;

            // Check to see if the special Zapier flag is set
            $getAttendanceOnly = @$this->api->body->Zapier;
        }

        $attendanceFilters = array();
        $virtualMeetingFilters = array();

        if(!empty($beginDate) && !empty($endDate)) {

            /* We want to make the date filter range match attendance dates between midnight on the first day and 23:59:59
             * on the last day, for a more natural date range filtering on just YYYY-MM-DD date strings.
             */
            $endDate .= " 23:59:59";

            $attendanceFilters[] = "{$db->qn('date')} BETWEEN CAST({$db->quote($beginDate)} AS DATETIME) AND CAST({$db->quote($endDate)} AS DATETIME)";
        } else if(!empty($beginDate) && empty($endDate)) {

            // Find all meetings after and including the given begin date
            $attendanceFilters[] = "{$db->qn('date')} >= CAST({$db->quote($beginDate)} AS DATETIME)";
        } else if(empty($beginDate) && !empty($endDate)) {

            // Find all meetings prior to and including the given end date
            $endDate .= " 23:59:59";

            $attendanceFilters[] = "{$db->qn('date')} <= CAST({$db->quote($endDate)} AS DATETIME)";
        }

        $query->select('*')
              ->from('#__bbb_meetings');

        if(!empty($virtualMeetingId)) {

            $virtualMeetingFilters[] = "{$db->qn('meetingId')} = {$db->quote($virtualMeetingId)}";
        } else if(!empty($this->meetingIds)) {

            $virtualMeetingFilters[] = "{$db->qn('id')} IN ($this->meetingIds)";
        }

        if(!empty($virtualMeetingName)) {

            $virtualMeetingFilters[] = "{$db->qn('meetingName')} = {$db->quote($virtualMeetingName)}";
        }

        if(!empty($virtualMeetingFilters)) {

            $query->where($virtualMeetingFilters);
        }

        $db->setQuery($query);

        $results = $db->loadObjectList();

        $attendanceRecords = array();

        if(!empty($results)) {

            foreach($results as $meetingData) {

                $virtualMeeting = new AxsVirtualClassroom();
                $meetingParams = new stdClass();

                $meetingParams->meetingId = $meetingData->meetingId;
                $meetingParams->userId = $userId;

                $attendanceData = $virtualMeeting->getAttendance($meetingParams, $attendanceFilters);

                if(empty($attendanceData)) {

                    // We don't have any attendance data coming back, so we need to skip this record entirely.
                    continue;
                }

                if(!$getAttendanceOnly) {

                    $data = new stdClass();

                    $data->MeetingId = $meetingData->id;
                    $data->MeetingUUID = $meetingData->meetingId;
                    $data->MeetingName = $meetingData->meetingName;

                    $data->Attendees = array();
                }
                
                foreach($attendanceData as $attendance) {

                    $attendee = new stdClass();

                    $attendee->id = $attendance->attendance_id;
                    $attendee->UserId = $attendance->user_id;
                    $attendee->Name = $attendance->attendee_name;
                    $attendee->Email = $attendance->email;
                    $attendee->Date = $attendance->date;

                    $startTime = DateTime::createFromFormat(AxsLMS::ISO_TIMESTAMP_FORMAT, $attendance->check_in);
                    $endTime = DateTime::createFromFormat(AxsLMS::ISO_TIMESTAMP_FORMAT, $attendance->check_out);

                    if(empty($startTime) || empty($endTime)) {

                        $duration = null;
                    } else {

                        $duration = $endTime->diff($startTime);
                    }

                    $attendee->Duration = !empty($duration) ? $duration->format("%H:%I:%S") : null;

                    if(!$getAttendanceOnly) {
                        $data->Attendees[] = $attendee;
                    } else {
                        $attendanceRecords[] = $attendee;
                    }                    
                }

                if(!$getAttendanceOnly) {
                    $attendanceRecords[] = $data;
                }
            }
        }

        return $attendanceRecords;
    }

    public function editMeeting() {

        $db = JFactory::getDbo();

        $meetingId = (int)trim($this->meetingIds, '\'');

        $payload = new stdClass();
        $response = new stdClass();

        $payload->id = $meetingId;

        $payload->meetingName = @$this->api->body->Name;
        $payload->moderatorPW = @$this->api->body->ModeratorPassword;
        $payload->attendeePW = @$this->api->body->AttendeePassword;
        $payload->maxParticipants = @$this->api->body->MaxParticipants;
        $payload->record = @$this->api->body->RecordMeeting;
        $payload->description = @$this->api->body->Description;
        $payload->enabled = @$this->api->body->Enabled;
        $payload->duration = @$this->api->body->Duration;

        $payload->hosts = implode(',', @$this->api->body->Hosts);

        // Parse the meeting parameters
        $query = $db->getQuery(true);

        $query->select('settings')
            ->from('#__bbb_meetings')
            ->where("id = $meetingId");

        $db->setQuery($query);

        $meetingSettings = $db->loadObject();
        $meetingSettings = json_decode($meetingSettings->settings);

        if(!empty($this->api->body->ScheduledDate)) {

            // Ensure the given date / time string fits the specified format, and if not, do not update the date in the db record.
            $formattedDate = DateTime::createFromFormat(AxsLms::ISO_TIMESTAMP_FORMAT, $this->api->body->ScheduledDate);

            if(!empty($formattedDate)) {
                $payload->date = $formattedDate->format(AxsLMS::ISO_TIMESTAMP_FORMAT);
            }
        }

        if(!empty($this->api->body->ScheduledTime)) {

            // Ensure the given time string fits the specified format, and if not, do not update the time.
            $formattedTime = DateTime::createFromFormat('H:i:s', $this->api->body->ScheduledTime);

            if(!empty($formattedTime)) {
                $meetingSettings->time = $formattedTime->format('H:i:s');
            }
        }

        $meetingSettings->time_zone = @$this->api->body->ScheduledTimezone;

        // Meeting access permissioning
        $accessType = empty($this->api->body->AccessType) ? "users" : $this->api->body->AccessType;
        $userGroupList = @$this->api->body->UserGroups;
        $userList = @$this->api->body->Users;
        $accessLevelList = @$this->api->body->AccessLevels;

        // If $accessType is "users", store the access_type as 'list', etc.
        switch($accessType) {
            case 'users':
                $payload->userlist = TovutiAPI::parseArrayParameterAsString($userList);
                $payload->access_type = 'list';
            break;

            case 'groups':
                $payload->usergroup = TovutiAPI::parseArrayParameterAsString($userGroupList, $this->forbiddenGroups);
                $payload->access_type = 'user';
            break;

            case 'accessLevels':
                $payload->accesslevel = TovutiAPI::parseArrayParameterAsString($accessLevelList, $this->forbiddenAccessLevels);
                $payload->access_type = 'access';
            break;

            case 'none':
            case 'default':
            break;
        }

        try {

            $meetingSettings->meetingType = @$this->api->body->MeetingType;
            $meetingUrl = @$this->api->body->ExternalUrl;

            if($meetingSettings->meetingType != 'tovuti') {

                switch($meetingSettings->meetingType)
                {
                    case 'zoom':
                        // Right now, we're just going to use the manual Zoom URL.
                        // @TODO: Implement creating Zoom meetings via our Zoom API within the Create Meeting endpoint of our API.

                        $meetingSettings->zoomIntegration = 'manual';
                        $meetingSettings->zoomUrl = $meetingUrl;
                    break;

                    case 'webex':
                        $meetingSettings->webexUrl = $meetingUrl;
                    break;

                    case 'gotomeeting':
                        $meetingSettings->gotomeetingUrl = $meetingUrl;
                    break;

                    case 'google':
                        $meetingSettings->googleUrl = $meetingUrl;
                    break;

                    case 'joinme':
                        $meetingSettings->joinmeUrl = $meetingUrl;
                    break;

                    default: {
                        $meetingSettings->otherUrl = $meetingUrl;
                    }
                }
            }

            $payload->settings = json_encode($meetingSettings);
        } catch(Exception $ex) {

            $response->Status = 'fail';
            $response->Message = "Could not update meeting settings for meeting with ID $meetingId.";
        }

        try {

            $result = $db->updateObject('#__bbb_meetings', $payload, 'id');
        } catch (Exception $ex) {

            $response->Status = 'fail';
            $response->Message = "Could not update meeting with ID $meetingId.";
        } finally {

            if(!empty($result)) {

                $response->Status = 'success';
                $response->Id = $meetingId;
            } else {

                $response->Status = 'fail';
                $response->Message = "Could not update the virtual meeting data.";
            }

            return $response;
        }
    }

    public function deleteMeeting() {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $response = new stdClass();

        if(!empty($this->meetingIds)) {

            $meetingId = (int)trim($this->meetingIds, '\'');

            $query->delete('#__bbb_meetings')
                  ->where("id = $meetingId");

            $db->setQuery($query);

            try {

                $result = $db->execute();

                if($result) {
                    $response->Status = 'success';
                    $response->Id = $meetingId;
                } else {
                    $response->Status = 'fail';
                    $response->Message = 'Could not delete the given meeting.';
                }
            } catch(Exception $ex) {

                $response->Status = 'fail';
                $response->Message = 'Could not delete the given meeting.';
            }
        } else {

            $response->Status = 'fail';
            $response->Message = 'Please provide the ID for the virtual meeting that you wish to delete.';
        }

        return $response;
    }
}