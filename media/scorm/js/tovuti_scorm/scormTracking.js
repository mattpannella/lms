function saveData(data) {
        console.log(data);
        var dataString = btoa(encodeURI(JSON.stringify(data)));
        // console.log(dataString);
        $.ajax({
            url: '/index.php?option=com_splms&task=scormfactory.saveData&format=raw',
            type:'POST',
            data: {
                cmiData         : dataString,
                course_id       : course_id,
                lesson_id       : lesson_id,
                scorm_course_id : scorm_course_id,
                language        : language
            }
        }).done(function(response){
            console.log('SAVED');
            var result = JSON.parse(response);
            console.dir(result);
            if(result.progress == "completed" && result.runCheckComplete) {
                console.log("Checking Completion...");
                window.parent.checkComplete(true);
            }
        })
}

function initializeAICCWatchdog(course_id, lesson_id) {
    var status = false;

    // This only works in the lesson view - don't initialize the watchdog timer if we're not in a lesson
    if(lesson_id !== 0) {

        var watchdog = window.setInterval(function() {

            // Check for lesson complete status for the current lesson
            status = window.parent.checkCompleteAICC(true, course_id, lesson_id);

            if(status) {

                window.clearInterval(watchdog);
            }
        }, 5 * 1000, course_id, lesson_id);
    }
}