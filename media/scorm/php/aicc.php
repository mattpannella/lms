<?php

// This is the AICC processor endpoint

// Set flag that this is a parent file.
const _JEXEC = 1;
// error_reporting(E_ALL | E_NOTICE);
// ini_set('display_errors', 1);

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
    require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
    define('JPATH_BASE', dirname(__DIR__) . '/../..');

    require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';

require_once JPATH_CONFIGURATION . '/configuration.php';

$db = JFactory::getDbo();

// Slurp in all the AXS libs
JLoader::discover('Axs', JPATH_LIBRARIES . '/axslibs');

// Force lowercase post vars
$requestData = $_POST ?? $_REQUEST;

// No request data either... must be a malformed request - destroy session and respond with an error
if(empty($requestData)) {

    echo "error=1\r\nerror_text=Unknown\r\n";
    exit;
}

$command = $requestData['command'];
$aiccData = array_key_exists('aicc_data', $requestData) ? $requestData['aicc_data'] : null;
$sessionId = $requestData['session_id'];

$urlEncoded = false;

// error_log("AICC Data: " . print_r($aiccData, true));

// First, we need to check the AICC payload to make sure it's not urlencoded, and if it is, decode it.
if(!empty($aiccData) && strstr($aiccData, '%5bcore%5d')) {

    $aiccData = urldecode($aiccData);
    $urlEncoded = true;
}

// Figure out how to retrieve the user information regarding the user who is interacting with the AICC course
$sessionPayload = AxsLMS::retrieveAICCSession($sessionId);

if(empty($sessionPayload)) {

    echo "error=1\r\nerror_text=Unknown\r\n";
    exit;
}

$user = AxsUser::getUser($sessionPayload->user_id);

$scormDataParams = new stdClass();

$scormDataParams->user_id = $sessionPayload->user_id;
$scormDataParams->scorm_content_id = $sessionPayload->scorm_id;
$scormDataParams->course_id = $sessionPayload->course_id;
$scormDataParams->lesson_id = $sessionPayload->lesson_id;

$studentId = $sessionPayload->student_id;

$scormData = AxsScormFactory::getLearnerScormData($scormDataParams);
$scormMetadata = !empty($scormData) ? json_decode($scormData->data) : null;

$statusModel = [
    // Incomplete status
    'incomplete' => 'incomplete',
    'i' => 'incomplete',

    // Passed status
    'passed' => 'passed',
    'p' => 'passed',

    // Complete status
    'completed' => 'completed',
    'complete' => 'completed',
    'c' => 'completed'
];

ob_start();
switch(strtolower($command)) {

    case 'getparam':
        if(!empty($scormMetadata)) {

            $successStatus = $scormMetadata->success_status;
            $score = $scormMetadata->score->raw;
            $time = $scormMetadata->session_time;
            $location = $scormMetadata->location ?? '';

            // Suspend data is optional and may not exist in all AICC content.
            $suspendData = !empty($scormMetadata->suspend_data) ? $scormMetadata->suspend_data : null;
        } else {

            $successStatus = 'incomplete';
            $score = 0;
            $time = '00:00:00';
            $location = '';
        }

        $student_id = !empty($scormMetadata->student_id) ? $scormMetadata->student_id : $studentId;

        $existingCourseInformation = <<<AICC
        [Core]
            Student_ID=$student_id
            Student_Name=$user->name
            Lesson_Status=$successStatus
            Lesson_Location=$location
            Score=$score
            Time=$time
AICC;

        // Include any suspend data that may be stored with the AICC metadata
        if(!empty($suspendData)) {

            if(is_object($suspendData)) {

                $suspendDataString = '';

                foreach($suspendData as $field => $value) {
                    $suspendDataString = $field . ' = ' . $value . '\r\n';
                }
            } else {

                // The suspend data must be a string in this case
                $suspendDataString = $suspendData;
            }

            $existingCourseInformation .= ("\r\n" . "[Core_Lesson]" . "\r\n" . $suspendDataString);
        }

        $response = [
            'error=0',
            'error_text=Success',
            'version=' . $requestData['version'],
            'aicc_data=' . $existingCourseInformation
        ];

        $formattedResponse = (!$urlEncoded ? implode("\r\n", $response) . "\r\n" : urlencode(implode("\r\n", $response)));

        echo $formattedResponse;
    break;

    case 'putparam':
        $result = null;

        if(!empty($aiccData)) {

            // Parse the core variables into an array
            $aiccCoreData = parseAICCDataString($aiccData);
            $aiccSCO = new stdClass();

            $lessonStatus = strtolower($aiccCoreData->core->lesson_status);

            $aiccSCO->session_time = $aiccCoreData->core->time;
            $aiccSCO->success_status = $statusModel[$lessonStatus] ?? '';
            $aiccSCO->location = $aiccCoreData->core->lesson_location;
            $aiccSCO->student_id = $studentId;

            // Make sure to include the suspend data if any exist
            if(!empty($aiccCoreData->core_lesson)) {

                $aiccSCO->suspend_data = $aiccCoreData->core_lesson;
            }

            $aiccSCO->score = new stdClass();
            $aiccSCO->score->raw = $aiccCoreData->core->score;

            $result = AxsScormFactory::updateAICCScormData($user->id, $sessionPayload->course_id, $sessionPayload->lesson_id, $sessionPayload->scorm_id, $aiccSCO);

            if(!empty($result)) {

                switch($result->status) {

                    case 'success':
                        echo "error=0\r\nerror_text=Successful\r\n";
                    break;

                    case 'error':
                    default:
                        echo "error=1\r\nerror_text=Unknown\r\n";
                    break;
                }
            }
        }
    break;

    case 'exitau':

        // Delete the session from the database
        AxsLMS::destroyAICCSession($sessionId);

        echo "error=0\r\nerror_text=Successful\r\n";
    break;

    default:
        echo "error=0\r\nerror_text=Successful\r\n";
    break;
}

$aiccResponse = ob_get_contents();
ob_end_flush();

function parseAICCDataString($aiccData) {
    $parsedAICCData = new stdClass();

    $sectionObjectName = '';

    $vars = explode("\r\n", trim($aiccData));

    $sectionHeaders = [
        '[core]',
        '[core_lesson]',
        '[comments]',
        '[objectives_status]',
        '[student_preferences]'
    ];

    $lastSection = '';

    foreach($vars as $entry) {

        $lcEntry = strtolower($entry);

        // If the entry is a section header, make that a new object
        if(in_array($lcEntry, $sectionHeaders)) {

            $sectionObjectName = trim($lcEntry, '[]');
            $parsedAICCData->{$sectionObjectName} = new stdClass();

            $lastSection = $sectionObjectName;

            continue;
        }

        if($lastSection == 'core') {

            $tuple = explode('=', trim($entry, "'"));

            $key = strtolower($tuple[0]);

            if(!empty($key)) {

                if(count($tuple) == 2) {

                    $value = !empty($tuple[1]) ? $tuple[1] : '';
                    $parsedAICCData->{$sectionObjectName}->{$key} = $value;
                } else {

                    $parsedAICCData->{$sectionObjectName} = $key;
                }
            }
        } else {

            $parsedAICCData->{$sectionObjectName} = $entry;
        }
    }

    return $parsedAICCData;
}