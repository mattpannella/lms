<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $fileName = $_POST['fileName'];
    $contentType = $_POST['contentType'];
    
    $approved_files = array(
					    	'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					    	'application/pdf'
					    	);
    
    if (in_array($contentType, $approved_files)) {
	    $base64 = $_POST['base64'];
	    $data = base64_decode($base64);

	    header('Content-Type:' . $contentType);
	    header('Content-Length:' . strlen($data));
	    header('Content-Disposition: attachment; filename=' . $fileName);

	    echo $data;
	}
}