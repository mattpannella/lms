$(document).ready( function() {
	var myLat = null;
	var myLng = null;
	html = `<div class="modal fade" id="notificationModal" role="dialog">
		            <div class="modal-dialog">
		            	<div class="modal-content">
		            		<div class="notification-count"></div>
		            		<div class="notification-content" data-order="1">
			               		<div class="modal-header">
			                 		<h4 class="modal-title"></h4>
			               		</div>
				               	<div class="modal-body">
				               		<p></p>
				               	</div>
				               	<div class="modal-footer">
				               	</div>
				            </div>
		            	</div>
		           	</div>
	         	</div>`;
	$("body").append(html);




	function degreesToRadians(degrees) {

        var pi = 3.14159265;

        return degrees * pi / 180;
    }

    function checkDistance(lat1, lon1, lat2, lon2, type = "km") {
        var earthRadiusKm = 6371;

        var dLat = degreesToRadians(lat2 - lat1);
        var dLon = degreesToRadians(lon2 - lon1);

        var lat1 = degreesToRadians(lat1);
        var lat2 = degreesToRadians(lat2);

        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1)  * Math.cos(lat2);
        var b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        var distance = earthRadiusKm * b;

        switch (type) {
            case "km":
                //Already in km.
                break;
            case "mi":
                distance /= 1.609344;
                break;
            case "ft":
                distance *= 3280.839895;
                break;
            case "in":
                distance *= 39370.07874;
                break;
            case "m":
                distance *= 1000;
                break;
            case "cm":
                distance *= 100000;
                break;
        }

        return distance;
    }


	function getCookie(name) {
	    var v = document.cookie.match('(^|;) ?' + name + '=([^;]*)(;|$)');
	    return v ? v[2] : null;
	}

	if(geofence) {
		if(!getCookie('latitude') || !getCookie('longitude')) {
			if ("geolocation" in navigator) {
				var geo_options = {
					enableHighAccuracy: true,
					maximumAge        : 30000,
					timeout           : 27000
				};
				var hours = 0.5;
				var date = new Date();
				date.setTime(date.getTime()+(hours*60*60*1000));
				var expires = "; expires="+date.toGMTString();
				navigator.geolocation.getCurrentPosition(function(position) {
					//alert(position.coords.latitude+' '+position.coords.longitude);
					myLat = position.coords.latitude;
					myLng = position.coords.longitude;
					document.cookie = "latitude="+myLat+expires+"; path=/";
					document.cookie = "longitude="+myLng+expires+"; path=/";
					getNotifications();
				},null,geo_options);
			}
		} else {
			myLat = getCookie('latitude');
			myLng = getCookie('longitude');
			getNotifications();
		}
	} else {
		getNotifications();
	}

	function sendNotificationAction(action,url) {
		$.ajax({
			url		: "/index.php?option=com_notifications&task=notifications.saveAction&format=raw",
			type 	: "POST",
			data 	: {
				action : action
			}
		}).done(function(response) {
			//alert(response);
			//var data = JSON.parse(response);
			if(url) {
				window.location = url;
			}
		});
	}

	function loadNotification(order) {

		//if coming from an action that was clicked (without an order to it) then remove from array
		if(!order) {
			var current = $('#notificationModal .notification-content').data('order');
			//alert(current);
			var currentIndex = current - 1;
			//alert(currentIndex);
			notifications.splice(currentIndex,1);
			if(current > notifications.length) {
				current = 1;
			}
			var order = current;
		}
		var previous = '';
		var next = '';
		var navigation = '';
		var index = order - 1;
		var params = notifications[index];
		$('#notificationModal .notification-content').data('order',order);
		if(notifications.length > 0) {
			if(notifications.length > 1) {
				$('#notificationModal .notification-count').text(order +'/'+ notifications.length);
				$('#notificationModal .notification-count').show();
				if(index > 0) {
					previous = '<span class="btn btn-default notificationNav" data-order="'+(order - 1)+'"><i class="fa fa-backward"></i> Previous</span>';
				}
				if( (index + 1) < notifications.length ) {
					next = '<span class="btn btn-default notificationNav" data-order="'+(order + 1)+'">Next <i class="fa fa-forward"></i></span>';
				}
				navigation = '<div style="float:left;">'+previous+' '+next+'</div>';
			}
			$('#notificationModal .notification-content').hide();
			$('#notificationModal .modal-title').text(params.title);
			$('#notificationModal .modal-body').html(params.message);
			$('#notificationModal .modal-footer').html(navigation + params.buttons);
			$('#notificationModal .notification-content').fadeIn(700);
		} else {
			$("#notificationModal").modal('toggle');
		}
	}

	function getNotifications() {
		var itemsToRemove = [];
		var newNotifications = [];
		for(let i = 0; i < notifications.length; i++ ) {

			if( (notifications[i].geofence == 1) && notifications[i].longitude && notifications[i].latitude) {
				var distance = checkDistance(notifications[i].latitude, notifications[i].longitude, myLat, myLng, notifications[i].radius_unit);
				if( distance > notifications[i].radius_amount || (!myLat || !myLng) ) {
					itemsToRemove.push(i)
				}
			}
		}

		for(let i = 0; i < notifications.length; i++ ) {
			if(!itemsToRemove.includes(i)) {
				newNotifications.push(notifications[i]);
			}
		}

		notifications = newNotifications;

		if(notifications.length > 0) {
			$("#notificationModal").modal({backdrop: "static"});
			loadNotification(1);
		}
	}

	$(document).on('click','.notificationButton', function(){
		loadNotification();
		var action 	= $(this).data('action');
		var url 	= $(this).data('url');
		if(action) {
			sendNotificationAction(action,url);
		}
	});

	$(document).on('click','.notificationNav', function(){
		var index 	= $(this).data('order');
		loadNotification(index);
	});
});