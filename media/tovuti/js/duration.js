var last_time = Math.round(Date.now() / 1000);
var last_action = Math.round(Date.now() / 1000);

function setLastActionTime() {
    last_action = Math.round(Date.now() / 1000);
}

//This is to track activity on the page - because of the heavy use of iframes which don't track action we are going to hold of on this check and just use the visibilityState check for now
document.addEventListener('mousemove', setLastActionTime);
document.addEventListener('click', setLastActionTime);
document.addEventListener('scroll', setLastActionTime);


function trackDuration() {
    var now = Math.round(Date.now() / 1000);
    var duration = now - last_time;
    last_time = now;
    time_since_last_action = now - last_action;
    if(document.visibilityState != "hidden") {
        $.ajax({
            url: '/index.php?option=com_axs&task=duration.trackDuration&format=raw',
            type: 'POST',
            data: {
                data: duration_data,
                duration: duration
            }
        });
    }
}

var durationInterval = setInterval(trackDuration,60000);