window.fbAsyncInit = function() {
    FB.init({
      appId      : '300197817159121',
      xfbml      : true,
      version    : 'v2.11'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  
$(".member_badge_sharelink").click(
  function(){
    p=this.getAttribute('path');
    b=this.getAttribute('brand');
    h=b.replace(/\s/g, '');
    t=this.getAttribute('badge');
    FB.ui({
      method: 'share',
      display: 'popup',
      //href: 'https://open.tovuti.io/images/SWltRlRIMGZIb2xCcWZEVlFLWXc2QT09OjI0NzM/pixel/PLACEHOLDER.jpg',
      href: p,
      quote: b+' - '+t,
      hashtag: '#'+h,
    }, function(response){});
  }
);