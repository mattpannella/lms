
function trackVideo(element,player) {	
	var videoData   = $('#player_'+element).data('video');
	if(videoData) {
		var data = videoData;
		$.ajax({
			url: '/index.php?option=com_axs&task=videos.trackVideo&format=raw',
			type: 'POST',
			data: {
				data: data,
				current_frame: player.currentTime,
				video_duration: player.duration
			}
		}).done(function(response) {
			var result = JSON.parse(response);
			console.dir(result);
			if(result.completed) {
				if(result.runCheckComplete) {
					if(!$('.activity_'+result.activity_id).hasClass('ic_completed_active')) {
						$('.activity_'+result.activity_id).text('Completed');
						$('.activity_'+result.activity_id).addClass('ic_completed_active');
					}
					if($('.required_'+result.activity_id).hasClass('ic_required_active')) {
						$('.required_'+result.activity_id).removeClass('ic_required_active');
					}
					
					checkComplete(true);
				}
			}
		});
	}
}

