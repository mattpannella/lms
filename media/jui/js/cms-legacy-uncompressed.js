/**
 * @copyright  Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// Only define the Joomla namespace if not defined.
if (typeof(Joomla) === 'undefined') {
	var Joomla = {};
}

/**
 * Sets the HTML of the container-collapse element
 */
 Joomla.setcollapse = function(url, name, height) {
    if (!document.getElementById('collapse-' + name)) {
        document.getElementById('container-collapse').innerHTML = '<div class="collapse fade" id="collapse-' + name + '"><iframe class="iframe" src="' + url + '" height="'+ height + '" width="100%"></iframe></div>';
    }
}


// list of all the fields rendered with a showon attribute
// logic for adding these is in strapper.php renderFieldset()
var allShowOnFieldsIds = [];

// this variable is a workaround for views in com_eventbooking.
// eventbooking doesn't render its view using joomla fields or FOF
// so we need to hide showon fields onload
var cmsBindOnly = true;

jQuery(document).ready(function($) {

	applyShowon(cmsBindOnly);
	setButtons();

	function applyShowon(bindOnly, ids) {
		(ids || allShowOnFieldsIds).forEach(id => {
			let self = $('#' + id);
			if (!self.attr("showon_activated")) {
				self.attr("showon_activated", true);
			} else {
				return;
			}

			var target = self;
			var jsondata = $(self).data('showon');

			// Attach events to referenced element
			$.each(jsondata, (index, item) => {
				var fieldname = item['field'];
				var fields = $('[name="' + fieldname + '"], [name="' + fieldname + '[]"]');

				if (!fields.length) {
					// The fields are not found.  It is in a repeatable, and not a static element.
					fields = getArrayFields(fieldname, target);
				}

				fields.on('change', () => {
					updateLinkedOptions(target);
				});

				if (!bindOnly) {
					// showon needs to be applied on subform-row-add events
					updateLinkedOptions(target)
				}
			});
		});
	}

	function updateLinkedOptions(target) {
		var showfield = true, itemval, jsondata = $(target).data('showon');

		// Check if target conditions are satisfied
		$.each(jsondata, function(j, item) {

			var fieldname = jsondata[j]['field'];
			$fields = $('[name="' + fieldname + '"], [name="' + fieldname + '[]"]');
			jsondata[j]['valid'] = 0;

			if (!$fields.length) {
				$fields = getArrayFields(fieldname, target);
			}

			// Test in each of the elements in the field array if condition is valid
			$fields.each(function() {
				// If checkbox or radio box the value is read from proprieties
				if (['checkbox','radio'].indexOf($(this).attr('type')) != -1)
				{
					itemval = $(this).prop('checked') ? $(this).val() : '';
				}
				else
				{
					itemval = $(this).val();
				}

				// Convert to array to allow multiple values in the field (e.g. type=list multiple) and normalize as string
				if (!(typeof itemval === 'object'))
				{
					itemval = JSON.parse('["' + itemval + '"]');
				}

				// Test if any of the values of the field exists in showon conditions
				for (var i in itemval)
				{
					if (jsondata[j]['values'].indexOf(itemval[i]) != -1)
					{
						jsondata[j]['valid'] = 1;
					}
				}
			});

			// Verify conditions
			// First condition (no operator): current condition must be valid
			if (jsondata[j]['op'] == '')
			{
				if (jsondata[j]['valid'] == 0)
				{
					showfield = false;
				}
			}
			// Other conditions (if exists)
			else
			{
				// AND operator: both the previous and current conditions must be valid
				if (jsondata[j]['op'] == 'AND' && jsondata[j]['valid'] + jsondata[j-1]['valid'] < 2)
				{
					showfield = false;
				}
				// OR operator: one of the previous and current conditions must be valid
				if (jsondata[j]['op'] == 'OR'  && jsondata[j]['valid'] + jsondata[j-1]['valid'] > 0)
				{
					showfield = true;
				}
			}
		});

		// If conditions are satisfied show the target field(s), else hide
		(showfield) ? $(target).fadeIn() : $(target).hide();
	};

	function getArrayFields(fieldname, target) {
		var parent = target.parent();
		while (!$(parent).data('group')) {
			parent = parent.parent();
		}

		/*
			parent_group is parent_name with a number on the end, so replacing the parent_name with "" will simply get the number.
			ie.

			parent_group = "my_group1"
			parent_name = "my_group"
			which = 1
		*/
		var parent_group = parent.data('group');
		var parent_name = parent.data('base-name');
		var which = parent_group.replace(parent_name, "");

		fieldname = fieldname.replace(parent_name + "X", parent_name + which);
		$fields = $('[name="' + fieldname + '"], [name="' + fieldname + '[]"]');

		return $fields;
	}

	function setButtons() {
		$(".group-add").each(function() {
			if (!this.getAttribute("showon_button")) {
				this.setAttribute("showon_button", true);
				// Onclick callback is delayed slightly so that it
				// doesn't try to update the buttons before they're created.
				$(this).click(() => setTimeout(() => { setButtons(); applyShowon();	}, 100));
			}
		});
	}

	$(document).on('refresh-showon', (event, element = null) => {
		if (element == null) {
			applyShowon(false);
		} else {
			// Grab the ids of all the elements in the new element that have
			// data-showon attributes and add them to an array that we pass
			// to applyShowon to reduce the number of elements that we need to update.
			// This is mainly used by repeatable-subforms whenever a new row is added
			var newElementIds = [];
			$(element).find('[data-showon]').each((idx, el) => {
				newElementIds.push(el.id);
				allShowOnFieldsIds.push(el.id);
			});
			applyShowon(false, newElementIds);
		}
	});

});
