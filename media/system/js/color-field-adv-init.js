// instead of searching for color fields with
// jquery.find, we append a code snippet to the page
// whenever we render a color field that will 
// add the newly rendered field element's id
// to this array. See layouts/joomla/form/fields/color/advanced.php
// too see this in action
var allColorFieldIds = [];

jQuery(function () {
	allColorFieldIds.forEach((id) => {
		initMinicolorsField(id);
	});
});

jQuery(document).on('subform-row-add', (ev, row) => {
	jQuery(row).find('.minicolors').each((idx, el) => initMinicolorsField(el.id));
});

let initMinicolorsField = function(id) {
	var field = jQuery('#' + id);
	field.minicolors({
		control: field.attr('data-control') || 'hue',
		format: field.attr('data-validate') === 'color'
			? 'hex'
			: (field.attr('data-format') === 'rgba'
				? 'rgb'
				: field.attr('data-format'))
			|| 'hex',
		keywords: field.attr('data-keywords') || '',
		opacity: field.attr('data-format') === 'rgba' ? true : false || false,
		position: field.attr('data-position') || 'default',
		theme: 'bootstrap'
	});
}
