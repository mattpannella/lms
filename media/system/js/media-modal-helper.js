
jQuery(document).on('subform-row-add', (ev, row) => {
    // Locate all the buttons and inputs associated with the media field modal modal id="updateImageModal"
    const buttonIds = jQuery(row).find(`button[shared-modal-id='#updateImageModal']`).map((idx, button) => button.id);
    const inputIds = jQuery(row).find(`input[shared-modal-id='#updateImageModal']`).map((idx, input) => input.id);

    // Cycle through each button/input pair and initialize the media input and tooltips in the row
    buttonIds.each((idx, buttonId) => {
        var curInputId = inputIds[idx];
        initializeMediaInput(curInputId, true);
    });
});

function closeMediaModal() {
    jQuery('#updateImageModal').each((idx, value) => {
        const modalInstance = bootstrap.Modal.getInstance(value);
        if (modalInstance) {
            modalInstance.hide();
        }
    });
}

function initializeMediaInput(inputId, init = false) {
    let imageTooltipPreview =  jQuery('#' + inputId + '-img-tooltip');
    let displayedInput = jQuery('#' + inputId + '-forDisplay');
    let hiddenInput = jQuery('#' + inputId);
    let selectButton = jQuery('#' + inputId + '-selectMediaFieldButton');
    let clearButton = jQuery('#' + inputId + '-clearMediaFieldButton');

    displayedInput.on('change', () => {
        // Whenever the displayed input changes, the hidden input needs to
        // update its value so the form saves the proper value. Also,
        // the image preview tooltip needs to be updated to the correct image
        displayedInputOnchangeCallback();
    });

    hiddenInput.on('change', () => {
        // Whenever the hidden input changes, the shown input needs to 
        // be updated with the user friendly filename and the tooltip
        // preview needs to be updated with the proper image
        hiddenInputOnchangeCallback();
    });

    selectButton.on('click', () => {
        const val = MEDIA_FIELD_IFRAME_HTML.replace(/&amp;fieldid=(.+)&/, '&fieldid=' + inputId + '&');
        showModalPrependIframe('updateImageModal', val);
    });
    
    clearButton.on('click', () => {
        hiddenInput.val("");
        displayedInput.val("");
        displayedInput.attr("data-bs-original-title", "");
        imageTooltipPreview.attr("data-bs-original-title", "");
    });

    const updateTooltipImage = () => {
        const value = hiddenInput.val();
        const imgSrc = 
            value.indexOf('http') == -1
                ? window.location.origin + '/' + value
                : value;

        const newTooltipImg = `<img id='${inputId}-preview' class='media-preview' src='` + imgSrc + `' />`;
        imageTooltipPreview.attr('data-bs-content', newTooltipImg);
    }    

    const displayedInputOnchangeCallback = () => {
        var newValue = displayedInput.val();
        if (newValue.indexOf('http') == -1 && newValue.length > 0) {
            // newValue is a folder path, relative
            // to the configured imagespath in /images/
            const imagespath = hiddenInput.attr('data-imagespath');
            newValue = imagespath + newValue;
        }
        hiddenInput.val(newValue);
        displayedInput.attr('data-bs-original-title', newValue);

        updateTooltipImage();
    }

    const hiddenInputOnchangeCallback = () => {
        const value = hiddenInput.val();
        let valueJustFilename = '';
        if (value.indexOf('http') == -1) {
            valueJustFilename = value.split('/').slice(2).join('/');
        }
        if (!valueJustFilename) {
            valueJustFilename = value; 
        }
        displayedInput.val(valueJustFilename);
        displayedInput.attr('data-bs-original-title', valueJustFilename);
        
        updateTooltipImage();
    }

    if (init == true) {
        // When a subform row is added, or repeatable table initialized
        // we need to initialize the tooltips and displayedInput value
        hiddenInputOnchangeCallback();
        imageTooltipPreview[0].setAttribute("data-bs-trigger", "hover");
        new bootstrap.Popover(imageTooltipPreview[0]);
        new bootstrap.Tooltip(displayedInput[0])
    }
}
