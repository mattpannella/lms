/**
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

;(function($){
	"use strict";
	$.subformRepeatable = function(container, options){
		this.$container = $(container);

		// check if alredy exist
		if(this.$container.data("subformRepeatable")){
			return self;
		}

		// Add a reverse reference to the DOM object
		this.$container.data("subformRepeatable", self);

		// merge options
		this.options = $.extend({}, $.subformRepeatable.defaults, options);

		// template for the repeating group
		this.template = '';

		// prepare a row template, and find available field names
		this.prepareTemplate();

		// check rows container
		this.$containerRows = this.options.rowsContainer ? this.$container.find(this.options.rowsContainer) : this.$container;

		// last row number, help to avoid the name duplications
        this.lastRowNum = this.$containerRows.find(this.options.repeatableElement).length;

		// To avoid scope issues,
		var self = this;

		// bind add button
		this.$container.on('click', this.options.btAdd, function (e) {
			e.preventDefault();
			var after = $(this).parents(self.options.repeatableElement);
			if(!after.length){
				after = null;
			}
			self.addRow(after);
		});

		// bind remove button
		this.$container.on('click', this.options.btRemove, function (e) {
			e.preventDefault();
			var $row = $(this).parents(self.options.repeatableElement);
			self.removeRow($row);
		});

		// bind move button
		if(this.options.btMove){
			this.$containerRows.sortable({
				items: this.options.repeatableElement,
				handle: this.options.btMove,
				tolerance: 'pointer'
			});
		}

		// tell all that we a ready
		this.$container.trigger('subform-ready');

		this.$container.trigger('refresh-showon', this.$container);
	};

	// prepare a template that we will use repeating
	$.subformRepeatable.prototype.prepareTemplate = function(){
		// create from template
		if(this.options.rowTemplateSelector){
			var tmplElement = this.$container.find(this.options.rowTemplateSelector)[0] || {};
			this.template = $.trim(tmplElement.text || tmplElement.textContent); //(text || textContent) is IE8 fix
		}
		// create from existing rows
		else {
			//find first available
			var row = this.$container.find(this.options.repeatableElement).get(0),
				$row = $(row).clone();

			// clear scripts that can be attached to the fields
			try {
				this.clearScripts($row);
			} catch (e) {
				if(window.console){
					console.log(e);
				}
			}

			this.template = $row.prop('outerHTML');
		}
	};

	// add new row
	$.subformRepeatable.prototype.addRow = function(after){
		// count how much we already have
		var count = this.$containerRows.find(this.options.repeatableElement).length;
		if(count >= this.options.maximum){
			return null;
		}

		// Build a regex to find all the attributes that need to be updated and their values
		let attrs = ['data-bs-target', 'src', 'href', 'data-showon', 'data-group', 'onclick', 'for', 'name', 'id'];
		let regexStr = "";
		attrs.forEach((attr, idx) => {
			regexStr += `(${attr}=\"([^"]*X[^"]*)\")|(${attr}=\'([^']*X[^']*)\')`;
			if (idx != attrs.length -1) {
				regexStr += '|'
			}
		});
		let attrsRegex = RegExp(regexStr, 'g');

		// Use the regex we build to replace all X's with the current
		// row index of the subform row we are about to add. Also, replace
		// all double underscores with a single underscore becuase reasons.
		let template = this.template.replaceAll(
			attrsRegex,
			match => match = match.replaceAll('X', count + 1)
		);
		var row = $.parseHTML(template);

		//add to container
		if(after){
			$(after).after(row);
		} else {
			this.$containerRows.append(row);
		}

		var $row = $(row);
		//add marker that it is new
		$row.attr('data-new', 'true');
		// fix names and id`s, and reset values

		// tell everyone about the new row
		this.$container.trigger('subform-row-add', $row);

		// update showon fields based on showon logic
		jQuery(document).trigger('refresh-showon', $row);
		return $row;
	};

	// remove row
	$.subformRepeatable.prototype.removeRow = function($row){
		// count how much we have
		var count = this.$containerRows.find(this.options.repeatableElement).length;
		if(count <= this.options.minimum){
			return;
		}

		// tell everyoune about the row will be removed
		this.$container.trigger('subform-row-remove', $row);
		$row.remove();
	};

	// remove scripts attached to fields
	// @TODO: make thing better when something like that will be accepted https://github.com/joomla/joomla-cms/pull/6357
	$.subformRepeatable.prototype.clearScripts = function($row){
		// destroy chosen if any
		if($.fn.chosen){
			$row.find('select.chzn-done').each(function(){
				var $el = $(this);
				$el.next('.chzn-container').remove();
				$el.show().addClass('fix-chosen');
			});
		}
	};

	// defaults
	$.subformRepeatable.defaults = {
		btAdd: ".group-add", //  button selector for "add" action
		btRemove: ".group-remove",//  button selector for "remove" action
		btMove: ".group-move",//  button selector for "move" action
		minimum: 0, // minimum repeating
		maximum: 10, // maximum repeating
		repeatableElement: ".subform-repeatable-group",
		rowTemplateSelector: 'script.subform-repeatable-template-section', // selector for the row template <script>
		rowsContainer: null // container for rows, same as main container by default
	};

	$.fn.subformRepeatable = function(options){
		return this.each(function(){
			var options = options || {},
				data = $(this).data();

			if(data.subformRepeatable){
				// Alredy initialized, nothing to do here
				return;
			}

			for (var p in data) {
				// check options in the element
				if (data.hasOwnProperty(p)) {
					options[p] = data[p];
				}
			}

			var inst = new $.subformRepeatable(this, options);
			$(this).data('subformRepeatable', inst);
		});
	};

	// initialise all available
	// wait when all will be loaded, important for scripts fix
	$(window).on('load', function(){
		$('div.subform-repeatable').subformRepeatable();
	})

})(jQuery);
