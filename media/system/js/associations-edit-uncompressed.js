/**
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * Edit Associations javascript behavior
 *
 * Used for editing associations in the backend.
 *
 * @package  Joomla
 * @since    3.7.0
 */

window.hideAssociation = function(formControl, languageCode) {
	jQuery('#associations .row').each(function() {
		const rowLanguage = jQuery(this).find('label').attr('for').replace('_id', '')
		const currentlySelectedLanguage = formControl + '_associations_' + languageCode.replace('-', '_');
		if (rowLanguage == currentlySelectedLanguage) {
			jQuery(this).hide();
		}
	});
}

window.showAssociationNotPossibleMessage = function() {
	jQuery('#associations .row').hide();
	const notice = /*html*/`
		<div id="associations-notice" class="alert alert-info">${Joomla.JText._('JGLOBAL_ASSOC_NOT_POSSIBLE')}</div>
	`;
	jQuery('#associations').append(notice);
}

!(function() {
	jQuery(document).ready(function($) {
		const associationsEditOptions = Joomla.getOptions('system.associations.edit');
		var formControl = associationsEditOptions.formControl || 'jform';

		var selectedLanguage = $('#' + formControl + '_language').val();
		// If the form input is hidden
		if (selectedLanguage == '*') {
			// Show the associations not possible message
			window.showAssociationNotPossibleMessage();
		} else {
			// Hide only the associations for the current language.
			window.hideAssociation(formControl, $('#' + formControl + '_language').val());
		}

		// When changing the language.
		$('#' + formControl + '_language').on('change', function(event) {
			// Remove message if any.
			Joomla.removeMessages();
			$('#associations-notice').remove();

			var existsAssociations = false;

			// For each language, remove the associations, ie, empty the associations fields and reset the buttons to Select/Create.
			$('#associations .row').each(function() {
				var languageCode = $(this).find('label').attr('for').replace('_id', '').replace('jform_associations_', '');

				// Show the association fields.
				$(this).show();

				// Check if there was an association selected for this language.
				if (!existsAssociations && $('#' + formControl + '_associations_' + languageCode + '_id').val() !== '') {
					existsAssociations = true;
				}

				// Call the modal clear button.
				$('#' + formControl + '_associations_' + languageCode + '_clear').click();
			});

			// If associations existed
			if (existsAssociations) {
				// Send a warning to the user
				Joomla.renderMessages({warning: [Joomla.JText._('JGLOBAL_ASSOCIATIONS_RESET_WARNING')]});
			}

			var selectedLanguage = $(this).val();

			// If the selected language is All
			if (selectedLanguage == '*') {
				// Hide the fields and add a message.
				window.showAssociationNotPossibleMessage();
			} else {
				// Show the associations fields/buttons and hide the current selected language.
				window.hideAssociation(formControl, selectedLanguage);
			}
		});
	});
})(window, document, Joomla);
