$(document).on('click', '.customReportButton', function(e) {
    var action        = $(this).data('action');
    var reportId      = $(this).data('id');
    var redirectUrl   = $(this).data('url');

    if(data) {
    	data.action    = action;
    	data.report_id = reportId;
        $.ajax({
            url: '/index.php?option=com_axs&task=customreports.sendSubmission&format=raw',
            data: data,
            type: 'post'
            }).done(function(result) {
            	if(redirectUrl) {
			    	window.location.href = redirectUrl;
			    }
            	var response = JSON.parse(result);
                if(response.success) {
                	$('.button_container_'+reportId).each(function(){
                		$(this).html(response.message);
                	});
                }
            }).fail(function() {
                console.log('error');
            });		
	}
});