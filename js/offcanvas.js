$(document).ready(function() {
    var offCanvasContainer = `<div class="offCanvasContainer">
								<div class="closeBtn"><i class="fa fa-remove"></i></div>
								<div class="offCanvas-title"></div>
								<div class="nano has-scrollbar">
									<div class="nano-content">
										<div class="offCanvas-body"></div>
									</div>
								</div>
							  </div>`;
    $('body').append(offCanvasContainer);
    $(document).on('click', '.trigger-offcanvas', function() {
        var contentId = $(this).data('content');
        var content = $('#' + contentId);
        var title = content.find('#' + contentId + '-title').clone();
        var body = content.find('#' + contentId + '-body').clone();
        body.find('a').removeAttr('id');
        $('.offCanvas-title').html('').append(title);
        $('.offCanvas-body').html('').append(body);
        $('.offCanvasContainer').css('right', '0px');
    });

    $(document).on('click', '.closeBtn', function() {
        $('.offCanvasContainer').css('right', '-350px');
    });

    $(document).on('click', '#wrapper', function(el) {
        if ($('.offCanvasContainer').css('right') == '0px') {
            $('.offCanvasContainer').css('right', '-350px');
        }
    });
});