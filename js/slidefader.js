$(document).ready(function(){
    
	var slidesWrapper = $('.cd-hero-slider');
	
	if ( slidesWrapper.length > 0 ) {
		var sliderNav = $('.cd-slider-nav'),
			navigationMarker = $('.cd-marker'),
			slidesNumber = slidesWrapper.children('li').length,
			visibleSlidePosition = 0,
			autoPlayId,
			autoPlayDelay = 7000;
	}
	

	function playSlideShow(slidesWrapper,slidesNumber) {

		if(wrapper.hasClass('autoplay')) {
			clearInterval(autoPlayId);
			autoPlayId = window.setInterval(function() {
				autoplaySlider(length)
			}, delay);
		}

	}

	function nextSlide(visibleSlide, container, pagination, n){
		visibleSlide.removeClass('selected').fadeOut(700);
		container.children('li').eq(n).addClass('selected').fadeIn(700);
		checkVideo(visibleSlide, container, n);
	}

	function prevSlide(visibleSlide, container, pagination, n){
		visibleSlide.removeClass('selected').fadeOut(700);
		container.children('li').eq(n).addClass('selected').fadeIn(700);
		checkVideo(visibleSlide, container, n);
	}

	function updateSliderNavigation(pagination, n) {
		var navigationDot = pagination.find('.selected');
		navigationDot.removeClass('selected');
		pagination.find('li').eq(n).addClass('selected');
	}

	function setAutoplay(wrapper, length, delay) {
		wrapper.find('.selected').fadeIn(300);
		if(wrapper.hasClass('autoplay') && length > 1) {
			clearInterval(autoPlayId);
			autoPlayId = window.setInterval(function() {
				autoplaySlider(length)
			}, delay);
		}
	}

	function autoplaySlider(length) {		
		if( visibleSlidePosition < length - 1) {
			nextSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, visibleSlidePosition + 1);
			visibleSlidePosition +=1;
		} else {
			prevSlide(slidesWrapper.find('.selected'), slidesWrapper, sliderNav, 0);
			visibleSlidePosition = 0;
		}
		
	}

	function uploadVideo(container) {
		container.find('.cd-bg-video-wrapper').each(function(){
			var videoWrapper = $(this);
			var	videoUrl = videoWrapper.data('video'),
					video = $('<video loop><source src="'+videoUrl+'.mp4" type="video/mp4" /><source src="'+videoUrl+'.webm" type="video/webm" /></video>');
				video.appendTo(videoWrapper);
				// play video if first slide
				if(videoWrapper.parent('.cd-bg-video.selected').length > 0) {
					video.get(0).muted = true;
					video.get(0).play();
				}
			if( videoWrapper.is(':visible') ) {
				// if visible - we are not on a mobile device 
				
			}
		});
	}

	function checkVideo(hiddenSlide, container, n) {
		//check if a video outside the viewport is playing - if yes, pause it
		var hiddenVideo = hiddenSlide.find('video');
		if( hiddenVideo.length > 0 ) hiddenVideo.get(0).pause();

		//check if the select slide contains a video element - if yes, play the video
		var visibleVideo = container.children('li').eq(n).find('video');
		
		if( visibleVideo.length > 0 ) {
			visibleVideo.get(0).muted = true;
			visibleVideo.get(0).play();
		}
	}

	if ( slidesWrapper.length > 0 ) {
		//upload videos (if not on mobile devices)
		uploadVideo(slidesWrapper);

		//autoplay slider
		setAutoplay(slidesWrapper, slidesNumber, autoPlayDelay);
		
	}

}); 