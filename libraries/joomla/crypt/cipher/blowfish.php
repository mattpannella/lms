<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Crypt
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * JCrypt cipher for Blowfish encryption, decryption and key generation.
 */
class JCryptCipherBlowfish extends JCryptCipherOpenssl
{
	/**
	 * @var    string  The openssl cipher constant.
	 * @link   https://www.php.net/manual/en/function.openssl-get-cipher-methods.php
	 * @since  12.1
	 */
	protected $type = 'bf-cbc';

	/**
	 * @var    string  The JCrypt key type for validation.
	 * @since  12.1
	 */
	protected $keyType = 'blowfish';
}
