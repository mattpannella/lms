<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

/**
 * Form Field class for the Joomla Platform.
 * Display a JSON loaded window with a repeatable set of sub fields
 *
 * @since       3.2
 *
 * @deprecated  4.0  Use JFormFieldSubform
 */
class JFormFieldRepeatable extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  3.2
	 */
	protected $type = 'Repeatable';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string  The field input markup.
	 *
	 * @since   3.2
	 */
	protected function getInput()
	{
		JLog::add('JFormFieldRepeatable is deprecated. Use JFormFieldSubform instead.', JLog::WARNING, 'deprecated');

		// Initialize variables.
		$subForm = new JForm($this->name, array('control' => 'jform'));
		$xml = $this->element->children()->asXml();
		$subForm->load($xml);

		// Needed for repeating modals in gmaps
		// @TODO: what and where???
		$subForm->repeatCounter = (int) @$this->form->repeatCounter;

		$children = $this->element->children();
		$subForm->setFields($children);

		$maximum = $this->element['maximum'] ? (int) $this->element['maximum'] : '999';

		// Build a Table
		$head_row_str = array();
		$body_row_str = array();
		$body_row_str[] = '<td><span class="sortable-handler" style="cursor: move;"><span class="icon-menu" aria-hidden="true"></span></span></td>';
		$head_row_str[] = '<th></th>';
		$fieldset = $subForm->getFieldset();
		$field_count = count($fieldset);
		$column_width = strval(100 / $field_count) . '%';
		foreach ($fieldset as $field)
		{
			// Reset name to simple
			$field->name = (string) $field->element['name'];
			$field->width = (string) !empty($field->element['width']) ? $field->element['width'] : $column_width;

			// Build heading
			$head_row_str[] = '<th width="'. $column_width .'">' . strip_tags($field->getLabel($field->name));
			$head_row_str[] = '<br /><small style="font-weight:normal">' . JText::_($field->description) . '</small>';
			$head_row_str[] = '</th>';

			// Build body
			$body_row_str[] = '<td>' . $field->getInput() . '</td>';
		}

		// Append buttons
		$body_row_str[] = '<td><div class="btn-group">';
		$body_row_str[] = '<a class="add btn button btn-success"><span class="icon-plus"></span> </a>';
		$body_row_str[] = '<a class="remove btn button btn-danger"><span class="icon-minus"></span> </a>';
		$body_row_str[] = '</div></td>';

		$tableStyles = "display:inherit;";
		$tableStyles .= empty($this->element['modalContentWidth']) ? '' : 'min-width:' . $this->element['modalContentWidth'] . ';';
		$tableStyles .= empty($this->element['modalContentHeight']) ? '' : 'min-height:' . $this->element['modalContentHeight'] . ';';

		// Put all table parts together
		$table = '<table id="' . $this->id . '_table" class="adminlist ' . $this->element['class'] . ' table table-striped" style="'. $tableStyles .'">'
					. '<thead><tr>' . implode("\n", $head_row_str) . '</tr></thead>'
					. '<tbody><tr>' . implode("\n", $body_row_str) . '</tr></tbody>'
				. '</table>';

		// And finaly build a main container
		$str = array();
		$str[] = '<div id="' . $this->id . '_container">';
		$useOffCanvas = empty($this->element['useOffCanvas']) ? '' : $this->element['useOffCanvas'];
		if ($useOffCanvas == "true") {
			$str[] = trim('  
			<div class="offcanvas offcanvas-start" tabindex="-1" id="' . $this->id . '_offcanvas" aria-labelledby="offcanvasLabel" style="width:90%">
				<div id="offcanvas-container" class="offcanvas-header">
					<button class="close-modal btn button btn-danger" data-bs-dismiss="offcanvas">' . JText::_('JCANCEL') . '</button>
					<button class="save-modal-data btn button btn-primary" data-bs-dismiss="offcanvas">' . JText::_('JAPPLY') . '</button>
				</div>
				<div class="offcanvas-body">
					'.$table.'
				</div>
			</div>
		');
		} else {
			$modalSize = empty($this->element['modalSize']) ? '' : 'modal-' . $this->element['modalSize'];
			$modalBreakpoint = 'modal-fullscreen-sm-down';
			switch ($modalSize) {
				case 'modal-lg':
					$modalBreakpoint = 'modal-fullscreen-md-down';
				break;
				case 'modal-xl':
					$modalBreakpoint = 'modal-fullscreen-lg-down';
				break;
			}
			$modalStyle = "";
			$modalStyle .= empty($this->element['modalWidth']) ? '' : 'min-width:' . $this->element['modalWidth'] . ';';
			$modalStyle .= empty($this->element['modalHeight']) ? '' : 'min-height:' . $this->element['modalHeight'] . ';';
			$modalBodyStyle = "";
			$modalBodyStyle .= $modalStyle;
			$modalBodyStyle .= empty($this->element['modalContentWidth']) ? '' : 'overflow-x:scroll;';
			$modalBodyStyle .= 'height: 60vh';

			$str[] = trim('  
			<div id="' . $this->id . '_modal" class="modal fade">
			    <div class="modal-dialog '. $modalBreakpoint . ' ' . $modalSize . ' modal-dialog-centered modal-dialog-scrollable" style="'. $modalStyle .'">
			      	<div class="modal-content" style="' . $modalStyle .'">
					  	<div class="modal-header d-block">
							<div class="btn-group float-end"><a href="#" class="add btn button btn-success"><span class="icon-plus"></span> </a></div>
						</div>
			        	<div class="modal-body" style="' . $modalBodyStyle .'">
			          		'.$table.'
			        	</div>
			        	<div class="modal-footer">
			          		<button class="close-modal btn button btn-danger">' . JText::_('JCANCEL') . '</button>
			          		<button class="save-modal-data btn button btn-primary">' . JText::_('JAPPLY') . '</button>
			        	</div>
			      	</div>
			    </div>
			</div>
			');
		}


		// Close main container
		$str[] = '</div>';

		// Button for display the modal window
		$select = (string) $this->element['select'] ? JText::_((string) $this->element['select']) : JText::_('JLIB_FORM_BUTTON_SELECT');

		$icon = $this->element['icon'] ? '<span class="icon-' . $this->element['icon'] . '"></span> ' : '';

		$offCanvasAttrs = '';
		if ($useOffCanvas) {
			$offCanvasAttrs = 'data-bs-toggle="offcanvas" data-bs-target="#' . $this->id . '_offcanvas"';
		}
		$str[] = '<button type="button" '.$offCanvasAttrs.' class="open-modal btn bg-white border" id="' . $this->id . '_button" >' . $icon . $select . '</button>';
		
		if (is_array($this->value))
		{
			$this->value = array_shift($this->value);
		}

		// Script params
		$data = array();
		$data[] = 'data-container="#' . $this->id . '_container"';
		$data[] = 'data-modal-element="#' . $this->id . '_modal"';
		$data[] = 'data-repeatable-element="table tbody tr"';
		$data[] = 'data-bt-add="a.add"';
		$data[] = 'data-bt-remove="a.remove"';
		$data[] = 'data-bt-modal-open="#' . $this->id . '_button"';
		$data[] = 'data-bt-modal-close="button.close-modal"';
		$data[] = 'data-bt-modal-save-data="button.save-modal-data"';
		$data[] = 'data-maximum="' . $maximum . '"';
		$data[] = 'data-input="#' . $this->id . '"';

		// Hidden input, where the main value is
		$value = htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8');
		$str[] = '<input type="hidden" name="' . $this->name . '" id="' . $this->id . '" value="' . $value
				. '"  class="form-field-repeatable" ' . implode(' ', $data) . ' />';

		// Add scripts
		JHtml::_('bootstrap.framework');

		// Depends on jQuery UI
		JHtml::_('jquery.ui', array('core.latest'));

		JHtml::_('script', 'jui/sortablelist.js', array('version' => 'auto', 'relative' => true));
		JHtml::_('stylesheet', 'jui/sortablelist.css', array('version' => 'auto', 'relative' => true));
		JHtml::_('script', 'system/repeatable.js', array('framework' => true, 'version' => 'auto', 'relative' => true));

		$javascript = 'jQuery(document).ready(function($) { $("#' . $this->id . '_table tbody").sortable(); });';

		JFactory::getDocument()->addScriptDeclaration($javascript);

		return implode("\n", $str);
	}
}
