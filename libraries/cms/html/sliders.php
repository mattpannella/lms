<?php
/**
 * @package     Joomla.Libraries
 * @subpackage  HTML
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_PLATFORM') or die;

/**
 * Utility class for Sliders elements
 *
 * @since       1.6
 * @deprecated  3.7.0 These helpers are dependent on the deprecated MooTools support
 */
abstract class JHtmlSliders
{
	/**
	 * Creates a panes and loads the javascript behavior for it.
	 *
	 * @param   string  $group   The pane identifier.
	 * @param   array   $params  An array of options.
	 *
	 * @return  string
	 *
	 * @since   1.6
	 * @deprecated  3.7.0 These helpers are dependent on the deprecated MooTools support
	 */
	public static function start($group = 'sliders', $params = array())
	{
		return '<div id="' . $group . '"><div style="display:none;"><div>';
	}

	/**
	 * Close the current pane.
	 *
	 * @return  string  hTML to close the pane
	 *
	 * @since   1.6
	 * @deprecated  3.7.0 These helpers are dependent on the deprecated MooTools support
	 */
	public static function end()
	{
		return '</div></div></div>';
	}

	/**
	 * Begins the display of a new panel.
	 *
	 * @param   string  $text  Text to display.
	 * @param   string  $id    Identifier of the panel.
	 *
	 * @return  string  HTML to start a panel
	 *
	 * @since   1.6
	 * @deprecated  3.7.0 These helpers are dependent on the deprecated MooTools support
	 */
	public static function panel($text, $id)
	{
		return '</div></div><div><h3 id="' . $id . '"><span class="p-2">' . $text
			. '</span></h3><div class="mb-4">';
	}

}
