<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/3/16
 * Time: 5:11 PM
 */

namespace Omnipay\Heartland\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Exception\InvalidResponseException;

/**
 * Response.
 */

class Response extends AbstractResponse  {

	public $message;

	public function __construct(RequestInterface $request, $data) {
		
		$parser = xml_parser_create();
		xml_parse_into_struct($parser, $data, $val, $done);
		$data = array();

		foreach ($val as $info) {
			if (isset($info['value'])) {
				$data[$info['tag']] = $info['value'];
			}
		}

		parent::__construct($request, $data);
	}

	/**
	 * Is the response successful?
	 *
	 * @return boolean
	 */
	public function isSuccessful() {

		if (isset($this->data['GATEWAYRSPCODE'])) {
			$this->gatewayCode = $this->data['GATEWAYRSPCODE'];
		} else {
			$this->gatewayCode = null;
		}

		if (isset($this->data['RSPCODE'])) {
			$this->rspCode = $this->data['RSPCODE'];
		} else {
			$this->rspCode = null;
		}

		if (isset($this->data['GATEWAYRSPMSG'])) {
			if ($this->gatewayCode) {
				$this->message = "Heartland Error : " . $this->gatewayCode;
			} else {
				$this->message = "";
			}
			
			$this->message .= $this->data['GATEWAYRSPMSG'];
		} else {
			$this->message = "No message";
		}

		/*
			If the gateway response code is not set, then the action was not performed.
			Check the response code itself.
		*/
		if ($this->gatewayCode != null) {
			if ((int)$this->gatewayCode == 0) {
				return true;
			} else {
				return false;
			}
		} else if ($this->rspCode != null) {
			if ((int)$this->rspCode == 0) {
				return true;
			} else {
				return false;
			}
		}

		return false;
	}


	public function getTransactionReference() {		
		return $this->data['GATEWAYTXNID'];
	}

	public function getCardReference() {
		if (isset($this->data['TOKENVALUE'])) {			
			return $this->data['TOKENVALUE'];
		} else {
			return false;
		}
	}

	public function getMessage() {
		if (isset($this->data['GATEWAYRSPMSG'])) {
			return $this->data['GATEWAYRSPMSG'];
		} else {
			return "No error message";
		}
	}
}