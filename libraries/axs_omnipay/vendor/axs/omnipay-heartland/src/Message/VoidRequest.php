<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/11/16
 * Time: 1:02 PM
 */

namespace Omnipay\Heartland\Message;

class VoidRequest extends AbstractRequest {

	public $successMessage = "Transaction was Voided Successfully!";

	public function getData() {
		return $this->getBaseData('void');
	}
}