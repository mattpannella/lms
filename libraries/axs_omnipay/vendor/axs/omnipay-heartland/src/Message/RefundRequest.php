<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/11/16
 * Time: 12:35 PM
 */

namespace Omnipay\Heartland\Message;

class RefundRequest extends AbstractRequest {

	public $successMessage = "Transaction was Refunded Successfully!";

	public function getVoidIfRefundFails() {
		return $this->getParameter('voidIfRefundFails');
	}

	public function setVoidIfRefundFails($value) {		
		$this->setParameter('voidIfRefundFails', $value);
	}

	public function getData() {
		if($this->getVoidIfRefundFails()) {
			//try void first			
			return $this->getBaseData('void');
		} else {
			//try refund			
			return $this->getBaseData('refund');
		}
	}

	// Override send() so we can check and see whether to try a return if void is unsuccessful.
	public function send() {
		$response = parent::send();
		
		if (!$response->isSuccessful() && $this->getVoidIfRefundFails()) {
			// This transaction has already been settled, so do a traditional refund.
			$refundRequest = new RefundRequest($this->httpClient, $this->httpRequest);
			$refundRequest->initialize($this->getParameters());
			$refundRequest->setVoidIfRefundFails(false);
			$response = $refundRequest->send();
		}		

		return $response;
	}
}