<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/3/16
 * Time: 3:48 PM
 */

namespace Omnipay\Heartland\Message;

use DOMDocument as DOMDocument;

use Omnipay\Common\Message\AbstractRequest as BaseAbstractRequest;
use Omnipay\Common\Exception\InvalidCreditCardException;

function var_pretty($data){
	highlight_string(var_export($data, true));
}

/**
 * Abstract Request.
 */
abstract class AbstractRequest extends BaseAbstractRequest {

	public function setSoapServiceUriDev($value) {
		return $this->setParameter('soapServiceUriDev', $value);
	}
	public function setSoapServiceUriProd($value) {
		return $this->setParameter('soapServiceUriProd', $value);
	}
	public function setSoapServiceUriMode($value) {
		return $this->setParameter('soapServiceUriMode', $value);
	}
	public function getSoapServiceUri() {
    	if ($this->getParameter('soapServiceUriMode') == 'dev') {
    		return $this->getParameter('soapServiceUriDev');
    	} else if ($this->getParameter('soapServiceUriMode') == 'prod') {
    		return $this->getParameter('soapServiceUriProd');
    	}
    }

	public function setSecretApiKey($value) {
		return $this->setParameter('secretApiKey', $value);
	}
	public function setDeveloperId($value) {
		return $this->setParameter('developerId', $value);
	}
	public function setVersionNbr($value) {
		return $this->setParameter('versionNbr', $value);
	}
	public function setSiteTrace($value) {
		return $this->setParameter('siteTrace', $value);
	}

	public function getSecretApiKey() {
		return $this->setParameter('secretApiKey');
	}
	public function getDeveloperId() {
		return $this->getParameter('developerId');
	}
	public function getVersionNbr() {
		return $this->getParameter('versionNbr');
	}
	public function getSiteTrace() {
		return $this->getParameter('siteTrace');
	}

	private function getHeaderXML($xml) {	

		$hpsHeader = $xml->createElement('hps:Header');

    	$hpsHeader->appendChild($xml->createElement('hps:SecretAPIKey', $this->getParameter('secretApiKey')));        
        $hpsHeader->appendChild($xml->createElement('hps:DeveloperID',  $this->getParameter('developerId')));
        $hpsHeader->appendChild($xml->createElement('hps:VersionNbr',  $this->getParameter('versionNbr')));
        $hpsHeader->appendChild($xml->createElement('hps:SiteTrace', $this->getParameter('siteTrace')));

		return $hpsHeader;
	}

	private function getTransactionXML($xml, $type, $card) {

		$hpsTransaction = $xml->createElement('hps:Transaction');

		switch ($type) {
			case 'purchase':	//Purchase
        		$hpsTransType = $xml->createElement('hps:CreditSale');
        		break;
        	case 'authorize': //Authorize Payment
        		$hpsTransType = $xml->createElement('hps:CreditAuth');
        		break;
        	case 'refund': //Return Payment
        		$hpsTransType = $xml->createElement('hps:CreditReturn');
        		break;
        	case 'void': //Void Payment
        		//Void is very minimal
        		$hpsTransType = $xml->createElement('hps:CreditVoid');        		
				$transId = $xml->createElement('hps:GatewayTxnId', $this->getTransactionReference());
				$hpsTransType->appendChild($transId);
				$hpsTransaction->appendChild($hpsTransType);
				return $hpsTransaction;				
        		
        	case 'delete':	//Delete Card
        	case 'update': //Update Card
        		
        		$hpsTransType = $xml->createElement('hps:ManageTokens');
        		if (gettype($card) === 'string') {
        			$token = $card;
        		} else {
        			$token = $card->token;
        		}

        		$tokenValue = $xml->createElement('hps:TokenValue', $token);
        		$tokenActions = $xml->createElement('hps:TokenActions');
        		if ($type == 'delete') {
        			$delete = $xml->createElement('hps:Delete');
        			$tokenActions->appendChild($delete);
        		}
        		if ($type == 'update') {
        			$set = $xml->createElement('hps:Set');
        			$attributeMonth = $xml->createElement('hps:Attribute');
        			$attributeYear = $xml->createElement('hps:Attribute');
        			$attributeMonth->appendChild($xml->createElement('hps:Name', 'ExpMonth'));
        			$attributeMonth->appendChild($xml->createElement('hps:Value', $card->getExpiryMonth()));
        			$attributeYear->appendChild($xml->createElement('hps:Name', 'ExpYear'));
        			$attributeYear->appendChild($xml->createElement('hps:Value', $card->getExpiryYear()));
        			$set->appendChild($attributeMonth);
        			$set->appendChild($attributeYear);
        			$tokenActions->appendChild($set);
        		}
        		$hpsTransType->appendChild($tokenValue);
        		$hpsTransType->appendChild($tokenActions);
        		$hpsTransaction->appendChild($hpsTransType);
        		return $hpsTransaction;

		}

		$hpsBlock1 = $xml->createElement('hps:Block1');
        $hpsBlock1->appendChild($xml->createElement('hps:AllowDup', 'Y'));

        switch ($type) {
        	case 'purchase':
        	case 'authorize':
        		$hpsBlock1->appendChild($xml->createElement('hps:AllowPartialAuth', 'N'));
        		break;
        	case 'refund':
        	case 'void':
        		//AllowPartialAuth will break it.
        		break;
        }

        if ($type != 'authorize') {
        	$hpsBlock1->appendChild($xml->createElement('hps:Amt', $this->getParameter('amount')));
        } else {
        	//Authorization will not be supplied a dollar amount, but needs one to go through.
        	$hpsBlock1->appendChild($xml->createElement('hps:Amt', 1));
        }
        
        switch ($type) {
        	case 'purchase':        	 
        		$cardData = $xml->createElement('hps:CardData');
        		$cardData->appendChild($this->getTokenDataXML($xml, $card));        		
        		$tokenRequest = $xml->createElement('hps:TokenRequest', "N");
        		$cardData->appendChild($tokenRequest);
        		$hpsBlock1->appendChild($cardData);
        		break;
        	case 'authorize':
        		$cardData = $xml->createElement('hps:CardData');
        		$cardData->appendChild($this->getManualEntryXML($xml, $card));        		
        		$hpsBlock1->appendChild($this->getCardHolderDataXML($xml, $card));
        		$tokenRequest = $xml->createElement('hps:TokenRequest', "Y");
        		$cardData->appendChild($tokenRequest);
        		$hpsBlock1->appendChild($cardData);
        		break;
        	case 'refund':
        		$transId = $xml->createElement('hps:GatewayTxnId', $this->getTransactionReference());
        		$hpsBlock1->appendChild($transId);        	
        }

        $hpsTransType->appendChild($hpsBlock1);
        $hpsTransaction->appendChild($hpsTransType);

        return $hpsTransaction;
	}

	private function getCardHolderDataXML($xml, $card) {

		$cardHolderData = $xml->createElement('hps:CardHolderData');
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderFirstName', $card->getBillingFirstName()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderLastName', $card->getBillingLastName()));        
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderEmail', $card->getEmail()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderPhone', $card->getPhone()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderAddr', $card->getBillingAddress1()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderCity', $card->getBillingCity()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderState', $card->getBillingState()));
        $cardHolderData->appendChild($xml->createElement('hps:CardHolderZip', $card->getBillingPostcode()));

        return $cardHolderData;
	}

	private function getTokenDataXML($xml, $card) {
		$tokenData = $xml->createElement('hps:TokenData');
		if (gettype($card) === 'string') {
			$token = $card;
		} else {
			throw new InvalidCreditCardException("Card has no token.");
		}

        $tokenData->appendChild($xml->createElement('hps:TokenValue', $token));
        $tokenData->appendChild($xml->createElement('hps:CardPresent','N'));
        $tokenData->appendChild($xml->createElement('hps:ReaderPresent','N'));
        return $tokenData;
	}

	private function getManualEntryXML($xml, $card) {

		$manualEntry = $xml->createElement('hps:ManualEntry');

		$manualEntry->appendChild($xml->createElement('hps:CardNbr', $card->getNumber()));
		$manualEntry->appendChild($xml->createElement('hps:ExpMonth', $card->getExpiryDate("m")));
		$manualEntry->appendChild($xml->createElement('hps:ExpYear', $card->getExpiryDate("Y")));
		$manualEntry->appendChild($xml->createElement('hps:CVV2', $card->getCvv()));
		$manualEntry->appendChild($xml->createElement('hps:CardPresent', "N"));
		$manualEntry->appendChild($xml->createElement('hps:ReaderPresent', "N"));		

		return $manualEntry;
	}

	private function getXMLBase($type, $card) {

		$xml = new DOMDocument('1.0', 'utf-8');
        $soapEnvelope = $xml->createElement('soapenv:Envelope');
        $soapEnvelope->setAttribute('xmlns:soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');
        $soapEnvelope->setAttribute('xmlns:hps', 'http://Hps.Exchange.PosGateway');

        $soapBody = $xml->createElement('soapenv:Body');
        $hpsRequest = $xml->createElement('hps:PosRequest');

        $hpsVersion = $xml->createElement('hps:Ver1.0');
        
        $hpsVersion->appendChild($this->getHeaderXML($xml));
        $hpsVersion->appendChild($this->getTransactionXML($xml, $type, $card));

        $hpsRequest->appendChild($hpsVersion);
        $soapBody->appendChild($hpsRequest);
        $soapEnvelope->appendChild($soapBody);
        $xml->appendChild($soapEnvelope);

        $header = array(
            'Content-type: text/xml;charset="utf-8"',
            'Accept: text/xml',
            'SOAPAction: ""',
            'Content-length: ' . strlen($xml->saveXML()),
        );

        $data = $xml->saveXML();
        
        return $data;
	}

	public function getBaseData($type) {
		return $this->getXMLBase($type, $this->getCardInfo($type));		
	}

	public function getCardData() {
		return $this->getXMLBase('authorize', $this->getCardInfo('authorize'));		
	}

	public function getDeleteCardData() {
		return $this->getXMLBase('delete', $this->getCardInfo('delete'));		
	}

	public function getUpdateCardData() {
		return $this->getXMLBase('update', $this->getCardInfo('update'));		
	}

	private function getCardInfo($type) {

		$card = $this->getCard();
		$cardReference = $this->getCardReference();

		if ($card && $cardReference) {
			$cardData = $card;
			$cardData->token = $cardReference;
		} else if ($card) {
			$cardData = $this->getCard();
		} else if ($cardReference) {
			$cardData = $this->getCardReference();		
		} else {
			if ($type != 'refund' && $type != 'void') {
				throw new InvalidCreditCardException("No credit card supplied.");
			}
		}

		return $cardData;
	}

	public function getTransactionReference() {
		return $this->getParameter('transactionReference');
	}

	public function sendData($data) {
		$headers = array('Content-Type' => 'text/xml; charset=utf-8');

		$httpResponse = $this->httpClient->post($this->getSoapServiceUri(), $headers, $data)->send();
		$this->response = new Response($this, $httpResponse->getBody());
		
		return $this->response;
	}
}