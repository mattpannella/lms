<?php
/**
 * Created by PhpStorm.
 * User: mar
 * Date: 11/3/16
 * Time: 2:39 PM
 */

namespace Omnipay\Heartland\Message;

/**
 * Purchase Request.
 *
 * @method PurchaseResponse send()
 */
class PurchaseRequest extends AbstractRequest {

	public $successMessage = "Purchase made Successfully!";

	/**
	 * Get the raw data array for this message. The format of this varies from gateway to
	 * gateway, but will usually be either an associative array, or a SimpleXMLElement.
	 *
	 * @return mixed
	 */
	public function getData() {
		return $this->getBaseData('purchase');
	}

	private function invoiceNumber($prefix = "DC-", $length = 6) {
		$chars         = "0123456789";
		$invoiceNumber = "";
		srand((double) microtime() * 1000000);
		for ($i = 0; $i < $length; $i++) {
			$invoiceNumber .= $chars[rand() % strlen($chars)];
		}
		return $prefix . $invoiceNumber;
	}
}