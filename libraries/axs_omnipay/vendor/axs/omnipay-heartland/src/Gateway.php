<?php

namespace Omnipay\Heartland;

use Omnipay\Common\AbstractGateway;

/**
 * Heartland Gateway
 */
class Gateway extends AbstractGateway {
	public function getName() {
		return 'Heartland Payment Systems';
	}

	public function getDefaultParameters() {
		return array (
			'credentialToken' 		=> null,
		    'secretApiKey'    		=> null,
		    'publicApiKey'    		=> null,
		    'licenseId'       		=> null,
		    'siteId'          		=> null,
		    'deviceId'        		=> null,
		    'versionNumber'   		=> null,
		    'username'        		=> null,
		    'password'        		=> null,
		    'developerId'     		=> '002914',
		    'versionNbr'			=> '2564',
		    'siteTrace'       		=> null,
		    'useProxy'        		=> null,
		    'proxyOptions'    		=> null,
		    'soapServiceUriDev'  	=> "https://cert.api2.heartlandportico.com/Hps.Exchange.PosGateway/PosGatewayService.asmx",
		    'soapServiceUriProd'  	=> "https://api2.heartlandportico.com/Hps.Exchange.PosGateway/PosGatewayService.asmx",
		    'soapServiceUriMode'	=> null,
		    'payPlanBaseUri'  		=> null,
		    'curlOptions'     		=> null,
		);
	}


	public function getCredentialToken() {
		return $this->getParameter('credentialToken');
	}
    public function getSecretApiKey() {
    	return $this->getParameter('secretApiKey');
    }
    public function getPublicApiKey() {
    	return $this->getParameter('publicApiKey');
    }
    public function getLicenseId() {
    	return $this->getParameter('licenseId');
    }
    public function getSiteId() {
    	return $this->getParameter('siteId');
    }
    public function getDeviceId() {
    	return $this->getParameter('deviceId');
    }
    public function getVersionNumber() {
    	return $this->getParameter('versionNumber');
    }
    public function getUsername() {
    	return $this->getParameter('username');
    }
    public function getPassword() {
    	return $this->getParameter('password');
    }
    public function getDeveloperId() {
    	return $this->getParameter('developerId');
    }
    public function getVersionNbr() {
		return $this->getParameter('versionNbr');
	}
    public function getSiteTrace() {
    	return $this->getParameter('siteTrace');
    }
    public function getUseProxy() {
    	return $this->getParameter('useProxy');
    }
    public function getProxyOptions() {
    	return $this->getParameter('proxyOptions');
    }
    public function getSoapServiceUri() {

    	if ($this->getParameter('soapServiceUriMode') == 'dev') {
    		return $this->getParameter('soapServiceUriDev');
    	} else if ($this->getParameter('soapServiceUriMode') == 'prod') {
    		return $this->getParameter('soapServiceUriProd');
    	}
    }
    public function getPayPlanBaseUri() {
    	return $this->getParameter('payPlanBaseUri');
    }
    public function getCurlOptions() {
    	return $this->getParameter('curlOptions');
    }
    
	

    public function setCredentialToken($value) {
    	return $this->setParameter('credentialToken', $value);
    }
	public function setSecretApiKey($value) {
		return $this->setParameter('secretApiKey', $value);
	}
	public function setPublicApiKey($value) {
		return $this->setParameter('publicApiKey', $value);
	}
	public function setLicenseId($value) {
		return $this->setParameter('licenseId', $value);
	}
	public function setSiteId($value) {
		return $this->setParameter('siteId', $value);
	}
	public function setDeviceId($value) {
		return $this->setParameter('deviceId', $value);
	}
	public function setVersionNumber($value) {
		return $this->setParameter('versionNumber', $value);
	}
	public function setUsername($value) {
		return $this->setParameter('username', $value);
	}
	public function setPassword($value) {
		return $this->setParameter('password', $value);
	}
	public function setDeveloperId($value) {
		return $this->setParameter('developerId', $value);
	}
	public function setVersionNbr($value) {
		return $this->setParameter('versionNbr', $value);
	}
	public function setSiteTrace($value) {
		return $this->setParameter('siteTrace', $value);
	}
	public function setUseProxy($value) {
		return $this->setParameter('useProxy', $value);
	}
	public function setProxyOptions($value) {
		return $this->setParameter('proxyOptions', $value);
	}
	/*public function setSoapServiceUri($value) {
		return $this->setParameter('soapServiceUri', $value);
	}*/
	public function setSoapServiceUriMode($value) {
		return $this->setParameter('soapServiceUriMode', $value);
	}
	public function setPayPlanBaseUri($value) {
		return $this->setParameter('payPlanBaseUri', $value);
	}
	public function setCurlOptions($value) {
		return $this->setParameter('curlOptions', $value);
	}

	//TRXN methods
	public function purchase(array $parameters = array()) {
		return $this->createRequest('Omnipay\Heartland\Message\PurchaseRequest', $parameters);
	}

	public function void(array $parameters = array()) {		
		return $this->createRequest('\Omnipay\Heartland\Message\VoidRequest', $parameters);
	}

	public function refund(array $parameters = array()) {		
		return $this->createRequest('\Omnipay\Heartland\Message\RefundRequest', $parameters);
	}

	//TOKEN CARD methods
	public function createCard(array $parameters = array()) {		
		return $this->createRequest('Omnipay\Heartland\Message\CreateCardRequest', $parameters);
	}

	public function updateCard(array $parameters = array()) {
		return $this->createRequest('Omnipay\Heartland\Message\UpdateCardRequest', $parameters);
	}

	public function deleteCard(array $parameters = array()) {
		return $this->createRequest('Omnipay\Heartland\Message\DeleteCardRequest', $parameters);
	}
}