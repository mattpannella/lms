<?php
defined('_JEXEC') or die;
/**
 * @package    Tovuti
 * @author	   Tovuti Security Software Pvt. Ltd.
 * @license    GNU/GPLv3
 * @copyright  Copyright 2015 Tovuti. All Rights Reserved.
 *
 *
 * This file is part of Tovuti SAML plugin.
 */

class AESEncryption {
	/**
	* @param string $data - the key=value pairs separated with &
	* @return string
	*/
	public static function encrypt_data($data, $key) {
		$strIn = AESEncryption::pkcs5_pad($data);

		return openssl_encrypt($data, 'aes-128-ecb', $key, 0, $strIn);;
	}

	/**
	* @param string $data - crypt response from Sagepay
	* @return string
	*/
	public static function decrypt_data($data, $key) {
		$strIn = base64_decode($data);

		return AESEncryption::pkcs5_unpad(openssl_decrypt($data, 'aes-128-ecb', $key, 0, $strIn));
	}

	private static function pkcs5_pad($text) {
		$size = openssl_cipher_iv_length('aes-128-ecb');
		$pad = $size - (strlen($text) % $size);

		return $text . str_repeat(chr($pad), $pad);
	}

	private static function pkcs5_unpad($text) {
		$pad = ord($text[strlen($text) - 1]);

		if ($pad > strlen($text)) return false;

		if (strspn($text, $text[strlen($text) - 1], strlen($text) - $pad) != $pad) {
			return false;
		}

		return substr($text, 0, -1 * $pad);
	}
}
?>