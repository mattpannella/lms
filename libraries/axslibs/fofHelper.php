<?php
	defined ("_JEXEC") or die;

	class AxsFoFHelper {
		public static function addFieldset($original, $newFields) {

			$form = $original->get('form');
			$model = $original->getModel();
			$item = $original->item;

			$params = $item->getData();
			$model->set('_formData', $params);
			$newData = $model->loadFormData();
			
			$data = $form->getData();

			foreach ($newData as $key => $value) {
				$data["$key"] = $value;
			}

			if (!is_array($newFields)) {
				//A single new field was passed in.
				$newFields = array($newFields);
			}
			
			foreach ($newFields as $newField) {
				$form->setField(new SimpleXMLElement($newField));
			}

			echo $original->getRenderedForm();
			
		}
	}