<?php

class AxsTeams {

	public static function getTeamListFromIds($userIds) {

		$list = array();

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_teams'))
			->where($db->quoteName('published') . '=1');

		$db->setQuery($query);

		$teams = $db->loadObjectList();

		foreach ($teams as $team) {
			$team->user_ids = explode(",", $team->user_ids);
			foreach ($team->user_ids as $id) {
				if (in_array($id, $userIds)) {

					$team->team_lead = explode(",", $team->team_lead);

					$list[]= $team;
					break;
				}
			}
		}

		return $list;
	}

	public static function getTeamById($id) {
		if(!$id) {
			return false;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_teams'))
			->where($db->quoteName('id') . '=' . $db->quote($id));
		$db->setQuery($query);
		$team = $db->loadObject();
		return $team;
	}

	public static function addTeamMembers($newMembers, $teamId) {

		if (!is_array($newMembers) || count($newMembers) == 0 || !$teamId) {
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query
			->select('*')
			->from($db->quoteName('axs_teams'))
			->where($db->quoteName('id') . '=' . $db->quote($teamId));

		$db->setQuery($query);
		$team = $db->loadObject();

		if (!$team) {
			return;
		}

		$newMembers = array_unique($newMembers);

		$user_ids = explode(",", $team->user_ids);
		//Go through all of the new members, check if they're currently in the team list, and if not, add them.
		foreach ($newMembers as $member) {
			if (!in_array($member, $user_ids)) {
				$user_ids[]= $member;
			}
		}

		$team->user_ids = implode(",", $user_ids);
		$db->updateObject('axs_teams', $team, 'id');
	}

	public static function removeTeamMembers($xMembers, $teamId) {

		if (!is_array($xMembers) || count($xMembers) == 0 || !$teamId) {
			return;
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);

		$query
			->select('*')
			->from($db->quoteName('axs_teams'))
			->where($db->quoteName('id') . '=' . $db->quote($teamId));

		$db->setQuery($query);
		$team = $db->loadObject();

		if (!$team) {
			return;
		}

		$xMembers = array_unique($xMembers);
		$newList = array();
		$current = explode(",", $team->user_ids);
		//Go through all of the current team members, if they're not in the list of members to remove, add them to the new list.
		foreach ($current as $user) {
			if (!in_array($user, $xMembers)) {
				$newList[]= $user;
			}
		}

		$team->user_ids = implode(",", $newList);
		$db->updateObject('axs_teams', $team, 'id');
	}
}