<?php
defined('_JEXEC') or die;

class AxsEmail {

	public $recipients = null;
	public $content = null;
	public $subject = null;
	public $attachment = null;
	public $code = null;
	public $brand_id = null;

	private static $default_batch_size = 500;
	private static $mailer_array;		//This is used to store any mailers that have previously been used so that they don't need to be created again.

	/**
	 * This is called to add an email to the mailer queue once the public variables are set.
	 * @return null
	 */
	public function send() {
		if (!$this->code || !$this->content || !$this->recipients || !$this->subject) {
			return false;
		}
		if (!$this->db) {
			$db = JFactory::getDBO();
		} else {
			$db = $this->db;
		}

		$date = date('Y-m-d H:i:s');

		$query = $db->getQuery(true);

		$conditions = array(
			$db->quoteName('owner_code') . '=' . $db->quote($this->code),
			$db->quoteName('date') . '= DATE(NOW())'
		);

		$query
			->select('*')
			->from($db->quoteName('axs_email_queue'))
			->where($conditions);

		$db->setQuery($query);

		$emailer = $db->loadObject();

		if (!$emailer) {
			//Only add if it doesn't exist
			$emailer = new stdClass;
			$emailer->date = $date;
			$emailer->subject = $this->subject;
			$emailer->content = $this->content;
			$emailer->attachment = $this->attachment;
			$emailer->recipients = json_encode($this->recipients);
			$emailer->owner_code = $this->code;
			$emailer->brand_id = $this->brand_id;

			$db->insertObject('axs_email_queue', $emailer);

		}

		return true;
	}

	public static function getCode() {

		//This returns a unique email code to match the email queue with the sender (to varify that it's already been sent)

		$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+-=[]{}\|/?.>,<";
		$len = strlen($characters);

		$code = strtotime('now');
		$codelength = 32 - strlen($code);

		for ($i = 0; $i < $codelength; $i++) {
			$code .= $characters[rand(0, $len - 1)];
		}

		return $code;
	}

	public static function batchSendEmails($db = null) {

		//This should only be run from a CRON
		//if (!self::$mailer_array) {
		self::$mailer_array = array();
		//}

		if (!$db) {
			$db = JFactory::getDBO();
		}

		//$filename = "email_attachment.txt";

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_email_queue'))
			->where($db->quoteName('complete') . ' = 0 OR ' . $db->quoteName('complete') . ' IS NULL');
			//->where($db->quoteName('type') . ' = 1');

		$db->setQuery($query);
		$queues = $db->loadObjectList();

		//All the files that need to be removed when finished
		$filelist = array();

		foreach ($queues as $queue) {

			$content = $queue->content;
			$subject = $queue->subject;
			$attachment = $queue->attachment;
			$recipients = json_decode($queue->recipients);
			$sent = (int)$queue->recipients_sent;
			$params = json_decode($queue->params);

			if (isset($params->batch_size)) {
				$batch_size = $params->batch_size;
			} else {
				$batch_size = self::$default_batch_size;
			}

			$end_recipient = $sent + $batch_size;
			$total = count($recipients);

			if ($end_recipient >= $total) {
				$end_recipient = $total;
				$queue->complete = 1;
			}

			$queue->recipients_sent = $end_recipient;

			//echo "$sent  $end_recipient  $total\n";

			$mailer = self::getMailerByBrandId($queue->brand_id, $db);
			//echo $subject.":\n";
			//echo $mailer->FromName."\n\n";
			$mailer->setSubject($subject);
			$mailer->isHtml(true);
			$mailer->Encoding = 'base64';
			$mailer->setBody($content);

			if ($attachment) {
				/*echo "attaching...";

				$file = fopen($filename, "w");
				fwrite($file, $attachment);
				fclose($file);
				$mailer->addAttachment($filename);*/

				$attachments = json_decode($attachment);

				switch (gettype($attachments)) {
					case "array":

						foreach ($attachments as $attachment) {
							$file = fopen($attachment->name, "w");
							$filelist[]= $attachment->name;
							fwrite($file, $attachment->contents);
							fclose($file);
							$mailer->addAttachment($attachment->name);
						}

						break;

					case "object":

						$file = fopen($attachments->name, "w");
						$filelist[]= $attachments->name;
						fwrite($file, $attachments->contents);
						fclose($file);
						$mailer->addAttachment($attachments->name);

						break;
				}

				//var_dump(gettype($attachment));
				//die();
			}

			for ($i = $sent; $i < $end_recipient; $i++) {
				$target = $recipients[$i];
				echo "\n" . $target->email . "\n\n";

				if (!filter_var($target->email, FILTER_VALIDATE_EMAIL)) {
					echo "invalid email\n";
					continue;
				}

				$mailer->addRecipient($target->email, $target->name);
				$sent = $mailer->Send();
				//var_dump($target);


				if (gettype($sent) == "object" && get_class($sent) == "JException") {
					echo $sent->getMessage() . "\n\n\n";

					//var_dump($mailer);
				} else {
					var_dump($sent);
				}

				echo "From\t\t" . $mailer->From . "\n";
				echo "Port\t\t" . $mailer->Port . "\n";
				echo "FromName\t" . $mailer->FromName . "\n";
				echo "SMTPAuth\t" . $mailer->SMTPAuth . "\n";
				echo "Username\t" . $mailer->Username . "\n";
				echo "Password\t" . $mailer->Password . "\n";
				echo "Host\t\t" . $mailer->Host . "\n";
				echo "SMTPSecure\t" . $mailer->SMTPSecure . "\n\n";

				//sleep(rand(2,4));

				$mailer->ClearAllRecipients();

			}
			$mailer->clearAttachments();
			$db->updateObject('axs_email_queue', $queue, 'id');
		}

		foreach ($filelist as $filename) {
			unlink($filename);
		}
	}

	public static function getMailerByBrandId($brand_id = null, $db = null) {
		if (!$db) {
			$db = JFactory::getDBO();
		}

		if (!$brand_id) {
			$query = $db->getQuery(true);
			$query
				->select('id')
				->from('axs_brands')
				->where($db->quoteName('published') . '= 1')
				->limit(1);

			$db->setQuery($query);
			$brand_id = (int)$db->loadResult();
		}

		$mailer = JFactory::getMailer();

		if (isset(self::$mailer_array[$brand_id])) {
			return self::$mailer_array[$brand_id];
		} else {

			$brand = AxsBrands::getBrandById($brand_id);
			$emailer = AxsBrands::getEmailSettingsById($brand->email_settings_id);
			if( !$emailer->email_custom_smtp && $emailer->smtpuser == 'tovuti85' || (!$emailer->smtpuser || !$emailer->smtphost || !$emailer->smtppass || !$emailer->smtpport || !$emailer->smtpuser || $emailer->smtpuser == 'tovuti85') ) {
				$sendgrid = new AxsSendgrid();
				$emailer = $sendgrid->setDefaultSettings($emailer);
			}
			//echo var_dump($emailer)."\n\n";
			foreach ($emailer as $key => $value) {
				if ($value) {
					switch ($key) {
						case "mailfrom":
							$mailer_key = "From";
							break;
						case "smtpport":
							$mailer_key = "Port";
							break;
						case "fromname":
							$mailer_key = "FromName";
							break;
						case "smtpauth":
							$mailer_key = "SMTPAuth";
							break;
						case "smtpuser":
							$mailer_key = "Username";
							break;
						case "smtppass":
							$mailer_key = "Password";
							break;
						case "smtphost":
							$mailer_key = "Host";
							break;
						case "smtpsecure":
							$mailer_key = "SMTPSecure";
							break;

						default:
							$mailer_key = null;
							break;
					}

					if ($mailer_key) {
						$mailer->$mailer_key = $value;
					}
				}
			}


			if ($emailer->replyto) {
				$mailer->addReplyTo($emailer->replyto, $emailer->replytoname);
			}

			self::$mailer_array[$brand_id] = $mailer;
			return self::$mailer_array[$brand_id];
		}
	}

	public static function addToEmailQueue($emailData) {
		$data = new stdClass();
		$data->date = date('Y-m-d H:i:s');
		$data->subject = $emailData->subject;
		$data->content = $emailData->content;
		$data->recipients = $emailData->recipients;
		$data->recipients_sent = 0;
		$data->brand_id = $emailData->brand_id;
		$data->owner_code = $emailData->owner_code;
		$data->complete = 0;
		$data->type = $emailData->type;
		$db = JFactory::getDbo();
		$db->insertObject('axs_email_queue',$data);
	}
}