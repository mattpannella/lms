<?php

defined('_JEXEC') or die;

class AxsForum {

	public static function getTopics($category_id) {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();

        $conditions[] = $db->quoteName('category_id')   . "=" . $db->quote($category_id);

        $query
            ->select('*')
            ->from($db->quoteName('#__kunena_topics'))
            ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();

        return $results;
    }

	public static function getCategoryCounts($category_id) {
		$count = new stdClass();
		$count->topics = 0;
		$count->posts = 0;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();
        $conditions[] = $db->quoteName('category_id')   . "=" . $db->quote($category_id);
        $query
            ->select('*')
            ->from($db->quoteName('#__kunena_topics'))
            ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();

		if($results) {
			foreach($results as $topics) {
				$count->topics++;
				$count->posts +=  $topics->posts;
			}
		}

        return $count;
    }
}