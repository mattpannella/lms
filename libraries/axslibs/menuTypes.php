<?php

defined('_JEXEC') or die;

class AxsMenuTypes {

    /**
     * 
     * Filters out any deprecated menu types we don't want clients to see
     * 
     * @return array - an array of allowed menu types
     */
    public static function filterMenuTypes($items) {

        //Array of allowed menu items
        $allowedMenuItems = [

            'AXS' => [
                'title' => 'Assorted Links & Landing Pages',
                'list'  => [
                    'Module Popup',
                    'Media Category' => [
                        'title'  => 'Media Page'
                    ],
                    'Landing Page',
                    'Login Page',
                    'Learner Dashboard',
                    'Single Sign-On',
                    'Survey'
                ]
            ],

            'Articles' => [
                'title' => 'Pages',
                'list'  => [
                    'Single Article' => [
                        'title' => 'Single Page'
                    ]
                ]
            ],

            'Discussion Boards' => [
                'title' => 'Forum',
                'unsetList' => [
                    'Create Topic',
                    'Custom Content',
                    'Edit User',
                    'General Statisics',
                    'User List',
                    'User Profile'
                ]
            ],

            'Blog' => [
                'title' => 'Blog',
                'unsetList' => [
                    '(Dashboard) Edit Profile',
                    '(Tags) All Tags',
                    '(Tags) Single Tags',
                    '(System) Login',
                    '(dashboard Tags'
                ]
            ],

            'Community' => [
                'title' => 'Social Community',
                'list'  => [
                    'Home' => [
                        'title' => 'Main Community Page',
                        'description' => 'Main Social Feed'
                    ]
                ],
                'unsetList' => [
                    'Appointments Day View',
                    'Appointments Week View',
                    'Appointments Month View',
                    'My Appointments',
                    'Register'
                 ]
            ],

            'Contact Pages' => [
                'title' => 'Contact Form Page',
                'list' => [
                    'Contact Page' => [
                        'title' => 'Single Contact Page',
                        'description' => 'Used as a Contact Page or Lead Generation Page'
                    ]
                ]
            ],

            'Converge' => [
                'title' => 'Subscriptions',
                'list' => [
                    'Subscription Plan Category Listing',
                    'Single Plan View'
                ]
            ],

            'Event Booking' => [
                'title' => 'Events',
                'list' => [
                    'Calendar',
                    'Calendar Weekly Layout',
                    'Category Calendar Layout',
                    'Event Detail',
                    'Event List',
                    'Group Registration Form',
                    'Individual Registration Form'
                ]
            ],

            'SP LMS' => [
                'title' => 'Education & Courses',
                'list' => [
                    'Cart',
                    'Categories',
                    'Courses',
                    'Single Course'
                ]
                ],

            'System Links' => [
                'title' => 'System Links',
                'unsetList' => ['']
            ]
        ];


        //remove menu item types if not in allowed array and rename items if title is set
        foreach ($items as $name => $list) {

            if(array_key_exists($name, $allowedMenuItems) || in_array($name, $allowedMenuItems)){

                if($list) {
                    foreach ($list as $key => $value) {
                        if(in_array($key, $allowedMenuItems[$name]['list']) || array_key_exists($key, $allowedMenuItems[$name]['list'])) {
                            if($allowedMenuItems[$name]['list'][$key]['title']) {
                                unset($list[$key]);
                                if($allowedMenuItems[$name]['list'][$key]['description']) {
                                    $value->description = $allowedMenuItems[$name]['list'][$key]['description'];
                                }
                                $list[$allowedMenuItems[$name]['list'][$key]['title']] = $value;
                            }
                        } elseif($allowedMenuItems[$name]['unsetList']) {
                            foreach($allowedMenuItems[$name]['unsetList'] as $item) {
                                unset($list[$item]);
                            }
                        } else {
                            unset($list[$key]);
                        }
                    }
                }

                if($allowedMenuItems[$name]['title']) {
                    unset($items[$name]);
                    $items[$allowedMenuItems[$name]['title']] = $list;
                } else {
                    $items[$name] = $list;
                }

            } else {
                unset($items[$name]);
            }
        }

        return $items;
    }

}
