<?php
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once("/var/www/files/creds.php");

class AxsDbAccess {

	/**
	 * Checks whether or not the client has acess to the current part of the site.  If not, they don't have it active / it isn't purchased.
	 * @param string|null 	$specific 	A specifically requested section of the site.  If null, all are returned.
	 * @return object 					An object with the access levels of the client.
	 */
	public static function getAccessLevels($specific = null) {

		//Get the cookie parameters
		$cookie_params = AxsClients::getCookieParams();

		$db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
		//If there is no cookie
		if (!isset($db_manager->client)) {

			$db = self::getDBO();

			$clientId = AxsClients::getClientId();

			if (!$clientId) {
				//There is no clientId set, which will result in a SQL error.
				return null;
			}

			$query = "SELECT * FROM axs_dbmanager WHERE id = " . $db->quote($clientId);

			$db->setQuery($query);
			$result = $db->loadObject();

			$db_manager = new stdClass();
			//Place the data where it would be in the cookie
			$db_manager->client = $result->params;
		}

		//Get the data from the cookie
		$params = json_decode($db_manager->client);

		if(!empty($params->access)) {

			if($specific && !empty($params->access->$specific)) {
				$access = $params->access->$specific;
			} else {
				$access = $params->access;
			}
		}

		if(is_null($access)) {
			return false;
		} else {
			return $access;
		}
	}


	public static function getTovutiTempates($id = null) {
		$db = self::getDBO();
		if($id) {
			$conditions[] = $db->quoteName('id') .'='. $db->quote((int)$id);
		}
		$conditions[] = $db->quoteName('template') .'= 1';
		$conditions[] = $db->quoteName('enabled') .'= 1';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_dbmanager');
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

	/**
	 * Return a true/false as to whether or not the client has access to a specific section of the site.
	 * @param 	type 	$specific 	A specifically requested section of the site.
	 * @return 	bool 			Whether or not they have access
	 */
	public static function checkAccess($specific) {
		$deprecatedFeatures = ['business_directory','eshop'];
		if(in_array($specific,$deprecatedFeatures)) {
			return false;
		}
		$access = self::getAccessLevels($specific);

		if ($access === true) {
			return true;
		} else if ($access === false) {
			return false;
		} else {
			return false;
		}
	}

	/**
	 * Creates a custom database object based on the client's credentials.  Used for accessing their personal database.
	 * @return object 		The database object
	 */
	public static function getDBO() {

		//$dbInfo = self::getDbInfo();

		$creds = dbCreds::getCreds();

		$options = array();

		$options['driver']   = 'mysqli';
		$options['host']     = $creds->dbhost;
		$options['user']     = $creds->dbuser;
		$options['password'] = $creds->dbpass;
		$options['database'] = $creds->dbname;

		$db = JDatabaseDriver::getInstance($options);

		return $db;
	}
}

?>