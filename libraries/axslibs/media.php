<?php


defined('_JEXEC') or die;

class AxsMedia {

	public static function getVimeoThumbnailFromId($id) {
		if(strpos($id,'/')) {
			$request = file_get_contents( 'https://vimeo.com/api/oembed.json?url=https://vimeo.com/' . $id );
			$video = json_decode($request);
			return str_replace('http:','https:',$video->thumbnail_url);
		} else {
			$vimeo_images = unserialize(file_get_contents("https://vimeo.com/api/v2/video/$id.php"));
			return str_replace('http:','https:',$vimeo_images[0]['thumbnail_large']);
		}
	}

	public static function getVimeoVideoIdFromUrl($url = '') {
	   $urlParts = explode('/',$url);
	   $count = count($urlParts);
	   if($count < 1) {
		   return;
	   }
	   $secondToLastIndex = $count - 2;
	   $lastIndex = $count - 1;
	   if(is_numeric($urlParts[$secondToLastIndex])) {
		   return $urlParts[$secondToLastIndex].'/'.$urlParts[$lastIndex];
	   } else {
			return $urlParts[$lastIndex];
	   }
    }

	public static function getVideoID($video_url, $video_type) {
		$url = urldecode(rawurldecode($video_url));
		switch ($video_type) {
	       		case 'youtube':
	       			preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
					if($matches[1]) {
						$video_id = $matches[1];
					} else {
						$video_id = $video_url;
					}
	       			break;

	       		case 'vimeo':
	       			$video_id = str_replace("https://vimeo.com/", '',$video_url);
	       			break;

				   case 'screencast':
					if(strpos($video_url,'embed')) {
						preg_match('/src="([^"]+)"/', $video_url, $src);
						if($src[1]) {
							$video_id = $src[1];
						} else {
							$video_id = $video_url;
						}
					} else {
						$videoArray = explode('/',$video_url);
						$lastIndex = count($videoArray) - 1;
						$vid = $videoArray[$lastIndex];
						if(strpos($vid,'?')) {
							$vid = explode('?',$vid)[0];
						}
						$video_id = 'https://screencast-o-matic.com/embed?sc='.$vid.'&v=6&ff=1&title=0&controls=1';
					}
	       			break;

	   			case 'facebook':
	   				$video_id = urlencode($video_url);
	       			break;

	       		default:
	       			preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
					if($matches[1]) {
						$video_id = $matches[1];
					} else {
						$video_id = $video_url;
					}
	       			break;
	       	}
		return $video_id;
	}

	public static function getVideoThumbnail($video_url,$video_type = 'youtube',$maxresolution = true) {
		switch ($video_type) {
       		case 'youtube':
				$url = urldecode(rawurldecode($video_url));
				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
				if($matches[1]) {
					$youtube_id = $matches[1];
				} else {
					$youtube_id = $video_url;
				}

				$image = 'https://img.youtube.com/vi/'.$youtube_id.'/maxresdefault.jpg';
				if ( (!$maxresolution) || (!getimagesize($image)) ) {
					$image = 'https://img.youtube.com/vi/'.$youtube_id.'/0.jpg';
				}
				break;
		}


		return $image;
	}

	public static function getVideo($videoParams) {
		$video        = $videoParams->video;
		$video_type   = $videoParams->video_type;
		$video_poster = $videoParams->poster;
		if(isset($videoParams->responsive)) {
			$responsive = $videoParams->responsive;
		} else {
			$responsive = true;
		}

	 	$responsive_wrapper_class = '';
	 	$responsive_item_class = '';

	 	if($responsive){
	 		$responsive_wrapper_class = 'class="plyr__video-embed"';
	 		$responsive_item_class = '';
	 	}

	 	$html = '';
	 	$optional_audio = '';
	 	$optional_audio_script = '';
	 	$transcript = '';
	 	$altTag =  '';
	 	$titleTag = '';
	 	$captions = '';
	 	$frame = '';
	 	$accessibilityTags = '';
	 	$randID = rand(1,1000);
	 	$accessibility = AxsAccessibility::video($video);
	 	$accessibilityParams   = json_decode($accessibility->params);
	 	$accessibilityCaptions = json_decode($accessibilityParams->captions);

	 	if($accessibility->alt) {
	 		$accessibilityTags .= ' alt="'.$accessibility->alt.'" ';
	 	}

	 	if($accessibility->title) {
	 		$accessibilityTags .= ' title="'.$accessibility->title.'" ';
	 	}

	 	if($accessibilityCaptions) {
	 		foreach ($accessibilityCaptions as $caption) {
	 			if($caption->caption_file) {
	 				$laguageTitle = AxsAccessibility::getCaptionTitle($caption->caption_language);
					if(!strpos($caption->caption_file,'http')) {
						$caption->caption_file = '/'.$caption->caption_file;
					}
		 			if($caption->caption_default) {
		 				$default = 'default';
		 			} else {
		 				$default = '';
		 			}
		 			$captions .= '<track
		                            kind="captions"
		                            label="'.$laguageTitle.'"
		                            srclang="'.strtolower($caption->caption_language).'"
		                            src="'.$caption->caption_file.'"
		                            '.$default.'
		                        />';
		 			}
	 		}
	 	}


	 	if(!$video_poster && $accessibilityParams->video_poster) {
	 		$video_poster = $accessibilityParams->video_poster;
	 	}

	 	$settings = json_decode(AxsLMS::getSettings()->params);
	 	$videoSettings = json_decode($settings->video);

	 	if($videoSettings->allow_downloads) {
	 		$allow_downloads = '"download",';
	 	}
	 	if($videoSettings->restart_button || !isset($videoSettings->restart_button)) {
	 		$restart_button = '"restart",';
	 	}
	 	if($videoSettings->rewind_button || !isset($videoSettings->rewind_button)) {
	 		$rewind_button = '"rewind",';
	 	}
	 	if($videoSettings->fast_forward_button || !isset($videoSettings->fast_forward_button)) {
	 		$fast_forward_button = '"fast-forward",';
	 	}
	 	if($videoSettings->progress_button || !isset($videoSettings->progress_button)) {
	 		$progress_button = '"progress",';
	 	}
	 	if($videoSettings->current_time || !isset($videoSettings->current_time)) {
	 		$current_time = '"current-time",';
	 	}
	 	if($videoSettings->duration || !isset($videoSettings->duration)) {
	 		$duration = '"duration",';
	 	}
	 	if($videoSettings->mute_button || !isset($videoSettings->mute_button)) {
	 		$mute_button = '"mute",';
	 	}
	 	if($videoSettings->volume_slider || !isset($videoSettings->volume_slider)) {
	 		$volume_slider = '"volume",';
	 	}
	 	if($videoSettings->captions || !isset($videoSettings->captions)) {
	 		$caption_setting = '"captions",';
	 	}
	 	if($videoSettings->settings || !isset($videoSettings->settings)) {
	 		$settings_button = '"settings",';
	 	}
	 	if($videoSettings->pip || !isset($videoSettings->pip)) {
	 		$pip = '"pip",';
	 	}
	 	if($videoSettings->fullscreen_option || !isset($videoSettings->fullscreen_option)) {
	 		$fullscreen_option = '"fullscreen",';
	 	}
	 	if($videoSettings->play_button || !isset($videoSettings->play_button)) {
	 		$play_button = '"play-large",';
	 	}
	 	if($videoSettings->small_play_button || !isset($videoSettings->small_play_button)) {
	 		$small_play_button = '"play",';
	 	}

	 	if(!$video_poster && $videoSettings->default_video_cover) {
	 		$video_poster = $videoSettings->default_video_cover;
	 	}

	 	$player = '<script>
	 		var controls = [
				    '.$small_play_button.'
					'.$allow_downloads.'
					'.$restart_button.'
					'.$rewind_button.'
					'.$fast_forward_button.'
					'.$progress_button.'
					'.$current_time.'
					'.$duration.'
					'.$mute_button.'
					'.$volume_slider.'
					'.$caption_setting.'
					'.$settings_button.'
					'.$pip.'
					'.$fullscreen_option.'
					'.$play_button.'
				];
	 	const player_'.$randID.' = new Plyr("#player_'.$randID.'",{ controls }); player_'.$randID.'.muted = false;</script>';

	 	if($accessibility->optional_audio || $accessibility->transcript) {
	 		$html .= '<div class="accessibility_toolbar">';

	 		if($accessibility->optional_audio) {
	 			$html .= '<button tabindex="0" id="optional_audio_'.$randID.'" class="btn accessibility_toolbar_btn" style="float:left;" alt="Use Descriptive Audio" title="Use Descriptive Audio" muted="true"><i class="fa fa-volume-up"></i> Use Descriptive Audio</button>';
	 			$optional_audio = '<audio id="audio_'.$randID.'" controls muted style="display:none;">
		                                <source src="'.$accessibility->optional_audio.'" type="audio/mpeg"/>
		                           </audio>';
		        $optional_audio_script = '<script>
		        						var audio2_'.$randID.' = document.getElementById("audio_'.$randID.'");
							            player_'.$randID.'.on("play",function() {
							            	audio2_'.$randID.'.currentTime = player_'.$randID.'.currentTime;
							            	audio2_'.$randID.'.play();
							            });
							            player_'.$randID.'.on("pause",function() {
							            	audio2_'.$randID.'.currentTime = player_'.$randID.'.currentTime;
							            	audio2_'.$randID.'.pause();
							            });
		        						$("#optional_audio_'.$randID.'").click(function() {
		        							if( $(this).attr("muted") == "true" ) {
		        								player_'.$randID.'.muted = true;
							                	audio2_'.$randID.'.muted = false;
							                	$(this).html("<i class=\"fa fa-volume-up\"></i> Use Original Audio");
							                	$(this).attr("muted","false");
		        							} else {
		        								player_'.$randID.'.muted = false;
							                	audio2_'.$randID.'.muted = true;
							                	$(this).html("<i class=\"fa fa-volume-up\"></i> Use Descriptive Audio");
							                	$(this).attr("muted","true");
		        							}
							            });
		        				   </script>';
	 		}

	 		if($accessibility->transcript) {
	 			$html .= '<a href="'.$accessibility->transcript.'" target="_blank" tabindex="0" class="btn accessibility_toolbar_btn" style="float:right;" alt="Video Transcript" title="Video Transcript"><i class="fa fa-file"></i> Video Transcript</a>';
	 		}
	 		$html .= '<div class="clearfix"></div>';
	 		$html .= '</div>';
	 	}
       	switch ($video_type) {
       		case 'youtube':
       			$video_id = self::getVideoID($video,$video_type);
       			$html .= '<div '.$responsive_wrapper_class.' id="player_'.$randID.'" '.$accessibilityTags.' class="video_tracking" data-video=\''.base64_encode(json_encode($videoParams)).'\'>
                    <iframe style="width:100%;" '.$responsive_item_class.' frameborder="none" scrolling="no" src="https://www.youtube.com/embed/'.$video_id.'?iv_load_policy=3&amp;modestbranding=1&amp;playsinline=1&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1" seamless="seamless" allowfullscreen="allowfullscreen">
                    </iframe>
                    </div>';
       			break;

       		case 'vimeo':
       			$video_id = self::getVideoID($video,$video_type);
       			$html .= '<div '.$responsive_wrapper_class.' id="player_'.$randID.'" '.$accessibilityTags.' class="video_tracking" data-video=\''.base64_encode(json_encode($videoParams)).'\'>
                    <iframe '.$responsive_item_class.' src="https://player.vimeo.com/video/'.$video_id.'?loop=false&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=0&amp;gesture=media" style="width:100%" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    </div>';
       			break;

       		case 'screencast':
       			$video_id = self::getVideoID($video,$video_type);
       			$html .= '<div '.$responsive_wrapper_class.' >
                    <iframe style="width:100%;" '.$responsive_item_class.' name="tsc_player" frameborder="0" scrolling="no" type="text/html"  src="'.$video_id.'" seamless="seamless" allowfullscreen="allowfullscreen">
                    </iframe>
                    </div>';
                $player = '';
       			break;

   			case 'facebook':
   				$video_id = self::getVideoID($video,$video_type);
       			$html .= '<div '.$responsive_wrapper_class.' >
                    <iframe src="https://www.facebook.com/plugins/video.php?href='.$video_id.'&show_text=0" '.$responsive_item_class.'  style="border:none;overflow:hidden;width:100%" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                    </div>';
                $player = '';
       			break;

   			case 'mp4':
   				$base = JURI::base();
   				if(!strpos($video, 'http')) {
   					$video = $base.'/'.$video;
   				}
   				if(!strpos($video_poster, 'http')) {
   					$video_poster = $base.'/'.$video_poster;
   				}

       			$html .= '
                    <video '.$accessibilityTags.' controls crossorigin playsinline preload="auto"';

                //$html .= ` data-plyr-config="{ 'download': '`.$video.$frame.`"' }" `;
                if($video_poster) {
                    $html .= 'poster="'.$video_poster.'"';
                } else {
                	$frame = '#t=2';
                }
                $html .= '  id="player_'.$randID.'" class="video_tracking" data-video=\''.base64_encode(json_encode($videoParams)).'\'>
                		<source
                            src="'.$video.$frame.'"
                            type="video/mp4"
                    	/>
                    	'.$captions.'
                    </video>';
       			break;

       		case 'mp3':
       			$html .= '<audio preload="auto" style="width:100%"  controls id="player_'.$randID.'">
							<source src="'.$video.'" type="audio/mp3" />
       					  </audio>';
       			break;

       		default:
       			$html .= '
                    <video crossorigin playsinline preload="auto"  src="'.$video.'"';
                if($video_poster) {
                    $html .= 'poster="'.$video_poster.'"';
                }
                $html .= ' controls id="player_'.$randID.'">
                    </video>';
       			break;
		   }
		   	$html .= $player.$optional_audio.$optional_audio_script."\n";
            if(!$videoParams->activitySubmission) {

				$html .= '<script>
								var videoScrollPosition;

								var player_'.$randID.'_Tracking;
								player_'.$randID.'.on("play",function() {
									player_'.$randID.'_Tracking = setInterval(function(){
									trackVideo('.$randID.',player_'.$randID.');
									},5000);
								});
								player_'.$randID.'.on("pause",function() {
									var currentTime =  player_'.$randID.'.currentTime;
									clearInterval(player_'.$randID.'_Tracking);
									trackVideo('.$randID.',player_'.$randID.');
								});
								player_'.$randID.'.on("stop",function() {
									var currentTime =  player_'.$randID.'.currentTime;
									clearInterval(player_'.$randID.'_Tracking);
									trackVideo('.$randID.',player_'.$randID.');
								});
								$(window).scroll(function() {
									if(window.scrollY > 0) {
										videoScrollPosition = window.scrollY;
									}
								});
								player_'.$randID.'.on("exitfullscreen",function() {
									window.scrollTo(0,videoScrollPosition);
								});

						</script>';
			}
            return $html;

        }

}