<?php

defined('_JEXEC') or die;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

require_once(JPATH_ADMINISTRATOR . "/components/com_award/helpers/helper.php");

class AxsLMS {

    // Request-level cache
    static $loaded = array();

    private static $publishedLanguages = null;

    const ISO_TIMESTAMP_FORMAT = "Y-m-d H:i:s";

	public static function newActivity($params) {
		return new Activity($params);
	}

	public static function newActivityList($params) {
		return new ActivityList($params);
	}

    public static function newStudentList($params) {
        return new StudentList($params);
    }

    public static function updateBadges($badge_id = null, $user_id = null, $db = null, $issue_date = null) {
        BadgeHelper::updateBadges($badge_id, $user_id, $db, $issue_date);
    }

    public static function getUrl(){
        $protocol = "https://";
        return $protocol.$_SERVER['SERVER_NAME'];
    }

    public static function truncate($string,$length) {
    	if(strlen($string) > $length) {
    		$string = substr($string,0,$length)."...";
    	}
    	return $string;
    }

    public static function formatDate($date) {
        if($date) {
            if(date('y',strtotime($date)) == '69') {
                return;
            }
            return date('m/d/y',strtotime($date));
        } else {
            return;
        }
    }

    public static function randomString($length = 6) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'), range('0','9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }

    public static function createUniqueID() {
        $timestamp = strtotime('now');
        $uniqueID = $timestamp.self::randomString();
        return $uniqueID;
    }

    public static function createPassword() {
        $password = self::randomString(10);
        return $password;
    }

    public static function checkComplete($params) {
        if(!$params) {
            return false;
        }
        if($params->user_id) {
            if(AxsLMS::checkIfRequirementCompleted($params) == "completed") {
                $params->status = "completed";
                AxsLMS::insertLessonStatus($params);
            }
        }
    }

    public static function getUserAssignedData($user_id, $course_id = null, $assignment_id = null) {
        if(!$user_id) {
            return false;
        }
        //$groups = JAccess::getGroupsByUser($user_id); //gets all user groups
        $groups = JFactory::getUser($user_id)->groups; //gets exact user groups
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions[] = "type = 0";
        if($course_id) {
            $course_id = (int)$course_id;
            $conditions[] = "FIND_IN_SET($course_id, course_ids) > 0";
        }
        if($assignment_id) {
            $assignment_id = (int)$assignment_id;
            $conditions[] = "id = $assignment_id";
        }
        foreach($groups as $group) {
			$accessCheck[] = "FIND_IN_SET( $group, usergroups ) > 0";
        }
        $accessCheck[] = "FIND_IN_SET($user_id, user_ids) > 0";
        if($accessCheck) {
            $accessConditions = implode(' OR ',$accessCheck);
            $conditions[] = "( $accessConditions )";
        }
        $query->select('*');
        $query->from('#__splms_courses_groups');
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        if(!$results) {
            return false;
        }
        if(count($results) > 1) {
            foreach($results as $assignment) {
                if($assignment->free) {
                    return $assignment;
                }
            }
        } else {
            return $results[0];
        }
    }

    public static function getVideoTrackingSession($data) {
        $conditions = array('archive_date IS NULL');
        if($data->video_path) {
            $conditions[] = "video_path = '$data->video_path'";
        }
        if($data->user_id) {
            $conditions[] = "user_id = $data->user_id";
        }
        if($data->activity_id) {
            $conditions[] = "activity_id = '$data->activity_id'";
        }
        if($data->lesson_id) {
            $conditions[] = "lesson_id = $data->lesson_id";
        }
        if($data->course_id) {
            $conditions[] = "course_id = $data->course_id";
        }
        if($data->media_id) {
            $conditions[] = "media_id = $data->media_id";
        }
        if($data->date) {
            $conditions[] = "DATE(date) = DATE('$data->date')";
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_video_tracking')
              ->where($conditions)
              ->setLimit(1);
        $db->setQuery($query);
        $row = $db->loadObject();

        return $row;
    }

    public static function getActivityList($lesson) {
        if(!$lesson) {
            return false;
        }
        $conditions[] = "splms_lesson_id = $lesson";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lessons');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        $params = json_decode($result->params);
        $activites = json_decode($params->student_activities);
        return $activites;
    }

    public static function getActivityById($id) {
        if(!$id) {
            return false;
        }
        $conditions = "archive_date IS NULL AND id = $id";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getUserActivityByStudentResponse($params,$student_response) {
        $conditions[] = "course_id = $params->course_id";
        $conditions[] = "lesson_id = $params->lesson_id";
        $conditions[] = "user_id   = $params->user_id";
        $conditions[] = "activity_id   = '$params->activity_id'";
        $conditions[] = "student_response = '$student_response'";
        $conditions[] = "archive_date IS NULL";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getUserActivityByInteractiveId($params,$interactive_id) {
        $conditions[] = "course_id = $params->course_id";
        $conditions[] = "lesson_id = $params->lesson_id";
        $conditions[] = "user_id   = $params->user_id";
        $conditions[] = "student_response = '$interactive_id'";
        $conditions[] = "archive_date IS NULL";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result->completed;
    }

    public static function getUserActivityByActivityId($params,$activity_id) {
        $conditions[] = "course_id = $params->course_id";
        $conditions[] = "lesson_id = $params->lesson_id";
        $conditions[] = "user_id   = $params->user_id";
        $conditions[] = "activity_id = '$activity_id'";
        $conditions[] = "archive_date IS NULL";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where($conditions);
        $query->order('completed DESC');
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result->completed;
    }

    public static function getStudentActivityByActivityId($user_id,$activity_id) {
        $db = JFactory::getDbo();
        $conditions[] = "user_id = " . $db->quote($user_id);
        $conditions[] = "activity_id = " . $db->quote($activity_id);
        $conditions[] = "archive_date IS NULL";
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where($conditions);
        $query->order('completed DESC');
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function checkLessonCompletion($params) {
        $conditions[] = "course_id  = $params->course_id";
        $conditions[] = "lesson_id  = $params->lesson_id";
        $conditions[] = "user_id    = $params->user_id";
        $conditions[] = "language   = '$params->language'";
        $conditions[] = "status     = 'completed'";
        $conditions[] = "archive_date IS NULL";

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lesson_status');
        $query->where($conditions);
        $query->setLimit(1);

        $db->setQuery($query);
        $result = $db->loadObject();

        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function getLessonStatus($params) {
        $db = JFactory::getDbo();
        $conditions[] = "course_id = $params->course_id";
        $conditions[] = "lesson_id = $params->lesson_id";
        $conditions[] = "user_id   = $params->user_id";
        if($params->check_lesson_status) {
            $conditions[] = "status   = ".$db->quote($params->check_lesson_status);
        }
        if($params->check_lesson_language) {
            $conditions[] = "language   = ".$db->quote($params->check_lesson_language);
        }
        $conditions[] = "archive_date IS NULL";
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lesson_status');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getLessonQuiz($lesson,$course) {
        if(!$lesson) {
            return false;
        }
        $conditions[] = "splms_lesson_id = $lesson";
        $conditions[] = "splms_course_id = $course";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_quizquestions');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getInteractiveLibrary($interactive_id) {
        $conditions[] = "c.id = $interactive_id";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('c.*,l.name as content_type')
              ->from('ic_h5p_contents as c')
              ->join('LEFT','ic_h5p_libraries as l ON c.library_id = l.id')
              ->where($conditions)
              ->setLimit(1);
        $db->setQuery($query);
        $contentData = $db->loadObject();
        return $contentData->content_type;
    }

    public static function getInteractiveContentParameters($interactive_id) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('c.parameters')
              ->from('ic_h5p_contents as c')
              ->where('c.id = ' . $interactive_id)
              ->limit(1);

        $db->setQuery($query);

        $contentData = $db->loadObject();
        $params = json_decode($contentData->parameters);

        return $params;
    }

    /**
     * Archives interactive content when a new version of an existing piece of content is
     * created (when updating an existing Interactive Content item)
     *
     * @param mixed $contentId IC id
     * @return void
     */
    public static function archiveInteractiveContent($contentId) {

        $db = JFactory::getDbo();

        // Archive contents
		$timestamp = date('Y-m-d H:i:s');

		// Get previous version (if any)
		$query = $db->getQuery(true);

		$query->select('*')
			  ->from('ic_h5p_contents_archive')
			  ->where('content_id = ' . $db->quote($contentId));
        
		$db->setQuery($query);

		$versions = $db->loadObjectList();

        $version_number = array_column($versions, 'version_number');
        array_multisort($version_number, SORT_DESC, $versions);

		// Get the existing content data for the given contentId
        $query = $db->getQuery(true);

        $query->select('*')
              ->from('ic_h5p_contents')
              ->where('id = ' . $db->quote($contentId));

        $db->setQuery($query);

        $content = $db->loadObject();

		$lastVersionNumber = 0;

		if(!empty($versions)) {

			$lastVersionNumber = $versions[0]->version_number;
		}

        $nextVersionNumber = $lastVersionNumber + 1;

        $content->version_number = $nextVersionNumber;
        $content->archived_at = $timestamp;
        $content->content_id = $contentId;

        // We want the entire data row except for the id
        unset($content->id);

		try {

            $result = $db->insertObject('ic_h5p_contents_archive', $content);

			if(!$result) {

				/* TODO: decide whether or not we want to log exceptions or display them
				* to the user via the Joomla messaging system.
				*/
			}
		} catch(Exception $ex) {

			/* TODO: decide whether or not we want to log exceptions or display them
			* to the user via the Joomla messaging system.
			*/
		}
    }

    /**
     * Archive interactive content results
     *
     * @param mixed $contentId IC id
     * @return void
     */
    public static function archiveInteractiveContentResults($contentId) {

        $db = JFactory::getDbo();

        // Get the IC results that will be deleted
        $query = $db->getQuery(true);

        $query->select('*')
                ->from('ic_h5p_results')
                ->where('content_id = ' . $contentId);

        $db->setQuery($query);

        $results = $db->loadObjectList();

        // Archive results data
        foreach($results as $result) {

            // Get existing archive data (if any) and if it exists, increase the version number and store new data
            $query = $db->getQuery(true);

            $query->select('*')
			  ->from('ic_h5p_results_archive')
			  ->where('content_id = ' . $db->quote($contentId))
              ->andWhere('user_id = ' . $result->user_id);

            $db->setQuery($query);

            $versions = $db->loadObjectList();

            $version_number = array_column($versions, 'version_number');
            array_multisort($version_number, SORT_DESC, $versions);

            $lastVersionNumber = 0;

            if(!empty($versions)) {

                $lastVersionNumber = $versions[0]->version_number;
            }

            $nextVersionNumber = $lastVersionNumber + 1;

            // Don't need the result ID
            unset($result->id);
            $archivedResultObject = $result;

            $archivedResultObject->version_number = $nextVersionNumber;
            $archivedResultObject->archived_at = date('Y-m-d H:i:s');

            try {

                $status = $db->insertObject('ic_h5p_results_archive', $archivedResultObject);

                if(!$status) {

                    // TODO: Log problems with inserting the archived object
                }
            } catch(Exception $ex) {

                // TODO: Catch any exceptions and log them
            }
        }
    }

    /**
     * Archive interactive content user data
     *
     * @param mixed $contentId IC id
     * @return void
     */
    public static function archiveInteractiveContentUserData($contentId) {

        $db = JFactory::getDbo();

        // Grab the IC User Data
        $query = $db->getQuery(true);

        $query->select('*')
                ->from('ic_h5p_contents_user_data')
                ->where('content_id = ' . $contentId);

        $db->setQuery($query);

        $results = $db->loadObjectList();

        // Archive user data
        foreach($results as $result) {

            // Get existing archive data (if any) and if it exists, increase the version number and store new data
            $query = $db->getQuery(true);

            $query->select('*')
			  ->from('ic_h5p_contents_user_data_archive')
			  ->where('content_id = ' . $db->quote($contentId))
              ->andWhere('user_id = ' . $result->user_id);

            $db->setQuery($query);

            $versions = $db->loadObjectList();

            $version_number = array_column($versions, 'version_number');
            array_multisort($version_number, SORT_DESC, $versions);

            $lastVersionNumber = 0;

            if(!empty($versions)) {

                $lastVersionNumber = $versions[0]->version_number;
            }

            $nextVersionNumber = $lastVersionNumber + 1;

            // Don't need the result ID
            unset($result->id);
            $archivedResultObject = $result;

            $archivedResultObject->version_number = $nextVersionNumber;
            $archivedResultObject->archived_at = date('Y-m-d H:i:s');

            try {
                $status = $db->insertObject('ic_h5p_contents_user_data_archive', $archivedResultObject);

                if(!$status) {

                    // TODO: Log problems with inserting the archived object
                }
            } catch(Exception $ex) {

                // TODO: Catch any exceptions and log them
            }
        }
    }

    public static function checkIfRequirementCompleted($params) {
        $lesson = self::getLessonById($params->lesson_id);
        $lesson_params = json_decode($lesson->params);
        $status = "completed";

        if($lesson_params->use_library_content && $lesson_params->scorm_required && ($lesson_params->bizlibrary_content_id || $lesson_params->scorm_id) ) {
            switch($lesson_params->library_content_type) {
                case 'scorm':
                    $params->scorm_content_id = $lesson_params->scorm_id;
                break;
                case 'bizlibrary':
                    $params->scorm_content_id = $lesson_params->bizlibrary_content_id;
                break;
            }

            $learner_data = AxsScormFactory::getLearnerScormData($params);
			$scormData   = json_decode($learner_data->data);
			if($scormData->completion_status == 'completed' || $scormData->core->lesson_status == 'passed' || $scormData->completion_status == 'passed' || $scormData->core->lesson_status == 'completed') {
				$status = 'completed';
			} else {
                $status = 'incomplete';
            }
        }

        $activityList = self::getActivityList($params->lesson_id);

        if($activityList) {
            foreach($activityList as $activity) {
                $content_type = false;

                if($activity->type == "interactive" && $activity->required && $activity->interactive_id) {
                    $content_type = self::getInteractiveLibrary((int)$activity->interactive_id);
                    $student_response = $activity->interactive_id;
                    if($content_type && self::canBeRequired($content_type)) {
                        if(!self::getUserActivityByInteractiveId($params,$student_response)) {
                            return "incomplete";
                        }
                    }
                } elseif($activity->type == "video" && $activity->required && $activity->video_type) {
                    $content_type = $activity->video_type;
                    if($content_type && self::canBeRequired($content_type)) {
                        if(!self::getUserActivityByActivityId($params,$activity->id)) {
                            return "incomplete";
                        }
                    }
                } elseif( $activity->type == 'esignature') {
                    if(!self::getUserActivityByActivityId($params,$activity->id)) {
                        return "incomplete";
                    }
                } elseif($activity->required) {
                    if(self::canBeRequired("submitted_".$activity->type)) {
                        if(!self::getUserActivityByActivityId($params,$activity->id)) {
                            return "incomplete";
                        }
                    }
                }
            }
        }

        $requiredQuiz = self::getLessonQuiz($params->lesson_id,$params->course_id);
        if($requiredQuiz) {
            $quizParams = json_decode($requiredQuiz->params);
            if($quizParams->mode == "quiz" && $quizParams->required_score) {
                $highScore = self::getCourseLessonTotals($params->course_id,$params->lesson_id,$params->user_id);
                if(!$highScore) {
                    return "started";
                }
                if((int)$highScore < (int)$quizParams->required_score) {
                    return "failed";
                }
            }
        }
        //all requirements met -- good job!
        return $status;
    }

    public static function canBeRequired($content_type) {
        $canBeRequiredArray = [
            "H5P.Questionnaire",
            "H5P.SpeakTheWordsSet",
            "H5P.QuestionSet",
            "H5P.Essay",
            "H5P.InteractiveVideo",
            "H5P.ArithmeticQuiz",
            "H5P.Column",
            "H5P.CoursePresentation",
            "H5P.DragText",
            "H5P.Blanks",
            "H5P.ImageHotspotQuestion",
            "H5P.MarkTheWords",
            "H5P.MemoryGame",
            "H5P.MultiChoice",
            "H5P.SingleChoiceSet",
            "H5P.Summary",
            "H5P.TrueFalse",
            "H5P.DragQuestion",
            "H5P.ImageMultipleHotspotQuestion",
            "H5P.SpeakTheWords",
            "H5P.ImageSequencing",
            "H5P.Flashcards",
            "H5P.ImagePair",
            "H5P.Dictation",
            "H5P.BranchingScenario",
            'H5P.FindTheWords',
            "mp4",
            "youtube",
            "vimeo",
            "submitted_textbox",
            "submitted_upload",
            "submitted_mp4",
            "submitted_mp3",
            "submitted_youtube",
            "submitted_vimeo",
            "submitted_facebook",
            "submitted_screencast",
            "submitted_link",
            "submitted_survey"
        ];

        return in_array($content_type, $canBeRequiredArray);
    }

    public static function canBeScored($content_type) {
        $canBeScoredArray = [
            "H5P.SpeakTheWordsSet",
            "H5P.QuestionSet",
            "H5P.InteractiveVideo",
            "H5P.ArithmeticQuiz",
            "H5P.Column",
            "H5P.CoursePresentation",
            "H5P.DragText",
            "H5P.Blanks",
            "H5P.ImageHotspotQuestion",
            "H5P.MarkTheWords",
            "H5P.MultiChoice",
            "H5P.SingleChoiceSet",
            "H5P.Summary",
            "H5P.TrueFalse",
            "H5P.DragQuestion",
            "H5P.ImageMultipleHotspotQuestion",
            "H5P.SpeakTheWords",
            "H5P.ImageSequencing",
            "H5P.Flashcards",
            "H5P.ImagePair",
            "H5P.Dictation",
            "H5P.BranchingScenario",
            'H5P.FindTheWords'
        ];

        return in_array($content_type, $canBeScoredArray);
    }

    public static function getLessonById($lesson) {
        if(!$lesson) {
            return false;
        }
        $conditions[] = "splms_lesson_id = $lesson";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lessons');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getCourseById($course) {
        if(!$course) {
            return false;
        }
        $conditions[] = "splms_course_id = $course";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_courses');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getCourseCategoryById($id) {
        if(!$id) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions[] = "splms_coursescategory_id = ".$db->quote($id);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_coursescategories');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getCertificateById($certificate_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards_certificates'))
              ->where($db->qn('id').'='.(int)$certificate_id)
              ->setLimit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();
        return $certificate;
    }

    public static function getAwardById($award_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards'))
              ->where($db->qn('id').'='.(int)$award_id)
              ->setLimit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();
        return $certificate;
    }

    public static function getCourseListProgress($course_ids,$user_id) {
        if(!$course_ids || !$user_id) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('title,splms_course_id as course_id')
              ->from('#__splms_courses')
              ->where("splms_course_id IN ($course_ids)");
        $db->setQuery($query);
        $result = $db->loadObjectList();
        $courseList = array();
        foreach($result as $row) {
            $progress = self::getMaxCourseProgress((int)$row->course_id,$user_id);
            $courseList[$row->title] = $progress->progress;
        }
        $progress = new stdClass();
        $progress->courseList = $courseList;
        return $progress;
    }

    public static function getAssignmentById($id) {
        if(!$id) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__splms_courses_groups')
              ->where("id = ".(int)$id)
              ->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getExistingCourseCount($course_ids) {
        // Sometimes a course is deleted after it gets added to an assignment
        // This function allows us to get the number of courses in an assignment that still exist
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('splms_course_id');
        $query->from('joom_splms_courses');
        $query->where("splms_course_id IN ($course_ids)");

        $db->setQuery($query);
        $result = $db->loadObjectList();

        return count($result);
    }


    public static function getAssignmentProgress($course_ids,$user_id) {
        if(!$course_ids || !$user_id) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*,MAX(date_completed) as completed_date,MAX(progress) as max_progress, MAX(date_last_activity) as last_activity_date')
              ->from('#__splms_course_progress')
              ->where("archive_date IS NULL AND course_id IN ($course_ids) AND user_id = $user_id")
              ->group('course_id,user_id');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        $totalCompleted = array();
        $last_activity_date = 0;
        $completed_date = 0;
        foreach($result as $row) {
            array_push($totalCompleted,$row->max_progress);
            if(strtotime($row->last_activity_date) > strtotime($last_activity_date)) {
                $last_activity_date = $row->last_activity_date;
            }
            if(strtotime($row->completed_date) > strtotime($completed_date)) {
                $completed_date = $row->completed_date;
            }
        }

        $totalCourses = self::getExistingCourseCount($course_ids) * 100;
        if ($totalCourses > 0) {
            $percentageComplete = round( (array_sum($totalCompleted) / $totalCourses) * 100 );
        } else {
            $percentageComplete = 0;
        }

        $progress = new stdClass();

        if($percentageComplete < 1) {
            $percentageComplete = 0;
        }

        $progress->percentageComplete = $percentageComplete;
        $progress->last_activity_date = $last_activity_date;
        if($percentageComplete == 100) {
            $progress->completed_date = $completed_date;
        } else {
            $progress->completed_date = '';
        }
        return $progress;
    }

    public static function getUserCoursesInProgress($user_id,$ids_only = false) {
        if(!$user_id) {
            return false;
        }
        $course_ids = array();
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array('archive_date IS NULL');

        $conditions[] = $db->quoteName('user_id')   . "=" . $db->quote($user_id);

        $query
            ->select('*')
            ->from($db->quoteName('#__splms_course_progress'))
            ->where($conditions)
            ->group('course_id')
            ->order('progress DESC');

        $db->setQuery($query);
        $results = $db->loadObjectList();

        if($ids_only) {
            if($results) {
                foreach($results as $course) {
                    array_push($course_ids,$course->course_id);
                }
            }
            return $course_ids;
        } else {
            return $results;
        }

    }


    public function getUserAssignedRecommendedCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
        $courseAssignedIDs = array();
        $courseRecommendedIDs = array();
		//get courses that are recommended or assigned by usergroup
		$query = trim("
            SELECT a.*
            FROM `joom_splms_courses_groups` a
            WHERE FIND_IN_SET($user_id, a.user_ids)
        ");
        $usergroups = JUserHelper::getUserGroups($user_id);
        foreach ($usergroups as $usergroup) {
            $query .= "OR FIND_IN_SET($usergroup, a.usergroups) ";
        }
		$db->setQuery($query);
		$course_groups = $db->loadObjectList();

		foreach($course_groups as $cg) {
            if($cg->user_ids) {
                $userArray = explode(',',$cg->user_ids);
                if(!in_array($user_id,$userArray)) {
                    continue;
                }
            }
			$course_ids = explode(',', $cg->course_ids);
			foreach($course_ids as $cid) {
                if($cg->type == 1) {
                    array_push($courseRecommendedIDs, $cid);
                } else {
                    array_push($courseAssignedIDs, $cid);
                }

			}
        }

        $courseAssignedIDs = array_unique($courseAssignedIDs);
        $courseRecommendedIDs = array_unique($courseRecommendedIDs);
        $data = new stdClass();
        $data->assigned = $courseAssignedIDs;
        $data->recommended = $courseRecommendedIDs;
		return $data;
    }

    public function getUserPurchasedCourses($user_id) {
		$db = JFactory::getDbo();
		if(!$user_id){
			$user_id = JFactory::getUser()->id;
		}
		$query = "SELECT * FROM axs_course_purchases WHERE status = 'PAID' AND user_id = '$user_id'";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$courseIDs = array();
		foreach ($results as $id) {
			array_push($courseIDs, $id->course_id);
		}
		$result = array_unique($courseIDs);
		return $result;
	}

    public static function getUserCourses($user_id) {
        $courseIDs = array();
        $inProgress = self::getUserCoursesInProgress($user_id,true);
		//$wishlisted = $courses_model->getUserWishlistCourses($user_id);
		$purchased = self::getUserPurchasedCourses($user_id);
        $assignedAndRecommended = self::getUserAssignedRecommendedCourses($user_id);
        $assigned = $assignedAndRecommended->assigned;
        $recommended = $assignedAndRecommended->recommended;
		/* if($wishlisted){
			$courseIDs = array_merge($courseIDs, $wishlisted);
		} */

		if($purchased){
			$courseIDs = array_merge($courseIDs, $purchased);
		}

		if($assigned){
			$courseIDs = array_merge($courseIDs, $assigned);
        }

        if($recommended){
			$courseIDs = array_merge($courseIDs, $recommended);
        }

        if($inProgress){
			$courseIDs = array_merge($courseIDs, $inProgress);
		}
        $courseIDs = array_filter($courseIDs);
		$courses = array_unique($courseIDs);
        $courseList = implode(",", $courses);

        if(!$courseList) {
            return false;
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();
        $conditions[] = $db->quoteName('splms_course_id')   . " IN ($courseList)";
        $conditions[] = $db->quoteName('enabled').'='.(int)1;
        $query
            ->select('*')
            ->from($db->quoteName('#__splms_courses'))
            ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();

        foreach($results as $row) {
            $courseProgress = self::getCourseProgress((int)$row->splms_course_id,$user_id);
            $row->date_started = $courseProgress->date_started;
            $row->date_completed = $courseProgress->date_completed;
            $row->percentComplete = round($courseProgress->progress);
            if($row->percentComplete == 100) {
                $row->status = 'Completed';
            } else {
                $row->status = 'In Progress';
            }
            $row->score  = self::getCourseTotalScore((int)$row->splms_course_id,$user_id);
            if(in_array($row->splms_course_id,$assigned)) {
                $row->assigned = true;
            } elseif(in_array($row->splms_course_id,$recommended)) {
                $row->recommended = true;
            }
        }
        return $results;
    }

    public static function getEncryptedTranscriptLink($user_id) {
        $baseURL = JURI::base();
        $baseURL = str_replace('administrator/', '', $baseURL);
        $params = new stdClass();
        $params->user_id = $user_id;
        $key = AxsKeys::getKey('lms');
        $encryptedParams = base64_encode(AxsEncryption::encrypt($params, $key));
        $lang_tag = explode('-', JFactory::getLanguage()->getTag())[0];
        return $baseURL.$lang_tag.'/transcript?params='.$encryptedParams;
    }

    public static function getEncryptedLearnerDashboardLink($user_id, $learner_dashboard_id = 1) {
        $baseURL = JURI::base();
        $baseURL = str_replace('administrator/', '', $baseURL);
        $params = new stdClass();
        $params->user_id  = $user_id;
        $params->is_admin =  true;
        $key = AxsKeys::getKey('lms');
        $encryptedParams = base64_encode(AxsEncryption::encrypt($params, $key));
        return substr($baseURL, 0, strlen($baseURL) - 1).JRoute::_('/index.php?option=com_axs&view=learner_dashboard&id=' . $learner_dashboard_id).'&params='.$encryptedParams;
    }

    public static function getCourseTotalScore($course_id,$user_id) {

        $language = AxsLanguage::getCurrentLanguage()->get('tag');

        $course = new stdClass();
        $course->language = $language;
        $course->course_id = $course_id;
        $course->id_list = true;
        //Get lessons
        $lessons = self::getLessonList($course);
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('user_id').'='.$db->quote($user_id);
        $conditions[] = $db->quoteName('splms_course_id').'='.$db->quote($course_id);
        $conditions[] = "FIND_IN_SET(".$db->quoteName('splms_lesson_id').",".$db->quote($lessons).")";
        $conditions[] = $db->quoteName('user_id').'='.$db->quote($user_id);
        $conditions[] = 'archive_date IS NULL';
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from("#__splms_quizresults");
        $query->where($conditions);
        $query->group('splms_lesson_id');
        $db->setQuery($query);
        $results = $db->loadObjectList();

        $topQuizScore = 0;
        $totals = array();
        foreach ($results as $row) {
            if($row->splms_lesson_id) {
                $quiz = self::getQuiz($row->splms_quizquestion_id);
                $quizParams = json_decode($quiz->params);
                if($quizParams->mode != 'survey') {
                    $topQuizScore = self::getCourseLessonTotals($course_id,$row->splms_lesson_id,$user_id);
                    array_push($totals,$topQuizScore);
                }
            }
        }

        $scoreTotal = is_countable($totals) ? count($totals) : 0;

        $percent = $scoreTotal  > 0 ? round( ( array_sum($totals) / $scoreTotal  ) ) : 0;
        return $percent;
    }

    public static function getCourseLessonTotals($course_id,$lesson_id,$user_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from("#__splms_quizresults");
        $query->where('user_id='.$user_id);
        $query->where('splms_course_id='.$course_id);
        $query->where('splms_lesson_id='.$lesson_id);
        $query->where('archive_date IS NULL');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $highestScore = 0;
        foreach ($results as $row) {
            $quizScore = $row->point / $row->total_marks;
            if ($quizScore > $highestScore) {
                $highestScore = $quizScore;
            }
        }
        $percent = round($highestScore * 100);
        return $percent;
    }

    public static function getSettings() {
        $sig = md5(serialize('getSettings'));

        if (isset(static::$loaded[__METHOD__][$sig])) {
            return static::$loaded[__METHOD__][$sig];
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from("axs_lms_settings");
        $query->where('id=1');
        $query->setLimit(1);
        $db->setQuery($query);
        $data = $db->loadObject();

        static::$loaded[__METHOD__][$sig] = $data;

        return $data;
    }

    public static function getUserCertificate($award_id,$user_id,$certificate_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards_certificates'))
              ->where($db->qn('id').'='.(int)$certificate_id)
              ->setLimit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();

        return $certificate;
    }

    public static function getCertificate($certificate_id) {

        return self::getUserCertificate(null, null, $certificate_id);
    }

    /**
     * Gets All Certificates
     * Use getAllCertificates(true) to get records that can be used with Zapier - newest first.
     *
     * @param boolean $newestFirst Set this to true to grab all certificates from newest to oldest.
     * @return array Set of certificate template records, if any
     */
    public static function getAllCertificates($newestFirst = false) {

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query->select('*')
              ->from($db->qn('axs_awards_certificates'));

        if($newestFirst) {
            $query->order($db->qn('id') . ' DESC');
        }

        $db->setQuery($query);

        $certificates = $db->loadObjectList();

        return $certificates;
    }

    public static function getUserAward($award_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards'))
              ->where($db->qn('id').'='.(int)$award_id)
              ->setLimit(1);
        $db->setQuery($query);
        $award = $db->loadObject();

        return $award;
    }

    public static function getUserAwardEarned($award_id,$user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards_earned'))
              ->where($db->qn('badge_id').'='.(int)$award_id)
              ->where($db->qn('user_id').'='.(int)$user_id)
              ->setLimit(1);
        $db->setQuery($query);
        $awardEarned = $db->loadObject();

        return $awardEarned;
    }

    public static function getMaxCourseProgress($id, $user) {
        if(!$user) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array('archive_date IS NULL');


        $conditions[] = $db->quoteName('user_id')   . "=" . $db->quote($user);


        if ($id) {
            $conditions[] = $db->quoteName('course_id') . "=" . $db->quote($id);
        }

        $query
            ->select('*')
            ->from($db->quoteName('#__splms_course_progress'))
            ->where($conditions)
            ->order('progress DESC')
            ->setLimit(1);

        $db->setQuery($query);
        $results = $db->loadObject();

        return $results;
    }

    public static function getCourseProgress($id, $user, $lang = null) {
        if(!$user) {
            return false;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array('archive_date IS NULL');


        $conditions[] = $db->quoteName('user_id')   . "=" . $db->quote($user);


        if ($id) {
            $conditions[] = $db->quoteName('course_id') . "=" . $db->quote($id);
        }

        if ($lang) {
            $conditions[] = $db->quoteName('language')  . "=" . $db->quote($lang);
        } else {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
            $conditions[] = $db->quoteName('language')  . " = '$language' ";
        }

        $query
            ->select('*')
            ->from($db->quoteName('#__splms_course_progress'))
            ->where($conditions)
            ->order('progress DESC')
            ->setLimit(1);

        $db->setQuery($query);
        $results = $db->loadObject();

        return $results;
    }

    public static function setCourseCompleted($userId, $courseId, $startDate = null, $completeDate = null) {

        if(!$userId && !$courseId) {

            return false;
        }

        $courses = self::courseProgress_getCourseLessons($courseId);
        $languages = $courses[$courseId];

        if (count($courses) == 0) {

            //This course does not exist.
            return false;
        }

        $courseStartDate = empty($startDate) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($startDate));
        $courseCompleteDate = empty($completeDate) ? date('Y-m-d H:i:s') : date('Y-m-d H:i:s', strtotime($completeDate));

        //Check to see if it is already in the database, just in case.
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array('archive_date IS NULL');
        $conditions[] = $db->quoteName('user_id')   . "=" . $db->quote($userId);
        $conditions[] = $db->quoteName('course_id') . "=" . $db->quote($courseId);

        $query
            ->select('*')
            ->from($db->quoteName('#__splms_course_progress'))
            ->where($conditions);

        $db->setQuery($query);
        $results = $db->loadObjectList();

        if(!empty($results)) {

            foreach($results as $courseProgressData) {

                if(empty($courseProgressData->date_started) && empty($courseProgressData->date_completed)) {

                    $courseProgressData->date_started = $courseStartDate;
                    $courseProgressData->date_completed = $courseCompleteDate;
                }

                if(empty($courseProgressData->date_started) && !empty($courseProgressData->date_completed)) {

                    $courseProgressData->date_started = $courseProgressData->date_completed;
                }

                if(empty($courseProgressData->date_completed)) {

                    $courseProgressData->date_completed = $courseCompleteDate;
                }

                if($completeDate) {

                    $courseProgressData->date_completed = $courseCompleteDate;
                }

                if($startDate) {

                    $courseProgressData->date_started = $courseStartDate;
                }

                if($completeDate && !$startDate) {

                    $courseProgressData->date_started = $courseCompleteDate;
                }

                $courseProgressData->progress = 100;
                $db->updateObject("joom_splms_course_progress", $courseProgressData, 'id');
            }

        } else {

            if(empty($startDate) && !empty($courseCompleteDate)) {

                $courseStartDate = $courseCompleteDate;
            }

            foreach ($languages as $lang_code => $lesson_list) {

                $newCourse = new stdClass();
                $newCourse->user_id = $userId;
                $newCourse->course_id = $courseId;
                $newCourse->language = $lang_code;
                $newCourse->lessons_completed_list = implode(",", $lesson_list);
                $newCourse->lessons_total_list = implode(",", $lesson_list);
                $newCourse->progress = 100;
                $newCourse->date_started = $courseStartDate;
                $newCourse->date_completed = $courseCompleteDate;

                $db->insertObject("joom_splms_course_progress", $newCourse);
            }
        }

        // Now, update all checklist items associated with this course to show that they have been completed
        self::markUserChecklistItemsCompletedForItemType($userId, $courseId, 'course_completion');

        return true;
    }

    public static function getComments($params) {
        $db = JFactory::getDbo();
        $conditions = array();

        if($params->course_id) {
            $course = $params->course_id;
            $query = "SELECT splms_lesson_id as id FROM joom_splms_lessons WHERE splms_course_id = $course AND enabled = 1";
            $db->setQuery($query);
            $lessonsList = $db->loadObjectList();
            $lessonArray = array();

            foreach($lessonsList as $lesson) {
                array_push($lessonArray, "'lesson_".$lesson->id."'");
            }

            $lessons = implode(',', $lessonArray);
        }

        if($params->lesson_id) {
            $lessons = "'lesson_".$params->lesson_id."'";
        }

        if($params->totals) {
            $select = 'id';
        } else {
            $select = '*';
        }

        if($params->user_id) {
            $user = ' AND user_id = '.$params->user_id.' ';
        }

        $result = '';

        if($lessons) {
            $query = "SELECT $select FROM comments WHERE post_id IN ($lessons) AND deleted IS NULL $user";
            $db->setQuery($query);
            $result = $db->loadObjectList();
        }

        return $result;
    }

    public static function getStudentLessons($params) {
        $db = JFactory::getDbo();
        $course = $params->course_id;
        if($params->user_id) {
            $user = $params->user_id;
            $userCondition = "AND s.user_id = ".$db->quote($user);
        }
        if($params->language) {
            $userCondition = " AND s.language = ".$db->quote($params->language);
        }

        $newquery = "
            SELECT
                l.ordering as ordering,
                s.user_id as user,
                s.status as status,
                s.date as status_date,
                l.splms_lesson_id as lesson_id,
                l.title as lesson_title
            FROM
                joom_splms_lesson_status as s
            LEFT JOIN joom_splms_lessons as l ON l.splms_lesson_id = s.lesson_id
            WHERE
                s.archive_date IS NULL AND s.course_id = $course
                $userCondition
            GROUP BY s.lesson_id, s.user_id
            ORDER BY s.id DESC
        ";
        $db->setQuery($newquery);
        $result = $db->loadObjectList();

        return $result;
    }

	public static $userCourseStatuses = array(
		'Assigned' => 0,
		'Recommended' => 1,
		'Completed' => 2,
		'In Progress' => 3,
		'Wishlisted' => 4,
		'Purchased' => 5
	);

	public static function getQuiz($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('#__splms_quizquestions'))
			->where($db->quoteName('splms_quizquestion_id') . "=" . $db->quote($id));
		$db->setQuery($query);
		$results = $db->loadObject();

		return $results;
	}

	public static function insertLessonStatus($params) {
        if($params->user_id) {

            if (!$params->language) {
                $params->language = AxsLanguage::getCurrentLanguage()->get('tag');
            }

            if(isset($_SESSION['current_language'])) {
                $params->language = $_SESSION['current_language'];
            }

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            if ($params->status == "started") {
                //Check to see if it's the first lesson they've started in the course progress.
                self::courseProgress_startNewLesson($params->user_id, $params->course_id, $params->language);
            }

            $params->check_lesson_status = $params->status;
            $params->check_lesson_language = $params->language;

            $statusExists = AxsLMS::getLessonStatus($params);
			if(!$statusExists) {

                // Insert columns.
                $columns = array(
                    'course_id',
                    'lesson_id',
                    'user_id',
                    'status',
                    'date',
                    'language'
                );

                // Insert values.
                $values = array(
                    $db->q($params->course_id),
                    $db->q($params->lesson_id),
                    $db->q($params->user_id),
                    $db->q($params->status),
                    $db->q(date('Y-m-d H:i:s')),
                    $db->q($params->language)
                );

                // Prepare the insert query.
                $query
                    ->insert($db->quoteName('#__splms_lesson_status'))
                    ->columns($db->quoteName($columns))
                    ->values(implode(',', $values));

                $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
                $db->execute();
            }

            if ($params->status == "completed") {
                self::courseProgress_completeLesson($params->user_id, $params->course_id, $params->lesson_id, $params->language);
            }
        }
	}

    public static function getQuestionSetResults($surveyId,$userId) {
        $html = '';
        if(!$surveyId || !$userId) {
            return false;
        }

        $surveyObj       = AxsSurvey::getSurveyById($surveyId);
        $surveyProgress  = AxsSurvey::getSurveyProgress($surveyId,$userId);
        $surveyResponses = AxsSurvey::getUserSurveyResponses($userId,$surveyId);

        $html .= "<h3>".$surveyObj->title."</h3><br/>";
        $html .= "<b>Progress: </b>$surveyProgress->progress%<br/>";
        if($surveyProgress->nps) {
            $html .= "<b>NPS: </b>$surveyProgress->nps<br/>";
        }
        if($surveyProgress->csat) {
            $html .= "<b>CSAT Score: </b>$surveyProgress->csat%<br/>";
        }
        if($surveyProgress->nps) {
            $html .= "<b>CSAT Average: </b>$surveyProgress->average_score%<br/>";
        }
        $html .= "<hr/>";


        foreach($surveyResponses as $data) {
            $html .= "<b>Question: </b>$data->question<br/>";
            $html .= "<b style=\"color:green;\">Answer: </b>$data->answer<br/><br/>";
        }

        return $html;
    }

    public static function getVideoContentResults($activity_id,$user) {
        $html = '';
        if(!$activity_id && !$user) {
            return false;
        }

        $activitySubmission = self::getActivityById($activity_id);
        $conditions = array('archive_date IS NULL');
        $conditions[] = "video_path = '$activitySubmission->student_response'";
        $conditions[] = "lesson_id  = '$activitySubmission->lesson_id'";
        $conditions[] = "course_id  = '$activitySubmission->course_id'";
        $conditions[] = "user_id    = '$activitySubmission->user_id'";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_video_tracking')
              ->where($conditions);
        $db->setQuery($query);

        $videoTrackingData = $db->loadObjectList();

        $html .= "<h3>".$activitySubmission->activity_request."</h3><br/>";
        $html .= "<b>Video Duration: </b>";
        $html .= gmdate('H:i:s', round($videoTrackingData[0]->video_duration));
        $html .= "<hr/>";

        $html .= '<table class="table table-striped">
                    <thead>
                      <tr>
                        <th>Date</th>
                        <th>Finished</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                        <th>Last Frame</th>
                        <th>Total Time</th>
                      </tr>
                    </thead>
                    <tbody>';
        foreach($videoTrackingData as $data) {
            if($data->completed) {
                $completed = 'Yes';
            } else {
                $completed = 'No';
            }

            $html .= '<tr>
                        <td>'.date('m/d/Y',strtotime($data->date)).'</td>
                        <td>'.$completed.'</td>
                        <td>'.date('H:i:s',$data->time_started).'</td>
                        <td>'.date('H:i:s',$data->time_stopped).'</td>
                        <td>'.gmdate('H:i:s', round($data->current_frame)).'</td>
                        <td>'.gmdate('H:i:s', round($data->total_time)).'</td>
                      </tr>';
        }

        $html .= '      </tbody>
                  </table>';

        return $html;
    }

    public static function getInteractiveContentResults($content_id,$user,$encryptedToken,$versionNumber = null) {
        $html = '';

        if(!$content_id && !$user) {
            return false;
        }

        $db = JFactory::getDbo();

        $contentsTable = 'ic_h5p_contents';
        $userDataTable = 'ic_h5p_contents_user_data';
        $resultsTable = 'ic_h5p_results';

        $conditions = [];

        // We're getting a specific version number, so retrieve any and all archived data
        if($versionNumber !== null) {

            $conditions[] = "version_number = $versionNumber";

            $contentsTable = 'ic_h5p_contents_archive';
            $userDataTable = 'ic_h5p_contents_user_data_archive';
            $resultsTable = 'ic_h5p_results_archive';
        }

        // Get the user content data
        $sub_content_id = '';

        if(strpos($content_id, ':')) {
           $contentIDArray = explode(':', $content_id);
           $content_id = $contentIDArray[0];
           $sub_content_id = $contentIDArray[1];
        }
        $conditions = array('archive_date IS NULL');
        $conditions[] = "content_id = $content_id";
        $conditions[] = "user_id = $user";

        // Set the conditions necessary to grab the correct content record
        $contentConditions = "c.id = $content_id";

        if($versionNumber !== null) {

            // We need to grab the content archive by content_id instead of id if this is an archive
            $contentConditions = "c.content_id = $content_id";
        }

        if($sub_content_id) {
            $conditions[] = "sub_content_id = $sub_content_id";

            $contentConditions = "c.id = $sub_content_id";


            // We need to grab the content archive by content_id instead of id if this is an archive
            if($versionNumber !== null) {

                $contentConditions = "c.content_id = $sub_content_id";
            }
        }

        $query = $db->getQuery(true);
        $query->select('*')
              ->from($userDataTable)
              ->where($conditions)
              ->limit(1);

        $db->setQuery($query);
        $userData = $db->loadObject();

        $userResponses = json_decode($userData->data);

        // Get the results data
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($resultsTable)
              ->where($conditions)
              ->setLimit(1);
        $db->setQuery($query);
        $userResult = $db->loadObject();

        // Get the interactive content data
        $query = $db->getQuery(true);
        $query->select('c.*,l.name as content_type')
              ->from($contentsTable . ' as c')
              ->join('LEFT','ic_h5p_libraries as l ON c.library_id = l.id')
              ->where($contentConditions)
              ->limit(1);

        $db->setQuery($query);

        $contentData = $db->loadObject();

        $questionsParams = json_decode($contentData->parameters);
        $content_type = $contentData->content_type;

        $html .= "<h3>".$contentData->title."</h3><br/>";

        if($userResult->finished) {
            $scorePercentage = round(($userResult->score / $userResult->max_score) * 100, 2);

            $html .= "<b>Score: </b>";
            $html .= $userResult->score."/".$userResult->max_score."<br/>";
            $html .= "<b>Percent Correct: </b>" . $scorePercentage . '%<br/>';
            $html .= "<b>Finished on: </b>";
            $html .= date('m/d/Y',$userResult->finished);
            $html .= "<hr/>";
        }

        // Render the data / results using dynamic HTML
        switch($content_type) {

            case 'H5P.Questionnaire':

                $questions = $questionsParams->questionnaireElements;
                $questionsCount = count($questions);

                for($i = 0; $i < $questionsCount; $i++) {
                    $html .= "<b>".$questions[$i]->library->params->question."</b><br/>";
                    $html .= self::parseAnswer($userResponses->questions[$i],$questions[$i]->library)."<br/>";
                    $html .= "<hr/>";
                }
            break;

            case 'H5P.Essay':

                $html .= "<b>".$questionsParams->taskDescription."</b><br/>";
                $html .= str_replace("\n", '<br/>', $userResponses->inputField)."<br/>";
            break;

            default: break;
        }

        $useIframe = [
            'H5P.QuestionSet',
            'H5P.CoursePresentation',
            'H5P.DragQuestion',
            'H5P.DragText',
            'H5P.InteractiveVideo',
            'H5P.Summary',
            'H5P.Column',
            'H5P.MarkTheWords',
            'H5P.MultiChoice',
            'H5P.SingleChoiceSet',
            'H5P.TrueFalse',
            'H5P.Blanks'
        ];

        if(in_array($content_type, $useIframe)) {
            $iframeParams = [
                'embed_id' => $content_id,
                'user' => $user,
                'token' => $encryptedToken
            ];

            if($versionNumber !== null) {
                $iframeParams['version_number'] = $versionNumber;
            }

            $paramString = http_build_query($iframeParams);

            $html .= '<iframe src="/?tmpl=ic_embed&' . $paramString . '" style="width: 100%; height: 242px;" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        }

        if($versionNumber === null) {

            $conditions = [
                "content_id = $content_id",
                "user_id = $user"
            ];

            // Get all of the archived data
            $query = $db->getQuery(true);
            $query->select('version_number, archived_at')
                  ->from('ic_h5p_contents_user_data_archive')
                  ->where($conditions);

            $db->setQuery($query);
            $userDataArchiveVersions = $db->loadObjectList();

            $version_number = array_column($userDataArchiveVersions, 'version_number');
            array_multisort($version_number, SORT_DESC, $userDataArchiveVersions);

            // Render the links containing the archive versions obtained from the previous query (if any).
            if(!is_null($userDataArchiveVersions) && count($userDataArchiveVersions) > 0) {

                // Display the archived user data instead of the iframed content
                $html .= "<h3>Archived User Response Data: </h3><div class='splms-archived-versions container'>";
                $html .= "
                    <table class='table table-striped'>
                        <thead>
                            <th>Archive Version</th>
                            <th>Archive Timestamp</th>
                        </thead>
                        <tbody>
                ";

                foreach($userDataArchiveVersions as $archiveVersion) {

                    $version = $archiveVersion->version_number;
                    $timestamp = $archiveVersion->archived_at;

                    $html .= "
                        <tr>
                            <td>
                                <a href='#' class='archive-version-link' data-version='$version' data-content-id='$content_id'>

                                    Version #$version
                                </a>
                            </td>
                            <td>
                                <div class='archive-version-timestamp'>$timestamp</div>
                            </td>
                        </tr>
                    ";
                }

                $html .= "
                        </tbody>
                    </table>
                ";

                $html .= "</div>
                <div class='version-details-box'>
                    <div class='center spinner'>
                        <i class='fa fa-3x fa-spinner fa-spin'></i>
                    </div>

                    <div class='container-fluid' style='box-sizing: content-box; border-top: 2px black solid; padding-top: 1em; line-height: 1.5em;'>
                        <div class='row'>
                            <div class='answers splms-col-md-12'></div>
                        </div>
                    </div>
                </div>
                ";
            }
        }

        return $html;
    }

    public static function parseAnswer($answer,$library) {
        switch($library->metadata->contentType) {
            case 'Simple Multi Choice' :
                $answerArray = explode('[,]',$answer);
                foreach($answerArray as $index) {
                    $answers[] = $library->params->alternatives[$index]->text;
                    $answer = implode(',',$answers);
                }
            break;
            case 'checkbox' :
                $answerArray = explode('[,]',$answer);
                foreach($answerArray as $index) {
                    $answers[] = $library->params->alternatives[$index]->text;
                    $answer = implode(',',$answers);
                }
            break;
            case 'Open Ended Question' :
                $answer = $answer;
            break;
            default:
                $answer = $answer;
            break;
        }
        return $answer;
    }

    public static function parseActivitySubmission($item) {
        $mediaTypes = ['youtube', 'vimeo', 'screencast', 'facebook', 'mp4', 'mp3'];
        $interactiveTypes = ['interactive'];
        $videoTypes = ['video'];
        $questionSetFieldTypes = ['survey'];
        if ( $item->student_response && $item->activity_type) {
            if (in_array($item->activity_type, $mediaTypes))  {
                $videoParams = new stdClass();
                $videoParams->video      = $item->student_response;
                $videoParams->video_type = $item->activity_type;
                $videoParams->poster     = null;
                $videoParams->responsive = false;
                $videoParams->activitySubmission = true;
                $html = AxsMedia::getVideo($videoParams);
            }
            if (in_array($item->activity_type, $interactiveTypes))  {
                $html = self::getInteractiveContentResults($item->student_response,$item->user,$item->encryptedToken);
            }
            if (in_array($item->activity_type, $videoTypes))  {
                $html = self::getVideoContentResults($item->activity_id,$item->user);
            }
            if (in_array($item->activity_type, $questionSetFieldTypes))  {
                $html = self::getQuestionSetResults($item->student_response,$item->user);
            }
        }

        return $html;
    }

	public static function getVideo($item) {


        $videoParams = new stdClass();

        if($item->secure_video_url) {
            $videoParams->secure_video_url        = $item->secure_video_url;
        }
        if($item->lesson) {
            $lesson = json_decode($item->lesson);
            $videoParams->lesson_id               = $lesson->splms_lesson_id;
            $videoParams->activity_id             = $item->id;
            $videoParams->course_id               = $lesson->splms_course_id;
            $videoParams->activity_title          = $item->title;
            $videoParams->activity_required       = $item->required;
            if($item->title) {
                $videoParams->video_title         = $item->title;
            } else {
                $videoParams->video_title         = $lesson->title." (Video)";
            }
        } elseif($item->splms_course_id) {
            $videoParams->lesson_id               = $item->splms_lesson_id;
            $videoParams->course_id               = $item->splms_course_id;
            $videoParams->video_title             = $item->title." (Video)";
        } elseif($item->media_type) {
            $videoParams->media_id                = $item->id;
            $videoParams->video_title             = $item->title;
        }



        if($item->video_url) {
            $source = $item->video_url;
        }

        if($item->source) {
            $source = $item->source;
        }

        if(!$item->params) {
            $course_params = $item;
        } else {
            $course_params = json_decode($item->params);
	 	}

        if($course_params->cover_image && (!$item->splms_course_id && !$item->splms_lesson_id)) {
            $poster = $course_params->cover_image;
        } else {
            $poster = null;
        }

        if ($course_params->youtube_id || $source || $course_params->vimeo_id || $course_params->facebook_url || $course_params->screencast_id) {

            if ( ($course_params->youtube_id) && ($course_params->video_type == 'youtube') ) {
                $videoParams->video      = $course_params->youtube_id;
                $videoParams->video_type = $course_params->video_type;
                $html = AxsMedia::getVideo($videoParams);
            }
            else if ( ($course_params->screencast_id) && ($course_params->video_type == 'screencast') ) {
                $videoParams->video      = $course_params->screencast_id;
                $videoParams->video_type = $course_params->video_type;
                $html = AxsMedia::getVideo($videoParams);
             }
            else if ( ($course_params->vimeo_id) && ($course_params->video_type == 'vimeo') ) {
                $videoParams->video      = $course_params->vimeo_id;
                $videoParams->video_type = $course_params->video_type;
                $html = AxsMedia::getVideo($videoParams);
             }
            else if ( ($course_params->facebook_url) && ($course_params->video_type == 'facebook') ) {
                $videoParams->video      = $course_params->facebook_url;
                $videoParams->video_type = $course_params->video_type;
                $html = AxsMedia::getVideo($videoParams);
            }
            else if ( $source && ($course_params->video_type == 'mp4') ) {
                $videoParams->video      = $source;
                $videoParams->video_type = $course_params->video_type;
                $videoParams->poster     = $poster;
                $html = AxsMedia::getVideo($videoParams);
            }

            return $html;

        } else {

        	return FALSE;
        }
	}

    public static function getQuizQuestionMedia($media) {
        if($media) {
            $mime = mime_content_type($media);
            if(strpos($media, 'images/') !== false) {
                $media = '/'.$media;
            }
            if(strstr($mime, "video/")){
                // this code for video
                $mediaHTML = '<video controls class="quiz-media">
                                <source src="'.$media.'" type="video/mp4">
                                Your browser does not support the video tag.
                              </video>';
            }elseif(strstr($mime, "image/")){
                // this code for image
                $mediaHTML = '<img src="'.$media.'" class="quiz-media" />';
            }elseif(strstr($mime, "audio/")){
                // this code for audio
                $mediaHTML = '<audio controls class="quiz-media">
                                <source src="'.$media.'" type="audio/mpeg">
                                Your browser does not support the audio tag.
                              </audio>';
            }else {
                $mediaHTML = '';
            }

        } else {
            $mediaHTML = '';
        }
        return $mediaHTML;
    }

	/**
	 * @param $user_id int Pass a user's id in
	 *
	 * @return array of courses for that user.  Each course may be wishlisted, purchased, etc,.
	 */
	public static function getUserAllCourses($user_id, $ids_only = false, $filter = false) {
		$courses = array();
        $cids = array();
        $db = JFactory::getDbo();

        //get courses that are wishlisted
        if(!$filter || $filter == 'wishlisted') {

            $query = "SELECT * FROM `#__splms_course_wishlist` WHERE `user_id`=$user_id;";
            $db->setQuery($query);
            $course_wishlist = $db->loadObjectList();
            foreach($course_wishlist as $c) {
                $tmp = new stdClass();
                $tmp->type = 'Wishlisted';
                $tmp->wishlist_date = $c->date;
                $courses[$c->course_id][self::$userCourseStatuses['Wishlisted']] = $tmp;
                $cids[] = $c->course_id;
            }
        }

        //get courses that are recommended or assigned by usergroup
        if(!$filter || $filter == 'assigned' || $filter == 'recommended') {
            $query = trim("
                SELECT * FROM joom_splms_courses_groups AS a
                WHERE (a.course_ids != '' AND a.course_ids IS NOT NULL)
                AND (FIND_IN_SET(" . $db->quote($user_id) .", a.user_ids)
            ");
            $usergroups = JUserHelper::getUserGroups($user_id);
            foreach ($usergroups as $usergroup) {
                $query .= "OR FIND_IN_SET(" . $db->quote($usergroup) . ", a.usergroups) ";
            }
            $query .= ')';

            $db->setQuery($query);
            $course_groups = $db->loadObjectList();
            foreach($course_groups as $cg) {
                if($cg->user_ids) {
                    $userArray = explode(',',$cg->user_ids);
                    if(!in_array($user_id,$userArray)) {
                        continue;
                    }
                }
                $course_ids = explode(',', $cg->course_ids);
                foreach($course_ids as $cid) {
                    $tmp = new stdClass();
                    $tmp->title = $cg->title;
                    $tmp->type = $cg->type;
                    $tmp->free = $cg->free;

                    // Set the start, due and end dates for the course assignment
                    $tmp->start_date = $cg->start_date;
                    $tmp->due_date = $cg->due_date;
                    $tmp->end_date = $cg->end_date;

                    if($cg->type) {
                        $type = 'Recommended';
                    } else {
                        $type ='Assigned';
                    }
                    if($filter == 'assigned' && $type == 'Assigned') {
                        $courses[$cid][self::$userCourseStatuses[$type]] = $tmp;
                    }
                    if($filter == 'recommended' && $type == 'Recommended') {
                        $courses[$cid][self::$userCourseStatuses[$type]] = $tmp;
                    }
                    if(!$filter) {
                        $courses[$cid][self::$userCourseStatuses[$type]] = $tmp;
                    }

                    $cids[] = $cid;
                }
            }
        }

        //get courses that are purchased
        if(!$filter || $filter == 'purchased') {
            $query = "SELECT * FROM `axs_course_purchases` WHERE user_id=$user_id AND status='PAID' OR status='PART';";
            $db->setQuery($query);
            $course_purchases = $db->loadObjectList();
            foreach($course_purchases as $cp) {
                $tmp = new stdClass();
                $tmp->type = 'Purchased';
                $tmp->date = $cp->date;
                $courses[$cp->course_id][self::$userCourseStatuses['Purchased']] = $tmp;
                $cids[] = $cp->course_id;
            }
        }

        //get courses that are in progress or completed
        if(!$filter || $filter == 'inProgress' || $filter == 'completed') {
            $query = "SELECT * FROM `#__splms_course_progress` WHERE archive_date IS NULL AND `user_id`=$user_id GROUP BY user_id, course_id;";
            $db->setQuery($query);
            $courseList = $db->loadObjectList();

            foreach($courseList as $row) {
                $courseProgress = self::getCourseProgress((int)$row->course_id,$user_id);
                $tmp = new stdClass();
                $tmp->percentComplete = round($courseProgress->progress);
                $tmp->type = $tmp->percentComplete == 100 ? 'Completed' : 'In Progress';
                if($filter == 'completed' && $tmp->type == 'Completed') {
                    $courses[$row->course_id][self::$userCourseStatuses[$tmp->type]] = $tmp;
                }
                if($filter == 'inProgress' && $tmp->type == 'In Progress') {
                    $courses[$row->course_id][self::$userCourseStatuses[$tmp->type]] = $tmp;
                }
                if(!$filter) {
                    $courses[$row->course_id][self::$userCourseStatuses[$tmp->type]] = $tmp;
                }
            }
        }

        if ($ids_only) {
            $courses = array_keys($courses);
        }

		return $courses;
	}

    private static function getPublishedLanguages() {
        //Get all the published languages
        $db = JFactory::getDBO();
        if (!self::$publishedLanguages) {
            $query = "SELECT * FROM joom_languages WHERE published = 1";
            $db->setQuery($query);
            $languages = $db->loadObjectList();

            self::$publishedLanguages = array();
            foreach ($languages as $language) {
                array_push(self::$publishedLanguages, $language->lang_code);
            }

            if (count(self::$publishedLanguages) == 0) {
                //The site is not multilingual, just get the default language.
                $lang = JFactory::getLanguage()->getTag();
                array_push(self::$publishedLanguages, $lang);
            }
        }

        return self::$publishedLanguages;
    }

    //Called when a course is saved by an administrator
    public static function updateCourseSchedules($course) {

        if (!$course) {
            return;
        }

        $course_id = $course['splms_course_id'];

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $type_id = "course_" . $course_id . "_due_date";

        $params = json_decode($course['params']);

        if (!$params->use_due_date) {
            //There is no due date on the course.  If there are any calendar events for the course, remove them.

            $query
                ->delete($db->quoteName('axs_schedules'))
                ->where($db->quoteName('type_id') . '=' . $db->quote($type_id));

            $db->setQuery($query);
            $db->execute();

            return;
        } else {
            //Get all students who are already taking the course and update their due date
            $query
                ->select('*')
                ->from($db->quoteName('joom_splms_course_progress'))
                ->where($db->quoteName('course_id') . '=' . $db->quote($course_id));

            $db->setQuery($query);
            $students = $db->loadObjectList();

            foreach ($students as $student) {
                $student->date_due = self::getCourseDueDate($student, $course);

                if (!$student->date_due) {
                    $student->date_due = 0;
                }

                $db->updateObject('joom_splms_course_progress', $student, 'id');

                self::saveCourseDueDateToCalendar($student, $course);
            }
        }
    }



    private static function getCourseDueDate($student, $course, $language = null) {

        $db = JFactory::getDBO();

        if (!$language) {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
        }

        //See if the variables are just IDs.  If so, pull the info from the database.
        if (gettype($student) != "object") {

            $conditions = array(
                $db->quoteName('user_id') . '=' . $student,
                $db->quoteName('language') . '=' . $language
            );

            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from($db->quoteName("joom_splms_course_progress"))
                ->where($conditions);

            $db->setQuery($query);

            //The student can have multiple progresses depending on how many languages there are.
            $student = $db->loadObject();
        }

        if (gettype($course) != "object" && gettype($course) != "array") {
            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from($db->quoteName('joom_splms_courses'))
                ->where($db->quoteName('splms_course_id') . '=' . $db->quote($course));

            $db->setQuery($query);

            $course = $db->loadObject();
        }

        $date_due = null;

        if (isset($course->params)) {
            //Course is pulled from database
            $params = json_decode($course->params);
        } else {
            //Course is passed in by save function
            $params = json_decode($course['params']);
        }

        if(!$params->use_due_date) {
            return null;
        }

        switch ($params->due_date_type) {
            case "static":
                //The due date is static.  Just set the date and save it.
                $date_due = date("Y-m-d h:i:s", strtotime($params->due_static_date));
                //$db->updateObject('joom_splms_course_progress', $student, 'id');

                break;
            case "dynamic":
                //The due date is dynamic and is based off of their start date.
                $unit = $params->due_dynamic_unit;
                $number = $params->due_dynamic_number;

                $startedTime = strtotime($student->date_started);

                if (!$startedTime || $startedTime < 0) {
                    //The student has not started the course.  If a date exists, remove it.
                    //Setting to null has no effect, so it will leave the previous date.
                    $date_due = null;
                } else {
                    $startDate = date("h-i-s-m-d-Y", $startedTime);
                    $fields = explode("-", $startDate);

                    switch ($unit) {
                        case "minute":
                            $fields[1] += $number;
                            break;
                        case "hour":
                            $fields[0] += $number;
                            break;
                        case "day":
                            $fields[4] += $number;
                            break;
                        case "week":
                            $fields[4] += $number * 7;      //mktime does not have a week variable
                            break;
                        case "month":
                            $fields[3] += $number;
                            break;
                        case "year":
                            $fields[5] += $number;
                            break;
                    }

                    $date_due = date("Y-m-d h:i:s", mktime($fields[0], $fields[1], $fields[2], $fields[3], $fields[4], $fields[5]));
                }

                //$db->updateObject('joom_splms_course_progress', $student, "id");


                break;
            default:
                //There's an error
                $date_due = null;
        }

        return $date_due;
    }

    private static function saveCourseDueDateToCalendar($student, $course, $language = null) {

        if (!$language) {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
        }

        $db = JFactory::getDBO();

        //See if the variables are just IDs.  If so, pull the info from the database.
        if (gettype($student) != "object") {

            $conditions = array(
                $db->quoteName('user_id') . '=' . $student,
                $db->quoteName('language') . '=' . $language
            );

            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from($db->quoteName("joom_splms_course_progress"))
                ->where($conditions);

            $db->setQuery($query);

            //The student can have multiple progresses depending on how many languages there are.
            $student = $db->loadObject();
        }

        if (gettype($course) != "object" && gettype($course) != "array") {
            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from($db->quoteName('joom_splms_courses'))
                ->where($db->quoteName('splms_course_id') . '=' . $db->quote($course));

            $db->setQuery($query);

            $course = $db->loadObject();
        }

        $query = $db->getQuery(true);

        $type_id = "course_" . $student->course_id . "_due_date";

        $conditions = array(
            $db->quoteName('user_id') . '=' . $db->quote($student->user_id),
            $db->quoteName('type_id') . '=' . $db->quote($type_id),
            $db->quoteName('language') . '=' . $db->quote($student->language)
        );

        $query
            ->select('*')
            ->from('axs_schedules')
            ->where($conditions);

        $db->setQuery($query);
        $schedule = $db->loadObject();

        if (isset($course->title)) {
            $title = $course->title;
        } else {
            if (isset($course['title'])) {
                $title = $course['title'];
            } else {
                $title = "";
            }
        }

        $calendarData = new stdClass();

        $calendarData->title = $title . " due date.";
        //$calendarData->url = $course->url;
        $calendarData->description = "This course is due for completion.";
        $calendarData->start = $student->date_due;
        $calendarData->end = $student->date_due;
        $calendarData->isAllDay = false;

        $calendarObject = new stdClass();
        $calendarObject->data = json_encode($calendarData);
        $calendarObject->user_id = $student->user_id;
        $calendarObject->owner_id = -1;
        $calendarObject->type_id = $type_id;
        $calendarObject->start_date = $student->date_due;
        $calendarObject->end_date = $student->date_due;
        $calendarObject->language = $student->language;

        if ($schedule) {

            $calendarObject->id = $schedule->id;
            $db->updateObject('axs_schedules', $calendarObject, 'id');

            //They already have a schedule for this course.  Simply update the date.
        } else {
            //This schedule doesn't exist.  Add it.
            $db->insertObject('axs_schedules', $calendarObject);
        }
    }

    //Used for updating the lesson in the admin section.
    public static function courseProgress_updateLesson($lesson_id, $new_course_id, $new_language) {

        $db = JFactory::getDBO();

        //Check if the lesson is already part of a course's lineup.  A lesson cannot be in two courses, so we only need to find one result.
        $query = "SELECT * FROM joom_splms_course_progress WHERE FIND_IN_SET($lesson_id, lessons_total_list) LIMIT 1";
        $db->setQuery($query);
        $result = $db->loadObject();

        if ($result) {
            if ($new_course_id != $result->course_id) {
                //The course has changed.  Remove the lesson from the course it used to belong to.
                //self::courseProgress_removeLessonFromCourse($result->course_id, $lesson_id);
                self::courseProgress_refreshCourse($result->course_id);
            } else {
                //The lesson has not moved to a new course.
                if ($new_language != $result->language) {
                    //The course is the same but the language has changed.
                    self::courseProgress_refreshCourse($result->course_id);
                } else {
                    return;
                }
            }
        } //Else it's a new lesson and did not previously belong to a course.  Or the previous course was deleted.

        //Add the lesson to $new_course_id
        self::courseProgress_refreshCourse($new_course_id);
    }

    //Get all of the lessons that belong to the course.
    public static function courseProgress_getCourseLessons($course_id = null) {
        //if course_id is null, get all courses

        $db = JFactory::getDBO();

        $query = "SELECT * FROM joom_splms_courses WHERE enabled = 1";

        if ($course_id) {
            $query .= " AND splms_course_id = $course_id";
        }

        $db->setQuery($query);
        $results = $db->loadObjectList();

        $lang_codes = self::getPublishedLanguages();

        $courses = array();

        foreach ($results as $result) {
            $id = $result->splms_course_id;

            if (!isset($courses[$id])) {
                $courses[$id] = array();
            }

            foreach ($lang_codes as $lang_code) {

                if (!isset($courses[$id][$lang_code])) {
                    $courses[$id][$lang_code] = array();

                    $query = $db->getQuery(true);

                    $conditions = array(
                        $db->quoteName('enabled') . '= 1',
                        $db->quoteName('splms_course_id') . '=' . $db->quote($id),
                        $db->quoteName('language') . '=' . $db->quote($lang_code)
                    );

                    $query
                        ->select('*')
                        ->from($db->quoteName('joom_splms_lessons'))
                        ->where($conditions);

                    $db->setQuery($query);
                    $lessons = $db->loadObjectList();

                    foreach ($lessons as $lesson) {
                        array_push($courses[$id][$lang_code], $lesson->splms_lesson_id);
                    }
                }
            }
        }

        return $courses;

    }

    //Pull all the data from the lesson progress and update the course from fresh.
    public static function courseProgress_refreshCourse($refresh_course_id = null) {
        //If refresh_course_id is null, refresh everything.

        if ($refresh_course_id === 0) {
            return;
        }

        $db = JFactory::getDBO();

        $courses = self::courseProgress_getCourseLessons($refresh_course_id);

        $query = "SELECT * FROM joom_splms_lesson_status";

        if ($refresh_course_id) {
            $query .= " WHERE course_id = $refresh_course_id AND archive_date IS NULL";
        }

        $db->setQuery($query);

        $results = $db->loadObjectList();

        $lang_codes = self::getPublishedLanguages();

        //Get all of the user data
        foreach ($results as $result) {

            $user_id = $result->user_id;
            $course_id = $result->course_id;
            $lesson_id = $result->lesson_id;
            $status = $result->status;
            $date = $result->date;
            $language = $result->language;

            if (!$status || !$user_id || !$lesson_id || !$language) {
                continue;
            }

            if (!isset($user_list[$user_id])) {
                $user_list[$user_id] = array();
            }

            if (!isset($user_list[$user_id][$course_id])) {
                $user_list[$user_id][$course_id] = array();
            }

            if (!isset($user_list[$user_id][$course_id][$language])) {
                $user_list[$user_id][$course_id][$language] = array();
            }

            if (!isset($user_list[$user_id][$course_id][$language][$lesson_id])) {
                $user_list[$user_id][$course_id][$language][$lesson_id] = new stdClass();
            }

            //echo "user $user_id - course $course_id - language $language - lesson $lesson_id - status $status<br>";
            $user_list[$user_id][$course_id][$language][$lesson_id]->$status = $date;
        }

        $query = "SELECT * FROM axs_course_purchases WHERE status = 'PAID'";
        if ($refresh_course_id) {
            $query .= " AND course_id = $refresh_course_id";
        }

        $db->setQuery($query);
        $purchases = $db->loadObjectList();

        foreach ($purchases as $purchase) {
            $user_id = $purchase->user_id;
            $course_id = $purchase->course_id;

            if (!isset($user_list[$user_id])) {
                $user_list[$user_id] = array();
            }

            if (!isset($user_list[$user_id][$course_id])) {
                $user_list[$user_id][$course_id] = array();
            }

            //foreach ($courses[$course_id] as $language => $data) {
            foreach ($lang_codes as $lang_code) {
                if (!isset($user_list[$user_id][$course_id][$lang_code])) {
                    $user_list[$user_id][$course_id][$lang_code] = null;
                }
            }
        }

        //Check to be sure that each user's course has a setting for each language

        foreach ($user_list as $user_id => $course) {
            foreach ($course as $course_id => $language) {
                foreach ($lang_codes as $lang_code) {
                    if (!isset($user_list[$user_id][$course_id][$lang_code])) {
                        $user_list[$user_id][$course_id][$lang_code] = null;
                    }
                }
            }
        }

        foreach ($user_list as $user_id => $course) {
            foreach ($course as $course_id => $language) {

                /*
                if (!isset($courses[$course_id])) {
                    //The course the lesson belongs to does not exist.
                    continue;
                }
                */

                //Cycle through each available language and the lessons

                foreach ($language as $lang_code => $lang) {

                    $date_started = PHP_INT_MAX;        //Get a date way in the future since we want to find the earlist date.
                    $date_last_activity = 0;
                    $date_completed = 0;

                    $lessons_taken = array();

                    if ($lang) {
                        foreach ($lang as $lesson_id => $lesson) {

                            /*
                            if (!$courses[$course_id][$lang_code]) {
                                //There are no lessons for this course.
                                continue;
                            }
                            */

                            //Don't count it if the completed lesson is no longer required for the course.
                            if (in_array($lesson_id, $courses[$course_id][$lang_code])) {

                                if (isset($lesson->started)) {
                                    $start = strtotime($lesson->started);
                                    if ($start < $date_started) {
                                        $date_started = $start;
                                    }

                                    if ($start > $date_last_activity) {
                                        $date_last_activity = $start;
                                    }
                                }

                                if (isset($lesson->failed)) {
                                    $fail = strtotime($lesson->failed);
                                    if ($fail > $date_last_activity) {
                                        $date_last_activity = $fail;
                                    }
                                }

                                if (isset($lesson->completed)) {
                                    $complete = strtotime($lesson->completed);
                                    if ($complete > $date_last_activity) {
                                        $date_last_activity = $complete;
                                    }

                                    if ($complete > $date_completed) {
                                        $date_completed = $complete;
                                    }

                                    array_push($lessons_taken, $lesson_id);
                                }
                            }
                        }
                    } else {
                        $date_started = null;
                        $date_last_activity = null;
                        $date_completed = null;
                    }

                    if ($date_started == PHP_INT_MAX) {
                        //Bad data, they never started the course
                        continue;
                    }

                    //echo implode(",", $lessons_taken).$b;
                    //echo implode(",", $courses[$course_id][$lang_code]).$b.$b;

                    $completed = count($lessons_taken);
                    $total = count($courses[$course_id][$lang_code]);

                    if ($total > 0) {
                        $percent = ((float)($completed / $total) * 100);
                        if ($percent > 100) {
                            $percent = 100;
                        }
                    } else {
                        $percent = 0;
                    }

                    if ($date_started != null) {
                        $date_started = date("Y-m-d H:i:s", $date_started);
                    }

                    if ($date_last_activity != null) {
                        $date_last_activity = date("Y-m-d H:i:s", $date_last_activity);
                    }

                    if ($date_completed != null) {
                        if ($completed == $total) {
                            $date_completed = date("Y-m-d H:i:s", $date_completed);
                        } else {
                            $date_completed = null;
                        }
                    }

                    if ($date_completed == 0) {
                        $date_completed = null;
                    }

                    $progress = new stdClass();

                    $progress->user_id = $user_id;

                    $progress->user_id = $user_id;
                    $progress->course_id = $course_id;
                    $progress->language = $lang_code;
                    $progress->lessons_completed_list = implode(",", $lessons_taken);
                    $progress->lessons_total_list = implode(",", $courses[$course_id][$lang_code]);
                    $progress->progress = $percent;
                    $progress->date_started = $date_started;
                    $progress->date_last_activity = $date_last_activity;
                    $progress->date_completed = $date_completed;

                    try {
                        $db->insertObject("joom_splms_course_progress", $progress);
                        //echo "INSERT " . $user_id . " " . $course_id . " " . $lang_code . " " . $date_started . " " . $date_last_activity . " " . $date_completed . $b;
                    } catch (Exception $e) {
                        //It already exists.  Get the ID from the database that matches so we can update it.
                        $query = "SELECT id FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = $user_id AND course_id = $course_id AND language = '$lang_code'";

                        $db->setQuery($query);
                        $course = $db->loadObject();
                        $progress->id = $course->id;

                        $db->updateObject("joom_splms_course_progress", $progress, "id");
                        //echo "UPDATE " . $user_id . " " . $course_id . " " . $lang_code . " " . $date_started . " " . $date_last_activity . " " . $date_completed . $b;
                    }

                }
            }
        }
        $awardCheckParams = new stdClass();
        $awardCheckParams->requirement_type = 'course';
        $awardCheckParams->course_id = $refresh_course_id;
        $award_ids = self::getAvailableBadges($awardCheckParams);
        if($award_ids) {
            self::updateBadges($award_ids, null, null, null);
        }
    }

    //Check to see if the lesson is the first one they have taken for the course.  If so, mark the course as started.
    public static function courseProgress_startNewLesson($user_id, $course_id, $language) {
        if(!$language) {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
        }

        $db = JFactory::getDBO();

        $query = $db->getQuery(true);

        $conditions = array(
            $db->quoteName('user_id') . '=' . $db->quote($user_id),
            $db->quoteName('course_id') . '=' . $db->quote($course_id),
            $db->quoteName('language') . '=' . $db->quote($language),
            'archive_date IS NULL'
        );

        $query
            ->select('*')
            ->from($db->quoteName('joom_splms_course_progress'))
            ->where($conditions);

        $db->setQuery($query);
        $student = $db->loadObject();

        if (!$student) {
            //There is no entry for this user and course.   Add it and then try getting the course again.
            self::courseProgress_addNewCourse($user_id, $course_id);

            $db->setQuery($query);
            $student = $db->loadObject();

            if (!$student) {
                //Something is wrong.  Possibly an incorrect course id
                return false;
            }
        }

        //Check to see if the course has already been started.  If there is a date, they've already started the course.
        if ($student->date_started === null) {
            $student->lessons_completed_list = "";
            $student->progress = 0;
            $student->date_started = date("Y-m-d H:i:s");
            $student->date_due = self::getCourseDueDate($student, $course_id);

            self::saveCourseDueDateToCalendar($student, $course_id);

            //Add the course to the user's calendar.

            $db->updateObject("joom_splms_course_progress", $student, "id");
        }
    }

    // TODO: ScormContent
    public static function courseProgress_ScormContent($params) {
        $user_id    = $params->user_id;
        $course_id  = $params->course_id;
        $language   = $params->language;
        $status     = $params->status;

        if (!$language) {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
        }

        $db = JFactory::getDBO();
        $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = $user_id AND course_id = $course_id AND language = '$language'";
        $db->setQuery($query);
        $course = $db->loadObject();

        if (!$course) {
            //There is no entry for this user and course.   Add it and then try getting the course again.
            self::courseProgress_addNewCourse($user_id, $course_id);

            $db = JFactory::getDBO();
            $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = $user_id AND course_id = $course_id AND language = '$language'";
            $db->setQuery($query);

            $course = $db->loadObject();
            if (!$course) {

                //Something is wrong.  Possibly an incorrect course id
                return false;
            }
        }

        //Check to see if the course has already been started.  If there is a date, they've already started the course.
        if ($course->date_started === null) {

            $course->lessons_completed_list = "";
            $course->progress = 0;
            $course->date_started = date("Y-m-d H:i:s");

            if($status == 'completed') {
                $course->progress = 100;
                $course->date_completed = date("Y-m-d H:i:s");
            }

            $db->updateObject("joom_splms_course_progress", $course, "id");
        }

        if($status == 'completed') {

            if($course->progress != 100) {

                $course->progress = 100;
                $course->date_completed = date("Y-m-d H:i:s");
                $db->updateObject("joom_splms_course_progress", $course, "id");
            }

            // Now, update all checklist items associated with this course to show that they have been completed
            self::markUserChecklistItemsCompletedForItemType($user_id, $course_id, 'course_completion');
        }

        $awardCheckParams = new stdClass();
        $awardCheckParams->requirement_type = 'course';
        $awardCheckParams->course_id = $course_id;
        $award_ids = self::getAvailableBadges($awardCheckParams);

        if($award_ids) {
            self::updateBadges($award_ids, $user_id, null, null);
        }
    }

    public static function getAllUserQuizzes($course,$lesson,$user,$quiz) {

        $db = JFactory::getDbo();

        // Make this a raw text string to reduce information processing overhead
        $query = '
            SELECT splms_quizquestion_id, total_marks, point
            FROM joom_splms_quizresults
            WHERE archive_date IS NULL AND user_id = ' . $db->quote($user);

        if ($course) {
            $query .= ' AND splms_course_id = ' . $db->quote($course);
        }

        if ($lesson) {
            $query .= ' AND splms_lesson_id = ' . $db->quote($lesson);
        }

        if ($quiz) {
            $query .= ' AND splms_quizquestion_id = ' . $db->quote($quiz);
        }

        $db->setQuery($query);

        // Loading as a list of rows for performance improvement (raw data is less computationally expensive than
        // mapping columns to row data items)
        $results = $db->loadRowList();

        if(!$results) {
            return false;
        }

        $highestScore = 0;
        $breakdown = '';

        /*
            If the user has attempted the quiz multiple times,
            get their highest score.
        */
        foreach($results as $result) {

            // Give the un-keyed results meaningful names
            $pointValue = $result[2];
            $totalMarks = $result[1];
            $quizQuestionId = $result[0];

            $quizScore = $totalMarks > 0 ? $pointValue / $totalMarks : 0;

            if ($quizScore > $highestScore) {

                $highestScore = $quizScore;
                $breakdown = sprintf('(%s / %s)', $pointValue, $totalMarks);
            }

            $quizquestion_id = $quizQuestionId;
        }

        $quiz = new stdClass();
        $quiz->id = $quizquestion_id;
        $quiz->highscore = round($highestScore * 100);
        $quiz->attempts = count($results);
        $quiz->breakdown = $breakdown;

        return $quiz;
    }

    public static function courseUpdateLastActivity($user_id, $course_id, $lesson_id, $language) {

        if(isset($_SESSION['current_language'])) {
            $language = $_SESSION['current_language'];
        } else {
            if(!$language) {
                $language = AxsLanguage::getCurrentLanguage()->get('tag');
            }
        }

        $db = JFactory::getDBO();
        $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = $user_id AND course_id = $course_id AND language = '$language'";
        $db->setQuery($query);
        $course = $db->loadObject();

        if (!$course) {
            //There is no entry for this user and course.
            return false;
        }

        $course->date_last_activity = date("Y-m-d H:i:s");

        $db->updateObject("joom_splms_course_progress", $course, "id");
    }

    public static function getLessonCompletedList($course,$lessons) {
        $db = JFactory::getDBO();
        $query = "SELECT * FROM joom_splms_lesson_status WHERE archive_date IS NULL AND user_id = $course->user_id AND course_id = $course->course_id AND language = '$course->language' AND status = 'completed' AND lesson_id IN ($lessons)";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public static function getLessonList($course) {
        $db = JFactory::getDBO();
        $conditions[] = $db->quoteName('splms_course_id').'='.$db->quote($course->course_id);
        $conditions[] = $db->quoteName('language').'='.$db->quote($course->language);
        $conditions[] = $db->quoteName('enabled').'= 1';
        $query = $db->getQuery(true);
        $query->select('splms_lesson_id');
        $query->from("#__splms_lessons");
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        if($course->id_list) {
            $ids = array();
            foreach($results as $lesson) {
                array_push($ids,$lesson->splms_lesson_id);
            }
            return implode(',',$ids);
        }
        return $results;
    }

    //The user has finished a lesson. Add it to their completed list and update the progress
    public static function courseProgress_completeLesson($user_id, $course_id, $lesson_id, $language) {

        if(!$language) {
            $language = AxsLanguage::getCurrentLanguage()->get('tag');
        }

        $db = JFactory::getDBO();
        $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = $user_id AND course_id = $course_id AND language = '$language'";
        $db->setQuery($query);
        $course = $db->loadObject();

        if (!$course) {

            //There is no entry for this user and course.
            return;
        }

        if ($course->progress == 100) {

            // This course has already been completed, don't complete it again
            // This could happen if the course is marked compelted but a user
            // goes back and completes the final activity of the last lesson they
            // need to complete
            return;
        }

        $lesson_list = self::getLessonList($course);
        $lessonArray = array();

        foreach($lesson_list as $lesson) {
            $lessonArray[] = $lesson->splms_lesson_id;
        }

        $lessonCount = count($lessonArray);
        $lessons = implode(',', $lessonArray);

        if($lessons) {

            $completed_list  = self::getLessonCompletedList($course,$lessons);
            $completedCount = count($completed_list);
        } else {

            $completedCount = 0;
        }

        $currentCompletedList = explode(",", $course->lessons_completed_list);
        $completedListField = $currentCompletedList;

        //Don't add the lesson if it isn't in the course's lesson list or if it's already in the completed list.
        if (in_array($lesson_id, $lessonArray) && !in_array($lesson_id, $completedListField)) {

            array_push($completedListField, $lesson_id);
        }

        $currentCompletedList = explode(",", $course->lessons_completed_list);
        $completedListField = $currentCompletedList;

        $course->lessons_completed_list = implode(",", $completedListField);        //Array filter removes any empty elements
        $course->date_last_activity = date("Y-m-d H:i:s");
        $progress = 100 * (float)($completedCount / $lessonCount);

        if ($progress >= 100) {

            $progress = 100;
            $course->date_completed = date("Y-m-d H:i:s");
            $score = AxsLMS::getCourseTotalScore($course_id,$user_id);

            if($score) {
                $course->score = $score;
            }

            // Check the config option to notify an admin on completion
            // and send an email if it is enabled
            $courseData = self::getCourseById($course_id);
            $courseParams = json_decode($courseData->params);

            if ($courseParams->notify_admin == "1") {
                AxsNotifications::sendCourseCompletedAdminEmail($courseData, $user_id, $course->date_started, $course->date_completed);
            }

            // The course has been completed, so mark the course as completed for all course completion checklist items
            // for the user.
            self::markUserChecklistItemsCompletedForItemType($user_id, $course_id, 'course_completion');
        }

        $course->progress = $progress;

        $db->updateObject("joom_splms_course_progress", $course, "id");

        // Now, update all checklist items associated with this lesson to show that they have been completed
        self::markUserChecklistItemsCompletedForItemType($user_id, $lesson_id, 'lesson_completion');

        if($lesson_id && $course_id && $user_id) {

            $lessonData   = AxsLMS::getLessonById($lesson_id);
            $courseData   = AxsLMS::getCourseById($course_id);

            $lessonParams = json_decode($lessonData->params);
            $courseParams = json_decode($courseData->params);

            // Get lesson status for lesson_id and course_id and make sure we're not adding more points for a completed lesson
            if(!in_array($lesson_id, $currentCompletedList) && $lessonParams->award_points && $lessonParams->points_category && $lessonParams->points) {

                $awardData = new stdClass();
                $awardData->user_id = $user_id;
                $awardData->category_id = $lessonParams->points_category;
                $awardData->points = $lessonParams->points;
                $awardData->date = date("Y-m-d H:i:s");
                $awardData->course_id = $course_id;
                $awardData->lesson_id = $lesson_id;

                AxsAwards::awardPoints($awardData);
            }

            if($progress == 100 && $courseParams->award_points && $courseParams->points_category && $courseParams->points) {

                $awardData = new stdClass();
                $awardData->user_id = $user_id;
                $awardData->category_id = $courseParams->points_category;
                $awardData->points = $courseParams->points;
                $awardData->date = date("Y-m-d H:i:s");
                $awardData->course_id = $course_id;

                AxsAwards::awardPoints($awardData);
            }

            $awardCheckParams = new stdClass();
            $awardCheckParams->requirement_type = 'lesson';
            $awardCheckParams->lesson_id = $lesson_id;
            $awardCheckParams->course_id = $course_id;
            $award_ids = self::getAvailableBadges($awardCheckParams);

            if($award_ids) {

                self::updateBadges($award_ids, $user_id, null, null);
            }
        }
    }

    public static function getAvailableBadges($params) {
        $db = JFactory::getDBO();
        $conditions[] = $db->quoteName('enabled').'= 1';
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_awards');
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        if(empty($results)) {
            return false;
        }
        $award_ids = array();
        foreach($results as $row) {
            $awardParams = json_decode($row->params);
            $requirements = $awardParams->requirements;
            if($awardParams->requirement_type == 'conditional') {

                foreach ($requirements as $req) {

                    // We don't want to process requirements that are null or blank
                    if(empty($req)) {
                        continue;
                    }

                    //Don't check it if it's been turned off.
                    if ($req->requirement_active == "true") {

                        switch ($params->requirement_type) {

                            case "course":

                                if(!empty($req->required_course)) {

                                    $list = $req->required_course;
                                    if( in_array($params->course_id,$req->required_course) ){
                                        array_push($award_ids,$row->id);
                                    }
                                    if($req->requirement_type == 'category') {
                                        $list = $req->required_category;
                                        if ($list) {
                                            $query = "SELECT splms_coursescategory_id, splms_course_id FROM joom_splms_courses WHERE splms_course_id = ".(int)$params->course_id." AND FIND_IN_SET(splms_coursescategory_id, '" . implode(",", $list) . "')";
                                            $db->setQuery($query);
                                            $categories = $db->loadObjectList();
                                            if(!empty($categories)) {
                                                array_push($award_ids,$row->id);
                                            }
                                        }
                                    }
                                }

                                break;
                            case "event":
                                if( in_array($params->event_id,$req->required_event) ){
                                    array_push($award_ids,$row->id);
                                }
                                break;
                            case "lesson":
                                if( !empty($req->required_lesson) && in_array($params->lesson_id,$req->required_lesson) ){
                                    array_push($award_ids,$row->id);
                                }

                                $list = !empty($req->required_course) ? $req->required_course : null;

                                if( !empty($req->required_course) && in_array($params->course_id,$req->required_course) ){
                                    array_push($award_ids,$row->id);
                                }

                                if($req->requirement_type == 'category') {
                                    $list = $req->required_category;
                                    if ($list) {
                                        $query = "SELECT splms_coursescategory_id, splms_course_id FROM joom_splms_courses WHERE splms_course_id = ".(int)$params->course_id." AND FIND_IN_SET(splms_coursescategory_id, '" . implode(",", $list) . "')";
                                        $db->setQuery($query);
                                        $categories = $db->loadObjectList();
                                        if(!empty($categories)) {
                                            array_push($award_ids,$row->id);
                                        }
                                    }
                                }
                                break;

                            case "usergroup":
                                if( in_array($params->usergroup_id,$req->usergroup) ){
                                    array_push($award_ids,$row->id);
                                }
                                break;

                            case "accesslevel":
                                $list = $req->accesslevel;
                                if( in_array($params->accesslevel_id,$req->accesslevel) ){
                                    array_push($award_ids,$row->id);
                                }

                                break;
                        }
                    }
                }
            }
        }
        if(!empty($award_ids)) {
            return array_unique($award_ids);
        } else {
            return false;
        }
    }

    //The user has acquired or purchased a new course.  Add entries for it.
    public static function courseProgress_addNewCourse($user_id, $course_id, $start_date = null, $complete_date = null) {
        //Check to see if it is already in the database, just in case.

        $db = JFactory::getDBO();

        $courses = self::courseProgress_getCourseLessons($course_id);
        if (count($courses) == 0) {
            //This course does not exist.
            return false;
        }

        $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = ".$db->quote($user_id)." AND course_id = ".$db->quote($course_id);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        if(!$results) {
            $params = new stdClass();
            $params->user_id = $user_id;
            $params->course_id = $course_id;
            $alerts = new AxsAlerts($params);
            $alerts->runAutoAlert('course registration',$params);
        }

        $languages = $courses[$course_id];
        $courseStartDate = !empty($start_date) ? $start_date : null;
        $courseCompleteDate = !empty($complete_date) ? $complete_date : null;


       foreach ($languages as $lang_code => $lesson_list) {

            $newCourse = new stdClass();
            $newCourse->user_id = $user_id;
            $newCourse->course_id = $course_id;
            $newCourse->language = $lang_code;
            $newCourse->lessons_completed_list = "";
            $newCourse->lessons_total_list = implode(",", $lesson_list);
            $newCourse->params = null;
            $newCourse->progress = 0;
            $newCourse->date_started = $courseStartDate;
            $newCourse->date_last_activity = null;
            $newCourse->date_completed = $courseCompleteDate;

            $query = "SELECT * FROM joom_splms_course_progress WHERE archive_date IS NULL AND user_id = ".$db->quote($user_id)." AND course_id = ".$db->quote($course_id)." AND language = ".$db->quote($lang_code);
            $db->setQuery($query);
            $results = $db->loadObjectList();
            if(!$results) {
                $db->insertObject("joom_splms_course_progress", $newCourse);
            }
        }

        return true;
    }

    //An administrator has deleted a lesson.
    public static function courseProgress_deleteLesson($lesson_id) {
        $db = JFactory::getDBO();

        //Find all the progress listings that have lesson as part of their total.

        $query = "SELECT * FROM joom_splms_course_progress WHERE FIND_IN_SET($lesson_id, lessons_total_list)";
        $db->setQuery($query);
        $results = $db->loadObjectList();

        foreach ($results as $result) {
            //Get the list of lessons that are currently needed for the course.
            $lesson_list = explode(",", $result->lessons_total_list);
            //Find the index of the lesson that is being removed.
            $index = array_search($lesson_id, $lesson_list);
            //Remove it from the list.
            unset($lesson_list[$index]);
            //Create a new list entry without the lesson.
            $result->lessons_total_list = implode(",", $lesson_list);

            //If progress is already 0, it won't change if the lesson is removed since they never passed it.
            if ($result->progress > 0) {
                $passed_list = explode(",", $result->lessons_completed_list);

                //Check if the lesson is one they have passed.
                $index = array_search($lesson_id, $passed_list);
                if ($index !== false) {
                    unset($passed_list[$index]);
                }


                $result->progress = 100 * (float)(count($passed_list) / count($lesson_list));
            }

            $db->updateObject("joom_splms_course_progress", $result, "id");
        }
    }

    //An administrator has deleted a course.
    public static function courseProgress_deleteCourse($course_id) {

        //The course has been deleted.  Delete all progress associated with it.

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query
            ->delete($db->quoteName('joom_splms_course_progress'))
            ->where($db->quoteName('course_id') . '=' . $db->quote($course_id));

        $db->setQuery($query);
        $db->execute();

        $query->clear();

        //Delete all calendar schedules

        $type_id = "course_" . $course_id . "_due_date";

        $query
            ->delete($db->quoteName('axs_schedules'))
            ->where($db->quoteName('type_id') . '=' . $db->quote($type_id));

        $db->setQuery($query);
        $db->execute();
    }

    /**
     * Archives all the course_progress, lesson_status, quizresults, survey results,
     * student_activity data given a user/course pair
     *
     * @param string|int $user_id - the id of the user to archive progress for
     * @param string|int $course_id - the id of the course to archive progress for
     *
     * @return boolean - true on success, false on failure
     */
    public static function archiveCourseProgress($user_id, $course_id) {

        $success = true;

        $db = JFactory::getDbo();

        // Quote to prevent SQL injection
        $user_id = $db->quote($user_id);
        $course_id = $db->quote($course_id);

        $db->transactionStart();

        try {
            // Set archived date for course progress
            $db->setQuery("
                UPDATE joom_splms_course_progress
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id AND course_id = $course_id
            ");
            $db->execute();

            // Set archived date for any course completion checklist items
            $db->setQuery("
                UPDATE axs_checklist_activity
                SET archive_date = CURRENT_DATE()
                WHERE user_id = $user_id
                AND archive_date IS NULL
                AND item_type = 'course_completion'
                AND JSON_EXTRACT(params, '$.course_id') = $course_id;
            ");
            $db->execute();

            // Get all lessons based on course_id
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM joom_splms_lessons WHERE splms_course_id = $course_id");
            $lessonRows = $db->loadObjectList();

            // Create a comma-separated list of lesson ids in this course
            // to be used in subsequent queries
            $lessonIds = array();
            array_map(function ($lessonRow) use (&$lessonIds) { $lessonIds []= $lessonRow->splms_lesson_id; }, $lessonRows);
            $lessonIds = implode(',', $lessonIds);

            // Set archived date for lesson completion checklist items
            $db->setQuery("
                UPDATE axs_checklist_activity
                SET archive_date = CURRENT_DATE()
                WHERE user_id = $user_id
                AND archive_date IS NULL
                AND item_type = 'lesson_completion'
                AND FIND_IN_SET(JSON_UNQUOTE(JSON_EXTRACT(params, '$.lesson_id')),  '$lessonIds');
            ");
            $db->execute();

            // Set archived date for lesson statuses based on the lessons
            $db->setQuery("
                UPDATE joom_splms_lesson_status
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND lesson_id IN ($lessonIds)
            ");
            $db->execute();

            // Set archived date for  student activity based on lessons
            $db->setQuery("
                UPDATE joom_splms_student_activities
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND lesson_id IN ($lessonIds)
            ");
            $db->execute();

            // Set archived date for  quizresults based on lessons
            $db->setQuery("
                UPDATE joom_splms_quizresults
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND splms_lesson_id IN ($lessonIds)
            ");
            $db->execute();

            // Set archived date for  video tracking based on quizzes
            $db->setQuery("
                UPDATE axs_video_tracking
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND lesson_id IN ($lessonIds)
            ");
            $db->execute();

            // Create a comma separated list of interactive content ids and survey ids to archive
            $interactiveContentIds = array();
            array_map(function ($lessonRow) use (&$interactiveContentIds) {
                $lessonParams = json_decode($lessonRow->params);
                $studentActivities = json_decode($lessonParams->student_activities);
                foreach($studentActivities as $activity_id => $activity) {
                    if ($activity->type == 'interactive' && !empty($activity->interactive_id)) {
                        $interactiveContentIds []= $activity->interactive_id;
                    }
                    if ($activity->type == 'survey' && !empty($activity->survey)) {
                        $surveysToArchive []= $activity->survey;
                    }
                }
            }, $lessonRows);
            $interactiveContentIds = implode(',', $interactiveContentIds);

            // Update interactive content results with the archive date
            $db->setQuery("
                UPDATE ic_h5p_results
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND content_id IN ($interactiveContentIds)
            ");
            $db->execute();

            // Update interactive content user data (work on ic items that hasn't been submitted yet)
            $db->setQuery("
                UPDATE ic_h5p_contents_user_data
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND content_id IN ($interactiveContentIds)
            ");
            $db->execute();

            // Create a comma separated list of the ids of any surveys in these lessons, archive
            $surveysToArchive = array();
            foreach($lessonRows as $lessonRow) {
                $params = json_decode($lessonRow->params);
                $activites = json_decode($params->student_activities);
                foreach($activites as $activity_id => $activity) {
                    if (!empty($activity->survey) && $activity->type == 'survey') {
                        $surveysToArchive []= $activity->survey;
                    }
                }
            }
            $surveysToArchive = implode(',', $surveysToArchive);

            // $db->setQuery("SELECT * FROM axs_survey_progress WHERE archive_date IS NULL AND user_id = $user_id AND survey_id IN ($surveysToArchive)");
            // $surveyProgresses = $db->loadObjectList();
            $db->setQuery("
                UPDATE axs_survey_progress
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND survey_id IN ($surveysToArchive)
            ");
            $db->execute();

            // $db->setQuery("SELECT * FROM axs_survey_responses WHERE archive_date IS NULL AND user_id = $user_id AND survey_id IN ($surveysToArchive)");
            // $surveyResponses = $db->loadObjectList();
            $db->setQuery("
                UPDATE axs_survey_responses
                SET archive_date = CURRENT_DATE()
                WHERE archive_date IS NULL
                AND user_id = $user_id
                AND survey_id IN ($surveysToArchive)
            ");
            $db->execute();

            $db->transactionCommit();

        } catch (\Exception $e) {
            $db->transactionRollback();

			throw $e;
        }

        return true;
    }

    public static function giveAwardToUser($user_id, $award_id, $issue_date = null) {

		if(!$user_id && !$award_id) {
			return false;
		}

		if(!$issue_date) {
			$issue_date = date('Y-m-d H:i:s');
		} else {
			$issue_date = date('Y-m-d H:i:s',$issue_date);
		}

		$db = JFactory::getDBO();
		$data = new stdClass();
		$data->user_id = $user_id;
		$data->badge_id = $award_id;
		$data->date_earned = $issue_date;
		$data->date_expires = null;
		$award = AxsLearnerDashboard::getAwardById($data->badge_id);
		$params = json_decode($award->params);
        $expiration = null;

		if ($params->expiration_amount && $params->expiration_type && !$data->date_expires) {
			$date_earned = strtotime($data->date_earned);
			$expiration = date("Y-m-d H:i:s",strtotime($data->date_earned .' + '. $params->expiration_amount." ".$params->expiration_type));
			$data->date_expires = $expiration;
		}

		$badge_exist = AxsActions::checkAward($data->badge_id,$data->user_id);

		if($badge_exist) {
			$query = "DELETE FROM axs_awards_earned WHERE user_id = ".$db->quote($user_id)." AND badge_id = ".$db->quote($award_id);
			$db->setQuery($query);
			$db->execute();
		}

        $db->insertObject('axs_awards_earned',$data);

        if($params->usergroup_actions && ($params->add_usergroups || $params->remove_usergroups)) {
            $groupData = new stdClass();
            $groupData->user_id = $user_id;
            $groupData->usergroup_actions = $params->usergroup_actions;
            $groupData->add_usergroups = $params->add_usergroups;
            $groupData->remove_usergroups = $params->remove_usergroups;
            AxsActions::setGroups($groupData);
        }

        if($params->points_actions && $params->points_category && $params->points) {
            $awardData = new stdClass();
            $awardData->user_id = $user_id;
            $awardData->category_id = $params->points_category;
            $awardData->points = $params->points;
            $awardData->date = date("Y-m-d H:i:s");
            $awardData->params = json_encode(array('award_id' => $data->badge_id));
            AxsAwards::awardPoints($awardData);
        }
	}

    /**
     * Updates AICC Session vars
     *
     * @param stdClass $sessionData
     * @return true|false True if session update / creation happened normally, false if an error occurred.
     */
    public static function updateAICCSession($sessionData) {

        $db = JFactory::getDbo();

        $existingSessionData = self::retrieveAICCSession($sessionData->sessionId);

        try {

            if(!empty($existingSessionData)) {

                // Update existing session
                $existingSessionData->date_updated = date('Y-m-d H:i:s');

                $db->updateObject('axs_aicc_hacp_session', $existingSessionData, 'id');
            }

            return true;
        } catch(Exception $ex) {

            return false;
        }
    }

    public static function createAICCSession($sessionData) {

        $db = JFactory::getDbo();

        try {

            $hacpSession = new stdClass();

            $hacpSession->user_id = $sessionData->userId;
            $hacpSession->session_id = $sessionData->sessionId;
            $hacpSession->lesson_id = $sessionData->lessonId;
            $hacpSession->course_id = $sessionData->courseId;
            $hacpSession->scorm_id = $sessionData->scormId;
            $hacpSession->student_id = $sessionData->studentId;
            $hacpSession->date_created = date('Y-m-d H:i:s');
            $hacpSession->date_updated = date('Y-m-d H:i:s');

            $db->insertObject('axs_aicc_hacp_session', $hacpSession);

            return true;
        } catch(Exception $ex) {

            return false;
        }
    }

    public static function retrieveAICCSession($sessionId = null, $userId = null, $courseId = null, $scormCourseId = null) {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // Attempt to get the session by the sessionId passed in via $sessionData
        $query->select('*')
              ->from('axs_aicc_hacp_session');

        $conditions = [];

        if(!empty($sessionId)) {
            $conditions[] = 'session_id = ' . $db->quote($sessionId);
        } else {

            // If there's no user session, we cannot continue from this point.
            if(empty($userId)) {

                return null;
            }

            $conditions[] = 'user_id = ' . $db->quote($userId);
            $conditions[] = 'course_id = ' . $db->quote($courseId);
            $conditions[] = 'scorm_id = ' . $db->quote($scormCourseId);
        }

        $query->where($conditions);

        $db->setQuery($query);

        try {

            return $db->loadObject();
        } catch(Exception $ex) {

            return null;
        }
    }

    public static function destroyAICCSession($sessionId) {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->delete('axs_aicc_hacp_session')
              ->where('session_id = ' . $db->quote($sessionId));

        $db->setQuery($query);

        try {

            return $db->execute();
        } catch(Exception $ex) {

            return null;
        }
    }

    /**
     * Mark the given user's checklist items completed for a given item type and item ID
     *
     * @param integer $userId
     * @param integer $itemTypeId ID based on which type of checklist item this is (course => course ID, lesson => lesson ID, etc.)
     * @param string $itemType
     * @return bool
     */
    public static function markUserChecklistItemsCompletedForItemType(int $userId, int $itemTypeId, string $itemType) {

        // First, grab the checklist items for the current user that have the type $itemType.
        $courseCompletionChecklistItems = AxsChecklist::getChecklistItemsForUserByType($userId, $itemTypeId, $itemType);

        $checklistIds = array();

        if(is_countable($courseCompletionChecklistItems) && !is_null($courseCompletionChecklistItems)) {

            foreach($courseCompletionChecklistItems as $checklistItem) {

                $itemParams = json_decode($checklistItem->params);

                // Don't include manually checked checklist items
                if($itemParams->completion_type != 'auto') {
                    continue;
                }

                $checklistItem->completed = true;

                AxsChecklist::addChecklistActivity($checklistItem, $userId);

                $checklistIds[] = $checklistItem->checklist_id;
            }
        } else {

            return false;
        }

        $completeChecklistIds = array();

        // Filter out the checklist IDs to remove completed checklists
        foreach($checklistIds as $userChecklistId) {

            $checklistComplete = AxsChecklist::isUserChecklistComplete($userChecklistId, $userId);

            if($checklistComplete) {

                $completeChecklistIds[] = $userChecklistId;
            }
        }

        // Send checklist complete emails if any checklists associated with this operation have been completed
        foreach($completeChecklistIds as $checklistId) {

            $checklist = AxsChecklist::getChecklist($checklistId);

            $checklistParams = json_decode($checklist->params);

            // Send notification email for a completed checklist
            if ($checklistParams->notify_admin == "1") {

                // Check if an email has been sent for this user and this checklist already
                AxsNotifications::sendChecklistCompletedAdminEmail($userId, $checklist);
            } else {

                continue;
            }
        }

        return true;
    }
}

class StudentList {

    public $lesson_id;
    public $course_id;
    public $student_id;
    public $teacher_id;
    public $permissions;
    public $language;

    public function __construct($params) {
        foreach($params as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getStudents($limit = null) {

        $conditions = $this->setConditions();
        $result     = $this->buildQuery($conditions, $limit);

        return $result;
    }

    private function setConditions() {
        $db = JFactory::getDbo();
        $conditions = array();

        if($this->course_id) {
            array_push($conditions, $db->qn('c.splms_course_id').'='.(int)$this->course_id);
        }

        if($this->lesson_id) {
            array_push($conditions, $db->qn('s.lesson_id').'='.(int)$this->lesson_id);
        }

        return $conditions;
    }

    private function getActivityFiles($user) {
        $db = JFactory::getDbo();
        $baseURL = str_replace("/administrator/", "", JUri::base());
        $user_id = $user;
        $course = $this->course_id;

        if (!$course || !$user) {
            return;
        }

        $query = "
            SELECT
                student_response
            FROM
                joom_splms_student_activities
            WHERE
                course_id = $course
            AND
                user_id = $user_id
            AND
                (activity_type = 'upload' OR activity_type = 'link' OR activity_type = 'mp3' OR activity_type = 'mp4')
            ORDER BY id ASC
        ";

        $db->setQuery($query);
        $result = $db->loadObjectList();
        $array = [];
        foreach($result as $item) {
            array_push($array, $baseURL.$item->student_response);
        }
        $list = implode(',',$array);
        return $list;
    }

    private function buildQuery($conditions, $limit = null) {
        $db = JFactory::getDbo();
        $course = $this->course_id;

        if (!$course) {
            return;
        }

        if($this->language) {
            $language = $this->language;

            $setLanguage = "AND cp.language = '$language'";
        } else {
            $setLanguage = '';
        }

        $query = "
            SELECT
                cp.*,
                p.status as purchase,
                p.date as purchase_date,
                ROUND(cp.progress) as progress,
                cp.user_id as user,
                cp.course_id as course,
                t.amount as amount_paid,
                u.name as name,
                DATE_FORMAT(cp.date_started, '%m/%d/%Y') as date_started,
                DATE_FORMAT(cp.date_completed, '%m/%d/%Y') as date_completed
            FROM
                joom_splms_course_progress as cp
            LEFT JOIN axs_course_purchases as p ON p.course_id = cp.course_id AND p.user_id = cp.user_id AND p.status = 'PAID'
            LEFT JOIN axs_pay_transactions as t ON t.type_id = p.id AND t.status = 'SUC' AND refund_id IS NULL
            LEFT JOIN joom_users as u ON cp.user_id = u.id
            WHERE
                cp.archive_date IS NULL
                AND cp.course_id = $course
                $setLanguage
            GROUP BY cp.user_id
            ORDER BY cp.progress DESC
        ";

        $db->setQuery($query);
        $result = $db->loadObjectList();

        $params = new stdClass();
        $params->course_id = $course;
        $params->totals = true;
        foreach ($result as &$item) {
            $params->user_id = $item->user;
            $item->profile = AxsExtra::getUserProfileData($item->user);
            //Get all Uploaded files
            //$item->files = self::getActivityFiles($item->user);
            $comments = AxsLMS::getComments($params);

            $item->comments = is_countable($comments) ? count($comments) : 0;
        }

        return $result;
    }
}

class ActivityList {

	public $id;
	public $lesson_id;
	public $course_id;
	public $student_id;
	public $teacher_id;
	public $activity_id;
	public $permissions;

	public function __construct($params) {
  		foreach($params as $key => $value) {
  			$this->$key = $value;
  		}
  	}

  	public function getActivities($limit = null) {

  		$conditions                = $this->setConditions();
		$result 	               = $this->buildQuery($conditions, $limit);

		return $result;
  	}

	private function setConditions() {
		$db = JFactory::getDbo();
        $conditions = array();

  		if($this->course_id) {
  			array_push($conditions, $db->qn('course_id').'='.(int)$this->course_id);
  		}

  		if($this->lesson_id) {
  			array_push($conditions, $db->qn('lesson_id').'='.(int)$this->lesson_id);
  		}

  		if(isset($this->student_id)) {
  			array_push($conditions, $db->qn('user_id').'='.(int)$this->student_id);
  		}

  		if($this->activity_id) {
  			array_push($conditions, $db->qn('activity_id')." = '$this->activity_id' ");
  		}

  		return $conditions;
	}

    private function setInteractiveContentUserData($lesson,$activity) {
        if(!$content_id) {
            return false;
        }
        $content_id = $activity->interactive_id;
        $conditions = array('archive_date IS NULL');
        $conditions[] = "content_id = $content_id";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('ic_h5p_contents_user_data');
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $result) {

        }
        return $results;
    }

    private function updateActivityListWithAllContentTypes($conditions) {
        $lesson    = AxsLMS::getLessonById($this->lesson);
        $params    = json_decode($lesson->params);
        $activites = json_decode($params->student_activities);
        foreach($activites as $activity) {
            if($activity->interactive_id) {
                self::setInteractiveContentUserData($lesson,$activity);
            }
        }
    }

	private function buildQuery($conditions, $limit = null) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__splms_student_activities');
        $conditions []= "archive_date IS NULL";
		if($conditions) {
			$query->where($conditions);
		}
		if($limit) {
            $query->setLimit($limit);
            $query->order('completed DESC');

			$db->setQuery($query);
			$result = $db->loadObject();
		} else {

			$db->setQuery($query);
			$result = $db->loadObjectList();
		}
		return $result;
	}

}

class Activity {

    public $id;
  	public $course_id;
  	public $lesson_id;
  	public $activity_id;
  	public $user_id;
  	public $activity_type;
  	public $activity_request;
  	public $student_response;
  	public $teacher_comment;
  	public $grade;
  	public $request_resubmission;
  	public $completed;
  	public $params;
  	public $date_submitted;
  	public $date_modified;
  	public $file;

  	public function __construct($params) {
  		foreach($params as $key => $value) {
  			$this->$key = $value;
  		}
  	}

	public function save() {
		// foreach($params as $key => $value) {
  		// 	$this->$key = $value;
  		// }

        $db = JFactory::getDbo();

        if($this->id) {
        	$response = $db->updateObject('#__splms_student_activities',$this,'id');
        } else {
        	$response = $db->insertObject('#__splms_student_activities',$this);
        }
        return $response;
    }

    public function bind($params) {
  		foreach($params as $key => $value) {
  			$this->$key = $value;
  		}
  	}

    public function submit($save = true) {

        $response = new stdClass();
        $activityType = $this->activity_type;
        $externalVideoSources = ['youtube','vimeo','facebook','screencast'];
        $fileUploadSources = ['mp3','mp4','upload'];
        $allowedUploadTypes = null;

        if(in_array($activityType, $externalVideoSources)) {
        	  $activityType = 'externalvideo';
        }

        switch ($activityType) {
        	case 'textbox':
        		$response->message = AxsLanguage::text("COM_LMS_SUBMISSION_SAVED_SUCCESSFULLY","Your Submission Saved Successfully");
            	$response->status = "success";
        	break;

        	case 'externalvideo':
        		$result = $this->prepareExternalVideo();
        		if($result) {
        		    $this->student_response = $result;
        			$response->message = AxsLanguage::text("COM_LMS_SUBMISSION_SAVED_SUCCESSFULLY","Your Submission Saved Successfully");
            		$response->status = "success";
            	} else {
            		$response->message = AxsLanguage::text("COM_LMS_SUBMISSION_FAILED","There Was an Error With Your Submission");
            		$response->status = "failed";
            	}
        	break;

            case 'mp3':

                $allowedUploadTypes = [
                    'audio/mpeg',
                    'audio/mp3',
                    'audio/mpeg3',
                    'audio/ogg',
                    'audio/wav',
                    'audio/x-wav',
                ];
            break;

            case 'mp4':

                $allowedUploadTypes = [
                    'video/mp4',
                    'audio/mp4',
                    'audio/webm',
                    'video/webm'
                ];
            break;

        	default:
        		$response->message = AxsLanguage::text("COM_LMS_SUBMISSION_SAVED_SUCCESSFULLY","Your Submission Saved Successfully");
            	$response->status = "success";
        	break;
        }

        if(in_array($activityType, $fileUploadSources)) {

            $file = $this->upload($this->file, $allowedUploadTypes);

            if($file->status == 'success') {
                $this->student_response = $file->path;
                $response->message = AxsLanguage::text("COM_LMS_ACTIVITY_SUCCESS", "Your Submission Uploaded Successfully");
                $response->status = "success";
            } else {
                $response->message = $file->message;
                $response->status  = $file->status;
                $save = false;
            }
        }

        if($save) {
            $response->save = $this->save();
        }

        return $response;
    }

    public function prepareExternalVideo() {

    	$result = false;

    	switch ($this->activity_type) {

    		case 'youtube':
    			$url = urldecode(rawurldecode($this->student_response));
				preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
				if($matches[1]) {
					$result = $matches[1];
				} else {
					$result = $this->student_response;
				}
    		break;

    		case 'vimeo':
    			$result = str_replace("https://vimeo.com/", '', $this->student_response);
    		break;

    		case 'screencast':
    			preg_match('/src="([^"]+)"/', $this->student_response, $src);
				if($src[1]) {
	   				$result = $src[1];
	   			} else {
	   				$result = $this->student_response;
	   			}
    		break;

    		case 'facebook':
    			$result = urlencode($this->student_response);
    		break;
    	}

    	return $result;
    }

    private function upload($file, $allowedFileTypesOverride = null) {
        //Build json response
        $response = new stdClass();
        $response->message = AxsLanguage::text("COM_LMS_ACTIVITY_UPLOAD_FAILED", "Your Submission Failed to Upload");
        $response->status = "failed";

        if(isset($file['file']) && $file['file']['error'] == 0) {

            /* Get the file type from the file itself because Apache likes to map unknown MIME types
             * to application/x-octet-stream, which isn't on this list for security reasons.
             */
            $filetype = mime_content_type($file['file']['tmp_name']);

            if(!empty($allowedFileTypesOverride)) {

                $allowedFileTypes = $allowedFileTypesOverride;
            } else {

                // Make sure the uploaded file has an accepted MIME type
                $allowedFileTypes = [
                    'application/pdf',
                    'application/zip',
                    'application/x-zip-compressed',
                    'image/gif',
                    'image/jpg',
                    'image/jpeg',
                    'image/png',
                    'video/mpeg',
                    'video/mp4',
                    'audio/mp3',
                    'video/quicktime',
                    'video/x-ms-wmv',
                    'video/3gpp',
                    'audio/mpeg',
                    'audio/mp4',
                    'audio/ogg',
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.ms-excel',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-powerpoint',
                    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.documentapplication/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'text/csv',
                    'audio/wav',
                    'audio/x-wav',
                    'audio/webm',
                    'video/webm'
                ];
            }

            if(in_array($filetype, $allowedFileTypes)) {

                //build directory path
                $symlinkPath = AxsImages::getImagesPath().'/';
                $activityPath = 'courses/'.$this->course_id.'/lessons/'.$this->lesson_id.'/students/'.$this->user_id;
                $base = 'lmsfiles/';
                $pathArray = explode('/', $base.$activityPath);
                $pathCount = count($pathArray);
                $fullPath = $symlinkPath.$base.$activityPath.'/';

                $date = strtotime('now');

                for($i = 0; $i < $pathCount; $i++) {
                    if(!isset($filePath)) {
                        $filePath = $symlinkPath.'/'.$pathArray[$i];
                    } else {
                        $filePath = $filePath.'/'.$pathArray[$i];
                    }
                    if(!file_exists($filePath)) {
                        mkdir($filePath);
                        chmod($filePath, 0777);
                    }
                }

                $uploaddir = $fullPath;
                $uploadFile = basename($file['file']['name']);
                $fileurl = rand(0, 99999);
                $uploadFile = str_replace(' ', '-', $uploadFile);
                $newName = $uploaddir . $fileurl . $uploadFile;
                if(move_uploaded_file($file['file']['tmp_name'], $newName)) {
                    $response->path = $newName;
                    $response->status = "success";
                }
            } else {
                $response->message = AxsLanguage::text("COM_LMS_ACTIVITY_FILETYPE_NOT_ALLOWED", "Error: File Type Not Allowed");
            }
        }

        return $response;
    }

}
