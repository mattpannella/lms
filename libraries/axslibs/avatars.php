<?php

defined('_JEXEC') or die;

class AxsAvatars {

    public static function getAvatars() {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('id, path, title')
              ->from('axs_avatars')
              ->where('enabled = 1');

        $brandId = AxsBrands::getCurrentBrandId();

        // If $brandId is null, get the default brand
        if(empty($brandId)) {

            $defaultBrand = AxsBrands::getDefault();

            // If no default brand exists, assume that the brand with ID 1 is the default.
            $brandId = !empty($brandId) ? $defaultBrand->id : 1;
        }

        $query->andWhere("brand_id = $brandId")
              ->orWhere('brand_id = 0');

        try {

            $db->setQuery($query);
            $avatars = $db->loadObjectList();
        } catch(Exception $ex) {

            $avatars = null;
        }

        return $avatars;
    }

    /**
     * Gets an avatar record by avatarId
     *
     * @param int $avatarId Avatar ID
     * @return stdClass|null
     */
    public static function getAvatarById($avatarId) {

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_avatars')
              ->where("enabled = 1 AND id = $avatarId");

        try {

            $db->setQuery($query);
            $avatar = $db->loadObject();
        } catch(Exception $ex) {

            $avatar = null;
        } finally {

            return $avatar;
        }
    }

    public static function saveAvatarRecord($avatar) {

        if(!empty($avatar)) {

            $db = JFactory::getDbo();

            try {

                $result = $db->updateObject('axs_avatars', $avatar, 'id');
            } catch (Exception $ex) {

                $result = null;
            } finally {

                return $result;
            }
        }
    }
}