<?php

defined('_JEXEC') or die;

class AxsHTML {

	public static function getField($field) {
		switch ($field->type) {

			case 'text':
				return self::getTextBoxField($field);
			break;

			case 'textarea':
				return self::getTextAreaField($field);
			break;

			case 'select':
				return self::getSelectField($field);
			break;

			case 'list':
				return self::getSelectField($field);
			break;

			case 'radio':
				return self::getRadioField($field);
			break;

			case 'checkbox':
				return self::getCheckBoxField($field);
			break;

			case 'country':
				return self::getCountryField($field);
			break;

			case 'gender':
				return self::getGenderField($field);
			break;

			case 'email':
				return self::getEmailField($field);
			break;

			case 'time':
				return self::getTimeField($field);
			break;

			case 'date':
				return self::getDateField($field);
			break;

			case 'url':
				return self::getUrlField($field);
			break;

			case 'birthdate':
				return self::getDateField($field);
			break;

			default:
				return self::getTextBoxField($field);
			break;
		}

	}

	private static function getSelectField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$multiple  = '';
		$height	   = '';
		$brackets  = '';
		$fieldParams = json_decode($field->params);

		JHtml::_('script', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.full.js');
        JHtml::_('stylesheet', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
	    JHtml::_('stylesheet', 'https://cdn.jsdelivr.net/npm/select2-bootstrap-5-theme@1.2.0/dist/select2-bootstrap-5-theme.min.css');

		if($field->options) {
			$options = explode(PHP_EOL,$field->options);
			$optionCount = count($options);
		}

		if($field->type == 'list') {
			$multiple = ' multiple="true" ';
			$brackets = '[]';
			if($optionCount > 1) {
				$height	= ' style="height: '. 25 * $optionCount .'px" ';
			}
		}

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html  = '<select class="form-control" name="field'.$field->id.$brackets.'" '.$readonly.' '.$tooltip.' '.$required.' '.$multiple.' '.$height.'>';
		if($field->type == 'select') {
			$html  .= '<option value="">--Select--</option>';
		}

		foreach($options as $option) {
			if($field->value) {
				$values = explode(',',$field->value);
			}
			if(in_array($option,$values)) {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}
			$html  .= '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
		}

		$html .= '</select>';
		$html .= '
			<script>
			$(`[name="field'. $field->id . $brackets . '"`).select2({
				theme: "bootstrap-5",
				closeOnSelect: false,
			})
			</script>
		';

		return $html;
	}

	private static function getTextBoxField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$maxlength = '';
		$minlength = '';

		$fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($fieldParams->maxlength) {
			$maxlength = ' maxlength="'.$fieldParams->maxlength.'" ';
		}

		if($fieldParams->max_char) {
			$maxlength = ' maxlength="'.$fieldParams->max_char.'" ';
		}

		if($fieldParams->min_char) {
			$minlength = ' minlength="'.$fieldParams->min_char.'" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html = '<input type="text" class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$maxlength.' '.$minlength.' '.$required.' value="'.$field->value.'"/>';

		return $html;

	}

	private static function getTextAreaField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$maxlength = '';
		$minlength = '';

		$fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($fieldParams->maxlength) {
			$maxlength = ' maxlength="'.$fieldParams->maxlength.'" ';
		}

		if($fieldParams->max_char) {
			$maxlength = ' maxlength="'.$fieldParams->max_char.'" ';
		}

		if($fieldParams->min_char) {
			$minlength = ' minlength="'.$fieldParams->min_char.'" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html = '<textarea style="height: 80px;"  class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$maxlength.' '.$minlength.' '.$required.'>'.$field->value.'</textarea>';

		return $html;

	}

	private static function getRadioField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$html      = '';
		$fieldParams = json_decode($field->params);

		if($field->options) {
			$options = explode(PHP_EOL,$field->options);
			$optionCount = count($options);
		}

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		foreach($options as $option) {
			if($field->value == $option) {
				$selected = 'checked="checked"';
			} else {
				$selected = '';
			}
			$html .= '<div class="form-check"><input class="form-check-input" type="radio" value="'.$option.'" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$required.' '.$selected.'/> <label class="form-check-label">'.$option.'</label></div>';
		}

		return $html;

	}

	private static function getCheckBoxField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$html      = '';
		$fieldParams = json_decode($field->params);

		if($field->options) {
			$options = explode(PHP_EOL,$field->options);
			$optionCount = count($options);
		}

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		foreach($options as $option) {
			if($field->value) {
				$values = explode(',',$field->value);
			}
			if(in_array($option,$values)) {
				$selected = 'checked="checked"';
			} else {
				$selected = '';
			}
			$html .= '<div class="form-check"><input class="form-check-input" type="checkbox" value="'.$option.'" name="field'.$field->id.'[]" '.$readonly.' '.$tooltip.' '.$selected.'/> <label class="form-check-label">'.$option.'</label></div>';
		}

		return $html;
	}

	public static function getCountryList() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('options')
			  ->from('#__community_fields')
			  ->where("fieldcode = 'FIELD_COUNTRY' AND type = 'select'")
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result->options;
	}

	public static function getFieldByFieldCode($fieldcode) {
		if(!$fieldcode) {
			return false;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields')
			  ->where("fieldcode = '$fieldcode'")
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	public static function getFieldById($id) {
		if(!$id) {
			return false;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields')
			  ->where("id = $id")
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	private static function getCountryField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';

		$fieldParams = json_decode($field->params);
		$countryList = self::getCountryList();

		if($countryList) {
			$options = explode(PHP_EOL,$countryList);
			$optionCount = count($options);
		}

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html  = '<select class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$required.'>';

		$html  .= '<option value="">--Select--</option>';

		foreach($options as $option) {
			if($field->value == $option) {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}
			$html  .= '<option value="'.$option.'" '.$selected.'>'.$option.'</option>';
		}

		$html .= '</select>';

		return $html;
	}

	private static function getGenderField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';

		$fieldParams = json_decode($field->params);
		$options = array("COM_COMMUNITY_MALE" => "COM_COMMUNITY_MALE", "COM_COMMUNITY_FEMALE" => "COM_COMMUNITY_FEMALE");

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html  = '<select class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$required.'>';

		$html  .= '<option value="">--Select--</option>';

		foreach ($options as $key => $val) {
			if($field->value == $key) {
				$selected = 'selected="selected"';
			} else {
				$selected = '';
			}
            $html .= '<option value="'.$key.'" '.$selected.'>'.JText::_($val).'</option>';
        }

		$html .= '</select>';

		return $html;
	}

	private static function getEmailField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$maxlength = '';
		$minlength = '';

		$fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($fieldParams->maxlength) {
			$maxlength = ' maxlength="'.$fieldParams->maxlength.'" ';
		}

		if($fieldParams->max_char) {
			$maxlength = ' maxlength="'.$fieldParams->max_char.'" ';
		}

		if($fieldParams->min_char) {
			$minlength = ' minlength="'.$fieldParams->min_char.'" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}

		$html = '<input type="email" class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$maxlength.' '.$minlength.' '.$required.' value="'.$field->value.'"/>';

		return $html;

	}

	private static function getTimeField($field) {
		$html      = '';
        $hour      = '';
        $minute    = 0;
        $second    = '';
        $required  = '';
		$readonly  = '';

        $fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

        $hours = array();
        for($i=0; $i<24; $i++)
        {
            $hours[] = ($i<10)? '0'.$i : $i;
        }

        $minutes = array();
        for($i=0; $i<60; $i++)
        {
            $minutes[] = ($i<10)? '0'.$i : $i;
        }

        $seconds = array();
        for($i=0; $i<60; $i++)
        {
            $seconds[] = ($i<10)? '0'.$i : $i;
        }

        $html .= '<div class="form-group row">';
        $html .= '<div class="col-md-4 text-center">';
        $html .= '<select class="form-control" name="field' . $field->id . '[]" '.$readonly.''.$required.'>';
        for( $i = 0; $i < count($hours); $i++)
        {
            if($hours[$i]==$hour)
            {
                $html .= '<option value="' . $hours[$i] . '" selected="selected">' . $hours[$i] . '</option>';
            }
            else
            {
                $html .= '<option value="' . $hours[$i] . '">' . $hours[$i] . '</option>';
            }
        }
        $html .= '</select> Hour';
        $html .= '</div>';
        $html .= '<div class="col-md-4 text-center">';
        $html .= '<select class="form-control" name="field' . $field->id . '[]" >';
        for( $i = 0; $i < count($minutes); $i++)
        {
            if($minutes[$i]==$minute)
            {
                $html .= '<option value="' . $minutes[$i] . '" selected="selected">' . $minutes[$i] . '</option>';
            }
            else
            {
                $html .= '<option value="' . $minutes[$i] . '">' . $minutes[$i] . '</option>';
            }
        }
        $html .= '</select> Minute';
        $html .= '</div>';
        $html .= '<div class="col-md-4 text-center">';
        $html .= '<select class="form-control" name="field' . $field->id . '[]" >';
        for( $i = 0; $i < count($seconds); $i++)
        {
            if($seconds[$i]==$second)
            {
                $html .= '<option value="' . $seconds[$i] . '" selected="selected">' . $seconds[$i] . '</option>';
            }
            else
            {
                $html .= '<option value="' . $seconds[$i] . '">' . $seconds[$i] . '</option>';
            }
        }
        $html .= '</select> Second';
        $html .= '</div>';
        $html .= '</div>';

        return $html;

	}

	private static function getDateField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$maxrange  = '';
		$minrange  = '';

		$fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($fieldParams->maxrange) {
			$maxrange = ' max="'.$fieldParams->maxrange.'" ';
		}
		if($fieldParams->minrange) {
			$minrange = ' minlength="'.$fieldParams->minrange.'" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}
		if($field->value) {
			$date = date("Y-m-d",strtotime($field->value));
		} else {
			$date = "";
		}

		$html = '<input type="date" class="form-control" name="field'.$field->id.'" '.$readonly.' '.$tooltip.' '.$minrange.' '.$maxrange.' '.$required.' value="'.$date.'"/>';

		return $html;

	}

	private static function getUrlField($field) {
		$tooltip   = '';
		$required  = '';
		$readonly  = '';
		$maxlength = '';
		$minlength = '';

		$fieldParams = json_decode($field->params);

		if($fieldParams->readonly) {
			$readonly = ' readonly="true" ';
		}

		if($fieldParams->maxlength) {
			$maxlength = ' maxlength="'.$fieldParams->maxlength.'" ';
		}

		if($fieldParams->max_char) {
			$maxlength = ' maxlength="'.$fieldParams->max_char.'" ';
		}

		if($fieldParams->min_char) {
			$minlength = ' minlength="'.$fieldParams->min_char.'" ';
		}

		if($field->required && !$field->disableRequired) {
			$required = ' required="true" ';
		}

		if($field->tips) {
			$tooltip = ' data-toggle="tooltip" title="'.$field->tips.'" ';
		}
		$url = str_replace(array('http://','https://'),'',$field->value);
		$html = '<div class="input-group">
					<span class="input-group-text">https://</span>
					<input type="text" class="form-control" name="field'.$field->id.'[]" '.$readonly.' '.$tooltip.' '.$maxlength.' '.$minlength.' '.$required.' value="'.$url.'"/>
            	</div>';

		return $html;
	}

}