<?php

defined('_JEXEC') or die;

class AxsLandingPages {

	public static function getLandingPage($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from($db->qn('axs_homepage_templates'))
			  ->where($db->qn('id') .'='. (int)$id);
		$db->setQuery($query);
		$result = $db->loadObject();		
		$homepage = json_decode($result->data);

		return $homepage;
	}

	public static function getLoginPage($id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from($db->qn('axs_login_pages'))
			  ->where($db->qn('id') .'='. (int)$id);
		$db->setQuery($query);
		$result = $db->loadObject();		
		$params = json_decode($result->params);

		return $params;
	}
}