<?php
echo "here";
error_reporting(E_ALL);
ini_set('display_errors', 1);

?>

<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/libraries/axslibs/scorm/assets/css/uploader.css">


<form action="" method="post" id="scormUploadForm" enctype="multipart/form-data">
    <div id="scorm-uploader-message"></div>    
    <div id="scorm-uploader">
        <div class="input-group col-md-4">
            <span class="input-group-addon"><i class="fa fa-file"></i></span>
            <input type="text" data-id="scorm_upload" readonly  class="scorm_upload form-control input-md scorm_file" name="filename"  placeholder="Upload SCORM Package"/>
            <input id="scorm_upload" style="display: none;" type="file" name="filedata"  class="file"/>                       
            <span class="input-group-btn">
                <span class="browse btn btn-primary input-md" data-id="scorm_upload" style="border-bottom-left-radius: 0; border-top-left-radius: 0;"><i class="fa fa-search"></i> Browse</span>
            </span>
            <span class="input-group-btn"> 
            <span class="scorm-submit upload-scorm btn btn-success input-md" style="margin-left: 10px; border-radius: 5px; display: none;"  data-task="uploadActivity"><i class="fa fa-upload"></i> Upload</span>
            </span>           
        </div>
    </div>
</form>

<script>
	var sendURL   =  '<?php echo $course->CourseAsyncUrl; ?>';
	var returnURL =  '<?php echo $course->returnUrl; ?>';
</script>
<script src="/libraries/axslibs/scorm/assets/js/uploader.js"></script>
