<?php

header('Access-Control-Allow-Origin: *');


if(isset($_GET['success'])) {
	$result = new stdClass();
	$result->success  = $_GET['success'];
	$result->started  = $_GET['started'];
	$result->courseid = $_GET['courseid'];
	$result->tokenid  = $_GET['tokenid'];

	echo json_encode($result);
}

?>
