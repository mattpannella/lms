<?php

header('Access-Control-Allow-Origin: *');

require_once('../ScormLMS.php');

if(isset($_GET['success'])) {
	$result = new stdClass();
	$result->success  = $_GET['success'];
	$result->started  = $_GET['started'];
	$result->courseid = $_GET['courseid'];
	$result->tokenid  = $_GET['tokenid'];

	echo json_encode($result);
}

if($_GET['getData'] == 1) {
	$courseInfo = new stdClass();
	$courseInfo->courseID = $_GET['courseid'];
	$scormLMS = new AxsScormLMS($courseInfo);
	$metadata = $scormLMS->ScormService->getCourseService()->GetCourseDetail($scormLMS->courseID);

	echo $metadata;
}
?>
