<?php

/* Software License Agreement (BSD License)
 *
 * Copyright (c) 2010-2017, Rustici Software, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Rustici Software, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>untitled</title>
<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>
<?php

require_once('config.php');
require_once('../ScormEngineService.php');

global $CFG;

$ServiceUrl = $CFG->scormcloudurl;
$AppId = $CFG->scormcloudappid;
$SecretKey = $CFG->scormcloudsecretkey;
$Origin = $CFG->scormcloudorigin;

$ScormService = new ScormEngineService($ServiceUrl,$AppId,$SecretKey,$Origin);
$courseService = $ScormService->getCourseService();

$courseId = uniqid();
$interstitial = $CFG->wwwroot."/AsyncResult.php";
$asyncImportUrl = $courseService->GetImportCourseAsyncUrl($courseId, $interstitial);
//echo $asyncImportUrl.'<br/><br/>';
?>

<form action="" method="post" id="scormUploadForm" enctype="multipart/form-data">
    <div id="scorm-uploader-message"></div>
    <div id="scorm-uploader">
        <div class="input-group col-md-3">
            <span class="input-group-addon"><i class="fa fa-file"></i></span>
            <input type="text" data-id="scorm_upload" readonly  class="scorm_upload form-control input-md scorm_file" name="filename"  placeholder="Upload SCORM Package"/>
            <input id="scorm_upload" style="display: none;" type="file" name="filedata"  class="file"/>                       
            <span class="input-group-btn">
                <span class="browse btn btn-primary input-md" data-id="scorm_upload"><i class="fa fa-search"></i> Browse</span>
            </span>
        </div>                                     
        <span class="scorm-submit upload-scorm btn btn-success input-md" style="margin-top: 10px; display: none;"  data-task="uploadActivity"><i class="fa fa-upload"></i> Upload SCORM Package</span>
    </div>
</form>

            <script>
                $(document).on('click', '.scorm_file, .browse', function(e) {
                    e.preventDefault();
                    var id = $(this).data('id');
                    var file = $('#'+id);
                    file.trigger('click');
                    file.change( function() {
                        $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
                        $('.scorm-submit').show();
                    }); 
                });

                function courseData(id) {
                    var id = id;
                    $.ajax({
                        url  : 'https://jeremy.tovuti.io/libraries/axslibs/scorm/samples/CourseDetailSample.php?courseid='+id,
                        type : 'get',
                        success : function(response) {
                            console.log(response);
                            var iconSuccess = '<span class="fa fa-check-circle-o"></span> Successfully Uploaded!';
                             $('#scorm-uploader-message').html(iconSuccess);
                            window.parent.sendCourseID(response);
                        },
                        statusCode: {
                            500: function($result) {
                                console.log('round 2');
                                console.log($result);
                                courseData(id);
                            }
                        }
                    });
                }

                function sendData(data, url) {
                    if(data) {
                        var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
                        var loading = '<span class="fa fa-spinner fa-spin"></span> Uploading...';
                        $('#scorm-uploader').hide();
                        $('#scorm-uploader-message').show();
                        $('#scorm-uploader-message').html(loading);
                        $.ajax({
                            url  : url,
                            data : data,
                            enctype: 'multipart/form-data',
                            processData: false,
                            contentType: false,
                            type : 'post'
                            //dataType: "jsonp"
                        }).done( function(id) {
                            courseData(id);
                        });
                    }
                }

                $('.scorm-submit').click( function(e) {
                    e.preventDefault();
                    var url  = '<?php echo $asyncImportUrl; ?>';                    
					var data   = new FormData($('#scormUploadForm')[0]);
                    sendData(data, url);                   
                });


                
            </script>

</body>
</html>
