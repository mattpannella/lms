    $(document).on('click', '.scorm_file, .browse', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var file = $('#'+id);
        file.trigger('click');
        file.change( function() {
            $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
            $('.scorm-submit').show();
        }); 
    });

    function courseData(id) {
        var cid = id;
        $.ajax({
            url  : '/index.php?format=raw&option=com_splms&task=scorm.getCourseMetaData&courseid='+cid,
            type : 'get',
            success : function(response) {
                var iconSuccess = '<span class="fa fa-check-circle-o"></span> Successfully Uploaded!';
                $('#scorm-uploader-message').html(iconSuccess);
                window.parent.sendCourseID(response);
            },
            statusCode: {
                500: function($result) {
                    courseData(cid);
                }
            }
        });
    }

    function sendData(data, url) {
        if(data) {
            var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
            var loading     = '<span class="fa fa-spinner fa-spin"></span> Uploading...';
            var building    = '<span class="fa fa-cog fa-spin"></span> Building Course...';
            var error       = '<span style="color: red;"><b>There was an error uploading your scorm package.</b></span>';
            $('#scorm-uploader').hide();
            $('#scorm-uploader-message').show();
            $('#scorm-uploader-message').html(loading);
            $.ajax({
                url  : url,
                data : data,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type : 'post'
                //dataType: "jsonp"
            }).done( function(response) {
                var result = JSON.parse(response);
                if(result.success == 'true') {
                    courseData(result.courseid);
                    $('#scorm-uploader-message').html(building);
                } else {
                    $('#scorm-uploader-message').html(error);
                    $('#scorm-uploader').show();
                }
            });
        }
    }

    $('.scorm-submit').click( function(e) {
        e.preventDefault();
        var url  = sendURL;                    
		var data   = new FormData($('#scormUploadForm')[0]);
        sendData(data, url);                   
    });