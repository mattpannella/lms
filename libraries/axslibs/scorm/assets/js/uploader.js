$(document).on('click', '.scorm_file, .browse', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var file = $('#'+id);
    file.trigger('click');
    file.change( function() {
        $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
        $('.scorm-submit').show();
    }); 
});

function sendData(data, url) {
    if(data) {
        var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
        var loading     = '<span class="fa fa-spinner fa-spin"></span> Uploading...';
        var building    = '<span class="fa fa-cog fa-spin"></span> Building Course...';
        var error       = '<span style="color: red;"><b>There was an error uploading your scorm package.</b></span>';
        $('#scorm-uploader').hide();
        $('#scorm-uploader-message').show();
        $('#scorm-uploader-message').html(loading);
        $.ajax({
            url  : url,
            data : data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type : 'post'
        }).done( function(response) {
            var result = JSON.parse(response);
            console.dir(result);
            if(result) {
                var message = '<span style="color: red;"><b>'+result.message+'</b></span>';
                if(result.status == 'success') {
                    window.parent.getSCORMData(result);
                    var message = '<span class="fa fa-check-circle-o"></span> '+result.message;
                    $('#scorm-uploader-message').html(message);
                } else {
                    $('#scorm-uploader-message').html(message);
                    $('#scorm-uploader').show();
                }
            } else {
                $('#scorm-uploader-message').html(error);
                $('#scorm-uploader').show();
            }
        });
    }
}

$('.scorm-submit').click( function(e) {
    e.preventDefault();
    var url  = sendURL;                    
	var data   = new FormData($('#scormUploadForm')[0]);
    sendData(data, url);                   
});