<?php

defined('_JEXEC') or die;

class AxsAwards {

	public static function awardPoints($data) {
		$db = JFactory::getDbo();

		/* Get a points object that matches the data, if any. If there exists points for a given lesson_id, course_id
		 * and user_id, skip the insert as it will be duplicate data.
		 */

		if(!empty($data->course_id) || !empty($data->lesson_id)) {
			$conditions = [
				'user_id = ' . $data->user_id
			];

			if(!empty($data->course_id)) {
				$conditions[] = 'course_id = ' . $data->course_id;
			} else {
				$conditions[] = 'course_id IS NULL';
			}

			if(!empty($data->lesson_id)) {
				$conditions[] = 'lesson_id = ' . $data->lesson_id;
			} else {
				$conditions[] = 'lesson_id IS NULL';
			}

			$query = $db->getQuery(true);

			$query->select('*')
				  ->from('axs_points')
				  ->where($conditions);

			$db->setQuery($query);

			try {
				$results = $db->loadObjectList();

				if(empty($results)) {

					$db->insertObject("axs_points",$data);
				}
			} catch(Exception $ex) {}
		} else {

			// Make sure we don't bork the method for awarding points from non-course / lesson sources
			try {

				$db->insertObject('axs_points', $data);
			} catch(Exception $ex) {}
		}
	}

	public static function getLanguageOverrides($award) {

	}

	public static function removePoints($data) {
		$db = JFactory::getDBO();
		$conditions[] = "category_id = ".(int)$data->category_id;
		$conditions[] = "user_id = ".(int)$data->user_id;
		$conditions[] = "points = ".(int)$data->points;
		$query = $db->getQuery(true);
		$query->delete("axs_points")
			  ->where($conditions);
		$query = $query.' LIMIT 1';
		$db->setQuery($query);
		$result = $db->execute();
		return $result;
	}
}