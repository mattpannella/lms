<?php

defined('_JEXEC') or die;

require_once JPATH_LIBRARIES . '/axslibs/encryption.php';

require_once JPATH_LIBRARIES . '/php-jwt-master/src/JWT.php';
require_once JPATH_LIBRARIES . '/php-jwt-master/src/SignatureInvalidException.php';
require_once JPATH_LIBRARIES . '/php-jwt-master/src/ExpiredException.php';
require_once JPATH_LIBRARIES . '/php-jwt-master/src/BeforeValidException.php';
use \Firebase\JWT\JWT;


class AxsApiPortal {

	public static $key = "dTiFK3YYP2h9jyBWsj7NQFHCd58PCbxBE7NxMeMwxw5sNl0DfbUWa26DIqScG3r";
	public static $encrypt_key = "eqMtD9EaouMuV38tlaepAj6rkKiN7WlcTKfqc0Fot4b63ajaDfdppjVk9fuvw6t";

	public static function makeJWT($user = null) {
		if ($user == null) {
			return;
		}

		$data->adm = self::getAdminInfo($user->uid);
		$data->gtw = self::getGateways($user->uid);

		$payload->usr = $user;
		$payload->dat = AxsEncryption::encrypt($data, self::$encrypt_key);

		$jwt = JWT::encode($payload, self::$key, 'HS256', null, $header);

		return $jwt;
	}

	public static function getUserInfoById($userId) {
		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_users WHERE id = '" . $userId . "'";
		$db->setQuery($query);
		$dbUser = $db->loadObjectList()[0];

		$user = self::getUserInfoFromDBObject($dbUser);

		return $user;
	}

	public static function getUserInfoFromDBObject($userData) {
		$user = new stdClass();
		$user->uid = $userData->id;
		$user->eml = $userData->email;
		$user->name = $userData->name;
		$user->brd = "Test";

		return $user;
	}

	public static function getGateways($userId) {
		$gateway = array();

		$gateway[555] = new stdClass();
		$gateway[555]->type = "converge";
		$gateway[555]->uid = "000314";
		$gateway[555]->mid = "000314";
		$gateway[555]->pin = "L5GHNVQD4GA7IVO0WTSXOBJ37PF3ZM77WU81EBRIIMK7BD2LCN1VOY9SVWLNIYK0";
		$gateway[555]->roles = ['charge', 'refund'];

		$gateway[1234] = new stdClass();
		$gateway[1234]->type = "converge";
		$gateway[1234]->uid = 512;
		$gateway[1234]->mid = 512;
		$gateway[1234]->pin = 55555;
		$gateway[1234]->roles = ['charge', 'refund'];

		$gateway[4321] = new stdClass();
		$gateway[4321]->type = "converge";
		$gateway[4321]->uid = 1024;
		$gateway[4321]->mid = 1024;
		$gateway[4321]->pin = 55555;
		$gateway[4321]->roles = ['charge', 'refund'];

		return $gateway;
	}

	public static function getAdminInfo($userId) {
		$admin = array();		

		$admin[512] = new stdClass();
		$admin[512]->name = "Justin Lloyd";
		$admin[512]->roles = ["super", "admin"];

		$admin[256] = new stdClass();
		$admin[256]->name = "Jeremy Morris";
		$admin[256]->roles = ["super", "admin"];

		$admin[768] = new stdClass();
		$admin[768]->name = "Max Runia";
		$admin[768]->roles = ["super", "admin"];
	}	
}

?>