<?php

defined('_JEXEC') or die;

class AxsSecurity {

    public static function getFailedAttemptByIP($ipAddress) {
		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('ip_address').'='.$db->quote($ipAddress);
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_failed_logins');
		$query->where($conditions);
		$query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return $result;
		} else {
			return false;
		}
	}


	/**
     * Return Security Settings
     *
     * @return object $response
     *
     */

	public static function getSettings() {
		return AxsUser::getGlobalUserSettings();
	}

	/**
     * Validate Password Strength
     *
     * @param string $password
     *
     * @return object $response
     *
     */

	public static function validatePassword($password) {
		if(!$password) {
			return false;
		}
		$linebreak = '<br><hr>';
		$response = new stdClass();
		$response->success = true;

        $securitySettings = AxsSecurity::getSettings();
		if(!isset($securitySettings->custom_password_strength) || !$securitySettings->custom_password_strength) {
			return $response;
		}
		$passwordCharacters = str_split($password);
		$characterCount = count($passwordCharacters);
		$min_characters = $securitySettings->min_characters;
		$min_uppercase  = $securitySettings->min_uppercase;
		$min_lowercase  = $securitySettings->min_lowercase;
		$min_special    = $securitySettings->min_special;
		$min_numbers    = $securitySettings->min_numbers;

		$numbers   = 0;
		$uppercase = 0;
		$lowercase = 0;
		$special   = 0;

		forEach($passwordCharacters as $character) {
			if(preg_match('@[0-9]@', $character)) {
				$numbers++;
			} elseif(preg_match('@[A-Z]@', $character)) {
				$uppercase++;
			} elseif(preg_match('@[a-z]@', $character)) {
				$lowercase++;
			} elseif(preg_match('@[^\w]@', $character)) {
				$special++;
			}
		}

		$response->error  = AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_STRENGTH", "Password is not strong enough.").$linebreak;

		if($min_characters > $characterCount) {
			$response->success = false;
			$response->error .= str_replace('[min_characters]', $min_characters, AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_CHARACTERS", "Password Minimum Characters Required: [min_characters]")).$linebreak;
		}
		if($min_uppercase > $uppercase) {
			$response->success = false;
			$response->error .= str_replace('[min_uppercase]', $min_uppercase, AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_UPPERCASE_CHARACTERS", "Password Minimum Uppercase Characters Required: [min_uppercase]")).$linebreak;
		}
		if($min_lowercase > $lowercase) {
			$response->success = false;
			$response->error .= str_replace('[min_lowercase]', $min_lowercase, AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_LOWERCASE_CHARACTERS", "Password Minimum Lowercase Characters Required: [min_lowercase]")).$linebreak;
		}
		if($min_special > $special) {
			$response->success = false;
			$response->error .= str_replace('[min_special]', $min_special, AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_SPECIAL_CHARACTERS", "Password Minimum Special Characters Required: [min_special]")).$linebreak;
		}
		if($min_numbers > $numbers) {
			$response->success = false;
			$response->error .= str_replace('[min_numbers]', $min_numbers, AxsLanguage::text("AXS_PASSWORD_FAIL_MINIMUM_NUMBERS", "Password Minimum Numbers Required: [min_numbers]")).$linebreak;
		}
		if($response->success) {
			$response->error = '';
		}
		return $response;
	}

	public static function alphaNumericOnly($input) {
		return preg_replace("/[^A-Za-z0-9-. ]/", "",$input);
	}

    public static function cleanInput($input) {
		$input = strip_tags($input,'<b><table><tr><th><td><tbody><thead><b><i><p><div><span><h1><h2><h3><h4><a><em><img><ol><ul><li><br><br/>');
		$inlineScript = '/on\w+[\s\n\t]*=/i';
		$input = preg_replace($inlineScript,'',$input);
        return $input;
    }

    public static function checkCanLogin() {
        $response = new stdClass();
        $response->status  	= "success";
        $globalUserSettings = AxsUser::getGlobalUserSettings();
		if($globalUserSettings->max_login_attempts) {
			$ipAddress = AxsSecurity::getIPAddr();
			$failedAttempt = AxsSecurity::getFailedAttemptByIP($ipAddress);
			if(!empty($failedAttempt)) {
				(int)$time_diff_min = round(abs(time() - strtotime($failedAttempt->date_time)) / 60,2);
				(int)$time_diff_hours = round(abs(time() - strtotime($failedAttempt->date_time)) / 3600,2);
				$inWaitingPeriod = false;
				switch($globalUserSettings->max_login_waiting_period) {
					case '30 minutes':
						if($time_diff_min < 30) {
							$inWaitingPeriod = true;
						}
					break;
					case '1 hour':
						if($time_diff_hours < 1) {
							$inWaitingPeriod = true;
						}
					break;
					case '2 hours':
						if($time_diff_hours < 2) {
							$inWaitingPeriod = true;
						}
					break;
					case '24 hours':
						if($time_diff_hours < 24) {
							$inWaitingPeriod = true;
						}
					break;
					default:
						if($time_diff_hours < 1) {
							$inWaitingPeriod = true;
						}
					break;
				}
				if($failedAttempt->failed_attempts >= $globalUserSettings->max_failed_amount && $inWaitingPeriod) {
					$response->status  	= "fail";
					$response->message 	=  "ERROR: Too many failed login attempts. Please try again in $globalUserSettings->max_login_waiting_period.";
				}
			}
        }
        return $response;
    }

    public static function storeFailedAttempt($data) {
		$db = JFactory::getDbo();
		$ipAddress = AxsSecurity::getIPAddr();
		$date = date('Y-m-d H:i:s');
		$failedAttempt = AxsSecurity::getFailedAttemptByIP($ipAddress);
		if($failedAttempt) {
			$failedAttempt->date_time = $date;
			$failedAttempt->failed_attempts++;
			if(isset($data['user_id'])) {
				$failedAttempt->user_id = $data['user_id'];
			}
			if(isset($data['username'])) {
				$failedAttempt->username = $data['username'];
			}
			$result = $db->updateObject('axs_failed_logins',$failedAttempt,'id');
		} else {
			$newFailedAttempt = new stdClass();
			$newFailedAttempt->date_time = $date;
			$newFailedAttempt->failed_attempts = 1;
			$newFailedAttempt->ip_address = $ipAddress;
			$newFailedAttempt->username = $data['username'];
			if(isset($data['user_id'])) {
				$newFailedAttempt->user_id = $data['user_id'];
			}
			$result = $db->insertObject('axs_failed_logins',$newFailedAttempt);
		}
		if($result) {
			return true;
		} else {
			return false;
		}
    }

	public static function enableTwoFactor($enable = true,$type = null) {
		if($enable) {
			$enable = 1;
		} else {
			$enable = 0;
		}

		if(!$type) {
			return;
		}

		switch($type) {
			case 'yubikey':
				$plugin_name = 'plg_twofactorauth_yubikey';
			break;
			case 'otp':
				$plugin_name = 'plg_twofactorauth_totp';
			break;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		if($enable === 0) {
			$conditions[] = $db->quoteName('name').' IN ("plg_twofactorauth_yubikey",
			"plg_twofactorauth_totp")';
		} else {
			$conditions[] = $db->quoteName('name').'='.$db->quote($plugin_name);
		}

		$query->update('#__extensions')
			  ->set($db->quoteName('enabled').'='.(int)$enable)
			  ->where($conditions);
		switch($type) {
			case 'yubikey':
				$pugin_name = 'plg_twofactorauth_yubikey';
			break;
			case 'otp':
				$plugin_name = 'plg_twofactorauth_totp';
			break;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		if($enable === 0) {
			$conditions[] = $db->quoteName('name').' IN ("plg_twofactorauth_yubikey",
			"plg_twofactorauth_totp")';
		} else {
			$conditions[] = $db->quoteName('name').'='.$db->quote($plugin_name);
		}

		$query->update('#__extensions')
			  ->set($db->quoteName('enabled').'='.(int)$enable)
			  ->where($conditions);
		$db->setQuery($query);
		$result = $db->execute();
		if($result) {
			return true;
		} else {
			return false;
		}
    }

    public static function purgeFailedAttempts() {
        $globalUserSettings = AxsUser::getGlobalUserSettings();
        if($globalUserSettings->max_login_attempts && $globalUserSettings->max_login_waiting_period) {
            $db = JFactory::getDbo();
            $newTime = strtotime("-$globalUserSettings->max_login_waiting_period");
            $adjustedTime = date('Y-m-d H:i:s', $newTime);
            $conditions[] = $db->quoteName('date_time').' < '.$db->quote($adjustedTime);
            $query = $db->getQuery(true);
            $query->delete('axs_failed_logins');
            $query->where($conditions);
            $db->setQuery($query);
            $result = $db->execute();
        }
		if($result) {
			return $result;
		} else {
			return false;
		}
    }

    public static function deleteFailedAttempt() {
        $ipAddress = AxsSecurity::getIPAddr();
        if($ipAddress) {
            $db = JFactory::getDbo();
            $conditions[] = $db->quoteName('ip_address').'='.$db->quote($ipAddress);
            $query = $db->getQuery(true);
            $query->delete('axs_failed_logins');
            $query->where($conditions);
            $query->setLimit(1);
            $db->setQuery($query);
            $result = $db->execute();
        }
		if($result) {
			return $result;
		} else {
			return false;
		}
    }

    public static function getIPAddr() {
		$keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
		foreach ($keys as $key){
			if (array_key_exists($key, $_SERVER) === true){
				foreach (explode(',', $_SERVER[$key]) as $ip){
					$ip = trim($ip);
					if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
						return $ip;
					}
				}
			}
		}
		return $_SERVER['REMOTE_ADDR'];
	}

}