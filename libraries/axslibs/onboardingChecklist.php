<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

class AxsOnboardingChecklist {

    public $checklist;
    public $allItems;
    public $items = array();
    public $percentComplete;
    public $percentCompleteNumber;
    public $returnUrl;

    public function __construct($checklist) {
        $this->checklist = $checklist;

        $items = !empty($checklist->params) ? json_decode($checklist->params) : null;

        unset($items->published);
        $this->allItems = $items;
        $returnUrl = str_replace('&checklist_open=1','',$_SERVER['REQUEST_URI']);
        $returnUrl = str_replace('?checklist_open=1','',$returnUrl);
        if(strpos($returnUrl,'?')) {
            $connector = '&';
        } else {
            $connector = '?';
        }
        $this->returnUrl = base64_encode($returnUrl.$connector.'checklist_open=1');
        $this->storeChecklistItems();
    }

    public function getPercentComplete() {
        return $this->percentComplete;
    }

    public function getPercentCompleteNumber() {
        return $this->percentCompleteNumber;
    }

    public function getChecklist() {
        return $this->checklist;
    }

    public function getAllItems() {
        return $this->allItems;
    }

    public function getItems() {
        return $this->items;
    }

    public  function getChecklistItem($item) {
        $checklistItems = [
            'Quick_Start_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=zenbuilders',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/876969997/Complete+Quick+Start+Setup',
                'text' => 'Complete Quick Start Setup'

            ],
            'User_Creation' => [
                'action_link' => '/administrator/index.php?option=com_users&view=axsusers',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830342178/Add+a+User',
                'text' => 'Add a New User'

            ],
            'User_Group_Creation' => [
                'action_link' => '/administrator/index.php?option=com_users&view=groups',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/823853802/Create+a+User+Group',
                'text' => 'Create a User Group'

            ],
            'Course_Category_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=coursescategories',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830406723/Creating+Course+Categories',
                'text' => 'Create a Course Category'

            ],
            'Teacher_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=teachers',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/833191937/Create+a+Teacher',
                'text' => 'Create a Teacher'

            ],
            'Course_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=courses',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826081852/Create+a+Course',
                'text' => 'Create a Course'

            ],
            'Interactive_Content_Creation' => [
                'action_link' => '/administrator/index.php?option=com_interactivecontent',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826081762/Create+a+Piece+of+Interactive+Content',
                'text' => 'Create an Interactive Content'

            ],
            'Lesson_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=lessons',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/823853789/Create+a+Lesson',
                'text' => 'Create a Lesson'

            ],
            'View_Your_Test_Course' => [
                'action_link' => '/administrator/index.php?option=com_axs&task=admin_checklists.completeItem&checklist_item=View_Your_Test_Course&returnUrl='.base64_encode("/courses").'',
                'self_complete' => true,
                'instruction_link' => '',
                'text' => 'View Your Test Course'

            ],
            'Update_Course_Permissions' => [
                'action_link' => '/administrator/index.php?option=com_axs&task=admin_checklists.completeItem&checklist_item=Update_Course_Permissions&returnUrl='.base64_encode("/administrator/index.php?option=com_splms&view=courses").'',
                'self_complete' => true,
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826147539/Update+Course+Permissions',
                'text' => 'Update Course Permissions'

            ],
            'Validate_Test_User_End_to_End' => [
                'action_link' => '/administrator/index.php?option=com_axs&task=admin_checklists.completeItem&checklist_item=Validate_Test_User_End_to_End&returnUrl='.base64_encode("/courses").'',
                'self_complete' => true,
                'checklist_item' => 'Validate Test User End-to-End',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826147296/Validate+Test+User+End-to+End+Experience',
                'text' => 'Validate Test User End-to-End'

            ],
            'Quizzes_and_Survey_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=quizquestions',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837288025/Create+a+Quiz+with+Quiz+and+Survey+Engine',
                'text' => 'Create a Quiz'

            ],
            'Virtual_Classroom_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=virtual_classrooms',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829719082/How+to+set+up+a+new+Virtual+meeting',
                'text' => 'Create a Virtual Meeting'

            ],
            'SCORM_Item_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=scormitems',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830308430/Add+a+SCORM+File',
                'text' => 'Import a SCORM Course'

            ],
            'Event_Creation' => [
                'action_link' => '/administrator/index.php?option=com_eventbooking&view=events',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830112245/Configure+an+Event',
                'text' => 'Create an Event'

            ],
            'Domain_Creation' => [
                'action_link' => '/administrator/index.php?option=com_domains&view=domains',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/801342044/Add+a+Custom+Domain',
                'text' => 'Add Your Domain Name'

            ],
            'Brand_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brands',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/801243252/All+About+Brands',
                'text' => 'Configure Your Brand'

            ],
            'Brand_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brands',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/801243252/All+About+Brands',
                'text' => 'Create a New Brand'

            ],
            'Team_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=teams',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829686863/Create+a+Team',
                'text' => 'Edit a Team'

            ],
            'Team_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=teams',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829686863/Create+a+Team',
                'text' => 'Create a Team'

            ],
            'Subscription_Plan_Creation' => [
                'action_link' => '/administrator/index.php?option=com_converge&view=plans',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837877942/Subscription+Plans',
                'text' => 'Create a Subscription'

            ],
            'SSO_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=sso_configs',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826245224/Single+Sign-On+Capabilities',
                'text' => 'Setup Single-Sign-On'

            ],
            'LMS_Settings_Edit' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=settings',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829718763/Configure+Global+Lesson+Gating',
                'text' => 'Edit Your LMS Settings'

            ],
            'Checklist_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=checklists',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837746695/Create+a+Checklist',
                'text' => 'Create a Checklist'

            ],
            'Media_Category_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=categories',
                'instruction_link' => '',
                'text' => 'Create a Media Library Category'

            ],
            'Media_Item_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=videos',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/823853151/Add+Media+Items+to+the+Media+Library',
                'text' => 'Add Item to Media Library'

            ],
            'Certificate_Design_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=certificates',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837812292/Customize+Certificate+CSS',
                'text' => 'Design a Certificate'

            ],
            'Certificate_Creation' => [
                'action_link' => '/administrator/index.php?option=com_award&view=badges',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837714002/Create+a+Certificate',
                'text' => 'Create a Certificate'

            ],
            'Badge_Creation' => [
                'action_link' => '/administrator/index.php?option=com_award&view=badges',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837714016/Create+a+Badge',
                'text' => 'Create a Badge'

            ],
            'Milestone_Creation' => [
                'action_link' => '/administrator/index.php?option=com_award&view=badges',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837812370/Create+a+Milestone',
                'text' => 'Create a Milestone'

            ],
            'User_Importer_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=importers',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826081893/Batch+Import+Users+with+the+User+Importer',
                'text' => 'Import Users'

            ],
            'Activity_Dashboard_Creation' => [
                'action_link' => '/administrator/index.php?option=com_reports&view=activity_dashboards',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829686676/Add+an+Activity+KPI+Dashboard',
                'text' => 'Create an Activity Dashboard'

            ],
            'Auto_Notification_Creation' => [
                'action_link' => '/administrator/index.php?option=com_notifications&view=auto_alerts',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/823853923/Setup+Auto+Notifications',
                'text' => 'Setup Auto Notifications'

            ],
            'Learner_Portal_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brand_dashboards',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/801341774/Learner+Portals+and+Learner+Dashboards+Basics',
                'text' => 'Create a New Learner Portal'

            ],
            'Learner_Portal_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brand_dashboards',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/801341774/Learner+Portals+and+Learner+Dashboards+Basics',
                'text' => 'Edit Your Learner Portal'

            ],
            'Landing_Page_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brand_homepages',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/785383766/Create+a+Landing+Page',
                'text' => 'Create a New Landing Page'

            ],
            'Landing_Page_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=brand_homepages',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/785383766/Create+a+Landing+Page',
                'text' => 'Edit Your Landing Page'

            ],
            'Learner_Dashboard_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=learner_dashboards',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837713937/Learner+Dashboard+Setup',
                'text' => 'Create a New Learner Dashboard'

            ],
            'Learner_Dashboard_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=learner_dashboards',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837713937/Learner+Dashboard+Setup',
                'text' => 'Edit Your Learner Dashboard'

            ],
            'Assign_and_Recommend_Creation' => [
                'action_link' => '/administrator/index.php?option=com_splms&view=courseassignments',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/826082006/Assign+Courses+to+Learners',
                'text' => 'Assign Courses to Learners'

            ],
            'Email_Notification_Creation' => [
                'action_link' => '/administrator/index.php?option=com_notifications&view=email_notifications',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830112816/Setup+Recurring+Email+Reports',
                'text' => 'Setup Email Notification Reports'

            ],
            'Popup_Notification_Creation' => [
                'action_link' => '/administrator/index.php?option=com_notifications&view=popup_notifications',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830112874/Configure+Pop-Up+Notifications',
                'text' => 'Setup Popup Notifications'

            ],
            'Report_Creation' => [
                'action_link' => '/administrator/index.php?option=com_reports',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829686789/Education+Reporting',
                'text' => 'Create a New Report'

            ],
            'Admin_Permissions_Creation' => [
                'action_link' => '/administrator/index.php?option=com_permissioning&view=groups',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/830342296/Administrator+Permissioning',
                'text' => 'Setup Admin Permissions'

            ],
            'API_Key_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=apikeys',
                'instruction_link' => 'https://api.tovuti.io/',
                'text' => 'Setup Your API Keys'

            ],
            'Contact_Form_Creation' => [
                'action_link' => '/administrator/index.php?option=com_contactpage&view=contactpages',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/829686586/Configure+a+Contact+Form',
                'text' => 'Setup Your Contact Form'

            ],
            'Course_and_Subscription_Promo_Code_Creation' => [
                'action_link' => '/administrator/index.php?option=com_converge&view=promos',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/877035559/Setup+Promo+Codes+for+Courses+or+Subscription+Plans',
                'text' => 'Add a Promo Code to a Course or Subscription'

            ],
            'Font_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=fonts',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/847446034/Update+the+fonts+on+my+site',
                'text' => 'Add a New Font'

            ],
            'Forum_Category_Creation' => [
                'action_link' => '/administrator/index.php?option=com_kunena&view=categories',
                'instruction_link' => '',
                'text' => 'Create a New Forum Category'

            ],
            'Navigation_Item_Creation' => [
                'action_link' => '/administrator/index.php?option=com_menus&view=menus',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/847216684/Add+Custom+Navigation+Items',
                'text' => 'Create a Navigation Item'

            ],
            'Navigation_Item_Edit' => [
                'action_link' => '/administrator/index.php?option=com_menus&view=menus',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/847216684/Add+Custom+Navigation+Items',
                'text' => 'Edit a Navigation Item'

            ],
            'Points_Category_Creation' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=points_categories',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837714062/Categorize+Points',
                'text' => 'Create a New Points Category'

            ],
            'Points_Category_Edit' => [
                'action_link' => '/administrator/index.php?option=com_axs&view=points_categories',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/837714062/Categorize+Points',
                'text' => 'Edit the Points Category'

            ],
            'Site_Page_Creation' => [
                'action_link' => '/administrator/index.php?option=com_content',
                'instruction_link' => 'https://tovuti.atlassian.net/wiki/spaces/CE/pages/785384063/Site+Pages',
                'text' => 'Create a Site Page'

            ],
        ];

        if($checklistItems[$item]) {
            return $checklistItems[$item];
        } else {
            return false;
        }
    }

    public function storeChecklistItems() {
        $items = $this->allItems;
        $completedTotal = 0;
        ob_start();

        foreach($items as $key => $value) {
            if($value) {
                $item = $this->getChecklistItem($key);
                if($item) {
                    $item['completed'] = $this->checkItem($key);
                    if($item['completed']) {
                        $completedTotal++;
                    }
                    array_push($this->items,$item);
                }
            }
        }

        $totalItems = is_countable($this->items) ? count($this->items) : 0;
        $this->percentComplete = $totalItems != 0 ? round(($completedTotal / $totalItems) * 100) : 0;
        $this->percentCompleteNumber = $totalItems != 0 ? $completedTotal / $totalItems : 0;

        return ob_get_clean();
    }

    public function buildChecklistItems() {
        $items = $this->items;
        $totalItems = count($this->items);
        $i = 0;
        ob_start();
        foreach($items as $item) {
            $i++;
            if($i == $totalItems) {
                $lastItem = 'last-child';
            } else {
                $lastItem = '';
            }
            if($item['completed']) {
                $checked = 'checked';
            } else {
                $checked = '';
            }
            include 'components/com_axs/templates/onboarding_checklist_items.php';
        }
        return ob_get_clean();
    }

    public function checkItem($item) {
        if(!$item) {
            return false;
        }
        $item = str_replace('_and_','_&_',$item);
        $item = str_replace('_',' ',$item);
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('action'). '=' .$db->quote($item);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_actions_audit');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public  function buildChecklistHTML() {
        $input = JFactory::getApplication()->input;
        $checklist_open = $input->get('checklist_open',0,'INT');
        if($checklist_open) {
            $clippingStyle  = 'style="display: block;"';
            $overlayStyle   = 'style="opacity: 1;"';
            $containerStyle = 'style="right: 0px;"';
        } else {
            $clippingStyle  = 'style="display: none;"';
            $overlayStyle   = 'style="opacity: 0;"';
            $containerStyle = '';
        }
        if($this->percentCompleteNumber) {
            $amountCompleted  = $this->percentCompleteNumber;
        } else {
            $amountCompleted = 0;
        }
        if($this->percentComplete) {
            $percentCompleted = $this->percentComplete;
        } else {
            $percentCompleted = 0;
        }
        ob_start();
        require 'components/com_axs/templates/onboarding_checklist.php';
        return ob_get_clean();
    }

    public static function loadClientChecklist($creds,$checklist) {

        $options = array();

        $options['driver']   = 'mysqli';
        $options['host']     = $creds->dbhost;
        $options['user']     = $creds->dbuser;
        $options['password'] = $creds->dbpass;
        $options['database'] = $creds->dbname;

        $db = JDatabaseDriver::getInstance($options);

        $checklistObj = new AxsOnboardingChecklist($checklist);
        return $checklistObj;

    }

    public static function getChecklistById($id) {
        if(!$id) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('id'). '=' .$db->quote($id);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_admin_checklists');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return $result;
        } else {
            return false;
        }
    }
}