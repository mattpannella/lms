<?php

defined ('_JEXEC') or die();

use Joomla\Registry\Registry;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsUser {

	private static $superuser_group_id = 8;
	private static $developer_group_id = 8;
	private static $admin_group_id = 32;
	private static $subadmin_group_id = 20;

	public static $restricted_user_groups = array(
    					"superuser" 	=> "8", 	//Tovuti Admin
    					"subadmin" 		=> "20",	//Sub admin
    					"admin" 		=> "32",	//Admin
						"developer"		=> "8", 	//Developers
						"Registered" => "2"  //Registered Default Group
    					//"demo" 			=> "47",	//Reset Demo
    				);

	public static function isSuperUser($userId = null) {
		return self::checkUserGroup($userId, self::$superuser_group_id);
	}

	public static function isDeveloper($userId = null) {
		return self::checkUserGroup($userId, self::$developer_group_id);
	}

	public static function isAdmin($userId = null) {
		return self::checkUserGroup($userId, self::$admin_group_id);
	}

	public static function isSubadmin($userId = null) {
		return self::checkUserGroup($userId, self::$subadmin_group_id);
	}

	public static function getSuperUserIds() {
		$db = JFactory::getDBO();

		$query = "SELECT * FROM joom_user_usergroup_map WHERE group_id = " . self::$superuser_group_id;
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$list = array();

		foreach ($results as $result) {
			array_push($list, $result->user_id);
		}

		return $list;
	}

	public static function getDeveloperIds() {
		$db = JFactory::getDBO();

		$query = "SELECT * FROM joom_user_usergroup_map WHERE group_id = " . self::$developer_group_id;
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$list = array();

		foreach ($results as $result) {
			array_push($list, $result->user_id);
		}

		return $list;
	}

	private static function checkUserGroup($userId = null, $group) {

		$user = JFactory::getUser($userId);

		$user_groups = $user->getAuthorisedGroups();

        if (in_array($group, $user_groups)) {
        	return true;
        } else {
        	return false;
        }
	}

	public static function replaceItem($params) {
		$addedFields = ['[User ID]','[Date Issued]','[Date Expires]'];
		if(in_array($params->key,$addedFields)) {
			switch($params->key) {
				case "[User ID]":
					$placeholder = $params->user_id;
				break;
				case "[Date Issued]":
					$placeholder = $params->date_issued;
				break;
				case "[Date Expires]":
					$placeholder = $params->date_expires;
				break;
				default:
					 $placeholder = '';
				break;
			}
			$item = str_replace($params->key, $placeholder, $params->text);
		} else {
			$item = str_replace($params->key, AxsUser::getUserProfileFieldById($params->user_id,$params->field_id),$params->text);
		}
		return $item;
	}

	public static function mergeFields($user_id,$date_issued = null,$date_expires = null,$fieldArray,$params) {
		$itemParams = new stdClass();
		$itemParams->user_id = $user_id;
		$itemParams->date_issued = $date_issued;
		$itemParams->date_expires = $date_expires;
		foreach($fieldArray as $key => $value) {
			$itemParams->key = $key;
			$itemParams->field_id = $value;
			if(strpos($params->footer_area_left_text_bottom,$key) !== false) {
				$itemParams->text = $params->footer_area_left_text_bottom;
				$params->footer_area_left_text_bottom = self::replaceItem($itemParams);
			}
			if(strpos($params->footer_area_left_text_top,$key) !== false) {
				$itemParams->text = $params->footer_area_left_text_top;
				$params->footer_area_left_text_top = self::replaceItem($itemParams);
			}
			if(strpos($params->footer_area_right_text_bottom,$key) !== false) {
				$itemParams->text = $params->footer_area_right_text_bottom;
				$params->footer_area_right_text_bottom = self::replaceItem($itemParams);
			}
			if(strpos($params->footer_area_right_text_top,$key) !== false) {
				$itemParams->text = $params->footer_area_right_text_top;
				$params->footer_area_right_text_top = self::replaceItem($itemParams);
			}
			if(strpos($params->header_bottom_text,$key) !== false) {
				$itemParams->text = $params->header_bottom_text;
				$params->header_bottom_text = self::replaceItem($itemParams);
			}
			if(strpos($params->header_top_text,$key) !== false) {
				$itemParams->text = $params->header_top_text;
				$params->header_top_text = self::replaceItem($itemParams);
			}
			if(strpos($params->introduction_text,$key) !== false) {
				$itemParams->text = $params->introduction_text;
				$params->introduction_text = self::replaceItem($itemParams);
			}
			if(strpos($params->sub_text,$key) !== false) {
				$itemParams->text = $params->sub_text;
				$params->sub_text = self::replaceItem($itemParams);
			}
			if(strpos($params->custom_css,$key) !== false) {
				$itemParams->text = $params->custom_css;
				$params->custom_css = self::replaceItem($itemParams);
			}
		}

		return $params;
	}



	public static function getUserProfileFieldById($user_id,$field_id) {
		if(!$user_id || !$field_id) {
			return '';
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('user_id').' = '. (int)$user_id;
		$conditions[] = $db->quoteName('field_id').' = '. (int)$field_id;
		$query = $db->getQuery(true);
		$query->select('value')
			  ->from('#__community_fields_values')
			  ->where($conditions)
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return $result->value;
		} else {
			return '';
		}

	}

	public static function getCourseComments($user_id) {
		if(!$user_id) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('user_id').' = '. (int)$user_id;
		$conditions[] = $db->quoteName('post_id')." LIKE 'lesson%'";
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('comments')
			  ->where($conditions);
		$db->setQuery($query);
		$comments = $db->loadObjectList();
		if($comments) {
			foreach($comments as &$comment) {
				$lessonId = str_replace('lesson_','',$comment->post_id);
				if(is_numeric($lessonId)) {
					$lesson = AxsLMS::getLessonById($lessonId);
					$course = AxsLMS::getCourseById($lesson->splms_course_id);
					$comment->course_title = $course->title;
					$comment->lesson_title = $lesson->title;
					$comment->course_id = $course->splms_course_id;
					$comment->lesson_id = $lesson->splms_lesson_id;
				}

			}
			return $comments;
		} else {
			return false;
		}
	}

	public static function getUserProfileFieldRowById($user_id,$field_id) {
		if(!$user_id || !$field_id) {
			return '';
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('user_id').' = '. (int)$user_id;
		$conditions[] = $db->quoteName('field_id').' = '. (int)$field_id;
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields_values')
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public static function getProfileFields() {
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('type').' != '. $db->quote('group');
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('#__community_fields')
			  ->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if(!$result) {
			$result = array();
		}
		$addedField_1 = new stdClass();
		$addedField_1->name = 'User ID';
		$addedField_1->id 	= 'user_id';
		array_push($result,$addedField_1);

		$addedField_2 = new stdClass();
		$addedField_2->name = 'Date Issued';
		$addedField_2->id 	= 'date_issued';
		array_push($result,$addedField_2);

		$addedField_3 = new stdClass();
		$addedField_3->name = 'Date Expires';
		$addedField_3->id 	= 'date_expires';
		array_push($result,$addedField_3);

		return $result;
	}

	public static function getUserCustomFields($userId) {
		if(!$userId) {
			return false;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
        $query->select('*');
        $query->from('joom_community_fields_values as value');
        $query->join('INNER','joom_community_fields as field ON value.field_id = field.id');
		$conditions[] = $db->quoteName('value.user_id').' = '.$db->quote($userId);
		$query->where($conditions);
		$query->order('field.ordering ASC');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

	public static function getAllUserCustomFieldsValues($userId) {
		if(!$userId) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('type').' != '.$db->quote("group");
		$conditions[] = $db->quoteName('published').' = '.$db->quote("1");
		$query = $db->getQuery(true);
        $query->select('field.*, (SELECT value FROM joom_community_fields_values as v WHERE v.field_id = field.id AND '.$db->quoteName('v.user_id').' = '.$db->quote($userId).' LIMIT 1) AS value');
        $query->from('joom_community_fields as field');
		$query->where($conditions);
		$query->order('field.ordering ASC');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

	public static function getAllCustomFields() {
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('type').' != '.$db->quote("group");
		$conditions[] = $db->quoteName('published').' = '.$db->quote("1");
		$query = $db->getQuery(true);
        $query->select('*');
        $query->from('joom_community_fields');
		$query->where($conditions);
		$query->order('ordering ASC');
        $db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

	public static function getCustomFieldByName($field_name) {
		$db = JFactory::getDBO();
		$conditions[] = $db->quoteName('type').' != '.$db->quote("group");
		$conditions[] = $db->quoteName('published').' = '.$db->quote("1");
		$conditions[] = $db->quoteName('name').' = '.$db->quote($field_name);
		$query = $db->getQuery(true);
        $query->select('*');
        $query->from('joom_community_fields');
		$query->where($conditions);
		$query->setLimit(1);
        $db->setQuery($query);
		$results = $db->loadObject();
		return $results;
	}

	public function getUserGroupNames($userId) {
		if(!$userId) {
			return false;
		}
		$groupIds = JUserHelper::getUserGroups($userId);
		foreach($groupIds as $group) {
			$groups[] = AxsExtra::getUsergroupName($group);
		}
		return $groups;
	}

	public static function getUser($id = null) {
        $db = JFactory::getDBO();
        $conditions = [];

		if(!$id) {
            $id = JFactory::getUser();
		} else {
			$conditions[] = $db->qn('id').' = '.$db->q($id);
		}

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__users');
        $query->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObject();

		if($results) {
			return $results;
		} else {
			return false;
		}
	}

	/**
	 * Get all user IDs present in joom_users
	 *
	 * @return Array List of all extant users in this instance
	 */
	public static function getAllUserIds() {

		$db = JFactory::getDbo();

		$query = "SELECT id FROM #__users";
		$db->setQuery($query);

		$results = $db->loadAssocList('id');

		if(!empty($results)) {

			return array_keys($results);
		} else {

			return [];
		}
	}

	public function setCommunityFullName($user_id) {
		$user = JFactory::getUser($user_id);
		$nameArray = explode(' ',$user->name);
		$firstName = $nameArray[0];
		$lastName = array();
		$namePartCount = count($nameArray);
		for($i = 1; $i < $namePartCount; $i++) {
			$lastName[] = $nameArray[$i];
		}
		$lastName = implode(' ',$lastName);
		$db = JFactory::getDBO();
		$firstNameFieldID = AxsHTML::getFieldByFieldCode('FIELD_GIVENNAME')->id;
		$lastNameFieldID = AxsHTML::getFieldByFieldCode('FIELD_FAMILYNAME')->id;

		$queryFirstName = "INSERT IGNORE INTO #__community_fields_values(user_id, field_id, `value`, `access`) VALUES (".(int)$user_id.",".(int)$firstNameFieldID.",".$db->quote($firstName).",40)";
		$db->setQuery($queryFirstName);
		$db->execute();

		$queryLastName = "INSERT IGNORE INTO #__community_fields_values(user_id, field_id, `value`, `access`) VALUES (".(int)$user_id.",".(int)$lastNameFieldID.",".$db->quote($lastName).",40)";
		$db->setQuery($queryLastName);
		$db->execute();

        return true;
    }

    public static function getLastName($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('field.id,value.value')
              ->from('#__community_fields as field')
              ->join('INNER','#__community_fields_values as value ON field.id = value.field_id')
              ->where($db->qn('value.user_id').'='.(int)$user_id)
              ->where($db->qn('field.fieldcode')." = 'FIELD_FAMILYNAME'")
              ->limit(1);
        $db->setQuery($query);
        $results = $db->loadObject();
        return $results->value;
	}

	public static function getFirstName($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('field.id,value.value')
              ->from('#__community_fields as field')
              ->join('INNER','#__community_fields_values as value ON field.id = value.field_id')
              ->where($db->qn('value.user_id').'='.(int)$user_id)
              ->where($db->qn('field.fieldcode')." = 'FIELD_GIVENNAME'")
              ->limit(1);
        $db->setQuery($query);
        $results = $db->loadObject();
        return $results->value;
	}

	public static function setProfileNameFields($data) {
		if(!$data->userId || !$data->firstName || !$data->lastName) {
			return false;
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_GIVENNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$firstNameID = $db->loadObject();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_FAMILYNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$lastNameID = $db->loadObject();

		if($firstNameID->id) {
			$query = "INSERT IGNORE INTO joom_community_fields_values(`user_id`, `field_id`, `value`, `access`) VALUES (
				".$db->quote($data->userId).",
				".$db->quote($firstNameID->id).",
				".$db->quote($data->firstName).",
				40
				)";
			$db->setQuery($query);
			$db->execute();
		}

		if($lastNameID->id) {
			$query = "INSERT IGNORE INTO joom_community_fields_values(`user_id`, `field_id`, `value`, `access`) VALUES (
				".$db->quote($data->userId).",
				".$db->quote($lastNameID->id).",
				".$db->quote($data->lastName).",
				40
				)";
			$db->setQuery($query);
			$db->execute();
		}
		return true;
	}

	public static function updateProfileNameFields($data) {
		if(!$data->userId) {
			return false;
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_GIVENNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$firstNameID = $db->loadObject();

		$query = $db->getQuery(true);
		$query->select('id');
		$query->from('#__community_fields');
		$query->where($db->qn('fieldcode')." = 'FIELD_FAMILYNAME'");
		$db->setLimit(1);
		$db->setQuery($query);
		$lastNameID = $db->loadObject();

		if($firstNameID->id && $data->firstName) {
			$query = "UPDATE joom_community_fields_values SET value = ".$db->quote($data->firstName)." WHERE user_id =	".$db->quote($data->userId)." AND field_id = ".$db->quote($firstNameID->id);
			$db->setQuery($query);
			$db->execute();
		}

		if($lastNameID->id && $data->lastName) {
			$query = "UPDATE joom_community_fields_values SET value = ".$db->quote($data->lastName)." WHERE user_id =	".$db->quote($data->userId)." AND field_id = ".$db->quote($lastNameID->id);
			$db->setQuery($query);
			$db->execute();
		}
		return true;
	}

	/**
	 * Return an array of userIds that belong to the groups given in the $userGroups parameter.
	 *
	 * @param Array $userGroups Set of user groups to retrieve user IDs from
	 * @return Array | null Set of user IDs that belong to the given groups or null if an error occurred.
	 */
	public static function getUserIdsInGroups($userGroups) {
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$groupList = implode(',', $userGroups);

		$query->select('user_id')
			  ->from('#__user_usergroup_map')
			  ->where("group_id IN ($groupList)");

		$db->setQuery($query);
		$results = [];

		try {
			$userIdRows = $db->loadAssocList();

			foreach($userIdRows as $row) {
				$results[] = $row['user_id'];
			}
		} catch (Exception $ex) {
			$results = null;
		} finally {
			return $results;
		}
	}

	/**
	 * Get a set of groupIds associated with a given userId
	 *
	 * @param int $userId
	 * @return Array Set of group IDs that belong to a given user
	 */
	public static function getGroupIdsForUser(int $userId = null, $email = null, $useDefaultUser = true) {

		$groupIds = [];

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);

		// If the userID passed in is null, try to get it via other means
		if(empty($userId)) {

			if($useDefaultUser) {

				$userId = self::getUser()->id;
			}

			if(!empty($email)) {

				$userId = self::getUserForEmail(AxsSecurity::cleanInput($email));
			}
		}

		// Hopefully the userId isn't empty at this point. If it is, do nothing.
		if(!empty($userId)) {

			$query->select('group_id')
				->from('#__user_usergroup_map')
				->where("user_id = $userId");

			$db->setQuery($query);

			try {

				$rows = $db->loadRowList();

				foreach($rows as $row) {

					$groupIds[] = $row[0];
				}
			} catch(Exception $ex) {}
		}

		return $groupIds;
	}

	/**
	 * Gets a user ID associated with a given email address.
	 *
	 * @param string $email Email address associated with a user account
	 *
	 * @return int|null User ID or null if no record is found
	 */
	public static function getUserForEmail($email) {
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('id')
			  ->from('#__users')
			  ->where($db->qn('email') . ' = ' . $db->quote($email));

		$db->setQuery($query);

		try {
			$result = $db->loadObject();
		} catch (Exception $ex) {
			error_log($ex->getMessage());

			return null;
		}

		return !empty($result) ? $result->id : null;
	}

	/**
	 * Gets the global user settings object usable throughout the entire site
	 *
	 * @return stdClass Object containing the site's global user settings
	 */
	public static function getGlobalUserSettings() {

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('params');
		$query->from('axs_user_settings');
		$query->where('id = 1');

		$db->setQuery($query);
		$result = $db->loadObject();

		$userSettings = null;

		if(!is_null($result) && is_string($result->params)) {

			$userSettings = json_decode($result->params);
		}

		return $userSettings;
	}

	/**
	 * Sends an email with a pre-configured template - uses the admin user settings template if it exists.
	 * Otherwise, use the system default email body template.
	 *
	 * @param int $user_id
	 * @param boolean $includeLoginDetails
	 * @return void
	 */
	public static function sendEmailWithTemplate($user_id, $subjectTemplate = null, $bodyTemplate = null, $templateReplacements = null, $includeLoginDetails = true) {

		$user = self::getUser($user_id);
		$framework = JFactory::getApplication();
		$mailer = JFactory::getMailer();

		//validate email first.
		if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
			return false;
		}

		$lang = JFactory::getLanguage();
		$defaultLocale = $lang->getTag();

		/**
		 * Look for user language. Priority:
		 * 	1. User frontend language
		 * 	2. User backend language
		 */
		$userParams = new Registry($user->params);
		$userLocale = $userParams->get('language', $userParams->get('admin_language', $defaultLocale));

		if ($userLocale !== $defaultLocale)
		{
			$lang->setLanguage($userLocale);
		}

		$lang->load('lib_axslibs');

		if(!empty($bodyTemplate)) {

			//replace the const with the value in the email tmpl
			foreach($templateReplacements as $const => $value) {

				$bodyTemplate = str_replace("[$const]", $value, $bodyTemplate);
			}

			$mailer->isHtml(true);
			$mailer->Encoding = 'base64';

			$emailBody = "<html><body>$bodyTemplate</body></html>";
		} else {

			/* No custom email was created, so use a default template
			 * There are two templates, one with login info and one without
			 */
			if($includeLoginDetails) {

				$emailBody = JText::sprintf(
					'LIB_AXSLIBS_NEW_USER_EMAIL_BODY',
					$user->name,
					$framework->get('sitename'),
					JUri::root(),
					$user->username,
					$user->password_clear
				);
			} else {

				$emailBody = JText::sprintf(
					'LIB_AXSLIBS_NEW_USER_EMAIL_BODY_NODETAILS',
					$user->name,
					$framework->get('sitename'),
					JUri::root()
				);
			}
		}

		// Use the custom subject if one is given
		if(!empty($subjectTemplate)) {

			$emailSubject = str_replace('[website_name]', $templateReplacements['website_name'], $subjectTemplate);
		} else {

			$emailSubject = JText::_('LIB_AXSLIBS_NEW_USER_EMAIL_SUBJECT');
		}

		$mailer->setFrom($framework->get('mailfrom'), $framework->get('fromname'));
		$mailer->setSubject($emailSubject);
		$mailer->addRecipient($user->email);

		$mailer->setBody($emailBody);
		$mailer->Send();

		// Set application language back to default if we changed it
		if ($userLocale !== $defaultLocale)
		{
			$lang->setLanguage($defaultLocale);
		}
	}

	public static function getAvatar($user_id = null) {
		if ($user_id == null) {
			$user_id = JFactory::getUser()->id;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('avatar')
				->from('#__community_users')
				->where($db->qn('userid').'='.(int)$user_id)
				->limit(1);
		$db->setQuery($query);
		$results = $db->loadObject();

		$return = '/' . $results->avatar;
		if ($results->avatar == "") {
			$name = urlencode(JFactory::getUser()->name);
			$return = "https://ui-avatars.com/api/?name=$name&background=" . self::getRandomColorFromString();
		}
		return $return;
	}

	/**
     * Returns a pseudorandom color based on the hash of the given string
     *
     * @param string $str The seed string for the color
     *
     * @return string
     *
     */
    public static function getRandomColorFromString($str = "") {
		$available_colors = array(
			"fce38a",	"eaffd0",
			"95e1d3",	"ffcb91",
			"ffefa1",	"94ebcd",
			"6ddccf",	"b8b5ff",
			"7868e6",	"440a67",
			"93329e",	"b4aee8",
			"78c4d4",	"b7657b",
			"f25287",	"025955",
			"00917c",	"8c0000",
			"bd2000",	"fa1e0e",
			"e45826",	"28527a",
			"99bbad",	"94ebcd",
			"6ddccf"
		);
		$hash = substr(md5($str), 0, 5);

        $returns =  $available_colors[hexdec($hash) % count($available_colors)];
        // jdump($returns, "returns");
		return $returns;
	}

    /**
     * Send a new user email to the specified user from the fromAddress and fromName, for the site siteName
     *
     * @param Array $user Array of user data
     * @param string $fromAddress
     * @param string $fromName
     * @param string $siteName
     * @return bool Status of email send operation
     */
    public static function sendNewUserEmailToUser(Array $user, string $fromAddress, string $fromName, string $siteName,$siteRoot = null) {
        // Get the new user welcome email template
        // This is now in axs_auto_alerts set up as a notification tied to a specific brand.
        $currentBrand = AxsBrands::getBrand();
        $notifications = AxsNotifications::getNotificationsForBrand($currentBrand->id, 'new user welcome email', false);

        $emailConfig = null;

        $siteAddress = is_null($siteRoot) ? JUri::root() : $siteRoot;

        // If no new user welcome email notifications exist, forget the custom template and use the default.
        $useCustomTemplate = false;

        if(count($notifications) > 0 && $notifications[0]->enabled == 0) { //no emails sent at all if the custom email notification is disabled
			return;
		} elseif(count($notifications) > 0 && $notifications[0]->enabled == 1) {

            // There will be only one new user welcome email per brand
            $welcomeEmailNotification = $notifications[0];
            $emailConfig = json_decode($welcomeEmailNotification->params);

            $emailSubject = $emailConfig->welcome_email_subject;
            $emailBody = $emailConfig->welcome_email_body;
        } else {

            // Use the existing New User Welcome Email template, if any is found (legacy)
            $legacyEmailTemplate = AxsUser::getGlobalUserSettings();

            $emailSubject = $legacyEmailTemplate->subject;
            $emailBody = $legacyEmailTemplate->body;
        }

        if(!empty($emailSubject) && !empty($emailBody)) {

            $useCustomTemplate = true;
        }

        // We've got a custom template - let's parse it and substitute merge fields for actual data.
		if($useCustomTemplate) {

			// First, handle the subject line.
			$emailSubject = str_replace("[website_name]", $siteName, $emailSubject);

			// Next, replace merge fields in the body with actual data.
			$emailBody = "<html><body>" . $emailBody;
			$emailBody = str_replace("word-spacing: 15px;", '', $emailBody);
			$emailBody = str_replace("[name]", $user['name'], $emailBody);
			$emailBody = str_replace("[website_name]", $siteName, $emailBody);
			$emailBody = str_replace("[website_link]", $siteAddress, $emailBody);
			$emailBody = str_replace("[username]", $user['username'], $emailBody);
			$emailBody = str_replace("[password]", $user['password_clear'], $emailBody);
			$emailBody .= "</body></html>";
		} else {
			// No custom email was created, so use a default template
			$emailSubject = JText::_('PLG_USER_JOOMLA_NEW_USER_EMAIL_SUBJECT');

			$emailBody = JText::sprintf(
				'PLG_USER_JOOMLA_NEW_USER_EMAIL_BODY',
				$user['name'],
				$siteName,
				$siteAddress,
				$user['username'],
				$user['password_clear']
			);

            $emailBody = "<html><body>$emailBody</body></html>";
		}

		$res = JFactory::getMailer()->sendMail(
            $fromAddress,
            $fromName,
			$user['email'],
			$emailSubject,
			$emailBody,
			true
		);

        return $res;
    }
}

