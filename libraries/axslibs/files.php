<?php

defined('_JEXEC') or die;

class AxsFiles {

    static $GOOGLE_VIEWER = "https://docs.google.com/gview?embedded=true&url=";
	static $GOOGLE_VIEWER_PAGE = "https://drive.google.com/viewerng/viewer?embedded=true&url=";
    static $OFFICE_PPT_VIEWER =  "https://view.officeapps.live.com/op/embed.aspx?src=";

    public static function renderSecureFile($filename) 
    {
        $filename_exploded = explode('.', $filename);
        $extension = strtolower($filename_exploded[count($filename_exploded) - 1]);

        if (empty($extension)) {
            return 'Missing file extension';
        }

        $html = '';
        switch ($extension) {
            default:
                $html = 'Unrecognized File Extension';
            break;
            case 'ppt':
            case 'pptx':
                $embedUrl = self::$OFFICE_PPT_VIEWER . self::getSecureFileUrl($filename);
                ob_start(); ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <embed class="embed-responsive-item" src="<?php echo $embedUrl; ?>" style="width: 100%;" scrolling="auto" frameborder="0"></embed>
                    </div>
                <?php
                $html = ob_get_clean();
            break;
            case 'pdf':
                $isAppleDevice = AxsMobileHelper::isClientUsingMobile(['iPad','iPhone']);
                $isAndroidDevice = AxsMobileHelper::isClientUsingMobile(['Android']);
                $settings = json_decode(AxsLMS::getSettings()->params);
                $embedUrl = self::getSecureFileUrl($filename) . '#toolbar=0';
				if ($settings->allow_pdf_download) {
					$embedUrl = JUri::base() . $filename;
				}

                $linkStart = '';
                $linkEnd = '';
				if ($isAppleDevice) {
					$linkStart = '<a href="' . $embedUrl .' " target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '';
					$linkEnd   = '</a>';
				} else if ($settings->show_open_pdf_link) {
					$linkStart = '<a href="' . $embedUrl .'" target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '</a>';
					$linkEnd   = '';
				} else if ($isAndroidDevice) {
					$linkStart = '<a href="' . self::$GOOGLE_VIEWER_PAGE . JUri::base() . $filename.'" target="_blank"><i class="fa fa-external-link"></i>  ' . AxsLanguage::text("AXS_CLICK_TO_OPEN_PDF", "Click to open PDF") . '</a>';
					$linkEnd   = '';
					$embedUrl = self::$GOOGLE_VIEWER . JUri::base() . $filename;
				}

                ob_start(); ?>
                    <?php echo $linkStart; ?>
                    <div class="embed-responsive embed-responsive-16by9">
                        <embed class="embed-responsive-item" src="<?php echo $embedUrl; ?>" style="width: 100%;" scrolling="auto" frameborder="0"></embed>
                    </div>
                    <?php echo $linkEnd; ?>
                <?php 
                $html = ob_get_clean();
            break;
            case 'mp4':
                $embedObj = new stdClass();
                $embedObj->video_url = JUri::base() . "lockedfiles/mp4/" . self::getEncodedFile($filename) . '#t=2';
                $embedObj->secure_video_url = "lockedfiles/mp4/" . $filename;
                $embedObj->video = $filename;
                $embedObj->video_type = 'mp4';

                $html = '
                    <video id="player_' . rand(1, 1000) . '" style="max-width:100%" controls crossorigin playsinline preload="auto" class="video_tracking" data-video="' . base64_encode(json_encode($embedObj)) . '">
                        <source src="' . $embedObj->video_url . '" type="video/mp4" />
                    </video>
                ';
            break;
            case 'mp3':
                $embedUrl = self::getSecureFileUrl($filename);
                ob_start(); ?>
                    <audio style="width:100%;" src="<?php echo $embedUrl; ?>" controls> </audio>
                <?php 
                $html = ob_get_clean();
            break;
        }

        return $html;
    }

    public static function getSecureFileUrl($file) {
        return JUri::base() . "lockedfiles/file/" . self::getEncodedFile($file);
    }

	public static function addFileToLocker($data) {
		$db = JFactory::getDbo();
		$db->insertObject("axs_secure_file_locker",$data);
    }

    public static function removeFileFromLocker($data) {
        $result = new stdClass();
        $result->status  = "error";
        $result->message = "There was an error deleting the file";
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions[] = $db->quoteName("file").'='.$db->quote($data->file);
        $query->delete($db->quoteName("axs_secure_file_locker"));
        $query->where($conditions);
        $db->setQuery($query);
        $deleteProcess = $db->execute();
        if($deleteProcess) {
            $result->status  = "success";
            $result->message = "File Deleted Successfully";
            $clientId = AxsClients::getClientId();
            $directory = '/mnt/shared/'.$clientId.'/files/secure_file_locker/';
            $file = str_replace('../','',$data->file);
            $file = str_replace('./','',$file);
            $file = str_replace('/','',$file);
            $file = str_replace('*','',$file);
            if(file_exists($directory.$file)) {
                unlink($directory.$file);
            }
        }
        return $result;
	}

	public static function getLockerFiles() {
		$db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select("*");
        $query->from("axs_secure_file_locker");
        $query->order('file DESC');
		$db->setQuery($query);
		$results = $db->loadobjectList();
		return $results;
    }

    public static function getLockerFile($file) {
        if(!$file) {
            return false;
        }
        $db = JFactory::getDBO();
        $conditions[] = $db->quoteName("file").'='.$db->quote($file);
        $query = $db->getQuery(true);
        $query->select("*");
        $query->from("axs_secure_file_locker");
        $query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadobject();
		return $results;
    }

    public static function getEncodedFile($file) {
        $key = AxsKeys::getKey('lms');
        $user_id = JFactory::getUser()->id;
        $fileParams = new stdClass();
        $fileParams->timestamp = time();
        $fileParams->file = $file;
        $fileParams->user_id = $user_id;
        $session = JFactory::getSession();
        $session->set('access_user_id', $user_id);
        $encryptedFile = base64_encode(AxsEncryption::encrypt($fileParams, $key));
        $encryptedFile = str_replace('=','',$encryptedFile);
        return $encryptedFile;
    }

    public static function decryptFile($encryptedFile) {
        $key = AxsKeys::getKey('lms');
        $encryptedParams = base64_decode($encryptedFile);
        $decryptedParams = AxsEncryption::decrypt($encryptedParams, $key);
        return $decryptedParams;
    }
}