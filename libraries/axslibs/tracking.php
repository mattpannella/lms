<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

class AxsTracking {

    private static $appKey    = '1!nwTQSyssJfl2DGKQryEbZdqqezEnKsF1sn57ciIW9zkt7FD';

    public static function sendToPendo($params) {

        if($params->initiator == 'Tovuti System') {
            $email = 'tovuti@tovutiteam.com';
            $clientId = $params->clientId;
            $status = $params->status;
            $clientName = $params->clientName;
            $subscription_type = $params->subscription_type;
        } else {
            $email = JFactory::getUser()->email;
            $clientId = AxsClients::getClientId();
            $status = AxsClientData::getClientStatus();
            $clientName = AxsClientData::getClientName();
            $subscription_type = AxsClientData::getClientSubscription();
            $siteUrl = JURI::root();
        }


        $timestamp = round(microtime(true)*1000);
        //$timestamp = strtotime('2021-01-03 16:50:50') * 1000;
        if($status == 'active' || $status == 'active (onboarding)' || $status == 'trial') {
            header('Content-Type: application/json');
            $url   = "https://app.pendo.io/data/track";
            $curl  = curl_init($url);

            $header   = array();
            $header[] = 'Content-type: application/json';
            $header[] = 'x-pendo-integration-key: f2932d59-7f54-4541-4856-53b2e2a7888b';

            $body            = new stdClass();
            $properties      = new stdClass();
            $body->type      = 'track';
            $body->accountId = $clientId;
            $body->visitorId = $email;
            $body->event     = $params->eventName;
            $body->timestamp = $timestamp;

            $properties->AccountName   = $clientName;
            $properties->AccountStatus = $status;
            $properties->Description   = $params->description;
            if($params->quantity) {
                $properties->Quantity  = $params->quantity;
            }

            $body->properties = $properties;

            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
            $response = curl_exec($curl);
            curl_close($curl);
            sleep(1);
            return $response;
        } else {
            return false;
        }
    }

    public static function setPendoAccountInfo($params) {

        $account = self::getPendoAccountInfo($params->clientId);
        if(!$account) {
            return false;
        }
        $maxLicenses = (int)$account->metadata->pendo_hubspot->enter_max_number_of_users_purchased;
        $totalAnnualUsers = (int)$params->activeUsers;
        if($maxLicenses) {
            $tovutiLicenseUsagePercentage = round(($totalAnnualUsers / $maxLicenses) * 100);
        }

        $url   = "https://app.pendo.io/api/v1/metadata/account/custom/value";
        $curl  = curl_init($url);

        $header   = array();
        $header[] = 'Content-type: application/json';
        $header[] = 'x-pendo-integration-key: d588610d-9573-495f-7c7d-dff361ae9558.us';

        $body            = new stdClass();
        $properties      = new stdClass();
        $body->accountId = "$params->clientId";
        if($maxLicenses) {
            $properties->TovutiLicenseUsagePercentage  = "$tovutiLicenseUsagePercentage";
        }
        $properties->TotalAnnualUsers = "$totalAnnualUsers";
        $body->values = $properties;
        $values = json_encode(array($body));
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $values);
        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }

    public static function getPendoAccountInfo($clientId) {
            $url   = "https://app.pendo.io/api/v1/account/".$clientId;
            $curl  = curl_init($url);

            $header   = array();
            $header[] = 'Content-type: application/json';
            $header[] = 'x-pendo-integration-key: d588610d-9573-495f-7c7d-dff361ae9558.us';

            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($curl);

            curl_close($curl);
            return json_decode($response);
    }

}