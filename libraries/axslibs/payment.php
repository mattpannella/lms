<?php

/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/29/15
 * Time: 4:25 PM
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.helper');

use Omnipay\Common\CreditCard;
use Omnipay\Omnipay;

class AxsPayment {
    public static $initiators = array (
        'system' => 1,
        'admin' => 2,
        'user' => 3,
        'system (auto recover)' => 4
    );

    public static $trxn_types = array (
        'course' => 1,
        'subscription' => 2,
        'event' => 3,
        'payment_plan' => 4,
        'failed_signup' => 5,
        'e-commerce' => 6
    );

    public static $dispatcher;
    public static $grace_period;
    public static $billing;
    public static $gateway;

    private static $loaded = array();

    //################################ HELPER METHODS ##############################################
    public static function init($dispatcher, $brand) {
        self::$dispatcher = $dispatcher;
        self::$grace_period = $brand->billing->grace_period;
        self::$billing = $brand->billing;
    }

    public static function getGatewayInstance() {

            require_once JPATH_LIBRARIES . '/axs_omnipay/vendor/autoload.php';

            $gtw = null;
            switch(self::$billing->gateway_type) {
                case 'converge':
                    $gtw = Omnipay::create('Converge');
                    $gtw->setEndpoint(self::$billing->converge_production_mode ? 'production' : 'development');
                    if(self::$billing->converge_production_mode) {
                        $gtw->setMerchantId(self::$billing->converge_prod_merchant_id);
                        $gtw->setUserId(self::$billing->converge_prod_user_id);
                        $gtw->setPin(self::$billing->converge_prod_account_pin);
                    } else {
                        $gtw->setMerchantId(self::$billing->converge_dev_merchant_id);
                        $gtw->setUserId(self::$billing->converge_dev_user_id);
                        $gtw->setPin(self::$billing->converge_dev_account_pin);
                    }
                    break;

                case 'authnet':
                    $gtw = Omnipay::create('AuthorizeNet_CIM');
                    $gtw->setDeveloperMode(!self::$billing->authnet_production_mode);
                    if(self::$billing->authnet_production_mode) {
                        $gtw->setApiLoginId(self::$billing->authnet_prod_api_login_id);
                        $gtw->setTransactionKey(self::$billing->authnet_prod_transaction_key);
                    } else {
                        $gtw->setApiLoginId(self::$billing->authnet_dev_api_login_id);
                        $gtw->setTransactionKey(self::$billing->authnet_dev_transaction_key);
                    }
                    break;

                case 'stripe':
                    $gtw = Omnipay::create('Stripe');

                    if(self::$billing->stripe_production_mode) {
                        $gtw->setApiKey(self::$billing->stripe_prod_api_key);
                    } else {
                        $gtw->setApiKey(self::$billing->stripe_dev_api_key);
                    }
                    break;

                case 'heartland':
                    $gtw = Omnipay::create('Heartland');
                    if (self::$billing->heartland_production_mode) {
                        $gtw->setSecretApiKey(self::$billing->heartland_prod_secret_api_key);
                        $gtw->setPublicApiKey(self::$billing->heartland_prod_public_api_key);
                        $gtw->setSoapServiceUriMode('prod');
                    } else {
                        $gtw->setSecretApiKey(self::$billing->heartland_dev_secret_api_key);
                        $gtw->setPublicApiKey(self::$billing->heartland_dev_public_api_key);
                        $gtw->setSoapServiceUriMode('dev');
                    }

                    break;
            }
            self::$gateway = $gtw;
            return $gtw;
    }

    /*
    private static function lineSplit($line) {
        $a = explode("=", $line);
        return array(
            'key' => $a[0],
            'value' => isset($a[1]) ? $a[1] : ''
        );
    }
    //################################ CONVERGE METHODS ##############################################
    public static function searchTransactions($config) {
        $params = array (
            'ssl_transaction_type' => 'txnquery',
            'ssl_search_start_date' => $config['start'],
            'ssl_search_end_date'=> $config['end']
        );
        $response = self::talkToConverge($params, false);
        //parse the response
        $lines = explode("\n", $response);
        $return = array();
        $line = 3;
        $currTxn = 0;
        $ltmp = self::lineSplit($lines[2]);
        $currTxnArray = array(
            $ltmp['key'] => $ltmp['value']
        );
        while($line<count($lines)) {
            $currLine = $lines[$line];
            $larr = self::lineSplit($currLine);
            if($larr['key'] == 'ssl_txn_id') { //must be a new transaction
                $return[] = $currTxnArray; //assign the old txn array to return array.
                $currTxnArray = array(); //make a new array for the new transaction.
                $currTxn++;
            }
            //assign the line to the currTxnArray
            $currTxnArray[$larr['key']] = $larr['value'];
            $line++;
        }
        $return[] = $currTxnArray; //add the last transaction
        return $return;
    }
    public static function getUnsettledTotals() {
        $params = array (
            'ssl_transaction_type' => 'total'
        );
        return self::talkToConverge($params);
    }
    public static function settleBatch() {
        $params = array (
            'ssl_transaction_type' => 'settle'
        );
        $results = self::talkToConverge($params);
        if(isset($results['errorCode'])) {
            return false;
        } else {
            return true;
        }
    }
    */

    //################################ HIGH LEVEL METHODS ##############################################
    public static function getPlanById($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_pay_subscription_plans'))
            ->where($db->qn('id')."=".(int)$id);
        $db->setQuery($query);
        return $db->loadObject();
    }

    public static function getCurrency($currencyParams = null) {
        $db = JFactory::getDbo();
        $orderBy = 'country';
        if($currencyParams->id) {
            $conditions[] = $db->qn('id') . "=" . $db->q($currencyParams->id);
        }
        if($currencyParams->code) {
            $conditions[] = $db->qn('code') . "=" . $db->q($currencyParams->code);
        }
        if($currencyParams->country) {
            $conditions[] = $db->qn('country') . "=" . $db->q($currencyParams->country);
        }
        if($currencyParams->top) {
            $conditions[] = $db->qn('ordering') . " > 0";
            $orderBy = 'ordering';
        }
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->qn('currency'));
        if($conditions) {
            $query->where($conditions);
        }
        $query->order($db->qn($orderBy).' ASC');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if(count($result) == 1) {
            $result = $result[0];
        }
        return $result;
    }

    /* public function setGlobalCurrency($currencyParams = null) {
        $db = JFactory::getDbo();
        if($currencyParams->currency_code) {
            $query = $db->getQuery(true);
            $query->update($db->quoteName('#__eb_configs'));
            $query->set($db->quoteName('config_value').'='.$db->quote($currencyParams->currency_code));
            $query->where($db->quoteName('config_key').'='.$db->quote('currency_code'));
            $db->setQuery($query);
            $db->execute();
        }

    } */

    public static function buildStipePaymentForm($params = null) {
        ob_start();
        require JPATH_ROOT.'/components/com_axs/templates/stripe_credit_card_form.php';
        return ob_get_clean();
    }

    public static function getAllPlans() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_pay_subscription_plans'));
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    // TODO:
    public static function getPlansForBrand() {

    }

    public static function hasUserPurchasedCourse($course_id) {
        if(func_num_args() > 1) {
            $user_id = func_get_arg(1);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        $db = JFactory::getDbo();
        $cond = array (
            $db->qn('user_id').'='.(int)$user_id,
            $db->qn('course_id').'='.(int)$course_id,
            $db->qn('status')."='PAID' OR ".$db->qn('status')."='PART'"
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_course_purchases'))
            ->where($cond);
        $db->setQuery($query);
        $cp = $db->loadObject();
        return is_object($cp);
    }

    //TODO:
    public static function getUserEventPurchases() {
        if (func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        if ($user_id) {

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select($db->qn('id'))
                ->from($db->qn('axs_events_registration'))
                ->where($db->qn('user_id').'='.(int)$user_id);

            $db->setQuery($query);
            $cps = $db->loadObjectList();
            $ret = array();

            foreach ($cps as $cp) {
                $courseParams = CoursePurchase::getParams();
                $courseParams->id = $cp->id;
                $cp = self::newCoursePurchase($courseParams);
                $cp->getCoursePurchaseWithId();
                $ret[] = $cp;
            }

            return $ret;
        } else {
            return array();
        }
    }

    public static function getUserCoursePurchases() {
        if(func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        if ($user_id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select($db->qn('id'))
                ->from($db->qn('axs_course_purchases'))
                ->where($db->qn('user_id') . '=' . (int) $user_id);

            $db->setQuery($query);
            $cps = $db->loadObjectList();
            $ret = array();

            foreach ($cps as $cp) {
                $courseParams = CoursePurchase::getParams();
                $courseParams->id = $cp->id;
                $cp = self::newCoursePurchase($courseParams);
                $cp->getCoursePurchaseWithId();
                $ret[] = $cp;
            }

            return $ret;
        } else {
            return array();
        }
    }

    public static function getUserSubscriptions() {
        if (func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->qn('id'))
            ->from($db->qn('axs_pay_subscriptions'))
            ->where($db->qn('user_id').'='.(int)$user_id);
        $db->setQuery($query);
        $subs = $db->loadObjectList();
        $ret = array();

        foreach ($subs as $sub) {

            $subParams = Subscription::getParams();
            $subParams->id = $sub->id;

            $sub = self::newSubscription($subParams);
            $sub->getSubscriptionWithId();
            $ret[] = $sub;
        }
        return $ret;
    }

    //Course Purchases

    public static function newCoursePurchase($params) {
        return new CoursePurchase($params);
    }

    public static function getCoursePurchaseParams() {
        return CoursePurchase::getParams();
    }

    //Subscriptions

    public static function newSubscription($params) {
        return new Subscription($params);
    }

    public static function getSubscriptionParams() {
        return Subscription::getParams();
    }

    //Subscription Periods

    public static function newSubscriptionPeriod($params) {
        return new SubscriptionPeriod($params);
    }

    public static function getSubscriptionPeriodParams() {
        return SubscriptionPeriod::getParams();
    }

    //Transactions

    public static function newTransaction($params) {
        return new Transaction($params);
    }

    public static function getTransactionParams() {
        return Transaction::getParams();
    }

    //Cards

    public static function newCard($params) {
        return new Card($params);
    }

    public static function getCardParams() {
        return Card::getParams();
    }



    /**
     * returns an array of all cards on file that are the user's or the user's family members and they have access to use
     * @param $userid integer optional
     * @return array of Card objects the user has access to use, including family member cards
     */

    public static function getUserAllCards() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }
        if(!$userid) {
            return array ();
        }
        if(func_num_args() > 1) {
            $famIds = func_get_arg(1);
        } else {
            $famIds = AxsExtra::getFamilyMembers($userid);
        }

        $sig = md5(serialize(array($userid, $famIds)));
        if (isset(static::$loaded[__METHOD__][$sig])) {
            return static::$loaded[__METHOD__][$sig];
        }

        $idstr = implode(', ', $famIds);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('ccdata'))
            ->where($db->qn('user_id').' IN ('.$idstr.') AND '.$db->qn('type').'='.$db->q(self::$billing->gateway_type));
        $db->setQuery($query);
        $cards = $db->loadObjectList();
        $allCards = array();
        foreach ($cards as $card) {
            //now make sure the user has permission to use the card.
            if($card->user_id == $userid) {
                $cardParams = Card::getParams();

                $cardParams->cardNumber = $card->card;
                $cardParams->id = $card->id;
                $cardParams->token = $card->token;
                $cardParams->user_id = $userid;
                $cardParams->family_access = $card->family_member_access;
                $cardParams->type = $card->type;

                $c = self::newCard($cardParams);

                $allCards[] = $c;
            } else {
                if($card->family_member_access != "") {
                    $perm = unserialize($card->family_member_access);
                    if(in_array($userid, $perm)) {
                        $cardParams = Card::getParams();

                        $cardParams->cardNumber = $card->card;
                        $cardParams->id = $card->id;
                        $cardParams->token = $card->token;
                        $cardParams->user_id = $userid;
                        $cardParams->family_access = $card->family_member_access;
                        $cardParams->type = $card->type;

                        $c = self::newCard($cardParams);
                        $allCards[] = $c;
                    }
                }
            }
        }
        static::$loaded[__METHOD__][$sig] = $allCards;
        return $allCards;
    }

    /**
     * @return Card
     */
    public static function getTokenCard() {
        $lastFour = func_get_arg(0);
        if(func_num_args() > 1) {
            $userid = func_get_arg(1);
        } else {
            $userid = JFactory::getUser()->id;
        }
        $cards = self::getUserAllCards($userid);
        foreach($cards as $card) {
            if($card->cardNumber == $lastFour) return $card;
        }
    }

    public static function padCardNumber($cardNumber, $digits = 4) {
        $padded = (int)substr($cardNumber, -$digits);
        while (strlen($padded) < $digits) {
            $padded = "0" . $padded;
        }

        return $padded;
    }

    public static function getTestCard() {

        $params = new stdClass();

        $params->firstName = 'Max';
        $params->lastName = 'Runia';
        $params->cardNumber = '4111111111111111';
        $params->expire = '0120';
        $params->cvv = '123';
        $params->address = '123 Test St';
        $params->zip = '83706';
        $params->city = 'Boise';
        $params->state = 'Idaho';
        $params->country = 'United States';
        $params->phone = '1234567890';
        $params->email = 'test@test.com';
        $params->id = 0;
        $params->token = 0;
        $params->user_id = 1689;
        $params->family_access = 0;
        $params->type = 0;

        return new Card($params);
    }


}

class CoursePurchase {
    public $id;
    public $user_id;
    public $course_id;
    public $date;
    public $status;
    public $course;
    public $payment;

    public static $statuses = array (
        'PAID' => 'success',
        'NONE' => 'danger'
    );

    public function __construct($params) {
        $this->id = $params->id;
        $this->user_id = $params->user_id;
        $this->course_id = $params->course_id;
        $this->date = $params->date;
        $this->status = $params->status;
    }

    public static function getParams() {
        $params = new stdClass();

        $params->id = null;
        $params->user_id = null;
        $params->course_id = null;
        $params->date = null;
        $params->status = null;

        return $params;
    }

    public function save() {
        if (!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'user_id',
                'course_id',
                'date',
                'status'
            );

            $values = array(
                (int)$this->user_id,
                (int)$this->course_id,
                $db->q($this->date),
                $db->q($this->status)
            );

            $query->insert($db->qn('axs_course_purchases'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->user_id ? $db->qn('user_id').'='.(int)$this->user_id : '';
            $values .= $this->course_id ? ($values != '' ? ', ' : '') . $db->qn('course_id').'='.(int)$this->course_id : '';
            $values .= $this->date ? ($values != '' ? ', ' : '') . $db->qn('date').'='.$db->q($this->date) : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $query->update($db->qn('axs_course_purchases'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }

        if (strtolower($this->status) == 'paid') {
            AxsLMS::courseProgress_addNewCourse($this->user_id, $this->course_id);
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_course_purchases'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getCoursePurchaseWithId() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('a.user_id as cp_user_id, a.course_id, a.date as original_date, a.status as overall_status, b.*')
            ->from($db->qn('axs_course_purchases').' a')
            ->leftJoin($db->qn('axs_pay_transactions').' b ON a.id=b.type_id')
            ->where("a.id=".(int)$this->id)
            ->order('b.date ASC');
        $db->setQuery($query);
        $c = $db->loadObject();
        $this->user_id = $c->cp_user_id;
        $this->course_id = $c->course_id;
        $this->date = $c->original_date;
        $this->status = $c->overall_status;
        if ($c->type == AxsPayment::$trxn_types['course']) {

            $trxnParams = Transaction::getParams();

            $trxnParams->id = $c->id;
            $trxnParams->user_id = $c->user_id;
            $trxnParams->type = $c->type;
            $trxnParams->type_id = $c->type_id;
            $trxnParams->amount = $c->amount;
            $trxnParams->date = $c->date;
            $trxnParams->status = $c->statue;
            $trxnParams->card = $c->card;
            $trxnParams->trxn_id = $c->trxn_id;
            $trxnParams->refund_id = $c->refund_id;
            $trxnParams->initiator = $c->initiator;
            $trxnParams->status = $c->status;
            $trxnParams->saveCard = false;

            $this->payment = new Transaction($trxnParams);
        } elseif($c->type == AxsPayment::$trxn_types['payment_plan']) {
            //if there is no single trxn then they must have payed with a payment plan.
            $this->payment = new UserPPlan(0, 0, $this->user_id, 0, AxsPayment::$trxn_types['course'], $this->id, 0);
            //load the whole payment plan.
            $this->payment-> getUserPPlanWithTypeId();
        }
    }

    public function getCourse() {
        if(!$this->course) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                ->from($db->qn('joom_splms_courses'))
                ->where("splms_course_id=".(int)$this->course_id);
            $db->setQuery($query);
            $this->course = $db->loadObject();
        }
        return $this->course;
    }

    public function getStatusName($status = null) {

        if (!$status) {
            $status = $this->status;
        }

        switch ($status) {
            case 'PAID':
                return 'Fully Paid';
            case 'PART':
                return 'Partially Paid';
            case 'NONE':
                return 'None Paid';
            case 'PRO1':
                return 'Prorated Unpaid';
            case 'PRO2':
                return 'Prorated Paid';
            case 'COMP':
                return 'Comped';
            case 'FREE':
                return 'Free';
            default:
                return $status;
        }
    }

    public function getCssClass() {
        return self::$statuses[$this->status];
    }
}
/*
class PPlan {
    public $id;
    public $term_length;
    public $term_unit;
    public $total;
    public $interest;
    public $late_fee;
    public function __construct($id, $term_length, $term_unit, $total, $interest, $late_fee) {
        $this->id = $id;
        $this->term_length = $term_length;
        $this->term_unit = $term_unit;
        $this->total = $total;
        $this->interest = $interest;
        $this->late_fee = $late_fee;
    }
    //Database methods
    public function save() {
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'term_length',
                'term_unit',
                'total',
                'interest',
                'late_fee'
            );
            $values = array(
                (int)$this->term_length,
                $db->q($this->term_unit),
                (int)$this->total,
                $db->q($this->interest),
                $db->q($this->late_fee)
            );
            $query->insert($db->qn('axs_pay_pplans'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->term_length ? $db->qn('term_length').'='.(int)$this->term_length : '';
            $values .= $this->term_unit ? ($values != '' ? ', ' : '') . $db->qn('term_unit').'='.$db->q($this->term_unit) : '';
            $values .= $this->total ? ($values != '' ? ', ' : '') . $db->qn('total').'='.(int)$this->total : '';
            $values .= $this->interest ? ($values != '' ? ', ' : '') . $db->qn('interest').'='.$db->q($this->interest) : '';
            $values .= $this->late_fee ? ($values != '' ? ', ' : '') . $db->qn('late_fee').'='.$db->q($this->late_fee) : '';
            $query->update($db->qn('axs_pay_pplans'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }
    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_pplans'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }
}
*/

class UserPPlan {
    public $id;
    public $pplan_id;
    public $user_id;
    public $count;
    public $type;
    public $type_id;
    public $status;
    public $plan;
    public $periods;
    public function __construct($id, $pplan_id, $user_id, $count, $type, $type_id, $status) {
        $this->id = $id;
        $this->pplan_id = $pplan_id;
        $this->user_id = $user_id;
        $this->count = $count;
        $this->type = $type;
        $this->type_id = $type_id;
        $this->status = $status;
    }

    //Database methods
    public function save() {
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array (
                'pplan_id',
                'user_id',
                'count',
                'type',
                'type_id',
                'status'
            );
            $values = array (
                (int)$this->pplan_id,
                (int)$this->user_id,
                (int)$this->count,
                (int)$this->type,
                (int)$this->type_id,
                $db->q($this->status)
            );
            $query->insert($db->qn('axs_pay_user_pplans'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->pplan_id ? $db->qn('pplan_id').'='.(int)$this->pplan_id : '';
            $values .= $this->user_id ? ($values != '' ? ', ' : '') . $db->qn('user_id').'='.(int)$this->user_id : '';
            $values .= $this->count ? ($values != '' ? ', ' : '') . $db->qn('count').'='.(int)$this->count : '';
            $values .= $this->type ? ($values != '' ? ', ' : '') . $db->qn('type').'='.(int)$this->type : '';
            $values .= $this->type_id ? ($values != '' ? ', ' : '') . $db->qn('type_id').'='.(int)$this->type_id : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $query->update($db->qn('axs_pay_user_pplans'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_user_pplans'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getUserPPlanWithTypeId($loadCards = false) {
        $cards = false;
        $db = JFactory::getDbo();
        $where = array (
            "b.type=".(int)$this->type,
            "b.type_id=".(int)$this->type_id
        );
        $query = $db->getQuery(true);
        $query->select('a.*, b.pplan_id, b.user_id, b.count, b.type, b.type_id, b.status as pplan_status')
            ->from($db->qn('axs_pay_user_pplan_periods').' a')
            ->leftJoin($db->qn('axs_pay_user_pplans').' b ON a.upp_id=b.id')
            ->where($where)
            ->order('a.end ASC');
        $db->setQuery($query);
        $periods = $db->loadObjectList();
        if(count($periods)) {
            $firstRow = true;
            foreach($periods as $p) {
                //we can just grab the sub details from the first row since they'll be the same for every row.
                if($firstRow) {
                    $this->id = $p->upp_id;
                    $this->pplan_id = $p->pplan_id;
                    $this->user_id = $p->user_id;
                    $this->count = $p->count;
                    $this->type = $p->type;
                    $this->type_id = $p->type_id;
                    $this->status = $p->pplan_status;
                    $cards = $loadCards ? AxsPayment::getUserAllCards($this->user_id) : false;
                    $firstRow = false;
                }
                $this->periods[] = new UserPPlanPeriod($p->id, $p->upp_id, $p->start, $p->end, $p->status, $p->user_id, $cards);
            }
        } else {
            $query->clear();
            $query->select('*')
                ->from($db->qn('axs_pay_user_pplans'))
                ->where($where);
            $db->setQuery($query);
            $upplan = $db->loadObject();
            $this->id = $upplan->upp_id;
            $this->pplan_id = $upplan->pplan_id;
            $this->user_id = $upplan->user_id;
            $this->count = $upplan->count;
            $this->type = $upplan->type;
            $this->type_id = $upplan->type_id;
            $this->status = $upplan->status;
        }
    }

    public function getPlan() {
        if(!$this->plan) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                ->from($db->qn('axs_pay_pplans'))
                ->where("id=".(int)$this->pplan_id);
            $db->setQuery($query);
            $this->plan = $db->loadObject();
        }
        return $this->plan;
    }
}
class UserPPlanPeriod {
    public $id;
    public $upp_id;
    public $start;
    public $end;
    public $status;
    public $user_id;
    public $trxns;

    public function __construct($id, $upp_id, $start, $end, $status, $user_id, $cards = false) {
        $this->id = $id;
        $this->upp_id = $upp_id;
        $this->start = $start;
        $this->end = $end;
        $this->user_id = $user_id;
        $this->status = $status;
        $this->getTrxns($cards);
    }

    //Database methods
    public function save() {
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'upp_id',
                'start',
                'end',
                'status'
            );
            $values = array(
                (int)$this->upp_id,
                $db->q($this->start),
                $db->q($this->end),
                $db->q($this->status)
            );
            $query->insert($db->qn('axs_pay_user_pplan_periods'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->upp_id ? $db->qn('upp_id').'='.(int)$this->upp_id : '';
            $values .= $this->start ? ($values != '' ? ', ' : '') . $db->qn('start').'='.$db->q($this->start) : '';
            $values .= $this->end ? ($values != '' ? ', ' : '') . $db->qn('end').'='.$db->q($this->end) : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $query->update($db->qn('axs_pay_user_pplan_periods'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_user_pplan_periods'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getTrxns($cards) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $where = array (
            "axs_pay_transactions.type_id=".(int)$this->id,
            "axs_pay_transactions.type=".(int)AxsPayment::$trxn_types['payment_plan']
        );
        $query->select('*')
            ->from($db->qn('axs_pay_transactions'))
            ->leftJoin($db->qn('axs_pay_transactions_errors').' ON axs_pay_transactions.id=axs_pay_transactions_errors.trxn_row_id')
            ->where($where);
        $db->setQuery($query);
        $trxns = $db->loadObjectList();
        foreach($trxns as $t) {

            $trxnParams = Transaction::getParams();

            $trxnParams->id = $t->id;
            $trxnParams->user_id = $t->user_id;
            $trxnParams->type = $t->type;
            $trxnParams->type_id = $t->type_id;
            $trxnParams->amount = $t->amount;
            $trxnParams->date = $t->date;
            $trxnParams->status = $t->status;
            $trxnParams->card = $t->card;
            $trxnParams->trxn_id = $t->trxn_id;
            $trxnParams->refund_id = $t->refund_id;
            $trxnParams->initiator = $t->initiator;
            $trxnParams->saveCard = false;
            $trxnParams->code = $t->error_code;
            $trxnParams->message = $t->error_message;

            $t = new Transaction($trxnParams);

            if($cards) {
                foreach($cards as $c) {
                    if($c->cardNumber == $t->card && ($c->user_id == $t->user_id || in_array($t->user_id, $c->family_access))) {
                        $t->card = $c;
                        break;
                    }
                }
            }
            $this->trxns[] = $t;
        }
    }
}

class Subscription {
    public $id;
    public $user_id;
    public $cancel;
    public $plan_id;
    public $plan;
    public $original_amount;
    public $status;
    public $period_length;
    public $card_id;
    public $points;
    public $periods;

    public static $statuses = array (
        'ACT' => 'success',
        'INAC' => 'danger',
        'GRC' => 'warning',
        'PAUS' => 'warning'
    );

    public function __construct($params) {
        $this->id = $params->id;
        $this->user_id = $params->user_id;
        $this->cancel = $params->cancel;
        $this->plan_id = $params->plan_id;
        $this->original_amount = $params->original_amount;
        $this->status = $params->status;
        $this->period_length = $params->period_length;
        $this->card_id = $params->card_id;
        $this->points = $params->points;
    }

    public static function getParams() {
        $params = new stdClass();

        $params->id = null;
        $params->user_id = null;
        $params->cancel = null;
        $params->plan_id = null;
        $params->original_amount = 0;
        $params->status = null;
        $params->period_length = null;
        $params->card_id = null;
        $params->points = 0;

        return $params;
    }

    //Database methods
    public function save() {
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'user_id',
                'cancel',
                'plan_id',
                'original_amount',
                'status',
                'period_length',
                'card_id',
                'points'
            );
            $values = array(
                (int)$this->user_id,
                $db->qn('cancel').'='. (is_null($this->cancel) ? 'NULL' : $db->q($this->cancel)),
                (int)$this->plan_id,
                $db->q($this->original_amount),
                $db->q($this->status),
                $db->q($this->period_length),
                (int)$this->card_id,
                (int)$this->points
            );
            $query->insert($db->qn('axs_pay_subscriptions'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $query .= " ON DUPLICATE KEY UPDATE ";
            $query .= $db->qn('cancel')."=".(is_null($this->cancel) ? 'NULL' : $db->q($this->cancel)).",";
            $query .= $db->qn('original_amount')."=".$db->q($this->original_amount).",";
            $query .= $db->qn('status')."=".$db->q($this->status).",";
            $query .= $db->qn('period_length')."=".$db->q($this->period_length).",";
            $query .= $db->qn('card_id')."=".$db->q($this->card_id).",";
            $query .= $db->qn('points')."=".$db->q($this->points);
            $db->setQuery($query);
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->user_id ? $db->qn('user_id').'='.(int)$this->user_id : '';
            $values .= (is_null($this->cancel) || $this->cancel != 0) ? ($values != '' ? ', ' : '') . $db->qn('cancel').'='. (is_null($this->cancel) ? 'NULL' : $db->q($this->cancel)) : '';
            $values .= $this->plan_id ? ($values != '' ? ', ' : '') . $db->qn('plan_id').'='.(int)$this->plan_id : '';
            $values .= $this->original_amount ? ($values != '' ? ', ' : '') . $db->qn('original_amount').'='.$db->q($this->original_amount) : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $values .= $this->period_length ? ($values != '' ? ', ' : '') . $db->qn('period_length').'='.$db->q($this->period_length) : '';
            $values .= $this->card_id ? ($values != '' ? ', ' : '') . $db->qn('card_id').'='.(int)$this->card_id : '';
            $values .= $this->points ? ($values != '' ? ', ' : '') . $db->qn('points').'='.(int)$this->points : '';
            $query->update($db->qn('axs_pay_subscriptions'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_subscriptions'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getSubscriptionWithId($loadCards = true) {
        $cards = false;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('a.*, b.user_id, b.cancel, b.plan_id, b.original_amount, b.status as sub_status, b.period_length, b.card_id, b.points')
            ->from($db->qn('axs_pay_subscription_periods').' a')
            ->leftJoin($db->qn('axs_pay_subscriptions').' b ON a.sub_id=b.id')
            ->where("b.id=".(int)$this->id)
            ->order('a.end ASC');
        $db->setQuery($query);
        $periods = $db->loadObjectList();

        if (count($periods)) {
            $firstRow = true;
            foreach($periods as $p) {
                //we can just grab the sub details from the first row since they'll be the same for every row.
                if ($firstRow) {
                    $this->user_id = $p->user_id;
                    $this->cancel = $p->cancel;
                    $this->plan_id = $p->plan_id;
                    $this->original_amount = $p->original_amount;
                    $this->status = $p->sub_status;
                    $this->period_length = $p->period_length;
                    $this->card_id = $p->card_id;
                    $this->points = $p->points;
                    $cards = $loadCards ? AxsPayment::getUserAllCards($this->user_id) : false;
                    $firstRow = false;
                }

                $subPeriodParams = SubscriptionPeriod::getParams();

                $subPeriodParams->id = $p->id;
                $subPeriodParams->sub_id = $p->sub_id;
                $subPeriodParams->start = $p->start;
                $subPeriodParams->end = $p->end;
                $subPeriodParams->status = $p->status;
                $subPeriodParams->user_id = $p->user_id;
                $subPeriodParams->cards = $cards;

                $this->periods[] = new SubscriptionPeriod($subPeriodParams);
            }
        } else {
            $query->clear();
            $query->select('*')
                ->from($db->qn('axs_pay_subscriptions'))
                ->where("id=".(int)$this->id);
            $db->setQuery($query);
            $sub = $db->loadObject();
            $this->user_id = $sub->user_id;
            $this->cancel = $sub->cancel;
            $this->plan_id = $sub->plan_id;
            $this->original_amount = $sub->original_amount;
            $this->status = $sub->status;
            $this->period_length = $sub->period_length;
            $this->card_id = $sub->card_id;
            $this->points = $sub->points;
        }
    }

    public function getSubscriptionWithUserIdPlanId($loadCards = true) {
        $cards = false;
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $where = array (
            "b.user_id=".(int)$this->user_id,
            "b.plan_id=".(int)$this->plan_id
        );
        $query->select('a.*, b.user_id, b.cancel, b.plan_id, b.original_amount, b.status as sub_status, b.period_length, b.card_id, b.points')
            ->from($db->qn('axs_pay_subscription_periods').' a')
            ->leftJoin($db->qn('axs_pay_subscriptions').' b ON a.sub_id=b.id')
            ->where($where)
            ->order('a.end ASC');
        $db->setQuery($query);
        $periods = $db->loadObjectList();
        if(count($periods)) {
            $firstRow = true;
            foreach($periods as $p) {
                //we can just grab the sub details from the first row since they'll be the same for every row.
                if($firstRow) {
                    $this->id = $p->sub_id;
                    $this->user_id = $p->user_id;
                    $this->cancel = $p->cancel;
                    $this->plan_id = $p->plan_id;
                    $this->original_amount = $p->original_amount;
                    $this->status = $p->sub_status;
                    $this->period_length = $p->period_length;
                    $this->card_id = $p->card_id;
                    $this->points = $p->points;
                    $cards = $loadCards ? AxsPayment::getUserAllCards($this->user_id) : false;
                    $firstRow = false;
                }

                $subPeriodParams = SubscriptionPeriod::getParams();

                $subPeriodParams->id = $p->id;
                $subPeriodParams->sub_id = $p->sub_id;
                $subPeriodParams->start = $p->start;
                $subPeriodParams->end = $p->end;
                $subPeriodParams->status = $p->status;
                $subPeriodParams->user_id = $p->user_id;
                $subPeriodParams->cards = $cards;

                $this->periods[] = new SubscriptionPeriod($subPeriodParams);
            }
        } else {
            $query->clear();
            $query->select('*')
                ->from($db->qn('axs_pay_subscriptions'))
                ->where("id=".(int)$this->id);
            $db->setQuery($query);
            $sub = $db->loadObject();
            $this->user_id = $sub->user_id;
            $this->cancel = $sub->cancel;
            $this->plan_id = $sub->plan_id;
            $this->original_amount = $sub->original_amount;
            $this->status = $sub->status;
            $this->period_length = $sub->period_length;
            $this->card_id = $sub->card_id;
            $this->points = $sub->points;
        }
    }

    public function getPeriods() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_pay_subscription_periods'))
            ->where("sub_id=".(int)$this->id);

        $db->setQuery($query);
        $periods = $db->loadObject();

        foreach($periods as $p) {
            $subPeriodParams = SubscriptionPeriod::getParams();

            $subPeriodParams->id = $p->id;
            $subPeriodParams->sub_id = $p->sub_id;
            $subPeriodParams->start = $p->start;
            $subPeriodParams->end = $p->end;
            $subPeriodParams->status = $p->status;
            $subPeriodParams->user_id = $this->user_id;

            $this->periods[] = new SubscriptionPeriod($subPeriodParams);
        }
    }

    public function getStatusName($status = null) {

        if (!$status) {
            $status = $this->status;
        }

        switch ($status) {
            case 'ACT':
                return 'Active';
            case 'INAC':
                return 'Inactive';
            case 'GRC':
                return "Grace Period";
            case 'PAUS':
                return 'Paused';
            default:
                return $status;
        }
    }

    public function getCssClass() {
        return self::$statuses[$this->status];
    }

    public function pause() {
        $this->status = 'PAUS';
        $this->save();
    }

    public function cancel() {
        $this->status = 'INAC';
        $this->cancel = date("Y-m-d H:i:s");
        $this->save();
    }

    public function getPlan() {
        if(!$this->plan) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                ->from($db->qn('axs_pay_subscription_plans'))
                ->where("id=".(int)$this->plan_id);
            $db->setQuery($query);
            $this->plan = $db->loadObject();
        }
        return $this->plan;
    }

    public function getPeriodLengthName() {
        if(!$this->period_length) {
            return 'Lifetime';
        }

        $length = substr($this->period_length, 0, -1);
        $term = substr($this->period_length, -1);
        $postfix = $length > 1 ? 's' : '';
        if($length < 10) {
            switch($length) {
                case 1:
                    $length = 'One';
                    break;
                case 2:
                    $length = 'Two';
                    break;
                case 3:
                    $length = 'Three';
                    break;
                case 4:
                    $length = 'Four';
                    break;
                case 5:
                    $length = 'Five';
                    break;
                case 6:
                    $length = 'Six';
                    break;
                case 7:
                    $length = 'Seven';
                    break;
                case 8:
                    $length = 'Eight';
                    break;
                case 9:
                    $length = 'Nine';
                    break;
            }
        }

        switch($term) {
            case 'M':
                $term = 'Month';
                break;
            case 'D':
                $term = 'Day';
                break;
            case 'Y':
                $term = 'Year';
        }
        return $length.' '.$term.$postfix;
    }
}

class SubscriptionPeriod {
    public $id;
    public $sub_id;
    public $start;
    public $end;
    public $status;
    public $user_id;
    public $trxns;
    public static $statuses = array (
        'PAID' => 'success',
        'PART' => 'warning',
        'NONE' => 'danger',
        'PRO1' => 'danger',
        'PRO2' => 'success',
        'COMP' => 'primary',
        'PAUS' => 'pause',
        'FREE' => 'primary'
    );

    public function __construct($params) {

        $this->id = $params->id;
        $this->sub_id = $params->sub_id;
        $this->start = $params->start;
        $this->end = $params->end;
        $this->status = $params->status;
        $this->user_id = $params->user_id;

        $this->getTrxns($params->cards);
    }

    public static function getParams() {
        $params = new stdClass();

        $params->id = null;
        $params->sub_id = null;
        $params->start = null;
        $params->end = null;
        $params->status = null;
        $params->user_id = null;
        $params->cards = false;

        return $params;
    }

    //Database methods
    public function save() {
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'sub_id',
                'start',
                'end',
                'status'
            );
            $values = array(
                (int)$this->sub_id,
                $db->q($this->start),
                $db->q($this->end),
                $db->q($this->status)
            );
            $query->insert($db->qn('axs_pay_subscription_periods'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->sub_id ? $db->qn('sub_id').'='.(int)$this->sub_id : '';
            $values .= $this->start ? ($values != '' ? ', ' : '') . $db->qn('start').'='.$db->q($this->start) : '';
            $values .= $this->end ? ($values != '' ? ', ' : '') . $db->qn('end').'='.$db->q($this->end) : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $query->update($db->qn('axs_pay_subscription_periods'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_subscription_periods'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getSubPeriodWithId() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('axs_pay_subscription_periods'))
            ->where($db->qn('id').'='.(int)$this->id);

        $db->setQuery($query);
        $sp = $db->loadObject();

        $this->sub_id = $sp->sub_id;
        $this->start = $sp->start;
        $this->end = $sp->end;
        $this->status = $sp->status;
    }

    public function getTrxns($cards) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $where = array (
            "axs_pay_transactions.type_id=".(int)$this->id,
            "axs_pay_transactions.type=".(int)AxsPayment::$trxn_types['subscription']
        );

        $query
            ->select('*')
            ->from($db->qn('axs_pay_transactions'))
            ->leftJoin($db->qn('axs_pay_transactions_errors').' ON axs_pay_transactions.id=axs_pay_transactions_errors.trxn_row_id')
            ->where($where);

        $db->setQuery($query);
        $trxns = $db->loadObjectList();

        foreach($trxns as $t) {
            $trxnParams = Transaction::getParams();

            $trxnParams->id = $t->id;
            $trxnParams->user_id = $t->user_id;
            $trxnParams->type = $t->type;
            $trxnParams->type_id = $t->type_id;
            $trxnParams->amount = $t->amount;
            $trxnParams->date = $t->date;
            $trxnParams->status = $t->status;
            $trxnParams->card = $t->card;
            $trxnParams->trxn_id = $t->trxn_id;
            $trxnParams->refund_id = $t->refund_id;
            $trxnParams->initiator = $t->initiator;
            $trxnParams->saveCard = false;
            $trxnParams->code = $t->error_code;
            $trxnParams->message = $t->error_message;

            $t = new Transaction($trxnParams);

            if ($cards) {
                foreach($cards as $c) {
                    if ($c->cardNumber == $t->card && ($c->user_id == $t->user_id || in_array($t->user_id, $c->family_access))) {
                        $t->card = $c;
                        break;
                    }
                }
            }
            $this->trxns[] = $t;
        }
    }

    public function getStatusName($status = null) {

        if (!$status) {
            $status = $this->status;
        }

        switch ($status) {
            case 'PAID':
                return 'Fully Paid';
            case 'PART':
                return 'Partially Paid';
            case 'NONE':
                return 'None Paid';
            case 'PRO1':
                return 'Prorated Unpaid';
            case 'PRO2':
                return 'Prorated Paid';
            case 'COMP':
                return 'Comp Extension';
            case 'PAUS':
                return 'Paused';
            case 'FREE':
                return 'Free';
            default:
                return $status;
        }
    }

    public function getCssClass() {
        return self::$statuses[$this->status];
    }
}

class Transaction {
    public $id;
    public $user_id;
    public $type;
    public $type_id;
    public $amount;
    public $date;
    public $status;
    public $card;
    public $trxn_id;
    public $refund_id;
    public $initiator;
    public $description;
    public $saveCard;
    public $code;
    public $message;
    public $currency_code;
    public static $statuses = array (
        'SUC' => 'success',
        'DEC' => 'danger',
        'PLHO' => 'warning',
        'FAIL' => 'danger',
        'REF' => 'danger'
    );
    /**
     * Transaction constructor.
     *
     * @param $id integer
     * @param $user_id integer
     * @param $type integer
     * @param $type_id integer
     * @param $amount float
     * @param $date String
     * @param $status String can be either SUC, PLHO, REF, DEC, FAIL
     * @param $card Card
     * @param $trxn_id String
     * @param $refund_id String
     * @param $initiator integer
     * @param $description String
     * @param $saveCard boolean
     */

    public function __construct($params) {

        $this->id = $params->id;
        $this->user_id = $params->user_id;
        $this->type = $params->type;
        $this->type_id = $params->type_id;
        $this->mult_type_id = $params->mult_type_id;
        $this->amount = $params->amount;
        $this->date = $params->date;
        $this->status = $params->status;
        $this->card = $params->card;
        $this->trxn_id = $params->trxn_id;
        $this->refund_id = $params->refund_id;
        $this->initiator = $params->initiator;
        $this->description = $params->description;
        $this->saveCard = $params->saveCard;
        $this->code = $params->code;
        $this->message = $params->message;
        $this->currency_code = $params->currency_code;
    }

    public static function getParams() {
        $params = new stdClass();

        $params->id = null;
        $params->user_id = null;
        $params->type = null;
        $params->type_id = null;
        $params->mult_type_id = null;
        $params->amount = null;
        $params->date = null;
        $params->status = null;
        $params->card = null;
        $params->trxn_id = null;
        $params->refund_id = null;
        $params->initiator = null;
        $params->description = null;
        $params->saveCard = false;
        $params->code = null;
        $params->message = null;

        return $params;
    }

    //Payment methods
    public function charge($save = true) {
        $currency_code = 'USD';
        if(!$this->currency_code) {
            $brand = AxsBrands::getBrand();
            if($brand->billing->currency_code) {
                $currency_code = $brand->billing->currency_code;
            }
        } else {
            $currency_code = $this->currency_code;
        }

        if ($save && !$this->user_id) {
            $this->message = 'No User ID set for this transaction';
            $this->code = 0;
            $this->trxn_id = NULL;
            $this->status = "FAIL";

            return false;
        }

        try {
            $gtw = AxsPayment::getGatewayInstance();

            //first tokenize the card if it is not tokenized..
            if(!$this->card->token && !$this->card->tokenize()) {
                throw new Exception('Tokenization failed for this card.');
            }

            //convert to float and round to nearest cent.
            $this->amount = round($this->amount * 1.00, 2);

            //now make the charge
            $response = $gtw->purchase(array(
                'amount' => $this->amount,
                'currency' => $currency_code,
                'cardReference' => $this->card->token,
                'description' => $this->description
            ))->send();

            if ($response->isSuccessful()) {
                $this->message = $response->getMessage();
                $this->code = 0;
                $this->trxn_id = $response->getTransactionReference();
                $this->status = "SUC";

                //we also want to fire the success event so that we can make plugins to handle it.
                $eventResult = AxsPayment::$dispatcher->trigger('onSuccessfulPayment', array($this->user_id, $this->amount, $this->description, $this->trxn_id));
            } else {
                $this->message = $response->getMessage();
                if($gtw->getName() == "Authorize.Net CIM") {
                    if(JFactory::getUser()->id == 601) {
                        jdump($response->getData());
                    }
                    $this->reasonCode = $response->getReasonCode();
                }
                $this->code =  1;
                $this->trxn_id = $response->getTransactionReference();
                $this->status = "DEC";
                $this->saveCard = false;
            }

        } catch(Exception $e) {
            $this->message = $e->getMessage();
            $this->code = -1;
            $this->trxn_id = NULL;
            $this->status = "FAIL";
            $this->saveCard = false;
        }

        //now that we have a result, save the trxn
        if($save) $this->save();
    }

    public function refund() {

        try {

            $gtw = AxsPayment::getGatewayInstance();

            $response = $gtw->refund(array(
                'amount' => $this->amount,
                'transactionReference' => $this->trxn_id,
                'voidIfRefundFails' => true
            ))->send();

            if($response->isSuccessful()) {
                $this->refund_id = $response->getTransactionReference();
                $this->status = 'REF';
                $this->save();
                return true;
            } else {
                return false;
            }

        } catch(Exception $e) {
            return false;
        }
    }

    //Database methods
    public function save() {

        if (is_object($this->card)) {
            $cardNumber = AxsPayment::padCardNumber($this->card->cardNumber);
        } else {
            $cardNumber = AxsPayment::padCardNumber($this->card);
        }

        if(!$this->id) {
            //let's store this transaction in our DB for later lookup
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'user_id',
                'type',
                'type_id',
                'mult_type_id',
                'amount',
                'date',
                'status',
                'card',
                'trxn_id',
                'initiator'
            );

            $values = array(
                (int)$this->user_id,
                $db->q($this->type),
                (int)$this->type_id,
                $db->q($this->mult_type_id),
                $db->q($this->amount),
                $db->q($this->date),
                $db->q($this->status),
                $db->q($cardNumber),
                $this->trxn_id == NULL ? 'NULL' : $db->q($this->trxn_id),
                (int)$this->initiator
            );
            $query->insert($db->qn('axs_pay_transactions'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
            $this->id = $db->insertid();
            //if the result was anything other than SUCCESS, also store the error code and message.
            if($this->code != 0) {
                $query->clear();
                $cols = array(
                    'trxn_row_id',
                    'error_code',
                    'error_message'
                );
                $values = array(
                    (int)$this->id,
                    (int)$this->code,
                    $db->q($this->message)
                );
                $query->insert($db->quoteName('axs_pay_transactions_errors'))
                    ->columns($db->quoteName($cols))
                    ->values(implode(',', $values));
                $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
                $db->execute();
            }
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $values = $this->user_id ? $db->qn('user_id').'='.(int)$this->user_id : '';
            $values .= $this->type ? ($values != '' ? ', ' : '') . $db->qn('type').'='.(int)$this->type : '';
            $values .= $this->type_id ? ($values != '' ? ', ' : '') . $db->qn('type_id').'='.(int)$this->type_id : '';
            $values .= $this->amount ? ($values != '' ? ', ' : '') . $db->qn('amount').'='.$db->q($this->amount) : '';
            $values .= $this->date ? ($values != '' ? ', ' : '') . $db->qn('date').'='.$db->q($this->date) : '';
            $values .= $this->status ? ($values != '' ? ', ' : '') . $db->qn('status').'='.$db->q($this->status) : '';
            $values .= $this->card ? ($values != '' ? ', ' : '') . $db->qn('card').'='. $db->q($cardNumber) : '';
            $values .= $this->trxn_id ? ($values != '' ? ', ' : '') . $db->qn('trxn_id').'='.$db->q($this->trxn_id) : '';
            $values .= $this->refund_id ? ($values != '' ? ', ' : '') . $db->qn('refund_id').'='.$db->q($this->refund_id) : '';
            $values .= $this->initiator ? ($values != '' ? ', ' : '') . $db->qn('initiator').'='.(int)$this->initiator : '';
            $query->update($db->qn('axs_pay_transactions'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }

        //dont save the card until the trxn is saved, that way we for sure have a user_id to give it by now.
        if($this->saveCard) {
            $this->card->user_id = $this->user_id;
            $this->card->save();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('axs_pay_transactions'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getTrxnWithId() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_pay_transactions'))
            ->leftJoin($db->qn('axs_pay_transactions_errors').' ON axs_pay_transactions.id=axs_pay_transactions_errors.trxn_row_id')
            ->where("axs_pay_transactions.id=".(int)$this->id);

        $db->setQuery($query);
        $trxn = $db->loadObject();
        $this->user_id = $trxn->user_id;
        $this->type = $trxn->type;
        $this->type_id = $trxn->type_id;
        $this->amount = $trxn->amount;
        $this->date = $trxn->date;
        $this->status = $trxn->status;
        $this->card = AxsPayment::getTokenCard($trxn->card, $this->user_id);
        $this->trxn_id = $trxn->trxn_id;
        $this->refund_id = $trxn->refund_id;
        $this->initiator = $trxn->initiator;
        $this->code = $trxn->error_code;
        $this->message = $trxn->error_message;
    }

    public function getStatusName($status = null) {

        if (!$status) {
            $status = $this->status;
        }

        switch ($status) {
            case 'PLHO':
                return 'Placeholder';
            case 'FAIL':
                return 'Failed';
            case 'DEC':
                return 'Declined';
            case 'SUC':
                return 'Successful';
            case "REF":
                return 'Refunded';
            default:
                return $status;
        }
    }

    public function getCssClass() {
        return self::$statuses[$this->status];
    }
}

class Card {
    public $firstName;
    public $lastName;
    public $cardNumber;
    public $expire;
    public $cvv;
    public $address;
    public $zip;
    public $city;
    public $state;
    public $country;
    public $phone;
    public $email;
    public $id;
    public $token;
    public $user_id;
    public $family_access;
    public $type;
    public $stripeToken;
    public $description;
    public $customerId;

    public function __construct($params) {
        $this->firstName = $params->firstName;
        $this->lastName = $params->lastName;
        $this->cardNumber = $params->cardNumber;
        $this->expire = $params->expire;
        $this->cvv = $params->cvv;
        $this->address = substr($params->address, 0, 30);
        $this->zip = substr($params->zip, 0, 9);
        $this->city = $params->city;
        $this->state = $params->state;
        $this->country = $params->country;
        $this->phone = $params->phone;
        $this->email = $params->email;
        $this->id = $params->id;
        $this->token = $params->token;
        $this->user_id = $params->user_id;
        $this->stripeToken = $params->stripeToken ?? null;
        $this->description = $params->description ?? null;
        $this->customerId = $params->customerId ?? null;
        if ($params->family_access != "") {
            $this->family_access = unserialize($params->family_access);
        } else {
            $this->family_access = array();
        }

        if ($params->type) {
            $this->type = $params->type;
        } else {
            $this->type = AxsPayment::$billing->gateway_type;
        }

        //$this->family_access = $params->family_access != "" ? unserialize($params->family_access) : array();
        //$this->type = $type ? $params->type : AxsPayment::$billing->gateway_type;
    }

    public static function getParams() {
        $params = new stdClass();

        $params->firstName = null;
        $params->lastName = null;
        $params->cardNumber = null;
        $params->expire = null;
        $params->cvv = null;
        $params->address = null;
        $params->zip = null;
        $params->city = null;
        $params->state = null;
        $params->country = null;
        $params->phone = null;
        $params->email = null;
        $params->id = null;
        $params->token = null;
        $params->user_id = null;
        $params->family_access = null;
        $params->type = null;

        return $params;
    }

    //Payment methods
    public function tokenize() {

        //lets see if this user has already tokenized this card..
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $cardNumber = AxsPayment::padCardNumber($this->cardNumber);

        $query
            ->select('*')
            ->from($db->qn('ccdata'))
            ->where($db->qn('user_id').'='.(int)$this->user_id.' AND '.$db->qn('card').'='.$cardNumber.' AND '.$db->qn('type').'='.$db->q(AxsPayment::$billing->gateway_type));
        $db->setQuery($query);
        $card = $db->loadObject();

        if (is_object($card)) {
            $this->id = $card->id;
            $this->token = $card->token;
            $this->family_access = $card->family_access != "" ? unserialize($card->family_access) : array();
            $this->type = $card->type;
            return true;
        } else {
            try {
                $gtw = AxsPayment::getGatewayInstance();
                $gatewayName = $gtw->getName();
                if($gatewayName == "Stripe") {
                    $stripeCard = $this->makeStipeCard();
                    $response = $gtw->createCard($stripeCard)->send();
                } else {
                    $card = $this->makeOmnipayCard();

                    if(!$this->user_id) {
                        $customerId = AxsLMS::createUniqueID();
                    } else {
                        $customerId = $this->user_id;
                    }

                    $response = $gtw->createCard(
                        array(
                            'card' => $card,
                            'customerId' => $customerId,
                        )
                    )->send();
                }

                /* if(JFactory::getUser()->id == 601) {
                    jdump($response->getData());
                } */
                if ($response->isSuccessful()) {
                    $this->token = $response->getCardReference();
                    return true;
                } else {
                    $this->message = $response->getMessage();
                    if($gtw->getName() == "Authorize.Net CIM") {
                        if(JFactory::getUser()->id == 601) {
                            jdump($response->getData());
                        }
                        $this->reasonCode = $response->getReasonCode();
                    }
                    return false;
                }
            } catch(Exception $e) {
                return false;
            }
        }
    }

    public function deTokenize() {
        if ($this->token) {
            //check to see if anyone else is using this card before we delete out from under their feet.
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query->select('*')
                ->from($db->qn('ccdata'))
                ->where($db->qn('token').'='.$db->q($this->token));
            $db->setQuery($query);
            $cards = $db->loadObjectList();
            if (count($cards) == 1) {
                //if this user is the only one with access to this card, delete from payment
                try {
                    $gtw = AxsPayment::getGatewayInstance();
                    $response = $gtw->deleteCard(array(
                        'cardReference' => $this->token
                    ))->send();
                    if($response->isSuccessful()) {
                        //now delete card in our table
                        $this->delete();
                        return true;
                    } else {
                        return false;
                    }
                } catch(Exception $e) {
                    return false;
                }
            } else {
                //otherwise just delete card from this user...
                $this->delete();
                return true;
            }
        } else {
            //if token is empty, just delete from this user...
            $this->delete();
            return true;
        }
    }

    public function fetchTokenizedData() {
        //TODO: fetch data from gateway, must implement in OMNIPAY libs.
        /*
        $params = array (
            'ssl_transaction_type' => 'ccquerytoken',
            'ssl_token' => $this->token
        );
        $results = AxsPayment::talkToConverge($params);
        */

            //otherwise grab the data returned.
        $this->firstName = '';
        $this->lastName = '';
        $this->expire = '';
        $this->address = '';
        $this->zip = '';
        $this->city = '';
        $this->state = '';
        $this->country = '';
        $this->phone = '';
        $this->email = '';
        return true;
    }

    public function updateTokenizedData() {
        try {
            $gtw = AxsPayment::getGatewayInstance();
            $card = $this->makeOmnipayCard();

            $response = $gtw->updateCard(array(
                'card' => $card,
                'cardReference' => $this->token
            ))->send();
            if($response->isSuccessful()) {
                return array (
                    'result' => true
                );
            } else {
                return array (
                    'result' => false,
                    'code' => 0 //TODO: adapt to work with old code... isset($results['errorCode']) ? $results['errorCode'] : 0
                );
            }
        } catch(Exception $e) {
            return array (
                'result' => false,
                'code' => 0 //TODO: adapt to work with old code... isset($results['errorCode']) ? $results['errorCode'] : 0
            );
        }
    }

    //Database methods
    public function save() {
        if(!$this->cardNumber || !$this->user_id ||  !$this->token) {
            return false;
        }
        if(!$this->id) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $cols = array(
                'user_id',
                'card',
                'token',
                'family_member_access',
                'type'
            );
            $values = array(
                (int)$this->user_id,
                $db->q(AxsPayment::padCardNumber($this->cardNumber)),
                $db->q($this->token),
                $db->q(serialize($this->family_access)),
                $db->q($this->type)
            );
            $query->insert($db->qn('ccdata'))
                ->columns($db->qn($cols))
                ->values(implode(',', $values));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            //echo $query->__toString();
            $db->execute();
            $this->id = $db->insertid();
        } else {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $cardNumber = AxsPayment::padCardNumber($this->cardNumber);
            $values = $this->user_id ? $db->qn('user_id').'='.(int)$this->user_id : '';
            $values .= $this->cardNumber ? ($values != '' ? ', ' : '') . $db->qn('card').'='. $cardNumber : '';
            $values .= $this->token ? ($values != '' ? ', ' : '') . $db->qn('token').'='.$db->q($this->token) : '';
            $values .= (gettype($this->family_access) == 'array') ? ($values != '' ? ', ' : '') . $db->qn('family_member_access').'='.$db->q(serialize($this->family_access)) : '';
            $values .= $this->type ? ($values != '' ? ', ' : '') . $db->qn('type').'='.$db->q($this->type) : '';
            $query->update($db->qn('ccdata'))
                ->set($values)
                ->where($db->qn('id').'='.(int)$this->id);
            $db->setQuery($query);
            $db->execute();
        }
    }

    public function delete() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->qn('ccdata'))
            ->where($db->qn('id').'='.(int)$this->id);
        $db->setQuery($query);
        $db->execute();
    }

    public function getCardWithId() {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('ccdata'))
            ->where($db->qn('id') . '=' . (int)$this->id);

        $db->setQuery($query);
        $card = $db->loadObject();

        if ($card) {

            $family_member_access = $card->family_member_access != "" ? unserialize($card->family_member_access) : array();

            //Check that the card is owned by the user

            if (PHP_SAPI == 'cli') {
                //Never access JFactory in a cli
                $valid = true;
            } else {
                $app = JFactory::getApplication();
                $adminSection = $app->isAdmin();
                //If no user ID is supplied, get the current user.
                if (!$this->user_id) {
                    $this->user_id = JFactory::getUser()->id;
                }

                //The card does not belong to this user.
                $valid = false;

                //Check if the user_id associated with the card matches the current user.
                if ( ($card->user_id == $this->user_id) || ($adminSection) ) {
                    $valid = true;
                } else {
                    //If not, see if they are a family member.

                    foreach ($family_member_access as $member) {
                        if ($member == $this->user_id) {
                            $valid = true;
                            break;
                        }
                    }
                }
            }

            if ($valid) {
                $this->cardNumber = $card->card;
                $this->token = $card->token;
                $this->user_id = $card->user_id;
                $this->family_access = $family_member_access;
            } else {
                $this->cardNumber = null;
                $this->token = null;
                $this->user_id = null;
                $this->family_access = null;
                $this->id = null;
            }
        }
    }

    public function makeOmnipayCard() {
        //warning: MUST CALL AxsPayment::getGatewayInstance(); before calling this function

        return new CreditCard(array(
            //card info
            'number' => $this->cardNumber,
            'cvv' => $this->cvv,
            'expiryMonth' => substr($this->expire, 0, 2),
            'expiryYear' => $this->expire ? substr($this->expire, -2) : '',

            //billing info
            'billingFirstName' => $this->firstName,
            'billingLastName' => $this->lastName,
            'billingAddress1' => $this->address,
            'billingCity' => $this->city,
            'billingState' => $this->state,
            'billingCountry' => $this->country,
            'billingPostcode' => $this->zip
        ));
    }

     public function makeStipeCard() {
        //warning: MUST CALL AxsPayment::getGatewayInstance(); before calling this function
        return array(
            //card info
            'token'        => $this->stripeToken,
            'description'  => $this->description,
            'id'           => $this->customerId,
            'email'        => $this->email
        );
    }

    //Check that the user
    public function validateUserID() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query
            ->select ('*')
            ->from ($db->qn('ccdata'))
            ->where ($db->qn('id') . '=' . $this->id);

        $db->setQuery($query);
        $card = $db->loadObject();

        $user_id = $card->user_id;
        $family_member_access = unserialize($card->family_member_access);

        var_dump($card);
        var_dump($this->user_id);

        $valid = false;

        if ($user_id == $this->user_id) {
            $valid = true;
        } else {
            foreach ($family_member_access as $member) {
                if ($user_id == $member) {
                    $valid = true;
                    break;
                }
            }
        }

        var_dump($valid);
    }
}