<?php

defined('_JEXEC') or die;

class AxsDirectory {

	public function getUserPlans($params) {
		
	}

	public function getLocation($address) {
		//address is an array
		if($address) {
			$params   = urlencode(implode(',', $address));
			$location = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".$params."&key=AIzaSyAp7ydJCwvDe68lKTRnklpITFcdKHCDVzs");
			$locationData = json_decode($location)->results[0];
			$locationParams = new stdClass();
			foreach($locationData->address_components as $address) {
				switch($address->types[0]) {
					case 'street_number':
						$locationParams->street_number = $address->long_name;
					break;
					case 'route':
						$locationParams->route = $address->long_name;
					break;
					case 'locality':
						$locationParams->city = $address->long_name;
					break;
					case 'administrative_area_level_1':
						$locationParams->state = $address->long_name;
					break;
					case 'country':
						$locationParams->country = $address->long_name;
					break;
					case 'postal_code':
						$locationParams->postal_code = $address->long_name;
					break;
				}
			}
			$locationParams->latitude = $locationData->geometry->location->lat;
			$locationParams->longitude = $locationData->geometry->location->lng;
			return $locationParams;
		} else {
			return false;
		}		
	}

	public function getCountryName($countryId){
		$db =JFactory::getDBO();
		$query = "select * from #__jbusinessdirectory_countries where id = $countryId ";
		$db->setQuery($query);
		$country = $db->loadObject();
		
		return $country->country_name;
	}

	public function getPackageId($companyId) {
		if($companyId) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(true);
			$query
				->select('*')
				->from($db->quoteName('#__jbusinessdirectory_companies'))
				->where($db->quoteName('id') . '=' . $db->quote($companyId));

			$db->setQuery($query);
			$company = $db->loadObject();

			$user_id = $company->userId;

			if ($user_id == 0) {
				//This is an admin listing.  Get the package from the company listing.
				return $company->package_id;
			}
		}
		
		if(!$user_id) {
			$user_id = JFactory::getUser()->id;
		}

		$subscriptionPlan = AxsPayment::getUserSubscriptions($user_id);
		     
		$package_array = [];
		foreach ($subscriptionPlan as $sub) {
			$sub_status = $sub->status;
			
			if ($sub_status == "ACT" || $sub_status == "GRC") {
				$package_id = JBusinessDirectoryHelper::getDirectoryPackageFromPlan($sub->plan_id);
				if($package_id) {
					array_push($package_array, $package_id);
				}				
			}
		}

		if($package_array) {
			$business_directory_plan = JBusinessDirectoryHelper::sortDirectoryPackages($package_array);
			return $business_directory_plan;
		} else {
			return -1;
		}
	}

	function getPackageFromId($packageId) {

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$query
			->select('p.*, GROUP_CONCAT(pf.feature) AS featuresS')
			->from($db->quoteName('#__jbusinessdirectory_packages', 'p'))
			->join(
					'left', 
					$db->quoteName('#__jbusinessdirectory_package_fields', 'pf') . ' ON (' . $db->quoteName('pf.package_id') . '=' . $db->quoteName('p.id') . ')'
				)
			->where($db->quoteName('p.id') . '=' . $db->quote($packageId));

		$db->setQuery($query);

		$package = $db->loadObject();
		$package->features = explode(",", $package->featuresS);
		
		return $package;
	}

}