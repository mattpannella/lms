<?php

defined('_JEXEC') or die;

class AxsH5P {

    public static function overrides($type) {
        $output = "";
        switch($type) {
            case 'H5P.Chart':
                $output = "
                <style>
                    .pieLegend {
                        position: absolute;
                        left: 0px;
                        top: 0px;
                        width: 300px;
                    }
                    .pieColor {
                        width: 15px;
                        height: 15px;
                        display: inline-block;
                    }
                    .h5p-chart-pie {
                        float: right;
                    }
                    @media (max-width: 640px) {
                        .pieLegend {
                            position: relative;
                            margin-bottom: 15px;
                            width: 100%;
                        }
                        .h5p-chart-pie {
                            float: none;
                        }
                    }
                </style>
                <script>
                    $(document).ready(function() {
                    setTimeout(function() {
                        var legend = document.createElement('div');
                        legend.classList.add('pieLegend');
                        $('.h5p-chart').prepend(legend);
                        $('.arc').each(function(index){
                            var itemStyle = $(this).find('path').attr('style');
                            var itemColor = itemStyle.replace('fill','background')
                            var itemText = $(this).find('text').text();
                            var item = '<div><span class=\"pieColor\" style=\"'+itemColor+'\"></span> '+itemText+'</div>';
                            $('.pieLegend').append(item);
                        })
                        },700)
                    })
                </script>\n";
            break;

            case 'H5P.CoursePresentation':
                $output = "
                <style>
                    @media(max-width: 1279px) {
                        .h5p-wrapper {
                        height: calc( ( (100vw  * 0.5625) / 10) + (100vw  * 0.5625)  ) !important;
                        }
                    }
                </style>\n";
            break;

            case 'H5P.Dialogcards':
                $output = "
                <style>
                    .h5p-dialogcards-card-text-area {
                        text-align: left;
                    }
                </style>\n";
            break;


        }
        return $output;
    }
}