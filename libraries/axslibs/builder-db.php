<?php

defined('_JEXEC') OR die();
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

class AwsDatabase {

    public static $original = 'T1JtZ1pEREVsd2t3Tm1RdFNUVmwzZz09OjBjNDQ';
    //public static $original = 'K01DZUlmWllKdElIWklJSmtaN0w2dz09OjdmOTQ';

    public static function getDBO($overwrite = null) {

        $dbInfo = self::getDbInfo();

        $options = array();

        /*if (!$host) {
            $host = 'localhost';
        }

        if (!$database) {
            $database = $dbInfo->dbname;
        }*/

        if ($overwrite) {
            foreach ($overwrite as $key => $value) {
                $dbInfo->$key = $value;
            }
        }

        //var_dump($dbInfo, $overwrite);
        //die();

        $options['driver']   = 'mysqli';
        $options['host']     = $dbInfo->dbhost;
        $options['user']     = $dbInfo->dbuser;
        $options['password'] = $dbInfo->dbpass;
        $options['database'] = $dbInfo->dbname;

        $db = JDatabaseDriver::getInstance($options);

        return $db;
    }

    public static function getDbInfo() {

        $data = new stdClass();

        require_once ("/var/www/files/creds.php");
        $creds = dbCreds::getCreds();

        $data->dbuser = $creds->dbuser;
        $data->dbname = $creds->dbname;
        $data->dbpass = $creds->dbpass;
        $data->dbhost = $creds->dbhost;

        return $data;
    }
}