<?php
defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsDomains {
	private static $db = null;

    /**
	 * Method to create a new domain
	 * @param 	string 	$domain 	The name of the desired subdomain
     * @return  string - the ssl_id associated with the newly created domain
	 */
    public static function createDomain(string $domain, $brand_id) {

        $available = AxsDomains::checkDomainAvailable($domain);
		if (!$available){
			return false;
		}

        // Create new db row for this domain
        AxsDomains::saveDomain($domain, NULL);

        // Send to vision.tovuti.io and configure okta
        $appParams = new stdClass();
        $appParams->clientDB = AxsClients::getClientDbName();
        $app = new AxsOkta($appParams);
        $app->addClientAppDomains($domain);

        // Setup SSL
        $ssl_id = NULL;
        if (!AxsDomains::isTovutiManagedDomain($domain)) {
            $ssl_id = AxsDomains::addSSLRecord($domain);
        }
        
        $updatedDomains = AxsBrands::getUpdatedDomainsList($domain, "add", $brand_id);
        AxsBrands::updateBrand($updatedDomains, $brand_id);

        return $ssl_id;
    }

	/**
	 * Method to see if the desired subdomain is already in use.
	 * @param 	string 	$domain 	The name of the desired subdomain
	 * @return 	bool 				Whether the domain is available or not.
	 */
	public static function checkDomainAvailable($domain) {
		if (self::$db == null) {
			self::$db = AxsDbAccess::getDBO();
		}
		//$query = "SELECT * FROM axs_dbmanager WHERE (FIND_IN_SET('$domain', domains) > 0) AND enabled=1 LIMIT 1";

		$conditions = array(
			"FIND_IN_SET(" . self::$db->quote($domain) . ", domains) > 0",
			self::$db->quoteName('enabled') . '= 1'
		);

		$query = self::$db->getQuery(true);
		$query
			->select('*')
			->from(self::$db->quoteName('axs_dbmanager'))
			->where($conditions)
			->limit(1);


		self::$db->setQuery($query);
		$result = self::$db->loadObject();
		if($result){
			return FALSE;
		} else {
	 		return TRUE;
		}
	}

    /**
     * Return brand id associated with domain.
     * @param   string   $domain       domain name
     * @return 	string 					brand id associated with domain
     */
    public static function getBrandId($domain) {
        $db = JFactory::getDBO();

        //$query = "SELECT params FROM axs_domains WHERE domain = $domain LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_domains'))
            ->where(array(
                $db->quoteName('domain') . '=' . $db->quote($domain),
            ))
            ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if ($result) {
            $json = json_decode($result->params);
            return $json->brand;
        } else {
            return FALSE;
        }
    }

    /**
     * update brand id associated with domain.
     * @param   string   $domain       domain name
     * @param   string|null   $brand         brand id
     */
    public static function updateDomainBrandId($domain, $brand=null ) {
        $db = JFactory::getDBO();
        if($brand == null){
            $brand = "";
        }

        //$query = "SELECT params FROM axs_domains WHERE domain = $domain LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_domains'))
            ->where(array(
                $db->quoteName('domain') . '=' . $db->quote($domain),
            ))
            ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        $params = json_decode($result->params);
        $params->brand = $brand;
        $result->params = json_encode($params);
        $db->updateObject('axs_domains',$result,'id');

    }

    /**
     * update brand id associated with domain.
     * @param   string   $domain       domain name
     * @param   json   $params         json encoding params object
     */
    public static function updateDomainParams($domain, $params) {
        $db = JFactory::getDBO();

        //$query = "SELECT params FROM axs_domains WHERE domain = $domain LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->update($db->quoteName('axs_domains'))
            ->set($db->quoteName('params') . '=' . $db->quote($params))
            ->where($db->quoteName('domain') . '=' . $db->quote($domain))
            ->limit(1);
        $db->setQuery($query);
        $db->execute();
    }

    /**
     * get domain details by id
     * @param   string   $id     domain id
     */
    public static function getDomainDetailsById($id) {
        $db = JFactory::getDBO();

        //$query = "SELECT params FROM axs_domains WHERE domain = $domain LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_domains'))
            ->where(array(
                $db->quoteName('id') . '=' . $db->quote($id),
            ))
            ->limit(1);
        $db->setQuery($query);
        return $db->loadObject();

    }

    /**
     * confirm if domain is a tovuti managed domain
     * @param   string   $domain     domain name
     * @return  bool true|false
     */
    public static function isTovutiManagedDomain($domain) {
        if(preg_match("/^(.*tovuti\.io|.*tovuti-ca\.com|.*tovuti-eu\.com)$/",$domain,$matches)) {
            return true;
        } else {
            return false;
        }
    }

	/**
	 * Return the list of domains that belong to the client.
	 * @param 	integer 	$clientId 	The ID of the client.
	 * @return 	string 					A list of all of the client's domains
	 */
	public static function getClientDomains($clientId = null) {
		if (self::$db == null) {
			self::$db = AxsDbAccess::getDBO();
		}

		if (!$clientId) {
			$clientId = AxsClients::getClientId();
		}

		//$query = "SELECT * FROM axs_dbmanager WHERE id = $clientId AND enabled = 1 LIMIT 1";
		$query = self::$db->getQuery(true);
		$query
			->select('*')
			->from(self::$db->quoteName('axs_dbmanager'))
			->where(array(
					self::$db->quoteName('id') . '=' . self::$db->quote($clientId),
					self::$db->quoteName('enabled') . '=1'
				))
			->limit(1);

		self::$db->setQuery($query);
		$result = self::$db->loadObject();
		if ($result) {
			return $result->domains;
		} else {
	 		return FALSE;
		}
	}


	/**
	 * Save the client's new domain to the database.
	 * @param 	string 		$domain 	The new subdomain to save.
	 * @param 	integer|null 	$clientId 	The ID of the client.
	 * @return 	null
	 */
	public static function saveDomain($domain, $clientId = NULL) {

		if (self::$db == null) {
			self::$db = AxsDbAccess::getDBO();
		}

		if (!$clientId) {
			$clientId = AxsClients::getClientId();
		}

		$clientDomains = self::getClientDomains($clientId);
		$updatedDomains = $clientDomains.','.$domain;
		//$query = "UPDATE axs_dbmanager SET domains = '$updatedDomains' WHERE id = $clientId LIMIT 1";
		$query = self::$db->getQuery(true);
		$query
			->update(self::$db->quoteName('axs_dbmanager'))
			->set(self::$db->quoteName('domains') . '=' . self::$db->quote($updatedDomains))
			->where(self::$db->quoteName('id') . '=' . self::$db->quote($clientId))
			->limit(1);

		self::$db->setQuery($query);
		self::$db->execute();
	}

	/**
	 * Takes a list of domains and overwrites the client's current domain list with it.
	 * @param 	string 			$domainList  	A list of domains to save.
	 * @param 	integer|null 	$clientId		The ID of the clientt
	 * @return  null
	 */
	public static function updateDomainList($domainList, $clientId = NULL) {
		if (self::$db == null) {
			self::$db = AxsDbAccess::getDBO();
		}
		if (!$clientId) {
			$clientId = AxsClients::getClientId();
		}

		//$query = "UPDATE axs_dbmanager SET domains = '$domainList' WHERE id = $clientId LIMIT 1";

		$query = self::$db->getQuery(true);
		$query
			->update(self::$db->quoteName('axs_dbmanager'))
			->set(self::$db->quoteName('domains') . '=' . self::$db->quote($domainList))
			->where(self::$db->quoteName('id') . '=' . self::$db->quote($clientId))
			->limit(1);
			
		self::$db->setQuery($query);
		self::$db->execute();
	}

    /**
     * Add ssl record.
     * @param   string   $domain      domain name
     * @param   string   $clientId   id of client
     * @param   string   $clientName   Name of client
     */
    public static function addSSLRecord($domain, $clientId = NULL, $clientName = NULL) {
        if (!$clientId) {
            $clientId = AxsClients::getClientId();
        }

        if (!$clientName) {
            $db = AxsDbAccess::getDBO();
            $query = $db->getQuery(true);
            $query
                ->select('client_name')
                ->from($db->quoteName('axs_dbmanager'))
                ->where(array(
                    $db->quoteName('id') . '=' . $db->quote($clientId),
                ))
                ->limit(1);
            $db->setQuery($query);
            $res =  $db->loadObject();

            $clientName = $res->client_name;
        }

        $query = $db->getQuery(true);
        $columnsArray = array(
            $db->quoteName('domain'), $db->quoteName('created_date'), $db->quoteName('client_id'), $db->quoteName('client_name')
        );

        // Insert the link.
        $query->clear()
            ->insert($db->quoteName('axs_sslmanager'))
            ->columns($columnsArray)
            ->values(
                $db->quote($domain) . ', '
                . $query->currentTimestamp() . ', '
                . $db->quote($clientId) . ', '
                . $db->quote($clientName)

            );
        $db->setQuery($query);
        $db->execute();
        $id = $db->insertid();
        return $id;
    }

    /**
     * get ssl details by id
     * @param   string   $id     ssl id
     */
    public static function getSSLDetailsById($id) {
        $db = AxsDbAccess::getDBO();

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_sslmanager'))
            ->where(array(
                $db->quoteName('id') . '=' . $db->quote($id),
            ))
            ->limit(1);
        $db->setQuery($query);
        return $db->loadObject();

    }


    /**
     * get ssl details by id
     * @param   string   $id     ssl id
     */
    public static function removeSSLById($id) {
        $db = AxsDbAccess::getDBO();

        $query = $db->getQuery(true);
        $query
            ->delete($db->quoteName('axs_sslmanager'))
            ->where(array(
                $db->quoteName('id') . '=' . $db->quote($id),
            ));
        $db->setQuery($query);
        $result = $db->execute();
    }



}

