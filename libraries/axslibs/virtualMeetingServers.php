<?php

defined('_JEXEC') or die;

class AxsVirtualMeetingServers {

    private static $defaultServer = "server3";

    private static $serverArray = [
        "serverOld" => [
            'domain' =>'https://meet.tovuti.io',
            '_securitySalt' => 'df739f3c07051e46ea926651d8d9f9a9',
            '_bbbServerBaseUrl' => 'https://meet.tovuti.io/bigbluebutton/'
        ],
        "server1" => [
            'domain' =>'https://meeting.tovuti.io',
            '_securitySalt' => 'gOo4ffKWZAfple4ZOvt1FcSmHcazQWKNYghch9I6kE',
            '_bbbServerBaseUrl' => 'https://meeting.tovuti.io/bigbluebutton/'
       ],
       "server2" => [
                'domain' =>'https://meeting.tovuti.io',
                '_securitySalt' => 'gOo4ffKWZAfple4ZOvt1FcSmHcazQWKNYghch9I6kE',
                '_bbbServerBaseUrl' => 'https://meeting.tovuti.io/bigbluebutton/'
        ],
       "server3" => [
            'domain' =>'https://meeting.tovuti.io',
            '_securitySalt' => '4a1c26477dd2c3e754cac5d75b388712',
            '_bbbServerBaseUrl' => 'https://tovutiteam.api.rna1.blindsidenetworks.com/bigbluebutton/'
        ]   
    ];

    public static function getVirtualMeetingServerParams($server = null) {
        if(!$server) {
            $server = self::$defaultServer;
        }
        $setupParams = new stdClass();
        $setupParams->domain = self::$serverArray[$server]['domain'];
        $setupParams->_securitySalt = self::$serverArray[$server]['_securitySalt'];				
        $setupParams->_bbbServerBaseUrl = self::$serverArray[$server]['_bbbServerBaseUrl'];
        return $setupParams;
    }
   
}