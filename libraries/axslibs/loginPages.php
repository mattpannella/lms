<?php

defined('_JEXEC') or die;

class AxsLoginPages {

    public function setType($item, $type) {
		switch ($type) {
			case 'int':
				return (int) $item;
			break;

			case 'bool':
				return (bool) $item;
			break;

			case 'float':
				return (float) $item;
			break;

			case 'string':
				return (string) $item;
			break;

			case 'array':
				return (array) $item;
			break;

			case 'object':
				return (object) $item;
			break;

			default:
				(string) $item;
			break;
		}
	}

	public function quoteFilterList($items,$type = 'string') {
		$db = JFactory::getDbo();
		if(!is_array($items)) {
			$items = explode(',',$items);
		}
		foreach($items as $item) {
			if($item) {
				$itemSet = self::setType($item, $type);
				if($itemSet) {
					$outputArray[] = $db->quote($itemSet);
				}
			}
		}
		$output = implode(',',$outputArray);
		return $output;
    }

    public function getSsoItem($item) {
        $params = json_decode($item->params);

        $output = new stdClass();
        if($item->type == 'SAML') {
            switch ($params->saml_idp) {
                case "azure_active_directory":
                    $text = 'Azure Active Directory';
                    $image = 'windows';
                break;
                case "adfs":
                    $text = 'ADFS';
                    $image = 'windows';
                break;
                case "okta":
                    $text = 'Okta';
                break;
                case "google_apps":
                    $text = 'Google';
                    $image = 'google';
                break;
                case "salesforce":
                    $text = 'Salesforce';
                break;
                case "one_login":
                    $text = 'One Login';
                break;
                case "last_pass":
                    $text = 'Last Pass';
                break;
                case "other":
                    $text = 'SSO';
                break;
                default:
                    $text = 'SSO';
                break;
            }
            if(!$image && $params->saml_idp) {
                $image = $params->saml_idp;
            } elseif(!$image)  {
                $image = 'other';
            }
        }
        if($item->type == 'OAuth2') {
            switch ($params->oauth_idp) {
                case "google":
                    $text = 'Google';
                break;
                case "cognito":
                    $text = 'AWS Cognito';
                break;
                case "facebook":
                    $text = 'Facebook';
                break;
                case "windows":
                    $text = 'Windows';
                    $image = 'windows';
                break;
                case "linkedin":
                    $text = 'LinkedIn';
                break;
                case "instagram":
                    $text = 'Instagram';
                break;
                case "other":
                    $text = 'SSO';
                break;
                default:
                    $text = 'SSO';
                break;
            }
            if(!$image && $params->oauth_idp) {
                $image = $params->oauth_idp;
            } elseif(!$image) {
                $image = 'other';
            }
        }

        $output->image = '/components/com_axs/views/login_page/images/'.$image.'.svg';
        $output->link  = JRoute::_('index.php?option=com_axs&view=sso_config&id='.$item->id);
        $output->text  = 'Sign in with '.$text;

        return $output;
    }

    public function getSsoOptions($loginPage) {

        $list = self::quoteFilterList($loginPage->sso_options,'int');

        if(!$list) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('id')." IN ($list)";
        $conditions[] = $db->quoteName('enabled')." = 1";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__sso_saml_config')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        $html = '';
        if($loginPage->redirect) {
            if(strpos($loginPage->redirect,'?')) {
                $loginPage->redirect = urlencode($loginPage->redirect);
            }
            $redirectURL = '&forward='.$loginPage->redirect;
        } else {
            $redirectURL = '';
        }
        foreach($results as $row) {
            $buttonParams = self::getSsoItem($row);

            $html .= '<a class="tov_login-button sso w-button" href="'.$buttonParams->link.$redirectURL.'"><img src="'.$buttonParams->image.'" class="sso_image" /> '.$buttonParams->text.'</a>';
        }
        return $html;
    }

    public static function getRegistrationForm($loginPage, $loginPageId) {
        $return = "/";
        if ($loginPage->redirect) {
		    $return = urldecode($loginPage->redirect);
	    }
        include 'components/com_axs/templates/registration_form.php';
    }

    public static function getLoginForm($loginPage) {
        $twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
        $security = AxsSecurity::getSettings();
        if($security->twofactor_type == 'yubikey') {
            $sercretKeyInputType = 'password';
        } else {
            $sercretKeyInputType = 'text';
        }
        $return = "/";
        if ($loginPage->redirect) {
		    $return = urldecode($loginPage->redirect);
	    }
        include 'components/com_axs/templates/login_form.php';
    }

    public static function getPasswordResetForm($loginPage,$step = 'request') {
        $currentLoginPageUrl = explode('?',$_SERVER['REQUEST_URI'])[0];
        switch($step) {
            case 'request':
                include 'components/com_axs/templates/password_reset_form.php';
            break;
            case 'reset':
                include 'components/com_axs/templates/password_reset_form.php';
            break;
            case 'confirm':
                include 'components/com_axs/templates/password_reset_confirm.php';
            break;
            case 'complete':
                include 'components/com_axs/templates/password_reset_complete.php';
            break;
            default:
                include 'components/com_axs/templates/password_reset_form.php';
            break;
        }

    }

}