<?php

defined('_JEXEC') or die;

class AxsQuiz {

    private $quiz;
    private $questions;
    private $params;

    public function __construct($quiz) {
        $this->quiz = $quiz;

        $this->questions = (array)json_decode($quiz->questions);
        $this->questions = array_values($this->questions);

        $this->params = json_decode($quiz->params);

        $userQuizResponses = $this->getUserQuizResponses();
        $userQuizResponsesCount = empty($userQuizResponses) ? 0 : count($userQuizResponses);

        $this->quiz->question_number = $userQuizResponsesCount;
    }

    public function getQuiz() {
        return $this->quiz;
    }

    public function getId() {
        return $this->quiz->id;
    }

    public function getParams() {
        return $this->quiz->params;
    }

    public function getCurrentQuestion() {
        return $this->questions[$this->quiz->question_number];
    }

    private function getUserQuizResponses() {
        $userId = JFactory::getUser()->id;
        $quizId = $this->quiz->id;

        if(empty($userId)) {
            return null;
        } else {
            $db = JFactory::getDbo();

            $conditions = [
                $db->quoteName('user_id') . '=' . $db->quote($userId),
                $db->quoteName('quiz_id') . '=' . $db->quote($quizId)
            ];

            $query = $db->getQuery(true);
            $query->select('*')
                  ->from('axs_quiz_responses')
                  ->where($conditions)
                  ->order('id ASC');

            $db->setQuery($query);
            $result = $db->loadObjectList();

            return !empty($result) ? $result : null;
        }
    }

    /**
     * Get the quiz answers for a specific quiz and user ID.
     * If $questionId is null, get all of the answers in the table.
     *
     * @param int $userId
     * @param int $quizId
     * @param string $questionId Random-generated question ID string (optional)
     * @return stdClass Object consisting of a JSON object representing the attempt answers, and time elapsed in seconds
     */
    public static function getLatestLegacyQuizAttempt($userId, $quizId) {

        $attempt = null;

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $conditions = [
            "user_id = $userId",
            "quiz_id = $quizId"
        ];

        $query->select('answers, elapsed')
              ->from('#__splms_quiz_answers')
              ->where($conditions)
              ->order('id DESC');

        $db->setQuery($query);

        try {

            $attempt = $db->loadObject();
        } catch(Exception $ex) {

            JFactory::getApplication()->enqueueMessage('Error: could not load quiz attempt: ' . $ex->getMessage());
        }

        return $attempt;
    }

    /**
     * Save the given quiz answer in the joom_splms_quiz_answers table
     *
     * @param integer $userId
     * @param integer $quizId
     * @param integer $questionIndex
     * @param string $answer
     * @return bool True if the operation was successful, false if it wasn't.
     */
    public static function saveLegacyQuizAnswer(int $userId, int $quizId, int $questionIndex, ?string $answer, ?int $timeElapsed) {
        $db = JFactory::getDbo();

        $currentTimestamp = $db->quote(date('Y-m-d H:i:s'));

        $result = false;

        // Attempt to get the answer object for a quiz - if it exists, update, otherwise create a new record
        $query = $db->getQuery(true);

        $conditions = [
            "user_id = $userId",
            "quiz_id = $quizId"
        ];

        $query->select('*')
              ->from('#__splms_quiz_answers')
              ->where($conditions)
              ->order('id DESC');

        $db->setQuery($query);

        $answerRecord = $db->loadObject();

        try {

            // If no entry exists for the userID and quizID OR the quiz is completed, insert a new row
            if(is_null($answerRecord) || $questionIndex == 0) {

                $insertColumns = [
                    'user_id',
                    'quiz_id',
                    'answers',
                    'submitted_date'
                ];

                $values = [
                    $userId,
                    $quizId,
                    "JSON_SET('[]', '$[$questionIndex]', $answer)",
                    $currentTimestamp
                ];

                if(!is_null($timeElapsed)) {

                    $insertColumns[] = 'elapsed';
                    $values[] = $timeElapsed;
                }

                $quizAnswerCreateQuery = "
                    INSERT INTO joom_splms_quiz_answers
                    (" . implode(',', $insertColumns) . ")" .
                    "VALUES(" . implode(',', $values) . ")";

                $db->setQuery($quizAnswerCreateQuery);

                $result = $db->execute();
            } else {

                $updateColumnValues = [
                    "answers = JSON_SET(answers, '$[$questionIndex]', $answer)",
                    "submitted_date = $currentTimestamp"
                ];

                if(!is_null($timeElapsed)) {

                    $updateColumnValues[] = "elapsed = $timeElapsed";
                }

                $quizAnswerUpdateQuery = "
                    UPDATE joom_splms_quiz_answers
                    SET " . implode(',', $updateColumnValues) . "
                    WHERE id = $answerRecord->id
                ";

                $db->setQuery($quizAnswerUpdateQuery);

                $result = $db->execute();
            }
        } catch(Exception $ex) {

            JFactory::getApplication()->enqueueMessage('Error saving quiz answer: ' . $ex->getMessage(), 'error');
        } finally {

            if(!$result) {

                JFactory::getApplication()->enqueueMessage('Error saving quiz answer.', 'error');
            }

            return $result;
        }
    }

    /**
     * Delete row of quiz answer data from joom_splms_quiz_answers and return boolean status
     *
     * @param integer $userId
     * @param integer $quizId
     * @return boolean True if row was successfully deleted, false if something went wrong.
     */
    public static function deleteLegacyQuizAnswers(?int $userId, int $quizId) {
        $db = JFactory::getDbo();

        $conditions = [
            "quiz_id = $quizId"
        ];

        /* If a user ID is given (normal case), delete specific records for a given user ID and quizID,
         * delete all records associated with a given quizID. This should only happen if the number of questions
         * change when saving a quiz.
         */
        if(!is_null($userId)) {

            $conditions[] = "user_id = $userId";
        }

        $conditionString = implode(' AND ', $conditions);

        $query = "DELETE FROM joom_splms_quiz_answers WHERE $conditionString";

        $db->setQuery($query);

        $result = false;

        try {

            $result = $db->execute();
        } catch(Exception $ex) {

            JFactory::getApplication()->enqueueMessage('There was an error deleting quiz answers: ' . $ex->getMessage());
        } finally {

            if(!$result) {

                JFactory::getApplication()->enqueueMessage('There was an error deleting quiz answers');
            }
        }

        return !is_null($result);
    }

    public function buildQuestionHtml($questionNumber) {
        $questionHtml = "";
        $question = $this->questions[$questionNumber];

        if($this->hasQuizBeenCompleted()) {
            $questionHtml .= $this->getCompletedMessage();
        } else {
            $key = AxsKeys::getKey('surveys');
            $encryptedData = base64_encode(AxsEncryption::encrypt($this->quiz, $key));

            $questionHtml .= "<input id='questionType' type='hidden' value='$question->type' name='question_type'>";
            $questionHtml .= "<input id='quizData' type='hidden' value='$encryptedData' name='quiz_data'>";

            switch($question->type) {
                case 'multiplechoice': {
                    $radioGroup = $this->buildRadioGroupForQuestion($question);
                    $questionHtml .= $radioGroup;
                    break;
                }
                case 'textbox': {
                    $questionHtml .= "<span class='tovuti-input'>";
                    $questionHtml .= $question->text;
                    $questionHtml .= "<input type='text' name='quiz_response'></span>";
                    break;
                }
                case 'textarea':
                {
                    $questionHtml .= "<div class='tovuti-input'>";
                    $questionHtml .= $question->text;
                    $questionHtml .= "<div><textarea type='text' name='quiz_response'></textarea></div></div>";
                    break;
                }
                default: {break;}
            }
        }

        return $questionHtml;
    }

    public function getQuestionById($questionId) {
        $targetQuestion = array_filter($this->questions, function($question) use($questionId) {
            return $question->id == $questionId;
        });

        return $targetQuestion[0];
    }

    public function getQuestions() {
        return $this->questions;
    }

    private function buildRadioLabelsForQuestion($question) {
        $labels = array();
        $options = explode("\r\n", $question->multiple_choice_options);

        foreach($options as $option) {
            $labels[] = $option;
        }

        return $labels;
    }

    private function buildRadioGroupForQuestion($question) {
        $labels = $this->buildRadioLabelsForQuestion($question);
        $radioGroup = "<div class='tovuti-radio-group'>";

        foreach($labels as $label) {

            $radioGroup .= "<label class='tovuti-radio'>";
            $radioGroup .= "<input class='tovuti-radio-option' type='radio' name='quiz_response' value='$label'>";
            $radioGroup .= "<div class='tovuti-radio-circle'></div>";
            $radioGroup .= $label;
            $radioGroup .= "</label>";
        }

        return $radioGroup . "</div>";
    }

    public function hasQuizBeenCompleted() {

        return ($this->quiz->question_number >= count($this->questions));
    }

    public function buildQuizStats() {
        $html = '';
        $percentCorrect = $this->getPercentCorrect();
        $totalPoints = $this->getTotalPoints();

        $html .= "<div><span class='tovuti-stats-item'>Total Points: $totalPoints</span></div>";
        $html .= "<div><span class='tovuti-stats-item'>% Correct: $percentCorrect%</span></div>";

        if($percentCorrect >= $this->params->target_percentage) {
            $html .= "<div class='tovuti-quiz-passed'>Congratulations! You passed!</div>";
        } else {
            $html .= "<div class='tovuti-quiz-failed'>Unfortunately, you did not pass the quiz.</div>";
        }

        return $html;
    }

    public function getTotalPoints() {
        $points = 0;

        foreach($this->questions as $question) {
            $points += $question->points;
        }

        return $points;
    }

    public function getPercentCorrect() {
        $percent = 0;
        $totalPoints = $this->getTotalPoints();
        $totalCorrectPoints = $this->getTotalCorrectQuestionPoints();

        $percent = round(($totalCorrectPoints / $totalPoints) * 100.0);

        return $percent;
    }

    public function getTotalCorrectQuestionPoints() {
        $correct = 0;
        $quizResponses = $this->getUserQuizResponses();

        foreach($this->questions as $index => $question) {
            // Responses map directly to questions
            if($quizResponses[$index]->answer == $question->correct_answer) {
                // Correct answer!
                $correct += $question->points;
            }
        }

        return $correct;
    }

    public function getCompletedMessage() {

        if($this->params->custom_completed_message == 'custom' && !empty($this->params->completed_message)) {
            $completed_message = $this->params->completed_message;
        } else {
            $completed_message = "<h3>Quiz Complete</h3>";
        }

        return $completed_message;
    }

    public function getTemplate() {
        include 'components/com_axs/templates/quizzes/default.php';
    }
}