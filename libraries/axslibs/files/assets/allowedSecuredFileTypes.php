<?php

$allowedFileTypes = array(
	'pptx' => array(
		'name' => 'Microsoft Office - OOXML - Presentation',
		'type' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
    ),
    'ppt' => array(
		'name' => 'Microsoft PowerPoint',
		'type' => 'application/vnd.ms-powerpoint'
    ),
    'mpeg' => array(
		'name' => 'MPEG Video',
		'type' => 'video/mpeg'
	),
    'mp4' => array(
		'name' => 'MPEG-4 Video',
		'type' => 'video/mp4'
	),
	'mp3' => array(
		'name' => 'MP3 Audio',
		'type' => 'audio/mp3'
	),
	'mp4' => array(
		'name' => 'MPEG4',
		'type' => 'application/mp4'
    ),
    'pdf' => array(
		'name' => 'Adobe Portable Document Format',
		'type' => 'application/pdf'
	)
);