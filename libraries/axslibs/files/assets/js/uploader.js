$(document).on('click', '.file_file, .browse', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var file = $('#'+id);
    file.trigger('click');
    file.change( function() {
        $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
        $('.file-submit').show();
    }); 
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function sendData(data, url) {
    if(data) {
        var uid = getUrlVars()['uid'];
        data.append('uid',uid);
        var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
        var loading     = '<span class="fa fa-spinner fa-spin"></span> Uploading...';
        var error       = '<span style="color: red;"><b>There was an error uploading your file.</b></span>';
        $('#file-uploader-message').show();
        $('#file-uploader-message').html(loading);
        $.ajax({
            url  : url,
            data : data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type : 'post'
        }).done( function(response) {
            var result = JSON.parse(response);
            console.dir(result);
            if(result) {
                var message = '<span style="color: red;"><b>'+result.message+'</b></span>';
                if(result.status == 'success') {
                    window.parent.getfileData(result);
                    var message = '<span class="fa fa-check-circle-o"></span> '+result.message;
                    $('#file-uploader-message').html(message);
                } else {
                    $('#file-uploader-message').html(message);
                    $('#file-uploader').show();
                }
            } else {
                $('#file-uploader-message').html(error);
                
            }
        });
    }
}

$('.file-submit').click( function(e) {
    e.preventDefault();
    var url  = sendURL;                    
	var data   = new FormData($('#fileUploadForm')[0]);
    sendData(data, url);                   
});