$(document).on('click', '.browse', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    var file = $('#'+id);
    file.trigger('click');
    file.change( function() {
        $('.'+id).val(file.val().replace(/C:\\fakepath\\/i, ''));
        $('.file-submit').show();
    }); 
});

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}


function sendData(data, url) {
    if(data) {
        var uid = getUrlVars()['uid'];
        var title = $("#p_title").val();
        data.append('uid',uid);
        data.append('title',title);
        var iconSuccess = '<span class="fa fa-check-circle-o"></span> ';
        var loading     = 'Uploading...';
        var error       = '<span style="color: red;"><b>There was an error uploading your file.</b></span>';
        $('#file-uploader-message').show();
        $('#file-uploader-message').html(loading);
        window.parent.startImport(true);
        $.ajax({
            url  : url,
            data : data,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type : 'post'
        }).done( function(response) {
            var result = JSON.parse(response);
            console.dir(result);
            if(result) {
                var message = '<span style="color: red;"><b>'+result.message+'</b></span>';
                if(result.status == 'success') {
                    window.parent.getfileData(result);
                    var message = '<span class="fa fa-check-circle-o"></span> '+result.message;
                    $('#file-uploader-message').html(message);
                } else {
                    window.parent.startImport(false);
                    $('#file-uploader-message').html(message);
                    $('#file-uploader').show();
                }
            } else {
                window.parent.startImport(false);
                $('#file-uploader-message').html(error);
                
            }
        });
    }
}

$('.file-submit').click( function(e) {
    e.preventDefault();
    var url  = sendURL;                    
	var data   = new FormData($('#fileUploadForm')[0]);
    sendData(data, url);                   
});