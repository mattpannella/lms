<?php


defined('_JEXEC') or die;

class AxsAccessibility {

	public static function generateTags($path) {
		$path = substr($path, strrpos($path, '/') + 1);
		$image = explode('.',$path);
		$text = str_replace('_', ' ', $image[0]);
		$text = str_replace('-', ' ', $text);
		$data = new stdClass();
		$data->alt = $text;
		//Going to leave off the title tag on auto generated tags
		//$data->title = $text;

		return $data;		
	}

	public static function query($path,$type) {
		$path = ltrim($path,'/');

		if($type == 'videos') {
			if(strpos($path, 'youtu') && strpos($path, '//')) {
				$path = AxsMedia::getVideoID($path, 'youtube');
			}

			if(strpos($path, 'vimeo') && strpos($path, '//')) {
				$path = AxsMedia::getVideoID($path, 'vimeo');
			}
		}		

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from('accessibility_'.$type)
			  ->where($db->quoteName('path') . '=' . $db->quote($path))
			  ->where('enabled= 1')
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;		
	}

	public static function image($path) {
		if($path) {
			$data = self::query($path,'images');
			if(!$data) {
				$data = self::generateTags($path);
			}
			$html = '';

			if($data->alt) {
				$html .= ' alt="'.$data->alt.'" ';
			}
			if(!empty($data->title)) {
				$html .= ' title="'.$data->title.'" ';
			}

			return $html;
		} else {
			return;
		}		
	}

	public static function audio($path) {
		$data = self::query($path,'audios');
		return $data;
	}

	public static function video($path) {
		$data = self::query($path,'videos');
		return $data;
	}

	public static function getCaptionTitle($language_code) {
		$languages = [
		  "AF" => "Afrikanns",
		  "SQ" => "Albanian",
		  "AR" => "Arabic",
		  "HY" => "Armenian",
		  "EU" => "Basque",
		  "BN" => "Bengali",
		  "BG" => "Bulgarian",
		  "CA" => "Catalan",
		  "KM" => "Cambodian",
		  "ZH" => "Chinese (Mandarin)",
		  "HR" => "Croation",
		  "CS" => "Czech",
		  "DA" => "Danish",
		  "NL" => "Dutch",
		  "EN" => "English",
		  "ET" => "Estonian",
		  "FJ" => "Fiji",
		  "FI" => "Finnish",
		  "FR" => "French",
		  "KA" => "Georgian",
		  "DE" => "German",
		  "EL" => "Greek",
		  "GU" => "Gujarati",
		  "HE" => "Hebrew",
		  "HI" => "Hindi",
		  "HU" => "Hungarian",
		  "IS" => "Icelandic",
		  "ID" => "Indonesian",
		  "GA" => "Irish",
		  "IT" => "Italian",
		  "JA" => "Japanese",
		  "JW" => "Javanese",
		  "KO" => "Korean",
		  "LA" => "Latin",
		  "LV" => "Latvian",
		  "LT" => "Lithuanian",
		  "MK" => "Macedonian",
		  "MS" => "Malay",
		  "ML" => "Malayalam",
		  "MT" => "Maltese",
		  "MI" => "Maori",
		  "MR" => "Marathi",
		  "MN" => "Mongolian",
		  "NE" => "Nepali",
		  "NO" => "Norwegian",
		  "FA" => "Persian",
		  "PL" => "Polish",
		  "PT" => "Portuguese",
		  "PA" => "Punjabi",
		  "QU" => "Quechua",
		  "RO" => "Romanian",
		  "RU" => "Russian",
		  "SM" => "Samoan",
		  "SR" => "Serbian",
		  "SK" => "Slovak",
		  "SL" => "Slovenian",
		  "ES" => "Spanish",
		  "SW" => "Swahili",
		  "SV" => "Swedish ",
		  "TA" => "Tamil",
		  "TT" => "Tatar",
		  "TE" => "Telugu",
		  "TH" => "Thai",
		  "BO" => "Tibetan",
		  "TO" => "Tonga",
		  "TR" => "Turkish",
		  "UK" => "Ukranian",
		  "UR" => "Urdu",
		  "UZ" => "Uzbek",
		  "VI" => "Vietnamese",
		  "CY" => "Welsh",
		  "XH" => "Xhosa"
		];

		if($languages[$language_code]) {
			return $languages[$language_code];
		} else {
			return false;
		}
	}	
}