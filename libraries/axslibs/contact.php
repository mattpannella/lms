<?php

defined('_JEXEC') or die;

class AxsContact {

	/**
	 * Return the contact page information for the client based on its ID.  This allows for multiple contact pages to be used.
	 * @param object 	$brand 				The brand to pull the contact page from
	 * @param integer 	$contactPageId 		The ID of the brand.
	 * @return object 						An object with the contact page information
	 */
	public static function getContactTemplate($brand, $contactPageId = 0) {
		
		$db = JFactory::getDbo();
		if ($contactPageId) {
			$condition = "id = ".(int)$contactPageId;
		} else {
			$condition = "FIND_IN_SET($brand, brands)";
		}

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from('axs_contact_page')
			->where($condition)
			->limit(1);

		$db->setQuery($query);
		$contactPage = $db->loadObject();

		return $contactPage;
	}
}