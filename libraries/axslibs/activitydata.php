<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

class AxsActivityData {

    public static function getUserTotal($params) {
        if(!$params->users) {
            return 0;
        }
    	$conditions[] = "block = 0";
    	$users = $params->users;

    	if($users) {
    	    if (is_array($users)) {
    	        $users = implode(',', $users);
            }
    		$conditions[] = "id IN ($users)";
    	}

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
              ->from('#__users')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return count($result);
        } else {
            return 0;
        }
    }

    public static function getLoginTotal($params) {
        if(!$params->users) {
            return 0;
        }
		$dateRange = $params->default_range;
    	$field     = 'date';
		$conditions[] = "action = 'login'";
		$conditions[] = self::getDateRangeConditions($dateRange,$field);
    	$users = $params->users;
    	if($users) {
            if (is_array($users)) {
                $users = implode(',', $users);
            }
    		$conditions[] = "user_id IN ($users)";
    	}
    	$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
              ->from('axs_actions')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return count($result);
        } else {
            return 0;
        }
	}


	public static function getLastVistTotal($params) {
        if(!$params->users) {
            return 0;
        }
		$dateRange = $params->default_range;
    	$field     = 'lastvisitDate';
		$conditions[] = "block = 0";
		$conditions[] = self::getDateRangeConditions($dateRange,$field);
    	$users = self::getUserList($params);
    	if($users) {
    		$conditions[] = "id IN ($users)";
    	}
    	$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
              ->from('#__users')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return count($result);
        } else {
            return 0;
        }
	}

    public static function truncate($string,$length) {
    	if(strlen($string) > $length) {
    		$string = substr($string,0,$length)."...";
    	}
    	return $string;
    }

    public static function getUserList($params) {

    	if($params->user_selection_type == 'all' && $params->usergroups_selection_type != 'selected') {
            
            return implode(',', AxsUser::getAllUserIds());
        } else if(
            $params->usergroups_selection_type == 'selected' &&
            $params->usergroup_filter &&
            ($params->user_selection_type == 'all' || !$params->userlist)
        ) {
            $axsAlerts = new AxsAlerts(null);

            $userListResult = $axsAlerts->getUsersFromGroupList($params->usergroup_filter);
            foreach($userListResult as $user) {
                $userListArray[] = (int)$user->user_id;
            }
            return implode(',',$userListArray);
        } else if($params->user_selection_type == 'selected' && $params->userlist) {
            return $params->userlist;
        }
        return null;
    }

    public static function getLoginLogoutDuration($data) {
        $db = JFactory::getDbo();
        $conditions[] = "action = 'logout'";
        $conditions[] = "user_id = $data->user_id";
        $conditions[] = "(DATE(date) = DATE('$data->date') AND date > '$data->date' )";
        $query = $db->getQuery(true);
        $query->select("date")
              ->from('axs_actions')
              ->where($conditions)
              ->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result->date;
    }

    public static function getActivitySiteVisitDuration($params,$start_date,$end_date,$defaultDuration) {
    	$field     = 'a.date';
		$conditions[] = "a.action = 'login'";
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "a.user_id IN ($params->users)";
    	$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("a.user_id,u.name,'Site Visit Duration' AS 'activity_type', 'Duration: ' AS 'activity_data',a.date")
              ->from('axs_actions as a')
              ->join('INNER','#__users as u ON u.id = a.user_id')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $data) {
            $timeLogin = strtotime($data->date);
            $logoutTime = self::getLoginLogoutDuration($data);
            if(!$logoutTime) {
                $timeLogout = strtotime($data->date." +".$defaultDuration."minutes");
            } else {
                $timeLogout = strtotime($logoutTime);
            }
            $timeDiff = $timeLogout - $timeLogin;
            $duration = gmdate('H:i:s', round($timeDiff));
            $data->activity_data = "Duration: ".$duration;

        }
        return $results;
    }

    public static function getActivityNewSiteVisitDuration($params,$start_date,$end_date) {
    	$field     = 'a.date';
		$conditions[] = "a.type = 'site'";
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "a.user_id IN ($params->users)";
    	$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("a.user_id,u.name,'Active Time on Site' AS 'activity_type', 'Duration: ' AS 'activity_data',a.date, a.duration AS 'seconds'")
              ->from('axs_duration as a')
              ->join('INNER','#__users as u ON u.id = a.user_id')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $data) {
            $duration = gmdate('H:i:s', round($data->seconds));
            $data->activity_data = "Duration: ".$duration;
        }
        return $results;
    }

    public static function getActivityCourseStarted($params,$start_date,$end_date) {

        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "cp.archive_date IS NULL";
        }
        $conditions[] = "cp.user_id IN ($params->users)";
        $field     = 'cp.date_started';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "cp.course_id IN ($courseList)";
        }
        $query = $db->getQuery(true);
        $query->select("cp.user_id,u.name,'Course Started' AS 'activity_type', CONCAT(c.title, '<br>','<b>Progress:</b> ',ROUND(cp.progress),'%') AS activity_data,cp.date_started AS date, cp.archive_date AS archive_date")
              ->from('#__splms_course_progress as cp')
              ->join('INNER','#__splms_courses as c ON c.splms_course_id = cp.course_id')
              ->join('INNER','#__users as u ON u.id = cp.user_id')
              ->where($conditions)
              ->group('cp.course_id,cp.user_id');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityCourseCompleted($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "cp.archive_date IS NULL";
        }
    	$conditions[] = "cp.user_id IN ($params->users)";
        $field     = 'cp.date_completed';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "cp.course_id IN ($courseList)";
        }
        $query = $db->getQuery(true);
        $query->select("cp.user_id,u.name,'Course Completed' AS 'activity_type', c.title AS activity_data,cp.date_completed AS date, cp.archive_date AS archive_date")
              ->from('#__splms_course_progress as cp')
              ->join('INNER','#__splms_courses as c ON c.splms_course_id = cp.course_id')
              ->join('INNER','#__users as u ON u.id = cp.user_id')
              ->where($conditions)
              ->group('cp.course_id,cp.user_id');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityLessonStarted($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "lp.archive_date IS NULL";
        }
        if ($params->users != 'all') {
            $conditions[] = "lp.user_id IN ($params->users)";
        }
        
        $field     = 'lp.date';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "lp.status = 'started'";
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "lp.course_id IN ($courseList)";
        }

        // Get all 'started' lesson progress rows
        $query = $db->getQuery(true);
        $query->select("lesson_id, user_id,'Lesson Started' AS 'activity_type', date, archive_date")
              ->from('#__splms_lesson_status as lp')
              ->where($conditions);
        $db->setQuery($query);
        $lessonProgress = $db->loadObjectList();

        // Get all course names and lesson names
        $query = $db->getQuery(true);
        $query->select("splms_lesson_id, CONCAT('<b>Lesson:</b> ', l.title, '<br>','<b>Course:</b> ', c.title) AS activity_data")
              ->from('joom_splms_lessons as l')
              ->innerJoin('joom_splms_courses as c ON c.splms_course_id = l.splms_course_id');

        if($params->course_selection_type == 'selected' && $courseList) {
            $query->where("c.splms_course_id IN (" .  $courseList . ")");
        }              
        $db->setQuery($query);
        $coursesLessons = $db->loadAssocList('splms_lesson_id');

        // Get all users that are a part of this report
        $query = $db->getQuery(true);
        $query->select("id, name")
            ->from('joom_users');
        if ($params->users != 'all') {
            $query->where("id IN (" .  $params->users . ")");
        }            
        $db->setQuery($query);
        $users = $db->loadAssocList('id');

        // Aggregate the data
        $results = array();
        foreach($lessonProgress as $progressRow) {
            if (empty($coursesLessons[$progressRow->lesson_id]['activity_data'])) {
                continue;
            }
            $result = new stdClass();
            $result->user_id = $progressRow->user_id;
            $result->name = $users[$progressRow->user_id]['name'];
            $result->activity_type = $progressRow->activity_type;
            $result->activity_data = $coursesLessons[$progressRow->lesson_id]['activity_data'];
            $result->date = $progressRow->date;
            $result->archive_date = $progressRow->archive_date;

            $results []= $result;
        }

        return $results;
    }

    public static function getActivityLessonCompleted($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "lp.archive_date IS NULL";
        }
        $conditions[] = "lp.user_id IN ($params->users)";
        $field     = 'lp.date';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "lp.status = 'completed'";
        $query = $db->getQuery(true);
        $query->select("lp.lesson_id, lp.user_id, u.name, 'Lesson Completed' AS 'activity_type', lp.date, lp.archive_date AS archive_date")
              ->from('#__splms_lesson_status as lp')
              ->join('INNER','#__users as u ON u.id = lp.user_id')
              ->where($conditions);
        $db->setQuery($query);
        $lessonCompletionData = $db->loadObjectList();
        

        $query = $db->getQuery(true);
        $query->select("splms_lesson_id, CONCAT('<b>Lesson:</b> ', l.title, '<br>','<b>Course:</b> ', c.title) AS activity_data");
        $query->from('joom_splms_lessons as l');
        $query->innerJoin('joom_splms_courses as c ON c.splms_course_id = l.splms_course_id');

        if($params->course_selection_type == 'selected' && $courseList) {
            $query->where("c.splms_course_id IN (" .  $courseList . ")");
        }
        $db->setQuery($query);
        $lessonCourseData = $db->loadAssocList('splms_lesson_id');

        $results = array();
        foreach($lessonCompletionData as $lessonCompletionData) {
            if (empty($lessonCourseData[$lessonCompletionData->lesson_id]['activity_data'])) {
                continue;
            }
            $result = new stdClass();
            $result->user_id = $lessonCompletionData->user_id;
            $result->name = $lessonCompletionData->name;
            $result->activity_type = $lessonCompletionData->activity_type;
            $result->activity_data = $lessonCourseData[$lessonCompletionData->lesson_id]['activity_data'];
            $result->date = $lessonCompletionData->date;
            $result->archive_date = $lessonCompletionData->archive_date;

            $results []= $result;
        }

        return $results;
    }

    public static function getActivityCertificateAwarded($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$certificatesList = $params->certificates;
        $conditions[] = "a.user_id IN ($params->users)";
        $field = 'a.date_earned';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "b.type = 'certificate'";
        if($params->certificates_selection_type == 'selected' && !empty($certificatesList)) {
        	$conditions[] = "a.badge_id IN ($certificatesList)";
        }
        $query = $db->getQuery(true);
        $query->select("a.user_id,u.name,'Certificate Awarded' AS 'activity_type', b.title AS activity_data,a.date_earned as date")
              ->from('axs_awards_earned as a')
              ->join('INNER','axs_awards as b ON a.badge_id = b.id')
              ->join('INNER','#__users as u ON u.id = a.user_id')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityBadgeAwarded($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$badgesList = $params->badges;
        $conditions[] = "a.user_id IN ($params->users)";
        $field = 'a.date_earned';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "b.type = 'badge'";
        if($params->badges_selection_type == 'selected' && !empty($badgesList)) {
        	$conditions[] = "a.badge_id IN ($badgesList)";
        }
        $query = $db->getQuery(true);
        $query->select("a.user_id,u.name,'Badge Awarded' AS 'activity_type', b.title AS activity_data,a.date_earned as date")
              ->from('axs_awards_earned as a')
              ->join('INNER','axs_awards as b ON a.badge_id = b.id')
              ->join('INNER','#__users as u ON u.id = a.user_id')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityMilestoneAchieved($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$milestonesList = $params->milestones;
        $conditions[] = "a.user_id IN ($params->users)";
        $field = 'a.date_earned';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $conditions[] = "b.type = 'milestone'";
        if($params->milestone_selection_type == 'selected' && $milestonesList) {
        	$conditions[] = "a.badge_id IN ($milestonesList)";
        }
        $query = $db->getQuery(true);
        $query->select("a.user_id,u.name,'Milestones Achieved' AS 'activity_type', b.title AS activity_data,a.date_earned as date");
        $query->from('axs_awards_earned as a');
        $query->join('INNER','axs_awards as b ON a.badge_id = b.id');
        $query->join('INNER','#__users as u ON u.id = a.user_id');
        if($conditions) {
            $query->where($conditions);
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityLessonActivitySubmitted($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "lp.archive_date IS NULL";
        }
        $conditions[] = "lp.user_id IN ($params->users)";
        $field = 'lp.date_submitted';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "lp.course_id IN ($courseList)";
        }
        $query = $db->getQuery(true);
        $query->select("lp.user_id,u.name,'Lesson Activity Submitted' AS 'activity_type', CONCAT('<b>Activity:</b> ',lp.activity_request,CASE WHEN lp.completed = 1 THEN ' <b>(completed)</b>' ELSE '' END, '<br>','<b>Lesson:</b> ',l.title,'<br>','<b>Course:</b> ',c.title) AS activity_data,lp.date_submitted as date,lp.archive_date AS archive_date");
        $query->from('#__splms_student_activities as lp');
        $query->join('INNER','#__splms_lessons as l ON l.splms_lesson_id = lp.lesson_id');
        $query->join('INNER','#__splms_courses as c ON c.splms_course_id = lp.course_id');
        $query->join('INNER','#__users as u ON u.id = lp.user_id');
        if($conditions) {
            $query->where($conditions);
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityWatchedVideo($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
        $conditions[] = "v.user_id IN ($params->users)";
        $field = 'v.date';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "v.archive_date IS NULL";
        }
        $query = $db->getQuery(true);
        $query->select("v.user_id,u.name,'Watched Video' AS 'activity_type', v.video_title AS activity_data,v.date,v.archive_date AS archive_date");
        $query->from('axs_video_tracking as v');
        $query->join('INNER','#__users as u ON u.id = v.user_id');
        if($conditions) {
            $query->where($conditions);
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityEventRegistration($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
        $conditions[] = "e.user_id IN ($params->users)";
        $field = 'e.register_date';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $query = $db->getQuery(true);
        $query->select("e.user_id,u.name,'Event Registration' AS 'activity_type', ev.title AS activity_data,e.register_date as date");
        $query->from('#__eb_registrants as e');
        $query->join('INNER','#__eb_events as ev ON ev.id = e.event_id');
        $query->join('INNER','#__users as u ON u.id = e.user_id');
        if($conditions) {
            $query->where($conditions);
        }
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getActivityQuizTaken($params,$start_date,$end_date) {
        $db = JFactory::getDbo();
    	$courseList = $params->courses;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "qr.archive_date IS NULL";
        }
        $conditions[] = "qr.user_id IN ($params->users)";
        $field     = 'qr.date';
        $dateConditions = self::getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "qr.splms_course_id IN ($courseList)";
        }
        $query = $db->getQuery(true);
        $query->select("qr.user_id, u.name, q.title AS q_title, qr.point AS q_point, qr.total_marks AS qr_total_marks, qr.date,qr.archive_date AS archive_date, qr.splms_lesson_id");
        $query->from('#__splms_quizresults as qr');
        $query->join('INNER','#__splms_quizquestions as q ON q.splms_quizquestion_id = qr.splms_quizquestion_id');
        $query->join('INNER','#__users as u ON u.id = qr.user_id');
        if($conditions) {
            $query->where($conditions);
        }
        $db->setQuery($query);
        $quizResults = $db->loadObjectList();

        // Get all course names and lesson names
        $query = $db->getQuery(true);
        $query->select("splms_lesson_id, l.title AS lesson_title, c.title AS course_title")
              ->from('joom_splms_lessons as l')
              ->innerJoin('joom_splms_courses as c ON c.splms_course_id = l.splms_course_id');
        if($params->course_selection_type == 'selected' && $courseList) {
            $query->where("c.splms_course_id IN ($courseList)");
        }
        $db->setQuery($query);
        $lessonCourses = $db->loadAssocList('splms_lesson_id');

        $quizSlashSurvey = AxsLanguage::text("AXS_QUIZ_SLASH_SURVEY", "Quiz/Survey");
        $score = AxsLanguage::text("AXS_SCORE", "Score");
        $lesson = AxsLanguage::text("AXS_LESSON", "Lesson");
        $course = AxsLanguage::text("AXS_COURSE", "Course");
        $quizOrSurveyTaken = AxsLanguage::text("AXS_QUIZ_OR_SURVEY_TAKEN", "Quiz or Survey Taken");

        $results = array();
        foreach($quizResults as $quizResult) {
            if (empty($lessonCourses[$quizResult->splms_lesson_id]) || empty($lessonCourses[$quizResult->splms_lesson_id]['lesson_title'])) {
                continue;
            }
            $quizResult->activity_type = $quizOrSurveyTaken;
            $quizResult->activity_data =
                "<br/><b>$quizSlashSurvey:</b>" . $quizResult->q_title . "<br/>"
                . "<br/><b>$score:</b>" . $quizResult->q_point . '/' . $quizResult->qr_total_marks . "<br/>"
                . "<br/><b>$lesson:</b>" . $lessonCourses[$quizResult->splms_lesson_id]['lesson_title'] . "<br/>"
                . "<br/><b>$course:</b>" . $lessonCourses[$quizResult->splms_lesson_id]['course_title'] . "<br/>"
            ;
            $results []= $quizResult;
        }
        return $results;
    }


    public static function getCoursesStats($params) {
        if(!$params->users) {
            return 0;
        }
    	$userList = '';
    	$courseList = $params->courses;

        $db = JFactory::getDbo();
        $conditions[] = "enabled = 1";
        if($params->course_selection_type == 'selected' && $courseList) {
        	$conditions[] = "splms_course_id IN ($courseList)";
        }
        $query = $db->getQuery(true);
        $query->select("splms_course_id AS id, title")
        	  ->from("#__splms_courses")
        	  ->where($conditions);
        $db->setQuery($query);
        $courses = $db->loadObjectList();
        $coursesStatsArray = array();
        $coursesRegistrations = array();
        $coursesLastActivity = array();
        $coursesCompletions = array();
        $coursesAverageCompletions = array();
        $coursesTitles = array();


        foreach($courses as $course) {
            $coursesStats = new stdClass();
            $params->course_id = $course->id;
            $params->field = 'date_started';
            $coursesStats->start_count = self::getCourseDataCount($params);
            $params->field = 'date_last_activity';
            $coursesStats->last_activity_count = self::getCourseDataCount($params);
            $params->field = 'date_completed';
            $coursesStats->completed_count = self::getCourseDataCount($params);

            if($coursesStats->start_count > 0) {

                $coursesStats->average_completion = round(($coursesStats->completed_count / $coursesStats->start_count) * 100);
            } else {

                $coursesStats->average_completion = 0;
            }

            $coursesStats->title = $course->title;
            array_push($coursesRegistrations, (int)$coursesStats->start_count);
            array_push($coursesLastActivity, (int)$coursesStats->last_activity_count);
            array_push($coursesCompletions, (int)$coursesStats->completed_count);
            array_push($coursesAverageCompletions, (int)$coursesStats->average_completion);
            array_push($coursesTitles, $coursesStats->title);
            $coursesStatsArray[$course->id] = $coursesStats;
        }
        $courseStats = new stdClass();
        $courseStats->data = $coursesStatsArray;
        $courseStats->coursesRegistrations = $coursesRegistrations;
        $courseStats->coursesLastActivity = $coursesLastActivity;
        $courseStats->coursesCompletions = $coursesCompletions;
        $courseStats->coursesAverageCompletions = $coursesAverageCompletions;
        $courseStats->coursesTitles = $coursesTitles;

        return $courseStats;
    }

    public static function getDateRangeConditions($dateRange,$field) {
    	switch($dateRange) {
			case 'all' :
				$dateRangeCondition = "( DATE($field) <= DATE(CURDATE()) )";
			break;
			case 'day' :
				$dateRangeCondition = "( DATE($field) = DATE(CURDATE()) )";
			break;
			case 'week' :
				$dateRangeCondition = "( DATE($field) >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK) AND DATE($field) <= DATE(CURDATE()) )";
			break;
			case 'month' :
				$dateRangeCondition = "( DATE($field) >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH) AND DATE($field) <= DATE(CURDATE()) )";
			break;
			case 'year' :
				$dateRangeCondition = "( DATE($field) >= DATE_SUB(CURDATE(), INTERVAL 1 YEAR) AND DATE($field) <= DATE(CURDATE()) )";
			break;
		}
		return $dateRangeCondition;
    }

    public static function getStartEndDateRangeConditions($start_date,$end_date,$field) {
        if($start_date < 1 && $end_date < 1 ) {
            return '';
        }

        if($start_date > 0) {
            $startCondition = "DATE($field) >= DATE('$start_date')";
        }

        if($end_date > 0) {
            $connector = "";
            if($start_date > 0) {
                $connector = " AND ";
            }
            $endCondition = "$connector DATE($field) <= DATE('$end_date')";
        }

		$dateRangeCondition = "( $startCondition $endCondition )";

		return $dateRangeCondition;
    }

    public static function getCourseDataCount($params) {
    	$dateRange = $params->default_range;
    	$field     = $params->field;
    	$course_id = $params->course_id;
        if ($params->include_archived_activity != null && $params->include_archived_activity == "0") {
            $conditions []= "archive_date IS NULL";
        }
   		$conditions[] = "course_id = $course_id";

   		if($params->users) {
    		$conditions[] = "user_id IN ($params->users)";
    	}

		$conditions[] = self::getDateRangeConditions($dateRange,$field);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id')
              ->from('#__splms_course_progress')
              ->where($conditions)
              ->group('course_id,user_id');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return count($result);
        } else {
            return 0;
        }
    }

    public static function getCertificateStats($params) {
        if(!$params->users) {
            return 0;
        }
    	$userList = '';
    	$certificatesList = $params->certificates;
        $db = JFactory::getDbo();
        $conditions[] = "enabled = 1";
        if($params->certificates_selection_type == 'selected' && $certificatesList) {
        	$conditions[] = "id IN ($certificatesList)";
        }

        $query = $db->getQuery(true);
        $query->select("*")
        	  ->from("axs_awards")
        	  ->where($conditions);
        $db->setQuery($query);
        $certificates = $db->loadObjectList();
        $certificatesAwardedArray = array();
        $certificatesExpiredArray = array();
        $certificatesTitles = array();

        foreach($certificates as $certificate) {
            $certificatesStats = new stdClass();
            $params->certificate_id = $certificate->id;
            $params->field = 'date_earned';
            $awarded = self::getCertificateDataCount($params);
            $params->field = 'date_expires';
            $expired = self::getCertificateDataCount($params);
            $certificatesStats->title = $certificate->title;
            array_push($certificatesAwardedArray, (int)$awarded);
            array_push($certificatesExpiredArray, (int)$expired);
            array_push($certificatesTitles, $certificate->title);
        }
        $certificatesStats = new stdClass();
        $certificatesStats->awarded = $certificatesAwardedArray;
        $certificatesStats->expired = $certificatesExpiredArray;
        $certificatesStats->certificatesTitles = $certificatesTitles;

        return $certificatesStats;
    }

    public static function getCertificateDataCount($params) {
    	$dateRange      = $params->default_range;
    	$field          = $params->field;
    	$certificate_id = $params->certificate_id;
   		$conditions[]   = "badge_id = $certificate_id";

   		if($params->users) {
    		$conditions[] = "user_id IN ($params->users)";
    	}

		$conditions[] = self::getDateRangeConditions($dateRange,$field);

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('badge_id')
              ->from('axs_awards_earned')
              ->where($conditions)
              ->group('badge_id,user_id');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return count($result);
        } else {
            return 0;
        }
    }

    public static function getAssignmentUserList($assignment) {
        if($assignment->user_ids) {
            $assignedUsers = explode(',',$assignment->user_ids);

        } elseif($assignment->usergroups) {

            $axsAlerts = new AxsAlerts(null);

            $groupUsers = $axsAlerts->getUsersFromGroupList($assignment->usergroups);
            foreach($groupUsers as $user) {
                $uid = (int)$user->user_id;
                $assignedUsers[] = $uid;
            }
        }

        $userList = implode(',',$assignedUsers);
        if(!$userList) {
            return false;
        } else {
        	return $userList;
        }
    }

    public static function filterUserList($allUserList,$filterUserlist) {
    	if(!$filterUserlist) {
    		return explode(',',$allUserList);
    	}
        $filterUserlistArray = explode(',',$filterUserlist);
        $allUserListArray = explode(',',$allUserList);
        $count = count($allUserListArray);
        for($i = 0; $i < $count; $i++) {
        	if(!in_array($allUserListArray[$i], $filterUserlistArray)) {
        		unset($allUserListArray[$i]);
        	}
        }
        if(count($allUserListArray) > 0 ) {
        	return $allUserListArray;
        } else  {
        	return false;
        }

    }

    public static function getAssignmentStats($params) {
        if(!$params->users) {
            return 0;
        }
    	$userList = '';
    	$assignmentsList = $params->assignments;
    	$dashboardUsersFilter = $params->users;
        $db = JFactory::getDbo();
        $conditions = array();

        if($params->assignments_selection_type == 'selected' && $assignmentsList) {
        	$conditions[] = "id IN ($assignmentsList)";
        }

        $query = $db->getQuery(true);
        $query->select("*");
        $query->from("#__splms_courses_groups");
        if($conditions) {
        	$query->where($conditions);
        }
        $db->setQuery($query);
        $assignments = $db->loadObjectList();
        $assignmentsCompletedArray = array();
        $assignmentsTitles = array();

        foreach($assignments as $assignment) {
        	$assignedUserList   = self::getAssignmentUserList($assignment);
			$filteredUserList   = self::filterUserList($assignedUserList,$dashboardUsersFilter);
			// $params->users = $filteredUserList;
			$params->course_ids = $assignment->course_ids;
            $assignmentsStats = new stdClass();
            $params->assignment_id = $assignment->id;
            $params->assignment_id = $assignment->id;
            $completed = self::getAssignmentDataCount($params, $filteredUserList);
            $assignmentsStats->title = $assignment->title;
            array_push($assignmentsCompletedArray, (int)$completed);
            array_push($assignmentsTitles, $assignment->title);
        }
        $assignmentsStats = new stdClass();
        $assignmentsStats->assignmentsAverageCompletions = $assignmentsCompletedArray;
        $assignmentsStats->assignmentsTitles = $assignmentsTitles;

        return $assignmentsStats;
    }

    public static function getAssignmentDataCount($params, $users) {
    	$progressArray = array();
    	foreach($users as $user) {
			$progressArray[] = AxsLMS::getAssignmentProgress($params->course_ids,$user)->percentageComplete;
    	}
    	$average = round(array_sum($progressArray) / count($progressArray));
    	if($average) {
    		return $average;
    	} else {
    		return 0;
    	}
    }

}