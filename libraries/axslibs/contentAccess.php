<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
defined('_JEXEC') or die;

class AxsContentAccess {

	public static function getAccessConditions($userId = null) {

		if($userId) {
			$user_id = $userId;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$levels = JAccess::getAuthorisedViewLevels($user_id);
		$groups = JAccess::getGroupsByUser($user_id);

		$levelsCheck  = [];
		$groupsCheck  = [];
		$conditionSet = [];

		foreach($levels as $level) {
			$levelsCheck[] = " ( FIND_IN_SET( $level, accesslevel ) > 0 ) ";
		}

		foreach($groups as $group) {
			$groupsCheck[] = " ( FIND_IN_SET( $group, usergroup ) > 0 ) ";
		}

		$levelsSplit = implode(' OR ', $levelsCheck);
		$groupsSplit = implode(' OR ', $groupsCheck);

		$conditionSet[] = "  ( access_type = 'access' AND ( $levelsSplit ) ) ";
		$conditionSet[] = "  ( access_type = 'user'   AND ( $groupsSplit ) ) ";
		$conditionSet[] = "  ( access_type = 'list'   AND ( FIND_IN_SET( $user_id, userlist ) > 0 ) ) ";
		$conditionSet[] = "  ( access_type = 'none' OR access_type IS NULL ) ";

		$conditionsSplit = implode(' OR ', $conditionSet);

        $conditions = "($conditionsSplit)";

		return $conditions;
	}

	public static function checkCategoryAccess($userId,$categoryId) {
		if(!$categoryId) {
			return true;
		}
		if($userId) {
			$user_id = $userId;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn('splms_coursescategory_id').'='.(int)$categoryId
		);

		$accessConditions = self::getAccessConditions($user_id);

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query->select('*')
			->from($db->qn('#__splms_coursescategories'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$result = $db->loadObject();

		if($result) {
			if($result->parent_id) {
				$parentAccess = self::checkCategoryAccess($userId,$result->parent_id);
				if($parentAccess) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}

		} else {
			return false;
		}
	}

	public function checkVisibilityAccess($userId,$courseId) {

		if($userId) {
			$user_id = $userId;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn('splms_course_id').'='.(int)$courseId
		);

		$accessConditions = self::getAccessConditions($user_id);

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query->select('*')
			->from($db->qn('#__splms_courses'))
			->where($conditions)
			->order('ordering ASC');
		$db->setQuery($query);
		$result = $db->loadObject();

		if($result) {
			return true;
		} else {
			return false;
		}
	}


	public function checkMediaVisibilityAccess($userId,$mediaId) {

		if($userId) {
			$user_id = $userId;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$conditions = array(
			$db->qn('enabled').'='.(int)1,
			$db->qn('id').'='.(int)$mediaId
		);

		$accessConditions = self::getAccessConditions($user_id);

		if($accessConditions) {
			array_push($conditions,$accessConditions);
		}

		$query->select('*')
			->from($db->qn('axs_gin_media'))
			->where($conditions)
			->limit(1);
		$db->setQuery($query);
		$mediaAccess = $db->loadObject();

		if($mediaAccess) {
			return true;
		} else {
			return false;
		}
	}

	public function checkContentAccess($userId,$params) {
		$result = false;
		if($userId) {
			$user_id = $userId;
		} else {
			$user_id = JFactory::getUser()->id;
		}

		$levels = JAccess::getAuthorisedViewLevels($user_id);
		$groups = JAccess::getGroupsByUser($user_id);
		$accesLevels = explode(',', $params->access);

		if($params->access_purchase_type) {

			if($params->access_purchase_type == 'access') {
				foreach($levels as $level) {
					if(in_array($level, $accesLevels)) {
						$result = true;
					}
				}
			}

			if($params->access_purchase_type == 'list') {
				$users = explode(',', $params->userlist_purchase_register);
				if(in_array($user_id, $users)) {
					$result = true;
				}
			}

			if($params->access_purchase_type == 'user') {
				$grouplist = explode(',', $params->usergroup_purchase_register);
				foreach($groups as $group) {
					if(in_array($group, $grouplist)) {
						$result = true;
					}
				}
			}

			if($params->access_purchase_type == 'none') {
				$result = true;
			}

		} else {
			foreach($levels as $level) {
				if(in_array($level, $accessLevels)) {
					$result = true;
				}
			}
		}

		return $result;
	}

}