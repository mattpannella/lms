<?php

defined('_JEXEC') or die;
require_once JPATH_LIBRARIES . '/axslibs/sso/metadata_reader.php';
require_once '/var/www/files/creds.php';

class AxsOkta {

    private $token = '00ogX6KIfub7rfsnmZ6bUn3DbXwLqIjk-_0zPR297t';
    private $defaultGroupId = 'Tovuti Client Support';
    private $tovutiGroups = [
        'Tovuti LMS SSO Active',
        'Tovuti LMS SSO OnBoarding',
        'Tovuti LMS SSO Internal',
        'Tovuti LMS SSO Trials',
        'Tovuti LMS SSO Admin Exceptions'
    ];
    private $groups = array();
    private $domain;
    private $label;
    private $spEntityId;
    private $idpId = 204568;
    private $acsUrl;
    private $appId;
    private $save = true;
    private $db = null;
    private $clientDB;
    private $clientStatus;
    private $internalAppId = 'authorizessologin-tovuti-support-28956';

    public function __construct($params) {
        $this->setRootDBAccess();
        foreach($params as $key => $value) {
            $this->$key = $value;
        }
        if(!$params->spEntityId) {
            $this->spEntityId = AxsSSO::generateUUID();
        }
        if(!$params->groups) {
            $this->groups = array($this->defaultGroupId);
        }
        if($params->clientStatus && !$params->groups) {
            $this->setGroupsFromStatus();
        }
        $this->acsUrl = "https://".$this->domain."/authorizessologin?idp=".$this->idpId;
    }

    public function createClientApp() {
        $appBuild = $this->buildApp();
        $app      = json_decode($appBuild);
        $this->appId = $app->id;
        $this->assignAppToGroups();
        $metadata = $this->getMetadata();
        if($this->save) {
            $result = $this->saveApp($metadata);
        }
        return $app->id;
    }

    public function updateClientAppGroups() {
        $db = $this->db;
        $table = "{$this->clientDB}.axs_tovuti_support_sso";
        $app = $this->getClientApp();
        $this->appId = $app->app_id;
        $groups = $this->getAppGroups();
        $this->removeAppFromGroups($groups);
        $this->assignAppToGroups();
        $group_mapping = new stdClass();
        $i = 0;
        foreach($this->tovutiGroups as $group) {
            $map = 'group_mapping'.$i;
            $groupSettings = new stdClass();
            $groupSettings->user_group = 32;
            $groupSettings->idp_group  = $group;
            $group_mapping->$map = $groupSettings;
            $i++;
        }
        $app->group_mapping = json_encode($group_mapping);
        $response = $db->updateObject($table,$app,'id');
    }

    private function setRootDBAccess() {
        $creds   = dbCreds::getCreds();
        $options = array();
        $options['driver']   = 'mysqli';
        $options['host']     = $creds->dbhost;
        $options['user']     = $creds->dbuser;
        $options['password'] = $creds->dbpass;
        $options['database'] = $creds->dbname;
        $this->db = JDatabaseDriver::getInstance($options);
    }

    public function getClientApp() {
        $db = $this->db;
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from("`{$this->clientDB}`.`axs_tovuti_support_sso`");
        $query->where('id=204568');
        $db->setQuery($query);
        return $db->loadObject();
    }

    public function getClientDomains() {
        $db = $this->db;
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from("`{$this->clientDB}`.`axs_domains`");
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public function setAllClientAppDomains() {
        $domains = $this->getClientDomains();
        $mainDomain = "https://{$domains[0]->domain}/{$this->internalAppId}";
        if(!$this->appId) {
            $app = $this->getClientApp();
            $this->setAppId($app->app_id);
        }
        $clientApp = $this->getAppById($this->appId);
        $clientAppObject = json_decode($clientApp);
        $acsEndpoints = array();
        $i = 0;
        foreach($domains as $domain) {
            $newACS = new stdClass();
            $newACS->index = $i;
            $newACS->url =  "https://{$domain->domain}/{$this->internalAppId}";
            array_push($acsEndpoints,$newACS);
            $i++;
        }
        $clientAppObject->settings->signOn->ssoAcsUrl = $mainDomain;
        $clientAppObject->settings->signOn->recipient = $mainDomain;
        $clientAppObject->settings->signOn->destination = $mainDomain;
        $clientAppObject->settings->signOn->allowMultipleAcsEndpoints = true;
        $clientAppObject->settings->signOn->acsEndpoints = $acsEndpoints;
        return $this->updateClientApp($clientAppObject);
    }

    public function addClientAppDomains($domain) {
        if(!$this->appId) {
            $app = $this->getClientApp();
            $this->setAppId($app->app_id);
        }
        $clientApp = $this->getAppById($this->appId);
        $clientAppObject = json_decode($clientApp);
        $acsEndpoints = $clientAppObject->settings->signOn->acsEndpoints;
        $newACS = new stdClass();
        $newACS->index = count($acsEndpoints);
        $newACS->url =  "https://{$domain}/{$this->internalAppId}";
        array_push($acsEndpoints,$newACS);
        $clientAppObject->settings->signOn->acsEndpoints = $acsEndpoints;
        return $this->updateClientApp($clientAppObject);
    }

    private function assignAppToGroups() {
        $appId = $this->appId;
        foreach($this->groups as $group) {
            $groupId = $this->getGroupId($group);
            $params = new stdClass();
            $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId/groups/$groupId";
            $params->type = 'PUT';
            $this->sendRequest($params);
            sleep(1);
        }
    }

    public function updateClientApp($app) {
        $appId = $this->appId;
        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId";
        $params->type = 'PUT';
        $params->body = $app;
        return $this->sendRequest($params);
        sleep(1);
    }

    private function removeAppFromGroups($groups) {
        $appId = $this->appId;
        foreach($groups as $group) {
            $params = new stdClass();
            $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId/groups/$group->id";
            $params->type = 'DELETE';
            $this->sendRequest($params);
            sleep(1);
        }
    }

    private function getAppGroups() {
        $appId = $this->appId;
        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId/groups";
        $params->type = 'GET';
        $groups = $this->sendRequest($params);
        return json_decode($groups);
        sleep(1);
    }

    private function sendRequest($params) {
        if($params->contentType) {
            $contentType = $params->contentType;
        } else {
            $contentType = 'json';
        }
        $curl = curl_init($params->url);
        $header = array();
        $header[] = "Accept: application/$contentType";
        $header[] = "Content-Type: application/$contentType";
        $header[] =  "Authorization: SSWS ".$this->token;
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $params->type);
        if($params->body) {
            curl_setopt($curl, CURLOPT_POSTFIELDS,json_encode($params->body));
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function getAppById($appId) {
        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId";
        $params->type = 'GET';
        return $this->sendRequest($params);
    }

    public function setAppId($appId) {
        $this->appId = $appId;
    }

    public function deactivateApp($appId) {
        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId/lifecycle/deactivate";
        $params->type = 'POST';
        return $this->sendRequest($params);
    }

    private function buildApp() {
        $domain = $this->domain;
        $label  = $this->label;
        $idp    = $this->idpId;
        $spEntityId = $this->spEntityId;
        $settings   = new stdClass();
        $app        = new stdClass();
        $signOn     = new stdClass();
        $visibility = new stdClass();
        $body       = new stdClass();
        $logo       = new stdClass();

        $logo->name = "medium";
        $logo->href = "https://ok12static.oktacdn.com/fs/bcg/4/gfsti720leUEa7pMQ5d6";
        $logo->type = "image/png";

        $body->label = $label;
        $body->signOnMode = "SAML_2_0";

        $app->acsURL = $this->acsUrl;
        $app->spEntityId = $spEntityId;

        $visibility->autoSubmitToolbar = false;
        $body->visibility = $visibility;

        $signOn->ssoAcsUrl = $this->acsUrl;
        $signOn->audience = $spEntityId;
        $signOn->defaultRelayState= "";
        $signOn->recipient= $this->acsUrl;
        $signOn->destination= $this->acsUrl;
        $signOn->subjectNameIdTemplate= '${user.userName}';
        $signOn->subjectNameIdFormat= "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress";
        $signOn->responseSigned= true;
        $signOn->assertionSigned= true;
        $signOn->signatureAlgorithm= "RSA_SHA256";
        $signOn->digestAlgorithm= "SHA256";
        $signOn->honorForceAuthn= true;
        $signOn->authnContextClassRef= "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport";
        $signOn->spIssuer= null;
        $signOn->requestCompressed= false;

        $attributeStatementsArray = array();
        $attributes = ["firstName","lastName"];
        foreach($attributes as $attribute) {
            $attributeStatements = new stdClass();
            $attributeStatements->type = "EXPRESSION";
            $attributeStatements->name = $attribute;
            $attributeStatements->namespace = "urn:oasis:names:tc:SAML:2.0:attrname-format:unspecified";
            $attributeStatements->values = array("user.".$attribute);
            array_push($attributeStatementsArray,$attributeStatements);
        }

        $groups = new stdClass();
        $groups->type = "GROUP";
        $groups->name = "groups";
        $groups->namespace = "urn:oasis:names:tc:SAML:2.0:attrname-format:uri";
        $groups->filterType = "REGEX";
        $groups->filterValue = ".*.";
        array_push($attributeStatementsArray,$groups);

        $credentials = new stdClass();
        $userNameTemplate = new stdClass();
        $userNameTemplate->template = '${source.email}';
        $userNameTemplate->type = "BUILT_IN";
        $credentials->userNameTemplate = $userNameTemplate;

        $signOn->attributeStatements = $attributeStatementsArray;
        $settings->app = $app;
        $settings->signOn = $signOn;

        $body->settings = $settings;
        $body->credentials = $credentials;

        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps";
        $params->type = 'POST';
        $params->body = $body;
        return $this->sendRequest($params);
    }

    private function getMetadata() {
        $appId = $this->appId;
        $params = new stdClass();
        $params->url  = "https://tovutiteam-internal.okta.com/api/v1/apps/$appId/sso/saml/metadata";
        $params->type = 'GET';
        $params->contentType = 'xml';
        $response = $this->sendRequest($params);
        $document = new DOMDocument();
        $document->loadXML($response);
        restore_error_handler();
        $first_child = $document->firstChild;
        if(!empty($first_child)) {
            $metadata = new IDPMetadataReader($document);
            $identity_providers = $metadata->getIdentityProviders();
            if(empty($identity_providers)) {
            echo 'error1';
            return;
            }
            foreach($identity_providers as $key => $idp){
                $saml_login_url = $idp->getLoginURL('HTTP-Redirect');
                $saml_issuer = $idp->getEntityID();
                $saml_x509_certificate = $idp->getSigningCertificate();
                $data = new stdClass();
                $data->idp_entity_id = $saml_issuer;
                $data->single_signon_service_url =  $saml_login_url;
                $data->certificate = implode("\n/////////Certificate/////////\n",$saml_x509_certificate);
                break;
            }
        }
        if($data) {
            return $data;
        } else {
            return false;
        }
    }

    private function saveApp($params) {
        $db = $this->db;
        $table = "{$this->clientDB}.axs_tovuti_support_sso";
        $group_mapping = new stdClass();
        $i = 0;
        foreach($this->tovutiGroups as $group) {
            $map = 'group_mapping'.$i;
            $groupSettings = new stdClass();
            $groupSettings->user_group = 32;
            $groupSettings->idp_group  = $group;
            $group_mapping->$map = $groupSettings;
            $i++;
        }

        $data = new stdClass();
        $data->id = 204568;
		$data->title = "Tovuti Support";
		$data->idp_entity_id = $params->idp_entity_id;
		$data->single_signon_service_url = $params->single_signon_service_url;
		$data->single_logout_url = null;
		$data->binding = "HttpRedirect";
		$data->name_id_format = "urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress";
		$data->certificate = $params->certificate;
		$data->enable_email = 1;
		$data->username = "NameID";
		$data->email = "NameID";
		$data->name = "firstName,lastName";
		$data->grp = "groups";
		$data->uninstall_feedback = 0;
		$data->enabled = 1;
        $data->app_id = $this->appId;
		$data->field_mapping = '{"field_mapping0":{"field":"19","attribute":"firstName"},"field_mapping1":{"field":"20","attribute":"lastName"}}';
		$data->group_mapping = json_encode($group_mapping);
		$data->params = '{"sp_issuer":"'.$this->spEntityId.'","enable_field_mapping":"1","enable_group_mapping":"1","enable_update_group_mapping":"1","enable_update_field_mapping":"1","default_user_group":"2","add_link":"0","add_to_menu":"defaultpublic","link_name":"Login","oauth_idp":"","cognito_app_domain":"","oauth_callback_url":"'.$this->acsUrl.'","oauth_authorize_endpoint":"","oauth_token_endpoint":"","oauth_userinfo_endpoint":"","email_attribute":"","oauth_app_name":"","oauth_client_id":"","oauth_client_secret":"","oauth_app_scopes":"","saml_idp":"okta","last_menu":null}';
		$data->type = "SAML";
		$data->metadata_endpoint = "";
		$data->metadata_auto_update = 0;
        return $db->insertObject($table,$data);
    }


    public function createTable() {
        $db = $this->db;
        $table = "`{$this->clientDB}`.axs_tovuti_support_sso";
        $query = "DROP TABLE $table";
        $db->setQuery($query);
        $db->execute();

        $query = "CREATE TABLE $table (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `title` varchar(255) DEFAULT NULL,
            `idp_entity_id` varchar(255) NOT NULL,
            `single_signon_service_url` varchar(255) NOT NULL,
            `single_logout_url` varchar(255) DEFAULT NULL,
            `binding` varchar(255) NOT NULL,
            `name_id_format` varchar(255) NOT NULL,
            `certificate` varchar(4096) NOT NULL,
            `enable_email` tinyint(1) NOT NULL,
            `username` varchar(255) NOT NULL,
            `email` varchar(255) NOT NULL,
            `name` varchar(255) NOT NULL,
            `grp` varchar(255) NOT NULL,
            `uninstall_feedback` int(2) NOT NULL,
            `enabled` tinyint(4) DEFAULT NULL,
            `field_mapping` text,
            `group_mapping` text,
            `params` text,
            `type` varchar(255) DEFAULT NULL,
            `metadata_endpoint` text,
            `metadata_auto_update` tinyint(4) DEFAULT NULL,
            `app_id` varchar(255) NOT NULL,
            PRIMARY KEY (`id`)
        )";
        $db->setQuery($query);
        return $db->execute();
    }

    public function setGroupsFromStatus() {
        switch($this->clientStatus) {
            case 'active':
                $this->groups = ['Tovuti LMS SSO Active'];
                break;
            case 'active (onboarding)':
                $this->groups = ['Tovuti LMS SSO OnBoarding'];
                break;
            case 'inactive':
                $this->groups = ['Tovuti Client Support'];
                break;
            case 'trial':
                $this->groups = ['Tovuti LMS SSO Trials'];
                break;
            case 'test':
                $this->groups = ['Tovuti LMS SSO Internal'];
                break;
        }
    }

    public function getGroupId($groupName) {
        $id = null;
        switch($groupName) {
            case 'Tovuti LMS SSO Active':
                $id = '00gvfif69exwmceT05d6';
                break;
            case 'Tovuti LMS SSO OnBoarding':
                $id = '00gvfi9xe4Kz3C7xI5d6';
                break;
            case 'Tovuti LMS SSO Trials':
                $id = '00gvfid165JrPNQXt5d6';
                break;
            case 'Tovuti LMS SSO Internal':
                $id = '00gvfii8foVmdXoCC5d6';
                break;
            case 'Tovuti Client Support':
                $id = '00gt8j1el8SD3QNkt5d6';
                break;
        }
        return $id;
    }

}