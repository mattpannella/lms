<?php

defined('_JEXEC') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class AxsZoomMeetings {

    /**
     * Get a set of Zoom meeting metadata from the database by a UUID tied to our general meeting data
     *
     * @param $uuid string UUID that will be used to look up associated Zoom meeting information
     * @return stdClass Object containing Zoom meeting metadata
     */
    public static function getZoomMeetingByUUID($uuid) {
        $result = null;

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')
              ->from('axs_zoom_meetings')
              ->where("meeting_id = {$db->quote($uuid)}");

        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return $result;
        } else {
            return null;
        }
    }

    public static function deleteZoomMeetingByZoomID($zoomId) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('axs_zoom_meetings')
              ->where("zoom_id = '$zoomId'");

        $db->setQuery($query);
        $db->execute();
    }

    public static function deleteZoomMeetingByUUID($uuid) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->delete('axs_zoom_meetings')
              ->where("meeting_id = '$uuid'");

        $db->setQuery($query);
        $db->execute();
    }

    public static function createZoomMeetingWithUUID($uuid, $hostId, $apiParams) {
        $createdMeeting = self::createZoomMeetingViaApi($hostId, $apiParams);
        $zoomMeetingData = new stdClass();
        $zoomMeetingData->zoom_id = $createdMeeting->id;
        $zoomMeetingData->join_url = $createdMeeting->join_url;
        $zoomMeetingData->meeting_id = $uuid;

        // Store the meeting information in axs_zoom_meetings
        $db = JFactory::getDBO();
        $db->insertObject('axs_zoom_meetings', $zoomMeetingData);

        return $createdMeeting->join_url;
    }

    public static function createZoomMeetingViaApi($hostId, $meetingInfo) {
		$api = AxsZoomApi::getInstance();

        $result = $api->create("users/$hostId/meetings", $meetingInfo);

		return json_decode($result);
    }

    public static function deleteZoomMeetingViaApi($zoomId) {
        $api = AxsZoomApi::getInstance();

        $api->delete("meetings/$zoomId");
    }

    public static function updateZoomMeetingViaApi($zoomId, $meetingInfo) {
		$api = AxsZoomApi::getInstance();

		$result = $api->update("meetings/$zoomId", $meetingInfo);

		return json_decode($result);
    }

    public static function getZoomUsers($licensed = false) {
        $api = AxsZoomApi::getInstance();

        $options = new stdClass();

        $options->page_size = 300;
        $options->status = 'active';

        $zoomUsers = json_decode($api->read('users', $options), false);

        $users = $zoomUsers->users;

        if($licensed && !empty($users)) {

            // Now, filter out any users that are not licensed
            $users = array_filter($users, function($host) {
                return $host->type == AxsZoomApi::USER_TYPE_LICENSED;
            });
        }

        return $users;
    }

    public static function getZoomUserById($zoomUserId) {
        $api = AxsZoomApi::getInstance();

        $userInfo = $api->read("users/$zoomUserId");

        return json_decode($userInfo);
    }
}