	// input - <input> DOM element
	// arr - an array of objects with the shape { itemName: "Menu Title", uri: "/index.php?option=com_foo&view=bar", subItems: { itemName: "Menu Title", uri: "/index.php?option=com_foo&view=bar" } }
	function autocomplete(inp, arr) {
		// currentFocus is the index of the currently ative menu item
		var currentFocus;

		var callback =  function(e) {
			var a, b, i, val = this.value;
			// Close any already open lists of autocompleted values
			closeAllLists();
			var items = arr;
			
			currentFocus = -1;
			// Create a DIV element that will contain the items (values):
			a = document.createElement("DIV");
			a.setAttribute("id", this.id + "autocomplete-list");
			a.setAttribute("class", "autocomplete-items dropdown-menu d-block hide-scrollbar");
			a.setAttribute("style", "margin-top:4px;max-height:80vh;overflow-y:scroll;");
			// Append the DIV element as a child of the autocomplete container:*/
			this.parentNode.appendChild(a);

			if (val.length == 0) {
				// load the most recentItems
				var recentSearchItems = JSON.parse(window.localStorage.getItem('recentSearchItems')) || [];
				items = recentSearchItems;
				
				var header = document.createElement("span");
				header.setAttribute("class", "dropdown-header");
				header.append('Recent Searches');
				a.appendChild(header);
			}

			// Results will store the items that are matched and should be 
			// rendered in the dropdown
			let results = [];
			items.forEach(item => {
				// Check if the item starts with the same letters as the text field value
				// or if any of the subItems has the same letters ad the text field value
				const userInput = val.substr(0, val.length).toUpperCase();
				let matchExists = false;
				var topLevelNavItem = item.itemName;
				var bottomLevelNavItem = item.subItemName;
				var dropdownItemHtml = "";
				if (!bottomLevelNavItem) {
					if (userInput == topLevelNavItem.trim().substr(0, val.length).toUpperCase()) {
						matchExists = true;
						dropdownItemHtml += "<strong>" + topLevelNavItem.substr(0, val.length) + "</strong>";
						dropdownItemHtml += topLevelNavItem.substr(val.length);
					}
					// Setting these values to empty strings will prevent undefined errors
					// and cause all the following checks to be false
					topLevelNavItem = "";
					bottomLevelNavItem = "";
				}
				const topLevelNavItemCompare = topLevelNavItem.trim().substr(0, val.length).toUpperCase();
				const bottomLevelNavItemCompare = bottomLevelNavItem.trim().substr(0, val.length).toUpperCase();
				
				const bottomLevelNavItemWords = bottomLevelNavItem.split(' ');
				const bottomLevelNavItemWordsUppercase = bottomLevelNavItemWords.map(word => word.trim().substr(0, val.length).toUpperCase());
				const bottomLevelNavItemWordIndex = bottomLevelNavItemWordsUppercase.indexOf(userInput);

				const foundInTags = !item.hasOwnProperty('tags') ? false : item.tags.find(el => el.trim().substr(0, val.length).toUpperCase() == userInput);
				
				if (bottomLevelNavItemWordIndex != -1) {
					// Both top and bottom nav items should be highlighted\
					matchExists = true;
					dropdownItemHtml += bottomLevelNavItemWords.slice(0, bottomLevelNavItemWordIndex).join(' ');
					if (dropdownItemHtml.length > 0) {
						dropdownItemHtml += ' ';
					}
					dropdownItemHtml += "<strong>" + bottomLevelNavItemWords[bottomLevelNavItemWordIndex].substr(0, val.length) + "</strong>";
					dropdownItemHtml += bottomLevelNavItemWords[bottomLevelNavItemWordIndex].substr(val.length);
					dropdownItemHtml += ' ';
					dropdownItemHtml += bottomLevelNavItemWords.slice(bottomLevelNavItemWordIndex + 1, bottomLevelNavItemWords.length).join(' ');
					// dropdownItemHtml += bottomLevelNavItem.substr(val.length);
				} else if (userInput == topLevelNavItemCompare || foundInTags) {
					// Top level nav item should be highlighted.
					// Most top level nav items do not have corresponding uris so this is just
					// a means of grouping the bottom level nav items
					matchExists = true;
					dropdownItemHtml += bottomLevelNavItem;
				} else if (userInput == bottomLevelNavItemCompare) {
					// Bottom level nav_item should be highlighted
					matchExists = true;
					dropdownItemHtml += "<strong>" + bottomLevelNavItem.substr(0, val.length) + "</strong>";
					dropdownItemHtml += bottomLevelNavItem.substr(val.length);
				}

				if (matchExists) {
					// Look for the matched top level nav item in the results
					let existingItem = results.find(res => res.itemName == item.itemName);
					if (!existingItem) {
						results.push({itemName: item.itemName, subItems: []});
						existingItem = results.find(res => res.itemName == item.itemName);
					}
					existingItem.subItems.push({ itemName: item.subItemName, uri: item.uri, html: dropdownItemHtml });
				}
			});

			var bottomLevelNavItemRenderedCount = 0;
			for (let i = 0; i < results.length; i++) {
				const topLevelNavItem = results[i].itemName;
				const bottomLevelNavItems = results[i].subItems;
				var header = document.createElement("span");
				header.setAttribute("class", "dropdown-header");
				var icon = document.createElement("I");
				icon.setAttribute("class", "fas fa-circle");
				header.appendChild(icon);
				header.append(topLevelNavItem);
				a.appendChild(header);
				bottomLevelNavItems.sort((a, b) => a > b).forEach(item => {
					// Create a DIV element for each matching element:
					b = document.createElement("DIV");
					b.setAttribute("class", "dropdown-item d-block");
					b.setAttribute("style", "cursor:pointer");
					b.setAttribute("data-idx", bottomLevelNavItemRenderedCount++);
					b.innerHTML = item.html;
					// Insert a input field that will hold the current array item's value
					b.innerHTML += "<input type='hidden' value='" + item.uri + "'>";
					// Execute a function when someone clicks on the item value (DIV element)
					b.addEventListener("click", (e) => {
						// Grab the uri, we are going to navigate to it
						const uri = e.target.getElementsByTagName("input")[0].value;

						// Add the item to recent search items,
						// if it's already added, move it to the top
						var recentSearchItems = JSON.parse(window.localStorage.getItem('recentSearchItems')) || [];
						var exists = false;
						recentSearchItems.forEach(searchItem => {
							if (searchItem.subItemName == item.itemName) {
								exists = true;
							}
						});
						if (!exists) {
							recentSearchItems.unshift({itemName: topLevelNavItem, subItemName: item.itemName, uri: item.uri});
						} else {
							recentSearchItems = recentSearchItems.filter(searchItem => item.itemName != searchItem.subItemName);
							recentSearchItems.unshift({itemName: topLevelNavItem, subItemName: item.itemName, uri: item.uri});
						}
						// Limit recent nav items to 6
						if (recentSearchItems.length >= 6) {
							recentSearchItems.pop();
						}
						window.localStorage.setItem('recentSearchItems', JSON.stringify(recentSearchItems));
						closeAllLists();
						location.replace(`${window.location.protocol}//${window.location.hostname}/administrator/${uri}`);
					});

					// Set dropdown item to active if the cursor hovers over it
					b.addEventListener('mouseenter', (e) => {
						currentFocus = e.target.dataset.idx;
						var x = document.getElementById(this.id + "autocomplete-list");
						if (x) x = x.getElementsByTagName("div");
						addActive(x);
					});
					a.appendChild(b);
				});
			};
			if(results.length == 0) {
				b = document.createElement("DIV");
				var message = '0 Recent Items'; 
				if (!val) {
					message = 'Nothing Found...'
				}
				b.innerHTML = "<span class='dropdown-item'>0 Recent Items</span>";
				a.appendChild(b);
			} else {
				// Trigger the callback for keydown for the down arrow so the 
				// first item in the dropdown is selected
				inp.dispatchEvent(new KeyboardEvent('keydown',{'keyCode':40}));
			}
		}
		inp.addEventListener("input", callback);
		inp.addEventListener("click", callback);
		// Key bindings
		inp.addEventListener("keydown", function(e) {
			var x = document.getElementById(this.id + "autocomplete-list");
			if (x) x = x.getElementsByTagName("div");
			if (e.keyCode == 40) {
				// If the arrow DOWN key is pressed
				// increase the currentFocus variable:
				currentFocus++;
				// and and make the current item active
				addActive(x);
			} else if (e.keyCode == 38) { //up
				// If the arrow UP key is pressed,
				// decrease the currentFocus variable
				currentFocus--;
				// and and make the current item active
				addActive(x);
			} else if (e.keyCode == 13) {
				// If the ENTER key is pressed, prevent the form from being submitted
				e.preventDefault();
				if (currentFocus > -1) {
					// and simulate a click on the "active" item:
					if (x) x[currentFocus].click();
				}
			} else if (e.keyCode == 27) {
				closeAllLists();
			}
			var y = document.getElementsByClassName('autocomplete-active');
			if (y.length > 0) {
				var selectedItemPosition = y[0].offsetTop;
				var selectedItemHeight = y[0].clientHeight;
				var list = document.getElementById(this.id + "autocomplete-list");
				// If the item is below the dropdown menu
				if ((selectedItemPosition + selectedItemHeight) > list.clientHeight ) {
					// Scroll the menu down
					list.scrollTop = selectedItemPosition + selectedItemHeight;
				} else if((selectedItemPosition - list.scrollTop) < list.offsetTop) {
					// Scroll the menu up
					list.scrollTop = selectedItemPosition - (selectedItemHeight + 34);
				}
			}
		});
		function addActive(x) {
			// A function to classify an item as "active":*/
			if (!x) return false;
			// Start by removing the "active" class on all items:*/
			removeActive(x);
			if (currentFocus >= x.length) currentFocus = 0;
			if (currentFocus < 0) currentFocus = (x.length - 1);
			// Add class "autocomplete-active":*/
			x[currentFocus].classList.add("autocomplete-active");
		}
		function removeActive(x) {
			// A function to remove the "active" class from all autocomplete items:
			for (var i = 0; i < x.length; i++) {
				x[i].classList.remove("autocomplete-active");
			}
		}
		function closeAllLists(elmnt) {
			/*close all autocomplete lists in the document,
			except the one passed as an argument:*/
			var x = document.getElementsByClassName("autocomplete-items");
			for (var i = 0; i < x.length; i++) {
				if (elmnt != x[i] && elmnt != inp) {
				x[i].parentNode.removeChild(x[i]);
			}
		}
	}
	// Execute a function when someone clicks in the document:
	document.addEventListener("click", function (e) {
		closeAllLists(e.target);
	});
} 