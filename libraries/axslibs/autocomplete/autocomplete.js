// original code taken from : https://www.w3schools.com/howto/howto_js_autocomplete.asp

// input - <input> DOM element
// arr - an array of objects with the shape { text: "Menu Title", uri: "/index.php?option=com_foo&view=bar" }
function autocomplete(inp, arr) {
    // console.log(arr);
      /*the autocomplete function takes two arguments,
      the text field element and an array of possible autocompleted values:*/
      var currentFocus;
      /*execute a function when someone writes in the text field:*/
      inp.addEventListener("input", function(e) {
          var a, b, i, val = this.value;
          /*close any already open lists of autocompleted values*/
          closeAllLists();
          if (!val) { return false;}
          currentFocus = -1;
          /*create a DIV element that will contain the items (values):*/
          a = document.createElement("DIV");
          a.setAttribute("id", this.id + "autocomplete-list");
          a.setAttribute("class", "autocomplete-items");
          /*append the DIV element as a child of the autocomplete container:*/
          this.parentNode.appendChild(a);
          /*for each item in the array...*/
          for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            const userInput = val.substr(0, val.length).toUpperCase();
            let matchExists = false;
            var [ topLevelNavItem, bottomLevelNavItem ] = arr[i].text.split(" > ");
            var dropdownItemHtml = "";
            if (!bottomLevelNavItem) {
              if (userInput == topLevelNavItem.trim().substr(0, val.length).toUpperCase()) {
                matchExists = true;
                dropdownItemHtml += "<strong>" + topLevelNavItem.substr(0, val.length) + "</strong>";
                dropdownItemHtml += topLevelNavItem.substr(val.length);
              }
              // setting these values to empty strings will prevent undefined errors
              // and cause all the following checks to be false
              topLevelNavItem = "";
              bottomLevelNavItem = "";
            }
            const topLevelNavItemCompare = topLevelNavItem.trim().substr(0, val.length).toUpperCase();
            const bottomLevelNavItemCompare = bottomLevelNavItem.trim().substr(0, val.length).toUpperCase();
            if (userInput == topLevelNavItemCompare && userInput == bottomLevelNavItemCompare) {
              // both top and bottom nav items should be highlighted
              matchExists = true;
              dropdownItemHtml += "<strong>" + topLevelNavItem.substr(0, val.length) + "</strong>";
              dropdownItemHtml += topLevelNavItem.substr(val.length);
              dropdownItemHtml += " > ";
              dropdownItemHtml += "<strong>" + bottomLevelNavItem.substr(0, val.length) + "</strong>";
              dropdownItemHtml += bottomLevelNavItem.substr(val.length);
            } else if (userInput == topLevelNavItemCompare) {
              // top level nav item should be highlighted
              // note that most top level nav items do not have corresponding uris so this is just
              // a means of grouping the bottom level nav items
              matchExists = true;
              dropdownItemHtml += "<strong>" + topLevelNavItem.substr(0, val.length) + "</strong>";
              dropdownItemHtml += topLevelNavItem.substr(val.length);
              dropdownItemHtml += " > ";
              dropdownItemHtml += bottomLevelNavItem;
            } else if (userInput == bottomLevelNavItemCompare) {
              // bottom level nav_item should be highlighted
              matchExists = true;
              dropdownItemHtml += topLevelNavItem;
              dropdownItemHtml += " > ";
              dropdownItemHtml += "<strong>" + bottomLevelNavItem.substr(0, val.length) + "</strong>";
              dropdownItemHtml += bottomLevelNavItem.substr(val.length);
            }
            // console.log(dropdownItemHtml);
            if (matchExists) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              /*make the matching letters bold:*/
              b.innerHTML = dropdownItemHtml;
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + arr[i].uri + "'>";
              /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function(e) {
                  /*insert the value for the autocomplete text field:*/
                  const uri = this.getElementsByTagName("input")[0].value;
                  /*close the list of autocompleted values,
                  (or any other open lists of autocompleted values:*/
                  location.replace(`${window.location.protocol}//${window.location.hostname}/administrator/${uri}`);
                  closeAllLists();
              });
              a.appendChild(b);
            }
          }
  
          // if we didn't find any elements, show a little message
          if(a.childElementCount == 0) {
              b = document.createElement("DIV");
              b.innerHTML = "<span>Nothing found...</span>";
              a.appendChild(b);
          }
      });
      /*execute a function presses a key on the keyboard:*/
      inp.addEventListener("keydown", function(e) {
          var x = document.getElementById(this.id + "autocomplete-list");
          if (x) x = x.getElementsByTagName("div");
          if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
          } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
              /*and simulate a click on the "active" item:*/
              if (x) x[currentFocus].click();
            }
          }
      });
      function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
      }
      function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
          x[i].classList.remove("autocomplete-active");
        }
      }
      function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
          if (elmnt != x[i] && elmnt != inp) {
          x[i].parentNode.removeChild(x[i]);
        }
      }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
    } 