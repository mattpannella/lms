<?php

/**
 * Created by PhpStorm.
 * User: mar
 * Date: 12/29/15
 * Time: 4:26 PM
 */
defined('_JEXEC') or die;
jimport('joomla.application.component.helper');

class AxsExtra {
    public static $rewards_quotient;
    public static $grace_period;
    public static $brand_id;
    private static $dispatcher;

    private static $loaded = array();

    public static function init($dispatcher, $brand) {
        self::$dispatcher = $dispatcher;

        self::$brand_id = $brand->id;
        self::$rewards_quotient = $brand->billing->rewards_quotient;
        self::$grace_period = !empty($brand->site_details->grace_period) ? $brand->site_details->grace_period : null;

        //seed random number gen
        list($usec, $sec) = explode(' ', microtime());
        srand((float) $sec + ((float) $usec * 100000));
    }

	//MISCELLANEOUS
	public static function getUserLevels($userId = null) {

		$userData = new stdClass();

		$imageFolder = "/".AxsImages::getImagesPath()."/badges/";

		/*
				RETURN

				$userData->affiliate->level
														->name
														->image

				$userData->member->level
												 ->name
												 ->image

				$userData->recognitions[]   //array

														[]->id
														[]->name
														[]->image
		*/

		if (!$userId) {
			$userId = JFactory::getUser()->id;
		}

		$userData->affiliate = new stdClass();

		$userExtras = self::getUserExtras($userId);

		if ($userExtras) {
			$userData->affiliate->level = $userExtras->affiliate_level;
		} else {
			$userData->affiliate->level = NULL;
		}

		$affLevels = self::getAffiliateLevels();
		for($i = 0; $i < count($affLevels); $i++) {
			if ($userData->affiliate->level == $affLevels[$i]->level_id) {
				$userData->affiliate->name = $affLevels[$i]->name;
				break;
			}
		}

		$level = $userData->affiliate->level;
		if ($level != null && $level != "") {
			$userData->affiliate->image = $imageFolder . "affiliate_" . $level . ".png";
		} else {
			$userData->affiliate->image = null;
		}

		/*
				$affiliateLevel  -   $affiliateLevelName

				1   -   Affiliate
				2   -   Bronze
				3   -   Silver
				4   -   Gold
				5   -   Platinum
		*/

		$subscriptions = AxsPayment::getUserSubscriptions($userId);

		$brand =  AxsBrands::getBrand();

		$active = false;
		foreach($subscriptions as $sub) {
			$plan = $sub->getPlan();
			$brandPlans = $brand->site_details->plans_for_active;

			if ($sub->status == "ACT" && in_array($sub->plan_id, $brandPlans)) {
				$active = true;
			}
		}


		//Not paid.  Done.
		if ($active) {

			$userGroups = JUserHelper::getUserGroups($userId);

			$lev = (float)self::filterUsergroupsForLevel($userGroups);

			$source = null;

			switch ($lev) {
                case 1:     $name = "level_1.png";              $member_level = 14; break;
                case 2:     $name = "level_2.png";              $member_level = 15; break;
                case 3:     $name = "level_3.png";              $member_level = 16; break;
                case 4:     $name = "level_4.png";              $member_level = 17; break;
                case 4.5:   $name = "level_5_candidate.png";    $member_level = 33; break;
                case 5:     $name = "level_5.png";              $member_level = 18; break;
                case 5.5:   $name = "level_6_candidate.png";    $member_level = 34; break;
                case 6:     $name = "level_6.png";              $member_level = 19; break;
                case 6.5:   $name = "level_7_candidate.png";    $member_level = 48; break;
                case 7:     $name = "level_7.png";              $member_level = 50; break;
            }

            $source = $imageFolder . $name;

            $userData->member = new stdClass();
            $userData->member->level = $member_level;
            $userData->member->name = self::getUsergroupName($userData->member->level);
            $userData->member->image = $source;

			/*
					$userLevel  -   $userLevelName

					14  -   Level 1
					15  -   Level 2
					16  -   Level 3
					17  -   Level 4
					18  -   Level 5
					19  -   Level 6
                    50  -   Level 7

					33  -   Level 5 Candidate
					34  -   Level 6 Candidate
                    48  -   Level 7 Candidate
			*/

			$recognitionList = [13,20,25,26,27,28,29,30,44];

			$innerCircle = false;

			$userData->recognitions = array();
			foreach($userGroups as $userGroup) {

				if (!is_numeric($userGroup)) {
					$userGroup = (int)$userGroup;
				}

				if (in_array($userGroup, $recognitionList)) {

					$rec = new stdClass();

					//Only include one Inner Circle badge
					if ($userGroup == 13 || $userGroup == 27) {
						if ($innerCircle == false) {
							$innerCircle = true;
							$userGroup = 13;
						} else {
							continue;
						}
					}

					$rec->id = $userGroup;
					$rec->name = self::getUsergroupName($userGroup);

					switch ($rec->id) {
						case 13: $imageName = "inner_circle";  break;
						case 20: $imageName = "sanctioned_speaker";  break;
						case 25: $imageName = "visionary";  break;
						case 26: $imageName = "founder";  break;
						case 29: $imageName = "PSP_sanctioned_speaker";  break;
						case 30: $imageName = "PSP_certified";  break;
						case 44: $imageName = "BDP";  break;
						default: $imageName = null;  break;
					}


					if ($imageName != null) {
						$rec->image = $imageFolder . "badges_" . $imageName . ".png";

					} else {
						$rec->image = null;
					}

					array_push($userData->recognitions, $rec);

				}
			}

			/*
					Recognitions

					13  -   Inner Circle
					20  -   Sanctioned Speaker
					25  -   Visionary
					26  -   Founder
					27  -   Inner Circle 2
					28  -   Translators
					29  -   PSP Sanctioned Speaker
					30  -   PSP Certified

			*/
		}

		return $userData;

	}

	public static function setAdminAction($type_id, $user_id, $data_row, $reason) {
		$db  = JFactory::getDBO();
		$col = array (
			'date',
			'type_id',
			'admin_id',
			'user_id',
			'data_row',
			'reason'
		);
		$val = array (
			$db->q(date("Y-m-d H:i:s")),
			(int)$type_id,
			(int)JFactory::getUser()->id,
			(int)$user_id,
			$db->q($data_row),
			$db->q($reason)
		);
		$query = $db->getQuery(true);
		$query->insert($db->qn('axs_gin_admin_actions'))
			->columns($db->qn($col))
			->values(implode(',', $val));
		$db->setQuery($query);
		$db->execute();
	}

	public static function getLanguage() {
		$currentLanguage = JFactory::getLanguage();
		$lang = $currentLanguage->get('tag');
		switch ($lang) {
			case "fr-FR": $language = "french"; break;
			default: $language = "english"; break;
		}
		return $language;
	}


	public static function removeMessage($app, $error) {
		$appReflection = new ReflectionClass(get_class($app));
		$_messageQueue = $appReflection->getProperty('_messageQueue');
		$_messageQueue->setAccessible(true);
		$messages = $app->getMessageQueue();
		foreach($messages as $key=>$message) {
			if(strpos($message['message'], $error) !== false) {
				unset($messages[$key]);
			}
		}
		$_messageQueue->setValue($app,$messages);
	}

    public static function format_date($date, $showTime = false) {
        $lang = self::getLanguage();
        $return = '';
        if ($date != null && !preg_match('/[0]{4}-[0]{2}-[0]{2} [0]{2}:[0]{2}:[0]{2}/', $date)) {
            if($showTime) {
                switch($lang) {
                    case 'english':
                        $return = (new DateTime($date))->format('m/d/Y h:ia (T)');
                        break;
                    case 'french':
                        $return = (new DateTime($date))->format('d-m-Y h:ia (T)');
                        break;
                    default:
                        break;
                }
            } else {
                switch($lang) {
                    case 'english':
                        $return = (new DateTime($date))->format('m/d/Y');
                        break;
                    case 'french':
                        $return = (new DateTime($date))->format('d-m-Y');
                        break;
                    default:
                        break;
                }
            }
        }
        return $return;
    }

	public static function getUserProfileData() {
        if(func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        $ret = new stdClass();

        //now fetch the address and profile info
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$user_id
        );
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->qn('#__community_fields_values'))
            ->where($cond);
        $db->setQuery($query);
        $rows = $db->loadObjectList();

        foreach($rows as $row) {
            switch($row->field_id) {
                case 19:
                    $ret->first = $row->value;
                    break;
                case 20:
                    $ret->last = $row->value;
                    break;
                case 8:
                    $ret->address = $row->value;
                    break;
                case 10:
                    $ret->city = $row->value;
                    break;
                case 9:
                    $ret->state = $row->value;
                    break;
                case 11:
                    $ret->country = $row->value;
                    break;
                case 23:
                    $ret->zip = $row->value;
                    break;
                case 2:
                    $ret->gender = $row->value;
                    break;
                case 3:
                    $ret->birthdate = $row->value;
                    break;
                case 6:
                    $ret->phone = $row->value;
                    break;
            }
        }

        //fetch the user's photo and alias
        $query->clear();
        $query
          ->select('*')
          ->from($db->qn('#__community_users'))
          ->where($db->qn('userid') . '='.(int)$user_id);
        $db->setQuery($query);
        $row = $db->loadObject();
        if (is_object($row)) {
            $ret->photo = $row->avatar != '' && file_exists(JPATH_ROOT.'/'.$row->avatar) ? '/'.$row->avatar : '/images/user.png';
            $ret->alias = $row->alias;
        }

        //fetch the user's email and name if blank, and signup date
        $query->clear();
        $query
          ->select('*')
          ->from($db->qn('#__users'))
          ->where($db->qn('id') . '='.(int)$user_id);
        $db->setQuery($query);
        $user = $db->loadObject();
        $ret->email = $user->email;
				$ret->signup = $user->registerDate;
        if($ret->first == '') {
            $nameArray = explode(' ', $user->name);
            $ret->first = $nameArray[0];
            $ret->last = $nameArray[1];
        }

        return $ret;
    }

    public static function getCourses() {
        $db = JFactory::getDBO();
        $cond = array (
            $db->qn('enabled').'='.(int)1
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('#__splms_courses'))
            ->where($cond);
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public static function getCourseById($id) {
	    $db = JFactory::getDBO();
	    $cond = array (
		    $db->qn('enabled').'='.(int)1,
		    $db->qn('splms_course_id').'='.(int)$id
	    );
	    $query = $db->getQuery(true);
	    $query->select('*')
		    ->from($db->qn('#__splms_courses'))
		    ->where($cond);
	    $db->setQuery($query);
	    return $db->loadObject();
    }

	public static function getUserCourseActivity($user_id) {
        $db  = JFactory::getDbo();
        $db->setQuery("SELECT b.*, c.title as lesson_title, d.title as quiz_title
                                        FROM joom_splms_quizresults b
                                        JOIN joom_splms_lessons c ON b.splms_lesson_id=c.splms_lesson_id
                                        JOIN joom_splms_quizquestions d ON b.splms_quizquestion_id=d.splms_quizquestion_id
                                        WHERE b.user_id=$user_id AND b.archive_date IS NULL;");
        return $db->loadObjectList();
    }

    public static function getUserCourseProgress($user_id) {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*, MAX(progress) as progress,  MAX(date_completed) as date_completed')
              ->from('#__splms_course_progress cp')
              ->innerJoin('#__splms_courses c ON c.splms_course_id = cp.course_id')
              ->where("cp.archive_date IS NULL AND user_id = {$db->quote($user_id)}")
              ->group('user_id, course_id');

        $db->setQuery($query);

         return $db->loadObjectList();
    }

    public static function accessLevelName($accessLevel) {
        if ($accessLevel == null) {
            return null;
        }

        $db  = JFactory::getDBO();
        $conditions = array(
            $db->quoteName('id') . '=' . $accessLevel
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('#__viewlevels'))
            ->where($conditions);
        $db->setQuery($query);
        $row = $db->loadObject();
        return $row->title;
    }

    /**
     * Get an access level by its name
     *
     * @param string $accessLevelName
     * @return stdObject | null Access level record matching the given access level name, or null if nothing is found
     */
    public static function getAccessLevelByName($accessLevelName) {

        if ($accessLevelName == null) {

            return null;
        } else {
            
            $accessLevelName = strip_tags($accessLevelName);
        }

        $db  = JFactory::getDBO();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from($db->quoteName('#__viewlevels'))
              ->where('title = ' . $db->quote($accessLevelName));

        $db->setQuery($query);

        $accessLevel = $db->loadObject();

        return $accessLevel;
    }

    public static function checkViewLevel() {
        $accessLevel = func_get_arg(0);
        if(func_num_args() > 1) {
            $user_id = func_get_arg(1);
        } else {
            $user_id = JFactory::getUser()->id;
        }
        //include inherited groups
        jimport('joomla.access.access');
        $usergroups = JUserHelper::getUserGroups($user_id);

        if( (in_array(31, $usergroups)) || (in_array(8, $usergroups))  || (in_array(21, $usergroups)) ){
            return true;
        }
        $viewLevels = JAccess::getAuthorisedViewLevels($user_id);
        if(in_array($accessLevel, $viewLevels)){
            return true;
        }
        return false;
    }

    public static function getUserSignupDate() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }
        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('registerDate')
            ->from($db->quoteName('#__users'))
            ->where($db->qn('id').'='.(int)$userid);
        $db->setQuery($query);
        $row = $db->loadObject();
        return is_object($row) ? $row->registerDate : '';
    }

    public static function getUserUsergroups() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }
        //include inherited groups
        jimport('joomla.access.access');
        return JUserHelper::getUserGroups($userid);
    }

    public static function getUsergroupName($usergroup) {

        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('#__usergroups'))
            ->where($db->qn('id').'='.(int)$usergroup);
        $db->setQuery($query);
        $row = $db->loadObject();
        return isset($row) ? $row->title : '';
    }

		/**
		 * @param $usergroups
		 *
		 * @return int
		 * @deprecated
		 */
    public static function filterUsergroupsForLevel($usergroups) {
        $level = 0;

        foreach ($usergroups as $group) {
            $newLevel = 0;
            //if the group is within the range of GIN levels 1-6
            if ($group >= 14 && $group <= 19) {
                $newLevel = $group - 13;
            }


            switch ($group) {
                case 33:    //Level 5 candidate
                    $newLevel = 4.5;
                    break;
                case 34:    //Level 6 candidate
                    $newLevel = 5.5;
                    break;
                case 48:    //Level 7 candidate
                    $newLevel = 6.5;
                    break;
                case 50:    //Level 7
                    $newLevel = 7;
                    break;

            }

            //if the group is higher than the current, this is the new current.
            if ($newLevel > $level) {
                $level = $newLevel;
            }
        }

        return $level;
    }

		/**
		 * @return bool
		 * @deprecated
		 */
    public static function hasBoughtItem() {
        $item_id = func_get_arg(0);
        if(func_num_args() > 1) {
            $userid = func_get_arg(1);
        } else {
            $userid = JFactory::getUser()->id;
        }

        //check if they have the free usergroup.
        jimport('joomla.access.access');
        $usergroups = JUserHelper::getUserGroups($userid);
        if( (in_array(8, $usergroups)) || (in_array(31, $usergroups)) ) {
            return true;
        }
        $allowed_courses = array(1,4,5,6,7,8);
        if( (in_array(21, $usergroups)) && (in_array($item_id, $allowed_courses)) ) {
            return true;
        }

        //look up in DB
        $db  = JFactory::getDBO();
        $conditions = array(
            $db->quoteName('user_id') . '=' . (int)$userid,
            $db->quoteName('type') . '=' . (int)0,
            $db->quoteName('item_id') . '=' . (int)$item_id,
            '('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').')'
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->quoteName('cctransactions'))
            ->where($conditions);
        $db->setQuery($query);
        $row = $db->loadObject();
        if(is_object($row) && $row->id) return true;
        else return false;
    }

    public static function getAvailableCountries() {
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('fieldcode').'='.$db->quote('FIELD_COUNTRY')
        );
        $query = $db->getQuery(true);
        $query->select('`options`')
            ->from($db->qn('#__community_fields'))
            ->where($cond);
        $db->setQuery($query);
        $countries = $db->loadObject()->options;
        $countries = explode("\n", $countries);
        return $countries;
    }

    public static function getDayNextMonth() {
        if(func_num_args() > 0) {
            $date = func_get_arg(0);
        } else {
            $date = new DateTime(); //today's datetime
        }
        $year = (int)$date->format('Y');
        $month = (int)$date->format('m');
        $day = (int)$date->format('d');
        //lets set up some rules
        if($month == 12) {
            $month = 1;
            $year += 1;
        } else {
            $month += 1;
        }
        if((int)$day > 28) {
            $day = 28;
        }
        return new DateTime($year.'-'.$month.'-'.$day);
    }

    public static function getDayLastMonth() {
        if(func_num_args() > 0) {
            $date = func_get_arg(0);
        } else {
            $date = new DateTime(); //today's datetime
        }
        $year = (int)$date->format('Y');
        $month = (int)$date->format('m');
        $day = (int)$date->format('d');
        //lets set up some rules
        if($month == 1) {
            $month = 12;
            $year -= 1;
        } else {
            $month -= 1;
        }

        if((int)$day > 28) {
            $day = 28;
        }
        return new DateTime($year.'-'.$month.'-'.$day);
    }

    //FAMILY MEMBER STUFF
	public static function getUserExtras() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		//now fetch the info
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('axs_gin_extras'))
			->where($db->qn('user_id').'='.(int)$userid);
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}

	public static function setUserExtras() {
		$params = func_get_arg(0);
		if(func_num_args() > 1) {
			$userid = func_get_arg(1);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$col = array (
			'user_id',
			'member_type',
			'affiliate_level',
			'member_status',
			'referrer_id',
			'primary_family_member_id',
			'primary_family_member_relationship',
			'company',
			'no_mail',
			'no_call',
			'affiliate_enabled',
			'gin_rewards_points',
			'gin_payment_settings',
			'card_id'
		);
		$val = array (
			(int)$userid,
			(int)$params->member_type,
			(int)$params->affiliate_level,
			(int)$params->member_status,
			(int)$params->referrer_id,
			(int)$params->primary_family_member_id,
			$db->q($params->primary_family_member_relationship),
			$db->q($params->company),
			(int)$params->no_mail,
			(int)$params->no_call,
			(int)$params->affiliate_enabled,
			(int)$params->gin_rewards_points,
			$db->q(serialize($params->gin_payment_settings)),
			(int)$params->card_id
		);
		$query = $db->getQuery(true);
		$query->insert($db->qn('axs_gin_extras'))
			->columns($db->qn($col))
			->values(implode(',', $val));
		$db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
		$db->execute();
	}

    /**
     * Get all the user_id's that are part of this guy's family
     * @return array of user_id's belonging to members of the user's family
     */
    public static function getFamilyMembers() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }

        $ids = array();
        $primary = self::getUserPrimaryFamilyMember($userid);
        if(!$primary) {
            //he doesn't have a primary but he could be the primary for other peoples
            $ids[] = $userid; //he's all alone, just add the actual user's id
            $familyArray = AxsExtra::getPrimarysFamilyMembers($userid);
            foreach($familyArray as $u) {
                $ids[] = $u->user_id; //add all his family members
            }
        } else {
            //if the homie has a family
            $ids[] = $primary; //add the primary family member's id
            $familyArray = AxsExtra::getPrimarysFamilyMembers($primary);
            foreach($familyArray as $u) {
                $ids[] = $u->user_id; //add all his family members' including himself
            }
        }
        return $ids;
    }

    /**
     * @param $primary_id integer, user_id of the primary family member
     * @return array of row objects with one property, user_id
     */
    public static function getPrimarysFamilyMembers($primary_id) {

        if (isset(static::$loaded[__METHOD__][$primary_id])) {
            return static::$loaded[__METHOD__][$primary_id];
        }

        //now fetch the info
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('primary_family_member_id').'='.(int)$primary_id
        );
        $query = $db->getQuery(true);
        $query->select('`user_id`')
            ->from($db->qn('axs_gin_extras'))
            ->where($cond);
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        
        static::$loaded[__METHOD__][$primary_id] = $rows;

        return $rows;
    }

    /**
     * @return integer, user_id of the user's primary family member, if 0 they have not primary
     */
    public static function getUserPrimaryFamilyMember() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }
        if (isset(static::$loaded[__METHOD__][$userid])) {
            return static::$loaded[__METHOD__][$userid];
        }
        //now fetch the info
        $db = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$userid
        );
        $query = $db->getQuery(true);
        $query->select('`primary_family_member_id`')
            ->from($db->qn('axs_gin_extras'))
            ->where($cond);
        $db->setQuery($query);
        $row = $db->loadObject();

        $result = isset($row->primary_family_member_id) ? $row->primary_family_member_id : 0;

		static::$loaded[__METHOD__][$userid] = $result;

        return $result;
    }

    public static function setUserPrimaryFamilyMember() {
        $primary_id = func_get_arg(0);
        if(func_num_args() > 1) {
            $userid = func_get_arg(1);
        } else {
            $userid = JFactory::getUser()->id;
        }
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->update('axs_gin_extras')
          ->set($db->qn('primary_family_member_id').'='.(int)$primary_id)
          ->where($db->qn('user_id').'='.(int)$userid);
        $db->setQuery($query);
        $db->execute();
    }

    public static function setUserPrimaryFamilyMemberRelationship() {
        $relationship = func_get_arg(0);
        if(func_num_args() > 1) {
            $userid = func_get_arg(1);
        } else {
            $userid = JFactory::getUser()->id;
        }
        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->update('axs_gin_extras')
          ->set($db->qn('primary_family_member_relationship').'='.$db->q($relationship))
          ->where($db->qn('user_id').'='.(int)$userid);
        $db->setQuery($query);
        $db->execute();
    }

    //REWARDS STUFF
	public static function getUserRewardsData() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		//now fetch the info
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('axs_rewards_data'))
			->where($db->qn('user_id').'='.(int)$userid);
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}

	public static function getUserRewardsPoints() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		//now fetch the info
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select($db->qn('points'))
			->from($db->qn('axs_rewards_data'))
			->where($db->qn('user_id').'='.(int)$userid);
		$db->setQuery($query);
		$row = $db->loadObject();
		return isset($row->points) ? $row->points : 0;
	}

	public static function setUserRewardsPoints() {
		$pointsToAdjust = func_get_arg(0);
		if(func_num_args() > 1) {
			$userid = func_get_arg(1);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->update($db->qn('axs_rewards_data'))
			->set($db->qn('points') . '=' . $db->qn('points') . ' + ' . (int)$pointsToAdjust)
			->where($db->qn('user_id').'='.(int)$userid);
		$db->setQuery($query);
		$db->execute();
	}

    /**
     * @return array first 20 rewards transactions
     */
    public static function getUserRewardsTransactions() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }

        if(func_num_args() > 1) {
            $page = func_get_arg(1);
        } else {
            $page = 0;
        }

        $offset = $page*20;

        //now fetch the info
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$userid
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_rewards_transactions'))
            ->where($cond);
        $query .= ' ORDER BY `date` DESC, `id` DESC'; //so we can get the latest transactions first
        $db->setQuery($query, $offset, 20);
        return $db->loadObjectList();
    }

    /**
     * $params will be an object with 4 properties
     * referee_id ... the user_id of the person giving points
     * amount ... the amount of points to contribute
     * reason ... the reason to add or remove points
     * status ... the status of transaction
     */
    public static function setUserRewardsTransaction() {
        $params = func_get_arg(0);
        if(func_num_args() > 1) {
            $userid = func_get_arg(1);
        } else {
            $userid = JFactory::getUser()->id;
        }

        if($params->amount == 0) return; //if we aren't giving or taking any points then we are good.

        $date = isset($params->date) ? $params->date : date("Y-m-d H:i:s");

        //now fetch the info
        $db  = JFactory::getDBO();
        $col = array (
            'user_id',
            'date',
            'referee_id',
            'amount',
            'reason',
            'status'
        );
        $val = array (
            (int)$userid,
            $db->q($date),
            (int)$params->referee_id,
            (int)$params->amount,
            $db->q($params->reason),
            $db->q($params->status)
        );
        $query = $db->getQuery(true);
        $query->insert($db->qn('axs_rewards_transactions'))
            ->columns($col)
            ->values(implode(',', $val));
        $db->setQuery($query);
        $db->execute();
        return $db->insertid();
    }

    //COMMISSIONS STUFF
    public static function getCommissionById($id) {
	    $db = JFactory::getDbo();
	    $db->setQuery("SELECT * FROM axs_gin_commissions WHERE id=$id;");
	    return $db->loadObject();
    }

    public static function getUserCommissions() {
        if(func_num_args() > 0) {
            $userid = func_get_arg(0);
        } else {
            $userid = JFactory::getUser()->id;
        }
        //now fetch the info
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$userid
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_gin_commissions'))
            ->where($cond);
        $query .= ' ORDER BY `date` DESC'; //so we can get the latest transactions first
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public static function setUserCommission($params) {

        if($params->referrer_id != $params->referee_id) {
            $db  = JFactory::getDBO();
            $col = array (
                'amount',
                'user_id',
                'date',
                'status',
                'reason',
                'referee_id'
            );
            $val = array (
                $db->q($params->amount),
                $db->q($params->referrer_id),
                $db->q(date("Y-m-d H:i:s")),
                $db->q($params->status ? $params->status : 'Pending'),
                $db->q($params->reason),
                $db->q($params->referee_id),
            );
            $query = $db->getQuery(true);
            $query->insert($db->qn('axs_gin_commissions'))
                ->columns($col)
                ->values(implode(',', $val));
            $db->setQuery(str_replace('INSERT INTO', 'INSERT IGNORE INTO', $query));
            $db->execute();
        }
    }

    public static function updateUserCommissionStatus($params) {
        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->update($db->qn('axs_gin_commissions'))
            ->set($db->qn('status').'='.$db->q($params->status))
            ->where($db->qn('id').'='.$db->q($params->id));
        $db->setQuery($query);
        $db->execute();
    }

    //AFFILIATE STUFF
	public static function getUserParent() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$conditions = array(
			$db->qn('user_id').'='.(int)$userid
		);
		$query = $db->getQuery(true);
		$query->select('referrer_id')
			->from($db->qn('axs_rewards_data'))
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObject()->referrer_id;
	}

	public static function setUserParent() {
		$parentid = func_get_arg(0);
		if(func_num_args() > 1) {
			$userid = func_get_arg(1);
		} else {
			$userid = JFactory::getUser()->id;
		}

		$fam = self::getFamilyMembers($userid);
		foreach($fam as $fid) {
			self::setAdminAction(19, $fid, $parentid, '');

			$db  = JFactory::getDBO();
			$conditions = array(
				$db->qn('user_id').'='.(int)$fid
			);
			$query = $db->getQuery(true);
			$query->update($db->qn('axs_rewards_data'))
				->set($db->qn('referrer_id').'='.(int)$parentid)
				->where($conditions);
			$db->setQuery($query);
			$db->execute();
		}
	}

	public static function getUserChildren() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$conditions = array(
			'`a`.`referrer_id`='.(int)$userid
		);
		$query = $db->getQuery(true);
		$query->select('`a`.*, `b`.`registerDate`, `b`.`name`')
			->from('`axs_rewards_data` a')
			->join('inner', $db->qn('#__users').' b ON `a`.`user_id`=`b`.`id`')
			->where($conditions);
		$query .= ' ORDER BY `b`.`registerDate` DESC';
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	/**
	 * @return mixed
	 * @deprecated
	 */
	public static function getUserActiveMemberChildren() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$item_id = 6; //GIN member plan
		$second_id = 7; //GIN Family plan

		$db  = JFactory::getDBO();
		$conditions = array(
			'axs_gin_extras.referrer_id='.(int)$userid,
			$db->quoteName('type') . '=' . (int)1,
			'('.$db->quoteName('item_id') . '=' . (int)$item_id.' OR '.$db->qn('item_id').'='.(int)$second_id.')',
			'('.$db->qn('status').'='.$db->q('STL').' OR '.$db->qn('status').'='.$db->q('SUC').' OR '.$db->qn('status').'='.$db->q('REF').')'
        );

        $conditions[] = $db->quoteName('next_payment_date') . '>(NOW()-INTERVAL '.self::$grace_period.' DAY)';

		$query = $db->getQuery(true);
		$query->select('axs_gin_extras.*')
			->from($db->qn('axs_gin_extras'))
			->innerJoin($db->qn('cctransactions').' ON axs_gin_extras.user_id=cctransactions.user_id')
			->where($conditions);
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public static function getAffiliateLevels() {
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')
			->from($db->qn('axs_gin_affiliate_levels'))
			->where('brand_id='.(int)self::$brand_id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public static function getUserAffiliateLevel() {
		if(func_num_args() > 0) {
			$userid = func_get_arg(0);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('`axs_rewards_data`.`level`, `axs_gin_affiliate_levels`.*')
			->from($db->qn('axs_rewards_data'))
			->innerJoin('axs_gin_affiliate_levels ON axs_rewards_data.level=axs_gin_affiliate_levels.level_id')
			->where('`axs_rewards_data`.`user_id`='.(int)$userid);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public static function setUserAffiliateLevel() {
		$affiliateLevel = func_get_arg(0);
		if(func_num_args() > 1) {
			$userid = func_get_arg(1);
		} else {
			$userid = JFactory::getUser()->id;
		}
		$db  = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->update($db->qn('axs_rewards_data'))
			->set($db->qn('level').'='.(int)$affiliateLevel)
			->where($db->qn('user_id').'='.(int)$userid);
		$db->setQuery($query);
		$db->execute();
	}

    public static function getUserAffiliateHistory() {
        if(func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$user_id
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_gin_affiliate_history'))
            ->where($cond);
        $query .= ' ORDER BY `date` DESC'; //so we can get the latest transactions first
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public static function setUserAffiliateHistory($afhistory) {
        return JFactory::getDbo()->insertObject('axs_gin_affiliate_history', $afhistory);
    }

    public static function getUserFromAffiliateCode($code) {
        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_gin_affiliate_codes'))
            ->where($db->qn('code').'='.$db->q($code));
        $db->setQuery($query);
        $ret = $db->loadObject();
        return is_object($ret) ? $ret->user_id : 0;
    }

    public static function getUserAffiliateCodes() {
        if(func_num_args() > 0) {
            $user_id = func_get_arg(0);
        } else {
            $user_id = JFactory::getUser()->id;
        }
        $db  = JFactory::getDBO();
        $cond = array (
            $db->qn('user_id').'='.(int)$user_id
        );
        $query = $db->getQuery(true);
        $query->select('*')
            ->from($db->qn('axs_gin_affiliate_codes'))
            ->where($cond);
        $query .= ' ORDER BY `date` DESC'; //so we can get the latest first
        $db->setQuery($query);
        return $db->loadObjectList();
    }

    public static function setUserAffiliateCode() {
        $code = func_get_arg(0);
        if(func_num_args() > 1) {
            $user_id = func_get_arg(1);
        } else {
            $user_id = JFactory::getUser()->id;
        }

        if(is_numeric($code)) {
            return $code.' is a number!';
        }

        try {
            $db = JFactory::getDBO();
            $col = array (
                'user_id',
                'code',
                'date'
            );
            $val = array (
                (int)$user_id,
                $db->q($code),
                $db->q(date("Y-m-d H:i:s"))
            );
            $query = $db->getQuery(true);
            $query->insert('axs_gin_affiliate_codes')
                ->columns($db->qn($col))
                ->values(implode(',', $val));
            $db->setQuery($query);
            return $db->execute();
        } catch(RuntimeException $e) {
            if($e->getCode() == 1062) {
                return 'Affiliate code already exists';
            } else {
                return 'An error occurred';
            }
        }
    }

    public static function deleteUserAffiliateCode($code_id) {
        $db  = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->delete('axs_gin_affiliate_codes')
            ->where($db->qn('id').'='.(int)$code_id);
        $db->setQuery($query);
        return $db->execute();
    }

    public static function getUserGroupId($string = null) {
        if ($string == null) {
            return null;
        }

        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__usergroups";
        $db->setQuery($query);
        $list = $db->loadObjectList();

        foreach($list as $item) {
            if ($item->title == $string) {
                return $item->id;
            }
        }

        return null;
    }
    
}
