<?php

defined('_JEXEC') or die;

/**
 * This library is mostly used in the frontend com_splms component
 * for rendering user transcripts
 *
 */
class AxsTranscripts {

    /**
     * Retrieves all the transcript data for the given user, then renders it
     *
     * @param string|int $user_id  The user_id we want to get the courses transcript for
     * @param array $options = [
     *  'show_full_course_data' => boolean - If set to false, don't get lessons or activities. Default is false.
     *  'disable_collapse => boolean -  If set to true, don't render collapse areas or buttons. Default is true
     * ]
     *
     * @return string
     */
    public static function renderCourses($user_id = -1, $options) {
        if (!isset($options['show_full_course_data'])) {
            $options['show_full_course_data'] = false;
        }
        if (!isset($options['collapse'])) {
            $options['collapse'] = true;
        }

        // Get all the courses this user has course_progress in
        $courses = self::getTranscriptCourses($user_id);
        $course_ids = array_map(function($c) { return $c->course_id; }, $courses);
        $course_ids = implode(",", $course_ids);

        if ($options['show_full_course_data'] == false) {
            ob_start();
            require 'components/com_splms/templates/coursesTranscriptTable.php';
            return ob_get_clean();
        }

        // Get all the lessons in the courses retrieved above
        $lessons = self::getAllLessonsFromCourses($course_ids);
        $lesson_ids = array_map(function($l) { return $l->splms_lesson_id; }, $lessons);
        $lesson_ids = implode(",", $lesson_ids);

        // Get separate hashmaps of all lessons that the user has started and completed
        $lesson_statuses_started = self::getTranscriptLessonStatuses($lesson_ids, $user_id, 'started');
        $lesson_statuses_completed = self::getTranscriptLessonStatuses($lesson_ids, $user_id, 'completed');

        // Get all the activities in those lessons for the user with the given id
        $activities = self::getTranscriptUserActivities($lesson_ids, $user_id);

        // Get all the quiz results in the courses for this transcript
        $quizresults = self::getTranscriptQuizResults($course_ids, $user_id);

        $collapse = $options['collapse'] ? 'collapse' : '';

        ob_start();
        require 'components/com_splms/templates/coursesTranscriptTable.php';
        return ob_get_clean();
    }

    /**
     * Retrieves all the data neccesary to be sent to the events transcript template
     *
     * @param string|int $user_id  The user_id we want to get the events transcript for
     *
     * @return string
     */
    public static function renderEvents($user_id = -1) {
        $events = self::getTranscriptEvents($user_id);
        $brand = AxsBrands::getBrand();
        $currency_code = 'USD';
        $currency_symbol = '$';
        if ($brand->billing->currency_code) {
            $currency_code = $brand->billing->currency_code;
        }
        if ($brand->billing->currency_symbol) {
            $currency_symbol = $brand->billing->currency_symbol;
        }
        ob_start();
        require 'components/com_splms/templates/eventsTranscriptTable.php';
        return ob_get_clean();
    }

    /**
     * Retrieves all the courses that the user with user_id has joom_splms_course_progress for
     *
     * @param string|int $user_id  The user_id we want to get the events transcript for
     *
     * @return array
     */
    public static function getTranscriptCourses($user_id = -1) {
        $db = JFactory::getDbo();
        $query = trim("
            SELECT progress.*,course.*
            FROM `#__splms_course_progress` as progress
            LEFT JOIN `#__splms_courses` as course
            ON progress.course_id = course.splms_course_id
            WHERE progress.user_id = $user_id AND progress.archive_date IS NULL
            GROUP BY progress.user_id, progress.course_id;
        ");
        $db->setQuery($query);
        $courses = $db->loadObjectList();

        foreach ($courses as &$course) {
            $courseProgress = AxsLMS::getCourseProgress((int)$course->course_id,$user_id);
            $course->percentComplete = round($courseProgress->progress);
            if ($course->percentComplete == 100) {
                $course->status = 'Completed';
            } else {
                $course->status = 'In Progress';
            }
            $course->score  = AxsLMS::getCourseTotalScore((int)$course->course_id,$user_id);
            $course->params = json_decode($course->params);
        }

        // jdump($query->__toString(), 'getTranscriptEvents query tostring');
        // jdump($courses, 'getTranscriptCourses courses');
        return $courses;
    }

    /**
     * Retrieves all the events the user with user_id has registered for
     *
     * @param string|int $user_id  The user_id to get registered events for
     *
     * @return array
     */
    public static function getTranscriptEvents($user_id = -1) {
        $db = JFactory::getDbo();
        $query = trim("
            SELECT registration.*,event.*
            FROM `#__eb_registrants` AS registration
            LEFT JOIN `#__eb_events` AS event ON registration.event_id = event.id
            WHERE registration.user_id = $user_id AND registration.published = 1;
        ");
        $db->setQuery($query);
        $eventList = $db->loadObjectList();

        // jdump($query->__toString(), 'getTranscriptEvents query tostring');
        // jdump($results, 'getTranscriptEvents results');
        return $eventList;
    }

    /**
     * Retrieves all the lessons belonging to the courses whose id's
     * appear in course_ids
     *
     * @param string $course_ids  Comma separated list of course ids
     *
     * @return array
     */
    public static function getAllLessonsFromCourses($course_ids) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lessons');
        $query->where("FIND_IN_SET(splms_course_id, '$course_ids')");
        $query->order('splms_course_id, ordering');

        $db->setQuery($query);
        $results = $db->loadObjectList();
        // jdump($query->__toString(), 'get lessons query tostring');
        // jdump($results, 'getAllLessonsFromCourses results');
        return $results;
    }

    /**
     * Returns an object containing the color for the course and the lessons
     *
     * @param string $str  The str
     *
     * @return object
     */
    public static function getColorFromStr($str) {
        $available_colors = array(
			"#fce38a",	"#eaffd0",
			"#95e1d3",	"#ffcb91",
			"#ffefa1",	"#94ebcd",
			"#6ddccf",	"#b8b5ff",
			"#7868e6",	"#440a67",
			"#93329e",	"#b4aee8",
			"#78c4d4",	"#b7657b",
			"#f25287",	"#025955",
			"#00917c",	"#8c0000",
			"#bd2000",	"#fa1e0e",
			"#e45826",	"#28527a",
			"#99bbad",	"#94ebcd",
			"#6ddccf"
		);
		$hash = substr(md5($str), 0, 5);

        return $available_colors[$hash % (count($available_colors) - 1)];
    }

    /**
     * Returns all the quiz results for courses with the given $course_ids by the user
     * with the given $user_id
     *
     * @param string $course_ids  Comma-separated list of course ids
     * @param string|int $user_id  Id of the user this transcript is for
     *
     * @return array
     */
    public static function getTranscriptQuizResults($course_ids, $user_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->setQuery(trim("
            SELECT *
            FROM joom_splms_quizquestions AS quiz
            LEFT JOIN joom_splms_quizresults AS result ON result.splms_quizquestion_id = quiz.splms_quizquestion_id
            WHERE archive_date IS NULL AND FIND_IN_SET(result.splms_course_id, '$course_ids') AND result.user_id = $user_id
        "));
        $db->setQuery($query);
        $results = $db->loadObjectList();
        // jdump($results, 'getTranscriptQuizResults results');
        return $results;
    }

    /**
     * Returns a hashmap of the lesson statuses for courses with the given $lesson by the user
     * with the given $user_id with the given $status
     *
     * Hashmap is lesson_key->{lesson status row object}
     *
     * @param string $course_ids  Comma-separated list of course ids
     * @param string|int $user_id  Id of the user this transcript is for
     * @param string $status  The status to get, either 'started' or 'completed'
     *
     * @return array
     */
    public static function getTranscriptLessonStatuses($lesson_ids, $user_id, $status) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_lesson_status');
        $query->where(array(
            "FIND_IN_SET(lesson_id, '$lesson_ids')",
            "user_id = $user_id",
            "status = '$status'",
            "archive_date IS NULL"
        ));
        $db->setQuery($query);
        $results = $db->loadAssocList('lesson_id');
        // jdump($query->__toString(), 'getTranscriptLessonStatuses query tostring');
        // jdump($results, 'getTranscriptLessonStatuses results');
        return $results;
    }

    /**
     * Returns all the user activities for lessons with the given $lesson_ids by the user
     * with the given $user_id
     *
     * @param string $course_ids  Comma-separated list of course ids
     * @param string|int $user_id  Id of the user this transcript is for
     *
     * @return array
     */
    public static function getTranscriptUserActivities($lesson_ids, $user_id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__splms_student_activities');
        $query->where(array(
            "FIND_IN_SET(lesson_id, '$lesson_ids')",
            "user_id = $user_id",
            "archive_date IS NULL"
        ));
        $db->setQuery($query);
        $results = $db->loadObjectList();
        // jdump($query->__toString(), 'getTranscriptUserActivities query tostring');
        // jdump($results, 'getTranscriptUserActivities results');
        return $results;
    }

    /**
     * Returns true if the activity type is blacklisted
     *
     * We don't want to include any activity types which don't require student interaction
     *
     * Types are defined in administrator/components/com_splms/forms/student_activities.xml
     *
     * @param string $type
     *
     * @return array
     */
    public static function isBlacklistedActivityType($type) {
        $whitelist = array(
            'interactive',
            'survey',
            'textbox',
            'upload',
            'mp4',
            'mp3',
            'youtube',
            'vimeo',
            'facebook',
            'screencast',
            'link',
            'quiz'
        );
        $blacklist = array(
            'powerpoint',
            'pdf',
            'video',
            'audio',
            'embed',
            'powerpoint',
        );
        return in_array($type, $blacklist);
    }

    /**
     * Gets the icon class for the given activity type
     *
     * See AxsTranscripts::getStudentActivityTypesBlacklist() or administrator/components/com_splms/forms/student_activities.xml
     * for a list of all available types
     *
     * @param string $activity_type  The activity type we want to get the icon class for
     *
     * @return string
     */
    public static function getActivityIcon($activity_type) {
        $icon_table = array(
            'interactive' => 'fas fa-arrows',
            'survey' => 'fas fa-poll',
            'textbox' => 'fas fa-text',
            'upload' => 'fas fa-file-upload',
            'mp4' => 'fas fa-video',
            'mp3' => 'fas fa-file-audio',
            'youtube' => 'fab fa-youtube',
            'vimeo' => 'fab fa-vimeo-v',
            'facebook' => 'fab fa-facebook',
            'screencast' => 'fas fa-desktop',
            'link' => 'fas fa-code',
            'quiz' => 'fas fa-feather-alt'
        );
        $icon_class = $icon_table[$activity_type];
        // jdump($icon_class, "icon_class");
        return $icon_class;
    }

    /**
     * Each lesson has one or more student_activities.
     *
     * Some activities need to be removed from the transcript because they don't have any useful
     * information for the transcript, they are just for display for the user.
     *
     * Also, quizzes aren't grouped with all the other activities, so we need to
     * get any quizresults for the current lesson and append those as well.
     *
     * @param object $lesson_activities  The student activities JSON data from the lesson
     * @param array $quizresults  The quizresults associated with this transcript
     * @param string $lesson_id  The id of the lesson to get the quizresults for
     *
     * @return object
     */
    public static function processLessonActivities($lesson_activities, $quizresults, $activities, $lesson_id) {

        $lesson_activities_processed = array();

        foreach ($lesson_activities as $key => $lesson_activity) {
            // If this is a blacklisted item, remove it
            $blacklisted_interactive_content_type = false;
            if ($lesson_activity->type == 'interactive') {
                $interactive_content_type = AxsLMS::getInteractiveLibrary($lesson_activity->interactive_id);
                if (AxsLMS::canBeRequired($interactive_content_type) == false) {
                    $blacklisted_interactive_content_type = true;
                }
            }
            if (self::isBlacklistedActivityType($lesson_activity->type) || $blacklisted_interactive_content_type) {
                unset($lesson_activities->$key);
            } else {
                // Else, look for a user activity that matches this lesson activity
                $activity_data = new stdClass();
                foreach($activities as $activity) {
                    if ($activity->activity_id == $lesson_activity->id) {
                        $activity_data = $activity;
                        break;
                    }
                }

                $lesson_activity_data = array(
                    'type' => $lesson_activity->type,
                    'completed' => 'Not Completed',
                );

                // If we find user activity, populate table data with its information
                if (!empty($activity_data)) {
                    $lesson_activity_data['grade'] = isset($activity_data->grade) ? $activity_data->grade : "";
                    $lesson_activity_data['date'] = isset($activity_data->date_submitted) ? AxsLMS::formatDate($activity_data->date_submitted) : "";
                    $lesson_activity_data['completed'] = $activity_data->completed == "1" ? "Completed" : "Not Completed";
                    $lesson_activity_data['title'] = $lesson_activity->title;
                }
                $lesson_activities_processed []= (object)$lesson_activity_data;
            }
        }

        // Grab any quiz results for this lesson and append them to lesson_activities
        $lesson_quiz_results = array_filter($quizresults, function($r) use ($lesson_id) { return $r->splms_lesson_id == $lesson_id; });

        // Make sure that the grades are recorded as numbers to match the lesson_activity grades for universal data
        // compatibility for this data item.
        foreach ($lesson_quiz_results as $quiz_result) {
            $grade = 0;
            $date = "";
            $completed = "Not Completed";

            // The quiz has been completed, populate data
            if (isset($quiz_result->point)) {

                // Fractional grade parts are not needed for percentages
                $grade = round((floatval($quiz_result->point) / floatval($quiz_result->total_marks)) * 100.0);

                // Restrict the grade to 100% at maximum
                if($grade > 100) {

                    $grade = 100;
                }

                $date = $quiz_result->date;
                $completed = 'Completed';
            }

            $quiz_result_activity_activity_data = array(
                'type' => 'quiz',
                'grade' => $grade,
                'date' => $date,
                'completed' => $completed,
                'title' => $quiz_result->title
            );
            $lesson_activities_processed []= (object)$quiz_result_activity_activity_data;
        }

        // jdump($lesson_activities_processed, 'lesson_activities_processed');
        return $lesson_activities_processed;
    }

    /**
     * Gets the style rules for being printerfriendly
     *
     * @return string
     */
    public static function getPrinterFriendlyStyleRules() {
        ob_start();
        ?>
            <style>
                /* simplify borders from boxes */
                .box {
                    border-bottom: #ccc solid 1px;
                }
                /* show collapsed contents */
                .collapse {
                    display: block !important;
                    height: auto !important;
                }
                /* hide interactive buttons */
                .share-transcript,
                #transcriptButtons,
                .transcript-button,
                .tov-trans-course-expand-btn,
                .tov-trans-course-img,
                #toggleCollapseAllButton,
                #header-wrap,
                /* hide icons */
                .svg-inline--fa,
                [class*="lizicon"]
                {
                    display: none !important;
                }
                /* remove padding and background color from section headers */
                .tov-trans-card-header {
                    padding-right: 0px !important;
                    padding-left: 0px  !important;
                    padding-top: 0px  !important;
                    padding-bottom: 0px  !important;
                    font-size: medium !important;
                    color: black  !important;
                    background-color: white  !important;
                    margin: 0px !important;
                }
                /* add indentation */
                .tov-trans-lessons-wrapper {
                    margin-left: 15px;
                }
                .tov-trans-lessons-indicator {
                    margin-left: 15px;
                }
                .tov-trans-card-course {
                    border-radius: 0px !important;
                    padding: 4px !important;
                    border: none !important;
                    margin-bottom: 30px !important;
                    border-top: 1px solid black !important;
                }
                /* don't show link hrefs */
                a[href]:after {
                    content: unset !important;
                }
                /* don't bold everything on the page, uses less ink */
                .tov-trans-course-col-details p.tov-trans-detail-data,
                .tov-trans-detail-type {
                    font-weight: unset;
                }
                /* remove background color and set text color to black */
                [class*="tov-trans-badge"],
                .cover-overlay,
                .transcript-name,
                p {
                    color: black !important;
                    background-color: white !important;
                }
                /* reduce font-weight of badges */
                .tov-trans-course-badge {
                    display: inline-block;
                    min-width: 10px;
                    font-size: 12px;
                    font-weight: 400;
                    line-height: 1;
                    text-align: center;
                    white-space: nowrap;
                    vertical-align: middle;
                    border-radius: 4px !important;
                }
                .tov-trans-course-col-details p.tov-trans-detail-type {
                    font-weight: unset;
                    margin-bottom: 4px;
                }
                /* remove banner image from top of transcript */
                .cover-image {
                    background-repeat: no-repeat;
                    background-position: center;
                    background-size: cover;
                    overflow: hidden;
                    position: relative;
                    width: 100%;
                    height: 100px;
                    background-image: unset !important;
                }
                /* if an element is display:flex, it will cut off in the middle of words and
                   ruin the print view so don't remove this! */
                .tov-trans-course-column,
                .tov-trans-course-columns {
                    display: inline-block !important;
                    float: none !important;
                    margin-right: 30px;
                }
                /* ensure the page doesn't get cut off at the bottom in print view */
                body {
                    overflow-y: visible !important;
                    font-size:0.9rem;
                }
                /* make field headers a bit smaller */
                .tov-trans-course-col-details p.tov-trans-detail-header,
                .tov-trans-detail-type  {
                    color: var(--bs-secondary) !important;
                    margin-bottom: 4px !important;
                    font-size: small;
                }
                .tov-trans-course-col-details {
                    padding: 0px;
                }
                .box {
                    box-shadow: unset !important;
                }
            </style>
        <?php
        $style_rules = ob_get_clean();
        // A little hack here to make things a bit nicer
        // We don't want to return a string with the style tags, but we do like
        // writing the style rules within style tags for syntax highlighting :^)
        $style_rules = str_replace("<style>", "", $style_rules);
        $style_rules = str_replace("</style>", "", $style_rules);
        // jdump($style_rules, "style_rules");
        return $style_rules;
    }

    /**
     * Gets the translation for the given status
     * 
     * We need to do it this way because course progress statuses 
     * are stored in the db
     * 
     * @param string $status
     * 
     * @return string
     */
    function getStatusTranslation($status) {
        switch(strtolower($status)) {
            case 'completed':
                return AxsLanguage::text("AXS_COMPLETED", "Completed");
            case 'not completed':
                return AxsLanguage::text("AXS_NOT_COMPLETED", "Not Completed");
            case 'in progress':
                return AxsLanguage::text("AXS_IN_PROGRESS", "In Progress");
            case 'not started':
                return AxsLanguage::text("AXS_NOT_STARTED", "Not Started");
        }
    }
}
