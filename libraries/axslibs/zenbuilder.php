<?php

defined('_JEXEC') or die;

class AxsZenbuilder {

    public static function getZenData($brandID) {
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_zenbuilder');
		if($brandID) {
			$query->where('brand = '. $db->quote($brandID));
		}
		$query->limit(1);
		$db->setQuery($query);
		$data = $db->loadObject();
		if($data) {
			return json_decode($data->params);
		} else {
			return false;
		}
	}

	public static function getObjectRow($table,$id) {
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($table);
		$query->where('id = '. $db->quote($id));
		$query->limit(1);
		$db->setQuery($query);
		$data = $db->loadObject();
		if($data) {
			return $data;
		} else {
			return false;
		}
	}

	public static function saveZenData($data) {
		$db = JFactory::getDBO();
		$brandRow = self::getObjectRow('axs_brands',$data['brand']);
		$brandData = json_decode($brandRow->data);
		$homepageRow = self::getObjectRow('axs_homepage_templates',(int)$brandData->homepage->template);
		$homepageData = json_decode($homepageRow->data);
		$dashboardRow = self::getObjectRow('axs_dashboard_templates',(int)$brandData->dashboard->template);
		$dashboardData = json_decode($dashboardRow->data);

		$brandData->site_title = $data['site_name'];
		$brandData->logos->favicon = $data['favicon'];
		$brandRow->favicon = $data['favicon'];

		// These settings need to be passed to their respective db rows now that they are moved.
		AxsBrands::updateEmailSettings($brandData->email_settings_id, 'sitename', $data['site_name']);
		AxsBrands::updatePaymentGatewayDefaultAdminEmail($brandData->payment_gateway_id, $data['contact_email']);

		$brandData->social->pinterest_link = $data['pinterest_link'];
		$brandData->social->instagram_link = $data['instagram_link'];
		$brandData->social->facebook_link = $data['facebook_link'];
		$brandData->social->twitter_link = $data['twitter_link'];
		$brandData->social->google_plus_link = $data['google_plus_link'];
		$brandData->social->youtube_link = $data['youtube_link'];
		$brandData->social->linkedin_link = $data['linkedin_link'];
		$brandData->site_details->tovuti_root_styles = $data['tovuti_root_styles'];
		$brandData->site_details->root_primary_color = $data['root_primary_color'];
		$brandData->site_details->root_accent_color = $data['root_accent_color'];
		$brandData->site_details->root_base_color = $data['root_base_color'];

		if(!$data['show_socialmedia']) {
			$show_socialmedia = false;
		} else {
			$show_socialmedia = true;
		}

		$slides = json_decode($homepageData->header->slideshow);

		$homepageData->header->show_socialmedia = $show_socialmedia;
		$homepageData->logo = $data['logo'];

		$slides->slideshow0->file = $data['homepage_image'];
		$slides->slideshow0->text = $data['homepage_image_text'];
		$homepageData->header->slideshow = json_encode($slides);
		$homepageData->header->socialmedia_color = $data['logged_out_social_media_bar_color'];
		$homepageData->header->socialmedia_text_color = $data['logged_out_social_media_bar_text_color'];
		$homepageData->header->socialmedia_hover_color = $data['logged_out_social_media_bar_text_hover_color'];
		$homepageData->footer->background_color = $data['logged_out_footer_background_color'];
		$homepageData->footer->text_color = $data['logged_out_footer_text_color'];
		$homepageData->colors->background = $data['logged_out_header_background_color'];
		$homepageData->colors->header_text = $data['logged_out_header_text_color'];
		$homepageData->colors->subpages_background = $data['logged_out_header_background_color'];
		$homepageData->colors->subpages_text = $data['logged_out_header_text_color'];

		$dashboardData->logo->main = $data['logo'];
		$dashboardData->modules->header_background = $data['dashboard_image'];
		$dashboardData->modules->header_background_text = $data['dashboard_image_text'];

		$dashboardData->navigation_bar->color = $data['logged_in_navigation_background_color'];
		$dashboardData->navigation_bar->icon_color = $data['logged_in_navigation_text_color'];
		$dashboardData->navigation_bar->text_color = $data['logged_in_navigation_text_color'];
		$dashboardData->navigation_bar->sub_text_color = $data['logged_in_navigation_text_color'];
		$dashboardData->navigation_bar->hover_color = $data['logged_in_navigation_text_hover_color'];
		$dashboardData->user_bar->color = $data['logged_in_header_background_color'];
		$dashboardData->user_bar->text_color = $data['logged_in_header_text_color'];
		$dashboardData->logo->background_color = $data['logged_in_header_background_color'];
		$dashboardData->logo->logged_in_header_text_color = $data['logged_in_header_text_color'];
		$dashboardData->modules->title_bar_color = $data['logged_in_module_title_background_color'];
		$dashboardData->modules->title_bar_text_color = $data['logged_in_module_title_text_color'];

		$homepageRow->data = json_encode($homepageData);
		$dashboardRow->data = json_encode($dashboardData);
		$brandRow->data = json_encode($brandData);

		$homeSaveResult 	 = $db->updateObject('axs_homepage_templates',$homepageRow,'id');
		$dashboardSaveResult = $db->updateObject('axs_dashboard_templates',$dashboardRow,'id');
		$brandSaveResult 	 = $db->updateObject('axs_brands',$brandRow,'id');



		$menuItems = ['registration', 'community', 'profile', 'inbox', 'groups', 'login', 'contact', 'courses', 'media', 'supscriptions', 'blog', 'events', 'forum'];

		$menuHomepage  = $homepageData->header->menu;
		$menuDashboard = $dashboardData->navigation_bar->icons;

		$menus = new stdClass();
		$menus->home = $menuHomepage;
		$menus->dashboard = $menuDashboard;

		foreach($menuItems as $item) {
			self::updateMenuItem($menus,$item,$data[$item]);
		}

		if($homeSaveResult && $dashboardSaveResult && $brandSaveResult) {
			return true;
		} else {
			return false;
		}


	}

	public static function getMenuItemParams($item) {
		$params = new stdClass();
		$random = rand(0,1000);
		switch ($item) {
			case 'registration':
				$params->link = 'index.php?option=com_converge&view=subscription&plan_id=1&brand_req=1';
				$params->check = 'public';
				$params->title = 'Register';
				$params->component_id = 10338;
				$params->alias = 'register-page-'.$random;
			break;

			case 'community':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-users";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_community&view=frontpage';
				$params->check = 'registered';
				$params->title = 'Community';
				$params->component_id = 10021;
				$params->params = json_encode($icon);
				$params->alias = 'community-'.$random;
			break;

			case 'profile':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-account_circle";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_community&view=profile';
				$params->check = 'registered';
				$params->title = 'My Profile';
				$params->component_id = 10021;
				$params->params = json_encode($icon);
				$params->alias = 'profile-'.$random;
			break;

			case 'inbox':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-email";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_community&view=inbox';
				$params->check = 'registered';
				$params->title = 'My Inbox';
				$params->component_id = 10021;
				$params->params = json_encode($icon);
				$params->alias = 'my-inbox-'.$random;
			break;

			case 'groups':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-bubbles4";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_community&view=groups';
				$params->check = 'registered';
				$params->title = 'Groups';
				$params->component_id = 10021;
				$params->params = json_encode($icon);
				$params->alias = 'all-groups-'.$random;
			break;

			case 'login':
				$params->link   = 'index.php?option=com_axs&view=popupmodule&module_id=287';
				$params->link2  = 'index.php?option=com_users&view=login';
				$params->check  = 'public';
			break;

			case 'login_popup':
				$params->link   = 'index.php?option=com_axs&view=popupmodule&module_id=287';
				$params->check  = 'public';
				$params->title = 'Login';
				$params->component_id = 10286;
				$params->alias = 'login-'.$random;
			break;

			case 'login_page':
				$params->link  = 'index.php?option=com_users&view=login';
				$params->check  = 'public';
				$params->title = 'Login';
				$params->component_id = 10286;
				$params->alias = 'login-'.$random;
			break;

			case 'contact':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-email1";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_contactpage&view=contactpages&id=0';
				$params->check = 'both';
				$params->title = 'Contact';
				$params->component_id = 10531;
				$params->params = json_encode($icon);
				$params->alias = 'contact-'.$random;
			break;

			case 'courses':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-books";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = 'index.php?option=com_splms&view=courses';
				$params->check = 'both';
				$params->title = 'Courses';
				$params->component_id = 10181;
				$params->params = json_encode($icon);
				$params->alias = 'courses-'.$random;
			break;

			case 'media':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-film";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = '%index.php?option=com_axs&view=videos%';
				$params->linkAlt = 'index.php?option=com_axs&view=videos&category=0&subcategory=0';
				$params->check = 'both';
				$params->title = 'Media Library';
				$params->component_id = 10286;
				$params->params = json_encode($icon);
				$params->alias = 'media-'.$random;
			break;

			case 'supscriptions':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-equalizer";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = '%index.php?option=com_converge&view=plans%';
				$params->linkAlt = 'index.php?option=com_converge&view=plans&cat_id[0]=1';
				$params->check = 'both';
				$params->title = 'Subscriptions';
				$params->component_id = 10338;
				$params->params = json_encode($icon);
				$params->alias = 'subscriptions-'.$random;
			break;

			case 'blog':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-bullhorn";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = '%index.php?option=com_easyblog%';
				$params->linkAlt = 'index.php?option=com_easyblog&view=latest';
				$params->check = 'both';
				$params->title = 'Blog';
				$params->component_id = 10124;
				$params->params = json_encode($icon);
				$params->alias = 'blog-'.$random;
			break;

			case 'events':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-calendar";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = '%index.php?option=com_eventbooking&view=eventlist%';
				$params->linkAlt = 'index.php?option=com_eventbooking&view=eventlist&id=0';
				$params->check = 'both';
				$params->title = 'Events';
				$params->component_id = 10581;
				$params->params = json_encode($icon);
				$params->alias = 'events-'.$random;
			break;

			case 'forum':
				$icon = new stdClass();
				$icon->{'sidemenu-icon'} = "lizicon-comment1";
    			$icon->{'sidemenu-icon-position'} = "top";
				$params->link = '%index.php?option=com_kunena%';
				$params->linkAlt = 'index.php?option=com_kunena&view=home&defaultmenu=839';
				$params->check = 'both';
				$params->title = 'Forum';
				$params->component_id = 10711;
				$params->params = json_encode($icon);
				$params->alias = 'forum-'.$random;
			break;
		}
		return $params;
	}

	private static function saveMenuItem($menutype,$params,$publish,$icon = false) {
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__menu');
		$query->where('menutype = '. $db->quote($menutype));
		$query->where('link LIKE '. $db->quote($params->link));
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if($result) {
			foreach($result as $item) {
				$item->published = $publish;
				$db->updateObject('#__menu',$item,'id');
			}
		} elseif($publish) {
			if($params->linkAlt) {
				$link = $params->linkAlt;
			} else {
				$link = $params->link;
			}
			if($params->params && $icon) {
				$iconParams = $params->params;
			} else {
				$iconParams = '';
			}

			$menuItem = array(
				'menutype' => $menutype,
				'title' => $params->title,
				'alias' => $params->alias,
				'type' => 'component',
				'component_id' => $params->component_id,
				'link' => $link,
				'language' => '*',
				'published' => 1,
				'parent_id' => 1,
				'level' => 1,
				'params' => $iconParams
			);

			$menuTable = JTable::getInstance('Menu', 'JTable', array());

			$menuTable->setLocation(1, 'last-child');

			if (!$menuTable->save($menuItem)) {
				throw new Exception($menuTable->getError());
				return false;
			}
		}
	}

	public static function storeMenuItem($menu,$item,$publish,$icon) {
		$params = self::getMenuItemParams($item);
		self::saveMenuItem($menu,$params,$publish,$icon);
	}

	public static function updateMenuItem($menus,$item,$value) {

		if($item == 'login') {
			if($value == 'popup') {
				self::storeMenuItem($menus->home,'login_popup',1,false);
				self::storeMenuItem($menus->home,'login_page',0,false);
			}
			if($value == 'page') {
				self::storeMenuItem($menus->home,'login_popup',0,false);
				self::storeMenuItem($menus->home,'login_page',1,false);
			}
			if(!$value) {
				self::storeMenuItem($menus->home,'login_popup',0,false);
				self::storeMenuItem($menus->home,'login_page',0,false);
			}
		} else {
			if($value == 'public') {
				self::storeMenuItem($menus->home,$item,1,false);
				self::storeMenuItem($menus->dashboard,$item,0,true);
			}

			if($value == 'registered') {
				self::storeMenuItem($menus->home,$item,0,false);
				self::storeMenuItem($menus->dashboard,$item,1,true);
			}

			if($value == 'both') {
				self::storeMenuItem($menus->home,$item,1,false);
				self::storeMenuItem($menus->dashboard,$item,1,true);
			}

			if(!$value) {
				self::storeMenuItem($menus->home,$item,0,false);
				self::storeMenuItem($menus->dashboard,$item,0,true);
			}
		}
	}

	public static function checkMenuItem($menus,$item) {

		$params = self::getMenuItemParams($item);
		$db    = JFactory::getDBO();
		$status = 0;

		if($params->check == 'public' || $params->check == 'both') {
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__menu');
			$query->where('menutype = '. $db->quote($menus->home));
			$query->where('link LIKE '. $db->quote($params->link));
			$query->limit(1);
			$db->setQuery($query);
			$resultHome = $db->loadObject();
			if($resultHome->published == '1') {
				$status = 'public';
			}

			if($item == 'login') {
				$query = $db->getQuery(true);
				$query->select('*');
				$query->from('#__menu');
				$query->where('menutype = '. $db->quote($menus->home));
				$query->where('link LIKE '. $db->quote($params->link2));
				$query->limit(1);
				$db->setQuery($query);
				$resultHomeLogin = $db->loadObject();
				if($resultHome->published == '1') {
					$status = 'popup';
				}
				if($resultHomeLogin->published == '1') {
					$status = 'page';
				}
			}
		}

		if($params->check == 'registered' || $params->check == 'both') {
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__menu');
			$query->where('menutype = '. $db->quote($menus->dashboard));
			$query->where('link LIKE '. $db->quote($params->link));
			$query->limit(1);
			$db->setQuery($query);
			$resultDashboard = $db->loadObject();
			if($resultDashboard->published == '1') {
				$status = 'registered';
			}
		}

		if((!empty($resultDashboard) && $resultDashboard->published == '1') && (!empty($resultHome) && $resultHome->published == '1')){
			$status = 'both';
		}

		return $status;
	}
}
