<?php

defined('_JEXEC') or die;

class AxsTeamLeadDashboard {

    private $team;
    private $leader;
    private $dashboard;
    private $dashboardParams;
    private $userList;

    public function __construct($leader, $team, $dashboard) {
        if($team) {
            $this->team = $team;
            switch($this->team->member_type) {
                case "individuals":
                    if($this->team->user_ids) {
                        $list = $this->team->user_ids;
                    }
                break;
                case "groups":
                    if($this->team->user_groups) {
                        $list =  $this->getUsersFromGroupList($this->team->user_groups);
                    }
                break;
            }
            //add team leaders
            /* $leads     = explode(',',$team->team_lead);
            $members   = explode(',',$list);
            $listArray = array_unique(array_merge($leads,$members));
            $list = implode(',',$listArray); */
            if($list) {
                $this->userList = $this->convertListToIntegers($list);
            } else {
                $this->userList = null;
            }
        }
        if($leader) {
            $this->leader = $leader;
        }
        if($dashboard) {
            $this->dashboard = $dashboard;
            $this->dashboardParams = json_decode($dashboard->params);
        }
    }

    public function getTeam() {
        return $this->team;
    }

    public function getTeamParams() {
        return json_decode($this->team->params);
    }

    public function getLeader() {
        return $this->leader;
    }

    public function getDashboard() {
        return $this->dashboard;
    }

    public function getDashboardParams() {
        return $this->dashboardParams;
    }

    public function getUserList() {
        return $this->userList;
    }

    public function setTeam($team) {
        $this->team = $team;
    }

    public function setLeader($leader) {
        $this->leader = $leader;
    }

    public function setDashboard($dashboard) {
        $this->dashboard = $dashboard;
    }

    public function setDashboardParams($dashboardParams) {
        $this->dashboardParams = $dashboardParams;
    }

    public function setUserList($userList) {
        $this->userList = $userList;
    }


	public function buildTeamMembersHTML() {
        $key = AxsKeys::getKey('lms');
        $teamMembers = $this->getTeamMembers();
        $teamParams  = $this->getTeamParams();
		include 'components/com_axs/templates/team_members.php';
    }

    public function buildTeamDropdownHTML($teams,$defaultTeamId = null) {
        $key = AxsKeys::getKey('lms');
        $urlBase = $this->getURLBase();
        $teamParams  = $this->getTeamParams();
		include 'components/com_axs/templates/team_dropdown.php';
    }

    public function buildAttendanceHTML($reportParams) {
		include 'components/com_axs/templates/report_attendance.php';
    }

    public function buildUserUpdateHTML($userManagerParams) {
        $userId = JFactory::getUser()->id;
        $team = AxsTeamLeadDashboard::getSelectedTeam($userId,$userManagerParams->team_id);
        $teamParams = json_decode($team->params);
        $userManagerParams->team_params = $teamParams;
        $userManagerParams->user_id = $userId;
        $key = AxsKeys::getKey('lms');
        $encryptedParams = base64_encode(AxsEncryption::encrypt($userManagerParams, $key));
		include 'components/com_axs/templates/user_register.php';
    }

    public function buildAssignmentManagerHTML() {
        $userId = JFactory::getUser()->id;
        $teamParams = json_decode($this->team->params);
        $key = AxsKeys::getKey('lms');
		include 'components/com_axs/templates/assignments.php';
    }

    public function buildActivityHTML($reportParams) {
		include 'components/com_axs/templates/report_activity.php';
    }

    public function buildTeamReportsHTML() {
        $key = AxsKeys::getKey('lms');
		include 'components/com_axs/templates/team_reports.php';
    }

    public function buildTeamLearnerSummaryHTML($reportParams) {
        $key = AxsKeys::getKey('lms');
		include 'components/com_axs/templates/team_learner_summary.php';
    }

    public function buildTeamLeaderboardHTML() {
		include 'components/com_axs/templates/team_leaderboard.php';
    }
    public function getURLBase() {
        $url = $_SERVER['REQUEST_URI'];
        $urlArray = explode('?',$url);
        return $urlArray[0];
    }

	public function ordinalNumber($number) {
	    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13)) {
	        return $number. 'th';
	    } else {
	        return $number. $ends[$number % 10];
	    }
    }

    public function convertListToIntegers($list) {
        $listArray = array_map('intval', explode(',', $list));
        return implode(',',$listArray);
    }

    public function getAttendance($userId = null) {
        $db = JFactory::getDbo();
        if($userId) {
            $conditions[] = "user_id = " . (int)$userId;
        } else {
            if(!$this->userList) {
                return false;
            }
            $conditions[] = "user_id IN (".$this->userList.")";
        }
		$table = 'axs_virtual_classroom_attendees';
		$query = $db->getQuery(true);
		$query->select('attendee.*,user.*,meeting.meetingName');
		$query->from($db->quoteName($table,'attendee'));
        $query->join('INNER', $db->quoteName('#__users','user') .' ON '.$db->quoteName('attendee.user_id').'='.$db->quoteName('user.id'));
        $query->join('INNER', $db->quoteName('#__bbb_meetings','meeting') .' ON '.$db->quoteName('attendee.meeting_id').'='.$db->quoteName('meeting.meetingId'));
		$query->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
    }

    public function getActivityReport() {
        $db = JFactory::getDbo();
        if(!$this->dashboardParams->activity_report) {
            return false;
        }
        $conditions[] = "id = " . (int)$this->dashboardParams->activity_report;
		$table = 'axs_activity_reports';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($table));
        $query->where($conditions);
        $query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
    }

    public function getTeamLearnerSummary() {
        $db = JFactory::getDbo();
        if(!$this->dashboardParams->team_learner_summary_report) {
            return false;
        }
        $conditions[] = "id = " . (int)$this->dashboardParams->team_learner_summary_report;
		$table = 'axs_reports';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($table));
        $query->where($conditions);
        $query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
    }

    public static function getTeamAssignments($team_id) {
        $db = JFactory::getDbo();
        $conditions[] = "team_id = " . (int)$team_id;
		$table = '#__splms_courses_groups';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($table));
        $query->where($conditions);
        $query->order('id DESC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

	public function getLeaderboardList($leaderboardAmount) {
        if(!$this->userList) {
            return false;
        }
		$db = JFactory::getDBO();
		$limit = (int)$leaderboardAmount;
        //$conditions[] = "category_id IN (1,2)";
        $conditions[] = "user_id IN (".$this->userList.")";
		$query = $db->getQuery(true);
		$query->select('SUM(points) AS total, user_id, category_id')
		      ->from('axs_points')
		      ->where($conditions)
		      ->group('user_id')
		      ->order('total DESC')
		      ->setLimit($limit);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
    }

    public static function getLeadersTeams($userId = null) {
        if(!$userId) {
            $userId = JFactory::getUser()->id;
        }
        $db = JFactory::getDBO();
        $conditions[] = "team_lead != ''";
        $conditions[] = "enabled = 1";
		$conditions[] = "FIND_IN_SET( $userId, team_lead ) > 0 ";
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('axs_teams')
		      ->where($conditions)
		      ->order('title ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
    }

    public static function getLeaderDefaultTeam($user_id) {
        if(!$user_id) {
            return false;
        }
		$db = JFactory::getDBO();
        $conditions[] = "user_id = ".(int)$user_id;
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('axs_team_dashboard_defaults')
		      ->where($conditions)
		      ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
    }

    public static function getSelectedTeam($userId = null,$teamId) {
        if(!$userId) {
            $userId = JFactory::getUser()->id;
        }
        if(!$teamId) {
            return false;
        }
        $db = JFactory::getDBO();
        $conditions[] = "team_lead != ''";
        $conditions[] = "enabled = 1";
        $conditions[] = "id = ".$db->quote($teamId);
		$conditions[] = "FIND_IN_SET( $userId, team_lead ) > 0 ";
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('axs_teams')
		      ->where($conditions)
		      ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
    }

    public function getUsersFromGroupList($groups) {
        $db = JFactory::getDbo();
        $groups_list = explode(',',$groups);
        foreach($groups_list as $group) {
            $group_query[] = 'group_id = '. (int)$group;
        }
        $groupSet = implode(' OR ', $group_query);
        $conditions = '('.$groupSet.')';
        $query = $db->getQuery(true);
        $query->select('DISTINCT user_id')
              ->from('#__user_usergroup_map')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if(!$result) {
            return false;
        } else {
            $userArray = array();
            foreach($result as $user) {
                array_push($userArray, (int)$user->user_id);
            }
            return implode(',',$userArray);
        }
    }

    public function getTeamMembers() {
        if(!$this->userList) {
            return false;
        }
		$db = JFactory::getDBO();
		$conditions[] = "id IN (".$this->userList.")";
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('joom_users')
		      ->where($conditions)
		      ->order('name ASC');
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

    public static function getTeamMembersAJAX($sort = null, $search = null, $userList = null) {
        if(!$userList) {
            return false;
        }

		$db = JFactory::getDBO();
		$conditions[] = "id IN (".$userList.")";
        $order = 'name ASC';
        if($search) {
            $search = AxsSecurity::alphaNumericOnly($search);
            $conditions[] = "name LIKE '%$search%'";
        }

        if($sort) {
            $sort = AxsSecurity::alphaNumericOnly($sort);
            switch($sort) {
                case "alphabeticalASC":
                    $order = 'name ASC';
                break;
                case "alphabeticalDESC":
                    $order = 'name DESC';
                break;
                default:
                    $order = 'name ASC';
                break;
                /*
                case "coursesMost":
                    return b.dataset.courses - a.dataset.courses;
                break;
                case "coursesLeast":
                    return a.dataset.courses - b.dataset.courses;
                break;
                case "checklistMost":
                    return b.dataset.checklists - a.dataset.checklists;
                break;
                case "checklistLeast":
                    return a.dataset.checklists - b.dataset.checklists;
                break; */
            }
        }
		$query = $db->getQuery(true);
		$query->select('id')
		      ->from('joom_users')
		      ->where($conditions)
		      ->order($order);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

    public function getTeamLeaders($nameList = false) {
        if(!$this->team->team_lead) {
            return false;
        }
		$db = JFactory::getDBO();
		$conditions[] = "id IN (".$this->team->team_lead.")";
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('joom_users')
		      ->where($conditions)
		      ->order('name ASC');
		$db->setQuery($query);
        $results = $db->loadObjectList();

        if($nameList) {
            $leaderArray = array();
            foreach($results as $leader) {
                array_push($leaderArray,$leader->name);
            }
            return implode(',',$leaderArray);
        }

		return $results;
	}
}