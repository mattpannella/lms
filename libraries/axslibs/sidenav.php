<?php

defined('_JEXEC') or die;

class AxsSideNav {

    /**
     * Increment counter for menu ids
     *
     * @var int
     */
    private static $idCounter = 0;

    /**
     * Menu item data
     *
     * @var array
     *
     */
    private static $menuItems = array();


    /**
     * The active top level menu item
     *
     */
    private static $activeTopLevelItem;

    /**
     * Menu items that a (New) badge should go next to
     */
    private static $newMenuItems = array();

    /**
     * Renders the side navigation
     *
     * @return void
     *
     */
    public static function renderSideMenu() {
        JFactory::getDocument()->addScriptDeclaration("
            jQuery(document).on('click', '[data-parent-menu-item]', function () {
                jQuery(`[id*='menuItem']`).each((idx, el) => {
                    jQuery(el).find('.nav-link').removeClass('active');
                });
                jQuery(`[id*='submenu'`).hide();
                var submenuId = jQuery(this).attr('data-submenu-id');
                jQuery('#' + submenuId).show();

                // Make sure the sidenav is shown in case we are on the
                // splashpages view
                jQuery('#adminSubnavContainer').show();
                jQuery('.admin_main').attr('style', 'padding-left:328px !important');
            });
        ");
        ob_start();
        ?>
        
        <div class="tov-sidenav-radius" style="z-index:1000">
            <svg width="8" height="8" viewBox="0 0 8 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M8 0H0V8C0 3.6 3.6 0 8 0Z" fill="#1E2935"></path>
            </svg>
        </div>
        <div id="adminNavContainer" class="tov-sidenav-container flex-column hide-scrollbar" data-active-item="<?php echo self::getActiveTopLevelItem()->name; ?>" style="overflow:scroll">
            <ul class="nav flex-column text-center">
                <?php
                    foreach(self::getItems() as $menuItem) {
                        if ($menuItem != null && $menuItem->access == true) {
                            $href = !is_array($menuItem->function) ? $menuItem->function : '#';
                            ?>
                                <li id="menuItem-<?php echo $menuItem->id ?>" data-parent-menu-item="true" data-submenu-id="<?php echo $menuItem->id ?>-submenu" class="nav-item">
                                    <a href="<?php echo $href ?>" class="nav-link <?php echo $menuItem->active ? 'active' : '' ?>" aria-current="page" title="">
                                        <i class="<?php echo self::getIcon($menuItem->name) ?>"></i>
                                    </a>
                                </li>                                
                            <?
                            JFactory::getDocument()->addScriptDeclaration("
                                $(() => {
                                    tippy('#menuItem-$menuItem->id', {
                                        theme: 'tovuti',
                                        content: '$menuItem->name',
                                        placement: 'right',
                                        arrow: false,
                                    });
                                });
                            ");
                        }
                    }
                ?>
            </ul>
        </div>

        <div id="adminSubnavContainer" class="hide-scrollbar tov-subSidenav-container flex-column" style="overflow:scroll">
            <div class="tov-subSidenav-wrapper">
                <?php
                    foreach(self::getItems() as $menuItem) {
                        if ($menuItem != null) {
                            self::renderChildMenuItems($menuItem);
                        }
                    }
                ?>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    /**
     * Renders the sub-menu items for a given parent menu item
     *
     * @param object $parentMenuItem
     * {
     *  "function": array
     *  "name": string,
     *  "id": string
     * }
     */
    private static function renderChildMenuItems($parentMenuItem) {
        if (!is_array($parentMenuItem->function)) {
            // If function isnt an array, there are no sub menu items to render
            return;
        }
        if ($parentMenuItem->access == false) {
            return;
        }
        ?>
        <div id="<?php echo $parentMenuItem->id ?>-submenu" <?php echo $parentMenuItem->active == false ? 'style="display:none"' : '' ?>>
            <h6 class="tov-subSidenav-heading">
                <i class="fas fa-circle"></i>
                <?php echo $parentMenuItem->name ?>
            </h6>

            <ul class="nav flex-column">
                <?php
                    foreach ($parentMenuItem->function as $subMenuItem) {
                        if ($subMenuItem->access == false) {
                            continue;
                        }
                        if ($subMenuItem->type == 'header') {
                            ?>
                                <div class="tov-subSidenav-heading" id="<?php echo $subMenuItem->id ?>">
                                    <?php echo $subMenuItem->name ?>
                                </div>
                            <?php
                        }
                        if ($subMenuItem->type == "menuItem"):
                            ?>
                                <li class="nav-item child-menu-item" id="<?php echo $subMenuItem->id ?>">
                                    <a href="<?php echo $subMenuItem->function ?>" class="nav-link <?php echo $subMenuItem->active ? 'active' : '' ?>" aria-current="page" title="">
                                        <?php echo $subMenuItem->name ?>
                                        <?php if (in_array($subMenuItem->name, self::$newMenuItems)): ?>
                                            <span class="badge bg-secondary">New</span>
                                        <?php endif; ?>
                                    </a>
                                </li>
                            <?php
                        endif;
                    }
                ?>
            </ul>
        </div>


        <?php
    }

    public static function getActiveTopLevelItem() {
        if (!isset(self::$activeTopLevelItem)) {
            foreach (self::getItems() as $item) {
                if ($item->active) {
                    self::$activeTopLevelItem = $item;
                }
            }
        }
        return self::$activeTopLevelItem;
    }

    /**
     * @param string $name - the text shown for the nav item
     * @param array|string $function - either a uri or an array of the sub menu items
     * @param bool $access - if false, do not render this menu item
     *
     * @return object
     */
    private static function menuItem(string $name, $id = null, $function, $access = true, $tags = array()) {
        $item = new stdClass();

        $item->id = $id;
        $item->name = $name;
        $item->function = $function;
        $item->access = $access;
        $item->type = 'menuItem';
        $item->active = false;
        $item->tags = $tags;

        return $item;
    }

     /**
     * @param string $name
     * @param bool $access - if false, do not render this header
     *
     * @return object
     */
    private static function header(string $name, string $id = null, bool $access = true, $tags = array()) {
        $header = new stdClass();

        $header->name = $name;
        $header->id = $id;
        $header->access = $access;
        $header->type = 'header';
        $header->tags = $tags;

        return $header;
    }

    /**
     * Returns the associated icon based on the menu name
     * or a default if one isn't set
     */
    public static function getIcon($menuItemName) {
        $menuItemToIconMap = array(
            'All' => 'fas fa-ball-pile',
            'People' => 'fas fa-users',
            'Content Creation' => 'fas fa-shapes',
            'Learning' => 'fas fa-graduation-cap',
            'Communications' => 'fas fa-bullhorn',
            'E-commerce' => 'fas fa-money-bill-wave',
            'Analytics' => 'fas fa-analytics',
            'Social' => 'fas fa-comment-alt',
            'Design' => 'fas fa-palette',
            'Website' => 'fas fa-browser',
            'Configuration' => 'fas fa-cog',
            'Tovuti Clients' => 'fa fa-user-circle',
            'Onboarding Checklists' => 'fa fa-check-square'
        );

        if (isset($menuItemToIconMap[$menuItemName])) {
            return $menuItemToIconMap[$menuItemName];
        } else {
            return 'fas fa-question';
        }
    }

    /**
     * Gets a data object that represents the menu items
     *
     * Any menu items that they don't have access to will not be included
     *
     * After the data model is ready, a final access check is ran and the
     * active menu items are marked
     *
     * @return array
     */
    public static function getItems() {

        if (self::$menuItems) {
            return self::$menuItems;
        }

        $people = self::menuItem('People','tovuti_nav_people',
            array(
                self::menuItem('User Manager','tovuti_nav_user_manger', 'index.php?option=com_users&view=axsusers'),
                self::menuItem('User Groups','tovuti_nav_user_groups', 'index.php?option=com_users&view=groups'),
                self::menuItem('User Teams','tovuti_nav_user_teams', 'index.php?option=com_axs&view=teams'),
                self::menuItem('User Importer','tovuti_nav_user_importer', 'index.php?option=com_axs&view=importers'),
                self::menuItem('User Notes','tovuti_nav_user_notes', 'index.php?option=com_users&view=notes'),
                self::menuItem('User Note Categories','tovuti_nav_user_note_categories', 'index.php?option=com_categories&view=categories&extension=com_users'),
                self::menuItem('Access Levels','tovuti_nav_user_access_levels', 'index.php?option=com_users&view=levels'),
                self::menuItem('Admin Permissioning','tovuti_nav_user_admin_permissioning', 'index.php?option=com_permissioning&view=groups'),
                self::menuItem('Teachers','tovuti_nav_teachers', 'index.php?option=com_splms&view=teachers'),
            )
        );

        $content_creation = self::menuItem('Content Creation','tovuti_nav_content_creation',
            array(
                self::menuItem('Interactive Content','tovuti_nav_interactive_content', 'index.php?option=com_interactivecontent'),
                self::menuItem('Content Library','tovuti_nav_content_library', 'index.php?option=com_axs&view=content_libraries', AxsDbAccess::checkAccess("bizlibrary")),
                self::menuItem('SCORM Library','tovuti_nav_scorm_library', 'index.php?option=com_splms&view=scormitems', AxsDbAccess::checkAccess("scorm")),
                self::header('Assessments','tovuti_nav_content_assessments_header'),
                self::menuItem('Quizzes','tovuti_nav_quizzes', 'index.php?option=com_splms&view=quizquestions'),
                self::menuItem('Surveys','tovuti_nav_surveys', 'index.php?option=com_axs&view=surveys'),
                self::header('Media','tovuti_nav_content_media_header'),
                self::menuItem('Media Items','tovuti_nav_media_items', 'index.php?option=com_axs&view=videos', AxsDbAccess::checkAccess("media")),
                self::menuItem('Media Categories','tovuti_nav_media_categories', 'index.php?option=com_axs&view=categories', AxsDbAccess::checkAccess("media"), array('category')),
                self::menuItem('File Manager','tovuti_nav_file_manager', 'index.php?option=com_media'),
                self::menuItem('Secure File Locker','tovuti_nav_secure_file_locker', 'index.php?option=com_axs&view=lockedfiles')
            )
        );

        $learning = self::menuItem('Learning','tovuti_nav_learning',
            array(
                self::menuItem('Settings','tovuti_nav_learning_settings', 'index.php?option=com_splms&view=settings'),
                self::menuItem('Course Categories','tovuti_nav_learning_course_categories', 'index.php?option=com_splms&view=coursescategories', true, array('category')),
                self::menuItem('Courses','tovuti_nav_learning_courses', 'index.php?option=com_splms&view=courses'),
                self::menuItem('Lessons','tovuti_nav_learning_lessons', 'index.php?option=com_splms&view=lessons'),
                self::menuItem('Checklists','tovuti_nav_learning_checklists', 'index.php?option=com_axs&view=checklists'),
                self::menuItem('Course Assignments','tovuti_nav_learning_courseassignments', 'index.php?option=com_splms&view=courseassignments'),
                self::menuItem('Virtual Classroom','tovuti_nav_learning_virtual_classrooms', 'index.php?option=com_axs&view=virtual_classrooms', AxsDbAccess::checkAccess("virtual_classroom")),
                self::menuItem('Course Comments','tovuti_nav_learning_comments', 'index.php?option=com_axs&view=comments'),
                self::header('Gamification','tovuti_nav_learning_gamification_header'),
                self::menuItem('Badges & Certificates','tovuti_nav_learning_badges', 'index.php?option=com_award&view=badges', true, array('badges', 'certificates', 'milestones', 'awards')),
                self::menuItem('Certificate Designer','tovuti_nav_learning_certificates', 'index.php?option=com_splms&view=certificates', true, array('certificates', 'awards')),
                self::menuItem('Points Categories','tovuti_nav_learning_points_categories', 'index.php?option=com_axs&view=points_categories', true, array('awards', 'category'))
            )
        );

        $communications = self::menuItem('Communications','tovuti_nav_communications',
            array(
                self::menuItem('Notifications','tovuti_nav_notifications', 'index.php?option=com_notifications&view=auto_alerts'),
                self::menuItem('Email Notifications','tovuti_nav_email_notifications', 'index.php?option=com_notifications&view=email_notifications'),
                self::menuItem('Popup Notifications','tovuti_nav_popup_notifications', 'index.php?option=com_notifications&view=popup_notifications')
            )
        );

        $e_commerce = self::menuItem('E-commerce','tovuti_nav_ecommerce',
            array(
                self::menuItem('Sales','tovuti_nav_sales', 'index.php?option=com_converge&view=transactions'),
                self::menuItem('Subscription Categories','tovuti_nav_subscriptions_categories', 'index.php?option=com_converge&view=categories', true, array('category')),
                self::menuItem('Subscription Plans','tovuti_nav_subscriptions_plans', 'index.php?option=com_converge&view=plans'),
                self::menuItem('Subscription Requests','tovuti_nav_subscriptions_requests', 'index.php?option=com_converge&view=subscriptions'),
                self::menuItem('Subscription Cancellations','tovuti_nav_subscriptions_cancellations', 'index.php?option=com_converge&view=cancellation_requests'),
                self::menuItem('Failed Signups', 'tovuti_nav_analytics_sub_failed', 'index.php?option=com_converge&view=failed_signups'),
                self::menuItem('Promo Codes','tovuti_nav_subscriptions_promos', 'index.php?option=com_converge&view=promos', true, array('coupons'))
            )
        );


        $analytics = self::menuItem('Analytics','tovuti_nav_analytics',
            array(
                self::menuItem('Report Builder','tovuti_nav_analytics_report_builder', 'index.php?option=com_reports'),
                self::menuItem('Activity Reports','tovuti_nav_analytics_activity_reports', 'index.php?option=com_reports&view=activity_reports'),
                self::menuItem('Expiration Reports','tovuti_nav_analytics_expiration_reports', 'index.php?option=com_reports&view=expiration_reports'),
                self::menuItem('Activity Dashboards','tovuti_nav_analytics_activity_dashboards', 'index.php?option=com_reports&view=activity_dashboards'),
                self::menuItem('Financial Dashboards','tovuti_nav_analytics_financial_reports', 'index.php?option=com_dashboard'),
                self::menuItem('Quiz Results','tovuti_nav_analytics_quiz_results', 'index.php?option=com_splms&view=quizresults'),
            )
        );

        $tagsForSocial = array('social');

        $social = self::menuItem('Social','tovuti_nav_social', array());

        if (AxsDbAccess::checkAccess("events")) {
            $social->function = array_merge(
                $social->function,
                array(
                    self::header('Events','tovuti_nav_social_events_header', true, $tagsForSocial),
                    self::menuItem('Dashboard','tovuti_nav_social_event_dashboard', 'index.php?option=com_eventbooking', true, $tagsForSocial),
                    self::menuItem('Registrants','tovuti_nav_social_event_registrants', 'index.php?option=com_eventbooking&view=registrants', true, $tagsForSocial),
                    self::menuItem('Event Categories','tovuti_nav_social_event_categories', 'index.php?option=com_eventbooking&view=categories', true, array_merge($tagsForSocial, array('category'))),
                    self::menuItem('Events','tovuti_nav_social_events', 'index.php?option=com_eventbooking&view=events', true,  $tagsForSocial),
                    self::menuItem('Configuration','tovuti_nav_social_event_configuration',  'index.php?option=com_eventbooking&view=configuration', true, $tagsForSocial),
                )
            );
        }

        if (AxsDbAccess::checkAccess("community")) {
            $social->function = array_merge(
                $social->function,
                array(
                    self::header('Community','tovuti_nav_social_community_header', true, $tagsForSocial),
                    self::menuItem('Dashboard','tovuti_nav_social_community_dashboard', 'index.php?option=com_community', true, $tagsForSocial),
                    self::menuItem('Settings','tovuti_nav_social_community_settings', 'index.php?option=com_community&view=configuration&cfgSection=site', true, $tagsForSocial),
                    self::menuItem('Group Pages','tovuti_nav_social_community_groups', 'index.php?option=com_community&view=groups', true, $tagsForSocial),
                    self::menuItem('Community Events','tovuti_nav_social_community_events', 'index.php?option=com_community&view=events', true, $tagsForSocial)
                )
            );
        }

        $tagsForForum = array_merge($tagsForSocial, array('forums'));
        if (AxsDbAccess::checkAccess("forum")) {
            $social->function = array_merge(
                $social->function,
                array(
                    self::header('Discussion Boards','tovuti_nav_social_forum_header', true, $tagsForForum),
                    self::menuItem('Dashboard','tovuti_nav_social_forum_dashboard', 'index.php?option=com_kunena', true, $tagsForForum),
                    self::menuItem('Settings','tovuti_nav_social_forum_settings',  'index.php?option=com_kunena&view=config', true, $tagsForForum),
                    self::menuItem('Categories','tovuti_nav_social_forum_categories', 'index.php?option=com_kunena&view=categories', true, array_merge($tagsForForum, array('category')) ),
                    self::menuItem('Attachments','tovuti_nav_social_forum_attachments', 'index.php?option=com_kunena&view=attachments', true, $tagsForForum),
                )
            );
        }

        $design = self::menuItem('Design','tovuti_nav_design',
                array(
                    self::menuItem('Quick Start','tovuti_nav_design_quick_start', 'index.php?option=com_axs&view=zenbuilders'),
                    self::menuItem('Brands','tovuti_nav_design_brands', 'index.php?option=com_axs&view=brands', true, ['languages']),
                    self::menuItem('User Portals','tovuti_nav_design_user_portals', 'index.php?option=com_axs&view=brand_dashboards'),
                    self::menuItem('User Dashboards','tovuti_nav_design_user_dashboard', 'index.php?option=com_axs&view=learner_dashboards'),
                    self::menuItem('Login Pages','tovuti_nav_design_login_pages', 'index.php?option=com_axs&view=login_pages'),
                )
        );

        $website = self::menuItem('Website','tovuti_nav_design_website',
                array(
                    self::menuItem('Landing Pages','tovuti_nav_design_landing_pages', 'index.php?option=com_axs&view=brand_homepages'),
                    self::menuItem('Website Pages','tovuti_nav_design_pages', 'index.php?option=com_content'),
                    self::menuItem('Blogs','tovuti_nav_design_blogs', 'index.php?option=com_easyblog&view=blogs', AxsDbAccess::checkAccess("blog")),
                    self::menuItem('Forms','tovuti_nav_design_forms', 'index.php?option=com_contactpage&view=contactpages'),
                )
        );

        $configuration = self::menuItem('Configuration','tovuti_nav_configuration',
                array(
                    self::menuItem('Profile Fields','tovuti_nav_configuration_profile_fields', 'index.php?option=com_community&view=profiles', AxsDbAccess::checkAccess("community")),
                    self::menuItem('Domains','tovuti_nav_configuration_domains', 'index.php?option=com_domains&view=domains'),
                    self::menuItem('Email','tovuti_nav_configuration_email', 'index.php?option=com_axs&view=email_settings'),
                    self::menuItem('Payment Gateways','tovuti_nav_configuration_payment_gateways', 'index.php?option=com_axs&view=payment_gateways'),
                    self::menuItem('Navigation Menus','tovuti_nav_configuration_navigation', 'index.php?option=com_menus&view=menus'),
                    self::menuItem('Fonts','tovuti_nav_configuration_fonts', 'index.php?option=com_axs&view=fonts'),
                    self::menuItem('Integrations','tovuti_nav_configuration_integrations', 'index.php?option=com_axs&view=integrations'),
                    self::menuItem('Single Sign On','tovuti_nav_configuration_sso', 'index.php?option=com_axs&view=sso_configs', AxsDbAccess::checkAccess("sso"), array('SSO', 'SAML')),
                    self::menuItem('API Keys','tovuti_nav_configuration_api_keys', 'index.php?option=com_axs&view=apikeys', AxsDbAccess::checkAccess("api")),
                    self::menuItem('Security','tovuti_nav_configuration_security', 'index.php?option=com_axs&view=securities'),
                    self::menuItem('User Avatars','tovuti_nav_configuration_user_avatars', 'index.php?option=com_axs&view=avatars'),
                    self::header('Accessibility','tovuti_nav_accessibility'),
                    self::menuItem('Images', 'tovuti_nav_accessibility_images', 'index.php?option=com_accessibility&view=images'),
                    self::menuItem('Videos', 'tovuti_nav_accessibility_videos', 'index.php?option=com_accessibility&view=videos')
                )
        );

        $items = array(
            $people,
            $content_creation
        );

        if (AxsDbAccess::checkAccess("lms")) {
            $items []= $learning;
        }

        if (AxsDbAccess::checkAccess("notifications")) {
            $items []= $communications;
        }

        if (AxsDbAccess::checkAccess("subscriptions")) {
            $items []= $e_commerce;
        }

        if (AxsDbAccess::checkAccess("reporting")) {
            $items []= $analytics;
        }

        if (AxsDbAccess::checkAccess("events") || AxsDbAccess::checkAccess("forum") || AxsDbAccess::checkAccess("community")) {
            $items []= $social;
        }

        

        $items []= $design;

        $items []= $website;

        $items []= $configuration;


        $clientId = AxsClients::getClientId();
        $user = JFactory::getUser();
        $adminApproved = [601, 1620427, 1620537, 1620736, 1620326, 1620494];
        $isAdmin = AxsUser::isAdmin(JFactory::getUser()->id);
        $isSuperUser = AxsUser::isSuperUser(JFactory::getUser()->id);
        $isDeveloper = AxsUser::isDeveloper(JFactory::getUser()->id);

        if (($clientId == 9 || $clientId == 22) && (in_array($user->id,$adminApproved) || $isSuperUser)) {
            $clients = self::menuItem('Tovuti Clients', 'clients_list', array(
                self::menuItem('Client Admins', 'Admins', 'index.php?option=com_dbmanager&view=clients'),
                self::menuItem('Client Instances', 'Instances', 'index.php?option=com_dbmanager&view=databases'),
                self::menuItem('Client Active Users', 'activ_users', 'index.php?option=com_dbmanager&view=clients&layout=users'),
                self::menuItem('Client Blog Usage', 'usage', 'index.php?option=com_dbmanager&view=clients&layout=usage')
            ));
            $items[] = $clients;
        }

        if(($clientId == 9 || $clientId == 22)) {
            $onboarding = self::menuItem('Onboarding Checklists', 'onboarding_checklist', array(
                self::menuItem('Onboarding Checklists', 'onboarding_checklists', 'index.php?option=com_axs&view=admin_checklists')
            ));
            $items[] = $onboarding;
        }

        if (isset($_SESSION['admin_access_levels']->permissions) && !$isAdmin && !$isSuperUser && !$isDeveloper) {
            //They don't automatically have full permissions.  Check what they do have.
            $permissions = $_SESSION['admin_access_levels']->permissions;
            self::checkPermissions($permissions, $items);
        }

        self::markActiveItems($items);

        self::$menuItems = $items;

        return $items;
    }

    /**
     * Cycles through the menu items and determines which item to highlight as active
     *
     * The first pass it compares the option and view uri params to the option and view
     * uri params of the menu items. If the params match then the item and its parent
     * is marked as active. There is also a check for if the plural version of the view
     * matches or not.
     *
     * The second pass is ran whenever an active item isn't found in the first pass. This
     * is to account for cases where the current view does not exist in the sidenav.
     *
     * @param array $items - the menu items from self::getItems()
     *
     * @return void
     */
    private static function markActiveItems(&$items) {

        $input = JFactory::getApplication()->input;
        $option = $input->get('option');
        $view = $input->get('view');
        $view_plural = $view_plural = $view . 's';
        $view_plural_secondary = substr($view, 0, strlen($view) - 1) . 'ies';

        $activeFound = false;
        foreach ($items as &$item) {
            if ($item->function == null) {
                continue;
            }
            foreach($item->function as &$subItem) {
                $uri = '/administrator/' . $subItem->function;
                $uri_parsed = parse_url($uri);
                $query = explode('&', $uri_parsed['query']);
                $query_parsed = array();
                foreach($query as $var) {
                    $query_parsed[explode('=', $var)[0]] = explode('=', $var)[1];
                }
                $isActive =
                    $uri == $_SERVER['REQUEST_URI']
                    || (
                        in_array($query_parsed['view'], array($view, $view_plural, $view_plural_secondary))
                        && $option == $query_parsed['option']
                    );

                if ($isActive) {
                    $subItem->active = true;
                    $item->active = true;
                    $activeFound = true;
                    self::$activeTopLevelItem = $item;
                    break;
                }
            }
            if ($item->active == true) {
                break;
            }
        }
        if (!$activeFound) {
            $activeItemName = 'Social';
            $activeSubItemName = null;
            $input = JFactory::getApplication()->input;
            $option = $input->get('option', '', 'STRING');
            if ($option == 'com_easyblog') {
                // Any com_easyblog views should active the Blogs nav item
                $activeItemName = 'Website';
                $activeSubItemName = 'Blogs';
            }
            else if ($option == 'com_content') {
                // Any com_content views should active the Website Pages nav item
                $activeItemName = 'Website';
                $activeSubItemName = 'Website Pages';
            }
            else if ($option == 'com_reports') {
                // Any com_content views should active the Website Pages nav item
                $activeItemName = 'Analytics';
                $activeSubItemName = 'Report Builder';
            }
            else if ($option == 'com_menus' && $view = 'items') {
                // Hard code com_menus items view
                $activeItemName = 'Configuration';
                $activeSubItemName = 'Navigation Menus';
            }
            else if ($option == 'com_interactivecontent' && $view = 'new') {
                $activeItemName = 'Content Creation';
                $activeSubItemName = 'Interactive Content';
            }
            else if ($option == 'com_axs' && $view == 'splashpages') {
                JFactory::getDocument()->addStyleDeclaration('
                    #adminSubnavContainer {
                        display:none;
                    }
                    @media only screen and (min-width: 768px) {
                        .admin_main {
                            padding-left: 72px !important;
                        }
                    }
                    .tov-content-header {
                        padding: 0px !important;
                    }
                    .footer {
                        display: none;
                    }
                    #system-message-container {
                        padding: 0px;
                    }
                    .tov-content-actionbar {
                        padding: 0px;
                    }
                    .tov-dashboard-container {
                        margin-left: 0px;
                        top: 0px;
                    }
                    #system-message-container {
                        margin-top: 0px;
                    }
                ');
                // Don't active any nav items
                return;
            }
            foreach ($items as &$item) {
                if ($item->function == null) {
                    continue;
                }
                if ($item->name == $activeItemName) {
                    $item->active = true;
                    if ($activeSubItemName) {
                        foreach($item->function as $subItem) {
                            if ($subItem->name == $activeSubItemName) {
                                $subItem->active = true;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    /**
     * Get's a flattened array of the menu items to be
     * passed to the search dropdown
     *
     * @return array
     */
    public static function getMenuItemsForSearch() {
        $menuItemsForSearch = array();
        foreach(self::getItems() as $item) {
            if ($item->access == false) {
                continue;
            }
            $header = $item->name;
            foreach($item->function as $subItem) {
                if ($subItem->access == false) {
                    continue;
                }
                if ($subItem->type == 'header') {
                    $header = $subItem->name;
                } else {
                    $menuItemsForSearch []= array(
                        'itemName' => $header,
                        'subItemName' =>  $subItem->name,
                        'uri' => $subItem->function,
                        'tags' => array_merge($subItem->tags, $item->tags)
                    );
                }
            }
        }
        $menuItemsForSearch []= array(
            'itemName' => 'Events',
            'subItemName' =>  'Coupons',
            'uri' => 'index.php?option=com_eventbooking&view=coupons',
            'tags' => ['coupons']
        );
        return $menuItemsForSearch;
    }

    /**
     * Iterates through all of the menu items and remove anything
     * that they don't have access to.
     *
     *
     */
    private static function checkPermissions(&$permissions, &$items) {

        $empty = true;
        foreach ($items as &$item) {

            $set = $permissions[$item->id];

            if (!$set->allowed) {
                $item = null;
            } else {
                $empty = false;
                if (isset($set->children)) {
                    //The menu has children items.
                    $empty = self::checkPermissions($set->children, $item->function);
                    if ($empty) {
                        //All of its children are empty so don't have an empty title.
                        $item = null;
                    }
                }
            }
        }

        return $empty;
    }
}
