<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsAccess {

    private static $address;

    /**
     * This section detects which sections that the client's site has activated. 
     * For example, if they do not have the eshop active, access to the eshop component
     * (whether user or admin) is denied and they will be redirected.
     *    
     * @return null
     * 
     */
    public static function checkSiteAccessLevels() {

        $address = self::getRequest();
        
        //if (JFactory::getUser()->id == 1686) {
            //echo "Option: " . $address->option . " View: " . $address->view . " Task: " . $address->task . " Layout: " . $address->layout;
            //die();
        //}
        
        $feature = null;
        $subfeature = null;

        //Check if they are in an admin or a user view.
        $admin_section = self::isAdminSection();

        switch ($address->option) {

            //See which component (option) they're trying to go to and get the name of the feature.
            //Some only block particular views (such as com_axs) so these include an array of blocked views.

            case "com_eshop":   
                $feature = "eshop"; break;
            case "com_eventbooking":
                $feature = "events"; break;
            case "com_splms":
                $feature = "lms"; 
                if ($address->view == "scormitem" || $address->view == "scormitems") {
                    $subfeature = "scorm";
                }
                break;
            case "com_community":
                $feature = "community"; 
                if ($address->view == "profile") {
                    $subfeature = "profile";
                }
                break;
            case "com_bigbluebutton":
                $feature = "virtual_classroom"; break;
            case "com_kunena":
                $feature = "forum"; break;
            case "com_eventbooking":
                $feature = "events"; break;
            case "com_affiliate": 
                $feature = "events"; break;
            case "com_jbusinessdirectory":
                $feature = "business_directory"; break;
            case "com_axs":
                $boards = array(
                    "inspiration-list",
                    "inspiration-edit",
                    "gratitude-list",
                    "gratitude-edit",
                    "declaration-list",
                    "declaration-edit"
                );

                if (in_array($address->view, $boards)) {
                    $feature = "boards";
                    break;
                }

                if ($address->view == "videos" || $address->view == "categories") {
                    $feature = "media";
                    break;
                }

                if ($address->view == "apikeys") {
                    $feature = "api";
                    break;
                }

                break;
            case "com_easyblog":
                $feature = "blog"; break;
            case "com_reports":
                $feature = "reporting"; break;
            case "com_advpoll":
                $feature = "polls"; break;
            case "com_contracts":
                $feature = "contract_signing"; break;
            case "com_converge":

                if ($admin_section) {
                    $subscription = array(
                        "cancellation_requests",
                        "categories",
                        "category",
                        "failed_signups",
                        "plan",
                        "plans",
                        "registration_field",
                        "registration_fields",
                        "subscriptions"
                    );
                } else {
                    $subscription = array(
                        "plans",
                        "subscription"
                    );
                }

                if (in_array($address->view, $subscription)) {
                    $feature = "subscriptions";
                }

                break;
        }

        $override = false;
        if($feature == 'community' && $subfeature == 'profile') {
            $override = true;
        }

        if(!$override) {
            if (($feature && !AxsDbAccess::checkAccess($feature)) || ($subfeature && !AxsDbAccess::checkAccess($subfeature)) ) {            
                //JError::raiseWarning(404, JTEXT::_("AXS_NOT_ACTIVE"));
                self::redirect($address->app, $admin_section);
            }
        }
        
    }

    /** 
     *  This is to keep admin out of "Joomla admin" sections that only super users / developers should get into. 
     *  Normally, a Joomla install allows full access for all admin, but the Tovuti system does not.
     * 
     * @return null
     */
    public static function checkViewingAccess() {

        // Everything in this section is blocked for ALL users except Super Users.            

        $address = self::getRequest();
        $admin_section = self::isAdminSection();

        /*
        echo "Admin Section: " . $admin_section;
        echo "<br>Option: " . $address->option;
        echo "<br>View: " . $address->view;
        echo "<br>Task: " . $address->task;
        echo "<br>Layout: " . $address->layout;
        */
        //die();

        $user = JFactory::getUser();
        $user_id = $user->id;

        if (AxsUser::isSuperUser(JFactory::getUser()->id)) {
            //Super User has access to everything.  No need to check.            
            return;
        }

        $has_access = true;
        $block_view = null;
        $block_task = null;
        $block_layout = null;

        /*var_dump($address->option);
        var_dump($address->view);
        var_dump($address->task);
        var_dump($address->layout);*/

        if ($admin_section) {

            //Restriction for the admin section.
            switch ($address->option) {

                case "com_community":
                    $block_view = array(
                        "themecolors",
                        "themegeneral",
                        "themeprofile",
                        "themegroups",
                        "themeevents",
                        "moods",
                        "badges",
                        "configuration",
                        "applications",
                        "messaging",
                        "digest",
                        "troubleshoots",
                        "update",
                        "manualdbupgrade",
                        "about"
                    );
                    break;

                case "com_easyblog":
                    $block_view = array(
                        "themes",
                        "migrators",
                        "languages"
                    );

                    if ($address->view == "settings") {
                        $block_layout = array(
                            "media",
                            "seo",
                            "integrations",
                            "mailbox",
                            "remote",
                            "users",
                            "system",
                        );
                    }
                    break;

                case "com_eshop":
                    $block_view = array(
                        "plugins",
                        "themes",
                        "payments",
                        "shippings",
                        "tools",
                        "help"
                    );
                    
                    break;

                case "com_eventbooking":
                    $block_view = array(
                        "plugins"
                    );

                    $block_task = array(
                        "update_db_schema",
                        "check_update",
                        "reset_urls",
                        "share_translation"
                    );
                    break;

                case "com_jbusinessdirectory":
                    $block_view = array(
                        "about",
                        "updates",
                        "orders"
                    );
                    break;
                
                case "com_kunena":

                    $block_view = array(
                        "cpanel",
                        "plugins",
                        "tools",
                        "templates",
                        "misc"
                    );

                    break;
                
                case "com_modules":
                    //var_dump($address->view);
                    $block_view = array(
                        "modules",
                        "select",
                        "preview",
                        "positions"
                    );

                    break;
                
                case "com_admin":

                    $block_view = array(
                        "help",
                        "sysinfo"
                    );
                    break;

                case "com_jce":

                    $block_view = array(
                        "config",
                        "cpanel",
                        "help",
                        "mediabox",
                        "preferences",
                        "pofiles",
                        "updates",
                        "users"
                    );

                    break;

                case "com_users":
                    $block_view = array(
                        "user",
                        "users"
                    );
                    break;

                /*
                case "com_permissioning":
                    $block_view = array(
                        "setup"
                    );
                    break;*/

                //These are components that no one but super admin should ever go into.  All other use is restricted.
                case "com_cache":
                case "com_checkin":
                case "com_contact":
                case "com_config":
                case "com_dbmanager":
                case "com_fields":
                case "com_finder":
                case "com_installer":                
                case "com_joomlaupdate":
                case "com_languages":
                case "com_messages":
                case "com_plugins":
                case "com_postinstall":
                case "com_redirect":
                case "com_saas":
                case "com_search":
                case "com_templates":
                case "com_weblinks":
                    $has_access = false;
                    break;
            }
        } else {
            //Restriction for the front end.

            switch ($address->option) {
                case "com_affiliate":
                    $block_view = array(
                        "cards",
                        "dashboards"
                    );
                    break;

                case "com_axs":
                    /*$block_view = array(
                        "attendees",
                        "brand",
                        "brands",
                        "brand_board",
                        "brand_boards",
                        "brand_dashboard",
                        "brand_dashboards",
                        "brand_homepage",
                        "brand_homepages",
                        "brand_pulse",
                        "brand_pulses",
                        "categories",
                        "comment",
                        "comments",
                        "commission",
                        "commissions",
                        "declaration",
                        "declarations",
                        "font",
                        "fonts",
                        "gratitude",
                        "gratitudes",
                        "inspiration",
                        "inspirations",
                        "partner",
                        "partners",
                        "pulse",
                        "pulses",
                        "video",
                        "videos"
                    );

                    $block_task = array(
                        //"affiliates.deletecode",
                        //"affiliates.savecode",        These are on the front end as well.
                        //"affiliates.checkcode",
                        //"affiliates.removereferee",
                        "affiliates.movereferee",
                        "affiliates.addreferee",
                        "affiliates.changereferrer",
                        "affiliates.checkuser",
                        "affiliates.addfamily",
                        "affiliates.loadmorerewards",
                        "affiliates.adjustpoints",
                        "affiliates.addcommission",
                        "affiliates.changecommissionstatus",
                        "dashboard.getmemberinfo",
                        "dashboard.getextramemberinfo",
                        "dashboard.getboards",
                        "memberboards.deletegoalprogress",
                        "memberboards.updategoalprogress",
                        "memberboards.addgoalprogress",
                    );*/

                    break;

                case "com_converge":
                    $block_view = array(
                        "cancellation_requests",
                        "categories",
                        "category",
                        "failed_signups",
                        "promos",
                        "promo",
                        "promo_activities",
                        "registration_fields",
                        "subscriptions",
                        "transactions"
                    );

                    /*$block_task = array(
                        "cards.updatecard",
                        "cards.updatecardsave",
                        "cards.savecard",
                        "cards.deletecard",
                        "cards.editcardpermissions",
                        "cards.editcardpermissionssave",
                        "cards.updatetoken",
                        "helper.deletecard",
                        "plans.remove",
                    );*/

                    break;




                case "com_contactpages":
                    $has_access = false;
                    break;

                case "com_jbusinessdirectory":
                    $block_view = array(
                        "about",
                        "updates",
                        "orders",
                        "packages"
                    );
                    break;
            }
        }

        //If they have access to the component, check the views, tasks, and layouts.
        if ($has_access) {
            if ($block_view) {
                if (in_array(strtolower($address->view), $block_view)) {
                    $has_access = false;
                }
            }
           
            //override for event and group settings access
            if ($address->option == 'com_community' && $address->view == 'configuration' ) {
                $input = JFactory::getApplication()->input;
                $cfgSection = $input->get('cfgSection','','STRING');
                $allowedSettings = ['event','group','video','photo','site','privacy'];
                if(in_array($cfgSection,$allowedSettings)) {
                    $has_access = true;
                }                
            }

            if ($block_task) {
                if (in_array(strtolower($address->task), $block_task)) {
                    $has_access = false;
                }
            }

            if ($block_layout) {
                if (in_array(strtolower($address->layout), $block_layout)) {
                    $has_access = false;
                }
            }
        }

        if (!$has_access) {
           //JError::raiseWarning(404, JTEXT::_("AXS_NO_ACCESS"));
            self::redirect($address->app);
        }
    }

    /**
     * This is for user assigned admin permissions.
     * Administrators can set up permissioning for sub-administrators.  This section checks to see
     * if the sub-administrator has access to the section.  If not, they are redirected.
     * 
     * @return null
     */
    public static function checkAdminPermissions() {

        if (!self::isAdminSection()) {
            //This check is only for the admin section.  There is no point in checking any of this for front end access.
            return;
        }

        $user_id = JFactory::getUser()->id;
        if (AxsUser::isSuperUser($user_id) || AxsUser::isDeveloper($user_id) || AxsUser::isAdmin($user_id)) {
            //Only sub-admin have permissioning.  Full admins have full permissions.  No need to check.
            return;
        }

        if ($user_id == 0) {
            //Thier user ID is still 0.  This only happens when logging in and the view is the com_login.
            //Without this, they'll be routed back to the login view.
            return;
        }

        if (isset($_SESSION['admin_access_levels']) && isset($_SESSION['admin_access_levels']->permissions)) {
            $permissions = $_SESSION['admin_access_levels']->permissions;
        } else {
            //No permissions have been set for this user.
            $permissions = null;            
        }

        self::$address = self::getRequest();

        $option = self::$address->option;
        $view = self::$address->view;
        $task = self::$address->task;

        if ($permissions) {
            $has_access = true;
            //Go through all of the permissions to see if they have access to where they're trying to go.
            self::parseDepth($permissions, $has_access);
        } else {
            //They have no permissions. Check to see if they're being routed to the main admin page.  
            //If so, don't redirect them or it'll be an infinite redirect.

            if (!$option || $view == 'splashpages') {
                //Main admin page
                $has_access = true;
            } else {
                //Check if they're trying to access their own admin profile, or the login/logout page.
                if ($option == "com_login") {
                    $has_access = true;
                } else if ($option == "com_admin") {
                    if ($task == "profile.edit" || $view == "profile") {
                        $has_access = true;
                    } else {
                        $has_access = false;
                    }
                } else {
                    //Any other page.  They don't have access
                    $has_access = false;
                }                
            }
        }

        if (!$has_access) {
            //JError::raiseWarning(404, JTEXT::_("AXS_NO_ACCESS"));
            self::redirect(self::$address->app);
        }
    }

    private static function parseDepth($permissions, &$has_access) {

        $option = self::$address->option;
        $view = self::$address->view;
        $extension = self::$address->extension;

        /*

            This recursively goes through the permissioning object

            The permissioning object is set up like this:
            This would be the object to allow/block access to the homepage designer

            $homepage = $permissioning["home_page_themes"];         //the section of the site.  

            $homepage->allowed = true;                              //Are they allowed to go there?
            $blocked = $homepage->blocked;                          //Data on the section of the site this permissions

            $blocked->id = "configuration-home_page_themes";        //The menu item in the dashboard that this permissions.
            $blocked->option = "com_axs";                           //The component
            $blocked->view = "brand_dashboards,brand_dashboard";    //List of all views.
            $blocked->extension = "";                               //Some have extensions that need to be blocked too.



            Also, some of them have children.  In the dashboard menu, this would be when you click on "Configuration".
            The children are all of the menu items underneath it.  If "Configuration" is blocked, then all of its children 
            are blocked.  If it's not, then all of the children need to be checked.
        */

        foreach ($permissions as $system => $permission) {

            if (isset($permission->children)) {
                //Does this permission have children?  If so, recursively parse it.
                self::parseDepth($permission->children, $has_access);
            } else {
                //If not, see fi permission is allowed
                if (!$permission->allowed) {
                    //If not, find out what is blocked.
                    $blocked = $permission->blocked;

                    //See if the current site option matches the one for this permissioning.
                    if ($option == $blocked->option) {
                        //If so, see if it has views.
                        if ($blocked->view) {

                            //If it has views, create an array out of the list of views.
                            $view_list = explode(",", $blocked->view);

                            foreach ($view_list as $view_item) {
                                //Go through all of the views until a match is found.
                                if ($view == $view_item) {
                                    //See if it has extensions
                                    if ($blocked->extension) {
                                        //Repeat
                                        $extension_list = explode(",", $blocked->extension);

                                        //Go through each extension.
                                        foreach ($extension_list as $extension_item) {
                                            if ($extension == $extension_item) {
                                                //The option, view, and extension match this blocked access.  
                                                $has_access = false;
                                            }
                                        }
                                    } else {
                                        //The option and view match this blocked access.
                                        $has_access = false;
                                    }

                                }
                            }
                        } else {
                            //The whole option is blocked.
                            $has_access = false;
                        }
                    }   
                }
            }
        }
    }

    /**
     * Method for checking restricted content.  Tovuti needs certain data, such as user groups, in place to operate correctly.
     * Normally, Joomla allows these to be deleted, but Tovuti does not.
     * 
     * @return null
     */
    public static function checkRestrictedContent() {

        //This section is for denying access to anything that Tovuti needs to operate correctly and that users should never be allowed to change.
        $address = self::getRequest();
        $app = JFactory::getApplication();

        switch ($address->option) {
            case "com_users":
                //Block access to Tovuti required user groups
                if ($address->view == "group") {
                    $id = JRequest::getVar('id');

                    $restricted_user_groups = AxsUser::$restricted_user_groups;

                    if (in_array($id, $restricted_user_groups)) {
                        JError::raiseWarning(404, JText::_('This User Group cannot be edited or deleted.'));
                        $app->redirect(JRoute::_("/administrator/index.php?option=com_users&view=groups"));
                    }
                }

                //THis is a needed access level.
                if ($address->view == "level") {
                    $id = JRequest::getVar('id');
                    if ($id == 3) {
                        JError::raiseWarning(404, JText::_('This Access Level cannot be edited or deleted.'));
                        $app->redirect(JRoute::_("/administrator/index.php?option=com_users&view=levels"));
                    }
                }

                break;
        }
    }

    private static function getRequest() {

        //Get the current location the user is trying to visit.

        if (self::$address) {
            return self::$address;
        }

        $app = JFactory::getApplication();
        $menu  = $app->getMenu();

        $active = $menu->getActive();
        
        /*
            If $active exists, the link is using a search engine friendly (SEF) url, such as:
            /shop

            If $active is null, then the link is using a direct path, such as:
            /index.php?option=com_eshop

        */

        $address = new stdClass();

        if ($active) {
            $address->option = $active->query['option'];
            $address->view = $active->query['view'];
            $address->task = $active->query['task'] ?? null;
            $address->layout = $active->query['layout'] ?? null;
            $address->extension = $active->query['extension'] ?? null;
        } else {
            $address->option = JRequest::getVar('option');
            $address->view = JRequest::getVar('view');
            $address->task = JRequest::getVar('task');
            $address->layout = JRequest::getVar('layout');
            $address->extension = JRequest::getVar('extension');
        }

        $address->app = $app;

        self::$address = $address;

        return $address;
    }

    private static function isAdminSection() {

        //Check if the view is in the admin section.

        $app = JFactory::getApplication();

        if ($app->isAdmin()) {
            return true;
        } else {
            return false;
        }
    }

    private static function redirect($app = null, $admin_section = null) {

        //If the user is not allowed access to their destination, redirect the user to their new destination.

        if (!$app) {
            $app = JFactory::getApplication();
        }

        if (!$admin_section) {
            $admin_section = self::isAdminSection();
        }

        if ($admin_section) {
            $access = $_SESSION['admin_access_levels'];

            if (isset($access->homepage) && $access->homepage) {
                //A homepage is assigned.
                $homepage = $access->homepage;
                if($homepage->option) {
                    $redirect = "/administrator/index.php?option=com_axs&view=splashpages";
                } else {
                    $redirect = "/administrator/index.php?option=com_axs&view=splashpages";
                }
                
                /* if ($homepage->view) {
                    $views = explode(",", $homepage->view);
                    $redirect .= "&view=" . $views[0];
                } */
            } else {
                //No home page is assigned to send them to the main page.
                //If they have access to the dashboard, they'll be redirected to it.
                $redirect = "/administrator/index.php?option=com_axs&view=splashpages";
            }

            //$_SESSION['viiiisits']++;
            //var_dump($redirect, $_SESSION['viiiisits']);

            $app->redirect($redirect);
        } else {
            $app->redirect("/");
            //$app->redirect(404);
        }
    }
}
