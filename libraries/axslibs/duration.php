<?php

defined('_JEXEC') or die;

/**
 * Duration Class
 */

class AxsDuration {

    /**
     * Get the duration type to enter into the database
     *
     * @param object $data
     *
     * @return string $type
     *
     */
    public static function getDurationType($data) {
        switch($data->view) {
            case "course":
                $type = "course";
            break;
            case "lesson":
                $type = "lesson";
            break;
            default:
                $type = "site";
            break;
        }
        return $type;
    }

    /**
     * Get the duration row for user if one exists
     *
     * @param object $data
     *
     * @return object $row
     *
     */
    public static function getDurationRow($data) {
        $db = JFactory::getDbo();
        if($data->user_id) {
            $conditions[] = "user_id = ".$db->quote($data->user_id);
        }
        if($data->type) {
            $conditions[] = "type = ".$db->quote($data->type);
        }
        if($data->item_id) {
            $conditions[] = "item_id = ".$db->quote($data->item_id);
        }
        if($data->date && $data->type == "site") {
            $conditions[] = "DATE(date) = DATE(".$db->quote($data->date).")";
        }
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_duration')
              ->where($conditions)
              ->setLimit(1);
        $db->setQuery($query);
        $row = $db->loadObject();
        return $row;
    }

    /**
     * Get the duration span for user
     *
     * @param object $data
     *
     * @return object $duration
     *
     */
    public static function getDurationSpan($data) {
        $db = JFactory::getDbo();
        if($data->user_id) {
            $conditions[] = "user_id = ".$db->quote($data->user_id);
        }
        if($data->type) {
            $conditions[] = "type = ".$db->quote($data->type);
        }
        if($data->item_id) {
            $conditions[] = "item_id = ".$db->quote($data->item_id);
        }
        if($data->start && $data->end) {
            $conditions[] = "DATE(date) >= DATE(".$db->quote($data->start).") AND DATE(date) <= DATE(".$db->quote($data->end).")";
        }
        $query = $db->getQuery(true);
        if($data->duration_total) {
            $query->select('SUM(duration) AS total');
            $query->from('axs_duration');
            $query->where($conditions);
            $query->limit(1);
            $db->setQuery($query);
            $result = $db->loadObject();
        } else {
            $query->select('*');
            $query->from('axs_duration');
            $query->where($conditions);
            $db->setQuery($query);
            $results = $db->loadObjectList();
            return $results;
        }
        if($result) {
            if($data->get_minutes) {
                $duration = gmdate('G.i',$result->total);
            } else {
                $duration = round( ($result->total / 60) / 60, 2);
            }

        } else {
            $duration = '0.00';
        }
        return $duration;
    }
}