<?php

defined('_JEXEC') or die;

class AxsTopBar {

    /**
     * Renders the Top bar 
     * 
     * @return void
     * 
     */
    public static function renderTopBar() {

        ob_start(); 
        ?>
        <div class="tov-header container-fluid d-flex">
            <div class="tov-logo d-flex align-items-center justify-content-center p-2">
                <a href="/administrator/index.php?option=com_axs&view=splashpages">
                    <img src="/administrator/templates/isis3/img/logo.svg" />
                </a>
            </div>
            <?php echo self::renderSearch() ?>
            
            <?php echo self::renderStarredItems() ?>

            <div class="tov-topbar-right d-flex ms-auto">
                
                <?php // echo self::renderNotifications() Notifications is disabled for now, it will be built out in the future ?>

                <?php echo self::renderTrialButton() ?>
                
                <?php echo self::renderUserAvatarDropdown() ?>

            </div>
        <?php
        return ob_get_clean();
    }

    public static function addStarredItem($category, $text, $uri) {
        $db	= JFactory::getDbo();

        $newRow = new stdClass();
        $newRow->user_id = JFactory::getUser()->id;
        $newRow->category = $category;
        $newRow->uri = $uri;
        $newRow->text = $text;
        
        $db->insertObject('axs_starred', $newRow);

        return $db->insertId();
    }

    public static function removeStarredItem($item_id) {
        $db	= JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->delete($db->quoteName('axs_starred'));
        $query->where(array(
            "id = " . $db->quote($item_id),
            "user_id = " . $db->quote(JFactory::getUser()->id)
        ));
        $db->setQuery($query);

        return $db->execute();
    }

    public static function getStarredId() {
        $db	= JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from('axs_starred');
        $query->where(array(
            "user_id = " . $db->quote(JFactory::getUser()->id),
            "uri = " . $db->quote($_SERVER['REQUEST_URI'])
        ));
        $db->setQuery($query);
        $res = $db->loadObject();

        return $res->id;
    }

    // ---- PRIVATE METHODS ----
    private static function getStarredItems() {
        $db	= JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_starred');
        $query->where(array(
            "user_id = " . $db->quote(JFactory::getUser()->id),
        ));
        $query->order("text");
        $db->setQuery($query);
        $res = $db->loadObjectList();

        $categorized = array('All' => array());
        foreach ($res as $row) {
            if (!isset($categorized[$row->category])) {
                $categorized[$row->category] = array();
            }
            $categorized[$row->category] []= $row;
            $categorized['All'] []= $row;
        }

        return $categorized;
    }

    private static function renderSearch() {
        ob_start(); 
        ?>
            <div class="tov-search d-flex align-items-center">
                <input class="form-control" autocomplete="off" id="searchDropdown" type="text" class="form-control" placeholder="Search..." aria-label="Search" aria-describedby="basic-addon2">
                <i class="fas fa-search"></i>
            </div>
            
            <script src="/libraries/axslibs/topnav/tov-search.js?v=2"></script>
            <script>
			    const menuItems = <?php echo json_encode(AxsSideNav::getMenuItemsForSearch()); ?>;
                autocomplete(document.getElementById("searchDropdown"), menuItems);
            </script> 

        <?php
        return ob_get_clean();
    }

    private static function renderTrialButton() {
        $trialExpiration = AxsClientData::getClientExpiration();
        $clientStatus = AxsClientData::getClientStatus();
        $trial = false;
        if ($trialExpiration && $clientStatus == 'trial') {
            if (strtotime($trialExpiration) < strtotime('2021-10-11')) {
                $trialExpiration = '2021-10-11';
            }
            $today = date("Y-m-d");
            $expires = date("Y-m-d", strtotime($trialExpiration));
            $now = date_create($today);
            $expire_date = date_create($expires);
            $diff = date_diff($expire_date,$now);
            $daysLeft = $diff->days;
            if ($diff->invert != 1) {
                $daysLeft = '-' . $daysLeft;
            }
            if ($daysLeft == 1) {
                $daysText = "Day";
            } else {
                $daysText = "Days";
            }
            $trial = true;
        }

        ob_start(); 
        ?>
            <?php if ($trial): ?>
                <div class="tov-trial d-flex align-items-center d-none d-md-flex ms-md-2">
                    <p class="d-none d-lg-block text-light mb-0 me-2">Trial Expires in <?php echo $daysLeft; ?> <?php echo $daysText; ?></p>
                    <button id="trialToastBtn" class="btn btn-success d-none d-md-block" type="button">Upgrade Now</button>
                    <script>
                        jQuery("#trialToastBtn").click(function() {
                            jQuery.ajax({
                                url  : '/index.php?option=com_axs&task=trials.trialUpgradeRequest&format=raw',
                                type : 'POST'
                            }).done(function(response) {
                                var toastElList = [].slice.call(document.querySelectorAll('.toast'))
                                var toastList = toastElList.map(function (toastEl) {
                                    // Creates an array of toasts (it only initializes them)
                                    return new bootstrap.Toast(toastEl) // No need for options; use the default options
                                });
                                toastList.forEach(toast => toast.show()); // This show them
                            });
                        });
                    </script>
                </div>
            <?php endif; ?>
        <?php
        return ob_get_clean();
    }

    private static function renderNotifications() {
        ob_start(); 
        ?>
        <div class="tov-notifications d-none d-md-flex align-items-center ms-2">
            <button class="btn btn-secondary" type="button" id="notifications-dropdown" data-bs-toggle="dropdown" aria-expanded="false"><i class="fas fa-bell me-0"></i></button>
            <ul class="dropdown-menu" aria-labelledby="notifications-dropdown">
                <li><a class="dropdown-item" href="#">Action</a></li>
                <li><a class="dropdown-item" href="#">Another action</a></li>
                <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }

    private static function renderUserAvatarDropdown() {
        ob_start(); 
        ?>
            <div id="adminAvatarDropdown" class="tov-user align-items-center ms-2 me-1 me-lg-2 mt-lg-1">
				<div class="tov-user-details d-flex align-items-center" type="button" id="user-details-dropdown" data-bs-toggle="dropdown" aria-expanded="false">
					<p class="text-white d-none d-xl-block mb-0 me-2"><?php echo JFactory::getUser()->name ?></p>
					<img src="<?php echo AxsUser::getAvatar() ?>" class="rounded">
				</div>
				<ul class="dropdown-menu mt-2" aria-labelledby="user-details-dropdown">
					<li>
						<a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_users&view=axsuser&layout=edit&id=' . JFactory::getUser()->id) ?>">
							<i class="fas fa-user"></i>
							My Profile
						</a>
					</li>
					<li>
						<a class="dropdown-item" href="/" target="_blank">
							<i class="fas fa-eye"></i>
							User Portal
						</a>
					</li>
					<li>
						<a class="dropdown-item" href="https://tovuti.zendesk.com/hc/en-us" target="_blank">
							<i class="fas fa-question-circle"></i>
							Help
							<i class="fas fa-external-link-alt float-end mt-1"></i>
						</a>
					</li>
					<li>
						<a class="dropdown-item" href="<?php echo JRoute::_('index.php?option=com_login&task=logout&' . JSession::getFormToken() . '=1') ?>">
							<i class="fas fa-sign-out"></i>
							Sign Out
						</a>
					</li>
                    <?php
                        $filename = "/var/www/html/version.txt";
                        $handle = fopen($filename, "r");
                        $version_number = fread($handle, filesize($filename));
                        fclose($handle);
                    ?>
                    <li class="dropdown-item disabled">
                        Version <?php echo $version_number; ?>
                    </li>
				</ul>
			</div>
        <?php
        return ob_get_clean();
    }


    public static function renderStarredItems() {
        $starredItems = self::getStarredItems();

        ob_start(); 
        ?>
            <div class="tov-starred d-flex align-items-center ms-2 me-1 me-md-0">
                <button class="btn btn-warning d-inline-flex align-items-center" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" data-popper-placement="bottom-start" data-bs-auto-close="outside" data-bs-offset="0,9">
                    <i class="fas fa-star"></i> Starred
                </button>
                <ul class="dropdown-menu" style="max-height:80vh">
                    <div class="starred-filter">
                        <ul class="nav nav-pills" id="starred-filters" role="tablist">
                            <?php if (count($starredItems) == 0): ?>
                                <li class="nav-item">
                                    0 Starred Items
                                </li>
                            <?php endif; ?>
                            <?php foreach ($starredItems as $category => $items): ?>
                                <?php $categoryForAttrs = strtolower(str_replace(' ', '-', $category)); ?>
                                <li class="nav-item">
                                    <a class="nav-link" onclick="window.localStorage.setItem('lastActiveStarredCategory', '<?php echo $categoryForAttrs ?>')" id="<?php echo $categoryForAttrs; ?>-tab" data-bs-toggle="tab" data-bs-target="#<?php echo $categoryForAttrs; ?>" type="button" role="tab" aria-controls="<?php echo $categoryForAttrs; ?>" aria-selected="false"><i class="<?php echo AxsSideNav::getIcon($category) ?>"></i></a>
                                </li>
                            <?php endforeach; ?>
                            <script>
                                jQuery(() => {
                                    var lastActiveStarredCategory = window.localStorage.getItem('lastActiveStarredCategory');
                                    if (lastActiveStarredCategory !== null) {
                                        var someTabTriggerEl = document.querySelector('#' + lastActiveStarredCategory + '-tab');
                                        var tab = new bootstrap.Tab(someTabTriggerEl);
                                        tab.show();
                                    } else {
                                        var tab = new bootstrap.Tab(jQuery('#starred-filters').find('.nav-link').first());
                                        tab.show();
                                    }
                                });
                            </script>
                        </ul>
                    </div>
                    <li>
                        <hr class="dropdown-divider">
                    </li>
                    <div class="starred-results tab-content" id="myTabContent">
                        <?php foreach ($starredItems as $category => $items): ?>
                            <?php $categoryForAttrs = strtolower(str_replace(' ', '-', $category)); ?>
                            <div class="tab-pane fade show" id="<?php echo $categoryForAttrs; ?>" role="tabpanel" aria-labelledby="<?php echo $categoryForAttrs; ?>-tab" style="max-height:80vh; overflow-y:scroll">
                                <h4><?php echo $category ?></h4>

                                <?php foreach($items as $item): ?>
                                    <div style="cursor:pointer" class="starred-item" data-item-id="<?php echo $item->id ?>" <?php echo ($category != 'All') ? 'data-item-category="' . $categoryForAttrs . '"' : '' ?>  onclick="window.location.replace('<?php echo $item->uri ?>')">
                                        <div class="starred-left">
                                            <h5><?php echo $item->text; ?></h5>
                                            <div class="starred-details">
                                                <?php echo ($category == 'All') ? $item->category . ' |' : '' ?>
                                                <span role="button" onclick="TovutiStarred.removeStarredItem(event, <?php echo $item->id ?>)" style="text-decoration:underline">Remove</span>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-light"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </ul>
            </div>
        <?php
        return ob_get_clean();
    }

}
