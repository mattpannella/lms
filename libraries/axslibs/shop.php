<?php

defined('_JEXEC') or die;

class AxsShop {

    private $shop;
    
    public function __construct($shop) {
        $this->shop      = $shop;
        $stock = (array) json_decode($shop->stock);
        $this->stock = array_values($stock);
    }

    public function setSurvey($survey) {
        $this->survey = $survey;
    }

    public function getsurvey() {
        return $this->survey;
    }

    public function getTemplate() {
        include 'components/com_axs/templates/shop.php';
    }

}