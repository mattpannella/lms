<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

class AxsChecklist {

	public static function getChecklist($checklist_id) {
		if(!$checklist_id) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = "id = ".(int)$checklist_id;
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from("axs_checklists")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public static function getAllUsers() {
		$db = JFactory::getDBO();
		$conditions[] = "block = 0";
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("#__users")
			  ->where($conditions);
		$db->setQuery($query);
		$result = $db->loadColumn();
		return $result;
	}

	public static function getChecklistUsers($checklist) {
		$users = array();
		switch ($checklist->access_type) {
			case 'none':
				$users = self::getAllUsers();
			break;

			case 'access':
				$usersAccess = self::getAllUsers();
				$accessLevels = explode(',',$checklist->accesslevel);
				foreach($usersAccess as $user) {
					$levels = JAccess::getAuthorisedViewLevels($user);
					foreach($accessLevels as $level) {
						if(in_array($level, $levels)) {
							array_push($users,$user);
						}
					}
				}
			break;

			case 'user':
				$usersGroups = self::getAllUsers();
				$userGroupList = explode(',',$checklist->usergroup);
				foreach($usersGroups as $user) {
					$groups = JAccess::getGroupsByUser($user);
					foreach($userGroupList as $group) {
						if(in_array($group, $groups)) {
							array_push($users,$user);
						}
					}
				}
			break;

			case 'list':
				$users = explode(',',$checklist->userlist);
			break;

			default:
				$users = self::getAllUsers();
			break;
		}
		return $users;
	}

	public static function buildQuery($userId) {
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		$levels = JAccess::getAuthorisedViewLevels($userId);
		$groups = JAccess::getGroupsByUser($userId);
		$levelsCheck = [];
		$groupsCheck = [];
		$conditions  = [];

		foreach($levels as $level) {
			$levelsCheck[] = " ( FIND_IN_SET( $level, accesslevel ) > 0 ) ";
		}

		foreach($groups as $group) {
			$groupsCheck[] = " ( FIND_IN_SET( $group, usergroup ) > 0 ) ";
		}

		$levelsSplit = implode(' OR ', $levelsCheck);
		$groupsSplit = implode(' OR ', $groupsCheck);
		$where   = " WHERE enabled = 1 ";

		//$where  .= " AND   DATE(start_date) <= NOW() ";
		//$where  .= " AND   ( DATE(end_date) > NOW() OR end_date = 0 ) ";
		$conditions[] = "  ( access_type = 'access' AND ( $levelsSplit ) ) ";
		$conditions[] = "  ( access_type = 'user'   AND ( $groupsSplit ) ) ";
		$conditions[] = "  ( access_type = 'list'   AND ( FIND_IN_SET( $userId, userlist ) > 0 ) ) ";
		$conditions[] = "  ( access_type = 'none' ) ";

		$conditionsSplit = implode(' OR ', $conditions);

		$where .= ' AND ('.$conditionsSplit.')';

		$query = "SELECT * FROM axs_checklists $where ORDER BY id DESC";

		return $query;

	}

	public static function checkCourseCompletion($item, $userId) {
		$db = JFactory::getDBO();
		$conditions[] = "course_id = ".(int)$item->course_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "progress = 100";
		$conditions[] = "archive_date IS NULL";
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("#__splms_course_progress")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkLessonCompletion($item,$userId) {
		$db = JFactory::getDBO();
		$conditions[] = "lesson_id = ".(int)$item->lesson_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "status = 'completed'";
		$conditions[] = "archive_date IS NULL";
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("#__splms_lesson_status")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkInteractiveContentCompletion($item,$userId) {
		$db = JFactory::getDBO();
		$conditions = array('archive_date IS NULL');
		$conditions[] = "student_response = ".$db->quote($item->interactive_id);
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "completed = 1";
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("#__splms_student_activities")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkAward($item,$userId) {
		$db = JFactory::getDBO();
		$conditions[] = "badge_id = ".(int)$item->award_id;
		$conditions[] = "user_id = ".(int)$userId;
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from("axs_awards_earned")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkEvent($item,$userId) {
		$db = JFactory::getDBO();
		$conditions[] = "event_id = ".(int)$item->event_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "payment_status = 1";
		if($item->type == 'event_checkin') {
			$conditions[] = "checked_in = 1";
		}
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("#__eb_registrants")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkWatchedVideo($item,$userId) {
		$db = JFactory::getDBO();
		$conditions = array('archive_date IS NULL');
		$conditions[] = "media_id = ".(int)$item->media_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "completed = 1";
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("axs_video_tracking")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkIfExists($item,$userId) {
		$db = JFactory::getDBO();
		$conditions = array('archive_date IS NULL');
		$conditions[] = "checklist_id = ".(int)$item->checklist_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "item_id = ".$db->quote($item->id);
		$conditions[] = "item_type = ".$db->quote($item->type);
		$conditions[] = "completed = 1";

		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("axs_checklist_activity")
			  ->where($conditions)
			  ->setLimit(1);

        $db->setQuery($query);

		$result = $db->loadRow();

		if($result) {
			return true;
		} else {
			return false;
		}
	}

	public static function checkIfExistsMap($items,$userId) {

		$itemIds = [];
		foreach($items as $item) {
			$itemIds []= "'" . $item->id . "'";
		}
		$itemIds = implode(',', $itemIds);

		$db = JFactory::getDBO();
		$conditions = array('archive_date IS NULL');
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "item_id IN (" . $itemIds .")";
		$conditions[] = "completed = 1";

		$query = $db->getQuery(true);
		$query->select('id, item_id')
			  ->from("axs_checklist_activity")
			  ->where($conditions);

        $db->setQuery($query);

		$result = $db->loadAssocList('item_id');

		return $result;
	}

	public static function addChecklistActivity($item,$userId) {

        $exists = self::checkIfExists($item,$userId);

		if($exists || !$userId) {
			return false;
		}

        $db = JFactory::getDBO();
		$result = $db->insertObject("axs_checklist_activity",$item);
		$activity = json_decode($item->params);

        if($activity->points_category && $activity->points) {
            $awardData = new stdClass();
            $awardData->user_id = $userId;
            $awardData->category_id = $activity->points_category;
            $awardData->points = $activity->points;
            $awardData->date = date("Y-m-d H:i:s");
            AxsAwards::awardPoints($awardData);
        }

        return $result;
	}

	public static function removeChecklistActivity($item,$userId) {
		if(!$userId) {
			return false;
		}
		$db = JFactory::getDBO();
		$conditions[] = "checklist_id = ".(int)$item->checklist_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "item_id = ".$db->quote($item->item_id);
		$conditions[] = "item_type = ".$db->quote($item->item_type);
		$query = $db->getQuery(true);
		$query->delete("axs_checklist_activity")
			  ->where($conditions);
		$query = $query.' LIMIT 1';
		$db->setQuery($query);
		$result = $db->execute();
		$activity = json_decode($item->params);
		if($activity->points_category && $activity->points) {
            $awardData = new stdClass();
            $awardData->user_id = $userId;
            $awardData->category_id = $activity->points_category;
            $awardData->points = $activity->points;
            $awardData->date = date("Y-m-d H:i:s");
            AxsAwards::removePoints($awardData);
        }
		return $result;
	}

	public static function getChecklistActivity($item,$userId) {
		$db = JFactory::getDBO();
		$conditions = array('archive_date IS NULL');
		$conditions[] = "checklist_id = ".(int)$item->checklist_id;
		$conditions[] = "user_id = ".(int)$userId;
		$conditions[] = "item_id = ".$db->quote($item->id);
		$conditions[] = "item_type = ".$db->quote($item->type);
		$query = $db->getQuery(true);
		$query->select('id')
			  ->from("axs_checklist_activity")
			  ->where($conditions)
			  ->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
	}


	public static function checkItem($item, $userId) {
		$result = false;
		switch ($item->type) {
			case 'course_completion':
				$result = self::checkCourseCompletion($item,$userId);
			break;

			case 'lesson_completion':
				$result = self::checkLessonCompletion($item,$userId);
			break;

			case 'certificate_awarded':
				$item->award_id = $item->certificate_id;
				$result = self::checkAward($item,$userId);
			break;

			case 'badge_awarded':
				$item->award_id = $item->badge_id;
				$result = self::checkAward($item,$userId);
			break;

			case 'event_registration':
				$result = self::checkEvent($item,$userId);
			break;

			case 'event_checkin':
				$result = self::checkEvent($item,$userId);
			break;

			case 'watched_video':
				$result = self::checkWatchedVideo($item,$userId);
			break;
		}
		return $result;
	}

    /**
     * Gets the total amount of checklist items for a given user by type and filtered by
     * the ID of the item type (courseId for course items, lessonId, for lesson items, etc.)
     * 
     *
     * @param int $userId
     * @param int $itemTypeId
     * @param string $itemType
     * @return Array Array of checklist items for a user filtered by the given params
     */
    public static function getChecklistItemsForUserByType(int $userId, int $itemTypeId, string $itemType) {

        if(empty($userId) || is_null($itemTypeId)) {
            return;
        }

        $checklists = self::getUserChecklists($userId);
        $checklistItems = [];

        foreach($checklists as $checklist) {

            if(!is_null($checklist->params)) {

                $checklistParams = json_decode($checklist->params);
                $items = json_decode($checklistParams->items);

                foreach($items as $item) {

                    if(isset($item->completed) && $item->completed) {

                       continue; 
                    }

                    $newItem = new stdClass();

                    $newItem->checklist_id = $checklist->id;
                    $newItem->item_id = $item->id;
                    $newItem->user_id = $userId;
                    $newItem->item_type = $item->type;
                    $newItem->params = json_encode($item);
                    $newItem->date = date('Y-m-d H:i:s');

                    switch ($itemType) {
                        case 'course_completion':
                            if($item->course_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;
            
                        case 'lesson_completion':
                            if($item->lesson_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;
            
                        case 'certificate_awarded':
                            if($item->certificate_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;
            
                        case 'badge_awarded':
                            if($item->badge_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;
            
                        case 'event_registration':
                        case 'event_checkin':
                            
                            if($item->event_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;
            
                        case 'watched_video':
                            
                            if($item->video_id == $itemTypeId && $newItem->item_type == $itemType) {

                                $checklistItems[] = $newItem;
                            }
                        break;

                        default: break;
                    }
                }                
            }            
        }

        return $checklistItems;
    }

	public static function getUserChecklists($userId) {
		$db = JFactory::getDBO();
		$query = self::buildQuery($userId);
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return $results;
	}

	public static function updateAllChecklistsForUser($userId) {

        if(!$userId) {
			$userId = JFactory::getUser()->id;
		}

        $db = JFactory::getDBO();

        $checklists = self::getUserChecklists($userId);

		foreach($checklists as $checklist) {

			$params = json_decode($checklist->params);
			$items  = json_decode($params->items);

			$activity = new stdClass();
			$activity->checklist_id = $checklist->id;
			$activity->user_id = $userId;

            $allItemsComplete = true;

            foreach($items as $item) {

				$item->checklist_id = $checklist->id;
				$inChecklistActivityTable = self::checkIfExists($item, $userId);

                /* If the current checklist item does not already exist in the axs_checklist_activity table,
                 * check the item for completion. If the item is an autocomplete item, mark it as completed
                 * by creating a record of the data in the axs_checklist_activity table.
                 */
                if(!$inChecklistActivityTable) {

                    $isCompleted = self::checkItem($item, $userId);

                    if(!$isCompleted) {
                        
                        $allItemsComplete = false;
                    } else if($item->completion_type == 'auto') {

                        // Create a row in the axs_checklist_activity table and award points if necessary
                        $activity->item_type = $item->type;
                        $activity->item_id = $item->id;
                        $activity->completed = 1;
                        $activity->date = date("Y-m-d H:i:s");
                        $activity->params = json_encode($item);
                        $db->insertObject("axs_checklist_activity",$activity);

                        if($item->points_category && $item->points) {
                            $awardData = new stdClass();
                            $awardData->user_id = $userId;
                            $awardData->category_id = $item->points_category;
                            $awardData->points = $item->points;
                            $awardData->date = date("Y-m-d H:i:s");
                            AxsAwards::awardPoints($awardData);
                        }
                    }
                }
			}

            if ($allItemsComplete && $params->notify_admin == "1") {

                // Check if an email has been sent for this user and this checklist already
                AxsNotifications::sendChecklistCompletedAdminEmail($userId, $checklist);
            }
		}
	}

    public static function isUserChecklistComplete($checklistId, $userId) {

		$checklist = self::getChecklist($checklistId);

        $params = json_decode($checklist->params);
        $items  = json_decode($params->items);
        
        $allItemsComplete = true;

        // Check each item for completion - if one item is incomplete, the whole checklist is incomplete.
        foreach($items as $item) {

            $isCompleted = self::checkIfExists($item, $userId);
            
            if (!$isCompleted) {

                $allItemsComplete = false;
                break;
            }
        }

        return $allItemsComplete;
    }

	public static function getDynamicDueDate($dynamicParams) {
		if(!$dynamicParams->user_id) {
			return false;
		}
		if(!$dynamicParams->groups || !$dynamicParams->amount || !$dynamicParams->unit) {
			return false;
		}

		$db = JFactory::getDbo();
		$groups = implode(',',$db->quote($dynamicParams->groups));
		$conditions[] = $db->quoteName('user_id').' = '.$db->quote($dynamicParams->user_id);
		$conditions[] = $db->quoteName('group_id').' IN ('.$groups.')';
		$conditions[] = $db->quoteName('action').' = '.$db->quote('add');

		$query = $db->getQuery(true);
		$query->select('MIN(date) as start_date');
		$query->from($db->quoteName('axs_usergroup_tracking'));
		$query->where($conditions);
		$query->setLimit(1);
        $db->setQuery($query);
		$result = $db->loadObject();

		$due_date = gmdate('m/d',strtotime($result->start_date.' + '.$dynamicParams->amount .' '. $dynamicParams->unit));
		$dueDateParams = new stdClass();
		$dueDateParams->due_date = $due_date;
		$dueDateParams->start_date = $result->start_date;

		return $dueDateParams;
	}

	public static function getChecklistDueDate($userId,$checklist) {
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}

		$params = json_decode($checklist->params);
		$dynamicParams = new stdClass();
		$dynamicParams->user_id = $userId;
		$dynamicParams->amount = $params->checklist_due_dynamic_number;
		$dynamicParams->unit = $params->checklist_due_dynamic_unit;
		$dynamicParams->groups = explode(',',$checklist->usergroup);
		$dueDateParams = new stdClass();

		switch ($params->due_date_type) {
			case 'none':
				$dueDateParams->due_date = false;
			break;

			case 'dynamic':
				if($dynamicParams->user_id && $checklist->usergroup && $dynamicParams->amount && $dynamicParams->unit) {

					$dueDateParams = self::getDynamicDueDate($dynamicParams);
				} else {

					$dueDateParams->due_date = false;
				}
			break;

			case 'fixed':
            default:
				$dueDateParams->due_date = $checklist->due_date;
			break;
		}

		return $dueDateParams;
	}

	public static function getChecklistItemDueDate($userId, $item) {
        if(!$userId) {
			$userId = JFactory::getUser()->id;
		}

		switch ($item->due_date_type) {
			case 'none':
				$due_date = false;
			break;

			case 'dynamic':
				if(!$item->item_due_dynamic_usergroup || !$item->item_due_dynamic_number || !$item->item_due_dynamic_unit) {
					return false;
				}

				$dynamicParams = new stdClass();
				$dynamicParams->user_id = $userId;
				$dynamicParams->amount = $item->item_due_dynamic_number;
				$dynamicParams->unit = $item->item_due_dynamic_unit;
				$dynamicParams->groups = explode(',',$item->item_due_dynamic_usergroup);

				if($dynamicParams->user_id && $dynamicParams->groups && $dynamicParams->amount && $dynamicParams->unit) {

                    $dynamicDueDate = self::getDynamicDueDate($dynamicParams);

                    $due_date = $dynamicDueDate->due_date;
				} else {
					$due_date = false;
				}
			break;

			case 'fixed':
            default:
				$due_date = $item->due_date;
			break;
		}

		return $due_date;
	}

	public static function getUserChecklistProgress($userId,$checklist) {
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		if(!is_object($checklist)){
			$checklist = self::getChecklist($checklist);
		}
		$totalItems    = 0;
		$totalComplete = 0;
		$params = json_decode($checklist->params);
		$items  = json_decode($params->items);
		foreach($items as $item) {
			$totalItems++;
			$item->checklist_id = $checklist->id;
			$isComplete = self::checkIfExists($item,$userId);
			if($isComplete) {
				$totalComplete++;
			}
		}
		$percentage = round(($totalComplete / $totalItems) * 100);
		return $percentage;
	}

	public static function buildUserChecklistHTML($userId,$is_admin = false,$params = null) {
		$html = '';
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		$userChecklists = self::getUserChecklists($userId);
		$filters = new stdClass();
		$filters->course_completion = 'courses';
		$filters->lesson_completion = 'courses';
		$filters->certificate_awarded = 'awards';
		$filters->badge_awarded = 'awards';
		$filters->event_registration = 'events';
		$filters->event_checkin = 'events';
		$filters->watched_video = 'videos';
		$filters->custom = 'other';

		$key = AxsKeys::getKey('lms');
		include 'components/com_axs/templates/checklist_container.php';
	}
}
