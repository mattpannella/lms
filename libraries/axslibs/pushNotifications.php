<?php

defined('_JEXEC') or die;
/*
 <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
    <script>
      var OneSignal = window.OneSignal || [];
      OneSignal.push(function() {
        OneSignal.init({
          appId: "fb97506d-d6d8-4d2b-8984-8d583ea69fcb",
        });
      });
    </script>
    <?php AxsPushNotifications::sendMessage(); ?>
*/
class AxsPushNotifications {

	public function sendMessage($message) {
	    $content      = array(
	        "en" => $message
	    );

	    $headings      = array(
	        "en" => 'Tovuti Virtual Classroom'
	    );
	    $hashes_array = array();
	    array_push($hashes_array, array(
	        "id"   => "like-button",
	        "text" => "Click Here To Join Meeting",
	        "url"  => "https://development.tovuti.io/join-virtual-classroom?vcid=TldGaE1ESXdOV1JsT1RFek9EZzVZek5rTWpCa01EQXdPVGM1TkRWaE5USXdNVEV6WmpaallqQTFPV1F3TldZNU1EVXhZek13WlRReU5USTVORE00TURwUVFVeG1RVzVLYUhwWFNsTlNhMkZzY25SRksyd3dRa0ZFZDFSaVVrOHZRVXhMSzBac2ExRmxaVGRyTWpaelEyNUNUbHBDWVdGWFdsRjNSVXhvWjFoMFJHRjVUR3B5YmxWNk1sSXljM0JMVEVWbWRGQnpjR1V4TVZST2VVMVFUV2xLYlRCTGNVNVhiemxDYlVkVFZqSjJlU3QwS3pKSFEya3dhelJKYW1NNE16bHpVbU00YUZkWVdYTm5TbUZsUW5JemJETXdPVXN5TlcxU2VXRmpNVkpCWVdOdU4zSk5iR0ZrZW14WU5sQnRSRFZ6YlRSd01IaEhkVUppYlhSQmNIWT0="
	    ));
	    $fields = array(
	        'app_id' => "fb97506d-d6d8-4d2b-8984-8d583ea69fcb",
	        'included_segments' => array(
	            'All'
	        ),
	        'data' => array(
	            "foo" => "bar"
	        ),
	        'contents' => $content,
	        'headings' => $headings,
	        'web_buttons' => $hashes_array,
	        'chrome_web_icon' => "https://development.tovuti.io/images/SWltRlRIMGZIb2xCcWZEVlFLWXc2QT09OjI0NzM/community/avatar/3a966b907231fd1abbfe2a06.jpg"
	    );
	    
	    $fields = json_encode($fields);
	    
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	        'Content-Type: application/json; charset=utf-8',
	        'Authorization: Basic MWRkODgyZjYtYWFkMi00ZTI1LWJiNzYtNGFlYzkzMjVkZGU5'
	    ));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	    
	    $response = curl_exec($ch);
	    curl_close($ch);
	    
	    return $response;
	}
		
}