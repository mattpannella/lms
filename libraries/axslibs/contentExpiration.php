<?php

defined('_JEXEC') or die;

class AxsContentExpiration {

    public $id;
    public $content_type;
    public $content_id;
    public $title;
    public $custom_id;
    public $expiration_date;
    public $data;
    public $params;

    public function __construct($params) {
        $this->content_type    = $params->content_type;
        $this->content_id      = $params->content_id;
        $this->title           = $params->title;
        $this->custom_id       = $params->custom_id;
        $this->expiration_date = $params->expiration_date;
        $this->data            = $params->data;
        $this->params          = $params->params;
    }

    public function store() {
        $db    = JFactory::getDbo();
        $table = 'axs_content_expiration';
        $key   = 'id';
        $data  = $this->getData();
        if($data->id) {
            $db->updateObject($table,$data,$key);
        } else {
            $db->insertObject($table,$data);
        }
    }

    public function getData() {
        $row = $this->getRow();
        if($row) {
            $this->id = $row->id;
        }
        $data = new stdClass();
        $data->id              = $this->id;
        $data->content_type    = $this->content_type;
        $data->content_id      = $this->content_id;
        $data->title           = $this->title;
        $data->custom_id       = $this->custom_id;
        $data->expiration_date = $this->expiration_date;
        $data->data            = $this->data;
        $data->params          = $this->params;
        return $data;
    }

    public function getRow() {
        if(!$this->content_type || !$this->content_id) {
            return false;
        }
        $db           = JFactory::getDbo();
        $query        = $db->getQuery(true);
        $conditions[] = $db->quoteName('content_type').'='.$db->quote($this->content_type);
        $conditions[] = $db->quoteName('content_id').'='.$db->quote($this->content_id);
        $query->select('*');
        $query->from('axs_content_expiration');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $row = $db->loadObject();
        return $row;
    }

    public function getStartEndDateRangeConditions($start_date,$end_date,$field) {
        if($start_date < 1 && $end_date < 1 ) {
            return '';
        }

        if($start_date > 0) {
            $startCondition = "DATE($field) >= DATE('$start_date')";
        }

        if($end_date > 0) {
            $connector = "";
            if($start_date > 0) {
                $connector = " AND ";
            }
            $endCondition = "$connector DATE($field) <= DATE('$end_date')";
        }

		$dateRangeCondition = "( $startCondition $endCondition )";

		return $dateRangeCondition;
    }

    public function quoteFilterList($items) {
		$db = JFactory::getDbo();
		if(!is_array($items)) {
			$items = explode(',',$items);
		}
		foreach($items as $item) {
			$outputArray[] = $db->quote($item);
		}
		$output = implode(',',$outputArray);
		return $output;
	}

    public function getAllRows($params) {
        if(!$params->content_types) {
            return false;
        }
        $db           = JFactory::getDbo();
        $query        = $db->getQuery(true);
        $field        = 'expiration_date';
        $start_date   = $params->start_date;
        $end_date     = $params->end_date;

        $dateConditions = $this->getStartEndDateRangeConditions($start_date,$end_date,$field);
        if($dateConditions) {
            $conditions[] = $dateConditions;
        }
        $content_types = $this->quoteFilterList($params->content_types);
        $conditions[] = $db->quoteName('content_type')." IN ($content_types)";
        $query->select('content_type,title,custom_id,expiration_date');
        $query->from('axs_content_expiration');
        $query->where($conditions);
        $query->order('expiration_date ASC');
        $db->setQuery($query);
        $rows = $db->loadObjectList();
        return $rows;
    }

    public function delete() {
        if(!$this->content_type || !$this->content_id) {
            return false;
        }
        $db           = JFactory::getDbo();
        $query        = $db->getQuery(true);
        $conditions[] = $db->quoteName('content_type').'='.$db->quote($this->content_type);
        $conditions[] = $db->quoteName('content_id').'='.$db->quote($this->content_id);
        $query->delete('axs_content_expiration');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->execute();
        return $result;
    }
}