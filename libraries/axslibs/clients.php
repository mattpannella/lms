<?php

defined('_JEXEC') or die;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

//Library to manage the client / owner of the current site.

class AxsClients {

	/**
     * Returns the ID of the current client.
     *
     * @return  integer                          The ID of the client.
     */

	public static function getClientId() {
		$config = JFactory::getConfig();
		return $config->get('id');
	}

	public static function getClientDbName() {
		$config = JFactory::getConfig();
		return $config->get('db');
	}

	/**
	 * Returns the public IP address of the current site.
	 * @return string 		public IP address
	 */
	public static function getPublicIp() {
        $ip = str_replace("\n", "",shell_exec("dig +short myip.opendns.com @resolver1.opendns.com"));
        return $ip;
    }

    /**
     * Gets the 'encoded' path for the current client.  This is used to direct the system to their personal folder/files.
     * Note: After some time, it was decided that there was no reason to encode the client's id to make the path.  It is now
     * just a random string.
     *
     * @return string 			The client's encoded path.
     */
	public static function getClientEncode() {
		$config = JFactory::getConfig();
		return $config->get('encode');
	}

	/**
	 * Calls and returns the getClientEncode function
	 * @return type
	 */
	public static function getClientFolder() {
		return self::getClientEncode();
	}

	/**
	 * Creates a randomized (used to be encoded) file path for the client.
	 * @param integer $client_id 	The Client's ID.  Used to ensure the random path is unique.
	 * @return string 				The randomized string.
	 */
	public static function makeRandomFilePath($client_id) {

		$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$char_length = strlen($characters);

		//Something that will always be unique
		$code = strtotime('now') * $client_id;
		$encode = str_replace("=", "", base64_encode($code));

		$remain = 40 - strlen($encode);

		$string = "";
		for ($i = 0; $i < $remain; $i++) {
			$char = rand(0, $char_length - 1);
			$string .= substr($characters, $char, 1);
		}

		$string = $string . $encode;

		return $string;
	}

	/*
	With the update to the encryption algorythm, the encrypted path simply became too long.
	It went from 40 characters to 120.  The original idea was to encrypt the client's id so that the path could be used to determine the owner, if necessary.
	However, this turned out to never be necessary, so now it's just a random string.
	*/

	/*public static function encryptClientId($id) {
		//This should only be ever called once, upon client creation.
		//It will be different every time, which will not work for folder structure/names.

		if (!$id) {
			return null;
		}

		//Remove the '=' from the end of the string so that it is letters and numbers only.
		//The decrypt functionality will automatically add them back.

		$enc = AxsEncryption::encrypt($id, self::getPassword(), 2);
		$enc = str_replace("=", "", $enc);
		return $enc;
	}

	public static function decryptClientId($encrypted) {
		return AxsEncryption::decrypt($encrypted, self::getPassword());
	}*/

	private static function getPassword() {
		//This should NEVER change.  If it does, all of the encrypted params in axs_dbmanager need to be recreated with the new password
		return "@q6$~M_w?6pA\#9!";
	}

	/**
	 * Encrypt the clients Parameters into a single string.  This is used to be able to store their database username and password on the user's computer so that they don't have to retreive it from the main database everytime, which slows down site performance.
	 * @param string 	$dbname 	The database name
	 * @param string 	$dbuser 	The database's username
	 * @param string 	$dbpass 	The database's password
	 * @return string 				An encrypted string with the user's information.
	 */
	public static function encryptClientParams($dbname, $dbuser, $dbpass) {

		$data = new stdClass();
		$data->dbname = $dbname;
		$data->dbuser = $dbuser;
		$data->dbpass = $dbpass;

		return AxsEncryption::encrypt($data, self::getPassword());
	}

	/**
	 * Decrypts the a string containing the client's parameters.
	 * @param string 	$dbparams 	Encrypted object with the user's parambers
	 * @return object 	An object with the user's database information
	 */
	public static function decryptClientParams($dbparams) {
		return AxsEncryption::decrypt($dbparams, self::getPassword());
	}

	/**
	 * Creates an object with the parameters for the encrypted client parameters cookie.
	 * @return object 		The object with the parameters
	 */
	public static function getCookieParams() {
		$data = new stdClass();
		$data->key = AxsKeys::getKey("clients");
		$data->name = "sBz8EnrFpIBdH2fGBa1KcZkxE6lQadtN";

		return $data;
	}
}
