<?php

defined('_JEXEC') or die;

include_once("aws/AwsVolume.php");
include_once("aws/AwsInstance.php");
include_once("aws/AwsClient.php");

class AxsAmazonServer {

	public $instance = null;	
	public $volume = null;
	public $client_id = null;

	function __construct($params) {
		$type = gettype($params);
		if ($type == "array") {
			self::createAccount($params);
		}
	}

	/*
		$params = array(
			"dedicated" => false,
			"account_zone" => "us-west-2a",
			"instance_type" => "t2.micro",
			"volume_size" => 50,
			"volume_type" => "standard"
		);
	*/

	

	/*
	public function loadAccount($params) {

	}
	*/

	public function delete() {

	}

	public function changeInstanceType() {

	}

	public function changeVolumeSize() {

	}

	public function changeZone() {

	}

	public function getIp() {
		if (gettype($this->instance) == "object") {
			return $this->instance->getPublicIp();
		}
	}

	public function getInstance() {
		return $this->instance;
	}

	public function getVolume() {
		return $this->volume;
	}

	public static function newAccount($params) {
		$client = new AwsClient($params);		
	}

	public static function loadAccount() {

	}

	public static function getTemplate() {		
		return array(
			"dedicated" => false,
			"account_zone" => "us-west-2a",
			"instance_type" => "t2.micro",
			"volume_size" => 1000,
			"volume_type" => "standard"
		);
	}

	public static function getVolumeTypes() {
		return AwsVolume::getVolumeTypes();
	}

	public static function getInstanceTypes() {

	}

	public static function getInstanceParameters() {

	}
}


