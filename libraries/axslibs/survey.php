<?php

defined('_JEXEC') or die;

class AxsSurvey {

    private $survey;
    private $questions;
    private $params;

    public function __construct($survey) {
        $this->survey      = $survey;
        $questions = (array) json_decode($survey->questions);
        $this->questions = array_values($questions);
        $this->params    = json_decode($survey->params);

        //this is to set defaults to be backwards compatible
        if(!$this->params->rating_number) {
            $this->params->rating_number = 5;
        }
        if(!$this->params->rating_direction) {
            $this->params->rating_direction = 'vertical';
        }
    }

    public function setSurvey($survey) {
        $this->survey = $survey;
    }

    public function setQuestions($survey) {
        $questions = (array) json_decode($survey->questions);
        $this->questions = array_values($questions);
    }

    public function setParams($survey) {
        $this->params = json_decode($survey->params);
    }

    public function getsurvey() {
        return $this->survey;
    }

    public function getQuestions() {
        return $this->questions;
    }

    public function getParams() {
        return $this->params;
    }

    public function getTitle() {
        return $this->survey->title;
    }

    public function getDescription() {
        return $this->survey->description;
    }

    public function getCompletedMessage() {
        $surveyParams = $this->params;
        if($surveyParams->custom_completed_message && $surveyParams->completed_message) {
            $completed_message = $surveyParams->completed_message;
        } else {
            $completed_message = "<h3>Survey Complete</h3>";
        }
        return $completed_message;
    }

    public function getlikertDefaultLabels() {
        switch($this->params->rating_number) {
            case '5':
                $options = [
                    'Strongly Disagree',
                    'Disagree',
                    'Undecided',
                    'Agree',
                    'Strongly Agree'
                ];
            break;
            case '7':
                 $options = [
                    'Strongly Disagree',
                    'Disagree',
                    'Somewhat Disagree',
                    'Undecided',
                    'Somewhat Agree',
                    'Agree',
                    'Strongly Agree'
                ];
            break;
            case '11':
                 $options = [
                    'Very Strongly Disagree',
                    'Strongly Disagree',
                    'Disagree',
                    'Mostly Disagree',
                    'Somewhat Disagree',
                    'Undecided',
                    'Somewhat Agree',
                    'Mostly Agree',
                    'Agree',
                    'Strongly Agree',
                    'Very Strongly Agree'
                ];
            break;
        }
        return $options;
    }

    public function getlikertCustomLabels() {
        $options = array();
        $params = $this->params;
        for($i = 1; $i <= $params->rating_number; $i++) {
            $label = 'label_'.$i;
            $options[] = $params->$label;
        }
        return $options;
    }

    public function getOptionsHTML($options) {
        $html = '';
        foreach($options as $option) {
            $html .= '<option value="'.$option.'">'.$option.'</option>';
        }
        return $html;
    }

    public function getRadioButtonsHTML($options) {
        $html = '<div class="radio_group">';
        $i = 1;
        foreach($options as $option) {
            $html .= '<label class="tovuti_radio">';
            $html .= '<input class="tovuti_radio_input" data-index="'.$i++.'" type="radio" value="'.$option.'" name="survey_answer">';
            $html .= '<div class="tovuti_radio_circle"></div>';
            $html .= $option;
            $html .= '</label>';
        }
        $html .= '</div>';
        return $html;
    }

    public function getRadioButtonsHorizontalHTML($options) {
        $count = count($options);
        if($count == 11) {
            $i = 0;
            $start = 0;
            $end = $count - 1;
        } else {
            $i = 1;
            $start = 1;
            $end = $count;
        }

        $html = '<div class="radio_group hradio_group">';

        foreach($options as $option) {
            if($i == $start) {
                $html .= '<div class="tovuti_radio_left_label">'.$option.'</div>';
            }
            $html .= '<label class="tovuti_radio tovuti_radio_horizontal">';
            $html .= '<input class="tovuti_radio_input" data-index="'.$i.'" type="radio" value="'.$option.'" name="survey_answer">';
            $html .= '<div class="tovuti_radio_circle tovuti_radio_circle_horizontal"></div>';
            $html .= '<div class="tovuti_radio_number">'.$i.'</div>';
            $html .= '</label>';
            if($i == $end) {
                $html .= '<div class="tovuti_radio_right_label">'.$option.'</div>';
            }
            $i++;
        }
        $html .= '</div>';
        return $html;
    }

    public function getTextAreaHTML() {
        $html = '<textarea class="tovuti_survey_textarea" id="survey_answer" name="survey_answer"></textarea>';
        return $html;
    }

    public function getTextBoxHTML() {
        $html = '<input type="text" class="tovuti_survey_textbox" id="survey_answer" name="survey_answer">';
        return $html;
    }

    public function getLikertOptions() {
        $params = $this->params;
        if($params->likert_labels == 'default') {
            $labels = $this->getlikertDefaultLabels($params->likert_labels);
        } else {
            $labels = $this->getlikertCustomLabels();
        }
        if($params->rating_direction == 'horizontal') {
            $options = $this->getRadioButtonsHorizontalHTML($labels);
        } else {
            $options = $this->getRadioButtonsHTML($labels);
        }
        return $options;
    }

    public function getMultipleChoiceOptions($question) {
        $values = explode("\r\n",$question->multiple_choice_options);
        $options = $this->getRadioButtonsHTML($values);
        return $options;
    }

    public function loadQuestion($number) {
        $userId = JFactory::getUser()->id;
        $question = $this->questions[$number];
        $data = new stdClass();
        $data->user_id = $userId;
        $data->survey_id = $this->survey->id;
        $data->question_id = $question->id;
        $data->question_type = $question->question_type;
        $data->question = $question->question;
        $data->question_number = $number;
        $key = AxsKeys::getKey('surveys');
        $encryptedData = base64_encode(AxsEncryption::encrypt($data,$key));

        $html  = '<div class="tovuti_survey_question">'.$question->question.'</div>';
        $html .= '<input class="question_type" type="hidden" value="'.$question->question_type.'" name="question_type">';
        $html .= '<input class="survey_data" type="hidden" value="'.$encryptedData.'" name="survey_data">';
        switch ($question->question_type) {
            case "likert":
                $html .= $this->getLikertOptions();
            break;
            case "multiplechoice":
                $html .= $this->getMultipleChoiceOptions($question);
            break;
            case "textbox":
                $html .= $this->getTextBoxHTML();
            break;
            case "textarea":
                $html .= $this->getTextAreaHTML();
            break;
        }
        return $html;
    }

    public function getQuestionById($question_id) {
        $i = 0;
        foreach($this->questions as $question) {
            if($question_id == $question->id) {
                $question->index = $i;
                return $question;
            }
            $i++;
        }
        return false;
    }

    public function getTemplate() {
        include 'components/com_axs/templates/surveys/default.php';
    }

    public static function getSurveyById($id = null)  {
        if(!$id) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('id')." = ". $db->quote($id);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_surveys');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function getSurveyProgress($survey_id = null, $user_id = null)  {
        if(!$survey_id || !$user_id) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions = array('archive_date IS NULL');
        $conditions[] = $db->quoteName('survey_id')." = ". $db->quote($survey_id);
        $conditions[] = $db->quoteName('user_id')." = ". $db->quote($user_id);
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_survey_progress');
        $query->where($conditions);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function calculateScores($survey_id = null, $user_id = null)  {
        if(!$survey_id) {
            return false;
        }

        $survey = self::getSurveyById($survey_id);
        $surveyParams = json_decode($survey->params);
        if($surveyParams->rating_number) {
            $rating_number = $surveyParams->rating_number;
        } else {
            $rating_number = 5;
        }
        $detractors   = 0;
        $passives     = 0;
        $promoters    = 0;
        $satisfied    = 0;
        $nuetral      = 0;
        $unsatisfied  = 0;
        $totalScore   = 0;
        $db = JFactory::getDbo();
        $conditions = array();
        $conditions[] = 'archive_date IS NULL';
        $conditions[] = $db->quoteName('survey_id')." = ". $db->quote($survey_id);
        if($user_id) {
            $conditions[] = $db->quoteName('user_id')." = ". $db->quote($user_id);
        }
        $conditions[] = $db->quoteName('question_type')." = ". $db->quote('likert');
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_survey_responses');
        $query->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            $responseTotal = count($result);
            foreach($result as $row) {
                if($rating_number == 5) {
                    if($row->score >= 1 && $row->score <= 3) {
                        $detractors++;
                    }
                    if($row->score >= 1 && $row->score <= 2) {
                        $unsatisfied++;
                    }
                    if($row->score == 3) {
                        $nuetral++;
                    }
                    if($row->score == 4) {
                        $passives++;
                        $satisfied++;
                    }
                    if($row->score == 5) {
                        $promoters++;
                        $satisfied++;
                    }
                }

                if($rating_number == 7) {
                    if($row->score >= 1 && $row->score <= 4) {
                        $detractors++;
                    }
                    if($row->score >= 1 && $row->score <= 3) {
                        $unsatisfied++;
                    }
                    if($row->score == 4) {
                        $nuetral++;
                    }
                    if($row->score == 5 || $row->score == 6) {
                        $passives++;
                    }
                    if($row->score == 6) {
                        $satisfied++;
                    }
                    if($row->score == 7) {
                        $promoters++;
                        $satisfied++;
                    }
                }

                if($rating_number == 11) {
                    if($row->score >= 0 && $row->score <= 6) {
                        $detractors++;
                    }
                    if($row->score >= 0 && $row->score <= 5) {
                        $unsatisfied++;
                    }
                    if($row->score == 6) {
                        $nuetral++;
                    }
                    if($row->score == 7 || $row->score == 8) {
                        $passives++;
                    }
                    if($row->score == 8) {
                        $satisfied++;
                    }
                    if($row->score == 9 || $row->score == 10) {
                        $promoters++;
                        $satisfied++;
                    }
                }

                $totalScore = $totalScore + $row->score;
            }
            $NPS = new stdClass();
            $NPS->detractorsTotal = $detractors;
            $NPS->passivesTotal = $passives;
            $NPS->promotersTotal = $promoters;
            $NPS->responseTotal = $responseTotal;
            $NPS->detractorsPercentage = round(($detractors / $responseTotal) * 100);
            $NPS->passivesPercentage = round(($passives / $responseTotal) * 100);
            $NPS->promotersPercentage = round(($promoters / $responseTotal) * 100);
            $NPS->finalScore = $NPS->promotersPercentage - $NPS->detractorsPercentage;

            $CSAT = new stdClass();
            $CSAT->unsatisfied = $unsatisfied;
            $CSAT->nuetral = $nuetral;
            $CSAT->satisfied = $satisfied;
            $CSAT->questionTotal = $responseTotal;
            $CSAT->finalScore = round(($CSAT->satisfied / $responseTotal) * 100);

            if($rating_number == 11) {
                $rating_number = 10;
            }
            $score = new stdClass();
            $score->nps  = $NPS;
            $score->csat = $CSAT;
            $score->totalScore = $totalScore;
            $score->totalPossible = $responseTotal * $rating_number;
            $score->totalAverage = round(($totalScore / $score->totalPossible) * 100);
            return $score;
        } else {
            return 0;
        }
    }

    public static function getUserSurveyResponses($userId,$survey_id) {
        if(!$userId) {
            return false;
        }
        $db = JFactory::getDbo();
        $conditions = array();
        $conditions[] = $db->quoteName('user_id')." = ". $db->quote($userId);
        $conditions[] = $db->quoteName('survey_id')." = ". $db->quote($survey_id);
        $conditions[] = 'archive_date IS NULL';
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_survey_responses');
        $query->where($conditions);
        $query->order('id ASC');
        $db->setQuery($query);
        $result = $db->loadObjectList();
        if($result) {
            return $result;
        } else {
            return array();
        }
    }
}