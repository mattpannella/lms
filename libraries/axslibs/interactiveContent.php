<?php

defined('_JEXEC') or die;

class AxsInteractiveContent {
	

	public function getSubGroups($group) {
	    $db = JFactory::getDbo();
	    $query = $db->getQuery(true);
	    $query->select($db->quoteName('id'));
	    $query->from($db->quoteName('#__usergroups'));
	    $query->where($db->quoteName('parent_id').'='.(int)$group);
	    $db->setQuery($query);
	    $groups = $db->loadObjectList();

		return $groups;
	}

	public function getAllSubGroups($groupId) {
	    
	    $groupsArray = array(); 
	    $groups = self::getSubGroups($groupId);
	    
	    if($groups) {
			foreach($groups as $group) {
				array_push($groupsArray,(int)$group->id);
				$subGroups = self::getAllSubGroups((int)$group->id);
				if(count($subGroups) > 0) {
					for($i = 0; $i <= count($subGroups); $i++) {
						if($subGroups[$i]) {
							array_push($groupsArray,$subGroups[$i]);
						}						
					}
				}							
	    	}
	    }
	    return $groupsArray;	    
	}

	public function getUserMetaIds($userId,$key) {

		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
        $query->select('umeta_id')
              ->from('ic_usermeta')
              ->where('user_id ='.$userId)
              ->where('meta_key ='.$db->quote($key));
        $db->setQuery($query);
        
        $meta_id = $db->loadObject()->umeta_id;
        return $meta_id;
	}

	public function checkIfAdmin($userGroups,$adminGroups) {

		foreach($userGroups as $group) {
			if(in_array((int)$group,$adminGroups)) {
				return true;
			}						
		}
		return false;
	}

	public function createICUser($user) {
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
        $query->select('*')
              ->from('ic_users')
              ->where('id ='.$user->id);
        $db->setQuery($query);
        
        $userExist = $db->loadObject();		
		
		$userSync = new stdClass();

		$userSync->ID = $user->id;
		$userSync->user_login = $user->username;
		$userSync->user_nicename = $user->name;
		$userSync->display_name = $user->name;
		$userSync->user_pass = 'neraguertg8kj';

		$userMeta = new stdClass();

		$userMeta->user_id = $user->id;
		$userMeta->meta_key = 'ic_capabilities';

		$userMeta2 = new stdClass();

		$userMeta2->user_id = $user->id;
		$userMeta2->meta_key = 'ic_user_level';
		
		$topGroupId = 20;
		$superAdminGroupId = 36;        
        $subGroups = self::getAllSubGroups($topGroupId);
        $superAdminGroups = self::getAllSubGroups($superAdminGroupId);
        array_push($superAdminGroups,$superAdminGroupId);
        array_push($subGroups,$topGroupId);
        $allAdminGroups = array_merge($subGroups,$superAdminGroups);
        $isAdmin = self::checkIfAdmin($user->groups,$allAdminGroups);
        
        if( $isAdmin ){
            $userMeta->meta_value = 'a:1:{s:6:"editor";b:1;}';
            $userMeta2->meta_value = '7';
        } else {
        	$userMeta->meta_value = 'a:1:{s:10:"subscriber";b:1;}';
            $userMeta2->meta_value = '0';
        }
        

	    if(!$userExist) {
			$db->insertObject('ic_users',$userSync);
			$db->insertObject('ic_usermeta',$userMeta);
			$db->insertObject('ic_usermeta',$userMeta2);
		} else {
			$userMeta->umeta_id = self::getUserMetaIds($user->id,'ic_capabilities');
			$userMeta2->umeta_id = self::getUserMetaIds($user->id,'ic_user_level');
			$db->updateObject('ic_users',$userSync, 'ID');
			$db->updateObject('ic_usermeta',$userMeta, 'umeta_id');
			$db->updateObject('ic_usermeta',$userMeta2, 'umeta_id');
		}	

	}

}