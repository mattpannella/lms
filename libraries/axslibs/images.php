<?php

//defined('_JEXEC') or die;

/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/

class AxsImages {

	static $loaded = array();

	public static function getImagesPath($requestee = null) {
		
		if (!isset($_SESSION['image_path']) || !isset($_COOKIE['path'])) {
			self::setSession();
		}

		$add = "";
		if(!empty($requestee)) {
			switch ($requestee) {
				case "inspiration":
					$add = "/inspiration";
					break;
				case "business_directory":
					$add = "/business_directory";
					break;
				case "forum":
					$add = "/forum";
					break;
				case "events":
					$add = "/events";
					break;
				case "store":
					$add = "/store";
					break;
				case "easyblog":
					$add = "/blog";
					break;
				case "community":
					$add = "/community";
					break;
				case "h5p":
					$add = "/h5p";
					break;
				default:
					$add = "/".$requestee;
					break;
			}
		}

		return $_SESSION['image_path'] . $add;
	}

	private static function setSession() {
		$result = null;
		if (isset(static::$loaded[__METHOD__])) {
			return;
		}
		$db = JFactory::getDBO();
		$query = "SELECT * FROM joom_extensions WHERE name='com_media'";
		$db->setQuery($query);

		$result = $db->loadObject();
		$params = json_decode($result->params);

		$image_path = $params->image_path;
		$file_path = $params->file_path;

		$_SESSION['image_path'] = $file_path;
		setcookie("path", base64_encode($file_path));

		static::$loaded[__METHOD__] = 'session set';
	}
}

?>