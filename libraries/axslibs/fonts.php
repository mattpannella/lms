<?php

defined('_JEXEC') or die;

class AxsFonts {

	/**
	 *  Gets all the user configured fonts and renders the <link> for them
	 * 
	 * @return string $html
	 */
	public static function renderFontLinks() {
		foreach (self::getFonts() as $font) {
			if ($font->link) {
				$html .= $font->link . "\n";
			} else {
				$html .= "<link href='https://fonts.googleapis.com/css?family=$font->title' rel='stylesheet' type='text/css'>\n";
			}
		}
		return $html;
	}
	
	/**
	 *  Gets all enabled fonts
	 * 
	 * @return array $result - array of php objects representing rows from the axs_fonts db table
	 */
	public static function getFonts() {
		$db = JFactory::getDBO();
		
		$query = "SELECT * FROM axs_fonts WHERE enabled = 1";

		$db->setQuery($query);
		$result = $db->loadObjectList();

		return $result;
	}
}