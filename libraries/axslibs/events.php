<?php
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
defined('_JEXEC') or die;

class AxsEvents {

    public function copyTicketTypes($event_ids) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
                ->from('#__eb_ticket_types')
                ->where('event_id = '.(int)$event_ids->source_id);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        foreach($result as $row) {
            unset($row->id);
            $row->event_id = $event_ids->new_id;
            $db->insertObject('#__eb_ticket_types',$row);
        }
    }

    public function getEventById($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__eb_events')
            ->where('id = '.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function getEventByAlias($alias) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
            ->from('#__eb_events')
            ->where('alias = '.$db->quote($alias));
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public function getEventRegistrationId($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__eb_registrants')
              ->where('id = '.(int)$id)
              ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public function checkAlias($alias,$event_id = null) {
		if(!$alias) {
			return false;
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('id');
        $query->from('#__eb_events');
        if($alias) {
            $query->where('alias = '.$db->quote($alias));
        }
        if($event_id) {
            $query->where('id != '.$db->quote($event_id));
        }
        $query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		if($result) {
			return true;
		} else {
			return false;
		}
    }

    public static function eventCheckIn($user_id,$event_id) {
		if(!$user_id || !$event_id) {
            return;
        }

        $db = JFactory::getDbo();
        $setConditions[] = $db->quoteName('checked_in').'= 1';
        $whereConditions[] = $db->quoteName('event_id').' = '.$db->quote($event_id);
        $whereConditions[] = $db->quoteName('user_id').' = '.$db->quote($user_id);
        $query = $db->getQuery(true);
        $query->update('#__eb_registrants')
              ->set($setConditions)
              ->where($whereConditions);
        $db->setQuery($query);
        $result = $db->execute();

		$awardCheckParams = new stdClass();
		$awardCheckParams->requirement_type = 'event';
		$awardCheckParams->event_id = $event_id;
		$award_ids = AxsLMS::getAvailableBadges($awardCheckParams);
		if($award_ids) {
			AxsLMS::updateBadges($award_ids, $user_id,null,null);
		}

        // Mark checklist items of type event_checkin as completed for the given user and event ID
        AxsLMS::markUserChecklistItemsCompletedForItemType($user_id, $event_id, 'event_checkin');

        return $result;
	}

    // Event Category Functions
    public static function getEventCategoriesForEventId($eventId) {
        $categories = [];

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $categoryColumns = [
            'category.id as id',
            'category.parent as parent',
            'category.name as Name',
            'category.level as Level',
            'category.alias as Alias',
            'category.ordering as Ordering'
        ];

        $query->select($categoryColumns)
              ->from('#__eb_categories as category')
              ->leftJoin("#__eb_event_categories as eventCategory on eventCategory.category_id = category.id")
              ->where("eventCategory.event_id = $eventId")
              ->where("eventCategory.main_category = 1");
        $db->setQuery($query);

        $categories = $db->loadObjectList();

        return $categories;
    }

    public static function getEventCategoryParentByCategoryId($eventCategoryId) {
        $db = JFactory::getDBo();
        $categoryParent = null;

        $query = $db->getQuery(true);

        $query->select('*');

        $query->from('#__eb_categories')
              ->where("id = $eventCategoryId");

        $db->setQuery($query);

        try {
            $category = $db->loadObject();
            $query = $db->getQuery(true);

            $query->select('*');

            $query->from('#__eb_categories')
                  ->where("id = $category->parent");

            $db->setQuery($query);
            $categoryParent = $db->loadObject();
        } catch(Exception $ex) {
            echo $ex->getMessage();
            die;
        }

        return $categoryParent;
    }

    public static function getEventCategoryById($eventCategoryId) {
        $db = JFactory::getDBo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__eb_categories')
              ->where("id = ".(int)$eventCategoryId);
        $db->setQuery($query);
        $category = $db->loadObject();
        return $category;
    }

    /**
     * Gets an event category by its name
     *
     * @param string $eventCategoryName
     * @return stdClass Event category object
     */
    public static function getEventCategoryByName($eventCategoryName) {

        $categoryName = htmlentities(strip_tags($eventCategoryName));
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $query->select('*')
              ->from('#__eb_categories')
              ->where('name = ' . $db->quote($categoryName));

        $db->setQuery($query);

        $category = $db->loadObject();
        return $category;
    }

    public static function getAbbreviatedTimezone($rawTimeZone) {

        // Construct a DateTimeZone object from the given timezone of the event and get the abbreviated version
        $dateTime = new DateTime();
        $timeZone = !empty($rawTimeZone) ? $rawTimeZone : 'America/Boise';
        $dateTime->setTimeZone(new DateTimeZone($timeZone));
        $abbreviatedTimeZone = $dateTime->format('T');

        // This holds the backreferenced matches from the following regexp pattern.
        $offsetComponents = [];

        // Check the abbreviated timezone to see if it's in the form (+-)HH, where HH is a two-digit hour offset from GMT
        // An example: "-03" would match and be separated into "-" and "03", respectively.
        $isTimezoneGMTOffset = preg_match("/([+-])(.+)/i", $abbreviatedTimeZone, $offsetComponents);

        // If we're dealing with a GMT offset for an abbreviation, add "GMT" to the beginning of the string and remove
        // any leading zeroes to make it match the standard GMT timezone abbreviation format (GMT+-HH, with no leading zeroes in HH)
        if($isTimezoneGMTOffset) {

            // $offsetComponents[0] would refer to the entire timezone abbreviation, and we just want the components.
            $offsetDirection = $offsetComponents[1];
            $offsetValue = $offsetComponents[2];

            // Strip any leading zeroes from the offset value
            $offsetValue = ltrim($offsetValue, '0');

            // Covers the edge case where we get a timezone with a funky abbreviation like "+00" or "+0".
            $GMTSuffix = (!empty($offsetValue) ? ($offsetDirection . $offsetValue) : '');

            $abbreviatedTimeZone = "GMT" . $GMTSuffix;
        }

        return $abbreviatedTimeZone;
    }

    /**
     * Gets an event location name by location ID
     *
     * @param integer $location_id
     * @return string 'None' if the location ID given is empty (null, zero, etc.), or the matching location name
     */
    public static function getEventLocationNameById($location_id) {
		if ($location_id == "0" || empty($location_id)) {
			// Location_id will be 0 for any events
			// that do not have a location set
			return 'None';
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('joom_eb_locations');
		$query->where('id = ' . $db->quote($location_id));

		$db->setQuery($query);
		$result = $db->loadObject();

		return $result->name;
	}

    public static function getEventRegistrantCount($event_id) {
		if (empty($event_id)) {
			return 0;
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('SUM(number_registrants) as count');
		$query->from('joom_eb_registrants');
		$query->where(
            array(
                'event_id = ' . $db->quote($event_id),
                'published = 1'
            )
        );
        $query->groupBy('event_id');

		$db->setQuery($query);
		$result = $db->loadObject();

        if ($result->count) {
            return $result->count;
        } else {
            // Events with no registrant records
            // will give a null result for the count column
            return '0';
        }
    }

}