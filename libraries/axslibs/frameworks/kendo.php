<link rel="stylesheet" href="/media/kendo/styles/web/kendo.common.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.bootstrap.css" />
<link rel="stylesheet" href="/media/kendo/styles/web/kendo.bootstrap.mobile.css" />
<!-- <script src="/media/kendo/js/jquery.js"></script> -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.5.0/lodash.min.js"></script>
<script src="/media/kendo/js/jszip.min.js"></script>
<script src="/media/kendo/js/kendo.all.min.js"></script>
<!-- <link href="/media/kendo/styles/kendo.common.min.css" rel="stylesheet"/>
<link href="/media/kendo/styles/kendo.default.min.css" rel="stylesheet"/>
<link href="/media/kendo/styles/kendo.default.mobile.min.css" rel="stylesheet"/> -->
<?php echo AxsLanguage::getKendoLocalization(); ?>