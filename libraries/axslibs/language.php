<?php

defined('_JEXEC') or die;

class AxsLanguage {

	protected static $loaded = [];

	protected static $translation_map;

	public static function translate($text) {

		$language_tag = explode('-', self::getCurrentLanguage()->get('tag'))[0];

		if ($language_tag == 'en') {
			// English is the default Language
			return $text;
		}

		$sig = md5(serialize('translateText'));

		if (empty(static::$translation_map)) {
			self::$translation_map = self::getTranslationMap($language_tag);
		}

		// Charset for japanese 
		if ($language_tag == 'ja') {
			self::$translation_map = [];
		}

		if (!isset(static::$loaded[__METHOD__][$sig]))
		{
			JHtml::_('jquery.framework');

			
			JFactory::getDocument()->addScriptDeclaration(trim(
				"
				TRANSLATION_VARS = {};
				$(() => { 
					// Send vars off to our google translate controller endpoint
					$.ajax({
						url: 'index.php?option=com_axs&task=google_translate.translate&format=raw',
						data: {	TRANSLATION_VARS, target_language: '$language_tag' },
						type: 'post'
					}).done(function (result) {
						// console.log(result);
						if (!result.error) {
							// Result is a space separated list of translated words and their hashes
							result = JSON.parse(result);
							Object.keys(result).forEach(key => {
								jQuery('#' + key).each((idx, el) => {
									jQuery(el).text(result[key]);
								});
							})
						} else {
							console.error(result.error);
						}
					}).error(function (result) {
						console.error(result);
					});
				});
				"
			));

			static::$loaded[__METHOD__][$sig] = true;
		}

		$hash = md5(serialize(array($text, $language_tag)));

		$translationFromDb = self::$translation_map[$hash];

		if (!empty($translationFromDb)) 
		{
			return $translationFromDb['translated'];
		} 
		else if (!isset(static::$loaded[__METHOD__][$hash])) 
		{
			JFactory::getDocument()->addScriptDeclaration(
				"TRANSLATION_VARS['$hash'] = '$text';"
			);
			static::$loaded[__METHOD__][$hash] = true;

			return "<span id='$hash' style='all:unset'>$text</span>";
		}
		
	}

	private static function getTranslationMap($language_tag) {
		
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('*')->from('axs_translations')->where([
			'language_tag = ' . $db->quote($language_tag),
		]);

		$db->setQuery($query);

		$translationMap = $db->loadAssocList('hash');

		return $translationMap;
	}

	public static function storeTranslation(string $original, string $translated, string $language_tag) {

		$newTranslation = new stdClass();
		$newTranslation->original = $original;
		$newTranslation->translated = $translated;
		$newTranslation->language_tag = $language_tag;
		$newTranslation->hash = md5(serialize(array($original, $language_tag)));

		$db = JFactory::getDbo();
		
		$db->insertObject('axs_translations', $newTranslation);

		return $db;
	}

	public static function getSubdomainDefault() {

		$sig = md5(serialize('getSubdomainDefault'));

		if (isset(static::$loaded[__METHOD__][$sig]))
		{
			return static::$loaded[__METHOD__][$sig];
		}

		$db = JFactory::getDBO();
		$host = $_SERVER['HTTP_HOST'];
		$query = "SELECT * FROM axs_domains WHERE domain = '" . $host . "'";
        $db->setQuery($query);
        $domain = $db->loadObject();

        $lang_code = null;
        if ($domain) {
        	$params = json_decode($domain->params);
        	$lang_code = $params->language;
        }

        if (!$lang_code) {
        	$lang_code = "en-GB";
        }

        $default = JLanguage::getInstance($lang_code);

		static::$loaded[__METHOD__][$sig] = $default;

        return $default;

	}

	public static function getCurrentLanguageURL() {
		$language = self::getCurrentLanguage()->get('tag');
		$domainDefaultLanguage = self::getSubdomainDefault()->get('tag');
		$languageURL = "";
		if($language != $domainDefaultLanguage) {
			if(strpos($language,'-')) {
				$languageURL = "/".explode('-',$language)[0];
			}
		}
		return $languageURL;
	}

	public static function getCurrentLanguageCode() {
		$language = self::getCurrentLanguage()->get('tag');
		$languageCode = "";
		if(strpos($language,'-')) {
			$languageCode = explode('-',$language)[0];
		}
		return $languageCode;
	}

	public static function getLanguageOverrides($params,$language) {
		if(!$params->language_overrides || !$language) {
			return false;
		}
		foreach($params->language_overrides as $key => $value) {
			if($value && $key == $language) {
				return $value;
			}
		}
		return false;
	}


	public static function getCurrentLanguage() {
		$sig = md5(serialize('getCurrentLanguage'));

		if (isset(static::$loaded[__METHOD__][$sig]))
		{
			return static::$loaded[__METHOD__][$sig];
		}

		$sefs = explode("/", $_SERVER['REQUEST_URI']);
		$current_language = null;

		if ($sefs[1] != "") {

			//Check that the first sef is a language
			$languages = self::getPublishedLanguages();
			foreach ($languages as $language) {
				if ($language->sef == $sefs[1]) {
					$current_language = $language;
					break;
				}
			}
		}

		if ($current_language) {
			$language = JLanguage::getInstance($current_language->lang_code);
		} else {
			//No language tag is set
			$language = self::getSubdomainDefault();
		}

		static::$loaded[__METHOD__][$sig] = $language;

		return $language;
	}

	public static function getSefByLangCode($langCode) {

		$sig = md5(serialize($langCode));

		if (isset(static::$loaded[__METHOD__][$sig]))
		{
			return static::$loaded[__METHOD__][$sig];
		}

		$db = JFactory::getDBO();
		$query = "SELECT sef FROM joom_languages WHERE lang_code = '$langCode' LIMIT 1";

		$db->setQuery($query);
		$results = $db->loadObject();
		if (isset($results->sef)) {
			static::$loaded[__METHOD__][$sig] = $results->sef;
			return $results->sef;
		} else {
			static::$loaded[__METHOD__][$sig] = 'not found';
			return null;
		}
	}

	public static function getLangCodeBySef($sef) {
		$db = JFactory::getDBO();
		$query = "SELECT lang_code FROM joom_languages WHERE sef = '$sef' LIMIT 1";

		$db->setQuery($query);
		$results = $db->loadObject();
		if (isset($results->lang_code)) {
			return $results->lang_code;
		}
	}

	public static function getPublishedLanguages() {
		$sig = md5(serialize('getPublishedLanguages'));

		if (isset(static::$loaded[__METHOD__][$sig]))
		{
			return static::$loaded[__METHOD__][$sig];
		}

		$db = JFactory::getDBO();

		$query = "SELECT * FROM joom_languages WHERE published = 1";
		$db->setQuery($query);

		$result = $db->loadObjectList();

		static::$loaded[__METHOD__][$sig] = $result;

		return $result;
	}

	public static function publishNeededLanguages($list = null) {

		//If list is not an array of languages, create an empty array
		if (!is_array($list)) {
			$list = array();
		}

		//Get all of the languages used in the dropdown menues in the brand manager
		$db = JFactory::getDBO();
		$query = "SELECT data FROM axs_brands WHERE published = 1";
		$db->setQuery($query);
		$data = $db->loadObjectList();

		foreach ($data as $dat) {
			$dat = json_decode($dat->data);
			if ($dat->site_details->language_selector == "1") {
				$options = $dat->site_details->language_selector_options;
				if (!is_array($dat->site_details->language_selector_options)) {
					$options = explode(',', $dat->site_details->language_selector_options);
				}

				foreach ($options as $option) {
					//Add them to the list
					array_push($list, $option);
				}
			}
		}

		//Get all of the default languages that are used for the domains
		$query = "SELECT params FROM axs_domains";
		$db->setQuery($query);
		$data = $db->loadObjectList();

		foreach ($data as $dat) {
			$params = json_decode($dat->params);
			array_push($list, $params->language);
		}

		$languages = implode(",", array_unique($list));

		$query = "UPDATE joom_languages SET published = 0";
		$db->setQuery($query);
		$db->execute();

		$query = "UPDATE joom_languages SET published = 1 WHERE FIND_IN_SET(`lang_code`, '" . $languages . "')";
		$db->setQuery($query);
		$db->execute();
	}

	public static function text($code, $alt) {
		$text = JText::_($code);
	    if ($text == $code) {
	        return $alt;
	    } else {
	        return $text;
	    }
	}

	public static function getLocalizationVariable($value) {
		$value_map = array(
			'Index' => 'COM_KUNENA_MENU_ITEM_INDEX',
			'Recent Topics' => 'COM_KUNENA_MENU_ITEM_RECENT_TOPICS',
			'New Topic' => 'COM_KUNENA_MENU_ITEM_NEW_TOPIC',
			'No Replies' => 'COM_KUNENA_MENU_ITEM_NO_REPLIES',
			'My Topics' => 'COM_KUNENA_MENU_ITEM_MY_TOPICS',
			'Profile' => 'COM_KUNENA_MENU_ITEM_PROFILE',
			'Search' => 'COM_KUNENA_MENU_ITEM_SEARCH',
			'Community' => 'AXS_COMMUNITY',
			'My Profile' => 'COM_COMMUNITY_MY_PROFILE',
			'Change Profile Picture' => 'COM_COMMUNITY_CHANGE_PROFILE_PICTURE',
			'Change Profile Video' => 'COM_COMMUNITY_CHANGE_PROFILE_VIDEO',
			'Edit Profile' => 'AXS_EDIT_PROFILE',
			'Edit Details' => 'COM_COMMUNITY_EDIT_DETAILS',
			'Privacy' => 'AXS_PRIVACY',
			'Preferences' => 'AXS_PREFERENCES',
			'Photos' => 'COM_COMMUNITY_PHOTOS',
			'Groups' => 'COM_COMMUNITY_GROUPS',
			'Videos' => 'COM_COMMUNITY_VIDEOS',
			'Events' => 'COM_COMMUNITY_EVENTS',
			'Friends' => 'COM_COMMUNITY_FRIENDS',
			'All My Friends' => 'COM_COMMUNITY_ALL_MY_FRIENDS',
			'Search' => 'COM_COMMUNITY_SEARCH',
			'Advanced Search' => 'COM_COMMUNITY_CUSTOM_SEARCH',
			'Inbox' => 'AXS_INBOX',
			'Forum' => 'COM_KUNENA_FORUM'
		);
		if (array_key_exists($value, $value_map)) {
			return $value_map[$value];
		} else {
			return $value;
		}
	}

	/**
	 * Loads kendos localization files from CDN
	 * 
	 * See https://docs.telerik.com/kendo-ui/globalization/localization
	 */
	public static function getKendoLocalization() {
		$lang = AxsLanguage::getCurrentLanguage()->get('tag');
		if ($lang != 'en-GB') {
			// return JHtml::_('script', '/media/kendo/js/messages/kendo.messages.' . $lang . '.js', array('version' => 'auto'));
			return '<script src="/media/kendo/js/messages/kendo.messages.' . $lang . '.js"></script>';
		} else {
			return null;
		}
	}

}