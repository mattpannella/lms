<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

require_once JPATH_ROOT . '/components/shared/controllers/image-resizer.php';

class AxsScormFactory {

	public static function loadContent($params) {
		$key = AxsKeys::getKey('lms');
		$height = '1500px';
		switch ($params->scorm_type) {
		    case 'bizlibrary':
		        $height = '1500px';
		    break;

		    case 'scormlibrary':
		        $scormItem = AxsScormFactory::getScormItemById($params->scorm_course_id);
		        $scormItemParams = json_decode($scormItem->params);
		        if($scormItemParams->frame_height) {
		        	if((int)$scormItemParams->frame_height < 500) {
		        		$height = '500px';
		        	} else {
		        		$height = $scormItemParams->frame_height.'px';
		        	}

		        }
		    break;
		}
		$params->height = $height;
		$encryptedScormParams = base64_encode(AxsEncryption::encrypt($params, $key));
		include 'components/com_splms/templates/scorm_activity.php';
	}

	public static function getScormItems() {
		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('enabled').'=1';
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from($db->quoteName('#__splms_scorm_library'))
			  ->where($conditions);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}

	public static function getLearnerScormData($params) {
		$db = JFactory::getDbo();
		$conditions = array();

		if($params->course_id) {
			$conditions[] = $db->quoteName('course_id').'='.$db->quote($params->course_id);
		}
		if($params->lesson_id) {
			$conditions[] = $db->quoteName('lesson_id').'='.$db->quote($params->lesson_id);
		}

		$conditions[] = $db->quoteName('scorm_content_id').'='.$db->quote($params->scorm_content_id);
		$conditions[] = $db->quoteName('user_id').'='.$db->quote($params->user_id);

		$query = $db->getQuery(true);
		$query->select('*')
			  ->from($db->quoteName('axs_scorm_learner_data'))
			  ->where($conditions)
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	public static function getScormItemById($id) {
		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('id').'='.(int)$id;
		$query = $db->getQuery(true);
		$query->select('*')
			  ->from($db->quoteName('#__splms_scorm_library'))
			  ->where($conditions)
			  ->limit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		return $result;
	}

	public static function saveImage($image,$code) {
		$code = trim($code);
		$directory = '/mnt/shared/22/images/library_posters/';
		if(strpos($image, 'application/pdf')) {
			return false;
		}
		if(strpos($image, 'image/png')) {
			$extension = '.png';
			$textToRemove = 'data:image/png;base64, ';
		} elseif(strpos($image, 'image/jpeg') || strpos($image, 'image/jpg')){
			$extension = '.jpg';
			$textToRemove = 'data:image/jpeg;base64, ';
		}
		if(!$extension) {
			return false;
		}
		$image = str_replace($textToRemove, '', $image);
	    $image = str_replace(' ', '+', $image);
	    $data = base64_decode($image);
		$file = $directory . $code . $extension;
		$success = file_put_contents($file, $data);
		//Create Thumbnail
		if(file_exists($file)) {
			$imageThumb = new SimpleImage();
			$imageThumb->load($file);
			if($imageThumb->getWidth() > 600) {
				$imageThumb->resizeToWidth(600);
				$imageThumb->save($directory.$code.'_thumb'.$extension);
			}
		}

	    return $code.'_thumb'.$extension;
	}

	public static function bizlibraryAPIConnect($url) {
		$curl = curl_init($url);
		$header = array();
		$header[] = 'Content-type: application/json';
		$header[] = 'X-Api-Key: nGBgGKrCgggFTId75FvW2l2J7RIYHMJ964eoGGY8';
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_VERBOSE, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($curl);
		curl_close($curl);
		return $result;
	}

	public static function getCoursePosterTest($code) {
		$billingID = 'tovuti01';
		$code = trim($code);
		$url = "https://content-api.bizlibrary.com/latest/".$billingID."/courses/".$code."/poster";
		$result = self::bizlibraryAPIConnect($url);
	    return $result;
	}

	public static function getCoursePoster($code) {
		//$code = $code.'_thumb';
		$billingID = 'tovuti01';
		$directory = '/mnt/shared/22/images/library_posters/';
		$code = trim($code);
		$code = explode('_',$code);
		$code = $code[0].'_'.$code[1];

		if(file_exists($directory.$code.'_thumb.png')) {
			$image = 'https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/library_posters/'.$code.'_thumb.png';
		} elseif(file_exists($directory.$code.'_thumb.jpg')) {
			$image = 'https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/library_posters/'.$code.'_thumb.jpg';
		} else {
			$url = "https://content-api.bizlibrary.com/latest/".$billingID."/courses/".$code."/poster";
			$result = self::bizlibraryAPIConnect($url);
			$item = json_decode($result);
		    if(!empty($item->data->attributes->image)) {
		    	$image = $item->data->attributes->image;
				$savedImage = self::saveImage($image,$code);
				if(!$savedImage) {
					$image = 'https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg';
				}
		    } else {
		    	$image = 'https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/graphics/placeholder.jpg';
		    }
		}

	    return $image;
	}

	public static function getBizLibraryCourse($code) {
		$code = trim($code);
		$billingID = 'tovuti01';
		$url = "https://content-api.bizlibrary.com/latest/".$billingID."/courses/".$code;
		$result = self::bizlibraryAPIConnect($url);
		return $result;
	}

	public static function getBillingId($language) {
		$contentLibraryAccess = AxsDbAccess::getAccessLevels('bizlibrary_content_selection');
		switch ($language) {
			case 'english':
				if($contentLibraryAccess == 'production') {
					$billingID = 'tovuti02';
				} else {
					$billingID = 'tovuti01';
				}
			break;
			case 'spanish':
				$billingID = 'tovuti01_BLP_ES';
			break;
			case 'german':
				$billingID = 'tovuti01_BLP_German';
			break;
			case 'french':
				$billingID = 'tovuti01_BLP_French';
			break;
			case 'japanese':
				$billingID = 'tovuti01_BLP_Japanese';
			break;
			default:
				if($contentLibraryAccess == 'production') {
					$billingID = 'tovuti02';
				} else {
					$billingID = 'tovuti01';
				}
			break;
		}
		return $billingID;
	}

	public static function runAPICall($params) {
		$billingID = self::getBillingId($params->language);
		$queryParams = self::buildUrlParameters($params);
		$url = "https://content-api.bizlibrary.com/latest/".$billingID."/courses".$queryParams;
		$result = self::bizlibraryAPIConnect($url);
	    return $result;
	}

	public static function loadLibraryListings($params) {
	    $result = self::runAPICall($params);
		$items  = json_decode($result);
		ob_start();
		require 'administrator/components/com_splms/templates/contentlibrary_items.php';
		return ob_get_clean();
	}

	private static function buildUrlParameters($params) {
		$filterQuery = [];

		if (!empty($params->content_type_filter)) {
			$filterQuery []= $params->content_type_filter;
		}
		if($params->category) {
			$filterQuery []= "topic0:'$params->category'";
		}
		if($params->topic) {
			$filterQuery []= "topic1:'".$params->category.">".$params->topic."'";
		}


		$url = '?size='.rawurlencode($params->size);

		if($filterQuery) {
			$url .= '&filterQuery='.rawurlencode(implode(' ', $filterQuery));
		}
		if($params->keyword) {
			$url .= '&query='.rawurlencode($params->keyword);
		}

		if($params->sort) {
			switch ($params->sort) {
				case 'title':
					$direction = 'asc';
				break;
				case 'created':
					$direction = 'desc';
				break;
				case 'popularity':
					$direction = 'desc';
				break;
				case 'avg_rating':
					$direction = 'desc';
				break;
				case 'duration':
					$direction = 'asc';
				break;
				default:
					$direction = 'asc';
				break;
			}
			$url .= '&sort='.rawurlencode($params->sort.' '.$direction);
		}
		if($params->start) {
			$url .= '&start='.rawurlencode($params->start);
		}
		return $url;
	}

	public static function getBizLibraryCourseType($content_id) {
        $result = 'full';
        $productionBillingIDs = ["tovuti02","tovuti01_BLP_ES","tovuti01_BLP_German","tovuti01_BLP_French","tovuti01_BLP_Japanese"];
        foreach($productionBillingIDs as $billingID) {
            $url = "https://content-api.bizlibrary.com/latest/".$billingID."/courses/".$content_id;
            $result = self::bizlibraryAPIConnect($url);
            $courseObject = json_decode($result, true);
            if(!empty($courseObject) && count($courseObject) > 0) {
                return 'production';
            }
        }
        return $result;
    }

	public static function updateAICCScormData($userId, $courseId, $lessonId, $scormCourseId, $aiccData = null, $language = 'en-GB') {
		$db     = JFactory::getDbo();

		$response = new stdClass();

		if(!$userId) {
			$response->status  = "error";
			$response->message = "Your session has ended. Please login again.";
		} else {

			$data = new stdClass();
			$data->user_id 			= $userId;
			$data->course_id 		= $courseId;
			$data->lesson_id 		= $lessonId;
			$data->scorm_content_id = $scormCourseId;
			$data->modified_date    = date('Y-m-d H:i:s');
			$data->scorm_type = 'aicc';

			$row = AxsScormFactory::getLearnerScormData($data);

			switch($aiccData->success_status) {

				case 'passed':
				case 'completed':
					$scormStatus = 'completed';
				break;

				case 'failed':
					$scormStatus = 'failed';
				break;

				case 'unknown':
				default:
					$scormStatus = 'started';
				break;
			}

			$data->data = json_encode($aiccData);

			if($row) {
				$data->id = $row->id;
				$result = $db->updateObject('axs_scorm_learner_data',$data,'id');
			} else {
				$result = $db->insertObject('axs_scorm_learner_data',$data);
			}

			if($result) {
				$response->progress = $scormStatus;
				$response->status  = "success";
				$response->message = "Data Saved";
			} else {
				$response->progress = $scormStatus;
				$response->status  = "error";
				$response->message = "There was an issue saving your data";
			}

			$data->scorm_course_id = $scormCourseId;
	        $data->success  = $aiccData->success_status;
	        $data->status   = $scormStatus;
			$data->language = $language;

			if($data->lesson_id) {
	            AxsLMS::insertLessonStatus($data);
	        } else {
	            AxsLMS::courseProgress_ScormContent($data);
	        }
		}

		return $response;
	}

	public static function readManifest($scorm_id)
    {
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('id') . '=' . $db->quote($scorm_id);
        $query = $db->getQuery(true);
        $query->select('*')->from($db->quoteName('joom_splms_scorm_library'))->where($conditions)->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        $data = json_decode($result->params);
        
        $xmltext = file_get_contents($data->directory . "/imsmanifest.xml");
        
        if ($xmltext == null)
            throw new Exception("Unable to load package manifest file($scorm_id). Please contact your administrator.");
        $pattern = '/&(?!\w{2,6};)/';
        $replacement = '&amp;';
        $xmltext = preg_replace($pattern, $replacement, $xmltext);
        $objxml = new xml2Array();
        $manifests = $objxml->parse($xmltext);

        $scos = new stdClass();
        $scos->version = '';
        $scos = static::_getManifest($manifests, $scos);
        return $scos;
    }

	public static function getFirstItem($manifest) {
		$identifier = $manifest->identifier;
		$org = $manifest->defaultorg;
		$org = $manifest->elements[$identifier][$org];
		$item = $org[array_key_first($org)];

		return $item;
	}
    
    private static function _getResources($blocks)
    {
        $resources = array();
        foreach ($blocks as $block)
        {
            if ($block['name'] == 'RESOURCES' && isset($block['children']))
            {
                foreach ($block['children'] as $resource)
                {
                    if ($resource['name'] == 'RESOURCE')
                    {
                        $resources[static::addslashes($resource['attrs']['IDENTIFIER'])] = $resource['attrs'];
                    }
                }
            }
        }
        return $resources;
    }
    
    private static function addslashes($var)
    {
        if (is_string($var))
        {
            $var = str_replace('\\', '\\\\', $var);
            $var = str_replace(array(
                '\'',
                '"',
                "\n",
                "\r",
                "\0"
            ), array(
                '\\\'',
                '\\"',
                '\\n',
                '\\r',
                '\\0'
            ), $var);
            $var = str_replace('</', '<\/', $var); // XHTML compliance.
        }
        else if (is_array($var))
        {
            $var = array_map('addslashes', $var);
        }
        else if (is_object($var))
        {
            $a = get_object_vars($var);
            foreach ($a as $key => $value)
            {
                $a[$key] = static::addslashes($value);
            }
            $var = (object) $a;
        }
        return $var;
    }
    
    private static function _getManifest($blocks, $scos)
    {
        static $parents = array();
        static $resources;
        static $manifest;
        static $organization;
        $manifestresourcesnotfound = array();
        if (count($blocks) > 0)
        {
            foreach ($blocks as $block)
            {
                switch ($block['name'])
                {
                    case 'METADATA':
                        if (isset($block['children']))
                        {
                            foreach ($block['children'] as $metadata)
                            {
                                if ($metadata['name'] == 'SCHEMAVERSION')
                                {
                                    if (empty($scos->version))
                                    {
                                        $isversionset = (preg_match("/^(1\.2)$|^(CAM )?(1\.3)$/", $metadata['tagData'], $matches));
                                        if (isset($metadata['tagData']) && $isversionset)
                                        {
                                            $scos->version = 'SCORM_' . $matches[count($matches) - 1];
                                        }
                                        else
                                        {
                                            $isversionset = (preg_match("/^2004 (3rd|4th) Edition$/", $metadata['tagData'], $matches));
                                            if (isset($metadata['tagData']) && $isversionset)
                                            {
                                                $scos->version = 'SCORM_1.3';
                                            }
                                            else
                                            {
                                                $scos->version = 'SCORM_1.2';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 'MANIFEST':
                        $manifest = $block['attrs']['IDENTIFIER'];
                        $scos->identifier = $manifest;
                        $organization = '';
                        $resources = array();
                        $resources = static::_getResources($block['children']);
                        $scos = static::_getManifest($block['children'], $scos);
                        if (empty($scos->elements) || count($scos->elements) <= 0)
                        {
                            foreach ($resources as $item => $resource)
                            {
                                if (!empty($resource['HREF']))
                                {
                                    $sco = new stdClass();
                                    $sco->identifier = $item;
                                    $sco->title = $item;
                                    $sco->parent = '/';
                                    $sco->launch = $resource['HREF'];
                                    $sco->scormtype = $resource['ADLCP:SCORMTYPE'];
                                    $scos->elements[$manifest][$organization][$item] = $sco;
                                }
                            }
                        }
                        break;
                    case 'ORGANIZATIONS':
                        if (!isset($scos->defaultorg) && isset($block['attrs']['DEFAULT']))
                        {
                            $scos->defaultorg = $block['attrs']['DEFAULT'];
                        }
                        if (!empty($block['children']))
                        {
                            $scos = static::_getManifest($block['children'], $scos);
                        }
                        break;
                    case 'ORGANIZATION':
                        $identifier  = $block['attrs']['IDENTIFIER'];
                        $organization = '';
                        $scos->elements[$manifest][$organization][$identifier] = new stdClass();
                        $scos->elements[$manifest][$organization][$identifier]->identifier = $identifier;
                        $scos->elements[$manifest][$organization][$identifier]->parent = '/';
                        $scos->elements[$manifest][$organization][$identifier]->launch = '';
                        $scos->elements[$manifest][$organization][$identifier]->scormtype = '';
                        $parents = array();
                        $parent = new stdClass();
                        $parent->identifier = $identifier;
                        $parent->organization = $organization;
                        array_push($parents, $parent);
                        $organization = $identifier;
                        if (!empty($block['children']))
                        {
                            $scos = static::_getManifest($block['children'], $scos);
                        }
                        array_pop($parents);
                        break;
                    case 'ITEM':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        $identifier = $block['attrs']['IDENTIFIER'];
                        $scos->elements[$manifest][$organization][$identifier] = new stdClass();
                        $scos->elements[$manifest][$organization][$identifier]->identifier = $identifier;
                        $scos->elements[$manifest][$organization][$identifier]->parent = $parent->identifier;
                        if (!isset($block['attrs']['ISVISIBLE']))
                        {
                            $block['attrs']['ISVISIBLE'] = 'true';
                        }
                        $scos->elements[$manifest][$organization][$identifier]->isvisible = $block['attrs']['ISVISIBLE'];
                        if (!isset($block['attrs']['PARAMETERS']))
                        {
                            $block['attrs']['PARAMETERS'] = '';
                        }
                        $scos->elements[$manifest][$organization][$identifier]->parameters = $block['attrs']['PARAMETERS'];
                        if (!isset($block['attrs']['IDENTIFIERREF']))
                        {
                            $scos->elements[$manifest][$organization][$identifier]->launch = '';
                            $scos->elements[$manifest][$organization][$identifier]->scormtype = 'asset';
                        }
                        else
                        {
                            $idref = $block['attrs']['IDENTIFIERREF'];
                            $base = '';
                            if (isset($resources[$idref]['XML:BASE']))
                            {
                                $base = $resources[$idref]['XML:BASE'];
                            }
                            if (!isset($resources[$idref]))
                            {
                                $manifestresourcesnotfound[] = $idref;
                                $scos->elements[$manifest][$organization][$identifier]->launch = '';
                            }
                            else
                            {
                                $scos->elements[$manifest][$organization][$identifier]->launch = $base . $resources[$idref]['HREF'];
                                if (empty($resources[$idref]['ADLCP:SCORMTYPE']))
                                {
                                    $resources[$idref]['ADLCP:SCORMTYPE'] = 'asset';
                                }
                                $scos->elements[$manifest][$organization][$identifier]->scormtype = $resources[$idref]['ADLCP:SCORMTYPE'];
                                
                            }
                        }
                        $parent = new stdClass();
                        $parent->identifier = $identifier;
                        $parent->organization = $organization;
                        array_push($parents, $parent);
                        if (!empty($block['children']))
                        {
                            $scos = static::_getManifest($block['children'], $scos);
                        }
                        array_pop($parents);
                        break;
                    case 'TITLE':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['tagData']))
                        {
                            $block['tagData'] = '';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->title = $block['tagData'];
                        break;
                    case 'ADLCP:PREREQUISITES':
                        if ($block['attrs']['TYPE'] == 'aicc_script')
                        {
                            $parent = array_pop($parents);
                            array_push($parents, $parent);
                            if (!isset($block['tagData']))
                            {
                                $block['tagData'] = '';
                            }
                            $scos->elements[$manifest][$parent->organization][$parent->identifier]->prerequisites = $block['tagData'];
                        }
                        break;
                    case 'ADLCP:MAXTIMEALLOWED':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['tagData']))
                        {
                            $block['tagData'] = '';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->maxtimeallowed = $block['tagData'];
                        break;
                    case 'ADLCP:TIMELIMITACTION':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['tagData']))
                        {
                            $block['tagData'] = '';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->timelimitaction = $block['tagData'];
                        break;
                    case 'ADLCP:DATAFROMLMS':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['tagData']))
                        {
                            $block['tagData'] = '';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->datafromlms = $block['tagData'];
                        break;
                    case 'ADLCP:MASTERYSCORE':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['tagData']))
                        {
                            $block['tagData'] = '';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->masteryscore = $block['tagData'];
                        break;
                    case 'ADLCP:COMPLETIONTHRESHOLD':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!isset($block['attrs']['MINPROGRESSMEASURE']))
                        {
                            $block['attrs']['MINPROGRESSMEASURE'] = '1.0';
                        }
                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->threshold = $block['attrs']['MINPROGRESSMEASURE'];
                        break;
                    case 'ADLNAV:PRESENTATION':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!empty($block['children']))
                        {
                            foreach ($block['children'] as $adlnav)
                            {
                                if ($adlnav['name'] == 'ADLNAV:NAVIGATIONINTERFACE')
                                {
                                    foreach ($adlnav['children'] as $adlnavinterface)
                                    {
                                        if ($adlnavinterface['name'] == 'ADLNAV:HIDELMSUI')
                                        {
                                            if ($adlnavinterface['tagData'] == 'continue')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hidecontinue = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'previous')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hideprevious = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'exit')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hideexit = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'exitAll')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hideexitall = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'abandon')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hideabandon = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'abandonAll')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hideabandonall = 1;
                                            }
                                            if ($adlnavinterface['tagData'] == 'suspendAll')
                                            {
                                                $scos->elements[$manifest][$parent->organization][$parent->identifier]->hidesuspendall = 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    case 'IMSSS:SEQUENCING':
                        $parent = array_pop($parents);
                        array_push($parents, $parent);
                        if (!empty($block['children']))
                        {
                            foreach ($block['children'] as $sequencing)
                            {
                                if ($sequencing['name'] == 'IMSSS:CONTROLMODE')
                                {
                                    if (isset($sequencing['attrs']['CHOICE']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->choice = $sequencing['attrs']['CHOICE'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['CHOICEEXIT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->choiceexit = $sequencing['attrs']['CHOICEEXIT'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['FLOW']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->flow = $sequencing['attrs']['FLOW'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['FORWARDONLY']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->forwardonly = $sequencing['attrs']['FORWARDONLY'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptobjectinfo = $sequencing['attrs']['USECURRENTATTEMPTOBJECTINFO'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->usecurrentattemptprogressinfo = $sequencing['attrs']['USECURRENTATTEMPTPROGRESSINFO'] == 'true' ? 1 : 0;
                                    }
                                }
                                if ($sequencing['name'] == 'IMSSS:DELIVERYCONTROLS')
                                {
                                    if (isset($sequencing['attrs']['TRACKED']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->tracked = $sequencing['attrs']['TRACKED'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['COMPLETIONSETBYCONTENT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->completionsetbycontent = $sequencing['attrs']['COMPLETIONSETBYCONTENT'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['OBJECTIVESETBYCONTENT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->objectivesetbycontent = $sequencing['attrs']['OBJECTIVESETBYCONTENT'] == 'true' ? 1 : 0;
                                    }
                                }
                                if ($sequencing['name'] == 'ADLSEQ:CONSTRAINEDCHOICECONSIDERATIONS')
                                {
                                    if (isset($sequencing['attrs']['CONSTRAINCHOICE']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->constrainChoice = $sequencing['attrs']['CONSTRAINCHOICE'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['PREVENTACTIVATION']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->preventactivation = $sequencing['attrs']['PREVENTACTIVATION'] == 'true' ? 1 : 0;
                                    }
                                }
                                if ($sequencing['name'] == 'IMSSS:OBJECTIVES')
                                {
                                    $objectives = array();
                                    foreach ($sequencing['children'] as $objective)
                                    {
                                        $objectivedata = new stdClass();
                                        $objectivedata->primaryobj = 0;
                                        switch ($objective['name'])
                                        {
                                            case 'IMSSS:PRIMARYOBJECTIVE':
                                                $objectivedata->primaryobj = 1;
                                            case 'IMSSS:OBJECTIVE':
                                                $objectivedata->satisfiedbymeasure = 0;
                                                if (isset($objective['attrs']['SATISFIEDBYMEASURE']))
                                                {
                                                    $objectivedata->satisfiedbymeasure = $objective['attrs']['SATISFIEDBYMEASURE'] == 'true' ? 1 : 0;
                                                }
                                                $objectivedata->objectiveid = '';
                                                if (isset($objective['attrs']['OBJECTIVEID']))
                                                {
                                                    $objectivedata->objectiveid = $objective['attrs']['OBJECTIVEID'];
                                                }
                                                $objectivedata->minnormalizedmeasure = 1.0;
                                                if (!empty($objective['children']))
                                                {
                                                    $mapinfos = array();
                                                    foreach ($objective['children'] as $objectiveparam)
                                                    {
                                                        if ($objectiveparam['name'] == 'IMSSS:MINNORMALIZEDMEASURE')
                                                        {
                                                            if (isset($objectiveparam['tagData']))
                                                            {
                                                                $objectivedata->minnormalizedmeasure = $objectiveparam['tagData'];
                                                            }
                                                            else
                                                            {
                                                                $objectivedata->minnormalizedmeasure = 0;
                                                            }
                                                        }
                                                        if ($objectiveparam['name'] == 'IMSSS:MAPINFO')
                                                        {
                                                            $mapinfo = new stdClass();
                                                            $mapinfo->targetobjectiveid = '';
                                                            if (isset($objectiveparam['attrs']['TARGETOBJECTIVEID']))
                                                            {
                                                                $mapinfo->targetobjectiveid = $objectiveparam['attrs']['TARGETOBJECTIVEID'];
                                                            }
                                                            $mapinfo->readsatisfiedstatus = 1;
                                                            if (isset($objectiveparam['attrs']['READSATISFIEDSTATUS']))
                                                            {
                                                                $mapinfo->readsatisfiedstatus = $objectiveparam['attrs']['READSATISFIEDSTATUS'] == 'true' ? 1 : 0;
                                                            }
                                                            $mapinfo->writesatisfiedstatus = 0;
                                                            if (isset($objectiveparam['attrs']['WRITESATISFIEDSTATUS']))
                                                            {
                                                                $mapinfo->writesatisfiedstatus = $objectiveparam['attrs']['WRITESATISFIEDSTATUS'] == 'true' ? 1 : 0;
                                                            }
                                                            $mapinfo->readnormalizemeasure = 1;
                                                            if (isset($objectiveparam['attrs']['READNORMALIZEDMEASURE']))
                                                            {
                                                                $mapinfo->readnormalizemeasure = $objectiveparam['attrs']['READNORMALIZEDMEASURE'] == 'true' ? 1 : 0;
                                                            }
                                                            $mapinfo->writenormalizemeasure = 0;
                                                            if (isset($objectiveparam['attrs']['WRITENORMALIZEDMEASURE']))
                                                            {
                                                                $mapinfo->writenormalizemeasure = $objectiveparam['attrs']['WRITENORMALIZEDMEASURE'] == 'true' ? 1 : 0;
                                                            }
                                                            array_push($mapinfos, $mapinfo);
                                                        }
                                                    }
                                                    if (!empty($mapinfos))
                                                    {
                                                        if (!isset($objectivesdata))
                                                            $objectivesdata = new stdClass();
                                                        $objectivesdata->mapinfos = $mapinfos;
                                                    }
                                                }
                                                break;
                                        }
                                        array_push($objectives, $objectivedata);
                                    }
                                    $scos->elements[$manifest][$parent->organization][$parent->identifier]->objectives = $objectives;
                                }
                                if ($sequencing['name'] == 'IMSSS:LIMITCONDITIONS')
                                {
                                    if (isset($sequencing['attrs']['ATTEMPTLIMIT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->attemptLimit = $sequencing['attrs']['ATTEMPTLIMIT'];
                                    }
                                    if (isset($sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->attemptAbsoluteDurationLimit = $sequencing['attrs']['ATTEMPTABSOLUTEDURATIONLIMIT'];
                                    }
                                }
                                if ($sequencing['name'] == 'IMSSS:ROLLUPRULES')
                                {
                                    if (isset($sequencing['attrs']['ROLLUPOBJECTIVESATISFIED']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->rollupobjectivesatisfied = $sequencing['attrs']['ROLLUPOBJECTIVESATISFIED'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['ROLLUPPROGRESSCOMPLETION']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->rollupprogresscompletion = $sequencing['attrs']['ROLLUPPROGRESSCOMPLETION'] == 'true' ? 1 : 0;
                                    }
                                    if (isset($sequencing['attrs']['OBJECTIVEMEASUREWEIGHT']))
                                    {
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->objectivemeasureweight = $sequencing['attrs']['OBJECTIVEMEASUREWEIGHT'];
                                    }
                                    if (!empty($sequencing['children']))
                                    {
                                        $rolluprules = array();
                                        foreach ($sequencing['children'] as $sequencingrolluprule)
                                        {
                                            if ($sequencingrolluprule['name'] == 'IMSSS:ROLLUPRULE')
                                            {
                                                $rolluprule = new stdClass();
                                                $rolluprule->childactivityset = 'all';
                                                if (isset($sequencingrolluprule['attrs']['CHILDACTIVITYSET']))
                                                {
                                                    $rolluprule->childactivityset = $sequencingrolluprule['attrs']['CHILDACTIVITYSET'];
                                                }
                                                $rolluprule->minimumcount = 0;
                                                if (isset($sequencingrolluprule['attrs']['MINIMUMCOUNT']))
                                                {
                                                    $rolluprule->minimumcount = $sequencingrolluprule['attrs']['MINIMUMCOUNT'];
                                                }
                                                $rolluprule->minimumpercent = 0.0000;
                                                if (isset($sequencingrolluprule['attrs']['MINIMUMPERCENT']))
                                                {
                                                    $rolluprule->minimumpercent = $sequencingrolluprule['attrs']['MINIMUMPERCENT'];
                                                }
                                                if (!empty($sequencingrolluprule['children']))
                                                {
                                                    foreach ($sequencingrolluprule['children'] as $rolluproleconditions)
                                                    {
                                                        if ($rolluproleconditions['name'] == 'IMSSS:ROLLUPCONDITIONS')
                                                        {
                                                            $conditions = array();
                                                            $rolluprule->conditioncombination = 'all';
                                                            if (isset($rolluproleconditions['attrs']['CONDITIONCOMBINATION']))
                                                            {
                                                                $rolluprule->CONDITIONCOMBINATION = $rolluproleconditions['attrs']['CONDITIONCOMBINATION'];
                                                            }
                                                            foreach ($rolluproleconditions['children'] as $rolluprulecondition)
                                                            {
                                                                if ($rolluprulecondition['name'] == 'IMSSS:ROLLUPCONDITION')
                                                                {
                                                                    $condition = new stdClass();
                                                                    if (isset($rolluprulecondition['attrs']['CONDITION']))
                                                                    {
                                                                        $condition->cond = $rolluprulecondition['attrs']['CONDITION'];
                                                                    }
                                                                    $condition->operator = 'noOp';
                                                                    if (isset($rolluprulecondition['attrs']['OPERATOR']))
                                                                    {
                                                                        $condition->operator = $rolluprulecondition['attrs']['OPERATOR'];
                                                                    }
                                                                    array_push($conditions, $condition);
                                                                }
                                                            }
                                                            $rolluprule->conditions = $conditions;
                                                        }
                                                        if ($rolluproleconditions['name'] == 'IMSSS:ROLLUPACTION')
                                                        {
                                                            $rolluprule->rollupruleaction = $rolluproleconditions['attrs']['ACTION'];
                                                        }
                                                    }
                                                }
                                                array_push($rolluprules, $rolluprule);
                                            }
                                        }
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->rolluprules = $rolluprules;
                                    }
                                }
                                if ($sequencing['name'] == 'IMSSS:SEQUENCINGRULES')
                                {
                                    if (!empty($sequencing['children']))
                                    {
                                        $sequencingrules = array();
                                        foreach ($sequencing['children'] as $conditionrules)
                                        {
                                            $conditiontype = -1;
                                            switch ($conditionrules['name'])
                                            {
                                                case 'IMSSS:PRECONDITIONRULE':
                                                    $conditiontype = 0;
                                                    break;
                                                case 'IMSSS:POSTCONDITIONRULE':
                                                    $conditiontype = 1;
                                                    break;
                                                case 'IMSSS:EXITCONDITIONRULE':
                                                    $conditiontype = 2;
                                                    break;
                                            }
                                            if (!empty($conditionrules['children']))
                                            {
                                                $sequencingrule = new stdClass();
                                                foreach ($conditionrules['children'] as $conditionrule)
                                                {
                                                    if ($conditionrule['name'] == 'IMSSS:RULECONDITIONS')
                                                    {
                                                        $ruleconditions = array();
                                                        $sequencingrule->conditioncombination = 'all';
                                                        if (isset($conditionrule['attrs']['CONDITIONCOMBINATION']))
                                                        {
                                                            $sequencingrule->conditioncombination = $conditionrule['attrs']['CONDITIONCOMBINATION'];
                                                        }
                                                        foreach ($conditionrule['children'] as $rulecondition)
                                                        {
                                                            if ($rulecondition['name'] == 'IMSSS:RULECONDITION')
                                                            {
                                                                $condition = new stdClass();
                                                                if (isset($rulecondition['attrs']['CONDITION']))
                                                                {
                                                                    $condition->cond = $rulecondition['attrs']['CONDITION'];
                                                                }
                                                                $condition->operator = 'noOp';
                                                                if (isset($rulecondition['attrs']['OPERATOR']))
                                                                {
                                                                    $condition->operator = $rulecondition['attrs']['OPERATOR'];
                                                                }
                                                                $condition->measurethreshold = 0.0000;
                                                                if (isset($rulecondition['attrs']['MEASURETHRESHOLD']))
                                                                {
                                                                    $condition->measurethreshold = $rulecondition['attrs']['MEASURETHRESHOLD'];
                                                                }
                                                                $condition->referencedobjective = '';
                                                                if (isset($rulecondition['attrs']['REFERENCEDOBJECTIVE']))
                                                                {
                                                                    $condition->referencedobjective = $rulecondition['attrs']['REFERENCEDOBJECTIVE'];
                                                                }
                                                                array_push($ruleconditions, $condition);
                                                            }
                                                        }
                                                        $sequencingrule->ruleconditions = $ruleconditions;
                                                    }
                                                    if ($conditionrule['name'] == 'IMSSS:RULEACTION')
                                                    {
                                                        $sequencingrule->action = $conditionrule['attrs']['ACTION'];
                                                    }
                                                    $sequencingrule->type = $conditiontype;
                                                }
                                                array_push($sequencingrules, $sequencingrule);
                                            }
                                        }
                                        $scos->elements[$manifest][$parent->organization][$parent->identifier]->sequencingrules = $sequencingrules;
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
        
        return $scos;
    }
}

class xml2Array
{
    public $arroutput = array();
    public $resparser;
    public $strxmldata;
    
    /**
     * Parse an XML text string and create an array tree that represent the XML structure
     *
     * @param string $strinputxml The XML string
     * @return array
     */
    public function parse($strinputxml)
    {
        $this->resparser = xml_parser_create('UTF-8');
        xml_set_object($this->resparser, $this);
        xml_set_element_handler($this->resparser, "tagopen", "tagclosed");
        xml_set_character_data_handler($this->resparser, "tagdata");
        $this->strxmldata = xml_parse($this->resparser, $strinputxml);
        if (!$this->strxmldata)
        {
            die(sprintf("XML error: %s at line %d", xml_error_string(xml_get_error_code($this->resparser)), xml_get_current_line_number($this->resparser)));
        }
        xml_parser_free($this->resparser);
        return $this->arroutput;
    }
    public function tagopen($parser, $name, $attrs)
    {
        $tag = array(
            "name" => $name,
            "attrs" => $attrs
        );
        array_push($this->arroutput, $tag);
    }
    public function tagdata($parser, $tagdata)
    {
        if (trim($tagdata))
        {
            if (isset($this->arroutput[count($this->arroutput) - 1]['tagData']))
            {
                $this->arroutput[count($this->arroutput) - 1]['tagData'] .= $tagdata;
            }
            else
            {
                $this->arroutput[count($this->arroutput) - 1]['tagData'] = $tagdata;
            }
        }
    }
    public function tagclosed($parser, $name)
    {
        $this->arroutput[count($this->arroutput) - 2]['children'][] = $this->arroutput[count($this->arroutput) - 1];
        array_pop($this->arroutput);
    }
}