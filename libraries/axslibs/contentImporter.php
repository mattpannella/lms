<?php

defined('_JEXEC') or die;

class AxsContentImporter {

    public function getCourseCategorySelect() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("*");
        $query->from('#__splms_coursescategories');
        $query->order('title ASC');
        $db->setQuery($query);
        $categories = $db->loadObjectList();
        $html = '<select name="import_category" id="import_category" style="width: 100%;">';
        $html .= '<option value="">Select Category</option>';
        foreach($categories as $category) {
            $html .= '<option value="'.$category->splms_coursescategory_id.'">'.$category->title.'</option>';
        }
        $html .= '</select>';
        $html .= '<div class="bl_button" id="add_category">
        <i class="icon-plus"></i> Create New Category</div>';
        return $html;
    }

    public function getCourseSelect() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("*");
        $query->from('#__splms_courses');
        $query->order('title ASC');
        $db->setQuery($query);
        $courses = $db->loadObjectList();
        $html = '<select name="import_course" id="import_course" style="width: 100%;">';
        $html .= '<option value="">Select Course</option>';
        foreach($courses as $course) {
            $html .= '<option value="'.$course->splms_course_id.'">'.$course->title.'</option>';
        }
        $html .= '</select>';
        $html .= '<div class="bl_button" id="add_course">
        <i class="icon-plus"></i> Create New Course</div>';
        return $html;
    }
}