<?php 
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

JFormHelper::loadFieldClass('timezone');

class AxsTimezones {

    protected static $zones = array('Africa', 'America', 'Antarctica', 'Arctic', 'Asia', 'Atlantic', 'Australia', 'Europe', 'Indian', 'Pacific');


    public static function getTimezoneList($selected_option, $selectNameAppendString = null, $firstOption = null) {

        $groups = array();

        // Get the list of time zones from the server.
        $zones = DateTimeZone::listIdentifiers();

        // Build the group lists.
        foreach ($zones as $zone)
        {
            // Time zones not in a group we will ignore.
            if (strpos($zone, '/') === false)
            {
                continue;
            }

            // Get the group/locale from the timezone.
            list ($group, $locale) = explode('/', $zone, 2);

            // Only use known groups.
            if (in_array($group, self::$zones))
            {
                // Initialize the group if necessary.
                if (!isset($groups[$group]))
                {
                    $groups[$group] = array();
                }

                // Only add options where a locale exists.
                if (!empty($locale))
                {
                    
                    $groups[$group][$zone] = JHtml::_('select.option', $zone, str_replace('_', ' ', $locale), 'value', 'text',false);
                   
                    
                    
                }
            }
        }

        // Sort the group lists.
        ksort($groups);

        foreach ($groups as &$location)
        {
            sort($location);
        }
        $html = '<select name="time_zone'.$selectNameAppendString.'"><option value="">'.$firstOption.'</option>';
        foreach($groups as $key => $value) {
            $html .= '<optgroup label="'.$key.'">';
            foreach($value as $option) {
                if($option->value == $selected_option) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                $html .= '<option value="'.$option->value.'" '.$selected.'>'.$option->text.'</option>';
            }
            $html .= '</optgroup>';
        }
        $html .= '</select>';
       return $html;
    }
    
    public static function getTimezoneListArray() {

        $groups = array();

        // Get the list of time zones from the server.
        $zones = DateTimeZone::listIdentifiers();

        // Build the group lists.
        foreach ($zones as $zone)
        {
            // Time zones not in a group we will ignore.
            if (strpos($zone, '/') === false)
            {
                continue;
            }

            // Get the group/locale from the timezone.
            list ($group, $locale) = explode('/', $zone, 2);

            // Only use known groups.
            if (in_array($group, self::$zones))
            {
                // Initialize the group if necessary.
                if (!isset($groups[$group]))
                {
                    $groups[$group] = array();
                }

                // Only add options where a locale exists.
                if (!empty($locale))
                {
                    
                    $groups[$group][$zone] = JHtml::_('select.option', $zone, str_replace('_', ' ', $locale), 'value', 'text',false);
                   
                    
                    
                }
            }
        }

        // Sort the group lists.
        ksort($groups);

        foreach ($groups as &$location)
        {
            sort($location);
        }
        $timezones = array();
        $html = '<select name="time_zone">';
        foreach($groups as $key => $value) {
            foreach($value as $option) {
                array_push($timezones,$option->value);
            }
        }
       return $timezones;
    }
}
