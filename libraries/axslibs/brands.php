<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsBrands {

	private static $instance_brand = null;
	private static $current_id = null;

	/**
     * Initializes the brand on the first visit to the site.
     *
     * @return  null
     */
	public static function init() {
		self::getBrand();
	}

	/**
     * Description
     *
     * @return  object                          The Brand for the current site
     */
	public static function getBrand() {
		/*
			If it's already been loaded this refresh, just return it.
		*/

		if (self::$instance_brand) {
			self::setEmail(self::$instance_brand);
			return self::$instance_brand;
		}

		/*
			Otherwise, get it.
		*/
		/*if (!isset($_SESSION['brand_id'])) {
			$brand = self::getBrandObject();
		} else {
			$id = $_SESSION['brand_id'];
			$brand = self::getBrandById($id, true);*/
			/*
				Will return null if the id is no longer any good.
				(Should be a rare case!)
				If so, get it normally.
			*/
		/*	if ($brand == null) {
				$brand = self::getBrandObject();
			}
		}*/

		$brand = self::getBrandObject();

		/*
			If nothing above results in a brand, get the default.
		*/

		if ($brand == null) {
			$brand = self::getDefault();
		}

		if ($brand != null) {
			self::setSession($brand);
		}

		self::setEmail($brand);
		self::$instance_brand = $brand;
		
		return $brand;
	}

	/**
     * Gets the brand that is set as the default
     *
     * @return  object                          The default brand
     */
	public static function getDefault() {
		$db = JFactory::getDBO();
		$query = "SELECT * FROM `axs_brands` WHERE default_brand = 1 LIMIT 1";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$brandData = json_decode($result[0]->data);
		$brandData->email = self::getEmailSettingsById($brandData->email_settings_id);
		$brandData->billing = self::getPaymentGatewayById($brandData->payment_gateway_id);
		$brandData->api_settings = self::getIntegrationById($brandData->integration_id);
		$result[0]->data = $brandData;
		if ($result == null) {
			return $result[0];
		} else {
			/*
				No default is set.  All resources exhausted.  Return null to user.
			*/
			return null;
		}
	}

	/**
     * Returns a brand by its ID
     *
     * @param   integer     $id      	The ID of the requested brand
     * @param   boolean		$validate 	Should the brand be checked that it matches the url?  (False == return it no matter what)
     * @return  object                  The Brand object
     */
	public static function getBrandById($id = 0, $validate = false) {
		if ($id <= 0) {
			return null;
		}

		$data = self::getData(null, $id, !$validate);

		if ($data == null) {
			return null;
		}

		return self::getBrandObject($data);
	}

    /**
     * Returns a brand domains by its ID
     *
     * @param   integer     $id      	The ID of the requested brand
     * @param   boolean		$validate 	Should the brand be checked that it matches the url?  (False == return it no matter what)
     * @return  string                  comma separated list of domains
     */
    public static function getBrandDomainsById($id = 0, $validate = false) {
        if ($id <= 0) {
            return null;
        }

        $data = self::getData(null, $id, !$validate);

        if ($data == null) {
            return null;
        }

        return $data->domains;
    }

    /**
     * Return the list of domains that belong to a specific brand.
     * @param 	integer 	$brandId 	The ID of the brand.
     * @return 	string 					A list of all of the client's domains
     */
    public static function getBrandDomains($brandId = null) {
        $db = JFactory::getDbo();

        //$query = "SELECT * FROM axs_brands WHERE id = $brandId LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_brands'))
            ->where(array(
                $db->quoteName('id') . '=' . $db->quote($brandId),
            ))
            ->limit(1);

        $db->setQuery($query);
        $result = $db->loadObject();
        if ($result) {
            return $result->domains;
        } else {
            return FALSE;
        }
    }

    /**
     * Add the client's new domain to the correct brand.
     * @param 	string 		$domain 	The ID of the domain
     * @param   string      $action     add | remove  action for list
     * @param 	integer 	$brandId 	The ID of the brand to associate with the domain.
     * @return 	string       Comma separated string of domains
     */
    public static function getUpdatedDomainsList($domain, $action, $brandId ) {
        $brandDomains = self::getBrandDomains($brandId);
        if ($action == "add") {
            if (empty($brandDomains)){
                $arr = array();
            } else {
                $arr = explode(',', $brandDomains);
            }
            array_push($arr, $domain);
            $updatedDomains = implode(',', array_unique($arr));
        } elseif ($action == "remove") {
            $arr = explode(',', $brandDomains);
            $arr = array_diff($arr, array($domain));
            $updatedDomains = implode(',', array_unique($arr));
        }

        return $updatedDomains;
    }

    /**
     * Add the client's new domain to the correct brand.
     * @param 	string 		$updatedDomains 	Comma separated string of domains to update
     * @param 	integer|null 	$brandId 	The ID of the brand to associate with the domain.
     * @return 	null
     */
    public static function updateBrand($updatedDomains,$brandId = NULL) {

        $db = JFactory::getDbo();

        //$query = "UPDATE axs_brands SET domains = '$updatedDomains' WHERE id = $brandId LIMIT 1";
        $query = $db->getQuery(true);
        $query
            ->update($db->quoteName('axs_brands'))
            ->set($db->quoteName('domains') . '=' . $db->quote($updatedDomains))
            ->where($db->quoteName('id') . '=' . $db->quote($brandId))
            ->limit(1);

        $db->setQuery($query);
        $db->execute();
    }




    /**
	 * This returns a brand based on a url.
	 * @param string|null 	$url 	The url that the brand should belong to.
	 * @return object				The Brand object.
	 */
	public static function getBrandObjectByUrl($url = null) {
		if ($url == null) {
			return null;
		}

		$data = self::getData($url, null, true);
		if ($data == null) {
			return null;
		}
		return self::getBrandObject($data);
	}

	private static function getCurrentUrl() {
		if (isset($_SERVER['SERVER_NAME'])) {
			return $_SERVER['SERVER_NAME'];
		} else {
			return null;
		}
	}

	private static function setSession($brand) {
		if ($brand != null) {
			$_SESSION['brand_id'] = $brand->id;
		}
	}

	/**
     * Returns brands by domain
     *
     * @param   string      $domain     The domain of the requested brand
     * @return  string                  brands associated with the domain
     */
	public static function getBrandsByDomain($domain = null) {
		if ($domain == null) {
			return null;
		}

		$db = JFactory::getDBO();

		$query = "SELECT * FROM `axs_brands` WHERE (FIND_IN_SET(" . $db->quote($domain) . ", domains) > 0)";
		$db->setQuery($query);

		$result = $db->loadObjectList();
		if ($result == null) {
			return null;
		} else {
			return $result;
		}
	}

	private static function getData($url = null, $id = null, $requested = false) {
		$db = JFactory::getDBO();
		if ($url == null) {
			$url = self::getCurrentUrl();
		}

		/*
			If no $id is specified, find it by finding the current domain in the axs_brands database.
			Otherwise, get the data by id.
		*/
		if ($id == null) {
			$language = JFactory::getLanguage()->get('tag');
			$query = "SELECT * FROM `axs_brands` WHERE language = " . $db->quote($language) . " AND (FIND_IN_SET(" . $db->quote($url) . ", domains) > 0) LIMIT 1";

			$db->setQuery($query);
			$result = $db->loadObjectList();

			if ($result == null) {

				/*
					If no result is returned, they may not have a brand for every language.  Return the first entry with the url only.
				*/

				$query = "SELECT * FROM `axs_brands` WHERE (FIND_IN_SET(" . $db->quote($url) . ", domains) > 0) LIMIT 1";
				$db->setQuery($query);

				$result = $db->loadObjectList();
				if ($result == null) {
					return null;
				} else {
					return $result[0];
				}
			} else {
				return $result[0];
			}
		} else {
			$query = "SELECT * FROM `axs_brands` WHERE id = " . $id;
			$db->setQuery($query);

			$result = $db->loadObjectList();

			/*
				If the ID is a bad ID, or if it has been removed since the brand was last found, it will be null.
				If so, just get the brand as if it were not set.
			*/
			if ($result == null) {
				return null;
			} else {
				/*
					If the data has results, check to be sure it still contains the current domain in case the brand
					was changed or altered since the id was acquired and the brand at that id is no longer for that domain.
					(Should be a rare case!)

					However, if the data was requested directly (ie. getBrandById()), just return it.
				*/
				if ($requested == false) {
					$domains = explode(",", $result[0]->domains);
					if (in_array(self::getCurrentUrl(), $domains)) {
						return $result[0];
					} else {
						return null;
					}
				} else {
					return $result[0];
				}
			}
		}
	}

	/**
	 * Returns the ID of the Brand that is currently being used.
	 * @return integer
	 */
	public static function getCurrentBrandId() {
		return self::$current_id;
	}

	private static function getBrandObject($data = null) {

		if ($data == null) {
			$data = self::getData();

			if ($data == null) {
				//Still no good.
				return null;
			}
		}

		$brand = json_decode($data->data);
		$brand->id = $data->id;
		$brand->published = $data->published;

		self::$current_id = $brand->id;

		$db = JFactory::getDBO();

		$language = AxsLanguage::getCurrentLanguage()->get('tag');

		/* Get the homepage */

		if ($brand->homepage->multilingual == "yes") {
			$options = json_decode($brand->homepage->language_templates);
			$homepage_id = $options->$language;
		} else {
			$homepage_id = $brand->homepage->template;
		}

		if (!$homepage_id) {
			$homepage_id = 1;
		}

		$query = "SELECT * FROM `axs_homepage_templates` WHERE id=" . $db->quote($homepage_id);
		$db->setQuery($query);
		$result = $db->loadObjectList()[0];
		$brand->homepage = json_decode($result->data);

		/* Get the dashboard */

		if ($brand->dashboard->multilingual == "yes") {
			$options = json_decode($brand->dashboard->language_templates);
			$dashboard_id = $options->$language;
		} else {
			$dashboard_id = $brand->dashboard->template;
		}

		if (!$dashboard_id) {
			$dashboard_id = 1;
		}

		$query = "SELECT * FROM `axs_dashboard_templates` WHERE id=" . $db->quote($dashboard_id);
		$db->setQuery($query);
		$result = $db->loadObjectList()[0];
		$brand->dashboard = json_decode($result->data);

		$brand->email = self::getEmailSettingsById($brand->email_settings_id);
		$brand->billing = self::getPaymentGatewayById($brand->payment_gateway_id);
		$brand->api_settings = self::getIntegrationById($brand->integration_id);

		return $brand;
	}

	/**
	 * Return the contact page associated with the Brand.
	 * @param object 	$brand
	 * @return object 	contact page info
	 */
	public static function getContact($brand = null) {
		if (!$brand) {
			$brand = self::getBrand()->id;
		}

		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from('axs_contact_page')
			->where($brand .' IN (brands)')
			->limit(1);

		$db->setQuery($query);
		$contactPage = $db->loadObject();
		return $contactPage;
	}

	/**
	 * Set the email configurations for the brand.
	 * @param object $brand 	The brand object to get the email configurations from.
	 * @return null
	 */
	public static function setEmail($brand) {
		$config = JFactory::getConfig();
		$email = $brand->email;

		if( !$email->email_custom_smtp && $email->smtpuser == 'tovuti85' || (!$email->smtpuser || !$email->smtphost || !$email->smtppass || !$email->smtpport || !$email->smtpuser || $email->smtpuser == 'tovuti85') ) {
			$sendgrid = new AxsSendgrid();
			$email = $sendgrid->setDefaultSettings($email);
		}

		//Loop through each of the brand's configurations and, if it's set, overwrite the original.
		foreach ($email as $key => $val) {
			if ($val != "") {
				$config->set($key, $val);
			}
		}
	}

	/**
	 * Renders a label and checkbox to agree to the Terms of Use, Member Agreement, 
	 * and Privacy policy configured in the legal tab of Brands
	 * 
	 * If Privacy Policy Required, Member Agreement Required, and Terms of Use Required radios
	 * are toggled off then this will render nothing
	 * 
	 * @param string $submit_button_id
	 * 
	 * @return void
	 */
	public static function renderAgreementCheckbox(string $submit_button_id) {
		$brand = self::getBrand();

		// If the user hasn't gone in and updated their brand since the release
		// the toggles will be unset, so we want the legacy beavior where we just detect
		// for content in these fields
		$toggles_unset = !isset($brand->legal->require_terms_of_use)
			&& !isset($brand->legal->require_member_agreement)
			&& !isset($brand->legal->require_privacy_policy);

		$no_content = !$brand->legal->terms_of_use
			&& !$brand->legal->member_agreement
			&& !$brand->legal->privacy_policy;


		// If the user has updated their brand since the release, we just check for
		// all the toggles being false to cancel the render
		$toggles_all_false = !$brand->legal->require_terms_of_use && !$brand->legal->require_member_agreement && !$brand->legal->require_privacy_policy;

		

		if (($toggles_unset && $no_content) || (!$toggles_unset && $toggles_all_false)) {
			return;
		}
			
		// Render each link as a member of this array
		$links = array();

		if ((!isset($brand->legal->require_terms_of_use) && $brand->legal->terms_of_use) || $brand->legal->require_terms_of_use) { 
			ob_start() 
			?>
				<a href="index.php?option=com_axs&view=policy&type=terms" target="_blank">
					<?php echo AxsLanguage::text('COM_SUBSCRIPTION_TERMS', 'terms'); ?>
				</a>
			<?php
			$links []= ob_get_clean();
		}

		if ((!isset($brand->legal->require_privacy_policy) && $brand->legal->privacy_policy) || $brand->legal->require_privacy_policy) { 
			ob_start() 
			?>
				<a href="index.php?option=com_axs&view=policy&type=privacy" target="_blank">
					<?php echo AxsLanguage::text('COM_SUBSCRIPTION_PRIVACY_POLICY', 'privacy policy'); ?>
				</a>
			<?php
			$links []= ob_get_clean();
		}

		if ((!isset($brand->legal->require_member_agreement) && $brand->legal->member_agreement) || $brand->legal->require_member_agreement) { 
			ob_start() 
			?>
				<a href="index.php?option=com_axs&view=policy&type=agreement"  target="_blank">
					<?php echo AxsLanguage::text('COM_SUBSCRIPTION_MEMBER_AGREEMENT', 'member contract'); ?>
				</a>
			<?php
			$links []= ob_get_clean();
		}

		?>
		<div class="form-check">
			<input class="form-check-input" type="checkbox" name="agree" autocomplete="off" />
			<label class="form-check-label"><?php echo AxsLanguage::text('COM_SUBSCRIPTION_I_ACCEPT', 'I accept the'); ?>
				<?php
					if (count($links) == 3) {
						echo $links[0] . ",&nbsp;". $links[1] . AxsLanguage::text('COM_SUBSCRIPTION_AND', 'and') . $links[2]; 
					} else if (count($links) == 2) {
						echo $links[0] . AxsLanguage::text('COM_SUBSCRIPTION_AND', 'and') . $links[1]; 
					} else {
						echo $links[0];
					}
				?>
				<span class="required">*</span>
			</label>
			<script>
				// Disable the submit button on load
				$(() => {
					$('#<?php echo $submit_button_id ?>').addClass('disabled');
					$('#<?php echo $submit_button_id ?>').prop('disabled', true);
				});

				// Listen for checkbox value change and set
				// submit button disabled based on the value
				$('input[name=agree]').change(function(ev) {
					var checked = $(ev.target).prop( "checked" )
					if (checked) {
						$('#<?php echo $submit_button_id ?>').prop('disabled', false);
						$('#<?php echo $submit_button_id ?>').removeClass('disabled');
					} else {
						$('#<?php echo $submit_button_id ?>').addClass('disabled');
						$('#<?php echo $submit_button_id ?>').prop('disabled', true);
					}
				});
			</script>
		</div>
	<?php
	}

	public static function getPaymentGatewayById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_payment_gateways');
		$query->where('id = ' . $db->quote($id));
		$db->setQuery($query);
		
		$result = $db->loadObject();

		$params = json_decode($result->params);

			$params->converge_dev_account_pin = AxsEncryption::decrypt($params->converge_dev_account_pin, AxsKeys::getKey('lms'));
			$params->converge_prod_account_pin = AxsEncryption::decrypt($params->converge_prod_account_pin, AxsKeys::getKey('lms'));
			$params->authnet_dev_transaction_key = AxsEncryption::decrypt($params->authnet_dev_transaction_key, AxsKeys::getKey('lms'));
			$params->authnet_prod_transaction_key = AxsEncryption::decrypt($params->authnet_prod_transaction_key, AxsKeys::getKey('lms'));
			$params->stripe_dev_api_key = AxsEncryption::decrypt($params->stripe_dev_api_key, AxsKeys::getKey('lms'));
			$params->stripe_prod_api_key = AxsEncryption::decrypt($params->stripe_prod_api_key, AxsKeys::getKey('lms'));
			$params->heartland_prod_secret_api_key = AxsEncryption::decrypt($params->heartland_prod_secret_api_key, AxsKeys::getKey('lms'));
			$params->heartland_dev_secret_api_key = AxsEncryption::decrypt($params->heartland_dev_secret_api_key, AxsKeys::getKey('lms'));

			$result->params = json_encode($params);

		return json_decode($result->params);
	}

	public static function updatePaymentGatewayDefaultAdminEmail($id, $val) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_payment_gateways');
		$query->where('id = ' . $db->quote($id));
		$db->setQuery($query);
		$paymentGateway = $db->loadObject();

		$params = json_decode($paymentGateway->params);
		$adminEmails = $params->admin_emails;
		$adminEmails[0]->email = $val;

		$params->admin_emails = $adminEmails;
		$paymentGateway->params = json_encode($params);

		$db->updateObject('axs_payment_gateways', $paymentGateway, 'id');		
	}

	public static function getEmailSettingsById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_email_settings');
		$query->where('id = ' . $db->quote($id));
		$db->setQuery($query);
		
		$result = $db->loadObject();

		$params = json_decode($result->params);
		if (isset($params->smtppass)) {
			$params->smtppass = AxsEncryption::decrypt($params->smtppass, AxsKeys::getKey('lms'));
		}
		return $params;
	}

	public static function updateEmailSettings($id, $key, $value) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->update('axs_email_settings');
		$query->set("params = JSON_SET(params, '$.$key', " . $db->quote($value) . ")");
		$query->where('id = ' . $db->quote($id));
		$db->setQuery($query);
		$db->execute();
	}

	public static function getIntegrationById($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('axs_integrations');
		$query->where('id = ' . $db->quote($id));
		$db->setQuery($query);
		
		$result = $db->loadObject();
		return json_decode($result->params);
	}

	/**
	 * Checks for any Brand Specific Visibility Settings and applies
	 * and applies them to $conditions to be used in a query
	 */
	public static function getCourseCategoryBrandConditions() {
		//Get the Currently Used Brand
		$brand = AxsBrands::getCurrentBrandId();
		
		$specificBrandsCheck = " ( params->>'$.brand_visibility' = 'specific' AND FIND_IN_SET( $brand, params->>'$.brand' ) > 0 ) ";
	
		$noBrandsCheck = " ( params->>'$.brand_visibility' IS NULL OR params->>'$.brand_visibility' = 'all' )";
		
		$conditions []= '(' . $specificBrandsCheck . ' OR ' . $noBrandsCheck . ')';
		
		$conditions = implode(' and ', $conditions);
	
		return $conditions;
	 }

}