<?php

class AxsGPS {
	function degreesToRadians($degrees) {

		$pi = 3.14159265;

		return $degrees * $pi / 180;
	}

	function distance($lat1, $lon1, $lat2, $lon2, $type = "km") {

		$earthRadiusKm = 6371;

		$dLat = self::degreesToRadians($lat2 - $lat1);
		$dLon = self::degreesToRadians($lon2 - $lon1);

		$lat1 = self::degreesToRadians($lat1);
		$lat2 = self::degreesToRadians($lat2);

		$a = sin($dLat / 2) * sin($dLat / 2) + sin($dLon / 2) * sin($dLon / 2) * cos($lat1)  * cos($lat2); 
		$b = 2 * atan2(sqrt($a), sqrt(1 - $a));

		$distance = $earthRadiusKm * $b;

		switch ($type) {
			case "km":
				//Already in km.
				break;
			case "mi":
				$distance /= 1.609344;
				break;
			case "ft":
				$distance *= 3280.839895;
				break;
			case "in":
				$distance *= 39370.07874;
				break;
			case "m":
				$distance *= 1000;
				break;
			case "cm":
				$distance *= 100000;
				break;
		}

		return $distance;
	}
}

//AxsGPS::distance(42.555838,-114.47,43.6187,-116.2146);