<?php

defined('_JEXEC') or die;

class AxsKeys {

	public static function getKey($keyName = null) {
		switch ($keyName) {
			case "ratings": 		return "H0POr~p6o<c*BYQD=*Bm8To];{(aE@";
			case "boards": 			return "%T^%*b$Mjx(Hiq/f|XrbA3h^'q{<!}";
			case "lms": 			return "_CLibV`!1/zXGLPz?Gi%]'=L-6!?lD";
			case "notifications":	return "_KG6bVf!1/zXGLPz?pi%]'=L-6!?lr";
			case "jDump":			return "uF9b||Oc9-*PflC+FJpV:XwEw%F5.E";
			case "clients":			return "6c}m_hYyZI]`nFTD5^y;|OpK]!NF=%";
			case "subscription":	return "c[a34K(.R4'qb[.B2T[P%(*U}KFWqV";
			case "api":	            return "H39xbedB7N&Q*WL!7jC&3aB^tAyt3c";
			case "surveys":	        return "kMHn*Rc%WMWSNga#E5A!yXcjXKYRDs";
			case "builder":	        return "f9}KJ?CtRVsxD_-XABeXJ6%*XEV&[{";
			default:				return "$C}jkc&O<#XGbijJl<duCsQC2_5o+y";
		}
	}
}