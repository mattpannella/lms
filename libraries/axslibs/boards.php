<?php

defined('_JEXEC') or die;
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class AxsBoards {

	/**
     * Method to return the requested board based on the provided ID
     *
     * @param   integer      $board_id      The ID of the board
     * @return  object                     	The requested board
     */
	public static function getBoard($board_id) {
		
		$db = JFactory::getDBO();
				
		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_boards'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id));

		$db->setQuery($query);
		$board = $db->loadObjectList();

		//The board does not exist.
		if (count($board) == 0) {
			return null;
		}

		$board = $board[0];

		//Do not return the board if it is disabled.
		if ($board->enabled == 0) {
			return null;
		}

		//jDump($board);

		$user_id = JFactory::getUser()->id;

		if (!$user_id) {
			$is_member = false;
		} else {
			$is_member = true;
		}

		if (JFactory::getUser()->authorise('core.admin', 'com_board')) {
			$is_admin = true;
		} else {
			$is_admin = false;
		}

		switch ($board->access) {
			case "public":
				//Anyone can have access to public boards
				break;
			case "member":
				//Only members can have access to member boards
				if (!$is_member) {
					return null;
				}

			case "admin":
				if (!$is_admin) {
					return null;
				}

			case "private":
				break;
		}

		$board->params = json_decode($board->params);
		$params = $board->params;
		$field_settings = json_decode($params->field_settings);
		$num_fields = count($field_settings);

		foreach ($field_settings as &$field) {
			$field->config->field_description = base64_decode($field->config->field_description);
			
			if (isset($field->settings->text)) {
				$field->settings->text = base64_decode($field->settings->text);
			}			
		}

		$params->field_settings = $field_settings;
		$board->params = $params;

		return $board;
	}

	/*public static function getPost($board_id, $post_id) {
		
	}*/


	/**
     * Validates that a board exists
     *
     * @param   integer      $board_id      The ID of the board to validate
     * @return  bool                        Does the board exist
     */
	public static function checkValidBoard($board_id) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query
			->select('*')
			->from($db->quoteName('axs_boards'))
			->where($db->quoteName('id') . '=' . $db->quote($board_id));

		$db->setQuery($query);
		$board = $db->loadObjectList();

		if (count($board) > 0) {
			return true;
		} else {
			return false;
		}
	}
}

