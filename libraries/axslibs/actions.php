<?php

defined('_JEXEC') or die;

class AxsActions {

    public static function trackUserGroup($user_id,$group_id,$action) {
        $data = new stdClass();
        $data->user_id = $user_id;
        $data->group_id = $group_id;
        $data->action = $action;
        $data->date = date('Y-m-d H:i:s');
        $db = JFactory::getDbo();
        $table = 'axs_usergroup_tracking';
        return $db->insertObject($table,$data);
    }

    public static function checkAward($badge_id,$userId) {
        $db = JFactory::getDBO();
        $conditions[] = "badge_id = ".(int)$badge_id;
        $conditions[] = "user_id = ".(int)$userId;
        $query = $db->getQuery(true);
        $query->select('*')
              ->from("axs_awards_earned")
              ->where($conditions)
              ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function storeAction($user_id,$action) {
        if(!$user_id) {
            return false;
        }
        $data = new stdClass();
        $data->user_id = (int)$user_id;
        $data->date    = date("Y-m-d H:i:s");
        $data->action  = $action;
        $table = 'axs_actions';
        $db = JFactory::getDbo();
        $db->insertObject($table,$data);
    }

    public static function storeAdminAction($params) {
        if($params->initiator == 'Tovuti System') {
            $user = new stdClass();
            $user->id = 0;
            $user->name = 'Tovuti System';
            $user->email = 'Tovuti';
        } else {
            $user = JFactory::getUser();
            if(!$user->id) {
                return false;
            }
        }
        
        $data = new stdClass();
        $data->user_id = (int)$user->id;
        $data->date    = date("Y-m-d H:i:s");
        $data->action  = $params->eventName;
        $data->action_type = $params->action_type;
        $data->description = $params->description;
        if($params->description) {
            $data->description = $params->description;
        }
        if($params->quantity) {
            $data->description = $params->quantity;
        }
        if($params->data) {
            $data->data = $params->data;
        }        
        $data->user_name = $user->name;
        $data->user_email = $user->email;
        $table = 'axs_actions_audit';
        $db = JFactory::getDbo();
        $db->insertObject($table,$data);
    }

    public static function setGroups($groupData) {
        if(!$groupData->user_id || (!$groupData->add_usergroups && !$groupData->remove_usergroups)) {
            return false;
        }
        $user_groups = JUserHelper::getUserGroups($groupData->user_id);
        foreach($groupData->add_usergroups as $addGroupId) {
            if($addGroupId != 8 && !in_array($addGroupId,$user_groups)) {
                JUserHelper::addUserToGroup($groupData->user_id,$addGroupId); 
                AxsActions::trackUserGroup($groupData->user_id,$addGroupId,'add');  
            }            
        }
        
        foreach($groupData->remove_usergroups as $removeGroupId) {
            if($removeGroupId != 8 && in_array($removeGroupId,$user_groups)) {
                JUserHelper::removeUserFromGroup($groupData->user_id,$removeGroupId);
                AxsActions::trackUserGroup($groupData->user_id,$removeGroupId,'remove');    
            }            
        }
    }

    public static function updateLoginActions($user_id,$action,$date) {
        if(!$user_id) {
            return false;
        }
        $data = new stdClass();
        $data->user_id = $user_id;
        $data->date    = $date;
        $data->action  = $action;
        $table = 'axs_actions';
        $db = JFactory::getDbo();
        $db->insertObject($table,$data);
    }

    public static function getAllLastVisits() {
        $db = JFactory::getDbo();
        $conditions[] = "lastvisitDate > 0";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__users')
              ->where($conditions);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $row) {
            $user_id = $row->id;
            $action = 'login';
            $date = $row->lastvisitDate;
            self::updateLoginActions($user_id,$action,$date);
        }
    }
}