<?php

defined('_JEXEC') or die('Restricted access');

class OAuth_Handler {

	function getAccessToken($ssoParams) {
		$ch = curl_init($ssoParams->oauth_token_endpoint);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		curl_setopt( $ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Accept: application/json',
			'Authorization: Basic ' . base64_encode( $ssoParams->oauth_client_id . ":" . $ssoParams->oauth_client_secret )
		));
		if(!$ssoParams->use_openid_connect) {
			$credentials = '&client_id='.urlencode($ssoParams->oauth_client_id).'&client_secret='.urlencode($ssoParams->oauth_client_secret);
		} else {
			$credentials = '';
		}
        curl_setopt( $ch, CURLOPT_POSTFIELDS, 'redirect_uri='.urlencode($ssoParams->oauth_callback_url).'&grant_type='.$ssoParams->grant_type.$credentials.'&code='.$ssoParams->code);
		$content = curl_exec($ch);

		if(curl_error($ch)) {
			exit( curl_error($ch) );
		}

		if(!is_array(json_decode($content, true))) {
            exit("Invalid response received.");
        }

		$content = json_decode($content,true);
		if(isset($content["error_description"])) {
			exit($content["error_description"]);
		} else if(isset($content["error"])) {
			exit($content["error"]);
		} else if(isset($content["access_token"])) {
			$access_token = $content["access_token"];
		} else {
			exit('Invalid response received from OAuth Provider. Contact your administrator for more details.');
		}

		return $access_token;
	}

	function getResourceOwner($resourceOwnerDetailsUrl, $accessToken) {
		$ch = curl_init($resourceOwnerDetailsUrl);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
		curl_setopt( $ch, CURLOPT_ENCODING, "" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt( $ch, CURLOPT_MAXREDIRS, 10 );
		curl_setopt( $ch, CURLOPT_POST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Authorization: Bearer '.$accessToken,
				'User-Agent:web'
		));

        $content = curl_exec($ch);
		if(curl_error($ch)) {
			exit( curl_error($ch) );
		}

		if(!is_array(json_decode($content, true))) {
			exit("Invalid response received.");
        }
		$content = json_decode($content,true);
		if(isset($content["error_description"])) {
			if(is_array($content["error_description"])) {
                print_r($content["error_description"]);
            } else {
                echo $content["error_description"];
            }
			exit;
		} else if(isset($content["error"])) {
			if(is_array($content["error"])) {
                print_r($content["error"]);
            } else {
                echo $content["error"];
            }
			exit;
		}

		return $content;
	}

}