<?php

defined('_JEXEC') or die;

class AxsNotifications {

	/**
	 * Load all Notifications
	 * @return type string Returns Javascript variables and notifications.js or returns FALSE
	 */

	public static function loadNotifications() {
		if(AxsDbAccess::checkAccess("notifications")) {
			$db = JFactory::getDBO();
			$query = self::buildQuery();
			$db->setQuery($query);
			$results = $db->loadObjectList();

			if(!$results) {
				return FALSE;
			}

			$notifications = array();
			$useGeofence = 'false';
			foreach ($results as $result) {
				$notificationParams = NULL;
				$buttons = NULL;
				$params = NULL;

				$notificationParams  = json_decode($result->params);
				$buttons = json_decode($notificationParams->buttons);

				$params = new stdClass();
				$params->title = $result->title;
				$params->message = $result->message;
				$params->geofence = $notificationParams->geofence;
				$params->radius_unit = $notificationParams->radius_unit;
				$params->radius_amount = $notificationParams->radius_amount;
				$params->longitude = $notificationParams->longitude;
				$params->latitude = $notificationParams->latitude;

				$params->buttons = self::buildButtons($buttons,$result->id);

				array_push($notifications, json_encode($params));

				if($params->geofence) {
					$useGeofence = 'true';
				}
			}

			$output  = '<script> var notifications = ['.implode(',',$notifications).']; var geofence = '.$useGeofence.';</script>';
			$output .= '<script src="/media/tovuti/js/notifications.js?v=6" type="text/javascript"></script>';
			return $output;
		} else {
			return FALSE;
		}
	}

	/**
	 * Build The Query
	 * @return type string Returns query with all conditions
	 */
	public static function buildQuery() {
		$userId = JFactory::getUser()->id;
		$levels = JAccess::getAuthorisedViewLevels($userId);
		$groups = JAccess::getGroupsByUser($userId);
		$levelsCheck = [];
		$groupsCheck = [];
		$conditions  = [];

		foreach($levels as $level) {
			$levelsCheck[] = " ( FIND_IN_SET( $level, accesslevel ) > 0 ) ";
		}

		foreach($groups as $group) {
			$groupsCheck[] = " ( FIND_IN_SET( $group, usergroup ) > 0 ) ";
		}

		$levelsSplit = implode(' OR ', $levelsCheck);
		$groupsSplit = implode(' OR ', $groupsCheck);
		$where   = " WHERE enabled = 1 ";

		if(isset($_COOKIE['notifications'])) {
			$viewed = preg_replace("/[^0-9,]/", "", $_COOKIE['notifications']);
			if($viewed) {
				$where  .= " AND   ( FIND_IN_SET( id, '$viewed') = 0 )";
			}
		}

		if($userId) {
			$where  .= " AND NOT EXISTS ( SELECT id FROM axs_notifications_tracking WHERE user_id = $userId AND notification_id = axs_notifications.id )";
		}

		$where  .= " AND   DATE(start_date) <= NOW() ";
		$where  .= " AND   ( DATE(end_date) > NOW() OR end_date = 0 ) ";
		$conditions[] = "  ( access_type = 'access' AND ( $levelsSplit ) ) ";
		$conditions[] = "  ( access_type = 'user'   AND ( $groupsSplit ) ) ";
		$conditions[] = "  ( access_type = 'list'   AND ( FIND_IN_SET( $userId, userlist ) > 0 ) ) ";
		$conditions[] = "  ( access_type = 'none' ) ";

		$conditionsSplit = implode(' OR ', $conditions);

		$where .= ' AND ('.$conditionsSplit.')';

		$query = "SELECT * FROM axs_notifications $where";

		return $query;

	}

	/**
	 * Build Notification Buttons
	 * @param type $buttons is an object
	 * @param type $id this is the id of the notification
	 * @return type string returns HTML Button
	 */
	public static function buildButtons($buttons,$id) {
		$output = '';
		$key = AxsKeys::getKey('notifications');
		foreach ($buttons as $item) {
			$params = NULL;
			$encryptedParams = NULL;
			// build the params to be encrypted and passed into button html
			$params = new stdClass();
			$params->notification_id = $id;
			$params->action = $item->button_action;
			$encryptedParams = base64_encode(AxsEncryption::encrypt($params, $key));
			$output .= '<span
							class="btn btn-'.$item->button_color.' notificationButton"
							data-action="'.$encryptedParams.'"
							data-url="'.$item->button_url.'"
						>
							<i class="lizicon '.$item->button_icon.'"></i>
							'.$item->button_text.'
						</span>';
		}

		if(!$buttons) {
			$params = new stdClass();
			$params->notification_id = $id;
			$params->action = "closed notification";
			$encryptedParams = base64_encode(AxsEncryption::encrypt($params, $key));
			$output .= '<span class="btn btn-primary notificationButton"
							data-action="'.$encryptedParams.'"
						>
							<i class="lizicon lizicon-checkmark2"></i>
							Close
						</span>';
		}

		return $output;
	}

    /**
     * Get the notifications associated with a given brand and optional notification type.
     * If no notification type is given, return all the notifications associated with a given brand.
     *
     * @param integer $brandId
     * @param string|null $notificationType
	 * @param boolean $onlyPublished
     * @return Array array of objects representing records from the axs_auto_alerts table associated with the brand.
     */
    public static function getNotificationsForBrand(int $brandId, ?string $notificationType, $onlyPublished = true) {

        $results = [];

        $db = JFactory::getDbo();

        $query = $db->getQuery(true);

        $conditions = [
            "(brand_id = $brandId OR brand_id = 0 OR brand_id IS NULL)"
        ];

		if ($onlyPublished) {
			$conditions []= 'enabled = 1';
		}

        if(isset($notificationType) && !empty($notificationType)) {

            $conditions[] = 'alert_type = ' . $db->quote($notificationType);
        }

        $query->select('*')
              ->from('axs_auto_alerts')
              ->where($conditions);

        $db->setQuery($query);

        try {

            $results = $db->loadObjectList();
        } catch(Exception $ex) {

            JFactory::getApplication()->enqueueMessage('Attention: Invalid database operation', 'warning');
        }

        return $results;
    }

    /**
     * Get all notifications of the type provided in the parameter
     *
     * @param string $notificationType
     * @return Array Array of matching notifications
     */
    public static function getNotificationsByType(string $notificationType) {

        $notifications = [];

        $notificationType = AxsSecurity::alphaNumericOnly($notificationType);

        if(isset($notificationType) && !empty($notificationType)) {

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query->select('*')
                  ->from('axs_auto_alerts')
                  ->where('alert_type = ' . $db->quote($notificationType));

            $db->setQuery($query);

            try {

                $notifications = $db->loadObjectList();
            } catch(Exception $ex) {
    
                JFactory::getApplication()->enqueueMessage('Attention: Invalid database operation', 'warning');
            }   
        }

        return $notifications;
    }
	 /**
     * Builds an admin notification email that is sent when a course is completed and sends it
     *
     * @param object $course_data
     * @param string $user_id
     * @param string $course_start_date
     * @param string $course_course_complete_date
     *
     * @return void
     */
    public static function sendCourseCompletedAdminEmail($course_data, $user_id, $course_start_date, $course_complete_date) {
        // Get the admins email and other info
		$course_params = json_decode($course_data->params);
		if (empty($course_params->admin_id)) {
			return;
		}
        $admin_data = AxsUser::getUser($course_params->admin_id);
        $student_data = AxsUser::getUser($user_id);
		if (empty($admin_data->email)) {
			return;
		}

		$admin_name = $admin_data->name;
		$course_title = $course_data->title;
		$course_id = $course_data->splms_course_id;
		$student_name = $student_data->name;

        // Build message content
        $message_content = "
			<div style='white-space:pre'>
				Hello $admin_name,

				The course $course_title has been completed by $student_name.
				Date Started: $course_start_date
				Date Completed: $course_complete_date
			</div>
			";
		$message_content = trim(preg_replace('/[\t]+/', '', $message_content));

		// Build send configuration and send
        $emailData = new stdClass();
        $emailData->subject = "Course $course_title has been completed by $student_name";
        $emailData->content = $message_content;
        $emailData->brand_id = AxsBrands::getBrand()->id;
        $emailData->type = 'course_completion';
		$recipientData = new stdClass();
		$recipientData->name  =  $admin_data->name;
		$recipientData->email =  $admin_data->email;
		$emailData->recipients = json_encode(array($recipientData));

        AxsEmail::addToEmailQueue($emailData);
    }



	 /**
     * Builds an admin notification email that is sent when a checklist is completed and sends it
     *
     * @param string $user_id
	 * @param object $checklist_data
     *
     * @return void
     */
    public static function sendChecklistCompletedAdminEmail(string $user_id, object $checklist_data) {
        // Get the admins email and other info
		$checklist_params = json_decode($checklist_data->params);
        $admin_data = AxsUser::getUser($checklist_params->admin_id);
        $student_data = AxsUser::getUser($user_id);

		if (empty($admin_data->email)) {
			return;
		}

		if (in_array($user_id, $checklist_params->user_ids_emails_sent)) {
			return;
		} else {
			if (empty($checklist_params->user_ids_emails_sent)) {
				$checklist_params->user_ids_emails_sent = array();
			}
			$checklist_params->user_ids_emails_sent []= $user_id;
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->update('axs_checklists');
			$query->set('params = ' . $db->quote(json_encode($checklist_params)));
			$query->where('id = ' . $db->quote($checklist_data->id));
			$db->setQuery($query);
			$db->execute();
		}

		$admin_name = $admin_data->name;
		$checklist_title = $checklist_data->title;
		$checklist_id = $checklist_data->id;
		$student_name = $student_data->name;
		$checklist_title = $checklist_data->title;
		$date_completed = date('Y-m-d H:i:s');
		// Build message content
        $message_content = "
			<div style='white-space:pre'>
				Hello $admin_name,
				The checklist $checklist_title has been completed by $student_name.
				Date Completed: $date_completed
			</div>
		";
		$message_content = trim(preg_replace('/[\t]+/', '', $message_content));

        // Build send configuration and send
        $emailData = new stdClass();
        $emailData->subject = "Checklist $checklist_title has been completed by $student_name";
        $emailData->content = $message_content;
        $emailData->brand_id = AxsBrands::getBrand()->id;
        $emailData->type = 'checklist_completion';
		$recipientData = new stdClass();
		$recipientData->name  =  $admin_data->name;
		$recipientData->email =  $admin_data->email;
		$emailData->recipients = json_encode(array($recipientData));
        AxsEmail::addToEmailQueue($emailData);
    }

	private static function getActivityNameFromType($type) {
		$map = array(
			"powerpoint" => "PowerPoint Presentation",
			"pdf" => "PDF Document",
			"video" => "Video Player",
			"audio" => "Audio Player",
			"interactive" => "Interactive Content",
			"survey" => "Survey",
			"embed" =>"Custom Embed",
			"textbox" =>"Text Box",
			"upload" =>"File Upload",
			"mp4" =>"MP4 Video Upload",
			"mp3" =>"MP3 Audio Upload",
			"youtube" =>"Submit YouTube Video",
			"vimeo" =>"Submit Vimeo Video",
			"facebook" =>"Submit Facebook Video",
			"screencast" =>"Submit Screencast Video",
			"link" =>"Submit External Link"
		);
		return $map[$type];
	}

	/**
     * Builds an admin approval required email using activity_data and sends it
     * 
	 * @param object $lesson_data - a json object representing a lesson row from the joom_splms_lessons table
     * @param object $activity_data - a json object representing a student_activity from the
     *                                  student_activities subform
     * 
     * @return void
     */
    public static function sendActivityAdminApprovalEmail($lesson_data, $activity_data) {
		$lesson_params = json_decode($lesson_data->params);
		if (empty($lesson_params->approval_admin_id)) {
			return;
		}
        // Get the admins email and other info
        $admin_data = AxsUser::getUser($lesson_params->approval_admin_id);
		if (empty($admin_data->email)) {
			return;
		}
        $student_data = JFactory::getUser();
		
        // Build message content
		$admin_name = $admin_data->name;
		$lesson_title = $lesson_data->title;
		$lesson_id = $lesson_data->splms_lesson_id;
		$activity_title = $activity_data->title;
		if (empty($activity_title)) {
			$activity_title = self::getActivityNameFromType($activity_data->type);
		}
		$activity_id = $activity_data->id;
		$student_name = $student_data->name;
		$date_completed = date('Y-m-d H:i:s');
        $message_content = "
			<div style='white-space:pre'>
				Hello $admin_name,
				An activity has been completed and is awaiting your approval.
				Lesson: $lesson_title
				Activity: $activity_title
				Student Name: $student_name
				Date Submitted: $date_completed
			</div>
		";
		$message_content = trim(preg_replace('/[\t]+/', '', $message_content));

        // Build send configuration and send
        $emailData = new stdClass();
        $emailData->subject = "Activity submission by $student_name is waiting your approval";
        $emailData->content = $message_content;
        $emailData->brand_id = AxsBrands::getBrand()->id;
        $emailData->type = 'activity_completion';
		$recipientData = new stdClass();
		$recipientData->name  =  $admin_data->name;
		$recipientData->email =  $admin_data->email;
		$emailData->recipients = json_encode(array($recipientData));
        AxsEmail::addToEmailQueue($emailData);
    }
}
