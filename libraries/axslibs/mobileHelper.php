<?php

defined('_JEXEC') or die;

class AxsMobileHelper {

    /**
     * Determines whether or not the current user is using a mobile device.
     * An array of mobile devices or a string containing a single
     * device can be provided for a custom set of devices to check for.
     *
     * Some commonly used devices within user agents: Android, iPhone, iPad
     *
     * @param array $mobileDeviceList List of mobile devices that need to be checked for
     * @return boolean Whether or not the current client is using a mobile device
     */
    public static function isClientUsingMobile($mobileDeviceList = null) {

        $defaultMobileDevices = [
            'Android',
            'iPad',
            'iPhone'
        ];

        // Assume we're not on mobile by default
        $isOnMobileDevice = false;

        if(empty($mobileDeviceList)) {

            // We want to use the default mobile device set
            $matchString = implode('|', $defaultMobileDevices);
            $isOnMobileDevice = preg_match("/($matchString)/i", $_SERVER['HTTP_USER_AGENT']);
        } elseif(is_array($mobileDeviceList)) {

            // Otherwise, use the mobile device list passed in as an array
            $matchString = implode('|', $mobileDeviceList);
            $isOnMobileDevice = preg_match("/($matchString)/i", $_SERVER['HTTP_USER_AGENT']);
        } else {

            // We're dealing with a single device in a string, so a simple substring lookup should suffice.
            $isOnMobileDevice = stristr($_SERVER['HTTP_USER_AGENT'], $mobileDeviceList);
        }

        return $isOnMobileDevice;
    }
}