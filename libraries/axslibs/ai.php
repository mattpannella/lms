<?php

defined('_JEXEC') or die;

class AxsAi {

    public static function getImageData($image,$endpoint,$params) {
	
		$getParameters = '';

		foreach ($params as $key => $value) {
			$getParameters .= $key.'='.$value.'&';
		}

		$getParameters = rtrim($getParameters, '&');
		$url = "https://api.projectoxford.ai/vision/v1.0/".$endpoint."?".$getParameters;
		
		$curl = curl_init($url);
	    $body = '{"url":"'.$image.'"}';
	    //curl_setopt($curl, CURLOPT_URL, "https://api.projectoxford.ai/vision/v1.0/describe?maxCandidates=1");
	    //curl_setopt($curl, CURLOPT_URL, "https://api.projectoxford.ai/vision/v1.0/ocr?language=unk&detectOrientation=true");
	    curl_setopt($curl, CURLOPT_POST, 1);
	    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Ocp-Apim-Subscription-Key: 4fd0a3a6753c497bafe3f4b27ae6566a'));
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($curl, CURLOPT_VERBOSE, false);
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($curl, CURLOPT_POSTFIELDS,$body);
	    $result = curl_exec($curl);
	    curl_close($curl);
	    
	    return $result;
	    

	}

	public static function saveImageData($data) {
		
    	$db = JFactory::getDbo();

	    $db->insertObject('axs_ai_images', $data);

	}

	public static function getImageText($json) {
		
		$text = json_decode($json);
		$result = '';
 
	    foreach ($text->regions as $obj) {
	       
	        foreach ($obj->lines as $line) {
	            
	            foreach ($line->words as $word) {
	                
	            	$result .= $word->text.' ';
	                 
	            }
	           		$result .= '<br/>';
	        }
	       
	    }

	    return $result;
	}

	public static function checkAdultContent($json) {
		
		$check = json_decode($json);
		
		if ( ($check->adult->isAdultContent) || ($check->adult->isRacyContent) ) {
	    
	    	return 1;
		
		} else {

			return 0;

		}
	}


}

?>

