<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

//defined('_JEXEC') or die;
require_once('scorm/ScormEngineService.php');
require_once('scorm/ServiceRequest.php');
require_once('scorm/CourseData.php');

class AxsScormLMS {
	public  $registrationID;
	public  $courseID;
	public  $CourseAsyncUrl;
	public  $returnUrl;
	public  $baseURL;	
	public  $returnService;
	public  $ServiceUrl;	
	public  $ScormService;
	public  $courseService;
	private $AppId;
	private $SecretKey;
	private $Origin;
	private $clientID;

	public function __construct($params = null) {

		//$base = JUri::base();
		$this->baseURL = str_replace("/administrator/", "", JUri::base());
		require_once('scorm/config.php');
		$this->ServiceUrl = $CFG->scormcloudurl;
		$this->AppId = $CFG->scormcloudappid;
		$this->SecretKey = $CFG->scormcloudsecretkey;
		$this->Origin = $CFG->scormcloudorigin;
		$this->courseID = uniqid();
		$this->baseURL = $CFG->wwwroot;

		foreach($params as $key => $value) {
  			$this->$key = $value;
  		}

		$this->ScormService = new ScormEngineService($this->ServiceUrl,$this->AppId,$this->SecretKey,$this->Origin);
		$this->courseService = $this->ScormService->getCourseService();
	}

	public function getStudentProgress($registrationID = null) {
		$regService = $this->ScormService->getRegistrationService();
		$regResults = $regService->GetRegistrationResult($registrationID,0,'xml');	
		$xmlResults = simplexml_load_string($regResults);
		return $xmlResults->registrationreport;
	}

	public function getCourseSetup($service) {		
  		$course = new stdClass();
		$course->returnService = self::getReturnService($service);
		$course->returnUrl  = $this->baseURL."/".$course->returnService.".php";		
		$course->CourseAsyncUrl = $this->courseService->GetImportCourseAsyncUrl($this->courseID, $course->returnUrl);
		return $course;
	}

	public function getCourse($id) {		
  		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();        
        $conditions[] = $db->quoteName('id') . "=" . $db->quote($id);       
        $query
            ->select('*')
            ->from($db->quoteName('#__splms_scorm_library'))
            ->where($conditions)          
            ->limit(1);
        $db->setQuery($query);
        $results = $db->loadObject();        
        return $results;
	}

	public function loadScormContent($params, $startButton) {

										
		if($params->user_id) {
	        $returnURL = $this->getCourseSetup()->returnUrl;
	        $registration = $this->getScormRegistration($params);
	        $startText = 'Start Course';
	        if($params->lesson_id) {
	        	$startText = 'Start Lesson';
	        }
	        if($registration) {
				$regService = $this->ScormService->getRegistrationService();
				$launchUrl = $regService->GetLaunchUrl($registration->registration_id,$returnURL."?courseid=".$registration->scorm_course_id);
				$html = '<iframe src="'.$launchUrl.'" id="player" style="width:100%; height:700px;" frameborder="0" ></iframe>';
			} else {
				//$key = AxsKeys::getKey('lms');
				//$encryptedParams = base64_encode(AxsEncryption::encrypt($params, $key));
				$encryptedParams = base64_encode(json_encode($params));

				if($startButton) {					
					$html  = '<form action="/index.php?option=com_splms&task=scorm.createCourseRegistration" method="post">';
					$html .= JHtml::_('form.token');
					$html .= '<input type="hidden" name="data" value="'.$encryptedParams.'"/>';
					$html .= '<input type="hidden" name="id" value="'.$params->course_id.'"/>';
					$html .= '<center><button type="submit" class="btn btn-success btn-lg"><i class="fa fa-graduation-cap"></i> '.$startText.'</button></center>';
					$html .= '</form>';
				} else {
					$url = urlencode($_SERVER['REQUEST_URI']);
					header("Location: /index.php?option=com_splms&task=scorm.createCourseRegistration&data=".$encryptedParams."&url=".$url);
				}
			}
			return $html;
		} else {
			return FALSE;
		}
	}

	public function getScormRegistration($params) {
		$db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $conditions = array();
        foreach($params as $key => $value) {
  			$conditions[] = $db->quoteName($key) . "=" . $db->quote($value);
  		}
        $query
            ->select('*')
            ->from($db->quoteName('#__splms_scorm_registrations'))
            ->where($conditions)          
            ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();       
        return $result;
    }


	public function getReturnService($service = null) {
		switch ($service) {
			case 'upload':
				$file = 'ScormReturn';
			break;
			
			default:
				$file = 'ScormReturn';
			break;
		}
		return $file;
	}

}
/*$courseInfo = new stdClass();
$courseInfo->courseID = 12345678;

$scormLMS = new AxsScormLMS($courseInfo);
echo $scormLMS->baseURL."<hr/>";
$course = $scormLMS->getCourseSetup('upload');

echo $course->returnService."<hr/>";
echo $course->metadata."<hr/>";*/
//$result = $scormLMS->getStudentProgress('12836323825a31a0c0ae1364.24636235');
//echo $result->complete.'<br/>'.$result->success;