<?php

defined('_JEXEC') or die;

class AxsVirtualClassroom {


	/* private static $domain = 'https://meet.tovuti.io';
	private static $_securitySalt = 'df739f3c07051e46ea926651d8d9f9a9';
	private static $_bbbServerBaseUrl = 'https://meet.tovuti.io/bigbluebutton/'; */

	private  $domain = 'https://meeting.tovuti.io';
	private  $_securitySalt = 'gOo4ffKWZAfple4ZOvt1FcSmHcazQWKNYghch9I6kE';
	private  $_bbbServerBaseUrl = 'https://meeting.tovuti.io/bigbluebutton/';



    private $presentations = [];

	public function __construct($params = null) {
		if($params) {
			$this->domain = $params->domain;
			$this->_securitySalt = $params->_securitySalt;
			$this->_bbbServerBaseUrl = $params->_bbbServerBaseUrl;
		}
	}

	public static function getMeetingDropDown($selected_option, $selectorAppendString = null, $firstOption = 'Select Virtual Meeting') {
		$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query = "SELECT * FROM `#__bbb_meetings` WHERE `enabled`= 1";
    	$db->setQuery($query);
		$meetings = $db->loadObjectList();
		if($meetings) {
			$html  = '<select name="virtual_meeting' . $selectorAppendString . '" id="virtual_meeting' . $selectorAppendString . '">';
			$html .= '<option value="">'.$firstOption.'</option>';
			foreach($meetings as $option) {
				if($option->meetingId == $selected_option) {
					$selected = "selected";
				} else {
					$selected = "";
				}
				$html .= '<option value="'.$option->meetingId.'" '.$selected.'>'.$option->meetingName.'</option>';
			}
			$html .= '</select>';
		} else {
			$html = "No Virtual Meetings Found";
		}

		return $html;
	}

	public function getMeetingById($id) {
		$db = JFactory::getDbo();
    	$query = $db->getQuery(true);
    	$query = "SELECT * FROM `#__bbb_meetings` WHERE `meetingId`=".$db->quote($id);
    	$db->setQuery($query);
		$data = $db->loadObject();
		return $data;
	}

	public function getDomain() {
		return $this->domain;
	}

	public function get_securitySalt() {
		return $this->_securitySalt;
	}

	public function get_bbbServerBaseUrl() {
		return $this->_bbbServerBaseUrl;
	}

	public function getTimeZones() {
		$zones_array = array();
		$timestamp = time();
		foreach(timezone_identifiers_list() as $key => $zone) {
			date_default_timezone_set($zone);
			$zones_array[$key]['zone'] = $zone;
			$zones_array[$key]['diff_from_GMT'] = '(GMT ' . date('P', $timestamp) . ')';
		}
		return $zones_array;
	}

	public function checkHostAccess($userId,$meetingId) {
		$meeting = $this->getMeetingById($meetingId);
		$hosts   = explode(',', $meeting->hosts);
		if(in_array($userId, $hosts) || $userId == (int)$meeting->creator) {
			return true;
		}
		return false;
	}

	public function getMeeting($virtualClassParams) {

		//$input = JFactory::getApplication()->input;
		/*$id = $input->get('meetingID');
		$username = $input->get('username');
		$password = $input->get('password');*/
		$get = $this->meeting($virtualClassParams);
		$output = new stdClass();
		if (preg_match("/meetingID/",$get)) {
			$output->status = "yes";
			$output->url = $get;
		} elseif ($get && $get != "Sorry password is incorrect") {
			$output->status = "yes";
			$output->url = $get;
		} else {
			$output->status = "no";
			$output->message = $get;
		}
		return json_encode($output);
	}


	private function _processXmlResponse($url, $xml = ''){
	/*
	A private utility method used by other public methods to process XML responses.
	*/
		$xml = $this->getPresentationsAsXML();

		if (extension_loaded('curl')) {
			$ch = curl_init() or die ( curl_error() );
			$timeout = 10;
			curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt( $ch, CURLOPT_URL, $url );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
			curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, $timeout);
			if(!empty($xml)){
				curl_setopt($ch, CURLOPT_HEADER, 0);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                       'Content-type: application/xml',
                                       'Content-length: ' . strlen($xml)
                                     ));
			}
			$data = curl_exec( $ch );
			curl_close( $ch );

			if($data)
				return (new SimpleXMLElement($data));
			else
				return false;
		}
		if(!empty($xml))
			throw new Exception('Set xml, but curl does not installed.');

		return (simplexml_load_file($url));
	}

	private function _requiredParam($param, $name = '') {
		/* Process required params and throw errors if we don't get values */
		if ((isset($param)) && ($param != '')) {
			return $param;
		}
		elseif (!isset($param)) {
			throw new Exception('Missing parameter.');
		}
		else {
			throw new Exception(''.$name.' is required.');
		}
	}

	private function _optionalParam($param) {
		/* Pass most optional params through as set value, or set to '' */
		/* Don't know if we'll use this one, but let's build it in case. */
		if ((isset($param)) && ($param != '')) {
			return $param;
		}
		else {
			$param = '';
			return $param;
		}
	}

	/* __________________ BBB ADMINISTRATION METHODS _________________ */
	/* The methods in the following section support the following categories of the BBB API:
	-- create
	-- join
	-- end
	*/

	public function addPresentation($nameOrUrl, $content = null, $filename = null) {
        if (!$filename) {
            $this->presentations[$nameOrUrl] = !$content ?: base64_encode($content);
        } else {
            $this->presentations[$nameOrUrl] = $filename;
        }

        return $this;
	}

	public function getPresentations() {
        return $this->presentations;
	}

	public function getPresentationsAsXML() {
        $result = '';

        if (!empty($this->presentations)) {
            $xml    = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><modules/>');
            $module = $xml->addChild('module');
            $module->addAttribute('name', 'presentation');

            foreach ($this->presentations as $nameOrUrl => $content) {
                if (strpos($nameOrUrl, 'http') === 0) {
                    $presentation = $module->addChild('document');
                    $presentation->addAttribute('url', $nameOrUrl);
                    if (is_string($content)) {
                        $presentation->addAttribute('filename', $content);
                    }
                } else {
                    $document = $module->addChild('document');
                    $document->addAttribute('name', $nameOrUrl);
                    $document[0] = $content;
                }
            }
            $result = $xml->asXML();
        }

        return $result;
    }

	public function getCreateMeetingUrl($creationParams) {
		/*
		USAGE:
		(see $creationParams array in createMeetingArray method.)
		*/
		$this->addPresentation('https://www.tovuti.io/images/STBBQ1NYcVlrL1h2T3h3c3lYRU1sUT09OjBiMDg/lms/default.pdf');
		$this->_meetingId = $this->_requiredParam($creationParams['meetingId'], 'meetingId');
		$this->_meetingName = $this->_requiredParam($creationParams['meetingName'], 'meetingName');
		// Set up the basic creation URL:

		$creationUrl = $this->_bbbServerBaseUrl."api/create?";
		$meeting = $this->getMeetingById($creationParams['meetingId']);
		$meetingParams = json_decode($meeting->settings);
		if($meetingParams->settings_type == 'global') {
			$globalSettings = json_decode(AxsLMS::getSettings()->params);
			if($globalSettings) {
				$meetingGlobalSettings = json_decode($globalSettings->virtual_classroom);
			}
			if($meetingGlobalSettings) {
				$meetingParams = $meetingGlobalSettings;
				if($meetingParams->record) {
					$creationParams['record'] = "true";
				} else {
					$creationParams['record'] = "false";
				}
			}
		}
		// Add params:
		$params =
		'name='.urlencode($this->_meetingName).
		'&meetingID='.urlencode($this->_meetingId).
		'&attendeePW='.urlencode($creationParams['attendeePw']).
		'&moderatorPW='.urlencode($creationParams['moderatorPw']).
		'&dialNumber='.urlencode($creationParams['dialNumber']).
		'&voiceBridge='.urlencode($creationParams['voiceBridge']).
		'&webVoice='.urlencode($creationParams['webVoice']).
		'&logoutURL='.urlencode($creationParams['logoutUrl']).
		'&maxParticipants='.urlencode($creationParams['maxParticipants']).
		'&record='.urlencode($creationParams['record']).
		'&duration='.urlencode($creationParams['duration']);


		//'&meta_endCallbackUrl='.urlencode($creationParams['callbackUrl']);

		if($meetingParams->muteOnStart) {
			$params .= '&muteOnStart=true';
		}
		if($meetingParams->allowModsToUnmuteUsers) {
			$params .= '&allowModsToUnmuteUsers=true';
		}
		if($meetingParams->webcamsOnlyForModerator) {
			$params .= '&webcamsOnlyForModerator=true';
		} else {
			$params .= '&webcamsOnlyForModerator=false';
		}
		if($meetingParams->lockSettingsDisableCam) {
			$params .= '&lockSettingsDisableCam=true';
		}
		if($meetingParams->lockSettingsDisableMic) {
			$params .= '&lockSettingsDisableMic=true';
		}
		if($meetingParams->lockSettingsDisablePrivateChat) {
			$params .= '&lockSettingsDisablePrivateChat=true';
		}
		if($meetingParams->lockSettingsDisablePublicChat) {
			$params .= '&lockSettingsDisablePublicChat=true';
		}
		if($meetingParams->lockSettingsDisableNote) {
			$params .= '&lockSettingsDisableNote=true';
		}
		if($meetingParams->welcome) {
			$params .= '&welcome='.urlencode($meetingParams->welcome);
		} else {
			$params .= '&welcome='.urlencode("Welcome to ".$this->_meetingName);
		}
		if($meetingParams->moderatorOnlyMessage) {
			$params .= '&moderatorOnlyMessage='.urlencode($meetingParams->moderatorOnlyMessage);
		}

		if($meetingParams->useCustomLogo && $meetingParams->customLogo) {
			$siteUrl = JURI::root();
			$params .= '&logo='.urlencode($siteUrl.$meetingParams->customLogo);
		}
		// Return the complete URL:
		return ( $creationUrl.$params.'&checksum='.sha1("create".$params.$this->_securitySalt) );
	}

	public function createMeetingWithXmlResponseArray($creationParams, $xml = '') {
		/*
		USAGE:
		$creationParams = array(
			'name' => 'Meeting Name',	-- A name for the meeting (or username)
			'meetingId' => '1234',		-- A unique id for the meeting
			'attendeePw' => 'ap',  		-- Set to 'ap' and use 'ap' to join = no user pass required.
			'moderatorPw' => 'mp', 		-- Set to 'mp' and use 'mp' to join = no user pass required.
			'welcomeMsg' => '', 		-- ''= use default. Change to customize.
			'dialNumber' => '', 		-- The main number to call into. Optional.
			'voiceBridge' => '12345', 	-- 5 digit PIN to join voice conference.  Required.
			'webVoice' => '', 			-- Alphanumeric to join voice. Optional.
			'logoutUrl' => '', 			-- Default in bigbluebutton.properties. Optional.
			'maxParticipants' => '-1', 	-- Optional. -1 = unlimitted. Not supported in BBB. [number]
			'record' => 'false', 		-- New. 'true' will tell BBB to record the meeting.
			'duration' => '0', 			-- Default = 0 which means no set duration in minutes. [number]
			'meta_category' => '', 		-- Use to pass additional info to BBB server. See API docs to enable.
		);
		$xml = '';				-- Use to pass additional xml to BBB server. Example, use to Preupload Slides. See API docs.
		*/
		$xml = $this->_processXmlResponse($this->getCreateMeetingURL($creationParams), $xml);
		if($xml) {
			if($xml->meetingID)
				return array(
					'returncode' => $xml->returncode,
					'message' => $xml->message,
					'messageKey' => $xml->messageKey,
					'meetingId' => $xml->meetingID,
					'attendeePw' => $xml->attendeePW,
					'moderatorPw' => $xml->moderatorPW,
					'hasBeenForciblyEnded' => $xml->hasBeenForciblyEnded,
					'createTime' => $xml->createTime
					);
			else
				return array(
					'returncode' => $xml->returncode,
					'message' => $xml->message,
					'messageKey' => $xml->messageKey
					);
		}
		else {
			return null;
		}
	}

	public function getJoinMeetingURL($joinParams) {
		/* $user 			  = JFactory::getUser();
		$profile 		  = CFactory::getUser($user->id);
		$profileImage     = $profile->getAvatar(); */
		/*
		NOTE: At this point, we don't use a corresponding joinMeetingWithXmlResponse here because the API
		doesn't respond on success, but you can still code that method if you need it. Or, you can take the URL
		that's returned from this method and simply send your users off to that URL in your code.
		USAGE:
		$joinParams = array(
			'meetingId' => '1234',		-- REQUIRED - A unique id for the meeting
			'username' => 'Jane Doe',	-- REQUIRED - The name that will display for the user in the meeting
			'password' => 'ap',			-- REQUIRED - The attendee or moderator password, depending on what's passed here
			'createTime' => '',			-- OPTIONAL - string. Leave blank ('') unless you set this correctly.
			'userID' => '',				-- OPTIONAL - string
			'webVoiceConf' => ''		-- OPTIONAL - string
		);
		*/
		$siteUrl = JURI::root();
		$this->_meetingId = $this->_requiredParam($joinParams['meetingId'], 'meetingId');
		$this->_username  = $this->_requiredParam($joinParams['username'], 'username');
		$this->_password  = $this->_requiredParam($joinParams['password'], 'password');
		$this->_userID    = $this->_requiredParam($joinParams['userID'], 'userID');

		$meeting = $this->getMeetingById($joinParams['meetingId']);
		$meetingParams = json_decode($meeting->settings);
		if($meetingParams->settings_type == 'global') {
			$globalSettings = json_decode(AxsLMS::getSettings()->params);
			if($globalSettings) {
				$meetingGlobalSettings = json_decode($globalSettings->virtual_classroom);
			}
			if($meetingGlobalSettings) {
				$meetingParams = $meetingGlobalSettings;
			}
		}
		// Establish the basic join URL:
		$joinUrl = $this->_bbbServerBaseUrl."api/join?";
		// Add parameters to the URL:
		$params =
		'meetingID='.urlencode($this->_meetingId).
		'&fullName='.urlencode($this->_username).
		'&password='.urlencode($this->_password).
		'&userID='.urlencode($this->_userID);
		if($meetingParams->default_layout != 'presentation') {
			$params .= '&userdata-autoSwapLayout='.urlencode('true');
		}
		$params .= '&userdata-askForFeedbackOnLogout=false';

		if($meetingParams->useCustomLogo && $meetingParams->customLogo) {
			$params .= '&userdata-bbb_display_branding_area=true';
			$params .= '&userdata-bbb_custom_style='.urlencode("[class^='branding--'] img{display:none;} [class^='branding--'] {background:url(".$siteUrl.$meetingParams->customLogo."); width:100%; height:70px;background-repeat: no-repeat;background-size: contain; background-position: center; margin-top: 5px; margin-bottom:0px;}[class^='navbar--'] [class^='item--']:nth-child(3){display:none;} [class^='navbar--']  [class^='item--']:nth-child(4){display:none;} ");
		} else {
			$params .= '&userdata-bbb_custom_style='.urlencode("[class^='navbar--']  [class^='item--']:nth-child(3){display:none;}[class^='navbar--']  [class^='item--']:nth-child(4){display:none;} ");
		}
		$params .= '&userdata-bbb_skip_check_audio=true';
		$params .= '&userdata-bbb_client_title='.urlencode('Tovuti Virtual Classroom');
		$params .= '&webVoiceConf='.urlencode($joinParams['webVoiceConf']);
		// Only use createTime if we really want to use it. If it's '', then don't pass it:
		if (((isset($joinParams['createTime'])) && ($joinParams['createTime'] != ''))) {
			$params .= '&createTime='.urlencode($joinParams['createTime']);
		}
		// Return the URL:
		return ($joinUrl.$params.'&checksum='.sha1("join".$params.$this->_securitySalt));
	}

	public function getEndMeetingURL($endParams) {
		/* USAGE:
		$endParams = array (
			'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
			'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
		);
		*/
		$this->_meetingId = $this->_requiredParam($endParams['meetingId'], 'meetingId');
		$this->_password = $this->_requiredParam($endParams['password'], 'password');
		$endUrl = $this->_bbbServerBaseUrl."api/end?";
		$params =
		'meetingID='.urlencode($this->_meetingId).
		'&password='.urlencode($this->_password);
		return ($endUrl.$params.'&checksum='.sha1("end".$params.$this->_securitySalt));
	}

	public function endMeetingWithXmlResponseArray($endParams) {
		/* USAGE:
		$endParams = array (
			'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
			'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
		);
		*/
		$xml = $this->_processXmlResponse($this->getEndMeetingURL($endParams));
		if($xml) {
			return array(
				'returncode' => $xml->returncode,
				'message' => $xml->message,
				'messageKey' => $xml->messageKey
				);
		}
		else {
			return null;
		}

	}

	/* __________________ BBB MONITORING METHODS _________________ */
	/* The methods in the following section support the following categories of the BBB API:
	-- isMeetingRunning
	-- getMeetings
	-- getMeetingInfo
	*/

	public function getIsMeetingRunningUrl($meetingId) {
		/* USAGE:
		$meetingId = '1234'		-- REQUIRED - The unique id for the meeting
		*/
		$this->_meetingId = $this->_requiredParam($meetingId, 'meetingId');
		$runningUrl = $this->_bbbServerBaseUrl."api/isMeetingRunning?";
		$params =
		'meetingID='.urlencode($this->_meetingId);
		return ($runningUrl.$params.'&checksum='.sha1("isMeetingRunning".$params.$this->_securitySalt));
	}

	public function isMeetingRunningWithXmlResponseArray($meetingId) {
		/* USAGE:
		$meetingId = '1234'		-- REQUIRED - The unique id for the meeting
		*/
		$xml = $this->_processXmlResponse($this->getIsMeetingRunningUrl($meetingId));
		if($xml) {
			return $xml;
		}
		else {
			return null;
		}

	}

	public function getGetMeetingsUrl() {
		/* Simply formulate the getMeetings URL
		We do this in a separate function so we have the option to just get this
		URL and print it if we want for some reason.
		*/
		$getMeetingsUrl = $this->_bbbServerBaseUrl."api/getMeetings?checksum=".sha1("getMeetings".$this->_securitySalt);
		return $getMeetingsUrl;
	}

	public function getMeetingsWithXmlResponseArray() {
		/* USAGE:
		We don't need to pass any parameters with this one, so we just send the query URL off to BBB
		and then handle the results that we get in the XML response.
		*/
		$xml = $this->_processXmlResponse($this->getGetMeetingsUrl());
		if($xml) {
			// If we don't get a success code, stop processing and return just the returncode:
			if ($xml->returncode != 'SUCCESS') {
				$result = array(
					'returncode' => $xml->returncode
				);
				return $result;
			}
			elseif ($xml->messageKey == 'noMeetings') {
				/* No meetings on server, so return just this info: */
				$result = array(
					'returncode' => $xml->returncode,
					'messageKey' => $xml->messageKey,
					'message' => $xml->message
				);
				return $result;
			}
			else {
				// In this case, we have success and meetings. First return general response:
				$result = array(
					'returncode' => $xml->returncode,
					'messageKey' => $xml->messageKey,
					'message' => $xml->message
				);
				// Then interate through meeting results and return them as part of the array:
				foreach ($xml->meetings->meeting as $m) {
					$result[] = array(
						'meetingId' => $m->meetingID,
						'meetingName' => $m->meetingName,
						'createTime' => $m->createTime,
						'attendeePw' => $m->attendeePW,
						'moderatorPw' => $m->moderatorPW,
						'hasBeenForciblyEnded' => $m->hasBeenForciblyEnded,
						'running' => $m->running
						);
					}
				return $result;
			}
		}
		else {
			return null;
		}

	}

	public function getMeetingInfoUrl($meetingParams) {
		/* USAGE:
		$infoParams = array(
			'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
			'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
		);
		$meetingParams->meetingId
		$meetingParams->moderatorPW
		*/
		$this->_meetingId = $this->_requiredParam($meetingParams->meetingId, 'meetingId');
		$this->_password = $this->_requiredParam($meetingParams->moderatorPW, 'password');
		$infoUrl = $this->_bbbServerBaseUrl."api/getMeetingInfo?";
		$params =
		'meetingID='.urlencode($this->_meetingId).
		'&password='.urlencode($this->_password);
		return ($infoUrl.$params.'&checksum='.sha1("getMeetingInfo".$params.$this->_securitySalt));
	}

	public function getMeetingInfo($meetingParams) {
    	$itsAllGood = true;
		try {$result = $this->getMeetingInfoWithXmlResponseArray($meetingParams);}
			catch (Exception $e) {
				echo 'Caught exception: ', $e->getMessage(), "\n";
				$itsAllGood = false;
			}
		if ($itsAllGood == true) {
			return $result;
		}
    }

	public function getMeetingInfoWithXmlResponseArray($meetingParams) {
		/* USAGE:
		$infoParams = array(
			'meetingId' => '1234',		-- REQUIRED - The unique id for the meeting
			'password' => 'mp'			-- REQUIRED - The moderator password for the meeting
		);
		*/
		$xml = $this->_processXmlResponse($this->getMeetingInfoUrl($meetingParams));
		if($xml) {
			return $xml;
			// If we don't get a success code or messageKey, find out why:
			/*if (($xml->returncode != 'SUCCESS') || ($xml->messageKey == null)) {
				$result = array(
					'returncode' => $xml->returncode,
					'messageKey' => $xml->messageKey,
					'message' => $xml->message
				);
				return $result;
			}
			else {
				// In this case, we have success and meeting info:
				$result = array(
					'returncode' => $xml->returncode,
					'meetingName' => $xml->meetingName,
					'meetingId' => $xml->meetingID,
					'createTime' => $xml->createTime,
					'voiceBridge' => $xml->voiceBridge,
					'attendeePw' => $xml->attendeePW,
					'moderatorPw' => $xml->moderatorPW,
					'running' => $xml->running,
					'recording' => $xml->recording,
					'hasBeenForciblyEnded' => $xml->hasBeenForciblyEnded,
					'startTime' => $xml->startTime,
					'endTime' => $xml->endTime,
					'participantCount' => $xml->participantCount,
					'maxUsers' => $xml->maxUsers,
					'moderatorCount' => $xml->moderatorCount,
				);
				// Then interate through attendee results and return them as part of the array:
				foreach ($xml->attendees->attendee as $a) {
					$result[] = array(
						'userId' => $a->userID,
						'fullName' => $a->fullName,
						'role' => $a->role
						);
					}
				return $result;
			}*/
		}
		else {
			return null;
		}

	}

	/* __________________ BBB RECORDING METHODS _________________ */
	/* The methods in the following section support the following categories of the BBB API:
	-- getRecordings
	-- publishRecordings
	-- deleteRecordings
	*/

	public function getRecordingsUrl($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'meetingId' => '1234',		-- OPTIONAL - comma separate if multiple ids
		);
		*/
		$recordingsUrl = $this->_bbbServerBaseUrl."api/getRecordings?";
		$params =
		'meetingID='.urlencode($recordingParams['meetingId']);
		return ($recordingsUrl.$params.'&checksum='.sha1("getRecordings".$params.$this->_securitySalt));

	}

	public function getRecordingsWithXmlResponseArray($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'meetingId' => '1234',		-- OPTIONAL - comma separate if multiple ids
		);
		NOTE: 'duration' DOES work when creating a meeting, so if you set duration
		when creating a meeting, it will kick users out after the duration. Should
		probably be required in user code when 'recording' is set to true.
		*/
		$xml = $this->_processXmlResponse($this->getRecordingsUrl($recordingParams));
		if($xml) {
			// If we don't get a success code or messageKey, find out why:
			if (($xml->returncode != 'SUCCESS') || ($xml->messageKey == null)) {
				$result = array(
					'returncode' => $xml->returncode,
					'messageKey' => $xml->messageKey,
					'message' => $xml->message
				);
				return $result;
			}
			else {
				// In this case, we have success and recording info:
				$result = array(
					'returncode' => $xml->returncode,
					'messageKey' => $xml->messageKey,
					'message' => $xml->message
				);

				foreach ($xml->recordings->recording as $r) {
					$recordings[] = array(
						'recordId' => $r->recordID,
						'meetingId' => $r->meetingID,
						'name' => $r->name,
						'published' => $r->published,
						'startTime' => $r->startTime,
						'endTime' => $r->endTime,
						'playbackFormatType' => $r->playback->format->type,
						'playbackFormatUrl' => $r->playback->format->url,
						'playbackFormatLength' => $r->playback->format->length,
						'metadataTitle' => $r->metadata->title,
						'metadataSubject' => $r->metadata->subject,
						'metadataDescription' => $r->metadata->description,
						'metadataCreator' => $r->metadata->creator,
						'metadataContributor' => $r->metadata->contributor,
						'metadataLanguage' => $r->metadata->language,
						// Add more here as needed for your app depending on your
						// use of metadata when creating recordings.
						);
					}
				$output = new stdClass();
				$output->result = $result;
				$output->recordings = $recordings;
				return $output;
			}
		}
		else {
			return null;
		}
	}

	public function getPublishRecordingsUrl($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
			'publish' => 'true',		-- REQUIRED - boolean: true/false
		);
		*/
		$recordingsUrl = $this->_bbbServerBaseUrl."api/publishRecordings?";
		$params =
		'recordID='.urlencode($recordingParams['recordId']).
		'&publish='.urlencode($recordingParams['publish']);
		return ($recordingsUrl.$params.'&checksum='.sha1("publishRecordings".$params.$this->_securitySalt));

	}

	public function publishRecordingsWithXmlResponseArray($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
			'publish' => 'true',		-- REQUIRED - boolean: true/false
		);
		*/
		$xml = $this->_processXmlResponse($this->getPublishRecordingsUrl($recordingParams));
		if($xml) {
			return array(
				'returncode' => $xml->returncode,
				'published' => $xml->published 	// -- Returns true/false.
				);
		}
		else {
			return null;
		}


	}

	public function getDeleteRecordingsUrl($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
		);
		*/
		$recordingsUrl = $this->_bbbServerBaseUrl."api/deleteRecordings?";
		$params =
		'recordID='.urlencode($recordingParams['recordId']);
		return ($recordingsUrl.$params.'&checksum='.sha1("deleteRecordings".$params.$this->_securitySalt));
	}

	public function deleteRecordingsWithXmlResponseArray($recordingParams) {
		/* USAGE:
		$recordingParams = array(
			'recordId' => '1234',		-- REQUIRED - comma separate if multiple ids
		);
		*/

		$xml = $this->_processXmlResponse($this->getDeleteRecordingsUrl($recordingParams));
		if($xml) {
			return array(
				'returncode' => $xml->returncode,
				'deleted' => $xml->deleted 	// -- Returns true/false.
				);
		}
		else {
			return null;
		}

	}

	public function endMeetingCallback($virtualClassParams,$action) {
		if(!$virtualClassParams->meetingId) {
			return false;
		}
		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('meeting_id').'='.$db->quote($virtualClassParams->meetingId);
		$conditions[] = $db->quoteName('check_out').' IS NULL';
		$table = 'axs_virtual_classroom_attendees';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($table));
		$query->where($conditions);
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$now = date('Y-m-d H:i:s');
		if($results && $action == 'checkOutAll') {
			foreach($results as $result) {
				$result->check_out = $now;
				$db->updateObject($table,$result,'id');
			}
		}
	}

	public function trackAttendance($virtualClassParams,$action) {
		if((!$virtualClassParams->userId && !$virtualClassParams->guestId) || !$virtualClassParams->meetingId) {
			return false;
		}

		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('meeting_id').'='.$db->quote($virtualClassParams->meetingId);
		if(!$virtualClassParams->guestId) {
			$conditions[] = $db->quoteName('user_id').'='.$db->quote($virtualClassParams->userId);
		} else {
			$conditions[] = $db->quoteName('guest_id').'='.$db->quote($virtualClassParams->guestId);
		}
		$conditions[] = $db->quoteName('check_out').' IS NULL';
		$table = 'axs_virtual_classroom_attendees';
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName($table));
		$query->where($conditions);
		$query->order('id DESC');
		$query->setLimit(1);
		$db->setQuery($query);
		$result = $db->loadObject();
		$now = date('Y-m-d H:i:s');
		if($result && $action == 'checkout') {
			$result->check_out = $now;
			$db->updateObject($table,$result,'id');
		}
		if($action == 'checkin') {
			$meetingData = $this->getMeetingById($virtualClassParams->meetingId);
			$meetingParams = new stdClass();
			$meetingParams->meetingId = $meetingData->meetingId;
			$meetingParams->moderatorPW = $meetingData->moderatorPW;
			$virtualClassData = AxsVirtualClassroom::getMeetingInfo($meetingParams);
			$createTime = "$virtualClassData->createTime";
			$data = new stdClass();
			if(!$virtualClassParams->guestId) {
				$data->user_id = $virtualClassParams->userId;
			} else {
				$data->guest_id = $virtualClassParams->guestId;
			}
			$data->attendee_name = $virtualClassParams->userName;
			$data->meeting_id = $virtualClassParams->meetingId;
			$data->date = $now;
			$data->check_in = $now;
			$data->session = $createTime; //must cast to string
			$db->insertObject($table,$data);
		}
	}

	public function getAttendance($virtualClassParams, $filters = null) {

		if(!$virtualClassParams->meetingId) {
			return false;
		}

		$db = JFactory::getDbo();
		$conditions[] = $db->quoteName('meeting_id').'='.$db->quote($virtualClassParams->meetingId);

		if($virtualClassParams->userId) {
			$conditions[] = $db->quoteName('user_id').'='.$db->quote($virtualClassParams->userId);
		}

		$table = 'axs_virtual_classroom_attendees';
		$query = $db->getQuery(true);
		$query->select('attendee.id as attendance_id, attendee.*,user.*');
		$query->from($db->quoteName($table,'attendee'));
		$query->join('LEFT', $db->quoteName('#__users','user') .' ON '.$db->quoteName('attendee.user_id').'='.$db->quoteName('user.id'));
		$query->where($conditions);

		if(!empty($filters)) {

			$query->andWhere($filters);
		}

		$db->setQuery($query);

		$result = $db->loadObjectList();

		return $result;
	}

	public function meeting($virtualClassParams) {
   	   /*
   	   	Available Params
   	    $virtualClassParams->meetingId;
   	   	$virtualClassParams->userId;
		$virtualClassParams->userName;
		$virtualClassParams->userPassword;
		*/
    	$data = $this->getMeetingById($virtualClassParams->meetingId);
		$settings = json_decode($data->settings);

		if($settings->meetingType != 'tovuti' && $settings->meetingType) {
			$url = $settings->meetingType.'Url';
			return $settings->$url;
		}

		if ($data->attendeePW == $virtualClassParams->userPassword || $data->moderatorPW == $virtualClassParams->userPassword) {
			$base = JUri::base();
			if(substr($base, -1) == '/') {
			    $base = substr($base, 0, -1);
			}

			// We want to restrict existing virtual classrooms to use the global maximum participants as an absolute maximum.
			$maxParticipants = $data->maxParticipants;
			$globalMaximumParticipants = AxsClientData::getClientMaxVirtualClassroomParticipants();

			if( ( ($maxParticipants > $globalMaximumParticipants) || ($maxParticipants == -1 && $globalMaximumParticipants >= 1) ) && ($globalMaximumParticipants >= 1)) {
				$maxParticipants = $globalMaximumParticipants;
			}

			$returnParams = new stdClass();
			$returnParams->meetingId = $virtualClassParams->meetingId;
			if($virtualClassParams->guestId) {
				$returnParams->guestId = $virtualClassParams->guestId;
			} else {
				$returnParams->userId = $virtualClassParams->userId;
			}
			$returnParams->returnUrl = '/';
			$key = AxsKeys::getKey('lms');
			$encryptedParams = base64_encode(AxsEncryption::encrypt($returnParams, $key));
			$meetingId = base64_encode($virtualClassParams->meetingId);
			$logoutUrl = $base;//.'/?return_params='.$encryptedParams;
			$callbackUrl = $base.'/join-virtual-classroom?end_vcid='.$encryptedParams;
			$virtualClassParams->logoutUrl = $logoutUrl;
			$creationParams = array(
				'meetingId'   => $data->meetingId,
				'meetingName' => $data->meetingName,
				'attendeePw'  => $data->attendeePW,
				'moderatorPw' => $data->moderatorPW,
				'logoutUrl'   => $logoutUrl,
				'callbackUrl' => $callbackUrl,
				'welcomeMsg'  => 'Welcome to '.$data->meetingName,
				//'dialNumber' => $this->dialNumber,
				//'voiceBridge' => $data->voiceBridge,
				'maxParticipants' => $maxParticipants + 1,
				'record' => $data->record,
				'duration' => $data->duration
			);
			$itsAllGood = true;

			try {$result = $this->createMeetingWithXmlResponseArray($creationParams);}
				catch (Exception $e) {
					echo 'Caught exception: ', $e->getMessage(), "\n";
					$itsAllGood = false;
			}

			if ($itsAllGood == true) {
				if ($result == null) {
					$output = "Failed to get any response. Maybe we can't contact the BBB server.";

				}
				else {
					if ($result['returncode'] == 'SUCCESS') {
						$output = $this->getlink($virtualClassParams);
					}
					else {
						$output = $this->getlink($virtualClassParams);
						//$output = "Meeting creation failed";
					}
				}
			}
		}

		else {
			$output = "Sorry password is incorrect";

		}
	    	return $output;
    }

    protected function getlink ($virtualClassParams) {
    	$joinParams = array(
			'meetingId' =>  $virtualClassParams->meetingId,
			'username'  =>  $virtualClassParams->userName,
			'userID'    =>  $virtualClassParams->userId,
			'password'  =>  $virtualClassParams->userPassword,
			'logoutUrl' =>  $virtualClassParams->logoutUrl
		);
		$itsAllGood = true;
		try {$result = $this->getJoinMeetingURL($joinParams);}
			catch (Exception $e) {
				echo 'Caught exception: ', $e->getMessage(), "\n";
				$itsAllGood = false;
			}

		if ($itsAllGood == true) {
			return $result;
		}
    }

    public function isMeetingRunning ($meetingId = 1) {
    	$data = $this->getMeetingById($meetingId);
    	//$bbb = new BigBlueButton($this->salt, $this->url);
    	$itsAllGood = true;
		try {$result = $this->isMeetingRunningWithXmlResponseArray($data->meetingId);}
			catch (Exception $e) {
				echo 'Caught exception: ', $e->getMessage(), "\n";
				$itsAllGood = false;
			}
		if ($itsAllGood == true) {
			return $result->running;
		}
    }

    public function endMeeting ($meetingId = null, $password= null) {
	  	$data = $this->getMeetingById($meetingId);
	  	//$bbb = new BigBlueButton($this->salt, $this->url);
	  	$endParams = array(
			'meetingId' => $data->meetingId,
			'password' => $password,
		);
		$itsAllGood = true;

		try {$result = $this->endMeetingWithXmlResponseArray($endParams);}
		catch (Exception $e) {
			echo 'Caught exception: ', $e->getMessage(), "\n";
			$itsAllGood = false;
		}

		if ($itsAllGood == true) {

			if ($result == null) {
				echo "Failed to get any response. Maybe we can't contact the BBB server.";
			}
			else {
				if ($result['returncode'] == 'SUCCESS') {
					echo "<p>Meeting succesfullly ended.</p>";
				}
				else {
					echo "<p>Failed to end meeting.</p>";
				}
			}
		}
    }

    public function getRecordings ($meetingId = 1) {
    	$data = $this->getMeetingById($meetingId);
		//$bbb = new BigBlueButton($this->salt, $this->url);

		if(!$data->meetingId) {
			return null;
		}

    	$recordingsParams = array(
		'meetingId' => $data->meetingId,
		);

		$itsAllGood = true;

		try {$output = $this->getRecordingsWithXmlResponseArray($recordingsParams);}
		catch (Exception $e) {
			echo 'Caught exception: ', $e->getMessage(), "\n";
			$itsAllGood = false;
		}

		if ($itsAllGood == true) {
			$result = $output->result;
			$recordings = $output->recordings;
			if ($result['returncode'] == 'SUCCESS') {
				return $recordings;
			}
		}
		return null;
    }

    public function publishRecordings($recordId = null) {

    	//$bbb = new BigBlueButton($this->salt, $this->url);
    	$recordingParams = array(
    		'recordId' => $recordId,
    		'publish' => 'true'
    	);

    	$itsAllGood = true;
		try {$result = $this->publishRecordingsWithXmlResponseArray($recordingParams);}
			catch (Exception $e) {
				echo 'Caught exception: ', $e->getMessage(), "\n";
				$itsAllGood = false;
			}

		if ($itsAllGood == true) {
			echo $result['published'];
		}
    }

    public function deleteRecordings($recordId = null) {

    	//$bbb = new BigBlueButton($this->salt, $this->url);
    	$recordingParams = array(
    		'recordId' => $recordId,
    	);
    	$itsAllGood = true;
		try {$result = $this->deleteRecordingsWithXmlResponseArray($recordingParams);}
			catch (Exception $e) {
				echo 'Caught exception: ', $e->getMessage(), "\n";
				$itsAllGood = false;
			}

		if ($itsAllGood == true) {
			print_r($result);
		}
    }
}
