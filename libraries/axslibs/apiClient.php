<?php

defined('_JEXEC') or die;

class AxsApiClient {

    static function fetch($url, $method, $body) {
        // putting these here temporarily
        // these should be env vars.
        $API_PK = "api_pk_D400CED885EA41BD8028914630111402";
        $API_SK = "api_sk_456B22A63B434B1491A6F2CAC497E9EE";

        $curl = curl_init($url);
        $header = array(
            "Content-type: application/json",
            "x-public-key: $API_PK",
            "x-api-key: $API_SK"
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_VERBOSE, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        if ($body) {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
        }
        $result = curl_exec($curl);
        curl_close($curl);
        return json_decode($result);
    }

    static function cs_log($msg) {
        // log (c)lient-side
        echo("$msg<br>");
        // log (s)erver-side (use tailf /var/apache2/error.log)
        error_log($msg);
    }

    static function fetch_and_log($endpoint, $method, $body) {
        // putting these here temporarily
        // these should be env vars.
        
        self::cs_log("<b>$API_URL$endpoint $method</b>");
        if ($body) {
            self::cs_log(json_encode($body, JSON_PRETTY_PRINT));
        }    
        $result = self::fetch("$API_URL$endpoint", $method, $body);
        self::cs_log("<pre>".var_dump($result)."</pre>");
        self::cs_log("<br><br>");
    }

}
