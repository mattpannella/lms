<?php

defined('_JEXEC') or die;

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

class AxsAdminPermissions {

	//This library is called on every administrator page load and gets a list of all of the permissions teh user has.

	public static function getPermissions() {
		//Only update this if a setting has been update. The only reason to update at all is in case an admin changes permissions.
		//Otherwise, there's no point in processing it every page change.  Just use the permissions stored in the $_SESSION

		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$homepage = null;

		//Pull the whole table.  This table is a Super User created table that holds all of the options and views for each of the admin section menu items.
		$query
			->select('*')
			->from($db->quoteName("axs_permissions_admin"));

		$db->setQuery($query);
		$results = $db->loadObjectList();

		$permissions_list = array();
		foreach ($results as $result) {
			//Create an easier to use indexed array.
			$permissions_list[$result->id] = $result;
		}

		$query->clear();

		if (!$_SESSION['admin_access_levels']) {
			//There are no access levels stored in SESSION.  This is a fresh visit to the site.
    		$check_access = true;
    	} else {
    		//Data already exists.  Check to see if any of the permissions in the database have been updated
    		//since the SESSION variable was last set.
    		$data = $_SESSION['admin_access_levels'];

    		if (!$data->last_check) {
    			//There's no last check, so it needs to be checked
    			$check_access = true;
    		} else {

    			if (!isset($data->permissions) || !isset($data->rights)) {
    				//These are not set.  Double check, or they could be given a false "no access" error.
    				$check_access = true;
    			} else {
	    			$query
		    			->select('*')
		    			->from($db->quoteName('axs_permissions'))
		    			->where($db->quoteName('updated') . ">= date('{$data->last_check}')");

		    		$db->setQuery($query);
		    		$result = $db->loadObjectList();

		    		if (count($result) > 0) {
		    			//One or more of the permissions of been updated.  Need to recheck them.
		    			$check_access = true;
		    		} else {
		    			//Nothing's been updated.  Their permissions will stay the same.
		    			$check_access = false;
		    		}
		    	}
	    	}
    	}

    	//$check_access = true;

    	if ($check_access) {
    		$access = new stdClass();

    		$user = JFactory::getUser();
    		$restrictions = false;

    		//Full Admin have no restrictions

    		if (AxsUser::isAdmin($user->id) || AxsUser::isSuperUser($user->id) || AxsUser::isDeveloper($user->id)) {
    			$full_access = true;
    		} else {
    			$full_access = false;
    		}

			$access_levels = $user->getAuthorisedViewLevels();		//Get the user's access levels
    		$user_groups = JUserHelper::getUserGroups($user->id);	//Get the user's user groups.

    		$query->clear();

    		$query
    			->select('*')
    			->from($db->quoteName('axs_permissions'))
    			->where($db->quoteName('enabled') . '=1');

    		$db->setQuery($query);
    		$results = $db->loadObjectList();

    		$allowed = array();
    		$access_rights = array();

    		foreach ($results as $result) {

    			$check = false;
    			switch ($result->type) {

    				//Check to see if this permissioning setting affects the current user.

    				case "accesslevel":
    					//This permissioning level is based on access level
    					$access_level_list = json_decode($result->access_level);
    					foreach ($access_level_list as $level) {
    						if (in_array($level, $access_levels)) {
	    						$check = true;
	    					}
    					}

    					break;
    				case "usergroup":
    					//This permissioning level is based on user group
    					$user_group_list = json_decode($result->user_group);
    					foreach ($user_group_list as $group) {
	    					if (in_array($group, $user_groups)) {
	    						$check = true;
	    					}
	    				}

    					break;
    				case "userid":
    					//This permissioning level is based on the user's id.
    					$user_list = explode(",", $result->user_list);

    					if (in_array($user->id, $user_list)) {
    						$check = true;
    					}

    					break;
    			}

    			/*

    				Each of these systems may be called multiple times by different permissioning levels.

    				For example, the user might be an Event Planner, which has limited access to the events section.
    				But they also might be an Events Coordinator, which has full access to the events section, as well
    				as limited access to the user manager.

					Once permission is granted to a section, it will not be taken away.  This will build a list of all of the areas
					that the user can go to.

    			*/

    			if ($check) {

    				//Yes, this permissioning level affects this user.

    				$restrictions = true;
    				$permissions = json_decode($result->permissions);

    				foreach ($permissions as $permission) {

    					//Iterate through all of the permissions

    					if (isset($permission->system)) {
    						//System permissions allow/deny the user access to different systems/areas of the site.

    						if ($full_access) {
								//This is a site admin or super user/developer.  Overwrite this system's access to always allow.
								$permission->allowed = true;
							}

							//The 'system' is a generated ID for the menu item.  Each one is separated by a hyphen.
							//ie. configuration-home_page_themes
	    					$parts = explode("-", $permission->system);

	    					$parent = null;
	    					$numParts = count($parts);
	    					$id = "";

	    					//Loop through all of the parts.
	    					for ($i = 0; $i < $numParts; $i++) {
	    						//This is the name of the system.
	    						$part = $parts[$i];	

	    						if ($id == "") {
	    							//The $id is empty, so this is the start of the string.
	    							$id = $part;
	    						} else {
	    							//The $id already exists, so this is being added on.
	    							$id .= "-" . $part;
	    						}

	    						if ($numParts == 1) {
	    							//There is only one part.  This means that the top level menu item is a button, not a drop down.
	    							//ie. Report Builder
	    							if (!isset($allowed[$part])) {
	    								//Only set it to a new stdclass if it's empty (no permissions have been set for this system yet)
	    								$allowed[$part] = new stdClass();
	    							}

	    							//This this user allowed in this system.
	    							$allowed[$part]->allowed = $permission->allowed;

	    							if (!$allowed[$part]->allowed) {
	    								//They're not allowed. Store the data for the blocked area
	    								$allowed[$part]->blocked = $permissions_list[$id];	
	    							} else {
	    								if (!$homepage) {
	    									//If no alternative homepage is set, set it.
	    									$homepage = $permissions_list[$id];
	    								}
	    							}
	    						} else {
	    							//This item has children permissions underneath it.
	    							if ($i == 0) {
	    								//This is the top item.
	    								$parent = $allowed[$part];
	    							} else {

	    								//This is a child item.

		    							if (!isset($parent->children)) {
		    								//Only set this if it has not already been set by something else.
		    								//This is an array to store ALL of the children.
		    								$parent->children = array();
		    							}

		    							if (!isset($parent->children[$part])) {
		    								//This checks if the child element has already been set in the array.
		    								$parent->children[$part] = new stdClass();
		    							}

		    							//Only check if it either has not been set or if it is set to false.
		    							//If it is already set to true, it can't be taken away.
		    							if (!isset($parent->children[$part]->allowed) || !$parent->children[$part]->allowed) {
			    							if ($parent->allowed) {
			    								//The parent is allowed, so check the child's permissionging.
			    								$parent->children[$part]->allowed = $permission->allowed;
			    							} else {
			    								//Access to the parent is not allowed, so the child is automatically denied.
			    								$parent->children[$part]->allowed = $parent->allowed;
			    							}

			    							if (!$parent->children[$part]->allowed) {
			    								//If it's not allowed, get the data on what is blocked.
			    								$parent->children[$part]->blocked = $permissions_list[$id];
			    							} else {
			    								if (!$homepage) {
			    									//If the new homepage has not been set, set it.
			    									$homepage = $permissions_list[$id];
			    								}
			    							}
			    						}

			    						//Set the new parent to this child item, so this item's children see it as the parent.
		    							$parent = $parent->children[$part];
		    						}
	    						}
	    					}
	    				} else if (isset($permission->access)) {
	    					//access permissions allow/deny users rights to edit/create users, groups, etc.
	    					foreach ($permission->access as $right) {
	    						$action = $right->action;
	    						if (!isset($access_rights[$action]) || !$access_rights[$action]->allowed) {
	    							$access_rights[$action] = $right->allowed;
	    						}
	    					}
	    				}
    				}
    			}
    		}

    		//Create the object that will be stored.
    		$access->permissions = $allowed;
    		$access->rights = $access_rights;
    		$access->last_check = date("Y-m-d H:i:s");
    		$access->homepage = $homepage;

   			if ($restrictions) {
   				//Set it into SESSION to it can be accessed anywhere.
    			$_SESSION['admin_access_levels'] = $access;
    		} else {
    			//They have no restrictions.
    			$empty = new stdClass();
    			$empty->permissions = false;
    			$empty->last_check = date("Y-m-d H:i:s");
    			$_SESSION['admin_access_levels'] = $empty;
    		}
    	}
    }
}