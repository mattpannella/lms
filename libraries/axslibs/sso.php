<?php
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */
defined('_JEXEC') or die;

require_once "sso/xmlseclibs.php";
require_once "sso/oauth_handler.php";

class AxsSSO {

    private static $config_id;
    private static $config;

    public static function generateUUID() {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function getSAMLConfiguration($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__sso_saml_config'));
        $query->where($db->quoteName('id')." = ".(int)$id);
        $db->setQuery($query);
        $attributes = $db->loadAssoc();
        $result = $db->loadObject();
        self::$config = $result;
        return $attributes;
    }

    public static function getTovutiSupportConfiguration($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('axs_tovuti_support_sso'));
        $query->where($db->quoteName('id')." = ".(int)$id);
        $db->setQuery($query);
        $attributes = $db->loadAssoc();
        $result = $db->loadObject();
        self::$config = $result;
        return $attributes;
    }

    public static function sendOauthRequest($sso_config) {
        self::$config_id = $sso_config['id'];
        $ssoParams = json_decode($sso_config['params']);
        $state = base64_encode($ssoParams->oauth_app_name);
        $authorizationUrl = $ssoParams->oauth_authorize_endpoint;
        if (strpos($authorizationUrl, '?') !== false) {
            $connector = '&';
        } else {
            $connector = '?';
        }

        $authorizationUrl = $authorizationUrl . $connector . "client_id=" . $ssoParams->oauth_client_id . "&scope=" . $ssoParams->oauth_app_scopes . "&redirect_uri=" . $ssoParams->oauth_callback_url . "&response_type=code&state=" . $state;


        $session = JFactory::getSession();
        if (session_id() == '' || !isset($session)) {
            session_start();
        }

        $input = JFactory::getApplication()->input;

        if(isset($_GET['q'])) {
            if($input->get('q',null,'STRING') == 'test_config') {
                $session->set('testValidate', true);
            }
        }

        $session->set('oauth2state', $state);
        $session->set('appname', $ssoParams->oauth_app_name);
        header('Location: ' . $authorizationUrl);
        exit;
    }

    public static function getOauthResponse($sso_config) {
        self::$config_id = $sso_config['id'];
        $ssoParams = json_decode($sso_config['params']);
        $OAuth_Handler = new OAuth_Handler();
        $ssoParams->grant_type = 'authorization_code';
        $ssoParams->code = $sso_config['code'];
        $accessToken = $OAuth_Handler->getAccessToken($ssoParams);
        if(!$accessToken) {
            exit('Invalid token received.');
        }
        $resourceOwnerDetailsUrl = $ssoParams->oauth_userinfo_endpoint;
        if (substr($resourceOwnerDetailsUrl, -1) == "=") {
            $resourceOwnerDetailsUrl .= $accessToken;
        }
        $resourceOwner = $OAuth_Handler->getResourceOwner($resourceOwnerDetailsUrl, $accessToken);

        $session = JFactory::getSession();
        if($session->get('testValidate')) {
            echo self::showOAuthTestResult($resourceOwner);
            $session->set('testValidate',false);
            exit;
        }

        if($ssoParams->oauth_idp == 'linkedin') {
            $resourceOwnerEmailUrl = 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))';
            $resourceOwnerEmail = $OAuth_Handler->getResourceOwner($resourceOwnerEmailUrl, $accessToken);
            $email =  $resourceOwnerEmail['elements'][0]['handle~']['emailAddress'];
            $name  = $resourceOwner['localizedFirstName'].' '.$resourceOwner['localizedLastName'];
        } else {
            $email = $resourceOwner[$ssoParams->email_attribute];
            $name = $resourceOwner[$sso_config['name']];
        }

        $checkUser = self::get_user_from_email($email);
        if($sso_config['forward']) {
            $login_url = $sso_config['forward'];
        } else {
            $login_url = JURI::root();
        }
        //$resourceOwner['groups'] = ['test1','test3'];
        $attrs = $resourceOwner;

        $username = str_replace("'",'',$email);
        $matcher = 'email';
        $app = JFactory::getApplication('site');
        $app->initialise();
        $saml_groups = $resourceOwner[$sso_config['grp']];;
        if ($checkUser) {
            self::loginCurrentUser($checkUser, $attrs, $login_url, $name, $username, $email, $matcher, $app, $saml_groups);
        } else {
            self::RegisterCurrentUser($attrs, $login_url, $name, $username, $email, $saml_groups, $matcher, $app);
        }
    }

    public static function showOAuthTestResult($resourceOwner) {
        $html  = '<link rel="stylesheet" href="/templates/axs/css/bootstrap.css?v=6">';
        $html .= '  <div class="container-fluid">
                        <div class="col-md-12">
                            <div style="color: #3c763d; background-color: #dff0d8; padding:2%;margin-bottom:20px;text-align:center; border:1px solid #AEDB9A; font-size:18pt;">TEST SUCCESSFUL</div>
                            <h2><b>IDP Attributes</b></h2>
                            <table class="table table-striped">
                                <tr>
                                    <th>Attribute</th>
                                    <th>Value</th>
                                </tr>';
                            foreach ($resourceOwner as $key => $resource) {
                                $html .= "<tr>
                                            <td>" . $key . "</td>
                                            <td>" . $resource . "</td>
                                          </tr>";
                            }
        $html .= '         </table>
                        </div>
                    </div>';
        return $html;
    }

    public static function checkMenuItem($params) {
        $link = 'index.php?option=com_axs&view=sso_config&id='.$params->config_id;
        $db = JFactory::getDbo();
        $conditions[] = $db->quoteName('menutype')." = ".$db->quote($params->menu_type);
        $conditions[] = $db->quoteName('link')." = ".$db->quote($link);
        if($params->alias) {
            $conditions[] = $db->quoteName('alias')." = ".$db->quote($params->alias);
        }
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__menu'));
        $query->where($conditions);
        $query->limit(1);
        $db->setQuery($query);
        $item = $db->loadObject();
        return $item;
    }

    public static function deleteMenuItem($id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->delete($db->quoteName('#__menu'));
        $query->where($db->quoteName('id') . '=' . (int)$id);
        $db->setQuery($query);
        $db->execute();
    }

    public static function setMenuItem($params) {
        $link = 'index.php?option=com_axs&view=sso_config&id='.$params->config_id;
        $alias = 'login'.'-'.rand(10000,99999);
        $menu_item = new stdClass();
        $menu_item->menutype = $params->menu_type;
        $menu_item->title = $params->link_name;
        $menu_item->alias = $alias;
        $menu_item->note = "";
        $menu_item->path = $alias;
        $menu_item->link = $link;
        $menu_item->type = "component";
        $menu_item->published = "1";
        $menu_item->parent_id = "1";
        $menu_item->level = "1";
        $menu_item->component_id = "10286";
        $menu_item->browserNav = "0";
        $menu_item->access = "1";
        $menu_item->img = " ";
        $menu_item->template_style_id = "0";
        $menu_item->params = "";
        $menu_item->lft = "611";
        $menu_item->rgt = "612";
        $menu_item->home = "0";
        $menu_item->language = "*";
        $menu_item->client_id = "0";
        $db = JFactory::getDbo();
        $db->insertObject('#__menu',$menu_item);
    }

    public static function getNextIDPId() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__sso_saml_config'));
        $query->order('id DESC');
        $query->limit(1);
        $db->setQuery($query);
        $row = $db->loadObject();
        $id = (int)$row->id;
        return $id + 1;
    }

    public static function sendSamlRequest($sso_config) {
        self::$config_id = $sso_config['id'];
        $input = JFactory::getApplication()->input;
        $get = $input->get->getArray();
        $sp_base_url = "";
        $siteUrl = JURI::root();
        $sp_base_url = $siteUrl;
        $params = json_decode($sso_config['params']);
        $sp_entity_id = $params->sp_issuer;

        if (!defined('_JDEFINES')) {
            require_once JPATH_BASE . '/includes/defines.php';
        }
        require_once JPATH_BASE . '/includes/framework.php';

        $acsUrl = $sp_base_url . 'authorizessologin?idp='.$sso_config['id'];
        if($sso_config['id'] == 204568) {
            $acsUrl = $sp_base_url . 'authorizessologin-tovuti-support-28956';
        }
        $ssoUrl = $sso_config['single_signon_service_url'];
        $sso_binding_type = $sso_config['binding'];
        $name_id_format = $sso_config['name_id_format'];
        $sendRelayState = self::getRelayState($sp_base_url, $_REQUEST);

        $samlRequest = self::createAuthnRequest($acsUrl, $sp_entity_id, $ssoUrl, 'false', $sso_binding_type, $name_id_format);

        if(isset($get['q'])) {
            if($get['q']=="sso") {
                self::sso_saml_show_SAML_log($samlRequest, "displaySAMLRequest");
            }
            $session = JFactory::getSession();
            if($get['q'] == 'test_config') {
                $session->set('testValidate', true);
            }
        }

        $samlRequest = self::samlRequestBind($samlRequest, $sso_binding_type);

        self::sendSamlRequestByBindingType($samlRequest, $sso_binding_type, $sendRelayState,  $ssoUrl);
    }

    public static function getRelayState($sp_base_url, $request) {

        $sendRelayState = $sp_base_url;

        if(isset($request['q'])) {
            if($request['q'] == 'test_config') {
                $sendRelayState = 'testValidate';
            }
            if($request['q'] == 'get_xml') {
                $sendRelayState = 'getXML';
            }
        }

        else if(isset($request['RelayState']) && $request['RelayState'] != '/' && $request['RelayState'] != '') {
            $sendRelayState = $request['RelayState'];
        }

        else if(isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != '') {
            $sendRelayState = $_SERVER['HTTP_REFERER'];
        }


        return $sendRelayState;
    }

    public static function validateElement(DOMElement $root) {

        /* Create an XML security object. */
        $objXMLSecDSig = new XMLSecurityDSigSSO();

        /* Both SAML messages and SAML assertions use the 'ID' attribute. */
        $objXMLSecDSig->idKeys[] = 'ID';


        /* Locate the XMLDSig Signature element to be used. */
        $signatureElement = self::xpQuery($root, './ds:Signature');


        if (count($signatureElement) === 0) {
            /* We don't have a signature element to validate. */
            return FALSE;
        } elseif (count($signatureElement) > 1) {
            echo "XMLSec: more than one signature element in root.";
            exit;
        }

        $signatureElement = $signatureElement[0];
        $objXMLSecDSig->sigNode = $signatureElement;

        /* Canonicalize the XMLDSig SignedInfo element in the message. */
        $objXMLSecDSig->canonicalizeSignedInfo();

       /* Validate referenced xml nodes. */
        if (!$objXMLSecDSig->validateReference()) {
            echo "XMLsec: digest validation failed";
            exit;
        }

        /* Check that $root is one of the signed nodes. */
        $rootSigned = FALSE;
        /** @var DOMNode $signedNode */
        foreach ($objXMLSecDSig->getValidatedNodes() as $signedNode) {
            if ($signedNode->isSameNode($root)) {
                $rootSigned = TRUE;
                break;
            } elseif ($root->parentNode instanceof DOMDocument && $signedNode->isSameNode($root->ownerDocument)) {
                /* $root is the root element of a signed document. */
                $rootSigned = TRUE;
                break;
            }
        }

        if (!$rootSigned) {
            echo "XMLSec: The root element is not signed.";
            exit;
        }

        /* Now we extract all available X509 certificates in the signature element. */
        $certificates = array();
        foreach (self::xpQuery($signatureElement, './ds:KeyInfo/ds:X509Data/ds:X509Certificate') as $certNode) {
            $certData = trim($certNode->textContent);
            $certData = str_replace(array("\r", "\n", "\t", ' '), '', $certData);
            $certificates[] = $certData;

        }

        $ret = array(
            'Signature' => $objXMLSecDSig,
            'Certificates' => $certificates,
            );

        return $ret;
    }

    public static function extractStrings(DOMElement $parent, $namespaceURI, $localName) {

        $ret = array();
        for ($node = $parent->firstChild; $node !== NULL; $node = $node->nextSibling) {
            if ($node->namespaceURI !== $namespaceURI || $node->localName !== $localName) {
                continue;
            }
            $ret[] = trim($node->textContent);
        }

        return $ret;
    }

    public static function parseNameId(DOMElement $xml) {
        $ret = array('Value' => trim($xml->textContent));

        foreach (array('NameQualifier', 'SPNameQualifier', 'Format') as $attr) {
            if ($xml->hasAttribute($attr)) {
                $ret[$attr] = $xml->getAttribute($attr);
            }
        }

        return $ret;
    }

    public static function xsDateTimeToTimestamp($time) {
        $matches = array();

        // We use a very strict regex to parse the timestamp.
        $regex = '/^(\\d\\d\\d\\d)-(\\d\\d)-(\\d\\d)T(\\d\\d):(\\d\\d):(\\d\\d)(?:\\.\\d+)?Z$/D';
        if (preg_match($regex, $time, $matches) == 0) {
            throw new Exception(
                'Invalid SAML2 timestamp passed to xsDateTimeToTimestamp: ' . $time
            );
        }

        // Extract the different components of the time from the  matches in the regex.
        // intval will ignore leading zeroes in the string.
        $year   = intval($matches[1]);
        $month  = intval($matches[2]);
        $day    = intval($matches[3]);
        $hour   = intval($matches[4]);
        $minute = intval($matches[5]);
        $second = intval($matches[6]);

        // We use gmmktime because the timestamp will always be given
        //in UTC.
        $ts = gmmktime($hour, $minute, $second, $month, $day, $year);

        return $ts;
    }

    public static function generateTimestamp($instant = NULL) {
        if($instant === NULL) {
            $instant = time();
        }
        return gmdate('Y-m-d\TH:i:s\Z', $instant);
    }

    public static function generateID() {
        return '_' . self::stringToHex(self::generateRandomBytes(21));
    }

    public static function stringToHex($bytes) {
        $ret = '';
        for($i = 0; $i < strlen($bytes); $i++) {
            $ret .= sprintf('%02x', ord($bytes[$i]));
        }
        return $ret;
    }

    public static function generateRandomBytes($length, $fallback = TRUE) {
        return openssl_random_pseudo_bytes($length);
    }

    public static function createAuthnRequest($acsUrl, $issuer,  $destination, $force_authn = 'false',$sso_binding_type = 'HttpRedirect', $name_id_format) {

        $requestXmlStr = '<?xml version="1.0" encoding="UTF-8"?>' .
                '<samlp:AuthnRequest xmlns:samlp="urn:oasis:names:tc:SAML:2.0:protocol" ID="' . self::generateID() .
                '" Version="2.0" IssueInstant="' . self::generateTimestamp() . '"';
        if( $force_authn == 'true') {
            $requestXmlStr .= ' ForceAuthn="true"';
        }
        $requestXmlStr .= ' ProtocolBinding="urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST" AssertionConsumerServiceURL="' . $acsUrl .
                        '" Destination="' . $destination . '"><saml:Issuer xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">' . $issuer . '</saml:Issuer><samlp:NameIDPolicy AllowCreate="true" Format="'.$name_id_format.'"
                        /></samlp:AuthnRequest>';

        return $requestXmlStr;
    }

    public static function samlRequestBind($requestXmlStr,$sso_binding_type) {

       if(empty($sso_binding_type) || $sso_binding_type == 'HttpRedirect') {
            $deflatedStr = gzdeflate($requestXmlStr);
            $base64EncodedStr = base64_encode($deflatedStr);
            $urlEncoded = urlencode($base64EncodedStr);
            $requestXmlStr = $urlEncoded;
        }
        return $requestXmlStr;
    }

    public static function sendSamlRequestByBindingType($samlRequest, $sso_binding_type, $sendRelayState, $ssoUrl) {

        if(empty($sso_binding_type) || $sso_binding_type == 'HttpRedirect') {

            $samlRequest = "SAMLRequest=" . $samlRequest . "&RelayState=" . $sendRelayState;
            $param = array('type' => 'private');
            $redirect = $ssoUrl;
            if (strpos($ssoUrl,'?') !== false) {
                $redirect .= '&';
            } else {
                $redirect .= '?';
            }
            $redirect .=  $samlRequest;
            header('Location: '.$redirect);
            exit();
        } else {

            $base64EncodedXML = self::signXML( $samlRequest, $publicCertPath, $privateKeyPath, 'NameIDPolicy' );
            self::postSAMLRequest($ssoUrl, $samlRequest, $sendRelayState);
        }
    }

    public static function signXML($xml, $publicCertPath, $privateKeyPath, $insertBeforeTagName = "") {
        $param = array( 'type' => 'private');
        $key = new XMLSecurityKeySSO(XMLSecurityKeySSO::RSA_SHA256, $param);
        $key->loadKey($privateKeyPath, TRUE);
        $publicCertificate = file_get_contents( $publicCertPath );
        $document = new DOMDocument();
        $document->loadXML($xml);
        $element = $document->firstChild;
        if( !empty($insertBeforeTagName) ) {
            $domNode = $document->getElementsByTagName( $insertBeforeTagName )->item(0);
            self::insertSignature($key, array ( $publicCertificate ), $element, $domNode);
        } else {
            self::insertSignature($key, array ( $publicCertificate ), $element);
        }
        $requestXML = $element->ownerDocument->saveXML($element);
        $base64EncodedXML = base64_encode($requestXML);
        return $base64EncodedXML;
    }

    public static function postSAMLRequest($url, $samlRequestXML, $relayState) {
        echo "<html><head><script src='https://code.jquery.com/jquery-1.11.3.min.js'></script><script type=\"text/javascript\">$(function(){document.forms['saml-request-form'].submit();});</script></head><body>Please wait...<form action=\"" . $url . "\" method=\"post\" id=\"saml-request-form\"><input type=\"hidden\" name=\"SAMLRequest\" value=\"" . $samlRequestXML . "\" /><input type=\"hidden\" name=\"RelayState\" value=\"" . htmlentities($relayState) . "\" /></form></body></html>";
        exit;
    }

    public static function insertSignature(
        XMLSecurityKeySSO $key,
        array $certificates,
        DOMElement $root = NULL,
        DOMNode $insertBefore = NULL
    ) {
        $objXMLSecDSig = new XMLSecurityDSigSSO();
        $objXMLSecDSig->setCanonicalMethod(XMLSecurityDSigSSO::EXC_C14N);

        switch ($key->type) {
            case XMLSecurityKeySSO::RSA_SHA256:
                $type = XMLSecurityDSigSSO::SHA256;
                break;
            case XMLSecurityKeySSO::RSA_SHA384:
                $type = XMLSecurityDSigSSO::SHA384;
                break;
            case XMLSecurityKeySSO::RSA_SHA512:
                $type = XMLSecurityDSigSSO::SHA512;
                break;
            default:
                $type = XMLSecurityDSigSSO::SHA1;
        }

        $objXMLSecDSig->addReferenceList(
            array($root),
            $type,
            array('http://www.w3.org/2000/09/xmldsig#enveloped-signature', XMLSecurityDSigSSO::EXC_C14N),
            array('id_name' => 'ID', 'overwrite' => FALSE)
        );

        $objXMLSecDSig->sign($key);

        foreach ($certificates as $certificate) {
            $objXMLSecDSig->add509Cert($certificate, TRUE);
        }

        $objXMLSecDSig->insertSignature($root, $insertBefore);
    }

    public static function sso_saml_show_SAML_log($samlRequestResponceXML, $type) {

        header("Content-Type: text/html");
        $doc = new DOMDocument();
        $doc->preserveWhiteSpace = false;
        $doc->formatOutput = true;
        $doc->loadXML($samlRequestResponceXML);

        if($type=='displaySAMLRequest')
            $show_value='SAML Request';
        else
        $show_value='SAML Response';
        $out = $doc->saveXML();

        $out1 = htmlentities($out);
        $out1 = rtrim($out1);

        $xml   = simplexml_load_string( $out );

        $json  = json_encode( $xml );

        $array = json_decode( $json );

        echo '<link rel="stylesheet" type="text/css" href="' . JURI::root() . 'media/com_sso_saml/css/style_settings.css"/>



            <div class="mo-display-logs" ><p type="text"   id="SAML_type">'.$show_value.'</p></div>

            <div type="text" id="SAML_display" class="mo-display-block"><pre class=\'brush: xml;\'>'.$out1.'</pre></div>
            <br>
            <div style="margin:3%;display:block;text-align:center;">

            <div style="margin:3%;display:block;text-align:center;" >

            </div>
            <button id="copy" onclick="copyDivToClipboard()"  style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;" >Copy</button>
            &nbsp;
               <input id="dwn-btn" style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Download"
               ">
            </div>
            </div>

        ';

        ob_end_flush();
        ?>

        <script>

            function copyDivToClipboard() {
                var aux = document.createElement("input");
                aux.setAttribute("value", document.getElementById("SAML_display").textContent);
                document.body.appendChild(aux);
                aux.select();
                document.execCommand("copy");
                document.body.removeChild(aux);
                document.getElementById('copy').textContent = "Copied";
                document.getElementById('copy').style.background = "grey";
                window.getSelection().selectAllChildren( document.getElementById( "SAML_display" ) );
            }

            function download(filename, text) {
                var element = document.createElement('a');
                element.setAttribute('href', 'data:Application/octet-stream;charset=utf-8,' + encodeURIComponent(text));
                element.setAttribute('download', filename);

                element.style.display = 'none';
                document.body.appendChild(element);

                element.click();

                document.body.removeChild(element);
            }

            document.getElementById("dwn-btn").addEventListener("click", function () {

                var filename = document.getElementById("SAML_type").textContent+".xml";
                var node = document.getElementById("SAML_display");
                htmlContent = node.innerHTML;
                text = node.textContent;
                console.log(text);
                download(filename, text);
            }, false);

        </script>
        <?php
        exit;
    }

    public static function getSamlResponse($sso_config){
        self::$config_id = $sso_config['id'];
        $params = json_decode($sso_config['params']);
        $post = JFactory::getApplication()->input->post->getArray();

        if (!defined('_JDEFINES')) {
            require_once JPATH_BASE . '/includes/defines.php';
        }
        require_once JPATH_BASE . '/includes/framework.php';

        $authBase = JPATH_BASE. DIRECTORY_SEPARATOR. 'plugins'. DIRECTORY_SEPARATOR. 'authentication' . DIRECTORY_SEPARATOR . 'ssosaml';
        include_once $authBase . DIRECTORY_SEPARATOR  . 'saml2' . DIRECTORY_SEPARATOR. 'Response.php';

        //jimport('ssosamlplugin.utility.encryption');
        jimport ( 'joomla.application.application' );
        jimport ( 'joomla.html.parameter' );

        $sp_base_url = "";
        $sp_entity_id = "";

        if(isset($sso_config['sp_base_url'])){
            $sp_base_url  = $sso_config['sp_base_url'];
            $sp_entity_id = $sso_config['sp_entity_id'];
        }

        $siteUrl = JURI::root();

        if(empty($sp_base_url)) {
            $sp_base_url = $siteUrl;
        }

        $sp_entity_id = $params->sp_issuer;

        $app = JFactory::getApplication('site');
        $app->initialise();
        $get = JFactory::getApplication()->input->get->getArray();

        if (array_key_exists ( 'SAMLResponse', $post )) {
            self::validateSamlResponse($post, $sp_base_url, $sp_entity_id, $sso_config, $app);
        } else {
            header("Location: $siteUrl");
            //throw new Exception ( 'Missing SAMLRequest or SAMLResponse parameter.' );
        }
    }

    public static function sanitize_certificate( $certificate ) {
        $certificate = preg_replace("/[\r\n]+/", "", $certificate);
        $certificate = str_replace( "-", "", $certificate );
        $certificate = str_replace( "BEGIN CERTIFICATE", "", $certificate );
        $certificate = str_replace( "END CERTIFICATE", "", $certificate );
        $certificate = str_replace( " ", "", $certificate );
        $certificate = chunk_split($certificate, 64, "\r\n");
        $certificate = "-----BEGIN CERTIFICATE-----\r\n" . $certificate . "-----END CERTIFICATE-----";
        return $certificate;
    }

    private static function findCertificate(array $certFingerprints, array $certificates, $relayState,$ResCert) {

        $candidates = array();

        foreach ($certificates as $cert) {
            $fp = strtolower(sha1(base64_decode($cert)));
            if (!in_array($fp, $certFingerprints, TRUE)) {
                $candidates[] = $fp;
                continue;
            }

            /* We have found a matching fingerprint. */
            $pem = "-----BEGIN CERTIFICATE-----\n" .
                chunk_split($cert, 64) .
                "-----END CERTIFICATE-----\n";

            return $pem;
        }

        if($relayState=='testValidate'){
            echo '<div style="font-family:Calibri;padding:0 3%;">';
            echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;"> ERROR</div>
            <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>Unable to find a certificate matching the configured fingerprint.</p>
            <p><strong>Possible Cause: </strong>Content of \'X.509 Certificate\' field in Service Provider Settings is incorrect</p>
            <p><b>Expected value:</b>' . $ResCert . '</p>';
            echo str_repeat('&nbsp;', 15);
            echo'</div>
                <div style="margin:3%;display:block;text-align:center;">
                <form action="index.php">
                <div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
                exit;
            }
            else{
                echo ' <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>We could not sign you in. Please contact your Administrator.</p></div>
                  <div style="margin:3%;display:block;text-align:center;">
                        <form action='.JURI::root().'><input style="padding:1%;width:150px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="submit" value="Back to home"></form></div>';
                  exit;
            }
    }

    public static function decryptElement(DOMElement $encryptedData, XMLSecurityKeySSO $inputKey, array $blacklist = array()){
        try {
            return self::doDecryptElement($encryptedData, $inputKey, $blacklist);
        } catch (Exception $e) {
            /*
             * Something went wrong during decryption, but for security
             * reasons we cannot tell the user what failed.
             */

           throw new Exception('Failed to decrypt XML element.');
        }
    }

    private static function doDecryptElement(DOMElement $encryptedData, XMLSecurityKeySSO $inputKey, array &$blacklist) {
        $enc = new XMLSecEncSSO();
        $enc->setNode($encryptedData);

        $enc->type = $encryptedData->getAttribute("Type");
        $symmetricKey = $enc->locateKey($encryptedData);
        if (!$symmetricKey) {
            throw new Exception('Could not locate key algorithm in encrypted data.');
        }

        $symmetricKeyInfo = $enc->locateKeyInfo($symmetricKey);
        if (!$symmetricKeyInfo) {
            throw new Exception('Could not locate <dsig:KeyInfo> for the encrypted key.');
        }
        $inputKeyAlgo = $inputKey->getAlgorith();
        if ($symmetricKeyInfo->isEncrypted) {
            $symKeyInfoAlgo = $symmetricKeyInfo->getAlgorith();
            if (in_array($symKeyInfoAlgo, $blacklist, TRUE)) {
                throw new Exception('Algorithm disabled: ' . var_export($symKeyInfoAlgo, TRUE));
            }
            if ($symKeyInfoAlgo === XMLSecurityKeySSO::RSA_OAEP_MGF1P && $inputKeyAlgo === XMLSecurityKey::RSA_1_5) {
                /*
                 * The RSA key formats are equal, so loading an RSA_1_5 key
                 * into an RSA_OAEP_MGF1P key can be done without problems.
                 * We therefore pretend that the input key is an
                 * RSA_OAEP_MGF1P key.
                 */
                $inputKeyAlgo = XMLSecurityKeySSO::RSA_OAEP_MGF1P;
            }
            /* Make sure that the input key format is the same as the one used to encrypt the key. */
            if ($inputKeyAlgo !== $symKeyInfoAlgo) {
                throw new Exception(
                    'Algorithm mismatch between input key and key used to encrypt ' .
                    ' the symmetric key for the message. Key was: ' .
                    var_export($inputKeyAlgo, TRUE) . '; message was: ' .
                    var_export($symKeyInfoAlgo, TRUE)
                );
            }
            /** @var XMLSecEnc $encKey */
            $encKey = $symmetricKeyInfo->encryptedCtx;
            $symmetricKeyInfo->key = $inputKey->key;
            $keySize = $symmetricKey->getSymmetricKeySize();
            if ($keySize === NULL) {
                /* To protect against "key oracle" attacks, we need to be able to create a
                 * symmetric key, and for that we need to know the key size.
                 */
                throw new Exception('Unknown key size for encryption algorithm: ' . var_export($symmetricKey->type, TRUE));
            }
            try {
                $key = $encKey->decryptKey($symmetricKeyInfo);
                if (strlen($key) != $keySize) {
                    throw new Exception(
                        'Unexpected key size (' . strlen($key) * 8 . 'bits) for encryption algorithm: ' .
                        var_export($symmetricKey->type, TRUE)
                    );
                }
            } catch (Exception $e) {
                /* We failed to decrypt this key. Log it, and substitute a "random" key. */

                /* Create a replacement key, so that it looks like we fail in the same way as if the key was correctly padded. */
                /* We base the symmetric key on the encrypted key and private key, so that we always behave the
                 * same way for a given input key.
                 */
                $encryptedKey = $encKey->getCipherValue();
                $pkey = openssl_pkey_get_details($symmetricKeyInfo->key);
                $pkey = sha1(serialize($pkey), TRUE);
                $key = sha1($encryptedKey . $pkey, TRUE);
                /* Make sure that the key has the correct length. */
                if (strlen($key) > $keySize) {
                    $key = substr($key, 0, $keySize);
                } elseif (strlen($key) < $keySize) {
                    $key = str_pad($key, $keySize);
                }
            }
            $symmetricKey->loadkey($key);
        } else {
            $symKeyAlgo = $symmetricKey->getAlgorith();
            /* Make sure that the input key has the correct format. */
            if ($inputKeyAlgo !== $symKeyAlgo) {
                throw new Exception(
                    'Algorithm mismatch between input key and key in message. ' .
                    'Key was: ' . var_export($inputKeyAlgo, TRUE) . '; message was: ' .
                    var_export($symKeyAlgo, TRUE)
                );
            }
            $symmetricKey = $inputKey;
        }
        $algorithm = $symmetricKey->getAlgorith();
        if (in_array($algorithm, $blacklist, TRUE)) {
            throw new Exception('Algorithm disabled: ' . var_export($algorithm, TRUE));
        }
        /** @var string $decrypted */
        $decrypted = $enc->decryptNode($symmetricKey, FALSE);
        /*
         * This is a workaround for the case where only a subset of the XML
         * tree was serialized for encryption. In that case, we may miss the
         * namespaces needed to parse the XML.
         */
        $xml = '<root xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" '.
                     'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' .
            $decrypted .
            '</root>';
        $newDoc = new DOMDocument();
        if (!@$newDoc->loadXML($xml)) {
            throw new Exception('Failed to parse decrypted XML. Maybe the wrong sharedkey was used?');
        }
        $decryptedElement = $newDoc->firstChild->firstChild;
        if ($decryptedElement === NULL) {
            throw new Exception('Missing encrypted element.');
        }

        if (!($decryptedElement instanceof DOMElement)) {
            throw new Exception('Decrypted element was not actually a DOMElement.');
        }

        return $decryptedElement;
    }

    public static function validateSignature(array $info, XMLSecurityKeySSO $key) {

        /** @var XMLSecurityDSigSSO $objXMLSecDSig */
        $objXMLSecDSig = $info['Signature'];

        $sigMethod = self::xpQuery($objXMLSecDSig->sigNode, './ds:SignedInfo/ds:SignatureMethod');
        if (empty($sigMethod)) {
            throw new Exception('Missing SignatureMethod element.');
        }
        $sigMethod = $sigMethod[0];
        if (!$sigMethod->hasAttribute('Algorithm')) {
            throw new Exception('Missing Algorithm-attribute on SignatureMethod element.');
        }
        $algo = $sigMethod->getAttribute('Algorithm');

        if ($key->type === XMLSecurityKeySSO::RSA_SHA1 && $algo !== $key->type) {
            $key = self::castKey($key, $algo);
        }

        /* Check the signature. */
        if (! $objXMLSecDSig->verify($key)) {
            throw new Exception("Unable to validate Signature");
        }
    }

    public static function castKey(XMLSecurityKeySSO $key, $algorithm, $type = 'public') {

        // do nothing if algorithm is already the type of the key
        if ($key->type === $algorithm) {
            return $key;
        }

        $keyInfo = openssl_pkey_get_details($key->key);
        if ($keyInfo === FALSE) {
            throw new Exception('Unable to get key details from XMLSecurityKey.');
        }
        if (!isset($keyInfo['key'])) {
            throw new Exception('Missing key in public key details.');
        }

        $newKey = new XMLSecurityKeySSO($algorithm, array('type'=>$type));
        $newKey->loadKey($keyInfo['key']);

        return $newKey;
    }

    public static function xpQuery(DOMNode $node, $query) {
        static $xpCache = NULL;

        if ($node instanceof DOMDocument) {
            $doc = $node;
        } else {
            $doc = $node->ownerDocument;
        }

        if ($xpCache === NULL || !$xpCache->document->isSameNode($doc)) {
            $xpCache = new DOMXPath($doc);
            $xpCache->registerNamespace('soap-env', 'http://schemas.xmlsoap.org/soap/envelope/');
            $xpCache->registerNamespace('saml_protocol', 'urn:oasis:names:tc:SAML:2.0:protocol');
            $xpCache->registerNamespace('saml_assertion', 'urn:oasis:names:tc:SAML:2.0:assertion');
            $xpCache->registerNamespace('saml_metadata', 'urn:oasis:names:tc:SAML:2.0:metadata');
            $xpCache->registerNamespace('ds', 'http://www.w3.org/2000/09/xmldsig#');
            $xpCache->registerNamespace('xenc', 'http://www.w3.org/2001/04/xmlenc#');
        }

        $results = $xpCache->query($query, $node);
        $ret = array();
        for ($i = 0; $i < $results->length; $i++) {
            $ret[$i] = $results->item($i);
        }

        return $ret;
    }

    public static function checkSign($certFingerprint, $signatureData, $certFromPlugin, $relayState,$ResCert) {

        $certificates = $signatureData['Certificates'];

        if (count($certificates) === 0) {
            $pemCert = $certFromPlugin;
        } else {
            $pemCert = self::findCertificate($certFingerprint, $certificates, $relayState,$ResCert);
        }

        $lastException = NULL;

        $key = new XMLSecurityKeySSO(XMLSecurityKeySSO::RSA_SHA1, array('type'=>'public'));
        $key->loadKey($pemCert);

        try {
            /*
             * Make sure that we have a valid signature
             */
            //assert('$key->type === XMLSecurityKey::RSA_SHA1');
            self::validateSignature($signatureData, $key);
            return TRUE;
        } catch (Exception $e) {
            echo 'Validation with key failed with exception: ' . $e->getMessage();
            $lastException = $e;
        }


        /* We were unable to validate the signature with any of our keys. */
        if ($lastException !== NULL) {
            throw $lastException;
        } else {
            return FALSE;
        }

    }

    public static function processResponse($currentURL, $certFingerprint, $signatureData,
        SAML2_Response $response, $certFromPlugin, $relayState) {

        $ResCert = $signatureData['Certificates'][0];

        /* Validate Response-element destination. */
        $msgDestination = $response->getDestination();
        if ($msgDestination !== NULL && $msgDestination !== $currentURL) {
            echo 'Destination in response doesn\'t match the current URL. Destination is "' .
                $msgDestination . '", current URL is "' . $currentURL . '".';
                exit;
        }

        $responseSigned = self::checkSign($certFingerprint, $signatureData, $certFromPlugin, $relayState,$ResCert);

        /* Returning boolean $responseSigned */
        return $responseSigned;
    }

    public static function validateSamlResponse($post, $sp_base_url, $sp_entity_id, $attribute, $app) {

        $samlResponse = $post ['SAMLResponse'];

        if (array_key_exists ( 'RelayState', $_REQUEST ) && ($_REQUEST['RelayState'] != '') && ($_REQUEST['RelayState'] != '/')) {
            $relayState = $_REQUEST ['RelayState'];
        } else {
            $relayState = $sp_base_url;
        }

        $samlResponse = base64_decode($samlResponse);

        $document = new DOMDocument();
        $document->loadXML($samlResponse);
        $samlResponseXml = $document->firstChild;

        $doc = $document->documentElement;
        $xpath = new DOMXpath($document);
        $xpath->registerNamespace('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');
        $xpath->registerNamespace('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');

        $status = $xpath->query('/samlp:Response/samlp:Status/samlp:StatusCode', $doc);
        $statusString = $status->item(0)->getAttribute('Value');
        $statusChildString = '';
        if($status->item(0)->firstChild !== null) {
            $statusChildString = $status->item(0)->firstChild->getAttribute('Value');
        }

        $stat = explode(":",$statusString);
        $status = $stat[7];

        if($relayState=="getXML") {
            self::sso_saml_show_SAML_log($samlResponse, "displaySAMLResponse");
            exit;
        }

        if($status!="Success") {
            if(!empty($statusChildString)) {
                $stat = explode(":", $statusChildString);
                $status = $stat[7];
            }
            self::show_error_message($status, $relayState);
        }

        $id = self::get_config_id();
        if(!$id) {
            $id = 1;
        }

        if($id == 204568) {
            $acsUrl = $sp_base_url . str_replace('/','',$_SERVER['REQUEST_URI']);
        } else {
            $acsUrl = $sp_base_url . 'authorizessologin?idp='.$id;
        }
        $certFromPlugin = $attribute['certificate'];

        if(!empty($certFromPlugin)) {
            $certArray = explode("/////////Certificate/////////",$certFromPlugin);
            $certfpFromPluginArray = array();
            foreach($certArray as $cert) {
                $cfp = self::sanitize_certificate($cert);
                $certfpFromPlugin = XMLSecurityKeySSO::getRawThumbprint($cfp);
                /* convert to UTF-8 character encoding */
                $certfpFromPlugin = iconv ( "UTF-8", "CP1252//IGNORE", $certfpFromPlugin);
                /* remove whitespaces */
                $certfpFromPlugin = preg_replace ( '/\s+/', '', $certfpFromPlugin );
                array_push($certfpFromPluginArray,$certfpFromPlugin);
            }

        }

        $samlResponse = new SAML2_Response($samlResponseXml);

        $responseSignatureData = $samlResponse->getSignatureData();

        $assertionSignatureData = current($samlResponse->getAssertions())->getSignatureData();


        // /* Validate signature */
        if(!empty($certfpFromPlugin)) {
            if(!empty($responseSignatureData)) {

                $validSignature = self::processResponse($acsUrl, $certfpFromPluginArray, $responseSignatureData, $samlResponse, $certFromPlugin, $relayState);
                if($validSignature === FALSE) {
                    echo "Invalid signature in the SAML Response.<br><br>";
                    exit;
                }
            }

            if(!empty($assertionSignatureData)) {
                    $validSignature = self::processResponse($acsUrl, $certfpFromPluginArray, $assertionSignatureData, $samlResponse, $certFromPlugin, $relayState);
                    if($validSignature === FALSE) {
                        echo "Invalid signature in the SAML Assertion.<br><br>";
                        exit;
                    }
            }
        }

        // verify the issuer and audience from saml response
        $issuer = $attribute['idp_entity_id'];

        self::validateIssuerAndAudience($samlResponse, $sp_entity_id, $issuer, $relayState);

        $username = current ( current ( $samlResponse->getAssertions () )->getNameId () );
        $attrs = current ( $samlResponse->getAssertions () )->getAttributes ();
        $attrs ['NameID'] = current ( current ( $samlResponse->getAssertions () )->getNameId () );

        if($relayState=='testValidate') {
            self::sso_saml_show_test_result($username,$attrs,$sp_base_url);
            exit;
        }

        if($relayState=="getXML") {
            self::sso_saml_show_SAML_log($samlResponse, "displaySAMLResponse");
            exit;
        }

        $sessionIndex = current ( $samlResponse->getAssertions () )->getSessionIndex ();
        $attrs ['ASSERTION_SESSION_INDEX'] = $sessionIndex;

        $email = $username;
        $name = '';
        $saml_groups = '';
        //var_dump($attrs['tenantuserid']); die();
        $NameMapping = (string) $attribute['name'];
        $usernameMapping = $attribute['username'];
        $mailMapping = $attribute['email'];
        $groupsMapping = $attribute['grp'];

        if (!empty($usernameMapping) && isset($attrs[$usernameMapping]) && !empty($attrs[$usernameMapping])) {
            $username = $attrs[$usernameMapping];
            if(is_array($username)) {
                $username = $username[0];
            }
        }
        $username = str_replace("'",'',$username);
        if (!empty($mailMapping) && isset($attrs[$mailMapping]) && !empty($attrs[$mailMapping])) {
            $email = $attrs[$mailMapping];
            if(is_array($email)) {
                $email = $email[0];
            }
        }

        if (!empty($NameMapping)) {
            if(strpos($NameMapping,',')) {
                $nameArray = explode(',',$NameMapping);
                if(is_array($attrs[$nameArray[0]])) {
                    $fname = $attrs[$nameArray[0]][0];
                } else {
                    $fname = $attrs[$nameArray[0]];
                }
                if(is_array($attrs[$nameArray[1]])) {
                    $lname = $attrs[$nameArray[1]][0];
                } else {
                    $lname = $attrs[$nameArray[1]];
                }
                $name = $fname.' '.$lname;
            } else {
                if(isset($attrs[$NameMapping]) && !empty($attrs[$NameMapping])) {
                    $name = $attrs[$NameMapping];
                }
            }
        }

        if(is_array($name)) {
            $name = $name[0];
        }

        if (!empty($groupsMapping) && isset($attrs[$groupsMapping]) && !empty($attrs[$groupsMapping])) {
            $saml_groups = $attrs[$groupsMapping];
        } else {
            $saml_groups = array();
        }

        if(isset($attribute['enable_email']) && $attribute['enable_email'] == 0) {
            $matcher = 'username';
        } else {
            $matcher = 'email';
        }

        $result = self::get_user_from_joomla($matcher,$username,$email);

        /* if(isset($relayState)) {
            $login_url = $relayState;
        } else {
            $login_url = $sp_base_url;
        } */

        if($attribute['forward']) {
            $login_url = $attribute['forward'];
        } else {
            $login_url = JURI::root();
        }

        if($result) {
            self::loginCurrentUser($result, $attrs, $login_url, $name, $username, $email, $matcher, $app, $saml_groups);
        } else {
            self::RegisterCurrentUser($attrs, $login_url, $name, $username, $email, $saml_groups, $matcher, $app);
        }

    }

    public static function show_error_message($statusCode, $relayState) {

        if($relayState == 'testValidate') {
            echo '<div style="font-family:Calibri;padding:0 3%;">';
            echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;"> ERROR</div>
            <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong> Invalid SAML Response Status.</p>
            <p><strong>Causes</strong>: Identity Provider has sent \''.$statusCode.'\' status code in SAML Response. </p>
                            <p><strong>Reason</strong>: '.self::get_status_message($statusCode).'</p><br>
            </div>

            <div style="margin:3%;display:block;text-align:center;">
            <div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
            exit;
        } else {
            if($statusCode == 'RequestDenied' ) {
                echo 'You are not allowed to login into the site. Please contact your Administrator.';
                exit;
            } else {
                echo 'We could not sign you in. Please contact your Administrator.';
                exit;
            }

        }
    }

    public static function validateIssuerAndAudience($samlResponse, $spEntityId, $issuerToValidateAgainst, $relayState) {

        $issuer   = current($samlResponse->getAssertions())->getIssuer();
        $audience = current(current($samlResponse->getAssertions())->getValidAudiences());

        if(strcmp($issuerToValidateAgainst, $issuer) === 0) {

            if(strcmp($audience, $spEntityId) === 0) {
                return TRUE;
            } else {
                if($relayState == 'testValidate') {
                    ob_end_clean();

                    echo '<div style="font-family:Calibri;padding:0 3%;">';
                    echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;"> ERROR</div>
                    <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>Invalid Audience URI.</p>
                    <p>Please contact your administrator and report the following error:</p>
                    <p><strong>Possible Cause: </strong>The value of \'Audience URI\' field on Identity Provider\'s side is incorrect</p>
                    <p>Expected one of the Audiences to be: '.$spEntityId.'<p>
                    </div>
                    <div style="margin:3%;display:block;text-align:center;">
                    <div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
                    exit;
                } else {
                    echo ' <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>We could not sign you in. Please contact your Administrator.</p></div>
                           <div style="margin:3%;display:block;text-align:center;">
                          <form action='.JURI::root().'><input style="padding:1%;width:150px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="submit" value="Back to home"></form></div>';
                    exit;
                }
            }
        } else {
            if($relayState == 'testValidate') {
                ob_end_clean();

                echo '<div style="font-family:Calibri;padding:0 3%;">';
                echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;"> ERROR</div>
                    <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>Issuer cannot be verified.</p>
                    <p>Please contact your administrator and report the following error:</p>
                    <p><strong>Possible Cause: </strong>The value in \'IdP Entity ID or Issuer\' field in Service Provider Settings is incorrect</p>
                    <p><strong>Expected Entity ID: </strong>'.$issuer.'<p>
                    <p><strong>Entity ID Found: </strong>'.$issuerToValidateAgainst.'</p>
                    </div>
                    <div style="margin:3%;display:block;text-align:center;">
                    <div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
                exit;
            } else {
                echo ' <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>We could not sign you in. Please contact your Administrator.</p></div>
                        <div style="margin:3%;display:block;text-align:center;">
                        <form action='.JURI::root().'><input style="padding:1%;width:150px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="submit" value="Back to home"></form></div>';
                exit;
            }
        }
    }

    public static function sso_saml_show_test_result($username,$attrs,$siteUrl) {
        ob_end_clean();
        $siteUrl = $siteUrl. '/plugins/authentication/ssosaml/';
        echo '<div style="font-family:Calibri;padding:0 3%;">';
        if(!empty($username)) {
            echo '<div style="color: #3c763d;
                    background-color: #dff0d8; padding:2%;margin-bottom:20px;text-align:center; border:1px solid #AEDB9A; font-size:18pt;">TEST SUCCESSFUL</div>
                    <div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:15%;"src="'. $siteUrl . 'images/green_check.png"></div>';
        } else {
            echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">TEST FAILED</div>
                    <div style="color: #a94442;font-size:14pt; margin-bottom:20px;">WARNING: Some Attributes Did Not Match.</div>
                    <div style="display:block;text-align:center;margin-bottom:4%;"><img style="width:15%;"src="'. $siteUrl . 'images/wrong.png"></div>';
        }

        echo '<span style="font-size:14pt;"><b>Hello</b>, '.$username.'</span><br/><p style="font-weight:bold;font-size:14pt;margin-left:1%;">ATTRIBUTES RECEIVED:</p>
                <table style="border-collapse:collapse;border-spacing:0; display:table;width:100%; font-size:14pt;background-color:#EDEDED;">
                <tr style="text-align:center;"><td style="font-weight:bold;border:2px solid #949090;padding:2%;">ATTRIBUTE NAME</td><td style="font-weight:bold;padding:2%;border:2px solid #949090; word-wrap:break-word;">ATTRIBUTE VALUE</td></tr>';

        if(!empty($attrs)) {

            foreach ($attrs as $key => $value)
                echo "<tr><td style='font-weight:bold;border:2px solid #949090;padding:2%;'>" .$key . "</td><td style='padding:2%;border:2px solid #949090; word-wrap:break-word;'>" .implode('<br/>',(array)$value). "</td></tr>";
        } else {
            echo "No Attributes Received.";
            echo '</table></div>';
            echo '<div style="margin:3%;display:block;text-align:center;"><input style="padding:1%;width:100px;background: #0091CD none repeat scroll 0% 0%;cursor: pointer;font-size:15px;border-width: 1px;border-style: solid;border-radius: 3px;white-space: nowrap;box-sizing: border-box;border-color: #0073AA;box-shadow: 0px 1px 0px rgba(120, 200, 230, 0.6) inset;color: #FFF;"type="button" value="Done" onClick="self.close();"></div>';
            exit;
        }
    }

    public static function get_user_from_email($email) {
        //Check if email exist in database
        $db = JFactory::getDBO();
        $query = $db->getQuery(true)
            ->select('id')
            ->from('#__users')
            ->where('email=' . $db->quote($email));
        $db->setQuery($query);
        $user = $db->loadObject();
        return $user;
    }

    public static function get_user_from_joomla($matcher,$username,$email) {
        //Check if email exist in database
        $db = JFactory::getDBO();

        switch ($matcher) {
            case 'username':
                $query = $db->getQuery(true)
                            ->select('id')
                            ->from('#__users')
                            ->where('username=' . $db->quote($username));

            break;

            case 'email':
            default:
                $query = $db->getQuery(true)
                        ->select('id')
                        ->from('#__users')
                        ->where('email=' . $db->quote($email));
            break;
        }

        $db->setQuery($query);
        $result = $db->loadObject();
        return $result;
    }

    public static function loginCurrentUser($result, $attrs, $login_url, $name, $username, $email, $matcher, $app, $saml_groups){

        $user = JUser::getInstance($result->id);

        self::updateCurrentUserName($user->id, $name);

        $config = self::getConfig();
        $params = json_decode($config->params);
        if($config->group_mapping) {
            $group_mapping = json_decode($config->group_mapping);
        } else {
            $group_mapping = false;
        }

        if($config->field_mapping) {
            $field_mapping = json_decode($config->field_mapping);
        } else {
            $field_mapping = false;
        }

        if($params->enable_update_field_mapping && $field_mapping) {
            self::updateUserProfileFields($user,$field_mapping,$attrs);
        }

        jimport('joomla.user.helper');

        $default_group = $params->default_user_group;
        if($params->enable_update_group_mapping && $group_mapping && $params->enable_group_mapping) {
            $groupsArray[] = $default_group;
            if($params->enable_update_group_mapping && $group_mapping) {
                $groups = self::get_mapped_groups($group_mapping , $saml_groups);
                if (empty($groups)) {
                    $groupsArray[] = $default_group;
                } else {
                    foreach ($groups as $group) {
                        $groupsArray[] = $group;
                    }
                }
            }

            // old function JUserHelper::setUserGroups($user->id,$groupsArray);
            $current_user_groups = JUserHelper::getUserGroups($user->id);
            foreach($groupsArray as $addGroupId) {
                if($addGroupId != 8 && !in_array($addGroupId,$current_user_groups)) {
                    JUserHelper::addUserToGroup($user->id,$addGroupId);
                    AxsActions::trackUserGroup($user->id,$addGroupId,'add');
                }
            }

            foreach($current_user_groups as $removeGroupId) {
                if($removeGroupId != 8 && !in_array($removeGroupId,$groupsArray)) {
                    JUserHelper::removeUserFromGroup($user->id,$removeGroupId);
                    AxsActions::trackUserGroup($user->id,$removeGroupId,'remove');
                }
            }
        }

        AxsActions::storeAction($user->id,'login');
        $session = JFactory::getSession(); #Get current session vars
        // Register the needed session variables
        $session->set('user', $user);
        $session->set('MO_SAML_NAMEID', isset($attrs['NAME_ID'])? $attrs['NAME_ID']:'');
        $session->set('MO_SAML_SESSION_INDEX',isset($attrs['ASSERTION_SESSION_INDEX'])? $attrs['ASSERTION_SESSION_INDEX']:'');

        $app->checkSession();
        $sessionId = $session->getId();
        self::updateUsernameToSessionId($user->id,$user->username, $sessionId);

        $user->setLastVisit();

        $app->redirect(urldecode($login_url));
    }

    public static function RegisterCurrentUser($attrs, $login_url, $name, $username, $email, $saml_groups, $matcher, $app) {
        $config = self::getConfig();
        $params = json_decode($config->params);

        if($config->group_mapping) {
            $group_mapping = json_decode($config->group_mapping);
        } else {
            $group_mapping = false;
        }

        if($config->field_mapping) {
            $field_mapping = json_decode($config->field_mapping);
        } else {
            $field_mapping = false;
        }

        $default_group = $params->default_user_group;

        // user data
        if(isset($name) && !empty($name)) {
            $data['name'] = $name;
        } else {
            $data['name'] = $username;
        }

        $data['username'] = $username;
        $data['email'] = $data['email1'] = $data['email2'] = JStringPunycode::emailToPunycode($email);
        $data['password'] = $data['password1'] = $data['password2'] = JUserHelper::genRandomPassword();
        $data['activation'] = '0';
        $data['block'] = '0';
        $data['groups'][] = $default_group;
        if($params->enable_group_mapping && $group_mapping) {
            $groups = self::get_mapped_groups($group_mapping , $saml_groups);
            if (empty($groups)) {
                $data['groups'][] = $default_group;
            } else {
                foreach ($groups as $group) {
                    $data['groups'][] = $group;
                }
            }
        }

        // Get the model and validate the data.
        jimport('joomla.application.component.model');

        if (!defined('JPATH_COMPONENT')) {
            define('JPATH_COMPONENT', JPATH_BASE . '/components/');
        }

        $user = new JUser;
        //Write to database
        if(!$user->bind($data)) {
            //print_r($user->getError());exit;
            throw new Exception("Could not bind data. Error: " . $user->getError());
        }

        if (!$user->save()) {
            //print_r($user->getError());exit;
            $siteUrl = JURI::root();
            ob_end_clean();
            $siteUrl = $siteUrl. '/plugins/authentication/ssosaml/';
            echo '<div style="font-family:Calibri;padding:0 3%;">';
            echo '<div style="color: #a94442;background-color: #f2dede;padding: 15px;margin-bottom: 20px;text-align:center;border:1px solid #E6B3B2;font-size:18pt;">
            <img style="width:15;"src="'. $siteUrl . 'images/wrong.png"> ERROR</div>
            <div style="color: #a94442;font-size:14pt; margin-bottom:20px;"><p><strong>Error: </strong>Could not save user. '. $user->getError().'</p>
            <p>You are receiving this error because your email address is invalid.</p>
            <p>If you have checked your email address and the error still persists then please report following error to your System Administrator:
                    <ul>
                    <li>Attribute name for e-mail should be NAME_ID only.</li>
                    <li>Please change the attribute name in your IdP.</li>
                    </ul>
                    </p>

                </div>
                <div style="text-align:center;"><a href="index.php" target="_blank">Back to Home</a></div>';
            exit;
        }


        //Utilities::updateActivationStatusForUser($username);

        $result = self::get_user_from_joomla($matcher,$username,$email);
        if($result){
            $user = JUser::getInstance($result->id);
            if($params->enable_field_mapping && $field_mapping) {
                self::updateUserProfileFields($user,$field_mapping,$attrs);
            }

            $current_user_groups = JUserHelper::getUserGroups($user->id);
            foreach($current_user_groups as $addGroupId) {
                    AxsActions::trackUserGroup($user->id,$addGroupId,'add');
            }
            AxsActions::storeAction($user->id,'login');
            $session = JFactory::getSession(); #Get current session vars
            // Register the needed session variables
            $session->set('user',$user);
            $session->set('MO_SAML_NAMEID', $attrs['NAME_ID']);
            $session->set('MO_SAML_SESSION_INDEX', $attrs['ASSERTION_SESSION_INDEX']);

            $app->checkSession();
            $sessionId = $session->getId();
            self::updateUsernameToSessionId($user->id,$user->username, $sessionId);

            /* Update Last Visit Date */
            $user->setLastVisit();
            $app->redirect(urldecode($login_url), "Welcome $user->username", 'message');
        }

    }

    private static function updateUserProfileFields($user,$profile_fields,$idp_fields) {
        $db = JFactory::getDBO();
        foreach($profile_fields as $profile_field) {
            if($idp_fields[$profile_field->attribute]) {
                if(is_array($idp_fields[$profile_field->attribute])) {
                    $value = implode(',',$idp_fields[$profile_field->attribute]);
                } else {
                    $value = $idp_fields[$profile_field->attribute];
                }
                $fieldValues = new stdClass();
                $fieldValues->user_id  = $user->id;
                $fieldValues->field_id = $profile_field->field;
                $fieldValues->value    = $value;
                $fieldValues->access   = 40;
                $existingField = self::getProfileField($user->id,$profile_field->field);
                if($existingField) {
                    $fieldValues->id = $existingField->id;
                    $db->updateObject('#__community_fields_values',$fieldValues,'id');
                } else {
                    $db->insertObject('#__community_fields_values',$fieldValues);
                }
            }
        }
    }

    private static function getProfileField($user_id,$field_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__community_fields_values')
              ->where('user_id = '.(int)$user_id.' AND field_id = '.(int)$field_id)
              ->limit(1);
        $db->setQuery($query);
        return $db->loadobject();
    }

    public static function get_status_message($statusCode) {
        switch($statusCode){
            case 'RequestDenied':
                return 'You are not allowed to login into the site. Please contact your Administrator.';
            break;
            case 'Requester':
                return 'The request could not be performed due to an error on the part of the requester.';
            break;
            case 'Responder':
                return 'The request could not be performed due to an error on the part of the SAML responder or SAML authority.';
            break;
            case 'VersionMismatch':
                return 'The SAML responder could not process the request because the version of the request message was incorrect.';
            break;
            default:
                return 'Unknown';
            break;
        }
    }

    public static function updateCurrentUserName($id, $name) {
        if(empty($name)) {
            return;
        }

        $db     = JFactory::getDbo();
        $query  = $db->getQuery(true);
        $fields = array(
            $db->quoteName('name') . ' = ' . $db->quote($name),
        );

        $conditions = array(
            $db->quoteName('id') . ' = ' . $db->quote($id) ,
        );

        $query->update($db->quoteName('#__users'))
              ->set($fields)
              ->where($conditions);

        $db->setQuery($query);

        $result = $db->execute();
    }

    public static function get_config_id() {
        return self::$config_id;
    }

    public static function getConfig($id = null) {
        if(!$id) {
            $id = self::get_config_id();
        } else {
            $id = 1;
        }
        if(self::$config) {
            return self::$config;
        }
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__sso_saml_config'));
        $query->where($db->quoteName('id')." = ".(int)$id);
        $db->setQuery($query);
        $result = $db->loadObject();
        if(!$result) {
            return false;
        }
        return $result;
    }

    public static function updateUsernameToSessionId($userID,$username, $sessionId) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $fields = array(
            $db->quoteName('username') . ' = ' . $db->quote($username),
            $db->quoteName('guest') . ' = ' . $db->quote('0'),
            $db->quoteName('userid') . ' = ' . $db->quote($userID),
        );

        $conditions = array(
            $db->quoteName('session_id') . ' = ' . $db->quote($sessionId),
        );

        $query->update($db->quoteName('#__session'))
              ->set($fields)
              ->where($conditions);

        $db->setQuery($query);
        $result = $db->execute();
    }

    public static function get_mapped_groups($group_mapping, $saml_groups) {
        if(strpos($saml_groups[0], ';')) {
            $saml_groups = explode(';', $saml_groups[0]);
        }
        foreach ($group_mapping as $group) {
            if ($group->user_group && in_array($group->idp_group,$saml_groups) && $group->user_group != 8) {
                $groups[] = (int)$group->user_group;
            }
        }
        return array_unique($groups);
    }
}