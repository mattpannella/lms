<?php

defined('_JEXEC') or die;

class AxsFacebook {

	private static $appID = '300197817159121';
	private static $accessToken = '300197817159121%7C31a76f829885d3e0411d1ca01328354d';

	public function checkDomain($domain) {
		$splitdomain = explode('.',$domain);
		if(count($splitdomain) == 3) {
		  $basedomain = $splitdomain[1].'.'.$splitdomain[2];
		} else {
		  $basedomain = $domain;
		}
		$response = file_get_contents("https://graph.facebook.com/v2.11/".self::$appID."?fields=app_domains&access_token=".self::$accessToken);
		$domains = json_decode($response);
		if(in_array($basedomain,$domains->app_domains)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function addDomain($domain) {
		$response = file_get_contents("https://graph.facebook.com/v2.11/".self::$appID."?fields=app_domains&access_token=".self::$accessToken);
		$domains = json_decode($response);
		array_push($domains->app_domains, $domain);
		$domainList = "'" .implode("', '", $domains->app_domains) . "'";
		$url = "https://graph.facebook.com/v2.11/".self::$appID."?access_token=".self::$accessToken;
		$fields = array(
		          'app_domains' =>  '['.$domainList.']'
		      );
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec( $ch );
		$added = json_decode($result);
		return $added->success;
	}
}