<?php
defined('_JEXEC') or die;
include JPATH_LIBRARIES.'/axslibs/iCalParser.php';

class AxsCalendar {

    /* public static function downloadAllIcsFiles() {
        $timezoneList = AxsTimezones::getTimezoneListArray();
        $i = 0;
        foreach($timezoneList as $tz) {
            $file = 'http://tzurl.org/zoneinfo-outlook/'.$tz.'.ics';
            $loadFile = file_get_contents( $file );
            $timezoneParts = explode('/',$tz);
            $directory = '/mnt/shared/22/ics_files/'.$timezoneParts[0];

            if(!file_exists($directory)) {
                mkdir($directory);
            }

            if(!file_exists($directory."/".$timezoneParts[1].".ics")) {
                copy($file,$directory."/".$timezoneParts[1].".ics");
                $i++;
            }
        }
        return 'Files downloaded: '.$i;
    } */

    public static function getVtimezoneData($timezone) {
        $timezoneList = AxsTimezones::getTimezoneListArray();

        if(!$timezone || !in_array($timezone,$timezoneList)) {
            return false;
        }

        $file = '/mnt/shared/22/ics_files/'.$timezone.'.ics';
        if(!file_exists($file)) {
            return false;
        }

        $iCal = new iCalParser();
        $iCalData = $iCal->load( file_get_contents( $file ) );
        if($iCalData) {
            $VTIMEZONE = $iCalData['VTIMEZONE'];
            $content  = "BEGIN:VTIMEZONE\n";
            $content .= "TZID:$timezone\n";
            $content .= "TZURL:$file\n";
            $content .= "X-LIC-LOCATION:$timezone\n";
            if($VTIMEZONE['DAYLIGHT'][0]) {
                $daylight = $VTIMEZONE['DAYLIGHT'][0];
                $content .= "BEGIN:DAYLIGHT\n";
                if($daylight['TZOFFSETFROM']) {
                    $content .= "TZOFFSETFROM:".$daylight['TZOFFSETFROM']."\n";
                }
                if($daylight['TZOFFSETTO']) {
                    $content .= "TZOFFSETTO:".$daylight['TZOFFSETTO']."\n";
                }
                if($daylight['TZNAME']) {
                    $content .= "TZNAME:".$daylight['TZNAME']."\n";
                }
                if($daylight['DTSTART']) {
                    $content .= "DTSTART:".$daylight['DTSTART']."\n";
                }
                if($daylight['RRULE']) {
                    $RRULE = '';
                    if($daylight['RRULE']['FREQ']) {
                        $RRULE .= "FREQ=".$daylight['RRULE']['FREQ'].";";
                    }
                    if($daylight['RRULE']['BYMONTH']) {
                        $RRULE .= "BYMONTH=".$daylight['RRULE']['BYMONTH'].";";
                    }
                    if($daylight['RRULE']['BYDAY']) {
                        $RRULE .= "BYDAY=".$daylight['RRULE']['BYDAY'].";";
                    }
                    if($RRULE) {
                        $content .= "RRULE:".$RRULE."\n";
                    }
                }
                $content .= "END:DAYLIGHT\n";
            }

            if($VTIMEZONE['STANDARD'][0]) {
                $standard = $VTIMEZONE['STANDARD'][0];
                $content .= "BEGIN:STANDARD\n";
                if($standard['TZOFFSETFROM']) {
                    $content .= "TZOFFSETFROM:".$standard['TZOFFSETFROM']."\n";
                }
                if($standard['TZOFFSETTO']) {
                    $content .= "TZOFFSETTO:".$standard['TZOFFSETTO']."\n";
                }
                if($standard['TZNAME']) {
                    $content .= "TZNAME:".$standard['TZNAME']."\n";
                }
                if($standard['DTSTART']) {
                    $content .= "DTSTART:".$standard['DTSTART']."\n";
                }
                if($standard['RRULE']) {
                    $RRULE = '';
                    if($standard['RRULE']['FREQ']) {
                        $RRULE .= "FREQ=".$standard['RRULE']['FREQ'].";";
                    }
                    if($standard['RRULE']['BYMONTH']) {
                        $RRULE .= "BYMONTH=".$standard['RRULE']['BYMONTH'].";";
                    }
                    if($standard['RRULE']['BYDAY']) {
                        $RRULE .= "BYDAY=".$standard['RRULE']['BYDAY'].";";
                    }
                    if($RRULE) {
                        $content .= "RRULE:".$RRULE."\n";
                    }
                }
                $content .= "END:STANDARD\n";
            }

            $content .= "END:VTIMEZONE\n";
        }

        if($content) {
            return $content;
        } else {
            return false;
        }

    }
}
