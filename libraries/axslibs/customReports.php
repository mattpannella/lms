<?php

defined('_JEXEC') or die;

class AxsCustomReports {
	public function getCustomReport($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_reports_custom');
        $query->where('id = '.(int)$id);
        $query->limit(1);
        $db->setQuery($query);
        $report = $db->loadObject();
        return $report;
    }

    public function getCustomReportSubmissions($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('axs_reports_custom_submissions');
        $query->where('report_id = '.(int)$id);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    } 
}