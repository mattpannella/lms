<?php

defined('_JEXEC') or die;

class AxsLearnerDashboard {

	public static function buildUserAwardBarHTML($userId,$is_admin = false,$params = null) {
		$html = '';
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		if(!$params->show_badges && !$params->show_certificates && !$params->show_leaderboard) {
			return false;
		}
		$badges 	  = self::getBadges($userId);
		$certificates = self::getCertificates($userId);
		include 'components/com_axs/templates/award_bar.php';
	}

	public static function buildUserProfileBannerHTML($userId,$is_admin = false,$params = null) {
		$html = '';
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		include 'components/com_axs/templates/profile_banner.php';
	}

	public static function buildLeaderBoardHTML($userId,$is_admin = false,$params = null) {
		$html = '';
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		include 'components/com_axs/templates/leader_board.php';
	}

	public static function buildUserCoursesHTML($userId,$is_admin = false,$params = null) {
		$html = '';
		if(!$userId) {
			$userId = JFactory::getUser()->id;
		}
		$courses = AxsLMS::getUserCourses($userId);
		include 'components/com_axs/templates/course_list.php';
	}

	public static function ordinalNumber($number) {
	    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13)) {
	        return $number. 'th';
	    } else {
	        return $number. $ends[$number % 10];
	    }
    }

    public static function getTeamUserList($userId) {
        $teams = self::getUserTeams($userId);

        if(!$teams) {
            return false;
        }

        $data = new stdClass();
        $allTeamsArray = array();
        $allTeamsList = array();

        foreach($teams as $team) {
            $userList = null;

            switch($team->member_type) {
                case "individuals":
                    if($team->user_ids) {
                        $userList = $team->user_ids;
                    }
                break;
                case "groups":
                    if($team->user_groups) {
                        $userList = self::getUsersFromGroupList($team->user_groups);
                    }
                break;
            }

            if($userList) {
                $team->members = $userList;
                $teamMemberArray = explode(',',$userList);

                foreach($teamMemberArray as $index => $userId) {

                    $allTeamsArray[] = $userId;
                }
            }
        }        
        
        $allTeamsList = array_unique($allTeamsArray);

        $data->allTeamsList = $allTeamsList;
        $data->teams = $teams;

        return $data;
    }

    public static function getUserTeams($userId) {
        $db = JFactory::getDBO();
        $groups = JAccess::getGroupsByUser($userId);
        $groupsCheck = [];
        foreach($groups as $group) {
                    $groupsCheck[] = " ( FIND_IN_SET( $group, user_groups ) > 0 ) ";
                }
        $groupsSplit = implode(' OR ', $groupsCheck);
        if($groupsSplit) {
            $groupConditionSet = "  ( member_type = 'groups' AND ( $groupsSplit ) ) OR";
        } else {
            $groupConditionSet = "";
        }
        $conditionsSet =
            $groupConditionSet."
            ( FIND_IN_SET( $userId, user_ids ) > 0  AND member_type = 'individuals')
            OR
            ( FIND_IN_SET( $userId, team_lead ) > 0 )
        ";
        $conditions[] = "($conditionsSet)";
        $conditions[] = "enabled = 1";
		$query = $db->getQuery(true);
		$query->select('*')
		      ->from('axs_teams')
              ->where($conditions);
		$db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public static function getUsersFromGroupList($groups) {
        $db = JFactory::getDbo();

        $conditions[] = "group_id IN ($groups)";

        $query = $db->getQuery(true);
        $query->select('DISTINCT user_id')
              ->from('#__user_usergroup_map')
              ->where($conditions);

        $db->setQuery($query);

        $result = $db->loadColumn();
        
        if(!$result) {
            return false;
        } else {
            return implode(',', $result);
        }
    }

	public static function getLeaderboardList($leaderboardAmount,$userList = null) {
		$db = JFactory::getDBO();
        $limit = (int)$leaderboardAmount;
        $conditions = [];

        if($userList) {
            $conditions[] = "user_id IN (".$userList.")";
        }

		//$conditions[] = "category_id IN (1,2)";
		$query = $db->getQuery(true);
		$query->select('SUM(ap.points) AS total, ap.user_id, ap.category_id');
        $query->from('axs_points ap');
        $query->leftJoin('#__users u ON u.id = ap.user_id');

        // Now, filter out any users who are non-existent
        $conditions[] = 'u.id IS NOT NULL';

        $query->where($conditions);

		$query->group('user_id');
		$query->order('total DESC');
        $query->setLimit($limit);
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
    }

    public static function getLeaderBoardPointsForUser($userId) {
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);

        $query->select('SUM(points) AS totalPoints')
              ->from('axs_points')
              ->where("{$db->qn('user_id')} = $userId");

        $db->setQuery($query);

        $leaderboardPoints = $db->loadObject();

        return is_null($leaderboardPoints) ? 0 : $leaderboardPoints->totalPoints;
    }

	public static function getCertificateById($certificate_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards_certificates'))
              ->where($db->qn('id').'='.(int)$certificate_id)
              ->setLimit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();
        return $certificate;
    }

    public static function getAwardById($award_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_awards'))
              ->where($db->qn('id').'='.(int)$award_id)
              ->setLimit(1);
        $db->setQuery($query);
        $certificate = $db->loadObject();
        return $certificate;
    }

    public static function getDashboard($dashboard_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from($db->qn('axs_learner_dashboards'))
              ->where($db->qn('id').'='.(int)$dashboard_id)
              ->setLimit(1);
        $db->setQuery($query);
        $dashboard = $db->loadObject();
        return $dashboard;
    }

    public static function getCertificates($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('award.*,badge.*')
              ->from('axs_awards_earned as award')
              ->join('INNER','axs_awards as badge ON award.badge_id = badge.id')
              ->where($db->qn('award.user_id').'='.(int)$user_id)
              ->where($db->qn('badge.type')." = 'certificate'")
              ->order('id DESC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getAllAwards() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_awards')
              ->order('title ASC');
        $db->setQuery($query);
        $results = $db->loadObjectList();

        $html = '';

        foreach($results as $award) {
            $html .= '<option value="'.$award->id.'">'.$award->title.'</option>';
        }
        return $html;
    }

    public static function getAllUserAwards($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('award.*,badge.*')
              ->from('axs_awards_earned as award')
              ->join('INNER','axs_awards as badge ON award.badge_id = badge.id')
              ->where($db->qn('award.user_id').'='.(int)$user_id)
              ->order('id DESC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getBadges($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('award.*,badge.*')
              ->from('axs_awards_earned as award')
              ->join('INNER','axs_awards as badge ON award.badge_id = badge.id')
              ->where($db->qn('award.user_id').'='.(int)$user_id)
              ->where($db->qn('badge.type')." = 'badge'")
              ->order('id DESC');
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function getFirstAndLastName($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('field.id,field.fieldcode,value.value')
              ->from('#__community_fields as field')
              ->join('INNER','#__community_fields_values as value ON field.id = value.field_id')
              ->where($db->qn('value.user_id').'='.(int)$user_id)
              ->where($db->qn('field.fieldcode')." IN ('FIELD_GIVENNAME', 'FIELD_FAMILYNAME')");
        $db->setQuery($query);
        $results = $db->loadAssocList();
        if($results) {
            return array_reduce($results, function($acc, $item) {
                $acc[$item['fieldcode']] = $item['value'];
                return $acc;
            }, []);
        } else {
            return null;
        }
    }

    public static function getFirstName($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('field.id,value.value')
              ->from('#__community_fields as field')
              ->join('INNER','#__community_fields_values as value ON field.id = value.field_id')
              ->where($db->qn('value.user_id').'='.(int)$user_id)
              ->where($db->qn('field.fieldcode')." = 'FIELD_GIVENNAME'")
              ->setLimt(1);
        $db->setQuery($query);
        $results = $db->loadObject();
        if($results) {
            return $results->value;
        } else {
            return null;
        }
    }

    public static function getLastName($user_id) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('field.id,value.value')
              ->from('#__community_fields as field')
              ->join('INNER','#__community_fields_values as value ON field.id = value.field_id')
              ->where($db->qn('value.user_id').'='.(int)$user_id)
              ->where($db->qn('field.fieldcode')." = 'FIELD_FAMILYNAME'")
              ->setLimt(1);
        $db->setQuery($query);
        $results = $db->loadObject();
        if($results) {
            return $results->value;
        } else {
            return null;
        }
    }

    public static function buildUserEventsHTML($userId,$is_admin = false,$params = null) {
        $html = '';
        if(!$userId) {
            $userId = JFactory::getUser()->id;
        }
        $table = '#__bbb_meetings';
        $conditions[] = "( FIND_IN_SET( $userId, hosts ) > 0 )";
        $conditions[] = "(creator = $userId)";
        $fields ="*,meetingName as title, date as event_date, 'true' as 'virtual' ";
        $events1 = self::getUserAccessContent($userId,$table,$conditions,$fields);
        $events2 = AxsTranscripts::getTranscriptEvents($userId);
        $events1 = !empty($events1) ? $events1 : [];
        $events2 = !empty($events2) ? $events2 : [];
        $events = array_merge($events1,$events2);
        usort($events, function($a, $b) {
            return strtotime($a->event_date) - strtotime($b->event_date);
        });
        $key = AxsKeys::getKey('lms');
        include 'components/com_axs/templates/event_list.php';
    }

    public static function getUserAccessContent($userId,$table,$conditions = null, $fields = '*') {
        $db = JFactory::getDBO();
        if(!$table) {
            return false;
        }
        $query = self::buildAccessQuery($userId,$table,$conditions,$fields);
        $db->setQuery($query);
        $results = $db->loadObjectList();
        return $results;
    }

    public static function buildAccessQuery($userId,$table,$conditions = null, $fields = '*') {
        if(!$userId) {
            $userId = JFactory::getUser()->id;
        }
        if(!$table) {
            return false;
        }
        $levels = JAccess::getAuthorisedViewLevels($userId);
        $groups = JAccess::getGroupsByUser($userId);
        $levelsCheck = [];
        $groupsCheck = [];
        if(!$conditions) {
            $conditions  = [];
        }

        foreach($levels as $level) {
            $levelsCheck[] = " ( FIND_IN_SET( $level, accesslevel ) > 0 ) ";
        }

        foreach($groups as $group) {
            $groupsCheck[] = " ( FIND_IN_SET( $group, usergroup ) > 0 ) ";
        }

        $levelsSplit = implode(' OR ', $levelsCheck);
        $groupsSplit = implode(' OR ', $groupsCheck);
        $where   = " WHERE enabled = 1 ";

        //$where  .= " AND   DATE(start_date) <= NOW() ";
        //$where  .= " AND   ( DATE(end_date) > NOW() OR end_date = 0 ) ";
        $conditions[] = "  ( access_type = 'access' AND ( $levelsSplit ) ) ";
        $conditions[] = "  ( access_type = 'user'   AND ( $groupsSplit ) ) ";
        $conditions[] = "  ( access_type = 'list'   AND ( FIND_IN_SET( $userId, userlist ) > 0 ) ) ";
        $conditions[] = "  ( access_type = 'none' ) ";

        $conditionsSplit = implode(' OR ', $conditions);

        $where .= ' AND ('.$conditionsSplit.')';

        $query = "SELECT $fields FROM $table $where ORDER BY id DESC";

        return $query;

    }


}
