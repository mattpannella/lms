<?php
// error_reporting(E_ALL | E_NOTICE);
// ini_set('display_errors', 1);

defined('_JEXEC') or die;

//include_once JPATH_ROOT.'/components/com_community/libraries/core.php';

class AxsAlerts {

    private $fromname;
    private $mailfrom;
    private $db;
    private $mailer;
    private $sender;
    private $user;
    private $course;
    private $award;
    private $award_title;
    private $course_list;
    private $brand_id;
    private $assigned_start_date;
    private $assigned_end_date;
    private $assigned_due_date;
    private $expiration_date;
    private $course_due_date;
    private $subscription;
    private $course_purchase_amount;
    private $course_purchase_date;
    private $assignment_title;
    private $assignment_due_date;

    public function __construct($params = null) {

        $this->db = JFactory::getDBO();

        // Get the database from the params if params exist
        if(!empty($params) && property_exists($params, 'db')) {

            $this->db = $params->db;
        }
    }

    public function sendMail($params) {
    	$db = $this->db;
    	$mailer = $this->mailer;
        $mailer->clearAllRecipients();
        $mailer->clearAddresses();
        $mailer->clearReplyTos();
        $mailer->setSender($this->sender);
        $mailer->addRecipient($this->user->email);
        $mailer->addReplyTo($this->reply);
        $mailer->setSubject($params->subject);
        $mailer->isHtml(true);
        $mailer->Encoding = 'base64';
        $mailer->setBody($params->message);
        return $mailer->Send();
    }

    public function addToEmailQueue($params) {
        if(!$this->user->email) {
            return false;
        }
        $today = date("Y-m-d H:i:s");
        $code = AxsEmail::getCode();
        $recipient = new stdClass();
        $recipient->name  = $this->user->name;
        $recipient->email = $this->user->email;
        $recipients[] = $recipient;
        $axs_mailer = new AxsEmail();
        $axs_mailer->content = $params->message;
        $axs_mailer->subject = $params->subject;
        //$axs_mailer->attachment = json_encode($message->attachment);
        $axs_mailer->code = $code;
        $axs_mailer->recipients = $recipients;
        $axs_mailer->date = $today;
        $axs_mailer->brand_id = $this->brand_id;
        $axs_mailer->db = $this->db;
        $result = $axs_mailer->send();
    }

    public function getAlertData($params) {
        $db = $this->db;
        $conditions[] = "enabled = 1";
        if($params->alert_type != 'course assigned' && $params->alert_type != 'course recommended') {
            $conditions[] = "($params->id IN ($params->column) OR selection_type = 'all')";
        } else {
            $courses = explode(',', $params->item->course_ids);
            foreach($courses as $course) {
                $courseFilter[] = "$course IN ($params->column)";
            }
            $courseFilter[] = "selection_type = 'all'";
            $courseFilter = implode(' OR ',$courseFilter);
            $conditions[] = "($courseFilter)";
        }
        $conditions[] = "alert_type = " . $db->quote($params->alert_type);
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_auto_alerts')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }

    public function setCourse($id) {
        $db = $this->db;
        $conditions[] = "splms_course_id = ".(int)$id;
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__splms_courses')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObject();
        $this->course = $result;
    }

    public function setSubscription($id) {
        $db = $this->db;
        $conditions[] = "id = ".(int)$id;
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_pay_subscription_plans')
              ->where($conditions)
              ->limit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        $this->subscription = $result;
    }

    public function setCourseList($item) {
        $db = $this->db;
        if(!$item->course_ids) {
            return '';
        }
        $titles = '<ol>';
        $courses = explode(',', $item->course_ids);
        foreach($courses as $course) {
            $conditions = "splms_course_id = ".(int)$course;
            $query = $db->getQuery(true);
            $query->select('*')
                  ->from('#__splms_courses')
                  ->where($conditions);
            $db->setQuery($query);
            $result = $db->loadObject();
            $titles .= '<li>'.$result->title.'</li>';
        }
        $titles .= '</ol>';
        $this->course_list = $titles;
        if($item->start_date) {
            $this->assigned_start_date = date('m/d/Y',strtotime($item->start_date));
        }

        if($item->end_date) {
            $this->assigned_end_date   = date('m/d/Y',strtotime($item->end_date));
        }

        if($item->due_date) {
            $this->assigned_due_date   = date('m/d/Y',strtotime($item->due_date));
        }

    }

    private function clearVariables() {
        $this->award = '';
        $this->award_title = '';
        $this->expiration_date = '';
        $this->course_due_date = '';
        $this->course_list = '';
        $this->assigned_start_date = '';
        $this->assigned_end_date = '';
        $this->assigned_due_date = '';
        $this->course = '';
    }

    public function setAward($award) {
        $db = $this->db;
        $conditions[] = "id = ".(int)$award->badge_id;
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_awards')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObject();
        $this->award = $result;
        if($award->date_expires) {
            $this->expiration_date = date('m/d/Y',strtotime($award->date_expires));
        }
        if($result->title) {
            $this->award_title = $result->title;
        }
    }

    /**
     * Returns an array of user ids belonging to the group ids
     * 
     * @param string $groups - comma separated list or array of group ids
     * 
     * @return array
     */
    public static function getUsersFromGroupList($groups) {

        $db = JFactory::getDbo();

        $groups_list = explode(',', $groups);
        foreach($groups_list as $group) {
            $group_query[] = 'group_id = '. (int)$group;
        }
        $groupSet = implode(' OR ', $group_query);
        $conditions = '('.$groupSet.')';
        $query = $db->getQuery(true);
        $query->select('DISTINCT user_id')
              ->from('#__user_usergroup_map')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        if ($result != null) {
            return $result;
        } else {
            return array();
        }
    }

    public function replacePlaceholders($string) {
        $string = str_replace('[name]', $this->user->name, $string);
        $string = str_replace('[username]', $this->user->username, $string);
        if ($this->course != null) {
            $string = str_replace('[course]', $this->course->title, $string);
        }
        $string = str_replace('[certificate]', $this->award_title, $string);
        $string = str_replace('[badge]', $this->award_title, $string);
        $string = str_replace('[courselist]', $this->course_list, $string);
        $string = str_replace('[assigned_start_date]', $this->assigned_start_date, $string);
        $string = str_replace('[assigned_end_date]', $this->assigned_end_date, $string);
        $string = str_replace('[assigned_due_date]', $this->assigned_due_date, $string);
        $string = str_replace('[expiration_date]', $this->expiration_date, $string);
        $string = str_replace('[course_due_date]', $this->course_due_date, $string);
        $string = str_replace('[course_purchase_amount]', $this->course_purchase_amount, $string);
        $string = str_replace('[course_purchase_date]', $this->course_purchase_date, $string);
        if ($this->subscription != null) {
            $string = str_replace('[subscription]', $this->subscription->title, $string);
        }
        $string = str_replace('[assignment_due_date]', $this->assignment_due_date, $string);
        $string = str_replace('[assignment_title]', $this->assignment_title, $string);

        return $string;
    }

    public function newCourseRegistration($params) {
        $this->sendAutoAlert($params);
    }

    public function newSubscriptionRegistration($params) {
        $this->sendAutoAlert($params);
    }

    public function coursesAssigned($params) {
        if($params->item->user_ids) {
            $user_ids = explode(',',$params->item->user_ids);
            foreach($user_ids as $user_id) {
                $this->user = JFactory::getUser($user_id);
                $this->sendAutoAlert($params);
            }
        } elseif($params->item->usergroups) {
            $groupUsers = $this->getUsersFromGroupList($params->item->usergroups);
            foreach($groupUsers as $user) {
                $uid = (int)$user->user_id;
                $this->user = JFactory::getUser($uid);
                $this->sendAutoAlert($params);
            }
        } else {
            return false;
        }

    }

    public function sendAutoAlert($params) {
        $alerts = $this->getAlertData($params);
        $sendMethod = $params->send_type;

        if(is_iterable($alerts)) {

            foreach($alerts as $alert) {
                $this->mailer   = AxsEmail::getMailerByBrandId($alert->brand_id, $this->db);
                $this->brand_id = $alert->brand_id;
                $this->fromname = $this->mailer->FromName;
                $this->mailfrom = $this->mailer->From;
                $this->sender   = array($this->mailfrom,$this->fromname);
                $this->reply    = array($this->mailfrom,$this->fromname);
                $alertParams = json_decode($alert->params);
                $params->subject = $this->replacePlaceholders($alertParams->subject);
                $params->message = $this->replacePlaceholders($alertParams->{$params->message_type});
                $this->$sendMethod($params);
            }
        }
    }

    public function runAutoAlert($type,$params) {
        $params->alert_type = $type;
        switch($type) {
            case 'course registration':
                $params->column = 'courses';
                $params->id = $params->course_id;
                $params->message_type = 'course_registration_message';
                $params->send_type = 'addToEmailQueue';
                $method = 'newCourseRegistration';
                $this->clearVariables();
                $this->setCourse($params->course_id);
                if(!$params->user_id) {
                    $this->user = JFactory::getUser();
                } else {
                    $this->user =  JFactory::getUser($params->user_id);
                }

            break;

            case 'course purchase':
                $params->column = 'courses';
                $params->id = $params->course_id;
                $params->message_type = 'course_purchase_message';
                $params->send_type = 'addToEmailQueue';
                $method = 'newCourseRegistration';
                $this->course_purchase_amount = $params->amount;
                $this->course_purchase_date = $params->date;
                $this->clearVariables();
                $this->setCourse($params->course_id);
                if(!$params->user_id) {
                    $this->user = JFactory::getUser();
                } else {
                    $this->user =  JFactory::getUser($params->user_id);
                }

            break;

            case 'subscription registration':
                $params->column = 'subscription_plans';
                $params->id = $params->subscription_id;
                $params->message_type = 'subscription_registration_message';
                $params->send_type = 'addToEmailQueue';
                $method = 'newSubscriptionRegistration';
                $this->clearVariables();
                $this->setSubscription($params->subscription_id);
                if(!$params->user_id) {
                    $this->user = JFactory::getUser();
                } else {
                    $this->user =  JFactory::getUser($params->user_id);
                }

            break;

            case 'course assigned':
                $params->message_type = 'course_assigned_message';
                $params->send_type = 'addToEmailQueue';
                $params->column = 'courses';
                $method = 'coursesAssigned';
                $this->clearVariables();
                $this->setCourseList($params->item);
            break;

            case 'course recommended':
                $params->message_type = 'course_recommended_message';
                $params->send_type = 'addToEmailQueue';
                $params->column = 'courses';
                $method = 'coursesAssigned';
                $this->clearVariables();
                $this->setCourseList($params->item);
            break;

            case 'course due':
                $params->column = 'courses';
                $params->id = $params->course_id;
                $params->message_type = 'course_due_message';
                $params->send_type = 'addToEmailQueue';
                $method = 'newCourseRegistration';
                $this->clearVariables();
                $this->setCourse($params->course_id);
            break;

            case 'certificate or badge expiration':
                $params->column = 'certificate_badges';
                $params->id = $params->award_id;
                $params->message_type = 'certificate_expiration_message';
                $params->send_type = 'addToEmailQueue';
                $method = 'newCourseRegistration';
                $this->clearVariables();
                $this->setAward($params->award_id);
            break;
        }
        if(method_exists($this, $method)) {
            return $this->$method($params);
        }
    }

    public function getDailyCoursesDueAlerts() {
        $db = $this->db;
        $conditions[] = "enabled = 1";
        $conditions[] = "alert_type = 'course due'";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_auto_alerts')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        foreach($result as $alert) {
            $conditions = array();
            $alertParams    = json_decode($alert->params);
            $this->mailer   = AxsEmail::getMailerByBrandId($alert->brand_id, $this->db);
            $this->brand_id = $alert->brand_id;
            $this->fromname = $this->mailer->FromName;
            $this->mailfrom = $this->mailer->From;
            $this->sender   = array($this->mailfrom,$this->fromname);
            $this->reply    = array($this->mailfrom,$this->fromname);

            if(!empty($alert->courses) && $alert->selection_type == 'selected') {
                if (substr($alert->courses, 0, 1) == ',') {
                    $alert->courses = substr($alert->courses, 1);
                }
                $conditions[] = 'course_id IN ('.$alert->courses.')';
            }
            if($alert->interval_amount && $alert->interval_type && $alert->interval_time) {
                if($alert->interval_time == 'after') {
                    $operator = ' - ';
                }
                if($alert->interval_time == 'before') {
                    $operator = ' + ';
                }
                $interval_type = str_replace('s','',$alert->interval_type);
                $conditions[] = "DATE(date_due) = DATE( NOW() $operator INTERVAL $alert->interval_amount $interval_type )";
            } else {
                $conditions[] = "DATE(date_due) = DATE( NOW() )";
            }

            $query = 'SELECT * FROM (SELECT *, MAX(progress) AS top_progress, MAX(date_completed) AS top_date_completed
            FROM joom_splms_course_progress
            WHERE date_due IS NOT NULL AND archive_date IS NULL
            GROUP BY user_id, course_id) as courses ';

            $conditions[] = 'courses.top_date_completed IS NULL';

            if($conditions) {
                $query .= ' WHERE '.implode(' AND ',$conditions);
            }

            $db->setQuery($query);

            $coursesDue = $db->loadObjectList();

            foreach($coursesDue as $courseDue) {
                $this->clearVariables();
                $this->setCourse($courseDue->course_id);
                $this->course_due_date = date('m/d/Y',strtotime($courseDue->date_due));
                $this->user = JFactory::getUser($courseDue->user_id);
                $params = new stdClass();
                $params->subject = $this->replacePlaceholders($alertParams->subject);
                $params->message = $this->replacePlaceholders($alertParams->course_due_message);
                $this->addToEmailQueue($params);
            }
        }
    }

    public function getDailyAssignmentsDueAlerts() {
        $db = $this->db;
        $conditions[] = "enabled = 1";
        $conditions[] = "alert_type = 'assignment due'";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_auto_alerts')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        foreach($result as $alert) {
            $conditions = array();
            $alertParams    = json_decode($alert->params);
            $this->mailer   = AxsEmail::getMailerByBrandId($alert->brand_id, $this->db);
            $this->brand_id = $alert->brand_id;
            $this->fromname = $this->mailer->FromName;
            $this->mailfrom = $this->mailer->From;
            $this->sender   = array($this->mailfrom,$this->fromname);
            $this->reply    = array($this->mailfrom,$this->fromname);

            if($alert->interval_amount && $alert->interval_type && $alert->interval_time) {
                if($alert->interval_time == 'after') {
                    $operator = ' - ';
                }
                if($alert->interval_time == 'before') {
                    $operator = ' + ';
                }
                $interval_type = str_replace('s','',$alert->interval_type);
                $conditions[] = "DATE(due_date) = DATE( NOW() $operator INTERVAL $alert->interval_amount $interval_type )";
            } else {
                $conditions[] = "DATE(due_date) = DATE( NOW() )";
            }

            $query = 'SELECT * FROM joom_splms_courses_groups';

            if(!empty($conditions)) {
                $query .= ' WHERE ' . implode(' AND ', $conditions);
            }

            $db->setQuery($query);

            $assigmentsDue = $db->loadObjectList();

            foreach($assigmentsDue as $assignmentDue) {
                $this->clearVariables();
                $this->assignment_title = $assignmentDue->title;
                $this->setCourseList($assignmentDue);
                $this->assignment_due_date = date('m/d/Y', strtotime($assignmentDue->due_date));

                // Check whether usergroups or userids have been selected
                // for this course assignment
                $user_ids = array();
                $params = json_decode($assignmentDue->params);
                if ($params->assignment_filter_type == null || $params->assignment_filter_type == 'users') { 
                    $user_ids = explode(',', $assignmentDue->user_ids);
                } else if ($params->assignment_filter_type == 'groups') {
                    $user_ids_from_groups = self::getUsersFromGroupList($assignmentDue->usergroups);
                    foreach($user_ids_from_groups as $user_id_object) {
                        $user_ids []= $user_id_object->user_id;
                    }
                }
                  
                $user_ids = array_unique($user_ids);
                foreach ($user_ids as $user_id) {
                    $this->user = JFactory::getUser($user_id);
                    $params = new stdClass();
                    $params->subject = $this->replacePlaceholders($alertParams->subject);
                    $params->message = $this->replacePlaceholders($alertParams->assignment_due_message);
                    $this->addToEmailQueue($params);
                }
                
            }
        }
    }


    public function getDailyExpirationAlerts() {
        $db = $this->db;
        $conditions[] = "enabled = 1";
        $conditions[] = "(alert_type = 'certificate expiration')";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('axs_auto_alerts')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();

        foreach($result as $alert) {
            $conditions = array();
            $alertParams    = json_decode($alert->params);
            $this->mailer   = AxsEmail::getMailerByBrandId($alert->brand_id, $this->db);
            $this->brand_id = $alert->brand_id;
            $this->fromname = $this->mailer->FromName;
            $this->mailfrom = $this->mailer->From;
            $this->sender   = array($this->mailfrom,$this->fromname);
            $this->reply    = array($this->mailfrom,$this->fromname);

            $query = $db->getQuery(true);
            if(!empty($alert->certificates_badges) && $alert->selection_type == 'selected') {
                if (substr($alert->certificates_badges, 0, 1) == ',') {
                    $alert->certificates_badges = substr($alert->certificates_badges, 1);
                }
                $conditions[] = 'badge_id IN ('.$alert->certificates_badges.')';
            }
            if($alert->interval_amount && $alert->interval_type && $alert->interval_time) {
                if($alert->interval_time == 'after') {
                    $operator = ' - ';
                }
                if($alert->interval_time == 'before') {
                    $operator = ' + ';
                }
                $interval_type = str_replace('s','',$alert->interval_type);
                $conditions[] = "DATE(date_expires) = DATE( NOW() $operator INTERVAL $alert->interval_amount $interval_type )";
            } else {
                $conditions[] = "DATE(date_expires) = DATE( NOW() )";
            }

            $conditions[] = 'date_expires IS NOT NULL';

            $query->select('*')
                  ->from('axs_awards_earned')
                  ->where($conditions);
            $db->setQuery($query);

            $awardsToExpire = $db->loadObjectList();

            foreach($awardsToExpire as $award) {
                $this->clearVariables();
                $this->setAward($award);
                $this->user = JFactory::getUser($award->user_id);
                $params = new stdClass();
                $params->subject = $this->replacePlaceholders($alertParams->subject);
                $params->message = $this->replacePlaceholders($alertParams->certificate_expiration_message);
                $this->addToEmailQueue($params);
            }
        }
    }

    public function runDailyBatch() {
        $this->getDailyCoursesDueAlerts();
        $this->getDailyAssignmentsDueAlerts();
        $this->getDailyExpirationAlerts();
    }
}