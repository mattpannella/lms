<?php

defined('_JEXEC') or die;

interface AxsExternalApi {
    public static function getInstance();

    public function create($endpoint, $params);
    public function read($endpoint, $options = NULL);
    public function update($endpoint, $params);
    public function delete($endpoint, $options = NULL);

    public function execute($url, $bodyParams = NULL, $requestVerb = 'GET');
}