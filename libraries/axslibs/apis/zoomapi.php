<?php

defined('_JEXEC') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class AxsZoomApi extends AxsJwtApi implements AxsExternalApi {
    private $baseUrl;

    const USER_TYPE_BASIC = 1;
    const USER_TYPE_LICENSED = 2;

    // Meeting Constants
    const MEETING_SCHEDULE_INSTANT = 1;
    const MEETING_SCHEDULE_DEFERRED = 2;

    // @TODO: Implement these constants when building out recurrent Zoom meetings at some point in the future.
    const MEETING_SCHEDULE_RECURRING_NO_TIME = 3;
    const MEETING_SCHEDULE_RECURRING_FIXED_TIME = 8;

    private function __construct() {
        $this->baseUrl = "https://api.zoom.us/v2/";

        $currentBrand = AxsBrands::getBrand();

        if($currentBrand->api_settings->zoom_integration == 'yes') {

            $this->credentials = [
                'apiPublicKey' => $currentBrand->api_settings->zoom_api_public_key,
                'apiSecretKey' => AxsEncryption::decrypt($currentBrand->api_settings->zoom_api_secret_key, AxsKeys::getKey('lms'))
            ];
        } else {
            // We're not using the Zoom API, so return null
            return null;
        }
    }

    public static function getInstance() {
        static $instance;

        if(empty($instance)) {
            $instance = new AxsZoomApi();
        }

        return $instance;
    }

    public static function validApiKeys($api_public_key = null, $api_secret_key = null) {
        $api = new AxsZoomApi();

        if ($api_public_key && $api_secret_key) {
            $api->credentials = [
                'apiPublicKey' => $api_public_key,
                'apiSecretKey' => $api_secret_key
            ];
        }

        $options = new stdClass();
        $options->page_size = 5;
        $options->status = 'active';

        $req = json_decode($api->read('users', $options), false);

        // using response code 124 because that's the response for when the token
        // is invalid, which means the api key combination are invalid
        return ($req->code !== 124);
    }

    public function execute($url, $bodyParams = NULL, $requestVerb = 'GET') {
        $curl = curl_init();

        $token = $this->createToken();

        $mutableVerbs = ['POST', 'PATCH', 'PUT'];

        // Force uppercase in case a non-uppercased verb comes in
        $requestVerb = strtoupper($requestVerb);

        $curlOptions = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 20,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $requestVerb
        ];

        if($this->apiType == 'JWT') {
            $curlOptions[CURLOPT_HTTPHEADER] = [
                "Authorization: Bearer $token",
                "Content-Type: application/json"
            ];
        }

        if(in_array($requestVerb, $mutableVerbs)) {
            $curlOptions[CURLOPT_POSTFIELDS] = json_encode($bodyParams);
            $curlOptions[CURLOPT_POST] = true;
        }

        curl_setopt_array($curl, $curlOptions);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

    private function buildApiCallURL($endpoint, $options = NULL) {

        return $this->baseUrl . $endpoint . (!empty($options) ? ('?' . http_build_query($options)) : '');
    }

    public function create($endpointName, $params) {
        $url = $this->buildApiCallURL($endpointName);

        return $this->execute($url, $params, 'POST');
    }

    public function read($endpointName, $options = NULL) {
        $url = $this->buildApiCallURL($endpointName, $options);

        return $this->execute($url, NULL, 'GET');
    }

    public function update($endpointName, $params) {
        $url = $this->buildApiCallURL($endpointName);

        return $this->execute($url, $params, 'PATCH');
    }

    public function delete($endpointName, $options = NULL) {
        $url = $this->buildApiCallURL($endpointName);

        $this->execute($url, NULL, 'DELETE');
    }
}
