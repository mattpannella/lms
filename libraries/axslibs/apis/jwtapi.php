<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

defined('_JEXEC') or die;

class AxsJwtApi {
    protected $credentials = [];
    protected $apiType = 'JWT';

    protected function createToken() {
        $jwtData = [
            'header' => [
                'alg' => 'HS256',
                'typ' => 'JWT'
            ],
            'payload' => [
                'iss' => $this->credentials['apiPublicKey'],
                'exp' => time() + 20    // Corresponds to the 20-second API call timeout
            ]
        ];

        $header = $this->base64url_encode(json_encode($jwtData['header']));
        $payload = $this->base64url_encode(json_encode($jwtData['payload']));

        $hashData = sprintf("%s.%s", $header, $payload);
        $hashKey = $this->credentials['apiSecretKey'];

        $signature = $this->base64url_encode(hash_hmac('sha256', $hashData, $hashKey, true));

        $token = sprintf("%s.%s", $hashData, $signature);

        return $token;
    }

    private function base64url_encode($data) {
        $b64 = base64_encode($data);

        // Make sure you get a valid result, otherwise, return FALSE, as the base64_encode() function do
        if ($b64 === false) {
            return false;
        }

        // Convert Base64 to Base64URL by replacing “+” with “-” and “/” with “_”
        $encodedData = strtr($b64, '+/', '-_');

        // Remove padding character from the end of line and return the Base64URL result
        return rtrim($encodedData, '=');
    }
}