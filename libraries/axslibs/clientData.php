<?php

defined('_JEXEC') or die;

class AxsClientData {

    public static $months = array(
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December',
    );

    public static function getClientStatus() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        if($db_manager->config->status) {
            return $db_manager->config->status;
        } else {
            return "active";
        }
    }

    public static function getClientExpiration() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        return $db_manager->config->expires;
    }

    public static function getClientName() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        if($db_manager->config->client_name) {
            return $db_manager->config->client_name;
        } else {
            return str_replace('site_','', $db_manager->config->dbname);
        }
    }

    public static function getClientOnboardingChecklist() {
        if(isset($_SESSION['onboarding_checklist'])) {
            return $_SESSION['onboarding_checklist'];
        } else {
            return false;
        }
    }

    public static function getClientSubscription() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        if($db_manager->config->status) {
            return $db_manager->config->subscription_type;
        } else {
            return "Tovuti Pro";
        }
    }

    public static function getClientSubscriptionFromDb() {
        $db = AxsDbAccess::getDbo();
        $db->setQuery("SELECT subscription_type FROM axs_dbmanager WHERE id = " . $db->quote(AxsClients::getClientId()));
        $result = $db->loadObject();
        return $result->subscription_type;
    }

    public static function getClientDbName() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        $clientParams = json_decode($db_manager->config->params);
        if($clientParams->dbname) {
            return $clientParams->dbname;
        } else {
            return false;
        }
    }

    public static function getClientVirtualMeetingServer() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        $clientParams = json_decode($db_manager->config->params);
        if($clientParams->virtual_meeting_server) {
            return $clientParams->virtual_meeting_server;
        } else {
            return false;
        }
    }

    public static function getClientMaxVirtualClassroomParticipants() {
        $cookie_params = AxsClients::getCookieParams();
        $db_manager = AxsEncryption::decrypt($_COOKIE[$cookie_params->name], $cookie_params->key);
        if(!$db_manager->client) {
            return 0;
        }
            // Parse the params if we have access to the db_manager object
        $dbConfigParams = json_decode($db_manager->client);
        if($dbConfigParams->vc_participants_limit) {
            return $dbConfigParams->vc_participants_limit;
        } else {
            // Otherwise, use unlimited participants as a default limit just in case
            return -1;
        }
    }

    public static function getIds($ids,$dbName) {
        $list = implode(',',$ids);
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("id");
        $query->from($db->quoteName($dbName).'.'.$db->quoteName('joom_usergroups'));
        $query->where("parent_id IN ($list)");
        $db->setQuery($query);
        $results = $db->loadColumn();
        if($results) {
            $results = array_map('intval', $results);
        }
        return $results;
    }

    public static function getAdminIds($dbName) {
        $i = 0;
        $adminIds = [20];
        while($i < 50) {
            if($i < 1) {
            $idList = $adminIds;
            }
            $idList = self::getIds($idList,$dbName);
            if(!$idList) {
                break;
            } else {
                $adminIds = array_merge($adminIds,$idList);
            }
            $i++;
        }
        $adminIds = array_unique($adminIds);
        return $adminIds;
    }

    public static function getClientDatabaseNames() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("id,client_name,status,subscription_type,params,dbparams");
        $query->from("root_db.axs_dbmanager");
        $query->where("enabled = 1");
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $result) {
            $params = AxsClients::decryptClientParams($result->dbparams);
            $result->databaseName = $params->dbname;
            unset($result->dbparams);
        }
        return $results;
    }

    public static function getClientChecklists() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("client.id as clientId,client.client_name,client.status,client.subscription_type,client.params as clientParams,client.dbparams,checklist.id as checklistId,checklist.params as checklistParams");
        $query->from("axs_dbmanager as client");
        $query->join("LEFT",$db->quoteName('axs_admin_checklists', 'checklist') . ' ON ' . $db->quoteName('client.id') . ' = ' . $db->quoteName('checklist.client_id'));
        $query->where("client.enabled = 1");
        $query->order("client.id DESC");
        $db->setQuery($query);
        $results = $db->loadObjectList();
        foreach($results as $result) {
            if(!$result->client_name) {
                $params = AxsClients::decryptClientParams($result->dbparams);
                $result->client_name = $params->dbname;
                unset($result->dbparams);
            }
        }
        return $results;
    }

    public static function getClientById($id) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select("client.id as clientId,client.client_name,client.status,client.subscription_type,client.params as clientParams,client.dbparams,checklist.id as checklistId,checklist.params as checklistParams");
        $query->from("axs_dbmanager as client");
        $query->join("LEFT",$db->quoteName('axs_admin_checklists', 'checklist') . ' ON ' . $db->quoteName('client.id') . ' = ' . $db->quoteName('checklist.client_id'));
        $query->where("client.id = ".(int)$id);
        $query->setLimit(1);
        $db->setQuery($query);
        $result = $db->loadObject();
        if(!$result->client_name) {
            $params = AxsClients::decryptClientParams($result->dbparams);
            $result->client_name = $params->dbname;
            unset($result->dbparams);
        }

        return $result;
    }


    public static function getClientUsers() {
        $db = JFactory::getDbo();
        $clientDatabases = self::getClientDatabaseNames();
        $clientData = array();
        foreach($clientDatabases as $client) {
            $exists = self::checkIfDatabaseExists($client->databaseName);
            if($exists) {
                $adminIds = self::getAdminIds($client->databaseName);
                $ids = implode(',',$adminIds);
                $query = $db->getQuery(true);
                $query->select("u.id,u.name,u.email,u.registerDate,
                CASE WHEN u.productEmails = 1 THEN 'Yes' ELSE 'No' END AS productEmails,
                g.*");
                $query->from($db->quoteName($client->databaseName).".".$db->quoteName('joom_user_usergroup_map')." as g");
                $query->join('INNER',$db->quoteName($client->databaseName).".".$db->quoteName('joom_users')." as u ON u.id = g.user_id");
                $query->where("g.group_id IN ($ids)");
                $query->where("u.email NOT LIKE '%tovutiteam.com%'");
                $query->where("u.email NOT LIKE '%axsteam.com%'");
                $query->where("u.block = 0");
                $query->group("u.id");
                $db->setQuery($query);
                $results = $db->loadObjectList();
                $client->users = $results;
            }
            array_push($clientData,$client);
        }

        return $clientData;
    }


    public static function getClientUsage() {
        $db = JFactory::getDbo();
        $clientDatabases = self::getClientDatabaseNames();
        $clientData = array();
        foreach($clientDatabases as $client) {
            $exists = self::checkIfDatabaseExists($client->databaseName);
            if($exists) {
                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_jbusinessdirectory_companies');
                $db->setQuery($query);
                $businessDirectoryListings = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.axs_contracts');
                $db->setQuery($query);
                $contracts = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.axs_affiliate_dashboards');
                $db->setQuery($query);
                $affiliateDashboards = $db->loadObject();

                //must be more than one
                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.axs_rewards_transactions');
                $db->setQuery($query);
                $rewardsData = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_eshop_products');
                $db->setQuery($query);
                $eshopProducts = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.axs_gantt_charts');
                $db->setQuery($query);
                $ganttCharts = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.axs_reports_custom');
                $db->setQuery($query);
                $campaigns = $db->loadObject();

                //must be more than one
                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_community_activities');
                $db->setQuery($query);
                $communityActivity = $db->loadObject();

                //must be more than one
                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_post');
                $db->setQuery($query);
                $blogPosts = $db->loadObject();


                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_comment');
                $db->setQuery($query);
                $blogComments = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_feeds');
                $db->setQuery($query);
                $blogFeeds = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_subscriptions');
                $db->setQuery($query);
                $blogSubscriptions = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_category_subscriptions');
                $db->setQuery($query);
                $blogCategorySubscriptions = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_blogger_subscriptions');
                $db->setQuery($query);
                $blogBloggerSubscriptions = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_easyblog_team');
                $db->setQuery($query);
                $blogTeams = $db->loadObject();

                $query = $db->getQuery(true);
                $query->select('COUNT(*) as total');
                $query->from($db->quoteName($client->databaseName).'.joom_kunena_topics');
                $db->setQuery($query);
                $forumTopics = $db->loadObject();

                $client->business_directory = (int)$businessDirectoryListings->total;
                $client->contracts = (int)$contracts->total;
                $client->affiliate_dashboards = (int)$affiliateDashboards->total;
                if($rewardsData->total == 0) {
                    $rewardsData->total = 1;
                }
                $client->rewards = (int)$rewardsData->total - 1;

                $client->ecommerce_products = (int)$eshopProducts->total;
                $client->gant_charts = (int)$ganttCharts->total;
                $client->campaigns = (int)$campaigns->total;
                if($communityActivity->total == 0) {
                    $communityActivity->total = 1;
                }
                $client->community_activity = (int)$communityActivity->total - 1;
                if($blogPosts->total == 0) {
                    $blogPosts->total = 1;
                }
                $client->blog_posts = (int)$blogPosts->total - 1;
                $client->blog_comments = (int)$blogComments->total;
                $client->blogFeeds = (int)$blogFeeds->total;
                $client->blogSubscriptions = (int)$blogSubscriptions->total;
                $client->blogCategorySubscriptions = (int)$blogCategorySubscriptions->total;
                $client->blogBloggerSubscriptions = (int)$blogBloggerSubscriptions->total;
                $client->blogTeams = (int)$blogTeams->total;

                $client->forum_topics = (int)$forumTopics->total;

                array_push($clientData,$client);
            }

        }

        return $clientData;
    }

    public static function getClientActiveUsers($year = null) {
        $months = self::$months;
        if(!$year) {
            $year = date('Y');
        }
        $db = JFactory::getDbo();
        $clientDatabases = self::getClientDatabaseNames();
        $clientData = array();
        foreach($clientDatabases as $client) {
            $exists = self::checkIfDatabaseExists($client->databaseName);
            if($exists) {
                $data              = new stdClass();
                $data->client_id   = $client->id;
                $data->client_name = $client->client_name;
                foreach($months as $month) {
                    $query = $db->getQuery(true);
                    $query->select('COUNT(DISTINCT user_id) as total');
                    $query->from($db->quoteName($client->databaseName).'.axs_actions');
                    $query->where("YEAR(date) = ".$db->quote($year)." AND MONTHNAME(date) = ".$db->quote($month));
                    $query->where($db->quoteName('action')." = 'login'");
                    //echo $query; die();
                    $db->setQuery($query);
                    $userLogins = $db->loadObject();
                    $data->$month = $userLogins->total;
                }
                array_push($clientData,$data);
            }
        }
        return $clientData;
    }

    public static function getClientCostUsage($year = null) {
        $months = self::$months;
        if(!$year) {
            $year = date('Y');
        }
        $db = JFactory::getDbo();
        $clientDatabases = self::getClientDatabaseNames();
        $clientData = array();
        foreach($clientDatabases as $client) {
            $exists = self::checkIfDatabaseExists($client->databaseName);
            if($exists) {
                $data              = new stdClass();
                $data->client_id   = $client->id;
                $data->client_name = $client->client_name;
                foreach($months as $month) {
                    $query = $db->getQuery(true);
                    $query->select('COUNT(id) as total');
                    $query->from($db->quoteName($client->databaseName).'.axs_virtual_classroom_attendees');
                    $query->where("YEAR(date) = ".$db->quote($year)." AND MONTHNAME(date) = ".$db->quote($month));
                    $db->setQuery($query);
                    $vcAttendees = $db->loadObject();
                    $attendeesTotal = $vcAttendees->total;

                    $query = $db->getQuery(true);
                    $query->select('COUNT(DISTINCT meeting_id) as total');
                    $query->from($db->quoteName($client->databaseName).'.axs_virtual_classroom_attendees');
                    $query->where("YEAR(date) = ".$db->quote($year)." AND MONTHNAME(date) = ".$db->quote($month));
                    $db->setQuery($query);
                    $meetings = $db->loadObject();
                    $meetingsTotal = $meetings->total;

                    $data->$month = 'Meetings: '.$meetingsTotal.' Attendees: '.$attendeesTotal;
                }
                array_push($clientData,$data);
            }
        }
        return $clientData;
    }


    public static function getClientMonthlyVirtualAttendees($year = null) {
        $months = self::$months;
        if(!$year) {
            $year = date('Y');
        }
        $db = JFactory::getDbo();
        $clientDatabases = self::getClientDatabaseNames();
        $clientData = array();
        foreach($clientDatabases as $client) {
            $exists = self::checkIfDatabaseExists($client->databaseName);
            if($exists) {
                $data              = new stdClass();
                $data->client_id   = $client->id;
                $data->client_name = $client->client_name;
                foreach($months as $month) {
                    $query = $db->getQuery(true);
                    $query->select('COUNT(id) as total');
                    $query->from($db->quoteName($client->databaseName).'.axs_virtual_classroom_attendees');
                    $query->where("YEAR(date) = ".$db->quote($year)." AND MONTHNAME(date) = ".$db->quote($month));
                    //echo $query; die();
                    $db->setQuery($query);
                    $userLogins = $db->loadObject();
                    $data->$month = $userLogins->total;
                }
                array_push($clientData,$data);
            }
        }
        return $clientData;
    }

    public static function checkIfDatabaseExists($databaseName) {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select($db->quote('SCHEMA_NAME'));
        $query->from($db->quoteName('INFORMATION_SCHEMA.SCHEMATA'));
        $query->where($db->quoteName('SCHEMA_NAME').' = '.$db->quote($databaseName));
        $db->setQuery($query);
        $results = $db->loadObject();
        if($results) {
            return true;
        } else {
            return false;
        }
    }
}