<?php

defined('_JEXEC') or die;

class AxsAnalytics {

	public static $dateFormat = "Y-m-d h:i:s";

	public static function getDateRange() {
		$db = JFactory::getDBO();
		$query = "SELECT date FROM axs_daily_stats";
		$db->setQuery($query);

		$results = $db->loadObjectList();

		$dates = array();

		for ($i = 0; $i < count($results); $i++) {
			if ($results[$i]->date != null) {
				array_push($dates, strtotime($results[$i]->date));
			}
		}

		$range = new stdClass();

		$range->first = new stdClass();
		$range->last = new stdClass();

		$range->first->string = date(self::$dateFormat, min($dates));
		$range->first->datecode = min($dates);
		$range->last->string = date(self::$dateFormat, max($dates));
		$range->last->datecode = max($dates);

		return $range;
	}

	private static function getData($selector,  $startDate = null, $endDate = null, $min = false) {
		$db = JFactory::getDBO();

		$conditions = self::getDateConditions($db, $startDate, $endDate);

		if ($min) {
			$table = "axs_daily_stats_min";
		} else {
			$table = "axs_daily_stats";
		}

		$query = "SELECT " . $selector . " FROM $table " . $conditions . " ORDER BY date ASC";
		$db->setQuery($query);

		$results = $db->loadObjectList();

		return $results;
	}

	public static function getAllData($startDate = null, $endDate = null, $results, $uncap = true) {
		if (!$results) {
			$results = self::getData("*", $startDate, $endDate);
		}

		if ($uncap) {
			ini_set('memory_limit', '-1');
		}

		$count = count($results);

		$data = [];

		for ($i = 0; $i < $count; $i++) {

			$dat = $results[$i];

			$new = new stdClass();
			$new->id = $dat->id;
			$new->date = $dat->date;
			$new->site_visits = json_decode($dat->site_visits);
			$new->members_new = json_decode($dat->members_new);
			$new->cancels = json_decode($dat->cancels);
			$new->membership_current = json_decode($dat->membership_current);
			$new->revenue = json_decode($dat->revenue);
			$new->refunds = json_decode($dat->refunds);
			//$new->courses = json_decode($dat->courses);
			$new->certificates = json_decode($dat->certificates);
			//$new->reengages = json_decode($dat->reengages);
			//$new->downgrades = json_decode($dat->downgrades);
			$new->cc_declines = json_decode($dat->cc_declines);
			$new->cc_fails = json_decode($dat->cc_fails);
			$new->cc_recoveries = json_decode($dat->cc_recoveries);

			array_push($data, $new);
		}

		return $data;
	}

	public static function getDashboardData($startDate = null, $endDate = null) {
		$result = self::getData("*", $startDate, $endDate, true);

		return self::getAllData($startDate, $endDate, $result, false);
	}

	public static function getSiteVisits($startDate = null, $endDate = null) {
		$results = self::getData('date, site_visits', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->site_visits = json_decode($results[$i]->site_visits);
		}

		return $results;
	}

	public static function getNewMembers($startDate = null, $endDate = null) {
		$results = self::getData('date, members_new', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->members_new = json_decode($results[$i]->members_new);
		}

		return $results;
	}

	public static function getCurrentMembership($startDate = null, $endDate = null) {
		$results = self::getData('date, membership_current', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->membership_current = json_decode($results[$i]->membership_current);
		}

		return $results;
	}

	public static function getRevenue($startDate = null, $endDate = null) {
		$results = self::getData('date, revenue', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->revenue = json_decode($results[$i]->revenue);
		}

		return $results;
	}

	public static function getRefunds($startDate = null, $endDate = null) {
		$results = self::getData('date, refunds', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->refunds = json_decode($results[$i]->refunds);
		}

		return $results;
	}

	public static function getReengages($startDate = null, $endDate = null) {
		$results = self::getData('date, reengages', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->reengages = json_decode($results[$i]->reengages);
		}

		return $results;
	}

	public static function getDowngrades($startDate = null, $endDate = null) {
		$results = self::getData('date, downgrades', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->downgrades = json_decode($results[$i]->downgrades);
		}

		return $results;
	}

	public static function getPaymentDeclines($startDate = null, $endDate = null) {
		$results = self::getData('date, cc_declines', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->cc_declines = json_decode($results[$i]->cc_declines);
		}

		return $results;
	}

	public static function getPaymentRecoveries($startDate = null, $endDate = null) {
		$results = self::getData('date, cc_recoveries', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->cc_recoveries = json_decode($results[$i]->cc_recoveries);
		}

		return $results;
	}

	public static function getPaymentFails($startDate = null, $endDate = null) {
		$results = self::getData('date, cc_fails', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->cc_fails = json_decode($results[$i]->cc_fails);
		}

		return $results;
	}

	public static function getCancels($startDate = null, $endDate = null) {
		$results = self::getData('date, cancels', $startDate, $endDate);

		for ($i = 0; $i < count($results); $i++) {
			$results[$i]->cancels = json_decode($results[$i]->cancels);
		}

		return $results;
	}

	private static function getDateConditions($db, $startDate = null, $endDate = null) {

		if ($startDate) {
			if (!is_string($startDate)) {
				$startDate = date(self::$dateFormat, $startDate);
			}
		}

		if ($endDate) {
			if (!is_string($endDate)) {
				$endDate = date(self::$dateFormat, $endDate);
			}
		}

		//Start date but no end date
		if ($startDate && !$endDate) {			
			$conditions = " WHERE DATE(date) >= DATE('" . $startDate. "') ";

		//End date but no start date
		} else if (!$startDate && $endDate) {
			$conditions = " WHERE DATE(date) <= DATE('" . $endDate. "') ";

		//Both start date and end date
		} else if ($startDate && $endDate) {
			$conditions = " WHERE DATE(date) >= DATE('" . $startDate. "') AND DATE(date) <= DATE('" . $endDate . "')";

		//No start or end date	
		} else {
			$conditions = "";

		}

		return $conditions;
	}

	public function getAllCourses() {        
        $db = JFactory::getDBO();
        $conditions[] = "enabled = 1";
        $query = $db->getQuery(true);
        $query->select('*')
              ->from('#__splms_courses')
              ->where($conditions);
        $db->setQuery($query);
        $result = $db->loadObjectList();
        return $result;
    }
}

?>