<?php

defined('_JEXEC') or die;
/* error_reporting(E_ALL);
ini_set('display_errors', 1); */

require_once("builder-db.php");

if (!class_exists('AxsKeys')) {
    require_once("/var/www/html/libraries/axslibs/keys.php");
}


class TovutiBuilder {
    public static function init() {

        if (!isset($_SESSION['aws_creation']) || $_SESSION['aws_creation'] == null || !isset($_SESSION['aws_creation']['initialized'])) {

            if (!isset($_SESSION['aws_creation'])) {
                $_SESSION['aws_creation'] = array();
            }

            $_SESSION['aws_creation']['initialized'] = true;

            $code = strtotime('now') . "_" . rand(100000, 999999);
            $db = JFactory::getDBO();

            $query = $db->getQuery(true);

            $columns = array('id', 'status', 'date');
            $values = array(
                $db->quote($code),
                $db->quote('starting'),
                'NOW()'
            );

            $query
                ->insert($db->quoteName("axs_aws_status_update"))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $db->execute();

            $config = dbCreds::getAwsConfig();


            $_SESSION['aws_creation']['root_domain'] = $config->root_domain;

            $_SESSION['aws_creation']['code'] = $code;
            $_SESSION['aws_creation']['stage'] = 1;

            $_SESSION['aws_creation']['log'] = "awslogs/" . $code . ".log";
            $_SESSION['aws_creation']['plog'] = "awslogs/" . $code . "_prog.log";

            $log = fopen($_SESSION['aws_creation']['log'], "w");
            $prog = fopen($_SESSION['aws_creation']['plog'], "w");

            self::updateStatus("Starting build at: " . strtotime('now') . " - " . date("Y-m-d H:i:s"));

            self::updateStatus("initialized");
        }

        if ($_SESSION['aws_creation']['stage'] == -1) {
            //It intially errored, let them try again and restart.
            $_SESSION['aws_creation']['stage'] = 1;
        }

        self::updateProgress();
    }

    public static function getRootDomainName() {
        $return = new stdClass();
        $config = dbCreds::getAwsConfig();

        $return->domain = $config->root_domain;
        return $return;
    }


    //This can be used locally, or as an ajax call.
    public static function validateDomainName($name = null) {

        $return = new stdClass();

        if (!$name) {
            $name = $_SESSION['aws_creation']['site_domain'];
        }

        $name = strtolower($name);

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query
            ->select('*')
            ->from($db->quoteName('axs_aws_site_names'))
            ->where($db->quoteName('domain_name') . '=' . $db->quote($name));

        $db->setQuery($query);
        $result = $db->loadObject();

        if ($result) {
            $return->success = false;
            $return->message = "The Sub Domain '$name' is already in use.";
            return $return;
        }

        if (!$name) {
            $return->success = false;
            $return->message = "No Sub Domain supplied";
            return $return;
        } else {
            //Domain names can contain letters and numbers as well as hyphens (as long as the first and last character are not hyphens.)

            $first = substr($name, 0, 1);
            $last = substr($name, -1, 1);

            if ($first === "-" || $last === "-") {
                $return->success = false;
                $return->message = "Sub Domain name cannot start or end with a hyphen.";
                return $return;
            }

            if (strlen($name) > 32 || strlen($name) == 0) {
                $return->success = false;
                $return->message = "Sub Domain name must be between 1 and 32 characters in length.";
                return $return;
            }

            if (ctype_alnum($name)) {
                //No need to do any more checks.  It's good.
                $return->success = true;
                return $return;
            } else {
                //Last check.  Take all of the hyphens out of it and see if it is then alpha numeric.   If not, it contains invalid characters.
                $removeHyphen = str_replace("-", "", $name);
                if (ctype_alnum($removeHyphen)) {
                    $return->success = true;
                    return $return;
                } else {
                    $return->success = false;
                    $return->message = "Sub Domain name must only contain numbers, letters, and hyphens";
                    return $return;
                }
            }
        }
    }

    public static function updateStatus($status, $bullet = false, $showTime = false) {

        /*$statusMessage = array(
            "new_account" => "Creating new account.",
            "new_account_success" => "Account established.",
            "shared_instance_locate" => "Finding Shared Instance.",
            "shared_instance_found" => "Shared Instance Found.",
            "shared_instance_fail" => "Shared Instance acquisition failed",
            "dedicated_instance_creation" => "Creating Dedicated Instance.",
            "checking_instance_available" => "Checking for instance availablity.",
            "instance_available" => "Instance available!",
            "checking_instance_running" => "Checking that instance is running.",
            "instance_running" => "Instance is running!",
            "checking_apache_running" => "Checking that Apache software is running.",
            "apache_running" => "Server is running!",
            "creating_dedicated_volume" => "Creating new volume.",
            "dedicated_volume_failure" => "Volume creation failed.",
            "dedicated_volume_success" => "Volume creation complete.",
            "acquire_shared_volume" => "Acquiring shared volume.",
            "acquire_shared_volume_fail" => "Shared volume failure",
            "acquire_shared_volume_success" => "Shared volume acquired.",
            "check_volume_available" => "Checking for volume availability.",
            "volume_available" => "Volume available!",
            "checking_volume_attached" => "Checking that volume is attached to instance.",
            "volume_attached" => "Volume attached!",
            "volume_not_attached" => "Not attached!",
            "mount_volume_to_instance" => "Mounting user Volume to Linux",
            "format_volume" => "Connecting and formatting drive.",
            "create_folders" => "Creating user folders",
            "drive_mounted" => "User Drive Mounted",
            "adding_client_data_to_volume" => "Adding client data to volume.",
            "create_user_database" => "Creating new user database",
            "create_user_database_success" => "MySQL user and database created",
            "add_client_data_to_volume" => "Adding client to volume.",
            "save_user_settings" => "Saving User Data to Tovuti",
            "save_user_settings_success" => "User Data saved to Tovuti!",
            "complete" => "Site created.  Your TOVUTI is ready!<br>How neat is that?",
            "site_button" => "<center><a class='btn btn-lg btn-success' href='$url'><span fa fa-paper-plane></span> Launch " . $_SESSION['aws_creation']['site_name'] . "</a></center>",
            "ip_address" => "IP address = " . $_SESSION['aws_creation']['server_ip']
        );*/

        //$text = "<tr>";
        $text = "" . (int)microtime(true) . " - ";

        if ($bullet) {
            $text .= "&bull; ";
        }


        //$text .= "<td>" . $status . "</td>";
        //$text .= $status;

        $text .= $status;

        if ($showTime) {
            //$text .= "<td>" . date("H:i:s") . "</td>";
            $text .= " - " . date("H:i:s");
        }

        //$text .= "</tr>";

        $text .= "\n";

        if (isset($_SESSION['aws_creation']['log'])) {
            $log = fopen($_SESSION['aws_creation']['log'], "a");
            if ($log) {
                fwrite($log, $text);
                fclose($log);
            }
        }

        /*$status = htmlspecialchars($status, ENT_QUOTES);

        $_SESSION['aws_creation']['status_message'] .= $status . "  " . date("H:i:s") . "\n";

        $db = JFactory::getDBO();
        $query = "UPDATE axs_aws_status_update SET status = '" . $_SESSION['aws_creation']['status_message'] . "' WHERE id = '" . $_SESSION['aws_creation']['code'] . "'";
        $db->setQuery($query);
        $db->execute();*/
    }

    public static function newAccount($params) {

        $db = JFactory::getDBO();

        self::updateStatus("starting new account with params: ");


        $_SESSION['aws_creation']['error_message'] = "";
        $_SESSION['aws_creation']['success_message'] = "";
        $_SESSION['aws_creation']['status_message'] = "";

        if (!isset($_SESSION['aws_creation'])) {
            $_SESSION['aws_creation'] = array();
        }

        foreach ($params as $key => $value) {
            $_SESSION['aws_creation'][$key] = $value;
        }

        $settings = new stdClass();
        $settings->access = $_SESSION['aws_creation']['server_options'];
        $settings = json_encode($settings);
        $_SESSION['aws_creation']['server_options'] = $settings;

        self::createMySQLUser();

        $query = $db->getQuery(true);
        $query
            ->insert($db->quoteName('axs_dbmanager'))
            ->columns($db->quoteName('id'))
            ->values($db->quote(null));

        self::updateStatus("creating new dbmanager entry with query ");

        $db->setQuery($query);
        $db->execute();

        self::updateStatus("created new dbmanager entry");

        $client_id = $db->insertId();
        //$client_encrypt = AxsClients::encryptClientId($client_id);
        $client_encrypt = AxsClients::makeRandomFilePath($client_id);

        $user_id = JFactory::getUser()->id;
        $subscription_id = $_SESSION['aws_creation']['subscription_id'];

        $columns = array(
            'dbmanager_id',
            'user_id',
            'subscription_id'
        );

        $values = array(
            $db->quote($client_id),
            $db->quote($user_id),
            $db->quote($subscription_id)
        );

        $query = $db->getQuery(true);
        $query
            ->insert($db->quoteName('axs_aws_subscriptions'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        $db->setQuery($query);
        $db->execute();

        self::updateStatus("added new subscription");

        //Get the data that was inserted into the TOVUTI site for the user's subscription
        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('joom_users'))
            ->where($db->quoteName('id') . '=' . $db->quote($user_id));

        $db->setQuery($query);

        self::updateStatus("acquired user data");

        $_SESSION['aws_creation']['user_data'] = $db->loadObject();

        $_SESSION['aws_creation']['client_id'] = $client_id;
        $_SESSION['aws_creation']['client_encrypt'] = $client_encrypt;

        #template
        if (isset($_SESSION['aws_creation']['template'])) {
            $key = AxsKeys::getKey('clients');
            $templateParams = AxsEncryption::decrypt(base64_decode($_SESSION['aws_creation']['template']), $key);
            $_SESSION['aws_creation']['template_db'] = $templateParams->dbname;
            $_SESSION['aws_creation']['template_id'] = $templateParams->id;
            $_SESSION['aws_creation']['original_encrypt'] = $templateParams->symlink_path;
        } else {
            $_SESSION['aws_creation']['template_db'] = "site_template";
            $_SESSION['aws_creation']['template_id'] = 114;
            $_SESSION['aws_creation']['original_encrypt'] = AwsDatabase::$original;
        }

        self::setupDatabase();

        $_SESSION['aws_creation']['stage'] = 2;

        self::updateStatus("new account created");
        self::updateProgress();

    }

    public static function copyFiles() {

        $client_id = $_SESSION['aws_creation']['client_id'];
        $encrypt = $_SESSION['aws_creation']['client_encrypt'];
        $template_id = $_SESSION['aws_creation']['template_id'];
        $template_db = $_SESSION['aws_creation']['template_db'];

        //add logic to decrypt, and pull values...
        self::updateStatus("Using Template ID:" . $template_id ." and Template DB:" . $template_db);

        shell_exec("mkdir -p /mnt/shared/" . $client_id . "");
        shell_exec("mkdir -p /mnt/shared/" . $client_id . "/images");
        shell_exec("mkdir -p /mnt/shared/" . $client_id . "/files");
        shell_exec("mkdir -p /mnt/shared/" . $client_id . "/files/spreadsheets");

        shell_exec("ln -s /mnt/shared/" . $client_id . "/images /var/www/html/images/" . $encrypt);
        shell_exec("ln -s /mnt/shared/" . $client_id . "/images /var/www/html/interactive/content/files/" . $encrypt);
        shell_exec("ln -s /mnt/shared/" . $client_id . "/files /var/www/files/users/" . $encrypt);

        shell_exec("chown -R ubuntu:www-data /var/www/html/images/" . $encrypt);
        shell_exec("chown -R ubuntu:www-data /var/www/files/users/" . $encrypt);

        shell_exec("chown -R ubuntu:www-data /mnt/shared/" . $client_id . "/images/*");
        shell_exec("chown -R ubuntu:www-data /mnt/shared/" . $client_id . "/files/*");

        shell_exec("chmod -R 775 /mnt/shared/" . $client_id);

        shell_exec("cp -r /mnt/shared/" . $template_id . "/images/* /mnt/shared/" . $client_id . "/images");

        $_SESSION['aws_creation']['stage'] = 3;

        self::updateStatus("Files Copied");
        self::updateProgress();

    }

    public  static  function setupDatabase(){

        TovutiBuilder::updateStatus("creating user database");

        $newDB = "site_" . $_SESSION['aws_creation']['site_domain'];
        $site_user = $_SESSION['aws_creation']['database_user'];
        $password = $_SESSION['aws_creation']['database_password'];

        $dbCreds = AwsDatabase::getDbInfo();
        $dbInfo = array(
            "dbhost" => $dbCreds->dbhost,
            "dbname" => $newDB
        );
        $db = AwsDatabase::getDBO($dbInfo);
        TovutiBuilder::updateStatus("db variable set up");

        TovutiBuilder::updateStatus("creds: 'setting things up..'");

        $query = "CREATE DATABASE " . $db->quoteName($newDB);

        $db->setQuery($query);
        TovutiBuilder::updateStatus("trying to create site with query ");
        $db->execute();

        //Create the command line credentials for mysql
        $creds = "-h " . $dbCreds->dbhost . " -u " . $dbCreds->dbuser . " -p" . $dbCreds->dbpass;
        TovutiBuilder::updateStatus("using creds: $creds");

        $original_encrypt = $_SESSION['aws_creation']['original_encrypt'];
        $new_encrypt = $_SESSION['aws_creation']['client_encrypt'];
        $original_file = "/tmp/temp_$new_encrypt";
        $new_file = "/tmp/temp2_$new_encrypt";

        $template_db = $_SESSION['aws_creation']['template_db'];

        /*
            1	Dump the template database to a temporary text file
            2	Pass the original file into a string replace to swap out the template's encrypted path for the site's new one
            3	Pass the new file into mysql to create an actual database.
            4	Remove the temporary text file

        */

        $dump_db = "mysqldump $creds --single-transaction --set-gtid-purged=OFF $template_db > $original_file";
        $copy = "cat $original_file | sed 's:$original_encrypt:$new_encrypt:g' > $new_file";
        $create_db = "cat $new_file | mysql $creds $newDB";
        $cleanup = "rm $original_file $new_file";

        TovutiBuilder::updateStatus('Dumping');
        TovutiBuilder::updateStatus('copy');
        TovutiBuilder::updateStatus('create db');
        TovutiBuilder::updateStatus('clean up');


        TovutiBuilder::updateStatus("saving locally");
        shell_exec($dump_db);
        shell_exec($copy);
        shell_exec($create_db);
        shell_exec($cleanup);

        TovutiBuilder::updateStatus("DB Created");

        $newUser = $db->quote($site_user) . "@'%'";

        //Create the user in mysql
        $query = "CREATE USER $newUser IDENTIFIED BY " . $db->quote($password);

        TovutiBuilder::updateStatus("creating new user");

        $db->setQuery($query);
        $db->execute();

        TovutiBuilder::updateStatus("granting privileges");

        //Grant them privileges for their site
        $query = "GRANT ALL PRIVILEGES ON " . $db->quoteName($newDB) . " . * TO $newUser";


        $db->setQuery($query);
        $db->execute();

        TovutiBuilder::updateStatus("user database creation successful");
    }

    public static function saveUserData() {

        self::updateStatus("saving user settings");

        $db = JFactory::getDBO();

        $user_id = JFactory::getUser()->id;

        //self::updateStatus("Gathering user info.");

        //Add their site domain to the list
        $client_name = $_SESSION['aws_creation']['client_name'];
        $subscription_type = $_SESSION['aws_creation']['subscription_type'];
        $status = $_SESSION['aws_creation']['status'];
        $encode = $_SESSION['aws_creation']['client_encrypt'];
        $client_id = $_SESSION['aws_creation']['client_id'];
        $instance_id = $_SESSION['aws_creation']['instance'];
        $volume_id = $_SESSION['aws_creation']['volume'];
        $ip = $_SESSION['aws_creation']['server_ip'];
        $newDB = "site_" . $_SESSION['aws_creation']['site_domain'];
        $site_user = $_SESSION['aws_creation']['database_user'];
        $password = $_SESSION['aws_creation']['database_password'];

        //Create an object that stores what features the user has turned on or off.

        $settings =  $_SESSION['aws_creation']['server_options'];

        //Encrypt the client's parameters
        $dbparams = AxsClients::encryptClientParams($newDB, $site_user, $password);


        //Set Domain


        $site_domain = $_SESSION['aws_creation']['site_domain'];
        $root_domain =  $_SESSION['aws_creation']['root_domain'];
        $domains = $site_domain . "." . $root_domain;

        //Insert the client's data into the db manager
        $defaultExpirationDuration = 14;
        $trialExpiration = '';
        $created = date("Y-m-d H:i:s");
        $modified = date("Y-m-d H:i:s");
        if($status == 'trial') {
            $expires = date("Y-m-d H:i:s",strtotime("now + ".$defaultExpirationDuration." DAYS "));
        }

        $fields = array(
            $db->quoteName('client_name') . '=' . $db->quote($client_name),
            $db->quoteName('subscription_type') . '=' . $db->quote($subscription_type),
            $db->quoteName('status') . '=' . $db->quote($status),
            $db->quoteName('user_id') . '=' . $db->quote($user_id),
            $db->quoteName('server_ip') . '=' . $db->quote($ip),
            $db->quoteName('dbparams') . '=' . $db->quote($dbparams),
            $db->quoteName('dbprefix') . '=' . $db->quote('joom_'),
            $db->quoteName('run_crons') . '= 1',
            $db->quoteName('domains') . '=' . $db->quote($domains),
            $db->quoteName('encode') . '=' . $db->quote($encode),
            $db->quoteName('params') . '=' . $db->quote($settings),
            $db->quoteName('enabled') . '= 1',
            $db->quoteName('template') . '= 0',
            $db->quoteName('created') . '=' . $db->quote($created),
            $db->quoteName('modified') . '=' . $db->quote($modified),
            $db->quoteName('expires') . '=' . $db->quote($expires)
        );

        $query = $db->getQuery(true);
        $query
            ->update($db->quoteName('axs_dbmanager'))
            ->set($fields)
            ->where($db->quoteName('id') . '=' . $db->quote($client_id));

        $db->setQuery($query);
        $db->execute();
        $urls = explode(',',$domains);
        //Create the flags for whether or not the user has a dedicated instance/dedicated volumes.


        $dedicated_instance = 0;

        $dedicated_volume = 0;


        //self::updateStatus("dedicated instance " . json_encode($_SESSION['aws_creation']['dedicated_instance']));
        //self::updateStatus("dedicated volume " . json_encode($_SESSION['aws_creation']['dedicated_instance']));

        $columns = array(
            'dbmanager_id',
            'instance_id',
            'volume_id',
            'dedicated_instance',
            'dedicated_volume',
            'start_date',
            'active'
        );

        $values = array(
            (int)$client_id,
            $db->quote($instance_id),
            $db->quote($volume_id),
            (int)$dedicated_instance,
            (int)$dedicated_volume,
            'NOW()',
            1
        );

        $query = $db->getQuery(true);
        $query
            ->insert($db->quoteName('axs_aws_clients'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        self::updateStatus("clients query: ");
        self::updateStatus("inserting into axs_aws_clients");

        $db->setQuery($query);
        $db->execute();

        self::updateStatus("axs_aws_clients success");




        //Update the already existing instance.$query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_aws_instances'))
            ->where($db->quoteName('id') . '=' . $db->quote($instance_id));

        $db->setQuery($query);
        $loaded_instance = $db->loadObject();
        $loaded_instance->num_shared_users = $loaded_instance->num_shared_users + 1;

        //self::updateStatus("Updating object " . json_encode($loaded_instance));
        self::updateStatus("updating current instance with new data");
        $db->updateObject("axs_aws_instances", $loaded_instance, "id");
        self::updateStatus("updating success");


        $query = $db->getQuery(true);

        $columns = array(
            'domain_name',
            'mysql_user',
            'dbmanager_id'
        );

        $values = array(
            $db->quote($site_domain),
            $db->quote($site_user),
            $db->quote($client_id)
        );

        $query
            ->insert($db->quoteName('axs_aws_site_names'))
            ->columns($db->quoteName($columns))
            ->values(implode(',', $values));

        //Save the site name to the list
        self::updateStatus("saving to axs_aws_site_names");

        $db->setQuery($query);
        $db->execute();

        /*

            Warning, unless the database is specifically specified, ROOT_DB will be updated with any queries after this point.

        */

        //We need root privileges to update their new db.

        $db = AwsDatabase::getDBO();


        $query = $db->getQuery(true);
        $query
            ->update($db->quoteName($newDB . ".axs_brands"))
            ->set($db->quoteName('domains') . '=' . $db->quote($domains));

        self::updateStatus("UPDATE: ");

        self::updateStatus("saving domains to axs_brands");

        $db->setQuery($query);
        $db->execute();

        //Remove any previous entries from the domains table.
        $query = "TRUNCATE TABLE " . $db->quoteName($newDB . ".axs_domains");

        self::updateStatus("truncating table");

        $db->setQuery($query);
        $db->execute();

        $language = $_SESSION['aws_creation']['language'];
        $p = new stdClass();
        $p->language = $language;
        $p = json_encode($p);

        $domain_list = explode(",", $domains);

        $first = true;

        self::updateStatus("saving all domain names to axs_domains");
        foreach ($domain_list as $domain) {

            if ($first) {
                $first = false;
                $default = 1;
            } else {
                $default = 0;
            }

            $columns = array(
                'domain',
                'default_domain',
                'created_date',
                'params'
            );

            $values = array(
                $db->quote($domain),
                (int)$default,
                'NOW()',
                $db->quote($p)
            );

            $query = $db->getQuery(true);
            $query
                ->insert($db->quoteName($newDB . ".axs_domains"))
                ->columns($db->quoteName($columns))
                ->values(implode(',', $values));

            $db->setQuery($query);
            $db->execute();
        }

        //Insert the user into the new site.  To do this, there is a template user set that will be overwritten.
        $temp_user_id = 1620265;

        $user_data = $_SESSION['aws_creation']['user_data'];
        $user_data->id = $temp_user_id;
        self::updateStatus('users');

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName($newDB . ".joom_users"))
            ->where($db->quoteName('id') . '=' . $db->quote($temp_user_id));

        $db->setQuery($query);
        $before = $db->loadObject();

        self::updateStatus("before:\n");

        $x = $db->updateObject($newDB . '.joom_users', $user_data, 'id');
        self::updateStatus('x');

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName($newDB . ".joom_users"))
            ->where($db->quoteName('id') . '=' . $db->quote($temp_user_id));

        $db->setQuery($query);
        $after = $db->loadObject();

        self::updateStatus("after:\n");

        $_SESSION['aws_creation']['stage'] = 4;
        self::updateStatus("user settings saved");

        //create okta sso app
        $domainName  = $domain_list[0];
        if($client_name) {
            $label = $client_name;
        } else {
            $label = $domainName;
        }

        $appParams = new stdClass();
        $appParams->domain = $domainName;
        $appParams->label  = $label;
        $appParams->db     = $db;
        $appParams->clientStatus = $status;
        $appParams->clientDB = $newDB;
        $app = new AxsOkta($appParams);
        $app->createTable();
        $app->createClientApp();

        self::updateProgress();
    }


//    public static function sendCommand($command, $status = false): ?string
//    {
//        if ($status) {
//            TovutiBuilder::updateStatus($command);
//        }
//
//        return shell_exec($command);
//    }


    private static function createMySQLUser() {

        $good = false;
        $failsafe = 1000;
        //Mysql users have a max length of 16 characters.
        $user = substr($_SESSION['aws_creation']['site_domain'], 0, 16);

        $count = 1;
        while (!$good) {

            $db = JFactory::getDBO();
            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from($db->quoteName("axs_aws_site_names"))
                ->where($db->quoteName('mysql_user') . '=' . $db->quote($user));

            $db->setQuery($query);
            $result = $db->loadObject();

            if (!$result) {
                $good = true;
            } else {

                $append = "_$count";
                $count++;

                $user = substr($user, 0, (16 - strlen($append))) . $append;

                if ($count >= $failsafe) {
                    //Can't come up with anything that works.  This always will.
                    $user = "user_" . strtotime('now');
                    $good = true;
                }
            }
        }

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*_";
        $password = substr(str_shuffle($chars), 0, 16);

        $_SESSION['aws_creation']['database_user'] = $user;
        $_SESSION['aws_creation']['database_password'] = $password;

        self::updateStatus("created database user");

        //self::updateStatus("MySql user is $user with Password $password.");
    }


    public static function cleanup() {

        $return = new stdClass();

        self::updateStatus("cleanup");

        $url = "https://" . $_SESSION['aws_creation']['site_domain'] . "." . $_SESSION['aws_creation']['root_domain'];

        //Set the build as completed.

        $db = JFactory::getDBO();

        $sub_id = $_SESSION['aws_creation']['subscription_id'];
        $client_id = $_SESSION['aws_creation']['client_id'];
        $user_id = JFactory::getUser()->id;

        $fields = array(
            $db->quoteName('completed') . ' = 1',
            $db->quoteName('enabled') . ' = 1'
        );

        $conditions = array(
            $db->quoteName('user_id') . '=' . (int)$user_id,
            $db->quoteName('dbmanager_id') . '=' . (int)$client_id,
            $db->quoteName('subscription_id') . '=' . (int)$sub_id
        );

        $query = $db->getQuery(true);
        $query
            ->update($db->quoteName('axs_aws_subscriptions'))
            ->set($fields)
            ->where($conditions);

        self::updateStatus("Query: ");
        $db->setQuery($query);
        $db->execute();

//        self::updateProgress();

        self::updateStatus("success");

        self::updateStatus("Finishing build at: " . strtotime('now') . " - " . date("Y-m-d H:i:s"));


        unset($_SESSION['aws_creation']);
    }


    public static function validateAndPrice(&$settings, $validate = true) {

        //These settings are passed into the $_SESSION, so we need to be sure that there are no "extra" settings that can overwrite or mess up others.
        $accepted_settings = array(
            'account_region',
            'site_name',
            'site_domain',
            'language',
            'server_options',
            'plan_id',
            'client_name',
            'subscription_type',
            'status',
            'template'
        );

        foreach ($settings as $key => $val) {
            if (!in_array($key, $accepted_settings)) {
                unset($settings[$key]);
            }
        }

        $plan_id = AxsEncryption::decrypt($settings['plan_id'], AxsKeys::getKey('subscription'));
        $plan = AxsPayment::getPlanById($plan_id, 0);
        $planParams = json_decode($plan->params);

        if (gettype($settings) === "object") {
            $settings = (array)$settings;
        }

        if (!is_array($settings)) {
            return;
        }

        /*if (!isset($_SESSION['aws_creation'])) {
            $_SESSION['aws_creation'] = array();
        }

        $_SESSION['aws_creation']['validated'] = false;
        $_SESSION['aws_creation']['server_price'] = 0;*/

        $return = new stdClass();
        $return->price = null;
        $return->valid = true;
        $return->message = null;

        //Validate Settings

        if (!$settings) {
            $return->valid = false;
            $return->message = "No settings supplied";
            return $return;
        }

        if ($validate) {

            if (!$settings['site_name']) {
                $return->valid = false;
                $return->message = "No site name was supplied";
                return $return;
            }
        }

        $settings['site_domain'] = strtolower($settings['site_domain']);
        if ($settings['site_domain'] != ""){
            $check = self::validateDomainName($settings['site_domain']);
        }

        if (!$check->success && $validate) {
            $return->valid = false;
            $return->message = $check->message;
            //self::updateStatus($check->message);
            //$_SESSION['aws_creation']['stage'] = -1;
            return $return;
        }


        $settings['account_region'] = "us-west-2";


        $settings['site_name'] = str_replace("'", "", $settings['site_name']);
        $settings['site_name'] = htmlspecialchars($settings['site_name'], ENT_QUOTES);

        //Make sure all server options are only true or false booleans.
        $access = $settings['server_options'];

        if (gettype($access) === "object") {
            $access = (array)$access;
        }

        foreach ($access as &$a) {
            if ($a === "true" || $a === true) {
                $a = true;
            } else {
                $a = false;
            }
        }

        $settings['server_options'] = $access;

        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_saas_server_options'))
            ->where($db->quoteName('published') . '= 1')
            ->order('ordering ASC')
            ->setLimit(1);

        $db->setQuery($query);
        $data = $db->loadObject();

        $storage = json_decode($data->storage);
        $server = json_decode($data->server);
        $location = json_decode($data->location);

        $query = $db->getQuery(true);
        $query
            ->select('*')
            ->from($db->quoteName('axs_saas_apps'))
            ->where($db->quoteName('published') . ' = 1');

        $db->setQuery($query);
        $data = $db->loadObjectList();

        //Make a quick look up list for the apps
        $app_list = array();
        foreach ($data as $datum) {
            if ($datum->setting) {
                $app_list[$datum->setting] = $datum;
            }
        }

        //Calculate Apps
        //var_dump($access, $app_list);

        $app_price = 0;
        foreach ($access as $app => $active) {
            if ($active) {
                if (isset($app_list[$app])) {
                    $app_price += $app_list[$app]->price;
                } else {
                    $return->valid = false;
                    $return->message = "Invalid app " . $app . " set";
                    return $return;
                }
            }
        }

        //Check to be sure that ALL apps are set
        //Do not set an error if one is not set (just set it to false) in case the app exists but is not added to the app store yet.
        foreach ($app_list as $app => $details) {

            if (!isset($access[$app])) {
                $access[$app] = false;
            } else {
                if ($access[$app]) {
                    $access[$app] = true;
                } else {
                    $access[$app] = false;
                }
            }
        }

        //Calculate Server

        $server_price = $plan->default_amount;
        $volume_price = 0;

        $price = new stdClass();

        $price->total = $app_price + $server_price + $volume_price;
        $price->app = $app_price;
        $price->server = $server_price;
        $price->volume = $volume_price;

        $return->price = $price;

        $return->settings = $settings;

        return $return;
    }


    public static function updateProgress() {

        if (!isset($_SESSION['aws_creation']['progress'])) {
            $_SESSION['aws_creation']['progress'] = 0;
        }

        $_SESSION['aws_creation']['progress']++;

        $log = fopen($_SESSION['aws_creation']['plog'], "w");
        fwrite($log, $_SESSION['aws_creation']['progress']);
        fclose($log);
    }

}