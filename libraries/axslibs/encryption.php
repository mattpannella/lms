<?php

defined('_JEXEC') or die;

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class AxsEncryption {

	public static function decrypt_legacy($data, $key) {

		$parts = explode(":", base64_decode($data));
		
		if (count($parts) !== 2) {
			//Must be 2 parts
			return null;
		}

		if (!ctype_xdigit($parts[1])) {
			//Second part must be hexidecimal
			return null;
		}

		$dataEncrypted = $parts[0];
		$iv = hex2bin($parts[1]);
		$encryptHash = hash_pbkdf2("sha256", "0000", $key, 1000, 32, true);
		$dataDecode = openssl_decrypt($dataEncrypted, "aes-256-cbc", $encryptHash, 0, $iv);

		return json_decode($dataDecode);
	}

	public static function encrypt($data, $key, $length = null) {
		//The 3rd argument is the salt. Ideally this would be random, but it's not incredibly important.          
		$cryptoKey  = hash_pbkdf2("sha256", $key, "0000", 1000, 32, true);
		$iv 		= self::createRandomKey($length);
		$ciphertext = openssl_encrypt(json_encode($data), "aes-256-cbc", $cryptoKey, OPENSSL_RAW_DATA, $iv);
		$bundle 	= base64_encode($iv . $ciphertext);
		$hmac 		= hash_hmac('sha256', $bundle, $cryptoKey);
		$payload 	= $hmac . ':' . $bundle;

		return base64_encode($payload);
	}

	public static function decrypt($data, $key) {
		$parts = explode(':', base64_decode($data));
		if (count($parts) !== 2) {
			//Should always be 2 parts.
			return null;
		}
		
		$hmac = $parts[0];
		$bundle = $parts[1];

		$cryptoKey = hash_pbkdf2("sha256", $key, "0000", 1000, 32, true);
		$calculated_hmac = hash_hmac('sha256', $bundle, $cryptoKey);

		if (!hash_equals($hmac, $calculated_hmac)) {
			//Try decrypting with the old system.
			//This shouldn't hinder performance since it should ONLY take place if the data has been tweaked, or if it's an old encryption (such as the images path).
			return self::decrypt_legacy($data, $key);
			//return null;
		}

		$decoded_bundle = base64_decode($bundle);
		$iv = mb_substr($decoded_bundle, 0, 16, '8bit');
		$ciphertext = mb_substr($decoded_bundle, 16, null, '8bit');
		$plaintext = openssl_decrypt($ciphertext, "aes-256-cbc", $cryptoKey, OPENSSL_RAW_DATA, $iv);

		return json_decode($plaintext);
	}

	public static function createRandomKey($length = null) {
		if (!$length) {
			$length = openssl_cipher_iv_length("aes-256-cbc");
		}

		return openssl_random_pseudo_bytes($length);
	}
}