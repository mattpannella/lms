<?php
/**
 * @package     FOF
 * @copyright   2010-2015 Nicholas K. Dionysopoulos / Akeeba Ltd
 * @license     GNU GPL version 2 or later
 */

namespace FOF30\Encrypt;

use FOF30\Utils\Phpfunc;

defined('_JEXEC') or die;

/**
 * A simple implementation of AES-128, AES-192 and AES-256 encryption using the
 * high performance openssl library.
 */
class Aes
{
	/** @var   string  The AES cipher to use (this is an openssl identifier, not the bit strength) */
	private $cipherType = '';

	/** @var   string  The cipher key (password) */
	private $keyString = '';

	/**
	 * Initialise the AES encryption object
	 *
	 * @param   string  $key       The encryption key (password). It can be a raw key (32 bytes) or a passphrase.
	 * @param   int     $strength  Bit strength (128, 192 or 256)
	 * @param   string  $mode      Ecnryption mode. Can be ebc or cbc. We recommend using cbc.
	 */
	public function __construct($key, $strength = 256, $mode = 'cbc')
	{
		$this->keyString = $key;
		$cipherMode = strtoupper($mode);

		switch ($strength)
		{
			case 128:
				if($cipherMode == 'ECB') {
					$this->cipherType = 'aes-128-ecb';
				} elseif($cipherMode == 'CBC') {
					$this->cipherType = 'aes-128-cbc';
				}
			break;

			case 192:
				if($cipherMode == 'ECB') {
					$this->cipherType = 'aes-192-ecb';
				} elseif($cipherMode == 'CBC') {
					$this->cipherType = 'aes-192-cbc';
				}
			break;

			case 256:
			default:
				if($cipherMode == 'ECB') {
					$this->cipherType = 'aes-256-ecb';
				} elseif($cipherMode == 'CBC') {
					$this->cipherType = 'aes-256-cbc';
				}
			break;
		}
	}

	/**
	 * Encrypts a string using AES
	 *
	 * @param   string  $stringToEncrypt  The plaintext to encrypt
	 * @param   bool    $base64encoded    Should I Base64-encode the result?
	 *
	 * @return   string  The cryptotext. Please note that the first 16 bytes of
	 *                   the raw string is the IV (initialisation vector) which
	 *                   is necessary for decoding the string.
	 */
	public function encryptString($stringToEncrypt, $base64encoded = true)
	{
		if (strlen($this->keyString) != 32)
		{
			$key = hash('sha256', $this->keyString, true);
		}
		else
		{
			$key = $this->keyString;
		}

		// Set up the IV (Initialization Vector)
		$iv_size = openssl_cipher_iv_length($this->cipherType);
		$iv = openssl_random_pseudo_bytes($iv_size);

		// Encrypt the data

		// Base64-encoded data doesn't need any encryption options - the openssl_encrypt function will automagically 
		// handle the data without special options.
		$encryptionOptions = $base64encoded ? '' : OPENSSL_RAW_DATA;

		$cipherText = openssl_encrypt($stringToEncrypt, $this->cipherType, $key, $encryptionOptions, $iv);
	
		// Prepend the IV to the ciphertext
		$cipherText = $iv . $cipherText;

		// Return the result
		return $cipherText;
	}

	/**
	 * Decrypts a ciphertext into a plaintext string using AES
	 *
	 * @param   string  $stringToDecrypt  The ciphertext to decrypt. The first 16 bytes of the raw string must contain the IV (initialisation vector).
	 * @param   bool    $base64encoded    Should I Base64-decode the data before decryption?
	 *
	 * @return   string  The plain text string
	 */
	public function decryptString($stringToDecrypt, $base64encoded = true)
	{
		if (strlen($this->keyString) != 32)
		{
			$key = hash('sha256', $this->keyString, true);
		}
		else
		{
			$key = $this->keyString;
		}

		// Get the IV size for the configured cipher type
		$iv_size = openssl_cipher_iv_length($this->cipherType);

		// Extract IV
		$iv = substr($stringToDecrypt, 0, $iv_size);
		$stringToDecrypt = substr($stringToDecrypt, $iv_size);
		
		// Decrypt the data

		// Base64-encoded data doesn't need any encryption options - the openssl_encrypt function will automagically 
		// handle the data without special options.
		$encryptionOptions = $base64encoded ? '' : OPENSSL_RAW_DATA;

		$plainText = openssl_decrypt($stringToDecrypt, $this->cipherType, $key, $encryptionOptions, $iv);

		return $plainText;
	}

	/**
	 * Is AES encryption supported by this PHP installation?
	 *
	 * @return boolean
	 */
	public static function isSupported(Phpfunc $phpfunc = null)
	{
		if (!is_object($phpfunc) || !($phpfunc instanceof $phpfunc))
		{
			$phpfunc = new Phpfunc();
		}

		if (!$phpfunc->function_exists('openssl_cipher_iv_length'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('openssl_random_pseudo_bytes'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('openssl_encrypt'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('openssl_decrypt'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('openssl_get_cipher_methods'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('hash'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('hash_algos'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('base64_encode'))
		{
			return false;
		}

		if (!$phpfunc->function_exists('base64_decode'))
		{
			return false;
		}


		$algorithms = $phpfunc->hash_algos();

		if (!in_array('sha256', $algorithms))
		{
			return false;
		}

		return true;
	}
}
