<?php

$customCardClass = $cardAttributes['class'] ?? null;
$cardClass = "card" . (is_null($customCardClass) ? '' : $customCardClass);
$isTabbed = $form->getAttribute('tabbed', '1')
?>

<div class='<?php echo $cardClass; ?>' <?php echo $idx == 0 && !$isTabbed ? 'style="margin-top:0px"' : '' ?>>
    <div class='card-header bg-transparent'>
        <div class='card-title'><?php echo $cardAttributes['label']; ?></div>
        <div class='card-subtitle mb-2 text-muted'><?php echo $cardAttributes['description']; ?></div>
    </div>

    <div class='card-body'>
        <div class='row'>
            <?php echo $cardFieldsHTML; ?>
        </div>
    </div>
</div>