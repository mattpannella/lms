<?php
/**
 * @package     FrameworkOnFramework
 * @subpackage  form
 * @copyright   Copyright (C) 2010-2016 Nicholas K. Dionysopoulos / Akeeba Ltd. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// Protect from unauthorized access
defined('FOF_INCLUDED') or die;

/**
 * FOFFormColumn
 * A column layout element that contains cards and can be configured via XML within an item view
 *
 * @package  FrameworkOnFramework
 * @since    2.0
 */
final class FOFFormColumn
{
    protected $attributes = [];
    protected $columnElement = null;

    public function __construct(SimpleXMLElement $columnElement) {
        $attrs = $columnElement->attributes();

        // Parse all the attributes into a handy array
        foreach($attrs as $key => $value) {

            $this->attributes[$key] = $value;
        }

        $this->columnElement = $columnElement;
    }

    /**
     * Get a string of responsive class names that are pulled from the layout XML file with the format <column xs="6"
     * sm="6" md="8" lg="8" xl="8">, which correspond to Bootstrap's col-xs-6, etc. This allows for responsive sizing
     * on a per-column basis.
     *
     * @return string String of classes corresponding to the column's sizing attributes
     */
    public function getResponsiveClasses() {

        $columnSizeClasses = [];

        // Build out the responsive column widths
        foreach($this->attributes as $attribute => $value) {

            $columnSizeClasses[] = "col-$attribute-$value";
        }

        $responsiveColumnClasses = implode(' ', $columnSizeClasses);

        return $responsiveColumnClasses;
    }

    /**
     * Get a set of cards that are contained within the column element corresponding to this class.
     *
     * @return Array SimpleXMLElement array of <card></card> layout elements contained within the current column.
     */
    public function getCards() {

        $cards = $this->columnElement->xpath("./card");

        return $cards;
    }
}